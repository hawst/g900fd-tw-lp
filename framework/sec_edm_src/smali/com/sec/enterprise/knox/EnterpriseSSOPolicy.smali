.class public Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;
.super Ljava/lang/Object;
.source "EnterpriseSSOPolicy.java"


# static fields
.field public static final CENTRIFY_SSO_PKG:Ljava/lang/String; = "com.centrify.sso.samsung"

.field public static final INTENT_CONTAINER_ID:Ljava/lang/String; = "containerid"

.field public static final INTENT_SSO_PACKAGE_NAME:Ljava/lang/String; = "packageName"

.field public static final INTENT_SSO_SERVICE_DISCONNECTED:Ljava/lang/String; = "sso.enterprise.container.disconnected"

.field public static final INTENT_SSO_SERVICE_SETUP_SUCCESS:Ljava/lang/String; = "sso.enterprise.container.setup.success"

.field public static final SAMSUNG_SSO_BUNDLE_KEY_CUSTOMER_LOGO:Ljava/lang/String; = "SSO_CUSTOMER_LOGO"

.field public static final SAMSUNG_SSO_BUNDLE_KEY_CUSTOMER_NAME:Ljava/lang/String; = "SSO_CUSTOMER_NAME"

.field public static final SAMSUNG_SSO_BUNDLE_KEY_IDP_CONF_DATA:Ljava/lang/String; = "SSO_IDP_CONF_DATA"

.field public static final SAMSUNG_SSO_BUNDLE_KEY_KRB_CONF_DATA:Ljava/lang/String; = "SSO_KRB_CONF_DATA"

.field public static final SAMSUNG_SSO_PKG:Ljava/lang/String; = "com.sec.android.service.singlesignon"

.field public static final SSO_RESULT_FAILURE:I = 0x1

.field public static final SSO_RESULT_FAILURE_NOT_ENROLLED:I = 0x2

.field public static final SSO_RESULT_SUCCESS:I = 0x0

.field public static final SSO_SAMSUNG_SERVICE_PACKAGE_PATH:Ljava/lang/String; = "/system/preloadedsso/samsungsso.apk_"

.field public static final SSO_SERVICE_PACKAGE_PATH:Ljava/lang/String; = "/system/preloadedsso/ssoservice.apk_"

.field public static final SSO_TYPE_CENTRIFY:Ljava/lang/String; = "centrify"

.field public static final SSO_TYPE_SAMSUNG:Ljava/lang/String; = "samsung"

.field private static gSSOService:Lcom/sec/enterprise/knox/IEnterpriseSSOPolicy;


# instance fields
.field private final TAG:Ljava/lang/String;

.field private final mContainerId:I

.field private mContextInfo:Landroid/app/enterprise/ContextInfo;


# direct methods
.method public constructor <init>(Landroid/app/enterprise/ContextInfo;I)V
    .locals 4
    .param p1, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p2, "containerId"    # I

    .prologue
    .line 334
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 142
    const-string v2, "EnterpriseSSOPolicy"

    iput-object v2, p0, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;->TAG:Ljava/lang/String;

    .line 335
    iput p2, p0, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;->mContainerId:I

    .line 336
    iput-object p1, p0, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    .line 337
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;->getSSOService()Lcom/sec/enterprise/knox/IEnterpriseSSOPolicy;

    move-result-object v1

    .line 338
    .local v1, "ssoService":Lcom/sec/enterprise/knox/IEnterpriseSSOPolicy;
    if-eqz v1, :cond_0

    .line 340
    :try_start_0
    const-string v2, "com.centrify.sso.samsung"

    invoke-interface {v1, p1, p2, v2}, Lcom/sec/enterprise/knox/IEnterpriseSSOPolicy;->setupSSO(Landroid/app/enterprise/ContextInfo;ILjava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 345
    :cond_0
    :goto_0
    return-void

    .line 341
    :catch_0
    move-exception v0

    .line 342
    .local v0, "e":Landroid/os/RemoteException;
    const-string v2, "EnterpriseSSOPolicy"

    const-string v3, "Failed at EnterpriseSSOPolicy API setSSOService-Exception"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public constructor <init>(Landroid/app/enterprise/ContextInfo;ILjava/lang/String;)V
    .locals 4
    .param p1, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p2, "containerId"    # I
    .param p3, "packageName"    # Ljava/lang/String;

    .prologue
    .line 351
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 142
    const-string v2, "EnterpriseSSOPolicy"

    iput-object v2, p0, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;->TAG:Ljava/lang/String;

    .line 352
    iput p2, p0, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;->mContainerId:I

    .line 353
    iput-object p1, p0, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    .line 354
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;->getSSOService()Lcom/sec/enterprise/knox/IEnterpriseSSOPolicy;

    move-result-object v1

    .line 355
    .local v1, "ssoService":Lcom/sec/enterprise/knox/IEnterpriseSSOPolicy;
    if-eqz v1, :cond_1

    .line 357
    if-eqz p3, :cond_1

    .line 359
    :try_start_0
    const-string v2, "com.centrify.sso.samsung"

    invoke-virtual {p3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "com.sec.android.service.singlesignon"

    invoke-virtual {p3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 361
    :cond_0
    invoke-interface {v1, p1, p2, p3}, Lcom/sec/enterprise/knox/IEnterpriseSSOPolicy;->setupSSO(Landroid/app/enterprise/ContextInfo;ILjava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 368
    :cond_1
    :goto_0
    return-void

    .line 364
    :catch_0
    move-exception v0

    .line 365
    .local v0, "e":Landroid/os/RemoteException;
    const-string v2, "EnterpriseSSOPolicy"

    const-string v3, "Failed at EnterpriseSSOPolicy API setSSOService-Exception"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method static declared-synchronized getSSOService()Lcom/sec/enterprise/knox/IEnterpriseSSOPolicy;
    .locals 2

    .prologue
    .line 324
    const-class v1, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;->gSSOService:Lcom/sec/enterprise/knox/IEnterpriseSSOPolicy;

    if-nez v0, :cond_0

    .line 325
    const-string v0, "enterprise_sso_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/IEnterpriseSSOPolicy$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/enterprise/knox/IEnterpriseSSOPolicy;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;->gSSOService:Lcom/sec/enterprise/knox/IEnterpriseSSOPolicy;

    .line 328
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;->gSSOService:Lcom/sec/enterprise/knox/IEnterpriseSSOPolicy;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 324
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public deleteSSOWhiteList(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)I
    .locals 10
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "customerId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)I"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .prologue
    .line 661
    .local p3, "packageName":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "EnterpriseSSOPolicy.deleteSSOWhiteList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 662
    const/4 v6, 0x1

    .line 663
    .local v6, "deleteSSOWhiteListValue":I
    const/4 v9, 0x0

    .line 665
    .local v9, "enterpriseSSOResponseData":Landroid/app/enterprise/EnterpriseResponseData;, "Landroid/app/enterprise/EnterpriseResponseData<*>;"
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;->getSSOService()Lcom/sec/enterprise/knox/IEnterpriseSSOPolicy;

    move-result-object v0

    .line 666
    .local v0, "ssoService":Lcom/sec/enterprise/knox/IEnterpriseSSOPolicy;
    if-nez v0, :cond_0

    .line 667
    const-string v1, "EnterpriseSSOPolicy"

    const-string v2, "SSOService is not yet ready!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v7, v6

    .line 684
    .end local v6    # "deleteSSOWhiteListValue":I
    .local v7, "deleteSSOWhiteListValue":I
    :goto_0
    return v7

    .line 672
    .end local v7    # "deleteSSOWhiteListValue":I
    .restart local v6    # "deleteSSOWhiteListValue":I
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget v2, p0, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;->mContainerId:I

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-interface/range {v0 .. v5}, Lcom/sec/enterprise/knox/IEnterpriseSSOPolicy;->deleteSSOWhiteList(Landroid/app/enterprise/ContextInfo;ILjava/lang/String;Ljava/lang/String;Ljava/util/List;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v9

    .line 674
    if-eqz v9, :cond_1

    .line 675
    invoke-virtual {v9}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v1

    if-nez v1, :cond_2

    .line 676
    invoke-virtual {v9}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v6

    :cond_1
    :goto_1
    move v7, v6

    .line 684
    .end local v6    # "deleteSSOWhiteListValue":I
    .restart local v7    # "deleteSSOWhiteListValue":I
    goto :goto_0

    .line 677
    .end local v7    # "deleteSSOWhiteListValue":I
    .restart local v6    # "deleteSSOWhiteListValue":I
    :cond_2
    invoke-virtual {v9}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 678
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v1
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 681
    :catch_0
    move-exception v8

    .line 682
    .local v8, "e":Landroid/os/RemoteException;
    const-string v1, "EnterpriseSSOPolicy"

    const-string v2, "Failed at EnterpriseSSOPolicy API deleteSSOWhiteList-Exception"

    invoke-static {v1, v2, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public forceReauthenticate(Ljava/lang/String;)I
    .locals 7
    .param p1, "type"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .prologue
    .line 907
    const/4 v2, 0x1

    .line 908
    .local v2, "forceReauthenticateValue":I
    iget-object v5, p0, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v6, "EnterpriseSSOPolicy.forceReauthenticate"

    invoke-static {v5, v6}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 909
    const/4 v1, 0x0

    .line 911
    .local v1, "enterpriseSSOResponseData":Landroid/app/enterprise/EnterpriseResponseData;, "Landroid/app/enterprise/EnterpriseResponseData<*>;"
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;->getSSOService()Lcom/sec/enterprise/knox/IEnterpriseSSOPolicy;

    move-result-object v4

    .line 912
    .local v4, "ssoService":Lcom/sec/enterprise/knox/IEnterpriseSSOPolicy;
    if-nez v4, :cond_0

    .line 913
    const-string v5, "EnterpriseSSOPolicy"

    const-string v6, "SSOService is not yet ready!!!"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 930
    .end local v2    # "forceReauthenticateValue":I
    .local v3, "forceReauthenticateValue":I
    :goto_0
    return v3

    .line 919
    .end local v3    # "forceReauthenticateValue":I
    .restart local v2    # "forceReauthenticateValue":I
    :cond_0
    :try_start_0
    iget-object v5, p0, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget v6, p0, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;->mContainerId:I

    invoke-interface {v4, v5, v6, p1}, Lcom/sec/enterprise/knox/IEnterpriseSSOPolicy;->forceReauthenticate(Landroid/app/enterprise/ContextInfo;ILjava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v1

    .line 920
    if-eqz v1, :cond_1

    .line 921
    invoke-virtual {v1}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v5

    if-nez v5, :cond_2

    .line 922
    invoke-virtual {v1}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v2

    :cond_1
    :goto_1
    move v3, v2

    .line 930
    .end local v2    # "forceReauthenticateValue":I
    .restart local v3    # "forceReauthenticateValue":I
    goto :goto_0

    .line 923
    .end local v3    # "forceReauthenticateValue":I
    .restart local v2    # "forceReauthenticateValue":I
    :cond_2
    invoke-virtual {v1}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_1

    .line 924
    new-instance v5, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v5}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v5
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 927
    :catch_0
    move-exception v0

    .line 928
    .local v0, "e":Landroid/os/RemoteException;
    const-string v5, "EnterpriseSSOPolicy"

    const-string v6, "Failed at EnterpriseSSOPolicy API forceReauthenticate-Exception"

    invoke-static {v5, v6, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public getAppAllowedState(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 7
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "packageName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .prologue
    .line 744
    iget-object v5, p0, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v6, "EnterpriseSSOPolicy.getAppAllowedState"

    invoke-static {v5, v6}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 745
    const/4 v2, 0x0

    .line 746
    .local v2, "getAppAllowedStateValue":Z
    const/4 v1, 0x0

    .line 748
    .local v1, "enterpriseSSOResponseData":Landroid/app/enterprise/EnterpriseResponseData;, "Landroid/app/enterprise/EnterpriseResponseData<*>;"
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;->getSSOService()Lcom/sec/enterprise/knox/IEnterpriseSSOPolicy;

    move-result-object v4

    .line 749
    .local v4, "ssoService":Lcom/sec/enterprise/knox/IEnterpriseSSOPolicy;
    if-nez v4, :cond_0

    .line 750
    const-string v5, "EnterpriseSSOPolicy"

    const-string v6, "SSOService is not yet ready!!!"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 767
    .end local v2    # "getAppAllowedStateValue":Z
    .local v3, "getAppAllowedStateValue":I
    :goto_0
    return v3

    .line 755
    .end local v3    # "getAppAllowedStateValue":I
    .restart local v2    # "getAppAllowedStateValue":Z
    :cond_0
    :try_start_0
    iget-object v5, p0, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget v6, p0, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;->mContainerId:I

    invoke-interface {v4, v5, v6, p1, p2}, Lcom/sec/enterprise/knox/IEnterpriseSSOPolicy;->getAppAllowedState(Landroid/app/enterprise/ContextInfo;ILjava/lang/String;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v1

    .line 757
    if-eqz v1, :cond_1

    .line 758
    invoke-virtual {v1}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v5

    if-nez v5, :cond_2

    .line 759
    invoke-virtual {v1}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    :cond_1
    :goto_1
    move v3, v2

    .line 767
    .restart local v3    # "getAppAllowedStateValue":I
    goto :goto_0

    .line 760
    .end local v3    # "getAppAllowedStateValue":I
    :cond_2
    invoke-virtual {v1}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_1

    .line 761
    new-instance v5, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v5}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v5
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 764
    :catch_0
    move-exception v0

    .line 765
    .local v0, "e":Landroid/os/RemoteException;
    const-string v5, "EnterpriseSSOPolicy"

    const-string v6, "Failed at EnterpriseSSOPolicy API getAppAllowedState-Exception"

    invoke-static {v5, v6, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public getSSOCustomerId(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1, "type"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .prologue
    .line 475
    iget-object v6, p0, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v7, "EnterpriseSSOPolicy.getSSOCustomerId"

    invoke-static {v6, v7}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 476
    const/4 v3, 0x0

    .line 477
    .local v3, "getCustomerIdValue":Ljava/lang/String;
    const/4 v2, 0x0

    .line 479
    .local v2, "enterpriseSSOResponseData":Landroid/app/enterprise/EnterpriseResponseData;, "Landroid/app/enterprise/EnterpriseResponseData<*>;"
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;->getSSOService()Lcom/sec/enterprise/knox/IEnterpriseSSOPolicy;

    move-result-object v5

    .line 480
    .local v5, "ssoService":Lcom/sec/enterprise/knox/IEnterpriseSSOPolicy;
    if-nez v5, :cond_0

    .line 481
    const-string v6, "EnterpriseSSOPolicy"

    const-string v7, "SSOService is not yet ready!!!"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v4, v3

    .line 497
    .end local v3    # "getCustomerIdValue":Ljava/lang/String;
    .local v4, "getCustomerIdValue":Ljava/lang/String;
    :goto_0
    return-object v4

    .line 486
    .end local v4    # "getCustomerIdValue":Ljava/lang/String;
    .restart local v3    # "getCustomerIdValue":Ljava/lang/String;
    :cond_0
    :try_start_0
    iget-object v6, p0, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget v7, p0, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;->mContainerId:I

    invoke-interface {v5, v6, v7, p1}, Lcom/sec/enterprise/knox/IEnterpriseSSOPolicy;->getSSOCustomerId(Landroid/app/enterprise/ContextInfo;ILjava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v2

    .line 487
    if-eqz v2, :cond_1

    .line 488
    invoke-virtual {v2}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v6

    if-nez v6, :cond_2

    .line 489
    invoke-virtual {v2}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v6

    move-object v0, v6

    check-cast v0, Ljava/lang/String;

    move-object v3, v0

    :cond_1
    :goto_1
    move-object v4, v3

    .line 497
    .end local v3    # "getCustomerIdValue":Ljava/lang/String;
    .restart local v4    # "getCustomerIdValue":Ljava/lang/String;
    goto :goto_0

    .line 490
    .end local v4    # "getCustomerIdValue":Ljava/lang/String;
    .restart local v3    # "getCustomerIdValue":Ljava/lang/String;
    :cond_2
    invoke-virtual {v2}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_1

    .line 491
    new-instance v6, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v6}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v6
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 494
    :catch_0
    move-exception v1

    .line 495
    .local v1, "e":Landroid/os/RemoteException;
    const-string v6, "EnterpriseSSOPolicy"

    const-string v7, "Failed at EnterpriseSSOPolicy API getSSOCustomerId-Exception"

    invoke-static {v6, v7, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public isSSOReady(Ljava/lang/String;)Z
    .locals 6
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 1229
    iget-object v4, p0, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "EnterpriseSSOPolicy.isSSOReady"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1230
    const/4 v1, 0x0

    .line 1231
    .local v1, "enterpriseSSOResponseData":Landroid/app/enterprise/EnterpriseResponseData;, "Landroid/app/enterprise/EnterpriseResponseData<*>;"
    const/4 v2, 0x0

    .line 1233
    .local v2, "result":Z
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;->getSSOService()Lcom/sec/enterprise/knox/IEnterpriseSSOPolicy;

    move-result-object v3

    .line 1234
    .local v3, "ssoService":Lcom/sec/enterprise/knox/IEnterpriseSSOPolicy;
    if-eqz v3, :cond_0

    .line 1236
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget v5, p0, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;->mContainerId:I

    invoke-interface {v3, v4, v5, p1}, Lcom/sec/enterprise/knox/IEnterpriseSSOPolicy;->isSSOReady(Landroid/app/enterprise/ContextInfo;ILjava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v1

    .line 1237
    if-eqz v1, :cond_0

    .line 1238
    invoke-virtual {v1}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v4

    if-nez v4, :cond_1

    .line 1239
    invoke-virtual {v1}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 1248
    :cond_0
    :goto_0
    return v2

    .line 1240
    :cond_1
    invoke-virtual {v1}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_0

    .line 1241
    new-instance v4, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v4}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v4
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1244
    :catch_0
    move-exception v0

    .line 1245
    .local v0, "e":Landroid/os/RemoteException;
    const-string v4, "EnterpriseSSOPolicy"

    const-string v5, "Failed at EnterpriseSSOPolicy API isSSOReady-Exception"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public pushSSOData(Ljava/lang/String;Landroid/os/Bundle;)I
    .locals 7
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "dataBundle"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .prologue
    .line 1098
    iget-object v5, p0, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v6, "EnterpriseSSOPolicy.pushSSOData"

    invoke-static {v5, v6}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1099
    const/4 v2, 0x1

    .line 1100
    .local v2, "pushDataValue":I
    const/4 v1, 0x0

    .line 1102
    .local v1, "enterpriseSSOResponseData":Landroid/app/enterprise/EnterpriseResponseData;, "Landroid/app/enterprise/EnterpriseResponseData<*>;"
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;->getSSOService()Lcom/sec/enterprise/knox/IEnterpriseSSOPolicy;

    move-result-object v4

    .line 1103
    .local v4, "ssoService":Lcom/sec/enterprise/knox/IEnterpriseSSOPolicy;
    if-nez v4, :cond_0

    .line 1104
    const-string v5, "EnterpriseSSOPolicy"

    const-string v6, "SSOService is not yet ready!!!"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 1120
    .end local v2    # "pushDataValue":I
    .local v3, "pushDataValue":I
    :goto_0
    return v3

    .line 1109
    .end local v3    # "pushDataValue":I
    .restart local v2    # "pushDataValue":I
    :cond_0
    :try_start_0
    iget-object v5, p0, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget v6, p0, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;->mContainerId:I

    invoke-interface {v4, v5, v6, p1, p2}, Lcom/sec/enterprise/knox/IEnterpriseSSOPolicy;->pushSSOData(Landroid/app/enterprise/ContextInfo;ILjava/lang/String;Landroid/os/Bundle;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v1

    .line 1110
    if-eqz v1, :cond_1

    .line 1111
    invoke-virtual {v1}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v5

    if-nez v5, :cond_2

    .line 1112
    invoke-virtual {v1}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v2

    :cond_1
    :goto_1
    move v3, v2

    .line 1120
    .end local v2    # "pushDataValue":I
    .restart local v3    # "pushDataValue":I
    goto :goto_0

    .line 1113
    .end local v3    # "pushDataValue":I
    .restart local v2    # "pushDataValue":I
    :cond_2
    invoke-virtual {v1}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_1

    .line 1114
    new-instance v5, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v5}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v5
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1117
    :catch_0
    move-exception v0

    .line 1118
    .local v0, "e":Landroid/os/RemoteException;
    const-string v5, "EnterpriseSSOPolicy"

    const-string v6, "Failed at EnterpriseSSOPolicy API pushSSOData-Exception"

    invoke-static {v5, v6, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public setCustomerInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 10
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "companyName"    # Ljava/lang/String;
    .param p3, "logo"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .prologue
    .line 829
    iget-object v1, p0, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "EnterpriseSSOPolicy.setCustomerInfo"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 830
    const/4 v8, 0x1

    .line 831
    .local v8, "setCustomerInfoValue":I
    const/4 v7, 0x0

    .line 833
    .local v7, "enterpriseSSOResponseData":Landroid/app/enterprise/EnterpriseResponseData;, "Landroid/app/enterprise/EnterpriseResponseData<*>;"
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;->getSSOService()Lcom/sec/enterprise/knox/IEnterpriseSSOPolicy;

    move-result-object v0

    .line 834
    .local v0, "ssoService":Lcom/sec/enterprise/knox/IEnterpriseSSOPolicy;
    if-nez v0, :cond_0

    .line 835
    const-string v1, "EnterpriseSSOPolicy"

    const-string v2, "SSOService is not yet ready!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v9, v8

    .line 852
    .end local v8    # "setCustomerInfoValue":I
    .local v9, "setCustomerInfoValue":I
    :goto_0
    return v9

    .line 840
    .end local v9    # "setCustomerInfoValue":I
    .restart local v8    # "setCustomerInfoValue":I
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget v2, p0, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;->mContainerId:I

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-interface/range {v0 .. v5}, Lcom/sec/enterprise/knox/IEnterpriseSSOPolicy;->setCustomerInfo(Landroid/app/enterprise/ContextInfo;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v7

    .line 842
    if-eqz v7, :cond_1

    .line 843
    invoke-virtual {v7}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v1

    if-nez v1, :cond_2

    .line 844
    invoke-virtual {v7}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v8

    :cond_1
    :goto_1
    move v9, v8

    .line 852
    .end local v8    # "setCustomerInfoValue":I
    .restart local v9    # "setCustomerInfoValue":I
    goto :goto_0

    .line 845
    .end local v9    # "setCustomerInfoValue":I
    .restart local v8    # "setCustomerInfoValue":I
    :cond_2
    invoke-virtual {v7}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 846
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v1
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 849
    :catch_0
    move-exception v6

    .line 850
    .local v6, "e":Landroid/os/RemoteException;
    const-string v1, "EnterpriseSSOPolicy"

    const-string v2, "Failed at EnterpriseSSOPolicy API setCustomerInfo-Exception"

    invoke-static {v1, v2, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public setSSOCustomerId(Ljava/lang/String;Ljava/lang/String;)I
    .locals 7
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "customerId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .prologue
    .line 412
    iget-object v5, p0, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v6, "EnterpriseSSOPolicy.setSSOCustomerId"

    invoke-static {v5, v6}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 413
    const/4 v2, 0x1

    .line 414
    .local v2, "setSSOCustomerIdValue":I
    const/4 v1, 0x0

    .line 416
    .local v1, "enterpriseSSOResponseData":Landroid/app/enterprise/EnterpriseResponseData;, "Landroid/app/enterprise/EnterpriseResponseData<*>;"
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;->getSSOService()Lcom/sec/enterprise/knox/IEnterpriseSSOPolicy;

    move-result-object v4

    .line 417
    .local v4, "ssoService":Lcom/sec/enterprise/knox/IEnterpriseSSOPolicy;
    if-nez v4, :cond_0

    .line 418
    const-string v5, "EnterpriseSSOPolicy"

    const-string v6, "SSOService is not yet ready!!!"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 434
    .end local v2    # "setSSOCustomerIdValue":I
    .local v3, "setSSOCustomerIdValue":I
    :goto_0
    return v3

    .line 423
    .end local v3    # "setSSOCustomerIdValue":I
    .restart local v2    # "setSSOCustomerIdValue":I
    :cond_0
    :try_start_0
    iget-object v5, p0, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget v6, p0, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;->mContainerId:I

    invoke-interface {v4, v5, v6, p1, p2}, Lcom/sec/enterprise/knox/IEnterpriseSSOPolicy;->setSSOCustomerId(Landroid/app/enterprise/ContextInfo;ILjava/lang/String;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v1

    .line 424
    if-eqz v1, :cond_1

    .line 425
    invoke-virtual {v1}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v5

    if-nez v5, :cond_2

    .line 426
    invoke-virtual {v1}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v2

    :cond_1
    :goto_1
    move v3, v2

    .line 434
    .end local v2    # "setSSOCustomerIdValue":I
    .restart local v3    # "setSSOCustomerIdValue":I
    goto :goto_0

    .line 427
    .end local v3    # "setSSOCustomerIdValue":I
    .restart local v2    # "setSSOCustomerIdValue":I
    :cond_2
    invoke-virtual {v1}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_1

    .line 428
    new-instance v5, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v5}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v5
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 431
    :catch_0
    move-exception v0

    .line 432
    .local v0, "e":Landroid/os/RemoteException;
    const-string v5, "EnterpriseSSOPolicy"

    const-string v6, "Failed at EnterpriseSSOPolicy API setSSOCustomerId-Exception"

    invoke-static {v5, v6, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public setSSOWhiteList(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)I
    .locals 10
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "customerId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)I"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .prologue
    .line 568
    .local p3, "packageName":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "EnterpriseSSOPolicy.setSSOWhiteList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 569
    const/4 v8, 0x1

    .line 570
    .local v8, "setSSOWhiteListValue":I
    const/4 v7, 0x0

    .line 572
    .local v7, "enterpriseSSOResponseData":Landroid/app/enterprise/EnterpriseResponseData;, "Landroid/app/enterprise/EnterpriseResponseData<*>;"
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;->getSSOService()Lcom/sec/enterprise/knox/IEnterpriseSSOPolicy;

    move-result-object v0

    .line 573
    .local v0, "ssoService":Lcom/sec/enterprise/knox/IEnterpriseSSOPolicy;
    if-nez v0, :cond_0

    .line 574
    const-string v1, "EnterpriseSSOPolicy"

    const-string v2, "SSOService is not yet ready!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v9, v8

    .line 591
    .end local v8    # "setSSOWhiteListValue":I
    .local v9, "setSSOWhiteListValue":I
    :goto_0
    return v9

    .line 579
    .end local v9    # "setSSOWhiteListValue":I
    .restart local v8    # "setSSOWhiteListValue":I
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget v2, p0, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;->mContainerId:I

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-interface/range {v0 .. v5}, Lcom/sec/enterprise/knox/IEnterpriseSSOPolicy;->setSSOWhiteList(Landroid/app/enterprise/ContextInfo;ILjava/lang/String;Ljava/lang/String;Ljava/util/List;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v7

    .line 581
    if-eqz v7, :cond_1

    .line 582
    invoke-virtual {v7}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v1

    if-nez v1, :cond_2

    .line 583
    invoke-virtual {v7}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v8

    :cond_1
    :goto_1
    move v9, v8

    .line 591
    .end local v8    # "setSSOWhiteListValue":I
    .restart local v9    # "setSSOWhiteListValue":I
    goto :goto_0

    .line 584
    .end local v9    # "setSSOWhiteListValue":I
    .restart local v8    # "setSSOWhiteListValue":I
    :cond_2
    invoke-virtual {v7}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 585
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v1
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 588
    :catch_0
    move-exception v6

    .line 589
    .local v6, "e":Landroid/os/RemoteException;
    const-string v1, "EnterpriseSSOPolicy"

    const-string v2, "Failed at EnterpriseSSOPolicy API setSSOWhiteList-Exception"

    invoke-static {v1, v2, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public setupSSO(Ljava/lang/String;)I
    .locals 7
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 1167
    iget-object v5, p0, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v6, "EnterpriseSSOPolicy.setupSSO"

    invoke-static {v5, v6}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1168
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;->getSSOService()Lcom/sec/enterprise/knox/IEnterpriseSSOPolicy;

    move-result-object v4

    .line 1169
    .local v4, "ssoService":Lcom/sec/enterprise/knox/IEnterpriseSSOPolicy;
    const/4 v3, 0x1

    .line 1170
    .local v3, "setupSSOValue":I
    const/4 v1, 0x0

    .line 1172
    .local v1, "enterpriseSSOResponseData":Landroid/app/enterprise/EnterpriseResponseData;, "Landroid/app/enterprise/EnterpriseResponseData<*>;"
    if-eqz v4, :cond_0

    .line 1174
    const/4 v2, 0x0

    .line 1175
    .local v2, "packageName":Ljava/lang/String;
    :try_start_0
    const-string v5, "centrify"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1176
    const-string v2, "com.centrify.sso.samsung"

    .line 1179
    :goto_0
    iget-object v5, p0, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget v6, p0, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;->mContainerId:I

    invoke-interface {v4, v5, v6, v2}, Lcom/sec/enterprise/knox/IEnterpriseSSOPolicy;->setupSSO(Landroid/app/enterprise/ContextInfo;ILjava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v1

    .line 1180
    if-eqz v1, :cond_0

    .line 1181
    invoke-virtual {v1}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v5

    if-nez v5, :cond_2

    .line 1182
    invoke-virtual {v1}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 1191
    .end local v2    # "packageName":Ljava/lang/String;
    :cond_0
    :goto_1
    return v3

    .line 1178
    .restart local v2    # "packageName":Ljava/lang/String;
    :cond_1
    const-string v2, "com.sec.android.service.singlesignon"

    goto :goto_0

    .line 1183
    :cond_2
    invoke-virtual {v1}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_0

    .line 1184
    new-instance v5, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v5}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v5
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1187
    :catch_0
    move-exception v0

    .line 1188
    .local v0, "e":Landroid/os/RemoteException;
    const-string v5, "EnterpriseSSOPolicy"

    const-string v6, "Failed at EnterpriseSSOPolicy API setupSSO-Exception"

    invoke-static {v5, v6, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public unenroll(Ljava/lang/String;)I
    .locals 7
    .param p1, "type"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .prologue
    .line 987
    iget-object v5, p0, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v6, "EnterpriseSSOPolicy.unenroll"

    invoke-static {v5, v6}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 988
    const/4 v3, 0x1

    .line 989
    .local v3, "unenrollValue":I
    const/4 v1, 0x0

    .line 991
    .local v1, "enterpriseSSOResponseData":Landroid/app/enterprise/EnterpriseResponseData;, "Landroid/app/enterprise/EnterpriseResponseData<*>;"
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;->getSSOService()Lcom/sec/enterprise/knox/IEnterpriseSSOPolicy;

    move-result-object v2

    .line 992
    .local v2, "ssoService":Lcom/sec/enterprise/knox/IEnterpriseSSOPolicy;
    if-nez v2, :cond_0

    .line 993
    const-string v5, "EnterpriseSSOPolicy"

    const-string v6, "SSOService is not yet ready!!!"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v4, v3

    .line 1009
    .end local v3    # "unenrollValue":I
    .local v4, "unenrollValue":I
    :goto_0
    return v4

    .line 998
    .end local v4    # "unenrollValue":I
    .restart local v3    # "unenrollValue":I
    :cond_0
    :try_start_0
    iget-object v5, p0, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget v6, p0, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;->mContainerId:I

    invoke-interface {v2, v5, v6, p1}, Lcom/sec/enterprise/knox/IEnterpriseSSOPolicy;->unenroll(Landroid/app/enterprise/ContextInfo;ILjava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v1

    .line 999
    if-eqz v1, :cond_1

    .line 1000
    invoke-virtual {v1}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v5

    if-nez v5, :cond_2

    .line 1001
    invoke-virtual {v1}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v3

    :cond_1
    :goto_1
    move v4, v3

    .line 1009
    .end local v3    # "unenrollValue":I
    .restart local v4    # "unenrollValue":I
    goto :goto_0

    .line 1002
    .end local v4    # "unenrollValue":I
    .restart local v3    # "unenrollValue":I
    :cond_2
    invoke-virtual {v1}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_1

    .line 1003
    new-instance v5, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v5}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v5
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1006
    :catch_0
    move-exception v0

    .line 1007
    .local v0, "e":Landroid/os/RemoteException;
    const-string v5, "EnterpriseSSOPolicy"

    const-string v6, "Failed at EnterpriseSSOPolicy API unenroll-Exception"

    invoke-static {v5, v6, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method
