.class public Lcom/sec/enterprise/knox/auditlog/AuditLog;
.super Ljava/lang/Object;
.source "AuditLog.java"


# static fields
.field public static final ACTION_AUDIT_CRITICAL_SIZE:Ljava/lang/String; = "mdm.intent.action.audit.log.critical.size"

.field public static final ACTION_AUDIT_FULL_SIZE:Ljava/lang/String; = "mdm.intent.action.audit.log.full.size"

.field public static final ACTION_AUDIT_MAXIMUM_SIZE:Ljava/lang/String; = "mdm.intent.action.audit.log.maximum.size"

.field public static final ACTION_DUMP_LOG_RESULT:Ljava/lang/String; = "mdm.intent.action.dump.audit.log.result"

.field public static final ACTION_LOG_EXCEPTION:Ljava/lang/String; = "mdm.intent.action.audit.log.exception"

.field public static final AUDIT_LOG_GROUP_APPLICATION:I = 0x5

.field public static final AUDIT_LOG_GROUP_EVENTS:I = 0x4

.field public static final AUDIT_LOG_GROUP_NETWORK:I = 0x3

.field public static final AUDIT_LOG_GROUP_SECURITY:I = 0x1

.field public static final AUDIT_LOG_GROUP_SYSTEM:I = 0x2

.field public static final ERROR_NONE:I = 0x0

.field public static final ERROR_UNKNOWN:I = -0x7d0

.field public static final EXTRA_AUDIT_RESULT:Ljava/lang/String; = "mdm.intent.extra.audit.log.result"

.field private static final TAG:Ljava/lang/String; = "AuditLog"

.field private static mAuditLog:Lcom/sec/enterprise/knox/auditlog/AuditLog;

.field private static mContextInfo:Landroid/app/enterprise/ContextInfo;

.field private static final mSync:Ljava/lang/Object;


# instance fields
.field private mAuditService:Lcom/sec/enterprise/knox/auditlog/IAuditLog;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 74
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/sec/enterprise/knox/auditlog/AuditLog;->mSync:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V
    .locals 0
    .param p1, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 246
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 247
    sput-object p1, Lcom/sec/enterprise/knox/auditlog/AuditLog;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    .line 248
    return-void
.end method

.method public static a(IZILjava/lang/String;Ljava/lang/String;)V
    .locals 9
    .param p0, "moduleGroup"    # I
    .param p1, "outcome"    # Z
    .param p2, "uid"    # I
    .param p3, "swComponent"    # Ljava/lang/String;
    .param p4, "logMessage"    # Ljava/lang/String;

    .prologue
    .line 673
    sget-object v1, Lcom/sec/enterprise/knox/auditlog/AuditLog;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "AuditLog.a"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 674
    const-string v1, "auditlog"

    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/enterprise/knox/auditlog/IAuditLog$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/enterprise/knox/auditlog/IAuditLog;

    move-result-object v0

    .line 675
    .local v0, "auditService":Lcom/sec/enterprise/knox/auditlog/IAuditLog;
    if-eqz v0, :cond_0

    .line 677
    :try_start_0
    sget-object v1, Lcom/sec/enterprise/knox/auditlog/AuditLog;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v2, 0x1

    move v3, p0

    move v4, p1

    move v5, p2

    move-object v6, p3

    move-object v7, p4

    invoke-interface/range {v0 .. v7}, Lcom/sec/enterprise/knox/auditlog/IAuditLog;->AuditLogger(Landroid/app/enterprise/ContextInfo;IIZILjava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 683
    :cond_0
    :goto_0
    return-void

    .line 679
    :catch_0
    move-exception v8

    .line 680
    .local v8, "e":Landroid/os/RemoteException;
    const-string v1, "AuditLog"

    const-string v2, "Access to AuditLogger not allowed"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static c(IZILjava/lang/String;Ljava/lang/String;)V
    .locals 9
    .param p0, "moduleGroup"    # I
    .param p1, "outcome"    # Z
    .param p2, "uid"    # I
    .param p3, "swComponent"    # Ljava/lang/String;
    .param p4, "logMessage"    # Ljava/lang/String;

    .prologue
    .line 724
    sget-object v1, Lcom/sec/enterprise/knox/auditlog/AuditLog;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "AuditLog.c"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 725
    const-string v1, "auditlog"

    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/enterprise/knox/auditlog/IAuditLog$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/enterprise/knox/auditlog/IAuditLog;

    move-result-object v0

    .line 726
    .local v0, "auditService":Lcom/sec/enterprise/knox/auditlog/IAuditLog;
    if-eqz v0, :cond_0

    .line 728
    :try_start_0
    sget-object v1, Lcom/sec/enterprise/knox/auditlog/AuditLog;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v2, 0x2

    move v3, p0

    move v4, p1

    move v5, p2

    move-object v6, p3

    move-object v7, p4

    invoke-interface/range {v0 .. v7}, Lcom/sec/enterprise/knox/auditlog/IAuditLog;->AuditLogger(Landroid/app/enterprise/ContextInfo;IIZILjava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 734
    :cond_0
    :goto_0
    return-void

    .line 730
    :catch_0
    move-exception v8

    .line 731
    .local v8, "e":Landroid/os/RemoteException;
    const-string v1, "AuditLog"

    const-string v2, "Access to AuditLogger not allowed"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static createInstance(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)Lcom/sec/enterprise/knox/auditlog/AuditLog;
    .locals 2
    .param p0, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 299
    new-instance v0, Lcom/sec/enterprise/knox/auditlog/AuditLog;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/enterprise/knox/auditlog/AuditLog;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    return-object v0
.end method

.method public static e(IZILjava/lang/String;Ljava/lang/String;)V
    .locals 9
    .param p0, "moduleGroup"    # I
    .param p1, "outcome"    # Z
    .param p2, "uid"    # I
    .param p3, "swComponent"    # Ljava/lang/String;
    .param p4, "logMessage"    # Ljava/lang/String;

    .prologue
    .line 775
    sget-object v1, Lcom/sec/enterprise/knox/auditlog/AuditLog;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "AuditLog.e"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 776
    const-string v1, "auditlog"

    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/enterprise/knox/auditlog/IAuditLog$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/enterprise/knox/auditlog/IAuditLog;

    move-result-object v0

    .line 777
    .local v0, "auditService":Lcom/sec/enterprise/knox/auditlog/IAuditLog;
    if-eqz v0, :cond_0

    .line 779
    :try_start_0
    sget-object v1, Lcom/sec/enterprise/knox/auditlog/AuditLog;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v2, 0x3

    move v3, p0

    move v4, p1

    move v5, p2

    move-object v6, p3

    move-object v7, p4

    invoke-interface/range {v0 .. v7}, Lcom/sec/enterprise/knox/auditlog/IAuditLog;->AuditLogger(Landroid/app/enterprise/ContextInfo;IIZILjava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 785
    :cond_0
    :goto_0
    return-void

    .line 781
    :catch_0
    move-exception v8

    .line 782
    .local v8, "e":Landroid/os/RemoteException;
    const-string v1, "AuditLog"

    const-string v2, "Access to AuditLogger not allowed"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static getInstance(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)Lcom/sec/enterprise/knox/auditlog/AuditLog;
    .locals 3
    .param p0, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 287
    sget-object v1, Lcom/sec/enterprise/knox/auditlog/AuditLog;->mSync:Ljava/lang/Object;

    monitor-enter v1

    .line 288
    :try_start_0
    sget-object v0, Lcom/sec/enterprise/knox/auditlog/AuditLog;->mAuditLog:Lcom/sec/enterprise/knox/auditlog/AuditLog;

    if-nez v0, :cond_0

    .line 289
    new-instance v0, Lcom/sec/enterprise/knox/auditlog/AuditLog;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, p0, v2}, Lcom/sec/enterprise/knox/auditlog/AuditLog;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    sput-object v0, Lcom/sec/enterprise/knox/auditlog/AuditLog;->mAuditLog:Lcom/sec/enterprise/knox/auditlog/AuditLog;

    .line 291
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/auditlog/AuditLog;->mAuditLog:Lcom/sec/enterprise/knox/auditlog/AuditLog;

    monitor-exit v1

    return-object v0

    .line 292
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/sec/enterprise/knox/auditlog/AuditLog;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 274
    sget-object v2, Lcom/sec/enterprise/knox/auditlog/AuditLog;->mSync:Ljava/lang/Object;

    monitor-enter v2

    .line 275
    :try_start_0
    sget-object v1, Lcom/sec/enterprise/knox/auditlog/AuditLog;->mAuditLog:Lcom/sec/enterprise/knox/auditlog/AuditLog;

    if-nez v1, :cond_0

    if-eqz p0, :cond_0

    .line 276
    new-instance v0, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    invoke-direct {v0, v1}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    .line 277
    .local v0, "cxtInfo":Landroid/app/enterprise/ContextInfo;
    new-instance v1, Lcom/sec/enterprise/knox/auditlog/AuditLog;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v1, v0, v3}, Lcom/sec/enterprise/knox/auditlog/AuditLog;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    sput-object v1, Lcom/sec/enterprise/knox/auditlog/AuditLog;->mAuditLog:Lcom/sec/enterprise/knox/auditlog/AuditLog;

    .line 279
    .end local v0    # "cxtInfo":Landroid/app/enterprise/ContextInfo;
    :cond_0
    sget-object v1, Lcom/sec/enterprise/knox/auditlog/AuditLog;->mAuditLog:Lcom/sec/enterprise/knox/auditlog/AuditLog;

    monitor-exit v2

    return-object v1

    .line 280
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private getService()Lcom/sec/enterprise/knox/auditlog/IAuditLog;
    .locals 1

    .prologue
    .line 252
    iget-object v0, p0, Lcom/sec/enterprise/knox/auditlog/AuditLog;->mAuditService:Lcom/sec/enterprise/knox/auditlog/IAuditLog;

    if-nez v0, :cond_0

    .line 253
    const-string v0, "auditlog"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/auditlog/IAuditLog$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/enterprise/knox/auditlog/IAuditLog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/auditlog/AuditLog;->mAuditService:Lcom/sec/enterprise/knox/auditlog/IAuditLog;

    .line 255
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/auditlog/AuditLog;->mAuditService:Lcom/sec/enterprise/knox/auditlog/IAuditLog;

    return-object v0
.end method

.method public static n(IZILjava/lang/String;Ljava/lang/String;)V
    .locals 9
    .param p0, "moduleGroup"    # I
    .param p1, "outcome"    # Z
    .param p2, "uid"    # I
    .param p3, "swComponent"    # Ljava/lang/String;
    .param p4, "logMessage"    # Ljava/lang/String;

    .prologue
    .line 877
    sget-object v1, Lcom/sec/enterprise/knox/auditlog/AuditLog;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "AuditLog.n"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 878
    const-string v1, "auditlog"

    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/enterprise/knox/auditlog/IAuditLog$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/enterprise/knox/auditlog/IAuditLog;

    move-result-object v0

    .line 879
    .local v0, "auditService":Lcom/sec/enterprise/knox/auditlog/IAuditLog;
    if-eqz v0, :cond_0

    .line 881
    :try_start_0
    sget-object v1, Lcom/sec/enterprise/knox/auditlog/AuditLog;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v2, 0x5

    move v3, p0

    move v4, p1

    move v5, p2

    move-object v6, p3

    move-object v7, p4

    invoke-interface/range {v0 .. v7}, Lcom/sec/enterprise/knox/auditlog/IAuditLog;->AuditLogger(Landroid/app/enterprise/ContextInfo;IIZILjava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 887
    :cond_0
    :goto_0
    return-void

    .line 883
    :catch_0
    move-exception v8

    .line 884
    .local v8, "e":Landroid/os/RemoteException;
    const-string v1, "AuditLog"

    const-string v2, "Access to AuditLogger not allowed"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static w(IZILjava/lang/String;Ljava/lang/String;)V
    .locals 9
    .param p0, "moduleGroup"    # I
    .param p1, "outcome"    # Z
    .param p2, "uid"    # I
    .param p3, "swComponent"    # Ljava/lang/String;
    .param p4, "logMessage"    # Ljava/lang/String;

    .prologue
    .line 826
    sget-object v1, Lcom/sec/enterprise/knox/auditlog/AuditLog;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "AuditLog.w"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 827
    const-string v1, "auditlog"

    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/enterprise/knox/auditlog/IAuditLog$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/enterprise/knox/auditlog/IAuditLog;

    move-result-object v0

    .line 828
    .local v0, "auditService":Lcom/sec/enterprise/knox/auditlog/IAuditLog;
    if-eqz v0, :cond_0

    .line 830
    :try_start_0
    sget-object v1, Lcom/sec/enterprise/knox/auditlog/AuditLog;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v2, 0x4

    move v3, p0

    move v4, p1

    move v5, p2

    move-object v6, p3

    move-object v7, p4

    invoke-interface/range {v0 .. v7}, Lcom/sec/enterprise/knox/auditlog/IAuditLog;->AuditLogger(Landroid/app/enterprise/ContextInfo;IIZILjava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 836
    :cond_0
    :goto_0
    return-void

    .line 832
    :catch_0
    move-exception v8

    .line 833
    .local v8, "e":Landroid/os/RemoteException;
    const-string v1, "AuditLog"

    const-string v2, "Access to AuditLogger not allowed"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public disableAuditLog()Z
    .locals 3

    .prologue
    .line 371
    sget-object v1, Lcom/sec/enterprise/knox/auditlog/AuditLog;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "AuditLog.disableAuditLog"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 372
    invoke-direct {p0}, Lcom/sec/enterprise/knox/auditlog/AuditLog;->getService()Lcom/sec/enterprise/knox/auditlog/IAuditLog;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 374
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/auditlog/AuditLog;->mAuditService:Lcom/sec/enterprise/knox/auditlog/IAuditLog;

    sget-object v2, Lcom/sec/enterprise/knox/auditlog/AuditLog;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Lcom/sec/enterprise/knox/auditlog/IAuditLog;->disableAuditLog(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 379
    :goto_0
    return v1

    .line 375
    :catch_0
    move-exception v0

    .line 376
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "AuditLog"

    const-string v2, "Failed to disableAuditLog"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 379
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public disableIPTablesLogging()Z
    .locals 3

    .prologue
    .line 1056
    sget-object v1, Lcom/sec/enterprise/knox/auditlog/AuditLog;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "AuditLog.disableIPTablesLogging"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1057
    invoke-direct {p0}, Lcom/sec/enterprise/knox/auditlog/AuditLog;->getService()Lcom/sec/enterprise/knox/auditlog/IAuditLog;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1059
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/auditlog/AuditLog;->mAuditService:Lcom/sec/enterprise/knox/auditlog/IAuditLog;

    sget-object v2, Lcom/sec/enterprise/knox/auditlog/AuditLog;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Lcom/sec/enterprise/knox/auditlog/IAuditLog;->disableIPTablesLogging(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1064
    :goto_0
    return v1

    .line 1060
    :catch_0
    move-exception v0

    .line 1061
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "AuditLog"

    const-string v2, "Failed to disableIPTablesLogging"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1064
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public dumpLogFile(JJLjava/lang/String;Landroid/os/ParcelFileDescriptor;)Z
    .locals 9
    .param p1, "begin"    # J
    .param p3, "end"    # J
    .param p5, "filter"    # Ljava/lang/String;
    .param p6, "outputFile"    # Landroid/os/ParcelFileDescriptor;

    .prologue
    .line 972
    sget-object v0, Lcom/sec/enterprise/knox/auditlog/AuditLog;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v1, "AuditLog.dumpLogFile"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 973
    invoke-direct {p0}, Lcom/sec/enterprise/knox/auditlog/AuditLog;->getService()Lcom/sec/enterprise/knox/auditlog/IAuditLog;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 975
    :try_start_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/auditlog/AuditLog;->mAuditService:Lcom/sec/enterprise/knox/auditlog/IAuditLog;

    sget-object v1, Lcom/sec/enterprise/knox/auditlog/AuditLog;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    move-wide v2, p1

    move-wide v4, p3

    move-object v6, p5

    move-object v7, p6

    invoke-interface/range {v0 .. v7}, Lcom/sec/enterprise/knox/auditlog/IAuditLog;->dumpLogFile(Landroid/app/enterprise/ContextInfo;JJLjava/lang/String;Landroid/os/ParcelFileDescriptor;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 980
    :goto_0
    return v0

    .line 976
    :catch_0
    move-exception v8

    .line 977
    .local v8, "e":Landroid/os/RemoteException;
    const-string v0, "AuditLog"

    const-string v1, "Failed to dumpLogFile"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 980
    .end local v8    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public enableAuditLog()Z
    .locals 3

    .prologue
    .line 331
    sget-object v1, Lcom/sec/enterprise/knox/auditlog/AuditLog;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "AuditLog.enableAuditLog"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 332
    invoke-direct {p0}, Lcom/sec/enterprise/knox/auditlog/AuditLog;->getService()Lcom/sec/enterprise/knox/auditlog/IAuditLog;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 334
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/auditlog/AuditLog;->mAuditService:Lcom/sec/enterprise/knox/auditlog/IAuditLog;

    sget-object v2, Lcom/sec/enterprise/knox/auditlog/AuditLog;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Lcom/sec/enterprise/knox/auditlog/IAuditLog;->enableAuditLog(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 339
    :goto_0
    return v1

    .line 335
    :catch_0
    move-exception v0

    .line 336
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "AuditLog"

    const-string v2, "Failed to enableAuditLog"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 339
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public enableIPTablesLogging()Z
    .locals 3

    .prologue
    .line 1015
    sget-object v1, Lcom/sec/enterprise/knox/auditlog/AuditLog;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "AuditLog.enableIPTablesLogging"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1016
    invoke-direct {p0}, Lcom/sec/enterprise/knox/auditlog/AuditLog;->getService()Lcom/sec/enterprise/knox/auditlog/IAuditLog;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1018
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/auditlog/AuditLog;->mAuditService:Lcom/sec/enterprise/knox/auditlog/IAuditLog;

    sget-object v2, Lcom/sec/enterprise/knox/auditlog/AuditLog;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Lcom/sec/enterprise/knox/auditlog/IAuditLog;->enableIPTablesLogging(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1023
    :goto_0
    return v1

    .line 1019
    :catch_0
    move-exception v0

    .line 1020
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "AuditLog"

    const-string v2, "Failed to enableIPTablesLogging"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1023
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getCriticalLogSize()I
    .locals 3

    .prologue
    .line 538
    sget-object v1, Lcom/sec/enterprise/knox/auditlog/AuditLog;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "AuditLog.getCriticalLogSize"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 539
    invoke-direct {p0}, Lcom/sec/enterprise/knox/auditlog/AuditLog;->getService()Lcom/sec/enterprise/knox/auditlog/IAuditLog;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 541
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/auditlog/AuditLog;->mAuditService:Lcom/sec/enterprise/knox/auditlog/IAuditLog;

    sget-object v2, Lcom/sec/enterprise/knox/auditlog/AuditLog;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Lcom/sec/enterprise/knox/auditlog/IAuditLog;->getCriticalLogSize(Landroid/app/enterprise/ContextInfo;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 546
    :goto_0
    return v1

    .line 542
    :catch_0
    move-exception v0

    .line 543
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "AuditLog"

    const-string v2, "Failed to get current log size"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 546
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getCurrentLogFileSize()I
    .locals 3

    .prologue
    .line 452
    sget-object v1, Lcom/sec/enterprise/knox/auditlog/AuditLog;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "AuditLog.getCurrentLogFileSize"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 453
    invoke-direct {p0}, Lcom/sec/enterprise/knox/auditlog/AuditLog;->getService()Lcom/sec/enterprise/knox/auditlog/IAuditLog;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 455
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/auditlog/AuditLog;->mAuditService:Lcom/sec/enterprise/knox/auditlog/IAuditLog;

    sget-object v2, Lcom/sec/enterprise/knox/auditlog/AuditLog;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Lcom/sec/enterprise/knox/auditlog/IAuditLog;->getCurrentLogFileSize(Landroid/app/enterprise/ContextInfo;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 460
    :goto_0
    return v1

    .line 456
    :catch_0
    move-exception v0

    .line 457
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "AuditLog"

    const-string v2, "Failed to get current log size"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 460
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getMaximumLogSize()I
    .locals 3

    .prologue
    .line 624
    sget-object v1, Lcom/sec/enterprise/knox/auditlog/AuditLog;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "AuditLog.getMaximumLogSize"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 625
    invoke-direct {p0}, Lcom/sec/enterprise/knox/auditlog/AuditLog;->getService()Lcom/sec/enterprise/knox/auditlog/IAuditLog;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 627
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/auditlog/AuditLog;->mAuditService:Lcom/sec/enterprise/knox/auditlog/IAuditLog;

    sget-object v2, Lcom/sec/enterprise/knox/auditlog/AuditLog;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Lcom/sec/enterprise/knox/auditlog/IAuditLog;->getMaximumLogSize(Landroid/app/enterprise/ContextInfo;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 632
    :goto_0
    return v1

    .line 628
    :catch_0
    move-exception v0

    .line 629
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "AuditLog"

    const-string v2, "Failed to get current log size"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 632
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isAuditLogEnabled()Z
    .locals 3

    .prologue
    .line 411
    sget-object v1, Lcom/sec/enterprise/knox/auditlog/AuditLog;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "AuditLog.isAuditLogEnabled"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 412
    invoke-direct {p0}, Lcom/sec/enterprise/knox/auditlog/AuditLog;->getService()Lcom/sec/enterprise/knox/auditlog/IAuditLog;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 414
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/auditlog/AuditLog;->mAuditService:Lcom/sec/enterprise/knox/auditlog/IAuditLog;

    sget-object v2, Lcom/sec/enterprise/knox/auditlog/AuditLog;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Lcom/sec/enterprise/knox/auditlog/IAuditLog;->isAuditLogEnabled(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 419
    :goto_0
    return v1

    .line 415
    :catch_0
    move-exception v0

    .line 416
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "AuditLog"

    const-string v2, "Failed to isAuditLogEnabled"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 419
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isIPTablesLoggingEnabled()Z
    .locals 3

    .prologue
    .line 1096
    sget-object v1, Lcom/sec/enterprise/knox/auditlog/AuditLog;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "AuditLog.isIPTablesLoggingEnabled"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1097
    invoke-direct {p0}, Lcom/sec/enterprise/knox/auditlog/AuditLog;->getService()Lcom/sec/enterprise/knox/auditlog/IAuditLog;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1099
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/auditlog/AuditLog;->mAuditService:Lcom/sec/enterprise/knox/auditlog/IAuditLog;

    sget-object v2, Lcom/sec/enterprise/knox/auditlog/AuditLog;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Lcom/sec/enterprise/knox/auditlog/IAuditLog;->isIPTablesLoggingEnabled(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1104
    :goto_0
    return v1

    .line 1100
    :catch_0
    move-exception v0

    .line 1101
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "AuditLog"

    const-string v2, "Failed to isIPTablesLoggingEnabled"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1104
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setCriticalLogSize(I)Z
    .locals 4
    .param p1, "percentageValue"    # I

    .prologue
    .line 499
    sget-object v1, Lcom/sec/enterprise/knox/auditlog/AuditLog;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "AuditLog.setCriticalLogSize"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 500
    invoke-direct {p0}, Lcom/sec/enterprise/knox/auditlog/AuditLog;->getService()Lcom/sec/enterprise/knox/auditlog/IAuditLog;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 502
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/auditlog/AuditLog;->mAuditService:Lcom/sec/enterprise/knox/auditlog/IAuditLog;

    sget-object v2, Lcom/sec/enterprise/knox/auditlog/AuditLog;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Lcom/sec/enterprise/knox/auditlog/IAuditLog;->setCriticalLogSize(Landroid/app/enterprise/ContextInfo;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 507
    :goto_0
    return v1

    .line 503
    :catch_0
    move-exception v0

    .line 504
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "AuditLog"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to setCriticalLogSize size="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 507
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setMaximumLogSize(I)Z
    .locals 4
    .param p1, "percentageValue"    # I

    .prologue
    .line 585
    sget-object v1, Lcom/sec/enterprise/knox/auditlog/AuditLog;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "AuditLog.setMaximumLogSize"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 586
    invoke-direct {p0}, Lcom/sec/enterprise/knox/auditlog/AuditLog;->getService()Lcom/sec/enterprise/knox/auditlog/IAuditLog;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 588
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/auditlog/AuditLog;->mAuditService:Lcom/sec/enterprise/knox/auditlog/IAuditLog;

    sget-object v2, Lcom/sec/enterprise/knox/auditlog/AuditLog;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Lcom/sec/enterprise/knox/auditlog/IAuditLog;->setMaximumLogSize(Landroid/app/enterprise/ContextInfo;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 593
    :goto_0
    return v1

    .line 589
    :catch_0
    move-exception v0

    .line 590
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "AuditLog"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to setMaximumLogSize size="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 593
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
