.class public abstract Lcom/sec/enterprise/knox/auditlog/IAuditLog$Stub;
.super Landroid/os/Binder;
.source "IAuditLog.java"

# interfaces
.implements Lcom/sec/enterprise/knox/auditlog/IAuditLog;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/enterprise/knox/auditlog/IAuditLog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/enterprise/knox/auditlog/IAuditLog$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.sec.enterprise.knox.auditlog.IAuditLog"

.field static final TRANSACTION_AuditLogger:I = 0x9

.field static final TRANSACTION_disableAuditLog:I = 0x2

.field static final TRANSACTION_disableIPTablesLogging:I = 0xc

.field static final TRANSACTION_dumpLogFile:I = 0xa

.field static final TRANSACTION_enableAuditLog:I = 0x1

.field static final TRANSACTION_enableIPTablesLogging:I = 0xb

.field static final TRANSACTION_getCriticalLogSize:I = 0x6

.field static final TRANSACTION_getCurrentLogFileSize:I = 0x4

.field static final TRANSACTION_getMaximumLogSize:I = 0x8

.field static final TRANSACTION_isAuditLogEnabled:I = 0x3

.field static final TRANSACTION_isAuditServiceRunning:I = 0xe

.field static final TRANSACTION_isIPTablesLoggingEnabled:I = 0xd

.field static final TRANSACTION_setCriticalLogSize:I = 0x5

.field static final TRANSACTION_setMaximumLogSize:I = 0x7


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 19
    const-string v0, "com.sec.enterprise.knox.auditlog.IAuditLog"

    invoke-virtual {p0, p0, v0}, Lcom/sec/enterprise/knox/auditlog/IAuditLog$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 20
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/sec/enterprise/knox/auditlog/IAuditLog;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 27
    if-nez p0, :cond_0

    .line 28
    const/4 v0, 0x0

    .line 34
    :goto_0
    return-object v0

    .line 30
    :cond_0
    const-string v1, "com.sec.enterprise.knox.auditlog.IAuditLog"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 31
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/sec/enterprise/knox/auditlog/IAuditLog;

    if-eqz v1, :cond_1

    .line 32
    check-cast v0, Lcom/sec/enterprise/knox/auditlog/IAuditLog;

    goto :goto_0

    .line 34
    :cond_1
    new-instance v0, Lcom/sec/enterprise/knox/auditlog/IAuditLog$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/sec/enterprise/knox/auditlog/IAuditLog$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 38
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 19
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 42
    sparse-switch p1, :sswitch_data_0

    .line 281
    invoke-super/range {p0 .. p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v2

    :goto_0
    return v2

    .line 46
    :sswitch_0
    const-string v2, "com.sec.enterprise.knox.auditlog.IAuditLog"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 47
    const/4 v2, 0x1

    goto :goto_0

    .line 51
    :sswitch_1
    const-string v2, "com.sec.enterprise.knox.auditlog.IAuditLog"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 53
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_0

    .line 54
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 59
    .local v3, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_1
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/sec/enterprise/knox/auditlog/IAuditLog$Stub;->enableAuditLog(Landroid/app/enterprise/ContextInfo;)Z

    move-result v18

    .line 60
    .local v18, "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 61
    if-eqz v18, :cond_1

    const/4 v2, 0x1

    :goto_2
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 62
    const/4 v2, 0x1

    goto :goto_0

    .line 57
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v18    # "_result":Z
    :cond_0
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_1

    .line 61
    .restart local v18    # "_result":Z
    :cond_1
    const/4 v2, 0x0

    goto :goto_2

    .line 66
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v18    # "_result":Z
    :sswitch_2
    const-string v2, "com.sec.enterprise.knox.auditlog.IAuditLog"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 68
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_2

    .line 69
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 74
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_3
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/sec/enterprise/knox/auditlog/IAuditLog$Stub;->disableAuditLog(Landroid/app/enterprise/ContextInfo;)Z

    move-result v18

    .line 75
    .restart local v18    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 76
    if-eqz v18, :cond_3

    const/4 v2, 0x1

    :goto_4
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 77
    const/4 v2, 0x1

    goto :goto_0

    .line 72
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v18    # "_result":Z
    :cond_2
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_3

    .line 76
    .restart local v18    # "_result":Z
    :cond_3
    const/4 v2, 0x0

    goto :goto_4

    .line 81
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v18    # "_result":Z
    :sswitch_3
    const-string v2, "com.sec.enterprise.knox.auditlog.IAuditLog"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 83
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_4

    .line 84
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 89
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_5
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/sec/enterprise/knox/auditlog/IAuditLog$Stub;->isAuditLogEnabled(Landroid/app/enterprise/ContextInfo;)Z

    move-result v18

    .line 90
    .restart local v18    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 91
    if-eqz v18, :cond_5

    const/4 v2, 0x1

    :goto_6
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 92
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 87
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v18    # "_result":Z
    :cond_4
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_5

    .line 91
    .restart local v18    # "_result":Z
    :cond_5
    const/4 v2, 0x0

    goto :goto_6

    .line 96
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v18    # "_result":Z
    :sswitch_4
    const-string v2, "com.sec.enterprise.knox.auditlog.IAuditLog"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 98
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_6

    .line 99
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 104
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_7
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/sec/enterprise/knox/auditlog/IAuditLog$Stub;->getCurrentLogFileSize(Landroid/app/enterprise/ContextInfo;)I

    move-result v18

    .line 105
    .local v18, "_result":I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 106
    move-object/from16 v0, p3

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 107
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 102
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v18    # "_result":I
    :cond_6
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_7

    .line 111
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_5
    const-string v2, "com.sec.enterprise.knox.auditlog.IAuditLog"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 113
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_7

    .line 114
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 120
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_8
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 121
    .local v4, "_arg1":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Lcom/sec/enterprise/knox/auditlog/IAuditLog$Stub;->setCriticalLogSize(Landroid/app/enterprise/ContextInfo;I)Z

    move-result v18

    .line 122
    .local v18, "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 123
    if-eqz v18, :cond_8

    const/4 v2, 0x1

    :goto_9
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 124
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 117
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":I
    .end local v18    # "_result":Z
    :cond_7
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_8

    .line 123
    .restart local v4    # "_arg1":I
    .restart local v18    # "_result":Z
    :cond_8
    const/4 v2, 0x0

    goto :goto_9

    .line 128
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":I
    .end local v18    # "_result":Z
    :sswitch_6
    const-string v2, "com.sec.enterprise.knox.auditlog.IAuditLog"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 130
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_9

    .line 131
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 136
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_a
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/sec/enterprise/knox/auditlog/IAuditLog$Stub;->getCriticalLogSize(Landroid/app/enterprise/ContextInfo;)I

    move-result v18

    .line 137
    .local v18, "_result":I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 138
    move-object/from16 v0, p3

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 139
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 134
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v18    # "_result":I
    :cond_9
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_a

    .line 143
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_7
    const-string v2, "com.sec.enterprise.knox.auditlog.IAuditLog"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 145
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_a

    .line 146
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 152
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_b
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 153
    .restart local v4    # "_arg1":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Lcom/sec/enterprise/knox/auditlog/IAuditLog$Stub;->setMaximumLogSize(Landroid/app/enterprise/ContextInfo;I)Z

    move-result v18

    .line 154
    .local v18, "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 155
    if-eqz v18, :cond_b

    const/4 v2, 0x1

    :goto_c
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 156
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 149
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":I
    .end local v18    # "_result":Z
    :cond_a
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_b

    .line 155
    .restart local v4    # "_arg1":I
    .restart local v18    # "_result":Z
    :cond_b
    const/4 v2, 0x0

    goto :goto_c

    .line 160
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":I
    .end local v18    # "_result":Z
    :sswitch_8
    const-string v2, "com.sec.enterprise.knox.auditlog.IAuditLog"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 162
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_c

    .line 163
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 168
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_d
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/sec/enterprise/knox/auditlog/IAuditLog$Stub;->getMaximumLogSize(Landroid/app/enterprise/ContextInfo;)I

    move-result v18

    .line 169
    .local v18, "_result":I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 170
    move-object/from16 v0, p3

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 171
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 166
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v18    # "_result":I
    :cond_c
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_d

    .line 175
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_9
    const-string v2, "com.sec.enterprise.knox.auditlog.IAuditLog"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 177
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_d

    .line 178
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 184
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_e
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 186
    .restart local v4    # "_arg1":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    .line 188
    .local v5, "_arg2":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_e

    const/4 v6, 0x1

    .line 190
    .local v6, "_arg3":Z
    :goto_f
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    .line 192
    .local v7, "_arg4":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v8

    .line 194
    .local v8, "_arg5":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v9

    .local v9, "_arg6":Ljava/lang/String;
    move-object/from16 v2, p0

    .line 195
    invoke-virtual/range {v2 .. v9}, Lcom/sec/enterprise/knox/auditlog/IAuditLog$Stub;->AuditLogger(Landroid/app/enterprise/ContextInfo;IIZILjava/lang/String;Ljava/lang/String;)V

    .line 196
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 197
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 181
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":I
    .end local v5    # "_arg2":I
    .end local v6    # "_arg3":Z
    .end local v7    # "_arg4":I
    .end local v8    # "_arg5":Ljava/lang/String;
    .end local v9    # "_arg6":Ljava/lang/String;
    :cond_d
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_e

    .line 188
    .restart local v4    # "_arg1":I
    .restart local v5    # "_arg2":I
    :cond_e
    const/4 v6, 0x0

    goto :goto_f

    .line 201
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":I
    .end local v5    # "_arg2":I
    :sswitch_a
    const-string v2, "com.sec.enterprise.knox.auditlog.IAuditLog"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 203
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_f

    .line 204
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 210
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_10
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v12

    .line 212
    .local v12, "_arg1":J
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v14

    .line 214
    .local v14, "_arg2":J
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 216
    .local v6, "_arg3":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_10

    .line 217
    sget-object v2, Landroid/os/ParcelFileDescriptor;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/os/ParcelFileDescriptor;

    .local v7, "_arg4":Landroid/os/ParcelFileDescriptor;
    :goto_11
    move-object/from16 v10, p0

    move-object v11, v3

    move-object/from16 v16, v6

    move-object/from16 v17, v7

    .line 222
    invoke-virtual/range {v10 .. v17}, Lcom/sec/enterprise/knox/auditlog/IAuditLog$Stub;->dumpLogFile(Landroid/app/enterprise/ContextInfo;JJLjava/lang/String;Landroid/os/ParcelFileDescriptor;)Z

    move-result v18

    .line 223
    .local v18, "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 224
    if-eqz v18, :cond_11

    const/4 v2, 0x1

    :goto_12
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 225
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 207
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg3":Ljava/lang/String;
    .end local v7    # "_arg4":Landroid/os/ParcelFileDescriptor;
    .end local v12    # "_arg1":J
    .end local v14    # "_arg2":J
    .end local v18    # "_result":Z
    :cond_f
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_10

    .line 220
    .restart local v6    # "_arg3":Ljava/lang/String;
    .restart local v12    # "_arg1":J
    .restart local v14    # "_arg2":J
    :cond_10
    const/4 v7, 0x0

    .restart local v7    # "_arg4":Landroid/os/ParcelFileDescriptor;
    goto :goto_11

    .line 224
    .restart local v18    # "_result":Z
    :cond_11
    const/4 v2, 0x0

    goto :goto_12

    .line 229
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg3":Ljava/lang/String;
    .end local v7    # "_arg4":Landroid/os/ParcelFileDescriptor;
    .end local v12    # "_arg1":J
    .end local v14    # "_arg2":J
    .end local v18    # "_result":Z
    :sswitch_b
    const-string v2, "com.sec.enterprise.knox.auditlog.IAuditLog"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 231
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_12

    .line 232
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 237
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_13
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/sec/enterprise/knox/auditlog/IAuditLog$Stub;->enableIPTablesLogging(Landroid/app/enterprise/ContextInfo;)Z

    move-result v18

    .line 238
    .restart local v18    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 239
    if-eqz v18, :cond_13

    const/4 v2, 0x1

    :goto_14
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 240
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 235
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v18    # "_result":Z
    :cond_12
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_13

    .line 239
    .restart local v18    # "_result":Z
    :cond_13
    const/4 v2, 0x0

    goto :goto_14

    .line 244
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v18    # "_result":Z
    :sswitch_c
    const-string v2, "com.sec.enterprise.knox.auditlog.IAuditLog"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 246
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_14

    .line 247
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 252
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_15
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/sec/enterprise/knox/auditlog/IAuditLog$Stub;->disableIPTablesLogging(Landroid/app/enterprise/ContextInfo;)Z

    move-result v18

    .line 253
    .restart local v18    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 254
    if-eqz v18, :cond_15

    const/4 v2, 0x1

    :goto_16
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 255
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 250
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v18    # "_result":Z
    :cond_14
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_15

    .line 254
    .restart local v18    # "_result":Z
    :cond_15
    const/4 v2, 0x0

    goto :goto_16

    .line 259
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v18    # "_result":Z
    :sswitch_d
    const-string v2, "com.sec.enterprise.knox.auditlog.IAuditLog"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 261
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_16

    .line 262
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 267
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_17
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/sec/enterprise/knox/auditlog/IAuditLog$Stub;->isIPTablesLoggingEnabled(Landroid/app/enterprise/ContextInfo;)Z

    move-result v18

    .line 268
    .restart local v18    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 269
    if-eqz v18, :cond_17

    const/4 v2, 0x1

    :goto_18
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 270
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 265
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v18    # "_result":Z
    :cond_16
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_17

    .line 269
    .restart local v18    # "_result":Z
    :cond_17
    const/4 v2, 0x0

    goto :goto_18

    .line 274
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v18    # "_result":Z
    :sswitch_e
    const-string v2, "com.sec.enterprise.knox.auditlog.IAuditLog"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 275
    invoke-virtual/range {p0 .. p0}, Lcom/sec/enterprise/knox/auditlog/IAuditLog$Stub;->isAuditServiceRunning()Z

    move-result v18

    .line 276
    .restart local v18    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 277
    if-eqz v18, :cond_18

    const/4 v2, 0x1

    :goto_19
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 278
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 277
    :cond_18
    const/4 v2, 0x0

    goto :goto_19

    .line 42
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0xe -> :sswitch_e
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
