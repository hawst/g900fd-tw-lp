.class public Lcom/sec/enterprise/knox/ContainerPasswordPolicy;
.super Ljava/lang/Object;
.source "ContainerPasswordPolicy.java"


# static fields
.field public static final CONTAINER_INTENT_LISTENER:Ljava/lang/String; = "com.sec.knox.container"

.field public static final CONTAINER_INTENT_LISTENER_PERMISSION:Ljava/lang/String; = "android.permission.sec.MDM_SECURITY"

.field public static final EXTRA_CONTAINER_ID:Ljava/lang/String; = "containerid"

.field public static final EXTRA_NEW_VALUE:Ljava/lang/String; = "value"

.field public static final MILISECONDS:J = 0x3e8L

.field public static final MINUTE_MILISECONDS:J = 0xea60L

.field public static final PASSWORD_QUALITY_ALPHABETIC:I = 0x40000

.field public static final PASSWORD_QUALITY_ALPHANUMERIC:I = 0x50000

.field public static final PASSWORD_QUALITY_BIOMETRIC_WEAK:I = 0x8000

.field public static final PASSWORD_QUALITY_COMPLEX:I = 0x60000

.field public static final PASSWORD_QUALITY_NUMERIC:I = 0x20000

.field public static final PASSWORD_QUALITY_SIGNATURE:I = 0x9000

.field public static final PASSWORD_QUALITY_SOMETHING:I = 0x10000

.field public static final PASSWORD_QUALITY_UNSPECIFIED:I = 0x0

.field public static final PWD_CHANGE_ENFORCED:I = 0x1

.field public static final PWD_CHANGE_ENFORCED_CANCELLED:I = 0x2

.field public static final PWD_CHANGE_ENFORCED_NEW_PASSWORD:I = 0x3

.field public static final PWD_CHANGE_ENFORCE_CANCELLING:I = -0x1

.field public static final PWD_CHANGE_NOT_ENFORCED:I = 0x0

.field public static final PWD_CHANGE_TIMEOUT:Ljava/lang/String; = "com.android.server.enterprise.PWD_CHANGE_TIMEOUT"

.field private static TAG:Ljava/lang/String;

.field private static gPasswordService:Landroid/app/enterprise/IPasswordPolicy;

.field private static mMUMContainerService:Lcom/sec/enterprise/knox/container/IKnoxContainerManager;


# instance fields
.field private mContextInfo:Landroid/app/enterprise/ContextInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 69
    const-string v0, "ContainerPasswordPolicy"

    sput-object v0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    .line 214
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mMUMContainerService:Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    return-void
.end method

.method constructor <init>(Landroid/app/enterprise/ContextInfo;)V
    .locals 0
    .param p1, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;

    .prologue
    .line 234
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 235
    iput-object p1, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    .line 236
    return-void
.end method

.method static declared-synchronized getMUMContainerService()Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    .locals 2

    .prologue
    .line 226
    const-class v1, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mMUMContainerService:Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    if-nez v0, :cond_0

    .line 227
    const-string v0, "mum_container_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mMUMContainerService:Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    .line 231
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mMUMContainerService:Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 226
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static declared-synchronized getPasswordService()Landroid/app/enterprise/IPasswordPolicy;
    .locals 2

    .prologue
    .line 217
    const-class v1, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->gPasswordService:Landroid/app/enterprise/IPasswordPolicy;

    if-nez v0, :cond_0

    .line 218
    const-string v0, "password_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/app/enterprise/IPasswordPolicy$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->gPasswordService:Landroid/app/enterprise/IPasswordPolicy;

    .line 222
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->gPasswordService:Landroid/app/enterprise/IPasswordPolicy;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 217
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public deleteAllRestrictions()Z
    .locals 6

    .prologue
    .line 656
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerPasswordPolicy.deleteAllRestrictions"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 658
    const/4 v2, 0x0

    .line 659
    .local v2, "retVal":Z
    invoke-static {}, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->getPasswordService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    .line 660
    .local v1, "passwordService":Landroid/app/enterprise/IPasswordPolicy;
    if-nez v1, :cond_0

    .line 661
    sget-object v4, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v5, "ContainerPassword PolicyService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 670
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 666
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v4}, Landroid/app/enterprise/IPasswordPolicy;->deleteAllRestrictions(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 670
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 667
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 668
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed talking with ContainerPasswordPolicy"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public enforcePwdChange()Z
    .locals 7

    .prologue
    .line 932
    iget-object v5, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v6, "ContainerPasswordPolicy.enforcePwdChange"

    invoke-static {v5, v6}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 934
    const/4 v3, 0x0

    .line 935
    .local v3, "retVal":Z
    iget-object v5, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget v0, v5, Landroid/app/enterprise/ContextInfo;->mContainerId:I

    .line 936
    .local v0, "containerId":I
    invoke-static {}, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->getPasswordService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v2

    .line 938
    .local v2, "passwordService":Landroid/app/enterprise/IPasswordPolicy;
    if-nez v2, :cond_0

    .line 939
    sget-object v5, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v6, "ContainerPassword PolicyService is not yet ready!!!"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v4, v3

    .line 950
    .end local v3    # "retVal":Z
    .local v4, "retVal":I
    :goto_0
    return v4

    .line 944
    .end local v4    # "retVal":I
    .restart local v3    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v5, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v5}, Landroid/app/enterprise/IPasswordPolicy;->enforcePwdChange(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    :goto_1
    move v4, v3

    .line 950
    .restart local v4    # "retVal":I
    goto :goto_0

    .line 946
    .end local v4    # "retVal":I
    :catch_0
    move-exception v1

    .line 947
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v5, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v6, "Failed talking with ContainerPasswordPolicy"

    invoke-static {v5, v6, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public excludeExternalStorageForFailedPasswordsWipe(Z)Z
    .locals 6
    .param p1, "enable"    # Z

    .prologue
    .line 1921
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerPasswordPolicy.excludeExternalStorageForFailedPasswordsWipe"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1924
    const/4 v2, 0x0

    .line 1925
    .local v2, "retVal":Z
    invoke-static {}, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->getPasswordService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    .line 1926
    .local v1, "passwordService":Landroid/app/enterprise/IPasswordPolicy;
    if-nez v1, :cond_0

    .line 1927
    sget-object v4, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v5, "ContainerPassword PolicyService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 1937
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 1932
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v4, p1}, Landroid/app/enterprise/IPasswordPolicy;->excludeExternalStorageForFailedPasswordsWipe(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 1937
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 1934
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 1935
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed talking with password policy"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public getForbiddenStrings(Z)Ljava/util/List;
    .locals 6
    .param p1, "allAdmins"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1680
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerPasswordPolicy.getForbiddenStrings"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1682
    const/4 v2, 0x0

    .line 1683
    .local v2, "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {}, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->getPasswordService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    .line 1684
    .local v1, "passwordService":Landroid/app/enterprise/IPasswordPolicy;
    if-nez v1, :cond_0

    .line 1685
    sget-object v4, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v5, "ContainerPassword PolicyService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, v2

    .line 1694
    .end local v2    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local v3, "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :goto_0
    return-object v3

    .line 1690
    .end local v3    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v2    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v4, p1}, Landroid/app/enterprise/IPasswordPolicy;->getForbiddenStrings(Landroid/app/enterprise/ContextInfo;Z)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :goto_1
    move-object v3, v2

    .line 1694
    .end local v2    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v3    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    goto :goto_0

    .line 1691
    .end local v3    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v2    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :catch_0
    move-exception v0

    .line 1692
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed getDataForbidden!!!"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public getMaximumCharacterOccurences()I
    .locals 6

    .prologue
    .line 1783
    const/4 v2, 0x0

    .line 1784
    .local v2, "retVal":I
    invoke-static {}, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->getPasswordService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    .line 1785
    .local v1, "passwordService":Landroid/app/enterprise/IPasswordPolicy;
    if-nez v1, :cond_0

    .line 1786
    sget-object v4, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v5, "ContainerPassword PolicyService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 1795
    .end local v2    # "retVal":I
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 1791
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":I
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v4}, Landroid/app/enterprise/IPasswordPolicy;->getMaximumCharacterOccurences(Landroid/app/enterprise/ContextInfo;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 1795
    .end local v2    # "retVal":I
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 1792
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":I
    :catch_0
    move-exception v0

    .line 1793
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed getMaxRepeatedCharacters!!!"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public getMaximumCharacterSequenceLength()I
    .locals 6

    .prologue
    .line 1464
    const/4 v2, 0x0

    .line 1465
    .local v2, "retVal":I
    invoke-static {}, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->getPasswordService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    .line 1466
    .local v1, "passwordService":Landroid/app/enterprise/IPasswordPolicy;
    if-nez v1, :cond_0

    .line 1467
    sget-object v4, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v5, "ContainerPassword PolicyService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 1476
    .end local v2    # "retVal":I
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 1472
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":I
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v4}, Landroid/app/enterprise/IPasswordPolicy;->getMaximumCharacterSequenceLength(Landroid/app/enterprise/ContextInfo;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 1476
    .end local v2    # "retVal":I
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 1473
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":I
    :catch_0
    move-exception v0

    .line 1474
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed getMaximumCharacterSequenceLength!!!"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public getMaximumFailedPasswordsForDeviceDisable(Landroid/content/ComponentName;)I
    .locals 7
    .param p1, "admin"    # Landroid/content/ComponentName;

    .prologue
    .line 1249
    const/4 v3, 0x0

    .line 1250
    .local v3, "retVal":I
    iget-object v5, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget v0, v5, Landroid/app/enterprise/ContextInfo;->mContainerId:I

    .line 1251
    .local v0, "containerId":I
    invoke-static {}, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->getPasswordService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v2

    .line 1252
    .local v2, "passwordService":Landroid/app/enterprise/IPasswordPolicy;
    if-nez v2, :cond_0

    .line 1253
    sget-object v5, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v6, "ContainerPassword PolicyService is not yet ready!!!"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v4, v3

    .line 1263
    .end local v3    # "retVal":I
    .local v4, "retVal":I
    :goto_0
    return v4

    .line 1258
    .end local v4    # "retVal":I
    .restart local v3    # "retVal":I
    :cond_0
    :try_start_0
    iget-object v5, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v5}, Landroid/app/enterprise/IPasswordPolicy;->getMaximumFailedPasswordsForDisable(Landroid/app/enterprise/ContextInfo;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    :goto_1
    move v4, v3

    .line 1263
    .end local v3    # "retVal":I
    .restart local v4    # "retVal":I
    goto :goto_0

    .line 1259
    .end local v4    # "retVal":I
    .restart local v3    # "retVal":I
    :catch_0
    move-exception v1

    .line 1260
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v5, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v6, "Failed talking with ContainerPasswordPolicy"

    invoke-static {v5, v6, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public getMaximumNumericSequenceLength()I
    .locals 6

    .prologue
    .line 1356
    const/4 v2, 0x0

    .line 1357
    .local v2, "retVal":I
    invoke-static {}, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->getPasswordService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    .line 1358
    .local v1, "passwordService":Landroid/app/enterprise/IPasswordPolicy;
    if-nez v1, :cond_0

    .line 1359
    sget-object v4, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v5, "ContainerPassword PolicyService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 1368
    .end local v2    # "retVal":I
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 1364
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":I
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v4}, Landroid/app/enterprise/IPasswordPolicy;->getMaximumNumericSequenceLength(Landroid/app/enterprise/ContextInfo;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 1368
    .end local v2    # "retVal":I
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 1365
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":I
    :catch_0
    move-exception v0

    .line 1366
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed getNumericSequencesForbidden!!!"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public getMaximumTimeToLock(Landroid/content/ComponentName;)I
    .locals 12
    .param p1, "admin"    # Landroid/content/ComponentName;

    .prologue
    const-wide/16 v10, 0x0

    .line 1107
    const-wide/16 v6, -0x1

    .line 1108
    .local v6, "retVal":J
    iget-object v5, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget v1, v5, Landroid/app/enterprise/ContextInfo;->mContainerId:I

    .line 1109
    .local v1, "containerId":I
    invoke-static {}, Landroid/os/PersonaManager;->getKnoxInfo()Landroid/os/Bundle;

    move-result-object v0

    .line 1111
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v5, "2.0"

    const-string v8, "version"

    invoke-virtual {v0, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1112
    invoke-static {}, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->getPasswordService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v4

    .line 1113
    .local v4, "passwordService":Landroid/app/enterprise/IPasswordPolicy;
    if-nez v4, :cond_0

    .line 1114
    sget-object v5, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v8, "ContainerPassword PolicyService is not yet ready!!!"

    invoke-static {v5, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1115
    long-to-int v5, v6

    .line 1144
    .end local v4    # "passwordService":Landroid/app/enterprise/IPasswordPolicy;
    :goto_0
    return v5

    .line 1119
    .restart local v4    # "passwordService":Landroid/app/enterprise/IPasswordPolicy;
    :cond_0
    :try_start_0
    iget-object v5, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v4, v5, p1}, Landroid/app/enterprise/IPasswordPolicy;->getMaximumTimeToLock(Landroid/app/enterprise/ContextInfo;Landroid/content/ComponentName;)J

    move-result-wide v6

    .line 1120
    cmp-long v5, v6, v10

    if-lez v5, :cond_1

    .line 1121
    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1144
    .end local v4    # "passwordService":Landroid/app/enterprise/IPasswordPolicy;
    :cond_1
    :goto_1
    long-to-int v5, v6

    goto :goto_0

    .line 1123
    .restart local v4    # "passwordService":Landroid/app/enterprise/IPasswordPolicy;
    :catch_0
    move-exception v3

    .line 1124
    .local v3, "e":Landroid/os/RemoteException;
    sget-object v5, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v8, "Failed talking with ContainerPasswordPolicy"

    invoke-static {v5, v8, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 1129
    .end local v3    # "e":Landroid/os/RemoteException;
    .end local v4    # "passwordService":Landroid/app/enterprise/IPasswordPolicy;
    :cond_2
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->getContainerService()Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy;

    move-result-object v2

    .line 1130
    .local v2, "containerService":Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy;
    if-nez v2, :cond_3

    .line 1131
    sget-object v5, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v8, "ContainerPolicy Service is not yet ready!!!"

    invoke-static {v5, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1132
    long-to-int v5, v6

    goto :goto_0

    .line 1136
    :cond_3
    :try_start_1
    invoke-interface {v2, v1, p1}, Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy;->getMaximumTimeToLock(ILandroid/content/ComponentName;)J

    move-result-wide v6

    .line 1137
    cmp-long v5, v6, v10

    if-lez v5, :cond_1

    .line 1138
    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 1140
    :catch_1
    move-exception v3

    .line 1141
    .restart local v3    # "e":Landroid/os/RemoteException;
    sget-object v5, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v8, "Failed getMaximumTimeToLock"

    invoke-static {v5, v8, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public getMinPasswordComplexChars(Landroid/content/ComponentName;)I
    .locals 7
    .param p1, "admin"    # Landroid/content/ComponentName;

    .prologue
    .line 528
    const/4 v3, 0x0

    .line 529
    .local v3, "retVal":I
    iget-object v5, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget v0, v5, Landroid/app/enterprise/ContextInfo;->mContainerId:I

    .line 531
    .local v0, "containerId":I
    invoke-static {}, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->getPasswordService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v2

    .line 532
    .local v2, "passwordService":Landroid/app/enterprise/IPasswordPolicy;
    if-nez v2, :cond_0

    .line 533
    sget-object v5, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v6, "ContainerPassword PolicyService is not yet ready!!!"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v4, v3

    .line 543
    .end local v3    # "retVal":I
    .local v4, "retVal":I
    :goto_0
    return v4

    .line 538
    .end local v4    # "retVal":I
    .restart local v3    # "retVal":I
    :cond_0
    :try_start_0
    iget-object v5, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v5, p1}, Landroid/app/enterprise/IPasswordPolicy;->getPasswordMinimumNonLetter(Landroid/app/enterprise/ContextInfo;Landroid/content/ComponentName;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    :goto_1
    move v4, v3

    .line 543
    .end local v3    # "retVal":I
    .restart local v4    # "retVal":I
    goto :goto_0

    .line 539
    .end local v4    # "retVal":I
    .restart local v3    # "retVal":I
    :catch_0
    move-exception v1

    .line 540
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v5, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v6, "Failed talking with password policy"

    invoke-static {v5, v6, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public getMinimumCharacterChangeLength()I
    .locals 6

    .prologue
    .line 1569
    const/4 v2, 0x0

    .line 1570
    .local v2, "retVal":I
    invoke-static {}, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->getPasswordService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    .line 1571
    .local v1, "passwordService":Landroid/app/enterprise/IPasswordPolicy;
    if-nez v1, :cond_0

    .line 1572
    sget-object v4, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v5, "ContainerPassword PolicyService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 1581
    .end local v2    # "retVal":I
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 1577
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":I
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v4}, Landroid/app/enterprise/IPasswordPolicy;->getMinimumCharacterChangeLength(Landroid/app/enterprise/ContextInfo;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 1581
    .end local v2    # "retVal":I
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 1578
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":I
    :catch_0
    move-exception v0

    .line 1579
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed getMinimumCharacterChangeLength!!!"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public getPasswordExpires(Landroid/content/ComponentName;)I
    .locals 8
    .param p1, "admin"    # Landroid/content/ComponentName;

    .prologue
    .line 315
    const/4 v3, 0x0

    .line 316
    .local v3, "retVal":I
    iget-object v6, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget v0, v6, Landroid/app/enterprise/ContextInfo;->mContainerId:I

    .line 318
    .local v0, "containerId":I
    invoke-static {}, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->getPasswordService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v2

    .line 319
    .local v2, "passwordService":Landroid/app/enterprise/IPasswordPolicy;
    if-nez v2, :cond_0

    .line 320
    sget-object v6, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v7, "ContainerPassword PolicyService is not yet ready!!!"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 334
    .end local v3    # "retVal":I
    :goto_0
    return v3

    .line 325
    .restart local v3    # "retVal":I
    :cond_0
    :try_start_0
    iget-object v6, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v6, p1}, Landroid/app/enterprise/IPasswordPolicy;->getPasswordExpirationTimeout(Landroid/app/enterprise/ContextInfo;Landroid/content/ComponentName;)J

    move-result-wide v4

    .line 326
    .local v4, "timeout":J
    const-wide/16 v6, 0x0

    cmp-long v6, v4, v6

    if-lez v6, :cond_1

    .line 327
    const-wide/32 v6, 0x5265c00

    div-long v6, v4, v6
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    long-to-int v3, v6

    goto :goto_0

    .line 329
    :cond_1
    const/4 v3, 0x0

    goto :goto_0

    .line 330
    .end local v4    # "timeout":J
    :catch_0
    move-exception v1

    .line 331
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v6, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v7, "Failed talking with password policy"

    invoke-static {v6, v7, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public getPasswordHistory(Landroid/content/ComponentName;)I
    .locals 7
    .param p1, "admin"    # Landroid/content/ComponentName;

    .prologue
    .line 418
    const/4 v3, -0x1

    .line 419
    .local v3, "retVal":I
    iget-object v5, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget v0, v5, Landroid/app/enterprise/ContextInfo;->mContainerId:I

    .line 421
    .local v0, "containerId":I
    invoke-static {}, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->getPasswordService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v2

    .line 422
    .local v2, "passwordService":Landroid/app/enterprise/IPasswordPolicy;
    if-nez v2, :cond_0

    .line 423
    sget-object v5, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v6, "ContainerPassword PolicyService is not yet ready!!!"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v4, v3

    .line 433
    .end local v3    # "retVal":I
    .local v4, "retVal":I
    :goto_0
    return v4

    .line 428
    .end local v4    # "retVal":I
    .restart local v3    # "retVal":I
    :cond_0
    :try_start_0
    iget-object v5, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v5, p1}, Landroid/app/enterprise/IPasswordPolicy;->getPasswordHistoryLength(Landroid/app/enterprise/ContextInfo;Landroid/content/ComponentName;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    :goto_1
    move v4, v3

    .line 433
    .end local v3    # "retVal":I
    .restart local v4    # "retVal":I
    goto :goto_0

    .line 429
    .end local v4    # "retVal":I
    .restart local v3    # "retVal":I
    :catch_0
    move-exception v1

    .line 430
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v5, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v6, "Failed talking with password policy"

    invoke-static {v5, v6, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public getPasswordMinimumLength(Landroid/content/ComponentName;)I
    .locals 7
    .param p1, "admin"    # Landroid/content/ComponentName;

    .prologue
    .line 2191
    const/4 v3, 0x0

    .line 2192
    .local v3, "retVal":I
    iget-object v5, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget v0, v5, Landroid/app/enterprise/ContextInfo;->mContainerId:I

    .line 2193
    .local v0, "containerId":I
    invoke-static {}, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->getPasswordService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v2

    .line 2194
    .local v2, "passwordService":Landroid/app/enterprise/IPasswordPolicy;
    if-nez v2, :cond_0

    .line 2195
    sget-object v5, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v6, "ContainerPassword PolicyService is not yet ready!!!"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v4, v3

    .line 2205
    .end local v3    # "retVal":I
    .local v4, "retVal":I
    :goto_0
    return v4

    .line 2200
    .end local v4    # "retVal":I
    .restart local v3    # "retVal":I
    :cond_0
    :try_start_0
    iget-object v5, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v5, p1}, Landroid/app/enterprise/IPasswordPolicy;->getPasswordMinimumLength(Landroid/app/enterprise/ContextInfo;Landroid/content/ComponentName;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    :goto_1
    move v4, v3

    .line 2205
    .end local v3    # "retVal":I
    .restart local v4    # "retVal":I
    goto :goto_0

    .line 2201
    .end local v4    # "retVal":I
    .restart local v3    # "retVal":I
    :catch_0
    move-exception v1

    .line 2202
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v5, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v6, "Failed talking with password policy"

    invoke-static {v5, v6, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public getPasswordQuality(Landroid/content/ComponentName;)I
    .locals 7
    .param p1, "admin"    # Landroid/content/ComponentName;

    .prologue
    .line 2094
    const/4 v3, 0x0

    .line 2095
    .local v3, "retVal":I
    iget-object v5, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget v0, v5, Landroid/app/enterprise/ContextInfo;->mContainerId:I

    .line 2096
    .local v0, "containerId":I
    invoke-static {}, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->getPasswordService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v2

    .line 2097
    .local v2, "passwordService":Landroid/app/enterprise/IPasswordPolicy;
    if-nez v2, :cond_0

    .line 2098
    sget-object v5, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v6, "ContainerPassword PolicyService is not yet ready!!!"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v4, v3

    .line 2108
    .end local v3    # "retVal":I
    .local v4, "retVal":I
    :goto_0
    return v4

    .line 2103
    .end local v4    # "retVal":I
    .restart local v3    # "retVal":I
    :cond_0
    :try_start_0
    iget-object v5, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v5, p1}, Landroid/app/enterprise/IPasswordPolicy;->getPasswordQuality(Landroid/app/enterprise/ContextInfo;Landroid/content/ComponentName;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    :goto_1
    move v4, v3

    .line 2108
    .end local v3    # "retVal":I
    .restart local v4    # "retVal":I
    goto :goto_0

    .line 2104
    .end local v4    # "retVal":I
    .restart local v3    # "retVal":I
    :catch_0
    move-exception v1

    .line 2105
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v5, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v6, "Failed talking with password policy"

    invoke-static {v5, v6, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public getRequiredPwdPatternRestrictions(Z)Ljava/lang/String;
    .locals 6
    .param p1, "allAdmins"    # Z

    .prologue
    .line 702
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerPasswordPolicy.getRequiredPwdPatternRestrictions"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 704
    const/4 v2, 0x0

    .line 705
    .local v2, "retVal":Ljava/lang/String;
    invoke-static {}, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->getPasswordService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    .line 706
    .local v1, "passwordService":Landroid/app/enterprise/IPasswordPolicy;
    if-nez v1, :cond_0

    .line 707
    sget-object v4, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v5, "ContainerPassword PolicyService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, v2

    .line 716
    .end local v2    # "retVal":Ljava/lang/String;
    .local v3, "retVal":Ljava/lang/String;
    :goto_0
    return-object v3

    .line 712
    .end local v3    # "retVal":Ljava/lang/String;
    .restart local v2    # "retVal":Ljava/lang/String;
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v4, p1}, Landroid/app/enterprise/IPasswordPolicy;->getRequiredPwdPatternRestrictions(Landroid/app/enterprise/ContextInfo;Z)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :goto_1
    move-object v3, v2

    .line 716
    .end local v2    # "retVal":Ljava/lang/String;
    .restart local v3    # "retVal":Ljava/lang/String;
    goto :goto_0

    .line 713
    .end local v3    # "retVal":Ljava/lang/String;
    .restart local v2    # "retVal":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 714
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed talking with ContainerPasswordPolicy"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public hasForbiddenCharacterSequence(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 6
    .param p1, "password"    # Ljava/lang/String;
    .param p2, "oldPassword"    # Ljava/lang/String;

    .prologue
    .line 754
    const/4 v2, 0x1

    .line 755
    .local v2, "retVal":Z
    invoke-static {}, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->getPasswordService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    .line 756
    .local v1, "passwordService":Landroid/app/enterprise/IPasswordPolicy;
    if-nez v1, :cond_0

    .line 757
    sget-object v4, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v5, "ContainerPassword PolicyService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 766
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 762
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v4, p1}, Landroid/app/enterprise/IPasswordPolicy;->hasForbiddenCharacterSequence(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 766
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 763
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 764
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed talking with ContainerPasswordPolicy"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public hasForbiddenData(Ljava/lang/String;)Z
    .locals 6
    .param p1, "password"    # Ljava/lang/String;

    .prologue
    .line 804
    const/4 v2, 0x1

    .line 805
    .local v2, "retVal":Z
    invoke-static {}, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->getPasswordService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    .line 806
    .local v1, "passwordService":Landroid/app/enterprise/IPasswordPolicy;
    if-nez v1, :cond_0

    .line 807
    sget-object v4, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v5, "ContainerPassword PolicyService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 816
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 812
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v4, p1}, Landroid/app/enterprise/IPasswordPolicy;->hasForbiddenData(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 816
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 813
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 814
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed talking with ContainerPasswordPolicy"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public hasForbiddenNumericSequence(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 6
    .param p1, "password"    # Ljava/lang/String;
    .param p2, "oldPassword"    # Ljava/lang/String;

    .prologue
    .line 729
    const/4 v2, 0x1

    .line 730
    .local v2, "retVal":Z
    invoke-static {}, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->getPasswordService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    .line 731
    .local v1, "passwordService":Landroid/app/enterprise/IPasswordPolicy;
    if-nez v1, :cond_0

    .line 732
    sget-object v4, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v5, "ContainerPassword PolicyService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 741
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 737
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v4, p1}, Landroid/app/enterprise/IPasswordPolicy;->hasForbiddenNumericSequence(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 741
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 738
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 739
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed talking with ContainerPasswordPolicy"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public hasForbiddenStringDistance(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 6
    .param p1, "password"    # Ljava/lang/String;
    .param p2, "oldPassword"    # Ljava/lang/String;

    .prologue
    .line 779
    const/4 v2, 0x1

    .line 780
    .local v2, "retVal":Z
    invoke-static {}, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->getPasswordService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    .line 781
    .local v1, "passwordService":Landroid/app/enterprise/IPasswordPolicy;
    if-nez v1, :cond_0

    .line 782
    sget-object v4, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v5, "ContainerPassword PolicyService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 791
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 787
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v4, p1, p2}, Landroid/app/enterprise/IPasswordPolicy;->hasForbiddenStringDistance(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 791
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 788
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 789
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed talking with ContainerPasswordPolicy"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public hasMaxRepeatedCharacters(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 6
    .param p1, "password"    # Ljava/lang/String;
    .param p2, "oldPassword"    # Ljava/lang/String;

    .prologue
    .line 830
    const/4 v2, 0x1

    .line 831
    .local v2, "retVal":Z
    invoke-static {}, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->getPasswordService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    .line 832
    .local v1, "passwordService":Landroid/app/enterprise/IPasswordPolicy;
    if-nez v1, :cond_0

    .line 833
    sget-object v4, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v5, "ContainerPassword PolicyService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 842
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 838
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v4, p1}, Landroid/app/enterprise/IPasswordPolicy;->hasMaxRepeatedCharacters(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 842
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 839
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 840
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed talking with ContainerPasswordPolicy"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public isActivePasswordSufficient()Z
    .locals 6

    .prologue
    .line 1990
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerPasswordPolicy.isActivePasswordSufficient"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1992
    const/4 v2, 0x0

    .line 1993
    .local v2, "retVal":Z
    invoke-static {}, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->getPasswordService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    .line 1994
    .local v1, "passwordService":Landroid/app/enterprise/IPasswordPolicy;
    if-nez v1, :cond_0

    .line 1995
    sget-object v4, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v5, "ContainerPassword PolicyService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 2004
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 2000
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v4}, Landroid/app/enterprise/IPasswordPolicy;->isActivePasswordSufficient(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 2004
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 2001
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 2002
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed talking with ContainerPasswordPolicy"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public isChangeRequested()I
    .locals 6

    .prologue
    .line 877
    const/4 v2, 0x0

    .line 878
    .local v2, "retVal":I
    invoke-static {}, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->getPasswordService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    .line 879
    .local v1, "passwordService":Landroid/app/enterprise/IPasswordPolicy;
    if-nez v1, :cond_0

    .line 880
    sget-object v4, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v5, "ContainerPassword PolicyService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 889
    .end local v2    # "retVal":I
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 885
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":I
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v4}, Landroid/app/enterprise/IPasswordPolicy;->isChangeRequested(Landroid/app/enterprise/ContextInfo;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 889
    .end local v2    # "retVal":I
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 886
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":I
    :catch_0
    move-exception v0

    .line 887
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed talking with ContainerPasswordPolicy"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public isExternalStorageForFailedPasswordsWipeExcluded()Z
    .locals 6

    .prologue
    .line 1955
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerPasswordPolicy.isExternalStorageForFailedPasswordsWipeExcluded"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1958
    const/4 v2, 0x0

    .line 1959
    .local v2, "retVal":Z
    invoke-static {}, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->getPasswordService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    .line 1960
    .local v1, "passwordService":Landroid/app/enterprise/IPasswordPolicy;
    if-nez v1, :cond_0

    .line 1961
    sget-object v4, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v5, "ContainerPassword PolicyService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 1970
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 1966
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v4}, Landroid/app/enterprise/IPasswordPolicy;->isExternalStorageForFailedPasswordsWipeExcluded(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 1970
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 1967
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 1968
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed talking with password policy"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public isPasswordPatternMatched(Ljava/lang/String;)Z
    .locals 6
    .param p1, "password"    # Ljava/lang/String;

    .prologue
    .line 855
    const/4 v2, 0x1

    .line 856
    .local v2, "retVal":Z
    invoke-static {}, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->getPasswordService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    .line 857
    .local v1, "passwordService":Landroid/app/enterprise/IPasswordPolicy;
    if-nez v1, :cond_0

    .line 858
    sget-object v4, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v5, "ContainerPassword PolicyService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 867
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 863
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v4, p1}, Landroid/app/enterprise/IPasswordPolicy;->isPasswordPatternMatched(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 867
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 864
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 865
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed talking with ContainerPasswordPolicy"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public isPasswordVisibilityEnabled()Z
    .locals 6

    .prologue
    .line 1868
    const/4 v2, 0x1

    .line 1869
    .local v2, "retVal":Z
    invoke-static {}, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->getPasswordService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    .line 1870
    .local v1, "passwordService":Landroid/app/enterprise/IPasswordPolicy;
    if-nez v1, :cond_0

    .line 1871
    sget-object v4, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v5, "ContainerPassword PolicyService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 1880
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 1876
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v4}, Landroid/app/enterprise/IPasswordPolicy;->isPasswordVisibilityEnabled(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 1880
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 1877
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 1878
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed talking with password policy"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public resetPassword()Z
    .locals 8

    .prologue
    .line 2245
    iget-object v5, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v6, "ContainerPasswordPolicy.resetPassword"

    invoke-static {v5, v6}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2247
    const/4 v3, 0x0

    .line 2248
    .local v3, "retVal":Z
    invoke-static {}, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->getMUMContainerService()Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    move-result-object v1

    .line 2249
    .local v1, "mumService":Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    if-nez v1, :cond_0

    .line 2250
    sget-object v5, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v6, "ContainerPolicy Service is not yet ready!!!"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v4, v3

    .line 2263
    .end local v3    # "retVal":Z
    .local v4, "retVal":I
    :goto_0
    return v4

    .line 2255
    .end local v4    # "retVal":I
    .restart local v3    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v5, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-interface {v1, v5, v6, v7}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager;->forceResetPassword(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;I)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 2256
    .local v2, "retCode":I
    if-ltz v2, :cond_1

    .line 2257
    const/4 v3, 0x1

    .end local v2    # "retCode":I
    :cond_1
    :goto_1
    move v4, v3

    .line 2263
    .restart local v4    # "retVal":I
    goto :goto_0

    .line 2259
    .end local v4    # "retVal":I
    :catch_0
    move-exception v0

    .line 2260
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v5, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v6, "Failed talking with ContainerPasswordPolicy "

    invoke-static {v5, v6, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public setForbiddenStrings(Ljava/util/List;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1632
    .local p1, "forbiddenStrings":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerPasswordPolicy.setForbiddenStrings"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1634
    const/4 v2, 0x0

    .line 1635
    .local v2, "retVal":Z
    invoke-static {}, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->getPasswordService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    .line 1636
    .local v1, "passwordService":Landroid/app/enterprise/IPasswordPolicy;
    if-nez v1, :cond_0

    .line 1637
    sget-object v4, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v5, "ContainerPassword PolicyService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 1647
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 1642
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v4, p1}, Landroid/app/enterprise/IPasswordPolicy;->setForbiddenStrings(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 1647
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 1643
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 1644
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed setDataForbidden!!!"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public setMaximumCharacterOccurrences(I)Z
    .locals 6
    .param p1, "count"    # I

    .prologue
    .line 1746
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerPasswordPolicy.setMaximumCharacterOccurrences"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1748
    const/4 v2, 0x0

    .line 1749
    .local v2, "retVal":Z
    invoke-static {}, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->getPasswordService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    .line 1750
    .local v1, "passwordService":Landroid/app/enterprise/IPasswordPolicy;
    if-nez v1, :cond_0

    .line 1751
    sget-object v4, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v5, "ContainerPassword PolicyService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 1761
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 1756
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v4, p1}, Landroid/app/enterprise/IPasswordPolicy;->setMaximumCharacterOccurrences(Landroid/app/enterprise/ContextInfo;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 1761
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 1757
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 1758
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed setMaxRepeatedCharacters!!!"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public setMaximumCharacterSequenceLength(I)Z
    .locals 6
    .param p1, "length"    # I

    .prologue
    .line 1424
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerPasswordPolicy.setMaximumCharacterSequenceLength"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1426
    const/4 v2, 0x0

    .line 1427
    .local v2, "retVal":Z
    invoke-static {}, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->getPasswordService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    .line 1428
    .local v1, "passwordService":Landroid/app/enterprise/IPasswordPolicy;
    if-nez v1, :cond_0

    .line 1429
    sget-object v4, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v5, "ContainerPassword PolicyService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 1439
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 1434
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v4, p1}, Landroid/app/enterprise/IPasswordPolicy;->setMaximumCharacterSequenceLength(Landroid/app/enterprise/ContextInfo;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 1439
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 1435
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 1436
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed setMaximumCharacterSequenceLength!!!"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public setMaximumFailedPasswordsForDeviceDisable(Landroid/content/ComponentName;I)Z
    .locals 7
    .param p1, "admin"    # Landroid/content/ComponentName;
    .param p2, "num"    # I

    .prologue
    .line 1201
    iget-object v5, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v6, "ContainerPasswordPolicy.setMaximumFailedPasswordsForDeviceDisable"

    invoke-static {v5, v6}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1204
    const/4 v3, 0x0

    .line 1205
    .local v3, "retVal":Z
    iget-object v5, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget v0, v5, Landroid/app/enterprise/ContextInfo;->mContainerId:I

    .line 1206
    .local v0, "containerId":I
    invoke-static {}, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->getPasswordService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v2

    .line 1207
    .local v2, "passwordService":Landroid/app/enterprise/IPasswordPolicy;
    if-nez v2, :cond_0

    .line 1208
    sget-object v5, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v6, "ContainerPassword PolicyService is not yet ready!!!"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v4, v3

    .line 1218
    .end local v3    # "retVal":Z
    .local v4, "retVal":I
    :goto_0
    return v4

    .line 1213
    .end local v4    # "retVal":I
    .restart local v3    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v5, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v5, p2}, Landroid/app/enterprise/IPasswordPolicy;->setMaximumFailedPasswordsForDisable(Landroid/app/enterprise/ContextInfo;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    :goto_1
    move v4, v3

    .line 1218
    .restart local v4    # "retVal":I
    goto :goto_0

    .line 1214
    .end local v4    # "retVal":I
    :catch_0
    move-exception v1

    .line 1215
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v5, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v6, "Failed talking with ContainerPasswordPolicy"

    invoke-static {v5, v6, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public setMaximumNumericSequenceLength(I)Z
    .locals 6
    .param p1, "length"    # I

    .prologue
    .line 1317
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerPasswordPolicy.setMaximumNumericSequenceLength"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1319
    const/4 v2, 0x0

    .line 1320
    .local v2, "retVal":Z
    invoke-static {}, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->getPasswordService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    .line 1321
    .local v1, "passwordService":Landroid/app/enterprise/IPasswordPolicy;
    if-nez v1, :cond_0

    .line 1322
    sget-object v4, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v5, "ContainerPassword PolicyService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 1332
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 1327
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v4, p1}, Landroid/app/enterprise/IPasswordPolicy;->setMaximumNumericSequenceLength(Landroid/app/enterprise/ContextInfo;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 1332
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 1328
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 1329
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed setNumericSequencesForbidden!!!"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public setMaximumTimeToLock(Landroid/content/ComponentName;I)Z
    .locals 12
    .param p1, "admin"    # Landroid/content/ComponentName;
    .param p2, "time"    # I

    .prologue
    const-wide/16 v10, 0x3e8

    .line 1028
    iget-object v7, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v8, "ContainerPasswordPolicy.setMaximumTimeToLock"

    invoke-static {v7, v8}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1030
    const/4 v5, 0x0

    .line 1031
    .local v5, "retVal":Z
    iget-object v7, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget v1, v7, Landroid/app/enterprise/ContextInfo;->mContainerId:I

    .line 1032
    .local v1, "containerId":I
    invoke-static {}, Landroid/os/PersonaManager;->getKnoxInfo()Landroid/os/Bundle;

    move-result-object v0

    .line 1034
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v7, "2.0"

    const-string v8, "version"

    invoke-virtual {v0, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1035
    invoke-static {}, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->getPasswordService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v4

    .line 1036
    .local v4, "passwordService":Landroid/app/enterprise/IPasswordPolicy;
    if-nez v4, :cond_0

    .line 1037
    sget-object v7, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v8, "ContainerPassword PolicyService is not yet ready!!!"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v6, v5

    .line 1063
    .end local v4    # "passwordService":Landroid/app/enterprise/IPasswordPolicy;
    .end local v5    # "retVal":Z
    .local v6, "retVal":I
    :goto_0
    return v6

    .line 1042
    .end local v6    # "retVal":I
    .restart local v4    # "passwordService":Landroid/app/enterprise/IPasswordPolicy;
    .restart local v5    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v7, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    int-to-long v8, p2

    mul-long/2addr v8, v10

    invoke-interface {v4, v7, p1, v8, v9}, Landroid/app/enterprise/IPasswordPolicy;->setMaximumTimeToLock(Landroid/app/enterprise/ContextInfo;Landroid/content/ComponentName;J)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1043
    const/4 v5, 0x1

    .end local v4    # "passwordService":Landroid/app/enterprise/IPasswordPolicy;
    :goto_1
    move v6, v5

    .line 1063
    .restart local v6    # "retVal":I
    goto :goto_0

    .line 1044
    .end local v6    # "retVal":I
    .restart local v4    # "passwordService":Landroid/app/enterprise/IPasswordPolicy;
    :catch_0
    move-exception v3

    .line 1045
    .local v3, "e":Landroid/os/RemoteException;
    sget-object v7, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v8, "Failed talking with ContainerPasswordPolicy"

    invoke-static {v7, v8, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 1050
    .end local v3    # "e":Landroid/os/RemoteException;
    .end local v4    # "passwordService":Landroid/app/enterprise/IPasswordPolicy;
    :cond_1
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->getContainerService()Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy;

    move-result-object v2

    .line 1051
    .local v2, "containerService":Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy;
    if-nez v2, :cond_2

    .line 1052
    sget-object v7, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v8, "ContainerPolicy Service is not yet ready!!!"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v6, v5

    .line 1053
    .restart local v6    # "retVal":I
    goto :goto_0

    .line 1057
    .end local v6    # "retVal":I
    :cond_2
    int-to-long v8, p2

    mul-long/2addr v8, v10

    :try_start_1
    invoke-interface {v2, v1, p1, v8, v9}, Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy;->setMaximumTimeToLock(ILandroid/content/ComponentName;J)Z
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v5

    goto :goto_1

    .line 1059
    :catch_1
    move-exception v3

    .line 1060
    .restart local v3    # "e":Landroid/os/RemoteException;
    sget-object v7, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v8, "Failed setMaximumTimeToLock"

    invoke-static {v7, v8, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public setMinPasswordComplexChars(Landroid/content/ComponentName;I)V
    .locals 5
    .param p1, "admin"    # Landroid/content/ComponentName;
    .param p2, "size"    # I

    .prologue
    .line 487
    iget-object v3, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v4, "ContainerPasswordPolicy.setMinPasswordComplexChars"

    invoke-static {v3, v4}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 489
    iget-object v3, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget v0, v3, Landroid/app/enterprise/ContextInfo;->mContainerId:I

    .line 491
    .local v0, "containerId":I
    invoke-static {}, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->getPasswordService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v2

    .line 492
    .local v2, "passwordService":Landroid/app/enterprise/IPasswordPolicy;
    if-nez v2, :cond_0

    .line 493
    sget-object v3, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v4, "ContainerPassword PolicyService is not yet ready!!!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 502
    :goto_0
    return-void

    .line 498
    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3, p1, p2}, Landroid/app/enterprise/IPasswordPolicy;->setPasswordMinimumNonLetter(Landroid/app/enterprise/ContextInfo;Landroid/content/ComponentName;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 499
    :catch_0
    move-exception v1

    .line 500
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v3, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v4, "Failed talking with password policy"

    invoke-static {v3, v4, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public setMinimumCharacterChangeLength(I)Z
    .locals 6
    .param p1, "length"    # I

    .prologue
    .line 1531
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerPasswordPolicy.setMinimumCharacterChangeLength"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1533
    const/4 v2, 0x0

    .line 1534
    .local v2, "retVal":Z
    invoke-static {}, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->getPasswordService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    .line 1535
    .local v1, "passwordService":Landroid/app/enterprise/IPasswordPolicy;
    if-nez v1, :cond_0

    .line 1536
    sget-object v4, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v5, "ContainerPassword PolicyService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 1546
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 1541
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v4, p1}, Landroid/app/enterprise/IPasswordPolicy;->setMinimumCharacterChangeLength(Landroid/app/enterprise/ContextInfo;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 1546
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 1542
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 1543
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed setMinimumCharacterChangeLength!!!"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public setPasswordExpires(Landroid/content/ComponentName;I)V
    .locals 8
    .param p1, "admin"    # Landroid/content/ComponentName;
    .param p2, "value"    # I

    .prologue
    .line 279
    iget-object v3, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v4, "ContainerPasswordPolicy.setPasswordExpires"

    invoke-static {v3, v4}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 281
    iget-object v3, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget v0, v3, Landroid/app/enterprise/ContextInfo;->mContainerId:I

    .line 282
    .local v0, "containerId":I
    invoke-static {}, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->getPasswordService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v2

    .line 284
    .local v2, "passwordService":Landroid/app/enterprise/IPasswordPolicy;
    if-nez v2, :cond_0

    .line 285
    sget-object v3, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v4, "ContainerPassword PolicyService is not yet ready!!!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 295
    :goto_0
    return-void

    .line 290
    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    int-to-long v4, p2

    const-wide/32 v6, 0x5265c00

    mul-long/2addr v4, v6

    invoke-interface {v2, v3, p1, v4, v5}, Landroid/app/enterprise/IPasswordPolicy;->setPasswordExpirationTimeout(Landroid/app/enterprise/ContextInfo;Landroid/content/ComponentName;J)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 292
    :catch_0
    move-exception v1

    .line 293
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v3, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v4, "Failed talking with password policy"

    invoke-static {v3, v4, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public setPasswordHistory(Landroid/content/ComponentName;I)V
    .locals 5
    .param p1, "admin"    # Landroid/content/ComponentName;
    .param p2, "value"    # I

    .prologue
    .line 380
    iget-object v3, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v4, "ContainerPasswordPolicy.setPasswordHistory"

    invoke-static {v3, v4}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 381
    iget-object v3, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget v0, v3, Landroid/app/enterprise/ContextInfo;->mContainerId:I

    .line 383
    .local v0, "containerId":I
    invoke-static {}, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->getPasswordService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v2

    .line 384
    .local v2, "passwordService":Landroid/app/enterprise/IPasswordPolicy;
    if-nez v2, :cond_0

    .line 385
    sget-object v3, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v4, "ContainerPassword PolicyService is not yet ready!!!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 394
    :goto_0
    return-void

    .line 390
    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3, p1, p2}, Landroid/app/enterprise/IPasswordPolicy;->setPasswordHistoryLength(Landroid/app/enterprise/ContextInfo;Landroid/content/ComponentName;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 391
    :catch_0
    move-exception v1

    .line 392
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v3, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v4, "Failed talking with password policy"

    invoke-static {v3, v4, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public setPasswordMinimumLength(Landroid/content/ComponentName;I)V
    .locals 5
    .param p1, "admin"    # Landroid/content/ComponentName;
    .param p2, "length"    # I

    .prologue
    .line 2160
    iget-object v3, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v4, "ContainerPasswordPolicy.setPasswordMinimumLength"

    invoke-static {v3, v4}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2162
    iget-object v3, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget v0, v3, Landroid/app/enterprise/ContextInfo;->mContainerId:I

    .line 2163
    .local v0, "containerId":I
    invoke-static {}, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->getPasswordService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v2

    .line 2164
    .local v2, "passwordService":Landroid/app/enterprise/IPasswordPolicy;
    if-nez v2, :cond_0

    .line 2165
    sget-object v3, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v4, "ContainerPassword PolicyService is not yet ready!!!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2174
    :goto_0
    return-void

    .line 2170
    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3, p1, p2}, Landroid/app/enterprise/IPasswordPolicy;->setPasswordMinimumLength(Landroid/app/enterprise/ContextInfo;Landroid/content/ComponentName;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2171
    :catch_0
    move-exception v1

    .line 2172
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v3, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v4, "Failed talking with password policy"

    invoke-static {v3, v4, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public setPasswordQuality(Landroid/content/ComponentName;I)V
    .locals 5
    .param p1, "admin"    # Landroid/content/ComponentName;
    .param p2, "quality"    # I

    .prologue
    .line 2056
    iget-object v3, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v4, "ContainerPasswordPolicy.setPasswordQuality"

    invoke-static {v3, v4}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2058
    iget-object v3, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget v0, v3, Landroid/app/enterprise/ContextInfo;->mContainerId:I

    .line 2059
    .local v0, "containerId":I
    invoke-static {}, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->getPasswordService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v2

    .line 2060
    .local v2, "passwordService":Landroid/app/enterprise/IPasswordPolicy;
    if-nez v2, :cond_0

    .line 2061
    sget-object v3, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v4, "ContainerPassword PolicyService is not yet ready!!!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2070
    :goto_0
    return-void

    .line 2066
    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3, p1, p2}, Landroid/app/enterprise/IPasswordPolicy;->setPasswordQuality(Landroid/app/enterprise/ContextInfo;Landroid/content/ComponentName;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2067
    :catch_0
    move-exception v1

    .line 2068
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v3, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v4, "Failed talking with password policy"

    invoke-static {v3, v4, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public setPasswordVisibilityEnabled(Z)Z
    .locals 6
    .param p1, "allow"    # Z

    .prologue
    .line 1836
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerPasswordPolicy.setPasswordVisibilityEnabled"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1838
    const/4 v2, 0x0

    .line 1839
    .local v2, "retVal":Z
    invoke-static {}, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->getPasswordService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    .line 1840
    .local v1, "passwordService":Landroid/app/enterprise/IPasswordPolicy;
    if-nez v1, :cond_0

    .line 1841
    sget-object v4, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v5, "ContainerPassword PolicyService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 1850
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 1846
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v4, p1}, Landroid/app/enterprise/IPasswordPolicy;->setPasswordVisibilityEnabled(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 1850
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 1847
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 1848
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed talking with password policy"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public setPwdChangeRequested(I)Z
    .locals 6
    .param p1, "flag"    # I

    .prologue
    .line 964
    const/4 v2, 0x0

    .line 965
    .local v2, "retVal":Z
    invoke-static {}, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->getPasswordService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    .line 966
    .local v1, "passwordService":Landroid/app/enterprise/IPasswordPolicy;
    if-nez v1, :cond_0

    .line 967
    sget-object v4, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v5, "ContainerPassword PolicyService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 976
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 972
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v4, p1}, Landroid/app/enterprise/IPasswordPolicy;->setPwdChangeRequested(Landroid/app/enterprise/ContextInfo;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 976
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 973
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 974
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed talking with ContainerPasswordPolicy"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public setRequiredPasswordPattern(Ljava/lang/String;)Z
    .locals 6
    .param p1, "regex"    # Ljava/lang/String;

    .prologue
    .line 598
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerPasswordPolicy.setRequiredPasswordPattern"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 600
    const/4 v2, 0x0

    .line 601
    .local v2, "retVal":Z
    invoke-static {}, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->getPasswordService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    .line 602
    .local v1, "passwordService":Landroid/app/enterprise/IPasswordPolicy;
    if-nez v1, :cond_0

    .line 603
    sget-object v4, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v5, "ContainerPassword PolicyService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 612
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 608
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v4, p1}, Landroid/app/enterprise/IPasswordPolicy;->setRequiredPasswordPattern(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 612
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 609
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 610
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed talking with ContainerPasswordPolicy"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method
