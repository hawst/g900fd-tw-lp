.class public abstract Lcom/sec/enterprise/knox/IIntegrityResultSubscriber$Stub;
.super Landroid/os/Binder;
.source "IIntegrityResultSubscriber.java"

# interfaces
.implements Lcom/sec/enterprise/knox/IIntegrityResultSubscriber;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/enterprise/knox/IIntegrityResultSubscriber;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/enterprise/knox/IIntegrityResultSubscriber$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.sec.enterprise.knox.IIntegrityResultSubscriber"

.field static final TRANSACTION_onError:I = 0x6

.field static final TRANSACTION_onProgress:I = 0x2

.field static final TRANSACTION_onReady:I = 0x1

.field static final TRANSACTION_onRuntimeViolation:I = 0x4

.field static final TRANSACTION_onSuccess:I = 0x7

.field static final TRANSACTION_onTimaViolation:I = 0x5

.field static final TRANSACTION_onViolation:I = 0x3


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 15
    const-string v0, "com.sec.enterprise.knox.IIntegrityResultSubscriber"

    invoke-virtual {p0, p0, v0}, Lcom/sec/enterprise/knox/IIntegrityResultSubscriber$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 16
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/sec/enterprise/knox/IIntegrityResultSubscriber;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 23
    if-nez p0, :cond_0

    .line 24
    const/4 v0, 0x0

    .line 30
    :goto_0
    return-object v0

    .line 26
    :cond_0
    const-string v1, "com.sec.enterprise.knox.IIntegrityResultSubscriber"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 27
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/sec/enterprise/knox/IIntegrityResultSubscriber;

    if-eqz v1, :cond_1

    .line 28
    check-cast v0, Lcom/sec/enterprise/knox/IIntegrityResultSubscriber;

    goto :goto_0

    .line 30
    :cond_1
    new-instance v0, Lcom/sec/enterprise/knox/IIntegrityResultSubscriber$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/sec/enterprise/knox/IIntegrityResultSubscriber$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 8
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    .line 38
    sparse-switch p1, :sswitch_data_0

    .line 120
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v0

    :goto_0
    return v0

    .line 42
    :sswitch_0
    const-string v0, "com.sec.enterprise.knox.IIntegrityResultSubscriber"

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    move v0, v7

    .line 43
    goto :goto_0

    .line 47
    :sswitch_1
    const-string v0, "com.sec.enterprise.knox.IIntegrityResultSubscriber"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 48
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/IIntegrityResultSubscriber$Stub;->onReady()V

    move v0, v7

    .line 49
    goto :goto_0

    .line 53
    :sswitch_2
    const-string v0, "com.sec.enterprise.knox.IIntegrityResultSubscriber"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 55
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 57
    .local v1, "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 58
    .local v2, "_arg1":I
    invoke-virtual {p0, v1, v2}, Lcom/sec/enterprise/knox/IIntegrityResultSubscriber$Stub;->onProgress(II)V

    move v0, v7

    .line 59
    goto :goto_0

    .line 63
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    :sswitch_3
    const-string v0, "com.sec.enterprise.knox.IIntegrityResultSubscriber"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 65
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 67
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 69
    .restart local v2    # "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 71
    .local v3, "_arg2":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v4

    .line 73
    .local v4, "_arg3":[B
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v5

    .line 75
    .local v5, "_arg4":[B
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v6

    .local v6, "_arg5":I
    move-object v0, p0

    .line 76
    invoke-virtual/range {v0 .. v6}, Lcom/sec/enterprise/knox/IIntegrityResultSubscriber$Stub;->onViolation(IILjava/lang/String;[B[BI)V

    move v0, v7

    .line 77
    goto :goto_0

    .line 81
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    .end local v3    # "_arg2":Ljava/lang/String;
    .end local v4    # "_arg3":[B
    .end local v5    # "_arg4":[B
    .end local v6    # "_arg5":I
    :sswitch_4
    const-string v0, "com.sec.enterprise.knox.IIntegrityResultSubscriber"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 83
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 85
    .local v1, "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 86
    .local v2, "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Lcom/sec/enterprise/knox/IIntegrityResultSubscriber$Stub;->onRuntimeViolation(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v7

    .line 87
    goto :goto_0

    .line 91
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":Ljava/lang/String;
    :sswitch_5
    const-string v0, "com.sec.enterprise.knox.IIntegrityResultSubscriber"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 93
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 94
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/sec/enterprise/knox/IIntegrityResultSubscriber$Stub;->onTimaViolation(Ljava/lang/String;)V

    move v0, v7

    .line 95
    goto :goto_0

    .line 99
    .end local v1    # "_arg0":Ljava/lang/String;
    :sswitch_6
    const-string v0, "com.sec.enterprise.knox.IIntegrityResultSubscriber"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 101
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 103
    .local v1, "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 105
    .local v2, "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 107
    .restart local v3    # "_arg2":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 108
    .local v4, "_arg3":I
    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/sec/enterprise/knox/IIntegrityResultSubscriber$Stub;->onError(IILjava/lang/String;I)V

    move v0, v7

    .line 109
    goto :goto_0

    .line 113
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    .end local v3    # "_arg2":Ljava/lang/String;
    .end local v4    # "_arg3":I
    :sswitch_7
    const-string v0, "com.sec.enterprise.knox.IIntegrityResultSubscriber"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 115
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 116
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/sec/enterprise/knox/IIntegrityResultSubscriber$Stub;->onSuccess(I)V

    move v0, v7

    .line 117
    goto/16 :goto_0

    .line 38
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
