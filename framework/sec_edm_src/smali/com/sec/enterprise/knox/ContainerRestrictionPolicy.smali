.class public Lcom/sec/enterprise/knox/ContainerRestrictionPolicy;
.super Ljava/lang/Object;
.source "ContainerRestrictionPolicy.java"


# static fields
.field private static TAG:Ljava/lang/String;

.field private static gRestrictionService:Landroid/app/enterprise/IRestrictionPolicy;


# instance fields
.field private mContextInfo:Landroid/app/enterprise/ContextInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    const-string v0, "ContainerRestrictionPolicy"

    sput-object v0, Lcom/sec/enterprise/knox/ContainerRestrictionPolicy;->TAG:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroid/app/enterprise/ContextInfo;)V
    .locals 0
    .param p1, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    iput-object p1, p0, Lcom/sec/enterprise/knox/ContainerRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    .line 70
    return-void
.end method

.method static declared-synchronized getRestrictionService()Landroid/app/enterprise/IRestrictionPolicy;
    .locals 2

    .prologue
    .line 60
    const-class v1, Lcom/sec/enterprise/knox/ContainerRestrictionPolicy;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/enterprise/knox/ContainerRestrictionPolicy;->gRestrictionService:Landroid/app/enterprise/IRestrictionPolicy;

    if-nez v0, :cond_0

    .line 61
    const-string v0, "restriction_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/ContainerRestrictionPolicy;->gRestrictionService:Landroid/app/enterprise/IRestrictionPolicy;

    .line 65
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/ContainerRestrictionPolicy;->gRestrictionService:Landroid/app/enterprise/IRestrictionPolicy;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 60
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public allowShareList(Z)Z
    .locals 6
    .param p1, "allow"    # Z

    .prologue
    .line 293
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerRestrictionPolicy.allowShareList"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 295
    const/4 v2, 0x0

    .line 296
    .local v2, "retVal":Z
    invoke-static {}, Lcom/sec/enterprise/knox/ContainerRestrictionPolicy;->getRestrictionService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    .line 297
    .local v1, "restrictionService":Landroid/app/enterprise/IRestrictionPolicy;
    if-nez v1, :cond_0

    .line 298
    sget-object v4, Lcom/sec/enterprise/knox/ContainerRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v5, "ContainerRestriction PolicyService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 307
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 303
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v4, p1}, Landroid/app/enterprise/IRestrictionPolicy;->allowShareList(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 307
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 304
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 305
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed talking with restriction policy"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public isCameraEnabled(Z)Z
    .locals 6
    .param p1, "showMsg"    # Z

    .prologue
    .line 151
    const/4 v2, 0x1

    .line 152
    .local v2, "retVal":Z
    invoke-static {}, Lcom/sec/enterprise/knox/ContainerRestrictionPolicy;->getRestrictionService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    .line 153
    .local v1, "restrictionService":Landroid/app/enterprise/IRestrictionPolicy;
    if-nez v1, :cond_0

    .line 154
    sget-object v4, Lcom/sec/enterprise/knox/ContainerRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v5, "ContainerRestriction PolicyService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 163
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 159
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v4, p1}, Landroid/app/enterprise/IRestrictionPolicy;->isCameraEnabled(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 163
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 160
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 161
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed talking with device info policy"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public isScreenCaptureEnabled(Z)Z
    .locals 6
    .param p1, "showMsg"    # Z

    .prologue
    .line 240
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerRestrictionPolicy.isScreenCaptureEnabled"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 242
    const/4 v2, 0x1

    .line 243
    .local v2, "retVal":Z
    invoke-static {}, Lcom/sec/enterprise/knox/ContainerRestrictionPolicy;->getRestrictionService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    .line 244
    .local v1, "restrictionService":Landroid/app/enterprise/IRestrictionPolicy;
    if-nez v1, :cond_0

    .line 245
    sget-object v4, Lcom/sec/enterprise/knox/ContainerRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v5, "ContainerRestriction PolicyService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 254
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 250
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v4, p1}, Landroid/app/enterprise/IRestrictionPolicy;->isScreenCaptureEnabled(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 254
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 251
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 252
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed talking with misc policy"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public isShareListAllowed()Z
    .locals 6

    .prologue
    .line 327
    const/4 v2, 0x1

    .line 328
    .local v2, "retVal":Z
    invoke-static {}, Lcom/sec/enterprise/knox/ContainerRestrictionPolicy;->getRestrictionService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    .line 329
    .local v1, "restrictionService":Landroid/app/enterprise/IRestrictionPolicy;
    if-nez v1, :cond_0

    .line 330
    sget-object v4, Lcom/sec/enterprise/knox/ContainerRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v5, "ContainerRestriction PolicyService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 339
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 335
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v5, 0x0

    invoke-interface {v1, v4, v5}, Landroid/app/enterprise/IRestrictionPolicy;->isShareListAllowed(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 339
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 336
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 337
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed talking with restriction policy"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public isUseSecureKeypadEnabled()Z
    .locals 6

    .prologue
    .line 417
    const/4 v2, 0x0

    .line 418
    .local v2, "retVal":Z
    invoke-static {}, Lcom/sec/enterprise/knox/ContainerRestrictionPolicy;->getRestrictionService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    .line 419
    .local v1, "restrictionService":Landroid/app/enterprise/IRestrictionPolicy;
    if-nez v1, :cond_0

    .line 420
    sget-object v4, Lcom/sec/enterprise/knox/ContainerRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v5, "ContainerRestriction PolicyService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 429
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 425
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v4}, Landroid/app/enterprise/IRestrictionPolicy;->isUseSecureKeypadEnabled(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 429
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 426
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 427
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed talking with misc policy"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public setCameraState(Z)Z
    .locals 6
    .param p1, "enable"    # Z

    .prologue
    .line 117
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerRestrictionPolicy.setCameraState"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 119
    const/4 v2, 0x0

    .line 120
    .local v2, "retVal":Z
    invoke-static {}, Lcom/sec/enterprise/knox/ContainerRestrictionPolicy;->getRestrictionService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    .line 121
    .local v1, "restrictionService":Landroid/app/enterprise/IRestrictionPolicy;
    if-nez v1, :cond_0

    .line 122
    sget-object v4, Lcom/sec/enterprise/knox/ContainerRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v5, "ContainerRestriction PolicyService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 131
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 127
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v4, p1}, Landroid/app/enterprise/IRestrictionPolicy;->setCamera(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 131
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 128
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 129
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed talking with misc info policy"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public setScreenCapture(Z)Z
    .locals 6
    .param p1, "enable"    # Z

    .prologue
    .line 208
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerRestrictionPolicy.setScreenCapture"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 210
    const/4 v2, 0x0

    .line 211
    .local v2, "retVal":Z
    invoke-static {}, Lcom/sec/enterprise/knox/ContainerRestrictionPolicy;->getRestrictionService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    .line 212
    .local v1, "restrictionService":Landroid/app/enterprise/IRestrictionPolicy;
    if-nez v1, :cond_0

    .line 213
    sget-object v4, Lcom/sec/enterprise/knox/ContainerRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v5, "ContainerRestriction PolicyService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 222
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 218
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v4, p1}, Landroid/app/enterprise/IRestrictionPolicy;->setScreenCapture(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 222
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 219
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 220
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed talking with misc policy"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public setUseSecureKeypad(Z)Z
    .locals 6
    .param p1, "enable"    # Z

    .prologue
    .line 383
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerRestrictionPolicy.setUseSecureKeypad"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 385
    const/4 v2, 0x0

    .line 386
    .local v2, "retVal":Z
    invoke-static {}, Lcom/sec/enterprise/knox/ContainerRestrictionPolicy;->getRestrictionService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    .line 387
    .local v1, "restrictionService":Landroid/app/enterprise/IRestrictionPolicy;
    if-nez v1, :cond_0

    .line 388
    sget-object v4, Lcom/sec/enterprise/knox/ContainerRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v5, "ContainerRestriction PolicyService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 397
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 393
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v4, p1}, Landroid/app/enterprise/IRestrictionPolicy;->setUseSecureKeypad(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 397
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 394
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 395
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed talking with misc policy"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method
