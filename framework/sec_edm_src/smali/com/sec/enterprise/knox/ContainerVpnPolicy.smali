.class public Lcom/sec/enterprise/knox/ContainerVpnPolicy;
.super Ljava/lang/Object;
.source "ContainerVpnPolicy.java"


# static fields
.field private static final ADD_ALL_PACKAGES:Ljava/lang/String; = "ADD_ALL_PACKAGES"


# instance fields
.field private final TAG:Ljava/lang/String;

.field private final mContainerId:I

.field private mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/app/enterprise/EnterpriseResponseData",
            "<*>;"
        }
    .end annotation
.end field

.field private mEnterpriseVpnService:Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;


# direct methods
.method constructor <init>(I)V
    .locals 2
    .param p1, "containerId"    # I

    .prologue
    const/4 v1, 0x0

    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    const-string v0, "ContainerVpnPolicy"

    iput-object v0, p0, Lcom/sec/enterprise/knox/ContainerVpnPolicy;->TAG:Ljava/lang/String;

    .line 64
    iput-object v1, p0, Lcom/sec/enterprise/knox/ContainerVpnPolicy;->mEnterpriseVpnService:Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;

    .line 66
    iput-object v1, p0, Lcom/sec/enterprise/knox/ContainerVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    .line 72
    iput p1, p0, Lcom/sec/enterprise/knox/ContainerVpnPolicy;->mContainerId:I

    .line 73
    return-void
.end method

.method private getEnterpriseVpnPolicy()Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/enterprise/knox/ContainerVpnPolicy;->mEnterpriseVpnService:Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;

    if-nez v0, :cond_0

    .line 77
    const-string v0, "enterprise_premium_vpn_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/ContainerVpnPolicy;->mEnterpriseVpnService:Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;

    .line 80
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/ContainerVpnPolicy;->mEnterpriseVpnService:Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;

    return-object v0
.end method


# virtual methods
.method public addVpnProfile(Ljava/lang/String;)Z
    .locals 6
    .param p1, "profileName"    # Ljava/lang/String;

    .prologue
    .line 319
    new-instance v3, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v4

    invoke-direct {v3, v4}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const-string v4, "ContainerVpnPolicy.addVpnProfile"

    invoke-static {v3, v4}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 320
    const/4 v1, 0x0

    .line 321
    .local v1, "retVal":Z
    const/4 v2, 0x0

    .line 323
    .local v2, "serviceExist":Z
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerVpnPolicy;->getEnterpriseVpnPolicy()Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;

    move-result-object v3

    if-eqz v3, :cond_1

    const/4 v2, 0x1

    .line 324
    :goto_0
    if-eqz v2, :cond_0

    .line 325
    iget-object v3, p0, Lcom/sec/enterprise/knox/ContainerVpnPolicy;->mEnterpriseVpnService:Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;

    iget v4, p0, Lcom/sec/enterprise/knox/ContainerVpnPolicy;->mContainerId:I

    const-string v5, "ADD_ALL_PACKAGES"

    invoke-interface {v3, v4, v5, p1}, Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;->addVpnProfileToApp(ILjava/lang/String;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/enterprise/knox/ContainerVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    .line 326
    iget-object v3, p0, Lcom/sec/enterprise/knox/ContainerVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    if-eqz v3, :cond_0

    .line 327
    iget-object v3, p0, Lcom/sec/enterprise/knox/ContainerVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v3}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v3

    if-nez v3, :cond_0

    .line 328
    iget-object v3, p0, Lcom/sec/enterprise/knox/ContainerVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v3}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 334
    :cond_0
    :goto_1
    return v1

    .line 323
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 331
    :catch_0
    move-exception v0

    .line 332
    .local v0, "e":Landroid/os/RemoteException;
    const-string v3, "ContainerVpnPolicy"

    const-string v4, "Failed at ContainerVpnPolicy API addVpnProfile "

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public addVpnProfileToApp(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "profileName"    # Ljava/lang/String;

    .prologue
    .line 129
    new-instance v3, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v4

    invoke-direct {v3, v4}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const-string v4, "ContainerVpnPolicy.addVpnProfileToApp"

    invoke-static {v3, v4}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 130
    const/4 v1, 0x0

    .line 131
    .local v1, "retVal":Z
    const/4 v2, 0x0

    .line 133
    .local v2, "serviceExist":Z
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerVpnPolicy;->getEnterpriseVpnPolicy()Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;

    move-result-object v3

    if-eqz v3, :cond_1

    const/4 v2, 0x1

    .line 134
    :goto_0
    if-eqz v2, :cond_0

    .line 135
    iget-object v3, p0, Lcom/sec/enterprise/knox/ContainerVpnPolicy;->mEnterpriseVpnService:Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;

    iget v4, p0, Lcom/sec/enterprise/knox/ContainerVpnPolicy;->mContainerId:I

    invoke-interface {v3, v4, p1, p2}, Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;->addVpnProfileToApp(ILjava/lang/String;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/enterprise/knox/ContainerVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    .line 136
    iget-object v3, p0, Lcom/sec/enterprise/knox/ContainerVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    if-eqz v3, :cond_0

    .line 137
    iget-object v3, p0, Lcom/sec/enterprise/knox/ContainerVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v3}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v3

    if-nez v3, :cond_0

    .line 138
    iget-object v3, p0, Lcom/sec/enterprise/knox/ContainerVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v3}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 145
    :cond_0
    :goto_1
    return v1

    .line 133
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 142
    :catch_0
    move-exception v0

    .line 143
    .local v0, "e":Landroid/os/RemoteException;
    const-string v3, "ContainerVpnPolicy"

    const-string v4, "Failed at ContainerVpnPolicy API addVpnProfileToApp "

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public getAllPackagesForProfile(Ljava/lang/String;)[Ljava/lang/String;
    .locals 6
    .param p1, "profileName"    # Ljava/lang/String;

    .prologue
    .line 250
    new-instance v4, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v5

    invoke-direct {v4, v5}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const-string v5, "ContainerVpnPolicy.getAllPackagesForProfile"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 251
    const/4 v2, 0x0

    .line 252
    .local v2, "packageList":[Ljava/lang/String;
    const/4 v3, 0x0

    .line 254
    .local v3, "serviceExist":Z
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerVpnPolicy;->getEnterpriseVpnPolicy()Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;

    move-result-object v4

    if-eqz v4, :cond_1

    const/4 v3, 0x1

    .line 255
    :goto_0
    if-eqz v3, :cond_0

    .line 256
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerVpnPolicy;->mEnterpriseVpnService:Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;

    iget v5, p0, Lcom/sec/enterprise/knox/ContainerVpnPolicy;->mContainerId:I

    invoke-interface {v4, v5, p1}, Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;->getAllPackagesForProfile(ILjava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/enterprise/knox/ContainerVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    .line 257
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    if-eqz v4, :cond_0

    .line 258
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v4}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v4

    if-nez v4, :cond_0

    .line 259
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v4}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    move-object v0, v4

    check-cast v0, [Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 265
    :cond_0
    :goto_1
    return-object v2

    .line 254
    :cond_1
    const/4 v3, 0x0

    goto :goto_0

    .line 262
    :catch_0
    move-exception v1

    .line 263
    .local v1, "e":Landroid/os/RemoteException;
    const-string v4, "ContainerVpnPolicy"

    const-string v5, "Failed at ContainerVpnPolicy API getAllPackagesForProfile "

    invoke-static {v4, v5, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public removeVPNProfile()Z
    .locals 6

    .prologue
    .line 385
    new-instance v3, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v4

    invoke-direct {v3, v4}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const-string v4, "ContainerVpnPolicy.removeVPNProfile"

    invoke-static {v3, v4}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 386
    const/4 v1, 0x0

    .line 387
    .local v1, "retVal":Z
    const/4 v2, 0x0

    .line 389
    .local v2, "serviceExist":Z
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerVpnPolicy;->getEnterpriseVpnPolicy()Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;

    move-result-object v3

    if-eqz v3, :cond_1

    const/4 v2, 0x1

    .line 390
    :goto_0
    if-eqz v2, :cond_0

    .line 391
    iget-object v3, p0, Lcom/sec/enterprise/knox/ContainerVpnPolicy;->mEnterpriseVpnService:Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;

    iget v4, p0, Lcom/sec/enterprise/knox/ContainerVpnPolicy;->mContainerId:I

    const-string v5, "ADD_ALL_PACKAGES"

    invoke-interface {v3, v4, v5}, Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;->removeVpnForApplication(ILjava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/enterprise/knox/ContainerVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    .line 392
    iget-object v3, p0, Lcom/sec/enterprise/knox/ContainerVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    if-eqz v3, :cond_0

    .line 393
    iget-object v3, p0, Lcom/sec/enterprise/knox/ContainerVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v3}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v3

    if-nez v3, :cond_0

    .line 394
    iget-object v3, p0, Lcom/sec/enterprise/knox/ContainerVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v3}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 400
    :cond_0
    :goto_1
    return v1

    .line 389
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 397
    :catch_0
    move-exception v0

    .line 398
    .local v0, "e":Landroid/os/RemoteException;
    const-string v3, "ContainerVpnPolicy"

    const-string v4, "Failed at ContainerVpnPolicy API removeVPNProfile "

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public removeVpnForApplication(Ljava/lang/String;)Z
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 190
    new-instance v3, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v4

    invoke-direct {v3, v4}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const-string v4, "ContainerVpnPolicy.removeVpnForApplication"

    invoke-static {v3, v4}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 191
    const/4 v1, 0x0

    .line 192
    .local v1, "retVal":Z
    const/4 v2, 0x0

    .line 194
    .local v2, "serviceExist":Z
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerVpnPolicy;->getEnterpriseVpnPolicy()Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;

    move-result-object v3

    if-eqz v3, :cond_1

    const/4 v2, 0x1

    .line 195
    :goto_0
    if-eqz v2, :cond_0

    .line 196
    iget-object v3, p0, Lcom/sec/enterprise/knox/ContainerVpnPolicy;->mEnterpriseVpnService:Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;

    iget v4, p0, Lcom/sec/enterprise/knox/ContainerVpnPolicy;->mContainerId:I

    invoke-interface {v3, v4, p1}, Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;->removeVpnForApplication(ILjava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/enterprise/knox/ContainerVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    .line 197
    iget-object v3, p0, Lcom/sec/enterprise/knox/ContainerVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    if-eqz v3, :cond_0

    .line 198
    iget-object v3, p0, Lcom/sec/enterprise/knox/ContainerVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v3}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v3

    if-nez v3, :cond_0

    .line 199
    iget-object v3, p0, Lcom/sec/enterprise/knox/ContainerVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v3}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 205
    :cond_0
    :goto_1
    return v1

    .line 194
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 202
    :catch_0
    move-exception v0

    .line 203
    .local v0, "e":Landroid/os/RemoteException;
    const-string v3, "ContainerVpnPolicy"

    const-string v4, "Failed at ContainerVpnPolicy API addVpnProfileToApp "

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method
