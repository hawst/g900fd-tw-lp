.class public abstract Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy$Stub;
.super Landroid/os/Binder;
.source "IEnterpriseBillingPolicy.java"

# interfaces
.implements Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.sec.enterprise.knox.billing.IEnterpriseBillingPolicy"

.field static final TRANSACTION_activateProfile:I = 0x1b

.field static final TRANSACTION_addProfile:I = 0x1

.field static final TRANSACTION_addProfileForCurrentContainer:I = 0x1d

.field static final TRANSACTION_addVpnToBillingProfile:I = 0x15

.field static final TRANSACTION_addVpnToBillingProfileForCurrentContainer:I = 0x24

.field static final TRANSACTION_allowRoaming:I = 0x19

.field static final TRANSACTION_allowWifiFallback:I = 0x13

.field static final TRANSACTION_disableProfile:I = 0xa

.field static final TRANSACTION_disableProfileForApps:I = 0x9

.field static final TRANSACTION_disableProfileForContainer:I = 0x8

.field static final TRANSACTION_disableProfileForCurrentContainer:I = 0x20

.field static final TRANSACTION_enableProfileForApps:I = 0x7

.field static final TRANSACTION_enableProfileForContainer:I = 0x6

.field static final TRANSACTION_enableProfileForCurrentContainer:I = 0x1f

.field static final TRANSACTION_getApplicationsUsingProfile:I = 0x12

.field static final TRANSACTION_getAvailableProfiles:I = 0x4

.field static final TRANSACTION_getAvailableProfilesForCaller:I = 0x22

.field static final TRANSACTION_getContainersUsingProfile:I = 0x11

.field static final TRANSACTION_getProfileDetails:I = 0x5

.field static final TRANSACTION_getProfileForApplication:I = 0x10

.field static final TRANSACTION_getProfileForContainer:I = 0xf

.field static final TRANSACTION_getUidApntypeMappings:I = 0x18

.field static final TRANSACTION_getVpnsBoundToProfile:I = 0x17

.field static final TRANSACTION_isProfileActive:I = 0x1c

.field static final TRANSACTION_isProfileActiveByCaller:I = 0x21

.field static final TRANSACTION_isProfileEnabled:I = 0xb

.field static final TRANSACTION_isProfileTurnedOn:I = 0xe

.field static final TRANSACTION_isRoamingAllowed:I = 0x1a

.field static final TRANSACTION_isWifiFallbackAllowed:I = 0x14

.field static final TRANSACTION_removeProfile:I = 0x3

.field static final TRANSACTION_removeProfileForCurrentContainer:I = 0x1e

.field static final TRANSACTION_removeVpnFromBillingProfile:I = 0x16

.field static final TRANSACTION_removeVpnFromBillingProfileForCurrentContainer:I = 0x23

.field static final TRANSACTION_turnOffProfile:I = 0xd

.field static final TRANSACTION_turnOnProfile:I = 0xc

.field static final TRANSACTION_updateProfile:I = 0x2


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 16
    const-string v0, "com.sec.enterprise.knox.billing.IEnterpriseBillingPolicy"

    invoke-virtual {p0, p0, v0}, Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 17
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 24
    if-nez p0, :cond_0

    .line 25
    const/4 v0, 0x0

    .line 31
    :goto_0
    return-object v0

    .line 27
    :cond_0
    const-string v1, "com.sec.enterprise.knox.billing.IEnterpriseBillingPolicy"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 28
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;

    if-eqz v1, :cond_1

    .line 29
    check-cast v0, Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;

    goto :goto_0

    .line 31
    :cond_1
    new-instance v0, Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 35
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 11
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    const/4 v9, 0x1

    .line 39
    sparse-switch p1, :sswitch_data_0

    .line 641
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v9

    :goto_0
    return v9

    .line 43
    :sswitch_0
    const-string v8, "com.sec.enterprise.knox.billing.IEnterpriseBillingPolicy"

    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 48
    :sswitch_1
    const-string v10, "com.sec.enterprise.knox.billing.IEnterpriseBillingPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 50
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_1

    .line 51
    sget-object v10, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 57
    .local v0, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_1
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_2

    .line 58
    sget-object v10, Lcom/sec/enterprise/knox/billing/EnterpriseBillingProfile;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/enterprise/knox/billing/EnterpriseBillingProfile;

    .line 63
    .local v1, "_arg1":Lcom/sec/enterprise/knox/billing/EnterpriseBillingProfile;
    :goto_2
    invoke-virtual {p0, v0, v1}, Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy$Stub;->addProfile(Landroid/app/enterprise/ContextInfo;Lcom/sec/enterprise/knox/billing/EnterpriseBillingProfile;)Z

    move-result v6

    .line 64
    .local v6, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 65
    if-eqz v6, :cond_0

    move v8, v9

    :cond_0
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 54
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Lcom/sec/enterprise/knox/billing/EnterpriseBillingProfile;
    .end local v6    # "_result":Z
    :cond_1
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_1

    .line 61
    :cond_2
    const/4 v1, 0x0

    .restart local v1    # "_arg1":Lcom/sec/enterprise/knox/billing/EnterpriseBillingProfile;
    goto :goto_2

    .line 70
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Lcom/sec/enterprise/knox/billing/EnterpriseBillingProfile;
    :sswitch_2
    const-string v10, "com.sec.enterprise.knox.billing.IEnterpriseBillingPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 72
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_4

    .line 73
    sget-object v10, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 79
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_3
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_5

    .line 80
    sget-object v10, Lcom/sec/enterprise/knox/billing/EnterpriseBillingProfile;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/enterprise/knox/billing/EnterpriseBillingProfile;

    .line 85
    .restart local v1    # "_arg1":Lcom/sec/enterprise/knox/billing/EnterpriseBillingProfile;
    :goto_4
    invoke-virtual {p0, v0, v1}, Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy$Stub;->updateProfile(Landroid/app/enterprise/ContextInfo;Lcom/sec/enterprise/knox/billing/EnterpriseBillingProfile;)Z

    move-result v6

    .line 86
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 87
    if-eqz v6, :cond_3

    move v8, v9

    :cond_3
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 76
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Lcom/sec/enterprise/knox/billing/EnterpriseBillingProfile;
    .end local v6    # "_result":Z
    :cond_4
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_3

    .line 83
    :cond_5
    const/4 v1, 0x0

    .restart local v1    # "_arg1":Lcom/sec/enterprise/knox/billing/EnterpriseBillingProfile;
    goto :goto_4

    .line 92
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Lcom/sec/enterprise/knox/billing/EnterpriseBillingProfile;
    :sswitch_3
    const-string v10, "com.sec.enterprise.knox.billing.IEnterpriseBillingPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 94
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_7

    .line 95
    sget-object v10, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 101
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_5
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 102
    .local v1, "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v1}, Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy$Stub;->removeProfile(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v6

    .line 103
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 104
    if-eqz v6, :cond_6

    move v8, v9

    :cond_6
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 98
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Z
    :cond_7
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_5

    .line 109
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_4
    const-string v8, "com.sec.enterprise.knox.billing.IEnterpriseBillingPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 111
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_8

    .line 112
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 117
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_6
    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy$Stub;->getAvailableProfiles(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;

    move-result-object v6

    .line 118
    .local v6, "_result":Ljava/util/List;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 119
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    goto/16 :goto_0

    .line 115
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Ljava/util/List;
    :cond_8
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_6

    .line 124
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_5
    const-string v10, "com.sec.enterprise.knox.billing.IEnterpriseBillingPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 126
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_9

    .line 127
    sget-object v10, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 133
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_7
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 134
    .restart local v1    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v1}, Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy$Stub;->getProfileDetails(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Lcom/sec/enterprise/knox/billing/EnterpriseBillingProfile;

    move-result-object v6

    .line 135
    .local v6, "_result":Lcom/sec/enterprise/knox/billing/EnterpriseBillingProfile;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 136
    if-eqz v6, :cond_a

    .line 137
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 138
    invoke-virtual {v6, p3, v9}, Lcom/sec/enterprise/knox/billing/EnterpriseBillingProfile;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 130
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Lcom/sec/enterprise/knox/billing/EnterpriseBillingProfile;
    :cond_9
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_7

    .line 141
    .restart local v1    # "_arg1":Ljava/lang/String;
    .restart local v6    # "_result":Lcom/sec/enterprise/knox/billing/EnterpriseBillingProfile;
    :cond_a
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 147
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Lcom/sec/enterprise/knox/billing/EnterpriseBillingProfile;
    :sswitch_6
    const-string v10, "com.sec.enterprise.knox.billing.IEnterpriseBillingPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 149
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_c

    .line 150
    sget-object v10, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 156
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_8
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 157
    .restart local v1    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v1}, Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy$Stub;->enableProfileForContainer(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v6

    .line 158
    .local v6, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 159
    if-eqz v6, :cond_b

    move v8, v9

    :cond_b
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 153
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Z
    :cond_c
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_8

    .line 164
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_7
    const-string v10, "com.sec.enterprise.knox.billing.IEnterpriseBillingPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 166
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_e

    .line 167
    sget-object v10, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 173
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_9
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 175
    .restart local v1    # "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v4

    .line 176
    .local v4, "_arg2":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p0, v0, v1, v4}, Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy$Stub;->enableProfileForApps(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/util/List;)Z

    move-result v6

    .line 177
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 178
    if-eqz v6, :cond_d

    move v8, v9

    :cond_d
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 170
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v4    # "_arg2":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v6    # "_result":Z
    :cond_e
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_9

    .line 183
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_8
    const-string v10, "com.sec.enterprise.knox.billing.IEnterpriseBillingPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 185
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_10

    .line 186
    sget-object v10, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 191
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_a
    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy$Stub;->disableProfileForContainer(Landroid/app/enterprise/ContextInfo;)Z

    move-result v6

    .line 192
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 193
    if-eqz v6, :cond_f

    move v8, v9

    :cond_f
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 189
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Z
    :cond_10
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_a

    .line 198
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_9
    const-string v10, "com.sec.enterprise.knox.billing.IEnterpriseBillingPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 200
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_12

    .line 201
    sget-object v10, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 207
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_b
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v2

    .line 208
    .local v2, "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p0, v0, v2}, Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy$Stub;->disableProfileForApps(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z

    move-result v6

    .line 209
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 210
    if-eqz v6, :cond_11

    move v8, v9

    :cond_11
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 204
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v6    # "_result":Z
    :cond_12
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_b

    .line 215
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_a
    const-string v10, "com.sec.enterprise.knox.billing.IEnterpriseBillingPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 217
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_14

    .line 218
    sget-object v10, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 224
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_c
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 225
    .restart local v1    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v1}, Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy$Stub;->disableProfile(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v6

    .line 226
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 227
    if-eqz v6, :cond_13

    move v8, v9

    :cond_13
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 221
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Z
    :cond_14
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_c

    .line 232
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_b
    const-string v10, "com.sec.enterprise.knox.billing.IEnterpriseBillingPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 234
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_16

    .line 235
    sget-object v10, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 241
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_d
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 242
    .restart local v1    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v1}, Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy$Stub;->isProfileEnabled(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v6

    .line 243
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 244
    if-eqz v6, :cond_15

    move v8, v9

    :cond_15
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 238
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Z
    :cond_16
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_d

    .line 249
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_c
    const-string v10, "com.sec.enterprise.knox.billing.IEnterpriseBillingPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 251
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_18

    .line 252
    sget-object v10, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 258
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_e
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 259
    .restart local v1    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v1}, Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy$Stub;->turnOnProfile(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v6

    .line 260
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 261
    if-eqz v6, :cond_17

    move v8, v9

    :cond_17
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 255
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Z
    :cond_18
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_e

    .line 266
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_d
    const-string v10, "com.sec.enterprise.knox.billing.IEnterpriseBillingPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 268
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_1a

    .line 269
    sget-object v10, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 275
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_f
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 276
    .restart local v1    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v1}, Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy$Stub;->turnOffProfile(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v6

    .line 277
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 278
    if-eqz v6, :cond_19

    move v8, v9

    :cond_19
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 272
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Z
    :cond_1a
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_f

    .line 283
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_e
    const-string v10, "com.sec.enterprise.knox.billing.IEnterpriseBillingPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 285
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_1c

    .line 286
    sget-object v10, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 292
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_10
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 293
    .restart local v1    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v1}, Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy$Stub;->isProfileTurnedOn(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v6

    .line 294
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 295
    if-eqz v6, :cond_1b

    move v8, v9

    :cond_1b
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 289
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Z
    :cond_1c
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_10

    .line 300
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_f
    const-string v10, "com.sec.enterprise.knox.billing.IEnterpriseBillingPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 302
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_1d

    .line 303
    sget-object v10, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 308
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_11
    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy$Stub;->getProfileForContainer(Landroid/app/enterprise/ContextInfo;)Lcom/sec/enterprise/knox/billing/EnterpriseBillingProfile;

    move-result-object v6

    .line 309
    .local v6, "_result":Lcom/sec/enterprise/knox/billing/EnterpriseBillingProfile;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 310
    if-eqz v6, :cond_1e

    .line 311
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 312
    invoke-virtual {v6, p3, v9}, Lcom/sec/enterprise/knox/billing/EnterpriseBillingProfile;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 306
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Lcom/sec/enterprise/knox/billing/EnterpriseBillingProfile;
    :cond_1d
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_11

    .line 315
    .restart local v6    # "_result":Lcom/sec/enterprise/knox/billing/EnterpriseBillingProfile;
    :cond_1e
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 321
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Lcom/sec/enterprise/knox/billing/EnterpriseBillingProfile;
    :sswitch_10
    const-string v10, "com.sec.enterprise.knox.billing.IEnterpriseBillingPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 323
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_1f

    .line 324
    sget-object v10, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 330
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_12
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 331
    .restart local v1    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v1}, Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy$Stub;->getProfileForApplication(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Lcom/sec/enterprise/knox/billing/EnterpriseBillingProfile;

    move-result-object v6

    .line 332
    .restart local v6    # "_result":Lcom/sec/enterprise/knox/billing/EnterpriseBillingProfile;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 333
    if-eqz v6, :cond_20

    .line 334
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 335
    invoke-virtual {v6, p3, v9}, Lcom/sec/enterprise/knox/billing/EnterpriseBillingProfile;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 327
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Lcom/sec/enterprise/knox/billing/EnterpriseBillingProfile;
    :cond_1f
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_12

    .line 338
    .restart local v1    # "_arg1":Ljava/lang/String;
    .restart local v6    # "_result":Lcom/sec/enterprise/knox/billing/EnterpriseBillingProfile;
    :cond_20
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 344
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Lcom/sec/enterprise/knox/billing/EnterpriseBillingProfile;
    :sswitch_11
    const-string v8, "com.sec.enterprise.knox.billing.IEnterpriseBillingPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 346
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_21

    .line 347
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 353
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_13
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 354
    .restart local v1    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v1}, Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy$Stub;->getContainersUsingProfile(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/util/List;

    move-result-object v6

    .line 355
    .local v6, "_result":Ljava/util/List;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 356
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    goto/16 :goto_0

    .line 350
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Ljava/util/List;
    :cond_21
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_13

    .line 361
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_12
    const-string v8, "com.sec.enterprise.knox.billing.IEnterpriseBillingPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 363
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_22

    .line 364
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 370
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_14
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 371
    .restart local v1    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v1}, Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy$Stub;->getApplicationsUsingProfile(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/util/List;

    move-result-object v6

    .line 372
    .restart local v6    # "_result":Ljava/util/List;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 373
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    goto/16 :goto_0

    .line 367
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Ljava/util/List;
    :cond_22
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_14

    .line 378
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_13
    const-string v10, "com.sec.enterprise.knox.billing.IEnterpriseBillingPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 380
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_23

    .line 381
    sget-object v10, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 387
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_15
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 389
    .restart local v1    # "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_24

    move v3, v9

    .line 390
    .local v3, "_arg2":Z
    :goto_16
    invoke-virtual {p0, v0, v1, v3}, Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy$Stub;->allowWifiFallback(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Z)V

    .line 391
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 384
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":Z
    :cond_23
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_15

    .restart local v1    # "_arg1":Ljava/lang/String;
    :cond_24
    move v3, v8

    .line 389
    goto :goto_16

    .line 396
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Ljava/lang/String;
    :sswitch_14
    const-string v10, "com.sec.enterprise.knox.billing.IEnterpriseBillingPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 398
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_26

    .line 399
    sget-object v10, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 405
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_17
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 406
    .restart local v1    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v1}, Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy$Stub;->isWifiFallbackAllowed(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v6

    .line 407
    .local v6, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 408
    if-eqz v6, :cond_25

    move v8, v9

    :cond_25
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 402
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Z
    :cond_26
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_17

    .line 413
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_15
    const-string v10, "com.sec.enterprise.knox.billing.IEnterpriseBillingPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 415
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_28

    .line 416
    sget-object v10, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 422
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_18
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 424
    .restart local v1    # "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 426
    .local v3, "_arg2":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    .line 427
    .local v5, "_arg3":Ljava/lang/String;
    invoke-virtual {p0, v0, v1, v3, v5}, Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy$Stub;->addVpnToBillingProfile(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v6

    .line 428
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 429
    if-eqz v6, :cond_27

    move v8, v9

    :cond_27
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 419
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":Ljava/lang/String;
    .end local v5    # "_arg3":Ljava/lang/String;
    .end local v6    # "_result":Z
    :cond_28
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_18

    .line 434
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_16
    const-string v10, "com.sec.enterprise.knox.billing.IEnterpriseBillingPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 436
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_2a

    .line 437
    sget-object v10, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 443
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_19
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 445
    .restart local v1    # "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 446
    .restart local v3    # "_arg2":Ljava/lang/String;
    invoke-virtual {p0, v0, v1, v3}, Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy$Stub;->removeVpnFromBillingProfile(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v6

    .line 447
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 448
    if-eqz v6, :cond_29

    move v8, v9

    :cond_29
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 440
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":Ljava/lang/String;
    .end local v6    # "_result":Z
    :cond_2a
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_19

    .line 453
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_17
    const-string v8, "com.sec.enterprise.knox.billing.IEnterpriseBillingPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 455
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_2b

    .line 456
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 462
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_1a
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 463
    .restart local v1    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v1}, Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy$Stub;->getVpnsBoundToProfile(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/util/List;

    move-result-object v7

    .line 464
    .local v7, "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 465
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    goto/16 :goto_0

    .line 459
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v7    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_2b
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_1a

    .line 470
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_18
    const-string v8, "com.sec.enterprise.knox.billing.IEnterpriseBillingPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 472
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_2c

    .line 473
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 478
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_1b
    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy$Stub;->getUidApntypeMappings(Landroid/app/enterprise/ContextInfo;)Ljava/util/Map;

    move-result-object v6

    .line 479
    .local v6, "_result":Ljava/util/Map;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 480
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V

    goto/16 :goto_0

    .line 476
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Ljava/util/Map;
    :cond_2c
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_1b

    .line 485
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_19
    const-string v10, "com.sec.enterprise.knox.billing.IEnterpriseBillingPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 487
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_2e

    .line 488
    sget-object v10, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 494
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_1c
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 496
    .restart local v1    # "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_2f

    move v3, v9

    .line 497
    .local v3, "_arg2":Z
    :goto_1d
    invoke-virtual {p0, v0, v1, v3}, Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy$Stub;->allowRoaming(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Z)Z

    move-result v6

    .line 498
    .local v6, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 499
    if-eqz v6, :cond_2d

    move v8, v9

    :cond_2d
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 491
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":Z
    .end local v6    # "_result":Z
    :cond_2e
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_1c

    .restart local v1    # "_arg1":Ljava/lang/String;
    :cond_2f
    move v3, v8

    .line 496
    goto :goto_1d

    .line 504
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Ljava/lang/String;
    :sswitch_1a
    const-string v10, "com.sec.enterprise.knox.billing.IEnterpriseBillingPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 506
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_31

    .line 507
    sget-object v10, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 513
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_1e
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 514
    .restart local v1    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v1}, Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy$Stub;->isRoamingAllowed(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v6

    .line 515
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 516
    if-eqz v6, :cond_30

    move v8, v9

    :cond_30
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 510
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Z
    :cond_31
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_1e

    .line 521
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_1b
    const-string v10, "com.sec.enterprise.knox.billing.IEnterpriseBillingPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 523
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_33

    .line 524
    sget-object v10, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 530
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_1f
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 532
    .restart local v1    # "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_34

    move v3, v9

    .line 533
    .restart local v3    # "_arg2":Z
    :goto_20
    invoke-virtual {p0, v0, v1, v3}, Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy$Stub;->activateProfile(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Z)Z

    move-result v6

    .line 534
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 535
    if-eqz v6, :cond_32

    move v8, v9

    :cond_32
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 527
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":Z
    .end local v6    # "_result":Z
    :cond_33
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_1f

    .restart local v1    # "_arg1":Ljava/lang/String;
    :cond_34
    move v3, v8

    .line 532
    goto :goto_20

    .line 540
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Ljava/lang/String;
    :sswitch_1c
    const-string v10, "com.sec.enterprise.knox.billing.IEnterpriseBillingPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 542
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_36

    .line 543
    sget-object v10, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 549
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_21
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 550
    .restart local v1    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v1}, Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy$Stub;->isProfileActive(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v6

    .line 551
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 552
    if-eqz v6, :cond_35

    move v8, v9

    :cond_35
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 546
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Z
    :cond_36
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_21

    .line 557
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_1d
    const-string v10, "com.sec.enterprise.knox.billing.IEnterpriseBillingPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 559
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_38

    .line 560
    sget-object v10, Lcom/sec/enterprise/knox/billing/EnterpriseBillingProfile;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingProfile;

    .line 565
    .local v0, "_arg0":Lcom/sec/enterprise/knox/billing/EnterpriseBillingProfile;
    :goto_22
    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy$Stub;->addProfileForCurrentContainer(Lcom/sec/enterprise/knox/billing/EnterpriseBillingProfile;)Z

    move-result v6

    .line 566
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 567
    if-eqz v6, :cond_37

    move v8, v9

    :cond_37
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 563
    .end local v0    # "_arg0":Lcom/sec/enterprise/knox/billing/EnterpriseBillingProfile;
    .end local v6    # "_result":Z
    :cond_38
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Lcom/sec/enterprise/knox/billing/EnterpriseBillingProfile;
    goto :goto_22

    .line 572
    .end local v0    # "_arg0":Lcom/sec/enterprise/knox/billing/EnterpriseBillingProfile;
    :sswitch_1e
    const-string v10, "com.sec.enterprise.knox.billing.IEnterpriseBillingPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 574
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 575
    .local v0, "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy$Stub;->removeProfileForCurrentContainer(Ljava/lang/String;)Z

    move-result v6

    .line 576
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 577
    if-eqz v6, :cond_39

    move v8, v9

    :cond_39
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 582
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v6    # "_result":Z
    :sswitch_1f
    const-string v10, "com.sec.enterprise.knox.billing.IEnterpriseBillingPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 584
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 585
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy$Stub;->enableProfileForCurrentContainer(Ljava/lang/String;)Z

    move-result v6

    .line 586
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 587
    if-eqz v6, :cond_3a

    move v8, v9

    :cond_3a
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 592
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v6    # "_result":Z
    :sswitch_20
    const-string v10, "com.sec.enterprise.knox.billing.IEnterpriseBillingPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 593
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy$Stub;->disableProfileForCurrentContainer()Z

    move-result v6

    .line 594
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 595
    if-eqz v6, :cond_3b

    move v8, v9

    :cond_3b
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 600
    .end local v6    # "_result":Z
    :sswitch_21
    const-string v10, "com.sec.enterprise.knox.billing.IEnterpriseBillingPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 602
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 603
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy$Stub;->isProfileActiveByCaller(Ljava/lang/String;)Z

    move-result v6

    .line 604
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 605
    if-eqz v6, :cond_3c

    move v8, v9

    :cond_3c
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 610
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v6    # "_result":Z
    :sswitch_22
    const-string v8, "com.sec.enterprise.knox.billing.IEnterpriseBillingPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 611
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy$Stub;->getAvailableProfilesForCaller()Ljava/util/List;

    move-result-object v6

    .line 612
    .local v6, "_result":Ljava/util/List;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 613
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    goto/16 :goto_0

    .line 618
    .end local v6    # "_result":Ljava/util/List;
    :sswitch_23
    const-string v10, "com.sec.enterprise.knox.billing.IEnterpriseBillingPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 620
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 621
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy$Stub;->removeVpnFromBillingProfileForCurrentContainer(Ljava/lang/String;)Z

    move-result v6

    .line 622
    .local v6, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 623
    if-eqz v6, :cond_3d

    move v8, v9

    :cond_3d
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 628
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v6    # "_result":Z
    :sswitch_24
    const-string v10, "com.sec.enterprise.knox.billing.IEnterpriseBillingPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 630
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 632
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 634
    .restart local v1    # "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 635
    .local v3, "_arg2":Ljava/lang/String;
    invoke-virtual {p0, v0, v1, v3}, Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy$Stub;->addVpnToBillingProfileForCurrentContainer(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v6

    .line 636
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 637
    if-eqz v6, :cond_3e

    move v8, v9

    :cond_3e
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 39
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0xe -> :sswitch_e
        0xf -> :sswitch_f
        0x10 -> :sswitch_10
        0x11 -> :sswitch_11
        0x12 -> :sswitch_12
        0x13 -> :sswitch_13
        0x14 -> :sswitch_14
        0x15 -> :sswitch_15
        0x16 -> :sswitch_16
        0x17 -> :sswitch_17
        0x18 -> :sswitch_18
        0x19 -> :sswitch_19
        0x1a -> :sswitch_1a
        0x1b -> :sswitch_1b
        0x1c -> :sswitch_1c
        0x1d -> :sswitch_1d
        0x1e -> :sswitch_1e
        0x1f -> :sswitch_1f
        0x20 -> :sswitch_20
        0x21 -> :sswitch_21
        0x22 -> :sswitch_22
        0x23 -> :sswitch_23
        0x24 -> :sswitch_24
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
