.class final Lcom/sec/enterprise/knox/billing/EnterpriseApn$1;
.super Ljava/lang/Object;
.source "EnterpriseApn.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/enterprise/knox/billing/EnterpriseApn;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/sec/enterprise/knox/billing/EnterpriseApn;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/sec/enterprise/knox/billing/EnterpriseApn;
    .locals 4
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 91
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 92
    .local v0, "apn":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 93
    .local v2, "mcc":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 94
    .local v3, "mnc":Ljava/lang/String;
    new-instance v1, Lcom/sec/enterprise/knox/billing/EnterpriseApn;

    invoke-direct {v1, v0, v2, v3}, Lcom/sec/enterprise/knox/billing/EnterpriseApn;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    .local v1, "eapn":Lcom/sec/enterprise/knox/billing/EnterpriseApn;
    return-object v1
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Landroid/os/Parcel;

    .prologue
    .line 77
    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/billing/EnterpriseApn$1;->createFromParcel(Landroid/os/Parcel;)Lcom/sec/enterprise/knox/billing/EnterpriseApn;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/sec/enterprise/knox/billing/EnterpriseApn;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 83
    new-array v0, p1, [Lcom/sec/enterprise/knox/billing/EnterpriseApn;

    .line 84
    .local v0, "array":[Lcom/sec/enterprise/knox/billing/EnterpriseApn;
    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 77
    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/billing/EnterpriseApn$1;->newArray(I)[Lcom/sec/enterprise/knox/billing/EnterpriseApn;

    move-result-object v0

    return-object v0
.end method
