.class public Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicyForApp;
.super Ljava/lang/Object;
.source "EnterpriseBillingPolicyForApp.java"


# static fields
.field private static policy:Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicyForApp;


# instance fields
.field private billingPolicyService:Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicyForApp;->policy:Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicyForApp;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicyForApp;->billingPolicyService:Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;

    .line 93
    return-void
.end method

.method private declared-synchronized getEnterpriseBillingService()Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;
    .locals 3

    .prologue
    .line 290
    monitor-enter p0

    :try_start_0
    const-string v0, "EnterpriseBillingPolicyForApp : "

    const-string v1, "getEnterpriseBillingService -start"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 291
    iget-object v0, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicyForApp;->billingPolicyService:Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;

    if-nez v0, :cond_0

    .line 292
    const-string v0, "enterprise_billing_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicyForApp;->billingPolicyService:Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;

    .line 295
    const-string v0, "EnterpriseBillingPolicyForApp"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Created Client : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicyForApp;->billingPolicyService:Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 297
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicyForApp;->billingPolicyService:Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 290
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized getInstance()Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicyForApp;
    .locals 2

    .prologue
    .line 82
    const-class v1, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicyForApp;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicyForApp;->policy:Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicyForApp;

    if-nez v0, :cond_0

    .line 83
    new-instance v0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicyForApp;

    invoke-direct {v0}, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicyForApp;-><init>()V

    sput-object v0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicyForApp;->policy:Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicyForApp;

    .line 85
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicyForApp;->policy:Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicyForApp;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 82
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public addVpnProfile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 5
    .param p1, "billingProfileName"    # Ljava/lang/String;
    .param p2, "vpnProfileName"    # Ljava/lang/String;
    .param p3, "pacakgeName"    # Ljava/lang/String;

    .prologue
    .line 351
    const-string v2, "EnterpriseBillingPolicyForApp"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "addVpnToBillingProfile- start"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 352
    const/4 v1, 0x0

    .line 355
    .local v1, "returnValue":Z
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicyForApp;->getEnterpriseBillingService()Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 356
    iget-object v2, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicyForApp;->billingPolicyService:Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;

    invoke-interface {v2, p1, p2, p3}, Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;->addVpnToBillingProfileForCurrentContainer(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 361
    :cond_0
    :goto_0
    const-string v2, "EnterpriseBillingPolicyForApp"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "addVpnToBillingProfile- end"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 362
    return v1

    .line 358
    :catch_0
    move-exception v0

    .line 359
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public bindToProfile(Ljava/lang/String;)Z
    .locals 5
    .param p1, "profileName"    # Ljava/lang/String;

    .prologue
    .line 227
    const/4 v1, 0x0

    .line 228
    .local v1, "returnValue":Z
    const-string v2, "EnterpriseBillingPolicyForApp"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "bindToProfile - start"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 230
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicyForApp;->getEnterpriseBillingService()Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 231
    iget-object v2, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicyForApp;->billingPolicyService:Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;

    invoke-interface {v2, p1}, Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;->enableProfileForCurrentContainer(Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 236
    :cond_0
    :goto_0
    const-string v2, "EnterpriseBillingPolicyForApp"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "bindToProfile - end"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 237
    return v1

    .line 233
    :catch_0
    move-exception v0

    .line 234
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public createProfile(Lcom/sec/enterprise/knox/billing/EnterpriseBillingProfile;)Z
    .locals 5
    .param p1, "profile"    # Lcom/sec/enterprise/knox/billing/EnterpriseBillingProfile;

    .prologue
    .line 129
    const-string v2, "EnterpriseBillingPolicyForApp"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "createProfile- start"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    const/4 v1, 0x0

    .line 133
    .local v1, "returnValue":Z
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicyForApp;->getEnterpriseBillingService()Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 134
    iget-object v2, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicyForApp;->billingPolicyService:Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;

    invoke-interface {v2, p1}, Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;->addProfileForCurrentContainer(Lcom/sec/enterprise/knox/billing/EnterpriseBillingProfile;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 139
    :cond_0
    :goto_0
    const-string v2, "EnterpriseBillingPolicyForApp"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "createProfile- end"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    return v1

    .line 136
    :catch_0
    move-exception v0

    .line 137
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getAvailableProfilesForCaller()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 338
    const-string v2, "EnterpriseBillingPolicyForApp"

    const-string v3, "getAvailableProfilesForCaller - start"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 339
    const/4 v0, 0x0

    .line 341
    .local v0, "availableProfiles":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicyForApp;->getEnterpriseBillingService()Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 342
    iget-object v2, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicyForApp;->billingPolicyService:Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;

    invoke-interface {v2}, Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;->getAvailableProfilesForCaller()Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 347
    :cond_0
    :goto_0
    const-string v2, "EnterpriseBillingPolicyForApp"

    const-string v3, "getAvailableProfilesForCaller - end"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 348
    return-object v0

    .line 344
    :catch_0
    move-exception v1

    .line 345
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public isProfileActiveByCaller(Ljava/lang/String;)Z
    .locals 5
    .param p1, "profileName"    # Ljava/lang/String;

    .prologue
    .line 313
    const-string v2, "EnterpriseBillingPolicyForApp"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isProfileActiveByCaller - start"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 314
    const/4 v1, 0x0

    .line 316
    .local v1, "returnValue":Z
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicyForApp;->getEnterpriseBillingService()Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 317
    iget-object v2, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicyForApp;->billingPolicyService:Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;

    invoke-interface {v2, p1}, Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;->isProfileActiveByCaller(Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 322
    :cond_0
    :goto_0
    const-string v2, "EnterpriseBillingPolicyForApp"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isProfileActiveByCaller - end"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 323
    return v1

    .line 319
    :catch_0
    move-exception v0

    .line 320
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public removeProfile(Ljava/lang/String;)Z
    .locals 5
    .param p1, "profileName"    # Ljava/lang/String;

    .prologue
    .line 175
    const-string v2, "EnterpriseBillingPolicyForApp"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "removeProfile - start"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    const/4 v1, 0x0

    .line 179
    .local v1, "returnValue":Z
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicyForApp;->getEnterpriseBillingService()Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 180
    iget-object v2, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicyForApp;->billingPolicyService:Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;

    invoke-interface {v2, p1}, Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;->removeProfileForCurrentContainer(Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 185
    :cond_0
    :goto_0
    const-string v2, "EnterpriseBillingPolicyForApp"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "removeProfile - end"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    return v1

    .line 182
    :catch_0
    move-exception v0

    .line 183
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public removeVpnProfile(Ljava/lang/String;)Z
    .locals 5
    .param p1, "vpnProfileName"    # Ljava/lang/String;

    .prologue
    .line 377
    const-string v2, "EnterpriseBillingPolicyForApp"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "removeVpnProfile - start"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 378
    const/4 v1, 0x0

    .line 380
    .local v1, "returnValue":Z
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicyForApp;->getEnterpriseBillingService()Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 381
    iget-object v2, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicyForApp;->billingPolicyService:Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;

    invoke-interface {v2, p1}, Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;->removeVpnFromBillingProfileForCurrentContainer(Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 386
    :cond_0
    :goto_0
    const-string v2, "EnterpriseBillingPolicyForApp"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "removeVpnProfile - end"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 387
    return v1

    .line 383
    :catch_0
    move-exception v0

    .line 384
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public unbindFromProfile()Z
    .locals 5

    .prologue
    .line 271
    const/4 v1, 0x0

    .line 272
    .local v1, "returnValue":Z
    const-string v2, "EnterpriseBillingPolicyForApp"

    const-string v3, "unbindFromProfile - start for container- "

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicyForApp;->getEnterpriseBillingService()Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 275
    iget-object v2, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicyForApp;->billingPolicyService:Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;

    invoke-interface {v2}, Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;->disableProfileForCurrentContainer()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 280
    :cond_0
    :goto_0
    const-string v2, "EnterpriseBillingPolicyForApp"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "bindToProfile - end"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 281
    return v1

    .line 277
    :catch_0
    move-exception v0

    .line 278
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method
