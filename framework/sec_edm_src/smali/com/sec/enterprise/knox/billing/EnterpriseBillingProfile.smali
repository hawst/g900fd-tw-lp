.class public Lcom/sec/enterprise/knox/billing/EnterpriseBillingProfile;
.super Ljava/lang/Object;
.source "EnterpriseBillingProfile.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/enterprise/knox/billing/EnterpriseBillingProfile;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final apns:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/enterprise/knox/billing/EnterpriseApn;",
            ">;"
        }
    .end annotation
.end field

.field private final profileName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 173
    new-instance v0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingProfile$1;

    invoke-direct {v0}, Lcom/sec/enterprise/knox/billing/EnterpriseBillingProfile$1;-><init>()V

    sput-object v0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingProfile;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "profileName"    # Ljava/lang/String;

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 80
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 82
    :cond_1
    iput-object p1, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingProfile;->profileName:Ljava/lang/String;

    .line 83
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingProfile;->apns:Ljava/util/List;

    .line 84
    return-void
.end method


# virtual methods
.method public addApnToProfile(Lcom/sec/enterprise/knox/billing/EnterpriseApn;)V
    .locals 1
    .param p1, "apn"    # Lcom/sec/enterprise/knox/billing/EnterpriseApn;

    .prologue
    .line 103
    if-eqz p1, :cond_0

    .line 104
    iget-object v0, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingProfile;->apns:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 106
    :cond_0
    return-void
.end method

.method public addApnsToProfile(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/enterprise/knox/billing/EnterpriseApn;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 115
    .local p1, "apns":Ljava/util/List;, "Ljava/util/List<Lcom/sec/enterprise/knox/billing/EnterpriseApn;>;"
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingProfile;->apns:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 118
    :cond_0
    return-void
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 158
    const/4 v0, 0x0

    return v0
.end method

.method public getApnsFromProfile()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/enterprise/knox/billing/EnterpriseApn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingProfile;->apns:Ljava/util/List;

    return-object v0
.end method

.method public getProfileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingProfile;->profileName:Ljava/lang/String;

    return-object v0
.end method

.method public isProfileValid()Z
    .locals 4

    .prologue
    .line 137
    const/4 v2, 0x1

    .line 138
    .local v2, "returnVal":Z
    iget-object v3, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingProfile;->apns:Ljava/util/List;

    if-eqz v3, :cond_4

    .line 139
    iget-object v3, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingProfile;->profileName:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 140
    const/4 v2, 0x0

    .line 142
    :cond_0
    iget-object v3, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingProfile;->apns:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/billing/EnterpriseApn;

    .line 143
    .local v0, "enterpriseApn":Lcom/sec/enterprise/knox/billing/EnterpriseApn;
    iget-object v3, v0, Lcom/sec/enterprise/knox/billing/EnterpriseApn;->apn:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, v0, Lcom/sec/enterprise/knox/billing/EnterpriseApn;->mcc:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, v0, Lcom/sec/enterprise/knox/billing/EnterpriseApn;->mnc:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 144
    :cond_2
    const/4 v2, 0x0

    .line 151
    .end local v0    # "enterpriseApn":Lcom/sec/enterprise/knox/billing/EnterpriseApn;
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_3
    :goto_0
    return v2

    .line 149
    :cond_4
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingProfile;->profileName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 167
    iget-object v0, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingProfile;->apns:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 168
    return-void
.end method
