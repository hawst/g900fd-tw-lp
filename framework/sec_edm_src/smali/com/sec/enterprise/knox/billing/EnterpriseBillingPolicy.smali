.class public Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;
.super Ljava/lang/Object;
.source "EnterpriseBillingPolicy.java"


# static fields
.field public static final ALL_APPS_IN_SCOPE:Ljava/lang/String; = "*"

.field public static final CONTAINER_USERID_START:I = 0x64


# instance fields
.field private billingPolicyService:Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;

.field private mContextInfo:Landroid/app/enterprise/ContextInfo;

.field private policy:Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;


# direct methods
.method public constructor <init>(Landroid/app/enterprise/ContextInfo;)V
    .locals 1
    .param p1, "contextInfo"    # Landroid/app/enterprise/ContextInfo;

    .prologue
    const/4 v0, 0x0

    .line 1113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object v0, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->billingPolicyService:Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;

    .line 61
    iput-object v0, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    .line 64
    iput-object v0, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->policy:Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;

    .line 1114
    iput-object p1, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    .line 1115
    return-void
.end method

.method private declared-synchronized getEnterpriseBillingService()Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;
    .locals 3

    .prologue
    .line 1123
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->billingPolicyService:Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;

    if-nez v0, :cond_0

    .line 1124
    const-string v0, "enterprise_billing_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->billingPolicyService:Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;

    .line 1127
    const-string v0, "EnterpriseBillingPolicy"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Created Client : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->billingPolicyService:Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1129
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->billingPolicyService:Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 1123
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public activateProfile(Ljava/lang/String;Z)Z
    .locals 4
    .param p1, "ebProfileName"    # Ljava/lang/String;
    .param p2, "isActivate"    # Z

    .prologue
    .line 1026
    iget-object v2, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "EnterpriseBillingPolicy.activateProfile"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1027
    const/4 v1, 0x0

    .line 1029
    .local v1, "returnValue":Z
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->getEnterpriseBillingService()Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1030
    iget-object v2, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->billingPolicyService:Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;

    iget-object v3, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3, p1, p2}, Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;->activateProfile(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1035
    :cond_0
    :goto_0
    return v1

    .line 1032
    :catch_0
    move-exception v0

    .line 1033
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public allowRoaming(Ljava/lang/String;Z)Z
    .locals 4
    .param p1, "ebProfileName"    # Ljava/lang/String;
    .param p2, "allow"    # Z

    .prologue
    .line 880
    iget-object v2, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "EnterpriseBillingPolicy.allowRoaming"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 881
    const/4 v1, 0x0

    .line 883
    .local v1, "returnValue":Z
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->getEnterpriseBillingService()Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 884
    iget-object v2, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->billingPolicyService:Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;

    iget-object v3, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3, p1, p2}, Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;->allowRoaming(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 889
    :cond_0
    :goto_0
    return v1

    .line 886
    :catch_0
    move-exception v0

    .line 887
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public bindAppsToProfile(Ljava/lang/String;Ljava/util/List;)Z
    .locals 5
    .param p1, "ebProfileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p2, "packageList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/16 v4, 0x64

    .line 495
    iget-object v2, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "EnterpriseBillingPolicy.bindAppsToProfile"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 496
    const/4 v1, 0x0

    .line 499
    .local v1, "returnValue":Z
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->getEnterpriseBillingService()Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 501
    if-eqz p2, :cond_1

    const-string v2, "*"

    invoke-interface {p2, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget v2, v2, Landroid/app/enterprise/ContextInfo;->mContainerId:I

    if-lt v2, v4, :cond_1

    .line 504
    iget-object v2, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->billingPolicyService:Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;

    iget-object v3, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3, p1}, Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;->enableProfileForContainer(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v1

    .line 515
    :cond_0
    :goto_0
    return v1

    .line 506
    :cond_1
    iget-object v2, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget v2, v2, Landroid/app/enterprise/ContextInfo;->mContainerId:I

    if-ge v2, v4, :cond_0

    .line 508
    iget-object v2, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->billingPolicyService:Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;

    iget-object v3, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3, p1, p2}, Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;->enableProfileForApps(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 511
    :catch_0
    move-exception v0

    .line 512
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public bindVpnToProfile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4
    .param p1, "ebProfileName"    # Ljava/lang/String;
    .param p2, "vpnProfileName"    # Ljava/lang/String;
    .param p3, "vpnPackageName"    # Ljava/lang/String;

    .prologue
    .line 738
    iget-object v2, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "EnterpriseBillingPolicy.bindVpnToProfile"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 739
    const/4 v1, 0x0

    .line 741
    .local v1, "returnValue":Z
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->getEnterpriseBillingService()Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 743
    iget-object v2, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->billingPolicyService:Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;

    iget-object v3, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3, p1, p2, p3}, Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;->addVpnToBillingProfile(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 748
    :cond_0
    :goto_0
    return v1

    .line 745
    :catch_0
    move-exception v0

    .line 746
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public createProfile(Lcom/sec/enterprise/knox/billing/EnterpriseBillingProfile;)Z
    .locals 4
    .param p1, "ebProfile"    # Lcom/sec/enterprise/knox/billing/EnterpriseBillingProfile;

    .prologue
    .line 196
    iget-object v2, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "EnterpriseBillingPolicy.createProfile"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 197
    const/4 v1, 0x0

    .line 200
    .local v1, "returnValue":Z
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->getEnterpriseBillingService()Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 201
    iget-object v2, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->billingPolicyService:Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;

    iget-object v3, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3, p1}, Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;->addProfile(Landroid/app/enterprise/ContextInfo;Lcom/sec/enterprise/knox/billing/EnterpriseBillingProfile;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 207
    :cond_0
    :goto_0
    return v1

    .line 203
    :catch_0
    move-exception v0

    .line 204
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getAppsBoundToProfile(Ljava/lang/String;)Ljava/util/List;
    .locals 4
    .param p1, "ebProfileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 663
    iget-object v2, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "EnterpriseBillingPolicy.getAppsBoundToProfile"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 664
    const/4 v0, 0x0

    .line 667
    .local v0, "apps":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->getEnterpriseBillingService()Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 668
    iget-object v2, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->billingPolicyService:Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;

    iget-object v3, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3, p1}, Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;->getApplicationsUsingProfile(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 674
    :cond_0
    :goto_0
    return-object v0

    .line 670
    :catch_0
    move-exception v1

    .line 671
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getAvailableProfiles()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 117
    iget-object v2, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "EnterpriseBillingPolicy.getAvailableProfiles"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 118
    const/4 v0, 0x0

    .line 120
    .local v0, "availableProfiles":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->getEnterpriseBillingService()Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 121
    iget-object v2, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->billingPolicyService:Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;

    iget-object v3, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3}, Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;->getAvailableProfiles(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 126
    :cond_0
    :goto_0
    return-object v0

    .line 123
    :catch_0
    move-exception v1

    .line 124
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getBoundedProfile(Ljava/lang/String;)Lcom/sec/enterprise/knox/billing/EnterpriseBillingProfile;
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 1191
    iget-object v2, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "EnterpriseBillingPolicy.getBoundedProfile"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1192
    const/4 v1, 0x0

    .line 1194
    .local v1, "profile":Lcom/sec/enterprise/knox/billing/EnterpriseBillingProfile;
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->getEnterpriseBillingService()Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1195
    iget-object v2, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget v2, v2, Landroid/app/enterprise/ContextInfo;->mContainerId:I

    const/16 v3, 0x64

    if-lt v2, v3, :cond_1

    .line 1197
    iget-object v2, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->billingPolicyService:Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;

    iget-object v3, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3}, Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;->getProfileForContainer(Landroid/app/enterprise/ContextInfo;)Lcom/sec/enterprise/knox/billing/EnterpriseBillingProfile;

    move-result-object v1

    .line 1205
    :cond_0
    :goto_0
    return-object v1

    .line 1199
    :cond_1
    iget-object v2, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->billingPolicyService:Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;

    iget-object v3, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3, p1}, Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;->getProfileForApplication(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Lcom/sec/enterprise/knox/billing/EnterpriseBillingProfile;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 1202
    :catch_0
    move-exception v0

    .line 1203
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getProfileDetails(Ljava/lang/String;)Lcom/sec/enterprise/knox/billing/EnterpriseBillingProfile;
    .locals 4
    .param p1, "ebProfileName"    # Ljava/lang/String;

    .prologue
    .line 417
    iget-object v2, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "EnterpriseBillingPolicy.getProfileDetails"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 418
    const/4 v1, 0x0

    .line 421
    .local v1, "returnValue":Lcom/sec/enterprise/knox/billing/EnterpriseBillingProfile;
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->getEnterpriseBillingService()Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 422
    iget-object v2, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->billingPolicyService:Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;

    iget-object v3, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3, p1}, Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;->getProfileDetails(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Lcom/sec/enterprise/knox/billing/EnterpriseBillingProfile;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 428
    :cond_0
    :goto_0
    return-object v1

    .line 424
    :catch_0
    move-exception v0

    .line 425
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public isProfileActive(Ljava/lang/String;)Z
    .locals 4
    .param p1, "ebProfileName"    # Ljava/lang/String;

    .prologue
    .line 1095
    iget-object v2, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "EnterpriseBillingPolicy.isProfileActive"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1096
    const/4 v1, 0x0

    .line 1098
    .local v1, "returnValue":Z
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->getEnterpriseBillingService()Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1099
    iget-object v2, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->billingPolicyService:Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;

    iget-object v3, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3, p1}, Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;->isProfileActive(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1104
    :cond_0
    :goto_0
    return v1

    .line 1101
    :catch_0
    move-exception v0

    .line 1102
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public isRoamingAllowed(Ljava/lang/String;)Z
    .locals 4
    .param p1, "ebProfileName"    # Ljava/lang/String;

    .prologue
    .line 949
    iget-object v2, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "EnterpriseBillingPolicy.isRoamingAllowed"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 950
    const/4 v1, 0x0

    .line 953
    .local v1, "returnValue":Z
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->getEnterpriseBillingService()Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 954
    iget-object v2, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->billingPolicyService:Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;

    iget-object v3, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3, p1}, Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;->isRoamingAllowed(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 960
    :cond_0
    :goto_0
    return v1

    .line 956
    :catch_0
    move-exception v0

    .line 957
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public removeProfile(Ljava/lang/String;)Z
    .locals 4
    .param p1, "ebProfileName"    # Ljava/lang/String;

    .prologue
    .line 347
    iget-object v2, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "EnterpriseBillingPolicy.removeProfile"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 348
    const/4 v1, 0x0

    .line 351
    .local v1, "returnValue":Z
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->getEnterpriseBillingService()Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 352
    iget-object v2, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->billingPolicyService:Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;

    iget-object v3, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3, p1}, Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;->removeProfile(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 358
    :cond_0
    :goto_0
    return v1

    .line 354
    :catch_0
    move-exception v0

    .line 355
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public unbindAppsFromProfile(Ljava/lang/String;Ljava/util/List;)Z
    .locals 5
    .param p1, "ebProfileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p2, "packageList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/16 v4, 0x64

    .line 577
    iget-object v2, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "EnterpriseBillingPolicy.unbindAppsFromProfile"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 578
    const/4 v1, 0x0

    .line 581
    .local v1, "returnValue":Z
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->getEnterpriseBillingService()Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 583
    if-eqz p2, :cond_1

    const-string v2, "*"

    invoke-interface {p2, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget v2, v2, Landroid/app/enterprise/ContextInfo;->mContainerId:I

    if-lt v2, v4, :cond_1

    .line 586
    iget-object v2, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->billingPolicyService:Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;

    iget-object v3, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3}, Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;->disableProfileForContainer(Landroid/app/enterprise/ContextInfo;)Z

    move-result v1

    .line 597
    :cond_0
    :goto_0
    return v1

    .line 588
    :cond_1
    iget-object v2, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget v2, v2, Landroid/app/enterprise/ContextInfo;->mContainerId:I

    if-ge v2, v4, :cond_0

    .line 590
    iget-object v2, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->billingPolicyService:Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;

    iget-object v3, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3, p2}, Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;->disableProfileForApps(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 593
    :catch_0
    move-exception v0

    .line 594
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public unbindVpnFromProfile(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4
    .param p1, "ebProfileName"    # Ljava/lang/String;
    .param p2, "vpnProfileName"    # Ljava/lang/String;

    .prologue
    .line 814
    iget-object v2, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "EnterpriseBillingPolicy.unbindVpnFromProfile"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 815
    const/4 v1, 0x0

    .line 818
    .local v1, "returnValue":Z
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->getEnterpriseBillingService()Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 819
    iget-object v2, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->billingPolicyService:Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;

    iget-object v3, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3, p1, p2}, Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;->removeVpnFromBillingProfile(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 824
    :cond_0
    :goto_0
    return v1

    .line 821
    :catch_0
    move-exception v0

    .line 822
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public updateProfile(Lcom/sec/enterprise/knox/billing/EnterpriseBillingProfile;)Z
    .locals 4
    .param p1, "ebProfile"    # Lcom/sec/enterprise/knox/billing/EnterpriseBillingProfile;

    .prologue
    .line 277
    iget-object v2, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "EnterpriseBillingPolicy.updateProfile"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 278
    const/4 v1, 0x0

    .line 281
    .local v1, "returnValue":Z
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->getEnterpriseBillingService()Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 282
    iget-object v2, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->billingPolicyService:Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;

    iget-object v3, p0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3, p1}, Lcom/sec/enterprise/knox/billing/IEnterpriseBillingPolicy;->updateProfile(Landroid/app/enterprise/ContextInfo;Lcom/sec/enterprise/knox/billing/EnterpriseBillingProfile;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 288
    :cond_0
    :goto_0
    return v1

    .line 284
    :catch_0
    move-exception v0

    .line 285
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method
