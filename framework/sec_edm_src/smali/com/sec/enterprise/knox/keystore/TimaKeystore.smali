.class public Lcom/sec/enterprise/knox/keystore/TimaKeystore;
.super Ljava/lang/Object;
.source "TimaKeystore.java"


# static fields
.field private static TAG:Ljava/lang/String;

.field private static mKeystoreService:Lcom/sec/enterprise/knox/keystore/ITimaKeystore;

.field private static final mSync:Ljava/lang/Object;

.field private static mTimaKeystore:Lcom/sec/enterprise/knox/keystore/TimaKeystore;


# instance fields
.field private mContextInfo:Landroid/app/enterprise/ContextInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 66
    const-string v0, "TimaKeystore"

    sput-object v0, Lcom/sec/enterprise/knox/keystore/TimaKeystore;->TAG:Ljava/lang/String;

    .line 69
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/sec/enterprise/knox/keystore/TimaKeystore;->mSync:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V
    .locals 0
    .param p1, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    iput-object p1, p0, Lcom/sec/enterprise/knox/keystore/TimaKeystore;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    .line 85
    return-void
.end method

.method public static createInstance()Lcom/sec/enterprise/knox/keystore/TimaKeystore;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 211
    new-instance v0, Lcom/sec/enterprise/knox/keystore/TimaKeystore;

    invoke-direct {v0, v1, v1}, Lcom/sec/enterprise/knox/keystore/TimaKeystore;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    return-object v0
.end method

.method public static getInstance()Lcom/sec/enterprise/knox/keystore/TimaKeystore;
    .locals 4

    .prologue
    .line 199
    sget-object v1, Lcom/sec/enterprise/knox/keystore/TimaKeystore;->mSync:Ljava/lang/Object;

    monitor-enter v1

    .line 200
    :try_start_0
    sget-object v0, Lcom/sec/enterprise/knox/keystore/TimaKeystore;->mTimaKeystore:Lcom/sec/enterprise/knox/keystore/TimaKeystore;

    if-nez v0, :cond_0

    .line 201
    new-instance v0, Lcom/sec/enterprise/knox/keystore/TimaKeystore;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v0, v2, v3}, Lcom/sec/enterprise/knox/keystore/TimaKeystore;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    sput-object v0, Lcom/sec/enterprise/knox/keystore/TimaKeystore;->mTimaKeystore:Lcom/sec/enterprise/knox/keystore/TimaKeystore;

    .line 203
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/keystore/TimaKeystore;->mTimaKeystore:Lcom/sec/enterprise/knox/keystore/TimaKeystore;

    monitor-exit v1

    return-object v0

    .line 204
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method static declared-synchronized getKeystoreService()Lcom/sec/enterprise/knox/keystore/ITimaKeystore;
    .locals 2

    .prologue
    .line 73
    const-class v1, Lcom/sec/enterprise/knox/keystore/TimaKeystore;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/enterprise/knox/keystore/TimaKeystore;->mKeystoreService:Lcom/sec/enterprise/knox/keystore/ITimaKeystore;

    if-nez v0, :cond_0

    .line 74
    const-string v0, "knox_timakeystore_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/keystore/ITimaKeystore$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/enterprise/knox/keystore/ITimaKeystore;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/keystore/TimaKeystore;->mKeystoreService:Lcom/sec/enterprise/knox/keystore/ITimaKeystore;

    .line 77
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/keystore/TimaKeystore;->mKeystoreService:Lcom/sec/enterprise/knox/keystore/ITimaKeystore;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 73
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public enableTimaKeystore(Z)Z
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 115
    invoke-static {}, Lcom/sec/enterprise/knox/keystore/TimaKeystore;->getKeystoreService()Lcom/sec/enterprise/knox/keystore/ITimaKeystore;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 116
    iget-object v1, p0, Lcom/sec/enterprise/knox/keystore/TimaKeystore;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "TimaKeystore.enableTimaKeystore"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 118
    :try_start_0
    invoke-static {}, Lcom/sec/enterprise/knox/keystore/TimaKeystore;->getKeystoreService()Lcom/sec/enterprise/knox/keystore/ITimaKeystore;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/keystore/TimaKeystore;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Lcom/sec/enterprise/knox/keystore/ITimaKeystore;->enableTimaKeystore(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 123
    :goto_0
    return v1

    .line 119
    :catch_0
    move-exception v0

    .line 120
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/keystore/TimaKeystore;->TAG:Ljava/lang/String;

    const-string v2, "Failed at TimaKeystore API enableTimaKeystore-Exception"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 123
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isTimaKeyStoreDefaultForContainer()Z
    .locals 3

    .prologue
    .line 184
    invoke-static {}, Lcom/sec/enterprise/knox/keystore/TimaKeystore;->getKeystoreService()Lcom/sec/enterprise/knox/keystore/ITimaKeystore;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 185
    iget-object v1, p0, Lcom/sec/enterprise/knox/keystore/TimaKeystore;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "TimaKeystore.isTimaKeyStoreDefaultForContainer"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 187
    :try_start_0
    invoke-static {}, Lcom/sec/enterprise/knox/keystore/TimaKeystore;->getKeystoreService()Lcom/sec/enterprise/knox/keystore/ITimaKeystore;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/keystore/TimaKeystore;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Lcom/sec/enterprise/knox/keystore/ITimaKeystore;->isTimaKeyStoreDefaultForContainer(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 192
    :goto_0
    return v1

    .line 188
    :catch_0
    move-exception v0

    .line 189
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/keystore/TimaKeystore;->TAG:Ljava/lang/String;

    const-string v2, "Failed at TimaKeystore API isTimaKeyStoreDefaultForContainer-Exception"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 192
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isTimaKeystoreEnabled()Z
    .locals 3

    .prologue
    .line 153
    invoke-static {}, Lcom/sec/enterprise/knox/keystore/TimaKeystore;->getKeystoreService()Lcom/sec/enterprise/knox/keystore/ITimaKeystore;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 154
    iget-object v1, p0, Lcom/sec/enterprise/knox/keystore/TimaKeystore;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "TimaKeystore.isTimKeystoreEnabled"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 156
    :try_start_0
    invoke-static {}, Lcom/sec/enterprise/knox/keystore/TimaKeystore;->getKeystoreService()Lcom/sec/enterprise/knox/keystore/ITimaKeystore;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/keystore/TimaKeystore;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Lcom/sec/enterprise/knox/keystore/ITimaKeystore;->isTimaKeystoreEnabled(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 161
    :goto_0
    return v1

    .line 157
    :catch_0
    move-exception v0

    .line 158
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/keystore/TimaKeystore;->TAG:Ljava/lang/String;

    const-string v2, "Failed at TimaKeystore API isTimKeystoreEnabled-Exception"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 161
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isTimaKeystoreEnabledInDB()Z
    .locals 3

    .prologue
    .line 169
    invoke-static {}, Lcom/sec/enterprise/knox/keystore/TimaKeystore;->getKeystoreService()Lcom/sec/enterprise/knox/keystore/ITimaKeystore;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 170
    iget-object v1, p0, Lcom/sec/enterprise/knox/keystore/TimaKeystore;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "TimaKeystore.isTimaKeystoreEnabledInDB"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 172
    :try_start_0
    invoke-static {}, Lcom/sec/enterprise/knox/keystore/TimaKeystore;->getKeystoreService()Lcom/sec/enterprise/knox/keystore/ITimaKeystore;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/keystore/TimaKeystore;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Lcom/sec/enterprise/knox/keystore/ITimaKeystore;->isTimaKeystoreEnabledInDB(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 177
    :goto_0
    return v1

    .line 173
    :catch_0
    move-exception v0

    .line 174
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/keystore/TimaKeystore;->TAG:Ljava/lang/String;

    const-string v2, "Failed at TimaKeystore API isTimaKeystoreEnabledInDB-Exception"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 177
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
