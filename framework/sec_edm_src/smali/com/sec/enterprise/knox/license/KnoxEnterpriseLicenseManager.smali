.class public Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;
.super Ljava/lang/Object;
.source "KnoxEnterpriseLicenseManager.java"


# static fields
.field public static final ACTION_LICENSE_DEACTIVATION:Ljava/lang/String; = "edm.intent.action.knox_license.deactivation.internal"

.field public static final ACTION_LICENSE_REGISTRATION:Ljava/lang/String; = "edm.intent.action.knox_license.registration.internal"

.field public static final ACTION_LICENSE_REGISTRATION_UMC:Ljava/lang/String; = "edm.intent.action.knox_license.registration.internal_umc"

.field public static final ACTION_LICENSE_STATUS:Ljava/lang/String; = "edm.intent.action.knox_license.status"

.field public static final ERROR_INTERNAL:I = 0x12d

.field public static final ERROR_INTERNAL_SERVER:I = 0x191

.field public static final ERROR_INVALID_LICENSE:I = 0xc9

.field public static final ERROR_INVALID_PACKAGE_NAME:I = 0xcc

.field public static final ERROR_LICENSE_ACTIVATION_NOT_FOUND:I = 0x2bf

.field public static final ERROR_LICENSE_DEACTIVATED:I = 0x2bc

.field public static final ERROR_LICENSE_EXPIRED:I = 0x2bd

.field public static final ERROR_LICENSE_QUANTITY_EXHAUSTED:I = 0x2be

.field public static final ERROR_LICENSE_TERMINATED:I = 0xcb

.field public static final ERROR_NETWORK_DISCONNECTED:I = 0x1f5

.field public static final ERROR_NETWORK_GENERAL:I = 0x1f6

.field public static final ERROR_NONE:I = 0x0

.field public static final ERROR_NOT_CURRENT_DATE:I = 0xcd

.field public static final ERROR_NULL_PARAMS:I = 0x65

.field public static final ERROR_UNKNOWN:I = 0x66

.field public static final ERROR_USER_DISAGREES_LICENSE_AGREEMENT:I = 0x259

.field public static final EXTRA_LICENSE_ACTIVATION_INITIATOR:Ljava/lang/String; = "edm.intent.extra.knox_license.activaton_initiator"

.field public static final EXTRA_LICENSE_DATA_PACKAGENAME:Ljava/lang/String; = "edm.intent.extra.knox_license.data.pkgname"

.field public static final EXTRA_LICENSE_ERROR_CODE:Ljava/lang/String; = "edm.intent.extra.knox_license.errorcode"

.field public static final EXTRA_LICENSE_RESULT_TYPE:Ljava/lang/String; = "edm.intent.extra.knox_license.result_type"

.field public static final EXTRA_LICENSE_STATUS:Ljava/lang/String; = "edm.intent.extra.knox_license.status"

.field public static final LICENSE_ACTIVATION_INITIATOR_ADMIN:I = 0x384

.field public static final LICENSE_ACTIVATION_INITIATOR_SMS:I = 0x385

.field public static final LICENSE_ACTIVATION_INITIATOR_USER:I = 0x386

.field public static final LICENSE_RESULT_TYPE_ACTIVATION:I = 0x320

.field public static final LICENSE_RESULT_TYPE_DEACTIVATION:I = 0x322

.field public static final LICENSE_RESULT_TYPE_VALIDATION:I = 0x321

.field private static final TAG:Ljava/lang/String; = "KnoxKnoxEnterpriseLicenseManager"

.field private static gLicenseMgrInst:Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;

.field private static lService:Landroid/app/enterprise/license/IEnterpriseLicense;

.field private static final mSync:Ljava/lang/Object;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mContextInfo:Landroid/app/enterprise/ContextInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 59
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;->mSync:Ljava/lang/Object;

    return-void
.end method

.method constructor <init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V
    .locals 1
    .param p1, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 220
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 221
    const-string v0, "enterprise_license_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/app/enterprise/license/IEnterpriseLicense$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/license/IEnterpriseLicense;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;->lService:Landroid/app/enterprise/license/IEnterpriseLicense;

    .line 222
    iput-object p2, p0, Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;->mContext:Landroid/content/Context;

    .line 223
    iput-object p1, p0, Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    .line 224
    return-void
.end method

.method public static createInstance(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;
    .locals 1
    .param p0, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 275
    new-instance v0, Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;

    invoke-direct {v0, p0, p1}, Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    return-object v0
.end method

.method public static getInstance(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;
    .locals 2
    .param p0, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 261
    sget-object v1, Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;->mSync:Ljava/lang/Object;

    monitor-enter v1

    .line 262
    :try_start_0
    sget-object v0, Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;->gLicenseMgrInst:Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;

    if-nez v0, :cond_0

    .line 263
    new-instance v0, Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;

    invoke-direct {v0, p0, p1}, Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    sput-object v0, Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;->gLicenseMgrInst:Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;

    .line 265
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;->gLicenseMgrInst:Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;

    monitor-exit v1

    return-object v0

    .line 266
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 246
    sget-object v2, Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;->mSync:Ljava/lang/Object;

    monitor-enter v2

    .line 247
    :try_start_0
    sget-object v1, Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;->gLicenseMgrInst:Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;

    if-nez v1, :cond_0

    .line 248
    new-instance v0, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    invoke-direct {v0, v1}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    .line 249
    .local v0, "cxtInfo":Landroid/app/enterprise/ContextInfo;
    new-instance v1, Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;

    invoke-direct {v1, v0, p0}, Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    sput-object v1, Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;->gLicenseMgrInst:Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;

    .line 251
    .end local v0    # "cxtInfo":Landroid/app/enterprise/ContextInfo;
    :cond_0
    sget-object v1, Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;->gLicenseMgrInst:Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;

    monitor-exit v2

    return-object v1

    .line 252
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private static getService()Landroid/app/enterprise/license/IEnterpriseLicense;
    .locals 1

    .prologue
    .line 227
    sget-object v0, Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;->lService:Landroid/app/enterprise/license/IEnterpriseLicense;

    if-nez v0, :cond_0

    .line 228
    const-string v0, "enterprise_license_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/app/enterprise/license/IEnterpriseLicense$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/license/IEnterpriseLicense;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;->lService:Landroid/app/enterprise/license/IEnterpriseLicense;

    .line 230
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;->lService:Landroid/app/enterprise/license/IEnterpriseLicense;

    return-object v0
.end method


# virtual methods
.method public activateLicense(Ljava/lang/String;)V
    .locals 4
    .param p1, "knoxLicenseKey"    # Ljava/lang/String;

    .prologue
    .line 353
    :try_start_0
    invoke-static {}, Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;->getService()Landroid/app/enterprise/license/IEnterpriseLicense;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 354
    sget-object v1, Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;->lService:Landroid/app/enterprise/license/IEnterpriseLicense;

    iget-object v2, p0, Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v3, 0x0

    invoke-interface {v1, v2, p1, v3}, Landroid/app/enterprise/license/IEnterpriseLicense;->activateKnoxLicense(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 360
    :cond_0
    :goto_0
    return-void

    .line 357
    :catch_0
    move-exception v0

    .line 358
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxKnoxEnterpriseLicenseManager"

    const-string v2, "Failed talking to License policy service "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public activateLicense(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "knoxLicenseKey"    # Ljava/lang/String;
    .param p2, "pkgName"    # Ljava/lang/String;

    .prologue
    .line 364
    :try_start_0
    invoke-static {}, Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;->getService()Landroid/app/enterprise/license/IEnterpriseLicense;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 365
    sget-object v1, Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;->lService:Landroid/app/enterprise/license/IEnterpriseLicense;

    iget-object v2, p0, Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/license/IEnterpriseLicense;->activateKnoxLicense(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 371
    :cond_0
    :goto_0
    return-void

    .line 368
    :catch_0
    move-exception v0

    .line 369
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxKnoxEnterpriseLicenseManager"

    const-string v2, "Failed talking to License policy service "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public activateLicenseForUMC(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "knoxLicenseKey"    # Ljava/lang/String;
    .param p2, "pkgName"    # Ljava/lang/String;

    .prologue
    .line 390
    :try_start_0
    invoke-static {}, Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;->getService()Landroid/app/enterprise/license/IEnterpriseLicense;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 391
    sget-object v1, Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;->lService:Landroid/app/enterprise/license/IEnterpriseLicense;

    iget-object v2, p0, Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/license/IEnterpriseLicense;->activateKnoxLicenseForUMC(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 397
    :cond_0
    :goto_0
    return-void

    .line 394
    :catch_0
    move-exception v0

    .line 395
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxKnoxEnterpriseLicenseManager"

    const-string v2, "Failed talking to License policy service for UMC"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public deActivateLicense(Ljava/lang/String;)V
    .locals 4
    .param p1, "knoxLicenseKey"    # Ljava/lang/String;

    .prologue
    .line 436
    :try_start_0
    invoke-static {}, Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;->getService()Landroid/app/enterprise/license/IEnterpriseLicense;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 437
    sget-object v1, Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;->lService:Landroid/app/enterprise/license/IEnterpriseLicense;

    iget-object v2, p0, Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v3, 0x0

    invoke-interface {v1, v2, p1, v3}, Landroid/app/enterprise/license/IEnterpriseLicense;->deActivateKnoxLicense(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 443
    :cond_0
    :goto_0
    return-void

    .line 440
    :catch_0
    move-exception v0

    .line 441
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxKnoxEnterpriseLicenseManager"

    const-string v2, "Failed talking to License policy service "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public deActivateLicense(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "knoxLicenseKey"    # Ljava/lang/String;
    .param p2, "pkgName"    # Ljava/lang/String;

    .prologue
    .line 447
    :try_start_0
    invoke-static {}, Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;->getService()Landroid/app/enterprise/license/IEnterpriseLicense;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 448
    sget-object v1, Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;->lService:Landroid/app/enterprise/license/IEnterpriseLicense;

    iget-object v2, p0, Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/license/IEnterpriseLicense;->deActivateKnoxLicense(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 454
    :cond_0
    :goto_0
    return-void

    .line 451
    :catch_0
    move-exception v0

    .line 452
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxKnoxEnterpriseLicenseManager"

    const-string v2, "Failed talking to License policy service "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public deActivateLicenseForUMC(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "knoxLicenseKey"    # Ljava/lang/String;
    .param p2, "pkgName"    # Ljava/lang/String;

    .prologue
    .line 458
    :try_start_0
    invoke-static {}, Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;->getService()Landroid/app/enterprise/license/IEnterpriseLicense;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 459
    sget-object v1, Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;->lService:Landroid/app/enterprise/license/IEnterpriseLicense;

    iget-object v2, p0, Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/license/IEnterpriseLicense;->deActivateKnoxLicenseForUMC(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 465
    :cond_0
    :goto_0
    return-void

    .line 462
    :catch_0
    move-exception v0

    .line 463
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxKnoxEnterpriseLicenseManager"

    const-string v2, "Failed talking to License policy service "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public getKLMLicenseKey(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 409
    :try_start_0
    invoke-static {}, Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;->getService()Landroid/app/enterprise/license/IEnterpriseLicense;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 410
    sget-object v1, Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;->lService:Landroid/app/enterprise/license/IEnterpriseLicense;

    invoke-interface {v1, p1}, Landroid/app/enterprise/license/IEnterpriseLicense;->getKLMLicenseKey(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 416
    :goto_0
    return-object v1

    .line 412
    :catch_0
    move-exception v0

    .line 413
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxKnoxEnterpriseLicenseManager"

    const-string v2, "Failed talking to License policy service "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 416
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getKLMLicenseKeyForDeactivation(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 477
    :try_start_0
    invoke-static {}, Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;->getService()Landroid/app/enterprise/license/IEnterpriseLicense;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 478
    sget-object v1, Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;->lService:Landroid/app/enterprise/license/IEnterpriseLicense;

    invoke-interface {v1, p1}, Landroid/app/enterprise/license/IEnterpriseLicense;->getKLMLicenseKeyForDeactivation(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 484
    :goto_0
    return-object v1

    .line 480
    :catch_0
    move-exception v0

    .line 481
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxKnoxEnterpriseLicenseManager"

    const-string v2, "Failed talking to License policy service "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 484
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public processLicenseResponse(Ljava/lang/String;Ljava/lang/String;Landroid/app/enterprise/license/Error;II)Z
    .locals 7
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "status"    # Ljava/lang/String;
    .param p3, "error"    # Landroid/app/enterprise/license/Error;
    .param p4, "initiator"    # I
    .param p5, "result_type"    # I

    .prologue
    .line 297
    :try_start_0
    invoke-static {}, Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;->getService()Landroid/app/enterprise/license/IEnterpriseLicense;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 298
    sget-object v0, Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;->lService:Landroid/app/enterprise/license/IEnterpriseLicense;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    invoke-interface/range {v0 .. v5}, Landroid/app/enterprise/license/IEnterpriseLicense;->processKnoxLicenseResponse(Ljava/lang/String;Ljava/lang/String;Landroid/app/enterprise/license/Error;II)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 305
    :goto_0
    return v0

    .line 301
    :catch_0
    move-exception v6

    .line 302
    .local v6, "e":Landroid/os/RemoteException;
    const-string v0, "KnoxKnoxEnterpriseLicenseManager"

    const-string v1, "Failed talking to License policy service "

    invoke-static {v0, v1, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 305
    .end local v6    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public processLicenseResponseForUMC(Ljava/lang/String;Ljava/lang/String;Landroid/app/enterprise/license/Error;II)Z
    .locals 7
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "status"    # Ljava/lang/String;
    .param p3, "error"    # Landroid/app/enterprise/license/Error;
    .param p4, "initiator"    # I
    .param p5, "result_type"    # I

    .prologue
    .line 325
    :try_start_0
    invoke-static {}, Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;->getService()Landroid/app/enterprise/license/IEnterpriseLicense;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 326
    sget-object v0, Lcom/sec/enterprise/knox/license/KnoxEnterpriseLicenseManager;->lService:Landroid/app/enterprise/license/IEnterpriseLicense;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    invoke-interface/range {v0 .. v5}, Landroid/app/enterprise/license/IEnterpriseLicense;->processKnoxLicenseResponseForUMC(Ljava/lang/String;Ljava/lang/String;Landroid/app/enterprise/license/Error;II)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 333
    :goto_0
    return v0

    .line 329
    :catch_0
    move-exception v6

    .line 330
    .local v6, "e":Landroid/os/RemoteException;
    const-string v0, "KnoxKnoxEnterpriseLicenseManager"

    const-string v1, "Failed talking to License policy service for UMC"

    invoke-static {v0, v1, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 333
    .end local v6    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
