.class public Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;
.super Ljava/lang/Object;
.source "AdvancedRestrictionPolicy.java"


# static fields
.field public static final CONSTRAINED_STATE_DISABLED:I = 0x0

.field public static final CONSTRAINED_STATE_ENABLED_AND_DEVICE_CONSTRAINED:I = 0x2

.field public static final CONSTRAINED_STATE_ENABLED_BUT_DEVICE_NOT_CONSTRAINED:I = 0x1

.field public static final CONSTRAINED_STATE_RESTRICT_BLUETOOTH:I = 0x8

.field public static final CONSTRAINED_STATE_RESTRICT_CAMERA:I = 0x1

.field public static final CONSTRAINED_STATE_RESTRICT_EXTERNAL_SDCARD:I = 0x2

.field public static final CONSTRAINED_STATE_RESTRICT_MTP:I = 0x4

.field public static final CONSTRAINED_STATE_RESTRICT_SCREEN_CAPTURE:I = 0x40

.field public static final CONSTRAINED_STATE_RESTRICT_TETHERING:I = 0x10

.field public static final CONSTRAINED_STATE_RESTRICT_USB_DEBUGGING:I = 0x20

.field private static TAG:Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mContextInfo:Landroid/app/enterprise/ContextInfo;

.field private mService:Landroid/app/enterprise/IRestrictionPolicy;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-string v0, "AdvancedRestrictionPolicy"

    sput-object v0, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V
    .locals 0
    .param p1, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p2, "cxt"    # Landroid/content/Context;

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object p1, p0, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    .line 54
    iput-object p2, p0, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->mContext:Landroid/content/Context;

    .line 55
    return-void
.end method

.method private getService()Landroid/app/enterprise/IRestrictionPolicy;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    if-nez v0, :cond_0

    .line 59
    const-string v0, "restriction_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    .line 62
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    return-object v0
.end method


# virtual methods
.method public addNewAdminActivationAppWhiteList(Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 529
    .local p1, "packageNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "AdvancedRestrictionPolicy.addNewAdminActivationAppWhiteList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 530
    invoke-direct {p0}, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 532
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->addNewAdminActivationAppWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 537
    :goto_0
    return v1

    .line 533
    :catch_0
    move-exception v0

    .line 534
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 537
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public allowFirmwareAutoUpdate(Z)Z
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 101
    iget-object v1, p0, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "AdvancedRestrictionPolicy.allowFirmwareAutoUpdate"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 102
    invoke-direct {p0}, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 104
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->allowFirmwareAutoUpdate(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 109
    :goto_0
    return v1

    .line 105
    :catch_0
    move-exception v0

    .line 106
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 109
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public clearNewAdminActivationAppWhiteList()Z
    .locals 3

    .prologue
    .line 592
    iget-object v1, p0, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "AdvancedRestrictionPolicy.clearNewAdminActivationAppWhiteList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 593
    invoke-direct {p0}, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 595
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IRestrictionPolicy;->clearNewAdminActivationAppWhiteList(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 600
    :goto_0
    return v1

    .line 596
    :catch_0
    move-exception v0

    .line 597
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 600
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public disableConstrainedState()Z
    .locals 5

    .prologue
    .line 665
    iget-object v2, p0, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "AdvancedRestrictionPolicy.disableConstrainedState"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 666
    iget-object v2, p0, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->mContext:Landroid/content/Context;

    const-string v3, "enterprise_policy"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/EnterpriseDeviceManager;

    .line 668
    .local v1, "mEDM":Landroid/app/enterprise/EnterpriseDeviceManager;
    if-eqz v1, :cond_0

    .line 670
    :try_start_0
    invoke-virtual {v1}, Landroid/app/enterprise/EnterpriseDeviceManager;->disableConstrainedState()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 675
    :goto_0
    return v2

    .line 671
    :catch_0
    move-exception v0

    .line 672
    .local v0, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "exception occured! "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 675
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public enableConstrainedState(Ljava/lang/String;I)Z
    .locals 1
    .param p1, "Comment"    # Ljava/lang/String;
    .param p2, "PolicyBitMask"    # I

    .prologue
    .line 639
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->enableConstrainedState(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method public enableConstrainedState(Ljava/lang/String;Ljava/lang/String;I)Z
    .locals 5
    .param p1, "Comment"    # Ljava/lang/String;
    .param p2, "DownloadUrl"    # Ljava/lang/String;
    .param p3, "PolicyBitMask"    # I

    .prologue
    .line 647
    iget-object v2, p0, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "AdvancedRestrictionPolicy.enableConstrainedState"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 648
    iget-object v2, p0, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->mContext:Landroid/content/Context;

    const-string v3, "enterprise_policy"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/EnterpriseDeviceManager;

    .line 650
    .local v1, "mEDM":Landroid/app/enterprise/EnterpriseDeviceManager;
    if-eqz v1, :cond_0

    .line 652
    :try_start_0
    invoke-virtual {v1, p1, p2, p3}, Landroid/app/enterprise/EnterpriseDeviceManager;->enableConstrainedState(Ljava/lang/String;Ljava/lang/String;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 657
    :goto_0
    return v2

    .line 653
    :catch_0
    move-exception v0

    .line 654
    .local v0, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "exception occured! "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 657
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public enableODETrustedBootVerification(Z)Z
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 250
    iget-object v1, p0, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "AdvancedRestrictionPolicy.enableODETrustedBootVerification"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 251
    invoke-direct {p0}, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 253
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->enableODETrustedBootVerification(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 258
    :goto_0
    return v1

    .line 254
    :catch_0
    move-exception v0

    .line 255
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 258
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getConstrainedState()I
    .locals 5

    .prologue
    .line 683
    iget-object v2, p0, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->mContext:Landroid/content/Context;

    const-string v3, "enterprise_policy"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/EnterpriseDeviceManager;

    .line 685
    .local v1, "mEDM":Landroid/app/enterprise/EnterpriseDeviceManager;
    if-eqz v1, :cond_0

    .line 687
    :try_start_0
    invoke-virtual {v1}, Landroid/app/enterprise/EnterpriseDeviceManager;->getConstrainedState()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 692
    :goto_0
    return v2

    .line 688
    :catch_0
    move-exception v0

    .line 689
    .local v0, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "exception occured! "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 692
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getNewAdminActivationAppWhiteList()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 623
    iget-object v1, p0, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "AdvancedRestrictionPolicy.getNewAdminActivationAppWhiteList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 624
    invoke-direct {p0}, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 626
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IRestrictionPolicy;->getNewAdminActivationAppWhiteList(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 631
    :goto_0
    return-object v1

    .line 627
    :catch_0
    move-exception v0

    .line 628
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 631
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isCCModeEnabled(Z)Z
    .locals 3
    .param p1, "showMsg"    # Z

    .prologue
    .line 199
    invoke-direct {p0}, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 201
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->isCCModeEnabled(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 206
    :goto_0
    return v1

    .line 202
    :catch_0
    move-exception v0

    .line 203
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 206
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isCCModeSupported(Z)Z
    .locals 3
    .param p1, "showMsg"    # Z

    .prologue
    .line 223
    invoke-direct {p0}, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 225
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->isCCModeSupported(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 230
    :goto_0
    return v1

    .line 226
    :catch_0
    move-exception v0

    .line 227
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 230
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isFirmwareAutoUpdateAllowed(Z)Z
    .locals 3
    .param p1, "showMsg"    # Z

    .prologue
    .line 129
    invoke-direct {p0}, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 131
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->isFirmwareAutoUpdateAllowed(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 136
    :goto_0
    return v1

    .line 132
    :catch_0
    move-exception v0

    .line 133
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 136
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isNewAdminActivationEnabled(Z)Z
    .locals 3
    .param p1, "showMsg"    # Z

    .prologue
    .line 396
    invoke-direct {p0}, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 398
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->isNewAdminActivationEnabled(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 403
    :goto_0
    return v1

    .line 399
    :catch_0
    move-exception v0

    .line 400
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 403
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isNewAdminInstallationEnabled(Z)Z
    .locals 3
    .param p1, "showMsg"    # Z

    .prologue
    .line 369
    invoke-direct {p0}, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 371
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->isNewAdminInstallationEnabled(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 376
    :goto_0
    return v1

    .line 372
    :catch_0
    move-exception v0

    .line 373
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 376
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isODETrustedBootVerificationEnabled()Z
    .locals 3

    .prologue
    .line 277
    iget-object v1, p0, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "AdvancedRestrictionPolicy.isODETruestedBootVerfiicationEnabled"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 278
    invoke-direct {p0}, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 280
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IRestrictionPolicy;->isODETrustedBootVerificationEnabled(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 285
    :goto_0
    return v1

    .line 281
    :catch_0
    move-exception v0

    .line 282
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 285
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public preventNewAdminActivation(Z)Z
    .locals 3
    .param p1, "prevent"    # Z

    .prologue
    .line 463
    iget-object v1, p0, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "AdvancedRestrictionPolicy.preventNewAdminActivation"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 464
    invoke-direct {p0}, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 466
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->preventNewAdminActivation(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 471
    :goto_0
    return v1

    .line 467
    :catch_0
    move-exception v0

    .line 468
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 471
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public preventNewAdminInstallation(Z)Z
    .locals 3
    .param p1, "prevent"    # Z

    .prologue
    .line 342
    iget-object v1, p0, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "AdvancedRestrictionPolicy.preventNewAdminInstallation"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 343
    invoke-direct {p0}, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 345
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->preventNewAdminInstallation(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 350
    :goto_0
    return v1

    .line 346
    :catch_0
    move-exception v0

    .line 347
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 350
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setCCMode(Z)Z
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 174
    iget-object v1, p0, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "AdvancedRestrictionPolicy.setCCMode"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 175
    invoke-direct {p0}, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 177
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->setCCMode(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 182
    :goto_0
    return v1

    .line 178
    :catch_0
    move-exception v0

    .line 179
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 182
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
