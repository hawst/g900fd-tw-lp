.class public Lcom/sec/enterprise/knox/ContainerFirewallPolicy;
.super Ljava/lang/Object;
.source "ContainerFirewallPolicy.java"


# static fields
.field private static final ACCEPT:I = 0x1

.field private static final DROP:I = 0x2

.field public static final MARKET_ALL_NETWORKS:I = 0x0

.field public static final MARKET_WIFI_ONLY:I = 0x1

.field private static final PROXY:I = 0x3

.field private static final REDIRECT:I = 0x0

.field private static final REDIRECT_EXCEPTION:I = 0x7

.field private static TAG:Ljava/lang/String;

.field private static gFirewallService:Landroid/app/enterprise/IFirewallPolicy;


# instance fields
.field private mContextInfo:Landroid/app/enterprise/ContextInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 127
    const-string v0, "ContainerFirewallPolicy"

    sput-object v0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(I)V
    .locals 2
    .param p1, "containerId"    # I

    .prologue
    .line 166
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 167
    new-instance v0, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    invoke-direct {v0, v1, p1}, Landroid/app/enterprise/ContextInfo;-><init>(II)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    .line 168
    return-void
.end method

.method private checkEmptyList(Ljava/util/List;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p1, "rulesList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v0, 0x1

    .line 2012
    if-nez p1, :cond_1

    .line 2018
    :cond_0
    :goto_0
    return v0

    .line 2015
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2018
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private declared-synchronized getFirewallService()Landroid/app/enterprise/IFirewallPolicy;
    .locals 1

    .prologue
    .line 159
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->gFirewallService:Landroid/app/enterprise/IFirewallPolicy;

    if-nez v0, :cond_0

    .line 160
    const-string v0, "firewall_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/app/enterprise/IFirewallPolicy$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->gFirewallService:Landroid/app/enterprise/IFirewallPolicy;

    .line 163
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->gFirewallService:Landroid/app/enterprise/IFirewallPolicy;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 159
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public addIptablesAllowRules(Ljava/util/List;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 237
    .local p1, "rulesList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerFirewallPolicy.addIptablesAllowRules"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 239
    const/4 v2, 0x0

    .line 240
    .local v2, "retVal":Z
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->getFirewallService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v1

    .line 241
    .local v1, "firewallService":Landroid/app/enterprise/IFirewallPolicy;
    if-nez v1, :cond_0

    .line 242
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "FirewallService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 254
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 246
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->checkEmptyList(Ljava/util/List;)Z

    move-result v4

    if-eqz v4, :cond_1

    move v3, v2

    .line 247
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 250
    .end local v3    # "retVal":I
    :cond_1
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v5, 0x1

    invoke-interface {v1, v4, p1, v5}, Landroid/app/enterprise/IFirewallPolicy;->addRules(Landroid/app/enterprise/ContextInfo;Ljava/util/List;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 254
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 251
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 252
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "RemoteException..."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public addIptablesDenyRules(Ljava/util/List;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 333
    .local p1, "rulesList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerFirewallPolicy.addIptablesDenyRules"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 335
    const/4 v2, 0x0

    .line 336
    .local v2, "retVal":Z
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->getFirewallService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v1

    .line 337
    .local v1, "firewallService":Landroid/app/enterprise/IFirewallPolicy;
    if-nez v1, :cond_0

    .line 338
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "FirewallService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 350
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 342
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->checkEmptyList(Ljava/util/List;)Z

    move-result v4

    if-eqz v4, :cond_1

    move v3, v2

    .line 343
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 346
    .end local v3    # "retVal":I
    :cond_1
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v5, 0x2

    invoke-interface {v1, v4, p1, v5}, Landroid/app/enterprise/IFirewallPolicy;->addRules(Landroid/app/enterprise/ContextInfo;Ljava/util/List;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 350
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 347
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 348
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "RemoteException..."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public addIptablesRedirectExceptionsRules(Ljava/util/List;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 2758
    .local p1, "rulesList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerFirewallPolicy.addIptablesRedirectExceptionsRules"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2759
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "addIptablesRedirectExceptions"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2761
    const/4 v2, 0x0

    .line 2762
    .local v2, "retVal":Z
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->getFirewallService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v1

    .line 2763
    .local v1, "firewallService":Landroid/app/enterprise/IFirewallPolicy;
    if-nez v1, :cond_0

    .line 2764
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "FirewallService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 2773
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 2769
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v5, 0x7

    invoke-interface {v1, v4, p1, v5}, Landroid/app/enterprise/IFirewallPolicy;->addRules(Landroid/app/enterprise/ContextInfo;Ljava/util/List;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 2773
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 2770
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 2771
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "RemoteException on addIptablesRedirectExceptions"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public addIptablesRerouteRules(Ljava/util/List;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 430
    .local p1, "rulesList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerFirewallPolicy.addIptablesRerouteRules"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 432
    const/4 v2, 0x0

    .line 433
    .local v2, "retVal":Z
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->getFirewallService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v1

    .line 434
    .local v1, "firewallService":Landroid/app/enterprise/IFirewallPolicy;
    if-nez v1, :cond_0

    .line 435
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "FirewallService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 447
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 439
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->checkEmptyList(Ljava/util/List;)Z

    move-result v4

    if-eqz v4, :cond_1

    move v3, v2

    .line 440
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 443
    .end local v3    # "retVal":I
    :cond_1
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v5, 0x0

    invoke-interface {v1, v4, p1, v5}, Landroid/app/enterprise/IFirewallPolicy;->addRules(Landroid/app/enterprise/ContextInfo;Ljava/util/List;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 447
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 444
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 445
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "RemoteException..."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public cleanIptablesAllowRules()Z
    .locals 6

    .prologue
    .line 1592
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerFirewallPolicy.cleanIptablesAllowRules"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1594
    const/4 v2, 0x0

    .line 1595
    .local v2, "retVal":Z
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->getFirewallService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v1

    .line 1596
    .local v1, "firewallService":Landroid/app/enterprise/IFirewallPolicy;
    if-nez v1, :cond_0

    .line 1597
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "FirewallService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 1606
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 1602
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v5, 0x1

    invoke-interface {v1, v4, v5}, Landroid/app/enterprise/IFirewallPolicy;->cleanRules(Landroid/app/enterprise/ContextInfo;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 1606
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 1603
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 1604
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "RemoteException..."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public cleanIptablesDenyRules()Z
    .locals 6

    .prologue
    .line 1635
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerFirewallPolicy.cleanIptablesDenyRules"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1637
    const/4 v2, 0x0

    .line 1638
    .local v2, "retVal":Z
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->getFirewallService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v1

    .line 1639
    .local v1, "firewallService":Landroid/app/enterprise/IFirewallPolicy;
    if-nez v1, :cond_0

    .line 1640
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "FirewallService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 1649
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 1645
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v5, 0x2

    invoke-interface {v1, v4, v5}, Landroid/app/enterprise/IFirewallPolicy;->cleanRules(Landroid/app/enterprise/ContextInfo;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 1649
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 1646
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 1647
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "RemoteException..."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public cleanIptablesProxyRules()Z
    .locals 6

    .prologue
    .line 1767
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerFirewallPolicy.cleanIptablesProxyRules"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1769
    const/4 v2, 0x0

    .line 1770
    .local v2, "retVal":Z
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->getFirewallService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v1

    .line 1771
    .local v1, "firewallService":Landroid/app/enterprise/IFirewallPolicy;
    if-nez v1, :cond_0

    .line 1772
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "FirewallService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 1781
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 1777
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v5, 0x3

    invoke-interface {v1, v4, v5}, Landroid/app/enterprise/IFirewallPolicy;->cleanRules(Landroid/app/enterprise/ContextInfo;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 1781
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 1778
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 1779
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "RemoteException..."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public cleanIptablesRedirectExceptionsRules()Z
    .locals 6

    .prologue
    .line 2948
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerFirewallPolicy.cleanIptablesRedirectExceptionsRules"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2950
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "cleanIptablesRedirectExceptions"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2952
    const/4 v2, 0x0

    .line 2953
    .local v2, "retVal":Z
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->getFirewallService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v1

    .line 2954
    .local v1, "firewallService":Landroid/app/enterprise/IFirewallPolicy;
    if-nez v1, :cond_0

    .line 2955
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "FirewallService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 2964
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 2960
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v5, 0x7

    invoke-interface {v1, v4, v5}, Landroid/app/enterprise/IFirewallPolicy;->cleanRules(Landroid/app/enterprise/ContextInfo;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 2964
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 2961
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 2962
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "RemoteException on cleanIptablesRedirectExceptions"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public cleanIptablesRerouteRules()Z
    .locals 6

    .prologue
    .line 1678
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerFirewallPolicy.cleanIptablesRerouteRules"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1680
    const/4 v2, 0x0

    .line 1681
    .local v2, "retVal":Z
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->getFirewallService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v1

    .line 1682
    .local v1, "firewallService":Landroid/app/enterprise/IFirewallPolicy;
    if-nez v1, :cond_0

    .line 1683
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "FirewallService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 1692
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 1688
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v5, 0x0

    invoke-interface {v1, v4, v5}, Landroid/app/enterprise/IFirewallPolicy;->cleanRules(Landroid/app/enterprise/ContextInfo;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 1692
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 1689
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 1690
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "RemoteException..."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public getIptablesAllowRules()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 770
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerFirewallPolicy.getIptablesAllowRules"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 771
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "getIptablesAllowRules..."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 773
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 774
    .local v2, "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->getFirewallService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v1

    .line 775
    .local v1, "firewallService":Landroid/app/enterprise/IFirewallPolicy;
    if-nez v1, :cond_0

    .line 776
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "FirewallService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, v2

    .line 787
    .end local v2    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local v3, "retVal":Ljava/lang/Object;, "Ljava/util/List<Ljava/lang/String;>;"
    :goto_0
    return-object v3

    .line 781
    .end local v3    # "retVal":Ljava/lang/Object;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v2    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_0
    :try_start_0
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "getIptablesAllowRules... 1"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 782
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v5, 0x1

    invoke-interface {v1, v4, v5}, Landroid/app/enterprise/IFirewallPolicy;->getRules(Landroid/app/enterprise/ContextInfo;I)Ljava/util/List;

    move-result-object v2

    .line 783
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "getIptablesAllowRules..."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    move-object v3, v2

    .line 787
    .restart local v3    # "retVal":Ljava/lang/Object;, "Ljava/util/List<Ljava/lang/String;>;"
    goto :goto_0

    .line 784
    .end local v3    # "retVal":Ljava/lang/Object;, "Ljava/util/List<Ljava/lang/String;>;"
    :catch_0
    move-exception v0

    .line 785
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "RemoteException..."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public getIptablesDenyRules()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 844
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerFirewallPolicy.getIptablesDenyRules"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 846
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 847
    .local v2, "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->getFirewallService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v1

    .line 848
    .local v1, "firewallService":Landroid/app/enterprise/IFirewallPolicy;
    if-nez v1, :cond_0

    .line 849
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "FirewallService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, v2

    .line 858
    .end local v2    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local v3, "retVal":Ljava/lang/Object;, "Ljava/util/List<Ljava/lang/String;>;"
    :goto_0
    return-object v3

    .line 854
    .end local v3    # "retVal":Ljava/lang/Object;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v2    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v5, 0x2

    invoke-interface {v1, v4, v5}, Landroid/app/enterprise/IFirewallPolicy;->getRules(Landroid/app/enterprise/ContextInfo;I)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :goto_1
    move-object v3, v2

    .line 858
    .restart local v3    # "retVal":Ljava/lang/Object;, "Ljava/util/List<Ljava/lang/String;>;"
    goto :goto_0

    .line 855
    .end local v3    # "retVal":Ljava/lang/Object;, "Ljava/util/List<Ljava/lang/String;>;"
    :catch_0
    move-exception v0

    .line 856
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "RemoteException..."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public getIptablesOption()Z
    .locals 6

    .prologue
    .line 1141
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerFirewallPolicy.getIptablesOption"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1143
    const/4 v2, 0x0

    .line 1144
    .local v2, "retVal":Z
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->getFirewallService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v1

    .line 1145
    .local v1, "firewallService":Landroid/app/enterprise/IFirewallPolicy;
    if-nez v1, :cond_0

    .line 1146
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "FirewallService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 1155
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 1151
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v4}, Landroid/app/enterprise/IFirewallPolicy;->isEnabledRules(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 1155
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 1152
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 1153
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "RemoteException..."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public getIptablesProxyOption()Z
    .locals 6

    .prologue
    .line 1204
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerFirewallPolicy.getIptablesProxyOption"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1206
    const/4 v2, 0x0

    .line 1207
    .local v2, "retVal":Z
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->getFirewallService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v1

    .line 1208
    .local v1, "firewallService":Landroid/app/enterprise/IFirewallPolicy;
    if-nez v1, :cond_0

    .line 1209
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "FirewallService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 1218
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 1214
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v4}, Landroid/app/enterprise/IFirewallPolicy;->isEnabledProxy(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 1218
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 1215
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 1216
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "RemoteException..."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public getIptablesProxyRules()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 978
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerFirewallPolicy.getIptablesProxyRules"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 980
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 981
    .local v2, "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->getFirewallService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v1

    .line 982
    .local v1, "firewallService":Landroid/app/enterprise/IFirewallPolicy;
    if-nez v1, :cond_0

    .line 983
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "FirewallService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, v2

    .line 992
    .end local v2    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local v3, "retVal":Ljava/lang/Object;, "Ljava/util/List<Ljava/lang/String;>;"
    :goto_0
    return-object v3

    .line 988
    .end local v3    # "retVal":Ljava/lang/Object;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v2    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v5, 0x3

    invoke-interface {v1, v4, v5}, Landroid/app/enterprise/IFirewallPolicy;->getRules(Landroid/app/enterprise/ContextInfo;I)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :goto_1
    move-object v3, v2

    .line 992
    .restart local v3    # "retVal":Ljava/lang/Object;, "Ljava/util/List<Ljava/lang/String;>;"
    goto :goto_0

    .line 989
    .end local v3    # "retVal":Ljava/lang/Object;, "Ljava/util/List<Ljava/lang/String;>;"
    :catch_0
    move-exception v0

    .line 990
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "RemoteException..."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public getIptablesRedirectExceptionsRules()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2905
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerFirewallPolicy.getIptablesRedirectExceptionsRules"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2906
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "getIptablesRedirectExceptions"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2908
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2909
    .local v2, "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->getFirewallService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v1

    .line 2910
    .local v1, "firewallService":Landroid/app/enterprise/IFirewallPolicy;
    if-nez v1, :cond_0

    .line 2911
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "FirewallService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, v2

    .line 2920
    .end local v2    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local v3, "retVal":Ljava/lang/Object;, "Ljava/util/List<Ljava/lang/String;>;"
    :goto_0
    return-object v3

    .line 2916
    .end local v3    # "retVal":Ljava/lang/Object;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v2    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v5, 0x7

    invoke-interface {v1, v4, v5}, Landroid/app/enterprise/IFirewallPolicy;->getRules(Landroid/app/enterprise/ContextInfo;I)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :goto_1
    move-object v3, v2

    .line 2920
    .restart local v3    # "retVal":Ljava/lang/Object;, "Ljava/util/List<Ljava/lang/String;>;"
    goto :goto_0

    .line 2917
    .end local v3    # "retVal":Ljava/lang/Object;, "Ljava/util/List<Ljava/lang/String;>;"
    :catch_0
    move-exception v0

    .line 2918
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "RemoteException on getIptablesRedirectExceptions"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public getIptablesRerouteRules()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 915
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerFirewallPolicy.getIptablesRerouteRules"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 917
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 918
    .local v2, "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->getFirewallService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v1

    .line 919
    .local v1, "firewallService":Landroid/app/enterprise/IFirewallPolicy;
    if-nez v1, :cond_0

    .line 920
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "FirewallService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, v2

    .line 929
    .end local v2    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local v3, "retVal":Ljava/lang/Object;, "Ljava/util/List<Ljava/lang/String;>;"
    :goto_0
    return-object v3

    .line 925
    .end local v3    # "retVal":Ljava/lang/Object;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v2    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v5, 0x0

    invoke-interface {v1, v4, v5}, Landroid/app/enterprise/IFirewallPolicy;->getRules(Landroid/app/enterprise/ContextInfo;I)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :goto_1
    move-object v3, v2

    .line 929
    .restart local v3    # "retVal":Ljava/lang/Object;, "Ljava/util/List<Ljava/lang/String;>;"
    goto :goto_0

    .line 926
    .end local v3    # "retVal":Ljava/lang/Object;, "Ljava/util/List<Ljava/lang/String;>;"
    :catch_0
    move-exception v0

    .line 927
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "RemoteException..."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public getIptablesRules()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1076
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerFirewallPolicy.getIptablesRules"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1078
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1079
    .local v2, "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->getFirewallService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v1

    .line 1080
    .local v1, "firewallService":Landroid/app/enterprise/IFirewallPolicy;
    if-nez v1, :cond_0

    .line 1081
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "FirewallService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, v2

    .line 1090
    .end local v2    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local v3, "retVal":Ljava/lang/Object;, "Ljava/util/List<Ljava/lang/String;>;"
    :goto_0
    return-object v3

    .line 1086
    .end local v3    # "retVal":Ljava/lang/Object;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v2    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v4}, Landroid/app/enterprise/IFirewallPolicy;->getAllRulesForUid(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :goto_1
    move-object v3, v2

    .line 1090
    .restart local v3    # "retVal":Ljava/lang/Object;, "Ljava/util/List<Ljava/lang/String;>;"
    goto :goto_0

    .line 1087
    .end local v3    # "retVal":Ljava/lang/Object;, "Ljava/util/List<Ljava/lang/String;>;"
    :catch_0
    move-exception v0

    .line 1088
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "RemoteException..."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public getNetworkForMarket()I
    .locals 6

    .prologue
    .line 2537
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerFirewallPolicy.getNetworkForMarket"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2539
    const/4 v2, -0x1

    .line 2540
    .local v2, "retVal":I
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->getFirewallService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v1

    .line 2541
    .local v1, "firewallService":Landroid/app/enterprise/IFirewallPolicy;
    if-nez v1, :cond_0

    .line 2542
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "FirewallService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 2551
    .end local v2    # "retVal":I
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 2547
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":I
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v5, 0x1

    invoke-interface {v1, v4, v5}, Landroid/app/enterprise/IFirewallPolicy;->getNetworkForMarket(Landroid/app/enterprise/ContextInfo;Z)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 2551
    .end local v2    # "retVal":I
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 2548
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":I
    :catch_0
    move-exception v0

    .line 2549
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed getNetworkForMarket!!!"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public getURLFilterEnabled()Z
    .locals 7

    .prologue
    .line 2113
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerFirewallPolicy.getURLFilterEnabled"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2115
    const/4 v2, 0x0

    .line 2116
    .local v2, "retVal":Z
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->getFirewallService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v1

    .line 2117
    .local v1, "firewallService":Landroid/app/enterprise/IFirewallPolicy;
    if-nez v1, :cond_0

    .line 2118
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "FirewallService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 2128
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 2123
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "getURLFilterEnabled"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2124
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v5, 0x1

    const/4 v6, 0x1

    invoke-interface {v1, v4, v5, v6}, Landroid/app/enterprise/IFirewallPolicy;->getURLFilterEnabled(Landroid/app/enterprise/ContextInfo;ZZ)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 2128
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 2125
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 2126
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "RemoteException..."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public getURLFilterList()Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2228
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerFirewallPolicy.getURLFilterList"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2230
    const/4 v2, 0x0

    .line 2231
    .local v2, "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->getFirewallService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v1

    .line 2232
    .local v1, "firewallService":Landroid/app/enterprise/IFirewallPolicy;
    if-nez v1, :cond_0

    .line 2233
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "FirewallService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, v2

    .line 2243
    .end local v2    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local v3, "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :goto_0
    return-object v3

    .line 2238
    .end local v3    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v2    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_0
    :try_start_0
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "getURLFilterList"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2239
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v5, 0x1

    const/4 v6, 0x1

    invoke-interface {v1, v4, v5, v6}, Landroid/app/enterprise/IFirewallPolicy;->getURLFilterList(Landroid/app/enterprise/ContextInfo;ZZ)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :goto_1
    move-object v3, v2

    .line 2243
    .end local v2    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v3    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    goto :goto_0

    .line 2240
    .end local v3    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v2    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :catch_0
    move-exception v0

    .line 2241
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "RemoteException..."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public getURLFilterReport()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2427
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerFirewallPolicy.getURLFilterReport"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2429
    const/4 v2, 0x0

    .line 2430
    .local v2, "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->getFirewallService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v1

    .line 2431
    .local v1, "firewallService":Landroid/app/enterprise/IFirewallPolicy;
    if-nez v1, :cond_0

    .line 2432
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "FirewallService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, v2

    .line 2442
    .end local v2    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local v3, "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :goto_0
    return-object v3

    .line 2437
    .end local v3    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v2    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_0
    :try_start_0
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "getURLFilterReport"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2438
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v4}, Landroid/app/enterprise/IFirewallPolicy;->getURLFilterReport(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :goto_1
    move-object v3, v2

    .line 2442
    .end local v2    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v3    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    goto :goto_0

    .line 2439
    .end local v3    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v2    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :catch_0
    move-exception v0

    .line 2440
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "RemoteException..."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public getURLFilterReportEnabled()Z
    .locals 7

    .prologue
    .line 2365
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerFirewallPolicy.getURLFilterReportEnabled"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2367
    const/4 v2, 0x0

    .line 2368
    .local v2, "retVal":Z
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->getFirewallService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v1

    .line 2369
    .local v1, "firewallService":Landroid/app/enterprise/IFirewallPolicy;
    if-nez v1, :cond_0

    .line 2370
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "FirewallService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 2380
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 2375
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "getURLFilterReportEnabled"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2376
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v5, 0x1

    const/4 v6, 0x1

    invoke-interface {v1, v4, v5, v6}, Landroid/app/enterprise/IFirewallPolicy;->getURLFilterReportEnabled(Landroid/app/enterprise/ContextInfo;ZZ)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 2380
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 2377
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 2378
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "RemoteException..."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public isUrlBlocked(Ljava/lang/String;)Z
    .locals 6
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 2254
    const/4 v2, 0x0

    .line 2255
    .local v2, "retVal":Z
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->getFirewallService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v1

    .line 2256
    .local v1, "firewallService":Landroid/app/enterprise/IFirewallPolicy;
    if-nez v1, :cond_0

    .line 2257
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "FirewallService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 2267
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 2263
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v4, p1}, Landroid/app/enterprise/IFirewallPolicy;->isUrlBlocked(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 2267
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 2264
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 2265
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "RemoteException..."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public listIptablesRules()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1994
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerFirewallPolicy.listIptablesRules"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1996
    const/4 v2, 0x0

    .line 1997
    .local v2, "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->getFirewallService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v1

    .line 1998
    .local v1, "firewallService":Landroid/app/enterprise/IFirewallPolicy;
    if-nez v1, :cond_0

    .line 1999
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "FirewallService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, v2

    .line 2008
    .end local v2    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local v3, "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :goto_0
    return-object v3

    .line 2004
    .end local v3    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v2    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v4}, Landroid/app/enterprise/IFirewallPolicy;->listIptablesRules(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :goto_1
    move-object v3, v2

    .line 2008
    .end local v2    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v3    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    goto :goto_0

    .line 2005
    .end local v3    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v2    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :catch_0
    move-exception v0

    .line 2006
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "RemoteException..."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public removeIptablesAllowRules(Ljava/util/List;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 514
    .local p1, "rulesList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerFirewallPolicy.removeIptablesAllowRules"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 515
    const-string v4, "Application"

    const-string v5, "removeIptablesAllowRules..."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 517
    const/4 v2, 0x0

    .line 518
    .local v2, "retVal":Z
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->getFirewallService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v1

    .line 519
    .local v1, "firewallService":Landroid/app/enterprise/IFirewallPolicy;
    if-nez v1, :cond_0

    .line 520
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "FirewallService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 532
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 524
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->checkEmptyList(Ljava/util/List;)Z

    move-result v4

    if-eqz v4, :cond_1

    move v3, v2

    .line 525
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 528
    .end local v3    # "retVal":I
    :cond_1
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v5, 0x1

    invoke-interface {v1, v4, p1, v5}, Landroid/app/enterprise/IFirewallPolicy;->removeRules(Landroid/app/enterprise/ContextInfo;Ljava/util/List;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 532
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 529
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 530
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "RemoteException..."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public removeIptablesDenyRules(Ljava/util/List;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 606
    .local p1, "rulesList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerFirewallPolicy.removeIptablesDenyRules"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 608
    const/4 v2, 0x0

    .line 609
    .local v2, "retVal":Z
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->getFirewallService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v1

    .line 610
    .local v1, "firewallService":Landroid/app/enterprise/IFirewallPolicy;
    if-nez v1, :cond_0

    .line 611
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "FirewallService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 623
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 615
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->checkEmptyList(Ljava/util/List;)Z

    move-result v4

    if-eqz v4, :cond_1

    move v3, v2

    .line 616
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 619
    .end local v3    # "retVal":I
    :cond_1
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v5, 0x2

    invoke-interface {v1, v4, p1, v5}, Landroid/app/enterprise/IFirewallPolicy;->removeRules(Landroid/app/enterprise/ContextInfo;Ljava/util/List;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 623
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 620
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 621
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "RemoteException..."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public removeIptablesRedirectExceptionsRules(Ljava/util/List;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 2833
    .local p1, "rulesList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerFirewallPolicy.removeIptablesRedirectExceptionsRules"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2835
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "removeIptablesRedirectExceptions"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2837
    const/4 v2, 0x0

    .line 2838
    .local v2, "retVal":Z
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->getFirewallService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v1

    .line 2839
    .local v1, "firewallService":Landroid/app/enterprise/IFirewallPolicy;
    if-nez v1, :cond_0

    .line 2840
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "FirewallService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 2849
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 2845
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v5, 0x7

    invoke-interface {v1, v4, p1, v5}, Landroid/app/enterprise/IFirewallPolicy;->removeRules(Landroid/app/enterprise/ContextInfo;Ljava/util/List;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 2849
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 2846
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 2847
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "RemoteException on removeIptablesRedirectExceptionsRules"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public removeIptablesRerouteRules(Ljava/util/List;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 697
    .local p1, "rulesList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerFirewallPolicy.removeIptablesRerouteRules"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 699
    const/4 v2, 0x0

    .line 700
    .local v2, "retVal":Z
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->getFirewallService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v1

    .line 701
    .local v1, "firewallService":Landroid/app/enterprise/IFirewallPolicy;
    if-nez v1, :cond_0

    .line 702
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "FirewallService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 714
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 706
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->checkEmptyList(Ljava/util/List;)Z

    move-result v4

    if-eqz v4, :cond_1

    move v3, v2

    .line 707
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 710
    .end local v3    # "retVal":I
    :cond_1
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v5, 0x0

    invoke-interface {v1, v4, p1, v5}, Landroid/app/enterprise/IFirewallPolicy;->removeRules(Landroid/app/enterprise/ContextInfo;Ljava/util/List;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 714
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 711
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 712
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "RemoteException..."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public removeIptablesRules()Z
    .locals 6

    .prologue
    .line 1727
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerFirewallPolicy.removeIptablesRules"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1729
    const/4 v2, 0x0

    .line 1730
    .local v2, "retVal":Z
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->getFirewallService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v1

    .line 1731
    .local v1, "firewallService":Landroid/app/enterprise/IFirewallPolicy;
    if-nez v1, :cond_0

    .line 1732
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "FirewallService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 1741
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 1737
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v4}, Landroid/app/enterprise/IFirewallPolicy;->cleanAllRules(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 1741
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 1738
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 1739
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "RemoteException..."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public setIptablesAllowRules(Ljava/util/List;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1289
    .local p1, "rulesList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerFirewallPolicy.setIptablesAllowRules"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1291
    const/4 v2, 0x0

    .line 1292
    .local v2, "retVal":Z
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->getFirewallService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v1

    .line 1293
    .local v1, "firewallService":Landroid/app/enterprise/IFirewallPolicy;
    if-nez v1, :cond_0

    .line 1294
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "FirewallService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 1306
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 1298
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->checkEmptyList(Ljava/util/List;)Z

    move-result v4

    if-eqz v4, :cond_1

    move v3, v2

    .line 1299
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 1302
    .end local v3    # "retVal":I
    :cond_1
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v5, 0x1

    invoke-interface {v1, v4, p1, v5}, Landroid/app/enterprise/IFirewallPolicy;->setRules(Landroid/app/enterprise/ContextInfo;Ljava/util/List;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 1306
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 1303
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 1304
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "RemoteException..."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public setIptablesDenyRules(Ljava/util/List;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1387
    .local p1, "rulesList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerFirewallPolicy.setIptablesDenyRules"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1389
    const/4 v2, 0x0

    .line 1390
    .local v2, "retVal":Z
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->getFirewallService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v1

    .line 1391
    .local v1, "firewallService":Landroid/app/enterprise/IFirewallPolicy;
    if-nez v1, :cond_0

    .line 1392
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "FirewallService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 1404
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 1396
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->checkEmptyList(Ljava/util/List;)Z

    move-result v4

    if-eqz v4, :cond_1

    move v3, v2

    .line 1397
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 1400
    .end local v3    # "retVal":I
    :cond_1
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v5, 0x2

    invoke-interface {v1, v4, p1, v5}, Landroid/app/enterprise/IFirewallPolicy;->setRules(Landroid/app/enterprise/ContextInfo;Ljava/util/List;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 1404
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 1401
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 1402
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "RemoteException..."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public setIptablesOption(Z)Z
    .locals 6
    .param p1, "status"    # Z

    .prologue
    .line 1823
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerFirewallPolicy.setIptablesOption"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1825
    const/4 v2, 0x0

    .line 1826
    .local v2, "retVal":Z
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->getFirewallService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v1

    .line 1827
    .local v1, "firewallService":Landroid/app/enterprise/IFirewallPolicy;
    if-nez v1, :cond_0

    .line 1828
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "FirewallService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 1838
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 1834
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v4, p1}, Landroid/app/enterprise/IFirewallPolicy;->enableRules(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 1838
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 1835
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 1836
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "RemoteException..."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public setIptablesProxyOption(Z)Z
    .locals 6
    .param p1, "status"    # Z

    .prologue
    .line 1869
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerFirewallPolicy.setIptablesProxyOption"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1871
    const/4 v2, 0x0

    .line 1872
    .local v2, "retVal":Z
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->getFirewallService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v1

    .line 1873
    .local v1, "firewallService":Landroid/app/enterprise/IFirewallPolicy;
    if-nez v1, :cond_0

    .line 1874
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "FirewallService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 1884
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 1880
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v4, p1}, Landroid/app/enterprise/IFirewallPolicy;->enableProxy(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 1884
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 1881
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 1882
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "RemoteException..."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public setIptablesProxyRules(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 7
    .param p1, "ip"    # Ljava/lang/String;
    .param p2, "port"    # Ljava/lang/String;

    .prologue
    .line 1548
    iget-object v5, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v6, "ContainerFirewallPolicy.setIptablesProxyRules"

    invoke-static {v5, v6}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1550
    const/4 v2, 0x0

    .line 1551
    .local v2, "retVal":Z
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->getFirewallService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v1

    .line 1552
    .local v1, "firewallService":Landroid/app/enterprise/IFirewallPolicy;
    if-nez v1, :cond_0

    .line 1553
    sget-object v5, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v6, "FirewallService is not yet ready!!!"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 1564
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 1558
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1559
    .local v4, "rulesList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ":"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1560
    iget-object v5, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v5, p1, p2}, Landroid/app/enterprise/IFirewallPolicy;->setProxyRules(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .end local v4    # "rulesList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :goto_1
    move v3, v2

    .line 1564
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 1561
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 1562
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v5, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v6, "RemoteException..."

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public setIptablesRedirectExceptionsRules(Ljava/util/List;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 2648
    .local p1, "rulesList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerFirewallPolicy.setIptablesRedirectExceptionsRules"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2649
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "setIptablesRedirectExceptions"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2651
    const/4 v2, 0x0

    .line 2652
    .local v2, "retVal":Z
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->getFirewallService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v1

    .line 2653
    .local v1, "firewallService":Landroid/app/enterprise/IFirewallPolicy;
    if-nez v1, :cond_0

    .line 2654
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "FirewallService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 2663
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 2659
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v5, 0x7

    invoke-interface {v1, v4, p1, v5}, Landroid/app/enterprise/IFirewallPolicy;->setRules(Landroid/app/enterprise/ContextInfo;Ljava/util/List;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 2663
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 2660
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 2661
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "RemoteException on setIptablesRedirectExceptionsRules"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public setIptablesRerouteRules(Ljava/util/List;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1482
    .local p1, "rulesList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerFirewallPolicy.setIptablesRerouteRules"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1484
    const/4 v2, 0x0

    .line 1485
    .local v2, "retVal":Z
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->getFirewallService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v1

    .line 1486
    .local v1, "firewallService":Landroid/app/enterprise/IFirewallPolicy;
    if-nez v1, :cond_0

    .line 1487
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "FirewallService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 1499
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 1491
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->checkEmptyList(Ljava/util/List;)Z

    move-result v4

    if-eqz v4, :cond_1

    move v3, v2

    .line 1492
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 1495
    .end local v3    # "retVal":I
    :cond_1
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v5, 0x0

    invoke-interface {v1, v4, p1, v5}, Landroid/app/enterprise/IFirewallPolicy;->setRules(Landroid/app/enterprise/ContextInfo;Ljava/util/List;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 1499
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 1496
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 1497
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "RemoteException..."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public setNetworkForMarket(I)Z
    .locals 6
    .param p1, "networkType"    # I

    .prologue
    .line 2482
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerFirewallPolicy.setNetworkForMarket"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2484
    const/4 v2, 0x0

    .line 2485
    .local v2, "retVal":Z
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->getFirewallService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v1

    .line 2486
    .local v1, "firewallService":Landroid/app/enterprise/IFirewallPolicy;
    if-nez v1, :cond_0

    .line 2487
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "FirewallService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 2496
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 2492
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v4, p1}, Landroid/app/enterprise/IFirewallPolicy;->setNetworkForMarket(Landroid/app/enterprise/ContextInfo;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 2496
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 2493
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 2494
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed setNetworkForMarket!!!"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public setURLFilterEnabled(Z)Z
    .locals 7
    .param p1, "enable"    # Z

    .prologue
    .line 2060
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerFirewallPolicy.setURLFilterEnabled"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2062
    const/4 v2, 0x0

    .line 2063
    .local v2, "retVal":Z
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->getFirewallService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v1

    .line 2064
    .local v1, "firewallService":Landroid/app/enterprise/IFirewallPolicy;
    if-nez v1, :cond_0

    .line 2065
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "FirewallService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 2075
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 2070
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setURLFilterEnabled state = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2071
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v4, p1}, Landroid/app/enterprise/IFirewallPolicy;->setURLFilterEnabled(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 2075
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 2072
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 2073
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "RemoteException..."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public setURLFilterList(Ljava/util/List;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 2174
    .local p1, "urls":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerFirewallPolicy.setURLFilterList"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2176
    const/4 v2, 0x0

    .line 2177
    .local v2, "retVal":Z
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->getFirewallService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v1

    .line 2178
    .local v1, "firewallService":Landroid/app/enterprise/IFirewallPolicy;
    if-nez v1, :cond_0

    .line 2179
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "FirewallService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 2189
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 2184
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setURLFilterList urls = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2185
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v4, p1}, Landroid/app/enterprise/IFirewallPolicy;->setURLFilterList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 2189
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 2186
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 2187
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "RemoteException..."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public setURLFilterReportEnabled(Z)Z
    .locals 7
    .param p1, "enable"    # Z

    .prologue
    .line 2312
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerFirewallPolicy.setURLFilterReportEnabled"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2314
    const/4 v2, 0x0

    .line 2315
    .local v2, "retVal":Z
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->getFirewallService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v1

    .line 2316
    .local v1, "firewallService":Landroid/app/enterprise/IFirewallPolicy;
    if-nez v1, :cond_0

    .line 2317
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "FirewallService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 2327
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 2322
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setURLFilterReportEnabled state = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2323
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v4, p1}, Landroid/app/enterprise/IFirewallPolicy;->setURLFilterReportEnabled(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 2327
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 2324
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 2325
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;->TAG:Ljava/lang/String;

    const-string v5, "RemoteException..."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
