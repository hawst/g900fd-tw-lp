.class public abstract Lcom/sec/enterprise/knox/IEnterpriseISLPolicy$Stub;
.super Landroid/os/Binder;
.source "IEnterpriseISLPolicy.java"

# interfaces
.implements Lcom/sec/enterprise/knox/IEnterpriseISLPolicy;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/enterprise/knox/IEnterpriseISLPolicy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/enterprise/knox/IEnterpriseISLPolicy$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.sec.enterprise.knox.IEnterpriseISLPolicy"

.field static final TRANSACTION_clearBaseline:I = 0x4

.field static final TRANSACTION_establishBaselineScan:I = 0x2

.field static final TRANSACTION_getISAList:I = 0xb

.field static final TRANSACTION_isISAReady:I = 0xa

.field static final TRANSACTION_performPreBaselineScan:I = 0x1

.field static final TRANSACTION_performScanNow:I = 0x3

.field static final TRANSACTION_putPackageToBaseline:I = 0x5

.field static final TRANSACTION_removePackageFromBaseline:I = 0x6

.field static final TRANSACTION_requestBindISA:I = 0x9

.field static final TRANSACTION_setIntegrityResultSubscriber:I = 0x8

.field static final TRANSACTION_startRuntimeWatch:I = 0xc

.field static final TRANSACTION_stopRuntimeWatch:I = 0xd

.field static final TRANSACTION_updatePlatformBaseline:I = 0x7


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 19
    const-string v0, "com.sec.enterprise.knox.IEnterpriseISLPolicy"

    invoke-virtual {p0, p0, v0}, Lcom/sec/enterprise/knox/IEnterpriseISLPolicy$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 20
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/sec/enterprise/knox/IEnterpriseISLPolicy;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 27
    if-nez p0, :cond_0

    .line 28
    const/4 v0, 0x0

    .line 34
    :goto_0
    return-object v0

    .line 30
    :cond_0
    const-string v1, "com.sec.enterprise.knox.IEnterpriseISLPolicy"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 31
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/sec/enterprise/knox/IEnterpriseISLPolicy;

    if-eqz v1, :cond_1

    .line 32
    check-cast v0, Lcom/sec/enterprise/knox/IEnterpriseISLPolicy;

    goto :goto_0

    .line 34
    :cond_1
    new-instance v0, Lcom/sec/enterprise/knox/IEnterpriseISLPolicy$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/sec/enterprise/knox/IEnterpriseISLPolicy$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 38
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 8
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x1

    .line 42
    sparse-switch p1, :sswitch_data_0

    .line 258
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v6

    :goto_0
    return v6

    .line 46
    :sswitch_0
    const-string v5, "com.sec.enterprise.knox.IEnterpriseISLPolicy"

    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 51
    :sswitch_1
    const-string v7, "com.sec.enterprise.knox.IEnterpriseISLPolicy"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 53
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_1

    .line 54
    sget-object v7, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 59
    .local v0, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_1
    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/IEnterpriseISLPolicy$Stub;->performPreBaselineScan(Landroid/app/enterprise/ContextInfo;)Z

    move-result v3

    .line 60
    .local v3, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 61
    if-eqz v3, :cond_0

    move v5, v6

    :cond_0
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 57
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v3    # "_result":Z
    :cond_1
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_1

    .line 66
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_2
    const-string v7, "com.sec.enterprise.knox.IEnterpriseISLPolicy"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 68
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_3

    .line 69
    sget-object v7, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 75
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_2
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_4

    move v1, v6

    .line 76
    .local v1, "_arg1":Z
    :goto_3
    invoke-virtual {p0, v0, v1}, Lcom/sec/enterprise/knox/IEnterpriseISLPolicy$Stub;->establishBaselineScan(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v3

    .line 77
    .restart local v3    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 78
    if-eqz v3, :cond_2

    move v5, v6

    :cond_2
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 72
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v3    # "_result":Z
    :cond_3
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_2

    :cond_4
    move v1, v5

    .line 75
    goto :goto_3

    .line 83
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_3
    const-string v7, "com.sec.enterprise.knox.IEnterpriseISLPolicy"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 85
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_6

    .line 86
    sget-object v7, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 92
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_4
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 94
    .local v1, "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v2

    .line 95
    .local v2, "_arg2":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/enterprise/knox/IEnterpriseISLPolicy$Stub;->performScanNow(Landroid/app/enterprise/ContextInfo;ILjava/util/List;)Z

    move-result v3

    .line 96
    .restart local v3    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 97
    if-eqz v3, :cond_5

    move v5, v6

    :cond_5
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 89
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":I
    .end local v2    # "_arg2":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v3    # "_result":Z
    :cond_6
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_4

    .line 102
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_4
    const-string v7, "com.sec.enterprise.knox.IEnterpriseISLPolicy"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 104
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_8

    .line 105
    sget-object v7, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 110
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_5
    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/IEnterpriseISLPolicy$Stub;->clearBaseline(Landroid/app/enterprise/ContextInfo;)Z

    move-result v3

    .line 111
    .restart local v3    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 112
    if-eqz v3, :cond_7

    move v5, v6

    :cond_7
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 108
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v3    # "_result":Z
    :cond_8
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_5

    .line 117
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_5
    const-string v7, "com.sec.enterprise.knox.IEnterpriseISLPolicy"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 119
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_a

    .line 120
    sget-object v7, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 126
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_6
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 127
    .local v1, "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v1}, Lcom/sec/enterprise/knox/IEnterpriseISLPolicy$Stub;->putPackageToBaseline(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v3

    .line 128
    .restart local v3    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 129
    if-eqz v3, :cond_9

    move v5, v6

    :cond_9
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 123
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v3    # "_result":Z
    :cond_a
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_6

    .line 134
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_6
    const-string v7, "com.sec.enterprise.knox.IEnterpriseISLPolicy"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 136
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_c

    .line 137
    sget-object v7, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 143
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_7
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 144
    .restart local v1    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v1}, Lcom/sec/enterprise/knox/IEnterpriseISLPolicy$Stub;->removePackageFromBaseline(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v3

    .line 145
    .restart local v3    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 146
    if-eqz v3, :cond_b

    move v5, v6

    :cond_b
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 140
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v3    # "_result":Z
    :cond_c
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_7

    .line 151
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_7
    const-string v7, "com.sec.enterprise.knox.IEnterpriseISLPolicy"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 153
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_e

    .line 154
    sget-object v7, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 159
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_8
    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/IEnterpriseISLPolicy$Stub;->updatePlatformBaseline(Landroid/app/enterprise/ContextInfo;)Z

    move-result v3

    .line 160
    .restart local v3    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 161
    if-eqz v3, :cond_d

    move v5, v6

    :cond_d
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 157
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v3    # "_result":Z
    :cond_e
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_8

    .line 166
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_8
    const-string v5, "com.sec.enterprise.knox.IEnterpriseISLPolicy"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 168
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-eqz v5, :cond_f

    .line 169
    sget-object v5, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 175
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_9
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/enterprise/knox/IIntegrityResultSubscriber$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/enterprise/knox/IIntegrityResultSubscriber;

    move-result-object v1

    .line 176
    .local v1, "_arg1":Lcom/sec/enterprise/knox/IIntegrityResultSubscriber;
    invoke-virtual {p0, v0, v1}, Lcom/sec/enterprise/knox/IEnterpriseISLPolicy$Stub;->setIntegrityResultSubscriber(Landroid/app/enterprise/ContextInfo;Lcom/sec/enterprise/knox/IIntegrityResultSubscriber;)V

    .line 177
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 172
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Lcom/sec/enterprise/knox/IIntegrityResultSubscriber;
    :cond_f
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_9

    .line 182
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_9
    const-string v7, "com.sec.enterprise.knox.IEnterpriseISLPolicy"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 184
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_11

    .line 185
    sget-object v7, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 191
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_a
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 192
    .local v1, "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v1}, Lcom/sec/enterprise/knox/IEnterpriseISLPolicy$Stub;->requestBindISA(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v3

    .line 193
    .restart local v3    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 194
    if-eqz v3, :cond_10

    move v5, v6

    :cond_10
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 188
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v3    # "_result":Z
    :cond_11
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_a

    .line 199
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_a
    const-string v7, "com.sec.enterprise.knox.IEnterpriseISLPolicy"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 201
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_13

    .line 202
    sget-object v7, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 207
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_b
    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/IEnterpriseISLPolicy$Stub;->isISAReady(Landroid/app/enterprise/ContextInfo;)Z

    move-result v3

    .line 208
    .restart local v3    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 209
    if-eqz v3, :cond_12

    move v5, v6

    :cond_12
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 205
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v3    # "_result":Z
    :cond_13
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_b

    .line 214
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_b
    const-string v5, "com.sec.enterprise.knox.IEnterpriseISLPolicy"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 216
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-eqz v5, :cond_14

    .line 217
    sget-object v5, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 222
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_c
    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/IEnterpriseISLPolicy$Stub;->getISAList(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;

    move-result-object v4

    .line 223
    .local v4, "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 224
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    goto/16 :goto_0

    .line 220
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_14
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_c

    .line 229
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_c
    const-string v7, "com.sec.enterprise.knox.IEnterpriseISLPolicy"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 231
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_16

    .line 232
    sget-object v7, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 237
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_d
    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/IEnterpriseISLPolicy$Stub;->startRuntimeWatch(Landroid/app/enterprise/ContextInfo;)Z

    move-result v3

    .line 238
    .restart local v3    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 239
    if-eqz v3, :cond_15

    move v5, v6

    :cond_15
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 235
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v3    # "_result":Z
    :cond_16
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_d

    .line 244
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_d
    const-string v7, "com.sec.enterprise.knox.IEnterpriseISLPolicy"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 246
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_18

    .line 247
    sget-object v7, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 252
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_e
    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/IEnterpriseISLPolicy$Stub;->stopRuntimeWatch(Landroid/app/enterprise/ContextInfo;)Z

    move-result v3

    .line 253
    .restart local v3    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 254
    if-eqz v3, :cond_17

    move v5, v6

    :cond_17
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 250
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v3    # "_result":Z
    :cond_18
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_e

    .line 42
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
