.class public Lcom/sec/enterprise/knox/seams/SEAMS;
.super Ljava/lang/Object;
.source "SEAMS.java"


# static fields
.field public static final DEBUG:Z = false

.field public static final ERROR_ALREADY_CONTAINER_APP:I = -0x9

.field public static final ERROR_CERTS_NOT_MATCHED:I = -0xd

.field public static final ERROR_CONTAINER_COUNTS_OVERFLOW:I = -0x7

.field public static final ERROR_CONTAINER_ID_MISMATCH:I = -0xc

.field public static final ERROR_CONTAINER_NOT_EMPTY:I = -0xb

.field public static final ERROR_NOT_SUPPORTED:I = -0x3

.field public static final ERROR_NO_CERTS:I = -0xe

.field public static final FALSE:I = 0x0

.field public static final GENERIC_SECURED_APPTYPE:I = 0x3

.field public static final GENERIC_TRUSTED_APPTYPE:I = 0x4

.field public static final GET_SERVICE_ERROR:I = -0xa

.field public static final NOT_INSTALLED:I = -0x4

.field public static final POLICY_FAILED:I = -0x1

.field public static final POLICY_OK:I = 0x0

.field public static final POLICY_REFUSED:I = -0x2

.field public static final RUNNING:I = -0x8

.field public static final SET_DEFAULT_MASK:I = 0x0

.field private static STANDARD_TRANSFER_WINDOW:I = 0x0

.field private static final TAG:Ljava/lang/String; = "SEAMS"

.field public static final TRUE:I = 0x1

.field private static largeTransferWindow:[B

.field private static mSEAMS:Lcom/sec/enterprise/knox/seams/SEAMS;

.field private static final mSync:Ljava/lang/Object;

.field private static policyUpdateLock:Ljava/lang/Object;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mContextInfo:Landroid/app/enterprise/ContextInfo;

.field private mSEAMService:Lcom/sec/enterprise/knox/seams/ISEAMS;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 197
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/sec/enterprise/knox/seams/SEAMS;->mSync:Ljava/lang/Object;

    .line 200
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/sec/enterprise/knox/seams/SEAMS;->policyUpdateLock:Ljava/lang/Object;

    .line 203
    const v0, 0x3e800

    sput v0, Lcom/sec/enterprise/knox/seams/SEAMS;->STANDARD_TRANSFER_WINDOW:I

    .line 205
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/enterprise/knox/seams/SEAMS;->largeTransferWindow:[B

    return-void
.end method

.method private constructor <init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V
    .locals 0
    .param p1, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 213
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 214
    iput-object p1, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    .line 215
    iput-object p2, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mContext:Landroid/content/Context;

    .line 216
    return-void
.end method

.method public static createInstance(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)Lcom/sec/enterprise/knox/seams/SEAMS;
    .locals 2
    .param p0, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 276
    new-instance v0, Lcom/sec/enterprise/knox/seams/SEAMS;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/enterprise/knox/seams/SEAMS;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    return-object v0
.end method

.method public static getInstance(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)Lcom/sec/enterprise/knox/seams/SEAMS;
    .locals 3
    .param p0, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 264
    sget-object v1, Lcom/sec/enterprise/knox/seams/SEAMS;->mSync:Ljava/lang/Object;

    monitor-enter v1

    .line 265
    :try_start_0
    sget-object v0, Lcom/sec/enterprise/knox/seams/SEAMS;->mSEAMS:Lcom/sec/enterprise/knox/seams/SEAMS;

    if-nez v0, :cond_0

    .line 266
    new-instance v0, Lcom/sec/enterprise/knox/seams/SEAMS;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, p0, v2}, Lcom/sec/enterprise/knox/seams/SEAMS;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    sput-object v0, Lcom/sec/enterprise/knox/seams/SEAMS;->mSEAMS:Lcom/sec/enterprise/knox/seams/SEAMS;

    .line 268
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/seams/SEAMS;->mSEAMS:Lcom/sec/enterprise/knox/seams/SEAMS;

    monitor-exit v1

    return-object v0

    .line 269
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/sec/enterprise/knox/seams/SEAMS;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 246
    if-nez p0, :cond_0

    .line 247
    const/4 v1, 0x0

    .line 255
    :goto_0
    return-object v1

    .line 250
    :cond_0
    sget-object v2, Lcom/sec/enterprise/knox/seams/SEAMS;->mSync:Ljava/lang/Object;

    monitor-enter v2

    .line 251
    :try_start_0
    sget-object v1, Lcom/sec/enterprise/knox/seams/SEAMS;->mSEAMS:Lcom/sec/enterprise/knox/seams/SEAMS;

    if-nez v1, :cond_1

    .line 252
    new-instance v0, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    invoke-direct {v0, v1}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    .line 253
    .local v0, "cxtInfo":Landroid/app/enterprise/ContextInfo;
    new-instance v1, Lcom/sec/enterprise/knox/seams/SEAMS;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v1, v0, v3}, Lcom/sec/enterprise/knox/seams/SEAMS;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    sput-object v1, Lcom/sec/enterprise/knox/seams/SEAMS;->mSEAMS:Lcom/sec/enterprise/knox/seams/SEAMS;

    .line 255
    .end local v0    # "cxtInfo":Landroid/app/enterprise/ContextInfo;
    :cond_1
    sget-object v1, Lcom/sec/enterprise/knox/seams/SEAMS;->mSEAMS:Lcom/sec/enterprise/knox/seams/SEAMS;

    monitor-exit v2

    goto :goto_0

    .line 256
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private declared-synchronized getService()Lcom/sec/enterprise/knox/seams/ISEAMS;
    .locals 1

    .prologue
    .line 220
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mSEAMService:Lcom/sec/enterprise/knox/seams/ISEAMS;

    if-nez v0, :cond_0

    .line 221
    const-string v0, "SEAMService"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/seams/ISEAMS$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/enterprise/knox/seams/ISEAMS;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mSEAMService:Lcom/sec/enterprise/knox/seams/ISEAMS;

    .line 224
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mSEAMService:Lcom/sec/enterprise/knox/seams/ISEAMS;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 220
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public activateDomain()I
    .locals 3

    .prologue
    .line 320
    iget-object v1, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SEAMS.activateDomain"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 321
    invoke-direct {p0}, Lcom/sec/enterprise/knox/seams/SEAMS;->getService()Lcom/sec/enterprise/knox/seams/ISEAMS;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 323
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mSEAMService:Lcom/sec/enterprise/knox/seams/ISEAMS;

    invoke-interface {v1}, Lcom/sec/enterprise/knox/seams/ISEAMS;->activateDomain()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 328
    :goto_0
    return v1

    .line 324
    :catch_0
    move-exception v0

    .line 325
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "SEAMS"

    const-string v2, "Failed to get activation status of the agent"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 328
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public addAppToContainer(Ljava/lang/String;[Ljava/lang/String;II)I
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "certificate"    # [Ljava/lang/String;
    .param p3, "containerID"    # I
    .param p4, "appType"    # I

    .prologue
    .line 379
    iget-object v1, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SEAMS.addAppToContainer"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 380
    invoke-direct {p0}, Lcom/sec/enterprise/knox/seams/SEAMS;->getService()Lcom/sec/enterprise/knox/seams/ISEAMS;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 382
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mSEAMService:Lcom/sec/enterprise/knox/seams/ISEAMS;

    invoke-interface {v1, p1, p2, p3, p4}, Lcom/sec/enterprise/knox/seams/ISEAMS;->addAppToContainer(Ljava/lang/String;[Ljava/lang/String;II)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 388
    :goto_0
    return v1

    .line 384
    :catch_0
    move-exception v0

    .line 385
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "SEAMS"

    const-string v2, "Failed to add app to container"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 388
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public createSEContainer()I
    .locals 3

    .prologue
    .line 421
    iget-object v1, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SEAMS.createSEContainer"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 422
    invoke-direct {p0}, Lcom/sec/enterprise/knox/seams/SEAMS;->getService()Lcom/sec/enterprise/knox/seams/ISEAMS;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 424
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mSEAMService:Lcom/sec/enterprise/knox/seams/ISEAMS;

    invoke-interface {v1}, Lcom/sec/enterprise/knox/seams/ISEAMS;->createSEContainer()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 429
    :goto_0
    return v1

    .line 425
    :catch_0
    move-exception v0

    .line 426
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "SEAMS"

    const-string v2, "Failed to create SEContainer"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 429
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public deActivateDomain()I
    .locals 3

    .prologue
    .line 471
    iget-object v1, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SEAMS.deActivateDomain"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 472
    invoke-direct {p0}, Lcom/sec/enterprise/knox/seams/SEAMS;->getService()Lcom/sec/enterprise/knox/seams/ISEAMS;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 474
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mSEAMService:Lcom/sec/enterprise/knox/seams/ISEAMS;

    invoke-interface {v1}, Lcom/sec/enterprise/knox/seams/ISEAMS;->deActivateDomain()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 479
    :goto_0
    return v1

    .line 475
    :catch_0
    move-exception v0

    .line 476
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "SEAMS"

    const-string v2, "Failed to deActivateDomain"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 479
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public forceAuthorized(IILjava/lang/String;Ljava/lang/String;)I
    .locals 2
    .param p1, "pid"    # I
    .param p2, "uid"    # I
    .param p3, "service"    # Ljava/lang/String;
    .param p4, "method"    # Ljava/lang/String;

    .prologue
    .line 1433
    invoke-direct {p0}, Lcom/sec/enterprise/knox/seams/SEAMS;->getService()Lcom/sec/enterprise/knox/seams/ISEAMS;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1434
    const-string v0, "SEAMS"

    const-string v1, "Service is null"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1435
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    if-ne v0, p1, :cond_0

    .line 1437
    const/4 v0, 0x0

    .line 1443
    :goto_0
    return v0

    .line 1439
    :cond_0
    const-string v0, "SEAMS"

    const-string v1, "Process ID rejected."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1440
    const/4 v0, -0x1

    goto :goto_0

    .line 1443
    :cond_1
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/sec/enterprise/knox/seams/SEAMS;->isAuthorized(IILjava/lang/String;Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public getAMSLog()Ljava/lang/String;
    .locals 3

    .prologue
    .line 553
    iget-object v1, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SEAMS.getAMSLog"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 554
    invoke-direct {p0}, Lcom/sec/enterprise/knox/seams/SEAMS;->getService()Lcom/sec/enterprise/knox/seams/ISEAMS;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 556
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mSEAMService:Lcom/sec/enterprise/knox/seams/ISEAMS;

    iget-object v2, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Lcom/sec/enterprise/knox/seams/ISEAMS;->getAMSLog(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 561
    :goto_0
    return-object v1

    .line 557
    :catch_0
    move-exception v0

    .line 558
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "SEAMS"

    const-string v2, "Failed to get AMS Log"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 561
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getAMSLogLevel()I
    .locals 3

    .prologue
    .line 593
    iget-object v1, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SEAMS.getAMSLogLevel"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 594
    invoke-direct {p0}, Lcom/sec/enterprise/knox/seams/SEAMS;->getService()Lcom/sec/enterprise/knox/seams/ISEAMS;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 596
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mSEAMService:Lcom/sec/enterprise/knox/seams/ISEAMS;

    iget-object v2, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Lcom/sec/enterprise/knox/seams/ISEAMS;->getAMSLogLevel(Landroid/app/enterprise/ContextInfo;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 601
    :goto_0
    return v1

    .line 597
    :catch_0
    move-exception v0

    .line 598
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "SEAMS"

    const-string v2, "Failed to get AMS Log Level"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 601
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getAMSMode()I
    .locals 3

    .prologue
    .line 633
    iget-object v1, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SEAMS.getAMSMode"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 634
    invoke-direct {p0}, Lcom/sec/enterprise/knox/seams/SEAMS;->getService()Lcom/sec/enterprise/knox/seams/ISEAMS;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 636
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mSEAMService:Lcom/sec/enterprise/knox/seams/ISEAMS;

    iget-object v2, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Lcom/sec/enterprise/knox/seams/ISEAMS;->getAMSMode(Landroid/app/enterprise/ContextInfo;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 641
    :goto_0
    return v1

    .line 637
    :catch_0
    move-exception v0

    .line 638
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "SEAMS"

    const-string v2, "Failed to get AMS Mode"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 641
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getAVCLog()Ljava/lang/String;
    .locals 3

    .prologue
    .line 672
    iget-object v1, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SEAMS.getAVCLog"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 673
    invoke-direct {p0}, Lcom/sec/enterprise/knox/seams/SEAMS;->getService()Lcom/sec/enterprise/knox/seams/ISEAMS;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 675
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mSEAMService:Lcom/sec/enterprise/knox/seams/ISEAMS;

    iget-object v2, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Lcom/sec/enterprise/knox/seams/ISEAMS;->getAVCLog(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 680
    :goto_0
    return-object v1

    .line 676
    :catch_0
    move-exception v0

    .line 677
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "SEAMS"

    const-string v2, "Failed to get AVC Log"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 680
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getActivationStatus()I
    .locals 3

    .prologue
    .line 513
    iget-object v1, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SEAMS.getActivationStatus"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 514
    invoke-direct {p0}, Lcom/sec/enterprise/knox/seams/SEAMS;->getService()Lcom/sec/enterprise/knox/seams/ISEAMS;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 516
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mSEAMService:Lcom/sec/enterprise/knox/seams/ISEAMS;

    invoke-interface {v1}, Lcom/sec/enterprise/knox/seams/ISEAMS;->getActivationStatus()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 521
    :goto_0
    return v1

    .line 517
    :catch_0
    move-exception v0

    .line 518
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "SEAMS"

    const-string v2, "Failed to get activation status of the agent"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 521
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getDataType(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 794
    iget-object v1, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SEAMS.getDataType"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 795
    invoke-direct {p0}, Lcom/sec/enterprise/knox/seams/SEAMS;->getService()Lcom/sec/enterprise/knox/seams/ISEAMS;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 797
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mSEAMService:Lcom/sec/enterprise/knox/seams/ISEAMS;

    iget-object v2, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Binder;->getCallingUserHandle()Landroid/os/UserHandle;

    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    move-result v3

    invoke-interface {v1, v2, p1, v3}, Lcom/sec/enterprise/knox/seams/ISEAMS;->getDataType(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 803
    :goto_0
    return-object v1

    .line 799
    :catch_0
    move-exception v0

    .line 800
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "SEAMS"

    const-string v2, "Failed to get Datatype"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 803
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getDataType(Ljava/lang/String;I)Ljava/lang/String;
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "userId"    # I

    .prologue
    .line 838
    iget-object v1, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SEAMS.getDataType"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 839
    invoke-direct {p0}, Lcom/sec/enterprise/knox/seams/SEAMS;->getService()Lcom/sec/enterprise/knox/seams/ISEAMS;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 841
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mSEAMService:Lcom/sec/enterprise/knox/seams/ISEAMS;

    iget-object v2, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Lcom/sec/enterprise/knox/seams/ISEAMS;->getDataType(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 846
    :goto_0
    return-object v1

    .line 842
    :catch_0
    move-exception v0

    .line 843
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "SEAMS"

    const-string v2, "Failed to get Datatype"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 846
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getDomain(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 880
    iget-object v1, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SEAMS.getDomain"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 881
    invoke-direct {p0}, Lcom/sec/enterprise/knox/seams/SEAMS;->getService()Lcom/sec/enterprise/knox/seams/ISEAMS;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 883
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mSEAMService:Lcom/sec/enterprise/knox/seams/ISEAMS;

    iget-object v2, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Binder;->getCallingUserHandle()Landroid/os/UserHandle;

    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    move-result v3

    invoke-interface {v1, v2, p1, v3}, Lcom/sec/enterprise/knox/seams/ISEAMS;->getDomain(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 889
    :goto_0
    return-object v1

    .line 885
    :catch_0
    move-exception v0

    .line 886
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "SEAMS"

    const-string v2, "Failed to get Domain"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 889
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getDomain(Ljava/lang/String;I)Ljava/lang/String;
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "userId"    # I

    .prologue
    .line 923
    iget-object v1, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SEAMS.getDomain"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 924
    invoke-direct {p0}, Lcom/sec/enterprise/knox/seams/SEAMS;->getService()Lcom/sec/enterprise/knox/seams/ISEAMS;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 926
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mSEAMService:Lcom/sec/enterprise/knox/seams/ISEAMS;

    iget-object v2, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Lcom/sec/enterprise/knox/seams/ISEAMS;->getDomain(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 931
    :goto_0
    return-object v1

    .line 927
    :catch_0
    move-exception v0

    .line 928
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "SEAMS"

    const-string v2, "Failed to get Domain"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 931
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getPackageNamesFromSEContainer(II)[Ljava/lang/String;
    .locals 3
    .param p1, "containerID"    # I
    .param p2, "appType"    # I

    .prologue
    .line 964
    iget-object v1, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SEAMS.getPackageNamesfromSEContainer"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 965
    invoke-direct {p0}, Lcom/sec/enterprise/knox/seams/SEAMS;->getService()Lcom/sec/enterprise/knox/seams/ISEAMS;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 967
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mSEAMService:Lcom/sec/enterprise/knox/seams/ISEAMS;

    invoke-interface {v1, p1, p2}, Lcom/sec/enterprise/knox/seams/ISEAMS;->getPackageNamesFromSEContainer(II)[Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 972
    :goto_0
    return-object v1

    .line 968
    :catch_0
    move-exception v0

    .line 969
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "SEAMS"

    const-string v2, "Failed to get package names from SEcontainer ID"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 972
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getSEAMSLog()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1002
    iget-object v1, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SEAMS.getSEAMSLog"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1003
    invoke-direct {p0}, Lcom/sec/enterprise/knox/seams/SEAMS;->getService()Lcom/sec/enterprise/knox/seams/ISEAMS;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1005
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mSEAMService:Lcom/sec/enterprise/knox/seams/ISEAMS;

    iget-object v2, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Lcom/sec/enterprise/knox/seams/ISEAMS;->getSEAMSLog(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1010
    :goto_0
    return-object v1

    .line 1006
    :catch_0
    move-exception v0

    .line 1007
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "SEAMS"

    const-string v2, "Failed to get SEAMS Log"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1010
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getSEContainerIDs()[I
    .locals 3

    .prologue
    .line 752
    iget-object v1, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SEAMS.getSEContainerIDs"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 753
    invoke-direct {p0}, Lcom/sec/enterprise/knox/seams/SEAMS;->getService()Lcom/sec/enterprise/knox/seams/ISEAMS;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 755
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mSEAMService:Lcom/sec/enterprise/knox/seams/ISEAMS;

    invoke-interface {v1}, Lcom/sec/enterprise/knox/seams/ISEAMS;->getSEContainerIDs()[I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 760
    :goto_0
    return-object v1

    .line 756
    :catch_0
    move-exception v0

    .line 757
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "SEAMS"

    const-string v2, "Failed to get SEContainer IDs"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 760
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getSEContainerIDsFromPackageName(Ljava/lang/String;I)[I
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "appType"    # I

    .prologue
    .line 714
    iget-object v1, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SEAMS.getSEContainerIDFromPackageName"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 715
    invoke-direct {p0}, Lcom/sec/enterprise/knox/seams/SEAMS;->getService()Lcom/sec/enterprise/knox/seams/ISEAMS;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 717
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mSEAMService:Lcom/sec/enterprise/knox/seams/ISEAMS;

    invoke-interface {v1, p1, p2}, Lcom/sec/enterprise/knox/seams/ISEAMS;->getSEContainerIDsFromPackageName(Ljava/lang/String;I)[I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 722
    :goto_0
    return-object v1

    .line 718
    :catch_0
    move-exception v0

    .line 719
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "SEAMS"

    const-string v2, "Failed to get SE Container ID from Package Name"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 722
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getSELinuxMode()I
    .locals 3

    .prologue
    .line 1072
    iget-object v1, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SEAMS.getSELinuxMode"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1073
    invoke-direct {p0}, Lcom/sec/enterprise/knox/seams/SEAMS;->getService()Lcom/sec/enterprise/knox/seams/ISEAMS;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1075
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mSEAMService:Lcom/sec/enterprise/knox/seams/ISEAMS;

    iget-object v2, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Lcom/sec/enterprise/knox/seams/ISEAMS;->getSELinuxMode(Landroid/app/enterprise/ContextInfo;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1080
    :goto_0
    return v1

    .line 1076
    :catch_0
    move-exception v0

    .line 1077
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "SEAMS"

    const-string v2, "Failed to getSELinuxMode"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1080
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getSepolicyVersion()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1108
    iget-object v1, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SEAMS.getSepolicyVersion"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1109
    invoke-direct {p0}, Lcom/sec/enterprise/knox/seams/SEAMS;->getService()Lcom/sec/enterprise/knox/seams/ISEAMS;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1111
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mSEAMService:Lcom/sec/enterprise/knox/seams/ISEAMS;

    iget-object v2, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Lcom/sec/enterprise/knox/seams/ISEAMS;->getSepolicyVersion(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1116
    :goto_0
    return-object v1

    .line 1112
    :catch_0
    move-exception v0

    .line 1113
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "SEAMS"

    const-string v2, "Failed to getSepolicyVersion"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1116
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getSignatureFromCertificate([B)Ljava/lang/String;
    .locals 3
    .param p1, "certificate"    # [B

    .prologue
    .line 1239
    iget-object v1, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SEAMS.getSignatureFromCertificate"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1240
    invoke-direct {p0}, Lcom/sec/enterprise/knox/seams/SEAMS;->getService()Lcom/sec/enterprise/knox/seams/ISEAMS;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1242
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mSEAMService:Lcom/sec/enterprise/knox/seams/ISEAMS;

    invoke-interface {v1, p1}, Lcom/sec/enterprise/knox/seams/ISEAMS;->getSignatureFromCertificate([B)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1247
    :goto_0
    return-object v1

    .line 1243
    :catch_0
    move-exception v0

    .line 1244
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "SEAMS"

    const-string v2, "getSignatureFromCertificate Failed"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1247
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getSignatureFromPackage(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 1284
    iget-object v1, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SEAMS.getSignatureFromPackage"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1285
    invoke-direct {p0}, Lcom/sec/enterprise/knox/seams/SEAMS;->getService()Lcom/sec/enterprise/knox/seams/ISEAMS;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1287
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mSEAMService:Lcom/sec/enterprise/knox/seams/ISEAMS;

    invoke-interface {v1, p1}, Lcom/sec/enterprise/knox/seams/ISEAMS;->getSignatureFromPackage(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1292
    :goto_0
    return-object v1

    .line 1288
    :catch_0
    move-exception v0

    .line 1289
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "SEAMS"

    const-string v2, "getSignatureFromPackage Failed"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1292
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hasKnoxContainers()I
    .locals 3

    .prologue
    .line 1320
    invoke-direct {p0}, Lcom/sec/enterprise/knox/seams/SEAMS;->getService()Lcom/sec/enterprise/knox/seams/ISEAMS;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1322
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mSEAMService:Lcom/sec/enterprise/knox/seams/ISEAMS;

    invoke-interface {v1}, Lcom/sec/enterprise/knox/seams/ISEAMS;->hasKnoxContainers()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1327
    :goto_0
    return v1

    .line 1323
    :catch_0
    move-exception v0

    .line 1324
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "SEAMS"

    const-string v2, "hasKnoxContainers Failed"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1327
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public hasSEContainers()I
    .locals 3

    .prologue
    .line 1355
    invoke-direct {p0}, Lcom/sec/enterprise/knox/seams/SEAMS;->getService()Lcom/sec/enterprise/knox/seams/ISEAMS;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1357
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mSEAMService:Lcom/sec/enterprise/knox/seams/ISEAMS;

    invoke-interface {v1}, Lcom/sec/enterprise/knox/seams/ISEAMS;->hasSEContainers()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1362
    :goto_0
    return v1

    .line 1358
    :catch_0
    move-exception v0

    .line 1359
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "SEAMS"

    const-string v2, "hasSEContainers Failed"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1362
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public isAuthorized(IILjava/lang/String;Ljava/lang/String;)I
    .locals 3
    .param p1, "pid"    # I
    .param p2, "uid"    # I
    .param p3, "service"    # Ljava/lang/String;
    .param p4, "method"    # Ljava/lang/String;

    .prologue
    .line 1392
    invoke-direct {p0}, Lcom/sec/enterprise/knox/seams/SEAMS;->getService()Lcom/sec/enterprise/knox/seams/ISEAMS;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1395
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mSEAMService:Lcom/sec/enterprise/knox/seams/ISEAMS;

    invoke-interface {v1, p1, p2, p3, p4}, Lcom/sec/enterprise/knox/seams/ISEAMS;->isAuthorized(IILjava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1401
    :goto_0
    return v1

    .line 1396
    :catch_0
    move-exception v0

    .line 1397
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "SEAMS"

    const-string v2, "Failed to check the authenticity of the caller"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1400
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const-string v1, "SEAMS"

    const-string v2, "SystemService null. Returning GET_SERVICE_ERROR."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1401
    const/16 v1, -0xa

    goto :goto_0
.end method

.method public isSEAndroidLogDumpStateInclude()I
    .locals 3

    .prologue
    .line 1038
    iget-object v1, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SEAMS.isSEAndroidLogDumpStateInclude"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1039
    invoke-direct {p0}, Lcom/sec/enterprise/knox/seams/SEAMS;->getService()Lcom/sec/enterprise/knox/seams/ISEAMS;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1041
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mSEAMService:Lcom/sec/enterprise/knox/seams/ISEAMS;

    iget-object v2, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Lcom/sec/enterprise/knox/seams/ISEAMS;->isSEAndroidLogDumpStateInclude(Landroid/app/enterprise/ContextInfo;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1046
    :goto_0
    return v1

    .line 1042
    :catch_0
    move-exception v0

    .line 1043
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "SEAMS"

    const-string v2, "Failed to execute isSEAndroidLogDumpStateInclude"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1046
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public isSEPolicyAutoUpdateEnabled()I
    .locals 3

    .prologue
    .line 1149
    iget-object v1, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SEAMS.isSEPolicyAutoUpdateEnabled"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1150
    invoke-direct {p0}, Lcom/sec/enterprise/knox/seams/SEAMS;->getService()Lcom/sec/enterprise/knox/seams/ISEAMS;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1152
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mSEAMService:Lcom/sec/enterprise/knox/seams/ISEAMS;

    iget-object v2, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Lcom/sec/enterprise/knox/seams/ISEAMS;->isSEPolicyAutoUpdateEnabled(Landroid/app/enterprise/ContextInfo;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1157
    :goto_0
    return v1

    .line 1153
    :catch_0
    move-exception v0

    .line 1154
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "SEAMS"

    const-string v2, "isSEPolicyAutoUpdateEnabled Failed"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1157
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public loadContainerSetting(Ljava/lang/String;)I
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 1483
    iget-object v1, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SEAMS.loadContainerSetting"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1484
    invoke-direct {p0}, Lcom/sec/enterprise/knox/seams/SEAMS;->getService()Lcom/sec/enterprise/knox/seams/ISEAMS;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1486
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mSEAMService:Lcom/sec/enterprise/knox/seams/ISEAMS;

    invoke-interface {v1, p1}, Lcom/sec/enterprise/knox/seams/ISEAMS;->loadContainerSetting(Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1491
    :goto_0
    return v1

    .line 1487
    :catch_0
    move-exception v0

    .line 1488
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "SEAMS"

    const-string v2, "Failed to save container setting"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1491
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public relabelAppDir(Ljava/lang/String;)I
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 1531
    iget-object v1, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SEAMS.relabelAppDir"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1532
    invoke-direct {p0}, Lcom/sec/enterprise/knox/seams/SEAMS;->getService()Lcom/sec/enterprise/knox/seams/ISEAMS;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1534
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mSEAMService:Lcom/sec/enterprise/knox/seams/ISEAMS;

    invoke-interface {v1, p1}, Lcom/sec/enterprise/knox/seams/ISEAMS;->relabelAppDir(Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1539
    :goto_0
    return v1

    .line 1535
    :catch_0
    move-exception v0

    .line 1536
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "SEAMS"

    const-string v2, "Failed to Relabel Data"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1539
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public relabelData()I
    .locals 3

    .prologue
    .line 1571
    iget-object v1, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SEAMS.relabelData"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1572
    invoke-direct {p0}, Lcom/sec/enterprise/knox/seams/SEAMS;->getService()Lcom/sec/enterprise/knox/seams/ISEAMS;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1574
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mSEAMService:Lcom/sec/enterprise/knox/seams/ISEAMS;

    iget-object v2, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Lcom/sec/enterprise/knox/seams/ISEAMS;->relabelData(Landroid/app/enterprise/ContextInfo;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1579
    :goto_0
    return v1

    .line 1575
    :catch_0
    move-exception v0

    .line 1576
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "SEAMS"

    const-string v2, "Failed to Relabel Data"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1579
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public removeAppFromContainer(Ljava/lang/String;[Ljava/lang/String;)I
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "certificate"    # [Ljava/lang/String;

    .prologue
    const/4 v1, -0x1

    .line 1622
    iget-object v2, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "SEAMS.removeAppFromContainer"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1623
    invoke-direct {p0}, Lcom/sec/enterprise/knox/seams/SEAMS;->getService()Lcom/sec/enterprise/knox/seams/ISEAMS;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1625
    :try_start_0
    iget-object v2, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mSEAMService:Lcom/sec/enterprise/knox/seams/ISEAMS;

    const/4 v3, -0x1

    const/4 v4, -0x1

    invoke-interface {v2, p1, p2, v3, v4}, Lcom/sec/enterprise/knox/seams/ISEAMS;->removeAppFromContainer(Ljava/lang/String;[Ljava/lang/String;II)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1631
    :cond_0
    :goto_0
    return v1

    .line 1627
    :catch_0
    move-exception v0

    .line 1628
    .local v0, "e":Landroid/os/RemoteException;
    const-string v2, "SEAMS"

    const-string v3, "Failed to remove app from container"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public removeAppFromContainer(Ljava/lang/String;[Ljava/lang/String;II)I
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "certificate"    # [Ljava/lang/String;
    .param p3, "containerID"    # I
    .param p4, "appType"    # I

    .prologue
    .line 1674
    iget-object v1, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SEAMS.removeAppFromContainer"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1675
    invoke-direct {p0}, Lcom/sec/enterprise/knox/seams/SEAMS;->getService()Lcom/sec/enterprise/knox/seams/ISEAMS;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1677
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mSEAMService:Lcom/sec/enterprise/knox/seams/ISEAMS;

    invoke-interface {v1, p1, p2, p3, p4}, Lcom/sec/enterprise/knox/seams/ISEAMS;->removeAppFromContainer(Ljava/lang/String;[Ljava/lang/String;II)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1683
    :goto_0
    return v1

    .line 1679
    :catch_0
    move-exception v0

    .line 1680
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "SEAMS"

    const-string v2, "Failed to remove app from container"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1683
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public removeSEContainer(I)I
    .locals 3
    .param p1, "containerID"    # I

    .prologue
    .line 1716
    iget-object v1, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SEAMS.removeSEContainer"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1717
    invoke-direct {p0}, Lcom/sec/enterprise/knox/seams/SEAMS;->getService()Lcom/sec/enterprise/knox/seams/ISEAMS;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1719
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mSEAMService:Lcom/sec/enterprise/knox/seams/ISEAMS;

    invoke-interface {v1, p1}, Lcom/sec/enterprise/knox/seams/ISEAMS;->removeSEContainer(I)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1724
    :goto_0
    return v1

    .line 1720
    :catch_0
    move-exception v0

    .line 1721
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "SEAMS"

    const-string v2, "Failed to remove SE container"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1724
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public setAMSLogLevel(I)I
    .locals 3
    .param p1, "FLAG"    # I

    .prologue
    .line 1758
    iget-object v1, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SEAMS.setAMSLogLevel"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1759
    invoke-direct {p0}, Lcom/sec/enterprise/knox/seams/SEAMS;->getService()Lcom/sec/enterprise/knox/seams/ISEAMS;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1761
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mSEAMService:Lcom/sec/enterprise/knox/seams/ISEAMS;

    iget-object v2, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Lcom/sec/enterprise/knox/seams/ISEAMS;->setAMSLogLevel(Landroid/app/enterprise/ContextInfo;I)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1766
    :goto_0
    return v1

    .line 1762
    :catch_0
    move-exception v0

    .line 1763
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "SEAMS"

    const-string v2, "Failed to set AMS Log Level"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1766
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public setSEAndroidLogDumpStateInclude(Z)I
    .locals 3
    .param p1, "include"    # Z

    .prologue
    .line 1797
    iget-object v1, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SEAMS.setSEAndroidLogDumpStateInclude"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1798
    invoke-direct {p0}, Lcom/sec/enterprise/knox/seams/SEAMS;->getService()Lcom/sec/enterprise/knox/seams/ISEAMS;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1800
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mSEAMService:Lcom/sec/enterprise/knox/seams/ISEAMS;

    iget-object v2, p0, Lcom/sec/enterprise/knox/seams/SEAMS;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Lcom/sec/enterprise/knox/seams/ISEAMS;->setSEAndroidLogDumpStateInclude(Landroid/app/enterprise/ContextInfo;Z)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1805
    :goto_0
    return v1

    .line 1801
    :catch_0
    move-exception v0

    .line 1802
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "SEAMS"

    const-string v2, "setSEAndroidLogDumpStateInclude Failed"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1805
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method
