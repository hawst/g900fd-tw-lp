.class public Lcom/sec/enterprise/knox/smartcard/policy/SmartCardLockscreenPolicy;
.super Lcom/sec/enterprise/knox/smartcard/policy/SmartCardPolicy;
.source "SmartCardLockscreenPolicy.java"


# static fields
.field public static final ACTION_ENABLED_LOCKSCREEN_SMARTCARD_AUTHENTICATION:Ljava/lang/String; = "edm.intent.action.smartcard.lockscreen.authentication"

.field public static final EXTRA_AUTHENTICATION_ENABLED:Ljava/lang/String; = "edm.intent.extra.smartcard.authentication.enabled"

.field private static mSmartCardLockscreenPolicy:Lcom/sec/enterprise/knox/smartcard/policy/SmartCardLockscreenPolicy;

.field private static final mSync:Ljava/lang/Object;


# instance fields
.field private lService:Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardLockscreenPolicy;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 61
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardLockscreenPolicy;->mSync:Ljava/lang/Object;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 81
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardPolicy;-><init>(Landroid/content/Context;)V

    .line 82
    return-void
.end method

.method public static createInstance(Landroid/content/Context;)Lcom/sec/enterprise/knox/smartcard/policy/SmartCardLockscreenPolicy;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 142
    new-instance v0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardLockscreenPolicy;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardLockscreenPolicy;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/sec/enterprise/knox/smartcard/policy/SmartCardLockscreenPolicy;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 130
    sget-object v1, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardLockscreenPolicy;->mSync:Ljava/lang/Object;

    monitor-enter v1

    .line 131
    :try_start_0
    sget-object v0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardLockscreenPolicy;->mSmartCardLockscreenPolicy:Lcom/sec/enterprise/knox/smartcard/policy/SmartCardLockscreenPolicy;

    if-nez v0, :cond_0

    .line 132
    new-instance v0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardLockscreenPolicy;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardLockscreenPolicy;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardLockscreenPolicy;->mSmartCardLockscreenPolicy:Lcom/sec/enterprise/knox/smartcard/policy/SmartCardLockscreenPolicy;

    .line 134
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardLockscreenPolicy;->mSmartCardLockscreenPolicy:Lcom/sec/enterprise/knox/smartcard/policy/SmartCardLockscreenPolicy;

    monitor-exit v1

    return-object v0

    .line 135
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private getService()Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardLockscreenPolicy;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardLockscreenPolicy;->lService:Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardLockscreenPolicy;

    if-nez v0, :cond_0

    .line 87
    const-string v0, "smartcard_lockscreen_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardLockscreenPolicy$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardLockscreenPolicy;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardLockscreenPolicy;->lService:Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardLockscreenPolicy;

    .line 90
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardLockscreenPolicy;->lService:Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardLockscreenPolicy;

    return-object v0
.end method


# virtual methods
.method public disableAuthentication()Z
    .locals 3

    .prologue
    .line 225
    new-instance v1, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v2

    invoke-direct {v1, v2}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const-string v2, "SmartCardLockscreenPolicy.disableAuthentication"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 227
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardLockscreenPolicy;->getService()Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardLockscreenPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 228
    iget-object v1, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardLockscreenPolicy;->lService:Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardLockscreenPolicy;

    invoke-interface {v1}, Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardLockscreenPolicy;->disableAuthentication()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 233
    :goto_0
    return v1

    .line 230
    :catch_0
    move-exception v0

    .line 231
    .local v0, "e":Landroid/os/RemoteException;
    iget-object v1, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardLockscreenPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking to SmartCard policy service "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 233
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public enableAuthentication()Z
    .locals 3

    .prologue
    .line 180
    new-instance v1, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v2

    invoke-direct {v1, v2}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const-string v2, "SmartCardLockscreenPolicy.enableAuthentication"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 182
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardLockscreenPolicy;->getService()Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardLockscreenPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 183
    iget-object v1, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardLockscreenPolicy;->lService:Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardLockscreenPolicy;

    invoke-interface {v1}, Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardLockscreenPolicy;->enableAuthentication()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 188
    :goto_0
    return v1

    .line 185
    :catch_0
    move-exception v0

    .line 186
    .local v0, "e":Landroid/os/RemoteException;
    iget-object v1, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardLockscreenPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking to SmartCard policy service "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 188
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isAuthenticationEnabled()Z
    .locals 3

    .prologue
    .line 105
    new-instance v1, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v2

    invoke-direct {v1, v2}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const-string v2, "SmartCardLockscreenPolicy.isAuthenticationEnabled"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 107
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardLockscreenPolicy;->getService()Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardLockscreenPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 108
    iget-object v1, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardLockscreenPolicy;->lService:Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardLockscreenPolicy;

    invoke-interface {v1}, Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardLockscreenPolicy;->isAuthenticationEnabled()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 113
    :goto_0
    return v1

    .line 110
    :catch_0
    move-exception v0

    .line 111
    .local v0, "e":Landroid/os/RemoteException;
    iget-object v1, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardLockscreenPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking to SmartCard policy service "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 113
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
