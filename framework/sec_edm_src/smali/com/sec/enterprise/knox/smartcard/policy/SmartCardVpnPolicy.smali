.class public Lcom/sec/enterprise/knox/smartcard/policy/SmartCardVpnPolicy;
.super Lcom/sec/enterprise/knox/smartcard/policy/SmartCardPolicy;
.source "SmartCardVpnPolicy.java"


# static fields
.field public static final ACTION_ENABLED_VPN_SMARTCARD_AUTHENTICATION:Ljava/lang/String; = "edm.intent.action.smartcard.vpn.authentication"

.field public static final EXTRA_AUTHENTICATION_ENABLED:Ljava/lang/String; = "edm.intent.extra.smartcard.authentication.enabled"

.field private static mSmartCardVpnPolicy:Lcom/sec/enterprise/knox/smartcard/policy/SmartCardVpnPolicy;

.field private static final mSync:Ljava/lang/Object;


# instance fields
.field private lService:Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardVpnPolicy;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 62
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardVpnPolicy;->mSync:Ljava/lang/Object;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 83
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardPolicy;-><init>(Landroid/content/Context;)V

    .line 84
    const-string v0, "smartcard_vpn_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardVpnPolicy$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardVpnPolicy;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardVpnPolicy;->lService:Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardVpnPolicy;

    .line 86
    iget-object v0, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardVpnPolicy;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SmartCardVpnPolicy API - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardVpnPolicy;->lService:Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardVpnPolicy;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/sec/enterprise/knox/smartcard/policy/SmartCardVpnPolicy;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 112
    sget-object v1, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardVpnPolicy;->mSync:Ljava/lang/Object;

    monitor-enter v1

    .line 113
    :try_start_0
    sget-object v0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardVpnPolicy;->mSmartCardVpnPolicy:Lcom/sec/enterprise/knox/smartcard/policy/SmartCardVpnPolicy;

    if-nez v0, :cond_0

    .line 114
    new-instance v0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardVpnPolicy;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardVpnPolicy;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardVpnPolicy;->mSmartCardVpnPolicy:Lcom/sec/enterprise/knox/smartcard/policy/SmartCardVpnPolicy;

    .line 116
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardVpnPolicy;->mSmartCardVpnPolicy:Lcom/sec/enterprise/knox/smartcard/policy/SmartCardVpnPolicy;

    monitor-exit v1

    return-object v0

    .line 117
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private getService()Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardVpnPolicy;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardVpnPolicy;->lService:Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardVpnPolicy;

    if-nez v0, :cond_0

    .line 92
    const-string v0, "smartcard_vpn_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardVpnPolicy$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardVpnPolicy;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardVpnPolicy;->lService:Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardVpnPolicy;

    .line 95
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardVpnPolicy;->lService:Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardVpnPolicy;

    return-object v0
.end method


# virtual methods
.method public isAuthenticationEnabled()Z
    .locals 3

    .prologue
    .line 205
    new-instance v1, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v2

    invoke-direct {v1, v2}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const-string v2, "SmartCardVpnPolicy.isAuthenticationEnabled"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 207
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardVpnPolicy;->getService()Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardVpnPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 208
    iget-object v1, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardVpnPolicy;->lService:Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardVpnPolicy;

    invoke-interface {v1}, Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardVpnPolicy;->isAuthenticationEnabled()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 213
    :goto_0
    return v1

    .line 210
    :catch_0
    move-exception v0

    .line 211
    .local v0, "e":Landroid/os/RemoteException;
    iget-object v1, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardVpnPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking to SmartCard policy service "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 213
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isCredentialRequired(Ljava/lang/String;)Z
    .locals 3
    .param p1, "profileName"    # Ljava/lang/String;

    .prologue
    .line 179
    new-instance v1, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v2

    invoke-direct {v1, v2}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const-string v2, "SmartCardVpnPolicy.isCredentialRequired"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 181
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardVpnPolicy;->getService()Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardVpnPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 182
    iget-object v1, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardVpnPolicy;->lService:Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardVpnPolicy;

    invoke-interface {v1, p1}, Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardVpnPolicy;->isCredentialRequired(Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 187
    :goto_0
    return v1

    .line 184
    :catch_0
    move-exception v0

    .line 185
    .local v0, "e":Landroid/os/RemoteException;
    iget-object v1, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardVpnPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking to SmartCard policy service "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 187
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public requireCredentials(Ljava/lang/String;Z)Z
    .locals 3
    .param p1, "profileName"    # Ljava/lang/String;
    .param p2, "require"    # Z

    .prologue
    .line 157
    new-instance v1, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v2

    invoke-direct {v1, v2}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const-string v2, "SmartCardVpnPolicy.requireCredentials"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 159
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardVpnPolicy;->getService()Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardVpnPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 160
    iget-object v1, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardVpnPolicy;->lService:Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardVpnPolicy;

    invoke-interface {v1, p1, p2}, Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardVpnPolicy;->requireCredentials(Ljava/lang/String;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 165
    :goto_0
    return v1

    .line 162
    :catch_0
    move-exception v0

    .line 163
    .local v0, "e":Landroid/os/RemoteException;
    iget-object v1, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardVpnPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking to SmartCard policy service "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 165
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
