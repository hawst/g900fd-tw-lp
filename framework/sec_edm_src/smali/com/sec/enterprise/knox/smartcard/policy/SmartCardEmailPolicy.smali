.class public Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;
.super Lcom/sec/enterprise/knox/smartcard/policy/SmartCardPolicy;
.source "SmartCardEmailPolicy.java"


# static fields
.field public static final ACTION_ENABLED_EMAIL_SMARTCARD_AUTHENTICATION:Ljava/lang/String; = "edm.intent.action.smartcard.email.authentication"

.field public static final EXTRA_AUTHENTICATION_ENABLED:Ljava/lang/String; = "edm.intent.extra.smartcard.authentication.enabled"

.field private static mSmartCardEmailPolicy:Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;

.field private static final mSync:Ljava/lang/Object;


# instance fields
.field private lService:Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardEmailPolicy;

.field private mContextInfo:Landroid/app/enterprise/ContextInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 66
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;->mSync:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V
    .locals 3
    .param p1, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 95
    invoke-direct {p0, p2}, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardPolicy;-><init>(Landroid/content/Context;)V

    .line 96
    iput-object p1, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    .line 97
    iget-object v0, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SmartCardEmailPolicy API ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget v2, v2, Landroid/app/enterprise/ContextInfo;->mContainerId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget v2, v2, Landroid/app/enterprise/ContextInfo;->mCallerUid:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    return-void
.end method

.method public static createInstance(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;
    .locals 2
    .param p0, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 179
    new-instance v0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    return-object v0
.end method

.method public static getInstance(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;
    .locals 3
    .param p0, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 167
    sget-object v1, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;->mSync:Ljava/lang/Object;

    monitor-enter v1

    .line 168
    :try_start_0
    sget-object v0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;->mSmartCardEmailPolicy:Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;

    if-nez v0, :cond_0

    .line 169
    new-instance v0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, p0, v2}, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    sput-object v0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;->mSmartCardEmailPolicy:Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;

    .line 171
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;->mSmartCardEmailPolicy:Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;

    monitor-exit v1

    return-object v0

    .line 172
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 154
    sget-object v2, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;->mSync:Ljava/lang/Object;

    monitor-enter v2

    .line 155
    :try_start_0
    sget-object v1, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;->mSmartCardEmailPolicy:Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;

    if-nez v1, :cond_0

    .line 156
    new-instance v0, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    invoke-direct {v0, v1}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    .line 157
    .local v0, "cxtInfo":Landroid/app/enterprise/ContextInfo;
    new-instance v1, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v1, v0, v3}, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    sput-object v1, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;->mSmartCardEmailPolicy:Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;

    .line 159
    .end local v0    # "cxtInfo":Landroid/app/enterprise/ContextInfo;
    :cond_0
    sget-object v1, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;->mSmartCardEmailPolicy:Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;

    monitor-exit v2

    return-object v1

    .line 160
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private getService()Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardEmailPolicy;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;->lService:Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardEmailPolicy;

    if-nez v0, :cond_0

    .line 104
    const-string v0, "smartcard_email_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardEmailPolicy$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardEmailPolicy;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;->lService:Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardEmailPolicy;

    .line 107
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;->lService:Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardEmailPolicy;

    return-object v0
.end method


# virtual methods
.method public getSMIMEEncryptionCertificate(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "emailAddress"    # Ljava/lang/String;

    .prologue
    .line 386
    const/4 v0, 0x0

    .line 387
    .local v0, "alias_name":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "SmartCardEmailPolicy.getSMIMEEncryptionCertificate"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 389
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;->getService()Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardEmailPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 390
    iget-object v2, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;->lService:Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardEmailPolicy;

    iget-object v3, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3, p1}, Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardEmailPolicy;->getSMIMEEncryptionCertificate(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 395
    :cond_0
    :goto_0
    return-object v0

    .line 392
    :catch_0
    move-exception v1

    .line 393
    .local v1, "e":Landroid/os/RemoteException;
    iget-object v2, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;->TAG:Ljava/lang/String;

    const-string v3, "Failed talking to SmartCard policy service "

    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public getSMIMESignatureCertificate(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "emailAddress"    # Ljava/lang/String;

    .prologue
    .line 301
    const/4 v0, 0x0

    .line 302
    .local v0, "alias_name":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "SmartCardEmailPolicy.getSMIMESignatureCertificate"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 304
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;->getService()Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardEmailPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 305
    iget-object v2, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;->lService:Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardEmailPolicy;

    iget-object v3, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3, p1}, Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardEmailPolicy;->getSMIMESignatureCertificate(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 310
    :cond_0
    :goto_0
    return-object v0

    .line 307
    :catch_0
    move-exception v1

    .line 308
    .local v1, "e":Landroid/os/RemoteException;
    iget-object v2, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;->TAG:Ljava/lang/String;

    const-string v3, "Failed talking to SmartCard policy service "

    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public isAuthenticationEnabled()Z
    .locals 3

    .prologue
    .line 129
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;->getService()Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardEmailPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 130
    iget-object v1, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;->lService:Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardEmailPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardEmailPolicy;->isAuthenticationEnabled(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 135
    :goto_0
    return v1

    .line 132
    :catch_0
    move-exception v0

    .line 133
    .local v0, "e":Landroid/os/RemoteException;
    iget-object v1, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking to SmartCard policy service "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 135
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isCredentialRequired(Ljava/lang/String;)Z
    .locals 3
    .param p1, "emailAddress"    # Ljava/lang/String;

    .prologue
    .line 248
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;->getService()Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardEmailPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 249
    iget-object v1, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;->lService:Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardEmailPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardEmailPolicy;->isCredentialRequired(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 254
    :goto_0
    return v1

    .line 251
    :catch_0
    move-exception v0

    .line 252
    .local v0, "e":Landroid/os/RemoteException;
    iget-object v1, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking to SmartCard policy service "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 254
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public removeSMIMEEncryptionCertificate(Ljava/lang/String;)Z
    .locals 3
    .param p1, "emailAddress"    # Ljava/lang/String;

    .prologue
    .line 415
    iget-object v1, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SmartCardEmailPolicy.removeSMIMEEncryptionCertificate"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 417
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;->getService()Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardEmailPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 418
    iget-object v1, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;->lService:Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardEmailPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardEmailPolicy;->removeSMIMEEncryptionCertificate(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 423
    :goto_0
    return v1

    .line 420
    :catch_0
    move-exception v0

    .line 421
    .local v0, "e":Landroid/os/RemoteException;
    iget-object v1, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking to SmartCard policy service "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 423
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public removeSMIMESignatureCertificate(Ljava/lang/String;)Z
    .locals 3
    .param p1, "emailAddress"    # Ljava/lang/String;

    .prologue
    .line 330
    iget-object v1, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SmartCardEmailPolicy.removeSMIMESignatureCertificate"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 332
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;->getService()Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardEmailPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 333
    iget-object v1, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;->lService:Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardEmailPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardEmailPolicy;->removeSMIMESignatureCertificate(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 338
    :goto_0
    return v1

    .line 335
    :catch_0
    move-exception v0

    .line 336
    .local v0, "e":Landroid/os/RemoteException;
    iget-object v1, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking to SmartCard policy service "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 338
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public requireCredentials(Ljava/lang/String;Z)Z
    .locals 3
    .param p1, "emailAddress"    # Ljava/lang/String;
    .param p2, "require"    # Z

    .prologue
    .line 222
    iget-object v1, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SmartCardEmailPolicy.requireCredentials"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 224
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;->getService()Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardEmailPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 225
    iget-object v1, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;->lService:Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardEmailPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardEmailPolicy;->requireCredentials(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 230
    :goto_0
    return v1

    .line 227
    :catch_0
    move-exception v0

    .line 228
    .local v0, "e":Landroid/os/RemoteException;
    iget-object v1, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking to SmartCard policy service "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 230
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setForceSMIMEEncryptionCertificate(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "emailAddress"    # Ljava/lang/String;
    .param p2, "alias"    # Ljava/lang/String;

    .prologue
    .line 359
    iget-object v1, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SmartCardEmailPolicy.setForceSMIMEEncryptionCertificate"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 361
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;->getService()Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardEmailPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 362
    iget-object v1, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;->lService:Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardEmailPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardEmailPolicy;->setForceSMIMEEncryptionCertificate(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 367
    :goto_0
    return v1

    .line 364
    :catch_0
    move-exception v0

    .line 365
    .local v0, "e":Landroid/os/RemoteException;
    iget-object v1, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking to SmartCard policy service "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 367
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setForceSMIMESignatureCertificate(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "emailAddress"    # Ljava/lang/String;
    .param p2, "alias"    # Ljava/lang/String;

    .prologue
    .line 274
    iget-object v1, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SmartCardEmailPolicy.setForceSMIMESignatureCertificate"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 276
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;->getService()Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardEmailPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 277
    iget-object v1, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;->lService:Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardEmailPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardEmailPolicy;->setForceSMIMESignatureCertificate(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 282
    :goto_0
    return v1

    .line 279
    :catch_0
    move-exception v0

    .line 280
    .local v0, "e":Landroid/os/RemoteException;
    iget-object v1, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking to SmartCard policy service "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 282
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
