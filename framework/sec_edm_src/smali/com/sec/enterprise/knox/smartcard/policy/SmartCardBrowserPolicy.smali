.class public Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;
.super Lcom/sec/enterprise/knox/smartcard/policy/SmartCardPolicy;
.source "SmartCardBrowserPolicy.java"


# static fields
.field public static final ACTION_ENABLED_BROWSER_SMARTCARD_AUTHENTICATION:Ljava/lang/String; = "edm.intent.action.smartcard.browser.authentication"

.field public static final EXTRA_AUTHENTICATION_ENABLED:Ljava/lang/String; = "edm.intent.extra.smartcard.authentication.enabled"

.field private static mSmartCardBrowserPolicy:Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;

.field private static final mSync:Ljava/lang/Object;


# instance fields
.field private lService:Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardBrowserPolicy;

.field private mContextInfo:Landroid/app/enterprise/ContextInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 66
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;->mSync:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V
    .locals 3
    .param p1, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 94
    invoke-direct {p0, p2}, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardPolicy;-><init>(Landroid/content/Context;)V

    .line 95
    iput-object p1, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    .line 96
    iget-object v0, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SmartCardBrowserPolicy API ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/app/enterprise/ContextInfo;->mContainerId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/app/enterprise/ContextInfo;->mCallerUid:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    return-void
.end method

.method public static createInstance(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;
    .locals 2
    .param p0, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 149
    new-instance v0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    return-object v0
.end method

.method public static getInstance(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;
    .locals 3
    .param p0, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 136
    sget-object v1, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;->mSync:Ljava/lang/Object;

    monitor-enter v1

    .line 137
    :try_start_0
    sget-object v0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;->mSmartCardBrowserPolicy:Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;

    if-nez v0, :cond_0

    .line 138
    new-instance v0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, p0, v2}, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    sput-object v0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;->mSmartCardBrowserPolicy:Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;

    .line 141
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;->mSmartCardBrowserPolicy:Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;

    monitor-exit v1

    return-object v0

    .line 142
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 124
    sget-object v2, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;->mSync:Ljava/lang/Object;

    monitor-enter v2

    .line 125
    :try_start_0
    sget-object v1, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;->mSmartCardBrowserPolicy:Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;

    if-nez v1, :cond_0

    .line 126
    new-instance v0, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    invoke-direct {v0, v1}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    .line 127
    .local v0, "cxtInfo":Landroid/app/enterprise/ContextInfo;
    new-instance v1, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v1, v0, v3}, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    sput-object v1, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;->mSmartCardBrowserPolicy:Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;

    .line 130
    .end local v0    # "cxtInfo":Landroid/app/enterprise/ContextInfo;
    :cond_0
    sget-object v1, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;->mSmartCardBrowserPolicy:Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;

    monitor-exit v2

    return-object v1

    .line 131
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private getService()Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardBrowserPolicy;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;->lService:Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardBrowserPolicy;

    if-nez v0, :cond_0

    .line 102
    const-string v0, "smartcard_browser_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardBrowserPolicy$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardBrowserPolicy;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;->lService:Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardBrowserPolicy;

    .line 105
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;->lService:Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardBrowserPolicy;

    return-object v0
.end method


# virtual methods
.method public disableAuthentication()Z
    .locals 3

    .prologue
    .line 258
    iget-object v1, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SmartCardBrowserPolicy.disableAuthentication"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 260
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;->getService()Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardBrowserPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 261
    iget-object v1, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;->lService:Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardBrowserPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardBrowserPolicy;->disableAuthentication(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 266
    :goto_0
    return v1

    .line 263
    :catch_0
    move-exception v0

    .line 264
    .local v0, "e":Landroid/os/RemoteException;
    iget-object v1, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking to SmartCard policy service "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 266
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public enableAuthentication()Z
    .locals 3

    .prologue
    .line 213
    iget-object v1, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SmartCardBrowserPolicy.enableAuthentication"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 215
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;->getService()Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardBrowserPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 216
    iget-object v1, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;->lService:Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardBrowserPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardBrowserPolicy;->enableAuthentication(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 221
    :goto_0
    return v1

    .line 218
    :catch_0
    move-exception v0

    .line 219
    .local v0, "e":Landroid/os/RemoteException;
    iget-object v1, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking to SmartCard policy service "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 221
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getClientCertificateAlias(Ljava/lang/String;I)Ljava/lang/String;
    .locals 3
    .param p1, "hostURL"    # Ljava/lang/String;
    .param p2, "port"    # I

    .prologue
    .line 313
    iget-object v1, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SmartCardBrowserPolicy.getClientCertificateAlias"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 315
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;->getService()Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardBrowserPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 316
    iget-object v1, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;->lService:Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardBrowserPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardBrowserPolicy;->getClientCertificateAlias(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 321
    :goto_0
    return-object v1

    .line 318
    :catch_0
    move-exception v0

    .line 319
    .local v0, "e":Landroid/os/RemoteException;
    iget-object v1, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking to SmartCard policy service "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 321
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isAuthenticationEnabled()Z
    .locals 3

    .prologue
    .line 170
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;->getService()Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardBrowserPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 171
    iget-object v1, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;->lService:Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardBrowserPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardBrowserPolicy;->isAuthenticationEnabled(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 176
    :goto_0
    return v1

    .line 173
    :catch_0
    move-exception v0

    .line 174
    .local v0, "e":Landroid/os/RemoteException;
    iget-object v1, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking to SmartCard policy service "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 176
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public removeClientCertificateAlias(Ljava/lang/String;I)Z
    .locals 3
    .param p1, "hostURL"    # Ljava/lang/String;
    .param p2, "port"    # I

    .prologue
    .line 340
    iget-object v1, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SmartCardBrowserPolicy.removeClientCertificateAlias"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 342
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;->getService()Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardBrowserPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 343
    iget-object v1, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;->lService:Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardBrowserPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardBrowserPolicy;->removeClientCertificateAlias(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 348
    :goto_0
    return v1

    .line 345
    :catch_0
    move-exception v0

    .line 346
    .local v0, "e":Landroid/os/RemoteException;
    iget-object v1, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking to SmartCard policy service "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 348
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setClientCertificateAlias(Ljava/lang/String;ILjava/lang/String;)Z
    .locals 3
    .param p1, "hostURL"    # Ljava/lang/String;
    .param p2, "port"    # I
    .param p3, "alias"    # Ljava/lang/String;

    .prologue
    .line 286
    iget-object v1, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SmartCardBrowserPolicy.setClientCertificateAlias"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 288
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;->getService()Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardBrowserPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 289
    iget-object v1, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;->lService:Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardBrowserPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2, p3}, Lcom/sec/enterprise/knox/smartcard/policy/ISmartCardBrowserPolicy;->setClientCertificateAlias(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;ILjava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 294
    :goto_0
    return v1

    .line 291
    :catch_0
    move-exception v0

    .line 292
    .local v0, "e":Landroid/os/RemoteException;
    iget-object v1, p0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking to SmartCard policy service "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 294
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
