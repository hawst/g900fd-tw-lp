.class public Lcom/sec/enterprise/knox/EnterpriseContainerManager;
.super Ljava/lang/Object;
.source "EnterpriseContainerManager.java"


# static fields
.field public static final CONTAINER_ACTIVE:I = 0x5b

.field public static final CONTAINER_CREATED_NOT_ACTIVATED:I = 0x60

.field public static final CONTAINER_CREATION_IN_PROGRESS:I = 0x5d

.field public static final CONTAINER_DOESNT_EXISTS:I = -0x1

.field public static final CONTAINER_ID:Ljava/lang/String; = "containerid"

.field public static final CONTAINER_INACTIVE:I = 0x5a

.field public static final CONTAINER_LOCKED:I = 0x5f

.field public static final CONTAINER_PREFIX:Ljava/lang/String; = "sec_container_"

.field public static final CONTAINER_REMOVE_IN_PROGRESS:I = 0x5e

.field public static final FLAG_DISABLE_CONTAINER_ACTIVATION_FLOW:I = 0x2

.field public static final FLAG_ENABLE_CONTAINER_ACTIVATION_FLOW:I = 0x1

.field public static final INTENT_CONTAINER_CANCELLED:Ljava/lang/String; = "enterprise.container.cancelled"

.field public static final INTENT_CONTAINER_CREATION_SUCCESS:Ljava/lang/String; = "enterprise.container.created.nonactive"

.field public static final INTENT_CONTAINER_LOCKED_BY_ADMIN:Ljava/lang/String; = "enterprise.container.locked"

.field public static final INTENT_CONTAINER_REMOVED:Ljava/lang/String; = "enterprise.container.uninstalled"

.field public static final INTENT_CONTAINER_REMOVE_PROGRESS:Ljava/lang/String; = "enterprise.container.remove.progress"

.field public static final INTENT_CONTAINER_REMOVE_UNMOUNT_FAILURE:Ljava/lang/String; = "enterprise.container.unmountfailure"

.field public static final INTENT_CONTAINER_SETUP_FAILURE:Ljava/lang/String; = "enterprise.container.setup.failure"

.field public static final INTENT_CONTAINER_SETUP_SUCCESS:Ljava/lang/String; = "enterprise.container.setup.success"

.field public static final MAX_CONTAINER_NUMBER:I = 0x1

.field public static final MIN_PERSONA_ID:I = 0x64

.field private static TAG:Ljava/lang/String;

.field private static gContainerService:Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy;

.field private static mEDM:Landroid/app/enterprise/IEnterpriseDeviceManager;

.field private static mMUMContainerService:Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

.field private static mPersonaService:Landroid/os/IPersonaManager;

.field private static mRCPService:Lcom/sec/enterprise/knox/container/IRCPPolicy;


# instance fields
.field private mBrowserPolicy:Landroid/app/enterprise/BrowserPolicy;

.field private mBrowserPolicyCreated:Z

.field private mContainerAppPolicy:Lcom/sec/enterprise/knox/ContainerApplicationPolicy;

.field private mContainerAppPolicyCreated:Z

.field private mContainerFirewallPolicy:Lcom/sec/enterprise/knox/ContainerFirewallPolicy;

.field private mContainerFirewallPolicyCreated:Z

.field private final mContainerId:I

.field private mContainerRestrictionPolicy:Lcom/sec/enterprise/knox/ContainerRestrictionPolicy;

.field private mContainerRestrictionPolicyCreated:Z

.field private mContainerVpnPolicy:Lcom/sec/enterprise/knox/ContainerVpnPolicy;

.field private mContainerVpnPolicyCreated:Z

.field private mDeviceAccountPolicy:Landroid/app/enterprise/DeviceAccountPolicy;

.field private mDeviceAccountPolicyCreated:Z

.field private mEasAccountPolicy:Landroid/app/enterprise/ExchangeAccountPolicy;

.field private mEasAccountPolicyCreated:Z

.field private mEmailAccountPolicy:Landroid/app/enterprise/EmailAccountPolicy;

.field private mEmailAccountPolicyCreated:Z

.field private mEmailPolicy:Landroid/app/enterprise/EmailPolicy;

.field private mEmailPolicyCreated:Z

.field private mPasswordPolicy:Lcom/sec/enterprise/knox/ContainerPasswordPolicy;

.field private mPasswordPolicyCreated:Z

.field private mSCBrowserPolicy:Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;

.field private mSCBrowserPolicyCreated:Z

.field private mSCEmailPolicy:Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;

.field private mSCEmailPolicyCreated:Z

.field private mSSOPolicy:Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;

.field private mSSOPolicyCreated:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 308
    sput-object v0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->gContainerService:Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy;

    .line 309
    sput-object v0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mMUMContainerService:Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    .line 310
    sput-object v0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mPersonaService:Landroid/os/IPersonaManager;

    .line 311
    sput-object v0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mRCPService:Lcom/sec/enterprise/knox/container/IRCPPolicy;

    .line 312
    sput-object v0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mEDM:Landroid/app/enterprise/IEnterpriseDeviceManager;

    .line 363
    const-string v0, "EnterpriseContainerManager"

    sput-object v0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->TAG:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(I)V
    .locals 5
    .param p1, "containerId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NoSuchFieldException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 668
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 704
    iput-boolean v2, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mBrowserPolicyCreated:Z

    .line 705
    iput-boolean v2, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mSCBrowserPolicyCreated:Z

    .line 706
    iput-boolean v2, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mEasAccountPolicyCreated:Z

    .line 707
    iput-boolean v2, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mEmailAccountPolicyCreated:Z

    .line 708
    iput-boolean v2, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mEmailPolicyCreated:Z

    .line 709
    iput-boolean v2, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mSCEmailPolicyCreated:Z

    .line 710
    iput-boolean v2, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mDeviceAccountPolicyCreated:Z

    .line 711
    iput-boolean v2, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mPasswordPolicyCreated:Z

    .line 712
    iput-boolean v2, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mSSOPolicyCreated:Z

    .line 713
    iput-boolean v2, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mContainerVpnPolicyCreated:Z

    .line 714
    iput-boolean v2, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mContainerAppPolicyCreated:Z

    .line 715
    iput-boolean v2, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mContainerFirewallPolicyCreated:Z

    .line 716
    iput-boolean v2, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mContainerRestrictionPolicyCreated:Z

    .line 669
    invoke-static {p1}, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->doesContainerExists(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 671
    const-string v2, "persona"

    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Landroid/os/IPersonaManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/IPersonaManager;

    move-result-object v1

    .line 674
    .local v1, "mPersona":Landroid/os/IPersonaManager;
    if-eqz v1, :cond_0

    :try_start_0
    invoke-interface {v1, p1}, Landroid/os/IPersonaManager;->exists(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 675
    new-instance v2, Ljava/lang/NoSuchFieldException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Container with Id "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " does not exists"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/NoSuchFieldException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 678
    :catch_0
    move-exception v0

    .line 679
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Remote exception: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 680
    new-instance v2, Ljava/lang/NoSuchFieldException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Container with Id "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " does not exists"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/NoSuchFieldException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 685
    .end local v0    # "e":Landroid/os/RemoteException;
    .end local v1    # "mPersona":Landroid/os/IPersonaManager;
    :cond_0
    iput p1, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mContainerId:I

    .line 686
    return-void
.end method

.method public static createContainer(Lcom/sec/enterprise/knox/EnterpriseContainerCallback;)Z
    .locals 4
    .param p0, "callback"    # Lcom/sec/enterprise/knox/EnterpriseContainerCallback;

    .prologue
    .line 405
    new-instance v2, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v3

    invoke-direct {v2, v3}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const-string v3, "EnterpriseContainerManager.createContainer"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 407
    const/4 v1, 0x0

    .line 408
    .local v1, "retVal":Z
    new-instance v2, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v3

    invoke-direct {v2, v3}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const-string v3, "knox-1.0"

    invoke-static {v2, v3, p0}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->createContainer(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Lcom/sec/enterprise/knox/EnterpriseContainerCallback;)I

    move-result v0

    .line 410
    .local v0, "id":I
    if-ltz v0, :cond_0

    .line 411
    const/4 v1, 0x1

    .line 413
    :cond_0
    return v1
.end method

.method public static createContainer(Lcom/sec/enterprise/knox/EnterpriseContainerCallback;I)Z
    .locals 4
    .param p0, "callback"    # Lcom/sec/enterprise/knox/EnterpriseContainerCallback;
    .param p1, "flag"    # I

    .prologue
    .line 457
    const/4 v1, 0x0

    .line 458
    .local v1, "retVal":Z
    new-instance v2, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v3

    invoke-direct {v2, v3}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const-string v3, "knox-1.0"

    invoke-static {v2, v3, p0}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->createContainer(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Lcom/sec/enterprise/knox/EnterpriseContainerCallback;)I

    move-result v0

    .line 460
    .local v0, "id":I
    if-ltz v0, :cond_0

    .line 461
    const/4 v1, 0x1

    .line 463
    :cond_0
    return v1
.end method

.method public static doesContainerExists(I)Z
    .locals 5
    .param p0, "containerId"    # I

    .prologue
    .line 554
    new-instance v2, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v3

    invoke-direct {v2, v3}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const-string v3, "EnterpriseContainerManager.doesContainerExists"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 555
    const/4 v1, 0x0

    .line 557
    .local v1, "retVal":Z
    :try_start_0
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->getPersonaService()Landroid/os/IPersonaManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->getPersonaService()Landroid/os/IPersonaManager;

    move-result-object v2

    invoke-interface {v2, p0}, Landroid/os/IPersonaManager;->exists(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-eqz v2, :cond_0

    .line 558
    const/4 v1, 0x1

    .line 564
    :cond_0
    :goto_0
    return v1

    .line 560
    :catch_0
    move-exception v0

    .line 561
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Remote exception: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static enforceContainerPermission(I)I
    .locals 1
    .param p0, "containerId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;
        }
    .end annotation

    .prologue
    .line 1563
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    return v0
.end method

.method static declared-synchronized getContainerService()Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy;
    .locals 2

    .prologue
    .line 315
    const-class v1, Lcom/sec/enterprise/knox/EnterpriseContainerManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->gContainerService:Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy;

    if-nez v0, :cond_0

    .line 316
    const-string v0, "enterprise_container_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->gContainerService:Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy;

    .line 320
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->gContainerService:Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 315
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static getContainerStatus(I)I
    .locals 7
    .param p0, "containerId"    # I

    .prologue
    .line 1364
    const/4 v3, -0x1

    .line 1365
    .local v3, "retVal":I
    invoke-static {}, Landroid/os/PersonaManager;->getKnoxInfo()Landroid/os/Bundle;

    move-result-object v0

    .line 1368
    .local v0, "bundle":Landroid/os/Bundle;
    :try_start_0
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->getMUMContainerService()Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    move-result-object v2

    .line 1369
    .local v2, "mumService":Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    if-nez v2, :cond_0

    move v4, v3

    .line 1378
    .end local v2    # "mumService":Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    .end local v3    # "retVal":I
    .local v4, "retVal":I
    :goto_0
    return v4

    .line 1373
    .end local v4    # "retVal":I
    .restart local v2    # "mumService":Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    .restart local v3    # "retVal":I
    :cond_0
    new-instance v5, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v6

    invoke-direct {v5, v6, p0}, Landroid/app/enterprise/ContextInfo;-><init>(II)V

    invoke-interface {v2, v5}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager;->getStatus(Landroid/app/enterprise/ContextInfo;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .end local v2    # "mumService":Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    :goto_1
    move v4, v3

    .line 1378
    .end local v3    # "retVal":I
    .restart local v4    # "retVal":I
    goto :goto_0

    .line 1374
    .end local v4    # "retVal":I
    .restart local v3    # "retVal":I
    :catch_0
    move-exception v1

    .line 1375
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v5, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->TAG:Ljava/lang/String;

    const-string v6, "Failed at EnterpriseContainerManager API getContainerStatus "

    invoke-static {v5, v6, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method static declared-synchronized getEnterpriseDeviceManager()Landroid/app/enterprise/IEnterpriseDeviceManager;
    .locals 2

    .prologue
    .line 351
    const-class v1, Lcom/sec/enterprise/knox/EnterpriseContainerManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mEDM:Landroid/app/enterprise/IEnterpriseDeviceManager;

    if-nez v0, :cond_0

    .line 352
    const-string v0, "enterprise_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/app/enterprise/IEnterpriseDeviceManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/IEnterpriseDeviceManager;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mEDM:Landroid/app/enterprise/IEnterpriseDeviceManager;

    .line 356
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mEDM:Landroid/app/enterprise/IEnterpriseDeviceManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 351
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static declared-synchronized getMUMContainerService()Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    .locals 2

    .prologue
    .line 324
    const-class v1, Lcom/sec/enterprise/knox/EnterpriseContainerManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mMUMContainerService:Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    if-nez v0, :cond_0

    .line 325
    const-string v0, "mum_container_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mMUMContainerService:Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    .line 329
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mMUMContainerService:Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 324
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static getNonContainerizedString(ILjava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "containerId"    # I
    .param p1, "containerizedString"    # Ljava/lang/String;

    .prologue
    .line 1572
    sget-object v0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->TAG:Ljava/lang/String;

    const-string v1, "WRONG BUILD OPTION: KNOX 1.0 Container deprecated "

    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 1588
    return-object p1
.end method

.method public static getNonContainerizedString(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "containerizedString"    # Ljava/lang/String;

    .prologue
    .line 1598
    sget-object v0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->TAG:Ljava/lang/String;

    const-string v1, "WRONG BUILD OPTION: KNOX 1.0 Container deprecated "

    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 1613
    return-object p0
.end method

.method public static getOwnContainers()[Lcom/sec/enterprise/knox/EnterpriseContainerObject;
    .locals 5

    .prologue
    .line 588
    const/4 v2, 0x0

    .line 589
    .local v2, "retList":[Lcom/sec/enterprise/knox/EnterpriseContainerObject;
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->getMUMContainerService()Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    move-result-object v1

    .line 590
    .local v1, "mumService":Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    if-nez v1, :cond_0

    .line 591
    sget-object v3, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->TAG:Ljava/lang/String;

    const-string v4, "MUMContainerService is not yet ready!!!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 601
    :goto_0
    return-object v2

    .line 596
    :cond_0
    :try_start_0
    invoke-interface {v1}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager;->getOwnContainers()[Lcom/sec/enterprise/knox/EnterpriseContainerObject;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    .line 597
    :catch_0
    move-exception v0

    .line 598
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v3, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->TAG:Ljava/lang/String;

    const-string v4, "Failed at EnterpriseContainerManager API getOwnContainers "

    invoke-static {v3, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method static declared-synchronized getPersonaService()Landroid/os/IPersonaManager;
    .locals 2

    .prologue
    .line 333
    const-class v1, Lcom/sec/enterprise/knox/EnterpriseContainerManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mPersonaService:Landroid/os/IPersonaManager;

    if-nez v0, :cond_0

    .line 334
    const-string v0, "persona"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/os/IPersonaManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/IPersonaManager;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mPersonaService:Landroid/os/IPersonaManager;

    .line 338
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mPersonaService:Landroid/os/IPersonaManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 333
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static declared-synchronized getRCPPolicyService()Lcom/sec/enterprise/knox/container/IRCPPolicy;
    .locals 2

    .prologue
    .line 342
    const-class v1, Lcom/sec/enterprise/knox/EnterpriseContainerManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mRCPService:Lcom/sec/enterprise/knox/container/IRCPPolicy;

    if-nez v0, :cond_0

    .line 343
    const-string v0, "mum_container_rcp_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/container/IRCPPolicy$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/enterprise/knox/container/IRCPPolicy;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mRCPService:Lcom/sec/enterprise/knox/container/IRCPPolicy;

    .line 347
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mRCPService:Lcom/sec/enterprise/knox/container/IRCPPolicy;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 342
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static removeContainer(ILcom/sec/enterprise/knox/EnterpriseContainerCallback;)Z
    .locals 6
    .param p0, "containerId"    # I
    .param p1, "callback"    # Lcom/sec/enterprise/knox/EnterpriseContainerCallback;

    .prologue
    .line 509
    new-instance v4, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v5

    invoke-direct {v4, v5}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const-string v5, "EnterpriseContainerManager.removeContainer"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 511
    const/4 v2, 0x0

    .line 512
    .local v2, "retVal":Z
    invoke-static {p0}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->removeContainer(I)I

    move-result v3

    .line 513
    .local v3, "returnValue":I
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 514
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v4, "containerid"

    invoke-virtual {v0, v4, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 516
    const/16 v1, 0x3ed

    .line 517
    .local v1, "retCode":I
    if-nez v3, :cond_0

    .line 518
    const/16 v1, 0x3eb

    .line 519
    const/4 v2, 0x1

    .line 521
    :cond_0
    if-eqz p1, :cond_1

    .line 522
    invoke-virtual {p1, v1, v0}, Lcom/sec/enterprise/knox/EnterpriseContainerCallback;->updateStatus(ILandroid/os/Bundle;)V

    .line 525
    :cond_1
    return v2
.end method

.method static throwException(Ljava/lang/String;)V
    .locals 3
    .param p0, "info"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;
        }
    .end annotation

    .prologue
    .line 360
    new-instance v0, Ljava/lang/SecurityException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "API "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not supported"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public activateContainer()Z
    .locals 2

    .prologue
    .line 642
    new-instance v0, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    invoke-direct {v0, v1}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const-string v1, "EnterpriseContainerManager.activate"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 643
    const/4 v0, 0x1

    return v0
.end method

.method public allowContactInfoToNonKnox(Z)Z
    .locals 8
    .param p1, "isAllowed"    # Z

    .prologue
    .line 1725
    new-instance v5, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v6

    invoke-direct {v5, v6}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const-string v6, "EnterpriseContainerManager.allowContactInfoToNonKnox"

    invoke-static {v5, v6}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1727
    const/4 v3, 0x0

    .line 1728
    .local v3, "retVal":Z
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->getRCPPolicyService()Lcom/sec/enterprise/knox/container/IRCPPolicy;

    move-result-object v2

    .line 1729
    .local v2, "rcpService":Lcom/sec/enterprise/knox/container/IRCPPolicy;
    if-nez v2, :cond_0

    .line 1730
    sget-object v5, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->TAG:Ljava/lang/String;

    const-string v6, " RCP policy service is not yet ready!!!"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v4, v3

    .line 1743
    .end local v3    # "retVal":Z
    .local v4, "retVal":I
    :goto_0
    return v4

    .line 1735
    .end local v4    # "retVal":I
    .restart local v3    # "retVal":Z
    :cond_0
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1736
    .local v0, "appList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v5, "Contacts"

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1737
    new-instance v5, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v6

    iget v7, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mContainerId:I

    invoke-direct {v5, v6, v7}, Landroid/app/enterprise/ContextInfo;-><init>(II)V

    const-string v6, "knox-export-data"

    invoke-interface {v2, v5, v0, v6, p1}, Lcom/sec/enterprise/knox/container/IRCPPolicy;->setAllowChangeDataSyncPolicy(Landroid/app/enterprise/ContextInfo;Ljava/util/List;Ljava/lang/String;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .end local v0    # "appList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :goto_1
    move v4, v3

    .line 1743
    .restart local v4    # "retVal":I
    goto :goto_0

    .line 1739
    .end local v4    # "retVal":I
    :catch_0
    move-exception v1

    .line 1740
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v5, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Failed talking with RCP policy service: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {v1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public getBrowserPolicy()Landroid/app/enterprise/BrowserPolicy;
    .locals 8

    .prologue
    .line 734
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->getEnterpriseDeviceManager()Landroid/app/enterprise/IEnterpriseDeviceManager;

    move-result-object v1

    .line 735
    .local v1, "edmService":Landroid/app/enterprise/IEnterpriseDeviceManager;
    if-eqz v1, :cond_0

    .line 737
    :try_start_0
    invoke-interface {v1}, Landroid/app/enterprise/IEnterpriseDeviceManager;->isMigrationStateNOK()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 738
    .local v2, "stateNOK":Z
    if-eqz v2, :cond_0

    .line 739
    const/4 v3, 0x0

    .line 753
    .end local v2    # "stateNOK":Z
    :goto_0
    return-object v3

    .line 740
    :catch_0
    move-exception v0

    .line 741
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v3, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception: isMigrationStateNOK "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 744
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    iget-boolean v3, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mBrowserPolicyCreated:Z

    if-nez v3, :cond_2

    .line 745
    const-class v4, Landroid/app/enterprise/BrowserPolicy;

    monitor-enter v4

    .line 746
    :try_start_1
    iget-boolean v3, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mBrowserPolicyCreated:Z

    if-nez v3, :cond_1

    .line 747
    new-instance v3, Landroid/app/enterprise/BrowserPolicy;

    new-instance v5, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v6

    iget v7, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mContainerId:I

    invoke-direct {v5, v6, v7}, Landroid/app/enterprise/ContextInfo;-><init>(II)V

    invoke-direct {v3, v5}, Landroid/app/enterprise/BrowserPolicy;-><init>(Landroid/app/enterprise/ContextInfo;)V

    iput-object v3, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mBrowserPolicy:Landroid/app/enterprise/BrowserPolicy;

    .line 749
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mBrowserPolicyCreated:Z

    .line 751
    :cond_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 753
    :cond_2
    iget-object v3, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mBrowserPolicy:Landroid/app/enterprise/BrowserPolicy;

    goto :goto_0

    .line 751
    :catchall_0
    move-exception v3

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3
.end method

.method public getContainerApplicationPolicy()Lcom/sec/enterprise/knox/ContainerApplicationPolicy;
    .locals 6

    .prologue
    .line 1194
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->getEnterpriseDeviceManager()Landroid/app/enterprise/IEnterpriseDeviceManager;

    move-result-object v1

    .line 1195
    .local v1, "edmService":Landroid/app/enterprise/IEnterpriseDeviceManager;
    if-eqz v1, :cond_0

    .line 1197
    :try_start_0
    invoke-interface {v1}, Landroid/app/enterprise/IEnterpriseDeviceManager;->isMigrationStateNOK()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 1198
    .local v2, "stateNOK":Z
    if-eqz v2, :cond_0

    .line 1199
    const/4 v3, 0x0

    .line 1212
    .end local v2    # "stateNOK":Z
    :goto_0
    return-object v3

    .line 1200
    :catch_0
    move-exception v0

    .line 1201
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v3, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception: isMigrationStateNOK "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1204
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    iget-boolean v3, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mContainerAppPolicyCreated:Z

    if-nez v3, :cond_2

    .line 1205
    const-class v4, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;

    monitor-enter v4

    .line 1206
    :try_start_1
    iget-boolean v3, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mContainerAppPolicyCreated:Z

    if-nez v3, :cond_1

    .line 1207
    new-instance v3, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;

    iget v5, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mContainerId:I

    invoke-direct {v3, v5}, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;-><init>(I)V

    iput-object v3, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mContainerAppPolicy:Lcom/sec/enterprise/knox/ContainerApplicationPolicy;

    .line 1208
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mContainerAppPolicyCreated:Z

    .line 1210
    :cond_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1212
    :cond_2
    iget-object v3, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mContainerAppPolicy:Lcom/sec/enterprise/knox/ContainerApplicationPolicy;

    goto :goto_0

    .line 1210
    :catchall_0
    move-exception v3

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3
.end method

.method public getContainerFirewallPolicy()Lcom/sec/enterprise/knox/ContainerFirewallPolicy;
    .locals 6

    .prologue
    .line 1233
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->getEnterpriseDeviceManager()Landroid/app/enterprise/IEnterpriseDeviceManager;

    move-result-object v1

    .line 1234
    .local v1, "edmService":Landroid/app/enterprise/IEnterpriseDeviceManager;
    if-eqz v1, :cond_0

    .line 1236
    :try_start_0
    invoke-interface {v1}, Landroid/app/enterprise/IEnterpriseDeviceManager;->isMigrationStateNOK()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 1237
    .local v2, "stateNOK":Z
    if-eqz v2, :cond_0

    .line 1238
    const/4 v3, 0x0

    .line 1251
    .end local v2    # "stateNOK":Z
    :goto_0
    return-object v3

    .line 1239
    :catch_0
    move-exception v0

    .line 1240
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v3, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception: isMigrationStateNOK "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1243
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    iget-boolean v3, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mContainerFirewallPolicyCreated:Z

    if-nez v3, :cond_2

    .line 1244
    const-class v4, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;

    monitor-enter v4

    .line 1245
    :try_start_1
    iget-boolean v3, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mContainerFirewallPolicyCreated:Z

    if-nez v3, :cond_1

    .line 1246
    new-instance v3, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;

    iget v5, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mContainerId:I

    invoke-direct {v3, v5}, Lcom/sec/enterprise/knox/ContainerFirewallPolicy;-><init>(I)V

    iput-object v3, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mContainerFirewallPolicy:Lcom/sec/enterprise/knox/ContainerFirewallPolicy;

    .line 1247
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mContainerFirewallPolicyCreated:Z

    .line 1249
    :cond_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1251
    :cond_2
    iget-object v3, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mContainerFirewallPolicy:Lcom/sec/enterprise/knox/ContainerFirewallPolicy;

    goto :goto_0

    .line 1249
    :catchall_0
    move-exception v3

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3
.end method

.method public getContainerRestrictionPolicy()Lcom/sec/enterprise/knox/ContainerRestrictionPolicy;
    .locals 8

    .prologue
    .line 1273
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->getEnterpriseDeviceManager()Landroid/app/enterprise/IEnterpriseDeviceManager;

    move-result-object v1

    .line 1274
    .local v1, "edmService":Landroid/app/enterprise/IEnterpriseDeviceManager;
    if-eqz v1, :cond_0

    .line 1276
    :try_start_0
    invoke-interface {v1}, Landroid/app/enterprise/IEnterpriseDeviceManager;->isMigrationStateNOK()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 1277
    .local v2, "stateNOK":Z
    if-eqz v2, :cond_0

    .line 1278
    const/4 v3, 0x0

    .line 1292
    .end local v2    # "stateNOK":Z
    :goto_0
    return-object v3

    .line 1279
    :catch_0
    move-exception v0

    .line 1280
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v3, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception: isMigrationStateNOK "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1283
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    iget-boolean v3, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mContainerRestrictionPolicyCreated:Z

    if-nez v3, :cond_2

    .line 1284
    const-class v4, Lcom/sec/enterprise/knox/ContainerRestrictionPolicy;

    monitor-enter v4

    .line 1285
    :try_start_1
    iget-boolean v3, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mContainerRestrictionPolicyCreated:Z

    if-nez v3, :cond_1

    .line 1286
    new-instance v3, Lcom/sec/enterprise/knox/ContainerRestrictionPolicy;

    new-instance v5, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v6

    iget v7, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mContainerId:I

    invoke-direct {v5, v6, v7}, Landroid/app/enterprise/ContextInfo;-><init>(II)V

    invoke-direct {v3, v5}, Lcom/sec/enterprise/knox/ContainerRestrictionPolicy;-><init>(Landroid/app/enterprise/ContextInfo;)V

    iput-object v3, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mContainerRestrictionPolicy:Lcom/sec/enterprise/knox/ContainerRestrictionPolicy;

    .line 1288
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mContainerRestrictionPolicyCreated:Z

    .line 1290
    :cond_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1292
    :cond_2
    iget-object v3, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mContainerRestrictionPolicy:Lcom/sec/enterprise/knox/ContainerRestrictionPolicy;

    goto :goto_0

    .line 1290
    :catchall_0
    move-exception v3

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3
.end method

.method public getContainerVpnPolicy()Lcom/sec/enterprise/knox/ContainerVpnPolicy;
    .locals 6

    .prologue
    .line 1154
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->getEnterpriseDeviceManager()Landroid/app/enterprise/IEnterpriseDeviceManager;

    move-result-object v1

    .line 1155
    .local v1, "edmService":Landroid/app/enterprise/IEnterpriseDeviceManager;
    if-eqz v1, :cond_0

    .line 1157
    :try_start_0
    invoke-interface {v1}, Landroid/app/enterprise/IEnterpriseDeviceManager;->isMigrationStateNOK()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 1158
    .local v2, "stateNOK":Z
    if-eqz v2, :cond_0

    .line 1159
    const/4 v3, 0x0

    .line 1172
    .end local v2    # "stateNOK":Z
    :goto_0
    return-object v3

    .line 1160
    :catch_0
    move-exception v0

    .line 1161
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v3, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception: isMigrationStateNOK "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1164
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    iget-boolean v3, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mContainerVpnPolicyCreated:Z

    if-nez v3, :cond_2

    .line 1165
    const-class v4, Lcom/sec/enterprise/knox/ContainerVpnPolicy;

    monitor-enter v4

    .line 1166
    :try_start_1
    iget-boolean v3, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mContainerVpnPolicyCreated:Z

    if-nez v3, :cond_1

    .line 1167
    new-instance v3, Lcom/sec/enterprise/knox/ContainerVpnPolicy;

    iget v5, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mContainerId:I

    invoke-direct {v3, v5}, Lcom/sec/enterprise/knox/ContainerVpnPolicy;-><init>(I)V

    iput-object v3, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mContainerVpnPolicy:Lcom/sec/enterprise/knox/ContainerVpnPolicy;

    .line 1168
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mContainerVpnPolicyCreated:Z

    .line 1170
    :cond_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1172
    :cond_2
    iget-object v3, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mContainerVpnPolicy:Lcom/sec/enterprise/knox/ContainerVpnPolicy;

    goto :goto_0

    .line 1170
    :catchall_0
    move-exception v3

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3
.end method

.method public getDeviceAccountPolicy()Landroid/app/enterprise/DeviceAccountPolicy;
    .locals 8

    .prologue
    .line 890
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->getEnterpriseDeviceManager()Landroid/app/enterprise/IEnterpriseDeviceManager;

    move-result-object v1

    .line 891
    .local v1, "edmService":Landroid/app/enterprise/IEnterpriseDeviceManager;
    if-eqz v1, :cond_0

    .line 893
    :try_start_0
    invoke-interface {v1}, Landroid/app/enterprise/IEnterpriseDeviceManager;->isMigrationStateNOK()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 894
    .local v2, "stateNOK":Z
    if-eqz v2, :cond_0

    .line 895
    const/4 v3, 0x0

    .line 909
    .end local v2    # "stateNOK":Z
    :goto_0
    return-object v3

    .line 896
    :catch_0
    move-exception v0

    .line 897
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v3, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception: isMigrationStateNOK "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 900
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    iget-boolean v3, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mDeviceAccountPolicyCreated:Z

    if-nez v3, :cond_2

    .line 901
    const-class v4, Landroid/app/enterprise/DeviceAccountPolicy;

    monitor-enter v4

    .line 902
    :try_start_1
    iget-boolean v3, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mDeviceAccountPolicyCreated:Z

    if-nez v3, :cond_1

    .line 903
    new-instance v3, Landroid/app/enterprise/DeviceAccountPolicy;

    new-instance v5, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v6

    iget v7, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mContainerId:I

    invoke-direct {v5, v6, v7}, Landroid/app/enterprise/ContextInfo;-><init>(II)V

    invoke-direct {v3, v5}, Landroid/app/enterprise/DeviceAccountPolicy;-><init>(Landroid/app/enterprise/ContextInfo;)V

    iput-object v3, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mDeviceAccountPolicy:Landroid/app/enterprise/DeviceAccountPolicy;

    .line 905
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mDeviceAccountPolicyCreated:Z

    .line 907
    :cond_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 909
    :cond_2
    iget-object v3, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mDeviceAccountPolicy:Landroid/app/enterprise/DeviceAccountPolicy;

    goto :goto_0

    .line 907
    :catchall_0
    move-exception v3

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3
.end method

.method public getEmailAccountPolicy()Landroid/app/enterprise/EmailAccountPolicy;
    .locals 8

    .prologue
    .line 854
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->getEnterpriseDeviceManager()Landroid/app/enterprise/IEnterpriseDeviceManager;

    move-result-object v1

    .line 855
    .local v1, "edmService":Landroid/app/enterprise/IEnterpriseDeviceManager;
    if-eqz v1, :cond_0

    .line 857
    :try_start_0
    invoke-interface {v1}, Landroid/app/enterprise/IEnterpriseDeviceManager;->isMigrationStateNOK()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 858
    .local v2, "stateNOK":Z
    if-eqz v2, :cond_0

    .line 859
    const/4 v3, 0x0

    .line 873
    .end local v2    # "stateNOK":Z
    :goto_0
    return-object v3

    .line 860
    :catch_0
    move-exception v0

    .line 861
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v3, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception: isMigrationStateNOK "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 864
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    iget-boolean v3, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mEmailAccountPolicyCreated:Z

    if-nez v3, :cond_2

    .line 865
    const-class v4, Landroid/app/enterprise/EmailAccountPolicy;

    monitor-enter v4

    .line 866
    :try_start_1
    iget-boolean v3, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mEmailAccountPolicyCreated:Z

    if-nez v3, :cond_1

    .line 867
    new-instance v3, Landroid/app/enterprise/EmailAccountPolicy;

    new-instance v5, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v6

    iget v7, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mContainerId:I

    invoke-direct {v5, v6, v7}, Landroid/app/enterprise/ContextInfo;-><init>(II)V

    invoke-direct {v3, v5}, Landroid/app/enterprise/EmailAccountPolicy;-><init>(Landroid/app/enterprise/ContextInfo;)V

    iput-object v3, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mEmailAccountPolicy:Landroid/app/enterprise/EmailAccountPolicy;

    .line 869
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mEmailAccountPolicyCreated:Z

    .line 871
    :cond_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 873
    :cond_2
    iget-object v3, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mEmailAccountPolicy:Landroid/app/enterprise/EmailAccountPolicy;

    goto :goto_0

    .line 871
    :catchall_0
    move-exception v3

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3
.end method

.method public getEmailPolicy()Landroid/app/enterprise/EmailPolicy;
    .locals 8

    .prologue
    .line 1072
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->getEnterpriseDeviceManager()Landroid/app/enterprise/IEnterpriseDeviceManager;

    move-result-object v1

    .line 1073
    .local v1, "edmService":Landroid/app/enterprise/IEnterpriseDeviceManager;
    if-eqz v1, :cond_0

    .line 1075
    :try_start_0
    invoke-interface {v1}, Landroid/app/enterprise/IEnterpriseDeviceManager;->isMigrationStateNOK()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 1076
    .local v2, "stateNOK":Z
    if-eqz v2, :cond_0

    .line 1077
    const/4 v3, 0x0

    .line 1091
    .end local v2    # "stateNOK":Z
    :goto_0
    return-object v3

    .line 1078
    :catch_0
    move-exception v0

    .line 1079
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v3, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception: isMigrationStateNOK "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1082
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    iget-boolean v3, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mEmailPolicyCreated:Z

    if-nez v3, :cond_2

    .line 1083
    const-class v4, Landroid/app/enterprise/EmailPolicy;

    monitor-enter v4

    .line 1084
    :try_start_1
    iget-boolean v3, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mEmailPolicyCreated:Z

    if-nez v3, :cond_1

    .line 1085
    new-instance v3, Landroid/app/enterprise/EmailPolicy;

    new-instance v5, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v6

    iget v7, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mContainerId:I

    invoke-direct {v5, v6, v7}, Landroid/app/enterprise/ContextInfo;-><init>(II)V

    invoke-direct {v3, v5}, Landroid/app/enterprise/EmailPolicy;-><init>(Landroid/app/enterprise/ContextInfo;)V

    iput-object v3, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mEmailPolicy:Landroid/app/enterprise/EmailPolicy;

    .line 1087
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mEmailPolicyCreated:Z

    .line 1089
    :cond_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1091
    :cond_2
    iget-object v3, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mEmailPolicy:Landroid/app/enterprise/EmailPolicy;

    goto :goto_0

    .line 1089
    :catchall_0
    move-exception v3

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3
.end method

.method public getEnterpriseSSOPolicy()Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;
    .locals 8

    .prologue
    .line 969
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->getEnterpriseDeviceManager()Landroid/app/enterprise/IEnterpriseDeviceManager;

    move-result-object v1

    .line 970
    .local v1, "edmService":Landroid/app/enterprise/IEnterpriseDeviceManager;
    if-eqz v1, :cond_0

    .line 972
    :try_start_0
    invoke-interface {v1}, Landroid/app/enterprise/IEnterpriseDeviceManager;->isMigrationStateNOK()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 973
    .local v2, "stateNOK":Z
    if-eqz v2, :cond_0

    .line 974
    const/4 v3, 0x0

    .line 989
    .end local v2    # "stateNOK":Z
    :goto_0
    return-object v3

    .line 975
    :catch_0
    move-exception v0

    .line 976
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v3, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception: isMigrationStateNOK "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 979
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    iget-boolean v3, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mSSOPolicyCreated:Z

    if-nez v3, :cond_2

    .line 980
    const-class v4, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;

    monitor-enter v4

    .line 981
    :try_start_1
    iget-boolean v3, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mSSOPolicyCreated:Z

    if-nez v3, :cond_1

    .line 982
    new-instance v3, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;

    new-instance v5, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v6

    iget v7, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mContainerId:I

    invoke-direct {v5, v6, v7}, Landroid/app/enterprise/ContextInfo;-><init>(II)V

    iget v6, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mContainerId:I

    invoke-direct {v3, v5, v6}, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;-><init>(Landroid/app/enterprise/ContextInfo;I)V

    iput-object v3, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mSSOPolicy:Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;

    .line 984
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mSSOPolicyCreated:Z

    .line 985
    sget-object v3, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ContainerManager getEnterpriseSSOPolicy mSSOPolicy = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mSSOPolicy:Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 987
    :cond_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 989
    :cond_2
    iget-object v3, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mSSOPolicy:Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;

    goto :goto_0

    .line 987
    :catchall_0
    move-exception v3

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3
.end method

.method public getEnterpriseSSOPolicy(Ljava/lang/String;)Lcom/sec/enterprise/knox/EnterpriseSSOResponse;
    .locals 11
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    const/4 v10, 0x0

    const/4 v9, -0x1

    .line 1010
    invoke-static {}, Landroid/os/PersonaManager;->getKnoxInfo()Landroid/os/Bundle;

    move-result-object v0

    .line 1011
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v6, "2.0"

    const-string v7, "version"

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1012
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->getEnterpriseDeviceManager()Landroid/app/enterprise/IEnterpriseDeviceManager;

    move-result-object v2

    .line 1013
    .local v2, "edmService":Landroid/app/enterprise/IEnterpriseDeviceManager;
    if-eqz v2, :cond_1

    .line 1015
    :try_start_0
    invoke-interface {v2}, Landroid/app/enterprise/IEnterpriseDeviceManager;->isMigrationStateNOK()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    .line 1016
    .local v4, "stateNOK":Z
    if-eqz v4, :cond_1

    move-object v3, v5

    .line 1053
    .end local v2    # "edmService":Landroid/app/enterprise/IEnterpriseDeviceManager;
    .end local v4    # "stateNOK":Z
    :cond_0
    :goto_0
    return-object v3

    .line 1018
    .restart local v2    # "edmService":Landroid/app/enterprise/IEnterpriseDeviceManager;
    :catch_0
    move-exception v1

    .line 1019
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v6, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Exception: isMigrationStateNOK "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {v1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1023
    .end local v1    # "e":Landroid/os/RemoteException;
    .end local v2    # "edmService":Landroid/app/enterprise/IEnterpriseDeviceManager;
    :cond_1
    new-instance v3, Lcom/sec/enterprise/knox/EnterpriseSSOResponse;

    invoke-direct {v3}, Lcom/sec/enterprise/knox/EnterpriseSSOResponse;-><init>()V

    .line 1024
    .local v3, "mSSOResponse":Lcom/sec/enterprise/knox/EnterpriseSSOResponse;
    invoke-virtual {v3, v9}, Lcom/sec/enterprise/knox/EnterpriseSSOResponse;->SetError(I)V

    .line 1025
    invoke-virtual {v3, v5}, Lcom/sec/enterprise/knox/EnterpriseSSOResponse;->SetSSOPolicy(Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;)V

    .line 1027
    if-eqz p1, :cond_0

    const-string v5, "com.centrify.sso.samsung"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1030
    iget-boolean v5, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mSSOPolicyCreated:Z

    if-nez v5, :cond_4

    .line 1031
    const-class v6, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;

    monitor-enter v6

    .line 1032
    :try_start_1
    iget-boolean v5, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mSSOPolicyCreated:Z

    if-nez v5, :cond_2

    .line 1033
    new-instance v5, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;

    new-instance v7, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v8

    iget v9, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mContainerId:I

    invoke-direct {v7, v8, v9}, Landroid/app/enterprise/ContextInfo;-><init>(II)V

    iget v8, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mContainerId:I

    invoke-direct {v5, v7, v8, p1}, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;-><init>(Landroid/app/enterprise/ContextInfo;ILjava/lang/String;)V

    iput-object v5, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mSSOPolicy:Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;

    .line 1035
    iget-object v5, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mSSOPolicy:Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;

    if-eqz v5, :cond_3

    .line 1036
    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Lcom/sec/enterprise/knox/EnterpriseSSOResponse;->SetError(I)V

    .line 1037
    iget-object v5, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mSSOPolicy:Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;

    invoke-virtual {v3, v5}, Lcom/sec/enterprise/knox/EnterpriseSSOResponse;->SetSSOPolicy(Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;)V

    .line 1038
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mSSOPolicyCreated:Z

    .line 1044
    :goto_1
    sget-object v5, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "ContainerManager getEnterpriseSSOPolicy mSSOPolicy = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mSSOPolicy:Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1046
    :cond_2
    monitor-exit v6

    goto :goto_0

    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v5

    .line 1040
    :cond_3
    const/4 v5, -0x1

    :try_start_2
    invoke-virtual {v3, v5}, Lcom/sec/enterprise/knox/EnterpriseSSOResponse;->SetError(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 1048
    :cond_4
    iget-object v5, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mSSOPolicy:Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;

    if-eqz v5, :cond_0

    .line 1049
    invoke-virtual {v3, v10}, Lcom/sec/enterprise/knox/EnterpriseSSOResponse;->SetError(I)V

    .line 1050
    iget-object v5, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mSSOPolicy:Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;

    invoke-virtual {v3, v5}, Lcom/sec/enterprise/knox/EnterpriseSSOResponse;->SetSSOPolicy(Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;)V

    goto/16 :goto_0
.end method

.method public getExchangeAccountPolicy()Landroid/app/enterprise/ExchangeAccountPolicy;
    .locals 8

    .prologue
    .line 816
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->getEnterpriseDeviceManager()Landroid/app/enterprise/IEnterpriseDeviceManager;

    move-result-object v1

    .line 817
    .local v1, "edmService":Landroid/app/enterprise/IEnterpriseDeviceManager;
    if-eqz v1, :cond_0

    .line 819
    :try_start_0
    invoke-interface {v1}, Landroid/app/enterprise/IEnterpriseDeviceManager;->isMigrationStateNOK()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 820
    .local v2, "stateNOK":Z
    if-eqz v2, :cond_0

    .line 821
    const/4 v3, 0x0

    .line 835
    .end local v2    # "stateNOK":Z
    :goto_0
    return-object v3

    .line 822
    :catch_0
    move-exception v0

    .line 823
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v3, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception: isMigrationStateNOK "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 826
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    iget-boolean v3, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mEasAccountPolicyCreated:Z

    if-nez v3, :cond_2

    .line 827
    const-class v4, Landroid/app/enterprise/ExchangeAccountPolicy;

    monitor-enter v4

    .line 828
    :try_start_1
    iget-boolean v3, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mEasAccountPolicyCreated:Z

    if-nez v3, :cond_1

    .line 829
    new-instance v3, Landroid/app/enterprise/ExchangeAccountPolicy;

    new-instance v5, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v6

    iget v7, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mContainerId:I

    invoke-direct {v5, v6, v7}, Landroid/app/enterprise/ContextInfo;-><init>(II)V

    invoke-direct {v3, v5}, Landroid/app/enterprise/ExchangeAccountPolicy;-><init>(Landroid/app/enterprise/ContextInfo;)V

    iput-object v3, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mEasAccountPolicy:Landroid/app/enterprise/ExchangeAccountPolicy;

    .line 831
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mEasAccountPolicyCreated:Z

    .line 833
    :cond_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 835
    :cond_2
    iget-object v3, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mEasAccountPolicy:Landroid/app/enterprise/ExchangeAccountPolicy;

    goto :goto_0

    .line 833
    :catchall_0
    move-exception v3

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3
.end method

.method public getPasswordEnabledContainerLockTimeout()J
    .locals 2

    .prologue
    .line 1684
    sget-object v0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->TAG:Ljava/lang/String;

    const-string v1, "ContainerPolicy Service API deprecated!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1685
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getPasswordPolicy()Lcom/sec/enterprise/knox/ContainerPasswordPolicy;
    .locals 8

    .prologue
    .line 930
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->getEnterpriseDeviceManager()Landroid/app/enterprise/IEnterpriseDeviceManager;

    move-result-object v1

    .line 931
    .local v1, "edmService":Landroid/app/enterprise/IEnterpriseDeviceManager;
    if-eqz v1, :cond_0

    .line 933
    :try_start_0
    invoke-interface {v1}, Landroid/app/enterprise/IEnterpriseDeviceManager;->isMigrationStateNOK()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 934
    .local v2, "stateNOK":Z
    if-eqz v2, :cond_0

    .line 935
    const/4 v3, 0x0

    .line 949
    .end local v2    # "stateNOK":Z
    :goto_0
    return-object v3

    .line 936
    :catch_0
    move-exception v0

    .line 937
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v3, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception: isMigrationStateNOK "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 940
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    iget-boolean v3, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mPasswordPolicyCreated:Z

    if-nez v3, :cond_2

    .line 941
    const-class v4, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;

    monitor-enter v4

    .line 942
    :try_start_1
    iget-boolean v3, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mPasswordPolicyCreated:Z

    if-nez v3, :cond_1

    .line 943
    new-instance v3, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;

    new-instance v5, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v6

    iget v7, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mContainerId:I

    invoke-direct {v5, v6, v7}, Landroid/app/enterprise/ContextInfo;-><init>(II)V

    invoke-direct {v3, v5}, Lcom/sec/enterprise/knox/ContainerPasswordPolicy;-><init>(Landroid/app/enterprise/ContextInfo;)V

    iput-object v3, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mPasswordPolicy:Lcom/sec/enterprise/knox/ContainerPasswordPolicy;

    .line 945
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mPasswordPolicyCreated:Z

    .line 947
    :cond_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 949
    :cond_2
    iget-object v3, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mPasswordPolicy:Lcom/sec/enterprise/knox/ContainerPasswordPolicy;

    goto :goto_0

    .line 947
    :catchall_0
    move-exception v3

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3
.end method

.method public getSmartCardBrowserPolicy(Landroid/content/Context;)Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;
    .locals 9
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 774
    invoke-static {}, Landroid/os/PersonaManager;->getKnoxInfo()Landroid/os/Bundle;

    move-result-object v0

    .line 775
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v4, "2.0"

    const-string v5, "version"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 776
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->getEnterpriseDeviceManager()Landroid/app/enterprise/IEnterpriseDeviceManager;

    move-result-object v2

    .line 777
    .local v2, "edmService":Landroid/app/enterprise/IEnterpriseDeviceManager;
    if-eqz v2, :cond_0

    .line 779
    :try_start_0
    invoke-interface {v2}, Landroid/app/enterprise/IEnterpriseDeviceManager;->isMigrationStateNOK()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 780
    .local v3, "stateNOK":Z
    if-eqz v3, :cond_0

    .line 781
    const/4 v4, 0x0

    .line 796
    .end local v2    # "edmService":Landroid/app/enterprise/IEnterpriseDeviceManager;
    .end local v3    # "stateNOK":Z
    :goto_0
    return-object v4

    .line 782
    .restart local v2    # "edmService":Landroid/app/enterprise/IEnterpriseDeviceManager;
    :catch_0
    move-exception v1

    .line 783
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Exception: isMigrationStateNOK "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 787
    .end local v1    # "e":Landroid/os/RemoteException;
    .end local v2    # "edmService":Landroid/app/enterprise/IEnterpriseDeviceManager;
    :cond_0
    iget-boolean v4, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mSCBrowserPolicyCreated:Z

    if-nez v4, :cond_2

    .line 788
    const-class v5, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;

    monitor-enter v5

    .line 789
    :try_start_1
    iget-boolean v4, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mSCBrowserPolicyCreated:Z

    if-nez v4, :cond_1

    .line 790
    new-instance v4, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;

    new-instance v6, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v7

    iget v8, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mContainerId:I

    invoke-direct {v6, v7, v8}, Landroid/app/enterprise/ContextInfo;-><init>(II)V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v4, v6, v7}, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    iput-object v4, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mSCBrowserPolicy:Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;

    .line 792
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mSCBrowserPolicyCreated:Z

    .line 794
    :cond_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 796
    :cond_2
    iget-object v4, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mSCBrowserPolicy:Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;

    goto :goto_0

    .line 794
    :catchall_0
    move-exception v4

    :try_start_2
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v4
.end method

.method public getSmartCardEmailPolicy(Landroid/content/Context;)Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;
    .locals 9
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1112
    invoke-static {}, Landroid/os/PersonaManager;->getKnoxInfo()Landroid/os/Bundle;

    move-result-object v0

    .line 1113
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v4, "2.0"

    const-string v5, "version"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1114
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->getEnterpriseDeviceManager()Landroid/app/enterprise/IEnterpriseDeviceManager;

    move-result-object v2

    .line 1115
    .local v2, "edmService":Landroid/app/enterprise/IEnterpriseDeviceManager;
    if-eqz v2, :cond_0

    .line 1117
    :try_start_0
    invoke-interface {v2}, Landroid/app/enterprise/IEnterpriseDeviceManager;->isMigrationStateNOK()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 1118
    .local v3, "stateNOK":Z
    if-eqz v3, :cond_0

    .line 1119
    const/4 v4, 0x0

    .line 1134
    .end local v2    # "edmService":Landroid/app/enterprise/IEnterpriseDeviceManager;
    .end local v3    # "stateNOK":Z
    :goto_0
    return-object v4

    .line 1120
    .restart local v2    # "edmService":Landroid/app/enterprise/IEnterpriseDeviceManager;
    :catch_0
    move-exception v1

    .line 1121
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Exception: isMigrationStateNOK "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1125
    .end local v1    # "e":Landroid/os/RemoteException;
    .end local v2    # "edmService":Landroid/app/enterprise/IEnterpriseDeviceManager;
    :cond_0
    iget-boolean v4, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mSCEmailPolicyCreated:Z

    if-nez v4, :cond_2

    .line 1126
    const-class v5, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;

    monitor-enter v5

    .line 1127
    :try_start_1
    iget-boolean v4, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mSCEmailPolicyCreated:Z

    if-nez v4, :cond_1

    .line 1128
    new-instance v4, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;

    new-instance v6, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v7

    iget v8, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mContainerId:I

    invoke-direct {v6, v7, v8}, Landroid/app/enterprise/ContextInfo;-><init>(II)V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v4, v6, v7}, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    iput-object v4, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mSCEmailPolicy:Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;

    .line 1130
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mSCEmailPolicyCreated:Z

    .line 1132
    :cond_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1134
    :cond_2
    iget-object v4, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mSCEmailPolicy:Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;

    goto :goto_0

    .line 1132
    :catchall_0
    move-exception v4

    :try_start_2
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v4
.end method

.method public getStatus()I
    .locals 1

    .prologue
    .line 1331
    iget v0, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mContainerId:I

    invoke-static {v0}, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->getContainerStatus(I)I

    move-result v0

    return v0
.end method

.method public isContactInfoToNonKnoxAllowed()Z
    .locals 7

    .prologue
    .line 1770
    new-instance v4, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v5

    invoke-direct {v4, v5}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const-string v5, "EnterpriseContainerManager.isContactInfoToNonKnoxAllowed"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1772
    const/4 v2, 0x0

    .line 1773
    .local v2, "retVal":Z
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->getRCPPolicyService()Lcom/sec/enterprise/knox/container/IRCPPolicy;

    move-result-object v1

    .line 1774
    .local v1, "rcpService":Lcom/sec/enterprise/knox/container/IRCPPolicy;
    if-nez v1, :cond_0

    .line 1775
    sget-object v4, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->TAG:Ljava/lang/String;

    const-string v5, " RCP policy service is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 1785
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 1780
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    new-instance v4, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v5

    iget v6, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mContainerId:I

    invoke-direct {v4, v5, v6}, Landroid/app/enterprise/ContextInfo;-><init>(II)V

    const-string v5, "Contacts"

    const-string v6, "knox-export-data"

    invoke-interface {v1, v4, v5, v6}, Lcom/sec/enterprise/knox/container/IRCPPolicy;->getAllowChangeDataSyncPolicy(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 1785
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 1782
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 1783
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed talking with RCP policy service: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public lock()Z
    .locals 7

    .prologue
    .line 1422
    new-instance v4, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v5

    invoke-direct {v4, v5}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const-string v5, "EnterpriseContainerManager.lock"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1425
    const/4 v2, 0x0

    .line 1426
    .local v2, "retVal":Z
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->getMUMContainerService()Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    move-result-object v1

    .line 1427
    .local v1, "mumService":Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    if-nez v1, :cond_0

    .line 1428
    sget-object v4, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->TAG:Ljava/lang/String;

    const-string v5, "ContainerPolicy Service is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 1439
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 1434
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    new-instance v4, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v5

    iget v6, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mContainerId:I

    invoke-direct {v4, v5, v6}, Landroid/app/enterprise/ContextInfo;-><init>(II)V

    const/4 v5, 0x0

    invoke-interface {v1, v4, v5}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager;->lockContainer(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 1439
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 1435
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 1436
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->TAG:Ljava/lang/String;

    const-string v5, "Failed at EnterpriseContainerManager API lock "

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public setPasswordEnabledContainerLockTimeout(J)Z
    .locals 2
    .param p1, "timeMs"    # J

    .prologue
    .line 1653
    new-instance v0, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    invoke-direct {v0, v1}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const-string v1, "EnterpriseContainerManager.setPasswordEnabledContainerLockTimeout"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1654
    sget-object v0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->TAG:Ljava/lang/String;

    const-string v1, "ContainerPolicy Service API deprecated!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1655
    const/4 v0, 0x0

    return v0
.end method

.method public unlock()Z
    .locals 7

    .prologue
    .line 1486
    new-instance v4, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v5

    invoke-direct {v4, v5}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const-string v5, "EnterpriseContainerManager.unlock"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1488
    const/4 v2, 0x0

    .line 1489
    .local v2, "retVal":Z
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->getMUMContainerService()Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    move-result-object v1

    .line 1490
    .local v1, "mumService":Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    if-nez v1, :cond_0

    .line 1491
    sget-object v4, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->TAG:Ljava/lang/String;

    const-string v5, "ContainerPolicy Service is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 1501
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 1496
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    new-instance v4, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v5

    iget v6, p0, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->mContainerId:I

    invoke-direct {v4, v5, v6}, Landroid/app/enterprise/ContextInfo;-><init>(II)V

    invoke-interface {v1, v4}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager;->unlockContainer(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 1501
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 1497
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 1498
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->TAG:Ljava/lang/String;

    const-string v5, "Failed at EnterpriseContainerManager API unlock "

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public verifyPassword(Ljava/lang/String;)I
    .locals 3
    .param p1, "password"    # Ljava/lang/String;

    .prologue
    .line 1538
    const/4 v0, 0x0

    .line 1539
    .local v0, "retVal":I
    sget-object v1, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->TAG:Ljava/lang/String;

    const-string v2, "WRONG BUILD OPTION: KNOX 1.0 Container deprecated "

    invoke-static {v1, v2}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 1553
    return v0
.end method
