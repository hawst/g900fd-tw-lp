.class public Lcom/sec/enterprise/knox/EnterpriseKnoxManager;
.super Ljava/lang/Object;
.source "EnterpriseKnoxManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/enterprise/knox/EnterpriseKnoxManager$1;,
        Lcom/sec/enterprise/knox/EnterpriseKnoxManager$EnterpriseKnoxSdkVersion;
    }
.end annotation


# static fields
.field public static final DEVICE_KNOXIFIED:I = 0x1

.field public static final DEVICE_NOT_KNOXIFIED:I = 0x0

.field public static final KNOX_ENTERPRISE_POLICY_SERVICE:Ljava/lang/String; = "knox_enterprise_policy"

.field private static final KNOX_VPN_V1_ENABLED:Z

.field private static final KNOX_VPN_V2_ENABLED:Z

.field private static TAG:Ljava/lang/String;

.field private static gEKM:Lcom/sec/enterprise/knox/EnterpriseKnoxManager;

.field private static final mSync:Ljava/lang/Object;


# instance fields
.field private mAdvancedRestrictionPolicy:Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;

.field private mAdvancedRestrictionPolicyCreated:Z

.field private mClientCertificateManagerPolicy:Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;

.field private mClientCertificateManagerPolicyCreated:Z

.field private final mContextInfo:Landroid/app/enterprise/ContextInfo;

.field private mEContainerMgrMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/sec/enterprise/knox/EnterpriseContainerManager;",
            ">;"
        }
    .end annotation
.end field

.field private mEnterpriseAuditLogPolicy:Lcom/sec/enterprise/knox/auditlog/AuditLog;

.field private mEnterpriseAuditLogPolicyCreated:Z

.field private mEnterpriseBillingPolicy:Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;

.field private mEnterpriseBillingPolicyCreated:Z

.field private mEnterpriseISLPolicy:Lcom/sec/enterprise/knox/EnterpriseISLPolicy;

.field private mEnterpriseISLPolicyCreated:Z

.field private mEnterprisePremiumVpnPolicy:Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;

.field private mEnterpriseVpnPolicyCreated:Z

.field private mKnoxContainerMgrMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/sec/enterprise/knox/container/KnoxContainerManager;",
            ">;>;"
        }
    .end annotation
.end field

.field private mTimaKeystorePolicy:Lcom/sec/enterprise/knox/keystore/TimaKeystore;

.field private mTimaKeystorePolicyCreated:Z

.field private mTrustedPinPadPolicy:Lcom/sec/enterprise/knox/trustedpinpad/TrustedPinPad;

.field private mTrustedPinPadPolicyCreated:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 87
    const-string v0, "EnterpriseKnoxManager"

    sput-object v0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->TAG:Ljava/lang/String;

    .line 90
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mSync:Ljava/lang/Object;

    .line 91
    const-string v0, "1"

    const-string v1, "ro.config.knox"

    const-string v2, "0"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->KNOX_VPN_V1_ENABLED:Z

    .line 92
    const-string v0, "v30"

    const-string v1, "ro.config.knox"

    const-string v2, "0"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->KNOX_VPN_V2_ENABLED:Z

    return-void
.end method

.method constructor <init>(Landroid/app/enterprise/ContextInfo;)V
    .locals 1
    .param p1, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;

    .prologue
    const/4 v0, 0x0

    .line 147
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    iput-boolean v0, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mEnterpriseBillingPolicyCreated:Z

    .line 298
    iput-boolean v0, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mEnterpriseVpnPolicyCreated:Z

    .line 302
    iput-boolean v0, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mEnterpriseISLPolicyCreated:Z

    .line 304
    iput-boolean v0, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mEnterpriseAuditLogPolicyCreated:Z

    .line 310
    iput-boolean v0, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mAdvancedRestrictionPolicyCreated:Z

    .line 312
    iput-boolean v0, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mClientCertificateManagerPolicyCreated:Z

    .line 314
    iput-boolean v0, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mTimaKeystorePolicyCreated:Z

    .line 316
    iput-boolean v0, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mTrustedPinPadPolicyCreated:Z

    .line 151
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mEContainerMgrMap:Ljava/util/HashMap;

    .line 152
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mKnoxContainerMgrMap:Ljava/util/HashMap;

    .line 153
    iput-object p1, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    .line 154
    return-void
.end method

.method private checkVpnFrameworkProperty(Ljava/lang/String;)Z
    .locals 4
    .param p1, "currentFramework"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    .line 438
    const-string v3, "1.0"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 439
    const-string v1, "2.0"

    .line 443
    .local v1, "targetFramework":Ljava/lang/String;
    :goto_0
    const-string v3, "net.vpn.framework"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 444
    .local v0, "getProperty":Ljava/lang/String;
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 449
    :cond_0
    :goto_1
    return v2

    .line 441
    .end local v0    # "getProperty":Ljava/lang/String;
    .end local v1    # "targetFramework":Ljava/lang/String;
    :cond_1
    const-string v1, "1.0"

    .restart local v1    # "targetFramework":Ljava/lang/String;
    goto :goto_0

    .line 446
    .restart local v0    # "getProperty":Ljava/lang/String;
    :cond_2
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 447
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public static createInstance(Landroid/app/enterprise/ContextInfo;)Lcom/sec/enterprise/knox/EnterpriseKnoxManager;
    .locals 1
    .param p0, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;

    .prologue
    .line 137
    new-instance v0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;

    invoke-direct {v0, p0}, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;-><init>(Landroid/app/enterprise/ContextInfo;)V

    return-object v0
.end method

.method public static getDeviceKnoxifiedState()I
    .locals 3

    .prologue
    .line 269
    const-string v0, "1"

    const-string v1, "ro.config.knoxtakeover"

    const-string v2, "0"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 270
    const/4 v0, 0x1

    .line 272
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getInstance()Lcom/sec/enterprise/knox/EnterpriseKnoxManager;
    .locals 4

    .prologue
    .line 112
    sget-object v1, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mSync:Ljava/lang/Object;

    monitor-enter v1

    .line 113
    :try_start_0
    sget-object v0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->gEKM:Lcom/sec/enterprise/knox/EnterpriseKnoxManager;

    if-nez v0, :cond_0

    .line 114
    new-instance v0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;

    new-instance v2, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v3

    invoke-direct {v2, v3}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    invoke-direct {v0, v2}, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;-><init>(Landroid/app/enterprise/ContextInfo;)V

    sput-object v0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->gEKM:Lcom/sec/enterprise/knox/EnterpriseKnoxManager;

    .line 116
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->gEKM:Lcom/sec/enterprise/knox/EnterpriseKnoxManager;

    monitor-exit v1

    return-object v0

    .line 117
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static getInstance(Landroid/app/enterprise/ContextInfo;)Lcom/sec/enterprise/knox/EnterpriseKnoxManager;
    .locals 2
    .param p0, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;

    .prologue
    .line 125
    sget-object v1, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mSync:Ljava/lang/Object;

    monitor-enter v1

    .line 126
    :try_start_0
    sget-object v0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->gEKM:Lcom/sec/enterprise/knox/EnterpriseKnoxManager;

    if-nez v0, :cond_0

    .line 127
    new-instance v0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;

    invoke-direct {v0, p0}, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;-><init>(Landroid/app/enterprise/ContextInfo;)V

    sput-object v0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->gEKM:Lcom/sec/enterprise/knox/EnterpriseKnoxManager;

    .line 129
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->gEKM:Lcom/sec/enterprise/knox/EnterpriseKnoxManager;

    monitor-exit v1

    return-object v0

    .line 130
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public getAdvancedRestrictionPolicy(Landroid/content/Context;)Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 660
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mAdvancedRestrictionPolicyCreated:Z

    if-nez v0, :cond_1

    .line 661
    const-class v1, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;

    monitor-enter v1

    .line 662
    :try_start_0
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mAdvancedRestrictionPolicyCreated:Z

    if-nez v0, :cond_0

    .line 663
    new-instance v0, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-direct {v0, v2, p1}, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mAdvancedRestrictionPolicy:Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;

    .line 664
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mAdvancedRestrictionPolicyCreated:Z

    .line 666
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 668
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mAdvancedRestrictionPolicy:Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;

    return-object v0

    .line 666
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getClientCertificateManagerPolicy(Landroid/content/Context;)Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 682
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mClientCertificateManagerPolicyCreated:Z

    if-nez v0, :cond_1

    .line 683
    const-class v1, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;

    monitor-enter v1

    .line 684
    :try_start_0
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mClientCertificateManagerPolicyCreated:Z

    if-nez v0, :cond_0

    .line 685
    new-instance v0, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;

    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-direct {v0, v2, p1}, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mClientCertificateManagerPolicy:Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;

    .line 686
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mClientCertificateManagerPolicyCreated:Z

    .line 688
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 690
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mClientCertificateManagerPolicy:Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;

    return-object v0

    .line 688
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getEnterpriseAuditLogPolicy(Landroid/content/Context;)Lcom/sec/enterprise/knox/auditlog/AuditLog;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 637
    if-nez p1, :cond_0

    .line 638
    const/4 v0, 0x0

    .line 647
    :goto_0
    return-object v0

    .line 639
    :cond_0
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mEnterpriseAuditLogPolicyCreated:Z

    if-nez v0, :cond_2

    .line 640
    const-class v1, Lcom/sec/enterprise/knox/auditlog/AuditLog;

    monitor-enter v1

    .line 641
    :try_start_0
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mEnterpriseAuditLogPolicyCreated:Z

    if-nez v0, :cond_1

    .line 642
    iget-object v0, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-static {v0, p1}, Lcom/sec/enterprise/knox/auditlog/AuditLog;->getInstance(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)Lcom/sec/enterprise/knox/auditlog/AuditLog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mEnterpriseAuditLogPolicy:Lcom/sec/enterprise/knox/auditlog/AuditLog;

    .line 643
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mEnterpriseAuditLogPolicyCreated:Z

    .line 645
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 647
    :cond_2
    iget-object v0, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mEnterpriseAuditLogPolicy:Lcom/sec/enterprise/knox/auditlog/AuditLog;

    goto :goto_0

    .line 645
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getEnterpriseBillingPolicy()Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;
    .locals 4

    .prologue
    .line 613
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mEnterpriseBillingPolicyCreated:Z

    if-nez v0, :cond_1

    .line 614
    const-class v1, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;

    monitor-enter v1

    .line 615
    :try_start_0
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mEnterpriseBillingPolicyCreated:Z

    if-nez v0, :cond_0

    .line 617
    new-instance v0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-direct {v0, v2}, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;-><init>(Landroid/app/enterprise/ContextInfo;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mEnterpriseBillingPolicy:Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;

    .line 618
    const-string v0, "EnterpriseBillingPolicy"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Added Client : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mEnterpriseBillingPolicy:Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 619
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mEnterpriseBillingPolicyCreated:Z

    .line 621
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 623
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mEnterpriseBillingPolicy:Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;

    return-object v0

    .line 621
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getEnterpriseCertEnrollPolicy(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cepProtocol"    # Ljava/lang/String;

    .prologue
    .line 747
    iget-object v0, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-static {v0, p1, p2}, Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;->getInstance(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;Ljava/lang/String;)Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized getEnterpriseContainerManager(I)Lcom/sec/enterprise/knox/EnterpriseContainerManager;
    .locals 6
    .param p1, "containerId"    # I

    .prologue
    .line 472
    monitor-enter p0

    const/4 v1, 0x0

    .line 474
    .local v1, "eContainerMgr":Lcom/sec/enterprise/knox/EnterpriseContainerManager;
    :try_start_0
    iget-object v3, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mEContainerMgrMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 475
    invoke-static {p1}, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->doesContainerExists(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 476
    iget-object v3, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mEContainerMgrMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/enterprise/knox/EnterpriseContainerManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 490
    :goto_0
    monitor-exit p0

    return-object v3

    .line 478
    :cond_0
    :try_start_1
    iget-object v3, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mEContainerMgrMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 479
    sget-object v3, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Container("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") no more exists"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 480
    const/4 v3, 0x0

    goto :goto_0

    .line 484
    :cond_1
    :try_start_2
    new-instance v2, Lcom/sec/enterprise/knox/EnterpriseContainerManager;

    invoke-direct {v2, p1}, Lcom/sec/enterprise/knox/EnterpriseContainerManager;-><init>(I)V
    :try_end_2
    .catch Ljava/lang/NoSuchFieldException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 485
    .end local v1    # "eContainerMgr":Lcom/sec/enterprise/knox/EnterpriseContainerManager;
    .local v2, "eContainerMgr":Lcom/sec/enterprise/knox/EnterpriseContainerManager;
    :try_start_3
    iget-object v3, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mEContainerMgrMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catch Ljava/lang/NoSuchFieldException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-object v1, v2

    .end local v2    # "eContainerMgr":Lcom/sec/enterprise/knox/EnterpriseContainerManager;
    .restart local v1    # "eContainerMgr":Lcom/sec/enterprise/knox/EnterpriseContainerManager;
    :goto_1
    move-object v3, v1

    .line 490
    goto :goto_0

    .line 486
    :catch_0
    move-exception v0

    .line 487
    .local v0, "e":Ljava/lang/NoSuchFieldException;
    :goto_2
    :try_start_4
    sget-object v3, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->TAG:Ljava/lang/String;

    const-string v4, "Failed at EnterpriseContainerManager API getEnterpriseContainerManager "

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 472
    .end local v0    # "e":Ljava/lang/NoSuchFieldException;
    :catchall_0
    move-exception v3

    :goto_3
    monitor-exit p0

    throw v3

    .line 486
    .end local v1    # "eContainerMgr":Lcom/sec/enterprise/knox/EnterpriseContainerManager;
    .restart local v2    # "eContainerMgr":Lcom/sec/enterprise/knox/EnterpriseContainerManager;
    :catch_1
    move-exception v0

    move-object v1, v2

    .end local v2    # "eContainerMgr":Lcom/sec/enterprise/knox/EnterpriseContainerManager;
    .restart local v1    # "eContainerMgr":Lcom/sec/enterprise/knox/EnterpriseContainerManager;
    goto :goto_2

    .line 472
    .end local v1    # "eContainerMgr":Lcom/sec/enterprise/knox/EnterpriseContainerManager;
    .restart local v2    # "eContainerMgr":Lcom/sec/enterprise/knox/EnterpriseContainerManager;
    :catchall_1
    move-exception v3

    move-object v1, v2

    .end local v2    # "eContainerMgr":Lcom/sec/enterprise/knox/EnterpriseContainerManager;
    .restart local v1    # "eContainerMgr":Lcom/sec/enterprise/knox/EnterpriseContainerManager;
    goto :goto_3
.end method

.method public getEnterpriseISLPolicy()Lcom/sec/enterprise/knox/EnterpriseISLPolicy;
    .locals 3

    .prologue
    .line 593
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mEnterpriseISLPolicyCreated:Z

    if-nez v0, :cond_1

    .line 594
    const-class v1, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;

    monitor-enter v1

    .line 595
    :try_start_0
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mEnterpriseISLPolicyCreated:Z

    if-nez v0, :cond_0

    .line 596
    new-instance v0, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-direct {v0, v2}, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;-><init>(Landroid/app/enterprise/ContextInfo;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mEnterpriseISLPolicy:Lcom/sec/enterprise/knox/EnterpriseISLPolicy;

    .line 597
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mEnterpriseISLPolicyCreated:Z

    .line 599
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 601
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mEnterpriseISLPolicy:Lcom/sec/enterprise/knox/EnterpriseISLPolicy;

    return-object v0

    .line 599
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getEnterpriseVpnPolicy()Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;
    .locals 4

    .prologue
    .line 336
    :try_start_0
    iget-boolean v1, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mEnterpriseVpnPolicyCreated:Z

    if-nez v1, :cond_2

    .line 337
    const-string v1, "1.0"

    invoke-direct {p0, v1}, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->checkVpnFrameworkProperty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 338
    sget-object v1, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->TAG:Ljava/lang/String;

    const-string v2, "getEnterpriseVpnPolicy: Generic VpnPolicy Profiles are present in the device"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 339
    const/4 v1, 0x0

    .line 353
    :goto_0
    return-object v1

    .line 341
    :cond_0
    const-class v2, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;

    monitor-enter v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 342
    :try_start_1
    iget-boolean v1, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mEnterpriseVpnPolicyCreated:Z

    if-nez v1, :cond_1

    .line 343
    new-instance v1, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;

    invoke-direct {v1}, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;-><init>()V

    iput-object v1, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mEnterprisePremiumVpnPolicy:Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;

    .line 344
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mEnterpriseVpnPolicyCreated:Z

    .line 345
    iget-object v1, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mEnterprisePremiumVpnPolicy:Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->startMocanaService()V

    .line 347
    :cond_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 349
    :cond_2
    :try_start_2
    iget-object v1, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mEnterprisePremiumVpnPolicy:Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;

    const-string v2, "1.0"

    invoke-virtual {v1, v2}, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->setVpnFrameworkSystemProperty(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 353
    :goto_1
    iget-object v1, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mEnterprisePremiumVpnPolicy:Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;

    goto :goto_0

    .line 347
    :catchall_0
    move-exception v1

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v1
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    .line 350
    :catch_0
    move-exception v0

    .line 351
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception at getEnterpriseVpnPolicy"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public getGenericVpnPolicy(Ljava/lang/String;)Lcom/sec/enterprise/knox/GenericVpnPolicy;
    .locals 8
    .param p1, "vendorPkgName"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 406
    const-string v5, "1"

    const-string v6, "ro.config.knox"

    const-string v7, "0"

    invoke-static {v6, v7}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 407
    .local v1, "knoxvpnV1Enabled":Z
    const-string v5, "v30"

    const-string v6, "ro.config.knox"

    const-string v7, "0"

    invoke-static {v6, v7}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    .line 408
    .local v3, "mKnoxVpnV2Enabled":Z
    sget-boolean v5, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->KNOX_VPN_V1_ENABLED:Z

    if-eqz v5, :cond_2

    .line 410
    const/4 v2, 0x0

    .line 412
    .local v2, "mGenericVpnPolicy":Lcom/sec/enterprise/knox/GenericVpnPolicy;
    :try_start_0
    const-string v5, "2.0"

    invoke-direct {p0, v5}, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->checkVpnFrameworkProperty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 413
    sget-object v5, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->TAG:Ljava/lang/String;

    const-string v6, "Knox 1.1 : getGenericVpnPolicy: Enterprise Premium VpnPolicy Profiles are present in the device"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v2, v4

    .line 433
    .end local v2    # "mGenericVpnPolicy":Lcom/sec/enterprise/knox/GenericVpnPolicy;
    :cond_0
    :goto_0
    return-object v2

    .line 416
    .restart local v2    # "mGenericVpnPolicy":Lcom/sec/enterprise/knox/GenericVpnPolicy;
    :cond_1
    invoke-static {p1}, Lcom/sec/enterprise/knox/GenericVpnPolicy;->getInstance(Ljava/lang/String;)Lcom/sec/enterprise/knox/GenericVpnPolicy;

    move-result-object v2

    .line 417
    if-eqz v2, :cond_0

    .line 418
    const-string v4, "2.0"

    invoke-virtual {v2, v4}, Lcom/sec/enterprise/knox/GenericVpnPolicy;->setVpnFrameworkSystemProperty(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 420
    :catch_0
    move-exception v0

    .line 421
    .local v0, "e":Ljava/lang/Exception;
    sget-object v4, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Exception at getGenericVpnPolicy"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 424
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v2    # "mGenericVpnPolicy":Lcom/sec/enterprise/knox/GenericVpnPolicy;
    :cond_2
    sget-boolean v5, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->KNOX_VPN_V2_ENABLED:Z

    if-eqz v5, :cond_3

    .line 427
    :try_start_1
    sget-object v5, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getGenericVpnPolicy user space id : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget v7, v7, Landroid/app/enterprise/ContextInfo;->mCallerUid:I

    invoke-static {v7}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 428
    iget-object v5, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget v5, v5, Landroid/app/enterprise/ContextInfo;->mCallerUid:I

    invoke-static {v5}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v5

    invoke-virtual {p0, p1, v5}, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->getGenericVpnPolicy(Ljava/lang/String;I)Lcom/sec/enterprise/knox/GenericVpnPolicy;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v2

    goto :goto_0

    .line 429
    :catch_1
    move-exception v0

    .line 430
    .restart local v0    # "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .end local v0    # "e":Ljava/lang/Exception;
    :cond_3
    move-object v2, v4

    .line 433
    goto :goto_0
.end method

.method public getGenericVpnPolicy(Ljava/lang/String;I)Lcom/sec/enterprise/knox/GenericVpnPolicy;
    .locals 3
    .param p1, "vendorPkgName"    # Ljava/lang/String;
    .param p2, "containerId"    # I

    .prologue
    .line 375
    const-string v1, "2.0"

    invoke-direct {p0, v1}, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->checkVpnFrameworkProperty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 376
    sget-object v1, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->TAG:Ljava/lang/String;

    const-string v2, "Knox 2.0 : getGenericVpnPolicy: Enterprise Premium VpnPolicy Profiles are present in the device"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 377
    const/4 v0, 0x0

    .line 385
    :cond_0
    :goto_0
    return-object v0

    .line 379
    :cond_1
    new-instance v1, Lcom/sec/enterprise/knox/KnoxVpnContext;

    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget v2, v2, Landroid/app/enterprise/ContextInfo;->mCallerUid:I

    invoke-direct {v1, v2, p2, p1}, Lcom/sec/enterprise/knox/KnoxVpnContext;-><init>(IILjava/lang/String;)V

    invoke-static {v1}, Lcom/sec/enterprise/knox/GenericVpnPolicy;->getInstance(Lcom/sec/enterprise/knox/KnoxVpnContext;)Lcom/sec/enterprise/knox/GenericVpnPolicy;

    move-result-object v0

    .line 381
    .local v0, "mGenericVpnPolicy":Lcom/sec/enterprise/knox/GenericVpnPolicy;
    if-eqz v0, :cond_0

    .line 382
    const-string v1, "2.0"

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/GenericVpnPolicy;->setVpnFrameworkSystemProperty(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public declared-synchronized getKnoxContainerManager(Landroid/content/Context;I)Lcom/sec/enterprise/knox/container/KnoxContainerManager;
    .locals 2
    .param p1, "cxt"    # Landroid/content/Context;
    .param p2, "containerId"    # I

    .prologue
    .line 509
    monitor-enter p0

    :try_start_0
    new-instance v0, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    invoke-direct {v0, v1, p2}, Landroid/app/enterprise/ContextInfo;-><init>(II)V

    invoke-virtual {p0, p1, v0}, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->getKnoxContainerManager(Landroid/content/Context;Landroid/app/enterprise/ContextInfo;)Lcom/sec/enterprise/knox/container/KnoxContainerManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getKnoxContainerManager(Landroid/content/Context;Landroid/app/enterprise/ContextInfo;)Lcom/sec/enterprise/knox/container/KnoxContainerManager;
    .locals 10
    .param p1, "cxt"    # Landroid/content/Context;
    .param p2, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;

    .prologue
    .line 520
    monitor-enter p0

    const/4 v2, 0x0

    .line 521
    .local v2, "eContainerMgr":Lcom/sec/enterprise/knox/container/KnoxContainerManager;
    :try_start_0
    iget v0, p2, Landroid/app/enterprise/ContextInfo;->mContainerId:I

    .line 523
    .local v0, "containerId":I
    const-string v6, "persona"

    invoke-static {v6}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v6

    invoke-static {v6}, Landroid/os/IPersonaManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/IPersonaManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    .line 526
    .local v5, "persona":Landroid/os/IPersonaManager;
    if-eqz v5, :cond_1

    :try_start_1
    invoke-interface {v5, v0}, Landroid/os/IPersonaManager;->exists(I)Z

    move-result v6

    if-nez v6, :cond_1

    .line 527
    iget-object v6, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mKnoxContainerMgrMap:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 528
    iget-object v6, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mKnoxContainerMgrMap:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 530
    :cond_0
    const/4 v6, 0x0

    .line 554
    :goto_0
    monitor-exit p0

    return-object v6

    .line 532
    :catch_0
    move-exception v1

    .line 533
    .local v1, "e":Landroid/os/RemoteException;
    :try_start_2
    sget-object v6, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Container("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ") no more exists"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 536
    .end local v1    # "e":Landroid/os/RemoteException;
    :cond_1
    iget-object v6, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mKnoxContainerMgrMap:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 537
    iget-object v6, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mKnoxContainerMgrMap:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/util/Pair;

    .line 538
    .local v4, "kcm":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Lcom/sec/enterprise/knox/container/KnoxContainerManager;>;"
    iget-object v6, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iget v7, p2, Landroid/app/enterprise/ContextInfo;->mCallerUid:I

    if-ne v6, v7, :cond_2

    .line 539
    iget-object v6, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mKnoxContainerMgrMap:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/util/Pair;

    iget-object v6, v6, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v6, Lcom/sec/enterprise/knox/container/KnoxContainerManager;

    goto :goto_0

    .line 541
    :cond_2
    iget-object v6, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mKnoxContainerMgrMap:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 547
    .end local v4    # "kcm":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Lcom/sec/enterprise/knox/container/KnoxContainerManager;>;"
    :cond_3
    :try_start_3
    new-instance v3, Lcom/sec/enterprise/knox/container/KnoxContainerManager;

    invoke-direct {v3, p1, p2}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;-><init>(Landroid/content/Context;Landroid/app/enterprise/ContextInfo;)V
    :try_end_3
    .catch Ljava/lang/NoSuchFieldException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 548
    .end local v2    # "eContainerMgr":Lcom/sec/enterprise/knox/container/KnoxContainerManager;
    .local v3, "eContainerMgr":Lcom/sec/enterprise/knox/container/KnoxContainerManager;
    :try_start_4
    iget-object v6, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mKnoxContainerMgrMap:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    new-instance v8, Landroid/util/Pair;

    iget v9, p2, Landroid/app/enterprise/ContextInfo;->mCallerUid:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-direct {v8, v9, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_4
    .catch Ljava/lang/NoSuchFieldException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-object v2, v3

    .end local v3    # "eContainerMgr":Lcom/sec/enterprise/knox/container/KnoxContainerManager;
    .restart local v2    # "eContainerMgr":Lcom/sec/enterprise/knox/container/KnoxContainerManager;
    :goto_1
    move-object v6, v2

    .line 554
    goto :goto_0

    .line 550
    :catch_1
    move-exception v1

    .line 551
    .local v1, "e":Ljava/lang/NoSuchFieldException;
    :goto_2
    :try_start_5
    sget-object v6, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->TAG:Ljava/lang/String;

    const-string v7, "Failed at KnoxContainerManager API getKnoxContainerManager "

    invoke-static {v6, v7, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1

    .line 520
    .end local v0    # "containerId":I
    .end local v1    # "e":Ljava/lang/NoSuchFieldException;
    .end local v5    # "persona":Landroid/os/IPersonaManager;
    :catchall_0
    move-exception v6

    :goto_3
    monitor-exit p0

    throw v6

    .line 550
    .end local v2    # "eContainerMgr":Lcom/sec/enterprise/knox/container/KnoxContainerManager;
    .restart local v0    # "containerId":I
    .restart local v3    # "eContainerMgr":Lcom/sec/enterprise/knox/container/KnoxContainerManager;
    .restart local v5    # "persona":Landroid/os/IPersonaManager;
    :catch_2
    move-exception v1

    move-object v2, v3

    .end local v3    # "eContainerMgr":Lcom/sec/enterprise/knox/container/KnoxContainerManager;
    .restart local v2    # "eContainerMgr":Lcom/sec/enterprise/knox/container/KnoxContainerManager;
    goto :goto_2

    .line 520
    .end local v2    # "eContainerMgr":Lcom/sec/enterprise/knox/container/KnoxContainerManager;
    .restart local v3    # "eContainerMgr":Lcom/sec/enterprise/knox/container/KnoxContainerManager;
    :catchall_1
    move-exception v6

    move-object v2, v3

    .end local v3    # "eContainerMgr":Lcom/sec/enterprise/knox/container/KnoxContainerManager;
    .restart local v2    # "eContainerMgr":Lcom/sec/enterprise/knox/container/KnoxContainerManager;
    goto :goto_3
.end method

.method public getTimaKeystorePolicy(Landroid/content/Context;)Lcom/sec/enterprise/knox/keystore/TimaKeystore;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 704
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mTimaKeystorePolicyCreated:Z

    if-nez v0, :cond_1

    .line 705
    const-class v1, Lcom/sec/enterprise/knox/keystore/TimaKeystore;

    monitor-enter v1

    .line 706
    :try_start_0
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mTimaKeystorePolicyCreated:Z

    if-nez v0, :cond_0

    .line 707
    new-instance v0, Lcom/sec/enterprise/knox/keystore/TimaKeystore;

    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-direct {v0, v2, p1}, Lcom/sec/enterprise/knox/keystore/TimaKeystore;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mTimaKeystorePolicy:Lcom/sec/enterprise/knox/keystore/TimaKeystore;

    .line 708
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mTimaKeystorePolicyCreated:Z

    .line 710
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 712
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mTimaKeystorePolicy:Lcom/sec/enterprise/knox/keystore/TimaKeystore;

    return-object v0

    .line 710
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getTrustedPinPadPolicy(Landroid/content/Context;)Lcom/sec/enterprise/knox/trustedpinpad/TrustedPinPad;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 727
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mTrustedPinPadPolicyCreated:Z

    if-nez v0, :cond_1

    .line 728
    const-class v1, Lcom/sec/enterprise/knox/trustedpinpad/TrustedPinPad;

    monitor-enter v1

    .line 729
    :try_start_0
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mTrustedPinPadPolicyCreated:Z

    if-nez v0, :cond_0

    .line 730
    new-instance v0, Lcom/sec/enterprise/knox/trustedpinpad/TrustedPinPad;

    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-direct {v0, v2, p1}, Lcom/sec/enterprise/knox/trustedpinpad/TrustedPinPad;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mTrustedPinPadPolicy:Lcom/sec/enterprise/knox/trustedpinpad/TrustedPinPad;

    .line 731
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mTrustedPinPadPolicyCreated:Z

    .line 733
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 735
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->mTrustedPinPadPolicy:Lcom/sec/enterprise/knox/trustedpinpad/TrustedPinPad;

    return-object v0

    .line 733
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getVersion()Lcom/sec/enterprise/knox/EnterpriseKnoxManager$EnterpriseKnoxSdkVersion;
    .locals 4

    .prologue
    .line 227
    invoke-static {}, Landroid/os/PersonaManager;->getKnoxInfo()Landroid/os/Bundle;

    move-result-object v0

    .line 228
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "2.0"

    const-string v3, "version"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 229
    const-string v2, "9"

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 230
    .local v1, "knox_config_version":I
    const/16 v2, 0x9

    if-ne v1, v2, :cond_0

    .line 231
    sget-object v2, Lcom/sec/enterprise/knox/EnterpriseKnoxManager$EnterpriseKnoxSdkVersion;->KNOX_ENTERPRISE_SDK_VERSION_2_3:Lcom/sec/enterprise/knox/EnterpriseKnoxManager$EnterpriseKnoxSdkVersion;

    .line 239
    .end local v1    # "knox_config_version":I
    :goto_0
    return-object v2

    .line 232
    .restart local v1    # "knox_config_version":I
    :cond_0
    const/16 v2, 0x8

    if-ne v1, v2, :cond_1

    .line 233
    sget-object v2, Lcom/sec/enterprise/knox/EnterpriseKnoxManager$EnterpriseKnoxSdkVersion;->KNOX_ENTERPRISE_SDK_VERSION_2_2:Lcom/sec/enterprise/knox/EnterpriseKnoxManager$EnterpriseKnoxSdkVersion;

    goto :goto_0

    .line 235
    :cond_1
    sget-object v2, Lcom/sec/enterprise/knox/EnterpriseKnoxManager$EnterpriseKnoxSdkVersion;->KNOX_ENTERPRISE_SDK_VERSION_2_1:Lcom/sec/enterprise/knox/EnterpriseKnoxManager$EnterpriseKnoxSdkVersion;

    goto :goto_0

    .line 236
    .end local v1    # "knox_config_version":I
    :cond_2
    const-string v2, "1.0"

    const-string v3, "version"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 237
    sget-object v2, Lcom/sec/enterprise/knox/EnterpriseKnoxManager$EnterpriseKnoxSdkVersion;->KNOX_ENTERPRISE_SDK_VERSION_1_2_0:Lcom/sec/enterprise/knox/EnterpriseKnoxManager$EnterpriseKnoxSdkVersion;

    goto :goto_0

    .line 239
    :cond_3
    sget-object v2, Lcom/sec/enterprise/knox/EnterpriseKnoxManager$EnterpriseKnoxSdkVersion;->KNOX_ENTERPRISE_SDK_VERSION_NONE:Lcom/sec/enterprise/knox/EnterpriseKnoxManager$EnterpriseKnoxSdkVersion;

    goto :goto_0
.end method
