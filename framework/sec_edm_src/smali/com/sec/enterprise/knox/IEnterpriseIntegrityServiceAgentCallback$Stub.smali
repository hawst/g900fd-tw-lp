.class public abstract Lcom/sec/enterprise/knox/IEnterpriseIntegrityServiceAgentCallback$Stub;
.super Landroid/os/Binder;
.source "IEnterpriseIntegrityServiceAgentCallback.java"

# interfaces
.implements Lcom/sec/enterprise/knox/IEnterpriseIntegrityServiceAgentCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/enterprise/knox/IEnterpriseIntegrityServiceAgentCallback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/enterprise/knox/IEnterpriseIntegrityServiceAgentCallback$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.sec.enterprise.knox.IEnterpriseIntegrityServiceAgentCallback"

.field static final TRANSACTION_checkResult:I = 0x2

.field static final TRANSACTION_commit:I = 0x11

.field static final TRANSACTION_delete3rdPartyBaseLine:I = 0x10

.field static final TRANSACTION_delete3rdPartyBaseline:I = 0x16

.field static final TRANSACTION_deletePlatformBaseLine:I = 0xc

.field static final TRANSACTION_deletePreBaseLine:I = 0x8

.field static final TRANSACTION_getPackageInfo:I = 0x15

.field static final TRANSACTION_getPackageList:I = 0x14

.field static final TRANSACTION_getVersion:I = 0x13

.field static final TRANSACTION_performPlatformScan:I = 0x1

.field static final TRANSACTION_progress:I = 0x5

.field static final TRANSACTION_read3rdPartyBaseLine:I = 0xf

.field static final TRANSACTION_readPlatformBaseLine:I = 0xb

.field static final TRANSACTION_readPreBaseLine:I = 0x7

.field static final TRANSACTION_reportError:I = 0x3

.field static final TRANSACTION_reportSuccess:I = 0x4

.field static final TRANSACTION_rollback:I = 0x12

.field static final TRANSACTION_store3rdPartyBaseLine:I = 0xd

.field static final TRANSACTION_storePlatformBaseLine:I = 0x9

.field static final TRANSACTION_storePreBaseLine:I = 0x6

.field static final TRANSACTION_update3rdPartyBaseLine:I = 0xe

.field static final TRANSACTION_updatePlatformBaseLine:I = 0xa


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 15
    const-string v0, "com.sec.enterprise.knox.IEnterpriseIntegrityServiceAgentCallback"

    invoke-virtual {p0, p0, v0}, Lcom/sec/enterprise/knox/IEnterpriseIntegrityServiceAgentCallback$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 16
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/sec/enterprise/knox/IEnterpriseIntegrityServiceAgentCallback;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 23
    if-nez p0, :cond_0

    .line 24
    const/4 v0, 0x0

    .line 30
    :goto_0
    return-object v0

    .line 26
    :cond_0
    const-string v1, "com.sec.enterprise.knox.IEnterpriseIntegrityServiceAgentCallback"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 27
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/sec/enterprise/knox/IEnterpriseIntegrityServiceAgentCallback;

    if-eqz v1, :cond_1

    .line 28
    check-cast v0, Lcom/sec/enterprise/knox/IEnterpriseIntegrityServiceAgentCallback;

    goto :goto_0

    .line 30
    :cond_1
    new-instance v0, Lcom/sec/enterprise/knox/IEnterpriseIntegrityServiceAgentCallback$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/sec/enterprise/knox/IEnterpriseIntegrityServiceAgentCallback$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 11
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 38
    sparse-switch p1, :sswitch_data_0

    .line 264
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v0

    :goto_0
    return v0

    .line 42
    :sswitch_0
    const-string v0, "com.sec.enterprise.knox.IEnterpriseIntegrityServiceAgentCallback"

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 43
    const/4 v0, 0x1

    goto :goto_0

    .line 47
    :sswitch_1
    const-string v0, "com.sec.enterprise.knox.IEnterpriseIntegrityServiceAgentCallback"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 48
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/IEnterpriseIntegrityServiceAgentCallback$Stub;->performPlatformScan()[B

    move-result-object v8

    .line 49
    .local v8, "_result":[B
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 50
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 51
    const/4 v0, 0x1

    goto :goto_0

    .line 55
    .end local v8    # "_result":[B
    :sswitch_2
    const-string v0, "com.sec.enterprise.knox.IEnterpriseIntegrityServiceAgentCallback"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 57
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 59
    .local v1, "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 61
    .local v2, "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 63
    .local v3, "_arg2":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v4

    .line 65
    .local v4, "_arg3":[B
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v5

    .line 67
    .local v5, "_arg4":[B
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v6

    .local v6, "_arg5":I
    move-object v0, p0

    .line 68
    invoke-virtual/range {v0 .. v6}, Lcom/sec/enterprise/knox/IEnterpriseIntegrityServiceAgentCallback$Stub;->checkResult(IILjava/lang/String;[B[BI)V

    .line 69
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 70
    const/4 v0, 0x1

    goto :goto_0

    .line 74
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    .end local v3    # "_arg2":Ljava/lang/String;
    .end local v4    # "_arg3":[B
    .end local v5    # "_arg4":[B
    .end local v6    # "_arg5":I
    :sswitch_3
    const-string v0, "com.sec.enterprise.knox.IEnterpriseIntegrityServiceAgentCallback"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 76
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 78
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 80
    .restart local v2    # "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 82
    .restart local v3    # "_arg2":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 83
    .local v4, "_arg3":I
    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/sec/enterprise/knox/IEnterpriseIntegrityServiceAgentCallback$Stub;->reportError(IILjava/lang/String;I)V

    .line 84
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 85
    const/4 v0, 0x1

    goto :goto_0

    .line 89
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    .end local v3    # "_arg2":Ljava/lang/String;
    .end local v4    # "_arg3":I
    :sswitch_4
    const-string v0, "com.sec.enterprise.knox.IEnterpriseIntegrityServiceAgentCallback"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 91
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 92
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/sec/enterprise/knox/IEnterpriseIntegrityServiceAgentCallback$Stub;->reportSuccess(I)V

    .line 93
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 94
    const/4 v0, 0x1

    goto :goto_0

    .line 98
    .end local v1    # "_arg0":I
    :sswitch_5
    const-string v0, "com.sec.enterprise.knox.IEnterpriseIntegrityServiceAgentCallback"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 100
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 102
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 103
    .restart local v2    # "_arg1":I
    invoke-virtual {p0, v1, v2}, Lcom/sec/enterprise/knox/IEnterpriseIntegrityServiceAgentCallback$Stub;->progress(II)V

    .line 104
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 105
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 109
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    :sswitch_6
    const-string v0, "com.sec.enterprise.knox.IEnterpriseIntegrityServiceAgentCallback"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 111
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v1

    .line 112
    .local v1, "_arg0":[B
    invoke-virtual {p0, v1}, Lcom/sec/enterprise/knox/IEnterpriseIntegrityServiceAgentCallback$Stub;->storePreBaseLine([B)Z

    move-result v8

    .line 113
    .local v8, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 114
    if-eqz v8, :cond_0

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 115
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 114
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 119
    .end local v1    # "_arg0":[B
    .end local v8    # "_result":Z
    :sswitch_7
    const-string v0, "com.sec.enterprise.knox.IEnterpriseIntegrityServiceAgentCallback"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 120
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/IEnterpriseIntegrityServiceAgentCallback$Stub;->readPreBaseLine()[B

    move-result-object v8

    .line 121
    .local v8, "_result":[B
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 122
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 123
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 127
    .end local v8    # "_result":[B
    :sswitch_8
    const-string v0, "com.sec.enterprise.knox.IEnterpriseIntegrityServiceAgentCallback"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 128
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/IEnterpriseIntegrityServiceAgentCallback$Stub;->deletePreBaseLine()Z

    move-result v8

    .line 129
    .local v8, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 130
    if-eqz v8, :cond_1

    const/4 v0, 0x1

    :goto_2
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 131
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 130
    :cond_1
    const/4 v0, 0x0

    goto :goto_2

    .line 135
    .end local v8    # "_result":Z
    :sswitch_9
    const-string v0, "com.sec.enterprise.knox.IEnterpriseIntegrityServiceAgentCallback"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 137
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v1

    .line 138
    .restart local v1    # "_arg0":[B
    invoke-virtual {p0, v1}, Lcom/sec/enterprise/knox/IEnterpriseIntegrityServiceAgentCallback$Stub;->storePlatformBaseLine([B)Z

    move-result v8

    .line 139
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 140
    if-eqz v8, :cond_2

    const/4 v0, 0x1

    :goto_3
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 141
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 140
    :cond_2
    const/4 v0, 0x0

    goto :goto_3

    .line 145
    .end local v1    # "_arg0":[B
    .end local v8    # "_result":Z
    :sswitch_a
    const-string v0, "com.sec.enterprise.knox.IEnterpriseIntegrityServiceAgentCallback"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 147
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v1

    .line 148
    .restart local v1    # "_arg0":[B
    invoke-virtual {p0, v1}, Lcom/sec/enterprise/knox/IEnterpriseIntegrityServiceAgentCallback$Stub;->updatePlatformBaseLine([B)Z

    move-result v8

    .line 149
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 150
    if-eqz v8, :cond_3

    const/4 v0, 0x1

    :goto_4
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 151
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 150
    :cond_3
    const/4 v0, 0x0

    goto :goto_4

    .line 155
    .end local v1    # "_arg0":[B
    .end local v8    # "_result":Z
    :sswitch_b
    const-string v0, "com.sec.enterprise.knox.IEnterpriseIntegrityServiceAgentCallback"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 156
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/IEnterpriseIntegrityServiceAgentCallback$Stub;->readPlatformBaseLine()[B

    move-result-object v8

    .line 157
    .local v8, "_result":[B
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 158
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 159
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 163
    .end local v8    # "_result":[B
    :sswitch_c
    const-string v0, "com.sec.enterprise.knox.IEnterpriseIntegrityServiceAgentCallback"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 164
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/IEnterpriseIntegrityServiceAgentCallback$Stub;->deletePlatformBaseLine()Z

    move-result v8

    .line 165
    .local v8, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 166
    if-eqz v8, :cond_4

    const/4 v0, 0x1

    :goto_5
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 167
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 166
    :cond_4
    const/4 v0, 0x0

    goto :goto_5

    .line 171
    .end local v8    # "_result":Z
    :sswitch_d
    const-string v0, "com.sec.enterprise.knox.IEnterpriseIntegrityServiceAgentCallback"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 173
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 175
    .local v1, "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v2

    .line 176
    .local v2, "_arg1":[B
    invoke-virtual {p0, v1, v2}, Lcom/sec/enterprise/knox/IEnterpriseIntegrityServiceAgentCallback$Stub;->store3rdPartyBaseLine(Ljava/lang/String;[B)Z

    move-result v8

    .line 177
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 178
    if-eqz v8, :cond_5

    const/4 v0, 0x1

    :goto_6
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 179
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 178
    :cond_5
    const/4 v0, 0x0

    goto :goto_6

    .line 183
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":[B
    .end local v8    # "_result":Z
    :sswitch_e
    const-string v0, "com.sec.enterprise.knox.IEnterpriseIntegrityServiceAgentCallback"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 185
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 187
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v2

    .line 188
    .restart local v2    # "_arg1":[B
    invoke-virtual {p0, v1, v2}, Lcom/sec/enterprise/knox/IEnterpriseIntegrityServiceAgentCallback$Stub;->update3rdPartyBaseLine(Ljava/lang/String;[B)Z

    move-result v8

    .line 189
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 190
    if-eqz v8, :cond_6

    const/4 v0, 0x1

    :goto_7
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 191
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 190
    :cond_6
    const/4 v0, 0x0

    goto :goto_7

    .line 195
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":[B
    .end local v8    # "_result":Z
    :sswitch_f
    const-string v0, "com.sec.enterprise.knox.IEnterpriseIntegrityServiceAgentCallback"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 197
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 198
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/sec/enterprise/knox/IEnterpriseIntegrityServiceAgentCallback$Stub;->read3rdPartyBaseLine(Ljava/lang/String;)[B

    move-result-object v8

    .line 199
    .local v8, "_result":[B
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 200
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 201
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 205
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v8    # "_result":[B
    :sswitch_10
    const-string v0, "com.sec.enterprise.knox.IEnterpriseIntegrityServiceAgentCallback"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 207
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 208
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/sec/enterprise/knox/IEnterpriseIntegrityServiceAgentCallback$Stub;->delete3rdPartyBaseLine(Ljava/lang/String;)Z

    move-result v8

    .line 209
    .local v8, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 210
    if-eqz v8, :cond_7

    const/4 v0, 0x1

    :goto_8
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 211
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 210
    :cond_7
    const/4 v0, 0x0

    goto :goto_8

    .line 215
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v8    # "_result":Z
    :sswitch_11
    const-string v0, "com.sec.enterprise.knox.IEnterpriseIntegrityServiceAgentCallback"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 216
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/IEnterpriseIntegrityServiceAgentCallback$Stub;->commit()Z

    move-result v8

    .line 217
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 218
    if-eqz v8, :cond_8

    const/4 v0, 0x1

    :goto_9
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 219
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 218
    :cond_8
    const/4 v0, 0x0

    goto :goto_9

    .line 223
    .end local v8    # "_result":Z
    :sswitch_12
    const-string v0, "com.sec.enterprise.knox.IEnterpriseIntegrityServiceAgentCallback"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 224
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/IEnterpriseIntegrityServiceAgentCallback$Stub;->rollback()Z

    move-result v8

    .line 225
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 226
    if-eqz v8, :cond_9

    const/4 v0, 0x1

    :goto_a
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 227
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 226
    :cond_9
    const/4 v0, 0x0

    goto :goto_a

    .line 231
    .end local v8    # "_result":Z
    :sswitch_13
    const-string v0, "com.sec.enterprise.knox.IEnterpriseIntegrityServiceAgentCallback"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 232
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/IEnterpriseIntegrityServiceAgentCallback$Stub;->getVersion()Ljava/lang/String;

    move-result-object v8

    .line 233
    .local v8, "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 234
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 235
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 239
    .end local v8    # "_result":Ljava/lang/String;
    :sswitch_14
    const-string v0, "com.sec.enterprise.knox.IEnterpriseIntegrityServiceAgentCallback"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 240
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/IEnterpriseIntegrityServiceAgentCallback$Stub;->getPackageList()Ljava/util/List;

    move-result-object v10

    .line 241
    .local v10, "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 242
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 243
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 247
    .end local v10    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :sswitch_15
    const-string v0, "com.sec.enterprise.knox.IEnterpriseIntegrityServiceAgentCallback"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 249
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v7

    .line 250
    .local v7, "_arg0":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p0, v7}, Lcom/sec/enterprise/knox/IEnterpriseIntegrityServiceAgentCallback$Stub;->getPackageInfo(Ljava/util/List;)Ljava/util/List;

    move-result-object v9

    .line 251
    .local v9, "_result":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 252
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 253
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 257
    .end local v7    # "_arg0":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v9    # "_result":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    :sswitch_16
    const-string v0, "com.sec.enterprise.knox.IEnterpriseIntegrityServiceAgentCallback"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 258
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/IEnterpriseIntegrityServiceAgentCallback$Stub;->delete3rdPartyBaseline()Z

    move-result v8

    .line 259
    .local v8, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 260
    if-eqz v8, :cond_a

    const/4 v0, 0x1

    :goto_b
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 261
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 260
    :cond_a
    const/4 v0, 0x0

    goto :goto_b

    .line 38
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0xe -> :sswitch_e
        0xf -> :sswitch_f
        0x10 -> :sswitch_10
        0x11 -> :sswitch_11
        0x12 -> :sswitch_12
        0x13 -> :sswitch_13
        0x14 -> :sswitch_14
        0x15 -> :sswitch_15
        0x16 -> :sswitch_16
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
