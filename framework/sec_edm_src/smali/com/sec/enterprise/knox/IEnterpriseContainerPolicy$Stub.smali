.class public abstract Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy$Stub;
.super Landroid/os/Binder;
.source "IEnterpriseContainerPolicy.java"

# interfaces
.implements Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.sec.enterprise.knox.IEnterpriseContainerPolicy"

.field static final TRANSACTION_activateContainer:I = 0x2a

.field static final TRANSACTION_addHomeShortcut:I = 0x14

.field static final TRANSACTION_addVpnProfile:I = 0x16

.field static final TRANSACTION_addVpnProfileToApp:I = 0x18

.field static final TRANSACTION_allowContactInfoToNonKnox:I = 0x2d

.field static final TRANSACTION_createContainer:I = 0x1

.field static final TRANSACTION_deleteHomeShortcut:I = 0x15

.field static final TRANSACTION_enforcePasswordChange:I = 0xc

.field static final TRANSACTION_getAllPackagesForProfile:I = 0x1a

.field static final TRANSACTION_getContainerId:I = 0x4

.field static final TRANSACTION_getContainerOwnerUid:I = 0x5

.field static final TRANSACTION_getContainerPackages:I = 0xf

.field static final TRANSACTION_getContainerType:I = 0x7

.field static final TRANSACTION_getContainerizedPackageName:I = 0x6

.field static final TRANSACTION_getContainerizedPackages:I = 0x10

.field static final TRANSACTION_getContainerizedString:I = 0x23

.field static final TRANSACTION_getMaximumFailedPasswordsForDeviceDisable:I = 0x25

.field static final TRANSACTION_getMaximumTimeToLock:I = 0x20

.field static final TRANSACTION_getMinPasswordComplexChars:I = 0x22

.field static final TRANSACTION_getOwnContainers:I = 0x3

.field static final TRANSACTION_getPasswordEnabledContainerLockTimeout:I = 0x2c

.field static final TRANSACTION_getPasswordExpires:I = 0x1c

.field static final TRANSACTION_getPasswordHistory:I = 0x1e

.field static final TRANSACTION_getPasswordMinimumLength:I = 0x29

.field static final TRANSACTION_getPasswordQuality:I = 0x27

.field static final TRANSACTION_getStatus:I = 0x8

.field static final TRANSACTION_installPackage:I = 0xd

.field static final TRANSACTION_isContactInfoToNonKnoxAllowed:I = 0x2e

.field static final TRANSACTION_lockContainer:I = 0x9

.field static final TRANSACTION_removeContainer:I = 0x2

.field static final TRANSACTION_removeVPNProfile:I = 0x17

.field static final TRANSACTION_removeVpnForApplication:I = 0x19

.field static final TRANSACTION_setMaximumFailedPasswordsForDeviceDisable:I = 0x24

.field static final TRANSACTION_setMaximumTimeToLock:I = 0x1f

.field static final TRANSACTION_setMinPasswordComplexChars:I = 0x21

.field static final TRANSACTION_setPasswordEnabledContainerLockTimeout:I = 0x2b

.field static final TRANSACTION_setPasswordExpires:I = 0x1b

.field static final TRANSACTION_setPasswordHistory:I = 0x1d

.field static final TRANSACTION_setPasswordMinimumLength:I = 0x28

.field static final TRANSACTION_setPasswordQuality:I = 0x26

.field static final TRANSACTION_startApp:I = 0x11

.field static final TRANSACTION_stopApp:I = 0x12

.field static final TRANSACTION_uninstallPackage:I = 0xe

.field static final TRANSACTION_unlockContainer:I = 0xa

.field static final TRANSACTION_verifyPassword:I = 0xb

.field static final TRANSACTION_writeData:I = 0x13


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 19
    const-string v0, "com.sec.enterprise.knox.IEnterpriseContainerPolicy"

    invoke-virtual {p0, p0, v0}, Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 20
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 27
    if-nez p0, :cond_0

    .line 28
    const/4 v0, 0x0

    .line 34
    :goto_0
    return-object v0

    .line 30
    :cond_0
    const-string v1, "com.sec.enterprise.knox.IEnterpriseContainerPolicy"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 31
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy;

    if-eqz v1, :cond_1

    .line 32
    check-cast v0, Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy;

    goto :goto_0

    .line 34
    :cond_1
    new-instance v0, Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 38
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 12
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 42
    sparse-switch p1, :sswitch_data_0

    .line 667
    invoke-super/range {p0 .. p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v0

    :goto_0
    return v0

    .line 46
    :sswitch_0
    const-string v0, "com.sec.enterprise.knox.IEnterpriseContainerPolicy"

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 47
    const/4 v0, 0x1

    goto :goto_0

    .line 51
    :sswitch_1
    const-string v0, "com.sec.enterprise.knox.IEnterpriseContainerPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 53
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/IEnterpriseContainerCallback$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/enterprise/knox/IEnterpriseContainerCallback;

    move-result-object v1

    .line 55
    .local v1, "_arg0":Lcom/sec/enterprise/knox/IEnterpriseContainerCallback;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 56
    .local v2, "_arg1":I
    invoke-virtual {p0, v1, v2}, Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy$Stub;->createContainer(Lcom/sec/enterprise/knox/IEnterpriseContainerCallback;I)Z

    move-result v10

    .line 57
    .local v10, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 58
    if-eqz v10, :cond_0

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 59
    const/4 v0, 0x1

    goto :goto_0

    .line 58
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 63
    .end local v1    # "_arg0":Lcom/sec/enterprise/knox/IEnterpriseContainerCallback;
    .end local v2    # "_arg1":I
    .end local v10    # "_result":Z
    :sswitch_2
    const-string v0, "com.sec.enterprise.knox.IEnterpriseContainerPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 65
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 67
    .local v1, "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/IEnterpriseContainerCallback$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/enterprise/knox/IEnterpriseContainerCallback;

    move-result-object v2

    .line 68
    .local v2, "_arg1":Lcom/sec/enterprise/knox/IEnterpriseContainerCallback;
    invoke-virtual {p0, v1, v2}, Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy$Stub;->removeContainer(ILcom/sec/enterprise/knox/IEnterpriseContainerCallback;)Z

    move-result v10

    .line 69
    .restart local v10    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 70
    if-eqz v10, :cond_1

    const/4 v0, 0x1

    :goto_2
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 71
    const/4 v0, 0x1

    goto :goto_0

    .line 70
    :cond_1
    const/4 v0, 0x0

    goto :goto_2

    .line 75
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":Lcom/sec/enterprise/knox/IEnterpriseContainerCallback;
    .end local v10    # "_result":Z
    :sswitch_3
    const-string v0, "com.sec.enterprise.knox.IEnterpriseContainerPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 76
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy$Stub;->getOwnContainers()[Lcom/sec/enterprise/knox/EnterpriseContainerObject;

    move-result-object v10

    .line 77
    .local v10, "_result":[Lcom/sec/enterprise/knox/EnterpriseContainerObject;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 78
    const/4 v0, 0x1

    invoke-virtual {p3, v10, v0}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    .line 79
    const/4 v0, 0x1

    goto :goto_0

    .line 83
    .end local v10    # "_result":[Lcom/sec/enterprise/knox/EnterpriseContainerObject;
    :sswitch_4
    const-string v0, "com.sec.enterprise.knox.IEnterpriseContainerPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 85
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 86
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy$Stub;->getContainerId(I)I

    move-result v10

    .line 87
    .local v10, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 88
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    .line 89
    const/4 v0, 0x1

    goto :goto_0

    .line 93
    .end local v1    # "_arg0":I
    .end local v10    # "_result":I
    :sswitch_5
    const-string v0, "com.sec.enterprise.knox.IEnterpriseContainerPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 95
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 96
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy$Stub;->getContainerOwnerUid(I)I

    move-result v10

    .line 97
    .restart local v10    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 98
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    .line 99
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 103
    .end local v1    # "_arg0":I
    .end local v10    # "_result":I
    :sswitch_6
    const-string v0, "com.sec.enterprise.knox.IEnterpriseContainerPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 105
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 107
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 108
    .local v2, "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy$Stub;->getContainerizedPackageName(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 109
    .local v10, "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 110
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 111
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 115
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v10    # "_result":Ljava/lang/String;
    :sswitch_7
    const-string v0, "com.sec.enterprise.knox.IEnterpriseContainerPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 117
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 118
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy$Stub;->getContainerType(I)I

    move-result v10

    .line 119
    .local v10, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 120
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    .line 121
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 125
    .end local v1    # "_arg0":I
    .end local v10    # "_result":I
    :sswitch_8
    const-string v0, "com.sec.enterprise.knox.IEnterpriseContainerPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 127
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 128
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy$Stub;->getStatus(I)I

    move-result v10

    .line 129
    .restart local v10    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 130
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    .line 131
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 135
    .end local v1    # "_arg0":I
    .end local v10    # "_result":I
    :sswitch_9
    const-string v0, "com.sec.enterprise.knox.IEnterpriseContainerPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 137
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 138
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy$Stub;->lockContainer(I)Z

    move-result v10

    .line 139
    .local v10, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 140
    if-eqz v10, :cond_2

    const/4 v0, 0x1

    :goto_3
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 141
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 140
    :cond_2
    const/4 v0, 0x0

    goto :goto_3

    .line 145
    .end local v1    # "_arg0":I
    .end local v10    # "_result":Z
    :sswitch_a
    const-string v0, "com.sec.enterprise.knox.IEnterpriseContainerPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 147
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 148
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy$Stub;->unlockContainer(I)Z

    move-result v10

    .line 149
    .restart local v10    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 150
    if-eqz v10, :cond_3

    const/4 v0, 0x1

    :goto_4
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 151
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 150
    :cond_3
    const/4 v0, 0x0

    goto :goto_4

    .line 155
    .end local v1    # "_arg0":I
    .end local v10    # "_result":Z
    :sswitch_b
    const-string v0, "com.sec.enterprise.knox.IEnterpriseContainerPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 157
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 159
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 160
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy$Stub;->verifyPassword(ILjava/lang/String;)I

    move-result v10

    .line 161
    .local v10, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 162
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    .line 163
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 167
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v10    # "_result":I
    :sswitch_c
    const-string v0, "com.sec.enterprise.knox.IEnterpriseContainerPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 169
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 170
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy$Stub;->enforcePasswordChange(I)Z

    move-result v10

    .line 171
    .local v10, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 172
    if-eqz v10, :cond_4

    const/4 v0, 0x1

    :goto_5
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 173
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 172
    :cond_4
    const/4 v0, 0x0

    goto :goto_5

    .line 177
    .end local v1    # "_arg0":I
    .end local v10    # "_result":Z
    :sswitch_d
    const-string v0, "com.sec.enterprise.knox.IEnterpriseContainerPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 179
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 181
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 183
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/IEnterpriseContainerCallback$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/enterprise/knox/IEnterpriseContainerCallback;

    move-result-object v3

    .line 185
    .local v3, "_arg2":Lcom/sec/enterprise/knox/IEnterpriseContainerCallback;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 186
    .local v4, "_arg3":I
    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy$Stub;->installPackage(ILjava/lang/String;Lcom/sec/enterprise/knox/IEnterpriseContainerCallback;I)Z

    move-result v10

    .line 187
    .restart local v10    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 188
    if-eqz v10, :cond_5

    const/4 v0, 0x1

    :goto_6
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 189
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 188
    :cond_5
    const/4 v0, 0x0

    goto :goto_6

    .line 193
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":Lcom/sec/enterprise/knox/IEnterpriseContainerCallback;
    .end local v4    # "_arg3":I
    .end local v10    # "_result":Z
    :sswitch_e
    const-string v0, "com.sec.enterprise.knox.IEnterpriseContainerPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 195
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 197
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 199
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/IEnterpriseContainerCallback$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/enterprise/knox/IEnterpriseContainerCallback;

    move-result-object v3

    .line 200
    .restart local v3    # "_arg2":Lcom/sec/enterprise/knox/IEnterpriseContainerCallback;
    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy$Stub;->uninstallPackage(ILjava/lang/String;Lcom/sec/enterprise/knox/IEnterpriseContainerCallback;)Z

    move-result v10

    .line 201
    .restart local v10    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 202
    if-eqz v10, :cond_6

    const/4 v0, 0x1

    :goto_7
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 203
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 202
    :cond_6
    const/4 v0, 0x0

    goto :goto_7

    .line 207
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":Lcom/sec/enterprise/knox/IEnterpriseContainerCallback;
    .end local v10    # "_result":Z
    :sswitch_f
    const-string v0, "com.sec.enterprise.knox.IEnterpriseContainerPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 209
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 210
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy$Stub;->getContainerPackages(I)[Ljava/lang/String;

    move-result-object v10

    .line 211
    .local v10, "_result":[Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 212
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    .line 213
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 217
    .end local v1    # "_arg0":I
    .end local v10    # "_result":[Ljava/lang/String;
    :sswitch_10
    const-string v0, "com.sec.enterprise.knox.IEnterpriseContainerPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 219
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 220
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy$Stub;->getContainerizedPackages(I)[Ljava/lang/String;

    move-result-object v10

    .line 221
    .restart local v10    # "_result":[Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 222
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    .line 223
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 227
    .end local v1    # "_arg0":I
    .end local v10    # "_result":[Ljava/lang/String;
    :sswitch_11
    const-string v0, "com.sec.enterprise.knox.IEnterpriseContainerPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 229
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 231
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 233
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 234
    .local v3, "_arg2":Ljava/lang/String;
    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy$Stub;->startApp(ILjava/lang/String;Ljava/lang/String;)Z

    move-result v10

    .line 235
    .local v10, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 236
    if-eqz v10, :cond_7

    const/4 v0, 0x1

    :goto_8
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 237
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 236
    :cond_7
    const/4 v0, 0x0

    goto :goto_8

    .line 241
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":Ljava/lang/String;
    .end local v10    # "_result":Z
    :sswitch_12
    const-string v0, "com.sec.enterprise.knox.IEnterpriseContainerPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 243
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 245
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 246
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy$Stub;->stopApp(ILjava/lang/String;)Z

    move-result v10

    .line 247
    .restart local v10    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 248
    if-eqz v10, :cond_8

    const/4 v0, 0x1

    :goto_9
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 249
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 248
    :cond_8
    const/4 v0, 0x0

    goto :goto_9

    .line 253
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v10    # "_result":Z
    :sswitch_13
    const-string v0, "com.sec.enterprise.knox.IEnterpriseContainerPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 255
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 257
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 259
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 261
    .restart local v3    # "_arg2":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v4

    .line 263
    .local v4, "_arg3":[B
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_9

    const/4 v5, 0x1

    .local v5, "_arg4":Z
    :goto_a
    move-object v0, p0

    .line 264
    invoke-virtual/range {v0 .. v5}, Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy$Stub;->writeData(ILjava/lang/String;Ljava/lang/String;[BZ)Z

    move-result v10

    .line 265
    .restart local v10    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 266
    if-eqz v10, :cond_a

    const/4 v0, 0x1

    :goto_b
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 267
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 263
    .end local v5    # "_arg4":Z
    .end local v10    # "_result":Z
    :cond_9
    const/4 v5, 0x0

    goto :goto_a

    .line 266
    .restart local v5    # "_arg4":Z
    .restart local v10    # "_result":Z
    :cond_a
    const/4 v0, 0x0

    goto :goto_b

    .line 271
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":Ljava/lang/String;
    .end local v4    # "_arg3":[B
    .end local v5    # "_arg4":Z
    .end local v10    # "_result":Z
    :sswitch_14
    const-string v0, "com.sec.enterprise.knox.IEnterpriseContainerPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 273
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 275
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 276
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy$Stub;->addHomeShortcut(ILjava/lang/String;)Z

    move-result v10

    .line 277
    .restart local v10    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 278
    if-eqz v10, :cond_b

    const/4 v0, 0x1

    :goto_c
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 279
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 278
    :cond_b
    const/4 v0, 0x0

    goto :goto_c

    .line 283
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v10    # "_result":Z
    :sswitch_15
    const-string v0, "com.sec.enterprise.knox.IEnterpriseContainerPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 285
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 287
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 288
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy$Stub;->deleteHomeShortcut(ILjava/lang/String;)Z

    move-result v10

    .line 289
    .restart local v10    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 290
    if-eqz v10, :cond_c

    const/4 v0, 0x1

    :goto_d
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 291
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 290
    :cond_c
    const/4 v0, 0x0

    goto :goto_d

    .line 295
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v10    # "_result":Z
    :sswitch_16
    const-string v0, "com.sec.enterprise.knox.IEnterpriseContainerPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 297
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 299
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 300
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy$Stub;->addVpnProfile(ILjava/lang/String;)Z

    move-result v10

    .line 301
    .restart local v10    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 302
    if-eqz v10, :cond_d

    const/4 v0, 0x1

    :goto_e
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 303
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 302
    :cond_d
    const/4 v0, 0x0

    goto :goto_e

    .line 307
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v10    # "_result":Z
    :sswitch_17
    const-string v0, "com.sec.enterprise.knox.IEnterpriseContainerPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 309
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 310
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy$Stub;->removeVPNProfile(I)Z

    move-result v10

    .line 311
    .restart local v10    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 312
    if-eqz v10, :cond_e

    const/4 v0, 0x1

    :goto_f
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 313
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 312
    :cond_e
    const/4 v0, 0x0

    goto :goto_f

    .line 317
    .end local v1    # "_arg0":I
    .end local v10    # "_result":Z
    :sswitch_18
    const-string v0, "com.sec.enterprise.knox.IEnterpriseContainerPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 319
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 321
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 323
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 324
    .restart local v3    # "_arg2":Ljava/lang/String;
    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy$Stub;->addVpnProfileToApp(ILjava/lang/String;Ljava/lang/String;)Z

    move-result v10

    .line 325
    .restart local v10    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 326
    if-eqz v10, :cond_f

    const/4 v0, 0x1

    :goto_10
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 327
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 326
    :cond_f
    const/4 v0, 0x0

    goto :goto_10

    .line 331
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":Ljava/lang/String;
    .end local v10    # "_result":Z
    :sswitch_19
    const-string v0, "com.sec.enterprise.knox.IEnterpriseContainerPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 333
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 335
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 336
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy$Stub;->removeVpnForApplication(ILjava/lang/String;)Z

    move-result v10

    .line 337
    .restart local v10    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 338
    if-eqz v10, :cond_10

    const/4 v0, 0x1

    :goto_11
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 339
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 338
    :cond_10
    const/4 v0, 0x0

    goto :goto_11

    .line 343
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v10    # "_result":Z
    :sswitch_1a
    const-string v0, "com.sec.enterprise.knox.IEnterpriseContainerPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 345
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 347
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 348
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy$Stub;->getAllPackagesForProfile(ILjava/lang/String;)[Ljava/lang/String;

    move-result-object v10

    .line 349
    .local v10, "_result":[Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 350
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    .line 351
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 355
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v10    # "_result":[Ljava/lang/String;
    :sswitch_1b
    const-string v0, "com.sec.enterprise.knox.IEnterpriseContainerPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 357
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 359
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_11

    .line 360
    sget-object v0, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/ComponentName;

    .line 366
    .local v2, "_arg1":Landroid/content/ComponentName;
    :goto_12
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 367
    .local v3, "_arg2":I
    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy$Stub;->setPasswordExpires(ILandroid/content/ComponentName;I)V

    .line 368
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 369
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 363
    .end local v2    # "_arg1":Landroid/content/ComponentName;
    .end local v3    # "_arg2":I
    :cond_11
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Landroid/content/ComponentName;
    goto :goto_12

    .line 373
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":Landroid/content/ComponentName;
    :sswitch_1c
    const-string v0, "com.sec.enterprise.knox.IEnterpriseContainerPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 375
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 377
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_12

    .line 378
    sget-object v0, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/ComponentName;

    .line 383
    .restart local v2    # "_arg1":Landroid/content/ComponentName;
    :goto_13
    invoke-virtual {p0, v1, v2}, Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy$Stub;->getPasswordExpires(ILandroid/content/ComponentName;)I

    move-result v10

    .line 384
    .local v10, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 385
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    .line 386
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 381
    .end local v2    # "_arg1":Landroid/content/ComponentName;
    .end local v10    # "_result":I
    :cond_12
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Landroid/content/ComponentName;
    goto :goto_13

    .line 390
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":Landroid/content/ComponentName;
    :sswitch_1d
    const-string v0, "com.sec.enterprise.knox.IEnterpriseContainerPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 392
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 394
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_13

    .line 395
    sget-object v0, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/ComponentName;

    .line 401
    .restart local v2    # "_arg1":Landroid/content/ComponentName;
    :goto_14
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 402
    .restart local v3    # "_arg2":I
    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy$Stub;->setPasswordHistory(ILandroid/content/ComponentName;I)V

    .line 403
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 404
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 398
    .end local v2    # "_arg1":Landroid/content/ComponentName;
    .end local v3    # "_arg2":I
    :cond_13
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Landroid/content/ComponentName;
    goto :goto_14

    .line 408
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":Landroid/content/ComponentName;
    :sswitch_1e
    const-string v0, "com.sec.enterprise.knox.IEnterpriseContainerPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 410
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 412
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_14

    .line 413
    sget-object v0, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/ComponentName;

    .line 418
    .restart local v2    # "_arg1":Landroid/content/ComponentName;
    :goto_15
    invoke-virtual {p0, v1, v2}, Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy$Stub;->getPasswordHistory(ILandroid/content/ComponentName;)I

    move-result v10

    .line 419
    .restart local v10    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 420
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    .line 421
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 416
    .end local v2    # "_arg1":Landroid/content/ComponentName;
    .end local v10    # "_result":I
    :cond_14
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Landroid/content/ComponentName;
    goto :goto_15

    .line 425
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":Landroid/content/ComponentName;
    :sswitch_1f
    const-string v0, "com.sec.enterprise.knox.IEnterpriseContainerPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 427
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 429
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_15

    .line 430
    sget-object v0, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/ComponentName;

    .line 436
    .restart local v2    # "_arg1":Landroid/content/ComponentName;
    :goto_16
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v8

    .line 437
    .local v8, "_arg2":J
    invoke-virtual {p0, v1, v2, v8, v9}, Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy$Stub;->setMaximumTimeToLock(ILandroid/content/ComponentName;J)Z

    move-result v10

    .line 438
    .local v10, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 439
    if-eqz v10, :cond_16

    const/4 v0, 0x1

    :goto_17
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 440
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 433
    .end local v2    # "_arg1":Landroid/content/ComponentName;
    .end local v8    # "_arg2":J
    .end local v10    # "_result":Z
    :cond_15
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Landroid/content/ComponentName;
    goto :goto_16

    .line 439
    .restart local v8    # "_arg2":J
    .restart local v10    # "_result":Z
    :cond_16
    const/4 v0, 0x0

    goto :goto_17

    .line 444
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":Landroid/content/ComponentName;
    .end local v8    # "_arg2":J
    .end local v10    # "_result":Z
    :sswitch_20
    const-string v0, "com.sec.enterprise.knox.IEnterpriseContainerPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 446
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 448
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_17

    .line 449
    sget-object v0, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/ComponentName;

    .line 454
    .restart local v2    # "_arg1":Landroid/content/ComponentName;
    :goto_18
    invoke-virtual {p0, v1, v2}, Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy$Stub;->getMaximumTimeToLock(ILandroid/content/ComponentName;)J

    move-result-wide v10

    .line 455
    .local v10, "_result":J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 456
    invoke-virtual {p3, v10, v11}, Landroid/os/Parcel;->writeLong(J)V

    .line 457
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 452
    .end local v2    # "_arg1":Landroid/content/ComponentName;
    .end local v10    # "_result":J
    :cond_17
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Landroid/content/ComponentName;
    goto :goto_18

    .line 461
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":Landroid/content/ComponentName;
    :sswitch_21
    const-string v0, "com.sec.enterprise.knox.IEnterpriseContainerPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 463
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 465
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_18

    .line 466
    sget-object v0, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/ComponentName;

    .line 472
    .restart local v2    # "_arg1":Landroid/content/ComponentName;
    :goto_19
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 473
    .restart local v3    # "_arg2":I
    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy$Stub;->setMinPasswordComplexChars(ILandroid/content/ComponentName;I)V

    .line 474
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 475
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 469
    .end local v2    # "_arg1":Landroid/content/ComponentName;
    .end local v3    # "_arg2":I
    :cond_18
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Landroid/content/ComponentName;
    goto :goto_19

    .line 479
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":Landroid/content/ComponentName;
    :sswitch_22
    const-string v0, "com.sec.enterprise.knox.IEnterpriseContainerPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 481
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 483
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_19

    .line 484
    sget-object v0, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/ComponentName;

    .line 489
    .restart local v2    # "_arg1":Landroid/content/ComponentName;
    :goto_1a
    invoke-virtual {p0, v1, v2}, Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy$Stub;->getMinPasswordComplexChars(ILandroid/content/ComponentName;)I

    move-result v10

    .line 490
    .local v10, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 491
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    .line 492
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 487
    .end local v2    # "_arg1":Landroid/content/ComponentName;
    .end local v10    # "_result":I
    :cond_19
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Landroid/content/ComponentName;
    goto :goto_1a

    .line 496
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":Landroid/content/ComponentName;
    :sswitch_23
    const-string v0, "com.sec.enterprise.knox.IEnterpriseContainerPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 498
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 500
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 501
    .local v2, "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy$Stub;->getContainerizedString(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 502
    .local v10, "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 503
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 504
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 508
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v10    # "_result":Ljava/lang/String;
    :sswitch_24
    const-string v0, "com.sec.enterprise.knox.IEnterpriseContainerPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 510
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 512
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1a

    .line 513
    sget-object v0, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/ComponentName;

    .line 519
    .local v2, "_arg1":Landroid/content/ComponentName;
    :goto_1b
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 520
    .restart local v3    # "_arg2":I
    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy$Stub;->setMaximumFailedPasswordsForDeviceDisable(ILandroid/content/ComponentName;I)Z

    move-result v10

    .line 521
    .local v10, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 522
    if-eqz v10, :cond_1b

    const/4 v0, 0x1

    :goto_1c
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 523
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 516
    .end local v2    # "_arg1":Landroid/content/ComponentName;
    .end local v3    # "_arg2":I
    .end local v10    # "_result":Z
    :cond_1a
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Landroid/content/ComponentName;
    goto :goto_1b

    .line 522
    .restart local v3    # "_arg2":I
    .restart local v10    # "_result":Z
    :cond_1b
    const/4 v0, 0x0

    goto :goto_1c

    .line 527
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":Landroid/content/ComponentName;
    .end local v3    # "_arg2":I
    .end local v10    # "_result":Z
    :sswitch_25
    const-string v0, "com.sec.enterprise.knox.IEnterpriseContainerPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 529
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 531
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1c

    .line 532
    sget-object v0, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/ComponentName;

    .line 537
    .restart local v2    # "_arg1":Landroid/content/ComponentName;
    :goto_1d
    invoke-virtual {p0, v1, v2}, Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy$Stub;->getMaximumFailedPasswordsForDeviceDisable(ILandroid/content/ComponentName;)I

    move-result v10

    .line 538
    .local v10, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 539
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    .line 540
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 535
    .end local v2    # "_arg1":Landroid/content/ComponentName;
    .end local v10    # "_result":I
    :cond_1c
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Landroid/content/ComponentName;
    goto :goto_1d

    .line 544
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":Landroid/content/ComponentName;
    :sswitch_26
    const-string v0, "com.sec.enterprise.knox.IEnterpriseContainerPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 546
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 548
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1d

    .line 549
    sget-object v0, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/ComponentName;

    .line 555
    .restart local v2    # "_arg1":Landroid/content/ComponentName;
    :goto_1e
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 556
    .restart local v3    # "_arg2":I
    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy$Stub;->setPasswordQuality(ILandroid/content/ComponentName;I)V

    .line 557
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 558
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 552
    .end local v2    # "_arg1":Landroid/content/ComponentName;
    .end local v3    # "_arg2":I
    :cond_1d
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Landroid/content/ComponentName;
    goto :goto_1e

    .line 562
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":Landroid/content/ComponentName;
    :sswitch_27
    const-string v0, "com.sec.enterprise.knox.IEnterpriseContainerPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 564
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 566
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1e

    .line 567
    sget-object v0, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/ComponentName;

    .line 572
    .restart local v2    # "_arg1":Landroid/content/ComponentName;
    :goto_1f
    invoke-virtual {p0, v1, v2}, Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy$Stub;->getPasswordQuality(ILandroid/content/ComponentName;)I

    move-result v10

    .line 573
    .restart local v10    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 574
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    .line 575
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 570
    .end local v2    # "_arg1":Landroid/content/ComponentName;
    .end local v10    # "_result":I
    :cond_1e
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Landroid/content/ComponentName;
    goto :goto_1f

    .line 579
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":Landroid/content/ComponentName;
    :sswitch_28
    const-string v0, "com.sec.enterprise.knox.IEnterpriseContainerPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 581
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 583
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1f

    .line 584
    sget-object v0, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/ComponentName;

    .line 590
    .restart local v2    # "_arg1":Landroid/content/ComponentName;
    :goto_20
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 591
    .restart local v3    # "_arg2":I
    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy$Stub;->setPasswordMinimumLength(ILandroid/content/ComponentName;I)V

    .line 592
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 593
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 587
    .end local v2    # "_arg1":Landroid/content/ComponentName;
    .end local v3    # "_arg2":I
    :cond_1f
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Landroid/content/ComponentName;
    goto :goto_20

    .line 597
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":Landroid/content/ComponentName;
    :sswitch_29
    const-string v0, "com.sec.enterprise.knox.IEnterpriseContainerPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 599
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 601
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_20

    .line 602
    sget-object v0, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/ComponentName;

    .line 607
    .restart local v2    # "_arg1":Landroid/content/ComponentName;
    :goto_21
    invoke-virtual {p0, v1, v2}, Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy$Stub;->getPasswordMinimumLength(ILandroid/content/ComponentName;)I

    move-result v10

    .line 608
    .restart local v10    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 609
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    .line 610
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 605
    .end local v2    # "_arg1":Landroid/content/ComponentName;
    .end local v10    # "_result":I
    :cond_20
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Landroid/content/ComponentName;
    goto :goto_21

    .line 614
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":Landroid/content/ComponentName;
    :sswitch_2a
    const-string v0, "com.sec.enterprise.knox.IEnterpriseContainerPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 616
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 617
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy$Stub;->activateContainer(I)Z

    move-result v10

    .line 618
    .local v10, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 619
    if-eqz v10, :cond_21

    const/4 v0, 0x1

    :goto_22
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 620
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 619
    :cond_21
    const/4 v0, 0x0

    goto :goto_22

    .line 624
    .end local v1    # "_arg0":I
    .end local v10    # "_result":Z
    :sswitch_2b
    const-string v0, "com.sec.enterprise.knox.IEnterpriseContainerPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 626
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 628
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v6

    .line 629
    .local v6, "_arg1":J
    invoke-virtual {p0, v1, v6, v7}, Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy$Stub;->setPasswordEnabledContainerLockTimeout(IJ)Z

    move-result v10

    .line 630
    .restart local v10    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 631
    if-eqz v10, :cond_22

    const/4 v0, 0x1

    :goto_23
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 632
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 631
    :cond_22
    const/4 v0, 0x0

    goto :goto_23

    .line 636
    .end local v1    # "_arg0":I
    .end local v6    # "_arg1":J
    .end local v10    # "_result":Z
    :sswitch_2c
    const-string v0, "com.sec.enterprise.knox.IEnterpriseContainerPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 638
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 639
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy$Stub;->getPasswordEnabledContainerLockTimeout(I)J

    move-result-wide v10

    .line 640
    .local v10, "_result":J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 641
    invoke-virtual {p3, v10, v11}, Landroid/os/Parcel;->writeLong(J)V

    .line 642
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 646
    .end local v1    # "_arg0":I
    .end local v10    # "_result":J
    :sswitch_2d
    const-string v0, "com.sec.enterprise.knox.IEnterpriseContainerPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 648
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 650
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_23

    const/4 v2, 0x1

    .line 651
    .local v2, "_arg1":Z
    :goto_24
    invoke-virtual {p0, v1, v2}, Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy$Stub;->allowContactInfoToNonKnox(IZ)Z

    move-result v10

    .line 652
    .local v10, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 653
    if-eqz v10, :cond_24

    const/4 v0, 0x1

    :goto_25
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 654
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 650
    .end local v2    # "_arg1":Z
    .end local v10    # "_result":Z
    :cond_23
    const/4 v2, 0x0

    goto :goto_24

    .line 653
    .restart local v2    # "_arg1":Z
    .restart local v10    # "_result":Z
    :cond_24
    const/4 v0, 0x0

    goto :goto_25

    .line 658
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":Z
    .end local v10    # "_result":Z
    :sswitch_2e
    const-string v0, "com.sec.enterprise.knox.IEnterpriseContainerPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 660
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 661
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy$Stub;->isContactInfoToNonKnoxAllowed(I)Z

    move-result v10

    .line 662
    .restart local v10    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 663
    if-eqz v10, :cond_25

    const/4 v0, 0x1

    :goto_26
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 664
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 663
    :cond_25
    const/4 v0, 0x0

    goto :goto_26

    .line 42
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0xe -> :sswitch_e
        0xf -> :sswitch_f
        0x10 -> :sswitch_10
        0x11 -> :sswitch_11
        0x12 -> :sswitch_12
        0x13 -> :sswitch_13
        0x14 -> :sswitch_14
        0x15 -> :sswitch_15
        0x16 -> :sswitch_16
        0x17 -> :sswitch_17
        0x18 -> :sswitch_18
        0x19 -> :sswitch_19
        0x1a -> :sswitch_1a
        0x1b -> :sswitch_1b
        0x1c -> :sswitch_1c
        0x1d -> :sswitch_1d
        0x1e -> :sswitch_1e
        0x1f -> :sswitch_1f
        0x20 -> :sswitch_20
        0x21 -> :sswitch_21
        0x22 -> :sswitch_22
        0x23 -> :sswitch_23
        0x24 -> :sswitch_24
        0x25 -> :sswitch_25
        0x26 -> :sswitch_26
        0x27 -> :sswitch_27
        0x28 -> :sswitch_28
        0x29 -> :sswitch_29
        0x2a -> :sswitch_2a
        0x2b -> :sswitch_2b
        0x2c -> :sswitch_2c
        0x2d -> :sswitch_2d
        0x2e -> :sswitch_2e
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
