.class public Lcom/sec/enterprise/knox/certificate/CertificatePolicy;
.super Ljava/lang/Object;
.source "CertificatePolicy.java"


# static fields
.field public static final ACTION_CERTIFICATE_FAILURE:Ljava/lang/String; = "edm.intent.certificate.action.certificate.failure"

.field public static final ACTION_CERTIFICATE_REMOVED:Ljava/lang/String; = "com.sec.enterprise.knox.certificate.CertificatePolicy.ACTION_CERTIFICATE_REMOVED"

.field public static final ACTION_REFRESH_CREDENTIALS_UI:Ljava/lang/String; = "edm.intent.action.REFRESH_UI"

.field public static final APP_INFO_PKGNAME:Ljava/lang/String; = "appInfoPkgName"

.field public static final BROWSER_MODULE:Ljava/lang/String; = "browser_module"

.field public static final CERTIFICATE_FAIL_ALG_NON_FIPS_APPROVED:I = 0x9

.field public static final CERTIFICATE_FAIL_ALTSUBJECT_MISMATCH:I = 0x6

.field public static final CERTIFICATE_FAIL_BAD_CERTIFICATE:I = 0x7

.field public static final CERTIFICATE_FAIL_EXPIRED:I = 0x4

.field public static final CERTIFICATE_FAIL_INSTALL_PARSE_CERTIFICATE_ENCODING:I = 0xb

.field public static final CERTIFICATE_FAIL_INSTALL_PARSE_INCONSISTENT_CERTIFICATES:I = 0xc

.field public static final CERTIFICATE_FAIL_INSTALL_PARSE_NO_CERTIFICATES:I = 0xa

.field public static final CERTIFICATE_FAIL_NOT_YET_VALID:I = 0x3

.field public static final CERTIFICATE_FAIL_REVOKED:I = 0x2

.field public static final CERTIFICATE_FAIL_SERVER_CHAIN_PROBE:I = 0x8

.field public static final CERTIFICATE_FAIL_SUBJECT_MISMATCH:I = 0x5

.field public static final CERTIFICATE_FAIL_UNABLE_TO_CHECK_REVOCATION_STATUS:I = 0xd

.field public static final CERTIFICATE_FAIL_UNSPECIFIED:I = 0x0

.field public static final CERTIFICATE_FAIL_UNTRUSTED:I = 0x1

.field public static final CERTIFICATE_VALIDATED_SUCCESSFULLY:I = -0x1

.field public static final EXTRA_CERTIFICATE_FAILURE_MESSAGE:Ljava/lang/String; = "certificate_failure_message"

.field public static final EXTRA_CERTIFICATE_FAILURE_MODULE:Ljava/lang/String; = "certificate_failure_module"

.field public static final EXTRA_CERTIFICATE_REMOVED_SUBJECT:Ljava/lang/String; = "certificate_removed_subject"

.field public static final INSTALLER_MODULE:Ljava/lang/String; = "installer_module"

.field public static final IS_MARKET_INSTALLATION:Ljava/lang/String; = "isMarketInstallation"

.field public static final PACKAGE_MODULE:Ljava/lang/String; = "package_manager_module"

.field private static TAG:Ljava/lang/String; = null

.field public static final WIFI_MODULE:Ljava/lang/String; = "wifi_module"

.field private static gCertificatePolicy:Lcom/sec/enterprise/knox/certificate/CertificatePolicy;

.field private static final mSync:Ljava/lang/Object;


# instance fields
.field private mAppService:Landroid/app/enterprise/IApplicationPolicy;

.field private mContextInfo:Landroid/app/enterprise/ContextInfo;

.field private mRestrictionService:Landroid/app/enterprise/IRestrictionPolicy;

.field private mService:Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 71
    const-string v0, "CertificatePolicy"

    sput-object v0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->TAG:Ljava/lang/String;

    .line 76
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mSync:Ljava/lang/Object;

    return-void
.end method

.method constructor <init>(Landroid/app/enterprise/ContextInfo;)V
    .locals 0
    .param p1, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;

    .prologue
    .line 140
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 141
    iput-object p1, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    .line 142
    return-void
.end method

.method public static createInstance(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)Lcom/sec/enterprise/knox/certificate/CertificatePolicy;
    .locals 1
    .param p0, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 280
    new-instance v0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;

    invoke-direct {v0, p0}, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;-><init>(Landroid/app/enterprise/ContextInfo;)V

    return-object v0
.end method

.method private getAppService()Landroid/app/enterprise/IApplicationPolicy;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mAppService:Landroid/app/enterprise/IApplicationPolicy;

    if-nez v0, :cond_0

    .line 155
    const-string v0, "application_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/app/enterprise/IApplicationPolicy$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mAppService:Landroid/app/enterprise/IApplicationPolicy;

    .line 158
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mAppService:Landroid/app/enterprise/IApplicationPolicy;

    return-object v0
.end method

.method public static getInstance(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)Lcom/sec/enterprise/knox/certificate/CertificatePolicy;
    .locals 2
    .param p0, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 270
    sget-object v1, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mSync:Ljava/lang/Object;

    monitor-enter v1

    .line 271
    :try_start_0
    new-instance v0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;

    invoke-direct {v0, p0}, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;-><init>(Landroid/app/enterprise/ContextInfo;)V

    sput-object v0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->gCertificatePolicy:Lcom/sec/enterprise/knox/certificate/CertificatePolicy;

    .line 272
    sget-object v0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->gCertificatePolicy:Lcom/sec/enterprise/knox/certificate/CertificatePolicy;

    monitor-exit v1

    return-object v0

    .line 273
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/sec/enterprise/knox/certificate/CertificatePolicy;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 259
    sget-object v2, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mSync:Ljava/lang/Object;

    monitor-enter v2

    .line 260
    :try_start_0
    new-instance v0, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    invoke-direct {v0, v1}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    .line 261
    .local v0, "cxtInfo":Landroid/app/enterprise/ContextInfo;
    new-instance v1, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;

    invoke-direct {v1, v0}, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;-><init>(Landroid/app/enterprise/ContextInfo;)V

    sput-object v1, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->gCertificatePolicy:Lcom/sec/enterprise/knox/certificate/CertificatePolicy;

    .line 262
    sget-object v1, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->gCertificatePolicy:Lcom/sec/enterprise/knox/certificate/CertificatePolicy;

    monitor-exit v2

    return-object v1

    .line 263
    .end local v0    # "cxtInfo":Landroid/app/enterprise/ContextInfo;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private getRestrictionService()Landroid/app/enterprise/IRestrictionPolicy;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mRestrictionService:Landroid/app/enterprise/IRestrictionPolicy;

    if-nez v0, :cond_0

    .line 163
    const-string v0, "restriction_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mRestrictionService:Landroid/app/enterprise/IRestrictionPolicy;

    .line 166
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mRestrictionService:Landroid/app/enterprise/IRestrictionPolicy;

    return-object v0
.end method

.method private getService()Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mService:Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;

    if-nez v0, :cond_0

    .line 147
    const-string v0, "certificate_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/certificate/ICertificatePolicy$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mService:Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;

    .line 150
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mService:Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;

    return-object v0
.end method


# virtual methods
.method public addPermissionApplicationPrivateKey(Lcom/sec/enterprise/knox/certificate/PermissionApplicationPrivateKey;)Z
    .locals 3
    .param p1, "permission"    # Lcom/sec/enterprise/knox/certificate/PermissionApplicationPrivateKey;

    .prologue
    .line 1359
    iget-object v1, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "CertificatePolicy.addPermissionApplicationPrivateKey"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1360
    invoke-direct {p0}, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->getService()Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1362
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mService:Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;->addPermissionApplicationPrivateKey(Landroid/app/enterprise/ContextInfo;Lcom/sec/enterprise/knox/certificate/PermissionApplicationPrivateKey;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1367
    :goto_0
    return v1

    .line 1363
    :catch_0
    move-exception v0

    .line 1364
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with certificate policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1367
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public addTrustedCaCertificateList(Ljava/util/List;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/security/cert/X509Certificate;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p1, "certificates":Ljava/util/List;, "Ljava/util/List<Ljava/security/cert/X509Certificate;>;"
    const/4 v4, 0x0

    .line 331
    iget-object v5, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v6, "CertificatePolicy.addTrustedCaCertificateList"

    invoke-static {v5, v6}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 332
    invoke-direct {p0}, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->getService()Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 334
    if-eqz p1, :cond_0

    .line 335
    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 336
    .local v1, "certList":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/CertificateInfo;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/security/cert/X509Certificate;

    .line 337
    .local v0, "cert":Ljava/security/cert/X509Certificate;
    if-nez v0, :cond_1

    .line 348
    .end local v0    # "cert":Ljava/security/cert/X509Certificate;
    .end local v1    # "certList":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/CertificateInfo;>;"
    .end local v3    # "i$":Ljava/util/Iterator;
    :cond_0
    :goto_1
    return v4

    .line 340
    .restart local v0    # "cert":Ljava/security/cert/X509Certificate;
    .restart local v1    # "certList":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/CertificateInfo;>;"
    .restart local v3    # "i$":Ljava/util/Iterator;
    :cond_1
    new-instance v5, Landroid/app/enterprise/CertificateInfo;

    invoke-direct {v5, v0}, Landroid/app/enterprise/CertificateInfo;-><init>(Ljava/security/cert/Certificate;)V

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 344
    .end local v0    # "cert":Ljava/security/cert/X509Certificate;
    .end local v1    # "certList":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/CertificateInfo;>;"
    .end local v3    # "i$":Ljava/util/Iterator;
    :catch_0
    move-exception v2

    .line 345
    .local v2, "e":Landroid/os/RemoteException;
    sget-object v5, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->TAG:Ljava/lang/String;

    const-string v6, "Failed talking with certificate policy"

    invoke-static {v5, v6, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 342
    .end local v2    # "e":Landroid/os/RemoteException;
    .restart local v1    # "certList":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/CertificateInfo;>;"
    .restart local v3    # "i$":Ljava/util/Iterator;
    :cond_2
    :try_start_1
    iget-object v5, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mService:Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;

    iget-object v6, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v5, v6, v1}, Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;->addTrustedCaCertificateList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v4

    goto :goto_1
.end method

.method public addUntrustedCertificateList(Ljava/util/List;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/security/cert/X509Certificate;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p1, "certificates":Ljava/util/List;, "Ljava/util/List<Ljava/security/cert/X509Certificate;>;"
    const/4 v4, 0x0

    .line 587
    iget-object v5, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v6, "CertificatePolicy.addUntrustedCertificateList"

    invoke-static {v5, v6}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 588
    invoke-direct {p0}, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->getService()Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 590
    if-eqz p1, :cond_0

    .line 591
    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 592
    .local v1, "certList":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/CertificateInfo;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/security/cert/X509Certificate;

    .line 593
    .local v0, "cert":Ljava/security/cert/X509Certificate;
    if-nez v0, :cond_1

    .line 604
    .end local v0    # "cert":Ljava/security/cert/X509Certificate;
    .end local v1    # "certList":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/CertificateInfo;>;"
    .end local v3    # "i$":Ljava/util/Iterator;
    :cond_0
    :goto_1
    return v4

    .line 596
    .restart local v0    # "cert":Ljava/security/cert/X509Certificate;
    .restart local v1    # "certList":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/CertificateInfo;>;"
    .restart local v3    # "i$":Ljava/util/Iterator;
    :cond_1
    new-instance v5, Landroid/app/enterprise/CertificateInfo;

    invoke-direct {v5, v0}, Landroid/app/enterprise/CertificateInfo;-><init>(Ljava/security/cert/Certificate;)V

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 600
    .end local v0    # "cert":Ljava/security/cert/X509Certificate;
    .end local v1    # "certList":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/CertificateInfo;>;"
    .end local v3    # "i$":Ljava/util/Iterator;
    :catch_0
    move-exception v2

    .line 601
    .local v2, "e":Landroid/os/RemoteException;
    sget-object v5, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->TAG:Ljava/lang/String;

    const-string v6, "Failed talking with certificate policy"

    invoke-static {v5, v6, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 598
    .end local v2    # "e":Landroid/os/RemoteException;
    .restart local v1    # "certList":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/CertificateInfo;>;"
    .restart local v3    # "i$":Ljava/util/Iterator;
    :cond_2
    :try_start_1
    iget-object v5, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mService:Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;

    iget-object v6, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v5, v6, v1}, Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;->addUntrustedCertificateList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v4

    goto :goto_1
.end method

.method public allowUserRemoveCertificates(Z)Z
    .locals 3
    .param p1, "allow"    # Z

    .prologue
    .line 769
    iget-object v1, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "CertificatePolicy.allowUserRemoveCertificates"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 770
    invoke-direct {p0}, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->getService()Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 772
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mService:Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;->allowUserRemoveCertificates(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 777
    :goto_0
    return v1

    .line 773
    :catch_0
    move-exception v0

    .line 774
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with certificate policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 777
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public clearPermissionApplicationPrivateKey()Z
    .locals 3

    .prologue
    .line 1391
    iget-object v1, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "CertificatePolicy.clearPermissionApplicationPrivateKey"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1392
    invoke-direct {p0}, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->getService()Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1394
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mService:Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;->clearPermissionApplicationPrivateKey(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1399
    :goto_0
    return v1

    .line 1395
    :catch_0
    move-exception v0

    .line 1396
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with certificate policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1399
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public clearTrustedCaCertificateList()Z
    .locals 3

    .prologue
    .line 461
    iget-object v1, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "CertificatePolicy.clearTrustedCaCertificateList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 462
    invoke-direct {p0}, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->getService()Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 464
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mService:Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;->clearTrustedCaCertificateList(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 469
    :goto_0
    return v1

    .line 465
    :catch_0
    move-exception v0

    .line 466
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with certificate policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 469
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public clearUntrustedCertificateList()Z
    .locals 3

    .prologue
    .line 752
    iget-object v1, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "CertificatePolicy.clearUntrustedCertificateList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 753
    invoke-direct {p0}, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->getService()Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 755
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mService:Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;->clearUntrustedCertificateList(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 760
    :goto_0
    return v1

    .line 756
    :catch_0
    move-exception v0

    .line 757
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with certificate policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 760
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public enableCertificateFailureNotification(Z)Z
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 965
    iget-object v1, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "CertificatePolicy.enableCertificateFailureNotification"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 966
    invoke-direct {p0}, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->getService()Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 968
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mService:Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;->enableCertificateFailureNotification(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 973
    :goto_0
    return v1

    .line 969
    :catch_0
    move-exception v0

    .line 970
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with certificate policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 973
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public enableCertificateValidationAtInstall(Z)Z
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 1275
    iget-object v1, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "CertificatePolicy.enableCertificateValidationAtInstall"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1276
    invoke-direct {p0}, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->getService()Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1278
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mService:Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;->enableCertificateValidationAtInstall(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1283
    :goto_0
    return v1

    .line 1279
    :catch_0
    move-exception v0

    .line 1280
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with certificate policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1283
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public enableOcspCheck(Ljava/lang/String;Z)Z
    .locals 3
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "enable"    # Z

    .prologue
    .line 1222
    iget-object v1, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "CertificatePolicy.enableOcspCheck"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1223
    invoke-direct {p0}, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->getAppService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1225
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mAppService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IApplicationPolicy;->enableOcspCheck(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1230
    :goto_0
    return v1

    .line 1226
    :catch_0
    move-exception v0

    .line 1227
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1230
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public enableRevocationCheck(Ljava/lang/String;Z)Z
    .locals 3
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "enable"    # Z

    .prologue
    .line 1117
    iget-object v1, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "CertificatePolicy.enableRevocationCheck"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1118
    invoke-direct {p0}, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->getAppService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1120
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mAppService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IApplicationPolicy;->enableRevocationCheck(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1125
    :goto_0
    return v1

    .line 1121
    :catch_0
    move-exception v0

    .line 1122
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1125
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public enableSignatureIdentityInformation(Z)Z
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 854
    iget-object v1, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "CertificatePolicy.enableSignatureIdentityInformation"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 855
    invoke-direct {p0}, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->getService()Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 857
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mService:Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;->enableSignatureIdentityInformation(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 862
    :goto_0
    return v1

    .line 858
    :catch_0
    move-exception v0

    .line 859
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with certificate policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 862
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getIdentitiesFromSignatures([Landroid/content/pm/Signature;)Ljava/util/List;
    .locals 3
    .param p1, "signatures"    # [Landroid/content/pm/Signature;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Landroid/content/pm/Signature;",
            ")",
            "Ljava/util/List",
            "<[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 801
    invoke-direct {p0}, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->getService()Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 803
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mService:Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;->getIdentitiesFromSignatures(Landroid/app/enterprise/ContextInfo;[Landroid/content/pm/Signature;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 808
    :goto_0
    return-object v1

    .line 804
    :catch_0
    move-exception v0

    .line 805
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with certificate policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 808
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getListPermissionApplicationPrivateKey()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/enterprise/knox/certificate/PermissionApplicationPrivateKey;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1407
    iget-object v1, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "CertificatePolicy.getListPermissionApplicationPrivateKey"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1408
    invoke-direct {p0}, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->getService()Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1410
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mService:Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;->getListPermissionApplicationPrivateKey(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1415
    :goto_0
    return-object v1

    .line 1411
    :catch_0
    move-exception v0

    .line 1412
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with certificate policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1415
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    goto :goto_0
.end method

.method public getTrustedCaCertificateList()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/enterprise/knox/certificate/CertificateControlInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 389
    iget-object v1, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "CertificatePolicy.getTrustedCaCertificateList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 390
    invoke-direct {p0}, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->getService()Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 392
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mService:Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;->getTrustedCaCertificateList(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 397
    :goto_0
    return-object v1

    .line 393
    :catch_0
    move-exception v0

    .line 394
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with certificate policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 397
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method public getUntrustedCertificateList()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/enterprise/knox/certificate/CertificateControlInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 707
    iget-object v1, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "CertificatePolicy.getUntrustedCertificateList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 708
    invoke-direct {p0}, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->getService()Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 710
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mService:Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;->getUntrustedCertificateList(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 715
    :goto_0
    return-object v1

    .line 711
    :catch_0
    move-exception v0

    .line 712
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with certificate policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 715
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method public isCaCertificateDisabled(Ljava/lang/String;)Z
    .locals 3
    .param p1, "certAlias"    # Ljava/lang/String;

    .prologue
    .line 534
    iget-object v1, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "CertificatePolicy.isCaCertificateDisabled"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 536
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->getService()Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 537
    iget-object v1, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mService:Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;->isCaCertificateDisabled(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 542
    :goto_0
    return v1

    .line 539
    :catch_0
    move-exception v0

    .line 540
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at Certificate policy API isCaCertificateDisabled "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 542
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isCaCertificateTrusted([BZ)Z
    .locals 9
    .param p1, "certBytes"    # [B
    .param p2, "showMsg"    # Z

    .prologue
    const/4 v5, 0x1

    .line 408
    iget-object v6, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v7, "CertificatePolicy.isCaCertificateTrusted"

    invoke-static {v6, v7}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 409
    invoke-direct {p0}, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->getService()Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 411
    const/4 v0, 0x0

    .line 413
    .local v0, "cert":Ljava/security/cert/X509Certificate;
    :try_start_0
    const-string v6, "X.509"

    invoke-static {v6}, Ljava/security/cert/CertificateFactory;->getInstance(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;

    move-result-object v1

    .line 414
    .local v1, "certFactory":Ljava/security/cert/CertificateFactory;
    new-instance v4, Ljava/io/ByteArrayInputStream;

    invoke-direct {v4, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 415
    .local v4, "in":Ljava/io/InputStream;
    invoke-virtual {v1, v4}, Ljava/security/cert/CertificateFactory;->generateCertificate(Ljava/io/InputStream;)Ljava/security/cert/Certificate;

    move-result-object v0

    .end local v0    # "cert":Ljava/security/cert/X509Certificate;
    check-cast v0, Ljava/security/cert/X509Certificate;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 421
    .restart local v0    # "cert":Ljava/security/cert/X509Certificate;
    :try_start_1
    new-instance v2, Landroid/app/enterprise/CertificateInfo;

    invoke-direct {v2, v0}, Landroid/app/enterprise/CertificateInfo;-><init>(Ljava/security/cert/Certificate;)V

    .line 422
    .local v2, "certInfo":Landroid/app/enterprise/CertificateInfo;
    iget-object v6, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mService:Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;

    iget-object v7, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v6, v7, v2, p2}, Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;->isCaCertificateTrusted(Landroid/app/enterprise/ContextInfo;Landroid/app/enterprise/CertificateInfo;Z)Z
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v5

    .line 427
    .end local v0    # "cert":Ljava/security/cert/X509Certificate;
    .end local v1    # "certFactory":Ljava/security/cert/CertificateFactory;
    .end local v2    # "certInfo":Landroid/app/enterprise/CertificateInfo;
    .end local v4    # "in":Ljava/io/InputStream;
    :cond_0
    :goto_0
    return v5

    .line 416
    :catch_0
    move-exception v3

    .line 417
    .local v3, "e":Ljava/lang/Exception;
    sget-object v6, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Problem converting certificate! "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 423
    .end local v3    # "e":Ljava/lang/Exception;
    .restart local v0    # "cert":Ljava/security/cert/X509Certificate;
    .restart local v1    # "certFactory":Ljava/security/cert/CertificateFactory;
    .restart local v4    # "in":Ljava/io/InputStream;
    :catch_1
    move-exception v3

    .line 424
    .local v3, "e":Landroid/os/RemoteException;
    sget-object v6, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->TAG:Ljava/lang/String;

    const-string v7, "Failed talking with certificate policy"

    invoke-static {v6, v7, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public isCertificateFailureNotificationEnabled()Z
    .locals 3

    .prologue
    .line 1011
    iget-object v1, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "CertificatePolicy.isCertificateFailureNotificationEnabled"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1012
    invoke-direct {p0}, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->getService()Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1014
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mService:Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;->isCertificateFailureNotificationEnabled(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1019
    :goto_0
    return v1

    .line 1015
    :catch_0
    move-exception v0

    .line 1016
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with certificate policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1019
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isCertificateValidationAtInstallEnabled()Z
    .locals 3

    .prologue
    .line 1292
    invoke-direct {p0}, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->getService()Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1294
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mService:Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;->isCertificateValidationAtInstallEnabled(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1299
    :goto_0
    return v1

    .line 1295
    :catch_0
    move-exception v0

    .line 1296
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with certificate policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1299
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isNonTrustedAppInstallBlocked()Z
    .locals 3

    .prologue
    .line 1469
    iget-object v1, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "CertificatePolicy.isNonTrustedAppInstallBlocked"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1470
    invoke-direct {p0}, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->getRestrictionService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1472
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mRestrictionService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IRestrictionPolicy;->isNonTrustedAppInstallBlocked(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1477
    :goto_0
    return v1

    .line 1473
    :catch_0
    move-exception v0

    .line 1474
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1477
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isNonTrustedAppInstallBlocked(I)Z
    .locals 3
    .param p1, "userId"    # I

    .prologue
    .line 1481
    invoke-direct {p0}, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->getRestrictionService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1483
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mRestrictionService:Landroid/app/enterprise/IRestrictionPolicy;

    invoke-interface {v1, p1}, Landroid/app/enterprise/IRestrictionPolicy;->isNonTrustedAppInstallBlockedAsUser(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1488
    :goto_0
    return v1

    .line 1484
    :catch_0
    move-exception v0

    .line 1485
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1488
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isOcspCheckEnabled(Ljava/lang/String;)Z
    .locals 3
    .param p1, "pkgName"    # Ljava/lang/String;

    .prologue
    .line 1260
    invoke-direct {p0}, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->getAppService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1262
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mAppService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->isOcspCheckEnabled(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1267
    :goto_0
    return v1

    .line 1263
    :catch_0
    move-exception v0

    .line 1264
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1267
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isPrivateKeyApplicationPermitted(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;
    .locals 3
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "host"    # Ljava/lang/String;
    .param p3, "port"    # I

    .prologue
    .line 1422
    iget-object v1, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "CertificatePolicy.isPrivateKeyApplicationPermitted"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1423
    invoke-direct {p0}, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->getService()Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1425
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mService:Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2, p3}, Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;->isPrivateKeyApplicationPermitted(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1430
    :goto_0
    return-object v1

    .line 1426
    :catch_0
    move-exception v0

    .line 1427
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with certificate policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1430
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isPrivateKeyApplicationPermittedAsUser(Ljava/lang/String;Ljava/lang/String;II)Ljava/lang/String;
    .locals 3
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "host"    # Ljava/lang/String;
    .param p3, "port"    # I
    .param p4, "userId"    # I

    .prologue
    .line 1438
    iget-object v1, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "CertificatePolicy.isPrivateKeyApplicationPermittedAsUser"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1439
    invoke-direct {p0}, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->getService()Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1441
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mService:Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;

    invoke-interface {v1, p1, p2, p3, p4}, Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;->isPrivateKeyApplicationPermittedAsUser(Ljava/lang/String;Ljava/lang/String;II)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1446
    :goto_0
    return-object v1

    .line 1442
    :catch_0
    move-exception v0

    .line 1443
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with certificate policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1446
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isRevocationCheckEnabled(Ljava/lang/String;)Z
    .locals 3
    .param p1, "pkgName"    # Ljava/lang/String;

    .prologue
    .line 1154
    invoke-direct {p0}, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->getAppService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1156
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mAppService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->isRevocationCheckEnabled(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1161
    :goto_0
    return v1

    .line 1157
    :catch_0
    move-exception v0

    .line 1158
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1161
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isSignatureIdentityInformationEnabled()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 901
    iget-object v2, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "CertificatePolicy.isSignatureIdentityInformationEnabled"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 902
    invoke-direct {p0}, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->getService()Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 904
    :try_start_0
    iget-object v2, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mService:Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;

    iget-object v3, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;->isSignatureIdentityInformationEnabled(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 909
    :cond_0
    :goto_0
    return v1

    .line 905
    :catch_0
    move-exception v0

    .line 906
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->TAG:Ljava/lang/String;

    const-string v3, "Failed talking with certificate policy"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public isSignatureIdentityInformationEnabled(Z)Z
    .locals 3
    .param p1, "showMsg"    # Z

    .prologue
    .line 916
    iget-object v1, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "CertificatePolicy.isSignatureIdentityInformationEnabled"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 917
    invoke-direct {p0}, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->getService()Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 919
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mService:Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;->isSignatureIdentityInformationEnabled(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 924
    :goto_0
    return v1

    .line 920
    :catch_0
    move-exception v0

    .line 921
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with certificate policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 924
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isUserRemoveCertificatesAllowed()Z
    .locals 3

    .prologue
    .line 787
    invoke-direct {p0}, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->getService()Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 789
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mService:Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;->isUserRemoveCertificatesAllowed(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 794
    :goto_0
    return v1

    .line 790
    :catch_0
    move-exception v0

    .line 791
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with certificate policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 794
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public notifyCertificateFailure(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "module"    # Ljava/lang/String;
    .param p2, "msg"    # Ljava/lang/String;

    .prologue
    .line 1047
    invoke-direct {p0}, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->getService()Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1049
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mService:Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;

    const/4 v2, 0x0

    invoke-interface {v1, p1, p2, v2}, Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;->notifyCertificateFailure(Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1054
    :cond_0
    :goto_0
    return-void

    .line 1050
    :catch_0
    move-exception v0

    .line 1051
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with certificate policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public notifyCertificateFailure(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 3
    .param p1, "module"    # Ljava/lang/String;
    .param p2, "msg"    # Ljava/lang/String;
    .param p3, "showMsg"    # Z

    .prologue
    .line 1030
    invoke-direct {p0}, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->getService()Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1032
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mService:Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;

    invoke-interface {v1, p1, p2, p3}, Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;->notifyCertificateFailure(Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1037
    :cond_0
    :goto_0
    return-void

    .line 1033
    :catch_0
    move-exception v0

    .line 1034
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with certificate policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public removePermissionApplicationPrivateKey(Lcom/sec/enterprise/knox/certificate/PermissionApplicationPrivateKey;)Z
    .locals 3
    .param p1, "permission"    # Lcom/sec/enterprise/knox/certificate/PermissionApplicationPrivateKey;

    .prologue
    .line 1375
    iget-object v1, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "CertificatePolicy.removePermissionApplicationPrivateKey"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1376
    invoke-direct {p0}, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->getService()Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1378
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mService:Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;->removePermissionApplicationPrivateKey(Landroid/app/enterprise/ContextInfo;Lcom/sec/enterprise/knox/certificate/PermissionApplicationPrivateKey;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1383
    :goto_0
    return v1

    .line 1379
    :catch_0
    move-exception v0

    .line 1380
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with certificate policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1383
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public removeTrustedCaCertificateList(Ljava/util/List;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/security/cert/X509Certificate;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p1, "certificates":Ljava/util/List;, "Ljava/util/List<Ljava/security/cert/X509Certificate;>;"
    const/4 v4, 0x0

    .line 507
    iget-object v5, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v6, "CertificatePolicy.removeTrustedCaCertificateList"

    invoke-static {v5, v6}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 508
    invoke-direct {p0}, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->getService()Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 510
    if-eqz p1, :cond_0

    .line 511
    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 512
    .local v1, "certList":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/CertificateInfo;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/security/cert/X509Certificate;

    .line 513
    .local v0, "cert":Ljava/security/cert/X509Certificate;
    if-nez v0, :cond_1

    .line 524
    .end local v0    # "cert":Ljava/security/cert/X509Certificate;
    .end local v1    # "certList":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/CertificateInfo;>;"
    .end local v3    # "i$":Ljava/util/Iterator;
    :cond_0
    :goto_1
    return v4

    .line 516
    .restart local v0    # "cert":Ljava/security/cert/X509Certificate;
    .restart local v1    # "certList":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/CertificateInfo;>;"
    .restart local v3    # "i$":Ljava/util/Iterator;
    :cond_1
    new-instance v5, Landroid/app/enterprise/CertificateInfo;

    invoke-direct {v5, v0}, Landroid/app/enterprise/CertificateInfo;-><init>(Ljava/security/cert/Certificate;)V

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 520
    .end local v0    # "cert":Ljava/security/cert/X509Certificate;
    .end local v1    # "certList":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/CertificateInfo;>;"
    .end local v3    # "i$":Ljava/util/Iterator;
    :catch_0
    move-exception v2

    .line 521
    .local v2, "e":Landroid/os/RemoteException;
    sget-object v5, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->TAG:Ljava/lang/String;

    const-string v6, "Failed talking with certificate policy"

    invoke-static {v5, v6, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 518
    .end local v2    # "e":Landroid/os/RemoteException;
    .restart local v1    # "certList":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/CertificateInfo;>;"
    .restart local v3    # "i$":Ljava/util/Iterator;
    :cond_2
    :try_start_1
    iget-object v5, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mService:Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;

    iget-object v6, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v5, v6, v1}, Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;->removeTrustedCaCertificateList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v4

    goto :goto_1
.end method

.method public removeUntrustedCertificateList(Ljava/util/List;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/security/cert/X509Certificate;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p1, "certificates":Ljava/util/List;, "Ljava/util/List<Ljava/security/cert/X509Certificate;>;"
    const/4 v4, 0x0

    .line 647
    iget-object v5, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v6, "CertificatePolicy.removeUntrustedCertificateList"

    invoke-static {v5, v6}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 648
    invoke-direct {p0}, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->getService()Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 650
    if-eqz p1, :cond_0

    .line 651
    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 652
    .local v1, "certList":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/CertificateInfo;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/security/cert/X509Certificate;

    .line 653
    .local v0, "cert":Ljava/security/cert/X509Certificate;
    if-nez v0, :cond_1

    .line 664
    .end local v0    # "cert":Ljava/security/cert/X509Certificate;
    .end local v1    # "certList":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/CertificateInfo;>;"
    .end local v3    # "i$":Ljava/util/Iterator;
    :cond_0
    :goto_1
    return v4

    .line 656
    .restart local v0    # "cert":Ljava/security/cert/X509Certificate;
    .restart local v1    # "certList":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/CertificateInfo;>;"
    .restart local v3    # "i$":Ljava/util/Iterator;
    :cond_1
    new-instance v5, Landroid/app/enterprise/CertificateInfo;

    invoke-direct {v5, v0}, Landroid/app/enterprise/CertificateInfo;-><init>(Ljava/security/cert/Certificate;)V

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 660
    .end local v0    # "cert":Ljava/security/cert/X509Certificate;
    .end local v1    # "certList":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/CertificateInfo;>;"
    .end local v3    # "i$":Ljava/util/Iterator;
    :catch_0
    move-exception v2

    .line 661
    .local v2, "e":Landroid/os/RemoteException;
    sget-object v5, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->TAG:Ljava/lang/String;

    const-string v6, "Failed talking with certificate policy"

    invoke-static {v5, v6, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 658
    .end local v2    # "e":Landroid/os/RemoteException;
    .restart local v1    # "certList":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/CertificateInfo;>;"
    .restart local v3    # "i$":Ljava/util/Iterator;
    :cond_2
    :try_start_1
    iget-object v5, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mService:Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;

    iget-object v6, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v5, v6, v1}, Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;->removeUntrustedCertificateList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v4

    goto :goto_1
.end method

.method public setNonTrustedAppInstallBlock(Z)Z
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 1454
    iget-object v1, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "CertificatePolicy.setNonTrustedAppInstallBlock"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1455
    invoke-direct {p0}, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->getRestrictionService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1457
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mRestrictionService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->setNonTrustedAppInstallBlock(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1462
    :goto_0
    return v1

    .line 1458
    :catch_0
    move-exception v0

    .line 1459
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1462
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public validateCertificateAtInstall([B)I
    .locals 9
    .param p1, "certBytes"    # [B

    .prologue
    const/4 v5, -0x1

    .line 1307
    iget-object v6, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v7, "CertificatePolicy.validateCertificateAtInstall"

    invoke-static {v6, v7}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1308
    invoke-direct {p0}, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->getService()Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 1310
    const/4 v0, 0x0

    .line 1312
    .local v0, "cert":Ljava/security/cert/X509Certificate;
    :try_start_0
    const-string v6, "X.509"

    invoke-static {v6}, Ljava/security/cert/CertificateFactory;->getInstance(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;

    move-result-object v1

    .line 1313
    .local v1, "certFactory":Ljava/security/cert/CertificateFactory;
    new-instance v4, Ljava/io/ByteArrayInputStream;

    invoke-direct {v4, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 1314
    .local v4, "in":Ljava/io/InputStream;
    invoke-virtual {v1, v4}, Ljava/security/cert/CertificateFactory;->generateCertificate(Ljava/io/InputStream;)Ljava/security/cert/Certificate;

    move-result-object v0

    .end local v0    # "cert":Ljava/security/cert/X509Certificate;
    check-cast v0, Ljava/security/cert/X509Certificate;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1321
    .restart local v0    # "cert":Ljava/security/cert/X509Certificate;
    :try_start_1
    new-instance v2, Landroid/app/enterprise/CertificateInfo;

    invoke-direct {v2, v0}, Landroid/app/enterprise/CertificateInfo;-><init>(Ljava/security/cert/Certificate;)V

    .line 1322
    .local v2, "certInfo":Landroid/app/enterprise/CertificateInfo;
    iget-object v6, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mService:Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;

    invoke-interface {v6, v2}, Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;->validateCertificateAtInstall(Landroid/app/enterprise/CertificateInfo;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v5

    .line 1327
    .end local v0    # "cert":Ljava/security/cert/X509Certificate;
    .end local v1    # "certFactory":Ljava/security/cert/CertificateFactory;
    .end local v2    # "certInfo":Landroid/app/enterprise/CertificateInfo;
    .end local v4    # "in":Ljava/io/InputStream;
    :cond_0
    :goto_0
    return v5

    .line 1315
    :catch_0
    move-exception v3

    .line 1316
    .local v3, "e":Ljava/lang/Exception;
    sget-object v6, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Problem converting certificate! "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1323
    .end local v3    # "e":Ljava/lang/Exception;
    .restart local v0    # "cert":Ljava/security/cert/X509Certificate;
    .restart local v1    # "certFactory":Ljava/security/cert/CertificateFactory;
    .restart local v4    # "in":Ljava/io/InputStream;
    :catch_1
    move-exception v3

    .line 1324
    .local v3, "e":Landroid/os/RemoteException;
    sget-object v6, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->TAG:Ljava/lang/String;

    const-string v7, "Failed talking with certificate policy"

    invoke-static {v6, v7, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public validateChainAtInstall(Ljava/util/List;)I
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/security/cert/X509Certificate;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 1335
    .local p1, "listCerts":Ljava/util/List;, "Ljava/util/List<Ljava/security/cert/X509Certificate;>;"
    iget-object v4, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "CertificatePolicy.validateChainAtInstall"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1336
    invoke-direct {p0}, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->getService()Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 1339
    if-eqz p1, :cond_0

    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    if-eqz v4, :cond_0

    .line 1340
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1341
    .local v3, "listCertInfo":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/CertificateInfo;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/security/cert/X509Certificate;

    .line 1342
    .local v0, "cert":Ljava/security/cert/X509Certificate;
    new-instance v4, Landroid/app/enterprise/CertificateInfo;

    invoke-direct {v4, v0}, Landroid/app/enterprise/CertificateInfo;-><init>(Ljava/security/cert/Certificate;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1346
    .end local v0    # "cert":Ljava/security/cert/X509Certificate;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "listCertInfo":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/CertificateInfo;>;"
    :catch_0
    move-exception v1

    .line 1347
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed talking with certificate policy"

    invoke-static {v4, v5, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1350
    .end local v1    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v4, -0x1

    :goto_1
    return v4

    .line 1344
    .restart local v2    # "i$":Ljava/util/Iterator;
    .restart local v3    # "listCertInfo":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/CertificateInfo;>;"
    :cond_1
    :try_start_1
    iget-object v4, p0, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->mService:Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;

    invoke-interface {v4, v3}, Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;->validateChainAtInstall(Ljava/util/List;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v4

    goto :goto_1
.end method
