.class public Lcom/sec/enterprise/knox/certificate/PermissionApplicationPrivateKey;
.super Ljava/lang/Object;
.source "PermissionApplicationPrivateKey.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/enterprise/knox/certificate/PermissionApplicationPrivateKey;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mAdminPkgName:Ljava/lang/String;

.field private mAlias:Ljava/lang/String;

.field private mHost:Ljava/lang/String;

.field private mPackageName:Ljava/lang/String;

.field private mPort:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 82
    new-instance v0, Lcom/sec/enterprise/knox/certificate/PermissionApplicationPrivateKey$1;

    invoke-direct {v0}, Lcom/sec/enterprise/knox/certificate/PermissionApplicationPrivateKey$1;-><init>()V

    sput-object v0, Lcom/sec/enterprise/knox/certificate/PermissionApplicationPrivateKey;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v1, 0x0

    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object v1, p0, Lcom/sec/enterprise/knox/certificate/PermissionApplicationPrivateKey;->mAdminPkgName:Ljava/lang/String;

    .line 42
    iput-object v1, p0, Lcom/sec/enterprise/knox/certificate/PermissionApplicationPrivateKey;->mPackageName:Ljava/lang/String;

    .line 43
    iput-object v1, p0, Lcom/sec/enterprise/knox/certificate/PermissionApplicationPrivateKey;->mHost:Ljava/lang/String;

    .line 44
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/enterprise/knox/certificate/PermissionApplicationPrivateKey;->mPort:I

    .line 45
    iput-object v1, p0, Lcom/sec/enterprise/knox/certificate/PermissionApplicationPrivateKey;->mAlias:Ljava/lang/String;

    .line 94
    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/certificate/PermissionApplicationPrivateKey;->readFromParcel(Landroid/os/Parcel;)V

    .line 95
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/enterprise/knox/certificate/PermissionApplicationPrivateKey$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/sec/enterprise/knox/certificate/PermissionApplicationPrivateKey$1;

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/certificate/PermissionApplicationPrivateKey;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 2
    .param p1, "mPackageName"    # Ljava/lang/String;
    .param p2, "mHost"    # Ljava/lang/String;
    .param p3, "mPort"    # I
    .param p4, "mAlias"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object v1, p0, Lcom/sec/enterprise/knox/certificate/PermissionApplicationPrivateKey;->mAdminPkgName:Ljava/lang/String;

    .line 42
    iput-object v1, p0, Lcom/sec/enterprise/knox/certificate/PermissionApplicationPrivateKey;->mPackageName:Ljava/lang/String;

    .line 43
    iput-object v1, p0, Lcom/sec/enterprise/knox/certificate/PermissionApplicationPrivateKey;->mHost:Ljava/lang/String;

    .line 44
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/enterprise/knox/certificate/PermissionApplicationPrivateKey;->mPort:I

    .line 45
    iput-object v1, p0, Lcom/sec/enterprise/knox/certificate/PermissionApplicationPrivateKey;->mAlias:Ljava/lang/String;

    .line 54
    iput-object p1, p0, Lcom/sec/enterprise/knox/certificate/PermissionApplicationPrivateKey;->mPackageName:Ljava/lang/String;

    .line 55
    iput-object p2, p0, Lcom/sec/enterprise/knox/certificate/PermissionApplicationPrivateKey;->mHost:Ljava/lang/String;

    .line 56
    iput p3, p0, Lcom/sec/enterprise/knox/certificate/PermissionApplicationPrivateKey;->mPort:I

    .line 57
    iput-object p4, p0, Lcom/sec/enterprise/knox/certificate/PermissionApplicationPrivateKey;->mAlias:Ljava/lang/String;

    .line 58
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 76
    const/4 v0, 0x0

    return v0
.end method

.method public getAdminPkgName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/sec/enterprise/knox/certificate/PermissionApplicationPrivateKey;->mAdminPkgName:Ljava/lang/String;

    return-object v0
.end method

.method public getAlias()Ljava/lang/String;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/sec/enterprise/knox/certificate/PermissionApplicationPrivateKey;->mAlias:Ljava/lang/String;

    return-object v0
.end method

.method public getHost()Ljava/lang/String;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/sec/enterprise/knox/certificate/PermissionApplicationPrivateKey;->mHost:Ljava/lang/String;

    return-object v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/sec/enterprise/knox/certificate/PermissionApplicationPrivateKey;->mPackageName:Ljava/lang/String;

    return-object v0
.end method

.method public getPort()I
    .locals 1

    .prologue
    .line 155
    iget v0, p0, Lcom/sec/enterprise/knox/certificate/PermissionApplicationPrivateKey;->mPort:I

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 112
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/certificate/PermissionApplicationPrivateKey;->mAdminPkgName:Ljava/lang/String;

    .line 113
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/certificate/PermissionApplicationPrivateKey;->mPackageName:Ljava/lang/String;

    .line 114
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/certificate/PermissionApplicationPrivateKey;->mHost:Ljava/lang/String;

    .line 115
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/enterprise/knox/certificate/PermissionApplicationPrivateKey;->mPort:I

    .line 116
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/certificate/PermissionApplicationPrivateKey;->mAlias:Ljava/lang/String;

    .line 117
    return-void
.end method

.method public setAdminPkgName(Ljava/lang/String;)V
    .locals 0
    .param p1, "pkgName"    # Ljava/lang/String;

    .prologue
    .line 123
    iput-object p1, p0, Lcom/sec/enterprise/knox/certificate/PermissionApplicationPrivateKey;->mAdminPkgName:Ljava/lang/String;

    .line 124
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 64
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PermissionApplicationPrivateKey\nmAdminPackageName: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/enterprise/knox/certificate/PermissionApplicationPrivateKey;->mAdminPkgName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\nmPackageName: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/enterprise/knox/certificate/PermissionApplicationPrivateKey;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\nmHost: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/enterprise/knox/certificate/PermissionApplicationPrivateKey;->mHost:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\nmPort: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/enterprise/knox/certificate/PermissionApplicationPrivateKey;->mPort:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\nmAlias: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/enterprise/knox/certificate/PermissionApplicationPrivateKey;->mAlias:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flag"    # I

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/enterprise/knox/certificate/PermissionApplicationPrivateKey;->mAdminPkgName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 102
    iget-object v0, p0, Lcom/sec/enterprise/knox/certificate/PermissionApplicationPrivateKey;->mPackageName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 103
    iget-object v0, p0, Lcom/sec/enterprise/knox/certificate/PermissionApplicationPrivateKey;->mHost:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 104
    iget v0, p0, Lcom/sec/enterprise/knox/certificate/PermissionApplicationPrivateKey;->mPort:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 105
    iget-object v0, p0, Lcom/sec/enterprise/knox/certificate/PermissionApplicationPrivateKey;->mAlias:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 106
    return-void
.end method
