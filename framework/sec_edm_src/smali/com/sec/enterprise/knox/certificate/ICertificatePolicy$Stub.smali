.class public abstract Lcom/sec/enterprise/knox/certificate/ICertificatePolicy$Stub;
.super Landroid/os/Binder;
.source "ICertificatePolicy.java"

# interfaces
.implements Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/enterprise/knox/certificate/ICertificatePolicy$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.sec.enterprise.knox.certificate.ICertificatePolicy"

.field static final TRANSACTION_addPermissionApplicationPrivateKey:I = 0x18

.field static final TRANSACTION_addTrustedCaCertificateList:I = 0x1

.field static final TRANSACTION_addUntrustedCertificateList:I = 0x7

.field static final TRANSACTION_allowUserRemoveCertificates:I = 0x16

.field static final TRANSACTION_clearPermissionApplicationPrivateKey:I = 0x1a

.field static final TRANSACTION_clearTrustedCaCertificateList:I = 0x4

.field static final TRANSACTION_clearUntrustedCertificateList:I = 0xa

.field static final TRANSACTION_enableCertificateFailureNotification:I = 0xe

.field static final TRANSACTION_enableCertificateValidationAtInstall:I = 0x11

.field static final TRANSACTION_enableSignatureIdentityInformation:I = 0xb

.field static final TRANSACTION_getIdentitiesFromSignatures:I = 0xd

.field static final TRANSACTION_getListPermissionApplicationPrivateKey:I = 0x1b

.field static final TRANSACTION_getTrustedCaCertificateList:I = 0x2

.field static final TRANSACTION_getUntrustedCertificateList:I = 0x9

.field static final TRANSACTION_isCaCertificateDisabled:I = 0x6

.field static final TRANSACTION_isCaCertificateTrusted:I = 0x3

.field static final TRANSACTION_isCertificateFailureNotificationEnabled:I = 0xf

.field static final TRANSACTION_isCertificateValidationAtInstallEnabled:I = 0x12

.field static final TRANSACTION_isPrivateKeyApplicationPermitted:I = 0x1c

.field static final TRANSACTION_isPrivateKeyApplicationPermittedAsUser:I = 0x1d

.field static final TRANSACTION_isSignatureIdentityInformationEnabled:I = 0xc

.field static final TRANSACTION_isUserRemoveCertificatesAllowed:I = 0x17

.field static final TRANSACTION_notifyCertificateFailure:I = 0x10

.field static final TRANSACTION_notifyCertificateRemoved:I = 0x15

.field static final TRANSACTION_notifyUserKeystoreUnlocked:I = 0x1e

.field static final TRANSACTION_removePermissionApplicationPrivateKey:I = 0x19

.field static final TRANSACTION_removeTrustedCaCertificateList:I = 0x5

.field static final TRANSACTION_removeUntrustedCertificateList:I = 0x8

.field static final TRANSACTION_validateCertificateAtInstall:I = 0x13

.field static final TRANSACTION_validateChainAtInstall:I = 0x14


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 18
    const-string v0, "com.sec.enterprise.knox.certificate.ICertificatePolicy"

    invoke-virtual {p0, p0, v0}, Lcom/sec/enterprise/knox/certificate/ICertificatePolicy$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 19
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 26
    if-nez p0, :cond_0

    .line 27
    const/4 v0, 0x0

    .line 33
    :goto_0
    return-object v0

    .line 29
    :cond_0
    const-string v1, "com.sec.enterprise.knox.certificate.ICertificatePolicy"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 30
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;

    if-eqz v1, :cond_1

    .line 31
    check-cast v0, Lcom/sec/enterprise/knox/certificate/ICertificatePolicy;

    goto :goto_0

    .line 33
    :cond_1
    new-instance v0, Lcom/sec/enterprise/knox/certificate/ICertificatePolicy$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/sec/enterprise/knox/certificate/ICertificatePolicy$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 37
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 10
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 41
    sparse-switch p1, :sswitch_data_0

    .line 532
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v9

    :goto_0
    return v9

    .line 45
    :sswitch_0
    const-string v9, "com.sec.enterprise.knox.certificate.ICertificatePolicy"

    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 46
    const/4 v9, 0x1

    goto :goto_0

    .line 50
    :sswitch_1
    const-string v9, "com.sec.enterprise.knox.certificate.ICertificatePolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 52
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_0

    .line 53
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 59
    .local v0, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_1
    sget-object v9, Landroid/app/enterprise/CertificateInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v3

    .line 60
    .local v3, "_arg1":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/CertificateInfo;>;"
    invoke-virtual {p0, v0, v3}, Lcom/sec/enterprise/knox/certificate/ICertificatePolicy$Stub;->addTrustedCaCertificateList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z

    move-result v6

    .line 61
    .local v6, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 62
    if-eqz v6, :cond_1

    const/4 v9, 0x1

    :goto_2
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 63
    const/4 v9, 0x1

    goto :goto_0

    .line 56
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v3    # "_arg1":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/CertificateInfo;>;"
    .end local v6    # "_result":Z
    :cond_0
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_1

    .line 62
    .restart local v3    # "_arg1":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/CertificateInfo;>;"
    .restart local v6    # "_result":Z
    :cond_1
    const/4 v9, 0x0

    goto :goto_2

    .line 67
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v3    # "_arg1":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/CertificateInfo;>;"
    .end local v6    # "_result":Z
    :sswitch_2
    const-string v9, "com.sec.enterprise.knox.certificate.ICertificatePolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 69
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_2

    .line 70
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 75
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_3
    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/certificate/ICertificatePolicy$Stub;->getTrustedCaCertificateList(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;

    move-result-object v7

    .line 76
    .local v7, "_result":Ljava/util/List;, "Ljava/util/List<Lcom/sec/enterprise/knox/certificate/CertificateControlInfo;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 77
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 78
    const/4 v9, 0x1

    goto :goto_0

    .line 73
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v7    # "_result":Ljava/util/List;, "Ljava/util/List<Lcom/sec/enterprise/knox/certificate/CertificateControlInfo;>;"
    :cond_2
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_3

    .line 82
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_3
    const-string v9, "com.sec.enterprise.knox.certificate.ICertificatePolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 84
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_3

    .line 85
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 91
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_4
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_4

    .line 92
    sget-object v9, Landroid/app/enterprise/CertificateInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/enterprise/CertificateInfo;

    .line 98
    .local v2, "_arg1":Landroid/app/enterprise/CertificateInfo;
    :goto_5
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_5

    const/4 v4, 0x1

    .line 99
    .local v4, "_arg2":Z
    :goto_6
    invoke-virtual {p0, v0, v2, v4}, Lcom/sec/enterprise/knox/certificate/ICertificatePolicy$Stub;->isCaCertificateTrusted(Landroid/app/enterprise/ContextInfo;Landroid/app/enterprise/CertificateInfo;Z)Z

    move-result v6

    .line 100
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 101
    if-eqz v6, :cond_6

    const/4 v9, 0x1

    :goto_7
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 102
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 88
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Landroid/app/enterprise/CertificateInfo;
    .end local v4    # "_arg2":Z
    .end local v6    # "_result":Z
    :cond_3
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_4

    .line 95
    :cond_4
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Landroid/app/enterprise/CertificateInfo;
    goto :goto_5

    .line 98
    :cond_5
    const/4 v4, 0x0

    goto :goto_6

    .line 101
    .restart local v4    # "_arg2":Z
    .restart local v6    # "_result":Z
    :cond_6
    const/4 v9, 0x0

    goto :goto_7

    .line 106
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Landroid/app/enterprise/CertificateInfo;
    .end local v4    # "_arg2":Z
    .end local v6    # "_result":Z
    :sswitch_4
    const-string v9, "com.sec.enterprise.knox.certificate.ICertificatePolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 108
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_7

    .line 109
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 114
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_8
    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/certificate/ICertificatePolicy$Stub;->clearTrustedCaCertificateList(Landroid/app/enterprise/ContextInfo;)Z

    move-result v6

    .line 115
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 116
    if-eqz v6, :cond_8

    const/4 v9, 0x1

    :goto_9
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 117
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 112
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Z
    :cond_7
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_8

    .line 116
    .restart local v6    # "_result":Z
    :cond_8
    const/4 v9, 0x0

    goto :goto_9

    .line 121
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Z
    :sswitch_5
    const-string v9, "com.sec.enterprise.knox.certificate.ICertificatePolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 123
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_9

    .line 124
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 130
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_a
    sget-object v9, Landroid/app/enterprise/CertificateInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v3

    .line 131
    .restart local v3    # "_arg1":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/CertificateInfo;>;"
    invoke-virtual {p0, v0, v3}, Lcom/sec/enterprise/knox/certificate/ICertificatePolicy$Stub;->removeTrustedCaCertificateList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z

    move-result v6

    .line 132
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 133
    if-eqz v6, :cond_a

    const/4 v9, 0x1

    :goto_b
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 134
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 127
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v3    # "_arg1":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/CertificateInfo;>;"
    .end local v6    # "_result":Z
    :cond_9
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_a

    .line 133
    .restart local v3    # "_arg1":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/CertificateInfo;>;"
    .restart local v6    # "_result":Z
    :cond_a
    const/4 v9, 0x0

    goto :goto_b

    .line 138
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v3    # "_arg1":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/CertificateInfo;>;"
    .end local v6    # "_result":Z
    :sswitch_6
    const-string v9, "com.sec.enterprise.knox.certificate.ICertificatePolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 140
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_b

    .line 141
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 147
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_c
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 148
    .local v2, "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v2}, Lcom/sec/enterprise/knox/certificate/ICertificatePolicy$Stub;->isCaCertificateDisabled(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v6

    .line 149
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 150
    if-eqz v6, :cond_c

    const/4 v9, 0x1

    :goto_d
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 151
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 144
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Z
    :cond_b
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_c

    .line 150
    .restart local v2    # "_arg1":Ljava/lang/String;
    .restart local v6    # "_result":Z
    :cond_c
    const/4 v9, 0x0

    goto :goto_d

    .line 155
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Z
    :sswitch_7
    const-string v9, "com.sec.enterprise.knox.certificate.ICertificatePolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 157
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_d

    .line 158
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 164
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_e
    sget-object v9, Landroid/app/enterprise/CertificateInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v3

    .line 165
    .restart local v3    # "_arg1":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/CertificateInfo;>;"
    invoke-virtual {p0, v0, v3}, Lcom/sec/enterprise/knox/certificate/ICertificatePolicy$Stub;->addUntrustedCertificateList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z

    move-result v6

    .line 166
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 167
    if-eqz v6, :cond_e

    const/4 v9, 0x1

    :goto_f
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 168
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 161
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v3    # "_arg1":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/CertificateInfo;>;"
    .end local v6    # "_result":Z
    :cond_d
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_e

    .line 167
    .restart local v3    # "_arg1":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/CertificateInfo;>;"
    .restart local v6    # "_result":Z
    :cond_e
    const/4 v9, 0x0

    goto :goto_f

    .line 172
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v3    # "_arg1":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/CertificateInfo;>;"
    .end local v6    # "_result":Z
    :sswitch_8
    const-string v9, "com.sec.enterprise.knox.certificate.ICertificatePolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 174
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_f

    .line 175
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 181
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_10
    sget-object v9, Landroid/app/enterprise/CertificateInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v3

    .line 182
    .restart local v3    # "_arg1":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/CertificateInfo;>;"
    invoke-virtual {p0, v0, v3}, Lcom/sec/enterprise/knox/certificate/ICertificatePolicy$Stub;->removeUntrustedCertificateList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z

    move-result v6

    .line 183
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 184
    if-eqz v6, :cond_10

    const/4 v9, 0x1

    :goto_11
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 185
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 178
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v3    # "_arg1":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/CertificateInfo;>;"
    .end local v6    # "_result":Z
    :cond_f
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_10

    .line 184
    .restart local v3    # "_arg1":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/CertificateInfo;>;"
    .restart local v6    # "_result":Z
    :cond_10
    const/4 v9, 0x0

    goto :goto_11

    .line 189
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v3    # "_arg1":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/CertificateInfo;>;"
    .end local v6    # "_result":Z
    :sswitch_9
    const-string v9, "com.sec.enterprise.knox.certificate.ICertificatePolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 191
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_11

    .line 192
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 197
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_12
    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/certificate/ICertificatePolicy$Stub;->getUntrustedCertificateList(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;

    move-result-object v7

    .line 198
    .restart local v7    # "_result":Ljava/util/List;, "Ljava/util/List<Lcom/sec/enterprise/knox/certificate/CertificateControlInfo;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 199
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 200
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 195
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v7    # "_result":Ljava/util/List;, "Ljava/util/List<Lcom/sec/enterprise/knox/certificate/CertificateControlInfo;>;"
    :cond_11
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_12

    .line 204
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_a
    const-string v9, "com.sec.enterprise.knox.certificate.ICertificatePolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 206
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_12

    .line 207
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 212
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_13
    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/certificate/ICertificatePolicy$Stub;->clearUntrustedCertificateList(Landroid/app/enterprise/ContextInfo;)Z

    move-result v6

    .line 213
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 214
    if-eqz v6, :cond_13

    const/4 v9, 0x1

    :goto_14
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 215
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 210
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Z
    :cond_12
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_13

    .line 214
    .restart local v6    # "_result":Z
    :cond_13
    const/4 v9, 0x0

    goto :goto_14

    .line 219
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Z
    :sswitch_b
    const-string v9, "com.sec.enterprise.knox.certificate.ICertificatePolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 221
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_14

    .line 222
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 228
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_15
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_15

    const/4 v2, 0x1

    .line 229
    .local v2, "_arg1":Z
    :goto_16
    invoke-virtual {p0, v0, v2}, Lcom/sec/enterprise/knox/certificate/ICertificatePolicy$Stub;->enableSignatureIdentityInformation(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v6

    .line 230
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 231
    if-eqz v6, :cond_16

    const/4 v9, 0x1

    :goto_17
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 232
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 225
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Z
    .end local v6    # "_result":Z
    :cond_14
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_15

    .line 228
    :cond_15
    const/4 v2, 0x0

    goto :goto_16

    .line 231
    .restart local v2    # "_arg1":Z
    .restart local v6    # "_result":Z
    :cond_16
    const/4 v9, 0x0

    goto :goto_17

    .line 236
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Z
    .end local v6    # "_result":Z
    :sswitch_c
    const-string v9, "com.sec.enterprise.knox.certificate.ICertificatePolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 238
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_17

    .line 239
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 245
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_18
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_18

    const/4 v2, 0x1

    .line 246
    .restart local v2    # "_arg1":Z
    :goto_19
    invoke-virtual {p0, v0, v2}, Lcom/sec/enterprise/knox/certificate/ICertificatePolicy$Stub;->isSignatureIdentityInformationEnabled(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v6

    .line 247
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 248
    if-eqz v6, :cond_19

    const/4 v9, 0x1

    :goto_1a
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 249
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 242
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Z
    .end local v6    # "_result":Z
    :cond_17
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_18

    .line 245
    :cond_18
    const/4 v2, 0x0

    goto :goto_19

    .line 248
    .restart local v2    # "_arg1":Z
    .restart local v6    # "_result":Z
    :cond_19
    const/4 v9, 0x0

    goto :goto_1a

    .line 253
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Z
    .end local v6    # "_result":Z
    :sswitch_d
    const-string v9, "com.sec.enterprise.knox.certificate.ICertificatePolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 255
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_1a

    .line 256
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 262
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_1b
    sget-object v9, Landroid/content/pm/Signature;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->createTypedArray(Landroid/os/Parcelable$Creator;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Landroid/content/pm/Signature;

    .line 263
    .local v2, "_arg1":[Landroid/content/pm/Signature;
    invoke-virtual {p0, v0, v2}, Lcom/sec/enterprise/knox/certificate/ICertificatePolicy$Stub;->getIdentitiesFromSignatures(Landroid/app/enterprise/ContextInfo;[Landroid/content/pm/Signature;)Ljava/util/List;

    move-result-object v6

    .line 264
    .local v6, "_result":Ljava/util/List;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 265
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 266
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 259
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":[Landroid/content/pm/Signature;
    .end local v6    # "_result":Ljava/util/List;
    :cond_1a
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_1b

    .line 270
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_e
    const-string v9, "com.sec.enterprise.knox.certificate.ICertificatePolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 272
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_1b

    .line 273
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 279
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_1c
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_1c

    const/4 v2, 0x1

    .line 280
    .local v2, "_arg1":Z
    :goto_1d
    invoke-virtual {p0, v0, v2}, Lcom/sec/enterprise/knox/certificate/ICertificatePolicy$Stub;->enableCertificateFailureNotification(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v6

    .line 281
    .local v6, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 282
    if-eqz v6, :cond_1d

    const/4 v9, 0x1

    :goto_1e
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 283
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 276
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Z
    .end local v6    # "_result":Z
    :cond_1b
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_1c

    .line 279
    :cond_1c
    const/4 v2, 0x0

    goto :goto_1d

    .line 282
    .restart local v2    # "_arg1":Z
    .restart local v6    # "_result":Z
    :cond_1d
    const/4 v9, 0x0

    goto :goto_1e

    .line 287
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Z
    .end local v6    # "_result":Z
    :sswitch_f
    const-string v9, "com.sec.enterprise.knox.certificate.ICertificatePolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 289
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_1e

    .line 290
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 295
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_1f
    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/certificate/ICertificatePolicy$Stub;->isCertificateFailureNotificationEnabled(Landroid/app/enterprise/ContextInfo;)Z

    move-result v6

    .line 296
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 297
    if-eqz v6, :cond_1f

    const/4 v9, 0x1

    :goto_20
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 298
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 293
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Z
    :cond_1e
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_1f

    .line 297
    .restart local v6    # "_result":Z
    :cond_1f
    const/4 v9, 0x0

    goto :goto_20

    .line 302
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Z
    :sswitch_10
    const-string v9, "com.sec.enterprise.knox.certificate.ICertificatePolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 304
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 306
    .local v0, "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 308
    .local v2, "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_20

    const/4 v4, 0x1

    .line 309
    .restart local v4    # "_arg2":Z
    :goto_21
    invoke-virtual {p0, v0, v2, v4}, Lcom/sec/enterprise/knox/certificate/ICertificatePolicy$Stub;->notifyCertificateFailure(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 310
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 311
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 308
    .end local v4    # "_arg2":Z
    :cond_20
    const/4 v4, 0x0

    goto :goto_21

    .line 315
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":Ljava/lang/String;
    :sswitch_11
    const-string v9, "com.sec.enterprise.knox.certificate.ICertificatePolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 317
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_21

    .line 318
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 324
    .local v0, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_22
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_22

    const/4 v2, 0x1

    .line 325
    .local v2, "_arg1":Z
    :goto_23
    invoke-virtual {p0, v0, v2}, Lcom/sec/enterprise/knox/certificate/ICertificatePolicy$Stub;->enableCertificateValidationAtInstall(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v6

    .line 326
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 327
    if-eqz v6, :cond_23

    const/4 v9, 0x1

    :goto_24
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 328
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 321
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Z
    .end local v6    # "_result":Z
    :cond_21
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_22

    .line 324
    :cond_22
    const/4 v2, 0x0

    goto :goto_23

    .line 327
    .restart local v2    # "_arg1":Z
    .restart local v6    # "_result":Z
    :cond_23
    const/4 v9, 0x0

    goto :goto_24

    .line 332
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Z
    .end local v6    # "_result":Z
    :sswitch_12
    const-string v9, "com.sec.enterprise.knox.certificate.ICertificatePolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 334
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_24

    .line 335
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 340
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_25
    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/certificate/ICertificatePolicy$Stub;->isCertificateValidationAtInstallEnabled(Landroid/app/enterprise/ContextInfo;)Z

    move-result v6

    .line 341
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 342
    if-eqz v6, :cond_25

    const/4 v9, 0x1

    :goto_26
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 343
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 338
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Z
    :cond_24
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_25

    .line 342
    .restart local v6    # "_result":Z
    :cond_25
    const/4 v9, 0x0

    goto :goto_26

    .line 347
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Z
    :sswitch_13
    const-string v9, "com.sec.enterprise.knox.certificate.ICertificatePolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 349
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_26

    .line 350
    sget-object v9, Landroid/app/enterprise/CertificateInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/CertificateInfo;

    .line 355
    .local v0, "_arg0":Landroid/app/enterprise/CertificateInfo;
    :goto_27
    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/certificate/ICertificatePolicy$Stub;->validateCertificateAtInstall(Landroid/app/enterprise/CertificateInfo;)I

    move-result v6

    .line 356
    .local v6, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 357
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 358
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 353
    .end local v0    # "_arg0":Landroid/app/enterprise/CertificateInfo;
    .end local v6    # "_result":I
    :cond_26
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/CertificateInfo;
    goto :goto_27

    .line 362
    .end local v0    # "_arg0":Landroid/app/enterprise/CertificateInfo;
    :sswitch_14
    const-string v9, "com.sec.enterprise.knox.certificate.ICertificatePolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 364
    sget-object v9, Landroid/app/enterprise/CertificateInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v1

    .line 365
    .local v1, "_arg0":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/CertificateInfo;>;"
    invoke-virtual {p0, v1}, Lcom/sec/enterprise/knox/certificate/ICertificatePolicy$Stub;->validateChainAtInstall(Ljava/util/List;)I

    move-result v6

    .line 366
    .restart local v6    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 367
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 368
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 372
    .end local v1    # "_arg0":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/CertificateInfo;>;"
    .end local v6    # "_result":I
    :sswitch_15
    const-string v9, "com.sec.enterprise.knox.certificate.ICertificatePolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 374
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 375
    .local v0, "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/certificate/ICertificatePolicy$Stub;->notifyCertificateRemoved(Ljava/lang/String;)V

    .line 376
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 377
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 381
    .end local v0    # "_arg0":Ljava/lang/String;
    :sswitch_16
    const-string v9, "com.sec.enterprise.knox.certificate.ICertificatePolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 383
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_27

    .line 384
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 390
    .local v0, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_28
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_28

    const/4 v2, 0x1

    .line 391
    .restart local v2    # "_arg1":Z
    :goto_29
    invoke-virtual {p0, v0, v2}, Lcom/sec/enterprise/knox/certificate/ICertificatePolicy$Stub;->allowUserRemoveCertificates(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v6

    .line 392
    .local v6, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 393
    if-eqz v6, :cond_29

    const/4 v9, 0x1

    :goto_2a
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 394
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 387
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Z
    .end local v6    # "_result":Z
    :cond_27
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_28

    .line 390
    :cond_28
    const/4 v2, 0x0

    goto :goto_29

    .line 393
    .restart local v2    # "_arg1":Z
    .restart local v6    # "_result":Z
    :cond_29
    const/4 v9, 0x0

    goto :goto_2a

    .line 398
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Z
    .end local v6    # "_result":Z
    :sswitch_17
    const-string v9, "com.sec.enterprise.knox.certificate.ICertificatePolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 400
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_2a

    .line 401
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 406
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_2b
    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/certificate/ICertificatePolicy$Stub;->isUserRemoveCertificatesAllowed(Landroid/app/enterprise/ContextInfo;)Z

    move-result v6

    .line 407
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 408
    if-eqz v6, :cond_2b

    const/4 v9, 0x1

    :goto_2c
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 409
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 404
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Z
    :cond_2a
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_2b

    .line 408
    .restart local v6    # "_result":Z
    :cond_2b
    const/4 v9, 0x0

    goto :goto_2c

    .line 413
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Z
    :sswitch_18
    const-string v9, "com.sec.enterprise.knox.certificate.ICertificatePolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 415
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_2c

    .line 416
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 422
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_2d
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_2d

    .line 423
    sget-object v9, Lcom/sec/enterprise/knox/certificate/PermissionApplicationPrivateKey;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/enterprise/knox/certificate/PermissionApplicationPrivateKey;

    .line 428
    .local v2, "_arg1":Lcom/sec/enterprise/knox/certificate/PermissionApplicationPrivateKey;
    :goto_2e
    invoke-virtual {p0, v0, v2}, Lcom/sec/enterprise/knox/certificate/ICertificatePolicy$Stub;->addPermissionApplicationPrivateKey(Landroid/app/enterprise/ContextInfo;Lcom/sec/enterprise/knox/certificate/PermissionApplicationPrivateKey;)Z

    move-result v6

    .line 429
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 430
    if-eqz v6, :cond_2e

    const/4 v9, 0x1

    :goto_2f
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 431
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 419
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Lcom/sec/enterprise/knox/certificate/PermissionApplicationPrivateKey;
    .end local v6    # "_result":Z
    :cond_2c
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_2d

    .line 426
    :cond_2d
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Lcom/sec/enterprise/knox/certificate/PermissionApplicationPrivateKey;
    goto :goto_2e

    .line 430
    .restart local v6    # "_result":Z
    :cond_2e
    const/4 v9, 0x0

    goto :goto_2f

    .line 435
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Lcom/sec/enterprise/knox/certificate/PermissionApplicationPrivateKey;
    .end local v6    # "_result":Z
    :sswitch_19
    const-string v9, "com.sec.enterprise.knox.certificate.ICertificatePolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 437
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_2f

    .line 438
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 444
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_30
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_30

    .line 445
    sget-object v9, Lcom/sec/enterprise/knox/certificate/PermissionApplicationPrivateKey;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/enterprise/knox/certificate/PermissionApplicationPrivateKey;

    .line 450
    .restart local v2    # "_arg1":Lcom/sec/enterprise/knox/certificate/PermissionApplicationPrivateKey;
    :goto_31
    invoke-virtual {p0, v0, v2}, Lcom/sec/enterprise/knox/certificate/ICertificatePolicy$Stub;->removePermissionApplicationPrivateKey(Landroid/app/enterprise/ContextInfo;Lcom/sec/enterprise/knox/certificate/PermissionApplicationPrivateKey;)Z

    move-result v6

    .line 451
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 452
    if-eqz v6, :cond_31

    const/4 v9, 0x1

    :goto_32
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 453
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 441
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Lcom/sec/enterprise/knox/certificate/PermissionApplicationPrivateKey;
    .end local v6    # "_result":Z
    :cond_2f
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_30

    .line 448
    :cond_30
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Lcom/sec/enterprise/knox/certificate/PermissionApplicationPrivateKey;
    goto :goto_31

    .line 452
    .restart local v6    # "_result":Z
    :cond_31
    const/4 v9, 0x0

    goto :goto_32

    .line 457
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Lcom/sec/enterprise/knox/certificate/PermissionApplicationPrivateKey;
    .end local v6    # "_result":Z
    :sswitch_1a
    const-string v9, "com.sec.enterprise.knox.certificate.ICertificatePolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 459
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_32

    .line 460
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 465
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_33
    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/certificate/ICertificatePolicy$Stub;->clearPermissionApplicationPrivateKey(Landroid/app/enterprise/ContextInfo;)Z

    move-result v6

    .line 466
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 467
    if-eqz v6, :cond_33

    const/4 v9, 0x1

    :goto_34
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 468
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 463
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Z
    :cond_32
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_33

    .line 467
    .restart local v6    # "_result":Z
    :cond_33
    const/4 v9, 0x0

    goto :goto_34

    .line 472
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Z
    :sswitch_1b
    const-string v9, "com.sec.enterprise.knox.certificate.ICertificatePolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 474
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_34

    .line 475
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 480
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_35
    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/certificate/ICertificatePolicy$Stub;->getListPermissionApplicationPrivateKey(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;

    move-result-object v8

    .line 481
    .local v8, "_result":Ljava/util/List;, "Ljava/util/List<Lcom/sec/enterprise/knox/certificate/PermissionApplicationPrivateKey;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 482
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 483
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 478
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v8    # "_result":Ljava/util/List;, "Ljava/util/List<Lcom/sec/enterprise/knox/certificate/PermissionApplicationPrivateKey;>;"
    :cond_34
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_35

    .line 487
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_1c
    const-string v9, "com.sec.enterprise.knox.certificate.ICertificatePolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 489
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_35

    .line 490
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 496
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_36
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 498
    .local v2, "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 500
    .local v4, "_arg2":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    .line 501
    .local v5, "_arg3":I
    invoke-virtual {p0, v0, v2, v4, v5}, Lcom/sec/enterprise/knox/certificate/ICertificatePolicy$Stub;->isPrivateKeyApplicationPermitted(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v6

    .line 502
    .local v6, "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 503
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 504
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 493
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v4    # "_arg2":Ljava/lang/String;
    .end local v5    # "_arg3":I
    .end local v6    # "_result":Ljava/lang/String;
    :cond_35
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_36

    .line 508
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_1d
    const-string v9, "com.sec.enterprise.knox.certificate.ICertificatePolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 510
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 512
    .local v0, "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 514
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 516
    .local v4, "_arg2":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    .line 517
    .restart local v5    # "_arg3":I
    invoke-virtual {p0, v0, v2, v4, v5}, Lcom/sec/enterprise/knox/certificate/ICertificatePolicy$Stub;->isPrivateKeyApplicationPermittedAsUser(Ljava/lang/String;Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v6

    .line 518
    .restart local v6    # "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 519
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 520
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 524
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v4    # "_arg2":I
    .end local v5    # "_arg3":I
    .end local v6    # "_result":Ljava/lang/String;
    :sswitch_1e
    const-string v9, "com.sec.enterprise.knox.certificate.ICertificatePolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 526
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 527
    .local v0, "_arg0":I
    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/certificate/ICertificatePolicy$Stub;->notifyUserKeystoreUnlocked(I)V

    .line 528
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 529
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 41
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0xe -> :sswitch_e
        0xf -> :sswitch_f
        0x10 -> :sswitch_10
        0x11 -> :sswitch_11
        0x12 -> :sswitch_12
        0x13 -> :sswitch_13
        0x14 -> :sswitch_14
        0x15 -> :sswitch_15
        0x16 -> :sswitch_16
        0x17 -> :sswitch_17
        0x18 -> :sswitch_18
        0x19 -> :sswitch_19
        0x1a -> :sswitch_1a
        0x1b -> :sswitch_1b
        0x1c -> :sswitch_1c
        0x1d -> :sswitch_1d
        0x1e -> :sswitch_1e
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
