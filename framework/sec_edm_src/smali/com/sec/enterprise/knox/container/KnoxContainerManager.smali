.class public Lcom/sec/enterprise/knox/container/KnoxContainerManager;
.super Ljava/lang/Object;
.source "KnoxContainerManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/enterprise/knox/container/KnoxContainerManager$ConfigType;
    }
.end annotation


# static fields
.field public static final CONTAINER_ACTIVE:I = 0x5b

.field public static final CONTAINER_BROADCAST_PERMISSION:Ljava/lang/String; = "com.sec.enterprise.knox.permission.CONTAINER_BROADCAST"

.field public static final CONTAINER_CREATION_IN_PROGRESS:I = 0x5d

.field public static final CONTAINER_CREATION_REQUEST_ID:Ljava/lang/String; = "requestId"

.field public static final CONTAINER_CREATION_STATUS_CODE:Ljava/lang/String; = "code"

.field public static final CONTAINER_DOESNT_EXISTS:I = -0x1

.field public static final CONTAINER_ID:Ljava/lang/String; = "containerid"

.field public static final CONTAINER_INACTIVE:I = 0x5a

.field public static final CONTAINER_LOCKED:I = 0x5f

.field public static final CONTAINER_NEW_STATE:Ljava/lang/String; = "container_new_state"

.field public static final CONTAINER_OLD_STATE:Ljava/lang/String; = "container_old_state"

.field public static final CONTAINER_REMOVE_IN_PROGRESS:I = 0x5e

.field public static final ERROR_ADMIN_ACTIVATION_FAILED:I = -0x3f1

.field public static final ERROR_ADMIN_INSTALLATION_FAILED:I = -0x3f0

.field public static final ERROR_CONTAINER_MODE_CREATION_FAILED_BYOD_NOT_ALLOWED:I = -0x3ff

.field public static final ERROR_CONTAINER_MODE_CREATION_FAILED_CONTAINER_EXIST:I = -0x3fd

.field public static final ERROR_CONTAINER_MODE_CREATION_FAILED_KIOSK_ON_OWNER_EXIST:I = -0x3fe

.field public static final ERROR_CREATION_ALREADY_IN_PROGRESS:I = -0x3f8

.field public static final ERROR_CREATION_CANCELLED:I = -0x3f9

.field public static final ERROR_CREATION_FAILED_CONTAINER_MODE_EXIST:I = -0x3fc

.field public static final ERROR_CREATION_FAILED_TIMA_DISABLED:I = -0x3fa

.field public static final ERROR_DOES_NOT_EXIST:I = -0x4b2

.field public static final ERROR_FILESYSTEM_ERROR:I = -0x3f3

.field private static final ERROR_HANDLER_INSTALLATION_FAILED:I = -0x3ee

.field public static final ERROR_INTERNAL_ERROR:I = -0x3f6

.field public static final ERROR_INVALID_PASSWORD_RESET_TOKEN:I = -0x401

.field public static final ERROR_KLMS_LICENCE_CHECK_ERROR:I = -0x3f7

.field public static final ERROR_MAX_LIMIT_REACHED:I = -0x3f4

.field public static final ERROR_NOT_CONTAINER_OWNER:I = -0x4b3

.field public static final ERROR_NO_ADMIN_APK:I = -0x3ec

.field public static final ERROR_NO_CONFIGURATION_TYPE:I = -0x3ed

.field private static final ERROR_NO_HANDLER_APK:I = -0x3ea

.field private static final ERROR_NO_NAME:I = -0x3e9

.field private static final ERROR_NO_SETUPWIZARD_APK:I = -0x3eb

.field public static final ERROR_PLATFORM_VERSION_MISMATCH_IN_CONFIGURATION_TYPE:I = -0x3fb

.field public static final ERROR_POLICY_ENFORCEMENT_FAILED:I = -0x3f5

.field public static final ERROR_REMOVE_FAILED:I = -0x4b1

.field public static final ERROR_SDP_NOTSUPPORTED:I = -0x400

.field private static final ERROR_SETUPWIZARD_INSTALLATION_FAILED:I = -0x3ef

.field private static final ERROR_SYSTEM_APP_INSTALLATION_FAILED:I = -0x3f2

.field public static final EXTRA_CONTAINER_ID:Ljava/lang/String; = "containerid"

.field public static final FEATURE_TYPE_MY_KNOX:Ljava/lang/String; = "MY_KNOX"

.field private static final FLAG_ADMIN_TYPE_APK:I = 0x10

.field private static final FLAG_ADMIN_TYPE_NONE:I = 0x40

.field private static final FLAG_ADMIN_TYPE_PACKAGENAME:I = 0x20

.field private static final FLAG_BASE:I = 0x1

.field private static final FLAG_CREATOR_SELF_DESTROY:I = 0x8

.field private static final FLAG_ECRYPT_FILESYSTEM:I = 0x2

.field private static final FLAG_MIGRATION:I = 0x100

.field private static final FLAG_TIMA_STORAGE:I = 0x4

.field public static final INTENT_BUNDLE:Ljava/lang/String; = "intent"

.field public static final INTENT_CONTAINER_ADMIN_LOCK:Ljava/lang/String; = "com.samsung.knox.container.admin.locked"

.field public static final INTENT_CONTAINER_CREATION_STATUS:Ljava/lang/String; = "com.samsung.knox.container.creation.status"

.field public static final INTENT_CONTAINER_REMOVED:Ljava/lang/String; = "com.samsung.knox.container.removed"

.field public static final INTENT_CONTAINER_STATE_CHANGED:Ljava/lang/String; = "com.samsung.enterprise.container_state_changed"

.field public static final MAX_CONTAINERS:I = 0x2

.field public static final MINIMUM_INTERNAL_MEMORY_FOR_CREATE_CONTAINER_IN_MB:I = 0x264

.field public static final REMOVE_CONTAINER_SUCCESS:I = 0x0

.field private static final TAG:Ljava/lang/String; = "KnoxContainerManager"

.field public static final TIMA_VALIDATION_SUCCESS_CODE:I = 0x0

.field public static final TZ_COMMON_CLOSE_COMMUNICATION_ERROR:I = -0x10002

.field public static final TZ_COMMON_COMMUNICATION_ERROR:I = -0x10001

.field public static final TZ_COMMON_INIT_ERROR:I = -0x1000a

.field public static final TZ_COMMON_INIT_ERROR_TAMPER_FUSE_FAIL:I = -0x1000c

.field public static final TZ_COMMON_INIT_MSR_MISMATCH:I = -0x1000d

.field public static final TZ_COMMON_INIT_MSR_MODIFIED:I = -0x1000e

.field public static final TZ_COMMON_INIT_UNINITIALIZED_SECURE_MEM:I = -0x1000b

.field public static final TZ_COMMON_INTERNAL_ERROR:I = -0x10005

.field public static final TZ_COMMON_NULL_POINTER_EXCEPTION:I = -0x10006

.field public static final TZ_COMMON_RESPONSE_REQUEST_MISMATCH:I = -0x10003

.field public static final TZ_COMMON_UNDEFINED_ERROR:I = -0x10007

.field public static final TZ_KEYSTORE_ERROR:I = -0x1

.field public static final TZ_KEYSTORE_INIT_FAILED:I = -0x2

.field private static mContainerService:Lcom/sec/enterprise/knox/container/IKnoxContainerManager;


# instance fields
.field private mAdvancedRestrictionPolicy:Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;

.field private mAdvancedRestrictionPolicyCreated:Z

.field private mApplicationPolicy:Landroid/app/enterprise/ApplicationPolicy;

.field private mApplicationPolicyCreated:Z

.field private mBasePasswordPolicy:Landroid/app/enterprise/BasePasswordPolicy;

.field private mBasePasswordPolicyCreated:Z

.field private mBrowserPolicy:Landroid/app/enterprise/BrowserPolicy;

.field private mBrowserPolicyCreated:Z

.field private mCertificatePolicy:Lcom/sec/enterprise/knox/certificate/CertificatePolicy;

.field private mCertificatePolicyCreated:Z

.field private mClientCertificateManagerPolicy:Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;

.field private mClientCertificateManagerPolicyCreated:Z

.field private mContainerConfigurationPolicy:Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;

.field private mContainerConfigurationPolicyCreated:Z

.field private final mContext:Landroid/content/Context;

.field private final mContextInfo:Landroid/app/enterprise/ContextInfo;

.field private mDateTimePolicy:Landroid/app/enterprise/DateTimePolicy;

.field private mDateTimePolicyCreated:Z

.field private mDeviceAccountPolicy:Landroid/app/enterprise/DeviceAccountPolicy;

.field private mDeviceAccountPolicyCreated:Z

.field private mDeviceInventory:Landroid/app/enterprise/DeviceInventory;

.field private mDeviceInventoryCreated:Z

.field private mEasAccountPolicy:Landroid/app/enterprise/ExchangeAccountPolicy;

.field private mEasAccountPolicyCreated:Z

.field private mEmailAccountPolicy:Landroid/app/enterprise/EmailAccountPolicy;

.field private mEmailAccountPolicyCreated:Z

.field private mEmailPolicy:Landroid/app/enterprise/EmailPolicy;

.field private mEmailPolicyCreated:Z

.field private mEnterpriseBillingPolicy:Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;

.field private mEnterpriseBillingPolicyCreated:Z

.field private mEnterpriseSSOPolicy:Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;

.field private mEnterpriseSSOPolicyCreated:Z

.field private mEnterpriseVpnPolicy:Landroid/app/enterprise/EnterpriseVpnPolicy;

.field private mEnterpriseVpnPolicyCreated:Z

.field private mFirewallPolicy:Landroid/app/enterprise/FirewallPolicy;

.field private mFirewallPolicyCreated:Z

.field private mGenericSSO:Landroid/app/enterprise/sso/GenericSSO;

.field private mGenericSSOCreated:Z

.field private mGeofencing:Landroid/app/enterprise/geofencing/Geofencing;

.field private mGeofencingCreated:Z

.field private mKioskMode:Landroid/app/enterprise/kioskmode/KioskMode;

.field private mKioskModeCreated:Z

.field private mLDAPAccountPolicy:Landroid/app/enterprise/LDAPAccountPolicy;

.field private mLDAPAccountPolicyCreated:Z

.field private mLocationPolicy:Landroid/app/enterprise/LocationPolicy;

.field private mLocationPolicyCreated:Z

.field private mMiscPolicy:Landroid/app/enterprise/MiscPolicy;

.field private mMiscPolicyCreated:Z

.field private mPasswordPolicy:Landroid/app/enterprise/PasswordPolicy;

.field private mPasswordPolicyCreated:Z

.field private mRCPPolicy:Lcom/sec/enterprise/knox/container/RCPPolicy;

.field private mRestrictionPolicy:Landroid/app/enterprise/RestrictionPolicy;

.field private mRestrictionPolicyCreated:Z

.field private mSecurityPolicy:Landroid/app/enterprise/SecurityPolicy;

.field private mSecurityPolicyCreated:Z

.field private mSmartCardAccessPolicy:Lcom/sec/enterprise/knox/container/SmartCardAccessPolicy;

.field private mSmartCardBrowserPolicy:Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;

.field private mSmartCardBrowserPolicyCreated:Z

.field private mSmartCardEmailPolicy:Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;

.field private mSmartCardEmailPolicyCreated:Z

.field private mWifiPolicy:Landroid/app/enterprise/WifiPolicy;

.field private mWifiPolicyCreated:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 679
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContainerService:Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/app/enterprise/ContextInfo;)V
    .locals 5
    .param p1, "cxt"    # Landroid/content/Context;
    .param p2, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NoSuchFieldException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 724
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 674
    iput-boolean v2, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mEnterpriseBillingPolicyCreated:Z

    .line 681
    iput-object v3, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mRCPPolicy:Lcom/sec/enterprise/knox/container/RCPPolicy;

    .line 686
    iput-object v3, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mSmartCardAccessPolicy:Lcom/sec/enterprise/knox/container/SmartCardAccessPolicy;

    .line 2317
    iput-boolean v2, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mApplicationPolicyCreated:Z

    .line 2318
    iput-boolean v2, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mBrowserPolicyCreated:Z

    .line 2319
    iput-boolean v2, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContainerConfigurationPolicyCreated:Z

    .line 2320
    iput-boolean v2, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mDateTimePolicyCreated:Z

    .line 2321
    iput-boolean v2, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mDeviceAccountPolicyCreated:Z

    .line 2322
    iput-boolean v2, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mDeviceInventoryCreated:Z

    .line 2323
    iput-boolean v2, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mEasAccountPolicyCreated:Z

    .line 2324
    iput-boolean v2, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mEmailAccountPolicyCreated:Z

    .line 2325
    iput-boolean v2, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mEmailPolicyCreated:Z

    .line 2326
    iput-boolean v2, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mFirewallPolicyCreated:Z

    .line 2327
    iput-boolean v2, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mGeofencingCreated:Z

    .line 2328
    iput-boolean v2, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mKioskModeCreated:Z

    .line 2329
    iput-boolean v2, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mLDAPAccountPolicyCreated:Z

    .line 2330
    iput-boolean v2, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mLocationPolicyCreated:Z

    .line 2331
    iput-boolean v2, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mMiscPolicyCreated:Z

    .line 2332
    iput-boolean v2, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mBasePasswordPolicyCreated:Z

    .line 2333
    iput-boolean v2, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mPasswordPolicyCreated:Z

    .line 2334
    iput-boolean v2, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mRestrictionPolicyCreated:Z

    .line 2335
    iput-boolean v2, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mSecurityPolicyCreated:Z

    .line 2336
    iput-boolean v2, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mSmartCardBrowserPolicyCreated:Z

    .line 2337
    iput-boolean v2, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mSmartCardEmailPolicyCreated:Z

    .line 2338
    iput-boolean v2, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mWifiPolicyCreated:Z

    .line 2339
    iput-boolean v2, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mEnterpriseSSOPolicyCreated:Z

    .line 2340
    iput-boolean v2, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mAdvancedRestrictionPolicyCreated:Z

    .line 2341
    iput-boolean v2, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mEnterpriseVpnPolicyCreated:Z

    .line 2343
    iput-boolean v2, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mClientCertificateManagerPolicyCreated:Z

    .line 2345
    iput-boolean v2, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mCertificatePolicyCreated:Z

    .line 2346
    iput-boolean v2, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mGenericSSOCreated:Z

    .line 725
    const-string v2, "persona"

    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Landroid/os/IPersonaManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/IPersonaManager;

    move-result-object v1

    .line 728
    .local v1, "persona":Landroid/os/IPersonaManager;
    if-eqz v1, :cond_0

    :try_start_0
    iget v2, p2, Landroid/app/enterprise/ContextInfo;->mContainerId:I

    invoke-interface {v1, v2}, Landroid/os/IPersonaManager;->exists(I)Z

    move-result v2

    if-nez v2, :cond_1

    .line 729
    :cond_0
    new-instance v2, Ljava/lang/NoSuchFieldException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Container with Id "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p2, Landroid/app/enterprise/ContextInfo;->mContainerId:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " does not exists"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/NoSuchFieldException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 731
    :catch_0
    move-exception v0

    .line 732
    .local v0, "e":Landroid/os/RemoteException;
    const-string v2, "KnoxContainerManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Remote exception: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 733
    new-instance v2, Ljava/lang/NoSuchFieldException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Container with Id "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p2, Landroid/app/enterprise/ContextInfo;->mContainerId:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " does not exists"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/NoSuchFieldException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 736
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_1
    iput-object p2, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    .line 737
    iput-object p1, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContext:Landroid/content/Context;

    .line 738
    return-void
.end method

.method public static addConfigurationType(Landroid/content/Context;Landroid/app/enterprise/ContextInfo;Lcom/sec/enterprise/knox/container/KnoxConfigurationType;)Z
    .locals 6
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p2, "type"    # Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    .prologue
    .line 1539
    const/4 v1, 0x0

    .line 1540
    .local v1, "retVal":Z
    invoke-static {}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->getContainerService()Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    move-result-object v3

    .line 1541
    .local v3, "service":Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    if-nez v3, :cond_0

    .line 1542
    const-string v4, "KnoxContainerManager"

    const-string v5, "KnoxMUMContainerPolicy Service is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v1

    .line 1564
    .end local v1    # "retVal":Z
    .local v2, "retVal":I
    :goto_0
    return v2

    .line 1546
    .end local v2    # "retVal":I
    .restart local v1    # "retVal":Z
    :cond_0
    if-nez p2, :cond_1

    .line 1547
    const-string v4, "KnoxContainerManager"

    const-string v5, "type object is NULL!!, returning.."

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v1

    .line 1548
    .restart local v2    # "retVal":I
    goto :goto_0

    .line 1551
    .end local v2    # "retVal":I
    :cond_1
    if-nez p0, :cond_2

    .line 1552
    const-string v4, "KnoxContainerManager"

    const-string v5, "Context is NULL!!, returning.."

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v1

    .line 1553
    .restart local v2    # "retVal":I
    goto :goto_0

    .line 1557
    .end local v2    # "retVal":I
    :cond_2
    :try_start_0
    invoke-virtual {p2}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->dumpState()V

    .line 1558
    invoke-static {p0, p2}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->processNewTypeObject(Landroid/content/Context;Lcom/sec/enterprise/knox/container/KnoxConfigurationType;)V

    .line 1559
    const/4 v4, 0x1

    new-array v4, v4, [Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    const/4 v5, 0x0

    aput-object p2, v4, v5

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v3, p1, v4}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager;->addConfigurationType(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_1
    move v2, v1

    .line 1564
    .restart local v2    # "retVal":I
    goto :goto_0

    .line 1560
    .end local v2    # "retVal":I
    :catch_0
    move-exception v0

    .line 1561
    .local v0, "e":Landroid/os/RemoteException;
    const-string v4, "KnoxContainerManager"

    const-string v5, "Failed at KnoxContainerManager API addContainer:"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1562
    const-string v4, "KnoxContainerManager"

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public static addConfigurationType(Landroid/content/Context;Lcom/sec/enterprise/knox/container/KnoxConfigurationType;)Z
    .locals 2
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "type"    # Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    .prologue
    .line 1529
    new-instance v0, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    invoke-direct {v0, v1}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const-string v1, "KnoxContainerManager.addConfigurationType"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1530
    const/4 v0, 0x0

    invoke-static {p0, v0, p1}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->addConfigurationType(Landroid/content/Context;Landroid/app/enterprise/ContextInfo;Lcom/sec/enterprise/knox/container/KnoxConfigurationType;)Z

    move-result v0

    return v0
.end method

.method public static cancelCreateContainer(Lcom/sec/knox/container/ContainerCreationParams;)Z
    .locals 7
    .param p0, "params"    # Lcom/sec/knox/container/ContainerCreationParams;

    .prologue
    .line 1020
    const/4 v1, 0x0

    .line 1021
    .local v1, "retVal":Z
    invoke-static {}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->getContainerService()Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    move-result-object v3

    .line 1022
    .local v3, "service":Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    if-nez v3, :cond_0

    .line 1023
    const-string v4, "KnoxContainerManager"

    const-string v5, "KnoxMUMContainerPolicy Service is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v1

    .line 1032
    .end local v1    # "retVal":Z
    .local v2, "retVal":I
    :goto_0
    return v2

    .line 1027
    .end local v2    # "retVal":I
    .restart local v1    # "retVal":Z
    :cond_0
    :try_start_0
    invoke-interface {v3, p0}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager;->cancelCreateContainer(Lcom/sec/knox/container/ContainerCreationParams;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_1
    move v2, v1

    .line 1032
    .restart local v2    # "retVal":I
    goto :goto_0

    .line 1029
    .end local v2    # "retVal":I
    :catch_0
    move-exception v0

    .line 1030
    .local v0, "e":Landroid/os/RemoteException;
    const-string v4, "KnoxContainerManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed at KnoxContainerManager API cancelCreateContainer "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public static changeContainerOwner(ILjava/lang/String;)Z
    .locals 4
    .param p0, "containerId"    # I
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 3108
    new-instance v2, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v3

    invoke-direct {v2, v3}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const-string v3, "KnoxContainerManager.changeContainerOwnership"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 3109
    new-instance v0, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v2

    invoke-direct {v0, v2, p0}, Landroid/app/enterprise/ContextInfo;-><init>(II)V

    .line 3110
    .local v0, "contextInfo":Landroid/app/enterprise/ContextInfo;
    invoke-static {v0, p1}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->changeContainerOwner(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v1

    .line 3111
    .local v1, "retVal":Z
    return v1
.end method

.method public static changeContainerOwner(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    .locals 7
    .param p0, "contextInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 3119
    const/4 v1, 0x0

    .line 3120
    .local v1, "retVal":Z
    invoke-static {}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->getContainerService()Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    move-result-object v3

    .line 3121
    .local v3, "service":Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    if-nez v3, :cond_0

    .line 3122
    const-string v4, "KnoxContainerManager"

    const-string v5, "KnoxContainerManager Service is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v1

    .line 3131
    .end local v1    # "retVal":Z
    .local v2, "retVal":I
    :goto_0
    return v2

    .line 3127
    .end local v2    # "retVal":I
    .restart local v1    # "retVal":Z
    :cond_0
    :try_start_0
    invoke-interface {v3, p0, p1}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager;->changeContainerOwner(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_1
    move v2, v1

    .line 3131
    .restart local v2    # "retVal":I
    goto :goto_0

    .line 3128
    .end local v2    # "retVal":I
    :catch_0
    move-exception v0

    .line 3129
    .local v0, "e":Landroid/os/RemoteException;
    const-string v4, "KnoxContainerManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed at KnoxContainerManager API changeContainerOwnership "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public static createContainer(Landroid/app/enterprise/ContextInfo;Lcom/sec/enterprise/knox/container/CreationParams;)I
    .locals 8
    .param p0, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p1, "params"    # Lcom/sec/enterprise/knox/container/CreationParams;

    .prologue
    .line 947
    const/16 v2, -0x3f6

    .line 948
    .local v2, "retVal":I
    invoke-static {}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->getContainerService()Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    move-result-object v3

    .line 949
    .local v3, "service":Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    if-nez v3, :cond_0

    .line 950
    const-string v5, "KnoxContainerManager"

    const-string v6, "KnoxMUMContainerPolicy Service is not yet ready!!!"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v5, v2

    .line 972
    :goto_0
    return v5

    .line 953
    :cond_0
    if-nez p1, :cond_1

    move v5, v2

    .line 954
    goto :goto_0

    .line 956
    :cond_1
    invoke-virtual {p1}, Lcom/sec/enterprise/knox/container/CreationParams;->getPasswordResetToken()Ljava/lang/String;

    move-result-object v4

    .line 957
    .local v4, "token":Ljava/lang/String;
    if-eqz v4, :cond_2

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 958
    :cond_2
    const/16 v5, -0x401

    goto :goto_0

    .line 960
    :cond_3
    const/4 v1, 0x6

    .line 961
    .local v1, "flags":I
    invoke-virtual {p1}, Lcom/sec/enterprise/knox/container/CreationParams;->getAdminPackageName()Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_4

    .line 962
    or-int/lit8 v1, v1, 0x40

    .line 967
    :goto_1
    :try_start_0
    invoke-interface {v3, p0, p1, v1}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager;->createContainer(Landroid/app/enterprise/ContextInfo;Lcom/sec/enterprise/knox/container/CreationParams;I)I

    move-result v2

    .line 968
    invoke-static {v2}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->processCreateReturn(I)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_2
    move v5, v2

    .line 972
    goto :goto_0

    .line 964
    :cond_4
    or-int/lit8 v1, v1, 0x28

    goto :goto_1

    .line 969
    :catch_0
    move-exception v0

    .line 970
    .local v0, "e":Landroid/os/RemoteException;
    const-string v5, "KnoxContainerManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Failed at KnoxContainerManager API createContainer "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method public static createContainer(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)I
    .locals 9
    .param p0, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 1264
    const/16 v2, -0x3f6

    .line 1265
    .local v2, "retVal":I
    invoke-static {}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->getContainerService()Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    move-result-object v4

    .line 1266
    .local v4, "service":Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    const/4 v5, 0x0

    .line 1267
    .local v5, "typeObj":Lcom/sec/enterprise/knox/container/KnoxConfigurationType;
    if-nez v4, :cond_0

    .line 1268
    const-string v6, "KnoxContainerManager"

    const-string v7, "KnoxMUMContainerPolicy Service is not yet ready!!!"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 1283
    .end local v2    # "retVal":I
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 1271
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":I
    :cond_0
    new-instance v1, Lcom/sec/enterprise/knox/container/CreationParams;

    invoke-direct {v1}, Lcom/sec/enterprise/knox/container/CreationParams;-><init>()V

    .line 1272
    .local v1, "params":Lcom/sec/enterprise/knox/container/CreationParams;
    invoke-virtual {v1, p1}, Lcom/sec/enterprise/knox/container/CreationParams;->setConfigurationName(Ljava/lang/String;)V

    .line 1275
    const/16 v6, 0x46

    :try_start_0
    invoke-interface {v4, p0, v1, v6}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager;->createContainer(Landroid/app/enterprise/ContextInfo;Lcom/sec/enterprise/knox/container/CreationParams;I)I

    move-result v2

    .line 1279
    invoke-static {v2}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->processCreateReturn(I)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 1283
    .end local v2    # "retVal":I
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 1280
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":I
    :catch_0
    move-exception v0

    .line 1281
    .local v0, "e":Landroid/os/RemoteException;
    const-string v6, "KnoxContainerManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Failed at KnoxContainerManager API createContainer "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public static createContainer(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Landroid/net/Uri;)I
    .locals 9
    .param p0, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "apkFile"    # Landroid/net/Uri;

    .prologue
    .line 1114
    const/16 v2, -0x3f6

    .line 1115
    .local v2, "retVal":I
    invoke-static {}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->getContainerService()Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    move-result-object v4

    .line 1116
    .local v4, "service":Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    const/4 v5, 0x0

    .line 1117
    .local v5, "typeObj":Lcom/sec/enterprise/knox/container/KnoxConfigurationType;
    if-nez v4, :cond_0

    .line 1118
    const-string v6, "KnoxContainerManager"

    const-string v7, "KnoxMUMContainerPolicy Service is not yet ready!!!"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 1134
    .end local v2    # "retVal":I
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 1121
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":I
    :cond_0
    new-instance v1, Lcom/sec/enterprise/knox/container/CreationParams;

    invoke-direct {v1}, Lcom/sec/enterprise/knox/container/CreationParams;-><init>()V

    .line 1122
    .local v1, "params":Lcom/sec/enterprise/knox/container/CreationParams;
    invoke-virtual {v1, p1}, Lcom/sec/enterprise/knox/container/CreationParams;->setConfigurationName(Ljava/lang/String;)V

    .line 1123
    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/sec/enterprise/knox/container/CreationParams;->setAdminPackageName(Ljava/lang/String;)V

    .line 1125
    const/16 v6, 0x1e

    :try_start_0
    invoke-interface {v4, p0, v1, v6}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager;->createContainer(Landroid/app/enterprise/ContextInfo;Lcom/sec/enterprise/knox/container/CreationParams;I)I

    move-result v2

    .line 1130
    invoke-static {v2}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->processCreateReturn(I)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 1134
    .end local v2    # "retVal":I
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 1131
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":I
    :catch_0
    move-exception v0

    .line 1132
    .local v0, "e":Landroid/os/RemoteException;
    const-string v6, "KnoxContainerManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Failed at KnoxContainerManager API createContainer "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public static createContainer(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Lcom/sec/enterprise/knox/EnterpriseContainerCallback;)I
    .locals 9
    .param p0, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "callback"    # Lcom/sec/enterprise/knox/EnterpriseContainerCallback;

    .prologue
    .line 1292
    const/16 v2, -0x3f6

    .line 1293
    .local v2, "retVal":I
    invoke-static {}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->getContainerService()Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    move-result-object v4

    .line 1294
    .local v4, "service":Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    const/4 v5, 0x0

    .line 1295
    .local v5, "typeObj":Lcom/sec/enterprise/knox/container/KnoxConfigurationType;
    if-nez v4, :cond_0

    .line 1296
    const-string v6, "KnoxContainerManager"

    const-string v7, "KnoxMUMContainerPolicy Service is not yet ready!!!"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 1310
    .end local v2    # "retVal":I
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 1299
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":I
    :cond_0
    new-instance v1, Lcom/sec/enterprise/knox/container/CreationParams;

    invoke-direct {v1}, Lcom/sec/enterprise/knox/container/CreationParams;-><init>()V

    .line 1300
    .local v1, "params":Lcom/sec/enterprise/knox/container/CreationParams;
    invoke-virtual {v1, p1}, Lcom/sec/enterprise/knox/container/CreationParams;->setConfigurationName(Ljava/lang/String;)V

    .line 1302
    const/16 v6, 0x46

    :try_start_0
    invoke-interface {v4, p0, v1, v6, p2}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager;->createContainerWithCallback(Landroid/app/enterprise/ContextInfo;Lcom/sec/enterprise/knox/container/CreationParams;ILcom/sec/enterprise/knox/IEnterpriseContainerCallback;)I

    move-result v2

    .line 1306
    invoke-static {v2}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->processCreateReturn(I)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 1310
    .end local v2    # "retVal":I
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 1307
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":I
    :catch_0
    move-exception v0

    .line 1308
    .local v0, "e":Landroid/os/RemoteException;
    const-string v6, "KnoxContainerManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Failed at KnoxContainerManager API createContainer "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public static createContainer(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)I
    .locals 8
    .param p0, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "adminPackageName"    # Ljava/lang/String;

    .prologue
    .line 849
    const/16 v2, -0x3f6

    .line 850
    .local v2, "retVal":I
    invoke-static {}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->getContainerService()Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    move-result-object v4

    .line 851
    .local v4, "service":Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    if-nez v4, :cond_0

    .line 852
    const-string v5, "KnoxContainerManager"

    const-string v6, "KnoxMUMContainerPolicy Service is not yet ready!!!"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 868
    .end local v2    # "retVal":I
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 855
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":I
    :cond_0
    new-instance v1, Lcom/sec/enterprise/knox/container/CreationParams;

    invoke-direct {v1}, Lcom/sec/enterprise/knox/container/CreationParams;-><init>()V

    .line 856
    .local v1, "params":Lcom/sec/enterprise/knox/container/CreationParams;
    invoke-virtual {v1, p1}, Lcom/sec/enterprise/knox/container/CreationParams;->setConfigurationName(Ljava/lang/String;)V

    .line 857
    invoke-virtual {v1, p2}, Lcom/sec/enterprise/knox/container/CreationParams;->setAdminPackageName(Ljava/lang/String;)V

    .line 859
    const/16 v5, 0x2e

    :try_start_0
    invoke-interface {v4, p0, v1, v5}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager;->createContainer(Landroid/app/enterprise/ContextInfo;Lcom/sec/enterprise/knox/container/CreationParams;I)I

    move-result v2

    .line 864
    invoke-static {v2}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->processCreateReturn(I)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 868
    .end local v2    # "retVal":I
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 865
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":I
    :catch_0
    move-exception v0

    .line 866
    .local v0, "e":Landroid/os/RemoteException;
    const-string v5, "KnoxContainerManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Failed at KnoxContainerManager API createContainer "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public static createContainer(Lcom/sec/enterprise/knox/container/CreationParams;)I
    .locals 2
    .param p0, "params"    # Lcom/sec/enterprise/knox/container/CreationParams;

    .prologue
    .line 938
    new-instance v0, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    invoke-direct {v0, v1}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const-string v1, "KnoxContainerManager.createContainer"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 939
    const/4 v0, 0x0

    invoke-static {v0, p0}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->createContainer(Landroid/app/enterprise/ContextInfo;Lcom/sec/enterprise/knox/container/CreationParams;)I

    move-result v0

    return v0
.end method

.method public static createContainer(Ljava/lang/String;)I
    .locals 3
    .param p0, "type"    # Ljava/lang/String;

    .prologue
    .line 1254
    new-instance v1, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v2

    invoke-direct {v1, v2}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const-string v2, "KnoxContainerManager.createContainer"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1255
    const/4 v0, 0x0

    .line 1256
    .local v0, "obj":Landroid/app/enterprise/ContextInfo;
    invoke-static {v0, p0}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->createContainer(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)I

    move-result v1

    return v1
.end method

.method public static createContainer(Ljava/lang/String;Landroid/net/Uri;)I
    .locals 2
    .param p0, "type"    # Ljava/lang/String;
    .param p1, "apkFile"    # Landroid/net/Uri;

    .prologue
    .line 1105
    new-instance v0, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    invoke-direct {v0, v1}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const-string v1, "KnoxContainerManager.createContainer"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1106
    const/4 v0, 0x0

    invoke-static {v0, p0, p1}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->createContainer(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Landroid/net/Uri;)I

    move-result v0

    return v0
.end method

.method public static createContainer(Ljava/lang/String;Ljava/lang/String;)I
    .locals 3
    .param p0, "type"    # Ljava/lang/String;
    .param p1, "adminPackageName"    # Ljava/lang/String;

    .prologue
    .line 839
    new-instance v1, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v2

    invoke-direct {v1, v2}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const-string v2, "KnoxContainerManager.createContainer"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 840
    const/4 v0, 0x0

    .line 841
    .local v0, "cxtInfo":Landroid/app/enterprise/ContextInfo;
    invoke-static {v0, p0, p1}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->createContainer(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    return v1
.end method

.method public static createContainerForMigration(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)I
    .locals 8
    .param p0, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 1143
    const/16 v2, -0x3f6

    .line 1144
    .local v2, "retVal":I
    invoke-static {}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->getContainerService()Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    move-result-object v4

    .line 1145
    .local v4, "service":Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    if-nez v4, :cond_0

    .line 1146
    const-string v5, "KnoxContainerManager"

    const-string v6, "KnoxMUMContainerPolicy Service is not yet ready!!!"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 1161
    .end local v2    # "retVal":I
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 1149
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":I
    :cond_0
    new-instance v1, Lcom/sec/enterprise/knox/container/CreationParams;

    invoke-direct {v1}, Lcom/sec/enterprise/knox/container/CreationParams;-><init>()V

    .line 1150
    .local v1, "params":Lcom/sec/enterprise/knox/container/CreationParams;
    invoke-virtual {v1, p1}, Lcom/sec/enterprise/knox/container/CreationParams;->setConfigurationName(Ljava/lang/String;)V

    .line 1152
    const/16 v5, 0x146

    :try_start_0
    invoke-interface {v4, p0, v1, v5}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager;->createContainer(Landroid/app/enterprise/ContextInfo;Lcom/sec/enterprise/knox/container/CreationParams;I)I

    move-result v2

    .line 1157
    invoke-static {v2}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->processCreateReturn(I)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 1161
    .end local v2    # "retVal":I
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 1158
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":I
    :catch_0
    move-exception v0

    .line 1159
    .local v0, "e":Landroid/os/RemoteException;
    const-string v5, "KnoxContainerManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Failed at KnoxContainerManager API createContainerForMigration "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public static createContainerForMigration(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Landroid/net/Uri;)I
    .locals 8
    .param p0, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "apkFile"    # Landroid/net/Uri;

    .prologue
    .line 1169
    const/16 v2, -0x3f6

    .line 1170
    .local v2, "retVal":I
    invoke-static {}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->getContainerService()Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    move-result-object v4

    .line 1171
    .local v4, "service":Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    if-nez v4, :cond_0

    .line 1172
    const-string v5, "KnoxContainerManager"

    const-string v6, "KnoxMUMContainerPolicy Service is not yet ready!!!"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 1189
    .end local v2    # "retVal":I
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 1175
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":I
    :cond_0
    new-instance v1, Lcom/sec/enterprise/knox/container/CreationParams;

    invoke-direct {v1}, Lcom/sec/enterprise/knox/container/CreationParams;-><init>()V

    .line 1176
    .local v1, "params":Lcom/sec/enterprise/knox/container/CreationParams;
    invoke-virtual {v1, p1}, Lcom/sec/enterprise/knox/container/CreationParams;->setConfigurationName(Ljava/lang/String;)V

    .line 1177
    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/sec/enterprise/knox/container/CreationParams;->setAdminPackageName(Ljava/lang/String;)V

    .line 1179
    const/16 v5, 0x11e

    :try_start_0
    invoke-interface {v4, p0, v1, v5}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager;->createContainer(Landroid/app/enterprise/ContextInfo;Lcom/sec/enterprise/knox/container/CreationParams;I)I

    move-result v2

    .line 1185
    invoke-static {v2}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->processCreateReturn(I)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 1189
    .end local v2    # "retVal":I
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 1186
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":I
    :catch_0
    move-exception v0

    .line 1187
    .local v0, "e":Landroid/os/RemoteException;
    const-string v5, "KnoxContainerManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Failed at KnoxContainerManager API createContainerForMigration "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public static createContainerInternal(Lcom/sec/knox/container/ContainerCreationParams;)I
    .locals 7
    .param p0, "params"    # Lcom/sec/knox/container/ContainerCreationParams;

    .prologue
    .line 980
    const/16 v1, -0x3f6

    .line 981
    .local v1, "retVal":I
    invoke-static {}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->getContainerService()Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    move-result-object v3

    .line 982
    .local v3, "service":Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    if-nez v3, :cond_0

    .line 983
    const-string v4, "KnoxContainerManager"

    const-string v5, "KnoxMUMContainerPolicy Service is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v1

    .line 992
    .end local v1    # "retVal":I
    .local v2, "retVal":I
    :goto_0
    return v2

    .line 987
    .end local v2    # "retVal":I
    .restart local v1    # "retVal":I
    :cond_0
    :try_start_0
    invoke-interface {v3, p0}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager;->createContainerInternal(Lcom/sec/knox/container/ContainerCreationParams;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_1
    move v2, v1

    .line 992
    .end local v1    # "retVal":I
    .restart local v2    # "retVal":I
    goto :goto_0

    .line 989
    .end local v2    # "retVal":I
    .restart local v1    # "retVal":I
    :catch_0
    move-exception v0

    .line 990
    .local v0, "e":Landroid/os/RemoteException;
    const-string v4, "KnoxContainerManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed at KnoxContainerManager API createContainerInternal "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public static createContainerMarkSuccess(Lcom/sec/knox/container/ContainerCreationParams;)Z
    .locals 7
    .param p0, "params"    # Lcom/sec/knox/container/ContainerCreationParams;

    .prologue
    .line 1000
    const/4 v1, 0x0

    .line 1001
    .local v1, "retVal":Z
    invoke-static {}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->getContainerService()Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    move-result-object v3

    .line 1002
    .local v3, "service":Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    if-nez v3, :cond_0

    .line 1003
    const-string v4, "KnoxContainerManager"

    const-string v5, "KnoxMUMContainerPolicy Service is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v1

    .line 1012
    .end local v1    # "retVal":Z
    .local v2, "retVal":I
    :goto_0
    return v2

    .line 1007
    .end local v2    # "retVal":I
    .restart local v1    # "retVal":Z
    :cond_0
    :try_start_0
    invoke-interface {v3, p0}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager;->createContainerMarkSuccess(Lcom/sec/knox/container/ContainerCreationParams;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_1
    move v2, v1

    .line 1012
    .restart local v2    # "retVal":I
    goto :goto_0

    .line 1009
    .end local v2    # "retVal":I
    :catch_0
    move-exception v0

    .line 1010
    .local v0, "e":Landroid/os/RemoteException;
    const-string v4, "KnoxContainerManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed at KnoxContainerManager API createContainerMarkSuccess "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public static doSelfUninstall()V
    .locals 5

    .prologue
    .line 1430
    invoke-static {}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->getContainerService()Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    move-result-object v1

    .line 1431
    .local v1, "service":Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    if-nez v1, :cond_0

    .line 1432
    const-string v2, "KnoxContainerManager"

    const-string v3, "KnoxMUMContainerPolicy Service is not yet ready!!!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1441
    :goto_0
    return-void

    .line 1437
    :cond_0
    :try_start_0
    invoke-interface {v1}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager;->doSelfUninstall()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1438
    :catch_0
    move-exception v0

    .line 1439
    .local v0, "e":Landroid/os/RemoteException;
    const-string v2, "KnoxContainerManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed at KnoxContainerManager API getContainers :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static getConfigurationType(I)Lcom/sec/enterprise/knox/container/KnoxConfigurationType;
    .locals 1
    .param p0, "containerId"    # I

    .prologue
    .line 1765
    const/4 v0, 0x0

    invoke-static {v0, p0}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->getConfigurationType(Landroid/app/enterprise/ContextInfo;I)Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    move-result-object v0

    return-object v0
.end method

.method public static getConfigurationType(Landroid/app/enterprise/ContextInfo;I)Lcom/sec/enterprise/knox/container/KnoxConfigurationType;
    .locals 7
    .param p0, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p1, "containerId"    # I

    .prologue
    const/4 v3, 0x0

    .line 1773
    const/4 v1, 0x0

    .line 1774
    .local v1, "retVal":Ljava/util/List;, "Ljava/util/List<Lcom/sec/enterprise/knox/container/KnoxConfigurationType;>;"
    invoke-static {}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->getContainerService()Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    move-result-object v2

    .line 1775
    .local v2, "service":Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    if-nez v2, :cond_1

    .line 1776
    const-string v4, "KnoxContainerManager"

    const-string v5, "KnoxMUMContainerPolicy Service is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1789
    :cond_0
    :goto_0
    return-object v3

    .line 1781
    :cond_1
    :try_start_0
    invoke-interface {v2, p0, p1}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager;->getConfigurationType(Landroid/app/enterprise/ContextInfo;I)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1786
    :goto_1
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 1787
    const/4 v3, 0x0

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    goto :goto_0

    .line 1782
    :catch_0
    move-exception v0

    .line 1783
    .local v0, "e":Landroid/os/RemoteException;
    const-string v4, "KnoxContainerManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed at KnoxContainerManager API getConfigurationType by id:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public static getConfigurationTypeByName(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Lcom/sec/enterprise/knox/container/KnoxConfigurationType;
    .locals 7
    .param p0, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 1691
    const/4 v1, 0x0

    .line 1692
    .local v1, "retVal":Ljava/util/List;, "Ljava/util/List<Lcom/sec/enterprise/knox/container/KnoxConfigurationType;>;"
    invoke-static {}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->getContainerService()Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    move-result-object v2

    .line 1694
    .local v2, "service":Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    if-nez v2, :cond_1

    .line 1695
    const-string v4, "KnoxContainerManager"

    const-string v5, "KnoxMUMContainerPolicy Service is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1707
    :cond_0
    :goto_0
    return-object v3

    .line 1700
    :cond_1
    :try_start_0
    invoke-interface {v2, p0, p1}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager;->getConfigurationTypeByName(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1704
    :goto_1
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 1705
    const/4 v3, 0x0

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    goto :goto_0

    .line 1701
    :catch_0
    move-exception v0

    .line 1702
    .local v0, "e":Landroid/os/RemoteException;
    const-string v4, "KnoxContainerManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed at KnoxContainerManager API getContainer("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ") :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public static getConfigurationTypeByName(Ljava/lang/String;)Lcom/sec/enterprise/knox/container/KnoxConfigurationType;
    .locals 1
    .param p0, "type"    # Ljava/lang/String;

    .prologue
    .line 1683
    const/4 v0, 0x0

    invoke-static {v0, p0}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->getConfigurationTypeByName(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    move-result-object v0

    return-object v0
.end method

.method public static getConfigurationTypes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/enterprise/knox/container/KnoxConfigurationType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1726
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->getConfigurationTypes(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static getConfigurationTypes(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    .locals 7
    .param p0, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/enterprise/ContextInfo;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/sec/enterprise/knox/container/KnoxConfigurationType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1734
    const/4 v1, 0x0

    .line 1735
    .local v1, "retVal":Ljava/util/List;, "Ljava/util/List<Lcom/sec/enterprise/knox/container/KnoxConfigurationType;>;"
    invoke-static {}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->getContainerService()Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    move-result-object v3

    .line 1736
    .local v3, "service":Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    if-nez v3, :cond_0

    .line 1737
    const-string v4, "KnoxContainerManager"

    const-string v5, "KnoxMUMContainerPolicy Service is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v2, v1

    .line 1746
    .end local v1    # "retVal":Ljava/util/List;, "Ljava/util/List<Lcom/sec/enterprise/knox/container/KnoxConfigurationType;>;"
    .local v2, "retVal":Ljava/util/List;, "Ljava/util/List<Lcom/sec/enterprise/knox/container/KnoxConfigurationType;>;"
    :goto_0
    return-object v2

    .line 1742
    .end local v2    # "retVal":Ljava/util/List;, "Ljava/util/List<Lcom/sec/enterprise/knox/container/KnoxConfigurationType;>;"
    .restart local v1    # "retVal":Ljava/util/List;, "Ljava/util/List<Lcom/sec/enterprise/knox/container/KnoxConfigurationType;>;"
    :cond_0
    :try_start_0
    invoke-interface {v3, p0}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager;->getConfigurationTypes(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_1
    move-object v2, v1

    .line 1746
    .end local v1    # "retVal":Ljava/util/List;, "Ljava/util/List<Lcom/sec/enterprise/knox/container/KnoxConfigurationType;>;"
    .restart local v2    # "retVal":Ljava/util/List;, "Ljava/util/List<Lcom/sec/enterprise/knox/container/KnoxConfigurationType;>;"
    goto :goto_0

    .line 1743
    .end local v2    # "retVal":Ljava/util/List;, "Ljava/util/List<Lcom/sec/enterprise/knox/container/KnoxConfigurationType;>;"
    .restart local v1    # "retVal":Ljava/util/List;, "Ljava/util/List<Lcom/sec/enterprise/knox/container/KnoxConfigurationType;>;"
    :catch_0
    move-exception v0

    .line 1744
    .local v0, "e":Landroid/os/RemoteException;
    const-string v4, "KnoxContainerManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed at KnoxContainerManager API getConfigurationType:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public static getContainerCreationParams(I)Lcom/sec/knox/container/ContainerCreationParams;
    .locals 6
    .param p0, "containerId"    # I

    .prologue
    const/4 v2, 0x0

    .line 1817
    invoke-static {}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->getContainerService()Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    move-result-object v1

    .line 1818
    .local v1, "service":Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    if-nez v1, :cond_0

    .line 1819
    const-string v3, "KnoxContainerManager"

    const-string v4, "KnoxMUMContainerPolicy Service is not yet ready!!!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1828
    :goto_0
    return-object v2

    .line 1824
    :cond_0
    :try_start_0
    invoke-interface {v1, p0}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager;->getContainerCreationParams(I)Lcom/sec/knox/container/ContainerCreationParams;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    .line 1825
    :catch_0
    move-exception v0

    .line 1826
    .local v0, "e":Landroid/os/RemoteException;
    const-string v3, "KnoxContainerManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed at KnoxContainerManager API getConfigurationType by id:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private static declared-synchronized getContainerService()Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    .locals 2

    .prologue
    .line 741
    const-class v1, Lcom/sec/enterprise/knox/container/KnoxContainerManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContainerService:Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    if-nez v0, :cond_0

    .line 742
    const-string v0, "mum_container_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContainerService:Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    .line 745
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContainerService:Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 741
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static getContainers()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1401
    new-instance v0, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    invoke-direct {v0, v1}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const-string v1, "KnoxContainerManager.getContainers"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1402
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->getContainers(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static getContainers(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    .locals 7
    .param p0, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/enterprise/ContextInfo;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1410
    const/4 v1, 0x0

    .line 1411
    .local v1, "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-static {}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->getContainerService()Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    move-result-object v3

    .line 1412
    .local v3, "service":Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    if-nez v3, :cond_0

    .line 1413
    const-string v4, "KnoxContainerManager"

    const-string v5, "KnoxMUMContainerPolicy Service is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v2, v1

    .line 1422
    .end local v1    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .local v2, "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :goto_0
    return-object v2

    .line 1418
    .end local v2    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .restart local v1    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :cond_0
    :try_start_0
    invoke-interface {v3, p0}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager;->getContainers(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_1
    move-object v2, v1

    .line 1422
    .end local v1    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .restart local v2    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    goto :goto_0

    .line 1419
    .end local v2    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .restart local v1    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :catch_0
    move-exception v0

    .line 1420
    .local v0, "e":Landroid/os/RemoteException;
    const-string v4, "KnoxContainerManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed at KnoxContainerManager API getContainers :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public static getDefaultConfigurationTypes()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/enterprise/knox/container/KnoxConfigurationType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1797
    const/4 v2, 0x0

    .line 1798
    .local v2, "typeList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/enterprise/knox/container/KnoxConfigurationType;>;"
    invoke-static {}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->getContainerService()Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    move-result-object v1

    .line 1799
    .local v1, "service":Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    if-nez v1, :cond_0

    .line 1800
    const-string v3, "KnoxContainerManager"

    const-string v4, "KnoxMUMContainerPolicy Service is not yet ready!!!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1809
    :goto_0
    return-object v2

    .line 1805
    :cond_0
    :try_start_0
    invoke-interface {v1}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager;->getDefaultConfigurationTypes()Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    .line 1806
    :catch_0
    move-exception v0

    .line 1807
    .local v0, "e":Landroid/os/RemoteException;
    const-string v3, "KnoxContainerManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed at KnoxContainerManager API getConfigurationType by id:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private static processCreateReturn(I)I
    .locals 0
    .param p0, "value"    # I

    .prologue
    .line 749
    packed-switch p0, :pswitch_data_0

    .line 760
    .end local p0    # "value":I
    :goto_0
    :pswitch_0
    return p0

    .line 758
    .restart local p0    # "value":I
    :pswitch_1
    const/16 p0, -0x3f6

    goto :goto_0

    .line 749
    nop

    :pswitch_data_0
    .packed-switch -0x3f5
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private static processNewTypeObject(Landroid/content/Context;Lcom/sec/enterprise/knox/container/KnoxConfigurationType;)V
    .locals 12
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "type"    # Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    .line 1625
    const-string v5, "KnoxContainerManager"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Images before copy:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p1}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->getCustomBadgeIcon()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p1}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->getCustomHomeScreenWallpaper()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p1}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->getCustomLockScreenWallpaper()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p1}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->getCustomStatusIcon()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v5, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1629
    const-string v9, "KnoxContainerManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Images value conditions:"

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {p1}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->getCustomBadgeIcon()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-virtual {p1}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->getCustomBadgeIcon()Ljava/lang/String;

    move-result-object v5

    const-string v11, ""

    invoke-virtual {v5, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    move v5, v6

    :goto_0
    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v10, " "

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {p1}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->getCustomHomeScreenWallpaper()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-virtual {p1}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->getCustomHomeScreenWallpaper()Ljava/lang/String;

    move-result-object v5

    const-string v11, ""

    invoke-virtual {v5, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    move v5, v6

    :goto_1
    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v10, " "

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {p1}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->getCustomLockScreenWallpaper()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_3

    invoke-virtual {p1}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->getCustomLockScreenWallpaper()Ljava/lang/String;

    move-result-object v5

    const-string v11, ""

    invoke-virtual {v5, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    move v5, v6

    :goto_2
    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v10, " "

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->getCustomStatusIcon()Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_4

    invoke-virtual {p1}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->getCustomStatusIcon()Ljava/lang/String;

    move-result-object v10

    const-string v11, ""

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_4

    :goto_3
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v9, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1634
    invoke-virtual {p1}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->getCustomBadgeIcon()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_5

    invoke-virtual {p1}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->getCustomBadgeIcon()Ljava/lang/String;

    move-result-object v5

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_5

    invoke-virtual {p1}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->getCustomBadgeIcon()Ljava/lang/String;

    move-result-object v5

    const-string v6, "icon"

    invoke-static {p0, v5, v6}, Landroid/app/enterprise/lso/LSOUtils;->copyFileToDataLocalDirectory(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1637
    .local v0, "customBadgeIcon":Ljava/lang/String;
    :goto_4
    invoke-virtual {p1}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->getCustomHomeScreenWallpaper()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_6

    invoke-virtual {p1}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->getCustomHomeScreenWallpaper()Ljava/lang/String;

    move-result-object v5

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_6

    invoke-virtual {p1}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->getCustomHomeScreenWallpaper()Ljava/lang/String;

    move-result-object v5

    const-string v6, "wp"

    invoke-static {p0, v5, v6}, Landroid/app/enterprise/lso/LSOUtils;->copyFileToDataLocalDirectory(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1640
    .local v1, "customHomeScreenWallpaper":Ljava/lang/String;
    :goto_5
    invoke-virtual {p1}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->getCustomLockScreenWallpaper()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_7

    invoke-virtual {p1}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->getCustomLockScreenWallpaper()Ljava/lang/String;

    move-result-object v5

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_7

    invoke-virtual {p1}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->getCustomLockScreenWallpaper()Ljava/lang/String;

    move-result-object v5

    const-string v6, "wp"

    invoke-static {p0, v5, v6}, Landroid/app/enterprise/lso/LSOUtils;->copyFileToDataLocalDirectory(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1643
    .local v2, "customLockScreenWallpaper":Ljava/lang/String;
    :goto_6
    invoke-virtual {p1}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->getCustomStatusIcon()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_8

    invoke-virtual {p1}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->getCustomStatusIcon()Ljava/lang/String;

    move-result-object v5

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_8

    invoke-virtual {p1}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->getCustomStatusIcon()Ljava/lang/String;

    move-result-object v5

    const-string v6, "icon"

    invoke-static {p0, v5, v6}, Landroid/app/enterprise/lso/LSOUtils;->copyFileToDataLocalDirectory(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1646
    .local v3, "customStatusIcon":Ljava/lang/String;
    :goto_7
    invoke-virtual {p1, v0}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->setCustomBadgeIcon(Ljava/lang/String;)V

    .line 1647
    invoke-virtual {p1, v1}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->setCustomHomeScreenWallpaper(Ljava/lang/String;)V

    .line 1648
    invoke-virtual {p1, v2}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->setCustomLockScreenWallpaper(Ljava/lang/String;)V

    .line 1649
    invoke-virtual {p1, v3}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->setCustomStatusIcon(Ljava/lang/String;)V

    .line 1651
    instance-of v5, p1, Lcom/sec/enterprise/knox/container/LightweightConfigurationType;

    if-eqz v5, :cond_0

    move-object v5, p1

    .line 1652
    check-cast v5, Lcom/sec/enterprise/knox/container/LightweightConfigurationType;

    invoke-virtual {v5}, Lcom/sec/enterprise/knox/container/LightweightConfigurationType;->getFolderHeaderIcon()Ljava/lang/String;

    move-result-object v4

    .line 1653
    .local v4, "folderHeaderIcon":Ljava/lang/String;
    const-string v5, "KnoxContainerManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "folder header icon: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1654
    if-eqz v4, :cond_9

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_9

    const-string v5, "icon"

    invoke-static {p0, v4, v5}, Landroid/app/enterprise/lso/LSOUtils;->copyFileToDataLocalDirectory(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1657
    :goto_8
    const-string v5, "KnoxContainerManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "folder header icon after copy: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v5, p1

    .line 1658
    check-cast v5, Lcom/sec/enterprise/knox/container/LightweightConfigurationType;

    invoke-virtual {v5, v4}, Lcom/sec/enterprise/knox/container/LightweightConfigurationType;->setFolderHeaderIcon(Ljava/lang/String;)V

    .line 1661
    .end local v4    # "folderHeaderIcon":Ljava/lang/String;
    :cond_0
    const-string v5, "KnoxContainerManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Images after copy:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->getCustomBadgeIcon()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->getCustomHomeScreenWallpaper()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->getCustomLockScreenWallpaper()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->getCustomStatusIcon()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1665
    return-void

    .end local v0    # "customBadgeIcon":Ljava/lang/String;
    .end local v1    # "customHomeScreenWallpaper":Ljava/lang/String;
    .end local v2    # "customLockScreenWallpaper":Ljava/lang/String;
    .end local v3    # "customStatusIcon":Ljava/lang/String;
    :cond_1
    move v5, v7

    .line 1629
    goto/16 :goto_0

    :cond_2
    move v5, v7

    goto/16 :goto_1

    :cond_3
    move v5, v7

    goto/16 :goto_2

    :cond_4
    move v6, v7

    goto/16 :goto_3

    :cond_5
    move-object v0, v8

    .line 1634
    goto/16 :goto_4

    .restart local v0    # "customBadgeIcon":Ljava/lang/String;
    :cond_6
    move-object v1, v8

    .line 1637
    goto/16 :goto_5

    .restart local v1    # "customHomeScreenWallpaper":Ljava/lang/String;
    :cond_7
    move-object v2, v8

    .line 1640
    goto/16 :goto_6

    .restart local v2    # "customLockScreenWallpaper":Ljava/lang/String;
    :cond_8
    move-object v3, v8

    .line 1643
    goto/16 :goto_7

    .restart local v3    # "customStatusIcon":Ljava/lang/String;
    .restart local v4    # "folderHeaderIcon":Ljava/lang/String;
    :cond_9
    move-object v4, v8

    .line 1654
    goto :goto_8
.end method

.method private static processRemoveReturn(I)I
    .locals 0
    .param p0, "value"    # I

    .prologue
    .line 764
    packed-switch p0, :pswitch_data_0

    .line 768
    .end local p0    # "value":I
    :goto_0
    return p0

    .line 766
    .restart local p0    # "value":I
    :pswitch_0
    const/16 p0, -0x3f6

    goto :goto_0

    .line 764
    nop

    :pswitch_data_0
    .packed-switch -0x4b1
        :pswitch_0
    .end packed-switch
.end method

.method public static removeConfigurationType(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    .locals 6
    .param p0, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 1603
    const/4 v1, 0x0

    .line 1604
    .local v1, "retVal":Z
    invoke-static {}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->getContainerService()Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    move-result-object v3

    .line 1605
    .local v3, "service":Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    if-nez v3, :cond_0

    .line 1606
    const-string v4, "KnoxContainerManager"

    const-string v5, "KnoxMUMContainerPolicy Service is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v1

    .line 1621
    .end local v1    # "retVal":Z
    .local v2, "retVal":I
    :goto_0
    return v2

    .line 1610
    .end local v2    # "retVal":I
    .restart local v1    # "retVal":Z
    :cond_0
    if-nez p1, :cond_1

    .line 1611
    const-string v4, "KnoxContainerManager"

    const-string v5, "type string is NULL!!, returning.."

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v1

    .line 1612
    .restart local v2    # "retVal":I
    goto :goto_0

    .line 1616
    .end local v2    # "retVal":I
    :cond_1
    :try_start_0
    invoke-interface {v3, p0, p1}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager;->removeConfigurationType(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_1
    move v2, v1

    .line 1621
    .restart local v2    # "retVal":I
    goto :goto_0

    .line 1617
    .end local v2    # "retVal":I
    :catch_0
    move-exception v0

    .line 1618
    .local v0, "e":Landroid/os/RemoteException;
    const-string v4, "KnoxContainerManager"

    const-string v5, "Failed at KnoxContainerManager API removeConfigurationType:"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1619
    const-string v4, "KnoxContainerManager"

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public static removeConfigurationType(Ljava/lang/String;)Z
    .locals 2
    .param p0, "type"    # Ljava/lang/String;

    .prologue
    .line 1594
    new-instance v0, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    invoke-direct {v0, v1}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const-string v1, "KnoxContainerManager.removeConfigurationType"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1595
    const/4 v0, 0x0

    invoke-static {v0, p0}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->removeConfigurationType(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static removeContainer(I)I
    .locals 2
    .param p0, "id"    # I

    .prologue
    .line 1359
    new-instance v0, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    invoke-direct {v0, v1}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const-string v1, "KnoxContainerManager.removeContainer"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1360
    new-instance v0, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    invoke-direct {v0, v1, p0}, Landroid/app/enterprise/ContextInfo;-><init>(II)V

    invoke-static {v0}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->removeContainer(Landroid/app/enterprise/ContextInfo;)I

    move-result v0

    return v0
.end method

.method public static removeContainer(Landroid/app/enterprise/ContextInfo;)I
    .locals 7
    .param p0, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;

    .prologue
    .line 1368
    const/16 v1, -0x4b1

    .line 1369
    .local v1, "retVal":I
    invoke-static {}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->getContainerService()Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    move-result-object v3

    .line 1370
    .local v3, "service":Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    if-nez v3, :cond_0

    .line 1371
    const-string v4, "KnoxContainerManager"

    const-string v5, "KnoxMUMContainerPolicy Service is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v1

    .line 1380
    .end local v1    # "retVal":I
    .local v2, "retVal":I
    :goto_0
    return v2

    .line 1376
    .end local v2    # "retVal":I
    .restart local v1    # "retVal":I
    :cond_0
    :try_start_0
    invoke-interface {v3, p0}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager;->removeContainer(Landroid/app/enterprise/ContextInfo;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_1
    move v2, v1

    .line 1380
    .end local v1    # "retVal":I
    .restart local v2    # "retVal":I
    goto :goto_0

    .line 1377
    .end local v2    # "retVal":I
    .restart local v1    # "retVal":I
    :catch_0
    move-exception v0

    .line 1378
    .local v0, "e":Landroid/os/RemoteException;
    const-string v4, "KnoxContainerManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed at KnoxContainerManager API removeContainer "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public static resetContainerPolicies(I)Z
    .locals 4
    .param p0, "containerId"    # I

    .prologue
    .line 3041
    new-instance v2, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v3

    invoke-direct {v2, v3}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const-string v3, "KnoxContainerManager.resetContainerPolicies"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 3042
    new-instance v0, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v2

    invoke-direct {v0, v2, p0}, Landroid/app/enterprise/ContextInfo;-><init>(II)V

    .line 3043
    .local v0, "contextInfo":Landroid/app/enterprise/ContextInfo;
    invoke-static {v0}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->resetContainerPolicies(Landroid/app/enterprise/ContextInfo;)Z

    move-result v1

    .line 3044
    .local v1, "retVal":Z
    return v1
.end method

.method public static resetContainerPolicies(Landroid/app/enterprise/ContextInfo;)Z
    .locals 7
    .param p0, "contextInfo"    # Landroid/app/enterprise/ContextInfo;

    .prologue
    .line 3052
    const/4 v1, 0x0

    .line 3053
    .local v1, "retVal":Z
    invoke-static {}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->getContainerService()Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    move-result-object v3

    .line 3054
    .local v3, "service":Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    if-nez v3, :cond_0

    .line 3055
    const-string v4, "KnoxContainerManager"

    const-string v5, "KnoxMUMContainerPolicy Service is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v1

    .line 3064
    .end local v1    # "retVal":Z
    .local v2, "retVal":I
    :goto_0
    return v2

    .line 3060
    .end local v2    # "retVal":I
    .restart local v1    # "retVal":Z
    :cond_0
    :try_start_0
    invoke-interface {v3, p0}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager;->resetContainerPolicies(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_1
    move v2, v1

    .line 3064
    .restart local v2    # "retVal":I
    goto :goto_0

    .line 3061
    .end local v2    # "retVal":I
    :catch_0
    move-exception v0

    .line 3062
    .local v0, "e":Landroid/os/RemoteException;
    const-string v4, "KnoxContainerManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed at KnoxContainerManager API resetContainerPolicies "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method


# virtual methods
.method public activateDevicePermissions(Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 2875
    .local p1, "devicePermissions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContext:Landroid/content/Context;

    const-string v2, "enterprise_policy"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/EnterpriseDeviceManager;

    .line 2878
    .local v0, "edm":Landroid/app/enterprise/EnterpriseDeviceManager;
    invoke-virtual {v0, p1}, Landroid/app/enterprise/EnterpriseDeviceManager;->activateDevicePermissions(Ljava/util/List;)Z

    move-result v1

    return v1
.end method

.method public enforceMultifactorAuthentication(Z)V
    .locals 5
    .param p1, "enforce"    # Z

    .prologue
    .line 2050
    iget-object v3, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v4, "KnoxContainerManager.enforceMultifactorAuthentication"

    invoke-static {v3, v4}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2052
    const/4 v1, 0x0

    .line 2053
    .local v1, "retVal":Z
    invoke-static {}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->getContainerService()Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    move-result-object v2

    .line 2054
    .local v2, "service":Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    if-nez v2, :cond_0

    .line 2055
    const-string v3, "KnoxContainerManager"

    const-string v4, "ContainerPolicy Service is not yet ready!!!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2064
    :goto_0
    return-void

    .line 2060
    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3, p1}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager;->enforceMultifactorAuthentication(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 2061
    :catch_0
    move-exception v0

    .line 2062
    .local v0, "e":Landroid/os/RemoteException;
    const-string v3, "KnoxContainerManager"

    const-string v4, "Failed at KnoxContainerManager API unlock "

    invoke-static {v3, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public getAdvancedRestrictionPolicy()Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;
    .locals 4

    .prologue
    .line 2938
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mAdvancedRestrictionPolicyCreated:Z

    if-nez v0, :cond_1

    .line 2939
    const-class v1, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;

    monitor-enter v1

    .line 2940
    :try_start_0
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mAdvancedRestrictionPolicyCreated:Z

    if-nez v0, :cond_0

    .line 2941
    new-instance v0, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget-object v3, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2, v3}, Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mAdvancedRestrictionPolicy:Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;

    .line 2942
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mAdvancedRestrictionPolicyCreated:Z

    .line 2944
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2946
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mAdvancedRestrictionPolicy:Lcom/sec/enterprise/knox/AdvancedRestrictionPolicy;

    return-object v0

    .line 2944
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getApplicationPolicy()Landroid/app/enterprise/ApplicationPolicy;
    .locals 3

    .prologue
    .line 2360
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mApplicationPolicyCreated:Z

    if-nez v0, :cond_1

    .line 2361
    const-class v1, Landroid/app/enterprise/ApplicationPolicy;

    monitor-enter v1

    .line 2362
    :try_start_0
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mApplicationPolicyCreated:Z

    if-nez v0, :cond_0

    .line 2363
    new-instance v0, Landroid/app/enterprise/ApplicationPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-direct {v0, v2}, Landroid/app/enterprise/ApplicationPolicy;-><init>(Landroid/app/enterprise/ContextInfo;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mApplicationPolicy:Landroid/app/enterprise/ApplicationPolicy;

    .line 2364
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mApplicationPolicyCreated:Z

    .line 2366
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2368
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mApplicationPolicy:Landroid/app/enterprise/ApplicationPolicy;

    return-object v0

    .line 2366
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getBasePasswordPolicy()Landroid/app/enterprise/BasePasswordPolicy;
    .locals 3

    .prologue
    .line 2733
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mBasePasswordPolicyCreated:Z

    if-nez v0, :cond_1

    .line 2734
    const-class v1, Landroid/app/enterprise/BasePasswordPolicy;

    monitor-enter v1

    .line 2735
    :try_start_0
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mBasePasswordPolicyCreated:Z

    if-nez v0, :cond_0

    .line 2736
    new-instance v0, Landroid/app/enterprise/BasePasswordPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-direct {v0, v2}, Landroid/app/enterprise/BasePasswordPolicy;-><init>(Landroid/app/enterprise/ContextInfo;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mBasePasswordPolicy:Landroid/app/enterprise/BasePasswordPolicy;

    .line 2737
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mBasePasswordPolicyCreated:Z

    .line 2739
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2741
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mBasePasswordPolicy:Landroid/app/enterprise/BasePasswordPolicy;

    return-object v0

    .line 2739
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getBrowserPolicy()Landroid/app/enterprise/BrowserPolicy;
    .locals 3

    .prologue
    .line 2387
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mBrowserPolicyCreated:Z

    if-nez v0, :cond_1

    .line 2388
    const-class v1, Landroid/app/enterprise/BrowserPolicy;

    monitor-enter v1

    .line 2389
    :try_start_0
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mBrowserPolicyCreated:Z

    if-nez v0, :cond_0

    .line 2390
    new-instance v0, Landroid/app/enterprise/BrowserPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-direct {v0, v2}, Landroid/app/enterprise/BrowserPolicy;-><init>(Landroid/app/enterprise/ContextInfo;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mBrowserPolicy:Landroid/app/enterprise/BrowserPolicy;

    .line 2391
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mBrowserPolicyCreated:Z

    .line 2393
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2395
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mBrowserPolicy:Landroid/app/enterprise/BrowserPolicy;

    return-object v0

    .line 2393
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getCertificatePolicy()Lcom/sec/enterprise/knox/certificate/CertificatePolicy;
    .locals 3

    .prologue
    .line 3004
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mCertificatePolicyCreated:Z

    if-nez v0, :cond_1

    .line 3005
    const-class v1, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;

    monitor-enter v1

    .line 3006
    :try_start_0
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mCertificatePolicyCreated:Z

    if-nez v0, :cond_0

    .line 3007
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget-object v2, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContext:Landroid/content/Context;

    invoke-static {v0, v2}, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->getInstance(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)Lcom/sec/enterprise/knox/certificate/CertificatePolicy;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mCertificatePolicy:Lcom/sec/enterprise/knox/certificate/CertificatePolicy;

    .line 3008
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mCertificatePolicyCreated:Z

    .line 3010
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3012
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mCertificatePolicy:Lcom/sec/enterprise/knox/certificate/CertificatePolicy;

    return-object v0

    .line 3010
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getClientCertificateManagerPolicy()Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;
    .locals 4

    .prologue
    .line 2981
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mClientCertificateManagerPolicyCreated:Z

    if-nez v0, :cond_1

    .line 2982
    const-class v1, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;

    monitor-enter v1

    .line 2983
    :try_start_0
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mClientCertificateManagerPolicyCreated:Z

    if-nez v0, :cond_0

    .line 2984
    new-instance v0, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;

    iget-object v2, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget-object v3, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2, v3}, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mClientCertificateManagerPolicy:Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;

    .line 2985
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mClientCertificateManagerPolicyCreated:Z

    .line 2987
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2989
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mClientCertificateManagerPolicy:Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;

    return-object v0

    .line 2987
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getContainerConfigurationPolicy()Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;
    .locals 3

    .prologue
    .line 2850
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContainerConfigurationPolicyCreated:Z

    if-nez v0, :cond_1

    .line 2851
    const-class v1, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;

    monitor-enter v1

    .line 2852
    :try_start_0
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContainerConfigurationPolicyCreated:Z

    if-nez v0, :cond_0

    .line 2853
    new-instance v0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-direct {v0, v2}, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;-><init>(Landroid/app/enterprise/ContextInfo;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContainerConfigurationPolicy:Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;

    .line 2854
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContainerConfigurationPolicyCreated:Z

    .line 2856
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2858
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContainerConfigurationPolicy:Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;

    return-object v0

    .line 2856
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getDateTimePolicy()Landroid/app/enterprise/DateTimePolicy;
    .locals 3

    .prologue
    .line 2410
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mDateTimePolicyCreated:Z

    if-nez v0, :cond_1

    .line 2411
    const-class v1, Landroid/app/enterprise/DateTimePolicy;

    monitor-enter v1

    .line 2412
    :try_start_0
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mDateTimePolicyCreated:Z

    if-nez v0, :cond_0

    .line 2413
    new-instance v0, Landroid/app/enterprise/DateTimePolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-direct {v0, v2}, Landroid/app/enterprise/DateTimePolicy;-><init>(Landroid/app/enterprise/ContextInfo;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mDateTimePolicy:Landroid/app/enterprise/DateTimePolicy;

    .line 2414
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mDateTimePolicyCreated:Z

    .line 2416
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2418
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mDateTimePolicy:Landroid/app/enterprise/DateTimePolicy;

    return-object v0

    .line 2416
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getDeviceAccountPolicy()Landroid/app/enterprise/DeviceAccountPolicy;
    .locals 3

    .prologue
    .line 2433
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mDeviceAccountPolicyCreated:Z

    if-nez v0, :cond_1

    .line 2434
    const-class v1, Landroid/app/enterprise/DeviceAccountPolicy;

    monitor-enter v1

    .line 2435
    :try_start_0
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mDeviceAccountPolicyCreated:Z

    if-nez v0, :cond_0

    .line 2436
    new-instance v0, Landroid/app/enterprise/DeviceAccountPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-direct {v0, v2}, Landroid/app/enterprise/DeviceAccountPolicy;-><init>(Landroid/app/enterprise/ContextInfo;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mDeviceAccountPolicy:Landroid/app/enterprise/DeviceAccountPolicy;

    .line 2437
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mDeviceAccountPolicyCreated:Z

    .line 2439
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2441
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mDeviceAccountPolicy:Landroid/app/enterprise/DeviceAccountPolicy;

    return-object v0

    .line 2439
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getDeviceInventory()Landroid/app/enterprise/DeviceInventory;
    .locals 4

    .prologue
    .line 2456
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mDeviceInventoryCreated:Z

    if-nez v0, :cond_1

    .line 2457
    const-class v1, Landroid/app/enterprise/DeviceInventory;

    monitor-enter v1

    .line 2458
    :try_start_0
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mDeviceInventoryCreated:Z

    if-nez v0, :cond_0

    .line 2459
    new-instance v0, Landroid/app/enterprise/DeviceInventory;

    iget-object v2, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget-object v3, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2, v3}, Landroid/app/enterprise/DeviceInventory;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mDeviceInventory:Landroid/app/enterprise/DeviceInventory;

    .line 2460
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mDeviceInventoryCreated:Z

    .line 2462
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2464
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mDeviceInventory:Landroid/app/enterprise/DeviceInventory;

    return-object v0

    .line 2462
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getEmailAccountPolicy()Landroid/app/enterprise/EmailAccountPolicy;
    .locals 3

    .prologue
    .line 2479
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mEmailAccountPolicyCreated:Z

    if-nez v0, :cond_1

    .line 2480
    const-class v1, Landroid/app/enterprise/EmailAccountPolicy;

    monitor-enter v1

    .line 2481
    :try_start_0
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mEmailAccountPolicyCreated:Z

    if-nez v0, :cond_0

    .line 2482
    new-instance v0, Landroid/app/enterprise/EmailAccountPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-direct {v0, v2}, Landroid/app/enterprise/EmailAccountPolicy;-><init>(Landroid/app/enterprise/ContextInfo;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mEmailAccountPolicy:Landroid/app/enterprise/EmailAccountPolicy;

    .line 2483
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mEmailAccountPolicyCreated:Z

    .line 2485
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2487
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mEmailAccountPolicy:Landroid/app/enterprise/EmailAccountPolicy;

    return-object v0

    .line 2485
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getEmailPolicy()Landroid/app/enterprise/EmailPolicy;
    .locals 3

    .prologue
    .line 2502
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mEmailPolicyCreated:Z

    if-nez v0, :cond_1

    .line 2503
    const-class v1, Landroid/app/enterprise/EmailPolicy;

    monitor-enter v1

    .line 2504
    :try_start_0
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mEmailPolicyCreated:Z

    if-nez v0, :cond_0

    .line 2505
    new-instance v0, Landroid/app/enterprise/EmailPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-direct {v0, v2}, Landroid/app/enterprise/EmailPolicy;-><init>(Landroid/app/enterprise/ContextInfo;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mEmailPolicy:Landroid/app/enterprise/EmailPolicy;

    .line 2506
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mEmailPolicyCreated:Z

    .line 2508
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2510
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mEmailPolicy:Landroid/app/enterprise/EmailPolicy;

    return-object v0

    .line 2508
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getEnterpriseBillingPolicy()Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;
    .locals 4

    .prologue
    .line 3177
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mEnterpriseBillingPolicyCreated:Z

    if-nez v0, :cond_1

    .line 3178
    const-class v1, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;

    monitor-enter v1

    .line 3179
    :try_start_0
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mEnterpriseBillingPolicyCreated:Z

    if-nez v0, :cond_0

    .line 3181
    new-instance v0, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-direct {v0, v2}, Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;-><init>(Landroid/app/enterprise/ContextInfo;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mEnterpriseBillingPolicy:Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;

    .line 3182
    const-string v0, "EnterpriseBillingPolicy"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Added Client : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mEnterpriseBillingPolicy:Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 3183
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mEnterpriseBillingPolicyCreated:Z

    .line 3185
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3187
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mEnterpriseBillingPolicy:Lcom/sec/enterprise/knox/billing/EnterpriseBillingPolicy;

    return-object v0

    .line 3185
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getEnterpriseCertEnrollPolicy(Ljava/lang/String;)Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;
    .locals 2
    .param p1, "cepProtocol"    # Ljava/lang/String;

    .prologue
    .line 3144
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget-object v1, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContext:Landroid/content/Context;

    invoke-static {v0, v1, p1}, Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;->getInstance(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;Ljava/lang/String;)Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;

    move-result-object v0

    return-object v0
.end method

.method public getEnterpriseSSOPolicy()Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;
    .locals 4

    .prologue
    .line 2897
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mEnterpriseSSOPolicyCreated:Z

    if-nez v0, :cond_1

    .line 2898
    const-class v1, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;

    monitor-enter v1

    .line 2899
    :try_start_0
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mEnterpriseSSOPolicyCreated:Z

    if-nez v0, :cond_0

    .line 2900
    new-instance v0, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget-object v3, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget v3, v3, Landroid/app/enterprise/ContextInfo;->mContainerId:I

    invoke-direct {v0, v2, v3}, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;-><init>(Landroid/app/enterprise/ContextInfo;I)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mEnterpriseSSOPolicy:Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;

    .line 2901
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mEnterpriseSSOPolicyCreated:Z

    .line 2903
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2905
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mEnterpriseSSOPolicy:Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;

    return-object v0

    .line 2903
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getEnterpriseSSOPolicy(Ljava/lang/String;)Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 2914
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mEnterpriseSSOPolicyCreated:Z

    if-nez v0, :cond_1

    .line 2915
    const-class v1, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;

    monitor-enter v1

    .line 2916
    :try_start_0
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mEnterpriseSSOPolicyCreated:Z

    if-nez v0, :cond_0

    .line 2917
    new-instance v0, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget-object v3, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget v3, v3, Landroid/app/enterprise/ContextInfo;->mContainerId:I

    invoke-direct {v0, v2, v3, p1}, Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;-><init>(Landroid/app/enterprise/ContextInfo;ILjava/lang/String;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mEnterpriseSSOPolicy:Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;

    .line 2918
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mEnterpriseSSOPolicyCreated:Z

    .line 2920
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2922
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mEnterpriseSSOPolicy:Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;

    return-object v0

    .line 2920
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getEnterpriseVpnPolicy()Landroid/app/enterprise/EnterpriseVpnPolicy;
    .locals 3

    .prologue
    .line 2958
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mEnterpriseVpnPolicyCreated:Z

    if-nez v0, :cond_1

    .line 2959
    const-class v1, Landroid/app/enterprise/EnterpriseVpnPolicy;

    monitor-enter v1

    .line 2960
    :try_start_0
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mEnterpriseVpnPolicyCreated:Z

    if-nez v0, :cond_0

    .line 2961
    new-instance v0, Landroid/app/enterprise/EnterpriseVpnPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-direct {v0, v2}, Landroid/app/enterprise/EnterpriseVpnPolicy;-><init>(Landroid/app/enterprise/ContextInfo;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mEnterpriseVpnPolicy:Landroid/app/enterprise/EnterpriseVpnPolicy;

    .line 2962
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mEnterpriseVpnPolicyCreated:Z

    .line 2964
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2966
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mEnterpriseVpnPolicy:Landroid/app/enterprise/EnterpriseVpnPolicy;

    return-object v0

    .line 2964
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getExchangeAccountPolicy()Landroid/app/enterprise/ExchangeAccountPolicy;
    .locals 3

    .prologue
    .line 2525
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mEasAccountPolicyCreated:Z

    if-nez v0, :cond_1

    .line 2526
    const-class v1, Landroid/app/enterprise/ExchangeAccountPolicy;

    monitor-enter v1

    .line 2527
    :try_start_0
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mEasAccountPolicyCreated:Z

    if-nez v0, :cond_0

    .line 2528
    new-instance v0, Landroid/app/enterprise/ExchangeAccountPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-direct {v0, v2}, Landroid/app/enterprise/ExchangeAccountPolicy;-><init>(Landroid/app/enterprise/ContextInfo;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mEasAccountPolicy:Landroid/app/enterprise/ExchangeAccountPolicy;

    .line 2529
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mEasAccountPolicyCreated:Z

    .line 2531
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2533
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mEasAccountPolicy:Landroid/app/enterprise/ExchangeAccountPolicy;

    return-object v0

    .line 2531
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getFirewallPolicy()Landroid/app/enterprise/FirewallPolicy;
    .locals 4

    .prologue
    .line 2548
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mFirewallPolicyCreated:Z

    if-nez v0, :cond_1

    .line 2549
    const-class v1, Landroid/app/enterprise/FirewallPolicy;

    monitor-enter v1

    .line 2550
    :try_start_0
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mFirewallPolicyCreated:Z

    if-nez v0, :cond_0

    .line 2551
    new-instance v0, Landroid/app/enterprise/FirewallPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget-object v3, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2, v3}, Landroid/app/enterprise/FirewallPolicy;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mFirewallPolicy:Landroid/app/enterprise/FirewallPolicy;

    .line 2552
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mFirewallPolicyCreated:Z

    .line 2554
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2556
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mFirewallPolicy:Landroid/app/enterprise/FirewallPolicy;

    return-object v0

    .line 2554
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getGenericSSO()Landroid/app/enterprise/sso/GenericSSO;
    .locals 3

    .prologue
    .line 3158
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mGenericSSOCreated:Z

    if-nez v0, :cond_1

    .line 3159
    const-class v1, Landroid/app/enterprise/sso/GenericSSO;

    monitor-enter v1

    .line 3160
    :try_start_0
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mGenericSSOCreated:Z

    if-nez v0, :cond_0

    .line 3161
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget-object v2, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContext:Landroid/content/Context;

    invoke-static {v0, v2}, Landroid/app/enterprise/sso/GenericSSO;->getInstance(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)Landroid/app/enterprise/sso/GenericSSO;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mGenericSSO:Landroid/app/enterprise/sso/GenericSSO;

    .line 3162
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mGenericSSOCreated:Z

    .line 3164
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3166
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mGenericSSO:Landroid/app/enterprise/sso/GenericSSO;

    return-object v0

    .line 3164
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getGeofencing()Landroid/app/enterprise/geofencing/Geofencing;
    .locals 3

    .prologue
    .line 2571
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mGeofencingCreated:Z

    if-nez v0, :cond_2

    .line 2572
    const-class v1, Landroid/app/enterprise/geofencing/Geofencing;

    monitor-enter v1

    .line 2573
    :try_start_0
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mGeofencingCreated:Z

    if-nez v0, :cond_1

    .line 2574
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget-object v2, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContext:Landroid/content/Context;

    invoke-static {v0, v2}, Landroid/app/enterprise/geofencing/Geofencing;->getInstance(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)Landroid/app/enterprise/geofencing/Geofencing;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mGeofencing:Landroid/app/enterprise/geofencing/Geofencing;

    .line 2575
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mGeofencing:Landroid/app/enterprise/geofencing/Geofencing;

    if-nez v0, :cond_0

    .line 2576
    const/4 v0, 0x0

    monitor-exit v1

    .line 2582
    :goto_0
    return-object v0

    .line 2578
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mGeofencingCreated:Z

    .line 2580
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2582
    :cond_2
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mGeofencing:Landroid/app/enterprise/geofencing/Geofencing;

    goto :goto_0

    .line 2580
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getKioskMode()Landroid/app/enterprise/kioskmode/KioskMode;
    .locals 3

    .prologue
    .line 2597
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mKioskModeCreated:Z

    if-nez v0, :cond_1

    .line 2598
    const-class v1, Landroid/app/enterprise/kioskmode/KioskMode;

    monitor-enter v1

    .line 2599
    :try_start_0
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mKioskModeCreated:Z

    if-nez v0, :cond_0

    .line 2600
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget-object v2, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContext:Landroid/content/Context;

    invoke-static {v0, v2}, Landroid/app/enterprise/kioskmode/KioskMode;->getInstance(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)Landroid/app/enterprise/kioskmode/KioskMode;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mKioskMode:Landroid/app/enterprise/kioskmode/KioskMode;

    .line 2601
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mKioskModeCreated:Z

    .line 2603
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2605
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mKioskMode:Landroid/app/enterprise/kioskmode/KioskMode;

    return-object v0

    .line 2603
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getLDAPAccountPolicy()Landroid/app/enterprise/LDAPAccountPolicy;
    .locals 4

    .prologue
    .line 2690
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mLDAPAccountPolicyCreated:Z

    if-nez v0, :cond_1

    .line 2691
    const-class v1, Landroid/app/enterprise/LDAPAccountPolicy;

    monitor-enter v1

    .line 2692
    :try_start_0
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mLDAPAccountPolicyCreated:Z

    if-nez v0, :cond_0

    .line 2693
    new-instance v0, Landroid/app/enterprise/LDAPAccountPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget-object v3, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2, v3}, Landroid/app/enterprise/LDAPAccountPolicy;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mLDAPAccountPolicy:Landroid/app/enterprise/LDAPAccountPolicy;

    .line 2694
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mLDAPAccountPolicyCreated:Z

    .line 2696
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2698
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mLDAPAccountPolicy:Landroid/app/enterprise/LDAPAccountPolicy;

    return-object v0

    .line 2696
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getLocationPolicy()Landroid/app/enterprise/LocationPolicy;
    .locals 3

    .prologue
    .line 2620
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mLocationPolicyCreated:Z

    if-nez v0, :cond_1

    .line 2621
    const-class v1, Landroid/app/enterprise/LocationPolicy;

    monitor-enter v1

    .line 2622
    :try_start_0
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mLocationPolicyCreated:Z

    if-nez v0, :cond_0

    .line 2623
    new-instance v0, Landroid/app/enterprise/LocationPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-direct {v0, v2}, Landroid/app/enterprise/LocationPolicy;-><init>(Landroid/app/enterprise/ContextInfo;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mLocationPolicy:Landroid/app/enterprise/LocationPolicy;

    .line 2624
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mLocationPolicyCreated:Z

    .line 2626
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2628
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mLocationPolicy:Landroid/app/enterprise/LocationPolicy;

    return-object v0

    .line 2626
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getMiscPolicy()Landroid/app/enterprise/MiscPolicy;
    .locals 4

    .prologue
    .line 2666
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mMiscPolicyCreated:Z

    if-nez v0, :cond_1

    .line 2667
    const-class v1, Landroid/app/enterprise/MiscPolicy;

    monitor-enter v1

    .line 2668
    :try_start_0
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mMiscPolicyCreated:Z

    if-nez v0, :cond_0

    .line 2669
    new-instance v0, Landroid/app/enterprise/MiscPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget-object v3, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2, v3}, Landroid/app/enterprise/MiscPolicy;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mMiscPolicy:Landroid/app/enterprise/MiscPolicy;

    .line 2670
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mMiscPolicyCreated:Z

    .line 2672
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2674
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mMiscPolicy:Landroid/app/enterprise/MiscPolicy;

    return-object v0

    .line 2672
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getPasswordPolicy()Landroid/app/enterprise/PasswordPolicy;
    .locals 4

    .prologue
    .line 2756
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mPasswordPolicyCreated:Z

    if-nez v0, :cond_1

    .line 2757
    const-class v1, Landroid/app/enterprise/PasswordPolicy;

    monitor-enter v1

    .line 2758
    :try_start_0
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mPasswordPolicyCreated:Z

    if-nez v0, :cond_0

    .line 2759
    new-instance v0, Landroid/app/enterprise/PasswordPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget-object v3, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2, v3}, Landroid/app/enterprise/PasswordPolicy;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mPasswordPolicy:Landroid/app/enterprise/PasswordPolicy;

    .line 2760
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mPasswordPolicyCreated:Z

    .line 2762
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2764
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mPasswordPolicy:Landroid/app/enterprise/PasswordPolicy;

    return-object v0

    .line 2762
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getRCPPolicy()Lcom/sec/enterprise/knox/container/RCPPolicy;
    .locals 3

    .prologue
    .line 2248
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mRCPPolicy:Lcom/sec/enterprise/knox/container/RCPPolicy;

    if-nez v0, :cond_0

    .line 2249
    const-class v1, Lcom/sec/enterprise/knox/container/RCPPolicy;

    monitor-enter v1

    .line 2250
    :try_start_0
    new-instance v0, Lcom/sec/enterprise/knox/container/RCPPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-direct {v0, v2}, Lcom/sec/enterprise/knox/container/RCPPolicy;-><init>(Landroid/app/enterprise/ContextInfo;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mRCPPolicy:Lcom/sec/enterprise/knox/container/RCPPolicy;

    .line 2251
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2253
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mRCPPolicy:Lcom/sec/enterprise/knox/container/RCPPolicy;

    return-object v0

    .line 2251
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getRestrictionPolicy()Landroid/app/enterprise/RestrictionPolicy;
    .locals 4

    .prologue
    .line 2714
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mRestrictionPolicyCreated:Z

    if-nez v0, :cond_1

    .line 2715
    const-class v1, Landroid/app/enterprise/RestrictionPolicy;

    monitor-enter v1

    .line 2716
    :try_start_0
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mRestrictionPolicyCreated:Z

    if-nez v0, :cond_0

    .line 2717
    new-instance v0, Landroid/app/enterprise/RestrictionPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget-object v3, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2, v3}, Landroid/app/enterprise/RestrictionPolicy;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mRestrictionPolicy:Landroid/app/enterprise/RestrictionPolicy;

    .line 2718
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mRestrictionPolicyCreated:Z

    .line 2720
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2722
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mRestrictionPolicy:Landroid/app/enterprise/RestrictionPolicy;

    return-object v0

    .line 2720
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getSecurityPolicy()Landroid/app/enterprise/SecurityPolicy;
    .locals 4

    .prologue
    .line 2779
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mSecurityPolicyCreated:Z

    if-nez v0, :cond_1

    .line 2780
    const-class v1, Landroid/app/enterprise/SecurityPolicy;

    monitor-enter v1

    .line 2781
    :try_start_0
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mSecurityPolicyCreated:Z

    if-nez v0, :cond_0

    .line 2782
    new-instance v0, Landroid/app/enterprise/SecurityPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget-object v3, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2, v3}, Landroid/app/enterprise/SecurityPolicy;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mSecurityPolicy:Landroid/app/enterprise/SecurityPolicy;

    .line 2783
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mSecurityPolicyCreated:Z

    .line 2785
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2787
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mSecurityPolicy:Landroid/app/enterprise/SecurityPolicy;

    return-object v0

    .line 2785
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getSmartCardAccessPolicy()Lcom/sec/enterprise/knox/container/SmartCardAccessPolicy;
    .locals 3

    .prologue
    .line 2273
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mSmartCardAccessPolicy:Lcom/sec/enterprise/knox/container/SmartCardAccessPolicy;

    if-nez v0, :cond_0

    .line 2274
    const-class v1, Lcom/sec/enterprise/knox/container/SmartCardAccessPolicy;

    monitor-enter v1

    .line 2275
    :try_start_0
    new-instance v0, Lcom/sec/enterprise/knox/container/SmartCardAccessPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-direct {v0, v2}, Lcom/sec/enterprise/knox/container/SmartCardAccessPolicy;-><init>(Landroid/app/enterprise/ContextInfo;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mSmartCardAccessPolicy:Lcom/sec/enterprise/knox/container/SmartCardAccessPolicy;

    .line 2276
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2278
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mSmartCardAccessPolicy:Lcom/sec/enterprise/knox/container/SmartCardAccessPolicy;

    return-object v0

    .line 2276
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getSmartCardBrowserPolicy()Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;
    .locals 4

    .prologue
    .line 2827
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mSmartCardBrowserPolicyCreated:Z

    if-nez v0, :cond_1

    .line 2828
    const-class v1, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;

    monitor-enter v1

    .line 2829
    :try_start_0
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mSmartCardBrowserPolicyCreated:Z

    if-nez v0, :cond_0

    .line 2830
    new-instance v0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget-object v3, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2, v3}, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mSmartCardBrowserPolicy:Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;

    .line 2831
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mSmartCardBrowserPolicyCreated:Z

    .line 2833
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2835
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mSmartCardBrowserPolicy:Lcom/sec/enterprise/knox/smartcard/policy/SmartCardBrowserPolicy;

    return-object v0

    .line 2833
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getSmartCardEmailPolicy()Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;
    .locals 4

    .prologue
    .line 2803
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mSmartCardEmailPolicyCreated:Z

    if-nez v0, :cond_1

    .line 2804
    const-class v1, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;

    monitor-enter v1

    .line 2805
    :try_start_0
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mSmartCardEmailPolicyCreated:Z

    if-nez v0, :cond_0

    .line 2806
    new-instance v0, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget-object v3, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2, v3}, Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mSmartCardEmailPolicy:Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;

    .line 2807
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mSmartCardEmailPolicyCreated:Z

    .line 2809
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2811
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mSmartCardEmailPolicy:Lcom/sec/enterprise/knox/smartcard/policy/SmartCardEmailPolicy;

    return-object v0

    .line 2809
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getStatus()I
    .locals 7

    .prologue
    .line 1481
    const/4 v1, -0x1

    .line 1482
    .local v1, "retVal":I
    invoke-static {}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->getContainerService()Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    move-result-object v3

    .line 1483
    .local v3, "service":Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    if-nez v3, :cond_0

    .line 1484
    const-string v4, "KnoxContainerManager"

    const-string v5, "KnoxMUMContainerPolicy Service is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v1

    .line 1493
    .end local v1    # "retVal":I
    .local v2, "retVal":I
    :goto_0
    return v2

    .line 1489
    .end local v2    # "retVal":I
    .restart local v1    # "retVal":I
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v3, v4}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager;->getStatus(Landroid/app/enterprise/ContextInfo;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_1
    move v2, v1

    .line 1493
    .end local v1    # "retVal":I
    .restart local v2    # "retVal":I
    goto :goto_0

    .line 1490
    .end local v2    # "retVal":I
    .restart local v1    # "retVal":I
    :catch_0
    move-exception v0

    .line 1491
    .local v0, "e":Landroid/os/RemoteException;
    const-string v4, "KnoxContainerManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed at KnoxContainerManager API getStatus("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget v6, v6, Landroid/app/enterprise/ContextInfo;->mContainerId:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ") :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public getWifiPolicy()Landroid/app/enterprise/WifiPolicy;
    .locals 3

    .prologue
    .line 2643
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mWifiPolicyCreated:Z

    if-nez v0, :cond_1

    .line 2644
    const-class v1, Landroid/app/enterprise/WifiPolicy;

    monitor-enter v1

    .line 2645
    :try_start_0
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mWifiPolicyCreated:Z

    if-nez v0, :cond_0

    .line 2646
    new-instance v0, Landroid/app/enterprise/WifiPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-direct {v0, v2}, Landroid/app/enterprise/WifiPolicy;-><init>(Landroid/app/enterprise/ContextInfo;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mWifiPolicy:Landroid/app/enterprise/WifiPolicy;

    .line 2647
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mWifiPolicyCreated:Z

    .line 2649
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2651
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mWifiPolicy:Landroid/app/enterprise/WifiPolicy;

    return-object v0

    .line 2649
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public isMultifactorAuthenticationEnforced()Z
    .locals 6

    .prologue
    .line 2106
    const/4 v1, 0x0

    .line 2107
    .local v1, "retVal":Z
    invoke-static {}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->getContainerService()Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    move-result-object v3

    .line 2108
    .local v3, "service":Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    if-nez v3, :cond_0

    .line 2109
    const-string v4, "KnoxContainerManager"

    const-string v5, "ContainerPolicy Service is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v1

    .line 2119
    .end local v1    # "retVal":Z
    .local v2, "retVal":I
    :goto_0
    return v2

    .line 2114
    .end local v2    # "retVal":I
    .restart local v1    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v3, v4}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager;->isMultifactorAuthenticationEnforced(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_1
    move v2, v1

    .line 2119
    .restart local v2    # "retVal":I
    goto :goto_0

    .line 2115
    .end local v2    # "retVal":I
    :catch_0
    move-exception v0

    .line 2116
    .local v0, "e":Landroid/os/RemoteException;
    const-string v4, "KnoxContainerManager"

    const-string v5, "Failed at KnoxContainerManager API unlock "

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public lock()Z
    .locals 6

    .prologue
    .line 1871
    iget-object v4, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "KnoxContainerManager.lock"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1873
    const/4 v1, 0x0

    .line 1874
    .local v1, "retVal":Z
    invoke-static {}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->getContainerService()Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    move-result-object v3

    .line 1875
    .local v3, "service":Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    if-nez v3, :cond_0

    .line 1876
    const-string v4, "KnoxContainerManager"

    const-string v5, "ContainerPolicy Service is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v1

    .line 1886
    .end local v1    # "retVal":Z
    .local v2, "retVal":I
    :goto_0
    return v2

    .line 1881
    .end local v2    # "retVal":I
    .restart local v1    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager;->lockContainer(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_1
    move v2, v1

    .line 1886
    .restart local v2    # "retVal":I
    goto :goto_0

    .line 1882
    .end local v2    # "retVal":I
    :catch_0
    move-exception v0

    .line 1883
    .local v0, "e":Landroid/os/RemoteException;
    const-string v4, "KnoxContainerManager"

    const-string v5, "Failed at KnoxContainerManager API lock "

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public lock(Ljava/lang/String;)Z
    .locals 6
    .param p1, "newPassword"    # Ljava/lang/String;

    .prologue
    .line 1932
    iget-object v4, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "KnoxContainerManager.lock"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1934
    const/4 v1, 0x0

    .line 1935
    .local v1, "retVal":Z
    invoke-static {}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->getContainerService()Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    move-result-object v3

    .line 1936
    .local v3, "service":Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    if-nez v3, :cond_0

    .line 1937
    const-string v4, "KnoxContainerManager"

    const-string v5, "ContainerPolicy Service is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v1

    .line 1947
    .end local v1    # "retVal":Z
    .local v2, "retVal":I
    :goto_0
    return v2

    .line 1942
    .end local v2    # "retVal":I
    .restart local v1    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v3, v4, p1}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager;->lockContainer(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_1
    move v2, v1

    .line 1947
    .restart local v2    # "retVal":I
    goto :goto_0

    .line 1943
    .end local v2    # "retVal":I
    :catch_0
    move-exception v0

    .line 1944
    .local v0, "e":Landroid/os/RemoteException;
    const-string v4, "KnoxContainerManager"

    const-string v5, "Failed at KnoxContainerManager API lock "

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public registerBroadcastReceiverIntent(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 6
    .param p1, "intent"    # Ljava/lang/String;
    .param p2, "action"    # Ljava/lang/String;

    .prologue
    .line 2162
    iget-object v4, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "KnoxContainerManager.registerBroadcastReceiverIntent"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2163
    const/4 v1, 0x0

    .line 2164
    .local v1, "retVal":Z
    invoke-static {}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->getContainerService()Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    move-result-object v3

    .line 2165
    .local v3, "service":Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    if-nez v3, :cond_0

    .line 2166
    const-string v4, "KnoxContainerManager"

    const-string v5, "ContainerPolicy Service is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v1

    .line 2175
    .end local v1    # "retVal":Z
    .local v2, "retVal":I
    :goto_0
    return v2

    .line 2171
    .end local v2    # "retVal":I
    .restart local v1    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v3, v4, p1, p2}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager;->registerBroadcastReceiverIntent(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_1
    move v2, v1

    .line 2175
    .restart local v2    # "retVal":I
    goto :goto_0

    .line 2172
    .end local v2    # "retVal":I
    :catch_0
    move-exception v0

    .line 2173
    .local v0, "e":Landroid/os/RemoteException;
    const-string v4, "KnoxContainerManager"

    const-string v5, "Failed at KnoxContainerManager API registerBroadcastReceiverIntent "

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public unlock()Z
    .locals 6

    .prologue
    .line 1994
    iget-object v4, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "KnoxContainerManager.unlock"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1996
    const/4 v1, 0x0

    .line 1997
    .local v1, "retVal":Z
    invoke-static {}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->getContainerService()Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    move-result-object v3

    .line 1998
    .local v3, "service":Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    if-nez v3, :cond_0

    .line 1999
    const-string v4, "KnoxContainerManager"

    const-string v5, "ContainerPolicy Service is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v1

    .line 2009
    .end local v1    # "retVal":Z
    .local v2, "retVal":I
    :goto_0
    return v2

    .line 2004
    .end local v2    # "retVal":I
    .restart local v1    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v3, v4}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager;->unlockContainer(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_1
    move v2, v1

    .line 2009
    .restart local v2    # "retVal":I
    goto :goto_0

    .line 2005
    .end local v2    # "retVal":I
    :catch_0
    move-exception v0

    .line 2006
    .local v0, "e":Landroid/os/RemoteException;
    const-string v4, "KnoxContainerManager"

    const-string v5, "Failed at KnoxContainerManager API unlock "

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public unregisterBroadcastReceiverIntent(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 6
    .param p1, "intent"    # Ljava/lang/String;
    .param p2, "action"    # Ljava/lang/String;

    .prologue
    .line 2217
    iget-object v4, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "KnoxContainerManager.unregisterBroadcastReceiverIntent"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2218
    const/4 v1, 0x0

    .line 2219
    .local v1, "retVal":Z
    invoke-static {}, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->getContainerService()Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    move-result-object v3

    .line 2220
    .local v3, "service":Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    if-nez v3, :cond_0

    .line 2221
    const-string v4, "KnoxContainerManager"

    const-string v5, "ContainerPolicy Service is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v1

    .line 2230
    .end local v1    # "retVal":Z
    .local v2, "retVal":I
    :goto_0
    return v2

    .line 2226
    .end local v2    # "retVal":I
    .restart local v1    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v3, v4, p1, p2}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager;->unregisterBroadcastReceiverIntent(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_1
    move v2, v1

    .line 2230
    .restart local v2    # "retVal":I
    goto :goto_0

    .line 2227
    .end local v2    # "retVal":I
    :catch_0
    move-exception v0

    .line 2228
    .local v0, "e":Landroid/os/RemoteException;
    const-string v4, "KnoxContainerManager"

    const-string v5, "Failed at KnoxContainerManager API unregisterBroadcastReceiverIntent "

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method
