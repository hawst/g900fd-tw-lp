.class public Lcom/sec/enterprise/knox/container/KnoxConfigurationType;
.super Ljava/lang/Object;
.source "KnoxConfigurationType.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/enterprise/knox/container/KnoxConfigurationType;",
            ">;"
        }
    .end annotation
.end field

.field private static final DEBUG:Z = false

.field private static final MIN_INVALID_PASSWORD_LEN:I = 0x11

.field private static final TAG:Ljava/lang/String; = "KnoxConfigurationType"


# instance fields
.field protected mAdminUid:I

.field private mAllowChangeDataSettings:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;>;"
        }
    .end annotation
.end field

.field protected mAllowMultiwindowMode:Z

.field protected mAllowTaskManager:Z

.field protected mAppInstallationList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected mAppRemoveList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected mBiometricAuthValue:I

.field protected mCustomBadgeIcon:Ljava/lang/String;

.field protected mCustomHomeScreenWallpaper:Ljava/lang/String;

.field protected mCustomLockScreenWallpaper:Ljava/lang/String;

.field protected mCustomStatusIcon:Ljava/lang/String;

.field protected mCustomStatusLabel:Ljava/lang/String;

.field protected mFOTADisableAppList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected mFOTAReenableAppList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected mForbiddenStrings:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected mGoogleAppsList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected mIsBiometricAuthEnabled:Z

.field protected mManagedType:Z

.field protected mMaximumCharacterOccurences:I

.field protected mMaximumCharacterSequenceLength:I

.field protected mMaximumFailedPasswordsForWipe:I

.field protected mMaximumNumericSequenceLength:I

.field protected mMaximumTimeToLock:I

.field protected mMultifactorAuthEnabled:Z

.field protected mName:Ljava/lang/String;

.field protected mPasswordMinimumLength:I

.field protected mPasswordMinimumLetters:I

.field protected mPasswordMinimumLowerCase:I

.field protected mPasswordMinimumNonLetters:I

.field protected mPasswordMinimumNumeric:I

.field protected mPasswordMinimumSymbols:I

.field protected mPasswordMinimumUpperCase:I

.field protected mPasswordPattern:Ljava/lang/String;

.field protected mPasswordQuality:I

.field protected mPersonaList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field protected mProtectedList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mRCPDataSettings:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private mRCPNotifSettings:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;>;"
        }
    .end annotation
.end field

.field protected mSimplePasswordEnabled:Z

.field protected mUserId:I

.field protected mVersion:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1571
    new-instance v0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType$1;

    invoke-direct {v0}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType$1;-><init>()V

    sput-object v0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1586
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput v1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mAdminUid:I

    .line 60
    iput v4, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mUserId:I

    .line 61
    const-string v0, "custom"

    iput-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mVersion:Ljava/lang/String;

    .line 62
    iput v1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPasswordMinimumNonLetters:I

    .line 63
    iput v1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPasswordMinimumLetters:I

    .line 64
    iput v1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPasswordMinimumNumeric:I

    .line 65
    iput v1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPasswordMinimumUpperCase:I

    .line 66
    iput v1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPasswordMinimumLowerCase:I

    .line 67
    iput v1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPasswordMinimumSymbols:I

    .line 68
    iput v1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPasswordQuality:I

    .line 69
    iput v1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mMaximumFailedPasswordsForWipe:I

    .line 70
    iput v1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mMaximumCharacterOccurences:I

    .line 71
    iput v1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mMaximumCharacterSequenceLength:I

    .line 72
    iput v1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mMaximumNumericSequenceLength:I

    .line 73
    iput v1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPasswordMinimumLength:I

    .line 74
    iput v1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mMaximumTimeToLock:I

    .line 75
    iput-object v2, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPasswordPattern:Ljava/lang/String;

    .line 76
    iput-object v2, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mName:Ljava/lang/String;

    .line 77
    iput-object v2, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mCustomBadgeIcon:Ljava/lang/String;

    .line 78
    iput-object v2, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mCustomHomeScreenWallpaper:Ljava/lang/String;

    .line 79
    iput-object v2, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mCustomLockScreenWallpaper:Ljava/lang/String;

    .line 80
    iput-object v2, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mCustomStatusLabel:Ljava/lang/String;

    .line 81
    iput-object v2, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mCustomStatusIcon:Ljava/lang/String;

    .line 82
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPersonaList:Ljava/util/List;

    .line 83
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mAppInstallationList:Ljava/util/List;

    .line 90
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mAppRemoveList:Ljava/util/List;

    .line 97
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mFOTADisableAppList:Ljava/util/List;

    .line 104
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mFOTAReenableAppList:Ljava/util/List;

    .line 105
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mForbiddenStrings:Ljava/util/List;

    .line 106
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mProtectedList:Ljava/util/List;

    .line 107
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mGoogleAppsList:Ljava/util/List;

    .line 109
    iput-boolean v1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mManagedType:Z

    .line 110
    iput-boolean v3, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mSimplePasswordEnabled:Z

    .line 111
    iput-boolean v1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mMultifactorAuthEnabled:Z

    .line 113
    iput-boolean v3, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mAllowMultiwindowMode:Z

    .line 114
    iput-boolean v3, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mAllowTaskManager:Z

    .line 119
    iput-boolean v1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mIsBiometricAuthEnabled:Z

    .line 124
    iput v4, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mBiometricAuthValue:I

    .line 126
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mRCPDataSettings:Ljava/util/HashMap;

    .line 129
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mAllowChangeDataSettings:Ljava/util/HashMap;

    .line 132
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mRCPNotifSettings:Ljava/util/HashMap;

    .line 1588
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 6
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v5, -0x1

    const/4 v4, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1591
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput v3, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mAdminUid:I

    .line 60
    iput v5, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mUserId:I

    .line 61
    const-string v1, "custom"

    iput-object v1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mVersion:Ljava/lang/String;

    .line 62
    iput v3, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPasswordMinimumNonLetters:I

    .line 63
    iput v3, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPasswordMinimumLetters:I

    .line 64
    iput v3, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPasswordMinimumNumeric:I

    .line 65
    iput v3, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPasswordMinimumUpperCase:I

    .line 66
    iput v3, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPasswordMinimumLowerCase:I

    .line 67
    iput v3, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPasswordMinimumSymbols:I

    .line 68
    iput v3, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPasswordQuality:I

    .line 69
    iput v3, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mMaximumFailedPasswordsForWipe:I

    .line 70
    iput v3, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mMaximumCharacterOccurences:I

    .line 71
    iput v3, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mMaximumCharacterSequenceLength:I

    .line 72
    iput v3, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mMaximumNumericSequenceLength:I

    .line 73
    iput v3, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPasswordMinimumLength:I

    .line 74
    iput v3, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mMaximumTimeToLock:I

    .line 75
    iput-object v4, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPasswordPattern:Ljava/lang/String;

    .line 76
    iput-object v4, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mName:Ljava/lang/String;

    .line 77
    iput-object v4, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mCustomBadgeIcon:Ljava/lang/String;

    .line 78
    iput-object v4, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mCustomHomeScreenWallpaper:Ljava/lang/String;

    .line 79
    iput-object v4, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mCustomLockScreenWallpaper:Ljava/lang/String;

    .line 80
    iput-object v4, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mCustomStatusLabel:Ljava/lang/String;

    .line 81
    iput-object v4, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mCustomStatusIcon:Ljava/lang/String;

    .line 82
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPersonaList:Ljava/util/List;

    .line 83
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mAppInstallationList:Ljava/util/List;

    .line 90
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mAppRemoveList:Ljava/util/List;

    .line 97
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mFOTADisableAppList:Ljava/util/List;

    .line 104
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mFOTAReenableAppList:Ljava/util/List;

    .line 105
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mForbiddenStrings:Ljava/util/List;

    .line 106
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mProtectedList:Ljava/util/List;

    .line 107
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mGoogleAppsList:Ljava/util/List;

    .line 109
    iput-boolean v3, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mManagedType:Z

    .line 110
    iput-boolean v2, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mSimplePasswordEnabled:Z

    .line 111
    iput-boolean v3, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mMultifactorAuthEnabled:Z

    .line 113
    iput-boolean v2, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mAllowMultiwindowMode:Z

    .line 114
    iput-boolean v2, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mAllowTaskManager:Z

    .line 119
    iput-boolean v3, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mIsBiometricAuthEnabled:Z

    .line 124
    iput v5, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mBiometricAuthValue:I

    .line 126
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mRCPDataSettings:Ljava/util/HashMap;

    .line 129
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mAllowChangeDataSettings:Ljava/util/HashMap;

    .line 132
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mRCPNotifSettings:Ljava/util/HashMap;

    .line 1592
    const/4 v0, 0x0

    .line 1593
    .local v0, "value":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mName:Ljava/lang/String;

    .line 1594
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 1595
    if-eqz v0, :cond_c

    .line 1596
    iput-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mVersion:Ljava/lang/String;

    .line 1600
    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPasswordMinimumNonLetters:I

    .line 1601
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPasswordMinimumLetters:I

    .line 1602
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPasswordMinimumNumeric:I

    .line 1603
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPasswordMinimumUpperCase:I

    .line 1604
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPasswordMinimumLowerCase:I

    .line 1605
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPasswordMinimumSymbols:I

    .line 1606
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPasswordQuality:I

    .line 1607
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mMaximumTimeToLock:I

    .line 1608
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mMaximumFailedPasswordsForWipe:I

    .line 1609
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v2, :cond_d

    move v1, v2

    :goto_1
    iput-boolean v1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mManagedType:Z

    .line 1610
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 1611
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1612
    :cond_0
    const/4 v0, 0x0

    .line 1614
    :cond_1
    iput-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mCustomBadgeIcon:Ljava/lang/String;

    .line 1615
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 1616
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1617
    :cond_2
    const/4 v0, 0x0

    .line 1619
    :cond_3
    iput-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mCustomHomeScreenWallpaper:Ljava/lang/String;

    .line 1620
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 1621
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1622
    :cond_4
    const/4 v0, 0x0

    .line 1624
    :cond_5
    iput-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mCustomLockScreenWallpaper:Ljava/lang/String;

    .line 1625
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 1626
    if-eqz v0, :cond_6

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1627
    :cond_6
    const/4 v0, 0x0

    .line 1629
    :cond_7
    iput-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mCustomStatusLabel:Ljava/lang/String;

    .line 1630
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 1631
    if-eqz v0, :cond_8

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1632
    :cond_8
    const/4 v0, 0x0

    .line 1634
    :cond_9
    iput-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mCustomStatusIcon:Ljava/lang/String;

    .line 1635
    iget-object v1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mAppInstallationList:Ljava/util/List;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readStringList(Ljava/util/List;)V

    .line 1636
    iget-object v1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mForbiddenStrings:Ljava/util/List;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readStringList(Ljava/util/List;)V

    .line 1637
    iget-object v1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mProtectedList:Ljava/util/List;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readStringList(Ljava/util/List;)V

    .line 1638
    iget-object v1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mGoogleAppsList:Ljava/util/List;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readStringList(Ljava/util/List;)V

    .line 1639
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mMaximumCharacterOccurences:I

    .line 1640
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mMaximumCharacterSequenceLength:I

    .line 1641
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mMaximumNumericSequenceLength:I

    .line 1642
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPasswordMinimumLength:I

    .line 1643
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 1644
    if-eqz v0, :cond_a

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_b

    .line 1645
    :cond_a
    const/4 v0, 0x0

    .line 1647
    :cond_b
    iput-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPasswordPattern:Ljava/lang/String;

    .line 1648
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v2, :cond_e

    move v1, v2

    :goto_2
    iput-boolean v1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mSimplePasswordEnabled:Z

    .line 1649
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v2, :cond_f

    move v1, v2

    :goto_3
    iput-boolean v1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mMultifactorAuthEnabled:Z

    .line 1651
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v2, :cond_10

    move v1, v2

    :goto_4
    iput-boolean v1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mAllowMultiwindowMode:Z

    .line 1652
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v2, :cond_11

    move v1, v2

    :goto_5
    iput-boolean v1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mAllowTaskManager:Z

    .line 1653
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v2, :cond_12

    :goto_6
    iput-boolean v2, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mIsBiometricAuthEnabled:Z

    .line 1654
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mBiometricAuthValue:I

    .line 1657
    iget-object v1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mRCPDataSettings:Ljava/util/HashMap;

    invoke-direct {p0, p1, v1}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->deserializeRCPSettings(Landroid/os/Parcel;Ljava/util/HashMap;)V

    .line 1658
    iget-object v1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mAllowChangeDataSettings:Ljava/util/HashMap;

    invoke-direct {p0, p1, v1}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->deserializeRCPSettings(Landroid/os/Parcel;Ljava/util/HashMap;)V

    .line 1659
    iget-object v1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mRCPNotifSettings:Ljava/util/HashMap;

    invoke-direct {p0, p1, v1}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->deserializeRCPSettings(Landroid/os/Parcel;Ljava/util/HashMap;)V

    .line 1662
    return-void

    .line 1598
    :cond_c
    const-string v1, "custom"

    iput-object v1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mVersion:Ljava/lang/String;

    goto/16 :goto_0

    :cond_d
    move v1, v3

    .line 1609
    goto/16 :goto_1

    :cond_e
    move v1, v3

    .line 1648
    goto :goto_2

    :cond_f
    move v1, v3

    .line 1649
    goto :goto_3

    :cond_10
    move v1, v3

    .line 1651
    goto :goto_4

    :cond_11
    move v1, v3

    .line 1652
    goto :goto_5

    :cond_12
    move v2, v3

    .line 1653
    goto :goto_6
.end method

.method private deserializeRCPSettings(Landroid/os/Parcel;Ljava/util/HashMap;)V
    .locals 9
    .param p1, "in"    # Landroid/os/Parcel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Parcel;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;>;)V"
        }
    .end annotation

    .prologue
    .line 1666
    .local p2, "settings":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/List<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;>;"
    const/4 v8, 0x0

    .line 1667
    .local v8, "value":Ljava/lang/String;
    const/4 v4, 0x0

    .line 1668
    .local v4, "pair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v5, 0x0

    .line 1669
    .local v5, "pairList":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    const/4 v7, 0x0

    .line 1670
    .local v7, "propertyCount":I
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 1672
    .local v1, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_1

    .line 1673
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 1674
    .local v0, "app":Ljava/lang/String;
    new-instance v5, Ljava/util/ArrayList;

    .end local v5    # "pairList":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1675
    .restart local v5    # "pairList":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v7

    .line 1676
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_1
    if-ge v3, v7, :cond_0

    .line 1677
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 1678
    .local v6, "property":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v8

    .line 1679
    new-instance v4, Landroid/util/Pair;

    .end local v4    # "pair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-direct {v4, v6, v8}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1680
    .restart local v4    # "pair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1676
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 1682
    .end local v6    # "property":Ljava/lang/String;
    :cond_0
    invoke-virtual {p2, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1672
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1684
    .end local v0    # "app":Ljava/lang/String;
    .end local v3    # "j":I
    :cond_1
    return-void
.end method

.method private dumpRCPSettings(Ljava/util/HashMap;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;>;)V"
        }
    .end annotation

    .prologue
    .line 255
    .local p1, "settings":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/List<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;>;"
    invoke-virtual {p1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    .line 256
    .local v3, "keySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-eqz v3, :cond_1

    invoke-interface {v3}, Ljava/util/Set;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_1

    .line 257
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 258
    .local v2, "key":Ljava/lang/String;
    const-string v6, "KnoxConfigurationType"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " {"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 259
    invoke-virtual {p1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/List;

    .line 260
    .local v5, "pairList":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    if-eqz v5, :cond_0

    .line 261
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/util/Pair;

    .line 262
    .local v4, "pair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v7, "KnoxConfigurationType"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "  ( "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v6, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ","

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v6, v4, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " )"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v7, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 265
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v4    # "pair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_0
    const-string v6, "KnoxConfigurationType"

    const-string v7, " }"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 268
    .end local v2    # "key":Ljava/lang/String;
    .end local v5    # "pairList":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    :cond_1
    return-void
.end method

.method private getListFromSyncPolicy(Ljava/lang/String;Ljava/lang/String;Ljava/util/HashMap;)Ljava/util/List;
    .locals 9
    .param p1, "property"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;>;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1033
    .local p3, "settings":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/List<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;>;"
    if-eqz p3, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 1035
    :cond_0
    const/4 v4, 0x0

    .line 1055
    :cond_1
    return-object v4

    .line 1037
    :cond_2
    const/4 v4, 0x0

    .line 1038
    .local v4, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p3}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    .line 1039
    .local v3, "keySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-eqz v3, :cond_1

    .line 1040
    new-instance v7, Landroid/util/Pair;

    invoke-direct {v7, p1, p2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1041
    .local v7, "searchPair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1042
    .local v2, "key":Ljava/lang/String;
    invoke-virtual {p3, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/List;

    .line 1043
    .local v6, "pairList":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    if-eqz v6, :cond_3

    .line 1044
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_4
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/util/Pair;

    .line 1045
    .local v5, "pair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {v7, v5}, Landroid/util/Pair;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 1046
    if-nez v4, :cond_5

    .line 1047
    new-instance v4, Ljava/util/ArrayList;

    .end local v4    # "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1049
    .restart local v4    # "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_5
    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private getRCPSyncPolicy(Ljava/lang/String;Ljava/lang/String;Ljava/util/HashMap;)Ljava/lang/String;
    .locals 5
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "property"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;>;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .local p3, "settings":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/List<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;>;"
    const/4 v4, 0x0

    .line 1016
    if-eqz p3, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    move-object v3, v4

    .line 1028
    :goto_0
    return-object v3

    .line 1020
    :cond_1
    invoke-virtual {p3, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 1021
    .local v2, "pairList":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    if-eqz v2, :cond_3

    .line 1022
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/Pair;

    .line 1023
    .local v1, "pair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v3, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1024
    iget-object v3, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    goto :goto_0

    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "pair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_3
    move-object v3, v4

    .line 1028
    goto :goto_0
.end method

.method private serializeRCPSettings(Landroid/os/Parcel;Ljava/util/HashMap;)V
    .locals 8
    .param p1, "dest"    # Landroid/os/Parcel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Parcel;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;>;)V"
        }
    .end annotation

    .prologue
    .local p2, "settings":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/List<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;>;"
    const/4 v7, 0x0

    .line 1769
    invoke-virtual {p2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    .line 1770
    .local v3, "keySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-eqz v3, :cond_2

    .line 1771
    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v6

    invoke-virtual {p1, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 1772
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1773
    .local v0, "app":Ljava/lang/String;
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1774
    invoke-virtual {p2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/List;

    .line 1775
    .local v5, "pairList":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    if-eqz v5, :cond_1

    .line 1776
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v6

    invoke-virtual {p1, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 1777
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/util/Pair;

    .line 1778
    .local v4, "pair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v6, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v6, Ljava/lang/String;

    invoke-virtual {p1, v6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1779
    iget-object v6, v4, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v6, Ljava/lang/String;

    invoke-virtual {p1, v6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_1

    .line 1782
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v4    # "pair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_1
    invoke-virtual {p1, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 1786
    .end local v0    # "app":Ljava/lang/String;
    .end local v5    # "pairList":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    :cond_2
    invoke-virtual {p1, v7}, Landroid/os/Parcel;->writeInt(I)V

    .line 1788
    :cond_3
    return-void
.end method

.method private setRCPSyncPolicy(Ljava/util/HashMap;Ljava/util/HashMap;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;>;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;>;)V"
        }
    .end annotation

    .prologue
    .line 949
    .local p1, "rcpSettings":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/List<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;>;"
    .local p2, "settings":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/List<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;>;"
    if-nez p2, :cond_1

    .line 973
    :cond_0
    return-void

    .line 952
    :cond_1
    invoke-virtual {p2}, Ljava/util/HashMap;->clear()V

    .line 955
    if-eqz p1, :cond_0

    .line 956
    const/4 v4, 0x0

    .line 957
    .local v4, "newPair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v5, 0x0

    .line 958
    .local v5, "newPairList":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    invoke-virtual {p1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    .line 959
    .local v3, "keySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-eqz v3, :cond_0

    .line 960
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 961
    .local v2, "key":Ljava/lang/String;
    invoke-virtual {p1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/List;

    .line 962
    .local v7, "pairList":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    if-eqz v7, :cond_2

    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_2

    .line 963
    new-instance v5, Ljava/util/ArrayList;

    .end local v5    # "newPairList":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 964
    .restart local v5    # "newPairList":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/util/Pair;

    .line 965
    .local v6, "pair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v4, Landroid/util/Pair;

    .end local v4    # "newPair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v8, v6, Landroid/util/Pair;->first:Ljava/lang/Object;

    iget-object v9, v6, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-direct {v4, v8, v9}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 966
    .restart local v4    # "newPair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 968
    .end local v6    # "pair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_3
    invoke-virtual {p2, v2, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private setRCPSyncPolicy(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/util/HashMap;)V
    .locals 11
    .param p2, "property"    # Ljava/lang/String;
    .param p3, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;>;)V"
        }
    .end annotation

    .prologue
    .line 977
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p4, "settings":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/List<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;>;"
    if-eqz p4, :cond_0

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_0

    if-eqz p3, :cond_0

    invoke-virtual {p3}, Ljava/lang/String;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 1012
    :cond_0
    return-void

    .line 982
    :cond_1
    const/4 v7, 0x1

    .line 983
    .local v7, "update":Z
    const/4 v6, 0x0

    .line 984
    .local v6, "pairToRemove":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v5, 0x0

    .line 985
    .local v5, "pairList":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    new-instance v3, Landroid/util/Pair;

    invoke-direct {v3, p2, p3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 986
    .local v3, "newPair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 987
    .local v2, "item":Ljava/lang/String;
    invoke-virtual {p4, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    .end local v5    # "pairList":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    check-cast v5, Ljava/util/List;

    .line 988
    .restart local v5    # "pairList":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    if-nez v5, :cond_3

    .line 989
    new-instance v5, Ljava/util/ArrayList;

    .end local v5    # "pairList":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1005
    .restart local v5    # "pairList":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;>;"
    :cond_2
    :goto_1
    if-eqz v7, :cond_7

    .line 1006
    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1007
    invoke-virtual {p4, v2, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 991
    :cond_3
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_4
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/util/Pair;

    .line 992
    .local v4, "pair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {v4, v3}, Landroid/util/Pair;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 993
    const-string v9, "KnoxConfigurationType"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Ignoring the duplicate entry: "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v8, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v8, Ljava/lang/String;

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v10, " "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v8, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v8, Ljava/lang/String;

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v9, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 994
    const/4 v7, 0x0

    goto :goto_2

    .line 995
    :cond_5
    iget-object v8, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v8, Ljava/lang/String;

    invoke-virtual {v8, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 996
    const-string v9, "KnoxConfigurationType"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "property found, remove and add it again: "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v8, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v8, Ljava/lang/String;

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v10, " "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v8, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v8, Ljava/lang/String;

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v9, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 997
    move-object v6, v4

    goto :goto_2

    .line 1000
    .end local v4    # "pair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_6
    if-eqz v6, :cond_2

    .line 1001
    invoke-interface {v5, v6}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1002
    const/4 v6, 0x0

    goto/16 :goto_1

    .line 1009
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_7
    const/4 v7, 0x1

    goto/16 :goto_0
.end method


# virtual methods
.method public addPersonaId(I)V
    .locals 2
    .param p1, "personaId"    # I

    .prologue
    .line 322
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPersonaList:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 323
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPersonaList:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 325
    :cond_0
    return-void
.end method

.method public allowMultiwindowMode(Z)V
    .locals 0
    .param p1, "allow"    # Z

    .prologue
    .line 1502
    iput-boolean p1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mAllowMultiwindowMode:Z

    .line 1503
    return-void
.end method

.method public allowTaskManager(Z)V
    .locals 0
    .param p1, "allow"    # Z

    .prologue
    .line 1522
    iput-boolean p1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mAllowTaskManager:Z

    .line 1523
    return-void
.end method

.method public clone(Ljava/lang/String;)Lcom/sec/enterprise/knox/container/KnoxConfigurationType;
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 157
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 158
    :cond_0
    const-string v1, "KnoxConfigurationType"

    const-string v2, "clone(): name is either null or empty, hence returning null"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    const/4 v0, 0x0

    .line 163
    :goto_0
    return-object v0

    .line 161
    :cond_1
    new-instance v0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    invoke-direct {v0}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;-><init>()V

    .line 162
    .local v0, "type":Lcom/sec/enterprise/knox/container/KnoxConfigurationType;
    invoke-virtual {p0, v0, p1}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->cloneConfiguration(Lcom/sec/enterprise/knox/container/KnoxConfigurationType;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected cloneConfiguration(Lcom/sec/enterprise/knox/container/KnoxConfigurationType;Ljava/lang/String;)V
    .locals 2
    .param p1, "type"    # Lcom/sec/enterprise/knox/container/KnoxConfigurationType;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 167
    invoke-virtual {p1, p2}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->setName(Ljava/lang/String;)V

    .line 168
    iget v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPasswordMinimumNonLetters:I

    invoke-virtual {p1, v0}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->setPasswordMinimumNonLetters(I)V

    .line 169
    iget v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPasswordMinimumLetters:I

    invoke-virtual {p1, v0}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->setPasswordMinimumLetters(I)V

    .line 170
    iget v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPasswordMinimumNumeric:I

    invoke-virtual {p1, v0}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->setPasswordMinimumNumeric(I)V

    .line 171
    iget v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPasswordMinimumUpperCase:I

    invoke-virtual {p1, v0}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->setPasswordMinimumUpperCase(I)V

    .line 172
    iget v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPasswordMinimumLowerCase:I

    invoke-virtual {p1, v0}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->setPasswordMinimumLowerCase(I)V

    .line 173
    iget v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPasswordMinimumSymbols:I

    invoke-virtual {p1, v0}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->setPasswordMinimumSymbols(I)V

    .line 174
    iget v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPasswordQuality:I

    invoke-virtual {p1, v0}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->setPasswordQuality(I)V

    .line 175
    iget v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mMaximumFailedPasswordsForWipe:I

    invoke-virtual {p1, v0}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->setMaximumFailedPasswordsForWipe(I)V

    .line 176
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mManagedType:Z

    invoke-virtual {p1, v0}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->setManagedType(Z)V

    .line 177
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mCustomBadgeIcon:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->setCustomBadgeIcon(Ljava/lang/String;)V

    .line 178
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mCustomHomeScreenWallpaper:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->setCustomHomeScreenWallpaper(Ljava/lang/String;)V

    .line 179
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mCustomLockScreenWallpaper:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->setCustomLockScreenWallpaper(Ljava/lang/String;)V

    .line 180
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mCustomStatusLabel:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->setCustomStatusLabel(Ljava/lang/String;)V

    .line 181
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mCustomStatusIcon:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->setCustomStatusIcon(Ljava/lang/String;)V

    .line 182
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mAppInstallationList:Ljava/util/List;

    invoke-virtual {p1, v0}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->setAppInstallationList(Ljava/util/List;)V

    .line 183
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mAppRemoveList:Ljava/util/List;

    invoke-virtual {p1, v0}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->setAppRemoveList(Ljava/util/List;)V

    .line 184
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mFOTADisableAppList:Ljava/util/List;

    invoke-virtual {p1, v0}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->setFOTADisableList(Ljava/util/List;)V

    .line 185
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mFOTAReenableAppList:Ljava/util/List;

    invoke-virtual {p1, v0}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->setFOTAReenableList(Ljava/util/List;)V

    .line 186
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mForbiddenStrings:Ljava/util/List;

    invoke-virtual {p1, v0}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->setForbiddenStrings(Ljava/util/List;)V

    .line 187
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mProtectedList:Ljava/util/List;

    invoke-virtual {p1, v0}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->setProtectedPackageList(Ljava/util/List;)V

    .line 188
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mGoogleAppsList:Ljava/util/List;

    invoke-virtual {p1, v0}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->setGoogleAppsList(Ljava/util/List;)V

    .line 189
    iget v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mMaximumCharacterOccurences:I

    invoke-virtual {p1, v0}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->setMaximumCharacterOccurences(I)V

    .line 190
    iget v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mMaximumCharacterSequenceLength:I

    invoke-virtual {p1, v0}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->setMaximumCharacterSequenceLength(I)V

    .line 191
    iget v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mMaximumNumericSequenceLength:I

    invoke-virtual {p1, v0}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->setMaximumNumericSequenceLength(I)V

    .line 192
    iget v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPasswordMinimumLength:I

    invoke-virtual {p1, v0}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->setPasswordMinimumLength(I)V

    .line 193
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mSimplePasswordEnabled:Z

    invoke-virtual {p1, v0}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->setSimplePasswordEnabled(Z)V

    .line 194
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mMultifactorAuthEnabled:Z

    invoke-virtual {p1, v0}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->enforceMultifactorAuthentication(Z)V

    .line 195
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPasswordPattern:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->setRequiredPasswordPattern(Ljava/lang/String;)V

    .line 196
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mRCPDataSettings:Ljava/util/HashMap;

    iget-object v1, p1, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mRCPDataSettings:Ljava/util/HashMap;

    invoke-direct {p1, v0, v1}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->setRCPSyncPolicy(Ljava/util/HashMap;Ljava/util/HashMap;)V

    .line 197
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mAllowChangeDataSettings:Ljava/util/HashMap;

    iget-object v1, p1, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mAllowChangeDataSettings:Ljava/util/HashMap;

    invoke-direct {p1, v0, v1}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->setRCPSyncPolicy(Ljava/util/HashMap;Ljava/util/HashMap;)V

    .line 198
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mRCPNotifSettings:Ljava/util/HashMap;

    iget-object v1, p1, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mRCPNotifSettings:Ljava/util/HashMap;

    invoke-direct {p1, v0, v1}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->setRCPSyncPolicy(Ljava/util/HashMap;Ljava/util/HashMap;)V

    .line 199
    iget v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mMaximumTimeToLock:I

    invoke-virtual {p1, v0}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->setMaximumTimeToLock(I)V

    .line 200
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mVersion:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->setVersion(Ljava/lang/String;)V

    .line 201
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mAllowMultiwindowMode:Z

    invoke-virtual {p1, v0}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->allowMultiwindowMode(Z)V

    .line 202
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mAllowTaskManager:Z

    invoke-virtual {p1, v0}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->allowTaskManager(Z)V

    .line 203
    iget v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mBiometricAuthValue:I

    iget-boolean v1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mIsBiometricAuthEnabled:Z

    invoke-virtual {p1, v0, v1}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->setBiometricAuthenticationEnabled(IZ)V

    .line 205
    return-void
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 1689
    const/4 v0, 0x0

    return v0
.end method

.method public dumpState()V
    .locals 0

    .prologue
    .line 252
    return-void
.end method

.method public enforceMultifactorAuthentication(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 867
    iput-boolean p1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mMultifactorAuthEnabled:Z

    .line 868
    return-void
.end method

.method public getAdminUid()I
    .locals 1

    .prologue
    .line 292
    iget v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mAdminUid:I

    return v0
.end method

.method public getAirCommandEnabled()Z
    .locals 1

    .prologue
    .line 1424
    const/4 v0, 0x1

    return v0
.end method

.method public getAllowAllShare()Z
    .locals 1

    .prologue
    .line 1373
    const/4 v0, 0x0

    return v0
.end method

.method public getAllowChangeDataSyncPolicy()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;>;"
        }
    .end annotation

    .prologue
    .line 1094
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mAllowChangeDataSettings:Ljava/util/HashMap;

    return-object v0
.end method

.method public getAllowChangeDataSyncPolicy(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p1, "appName"    # Ljava/lang/String;
    .param p2, "property"    # Ljava/lang/String;

    .prologue
    .line 1124
    iget-object v1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mAllowChangeDataSettings:Ljava/util/HashMap;

    invoke-direct {p0, p1, p2, v1}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->getRCPSyncPolicy(Ljava/lang/String;Ljava/lang/String;Ljava/util/HashMap;)Ljava/lang/String;

    move-result-object v0

    .line 1125
    .local v0, "value":Ljava/lang/String;
    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v1

    return v1
.end method

.method public getAllowContainerReset()Z
    .locals 1

    .prologue
    .line 1288
    const/4 v0, 0x0

    return v0
.end method

.method public getAllowCustomBadgeIcon()Z
    .locals 1

    .prologue
    .line 1220
    const/4 v0, 0x1

    return v0
.end method

.method public getAllowCustomColorIdentification()Z
    .locals 1

    .prologue
    .line 1271
    const/4 v0, 0x1

    return v0
.end method

.method public getAllowCustomPersonaIcon()Z
    .locals 1

    .prologue
    .line 1254
    const/4 v0, 0x1

    return v0
.end method

.method public getAllowDLNADataTransfer()Z
    .locals 1

    .prologue
    .line 1322
    const/4 v0, 0x0

    return v0
.end method

.method public getAllowExportAndDeleteFiles()Z
    .locals 1

    .prologue
    .line 1492
    const/4 v0, 0x0

    return v0
.end method

.method public getAllowExportFiles()Z
    .locals 1

    .prologue
    .line 1475
    const/4 v0, 0x0

    return v0
.end method

.method public getAllowImportFiles()Z
    .locals 1

    .prologue
    .line 1458
    const/4 v0, 0x1

    return v0
.end method

.method public getAllowPrint()Z
    .locals 1

    .prologue
    .line 1339
    const/4 v0, 0x0

    return v0
.end method

.method public getAllowShortCutCreation()Z
    .locals 1

    .prologue
    .line 1305
    const/4 v0, 0x1

    return v0
.end method

.method public getAllowUniversalCallerId()Z
    .locals 1

    .prologue
    .line 1441
    const/4 v0, 0x1

    return v0
.end method

.method public getAppInstallationList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 630
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mAppInstallationList:Ljava/util/List;

    return-object v0
.end method

.method public getAppRemoveList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 653
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mAppRemoveList:Ljava/util/List;

    return-object v0
.end method

.method public getBiometricAuthenticationEnabledType()I
    .locals 1

    .prologue
    .line 877
    iget v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mBiometricAuthValue:I

    return v0
.end method

.method public getBiometricAuthenticationEnabledValue()Z
    .locals 1

    .prologue
    .line 887
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mIsBiometricAuthEnabled:Z

    return v0
.end method

.method public getCameraModeChangeEnabled()Z
    .locals 1

    .prologue
    .line 1203
    const/4 v0, 0x0

    return v0
.end method

.method public getCustomBadgeIcon()Ljava/lang/String;
    .locals 1

    .prologue
    .line 536
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mCustomBadgeIcon:Ljava/lang/String;

    return-object v0
.end method

.method public getCustomHomeScreenWallpaper()Ljava/lang/String;
    .locals 1

    .prologue
    .line 554
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mCustomHomeScreenWallpaper:Ljava/lang/String;

    return-object v0
.end method

.method public getCustomLockScreenWallpaper()Ljava/lang/String;
    .locals 1

    .prologue
    .line 573
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mCustomLockScreenWallpaper:Ljava/lang/String;

    return-object v0
.end method

.method public getCustomStatusIcon()Ljava/lang/String;
    .locals 1

    .prologue
    .line 611
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mCustomStatusIcon:Ljava/lang/String;

    return-object v0
.end method

.method public getCustomStatusLabel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 592
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mCustomStatusLabel:Ljava/lang/String;

    return-object v0
.end method

.method public getDataSyncPolicy(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "appName"    # Ljava/lang/String;
    .param p2, "property"    # Ljava/lang/String;

    .prologue
    .line 1089
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mRCPDataSettings:Ljava/util/HashMap;

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->getRCPSyncPolicy(Ljava/lang/String;Ljava/lang/String;Ljava/util/HashMap;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDataSyncPolicy()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;>;"
        }
    .end annotation

    .prologue
    .line 1062
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mRCPDataSettings:Ljava/util/HashMap;

    return-object v0
.end method

.method public getDisableSwitchWidgetOnLockScreen()Z
    .locals 1

    .prologue
    .line 1237
    const/4 v0, 0x0

    return v0
.end method

.method public getFOTADisableList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 689
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mFOTADisableAppList:Ljava/util/List;

    return-object v0
.end method

.method public getFOTAReenableList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 714
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mFOTAReenableAppList:Ljava/util/List;

    return-object v0
.end method

.method public getForbiddenStrings()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 724
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mForbiddenStrings:Ljava/util/List;

    return-object v0
.end method

.method public getGearSupportEnabled()Z
    .locals 1

    .prologue
    .line 1390
    const/4 v0, 0x1

    return v0
.end method

.method public getGoogleAppsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1557
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mGoogleAppsList:Ljava/util/List;

    return-object v0
.end method

.method public getListFromAllowChangeDataSyncPolicy(Ljava/lang/String;Z)Ljava/util/List;
    .locals 2
    .param p1, "property"    # Ljava/lang/String;
    .param p2, "value"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1114
    invoke-static {p2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v0

    .line 1115
    .local v0, "strVal":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mAllowChangeDataSettings:Ljava/util/HashMap;

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->getListFromSyncPolicy(Ljava/lang/String;Ljava/lang/String;Ljava/util/HashMap;)Ljava/util/List;

    move-result-object v1

    return-object v1
.end method

.method public getListFromDataSyncPolicy(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .param p1, "property"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1080
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mRCPDataSettings:Ljava/util/HashMap;

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->getListFromSyncPolicy(Ljava/lang/String;Ljava/lang/String;Ljava/util/HashMap;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getManagedType()Z
    .locals 1

    .prologue
    .line 1177
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mManagedType:Z

    return v0
.end method

.method public getMaximumCharacterOccurences()I
    .locals 1

    .prologue
    .line 763
    iget v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mMaximumCharacterOccurences:I

    return v0
.end method

.method public getMaximumCharacterSequenceLength()I
    .locals 1

    .prologue
    .line 782
    iget v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mMaximumCharacterSequenceLength:I

    return v0
.end method

.method public getMaximumFailedPasswordsForWipe()I
    .locals 1

    .prologue
    .line 496
    iget v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mMaximumFailedPasswordsForWipe:I

    return v0
.end method

.method public getMaximumNumericSequenceLength()I
    .locals 1

    .prologue
    .line 802
    iget v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mMaximumNumericSequenceLength:I

    return v0
.end method

.method public getMaximumTimeToLock()I
    .locals 1

    .prologue
    .line 515
    iget v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mMaximumTimeToLock:I

    return v0
.end method

.method public getModifyLockScreenTimeout()Z
    .locals 1

    .prologue
    .line 1356
    const/4 v0, 0x1

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 276
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getNotificationSyncPolicy(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "appName"    # Ljava/lang/String;
    .param p2, "property"    # Ljava/lang/String;

    .prologue
    .line 1159
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mRCPNotifSettings:Ljava/util/HashMap;

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->getRCPSyncPolicy(Ljava/lang/String;Ljava/lang/String;Ljava/util/HashMap;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNotificationSyncPolicy()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;>;"
        }
    .end annotation

    .prologue
    .line 1132
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mRCPNotifSettings:Ljava/util/HashMap;

    return-object v0
.end method

.method public getPackagesFromNotificationSyncPolicy(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .param p1, "property"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1150
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mRCPNotifSettings:Ljava/util/HashMap;

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->getListFromSyncPolicy(Ljava/lang/String;Ljava/lang/String;Ljava/util/HashMap;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getPasswordMinimumLength()I
    .locals 1

    .prologue
    .line 831
    iget v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPasswordMinimumLength:I

    return v0
.end method

.method public getPasswordMinimumLetters()I
    .locals 1

    .prologue
    .line 371
    iget v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPasswordMinimumLetters:I

    return v0
.end method

.method public getPasswordMinimumLowerCase()I
    .locals 1

    .prologue
    .line 434
    iget v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPasswordMinimumLowerCase:I

    return v0
.end method

.method public getPasswordMinimumNonLetters()I
    .locals 1

    .prologue
    .line 350
    iget v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPasswordMinimumNonLetters:I

    return v0
.end method

.method public getPasswordMinimumNumeric()I
    .locals 1

    .prologue
    .line 392
    iget v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPasswordMinimumNumeric:I

    return v0
.end method

.method public getPasswordMinimumSymbols()I
    .locals 1

    .prologue
    .line 455
    iget v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPasswordMinimumSymbols:I

    return v0
.end method

.method public getPasswordMinimumUpperCase()I
    .locals 1

    .prologue
    .line 413
    iget v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPasswordMinimumUpperCase:I

    return v0
.end method

.method public getPasswordQuality()I
    .locals 1

    .prologue
    .line 476
    iget v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPasswordQuality:I

    return v0
.end method

.method public getPenWindowEnabled()Z
    .locals 1

    .prologue
    .line 1407
    const/4 v0, 0x1

    return v0
.end method

.method public getPersonaList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 312
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPersonaList:Ljava/util/List;

    return-object v0
.end method

.method public getProtectedPackageList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1550
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mProtectedList:Ljava/util/List;

    return-object v0
.end method

.method public getRequiredPwdPatternRestrictions()Ljava/lang/String;
    .locals 1

    .prologue
    .line 745
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPasswordPattern:Ljava/lang/String;

    return-object v0
.end method

.method public getSimplePasswordEnabled()Z
    .locals 1

    .prologue
    .line 840
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mSimplePasswordEnabled:Z

    return v0
.end method

.method public getUserId()I
    .locals 1

    .prologue
    .line 302
    iget v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mUserId:I

    return v0
.end method

.method public getVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 340
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mVersion:Ljava/lang/String;

    return-object v0
.end method

.method public isBiometricAuthenticationEnabled(I)Z
    .locals 4
    .param p1, "bioAuth"    # I

    .prologue
    const/4 v1, 0x0

    .line 897
    const/4 v0, 0x0

    .line 899
    .local v0, "hasValue":Z
    iget v2, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mBiometricAuthValue:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 900
    iget v2, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mBiometricAuthValue:I

    and-int/2addr v2, p1

    if-eq v2, p1, :cond_1

    .line 911
    :cond_0
    :goto_0
    return v1

    .line 903
    :cond_1
    const/4 v0, 0x1

    .line 905
    if-eqz v0, :cond_0

    .line 906
    const-string v1, "KnoxConfigurationType"

    const-string v2, "isBiometricAuthenticationEnabled: return true (hasValue)"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 907
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isMultifactorAuthenticationEnforced()Z
    .locals 1

    .prologue
    .line 858
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mMultifactorAuthEnabled:Z

    return v0
.end method

.method public isMultiwindowModeAllowed()Z
    .locals 1

    .prologue
    .line 1512
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mAllowMultiwindowMode:Z

    return v0
.end method

.method public isTaskManagerAllowed()Z
    .locals 1

    .prologue
    .line 1532
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mAllowTaskManager:Z

    return v0
.end method

.method public isUserManaged()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1186
    iget-boolean v1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mManagedType:Z

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public removePersonaId(I)V
    .locals 2
    .param p1, "personaId"    # I

    .prologue
    .line 329
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPersonaList:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 330
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPersonaList:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 332
    :cond_0
    return-void
.end method

.method public setAdminUid(I)V
    .locals 0
    .param p1, "uid"    # I

    .prologue
    .line 297
    iput p1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mAdminUid:I

    .line 298
    return-void
.end method

.method public setAirCommandEnabled(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 1416
    return-void
.end method

.method public setAllowAllShare(Z)V
    .locals 0
    .param p1, "allow"    # Z

    .prologue
    .line 1365
    return-void
.end method

.method public setAllowChangeDataSyncPolicy(Ljava/util/List;Ljava/lang/String;Z)V
    .locals 2
    .param p2, "property"    # Ljava/lang/String;
    .param p3, "value"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 1103
    .local p1, "applications":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v0

    .line 1104
    .local v0, "strVal":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mAllowChangeDataSettings:Ljava/util/HashMap;

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->setRCPSyncPolicy(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/util/HashMap;)V

    .line 1105
    return-void
.end method

.method public setAllowContainerReset(Z)V
    .locals 0
    .param p1, "allow"    # Z

    .prologue
    .line 1280
    return-void
.end method

.method public setAllowCustomBadgeIcon(Z)V
    .locals 0
    .param p1, "allow"    # Z

    .prologue
    .line 1212
    return-void
.end method

.method public setAllowCustomColorIdentification(Z)V
    .locals 0
    .param p1, "allow"    # Z

    .prologue
    .line 1263
    return-void
.end method

.method public setAllowCustomPersonaIcon(Z)V
    .locals 0
    .param p1, "allow"    # Z

    .prologue
    .line 1246
    return-void
.end method

.method public setAllowDLNADataTransfer(Z)V
    .locals 0
    .param p1, "allow"    # Z

    .prologue
    .line 1314
    return-void
.end method

.method public setAllowExportAndDeleteFiles(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 1484
    return-void
.end method

.method public setAllowExportFiles(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 1467
    return-void
.end method

.method public setAllowImportFiles(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 1450
    return-void
.end method

.method public setAllowPrint(Z)V
    .locals 0
    .param p1, "allow"    # Z

    .prologue
    .line 1331
    return-void
.end method

.method public setAllowShortCutCreation(Z)V
    .locals 0
    .param p1, "allow"    # Z

    .prologue
    .line 1297
    return-void
.end method

.method public setAllowUniversalCallerId(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 1433
    return-void
.end method

.method public setAppInstallationList(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 639
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mAppInstallationList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 640
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 641
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mAppInstallationList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 643
    :cond_0
    return-void
.end method

.method public setAppRemoveList(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 663
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mAppRemoveList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 664
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 665
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mAppRemoveList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 667
    :cond_0
    return-void
.end method

.method public setBiometricAuthenticationEnabled(IZ)V
    .locals 4
    .param p1, "bioAuth"    # I
    .param p2, "enable"    # Z

    .prologue
    .line 920
    if-gez p1, :cond_0

    .line 944
    :goto_0
    return-void

    .line 924
    :cond_0
    const/4 v0, 0x0

    .line 926
    .local v0, "value":I
    iget v1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mBiometricAuthValue:I

    if-lez v1, :cond_1

    .line 927
    iget v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mBiometricAuthValue:I

    .line 930
    :cond_1
    if-eqz p2, :cond_2

    .line 931
    or-int/2addr v0, p1

    .line 936
    :goto_1
    iput v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mBiometricAuthValue:I

    .line 937
    iget v1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mBiometricAuthValue:I

    if-gtz v1, :cond_3

    .line 938
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mIsBiometricAuthEnabled:Z

    .line 943
    :goto_2
    const-string v1, "KnoxConfigurationType"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setBiometricAuthenticationEnabled : bioAuth = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mBiometricAuthValue:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", enabled : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mIsBiometricAuthEnabled:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 933
    :cond_2
    xor-int/lit8 v1, p1, -0x1

    and-int/2addr v0, v1

    goto :goto_1

    .line 940
    :cond_3
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mIsBiometricAuthEnabled:Z

    goto :goto_2
.end method

.method public setCameraModeChangeEnabled(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 1195
    return-void
.end method

.method public setCustomBadgeIcon(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 545
    iput-object p1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mCustomBadgeIcon:Ljava/lang/String;

    .line 546
    return-void
.end method

.method public setCustomHomeScreenWallpaper(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 563
    iput-object p1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mCustomHomeScreenWallpaper:Ljava/lang/String;

    .line 564
    return-void
.end method

.method public setCustomLockScreenWallpaper(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 583
    iput-object p1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mCustomLockScreenWallpaper:Ljava/lang/String;

    .line 584
    return-void
.end method

.method public setCustomStatusIcon(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 621
    iput-object p1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mCustomStatusIcon:Ljava/lang/String;

    .line 622
    return-void
.end method

.method public setCustomStatusLabel(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 601
    iput-object p1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mCustomStatusLabel:Ljava/lang/String;

    .line 602
    return-void
.end method

.method public setDataSyncPolicy(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p2, "property"    # Ljava/lang/String;
    .param p3, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1071
    .local p1, "applications":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mRCPDataSettings:Ljava/util/HashMap;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->setRCPSyncPolicy(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/util/HashMap;)V

    .line 1072
    return-void
.end method

.method public setDisableSwitchWidgetOnLockScreen(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 1229
    return-void
.end method

.method public setFOTADisableList(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 676
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mFOTADisableAppList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 677
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 678
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mFOTADisableAppList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 680
    :cond_0
    return-void
.end method

.method public setFOTAReenableList(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 701
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mFOTAReenableAppList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 702
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 703
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mFOTAReenableAppList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 705
    :cond_0
    return-void
.end method

.method public setForbiddenStrings(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 733
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 734
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mForbiddenStrings:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 735
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mForbiddenStrings:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 737
    :cond_0
    return-void
.end method

.method public setGearSupportEnabled(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 1382
    return-void
.end method

.method public setGoogleAppsList(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1564
    .local p1, "pkgList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1565
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mGoogleAppsList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1566
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mGoogleAppsList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1568
    :cond_0
    return-void
.end method

.method public setManagedType(Z)V
    .locals 0
    .param p1, "userManaged"    # Z

    .prologue
    .line 1168
    iput-boolean p1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mManagedType:Z

    .line 1169
    return-void
.end method

.method public setMaximumCharacterOccurences(I)V
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 772
    if-ltz p1, :cond_0

    const/16 v0, 0x11

    if-ge p1, v0, :cond_0

    .line 773
    iput p1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mMaximumCharacterOccurences:I

    .line 774
    :cond_0
    return-void
.end method

.method public setMaximumCharacterSequenceLength(I)V
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 791
    if-ltz p1, :cond_0

    const/16 v0, 0x11

    if-ge p1, v0, :cond_0

    .line 792
    iput p1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mMaximumCharacterSequenceLength:I

    .line 793
    :cond_0
    return-void
.end method

.method public setMaximumFailedPasswordsForWipe(I)V
    .locals 0
    .param p1, "value"    # I

    .prologue
    .line 505
    if-ltz p1, :cond_0

    .line 506
    iput p1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mMaximumFailedPasswordsForWipe:I

    .line 507
    :cond_0
    return-void
.end method

.method public setMaximumNumericSequenceLength(I)V
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 811
    if-ltz p1, :cond_0

    const/16 v0, 0x11

    if-ge p1, v0, :cond_0

    .line 812
    iput p1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mMaximumNumericSequenceLength:I

    .line 813
    :cond_0
    return-void
.end method

.method public setMaximumTimeToLock(I)V
    .locals 0
    .param p1, "value"    # I

    .prologue
    .line 524
    if-ltz p1, :cond_0

    .line 525
    iput p1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mMaximumTimeToLock:I

    .line 526
    :cond_0
    return-void
.end method

.method public setModifyLockScreenTimeout(Z)V
    .locals 0
    .param p1, "allow"    # Z

    .prologue
    .line 1348
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 285
    if-eqz p1, :cond_0

    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 286
    iput-object p1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mName:Ljava/lang/String;

    .line 288
    :cond_0
    return-void
.end method

.method public setNotificationSyncPolicy(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p2, "property"    # Ljava/lang/String;
    .param p3, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1141
    .local p1, "packages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mRCPNotifSettings:Ljava/util/HashMap;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->setRCPSyncPolicy(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/util/HashMap;)V

    .line 1142
    return-void
.end method

.method public setPasswordMinimumLength(I)V
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 821
    if-ltz p1, :cond_0

    const/16 v0, 0x11

    if-ge p1, v0, :cond_0

    .line 822
    iput p1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPasswordMinimumLength:I

    .line 823
    :cond_0
    return-void
.end method

.method public setPasswordMinimumLetters(I)V
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 381
    if-ltz p1, :cond_0

    const/16 v0, 0x11

    if-ge p1, v0, :cond_0

    .line 382
    iput p1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPasswordMinimumLetters:I

    .line 383
    :cond_0
    return-void
.end method

.method public setPasswordMinimumLowerCase(I)V
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 444
    if-ltz p1, :cond_0

    const/16 v0, 0x11

    if-ge p1, v0, :cond_0

    .line 445
    iput p1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPasswordMinimumLowerCase:I

    .line 446
    :cond_0
    return-void
.end method

.method public setPasswordMinimumNonLetters(I)V
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 360
    if-ltz p1, :cond_0

    const/16 v0, 0x11

    if-ge p1, v0, :cond_0

    .line 361
    iput p1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPasswordMinimumNonLetters:I

    .line 362
    :cond_0
    return-void
.end method

.method public setPasswordMinimumNumeric(I)V
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 402
    if-ltz p1, :cond_0

    const/16 v0, 0x11

    if-ge p1, v0, :cond_0

    .line 403
    iput p1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPasswordMinimumNumeric:I

    .line 404
    :cond_0
    return-void
.end method

.method public setPasswordMinimumSymbols(I)V
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 465
    if-ltz p1, :cond_0

    const/16 v0, 0x11

    if-ge p1, v0, :cond_0

    .line 466
    iput p1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPasswordMinimumSymbols:I

    .line 467
    :cond_0
    return-void
.end method

.method public setPasswordMinimumUpperCase(I)V
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 423
    if-ltz p1, :cond_0

    const/16 v0, 0x11

    if-ge p1, v0, :cond_0

    .line 424
    iput p1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPasswordMinimumUpperCase:I

    .line 425
    :cond_0
    return-void
.end method

.method public setPasswordQuality(I)V
    .locals 0
    .param p1, "value"    # I

    .prologue
    .line 486
    if-ltz p1, :cond_0

    .line 487
    iput p1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPasswordQuality:I

    .line 488
    :cond_0
    return-void
.end method

.method public setPenWindowEnabled(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 1399
    return-void
.end method

.method public setPersonaList(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 317
    .local p1, "personaList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPersonaList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 318
    return-void
.end method

.method public setProtectedPackageList(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1540
    .local p1, "pkgList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1541
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mProtectedList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1542
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mProtectedList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1544
    :cond_0
    return-void
.end method

.method public setRequiredPasswordPattern(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 754
    iput-object p1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPasswordPattern:Ljava/lang/String;

    .line 755
    return-void
.end method

.method public setSimplePasswordEnabled(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 849
    iput-boolean p1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mSimplePasswordEnabled:Z

    .line 850
    return-void
.end method

.method public setUserId(I)V
    .locals 0
    .param p1, "userId"    # I

    .prologue
    .line 307
    iput p1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mUserId:I

    .line 308
    return-void
.end method

.method public setVersion(Ljava/lang/String;)V
    .locals 0
    .param p1, "v"    # Ljava/lang/String;

    .prologue
    .line 335
    iput-object p1, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mVersion:Ljava/lang/String;

    .line 336
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1695
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mName:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1696
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1700
    :goto_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mVersion:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1701
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mVersion:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1705
    :goto_1
    iget v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPasswordMinimumNonLetters:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1706
    iget v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPasswordMinimumLetters:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1707
    iget v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPasswordMinimumNumeric:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1708
    iget v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPasswordMinimumUpperCase:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1709
    iget v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPasswordMinimumLowerCase:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1710
    iget v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPasswordMinimumSymbols:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1711
    iget v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPasswordQuality:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1712
    iget v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mMaximumTimeToLock:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1713
    iget v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mMaximumFailedPasswordsForWipe:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1714
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mManagedType:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1715
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mCustomBadgeIcon:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 1716
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mCustomBadgeIcon:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1720
    :goto_3
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mCustomHomeScreenWallpaper:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 1721
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mCustomHomeScreenWallpaper:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1725
    :goto_4
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mCustomLockScreenWallpaper:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 1726
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mCustomLockScreenWallpaper:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1730
    :goto_5
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mCustomStatusLabel:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 1731
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mCustomStatusLabel:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1735
    :goto_6
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mCustomStatusIcon:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 1736
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mCustomStatusIcon:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1740
    :goto_7
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mAppInstallationList:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 1741
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mForbiddenStrings:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 1742
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mProtectedList:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 1743
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mGoogleAppsList:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 1744
    iget v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mMaximumCharacterOccurences:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1745
    iget v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mMaximumCharacterSequenceLength:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1746
    iget v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mMaximumNumericSequenceLength:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1747
    iget v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPasswordMinimumLength:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1748
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPasswordPattern:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 1749
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mPasswordPattern:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1753
    :goto_8
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mSimplePasswordEnabled:Z

    if-eqz v0, :cond_9

    move v0, v1

    :goto_9
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1754
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mMultifactorAuthEnabled:Z

    if-eqz v0, :cond_a

    move v0, v1

    :goto_a
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1755
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mAllowMultiwindowMode:Z

    if-eqz v0, :cond_b

    move v0, v1

    :goto_b
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1756
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mAllowTaskManager:Z

    if-eqz v0, :cond_c

    move v0, v1

    :goto_c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1757
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mIsBiometricAuthEnabled:Z

    if-eqz v0, :cond_d

    :goto_d
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1758
    iget v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mBiometricAuthValue:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1760
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mRCPDataSettings:Ljava/util/HashMap;

    invoke-direct {p0, p1, v0}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->serializeRCPSettings(Landroid/os/Parcel;Ljava/util/HashMap;)V

    .line 1761
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mAllowChangeDataSettings:Ljava/util/HashMap;

    invoke-direct {p0, p1, v0}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->serializeRCPSettings(Landroid/os/Parcel;Ljava/util/HashMap;)V

    .line 1762
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->mRCPNotifSettings:Ljava/util/HashMap;

    invoke-direct {p0, p1, v0}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->serializeRCPSettings(Landroid/os/Parcel;Ljava/util/HashMap;)V

    .line 1765
    return-void

    .line 1698
    :cond_0
    const-string v0, "custom"

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1703
    :cond_1
    const-string v0, "custom"

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_2
    move v0, v2

    .line 1714
    goto/16 :goto_2

    .line 1718
    :cond_3
    const-string v0, ""

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1723
    :cond_4
    const-string v0, ""

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_4

    .line 1728
    :cond_5
    const-string v0, ""

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_5

    .line 1733
    :cond_6
    const-string v0, ""

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_6

    .line 1738
    :cond_7
    const-string v0, ""

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_7

    .line 1751
    :cond_8
    const-string v0, ""

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_8

    :cond_9
    move v0, v2

    .line 1753
    goto :goto_9

    :cond_a
    move v0, v2

    .line 1754
    goto :goto_a

    :cond_b
    move v0, v2

    .line 1755
    goto :goto_b

    :cond_c
    move v0, v2

    .line 1756
    goto :goto_c

    :cond_d
    move v1, v2

    .line 1757
    goto :goto_d
.end method
