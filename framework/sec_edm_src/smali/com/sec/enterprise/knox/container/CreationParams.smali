.class public Lcom/sec/enterprise/knox/container/CreationParams;
.super Ljava/lang/Object;
.source "CreationParams.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/enterprise/knox/container/CreationParams;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String; = "CreationParams"


# instance fields
.field private mAdminPkgName:Ljava/lang/String;

.field private mConfigName:Ljava/lang/String;

.field private mPwdResetToken:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    new-instance v0, Lcom/sec/enterprise/knox/container/CreationParams$1;

    invoke-direct {v0}, Lcom/sec/enterprise/knox/container/CreationParams$1;-><init>()V

    sput-object v0, Lcom/sec/enterprise/knox/container/CreationParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object v0, p0, Lcom/sec/enterprise/knox/container/CreationParams;->mConfigName:Ljava/lang/String;

    .line 15
    iput-object v0, p0, Lcom/sec/enterprise/knox/container/CreationParams;->mAdminPkgName:Ljava/lang/String;

    .line 16
    iput-object v0, p0, Lcom/sec/enterprise/knox/container/CreationParams;->mPwdResetToken:Ljava/lang/String;

    .line 20
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v1, 0x0

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object v1, p0, Lcom/sec/enterprise/knox/container/CreationParams;->mConfigName:Ljava/lang/String;

    .line 15
    iput-object v1, p0, Lcom/sec/enterprise/knox/container/CreationParams;->mAdminPkgName:Ljava/lang/String;

    .line 16
    iput-object v1, p0, Lcom/sec/enterprise/knox/container/CreationParams;->mPwdResetToken:Ljava/lang/String;

    .line 74
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/container/CreationParams;->mConfigName:Ljava/lang/String;

    .line 75
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/CreationParams;->mConfigName:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/enterprise/knox/container/CreationParams;->mConfigName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76
    iput-object v1, p0, Lcom/sec/enterprise/knox/container/CreationParams;->mConfigName:Ljava/lang/String;

    .line 78
    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/container/CreationParams;->mAdminPkgName:Ljava/lang/String;

    .line 79
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/CreationParams;->mAdminPkgName:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/enterprise/knox/container/CreationParams;->mAdminPkgName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 80
    iput-object v1, p0, Lcom/sec/enterprise/knox/container/CreationParams;->mAdminPkgName:Ljava/lang/String;

    .line 82
    :cond_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/container/CreationParams;->mPwdResetToken:Ljava/lang/String;

    .line 83
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/CreationParams;->mPwdResetToken:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/enterprise/knox/container/CreationParams;->mPwdResetToken:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 84
    iput-object v1, p0, Lcom/sec/enterprise/knox/container/CreationParams;->mPwdResetToken:Ljava/lang/String;

    .line 86
    :cond_2
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 69
    const/4 v0, 0x0

    return v0
.end method

.method public getAdminPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/CreationParams;->mAdminPkgName:Ljava/lang/String;

    return-object v0
.end method

.method public getConfigurationName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/CreationParams;->mConfigName:Ljava/lang/String;

    return-object v0
.end method

.method public getPasswordResetToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/CreationParams;->mPwdResetToken:Ljava/lang/String;

    return-object v0
.end method

.method public setAdminPackageName(Ljava/lang/String;)V
    .locals 0
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 29
    iput-object p1, p0, Lcom/sec/enterprise/knox/container/CreationParams;->mAdminPkgName:Ljava/lang/String;

    .line 30
    return-void
.end method

.method public setConfigurationName(Ljava/lang/String;)V
    .locals 0
    .param p1, "configurationName"    # Ljava/lang/String;

    .prologue
    .line 24
    iput-object p1, p0, Lcom/sec/enterprise/knox/container/CreationParams;->mConfigName:Ljava/lang/String;

    .line 25
    return-void
.end method

.method public setPasswordResetToken(Ljava/lang/String;)V
    .locals 0
    .param p1, "token"    # Ljava/lang/String;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/sec/enterprise/knox/container/CreationParams;->mPwdResetToken:Ljava/lang/String;

    .line 35
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 91
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/CreationParams;->mConfigName:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/CreationParams;->mConfigName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 96
    :goto_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/CreationParams;->mAdminPkgName:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 97
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/CreationParams;->mAdminPkgName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 101
    :goto_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/CreationParams;->mPwdResetToken:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 102
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/CreationParams;->mPwdResetToken:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 106
    :goto_2
    return-void

    .line 94
    :cond_0
    const-string v0, ""

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 99
    :cond_1
    const-string v0, ""

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_1

    .line 104
    :cond_2
    const-string v0, ""

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_2
.end method
