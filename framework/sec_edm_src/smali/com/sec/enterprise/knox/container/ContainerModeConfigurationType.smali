.class public Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;
.super Lcom/sec/enterprise/knox/container/KnoxConfigurationType;
.source "ContainerModeConfigurationType.java"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String; = "ContainerModeConfigurationType"


# instance fields
.field protected mAllowClearAllNotification:Z

.field protected mAllowHomeKey:Z

.field protected mAllowSettingsChanges:Z

.field protected mAllowStatusBarExpansion:Z

.field protected mHideSystemBar:Z

.field protected mWipeRecentTasks:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 234
    new-instance v0, Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType$1;

    invoke-direct {v0}, Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType$1;-><init>()V

    sput-object v0, Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 250
    invoke-direct {p0}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;-><init>()V

    .line 49
    iput-boolean v1, p0, Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;->mAllowSettingsChanges:Z

    .line 50
    iput-boolean v1, p0, Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;->mAllowStatusBarExpansion:Z

    .line 51
    iput-boolean v1, p0, Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;->mAllowHomeKey:Z

    .line 52
    iput-boolean v0, p0, Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;->mAllowClearAllNotification:Z

    .line 53
    iput-boolean v0, p0, Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;->mHideSystemBar:Z

    .line 54
    iput-boolean v0, p0, Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;->mWipeRecentTasks:Z

    .line 252
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 256
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;-><init>(Landroid/os/Parcel;)V

    .line 49
    iput-boolean v1, p0, Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;->mAllowSettingsChanges:Z

    .line 50
    iput-boolean v1, p0, Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;->mAllowStatusBarExpansion:Z

    .line 51
    iput-boolean v1, p0, Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;->mAllowHomeKey:Z

    .line 52
    iput-boolean v2, p0, Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;->mAllowClearAllNotification:Z

    .line 53
    iput-boolean v2, p0, Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;->mHideSystemBar:Z

    .line 54
    iput-boolean v2, p0, Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;->mWipeRecentTasks:Z

    .line 257
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;->mAllowSettingsChanges:Z

    .line 258
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;->mAllowStatusBarExpansion:Z

    .line 259
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;->mAllowHomeKey:Z

    .line 260
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;->mAllowClearAllNotification:Z

    .line 261
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_4

    move v0, v1

    :goto_4
    iput-boolean v0, p0, Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;->mHideSystemBar:Z

    .line 262
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_5

    :goto_5
    iput-boolean v1, p0, Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;->mWipeRecentTasks:Z

    .line 263
    return-void

    :cond_0
    move v0, v2

    .line 257
    goto :goto_0

    :cond_1
    move v0, v2

    .line 258
    goto :goto_1

    :cond_2
    move v0, v2

    .line 259
    goto :goto_2

    :cond_3
    move v0, v2

    .line 260
    goto :goto_3

    :cond_4
    move v0, v2

    .line 261
    goto :goto_4

    :cond_5
    move v1, v2

    .line 262
    goto :goto_5
.end method


# virtual methods
.method public allowClearAllNotification(Z)V
    .locals 0
    .param p1, "allow"    # Z

    .prologue
    .line 179
    iput-boolean p1, p0, Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;->mAllowClearAllNotification:Z

    .line 180
    return-void
.end method

.method public allowHomeKey(Z)V
    .locals 0
    .param p1, "allow"    # Z

    .prologue
    .line 159
    iput-boolean p1, p0, Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;->mAllowHomeKey:Z

    .line 160
    return-void
.end method

.method public allowSettingsChanges(Z)V
    .locals 0
    .param p1, "allow"    # Z

    .prologue
    .line 119
    iput-boolean p1, p0, Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;->mAllowSettingsChanges:Z

    .line 120
    return-void
.end method

.method public allowStatusBarExpansion(Z)V
    .locals 0
    .param p1, "allow"    # Z

    .prologue
    .line 139
    iput-boolean p1, p0, Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;->mAllowStatusBarExpansion:Z

    .line 140
    return-void
.end method

.method public clone(Ljava/lang/String;)Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 82
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 83
    :cond_0
    const-string v1, "ContainerModeConfigurationType"

    const-string v2, "clone(): name is either null or empty, hence returning null"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    const/4 v0, 0x0

    .line 94
    :goto_0
    return-object v0

    .line 86
    :cond_1
    new-instance v0, Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;

    invoke-direct {v0}, Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;-><init>()V

    .line 87
    .local v0, "type":Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;
    invoke-virtual {p0, v0, p1}, Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;->cloneConfiguration(Lcom/sec/enterprise/knox/container/KnoxConfigurationType;Ljava/lang/String;)V

    .line 88
    iget-boolean v1, p0, Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;->mAllowSettingsChanges:Z

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;->allowSettingsChanges(Z)V

    .line 89
    iget-boolean v1, p0, Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;->mAllowStatusBarExpansion:Z

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;->allowStatusBarExpansion(Z)V

    .line 90
    iget-boolean v1, p0, Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;->mAllowHomeKey:Z

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;->allowHomeKey(Z)V

    .line 91
    iget-boolean v1, p0, Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;->mAllowClearAllNotification:Z

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;->allowClearAllNotification(Z)V

    .line 92
    iget-boolean v1, p0, Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;->mHideSystemBar:Z

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;->setHideSystemBar(Z)V

    .line 93
    iget-boolean v1, p0, Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;->mWipeRecentTasks:Z

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;->setWipeRecentTasks(Z)V

    goto :goto_0
.end method

.method public bridge synthetic clone(Ljava/lang/String;)Lcom/sec/enterprise/knox/container/KnoxConfigurationType;
    .locals 1
    .param p1, "x0"    # Ljava/lang/String;

    .prologue
    .line 46
    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;->clone(Ljava/lang/String;)Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;

    move-result-object v0

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 268
    const/4 v0, 0x0

    return v0
.end method

.method public dumpState()V
    .locals 3

    .prologue
    .line 101
    const-string v0, "ContainerModeConfigurationType"

    const-string v1, "COM config dump START:"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    const-string v0, "ContainerModeConfigurationType"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mAllowSettingsChanges : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;->mAllowSettingsChanges:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    const-string v0, "ContainerModeConfigurationType"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mAllowStatusBarExpansion : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;->mAllowStatusBarExpansion:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    const-string v0, "ContainerModeConfigurationType"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mAllowHomeKey : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;->mAllowHomeKey:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    const-string v0, "ContainerModeConfigurationType"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mAllowClearAllNotification : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;->mAllowClearAllNotification:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    const-string v0, "ContainerModeConfigurationType"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mHideSystemBar : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;->mHideSystemBar:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    const-string v0, "ContainerModeConfigurationType"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mWipeRecentTasks : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;->mWipeRecentTasks:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    invoke-super {p0}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->dumpState()V

    .line 109
    const-string v0, "ContainerModeConfigurationType"

    const-string v1, "COM config dump END."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    return-void
.end method

.method public isClearAllNotificationAllowed()Z
    .locals 1

    .prologue
    .line 189
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;->mAllowClearAllNotification:Z

    return v0
.end method

.method public isHideSystemBarEnabled()Z
    .locals 1

    .prologue
    .line 209
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;->mHideSystemBar:Z

    return v0
.end method

.method public isHomeKeyAllowed()Z
    .locals 1

    .prologue
    .line 169
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;->mAllowHomeKey:Z

    return v0
.end method

.method public isSettingChangesAllowed()Z
    .locals 1

    .prologue
    .line 129
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;->mAllowSettingsChanges:Z

    return v0
.end method

.method public isStatusBarExpansionAllowed()Z
    .locals 1

    .prologue
    .line 149
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;->mAllowStatusBarExpansion:Z

    return v0
.end method

.method public isWipeRecentTasksEnabled()Z
    .locals 1

    .prologue
    .line 229
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;->mWipeRecentTasks:Z

    return v0
.end method

.method public setHideSystemBar(Z)V
    .locals 0
    .param p1, "enabled"    # Z

    .prologue
    .line 199
    iput-boolean p1, p0, Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;->mHideSystemBar:Z

    .line 200
    return-void
.end method

.method public setWipeRecentTasks(Z)V
    .locals 0
    .param p1, "enabled"    # Z

    .prologue
    .line 219
    iput-boolean p1, p0, Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;->mWipeRecentTasks:Z

    .line 220
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 274
    invoke-super {p0, p1, p2}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->writeToParcel(Landroid/os/Parcel;I)V

    .line 275
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;->mAllowSettingsChanges:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 276
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;->mAllowStatusBarExpansion:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 277
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;->mAllowHomeKey:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 278
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;->mAllowClearAllNotification:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 279
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;->mHideSystemBar:Z

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 280
    iget-boolean v0, p0, Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;->mWipeRecentTasks:Z

    if-eqz v0, :cond_5

    :goto_5
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 281
    return-void

    :cond_0
    move v0, v2

    .line 275
    goto :goto_0

    :cond_1
    move v0, v2

    .line 276
    goto :goto_1

    :cond_2
    move v0, v2

    .line 277
    goto :goto_2

    :cond_3
    move v0, v2

    .line 278
    goto :goto_3

    :cond_4
    move v0, v2

    .line 279
    goto :goto_4

    :cond_5
    move v1, v2

    .line 280
    goto :goto_5
.end method
