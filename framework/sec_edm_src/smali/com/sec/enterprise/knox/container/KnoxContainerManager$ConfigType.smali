.class public final enum Lcom/sec/enterprise/knox/container/KnoxContainerManager$ConfigType;
.super Ljava/lang/Enum;
.source "KnoxContainerManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/enterprise/knox/container/KnoxContainerManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ConfigType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/enterprise/knox/container/KnoxContainerManager$ConfigType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/enterprise/knox/container/KnoxContainerManager$ConfigType;

.field public static final enum KIOSK:Lcom/sec/enterprise/knox/container/KnoxContainerManager$ConfigType;

.field public static final enum LAUNCHER:Lcom/sec/enterprise/knox/container/KnoxContainerManager$ConfigType;

.field public static final enum LIGHTWEIGHT:Lcom/sec/enterprise/knox/container/KnoxContainerManager$ConfigType;


# instance fields
.field private final mTypeString:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 695
    new-instance v0, Lcom/sec/enterprise/knox/container/KnoxContainerManager$ConfigType;

    const-string v1, "LIGHTWEIGHT"

    const-string v2, "lightweight"

    invoke-direct {v0, v1, v3, v2}, Lcom/sec/enterprise/knox/container/KnoxContainerManager$ConfigType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/enterprise/knox/container/KnoxContainerManager$ConfigType;->LIGHTWEIGHT:Lcom/sec/enterprise/knox/container/KnoxContainerManager$ConfigType;

    .line 696
    new-instance v0, Lcom/sec/enterprise/knox/container/KnoxContainerManager$ConfigType;

    const-string v1, "KIOSK"

    const-string v2, "kiosk"

    invoke-direct {v0, v1, v4, v2}, Lcom/sec/enterprise/knox/container/KnoxContainerManager$ConfigType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/enterprise/knox/container/KnoxContainerManager$ConfigType;->KIOSK:Lcom/sec/enterprise/knox/container/KnoxContainerManager$ConfigType;

    .line 697
    new-instance v0, Lcom/sec/enterprise/knox/container/KnoxContainerManager$ConfigType;

    const-string v1, "LAUNCHER"

    const-string v2, "launcher"

    invoke-direct {v0, v1, v5, v2}, Lcom/sec/enterprise/knox/container/KnoxContainerManager$ConfigType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/enterprise/knox/container/KnoxContainerManager$ConfigType;->LAUNCHER:Lcom/sec/enterprise/knox/container/KnoxContainerManager$ConfigType;

    .line 694
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/enterprise/knox/container/KnoxContainerManager$ConfigType;

    sget-object v1, Lcom/sec/enterprise/knox/container/KnoxContainerManager$ConfigType;->LIGHTWEIGHT:Lcom/sec/enterprise/knox/container/KnoxContainerManager$ConfigType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/enterprise/knox/container/KnoxContainerManager$ConfigType;->KIOSK:Lcom/sec/enterprise/knox/container/KnoxContainerManager$ConfigType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/enterprise/knox/container/KnoxContainerManager$ConfigType;->LAUNCHER:Lcom/sec/enterprise/knox/container/KnoxContainerManager$ConfigType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/enterprise/knox/container/KnoxContainerManager$ConfigType;->$VALUES:[Lcom/sec/enterprise/knox/container/KnoxContainerManager$ConfigType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 701
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 702
    iput-object p3, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager$ConfigType;->mTypeString:Ljava/lang/String;

    .line 703
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/enterprise/knox/container/KnoxContainerManager$ConfigType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 694
    const-class v0, Lcom/sec/enterprise/knox/container/KnoxContainerManager$ConfigType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/container/KnoxContainerManager$ConfigType;

    return-object v0
.end method

.method public static values()[Lcom/sec/enterprise/knox/container/KnoxContainerManager$ConfigType;
    .locals 1

    .prologue
    .line 694
    sget-object v0, Lcom/sec/enterprise/knox/container/KnoxContainerManager$ConfigType;->$VALUES:[Lcom/sec/enterprise/knox/container/KnoxContainerManager$ConfigType;

    invoke-virtual {v0}, [Lcom/sec/enterprise/knox/container/KnoxContainerManager$ConfigType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/enterprise/knox/container/KnoxContainerManager$ConfigType;

    return-object v0
.end method


# virtual methods
.method public getType(Ljava/lang/String;)Lcom/sec/enterprise/knox/container/KnoxContainerManager$ConfigType;
    .locals 5
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 710
    invoke-static {}, Lcom/sec/enterprise/knox/container/KnoxContainerManager$ConfigType;->values()[Lcom/sec/enterprise/knox/container/KnoxContainerManager$ConfigType;

    move-result-object v0

    .local v0, "arr$":[Lcom/sec/enterprise/knox/container/KnoxContainerManager$ConfigType;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 711
    .local v3, "type":Lcom/sec/enterprise/knox/container/KnoxContainerManager$ConfigType;
    iget-object v4, v3, Lcom/sec/enterprise/knox/container/KnoxContainerManager$ConfigType;->mTypeString:Ljava/lang/String;

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 715
    .end local v3    # "type":Lcom/sec/enterprise/knox/container/KnoxContainerManager$ConfigType;
    :goto_1
    return-object v3

    .line 710
    .restart local v3    # "type":Lcom/sec/enterprise/knox/container/KnoxContainerManager$ConfigType;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 715
    .end local v3    # "type":Lcom/sec/enterprise/knox/container/KnoxContainerManager$ConfigType;
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 706
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/KnoxContainerManager$ConfigType;->mTypeString:Ljava/lang/String;

    return-object v0
.end method
