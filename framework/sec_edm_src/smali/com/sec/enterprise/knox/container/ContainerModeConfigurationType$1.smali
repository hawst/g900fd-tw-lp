.class final Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType$1;
.super Ljava/lang/Object;
.source "ContainerModeConfigurationType.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 235
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 238
    new-instance v0, Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;

    invoke-direct {v0, p1}, Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;-><init>(Landroid/os/Parcel;)V

    .line 239
    .local v0, "f":Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;
    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Landroid/os/Parcel;

    .prologue
    .line 235
    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType$1;->createFromParcel(Landroid/os/Parcel;)Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;
    .locals 2
    .param p1, "size"    # I

    .prologue
    .line 244
    const-string v0, "ContainerModeConfigurationType"

    const-string v1, "ContainerModeConfigurationType[] array to be created"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 245
    new-array v0, p1, [Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 235
    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType$1;->newArray(I)[Lcom/sec/enterprise/knox/container/ContainerModeConfigurationType;

    move-result-object v0

    return-object v0
.end method
