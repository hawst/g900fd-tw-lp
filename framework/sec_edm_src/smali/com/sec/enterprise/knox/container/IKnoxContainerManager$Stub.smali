.class public abstract Lcom/sec/enterprise/knox/container/IKnoxContainerManager$Stub;
.super Landroid/os/Binder;
.source "IKnoxContainerManager.java"

# interfaces
.implements Lcom/sec/enterprise/knox/container/IKnoxContainerManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/enterprise/knox/container/IKnoxContainerManager$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.sec.enterprise.knox.container.IKnoxContainerManager"

.field static final TRANSACTION_addConfigurationType:I = 0x9

.field static final TRANSACTION_addNetworkSSID:I = 0x2a

.field static final TRANSACTION_addPackageToExternalStorageSBABlackList:I = 0x25

.field static final TRANSACTION_addPackageToExternalStorageWhiteList:I = 0x20

.field static final TRANSACTION_cancelCreateContainer:I = 0x3

.field static final TRANSACTION_changeContainerOwner:I = 0x2f

.field static final TRANSACTION_clearNetworkSSID:I = 0x2d

.field static final TRANSACTION_clearPackagesFromExternalStorageSBABlackList:I = 0x28

.field static final TRANSACTION_clearPackagesFromExternalStorageWhiteList:I = 0x24

.field static final TRANSACTION_createContainer:I = 0x1

.field static final TRANSACTION_createContainerInternal:I = 0x2

.field static final TRANSACTION_createContainerMarkSuccess:I = 0x4

.field static final TRANSACTION_createContainerWithCallback:I = 0x14

.field static final TRANSACTION_doSelfUninstall:I = 0x29

.field static final TRANSACTION_enableExternalStorage:I = 0x1e

.field static final TRANSACTION_enforceMultifactorAuthentication:I = 0x17

.field static final TRANSACTION_forceResetPassword:I = 0x16

.field static final TRANSACTION_getConfigurationType:I = 0xa

.field static final TRANSACTION_getConfigurationTypeByName:I = 0x7

.field static final TRANSACTION_getConfigurationTypes:I = 0x8

.field static final TRANSACTION_getContainerCreationParams:I = 0x11

.field static final TRANSACTION_getContainers:I = 0x6

.field static final TRANSACTION_getDefaultConfigurationTypes:I = 0xb

.field static final TRANSACTION_getEnforceAuthForContainer:I = 0xf

.field static final TRANSACTION_getHibernationTimeout:I = 0x1a

.field static final TRANSACTION_getNetworkSSID:I = 0x2c

.field static final TRANSACTION_getOwnContainers:I = 0x15

.field static final TRANSACTION_getPackageSignaturesFromExternalStorageWhiteList:I = 0x22

.field static final TRANSACTION_getPackagesFromExternalStorageSBABlackList:I = 0x26

.field static final TRANSACTION_getPackagesFromExternalStorageWhiteList:I = 0x21

.field static final TRANSACTION_getStatus:I = 0xc

.field static final TRANSACTION_isExternalStorageEnabled:I = 0x1f

.field static final TRANSACTION_isMultifactorAuthenticationEnforced:I = 0x18

.field static final TRANSACTION_isResetContainerOnRebootEnabled:I = 0x1d

.field static final TRANSACTION_isSettingsOptionEnabled:I = 0x31

.field static final TRANSACTION_isSettingsOptionEnabledInternal:I = 0x32

.field static final TRANSACTION_lockContainer:I = 0xd

.field static final TRANSACTION_registerBroadcastReceiverIntent:I = 0x12

.field static final TRANSACTION_removeConfigurationType:I = 0x19

.field static final TRANSACTION_removeContainer:I = 0x5

.field static final TRANSACTION_removeNetworkSSID:I = 0x2b

.field static final TRANSACTION_removePackageFromExternalStorageSBABlackList:I = 0x27

.field static final TRANSACTION_removePackageFromExternalStorageWhiteList:I = 0x23

.field static final TRANSACTION_resetContainerOnReboot:I = 0x1c

.field static final TRANSACTION_resetContainerPolicies:I = 0x2e

.field static final TRANSACTION_setEnforceAuthForContainer:I = 0x10

.field static final TRANSACTION_setHibernationTimeout:I = 0x1b

.field static final TRANSACTION_setSettingsOptionEnabled:I = 0x30

.field static final TRANSACTION_unlockContainer:I = 0xe

.field static final TRANSACTION_unregisterBroadcastReceiverIntent:I = 0x13


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 19
    const-string v0, "com.sec.enterprise.knox.container.IKnoxContainerManager"

    invoke-virtual {p0, p0, v0}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 20
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 27
    if-nez p0, :cond_0

    .line 28
    const/4 v0, 0x0

    .line 34
    :goto_0
    return-object v0

    .line 30
    :cond_0
    const-string v1, "com.sec.enterprise.knox.container.IKnoxContainerManager"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 31
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    if-eqz v1, :cond_1

    .line 32
    check-cast v0, Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    goto :goto_0

    .line 34
    :cond_1
    new-instance v0, Lcom/sec/enterprise/knox/container/IKnoxContainerManager$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 38
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 10
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 42
    sparse-switch p1, :sswitch_data_0

    .line 855
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v9

    :goto_0
    return v9

    .line 46
    :sswitch_0
    const-string v9, "com.sec.enterprise.knox.container.IKnoxContainerManager"

    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 47
    const/4 v9, 0x1

    goto :goto_0

    .line 51
    :sswitch_1
    const-string v9, "com.sec.enterprise.knox.container.IKnoxContainerManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 53
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_0

    .line 54
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 60
    .local v0, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_1
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_1

    .line 61
    sget-object v9, Lcom/sec/enterprise/knox/container/CreationParams;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/enterprise/knox/container/CreationParams;

    .line 67
    .local v2, "_arg1":Lcom/sec/enterprise/knox/container/CreationParams;
    :goto_2
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 68
    .local v1, "_arg2":I
    invoke-virtual {p0, v0, v2, v1}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager$Stub;->createContainer(Landroid/app/enterprise/ContextInfo;Lcom/sec/enterprise/knox/container/CreationParams;I)I

    move-result v6

    .line 69
    .local v6, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 70
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 71
    const/4 v9, 0x1

    goto :goto_0

    .line 57
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg2":I
    .end local v2    # "_arg1":Lcom/sec/enterprise/knox/container/CreationParams;
    .end local v6    # "_result":I
    :cond_0
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_1

    .line 64
    :cond_1
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Lcom/sec/enterprise/knox/container/CreationParams;
    goto :goto_2

    .line 75
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Lcom/sec/enterprise/knox/container/CreationParams;
    :sswitch_2
    const-string v9, "com.sec.enterprise.knox.container.IKnoxContainerManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 77
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_2

    .line 78
    sget-object v9, Lcom/sec/knox/container/ContainerCreationParams;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/knox/container/ContainerCreationParams;

    .line 83
    .local v0, "_arg0":Lcom/sec/knox/container/ContainerCreationParams;
    :goto_3
    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager$Stub;->createContainerInternal(Lcom/sec/knox/container/ContainerCreationParams;)I

    move-result v6

    .line 84
    .restart local v6    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 85
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 86
    const/4 v9, 0x1

    goto :goto_0

    .line 81
    .end local v0    # "_arg0":Lcom/sec/knox/container/ContainerCreationParams;
    .end local v6    # "_result":I
    :cond_2
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Lcom/sec/knox/container/ContainerCreationParams;
    goto :goto_3

    .line 90
    .end local v0    # "_arg0":Lcom/sec/knox/container/ContainerCreationParams;
    :sswitch_3
    const-string v9, "com.sec.enterprise.knox.container.IKnoxContainerManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 92
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_3

    .line 93
    sget-object v9, Lcom/sec/knox/container/ContainerCreationParams;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/knox/container/ContainerCreationParams;

    .line 98
    .restart local v0    # "_arg0":Lcom/sec/knox/container/ContainerCreationParams;
    :goto_4
    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager$Stub;->cancelCreateContainer(Lcom/sec/knox/container/ContainerCreationParams;)Z

    move-result v6

    .line 99
    .local v6, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 100
    if-eqz v6, :cond_4

    const/4 v9, 0x1

    :goto_5
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 101
    const/4 v9, 0x1

    goto :goto_0

    .line 96
    .end local v0    # "_arg0":Lcom/sec/knox/container/ContainerCreationParams;
    .end local v6    # "_result":Z
    :cond_3
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Lcom/sec/knox/container/ContainerCreationParams;
    goto :goto_4

    .line 100
    .restart local v6    # "_result":Z
    :cond_4
    const/4 v9, 0x0

    goto :goto_5

    .line 105
    .end local v0    # "_arg0":Lcom/sec/knox/container/ContainerCreationParams;
    .end local v6    # "_result":Z
    :sswitch_4
    const-string v9, "com.sec.enterprise.knox.container.IKnoxContainerManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 107
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_5

    .line 108
    sget-object v9, Lcom/sec/knox/container/ContainerCreationParams;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/knox/container/ContainerCreationParams;

    .line 113
    .restart local v0    # "_arg0":Lcom/sec/knox/container/ContainerCreationParams;
    :goto_6
    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager$Stub;->createContainerMarkSuccess(Lcom/sec/knox/container/ContainerCreationParams;)Z

    move-result v6

    .line 114
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 115
    if-eqz v6, :cond_6

    const/4 v9, 0x1

    :goto_7
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 116
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 111
    .end local v0    # "_arg0":Lcom/sec/knox/container/ContainerCreationParams;
    .end local v6    # "_result":Z
    :cond_5
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Lcom/sec/knox/container/ContainerCreationParams;
    goto :goto_6

    .line 115
    .restart local v6    # "_result":Z
    :cond_6
    const/4 v9, 0x0

    goto :goto_7

    .line 120
    .end local v0    # "_arg0":Lcom/sec/knox/container/ContainerCreationParams;
    .end local v6    # "_result":Z
    :sswitch_5
    const-string v9, "com.sec.enterprise.knox.container.IKnoxContainerManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 122
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_7

    .line 123
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 128
    .local v0, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_8
    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager$Stub;->removeContainer(Landroid/app/enterprise/ContextInfo;)I

    move-result v6

    .line 129
    .local v6, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 130
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 131
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 126
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":I
    :cond_7
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_8

    .line 135
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_6
    const-string v9, "com.sec.enterprise.knox.container.IKnoxContainerManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 137
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_8

    .line 138
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 143
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_9
    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager$Stub;->getContainers(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;

    move-result-object v6

    .line 144
    .local v6, "_result":Ljava/util/List;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 145
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 146
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 141
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Ljava/util/List;
    :cond_8
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_9

    .line 150
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_7
    const-string v9, "com.sec.enterprise.knox.container.IKnoxContainerManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 152
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_9

    .line 153
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 159
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_a
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 160
    .local v2, "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v2}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager$Stub;->getConfigurationTypeByName(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/util/List;

    move-result-object v6

    .line 161
    .restart local v6    # "_result":Ljava/util/List;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 162
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 163
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 156
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Ljava/util/List;
    :cond_9
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_a

    .line 167
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_8
    const-string v9, "com.sec.enterprise.knox.container.IKnoxContainerManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 169
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_a

    .line 170
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 175
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_b
    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager$Stub;->getConfigurationTypes(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;

    move-result-object v6

    .line 176
    .restart local v6    # "_result":Ljava/util/List;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 177
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 178
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 173
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Ljava/util/List;
    :cond_a
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_b

    .line 182
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_9
    const-string v9, "com.sec.enterprise.knox.container.IKnoxContainerManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 184
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_b

    .line 185
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 191
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_c
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v8

    .line 192
    .local v8, "cl":Ljava/lang/ClassLoader;
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v2

    .line 193
    .local v2, "_arg1":Ljava/util/List;
    invoke-virtual {p0, v0, v2}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager$Stub;->addConfigurationType(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z

    move-result v6

    .line 194
    .local v6, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 195
    if-eqz v6, :cond_c

    const/4 v9, 0x1

    :goto_d
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 196
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 188
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/util/List;
    .end local v6    # "_result":Z
    .end local v8    # "cl":Ljava/lang/ClassLoader;
    :cond_b
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_c

    .line 195
    .restart local v2    # "_arg1":Ljava/util/List;
    .restart local v6    # "_result":Z
    .restart local v8    # "cl":Ljava/lang/ClassLoader;
    :cond_c
    const/4 v9, 0x0

    goto :goto_d

    .line 200
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/util/List;
    .end local v6    # "_result":Z
    .end local v8    # "cl":Ljava/lang/ClassLoader;
    :sswitch_a
    const-string v9, "com.sec.enterprise.knox.container.IKnoxContainerManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 202
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_d

    .line 203
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 209
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_e
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 210
    .local v2, "_arg1":I
    invoke-virtual {p0, v0, v2}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager$Stub;->getConfigurationType(Landroid/app/enterprise/ContextInfo;I)Ljava/util/List;

    move-result-object v6

    .line 211
    .local v6, "_result":Ljava/util/List;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 212
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 213
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 206
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":I
    .end local v6    # "_result":Ljava/util/List;
    :cond_d
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_e

    .line 217
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_b
    const-string v9, "com.sec.enterprise.knox.container.IKnoxContainerManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 218
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager$Stub;->getDefaultConfigurationTypes()Ljava/util/List;

    move-result-object v6

    .line 219
    .restart local v6    # "_result":Ljava/util/List;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 220
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 221
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 225
    .end local v6    # "_result":Ljava/util/List;
    :sswitch_c
    const-string v9, "com.sec.enterprise.knox.container.IKnoxContainerManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 227
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_e

    .line 228
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 233
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_f
    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager$Stub;->getStatus(Landroid/app/enterprise/ContextInfo;)I

    move-result v6

    .line 234
    .local v6, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 235
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 236
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 231
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":I
    :cond_e
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_f

    .line 240
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_d
    const-string v9, "com.sec.enterprise.knox.container.IKnoxContainerManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 242
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_f

    .line 243
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 249
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_10
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 250
    .local v2, "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v2}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager$Stub;->lockContainer(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v6

    .line 251
    .local v6, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 252
    if-eqz v6, :cond_10

    const/4 v9, 0x1

    :goto_11
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 253
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 246
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Z
    :cond_f
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_10

    .line 252
    .restart local v2    # "_arg1":Ljava/lang/String;
    .restart local v6    # "_result":Z
    :cond_10
    const/4 v9, 0x0

    goto :goto_11

    .line 257
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Z
    :sswitch_e
    const-string v9, "com.sec.enterprise.knox.container.IKnoxContainerManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 259
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_11

    .line 260
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 265
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_12
    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager$Stub;->unlockContainer(Landroid/app/enterprise/ContextInfo;)Z

    move-result v6

    .line 266
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 267
    if-eqz v6, :cond_12

    const/4 v9, 0x1

    :goto_13
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 268
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 263
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Z
    :cond_11
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_12

    .line 267
    .restart local v6    # "_result":Z
    :cond_12
    const/4 v9, 0x0

    goto :goto_13

    .line 272
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Z
    :sswitch_f
    const-string v9, "com.sec.enterprise.knox.container.IKnoxContainerManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 274
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_13

    .line 275
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 280
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_14
    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager$Stub;->getEnforceAuthForContainer(Landroid/app/enterprise/ContextInfo;)Z

    move-result v6

    .line 281
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 282
    if-eqz v6, :cond_14

    const/4 v9, 0x1

    :goto_15
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 283
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 278
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Z
    :cond_13
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_14

    .line 282
    .restart local v6    # "_result":Z
    :cond_14
    const/4 v9, 0x0

    goto :goto_15

    .line 287
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Z
    :sswitch_10
    const-string v9, "com.sec.enterprise.knox.container.IKnoxContainerManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 289
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_15

    .line 290
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 296
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_16
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_16

    const/4 v2, 0x1

    .line 297
    .local v2, "_arg1":Z
    :goto_17
    invoke-virtual {p0, v0, v2}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager$Stub;->setEnforceAuthForContainer(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v6

    .line 298
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 299
    if-eqz v6, :cond_17

    const/4 v9, 0x1

    :goto_18
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 300
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 293
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Z
    .end local v6    # "_result":Z
    :cond_15
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_16

    .line 296
    :cond_16
    const/4 v2, 0x0

    goto :goto_17

    .line 299
    .restart local v2    # "_arg1":Z
    .restart local v6    # "_result":Z
    :cond_17
    const/4 v9, 0x0

    goto :goto_18

    .line 304
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Z
    .end local v6    # "_result":Z
    :sswitch_11
    const-string v9, "com.sec.enterprise.knox.container.IKnoxContainerManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 306
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 307
    .local v0, "_arg0":I
    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager$Stub;->getContainerCreationParams(I)Lcom/sec/knox/container/ContainerCreationParams;

    move-result-object v6

    .line 308
    .local v6, "_result":Lcom/sec/knox/container/ContainerCreationParams;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 309
    if-eqz v6, :cond_18

    .line 310
    const/4 v9, 0x1

    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 311
    const/4 v9, 0x1

    invoke-virtual {v6, p3, v9}, Lcom/sec/knox/container/ContainerCreationParams;->writeToParcel(Landroid/os/Parcel;I)V

    .line 316
    :goto_19
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 314
    :cond_18
    const/4 v9, 0x0

    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_19

    .line 320
    .end local v0    # "_arg0":I
    .end local v6    # "_result":Lcom/sec/knox/container/ContainerCreationParams;
    :sswitch_12
    const-string v9, "com.sec.enterprise.knox.container.IKnoxContainerManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 322
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_19

    .line 323
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 329
    .local v0, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_1a
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 331
    .local v2, "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 332
    .local v1, "_arg2":Ljava/lang/String;
    invoke-virtual {p0, v0, v2, v1}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager$Stub;->registerBroadcastReceiverIntent(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v6

    .line 333
    .local v6, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 334
    if-eqz v6, :cond_1a

    const/4 v9, 0x1

    :goto_1b
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 335
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 326
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg2":Ljava/lang/String;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Z
    :cond_19
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_1a

    .line 334
    .restart local v1    # "_arg2":Ljava/lang/String;
    .restart local v2    # "_arg1":Ljava/lang/String;
    .restart local v6    # "_result":Z
    :cond_1a
    const/4 v9, 0x0

    goto :goto_1b

    .line 339
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg2":Ljava/lang/String;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Z
    :sswitch_13
    const-string v9, "com.sec.enterprise.knox.container.IKnoxContainerManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 341
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_1b

    .line 342
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 348
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_1c
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 350
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 351
    .restart local v1    # "_arg2":Ljava/lang/String;
    invoke-virtual {p0, v0, v2, v1}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager$Stub;->unregisterBroadcastReceiverIntent(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v6

    .line 352
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 353
    if-eqz v6, :cond_1c

    const/4 v9, 0x1

    :goto_1d
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 354
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 345
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg2":Ljava/lang/String;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Z
    :cond_1b
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_1c

    .line 353
    .restart local v1    # "_arg2":Ljava/lang/String;
    .restart local v2    # "_arg1":Ljava/lang/String;
    .restart local v6    # "_result":Z
    :cond_1c
    const/4 v9, 0x0

    goto :goto_1d

    .line 358
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg2":Ljava/lang/String;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Z
    :sswitch_14
    const-string v9, "com.sec.enterprise.knox.container.IKnoxContainerManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 360
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_1d

    .line 361
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 367
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_1e
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_1e

    .line 368
    sget-object v9, Lcom/sec/enterprise/knox/container/CreationParams;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/enterprise/knox/container/CreationParams;

    .line 374
    .local v2, "_arg1":Lcom/sec/enterprise/knox/container/CreationParams;
    :goto_1f
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 376
    .local v1, "_arg2":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/enterprise/knox/IEnterpriseContainerCallback$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/enterprise/knox/IEnterpriseContainerCallback;

    move-result-object v4

    .line 377
    .local v4, "_arg3":Lcom/sec/enterprise/knox/IEnterpriseContainerCallback;
    invoke-virtual {p0, v0, v2, v1, v4}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager$Stub;->createContainerWithCallback(Landroid/app/enterprise/ContextInfo;Lcom/sec/enterprise/knox/container/CreationParams;ILcom/sec/enterprise/knox/IEnterpriseContainerCallback;)I

    move-result v6

    .line 378
    .local v6, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 379
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 380
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 364
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg2":I
    .end local v2    # "_arg1":Lcom/sec/enterprise/knox/container/CreationParams;
    .end local v4    # "_arg3":Lcom/sec/enterprise/knox/IEnterpriseContainerCallback;
    .end local v6    # "_result":I
    :cond_1d
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_1e

    .line 371
    :cond_1e
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Lcom/sec/enterprise/knox/container/CreationParams;
    goto :goto_1f

    .line 384
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Lcom/sec/enterprise/knox/container/CreationParams;
    :sswitch_15
    const-string v9, "com.sec.enterprise.knox.container.IKnoxContainerManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 385
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager$Stub;->getOwnContainers()[Lcom/sec/enterprise/knox/EnterpriseContainerObject;

    move-result-object v6

    .line 386
    .local v6, "_result":[Lcom/sec/enterprise/knox/EnterpriseContainerObject;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 387
    const/4 v9, 0x1

    invoke-virtual {p3, v6, v9}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    .line 388
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 392
    .end local v6    # "_result":[Lcom/sec/enterprise/knox/EnterpriseContainerObject;
    :sswitch_16
    const-string v9, "com.sec.enterprise.knox.container.IKnoxContainerManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 394
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_1f

    .line 395
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 401
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_20
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 403
    .local v2, "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 404
    .restart local v1    # "_arg2":I
    invoke-virtual {p0, v0, v2, v1}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager$Stub;->forceResetPassword(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;I)I

    move-result v6

    .line 405
    .local v6, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 406
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 407
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 398
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg2":I
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":I
    :cond_1f
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_20

    .line 411
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_17
    const-string v9, "com.sec.enterprise.knox.container.IKnoxContainerManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 413
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_20

    .line 414
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 420
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_21
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_21

    const/4 v2, 0x1

    .line 421
    .local v2, "_arg1":Z
    :goto_22
    invoke-virtual {p0, v0, v2}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager$Stub;->enforceMultifactorAuthentication(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v6

    .line 422
    .local v6, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 423
    if-eqz v6, :cond_22

    const/4 v9, 0x1

    :goto_23
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 424
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 417
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Z
    .end local v6    # "_result":Z
    :cond_20
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_21

    .line 420
    :cond_21
    const/4 v2, 0x0

    goto :goto_22

    .line 423
    .restart local v2    # "_arg1":Z
    .restart local v6    # "_result":Z
    :cond_22
    const/4 v9, 0x0

    goto :goto_23

    .line 428
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Z
    .end local v6    # "_result":Z
    :sswitch_18
    const-string v9, "com.sec.enterprise.knox.container.IKnoxContainerManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 430
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_23

    .line 431
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 436
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_24
    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager$Stub;->isMultifactorAuthenticationEnforced(Landroid/app/enterprise/ContextInfo;)Z

    move-result v6

    .line 437
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 438
    if-eqz v6, :cond_24

    const/4 v9, 0x1

    :goto_25
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 439
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 434
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Z
    :cond_23
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_24

    .line 438
    .restart local v6    # "_result":Z
    :cond_24
    const/4 v9, 0x0

    goto :goto_25

    .line 443
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Z
    :sswitch_19
    const-string v9, "com.sec.enterprise.knox.container.IKnoxContainerManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 445
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_25

    .line 446
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 452
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_26
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 453
    .local v2, "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v2}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager$Stub;->removeConfigurationType(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v6

    .line 454
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 455
    if-eqz v6, :cond_26

    const/4 v9, 0x1

    :goto_27
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 456
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 449
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Z
    :cond_25
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_26

    .line 455
    .restart local v2    # "_arg1":Ljava/lang/String;
    .restart local v6    # "_result":Z
    :cond_26
    const/4 v9, 0x0

    goto :goto_27

    .line 460
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Z
    :sswitch_1a
    const-string v9, "com.sec.enterprise.knox.container.IKnoxContainerManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 462
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_27

    .line 463
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 468
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_28
    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager$Stub;->getHibernationTimeout(Landroid/app/enterprise/ContextInfo;)J

    move-result-wide v6

    .line 469
    .local v6, "_result":J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 470
    invoke-virtual {p3, v6, v7}, Landroid/os/Parcel;->writeLong(J)V

    .line 471
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 466
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":J
    :cond_27
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_28

    .line 475
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_1b
    const-string v9, "com.sec.enterprise.knox.container.IKnoxContainerManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 477
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_28

    .line 478
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 484
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_29
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    .line 485
    .local v2, "_arg1":J
    invoke-virtual {p0, v0, v2, v3}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager$Stub;->setHibernationTimeout(Landroid/app/enterprise/ContextInfo;J)Z

    move-result v6

    .line 486
    .local v6, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 487
    if-eqz v6, :cond_29

    const/4 v9, 0x1

    :goto_2a
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 488
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 481
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":J
    .end local v6    # "_result":Z
    :cond_28
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_29

    .line 487
    .restart local v2    # "_arg1":J
    .restart local v6    # "_result":Z
    :cond_29
    const/4 v9, 0x0

    goto :goto_2a

    .line 492
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":J
    .end local v6    # "_result":Z
    :sswitch_1c
    const-string v9, "com.sec.enterprise.knox.container.IKnoxContainerManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 494
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_2a

    .line 495
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 501
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_2b
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_2b

    const/4 v2, 0x1

    .line 502
    .local v2, "_arg1":Z
    :goto_2c
    invoke-virtual {p0, v0, v2}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager$Stub;->resetContainerOnReboot(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v6

    .line 503
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 504
    if-eqz v6, :cond_2c

    const/4 v9, 0x1

    :goto_2d
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 505
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 498
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Z
    .end local v6    # "_result":Z
    :cond_2a
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_2b

    .line 501
    :cond_2b
    const/4 v2, 0x0

    goto :goto_2c

    .line 504
    .restart local v2    # "_arg1":Z
    .restart local v6    # "_result":Z
    :cond_2c
    const/4 v9, 0x0

    goto :goto_2d

    .line 509
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Z
    .end local v6    # "_result":Z
    :sswitch_1d
    const-string v9, "com.sec.enterprise.knox.container.IKnoxContainerManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 511
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_2d

    .line 512
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 517
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_2e
    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager$Stub;->isResetContainerOnRebootEnabled(Landroid/app/enterprise/ContextInfo;)Z

    move-result v6

    .line 518
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 519
    if-eqz v6, :cond_2e

    const/4 v9, 0x1

    :goto_2f
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 520
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 515
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Z
    :cond_2d
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_2e

    .line 519
    .restart local v6    # "_result":Z
    :cond_2e
    const/4 v9, 0x0

    goto :goto_2f

    .line 524
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Z
    :sswitch_1e
    const-string v9, "com.sec.enterprise.knox.container.IKnoxContainerManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 526
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_2f

    .line 527
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 533
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_30
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_30

    const/4 v2, 0x1

    .line 534
    .restart local v2    # "_arg1":Z
    :goto_31
    invoke-virtual {p0, v0, v2}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager$Stub;->enableExternalStorage(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v6

    .line 535
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 536
    if-eqz v6, :cond_31

    const/4 v9, 0x1

    :goto_32
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 537
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 530
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Z
    .end local v6    # "_result":Z
    :cond_2f
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_30

    .line 533
    :cond_30
    const/4 v2, 0x0

    goto :goto_31

    .line 536
    .restart local v2    # "_arg1":Z
    .restart local v6    # "_result":Z
    :cond_31
    const/4 v9, 0x0

    goto :goto_32

    .line 541
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Z
    .end local v6    # "_result":Z
    :sswitch_1f
    const-string v9, "com.sec.enterprise.knox.container.IKnoxContainerManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 543
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_32

    .line 544
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 549
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_33
    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager$Stub;->isExternalStorageEnabled(Landroid/app/enterprise/ContextInfo;)Z

    move-result v6

    .line 550
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 551
    if-eqz v6, :cond_33

    const/4 v9, 0x1

    :goto_34
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 552
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 547
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Z
    :cond_32
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_33

    .line 551
    .restart local v6    # "_result":Z
    :cond_33
    const/4 v9, 0x0

    goto :goto_34

    .line 556
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Z
    :sswitch_20
    const-string v9, "com.sec.enterprise.knox.container.IKnoxContainerManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 558
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_34

    .line 559
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 565
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_35
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 567
    .local v2, "_arg1":Ljava/lang/String;
    sget-object v9, Landroid/content/pm/Signature;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->createTypedArray(Landroid/os/Parcelable$Creator;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Landroid/content/pm/Signature;

    .line 568
    .local v1, "_arg2":[Landroid/content/pm/Signature;
    invoke-virtual {p0, v0, v2, v1}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager$Stub;->addPackageToExternalStorageWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;[Landroid/content/pm/Signature;)Z

    move-result v6

    .line 569
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 570
    if-eqz v6, :cond_35

    const/4 v9, 0x1

    :goto_36
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 571
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 562
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg2":[Landroid/content/pm/Signature;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Z
    :cond_34
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_35

    .line 570
    .restart local v1    # "_arg2":[Landroid/content/pm/Signature;
    .restart local v2    # "_arg1":Ljava/lang/String;
    .restart local v6    # "_result":Z
    :cond_35
    const/4 v9, 0x0

    goto :goto_36

    .line 575
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg2":[Landroid/content/pm/Signature;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Z
    :sswitch_21
    const-string v9, "com.sec.enterprise.knox.container.IKnoxContainerManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 577
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_36

    .line 578
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 583
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_37
    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager$Stub;->getPackagesFromExternalStorageWhiteList(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;

    move-result-object v5

    .line 584
    .local v5, "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 585
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 586
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 581
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v5    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_36
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_37

    .line 590
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_22
    const-string v9, "com.sec.enterprise.knox.container.IKnoxContainerManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 592
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_37

    .line 593
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 599
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_38
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 600
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v2}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager$Stub;->getPackageSignaturesFromExternalStorageWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)[Landroid/content/pm/Signature;

    move-result-object v6

    .line 601
    .local v6, "_result":[Landroid/content/pm/Signature;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 602
    const/4 v9, 0x1

    invoke-virtual {p3, v6, v9}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    .line 603
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 596
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":[Landroid/content/pm/Signature;
    :cond_37
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_38

    .line 607
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_23
    const-string v9, "com.sec.enterprise.knox.container.IKnoxContainerManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 609
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_38

    .line 610
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 616
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_39
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 617
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v2}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager$Stub;->removePackageFromExternalStorageWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v6

    .line 618
    .local v6, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 619
    if-eqz v6, :cond_39

    const/4 v9, 0x1

    :goto_3a
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 620
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 613
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Z
    :cond_38
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_39

    .line 619
    .restart local v2    # "_arg1":Ljava/lang/String;
    .restart local v6    # "_result":Z
    :cond_39
    const/4 v9, 0x0

    goto :goto_3a

    .line 624
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Z
    :sswitch_24
    const-string v9, "com.sec.enterprise.knox.container.IKnoxContainerManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 626
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_3a

    .line 627
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 632
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_3b
    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager$Stub;->clearPackagesFromExternalStorageWhiteList(Landroid/app/enterprise/ContextInfo;)Z

    move-result v6

    .line 633
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 634
    if-eqz v6, :cond_3b

    const/4 v9, 0x1

    :goto_3c
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 635
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 630
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Z
    :cond_3a
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_3b

    .line 634
    .restart local v6    # "_result":Z
    :cond_3b
    const/4 v9, 0x0

    goto :goto_3c

    .line 639
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Z
    :sswitch_25
    const-string v9, "com.sec.enterprise.knox.container.IKnoxContainerManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 641
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_3c

    .line 642
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 648
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_3d
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 649
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v2}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager$Stub;->addPackageToExternalStorageSBABlackList(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v6

    .line 650
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 651
    if-eqz v6, :cond_3d

    const/4 v9, 0x1

    :goto_3e
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 652
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 645
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Z
    :cond_3c
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_3d

    .line 651
    .restart local v2    # "_arg1":Ljava/lang/String;
    .restart local v6    # "_result":Z
    :cond_3d
    const/4 v9, 0x0

    goto :goto_3e

    .line 656
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Z
    :sswitch_26
    const-string v9, "com.sec.enterprise.knox.container.IKnoxContainerManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 658
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_3e

    .line 659
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 664
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_3f
    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager$Stub;->getPackagesFromExternalStorageSBABlackList(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;

    move-result-object v5

    .line 665
    .restart local v5    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 666
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 667
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 662
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v5    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_3e
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_3f

    .line 671
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_27
    const-string v9, "com.sec.enterprise.knox.container.IKnoxContainerManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 673
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_3f

    .line 674
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 680
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_40
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 681
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v2}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager$Stub;->removePackageFromExternalStorageSBABlackList(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v6

    .line 682
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 683
    if-eqz v6, :cond_40

    const/4 v9, 0x1

    :goto_41
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 684
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 677
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Z
    :cond_3f
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_40

    .line 683
    .restart local v2    # "_arg1":Ljava/lang/String;
    .restart local v6    # "_result":Z
    :cond_40
    const/4 v9, 0x0

    goto :goto_41

    .line 688
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Z
    :sswitch_28
    const-string v9, "com.sec.enterprise.knox.container.IKnoxContainerManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 690
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_41

    .line 691
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 696
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_42
    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager$Stub;->clearPackagesFromExternalStorageSBABlackList(Landroid/app/enterprise/ContextInfo;)Z

    move-result v6

    .line 697
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 698
    if-eqz v6, :cond_42

    const/4 v9, 0x1

    :goto_43
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 699
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 694
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Z
    :cond_41
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_42

    .line 698
    .restart local v6    # "_result":Z
    :cond_42
    const/4 v9, 0x0

    goto :goto_43

    .line 703
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Z
    :sswitch_29
    const-string v9, "com.sec.enterprise.knox.container.IKnoxContainerManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 704
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager$Stub;->doSelfUninstall()V

    .line 705
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 706
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 710
    :sswitch_2a
    const-string v9, "com.sec.enterprise.knox.container.IKnoxContainerManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 712
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_43

    .line 713
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 719
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_44
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 720
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v2}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager$Stub;->addNetworkSSID(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v6

    .line 721
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 722
    if-eqz v6, :cond_44

    const/4 v9, 0x1

    :goto_45
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 723
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 716
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Z
    :cond_43
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_44

    .line 722
    .restart local v2    # "_arg1":Ljava/lang/String;
    .restart local v6    # "_result":Z
    :cond_44
    const/4 v9, 0x0

    goto :goto_45

    .line 727
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Z
    :sswitch_2b
    const-string v9, "com.sec.enterprise.knox.container.IKnoxContainerManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 729
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_45

    .line 730
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 736
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_46
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 737
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v2}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager$Stub;->removeNetworkSSID(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v6

    .line 738
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 739
    if-eqz v6, :cond_46

    const/4 v9, 0x1

    :goto_47
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 740
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 733
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Z
    :cond_45
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_46

    .line 739
    .restart local v2    # "_arg1":Ljava/lang/String;
    .restart local v6    # "_result":Z
    :cond_46
    const/4 v9, 0x0

    goto :goto_47

    .line 744
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Z
    :sswitch_2c
    const-string v9, "com.sec.enterprise.knox.container.IKnoxContainerManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 746
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_47

    .line 747
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 752
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_48
    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager$Stub;->getNetworkSSID(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;

    move-result-object v5

    .line 753
    .restart local v5    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 754
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 755
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 750
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v5    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_47
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_48

    .line 759
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_2d
    const-string v9, "com.sec.enterprise.knox.container.IKnoxContainerManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 761
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_48

    .line 762
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 767
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_49
    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager$Stub;->clearNetworkSSID(Landroid/app/enterprise/ContextInfo;)Z

    move-result v6

    .line 768
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 769
    if-eqz v6, :cond_49

    const/4 v9, 0x1

    :goto_4a
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 770
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 765
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Z
    :cond_48
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_49

    .line 769
    .restart local v6    # "_result":Z
    :cond_49
    const/4 v9, 0x0

    goto :goto_4a

    .line 774
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Z
    :sswitch_2e
    const-string v9, "com.sec.enterprise.knox.container.IKnoxContainerManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 776
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_4a

    .line 777
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 782
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_4b
    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager$Stub;->resetContainerPolicies(Landroid/app/enterprise/ContextInfo;)Z

    move-result v6

    .line 783
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 784
    if-eqz v6, :cond_4b

    const/4 v9, 0x1

    :goto_4c
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 785
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 780
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Z
    :cond_4a
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_4b

    .line 784
    .restart local v6    # "_result":Z
    :cond_4b
    const/4 v9, 0x0

    goto :goto_4c

    .line 789
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Z
    :sswitch_2f
    const-string v9, "com.sec.enterprise.knox.container.IKnoxContainerManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 791
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_4c

    .line 792
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 798
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_4d
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 799
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v2}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager$Stub;->changeContainerOwner(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v6

    .line 800
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 801
    if-eqz v6, :cond_4d

    const/4 v9, 0x1

    :goto_4e
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 802
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 795
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Z
    :cond_4c
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_4d

    .line 801
    .restart local v2    # "_arg1":Ljava/lang/String;
    .restart local v6    # "_result":Z
    :cond_4d
    const/4 v9, 0x0

    goto :goto_4e

    .line 806
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Z
    :sswitch_30
    const-string v9, "com.sec.enterprise.knox.container.IKnoxContainerManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 808
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_4e

    .line 809
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 815
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_4f
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 817
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_4f

    const/4 v1, 0x1

    .line 818
    .local v1, "_arg2":Z
    :goto_50
    invoke-virtual {p0, v0, v2, v1}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager$Stub;->setSettingsOptionEnabled(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Z)Z

    move-result v6

    .line 819
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 820
    if-eqz v6, :cond_50

    const/4 v9, 0x1

    :goto_51
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 821
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 812
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg2":Z
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Z
    :cond_4e
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_4f

    .line 817
    .restart local v2    # "_arg1":Ljava/lang/String;
    :cond_4f
    const/4 v1, 0x0

    goto :goto_50

    .line 820
    .restart local v1    # "_arg2":Z
    .restart local v6    # "_result":Z
    :cond_50
    const/4 v9, 0x0

    goto :goto_51

    .line 825
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg2":Z
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Z
    :sswitch_31
    const-string v9, "com.sec.enterprise.knox.container.IKnoxContainerManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 827
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_51

    .line 828
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 834
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_52
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 835
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v2}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager$Stub;->isSettingsOptionEnabled(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v6

    .line 836
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 837
    if-eqz v6, :cond_52

    const/4 v9, 0x1

    :goto_53
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 838
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 831
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Z
    :cond_51
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_52

    .line 837
    .restart local v2    # "_arg1":Ljava/lang/String;
    .restart local v6    # "_result":Z
    :cond_52
    const/4 v9, 0x0

    goto :goto_53

    .line 842
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Z
    :sswitch_32
    const-string v9, "com.sec.enterprise.knox.container.IKnoxContainerManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 844
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 846
    .local v0, "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 848
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_53

    const/4 v1, 0x1

    .line 849
    .restart local v1    # "_arg2":Z
    :goto_54
    invoke-virtual {p0, v0, v2, v1}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager$Stub;->isSettingsOptionEnabledInternal(ILjava/lang/String;Z)Z

    move-result v6

    .line 850
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 851
    if-eqz v6, :cond_54

    const/4 v9, 0x1

    :goto_55
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 852
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 848
    .end local v1    # "_arg2":Z
    .end local v6    # "_result":Z
    :cond_53
    const/4 v1, 0x0

    goto :goto_54

    .line 851
    .restart local v1    # "_arg2":Z
    .restart local v6    # "_result":Z
    :cond_54
    const/4 v9, 0x0

    goto :goto_55

    .line 42
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0xe -> :sswitch_e
        0xf -> :sswitch_f
        0x10 -> :sswitch_10
        0x11 -> :sswitch_11
        0x12 -> :sswitch_12
        0x13 -> :sswitch_13
        0x14 -> :sswitch_14
        0x15 -> :sswitch_15
        0x16 -> :sswitch_16
        0x17 -> :sswitch_17
        0x18 -> :sswitch_18
        0x19 -> :sswitch_19
        0x1a -> :sswitch_1a
        0x1b -> :sswitch_1b
        0x1c -> :sswitch_1c
        0x1d -> :sswitch_1d
        0x1e -> :sswitch_1e
        0x1f -> :sswitch_1f
        0x20 -> :sswitch_20
        0x21 -> :sswitch_21
        0x22 -> :sswitch_22
        0x23 -> :sswitch_23
        0x24 -> :sswitch_24
        0x25 -> :sswitch_25
        0x26 -> :sswitch_26
        0x27 -> :sswitch_27
        0x28 -> :sswitch_28
        0x29 -> :sswitch_29
        0x2a -> :sswitch_2a
        0x2b -> :sswitch_2b
        0x2c -> :sswitch_2c
        0x2d -> :sswitch_2d
        0x2e -> :sswitch_2e
        0x2f -> :sswitch_2f
        0x30 -> :sswitch_30
        0x31 -> :sswitch_31
        0x32 -> :sswitch_32
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
