.class public Lcom/sec/enterprise/knox/container/RCPPolicy;
.super Ljava/lang/Object;
.source "RCPPolicy.java"


# static fields
.field public static final BOOKMARKS:Ljava/lang/String; = "Bookmarks"

.field public static final CALENDAR:Ljava/lang/String; = "Calendar"

.field public static final CALL_LOG:Ljava/lang/String; = "CallLog"

.field public static final CLIPBOARD:Ljava/lang/String; = "Clipboard"

.field public static final CONTACTS:Ljava/lang/String; = "Contacts"

.field public static final DEFAULT_APPS:Ljava/lang/String; = "DefaultApps"

.field public static final EXPORT_DATA:Ljava/lang/String; = "knox-export-data"

.field public static final IMPORT_DATA:Ljava/lang/String; = "knox-import-data"

.field public static final NOTIFICATIONS:Ljava/lang/String; = "Notifications"

.field public static final SANITIZE_DATA:Ljava/lang/String; = "knox-sanitize-data"

.field public static final SHORTCUTS:Ljava/lang/String; = "Shortcuts"

.field public static final SMS:Ljava/lang/String; = "Sms"

.field private static final TAG:Ljava/lang/String; = "RCPPolicy"

.field private static gRCPService:Lcom/sec/enterprise/knox/container/IRCPPolicy;


# instance fields
.field private mContextInfo:Landroid/app/enterprise/ContextInfo;


# direct methods
.method public constructor <init>(Landroid/app/enterprise/ContextInfo;)V
    .locals 0
    .param p1, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;

    .prologue
    .line 248
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 249
    iput-object p1, p0, Lcom/sec/enterprise/knox/container/RCPPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    .line 250
    return-void
.end method

.method static declared-synchronized getRCPPolicyService()Lcom/sec/enterprise/knox/container/IRCPPolicy;
    .locals 2

    .prologue
    .line 237
    const-class v1, Lcom/sec/enterprise/knox/container/RCPPolicy;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/enterprise/knox/container/RCPPolicy;->gRCPService:Lcom/sec/enterprise/knox/container/IRCPPolicy;

    if-nez v0, :cond_0

    .line 238
    const-string v0, "mum_container_rcp_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/container/IRCPPolicy$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/enterprise/knox/container/IRCPPolicy;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/container/RCPPolicy;->gRCPService:Lcom/sec/enterprise/knox/container/IRCPPolicy;

    .line 242
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/container/RCPPolicy;->gRCPService:Lcom/sec/enterprise/knox/container/IRCPPolicy;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 237
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public allowMoveAppsToContainer(Z)Z
    .locals 7
    .param p1, "allow"    # Z

    .prologue
    .line 821
    iget-object v4, p0, Lcom/sec/enterprise/knox/container/RCPPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "RCPPolicy.allowMoveAppsToContainer"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 823
    const/4 v2, 0x0

    .line 824
    .local v2, "retVal":Z
    invoke-static {}, Lcom/sec/enterprise/knox/container/RCPPolicy;->getRCPPolicyService()Lcom/sec/enterprise/knox/container/IRCPPolicy;

    move-result-object v1

    .line 825
    .local v1, "rcpService":Lcom/sec/enterprise/knox/container/IRCPPolicy;
    if-nez v1, :cond_0

    .line 826
    const-string v4, "RCPPolicy"

    const-string v5, " RCP policy service is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 835
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 831
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/container/RCPPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v4, p1}, Lcom/sec/enterprise/knox/container/IRCPPolicy;->allowMoveAppsToContainer(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 835
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 832
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 833
    .local v0, "e":Landroid/os/RemoteException;
    const-string v4, "RCPPolicy"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed talking with RCP policy service: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public allowMoveFilesToContainer(Z)Z
    .locals 7
    .param p1, "allow"    # Z

    .prologue
    .line 638
    iget-object v4, p0, Lcom/sec/enterprise/knox/container/RCPPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "RCPPolicy.allowMoveFilesToContainer"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 640
    const/4 v2, 0x0

    .line 641
    .local v2, "retVal":Z
    invoke-static {}, Lcom/sec/enterprise/knox/container/RCPPolicy;->getRCPPolicyService()Lcom/sec/enterprise/knox/container/IRCPPolicy;

    move-result-object v1

    .line 642
    .local v1, "rcpService":Lcom/sec/enterprise/knox/container/IRCPPolicy;
    if-nez v1, :cond_0

    .line 643
    const-string v4, "RCPPolicy"

    const-string v5, " RCP policy service is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 652
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 648
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/container/RCPPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v4, p1}, Lcom/sec/enterprise/knox/container/IRCPPolicy;->allowMoveFilesToContainer(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 652
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 649
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 650
    .local v0, "e":Landroid/os/RemoteException;
    const-string v4, "RCPPolicy"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed talking with RCP policy service: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public allowMoveFilesToOwner(Z)Z
    .locals 7
    .param p1, "allow"    # Z

    .prologue
    .line 685
    iget-object v4, p0, Lcom/sec/enterprise/knox/container/RCPPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "RCPPolicy.allowMoveFilesToOwner"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 687
    const/4 v2, 0x0

    .line 688
    .local v2, "retVal":Z
    invoke-static {}, Lcom/sec/enterprise/knox/container/RCPPolicy;->getRCPPolicyService()Lcom/sec/enterprise/knox/container/IRCPPolicy;

    move-result-object v1

    .line 689
    .local v1, "rcpService":Lcom/sec/enterprise/knox/container/IRCPPolicy;
    if-nez v1, :cond_0

    .line 690
    const-string v4, "RCPPolicy"

    const-string v5, " RCP policy service is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 699
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 695
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/container/RCPPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v4, p1}, Lcom/sec/enterprise/knox/container/IRCPPolicy;->allowMoveFilesToOwner(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 699
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 696
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 697
    .local v0, "e":Landroid/os/RemoteException;
    const-string v4, "RCPPolicy"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed talking with RCP policy service: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public getAllowChangeDataSyncPolicy(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 7
    .param p1, "appName"    # Ljava/lang/String;
    .param p2, "syncProperty"    # Ljava/lang/String;

    .prologue
    .line 363
    iget-object v4, p0, Lcom/sec/enterprise/knox/container/RCPPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "RCPPolicy.getAllowChangeDataSyncPolicy"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 365
    const/4 v2, 0x0

    .line 366
    .local v2, "retVal":Z
    invoke-static {}, Lcom/sec/enterprise/knox/container/RCPPolicy;->getRCPPolicyService()Lcom/sec/enterprise/knox/container/IRCPPolicy;

    move-result-object v1

    .line 367
    .local v1, "rcpService":Lcom/sec/enterprise/knox/container/IRCPPolicy;
    if-nez v1, :cond_0

    .line 368
    const-string v4, "RCPPolicy"

    const-string v5, " RCP policy service is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 377
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 373
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/container/RCPPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v4, p1, p2}, Lcom/sec/enterprise/knox/container/IRCPPolicy;->getAllowChangeDataSyncPolicy(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 377
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 374
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 375
    .local v0, "e":Landroid/os/RemoteException;
    const-string v4, "RCPPolicy"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed talking with RCP policy service: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public getListFromAllowChangeDataSyncPolicy(Ljava/lang/String;Z)Ljava/util/List;
    .locals 7
    .param p1, "syncProperty"    # Ljava/lang/String;
    .param p2, "value"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 427
    iget-object v4, p0, Lcom/sec/enterprise/knox/container/RCPPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "RCPPolicy.getListFromAllowChangeDataSyncPolicy"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 429
    const/4 v2, 0x0

    .line 430
    .local v2, "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {}, Lcom/sec/enterprise/knox/container/RCPPolicy;->getRCPPolicyService()Lcom/sec/enterprise/knox/container/IRCPPolicy;

    move-result-object v1

    .line 431
    .local v1, "rcpService":Lcom/sec/enterprise/knox/container/IRCPPolicy;
    if-nez v1, :cond_0

    .line 432
    const-string v4, "RCPPolicy"

    const-string v5, " RCP policy service is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, v2

    .line 441
    .end local v2    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local v3, "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :goto_0
    return-object v3

    .line 437
    .end local v3    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v2    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/container/RCPPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v4, p1, p2}, Lcom/sec/enterprise/knox/container/IRCPPolicy;->getListFromAllowChangeDataSyncPolicy(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Z)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :goto_1
    move-object v3, v2

    .line 441
    .end local v2    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v3    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    goto :goto_0

    .line 438
    .end local v3    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v2    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :catch_0
    move-exception v0

    .line 439
    .local v0, "e":Landroid/os/RemoteException;
    const-string v4, "RCPPolicy"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed talking with RCP policy service: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public getNotificationSyncPolicy(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "syncProperty"    # Ljava/lang/String;

    .prologue
    .line 537
    iget-object v4, p0, Lcom/sec/enterprise/knox/container/RCPPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "RCPPolicy.getNotificationSyncPolicy"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 539
    const/4 v2, 0x0

    .line 540
    .local v2, "retVal":Ljava/lang/String;
    invoke-static {}, Lcom/sec/enterprise/knox/container/RCPPolicy;->getRCPPolicyService()Lcom/sec/enterprise/knox/container/IRCPPolicy;

    move-result-object v1

    .line 541
    .local v1, "rcpService":Lcom/sec/enterprise/knox/container/IRCPPolicy;
    if-nez v1, :cond_0

    .line 542
    const-string v4, "RCPPolicy"

    const-string v5, " RCP policy service is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, v2

    .line 551
    .end local v2    # "retVal":Ljava/lang/String;
    .local v3, "retVal":Ljava/lang/String;
    :goto_0
    return-object v3

    .line 547
    .end local v3    # "retVal":Ljava/lang/String;
    .restart local v2    # "retVal":Ljava/lang/String;
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/container/RCPPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v4, p1, p2}, Lcom/sec/enterprise/knox/container/IRCPPolicy;->getNotificationSyncPolicy(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :goto_1
    move-object v3, v2

    .line 551
    .end local v2    # "retVal":Ljava/lang/String;
    .restart local v3    # "retVal":Ljava/lang/String;
    goto :goto_0

    .line 548
    .end local v3    # "retVal":Ljava/lang/String;
    .restart local v2    # "retVal":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 549
    .local v0, "e":Landroid/os/RemoteException;
    const-string v4, "RCPPolicy"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed talking with RCP policy service: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public getPackagesFromNotificationSyncPolicy(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 7
    .param p1, "syncProperty"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 592
    iget-object v4, p0, Lcom/sec/enterprise/knox/container/RCPPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "RCPPolicy.getPackagesFromNotificationSyncPolicy"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 594
    const/4 v2, 0x0

    .line 595
    .local v2, "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {}, Lcom/sec/enterprise/knox/container/RCPPolicy;->getRCPPolicyService()Lcom/sec/enterprise/knox/container/IRCPPolicy;

    move-result-object v1

    .line 596
    .local v1, "rcpService":Lcom/sec/enterprise/knox/container/IRCPPolicy;
    if-nez v1, :cond_0

    .line 597
    const-string v4, "RCPPolicy"

    const-string v5, " RCP policy service is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, v2

    .line 606
    .end local v2    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local v3, "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :goto_0
    return-object v3

    .line 602
    .end local v3    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v2    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/container/RCPPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v4, p1, p2}, Lcom/sec/enterprise/knox/container/IRCPPolicy;->getPackagesFromNotificationSyncPolicy(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :goto_1
    move-object v3, v2

    .line 606
    .end local v2    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v3    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    goto :goto_0

    .line 603
    .end local v3    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v2    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :catch_0
    move-exception v0

    .line 604
    .local v0, "e":Landroid/os/RemoteException;
    const-string v4, "RCPPolicy"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed talking with RCP policy service: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public isMoveAppsToContainerAllowed()Z
    .locals 7

    .prologue
    .line 866
    iget-object v4, p0, Lcom/sec/enterprise/knox/container/RCPPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "RCPPolicy.isMoveAppsToContainerAllowed"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 868
    const/4 v2, 0x0

    .line 869
    .local v2, "retVal":Z
    invoke-static {}, Lcom/sec/enterprise/knox/container/RCPPolicy;->getRCPPolicyService()Lcom/sec/enterprise/knox/container/IRCPPolicy;

    move-result-object v1

    .line 871
    .local v1, "rcpService":Lcom/sec/enterprise/knox/container/IRCPPolicy;
    if-nez v1, :cond_0

    .line 872
    const-string v4, "RCPPolicy"

    const-string v5, " RCP policy service is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 881
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 877
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/container/RCPPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v4}, Lcom/sec/enterprise/knox/container/IRCPPolicy;->isMoveAppsToContainerAllowed(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 881
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 878
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 879
    .local v0, "e":Landroid/os/RemoteException;
    const-string v4, "RCPPolicy"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed talking with RCP policy service: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public isMoveFilesToContainerAllowed()Z
    .locals 7

    .prologue
    .line 730
    iget-object v4, p0, Lcom/sec/enterprise/knox/container/RCPPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "RCPPolicy.isMoveFilesToContainerAllowed"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 732
    const/4 v2, 0x0

    .line 733
    .local v2, "retVal":Z
    invoke-static {}, Lcom/sec/enterprise/knox/container/RCPPolicy;->getRCPPolicyService()Lcom/sec/enterprise/knox/container/IRCPPolicy;

    move-result-object v1

    .line 734
    .local v1, "rcpService":Lcom/sec/enterprise/knox/container/IRCPPolicy;
    if-nez v1, :cond_0

    .line 735
    const-string v4, "RCPPolicy"

    const-string v5, " RCP policy service is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 744
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 740
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/container/RCPPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v4}, Lcom/sec/enterprise/knox/container/IRCPPolicy;->isMoveFilesToContainerAllowed(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 744
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 741
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 742
    .local v0, "e":Landroid/os/RemoteException;
    const-string v4, "RCPPolicy"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed talking with RCP policy service: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public isMoveFilesToOwnerAllowed()Z
    .locals 7

    .prologue
    .line 775
    iget-object v4, p0, Lcom/sec/enterprise/knox/container/RCPPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "RCPPolicy.isMoveFilesToOwnerAllowed"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 777
    const/4 v2, 0x0

    .line 778
    .local v2, "retVal":Z
    invoke-static {}, Lcom/sec/enterprise/knox/container/RCPPolicy;->getRCPPolicyService()Lcom/sec/enterprise/knox/container/IRCPPolicy;

    move-result-object v1

    .line 779
    .local v1, "rcpService":Lcom/sec/enterprise/knox/container/IRCPPolicy;
    if-nez v1, :cond_0

    .line 780
    const-string v4, "RCPPolicy"

    const-string v5, " RCP policy service is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 789
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 785
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/container/RCPPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v4}, Lcom/sec/enterprise/knox/container/IRCPPolicy;->isMoveFilesToOwnerAllowed(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 789
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 786
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 787
    .local v0, "e":Landroid/os/RemoteException;
    const-string v4, "RCPPolicy"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed talking with RCP policy service: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public setAllowChangeDataSyncPolicy(Ljava/util/List;Ljava/lang/String;Z)Z
    .locals 7
    .param p2, "syncProperty"    # Ljava/lang/String;
    .param p3, "value"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Z)Z"
        }
    .end annotation

    .prologue
    .line 299
    .local p1, "appNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v4, p0, Lcom/sec/enterprise/knox/container/RCPPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "RCPPolicy.setAllowChangeDataSyncPolicy"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 301
    const/4 v2, 0x0

    .line 302
    .local v2, "retVal":Z
    invoke-static {}, Lcom/sec/enterprise/knox/container/RCPPolicy;->getRCPPolicyService()Lcom/sec/enterprise/knox/container/IRCPPolicy;

    move-result-object v1

    .line 303
    .local v1, "rcpService":Lcom/sec/enterprise/knox/container/IRCPPolicy;
    if-nez v1, :cond_0

    .line 304
    const-string v4, "RCPPolicy"

    const-string v5, " RCP policy service is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 313
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 309
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/container/RCPPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v4, p1, p2, p3}, Lcom/sec/enterprise/knox/container/IRCPPolicy;->setAllowChangeDataSyncPolicy(Landroid/app/enterprise/ContextInfo;Ljava/util/List;Ljava/lang/String;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 313
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 310
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 311
    .local v0, "e":Landroid/os/RemoteException;
    const-string v4, "RCPPolicy"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed talking with RCP policy service: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public setNotificationSyncPolicy(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 7
    .param p2, "syncProperty"    # Ljava/lang/String;
    .param p3, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 484
    .local p1, "packageNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v4, p0, Lcom/sec/enterprise/knox/container/RCPPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "RCPPolicy.setNotificationSyncPolicy"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 486
    const/4 v2, 0x0

    .line 487
    .local v2, "retVal":Z
    invoke-static {}, Lcom/sec/enterprise/knox/container/RCPPolicy;->getRCPPolicyService()Lcom/sec/enterprise/knox/container/IRCPPolicy;

    move-result-object v1

    .line 488
    .local v1, "rcpService":Lcom/sec/enterprise/knox/container/IRCPPolicy;
    if-nez v1, :cond_0

    .line 489
    const-string v4, "RCPPolicy"

    const-string v5, " RCP policy service is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 498
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 494
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/container/RCPPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v4, p1, p2, p3}, Lcom/sec/enterprise/knox/container/IRCPPolicy;->setNotificationSyncPolicy(Landroid/app/enterprise/ContextInfo;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 498
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 495
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 496
    .local v0, "e":Landroid/os/RemoteException;
    const-string v4, "RCPPolicy"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed talking with RCP policy service: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
