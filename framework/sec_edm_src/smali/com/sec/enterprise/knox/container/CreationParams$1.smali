.class final Lcom/sec/enterprise/knox/container/CreationParams$1;
.super Ljava/lang/Object;
.source "CreationParams.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/enterprise/knox/container/CreationParams;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/sec/enterprise/knox/container/CreationParams;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/sec/enterprise/knox/container/CreationParams;
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 56
    new-instance v0, Lcom/sec/enterprise/knox/container/CreationParams;

    invoke-direct {v0, p1}, Lcom/sec/enterprise/knox/container/CreationParams;-><init>(Landroid/os/Parcel;)V

    .line 57
    .local v0, "f":Lcom/sec/enterprise/knox/container/CreationParams;
    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Landroid/os/Parcel;

    .prologue
    .line 53
    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/container/CreationParams$1;->createFromParcel(Landroid/os/Parcel;)Lcom/sec/enterprise/knox/container/CreationParams;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/sec/enterprise/knox/container/CreationParams;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 62
    new-array v0, p1, [Lcom/sec/enterprise/knox/container/CreationParams;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 53
    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/container/CreationParams$1;->newArray(I)[Lcom/sec/enterprise/knox/container/CreationParams;

    move-result-object v0

    return-object v0
.end method
