.class public Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;
.super Ljava/lang/Object;
.source "ContainerConfigurationPolicy.java"


# static fields
.field public static final ERROR_INTERNAL_ERROR:I = -0x2

.field public static final ERROR_INVALID_KEY:I = -0x1

.field public static final OPTION_CALLER_INFO:Ljava/lang/String; = "option_callerinfo"

.field private static TAG:Ljava/lang/String;

.field private static gRestrictionService:Landroid/app/enterprise/IRestrictionPolicy;

.field private static mMUMContainerService:Lcom/sec/enterprise/knox/container/IKnoxContainerManager;


# instance fields
.field private mAppService:Landroid/app/enterprise/IApplicationPolicy;

.field private final mContextInfo:Landroid/app/enterprise/ContextInfo;

.field private mRemoteControlService:Landroid/app/enterprise/remotecontrol/IRemoteInjection;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 66
    const-string v0, "ContainerConfigurationPolicy"

    sput-object v0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    .line 70
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mMUMContainerService:Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    return-void
.end method

.method constructor <init>(Landroid/app/enterprise/ContextInfo;)V
    .locals 0
    .param p1, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;

    .prologue
    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    iput-object p1, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    .line 102
    return-void
.end method

.method private checkExternalSDCardAPICallerPermission()Z
    .locals 5

    .prologue
    .line 1728
    sget-object v2, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    const-string v3, "External SDCard API caller permission check"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1729
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->getInstance()Lcom/sec/enterprise/knox/EnterpriseKnoxManager;

    move-result-object v0

    .line 1730
    .local v0, "ekm":Lcom/sec/enterprise/knox/EnterpriseKnoxManager;
    invoke-virtual {v0}, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->getVersion()Lcom/sec/enterprise/knox/EnterpriseKnoxManager$EnterpriseKnoxSdkVersion;

    move-result-object v1

    .line 1731
    .local v1, "version":Lcom/sec/enterprise/knox/EnterpriseKnoxManager$EnterpriseKnoxSdkVersion;
    sget-object v2, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "current version : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", Required version : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/sec/enterprise/knox/EnterpriseKnoxManager$EnterpriseKnoxSdkVersion;->KNOX_ENTERPRISE_SDK_VERSION_2_2:Lcom/sec/enterprise/knox/EnterpriseKnoxManager$EnterpriseKnoxSdkVersion;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1732
    sget-object v2, Lcom/sec/enterprise/knox/EnterpriseKnoxManager$EnterpriseKnoxSdkVersion;->KNOX_ENTERPRISE_SDK_VERSION_2_2:Lcom/sec/enterprise/knox/EnterpriseKnoxManager$EnterpriseKnoxSdkVersion;

    invoke-virtual {v1, v2}, Lcom/sec/enterprise/knox/EnterpriseKnoxManager$EnterpriseKnoxSdkVersion;->compareTo(Ljava/lang/Enum;)I

    move-result v2

    if-ltz v2, :cond_0

    .line 1733
    const/4 v2, 0x1

    .line 1735
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private getAppService()Landroid/app/enterprise/IApplicationPolicy;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mAppService:Landroid/app/enterprise/IApplicationPolicy;

    if-nez v0, :cond_0

    .line 106
    const-string v0, "application_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/app/enterprise/IApplicationPolicy$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mAppService:Landroid/app/enterprise/IApplicationPolicy;

    .line 110
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mAppService:Landroid/app/enterprise/IApplicationPolicy;

    return-object v0
.end method

.method static declared-synchronized getMUMContainerService()Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    .locals 2

    .prologue
    .line 114
    const-class v1, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mMUMContainerService:Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    if-nez v0, :cond_0

    .line 115
    const-string v0, "mum_container_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mMUMContainerService:Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    .line 119
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mMUMContainerService:Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 114
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private getRemoteControlService()Landroid/app/enterprise/remotecontrol/IRemoteInjection;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mRemoteControlService:Landroid/app/enterprise/remotecontrol/IRemoteInjection;

    if-nez v0, :cond_0

    .line 133
    const-string v0, "remoteinjection"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/app/enterprise/remotecontrol/IRemoteInjection$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/remotecontrol/IRemoteInjection;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mRemoteControlService:Landroid/app/enterprise/remotecontrol/IRemoteInjection;

    .line 137
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mRemoteControlService:Landroid/app/enterprise/remotecontrol/IRemoteInjection;

    return-object v0
.end method

.method static declared-synchronized getRestrictionService()Landroid/app/enterprise/IRestrictionPolicy;
    .locals 2

    .prologue
    .line 123
    const-class v1, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->gRestrictionService:Landroid/app/enterprise/IRestrictionPolicy;

    if-nez v0, :cond_0

    .line 124
    const-string v0, "restriction_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->gRestrictionService:Landroid/app/enterprise/IRestrictionPolicy;

    .line 128
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->gRestrictionService:Landroid/app/enterprise/IRestrictionPolicy;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 123
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public addNetworkSSID(Ljava/lang/String;)Z
    .locals 5
    .param p1, "ssid"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 1784
    iget-object v3, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v4, "ContainerConfigurationPolicy.addNetworkSSID"

    invoke-static {v3, v4}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1786
    invoke-static {}, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->getMUMContainerService()Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    move-result-object v1

    .line 1788
    .local v1, "mumService":Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1802
    :cond_0
    :goto_0
    return v2

    .line 1792
    :cond_1
    if-nez v1, :cond_2

    .line 1793
    sget-object v3, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    const-string v4, "ContainerPolicy Service is not yet ready!!!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1798
    :cond_2
    :try_start_0
    iget-object v3, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v3, p1}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager;->addNetworkSSID(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    goto :goto_0

    .line 1799
    :catch_0
    move-exception v0

    .line 1800
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v3, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    const-string v4, "Failed at ContainerConfigurationPolicy API addNetworkSSID"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public addPackageToExternalStorageSBABlackList(Ljava/lang/String;)Z
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 1546
    iget-object v3, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v4, "ContainerConfigurationPolicy.addPackageToExternalStorageSBABlackList"

    invoke-static {v3, v4}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1548
    invoke-direct {p0}, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->checkExternalSDCardAPICallerPermission()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1563
    :goto_0
    return v2

    .line 1552
    :cond_0
    invoke-static {}, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->getMUMContainerService()Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    move-result-object v1

    .line 1553
    .local v1, "mumService":Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    if-nez v1, :cond_1

    .line 1554
    sget-object v3, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    const-string v4, "ContainerPolicy Service is not yet ready!!!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1559
    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v3, p1}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager;->addPackageToExternalStorageSBABlackList(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    goto :goto_0

    .line 1560
    :catch_0
    move-exception v0

    .line 1561
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v3, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    const-string v4, "Failed at ContainerConfigurationPolicy API addPackageToExternalStorageSBABlackList"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public addPackageToExternalStorageWhiteList(Ljava/lang/String;[Landroid/content/pm/Signature;)Z
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "signatures"    # [Landroid/content/pm/Signature;

    .prologue
    const/4 v2, 0x0

    .line 1278
    iget-object v3, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v4, "ContainerConfigurationPolicy.addPackageToExternalStorageWhiteList"

    invoke-static {v3, v4}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1280
    invoke-direct {p0}, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->checkExternalSDCardAPICallerPermission()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1295
    :goto_0
    return v2

    .line 1284
    :cond_0
    invoke-static {}, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->getMUMContainerService()Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    move-result-object v1

    .line 1285
    .local v1, "mumService":Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    if-nez v1, :cond_1

    .line 1286
    sget-object v3, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    const-string v4, "ContainerPolicy Service is not yet ready!!!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1291
    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v3, p1, p2}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager;->addPackageToExternalStorageWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;[Landroid/content/pm/Signature;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    goto :goto_0

    .line 1292
    :catch_0
    move-exception v0

    .line 1293
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v3, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    const-string v4, "Failed at ContainerConfigurationPolicy API addPackageToExternalStorageWhiteList"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public addPackageToInstallWhiteList(Ljava/lang/String;)Z
    .locals 6
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 195
    iget-object v4, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerConfigurationPolicy.addPackageToInstallWhiteList"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 196
    const/4 v2, 0x0

    .line 197
    .local v2, "retVal":Z
    invoke-direct {p0}, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->getAppService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v0

    .line 198
    .local v0, "appService":Landroid/app/enterprise/IApplicationPolicy;
    if-nez v0, :cond_0

    .line 199
    sget-object v4, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Application PolicyService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 209
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 204
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v0, v4, p1}, Landroid/app/enterprise/IApplicationPolicy;->addPackageToInstallWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 209
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 205
    .end local v3    # "retVal":I
    :catch_0
    move-exception v1

    .line 206
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed at ContainerConfigurationPolicy API addPackageToInstallWhiteList "

    invoke-static {v4, v5, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public allowRemoteControl(Z)Z
    .locals 6
    .param p1, "state"    # Z

    .prologue
    .line 254
    iget-object v4, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerConfigurationPolicy.allowRemoteControl"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 255
    const/4 v2, 0x0

    .line 256
    .local v2, "retVal":Z
    invoke-direct {p0}, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->getRemoteControlService()Landroid/app/enterprise/remotecontrol/IRemoteInjection;

    move-result-object v1

    .line 257
    .local v1, "remoteControlService":Landroid/app/enterprise/remotecontrol/IRemoteInjection;
    if-nez v1, :cond_0

    .line 258
    sget-object v4, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Remote Control Service is not yet ready"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 267
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 263
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v4, p1}, Landroid/app/enterprise/remotecontrol/IRemoteInjection;->allowRemoteControl(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 267
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 264
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 265
    .local v0, "e":Ljava/lang/Exception;
    sget-object v4, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed at ContainerConfigurationPolicy API allowRemoteControl "

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public clearNetworkSSID()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1969
    iget-object v3, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v4, "ContainerConfigurationPolicy.clearNetworkSSID"

    invoke-static {v3, v4}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1971
    invoke-static {}, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->getMUMContainerService()Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    move-result-object v1

    .line 1972
    .local v1, "mumService":Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    if-nez v1, :cond_0

    .line 1973
    sget-object v3, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    const-string v4, "ContainerPolicy Service is not yet ready!!!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1982
    :goto_0
    return v2

    .line 1978
    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v3}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager;->clearNetworkSSID(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    goto :goto_0

    .line 1979
    :catch_0
    move-exception v0

    .line 1980
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v3, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    const-string v4, "Failed at ContainerConfigurationPolicy API clearNetworkSSID"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public clearPackagesFromExternalStorageSBABlackList()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1707
    iget-object v3, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v4, "ContainerConfigurationPolicy.clearPackagesFromExternalStorageSBABlackList"

    invoke-static {v3, v4}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1709
    invoke-direct {p0}, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->checkExternalSDCardAPICallerPermission()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1724
    :goto_0
    return v2

    .line 1713
    :cond_0
    invoke-static {}, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->getMUMContainerService()Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    move-result-object v1

    .line 1714
    .local v1, "mumService":Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    if-nez v1, :cond_1

    .line 1715
    sget-object v3, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    const-string v4, "ContainerPolicy Service is not yet ready!!!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1720
    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v3}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager;->clearPackagesFromExternalStorageSBABlackList(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    goto :goto_0

    .line 1721
    :catch_0
    move-exception v0

    .line 1722
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v3, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    const-string v4, "Failed at ContainerConfigurationPolicy API clearPackagesFromExternalStorageSBABlackList"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public clearPackagesFromExternalStorageWhiteList()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1492
    iget-object v3, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v4, "ContainerConfigurationPolicy.clearPackagesFromExternalStorageWhiteList"

    invoke-static {v3, v4}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1494
    invoke-direct {p0}, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->checkExternalSDCardAPICallerPermission()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1509
    :goto_0
    return v2

    .line 1498
    :cond_0
    invoke-static {}, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->getMUMContainerService()Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    move-result-object v1

    .line 1499
    .local v1, "mumService":Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    if-nez v1, :cond_1

    .line 1500
    sget-object v3, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    const-string v4, "ContainerPolicy Service is not yet ready!!!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1505
    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v3}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager;->clearPackagesFromExternalStorageWhiteList(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    goto :goto_0

    .line 1506
    :catch_0
    move-exception v0

    .line 1507
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v3, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    const-string v4, "Failed at ContainerConfigurationPolicy API clearPackagesFromExternalStorageWhiteList"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public enableExternalStorage(Z)Z
    .locals 5
    .param p1, "value"    # Z

    .prologue
    const/4 v2, 0x0

    .line 1200
    iget-object v3, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v4, "ContainerConfigurationPolicy.enableExternalStorage"

    invoke-static {v3, v4}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1202
    invoke-direct {p0}, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->checkExternalSDCardAPICallerPermission()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1217
    :goto_0
    return v2

    .line 1206
    :cond_0
    invoke-static {}, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->getMUMContainerService()Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    move-result-object v1

    .line 1207
    .local v1, "mumService":Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    if-nez v1, :cond_1

    .line 1208
    sget-object v3, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    const-string v4, "ContainerPolicy Service is not yet ready!!!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1213
    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v3, p1}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager;->enableExternalStorage(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    goto :goto_0

    .line 1214
    :catch_0
    move-exception v0

    .line 1215
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v3, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    const-string v4, "Failed at ContainerConfigurationPolicy API enableExternalStorage"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public enforceMultifactorAuthentication(Z)V
    .locals 5
    .param p1, "enforce"    # Z

    .prologue
    .line 772
    iget-object v3, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v4, "KnoxContainerManager.enforceMultifactorAuthentication"

    invoke-static {v3, v4}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 774
    const/4 v1, 0x0

    .line 775
    .local v1, "retVal":Z
    invoke-static {}, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->getMUMContainerService()Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    move-result-object v2

    .line 776
    .local v2, "service":Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    if-nez v2, :cond_0

    .line 777
    sget-object v3, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    const-string v4, "ContainerPolicy Service is not yet ready!!!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 786
    :goto_0
    return-void

    .line 782
    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3, p1}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager;->enforceMultifactorAuthentication(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 783
    :catch_0
    move-exception v0

    .line 784
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v3, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    const-string v4, "Failed at KnoxContainerManager API unlock "

    invoke-static {v3, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public getEnforceAuthForContainer()Z
    .locals 6

    .prologue
    .line 716
    iget-object v4, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "KnoxContainerManager.getEnforceAuthForContainer"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 718
    const/4 v1, 0x0

    .line 719
    .local v1, "retVal":Z
    invoke-static {}, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->getMUMContainerService()Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    move-result-object v3

    .line 720
    .local v3, "service":Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    if-nez v3, :cond_0

    .line 721
    sget-object v4, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "ContainerPolicy Service is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v1

    .line 731
    .end local v1    # "retVal":Z
    .local v2, "retVal":I
    :goto_0
    return v2

    .line 726
    .end local v2    # "retVal":I
    .restart local v1    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v3, v4}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager;->getEnforceAuthForContainer(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_1
    move v2, v1

    .line 731
    .restart local v2    # "retVal":I
    goto :goto_0

    .line 727
    .end local v2    # "retVal":I
    :catch_0
    move-exception v0

    .line 728
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed at KnoxContainerManager API unlock "

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public getHibernationTimeout()J
    .locals 8

    .prologue
    .line 990
    iget-object v6, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v7, "ContainerPasswordPolicy.getHibernationTimeout"

    invoke-static {v6, v7}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 992
    const-wide/16 v2, 0x0

    .line 994
    .local v2, "retVal":J
    invoke-static {}, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->getMUMContainerService()Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    move-result-object v1

    .line 995
    .local v1, "mumService":Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    if-nez v1, :cond_0

    .line 996
    sget-object v6, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    const-string v7, "ContainerPolicy Service is not yet ready!!!"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-wide v4, v2

    .line 1006
    .end local v2    # "retVal":J
    .local v4, "retVal":J
    :goto_0
    return-wide v4

    .line 1001
    .end local v4    # "retVal":J
    .restart local v2    # "retVal":J
    :cond_0
    :try_start_0
    iget-object v6, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v6}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager;->getHibernationTimeout(Landroid/app/enterprise/ContextInfo;)J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    :goto_1
    move-wide v4, v2

    .line 1006
    .end local v2    # "retVal":J
    .restart local v4    # "retVal":J
    goto :goto_0

    .line 1002
    .end local v4    # "retVal":J
    .restart local v2    # "retVal":J
    :catch_0
    move-exception v0

    .line 1003
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v6, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    const-string v7, "Failed talking with ContainerPolicy "

    invoke-static {v6, v7, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public getNetworkSSID()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 1910
    iget-object v2, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "ContainerConfigurationPolicy.getNetworkSSID"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1912
    invoke-static {}, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->getMUMContainerService()Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    move-result-object v1

    .line 1913
    .local v1, "mumService":Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    if-nez v1, :cond_0

    .line 1914
    sget-object v2, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    const-string v3, "ContainerPolicy Service is not yet ready!!!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1915
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 1923
    :goto_0
    return-object v2

    .line 1919
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager;->getNetworkSSID(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    .line 1920
    :catch_0
    move-exception v0

    .line 1921
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    const-string v3, "Failed at ContainerConfigurationPolicy API getNetworkSSID"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1923
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v4}, Ljava/util/ArrayList;-><init>(I)V

    goto :goto_0
.end method

.method public getPackageSignaturesFromExternalStorageWhiteList(Ljava/lang/String;)[Landroid/content/pm/Signature;
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 1384
    iget-object v3, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v4, "ContainerConfigurationPolicy.getPackageSignaturesFromExternalStorageWhiteList"

    invoke-static {v3, v4}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1386
    invoke-direct {p0}, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->checkExternalSDCardAPICallerPermission()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1401
    :goto_0
    return-object v2

    .line 1390
    :cond_0
    invoke-static {}, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->getMUMContainerService()Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    move-result-object v1

    .line 1391
    .local v1, "mumService":Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    if-nez v1, :cond_1

    .line 1392
    sget-object v3, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    const-string v4, "ContainerPolicy Service is not yet ready!!!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1397
    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v3, p1}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager;->getPackageSignaturesFromExternalStorageWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)[Landroid/content/pm/Signature;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    .line 1398
    :catch_0
    move-exception v0

    .line 1399
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v3, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    const-string v4, "Failed at ContainerConfigurationPolicy API getPackageSignaturesFromExternalStorageWhiteList"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public getPackagesFromExternalStorageSBABlackList()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1599
    iget-object v3, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v4, "ContainerConfigurationPolicy.getPackagesFromExternalStorageSBABlackList"

    invoke-static {v3, v4}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1601
    invoke-direct {p0}, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->checkExternalSDCardAPICallerPermission()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1616
    :goto_0
    return-object v2

    .line 1605
    :cond_0
    invoke-static {}, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->getMUMContainerService()Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    move-result-object v1

    .line 1606
    .local v1, "mumService":Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    if-nez v1, :cond_1

    .line 1607
    sget-object v3, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    const-string v4, "ContainerPolicy Service is not yet ready!!!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1612
    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v3}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager;->getPackagesFromExternalStorageSBABlackList(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    .line 1613
    :catch_0
    move-exception v0

    .line 1614
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v3, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    const-string v4, "Failed at ContainerConfigurationPolicy API getPackagesFromExternalStorageSBABlackList"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public getPackagesFromExternalStorageWhiteList()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1331
    iget-object v3, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v4, "ContainerConfigurationPolicy.getPackagesFromExternalStorageWhiteList"

    invoke-static {v3, v4}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1333
    invoke-direct {p0}, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->checkExternalSDCardAPICallerPermission()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1348
    :goto_0
    return-object v2

    .line 1337
    :cond_0
    invoke-static {}, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->getMUMContainerService()Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    move-result-object v1

    .line 1338
    .local v1, "mumService":Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    if-nez v1, :cond_1

    .line 1339
    sget-object v3, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    const-string v4, "ContainerPolicy Service is not yet ready!!!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1344
    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v3}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager;->getPackagesFromExternalStorageWhiteList(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    .line 1345
    :catch_0
    move-exception v0

    .line 1346
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v3, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    const-string v4, "Failed at ContainerConfigurationPolicy API getPackagesFromExternalStorageWhiteList"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public getPackagesFromInstallWhiteList()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 375
    iget-object v4, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerConfigurationPolicy.getPackagesFromInstallWhiteList"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 376
    const/4 v2, 0x0

    .line 377
    .local v2, "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->getAppService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v0

    .line 378
    .local v0, "appService":Landroid/app/enterprise/IApplicationPolicy;
    if-nez v0, :cond_0

    .line 379
    sget-object v4, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Application PolicyService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, v2

    .line 390
    .end local v2    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local v3, "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :goto_0
    return-object v3

    .line 384
    .end local v3    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v2    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v0, v4}, Landroid/app/enterprise/IApplicationPolicy;->getPackagesFromInstallWhiteList(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :goto_1
    move-object v3, v2

    .line 390
    .end local v2    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v3    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    goto :goto_0

    .line 385
    .end local v3    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v2    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :catch_0
    move-exception v1

    .line 386
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed at ContainerConfigurationPolicy API getPackagesFromInstallWhiteList "

    invoke-static {v4, v5, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public isExternalStorageEnabled()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1222
    iget-object v3, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v4, "ContainerConfigurationPolicy.isExternalStorageEnabled"

    invoke-static {v3, v4}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1224
    invoke-direct {p0}, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->checkExternalSDCardAPICallerPermission()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1239
    :goto_0
    return v2

    .line 1228
    :cond_0
    invoke-static {}, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->getMUMContainerService()Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    move-result-object v1

    .line 1229
    .local v1, "mumService":Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    if-nez v1, :cond_1

    .line 1230
    sget-object v3, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    const-string v4, "ContainerPolicy Service is not yet ready!!!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1235
    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v3}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager;->isExternalStorageEnabled(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    goto :goto_0

    .line 1236
    :catch_0
    move-exception v0

    .line 1237
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v3, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    const-string v4, "Failed at ContainerConfigurationPolicy API enableExternalStorage"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public isMultifactorAuthenticationEnforced()Z
    .locals 6

    .prologue
    .line 826
    iget-object v4, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "KnoxContainerManager.isMultifactorAuthenticationEnforced"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 828
    const/4 v1, 0x0

    .line 829
    .local v1, "retVal":Z
    invoke-static {}, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->getMUMContainerService()Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    move-result-object v3

    .line 830
    .local v3, "service":Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    if-nez v3, :cond_0

    .line 831
    sget-object v4, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "ContainerPolicy Service is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v1

    .line 841
    .end local v1    # "retVal":Z
    .local v2, "retVal":I
    :goto_0
    return v2

    .line 836
    .end local v2    # "retVal":I
    .restart local v1    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v3, v4}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager;->isMultifactorAuthenticationEnforced(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_1
    move v2, v1

    .line 841
    .restart local v2    # "retVal":I
    goto :goto_0

    .line 837
    .end local v2    # "retVal":I
    :catch_0
    move-exception v0

    .line 838
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed at KnoxContainerManager API unlock "

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public isPackageInInstallWhiteList(Ljava/lang/String;)Z
    .locals 6
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 441
    iget-object v4, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerConfigurationPolicy.isPackageInInstallWhiteList"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 442
    const/4 v2, 0x0

    .line 443
    .local v2, "retVal":Z
    invoke-direct {p0}, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->getAppService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v0

    .line 444
    .local v0, "appService":Landroid/app/enterprise/IApplicationPolicy;
    if-nez v0, :cond_0

    .line 445
    sget-object v4, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Application PolicyService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 455
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 450
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v0, v4, p1}, Landroid/app/enterprise/IApplicationPolicy;->isPackageInInstallWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 455
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 451
    .end local v3    # "retVal":I
    :catch_0
    move-exception v1

    .line 452
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed at ContainerConfigurationPolicy API isPackageInInstallWhiteList "

    invoke-static {v4, v5, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public isRemoteControlAllowed()Z
    .locals 6

    .prologue
    .line 312
    iget-object v4, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerConfigurationPolicy.isRemoteControlAllowed"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 313
    const/4 v2, 0x0

    .line 314
    .local v2, "retVal":Z
    invoke-direct {p0}, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->getRemoteControlService()Landroid/app/enterprise/remotecontrol/IRemoteInjection;

    move-result-object v1

    .line 315
    .local v1, "remoteControlService":Landroid/app/enterprise/remotecontrol/IRemoteInjection;
    if-nez v1, :cond_0

    .line 316
    sget-object v4, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Remote Control Service is not yet ready"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 325
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 321
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v4}, Landroid/app/enterprise/remotecontrol/IRemoteInjection;->isRemoteControlAllowed(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 325
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 322
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 323
    .local v0, "e":Ljava/lang/Exception;
    sget-object v4, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed at ContainerConfigurationPolicy API isRemoteControlAllowed "

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public isResetContainerOnRebootEnabled()Z
    .locals 6

    .prologue
    .line 1148
    iget-object v4, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerConfigurationPolicy.isResetContainerOnRebootEnabled"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1150
    const/4 v2, 0x0

    .line 1152
    .local v2, "retVal":Z
    invoke-static {}, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->getMUMContainerService()Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    move-result-object v1

    .line 1153
    .local v1, "mumService":Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    if-nez v1, :cond_0

    .line 1154
    sget-object v4, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "ContainerPolicy Service is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 1164
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 1159
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v4}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager;->isResetContainerOnRebootEnabled(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 1164
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 1160
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 1161
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed talking with ContainerConfigurationPolicy "

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public isSettingsOptionEnabled(Ljava/lang/String;)Z
    .locals 5
    .param p1, "option"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 2086
    invoke-static {}, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->getMUMContainerService()Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    move-result-object v1

    .line 2087
    .local v1, "mumService":Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    if-nez v1, :cond_0

    .line 2088
    sget-object v3, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    const-string v4, "ContainerPolicy Service is not yet ready!!!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2096
    :goto_0
    return v2

    .line 2092
    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v3, p1}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager;->isSettingsOptionEnabled(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    goto :goto_0

    .line 2093
    :catch_0
    move-exception v0

    .line 2094
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v3, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    const-string v4, "Failed at ContainerConfigurationPolicy API isSettingsOptionEnabled"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public isUseSecureKeypadEnabled()Z
    .locals 6

    .prologue
    .line 605
    iget-object v4, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerRestrictionPolicy.isUseSecureKeypadEnabled"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 607
    const/4 v2, 0x0

    .line 608
    .local v2, "retVal":Z
    invoke-static {}, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->getRestrictionService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    .line 609
    .local v1, "restrictionService":Landroid/app/enterprise/IRestrictionPolicy;
    if-nez v1, :cond_0

    .line 610
    sget-object v4, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "ContainerRestriction PolicyService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 619
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 615
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v4}, Landroid/app/enterprise/IRestrictionPolicy;->isUseSecureKeypadEnabled(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 619
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 616
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 617
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed talking with misc policy"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public removeNetworkSSID(Ljava/lang/String;)Z
    .locals 5
    .param p1, "ssid"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 1849
    iget-object v3, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v4, "ContainerConfigurationPolicy.removeNetworkSSID"

    invoke-static {v3, v4}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1850
    invoke-static {}, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->getMUMContainerService()Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    move-result-object v1

    .line 1852
    .local v1, "mumService":Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1866
    :cond_0
    :goto_0
    return v2

    .line 1856
    :cond_1
    if-nez v1, :cond_2

    .line 1857
    sget-object v3, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    const-string v4, "ContainerPolicy Service is not yet ready!!!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1862
    :cond_2
    :try_start_0
    iget-object v3, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v3, p1}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager;->removeNetworkSSID(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    goto :goto_0

    .line 1863
    :catch_0
    move-exception v0

    .line 1864
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v3, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    const-string v4, "Failed at ContainerConfigurationPolicy API removeNetworkSSID"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public removePackageFromExternalStorageSBABlackList(Ljava/lang/String;)Z
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 1654
    iget-object v3, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v4, "ContainerConfigurationPolicy.removePackageFromExternalStorageSBABlackList"

    invoke-static {v3, v4}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1656
    invoke-direct {p0}, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->checkExternalSDCardAPICallerPermission()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1671
    :goto_0
    return v2

    .line 1660
    :cond_0
    invoke-static {}, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->getMUMContainerService()Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    move-result-object v1

    .line 1661
    .local v1, "mumService":Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    if-nez v1, :cond_1

    .line 1662
    sget-object v3, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    const-string v4, "ContainerPolicy Service is not yet ready!!!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1667
    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v3, p1}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager;->removePackageFromExternalStorageSBABlackList(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    goto :goto_0

    .line 1668
    :catch_0
    move-exception v0

    .line 1669
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v3, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    const-string v4, "Failed at ContainerConfigurationPolicy API removePackageFromExternalStorageSBABlackList"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public removePackageFromExternalStorageWhiteList(Ljava/lang/String;)Z
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 1439
    iget-object v3, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v4, "ContainerConfigurationPolicy.removePackageFromExternalStorageWhiteList"

    invoke-static {v3, v4}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1441
    invoke-direct {p0}, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->checkExternalSDCardAPICallerPermission()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1456
    :goto_0
    return v2

    .line 1445
    :cond_0
    invoke-static {}, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->getMUMContainerService()Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    move-result-object v1

    .line 1446
    .local v1, "mumService":Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    if-nez v1, :cond_1

    .line 1447
    sget-object v3, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    const-string v4, "ContainerPolicy Service is not yet ready!!!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1452
    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v3, p1}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager;->removePackageFromExternalStorageWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    goto :goto_0

    .line 1453
    :catch_0
    move-exception v0

    .line 1454
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v3, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    const-string v4, "Failed at ContainerConfigurationPolicy API removePackageFromExternalStorageWhiteList"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public removePackageFromInstallWhiteList(Ljava/lang/String;)Z
    .locals 6
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 512
    iget-object v4, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerConfigurationPolicy.removePackageFromInstallWhiteList"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 514
    const/4 v2, 0x0

    .line 515
    .local v2, "retVal":Z
    invoke-direct {p0}, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->getAppService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v0

    .line 516
    .local v0, "appService":Landroid/app/enterprise/IApplicationPolicy;
    if-nez v0, :cond_0

    .line 517
    sget-object v4, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Application PolicyService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 529
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 522
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v0, v4, p1}, Landroid/app/enterprise/IApplicationPolicy;->removePackageFromInstallWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 529
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 523
    .end local v3    # "retVal":I
    :catch_0
    move-exception v1

    .line 524
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed at ContainerConfigurationPolicy API removePackageFromInstallWhiteList "

    invoke-static {v4, v5, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public resetContainerOnReboot(Z)Z
    .locals 6
    .param p1, "enable"    # Z

    .prologue
    .line 1098
    iget-object v4, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerConfigurationPolicy.resetContainerOnReboot"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1100
    const/4 v2, 0x0

    .line 1102
    .local v2, "retVal":Z
    invoke-static {}, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->getMUMContainerService()Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    move-result-object v1

    .line 1103
    .local v1, "mumService":Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    if-nez v1, :cond_0

    .line 1104
    sget-object v4, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "ContainerPolicy Service is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 1114
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 1109
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v4, p1}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager;->resetContainerOnReboot(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 1114
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 1110
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 1111
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed talking with ContainerConfigurationPolicy "

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public resetContainerPassword(Ljava/lang/String;I)I
    .locals 6
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "timeout"    # I

    .prologue
    .line 939
    iget-object v4, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerPasswordPolicy.resetPassword"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 941
    const/4 v2, -0x2

    .line 943
    .local v2, "retVal":I
    invoke-static {}, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->getMUMContainerService()Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    move-result-object v1

    .line 944
    .local v1, "mumService":Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    if-nez v1, :cond_0

    .line 945
    sget-object v4, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "ContainerPolicy Service is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 955
    .end local v2    # "retVal":I
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 950
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":I
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v4, p1, p2}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager;->forceResetPassword(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;I)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 955
    .end local v2    # "retVal":I
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 951
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":I
    :catch_0
    move-exception v0

    .line 952
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed talking with ContainerPasswordPolicy "

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public resetContainerPassword()Z
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 881
    iget-object v4, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerPasswordPolicy.resetPassword"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 883
    const/4 v2, -0x2

    .line 885
    .local v2, "retVal":I
    invoke-static {}, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->getMUMContainerService()Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    move-result-object v1

    .line 886
    .local v1, "mumService":Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    if-nez v1, :cond_1

    .line 887
    sget-object v4, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "ContainerPolicy Service is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 900
    :cond_0
    :goto_0
    return v3

    .line 892
    :cond_1
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-interface {v1, v4, v5, v6}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager;->forceResetPassword(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;I)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 897
    :goto_1
    if-ltz v2, :cond_0

    .line 898
    const/4 v3, 0x1

    goto :goto_0

    .line 893
    :catch_0
    move-exception v0

    .line 894
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed talking with ContainerPasswordPolicy "

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public setEnforceAuthForContainer(Z)Z
    .locals 6
    .param p1, "enforce"    # Z

    .prologue
    .line 660
    iget-object v4, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "KnoxContainerManager.setEnforceAuthForContainer"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 662
    const/4 v1, 0x0

    .line 663
    .local v1, "retVal":Z
    invoke-static {}, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->getMUMContainerService()Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    move-result-object v3

    .line 664
    .local v3, "service":Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    if-nez v3, :cond_0

    .line 665
    sget-object v4, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "ContainerPolicy Service is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v1

    .line 675
    .end local v1    # "retVal":Z
    .local v2, "retVal":I
    :goto_0
    return v2

    .line 670
    .end local v2    # "retVal":I
    .restart local v1    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v3, v4, p1}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager;->setEnforceAuthForContainer(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_1
    move v2, v1

    .line 675
    .restart local v2    # "retVal":I
    goto :goto_0

    .line 671
    .end local v2    # "retVal":I
    :catch_0
    move-exception v0

    .line 672
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed at KnoxContainerManager API unlock "

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public setHibernationTimeout(J)Z
    .locals 7
    .param p1, "time"    # J

    .prologue
    .line 1046
    iget-object v4, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerPasswordPolicy.setHibernationTimeout"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1048
    const/4 v2, 0x0

    .line 1050
    .local v2, "result":Z
    const-wide/32 v4, 0x493e0

    cmp-long v4, p1, v4

    if-ltz v4, :cond_1

    const-wide/32 v4, 0x5265c00

    cmp-long v4, p1, v4

    if-gtz v4, :cond_1

    .line 1051
    invoke-static {}, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->getMUMContainerService()Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    move-result-object v1

    .line 1052
    .local v1, "mumService":Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    if-nez v1, :cond_0

    .line 1053
    sget-object v4, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "ContainerPolicy Service is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 1064
    .end local v1    # "mumService":Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    .end local v2    # "result":Z
    .local v3, "result":I
    :goto_0
    return v3

    .line 1058
    .end local v3    # "result":I
    .restart local v1    # "mumService":Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    .restart local v2    # "result":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v4, p1, p2}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager;->setHibernationTimeout(Landroid/app/enterprise/ContextInfo;J)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .end local v1    # "mumService":Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    :cond_1
    :goto_1
    move v3, v2

    .line 1064
    .restart local v3    # "result":I
    goto :goto_0

    .line 1059
    .end local v3    # "result":I
    .restart local v1    # "mumService":Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    :catch_0
    move-exception v0

    .line 1060
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed talking with ContainerPolicy "

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public setSettingsOptionEnabled(Ljava/lang/String;Z)Z
    .locals 5
    .param p1, "option"    # Ljava/lang/String;
    .param p2, "enable"    # Z

    .prologue
    const/4 v2, 0x0

    .line 2030
    iget-object v3, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v4, "ContainerConfigurationPolicy.setSettingsOptionEnabled"

    invoke-static {v3, v4}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2032
    invoke-static {}, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->getMUMContainerService()Lcom/sec/enterprise/knox/container/IKnoxContainerManager;

    move-result-object v1

    .line 2033
    .local v1, "mumService":Lcom/sec/enterprise/knox/container/IKnoxContainerManager;
    if-nez v1, :cond_0

    .line 2034
    sget-object v3, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    const-string v4, "ContainerPolicy Service is not yet ready!!!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2042
    :goto_0
    return v2

    .line 2038
    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v3, p1, p2}, Lcom/sec/enterprise/knox/container/IKnoxContainerManager;->setSettingsOptionEnabled(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    goto :goto_0

    .line 2039
    :catch_0
    move-exception v0

    .line 2040
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v3, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    const-string v4, "Failed at ContainerConfigurationPolicy API setSettingsOptionEnabled"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public setUseSecureKeypad(Z)Z
    .locals 6
    .param p1, "enable"    # Z

    .prologue
    .line 573
    iget-object v4, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerRestrictionPolicy.setUseSecureKeypad"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 575
    const/4 v2, 0x0

    .line 576
    .local v2, "retVal":Z
    invoke-static {}, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->getRestrictionService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    .line 577
    .local v1, "restrictionService":Landroid/app/enterprise/IRestrictionPolicy;
    if-nez v1, :cond_0

    .line 578
    sget-object v4, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "ContainerRestriction PolicyService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 587
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 583
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v4, p1}, Landroid/app/enterprise/IRestrictionPolicy;->setUseSecureKeypad(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 587
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 584
    .end local v3    # "retVal":I
    :catch_0
    move-exception v0

    .line 585
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/container/ContainerConfigurationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed talking with misc policy"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method
