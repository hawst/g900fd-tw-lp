.class final Lcom/sec/enterprise/knox/container/LightweightConfigurationType$1;
.super Ljava/lang/Object;
.source "LightweightConfigurationType.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/enterprise/knox/container/LightweightConfigurationType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/sec/enterprise/knox/container/LightweightConfigurationType;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 141
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/sec/enterprise/knox/container/LightweightConfigurationType;
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 144
    new-instance v0, Lcom/sec/enterprise/knox/container/LightweightConfigurationType;

    invoke-direct {v0, p1}, Lcom/sec/enterprise/knox/container/LightweightConfigurationType;-><init>(Landroid/os/Parcel;)V

    .line 145
    .local v0, "f":Lcom/sec/enterprise/knox/container/LightweightConfigurationType;
    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Landroid/os/Parcel;

    .prologue
    .line 141
    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/container/LightweightConfigurationType$1;->createFromParcel(Landroid/os/Parcel;)Lcom/sec/enterprise/knox/container/LightweightConfigurationType;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/sec/enterprise/knox/container/LightweightConfigurationType;
    .locals 2
    .param p1, "size"    # I

    .prologue
    .line 150
    const-string v0, "LightweightConfigurationType"

    const-string v1, "LightweightConfigurationType[] array to be created"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    new-array v0, p1, [Lcom/sec/enterprise/knox/container/LightweightConfigurationType;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 141
    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/container/LightweightConfigurationType$1;->newArray(I)[Lcom/sec/enterprise/knox/container/LightweightConfigurationType;

    move-result-object v0

    return-object v0
.end method
