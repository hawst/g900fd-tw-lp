.class public Lcom/sec/enterprise/knox/container/SmartCardAccessPolicy;
.super Ljava/lang/Object;
.source "SmartCardAccessPolicy.java"


# static fields
.field public static final ALLOW_BT_SECURE_ACCESS_FAILURE:I = 0x1

.field public static final ALLOW_BT_SECURE_ACCESS_NOT_APPLICABLE:I = 0x3

.field public static final ALLOW_BT_SECURE_ACCESS_SUCCESS:I = 0x0

.field public static final BT_SECURE_MODE_DISABLED:I = 0x2

.field private static mSmartCardAccessPolicy:Lcom/sec/enterprise/knox/container/SmartCardAccessPolicy;


# instance fields
.field protected TAG:Ljava/lang/String;

.field private lService:Lcom/sec/enterprise/knox/container/ISmartCardAccessPolicy;

.field private mContextInfo:Landroid/app/enterprise/ContextInfo;


# direct methods
.method public constructor <init>(Landroid/app/enterprise/ContextInfo;)V
    .locals 3
    .param p1, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;

    .prologue
    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    const-string v0, "SmartCardAccessPolicy"

    iput-object v0, p0, Lcom/sec/enterprise/knox/container/SmartCardAccessPolicy;->TAG:Ljava/lang/String;

    .line 98
    iput-object p1, p0, Lcom/sec/enterprise/knox/container/SmartCardAccessPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    .line 99
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/SmartCardAccessPolicy;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SmartCardAccessPolicy API ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/container/SmartCardAccessPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget v2, v2, Landroid/app/enterprise/ContextInfo;->mContainerId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/container/SmartCardAccessPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget v2, v2, Landroid/app/enterprise/ContextInfo;->mCallerUid:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    return-void
.end method

.method private getService()Lcom/sec/enterprise/knox/container/ISmartCardAccessPolicy;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/SmartCardAccessPolicy;->lService:Lcom/sec/enterprise/knox/container/ISmartCardAccessPolicy;

    if-nez v0, :cond_0

    .line 125
    const-string v0, "smartcard_access_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/container/ISmartCardAccessPolicy$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/enterprise/knox/container/ISmartCardAccessPolicy;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/container/SmartCardAccessPolicy;->lService:Lcom/sec/enterprise/knox/container/ISmartCardAccessPolicy;

    .line 128
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/SmartCardAccessPolicy;->lService:Lcom/sec/enterprise/knox/container/ISmartCardAccessPolicy;

    return-object v0
.end method


# virtual methods
.method public allowBTSecureAccess(Z)I
    .locals 3
    .param p1, "allowAccess"    # Z

    .prologue
    .line 180
    iget-object v1, p0, Lcom/sec/enterprise/knox/container/SmartCardAccessPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SmartCardAccessPolicy.AllowBTSecureAccess"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 182
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/container/SmartCardAccessPolicy;->getService()Lcom/sec/enterprise/knox/container/ISmartCardAccessPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 183
    iget-object v1, p0, Lcom/sec/enterprise/knox/container/SmartCardAccessPolicy;->lService:Lcom/sec/enterprise/knox/container/ISmartCardAccessPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/container/SmartCardAccessPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Lcom/sec/enterprise/knox/container/ISmartCardAccessPolicy;->allowBTSecureAccess(Landroid/app/enterprise/ContextInfo;Z)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 188
    :goto_0
    return v1

    .line 185
    :catch_0
    move-exception v0

    .line 186
    .local v0, "e":Landroid/os/RemoteException;
    iget-object v1, p0, Lcom/sec/enterprise/knox/container/SmartCardAccessPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking to SmartCard Access policy service "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 188
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getAllWhitelistedAppFromBTSecureAccess()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 332
    invoke-direct {p0}, Lcom/sec/enterprise/knox/container/SmartCardAccessPolicy;->getService()Lcom/sec/enterprise/knox/container/ISmartCardAccessPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 334
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/container/SmartCardAccessPolicy;->lService:Lcom/sec/enterprise/knox/container/ISmartCardAccessPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/container/SmartCardAccessPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Lcom/sec/enterprise/knox/container/ISmartCardAccessPolicy;->getAllWhitelistedAppFromBTSecureAccess(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 339
    :goto_0
    return-object v1

    .line 335
    :catch_0
    move-exception v0

    .line 336
    .local v0, "e":Landroid/os/RemoteException;
    iget-object v1, p0, Lcom/sec/enterprise/knox/container/SmartCardAccessPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 339
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method public isBTSecureAccessAllowed()Z
    .locals 3

    .prologue
    .line 205
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/container/SmartCardAccessPolicy;->getService()Lcom/sec/enterprise/knox/container/ISmartCardAccessPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 206
    iget-object v1, p0, Lcom/sec/enterprise/knox/container/SmartCardAccessPolicy;->lService:Lcom/sec/enterprise/knox/container/ISmartCardAccessPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/container/SmartCardAccessPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Lcom/sec/enterprise/knox/container/ISmartCardAccessPolicy;->isBTSecureAccessAllowed(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 211
    :goto_0
    return v1

    .line 208
    :catch_0
    move-exception v0

    .line 209
    .local v0, "e":Landroid/os/RemoteException;
    iget-object v1, p0, Lcom/sec/enterprise/knox/container/SmartCardAccessPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking to SmartCard Access policy service "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 211
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isBTSecureAccessAllowedAsUser(I)Z
    .locals 3
    .param p1, "userId"    # I

    .prologue
    .line 220
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/container/SmartCardAccessPolicy;->getService()Lcom/sec/enterprise/knox/container/ISmartCardAccessPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 221
    iget-object v1, p0, Lcom/sec/enterprise/knox/container/SmartCardAccessPolicy;->lService:Lcom/sec/enterprise/knox/container/ISmartCardAccessPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/container/SmartCardAccessPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Lcom/sec/enterprise/knox/container/ISmartCardAccessPolicy;->isBTSecureAccessAllowedAsUser(Landroid/app/enterprise/ContextInfo;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 226
    :goto_0
    return v1

    .line 223
    :catch_0
    move-exception v0

    .line 224
    .local v0, "e":Landroid/os/RemoteException;
    iget-object v1, p0, Lcom/sec/enterprise/knox/container/SmartCardAccessPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking to SmartCard Access policy service "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 226
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isPackageWhitelistedFromBTSecureAccess(Ljava/lang/String;)Z
    .locals 3
    .param p1, "package_name"    # Ljava/lang/String;

    .prologue
    .line 363
    iget-object v1, p0, Lcom/sec/enterprise/knox/container/SmartCardAccessPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.getAppPackageNamesAllWhiteLists"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 364
    invoke-direct {p0}, Lcom/sec/enterprise/knox/container/SmartCardAccessPolicy;->getService()Lcom/sec/enterprise/knox/container/ISmartCardAccessPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 366
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/container/SmartCardAccessPolicy;->lService:Lcom/sec/enterprise/knox/container/ISmartCardAccessPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/container/SmartCardAccessPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Lcom/sec/enterprise/knox/container/ISmartCardAccessPolicy;->isPackageWhitelistedFromBTSecureAccess(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 371
    :goto_0
    return v1

    .line 367
    :catch_0
    move-exception v0

    .line 368
    .local v0, "e":Landroid/os/RemoteException;
    iget-object v1, p0, Lcom/sec/enterprise/knox/container/SmartCardAccessPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 371
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setBTSecureAccessAppWhiteList(Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 259
    .local p1, "packageNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/sec/enterprise/knox/container/SmartCardAccessPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SmartCardAccessPolicy.setBTSecureAccessAppWhiteList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 260
    invoke-direct {p0}, Lcom/sec/enterprise/knox/container/SmartCardAccessPolicy;->getService()Lcom/sec/enterprise/knox/container/ISmartCardAccessPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 262
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/container/SmartCardAccessPolicy;->lService:Lcom/sec/enterprise/knox/container/ISmartCardAccessPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/container/SmartCardAccessPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Lcom/sec/enterprise/knox/container/ISmartCardAccessPolicy;->setBTSecureAccessAppWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 267
    :goto_0
    return v1

    .line 263
    :catch_0
    move-exception v0

    .line 264
    .local v0, "e":Landroid/os/RemoteException;
    iget-object v1, p0, Lcom/sec/enterprise/knox/container/SmartCardAccessPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with SmartCard Access policy service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 267
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
