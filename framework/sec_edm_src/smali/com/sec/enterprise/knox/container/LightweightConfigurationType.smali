.class public Lcom/sec/enterprise/knox/container/LightweightConfigurationType;
.super Lcom/sec/enterprise/knox/container/KnoxConfigurationType;
.source "LightweightConfigurationType.java"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/enterprise/knox/container/LightweightConfigurationType;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String; = "LightweightConfigurationType"


# instance fields
.field private mFolderHeaderIcon:Ljava/lang/String;

.field private mFolderHeaderTitle:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 140
    new-instance v0, Lcom/sec/enterprise/knox/container/LightweightConfigurationType$1;

    invoke-direct {v0}, Lcom/sec/enterprise/knox/container/LightweightConfigurationType$1;-><init>()V

    sput-object v0, Lcom/sec/enterprise/knox/container/LightweightConfigurationType;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 156
    invoke-direct {p0}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;-><init>()V

    .line 49
    iput-object v0, p0, Lcom/sec/enterprise/knox/container/LightweightConfigurationType;->mFolderHeaderTitle:Ljava/lang/String;

    .line 50
    iput-object v0, p0, Lcom/sec/enterprise/knox/container/LightweightConfigurationType;->mFolderHeaderIcon:Ljava/lang/String;

    .line 158
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v1, 0x0

    .line 162
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;-><init>(Landroid/os/Parcel;)V

    .line 49
    iput-object v1, p0, Lcom/sec/enterprise/knox/container/LightweightConfigurationType;->mFolderHeaderTitle:Ljava/lang/String;

    .line 50
    iput-object v1, p0, Lcom/sec/enterprise/knox/container/LightweightConfigurationType;->mFolderHeaderIcon:Ljava/lang/String;

    .line 163
    const/4 v0, 0x0

    .line 164
    .local v0, "value":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 165
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 166
    :cond_0
    const/4 v0, 0x0

    .line 168
    :cond_1
    iput-object v0, p0, Lcom/sec/enterprise/knox/container/LightweightConfigurationType;->mFolderHeaderIcon:Ljava/lang/String;

    .line 169
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 170
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 171
    :cond_2
    const/4 v0, 0x0

    .line 173
    :cond_3
    iput-object v0, p0, Lcom/sec/enterprise/knox/container/LightweightConfigurationType;->mFolderHeaderTitle:Ljava/lang/String;

    .line 174
    return-void
.end method


# virtual methods
.method public bridge synthetic clone(Ljava/lang/String;)Lcom/sec/enterprise/knox/container/KnoxConfigurationType;
    .locals 1
    .param p1, "x0"    # Ljava/lang/String;

    .prologue
    .line 46
    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/container/LightweightConfigurationType;->clone(Ljava/lang/String;)Lcom/sec/enterprise/knox/container/LightweightConfigurationType;

    move-result-object v0

    return-object v0
.end method

.method public clone(Ljava/lang/String;)Lcom/sec/enterprise/knox/container/LightweightConfigurationType;
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 78
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 79
    :cond_0
    const-string v1, "LightweightConfigurationType"

    const-string v2, "clone(): name is either null or empty, hence returning null"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    const/4 v0, 0x0

    .line 86
    :goto_0
    return-object v0

    .line 82
    :cond_1
    new-instance v0, Lcom/sec/enterprise/knox/container/LightweightConfigurationType;

    invoke-direct {v0}, Lcom/sec/enterprise/knox/container/LightweightConfigurationType;-><init>()V

    .line 83
    .local v0, "type":Lcom/sec/enterprise/knox/container/LightweightConfigurationType;
    invoke-virtual {p0, v0, p1}, Lcom/sec/enterprise/knox/container/LightweightConfigurationType;->cloneConfiguration(Lcom/sec/enterprise/knox/container/KnoxConfigurationType;Ljava/lang/String;)V

    .line 84
    iget-object v1, p0, Lcom/sec/enterprise/knox/container/LightweightConfigurationType;->mFolderHeaderIcon:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/container/LightweightConfigurationType;->setFolderHeaderIcon(Ljava/lang/String;)V

    .line 85
    iget-object v1, p0, Lcom/sec/enterprise/knox/container/LightweightConfigurationType;->mFolderHeaderTitle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/enterprise/knox/container/LightweightConfigurationType;->setFolderHeaderTitle(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 179
    const/4 v0, 0x0

    return v0
.end method

.method public dumpState()V
    .locals 3

    .prologue
    .line 93
    const-string v0, "LightweightConfigurationType"

    const-string v1, "Lightweight config dump START:"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    const-string v0, "LightweightConfigurationType"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mFolderHeaderIcon : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/container/LightweightConfigurationType;->mFolderHeaderIcon:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    const-string v0, "LightweightConfigurationType"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mFolderHeaderTitle : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/container/LightweightConfigurationType;->mFolderHeaderTitle:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    invoke-super {p0}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->dumpState()V

    .line 97
    const-string v0, "LightweightConfigurationType"

    const-string v1, "Lightweight config dump END."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    return-void
.end method

.method public getFolderHeaderIcon()Ljava/lang/String;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/LightweightConfigurationType;->mFolderHeaderIcon:Ljava/lang/String;

    return-object v0
.end method

.method public getFolderHeaderTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/LightweightConfigurationType;->mFolderHeaderTitle:Ljava/lang/String;

    return-object v0
.end method

.method public setFolderHeaderIcon(Ljava/lang/String;)V
    .locals 0
    .param p1, "iconPath"    # Ljava/lang/String;

    .prologue
    .line 106
    iput-object p1, p0, Lcom/sec/enterprise/knox/container/LightweightConfigurationType;->mFolderHeaderIcon:Ljava/lang/String;

    .line 107
    return-void
.end method

.method public setFolderHeaderTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 125
    iput-object p1, p0, Lcom/sec/enterprise/knox/container/LightweightConfigurationType;->mFolderHeaderTitle:Ljava/lang/String;

    .line 126
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 185
    invoke-super {p0, p1, p2}, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;->writeToParcel(Landroid/os/Parcel;I)V

    .line 186
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/LightweightConfigurationType;->mFolderHeaderIcon:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 187
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/LightweightConfigurationType;->mFolderHeaderIcon:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 191
    :goto_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/LightweightConfigurationType;->mFolderHeaderTitle:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 192
    iget-object v0, p0, Lcom/sec/enterprise/knox/container/LightweightConfigurationType;->mFolderHeaderTitle:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 196
    :goto_1
    return-void

    .line 189
    :cond_0
    const-string v0, ""

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 194
    :cond_1
    const-string v0, ""

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_1
.end method
