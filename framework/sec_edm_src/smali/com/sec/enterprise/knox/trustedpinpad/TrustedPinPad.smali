.class public Lcom/sec/enterprise/knox/trustedpinpad/TrustedPinPad;
.super Ljava/lang/Object;
.source "TrustedPinPad.java"


# static fields
.field private static TAG:Ljava/lang/String; = null

.field public static final TRUSTED_PINPAD_FAILURE:I = 0x1

.field public static final TRUSTED_PINPAD_SUCCESS:I

.field private static mTrustedPinPadService:Lcom/sec/enterprise/knox/trustedpinpad/ITrustedPinPad;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mContextInfo:Landroid/app/enterprise/ContextInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 68
    const-string v0, "TrustedPinPad"

    sput-object v0, Lcom/sec/enterprise/knox/trustedpinpad/TrustedPinPad;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V
    .locals 3
    .param p1, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    iput-object p1, p0, Lcom/sec/enterprise/knox/trustedpinpad/TrustedPinPad;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    .line 95
    iput-object p2, p0, Lcom/sec/enterprise/knox/trustedpinpad/TrustedPinPad;->mContext:Landroid/content/Context;

    .line 96
    sget-object v0, Lcom/sec/enterprise/knox/trustedpinpad/TrustedPinPad;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "TrustedPinPad API ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/trustedpinpad/TrustedPinPad;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget v2, v2, Landroid/app/enterprise/ContextInfo;->mContainerId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/trustedpinpad/TrustedPinPad;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget v2, v2, Landroid/app/enterprise/ContextInfo;->mCallerUid:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    return-void
.end method

.method static declared-synchronized getTrustedPinPadService()Lcom/sec/enterprise/knox/trustedpinpad/ITrustedPinPad;
    .locals 2

    .prologue
    .line 104
    const-class v1, Lcom/sec/enterprise/knox/trustedpinpad/TrustedPinPad;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/enterprise/knox/trustedpinpad/TrustedPinPad;->mTrustedPinPadService:Lcom/sec/enterprise/knox/trustedpinpad/ITrustedPinPad;

    if-nez v0, :cond_0

    .line 105
    const-string v0, "knox_pinpad_service"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/trustedpinpad/ITrustedPinPad$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/enterprise/knox/trustedpinpad/ITrustedPinPad;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/trustedpinpad/TrustedPinPad;->mTrustedPinPadService:Lcom/sec/enterprise/knox/trustedpinpad/ITrustedPinPad;

    .line 109
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/trustedpinpad/TrustedPinPad;->mTrustedPinPadService:Lcom/sec/enterprise/knox/trustedpinpad/ITrustedPinPad;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 104
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public getTrustedPinPadInfo()Lcom/sec/enterprise/knox/trustedpinpad/TrustedPinPadInfo;
    .locals 9

    .prologue
    .line 156
    iget-object v7, p0, Lcom/sec/enterprise/knox/trustedpinpad/TrustedPinPad;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v8, "TrustedPinPad.getTrustedPinPadInfo"

    invoke-static {v7, v8}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 159
    :try_start_0
    invoke-static {}, Lcom/sec/enterprise/knox/trustedpinpad/TrustedPinPad;->getTrustedPinPadService()Lcom/sec/enterprise/knox/trustedpinpad/ITrustedPinPad;

    move-result-object v7

    if-eqz v7, :cond_0

    .line 161
    const/4 v6, 0x0

    .line 162
    .local v6, "returnList":Ljava/util/List;, "Ljava/util/List<Ljava/security/cert/X509Certificate;>;"
    sget-object v7, Lcom/sec/enterprise/knox/trustedpinpad/TrustedPinPad;->mTrustedPinPadService:Lcom/sec/enterprise/knox/trustedpinpad/ITrustedPinPad;

    iget-object v8, p0, Lcom/sec/enterprise/knox/trustedpinpad/TrustedPinPad;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v7, v8}, Lcom/sec/enterprise/knox/trustedpinpad/ITrustedPinPad;->getCertificates(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;

    move-result-object v1

    .line 164
    .local v1, "certInfoList":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/CertificateInfo;>;"
    if-eqz v1, :cond_2

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_2

    .line 165
    new-instance v6, Ljava/util/ArrayList;

    .end local v6    # "returnList":Ljava/util/List;, "Ljava/util/List<Ljava/security/cert/X509Certificate;>;"
    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 166
    .restart local v6    # "returnList":Ljava/util/List;, "Ljava/util/List<Ljava/security/cert/X509Certificate;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/CertificateInfo;

    .line 167
    .local v0, "certInfo":Landroid/app/enterprise/CertificateInfo;
    invoke-virtual {v0}, Landroid/app/enterprise/CertificateInfo;->getCertificate()Ljava/security/cert/Certificate;

    move-result-object v7

    check-cast v7, Ljava/security/cert/X509Certificate;

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 185
    .end local v0    # "certInfo":Landroid/app/enterprise/CertificateInfo;
    .end local v1    # "certInfoList":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/CertificateInfo;>;"
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v6    # "returnList":Ljava/util/List;, "Ljava/util/List<Ljava/security/cert/X509Certificate;>;"
    :catch_0
    move-exception v3

    .line 186
    .local v3, "e":Landroid/os/RemoteException;
    sget-object v7, Lcom/sec/enterprise/knox/trustedpinpad/TrustedPinPad;->TAG:Ljava/lang/String;

    const-string v8, "Failed at TrustedPinPad API getTrustedPinPadInfo-Exception"

    invoke-static {v7, v8, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 190
    .end local v3    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v5, 0x0

    :cond_1
    :goto_1
    return-object v5

    .line 173
    .restart local v1    # "certInfoList":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/CertificateInfo;>;"
    .restart local v6    # "returnList":Ljava/util/List;, "Ljava/util/List<Ljava/security/cert/X509Certificate;>;"
    :cond_2
    :try_start_1
    sget-object v7, Lcom/sec/enterprise/knox/trustedpinpad/TrustedPinPad;->mTrustedPinPadService:Lcom/sec/enterprise/knox/trustedpinpad/ITrustedPinPad;

    iget-object v8, p0, Lcom/sec/enterprise/knox/trustedpinpad/TrustedPinPad;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v7, v8}, Lcom/sec/enterprise/knox/trustedpinpad/ITrustedPinPad;->getSecretDimensions(Landroid/app/enterprise/ContextInfo;)[I

    move-result-object v2

    .line 176
    .local v2, "dimensions":[I
    new-instance v5, Lcom/sec/enterprise/knox/trustedpinpad/TrustedPinPadInfo;

    invoke-direct {v5}, Lcom/sec/enterprise/knox/trustedpinpad/TrustedPinPadInfo;-><init>()V

    .line 177
    .local v5, "retInfo":Lcom/sec/enterprise/knox/trustedpinpad/TrustedPinPadInfo;
    iput-object v6, v5, Lcom/sec/enterprise/knox/trustedpinpad/TrustedPinPadInfo;->pinPadCertificates:Ljava/util/List;

    .line 178
    if-eqz v2, :cond_1

    const/4 v7, 0x2

    array-length v8, v2

    if-ne v7, v8, :cond_1

    .line 179
    const/4 v7, 0x0

    aget v7, v2, v7

    iput v7, v5, Lcom/sec/enterprise/knox/trustedpinpad/TrustedPinPadInfo;->maximumImageWidth:I

    .line 180
    const/4 v7, 0x1

    aget v7, v2, v7

    iput v7, v5, Lcom/sec/enterprise/knox/trustedpinpad/TrustedPinPadInfo;->maximumImageHeight:I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method public launchTrustedPinPad(Ljava/lang/String;Ljava/lang/String;[BZI)[B
    .locals 8
    .param p1, "tzAppName"    # Ljava/lang/String;
    .param p2, "secretId"    # Ljava/lang/String;
    .param p3, "nonce"    # [B
    .param p4, "verify"    # Z
    .param p5, "minPinLength"    # I

    .prologue
    .line 325
    iget-object v0, p0, Lcom/sec/enterprise/knox/trustedpinpad/TrustedPinPad;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v1, "TrustedPinPad.launchTrustedPinPad"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 328
    :try_start_0
    invoke-static {}, Lcom/sec/enterprise/knox/trustedpinpad/TrustedPinPad;->getTrustedPinPadService()Lcom/sec/enterprise/knox/trustedpinpad/ITrustedPinPad;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 329
    sget-object v0, Lcom/sec/enterprise/knox/trustedpinpad/TrustedPinPad;->mTrustedPinPadService:Lcom/sec/enterprise/knox/trustedpinpad/ITrustedPinPad;

    iget-object v1, p0, Lcom/sec/enterprise/knox/trustedpinpad/TrustedPinPad;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move v6, p5

    invoke-interface/range {v0 .. v6}, Lcom/sec/enterprise/knox/trustedpinpad/ITrustedPinPad;->launchTrustedPinPad(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;[BZI)[B
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 337
    :goto_0
    return-object v0

    .line 332
    :catch_0
    move-exception v7

    .line 333
    .local v7, "e":Landroid/os/RemoteException;
    sget-object v0, Lcom/sec/enterprise/knox/trustedpinpad/TrustedPinPad;->TAG:Ljava/lang/String;

    const-string v1, "Failed at TrustedPinPad API launchTrustedPinPad-Exception"

    invoke-static {v0, v1, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 337
    .end local v7    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public loadTrustedPinPad()I
    .locals 3

    .prologue
    .line 448
    iget-object v1, p0, Lcom/sec/enterprise/knox/trustedpinpad/TrustedPinPad;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "TrustedPinPad.loadTrustedPinPad"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 451
    :try_start_0
    invoke-static {}, Lcom/sec/enterprise/knox/trustedpinpad/TrustedPinPad;->getTrustedPinPadService()Lcom/sec/enterprise/knox/trustedpinpad/ITrustedPinPad;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 452
    sget-object v1, Lcom/sec/enterprise/knox/trustedpinpad/TrustedPinPad;->mTrustedPinPadService:Lcom/sec/enterprise/knox/trustedpinpad/ITrustedPinPad;

    iget-object v2, p0, Lcom/sec/enterprise/knox/trustedpinpad/TrustedPinPad;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Lcom/sec/enterprise/knox/trustedpinpad/ITrustedPinPad;->loadTrustedPinPad(Landroid/app/enterprise/ContextInfo;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 459
    :goto_0
    return v1

    .line 454
    :catch_0
    move-exception v0

    .line 455
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/trustedpinpad/TrustedPinPad;->TAG:Ljava/lang/String;

    const-string v2, "Failed at TrustedPinPad API loadTrustedPinPad-Exception"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 459
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public setPin(Ljava/lang/String;[B)[B
    .locals 3
    .param p1, "tzAppName"    # Ljava/lang/String;
    .param p2, "pinBuffer"    # [B

    .prologue
    .line 389
    iget-object v1, p0, Lcom/sec/enterprise/knox/trustedpinpad/TrustedPinPad;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "TrustedPinPad.setPin"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 391
    :try_start_0
    invoke-static {}, Lcom/sec/enterprise/knox/trustedpinpad/TrustedPinPad;->getTrustedPinPadService()Lcom/sec/enterprise/knox/trustedpinpad/ITrustedPinPad;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 392
    sget-object v1, Lcom/sec/enterprise/knox/trustedpinpad/TrustedPinPad;->mTrustedPinPadService:Lcom/sec/enterprise/knox/trustedpinpad/ITrustedPinPad;

    iget-object v2, p0, Lcom/sec/enterprise/knox/trustedpinpad/TrustedPinPad;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Lcom/sec/enterprise/knox/trustedpinpad/ITrustedPinPad;->setPin(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;[B)[B
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 398
    :goto_0
    return-object v1

    .line 395
    :catch_0
    move-exception v0

    .line 396
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/trustedpinpad/TrustedPinPad;->TAG:Ljava/lang/String;

    const-string v2, "Failed at TrustedPinPad API setPin-Exception"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 398
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setSecretImage([BIILjava/lang/String;)I
    .locals 7
    .param p1, "secretImageBuffer"    # [B
    .param p2, "imageWidth"    # I
    .param p3, "imageHeight"    # I
    .param p4, "secretId"    # Ljava/lang/String;

    .prologue
    .line 264
    iget-object v0, p0, Lcom/sec/enterprise/knox/trustedpinpad/TrustedPinPad;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v1, "TrustedPinPad.setSecretImage"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 267
    :try_start_0
    invoke-static {}, Lcom/sec/enterprise/knox/trustedpinpad/TrustedPinPad;->getTrustedPinPadService()Lcom/sec/enterprise/knox/trustedpinpad/ITrustedPinPad;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 268
    sget-object v0, Lcom/sec/enterprise/knox/trustedpinpad/TrustedPinPad;->mTrustedPinPadService:Lcom/sec/enterprise/knox/trustedpinpad/ITrustedPinPad;

    iget-object v1, p0, Lcom/sec/enterprise/knox/trustedpinpad/TrustedPinPad;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    move-object v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    invoke-interface/range {v0 .. v5}, Lcom/sec/enterprise/knox/trustedpinpad/ITrustedPinPad;->setSecretImage(Landroid/app/enterprise/ContextInfo;[BIILjava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 275
    :goto_0
    return v0

    .line 271
    :catch_0
    move-exception v6

    .line 272
    .local v6, "e":Landroid/os/RemoteException;
    sget-object v0, Lcom/sec/enterprise/knox/trustedpinpad/TrustedPinPad;->TAG:Ljava/lang/String;

    const-string v1, "Failed at TrustedPinPad API setSecretImage-Exception"

    invoke-static {v0, v1, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 275
    .end local v6    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public unloadTrustedPinPad()I
    .locals 3

    .prologue
    .line 508
    iget-object v1, p0, Lcom/sec/enterprise/knox/trustedpinpad/TrustedPinPad;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "TrustedPinPad.unloadTrustedPinPad"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 511
    :try_start_0
    invoke-static {}, Lcom/sec/enterprise/knox/trustedpinpad/TrustedPinPad;->getTrustedPinPadService()Lcom/sec/enterprise/knox/trustedpinpad/ITrustedPinPad;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 512
    sget-object v1, Lcom/sec/enterprise/knox/trustedpinpad/TrustedPinPad;->mTrustedPinPadService:Lcom/sec/enterprise/knox/trustedpinpad/ITrustedPinPad;

    iget-object v2, p0, Lcom/sec/enterprise/knox/trustedpinpad/TrustedPinPad;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Lcom/sec/enterprise/knox/trustedpinpad/ITrustedPinPad;->unloadTrustedPinPad(Landroid/app/enterprise/ContextInfo;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 519
    :goto_0
    return v1

    .line 514
    :catch_0
    move-exception v0

    .line 515
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/trustedpinpad/TrustedPinPad;->TAG:Ljava/lang/String;

    const-string v2, "Failed at TrustedPinPad API unloadTrustedPinPad-Exception"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 519
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method
