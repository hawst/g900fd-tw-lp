.class public abstract Lcom/sec/enterprise/knox/trustedpinpad/ITrustedPinPad$Stub;
.super Landroid/os/Binder;
.source "ITrustedPinPad.java"

# interfaces
.implements Lcom/sec/enterprise/knox/trustedpinpad/ITrustedPinPad;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/enterprise/knox/trustedpinpad/ITrustedPinPad;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/enterprise/knox/trustedpinpad/ITrustedPinPad$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.sec.enterprise.knox.trustedpinpad.ITrustedPinPad"

.field static final TRANSACTION_getCertificates:I = 0x1

.field static final TRANSACTION_getSecretDimensions:I = 0x2

.field static final TRANSACTION_launchTrustedPinPad:I = 0x4

.field static final TRANSACTION_loadTrustedPinPad:I = 0x6

.field static final TRANSACTION_setPin:I = 0x5

.field static final TRANSACTION_setSecretImage:I = 0x3

.field static final TRANSACTION_unloadTrustedPinPad:I = 0x7


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 19
    const-string v0, "com.sec.enterprise.knox.trustedpinpad.ITrustedPinPad"

    invoke-virtual {p0, p0, v0}, Lcom/sec/enterprise/knox/trustedpinpad/ITrustedPinPad$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 20
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/sec/enterprise/knox/trustedpinpad/ITrustedPinPad;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 27
    if-nez p0, :cond_0

    .line 28
    const/4 v0, 0x0

    .line 34
    :goto_0
    return-object v0

    .line 30
    :cond_0
    const-string v1, "com.sec.enterprise.knox.trustedpinpad.ITrustedPinPad"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 31
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/sec/enterprise/knox/trustedpinpad/ITrustedPinPad;

    if-eqz v1, :cond_1

    .line 32
    check-cast v0, Lcom/sec/enterprise/knox/trustedpinpad/ITrustedPinPad;

    goto :goto_0

    .line 34
    :cond_1
    new-instance v0, Lcom/sec/enterprise/knox/trustedpinpad/ITrustedPinPad$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/sec/enterprise/knox/trustedpinpad/ITrustedPinPad$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 38
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 10
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x1

    .line 42
    sparse-switch p1, :sswitch_data_0

    .line 177
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v9

    :goto_0
    return v9

    .line 46
    :sswitch_0
    const-string v0, "com.sec.enterprise.knox.trustedpinpad.ITrustedPinPad"

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 51
    :sswitch_1
    const-string v0, "com.sec.enterprise.knox.trustedpinpad.ITrustedPinPad"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 53
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    .line 54
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 59
    .local v1, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_1
    invoke-virtual {p0, v1}, Lcom/sec/enterprise/knox/trustedpinpad/ITrustedPinPad$Stub;->getCertificates(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;

    move-result-object v8

    .line 60
    .local v8, "_result":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/CertificateInfo;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 61
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    goto :goto_0

    .line 57
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v8    # "_result":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/CertificateInfo;>;"
    :cond_0
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_1

    .line 66
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_2
    const-string v0, "com.sec.enterprise.knox.trustedpinpad.ITrustedPinPad"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 68
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1

    .line 69
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 74
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_2
    invoke-virtual {p0, v1}, Lcom/sec/enterprise/knox/trustedpinpad/ITrustedPinPad$Stub;->getSecretDimensions(Landroid/app/enterprise/ContextInfo;)[I

    move-result-object v7

    .line 75
    .local v7, "_result":[I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 76
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeIntArray([I)V

    goto :goto_0

    .line 72
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v7    # "_result":[I
    :cond_1
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_2

    .line 81
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_3
    const-string v0, "com.sec.enterprise.knox.trustedpinpad.ITrustedPinPad"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 83
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_2

    .line 84
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 90
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_3
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v2

    .line 92
    .local v2, "_arg1":[B
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 94
    .local v3, "_arg2":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 96
    .local v4, "_arg3":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    .local v5, "_arg4":Ljava/lang/String;
    move-object v0, p0

    .line 97
    invoke-virtual/range {v0 .. v5}, Lcom/sec/enterprise/knox/trustedpinpad/ITrustedPinPad$Stub;->setSecretImage(Landroid/app/enterprise/ContextInfo;[BIILjava/lang/String;)I

    move-result v7

    .line 98
    .local v7, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 99
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 87
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":[B
    .end local v3    # "_arg2":I
    .end local v4    # "_arg3":I
    .end local v5    # "_arg4":Ljava/lang/String;
    .end local v7    # "_result":I
    :cond_2
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_3

    .line 104
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_4
    const-string v0, "com.sec.enterprise.knox.trustedpinpad.ITrustedPinPad"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 106
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_3

    .line 107
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 113
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_4
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 115
    .local v2, "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 117
    .local v3, "_arg2":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v4

    .line 119
    .local v4, "_arg3":[B
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_4

    move v5, v9

    .line 121
    .local v5, "_arg4":Z
    :goto_5
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v6

    .local v6, "_arg5":I
    move-object v0, p0

    .line 122
    invoke-virtual/range {v0 .. v6}, Lcom/sec/enterprise/knox/trustedpinpad/ITrustedPinPad$Stub;->launchTrustedPinPad(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;[BZI)[B

    move-result-object v7

    .line 123
    .local v7, "_result":[B
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 124
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeByteArray([B)V

    goto/16 :goto_0

    .line 110
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":Ljava/lang/String;
    .end local v4    # "_arg3":[B
    .end local v5    # "_arg4":Z
    .end local v6    # "_arg5":I
    .end local v7    # "_result":[B
    :cond_3
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_4

    .line 119
    .restart local v2    # "_arg1":Ljava/lang/String;
    .restart local v3    # "_arg2":Ljava/lang/String;
    .restart local v4    # "_arg3":[B
    :cond_4
    const/4 v5, 0x0

    goto :goto_5

    .line 129
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":Ljava/lang/String;
    .end local v4    # "_arg3":[B
    :sswitch_5
    const-string v0, "com.sec.enterprise.knox.trustedpinpad.ITrustedPinPad"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 131
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_5

    .line 132
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 138
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_6
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 140
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v3

    .line 141
    .local v3, "_arg2":[B
    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/enterprise/knox/trustedpinpad/ITrustedPinPad$Stub;->setPin(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;[B)[B

    move-result-object v7

    .line 142
    .restart local v7    # "_result":[B
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 143
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeByteArray([B)V

    goto/16 :goto_0

    .line 135
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":[B
    .end local v7    # "_result":[B
    :cond_5
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_6

    .line 148
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_6
    const-string v0, "com.sec.enterprise.knox.trustedpinpad.ITrustedPinPad"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 150
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_6

    .line 151
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 156
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_7
    invoke-virtual {p0, v1}, Lcom/sec/enterprise/knox/trustedpinpad/ITrustedPinPad$Stub;->loadTrustedPinPad(Landroid/app/enterprise/ContextInfo;)I

    move-result v7

    .line 157
    .local v7, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 158
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 154
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v7    # "_result":I
    :cond_6
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_7

    .line 163
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_7
    const-string v0, "com.sec.enterprise.knox.trustedpinpad.ITrustedPinPad"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 165
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_7

    .line 166
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 171
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_8
    invoke-virtual {p0, v1}, Lcom/sec/enterprise/knox/trustedpinpad/ITrustedPinPad$Stub;->unloadTrustedPinPad(Landroid/app/enterprise/ContextInfo;)I

    move-result v7

    .line 172
    .restart local v7    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 173
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 169
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v7    # "_result":I
    :cond_7
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_8

    .line 42
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
