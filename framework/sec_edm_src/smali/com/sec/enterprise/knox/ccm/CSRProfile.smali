.class public Lcom/sec/enterprise/knox/ccm/CSRProfile;
.super Ljava/lang/Object;
.source "CSRProfile.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/enterprise/knox/ccm/CSRProfile$KeyAlgorithm;,
        Lcom/sec/enterprise/knox/ccm/CSRProfile$CSRFormat;,
        Lcom/sec/enterprise/knox/ccm/CSRProfile$ProfileType;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/enterprise/knox/ccm/CSRProfile;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public commonName:Ljava/lang/String;

.field public country:Ljava/lang/String;

.field public csrFormat:Lcom/sec/enterprise/knox/ccm/CSRProfile$CSRFormat;

.field public domainComponent:Ljava/lang/String;

.field public emailAddress:Ljava/lang/String;

.field public keyAlgType:Lcom/sec/enterprise/knox/ccm/CSRProfile$KeyAlgorithm;

.field public keyLength:I

.field public locality:Ljava/lang/String;

.field public organization:Ljava/lang/String;

.field public profileType:Lcom/sec/enterprise/knox/ccm/CSRProfile$ProfileType;

.field public state:Ljava/lang/String;

.field public templateName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 281
    new-instance v0, Lcom/sec/enterprise/knox/ccm/CSRProfile$1;

    invoke-direct {v0}, Lcom/sec/enterprise/knox/ccm/CSRProfile$1;-><init>()V

    sput-object v0, Lcom/sec/enterprise/knox/ccm/CSRProfile;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 227
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 105
    sget-object v0, Lcom/sec/enterprise/knox/ccm/CSRProfile$ProfileType;->SCEP:Lcom/sec/enterprise/knox/ccm/CSRProfile$ProfileType;

    iput-object v0, p0, Lcom/sec/enterprise/knox/ccm/CSRProfile;->profileType:Lcom/sec/enterprise/knox/ccm/CSRProfile$ProfileType;

    .line 116
    sget-object v0, Lcom/sec/enterprise/knox/ccm/CSRProfile$CSRFormat;->PKCS10:Lcom/sec/enterprise/knox/ccm/CSRProfile$CSRFormat;

    iput-object v0, p0, Lcom/sec/enterprise/knox/ccm/CSRProfile;->csrFormat:Lcom/sec/enterprise/knox/ccm/CSRProfile$CSRFormat;

    .line 124
    sget-object v0, Lcom/sec/enterprise/knox/ccm/CSRProfile$KeyAlgorithm;->RSA:Lcom/sec/enterprise/knox/ccm/CSRProfile$KeyAlgorithm;

    iput-object v0, p0, Lcom/sec/enterprise/knox/ccm/CSRProfile;->keyAlgType:Lcom/sec/enterprise/knox/ccm/CSRProfile$KeyAlgorithm;

    .line 133
    iput-object v1, p0, Lcom/sec/enterprise/knox/ccm/CSRProfile;->templateName:Ljava/lang/String;

    .line 140
    const/16 v0, 0x400

    iput v0, p0, Lcom/sec/enterprise/knox/ccm/CSRProfile;->keyLength:I

    .line 147
    iput-object v1, p0, Lcom/sec/enterprise/knox/ccm/CSRProfile;->commonName:Ljava/lang/String;

    .line 154
    iput-object v1, p0, Lcom/sec/enterprise/knox/ccm/CSRProfile;->organization:Ljava/lang/String;

    .line 161
    iput-object v1, p0, Lcom/sec/enterprise/knox/ccm/CSRProfile;->domainComponent:Ljava/lang/String;

    .line 168
    iput-object v1, p0, Lcom/sec/enterprise/knox/ccm/CSRProfile;->emailAddress:Ljava/lang/String;

    .line 175
    iput-object v1, p0, Lcom/sec/enterprise/knox/ccm/CSRProfile;->country:Ljava/lang/String;

    .line 182
    iput-object v1, p0, Lcom/sec/enterprise/knox/ccm/CSRProfile;->state:Ljava/lang/String;

    .line 189
    iput-object v1, p0, Lcom/sec/enterprise/knox/ccm/CSRProfile;->locality:Ljava/lang/String;

    .line 228
    sget-object v0, Lcom/sec/enterprise/knox/ccm/CSRProfile$ProfileType;->SCEP:Lcom/sec/enterprise/knox/ccm/CSRProfile$ProfileType;

    iput-object v0, p0, Lcom/sec/enterprise/knox/ccm/CSRProfile;->profileType:Lcom/sec/enterprise/knox/ccm/CSRProfile$ProfileType;

    .line 229
    sget-object v0, Lcom/sec/enterprise/knox/ccm/CSRProfile$CSRFormat;->PKCS10:Lcom/sec/enterprise/knox/ccm/CSRProfile$CSRFormat;

    iput-object v0, p0, Lcom/sec/enterprise/knox/ccm/CSRProfile;->csrFormat:Lcom/sec/enterprise/knox/ccm/CSRProfile$CSRFormat;

    .line 230
    sget-object v0, Lcom/sec/enterprise/knox/ccm/CSRProfile$KeyAlgorithm;->RSA:Lcom/sec/enterprise/knox/ccm/CSRProfile$KeyAlgorithm;

    iput-object v0, p0, Lcom/sec/enterprise/knox/ccm/CSRProfile;->keyAlgType:Lcom/sec/enterprise/knox/ccm/CSRProfile$KeyAlgorithm;

    .line 231
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v2, 0x0

    .line 236
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 105
    sget-object v1, Lcom/sec/enterprise/knox/ccm/CSRProfile$ProfileType;->SCEP:Lcom/sec/enterprise/knox/ccm/CSRProfile$ProfileType;

    iput-object v1, p0, Lcom/sec/enterprise/knox/ccm/CSRProfile;->profileType:Lcom/sec/enterprise/knox/ccm/CSRProfile$ProfileType;

    .line 116
    sget-object v1, Lcom/sec/enterprise/knox/ccm/CSRProfile$CSRFormat;->PKCS10:Lcom/sec/enterprise/knox/ccm/CSRProfile$CSRFormat;

    iput-object v1, p0, Lcom/sec/enterprise/knox/ccm/CSRProfile;->csrFormat:Lcom/sec/enterprise/knox/ccm/CSRProfile$CSRFormat;

    .line 124
    sget-object v1, Lcom/sec/enterprise/knox/ccm/CSRProfile$KeyAlgorithm;->RSA:Lcom/sec/enterprise/knox/ccm/CSRProfile$KeyAlgorithm;

    iput-object v1, p0, Lcom/sec/enterprise/knox/ccm/CSRProfile;->keyAlgType:Lcom/sec/enterprise/knox/ccm/CSRProfile$KeyAlgorithm;

    .line 133
    iput-object v2, p0, Lcom/sec/enterprise/knox/ccm/CSRProfile;->templateName:Ljava/lang/String;

    .line 140
    const/16 v1, 0x400

    iput v1, p0, Lcom/sec/enterprise/knox/ccm/CSRProfile;->keyLength:I

    .line 147
    iput-object v2, p0, Lcom/sec/enterprise/knox/ccm/CSRProfile;->commonName:Ljava/lang/String;

    .line 154
    iput-object v2, p0, Lcom/sec/enterprise/knox/ccm/CSRProfile;->organization:Ljava/lang/String;

    .line 161
    iput-object v2, p0, Lcom/sec/enterprise/knox/ccm/CSRProfile;->domainComponent:Ljava/lang/String;

    .line 168
    iput-object v2, p0, Lcom/sec/enterprise/knox/ccm/CSRProfile;->emailAddress:Ljava/lang/String;

    .line 175
    iput-object v2, p0, Lcom/sec/enterprise/knox/ccm/CSRProfile;->country:Ljava/lang/String;

    .line 182
    iput-object v2, p0, Lcom/sec/enterprise/knox/ccm/CSRProfile;->state:Ljava/lang/String;

    .line 189
    iput-object v2, p0, Lcom/sec/enterprise/knox/ccm/CSRProfile;->locality:Ljava/lang/String;

    .line 238
    :try_start_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/enterprise/knox/ccm/CSRProfile$ProfileType;->valueOf(Ljava/lang/String;)Lcom/sec/enterprise/knox/ccm/CSRProfile$ProfileType;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/enterprise/knox/ccm/CSRProfile;->profileType:Lcom/sec/enterprise/knox/ccm/CSRProfile$ProfileType;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 243
    :goto_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/ccm/CSRProfile;->profileType:Lcom/sec/enterprise/knox/ccm/CSRProfile$ProfileType;

    if-nez v1, :cond_0

    .line 244
    sget-object v1, Lcom/sec/enterprise/knox/ccm/CSRProfile$ProfileType;->SCEP:Lcom/sec/enterprise/knox/ccm/CSRProfile$ProfileType;

    iput-object v1, p0, Lcom/sec/enterprise/knox/ccm/CSRProfile;->profileType:Lcom/sec/enterprise/knox/ccm/CSRProfile$ProfileType;

    .line 248
    :cond_0
    :try_start_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/enterprise/knox/ccm/CSRProfile$CSRFormat;->valueOf(Ljava/lang/String;)Lcom/sec/enterprise/knox/ccm/CSRProfile$CSRFormat;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/enterprise/knox/ccm/CSRProfile;->csrFormat:Lcom/sec/enterprise/knox/ccm/CSRProfile$CSRFormat;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    .line 253
    :goto_1
    iget-object v1, p0, Lcom/sec/enterprise/knox/ccm/CSRProfile;->csrFormat:Lcom/sec/enterprise/knox/ccm/CSRProfile$CSRFormat;

    if-nez v1, :cond_1

    .line 254
    sget-object v1, Lcom/sec/enterprise/knox/ccm/CSRProfile$CSRFormat;->PKCS10:Lcom/sec/enterprise/knox/ccm/CSRProfile$CSRFormat;

    iput-object v1, p0, Lcom/sec/enterprise/knox/ccm/CSRProfile;->csrFormat:Lcom/sec/enterprise/knox/ccm/CSRProfile$CSRFormat;

    .line 258
    :cond_1
    :try_start_2
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/enterprise/knox/ccm/CSRProfile$KeyAlgorithm;->valueOf(Ljava/lang/String;)Lcom/sec/enterprise/knox/ccm/CSRProfile$KeyAlgorithm;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/enterprise/knox/ccm/CSRProfile;->keyAlgType:Lcom/sec/enterprise/knox/ccm/CSRProfile$KeyAlgorithm;
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_2

    .line 263
    :goto_2
    iget-object v1, p0, Lcom/sec/enterprise/knox/ccm/CSRProfile;->keyAlgType:Lcom/sec/enterprise/knox/ccm/CSRProfile$KeyAlgorithm;

    if-nez v1, :cond_2

    .line 264
    sget-object v1, Lcom/sec/enterprise/knox/ccm/CSRProfile$KeyAlgorithm;->RSA:Lcom/sec/enterprise/knox/ccm/CSRProfile$KeyAlgorithm;

    iput-object v1, p0, Lcom/sec/enterprise/knox/ccm/CSRProfile;->keyAlgType:Lcom/sec/enterprise/knox/ccm/CSRProfile$KeyAlgorithm;

    .line 267
    :cond_2
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/enterprise/knox/ccm/CSRProfile;->templateName:Ljava/lang/String;

    .line 268
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/sec/enterprise/knox/ccm/CSRProfile;->keyLength:I

    .line 269
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/enterprise/knox/ccm/CSRProfile;->commonName:Ljava/lang/String;

    .line 270
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/enterprise/knox/ccm/CSRProfile;->organization:Ljava/lang/String;

    .line 271
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/enterprise/knox/ccm/CSRProfile;->domainComponent:Ljava/lang/String;

    .line 272
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/enterprise/knox/ccm/CSRProfile;->emailAddress:Ljava/lang/String;

    .line 273
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/enterprise/knox/ccm/CSRProfile;->country:Ljava/lang/String;

    .line 274
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/enterprise/knox/ccm/CSRProfile;->state:Ljava/lang/String;

    .line 275
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/enterprise/knox/ccm/CSRProfile;->locality:Ljava/lang/String;

    .line 276
    return-void

    .line 239
    :catch_0
    move-exception v0

    .line 240
    .local v0, "ex":Ljava/lang/IllegalArgumentException;
    iput-object v2, p0, Lcom/sec/enterprise/knox/ccm/CSRProfile;->profileType:Lcom/sec/enterprise/knox/ccm/CSRProfile$ProfileType;

    .line 241
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 249
    .end local v0    # "ex":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 250
    .restart local v0    # "ex":Ljava/lang/IllegalArgumentException;
    iput-object v2, p0, Lcom/sec/enterprise/knox/ccm/CSRProfile;->csrFormat:Lcom/sec/enterprise/knox/ccm/CSRProfile$CSRFormat;

    .line 251
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_1

    .line 259
    .end local v0    # "ex":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .line 260
    .restart local v0    # "ex":Ljava/lang/IllegalArgumentException;
    iput-object v2, p0, Lcom/sec/enterprise/knox/ccm/CSRProfile;->keyAlgType:Lcom/sec/enterprise/knox/ccm/CSRProfile$KeyAlgorithm;

    .line 261
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_2
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/enterprise/knox/ccm/CSRProfile$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/sec/enterprise/knox/ccm/CSRProfile$1;

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/ccm/CSRProfile;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 295
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 195
    iget-object v0, p0, Lcom/sec/enterprise/knox/ccm/CSRProfile;->profileType:Lcom/sec/enterprise/knox/ccm/CSRProfile$ProfileType;

    if-nez v0, :cond_0

    .line 196
    sget-object v0, Lcom/sec/enterprise/knox/ccm/CSRProfile$ProfileType;->SCEP:Lcom/sec/enterprise/knox/ccm/CSRProfile$ProfileType;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/ccm/CSRProfile$ProfileType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 201
    :goto_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/ccm/CSRProfile;->csrFormat:Lcom/sec/enterprise/knox/ccm/CSRProfile$CSRFormat;

    if-nez v0, :cond_1

    .line 202
    sget-object v0, Lcom/sec/enterprise/knox/ccm/CSRProfile$CSRFormat;->PKCS10:Lcom/sec/enterprise/knox/ccm/CSRProfile$CSRFormat;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/ccm/CSRProfile$CSRFormat;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 207
    :goto_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/ccm/CSRProfile;->keyAlgType:Lcom/sec/enterprise/knox/ccm/CSRProfile$KeyAlgorithm;

    if-nez v0, :cond_2

    .line 208
    sget-object v0, Lcom/sec/enterprise/knox/ccm/CSRProfile$KeyAlgorithm;->RSA:Lcom/sec/enterprise/knox/ccm/CSRProfile$KeyAlgorithm;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/ccm/CSRProfile$KeyAlgorithm;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 213
    :goto_2
    iget-object v0, p0, Lcom/sec/enterprise/knox/ccm/CSRProfile;->templateName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 214
    iget v0, p0, Lcom/sec/enterprise/knox/ccm/CSRProfile;->keyLength:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 215
    iget-object v0, p0, Lcom/sec/enterprise/knox/ccm/CSRProfile;->commonName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 216
    iget-object v0, p0, Lcom/sec/enterprise/knox/ccm/CSRProfile;->organization:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 217
    iget-object v0, p0, Lcom/sec/enterprise/knox/ccm/CSRProfile;->domainComponent:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 218
    iget-object v0, p0, Lcom/sec/enterprise/knox/ccm/CSRProfile;->emailAddress:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 219
    iget-object v0, p0, Lcom/sec/enterprise/knox/ccm/CSRProfile;->country:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 220
    iget-object v0, p0, Lcom/sec/enterprise/knox/ccm/CSRProfile;->state:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 221
    iget-object v0, p0, Lcom/sec/enterprise/knox/ccm/CSRProfile;->locality:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 222
    return-void

    .line 198
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/ccm/CSRProfile;->profileType:Lcom/sec/enterprise/knox/ccm/CSRProfile$ProfileType;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/ccm/CSRProfile$ProfileType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 204
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/ccm/CSRProfile;->csrFormat:Lcom/sec/enterprise/knox/ccm/CSRProfile$CSRFormat;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/ccm/CSRProfile$CSRFormat;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_1

    .line 210
    :cond_2
    iget-object v0, p0, Lcom/sec/enterprise/knox/ccm/CSRProfile;->keyAlgType:Lcom/sec/enterprise/knox/ccm/CSRProfile$KeyAlgorithm;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/ccm/CSRProfile$KeyAlgorithm;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_2
.end method
