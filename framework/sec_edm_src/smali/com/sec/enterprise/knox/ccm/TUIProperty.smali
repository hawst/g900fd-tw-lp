.class public Lcom/sec/enterprise/knox/ccm/TUIProperty;
.super Ljava/lang/Object;
.source "TUIProperty.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/enterprise/knox/ccm/TUIProperty;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public loginExpirationPeriod:I

.field public loginRetry:I

.field public pin:[B

.field public secretImage:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 170
    new-instance v0, Lcom/sec/enterprise/knox/ccm/TUIProperty$1;

    invoke-direct {v0}, Lcom/sec/enterprise/knox/ccm/TUIProperty$1;-><init>()V

    sput-object v0, Lcom/sec/enterprise/knox/ccm/TUIProperty;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 147
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/enterprise/knox/ccm/TUIProperty;->loginRetry:I

    .line 55
    const/16 v0, 0x78

    iput v0, p0, Lcom/sec/enterprise/knox/ccm/TUIProperty;->loginExpirationPeriod:I

    .line 93
    iput-object v1, p0, Lcom/sec/enterprise/knox/ccm/TUIProperty;->pin:[B

    .line 129
    iput-object v1, p0, Lcom/sec/enterprise/knox/ccm/TUIProperty;->secretImage:[B

    .line 148
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v1, 0x0

    .line 153
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/enterprise/knox/ccm/TUIProperty;->loginRetry:I

    .line 55
    const/16 v0, 0x78

    iput v0, p0, Lcom/sec/enterprise/knox/ccm/TUIProperty;->loginExpirationPeriod:I

    .line 93
    iput-object v1, p0, Lcom/sec/enterprise/knox/ccm/TUIProperty;->pin:[B

    .line 129
    iput-object v1, p0, Lcom/sec/enterprise/knox/ccm/TUIProperty;->secretImage:[B

    .line 154
    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/ccm/TUIProperty;->readFromParcel(Landroid/os/Parcel;)V

    .line 155
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/enterprise/knox/ccm/TUIProperty$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/sec/enterprise/knox/ccm/TUIProperty$1;

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/ccm/TUIProperty;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 184
    const/4 v0, 0x0

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 161
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/enterprise/knox/ccm/TUIProperty;->loginRetry:I

    .line 162
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/enterprise/knox/ccm/TUIProperty;->loginExpirationPeriod:I

    .line 163
    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/ccm/TUIProperty;->pin:[B

    .line 164
    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/ccm/TUIProperty;->secretImage:[B

    .line 165
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 137
    iget v0, p0, Lcom/sec/enterprise/knox/ccm/TUIProperty;->loginRetry:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 138
    iget v0, p0, Lcom/sec/enterprise/knox/ccm/TUIProperty;->loginExpirationPeriod:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 139
    iget-object v0, p0, Lcom/sec/enterprise/knox/ccm/TUIProperty;->pin:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 140
    iget-object v0, p0, Lcom/sec/enterprise/knox/ccm/TUIProperty;->secretImage:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 141
    return-void
.end method
