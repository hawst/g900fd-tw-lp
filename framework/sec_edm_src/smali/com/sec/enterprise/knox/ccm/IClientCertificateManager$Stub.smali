.class public abstract Lcom/sec/enterprise/knox/ccm/IClientCertificateManager$Stub;
.super Landroid/os/Binder;
.source "IClientCertificateManager.java"

# interfaces
.implements Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/enterprise/knox/ccm/IClientCertificateManager$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.sec.enterprise.knox.ccm.IClientCertificateManager"

.field static final TRANSACTION_addPackageToExemptList:I = 0x11

.field static final TRANSACTION_deleteCCMProfile:I = 0x6

.field static final TRANSACTION_deleteCSRProfile:I = 0xe

.field static final TRANSACTION_deleteCertificate:I = 0x8

.field static final TRANSACTION_generateCSR:I = 0x1

.field static final TRANSACTION_generateCSRUsingTemplate:I = 0xf

.field static final TRANSACTION_getAliasesForCaller:I = 0x16

.field static final TRANSACTION_getAliasesForWiFi:I = 0x18

.field static final TRANSACTION_getCCMProfile:I = 0x5

.field static final TRANSACTION_getCCMVersion:I = 0x13

.field static final TRANSACTION_getCertificateAliases:I = 0x1b

.field static final TRANSACTION_getCertificateAliasesHavingPrivateKey:I = 0x1c

.field static final TRANSACTION_getDefaultCertificateAlias:I = 0x15

.field static final TRANSACTION_getSlotIdForCaller:I = 0xb

.field static final TRANSACTION_getSlotIdForPackage:I = 0xc

.field static final TRANSACTION_installCertificate:I = 0x7

.field static final TRANSACTION_installObject:I = 0x3

.field static final TRANSACTION_installObjectWithProfile:I = 0x1d

.field static final TRANSACTION_installObjectWithType:I = 0x14

.field static final TRANSACTION_isAccessControlMethodPassword:I = 0x17

.field static final TRANSACTION_isCCMEmptyForKeyChain:I = 0x1a

.field static final TRANSACTION_isCCMPolicyEnabledByAdmin:I = 0x10

.field static final TRANSACTION_isCCMPolicyEnabledForCaller:I = 0x9

.field static final TRANSACTION_isCCMPolicyEnabledForPackage:I = 0xa

.field static final TRANSACTION_keychainMarkedReset:I = 0x19

.field static final TRANSACTION_registerForDefaultCertificate:I = 0x2

.field static final TRANSACTION_removePackageFromExemptList:I = 0x12

.field static final TRANSACTION_setCCMProfile:I = 0x4

.field static final TRANSACTION_setCSRProfile:I = 0xd


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 19
    const-string v0, "com.sec.enterprise.knox.ccm.IClientCertificateManager"

    invoke-virtual {p0, p0, v0}, Lcom/sec/enterprise/knox/ccm/IClientCertificateManager$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 20
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 27
    if-nez p0, :cond_0

    .line 28
    const/4 v0, 0x0

    .line 34
    :goto_0
    return-object v0

    .line 30
    :cond_0
    const-string v1, "com.sec.enterprise.knox.ccm.IClientCertificateManager"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 31
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;

    if-eqz v1, :cond_1

    .line 32
    check-cast v0, Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;

    goto :goto_0

    .line 34
    :cond_1
    new-instance v0, Lcom/sec/enterprise/knox/ccm/IClientCertificateManager$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/sec/enterprise/knox/ccm/IClientCertificateManager$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 38
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 15
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 42
    sparse-switch p1, :sswitch_data_0

    .line 567
    invoke-super/range {p0 .. p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v2

    :goto_0
    return v2

    .line 46
    :sswitch_0
    const-string v2, "com.sec.enterprise.knox.ccm.IClientCertificateManager"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 47
    const/4 v2, 0x1

    goto :goto_0

    .line 51
    :sswitch_1
    const-string v2, "com.sec.enterprise.knox.ccm.IClientCertificateManager"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 53
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_0

    .line 54
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 60
    .local v3, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_1
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 62
    .local v4, "_arg1":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    .line 64
    .local v5, "_arg2":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 66
    .local v6, "_arg3":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v7

    .line 68
    .local v7, "_arg4":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_1

    const/4 v8, 0x1

    .local v8, "_arg5":Z
    :goto_2
    move-object v2, p0

    .line 69
    invoke-virtual/range {v2 .. v8}, Lcom/sec/enterprise/knox/ccm/IClientCertificateManager$Stub;->generateCSR(Landroid/app/enterprise/ContextInfo;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)[B

    move-result-object v12

    .line 70
    .local v12, "_result":[B
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 71
    move-object/from16 v0, p3

    invoke-virtual {v0, v12}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 72
    const/4 v2, 0x1

    goto :goto_0

    .line 57
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":I
    .end local v5    # "_arg2":Ljava/lang/String;
    .end local v6    # "_arg3":Ljava/lang/String;
    .end local v7    # "_arg4":Ljava/lang/String;
    .end local v8    # "_arg5":Z
    .end local v12    # "_result":[B
    :cond_0
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_1

    .line 68
    .restart local v4    # "_arg1":I
    .restart local v5    # "_arg2":Ljava/lang/String;
    .restart local v6    # "_arg3":Ljava/lang/String;
    .restart local v7    # "_arg4":Ljava/lang/String;
    :cond_1
    const/4 v8, 0x0

    goto :goto_2

    .line 76
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":I
    .end local v5    # "_arg2":Ljava/lang/String;
    .end local v6    # "_arg3":Ljava/lang/String;
    .end local v7    # "_arg4":Ljava/lang/String;
    :sswitch_2
    const-string v2, "com.sec.enterprise.knox.ccm.IClientCertificateManager"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 78
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_2

    .line 79
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 85
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_3
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 87
    .restart local v4    # "_arg1":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    .line 89
    .restart local v5    # "_arg2":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 91
    .restart local v6    # "_arg3":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_3

    const/4 v7, 0x1

    .local v7, "_arg4":Z
    :goto_4
    move-object v2, p0

    .line 92
    invoke-virtual/range {v2 .. v7}, Lcom/sec/enterprise/knox/ccm/IClientCertificateManager$Stub;->registerForDefaultCertificate(Landroid/app/enterprise/ContextInfo;ILjava/lang/String;Ljava/lang/String;Z)I

    move-result v12

    .line 93
    .local v12, "_result":I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 94
    move-object/from16 v0, p3

    invoke-virtual {v0, v12}, Landroid/os/Parcel;->writeInt(I)V

    .line 95
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 82
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":I
    .end local v5    # "_arg2":Ljava/lang/String;
    .end local v6    # "_arg3":Ljava/lang/String;
    .end local v7    # "_arg4":Z
    .end local v12    # "_result":I
    :cond_2
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_3

    .line 91
    .restart local v4    # "_arg1":I
    .restart local v5    # "_arg2":Ljava/lang/String;
    .restart local v6    # "_arg3":Ljava/lang/String;
    :cond_3
    const/4 v7, 0x0

    goto :goto_4

    .line 99
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":I
    .end local v5    # "_arg2":Ljava/lang/String;
    .end local v6    # "_arg3":Ljava/lang/String;
    :sswitch_3
    const-string v2, "com.sec.enterprise.knox.ccm.IClientCertificateManager"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 101
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_4

    .line 102
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 108
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_5
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 110
    .restart local v4    # "_arg1":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    .line 112
    .restart local v5    # "_arg2":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 114
    .restart local v6    # "_arg3":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v7

    .line 116
    .local v7, "_arg4":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    .line 118
    .local v8, "_arg5":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v9

    .line 120
    .local v9, "_arg6":[B
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v10

    .line 122
    .local v10, "_arg7":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_5

    const/4 v11, 0x1

    .local v11, "_arg8":Z
    :goto_6
    move-object v2, p0

    .line 123
    invoke-virtual/range {v2 .. v11}, Lcom/sec/enterprise/knox/ccm/IClientCertificateManager$Stub;->installObject(Landroid/app/enterprise/ContextInfo;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;I[BLjava/lang/String;Z)I

    move-result v12

    .line 124
    .restart local v12    # "_result":I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 125
    move-object/from16 v0, p3

    invoke-virtual {v0, v12}, Landroid/os/Parcel;->writeInt(I)V

    .line 126
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 105
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":I
    .end local v5    # "_arg2":Ljava/lang/String;
    .end local v6    # "_arg3":Ljava/lang/String;
    .end local v7    # "_arg4":Ljava/lang/String;
    .end local v8    # "_arg5":I
    .end local v9    # "_arg6":[B
    .end local v10    # "_arg7":Ljava/lang/String;
    .end local v11    # "_arg8":Z
    .end local v12    # "_result":I
    :cond_4
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_5

    .line 122
    .restart local v4    # "_arg1":I
    .restart local v5    # "_arg2":Ljava/lang/String;
    .restart local v6    # "_arg3":Ljava/lang/String;
    .restart local v7    # "_arg4":Ljava/lang/String;
    .restart local v8    # "_arg5":I
    .restart local v9    # "_arg6":[B
    .restart local v10    # "_arg7":Ljava/lang/String;
    :cond_5
    const/4 v11, 0x0

    goto :goto_6

    .line 130
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":I
    .end local v5    # "_arg2":Ljava/lang/String;
    .end local v6    # "_arg3":Ljava/lang/String;
    .end local v7    # "_arg4":Ljava/lang/String;
    .end local v8    # "_arg5":I
    .end local v9    # "_arg6":[B
    .end local v10    # "_arg7":Ljava/lang/String;
    :sswitch_4
    const-string v2, "com.sec.enterprise.knox.ccm.IClientCertificateManager"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 132
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_6

    .line 133
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 139
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_7
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_7

    .line 140
    sget-object v2, Lcom/sec/enterprise/knox/ccm/CCMProfile;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/enterprise/knox/ccm/CCMProfile;

    .line 145
    .local v4, "_arg1":Lcom/sec/enterprise/knox/ccm/CCMProfile;
    :goto_8
    invoke-virtual {p0, v3, v4}, Lcom/sec/enterprise/knox/ccm/IClientCertificateManager$Stub;->setCCMProfile(Landroid/app/enterprise/ContextInfo;Lcom/sec/enterprise/knox/ccm/CCMProfile;)Z

    move-result v12

    .line 146
    .local v12, "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 147
    if-eqz v12, :cond_8

    const/4 v2, 0x1

    :goto_9
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 148
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 136
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Lcom/sec/enterprise/knox/ccm/CCMProfile;
    .end local v12    # "_result":Z
    :cond_6
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_7

    .line 143
    :cond_7
    const/4 v4, 0x0

    .restart local v4    # "_arg1":Lcom/sec/enterprise/knox/ccm/CCMProfile;
    goto :goto_8

    .line 147
    .restart local v12    # "_result":Z
    :cond_8
    const/4 v2, 0x0

    goto :goto_9

    .line 152
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Lcom/sec/enterprise/knox/ccm/CCMProfile;
    .end local v12    # "_result":Z
    :sswitch_5
    const-string v2, "com.sec.enterprise.knox.ccm.IClientCertificateManager"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 154
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_9

    .line 155
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 160
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_a
    invoke-virtual {p0, v3}, Lcom/sec/enterprise/knox/ccm/IClientCertificateManager$Stub;->getCCMProfile(Landroid/app/enterprise/ContextInfo;)Lcom/sec/enterprise/knox/ccm/CCMProfile;

    move-result-object v12

    .line 161
    .local v12, "_result":Lcom/sec/enterprise/knox/ccm/CCMProfile;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 162
    if-eqz v12, :cond_a

    .line 163
    const/4 v2, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 164
    const/4 v2, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v12, v0, v2}, Lcom/sec/enterprise/knox/ccm/CCMProfile;->writeToParcel(Landroid/os/Parcel;I)V

    .line 169
    :goto_b
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 158
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v12    # "_result":Lcom/sec/enterprise/knox/ccm/CCMProfile;
    :cond_9
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_a

    .line 167
    .restart local v12    # "_result":Lcom/sec/enterprise/knox/ccm/CCMProfile;
    :cond_a
    const/4 v2, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_b

    .line 173
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v12    # "_result":Lcom/sec/enterprise/knox/ccm/CCMProfile;
    :sswitch_6
    const-string v2, "com.sec.enterprise.knox.ccm.IClientCertificateManager"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 175
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_b

    .line 176
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 181
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_c
    invoke-virtual {p0, v3}, Lcom/sec/enterprise/knox/ccm/IClientCertificateManager$Stub;->deleteCCMProfile(Landroid/app/enterprise/ContextInfo;)Z

    move-result v12

    .line 182
    .local v12, "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 183
    if-eqz v12, :cond_c

    const/4 v2, 0x1

    :goto_d
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 184
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 179
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v12    # "_result":Z
    :cond_b
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_c

    .line 183
    .restart local v12    # "_result":Z
    :cond_c
    const/4 v2, 0x0

    goto :goto_d

    .line 188
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v12    # "_result":Z
    :sswitch_7
    const-string v2, "com.sec.enterprise.knox.ccm.IClientCertificateManager"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 190
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_d

    .line 191
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 197
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_e
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_e

    .line 198
    sget-object v2, Lcom/sec/enterprise/knox/ccm/CertificateProfile;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/enterprise/knox/ccm/CertificateProfile;

    .line 204
    .local v4, "_arg1":Lcom/sec/enterprise/knox/ccm/CertificateProfile;
    :goto_f
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v5

    .line 206
    .local v5, "_arg2":[B
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 207
    .restart local v6    # "_arg3":Ljava/lang/String;
    invoke-virtual {p0, v3, v4, v5, v6}, Lcom/sec/enterprise/knox/ccm/IClientCertificateManager$Stub;->installCertificate(Landroid/app/enterprise/ContextInfo;Lcom/sec/enterprise/knox/ccm/CertificateProfile;[BLjava/lang/String;)Z

    move-result v12

    .line 208
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 209
    if-eqz v12, :cond_f

    const/4 v2, 0x1

    :goto_10
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 210
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 194
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Lcom/sec/enterprise/knox/ccm/CertificateProfile;
    .end local v5    # "_arg2":[B
    .end local v6    # "_arg3":Ljava/lang/String;
    .end local v12    # "_result":Z
    :cond_d
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_e

    .line 201
    :cond_e
    const/4 v4, 0x0

    .restart local v4    # "_arg1":Lcom/sec/enterprise/knox/ccm/CertificateProfile;
    goto :goto_f

    .line 209
    .restart local v5    # "_arg2":[B
    .restart local v6    # "_arg3":Ljava/lang/String;
    .restart local v12    # "_result":Z
    :cond_f
    const/4 v2, 0x0

    goto :goto_10

    .line 214
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Lcom/sec/enterprise/knox/ccm/CertificateProfile;
    .end local v5    # "_arg2":[B
    .end local v6    # "_arg3":Ljava/lang/String;
    .end local v12    # "_result":Z
    :sswitch_8
    const-string v2, "com.sec.enterprise.knox.ccm.IClientCertificateManager"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 216
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_10

    .line 217
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 223
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_11
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 224
    .local v4, "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v3, v4}, Lcom/sec/enterprise/knox/ccm/IClientCertificateManager$Stub;->deleteCertificate(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v12

    .line 225
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 226
    if-eqz v12, :cond_11

    const/4 v2, 0x1

    :goto_12
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 227
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 220
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":Z
    :cond_10
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_11

    .line 226
    .restart local v4    # "_arg1":Ljava/lang/String;
    .restart local v12    # "_result":Z
    :cond_11
    const/4 v2, 0x0

    goto :goto_12

    .line 231
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":Z
    :sswitch_9
    const-string v2, "com.sec.enterprise.knox.ccm.IClientCertificateManager"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 233
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_12

    .line 234
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 239
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_13
    invoke-virtual {p0, v3}, Lcom/sec/enterprise/knox/ccm/IClientCertificateManager$Stub;->isCCMPolicyEnabledForCaller(Landroid/app/enterprise/ContextInfo;)Z

    move-result v12

    .line 240
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 241
    if-eqz v12, :cond_13

    const/4 v2, 0x1

    :goto_14
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 242
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 237
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v12    # "_result":Z
    :cond_12
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_13

    .line 241
    .restart local v12    # "_result":Z
    :cond_13
    const/4 v2, 0x0

    goto :goto_14

    .line 246
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v12    # "_result":Z
    :sswitch_a
    const-string v2, "com.sec.enterprise.knox.ccm.IClientCertificateManager"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 248
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_14

    .line 249
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 255
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_15
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 256
    .restart local v4    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v3, v4}, Lcom/sec/enterprise/knox/ccm/IClientCertificateManager$Stub;->isCCMPolicyEnabledForPackage(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v12

    .line 257
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 258
    if-eqz v12, :cond_15

    const/4 v2, 0x1

    :goto_16
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 259
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 252
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":Z
    :cond_14
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_15

    .line 258
    .restart local v4    # "_arg1":Ljava/lang/String;
    .restart local v12    # "_result":Z
    :cond_15
    const/4 v2, 0x0

    goto :goto_16

    .line 263
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":Z
    :sswitch_b
    const-string v2, "com.sec.enterprise.knox.ccm.IClientCertificateManager"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 265
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_16

    .line 266
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 272
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_17
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 273
    .restart local v4    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v3, v4}, Lcom/sec/enterprise/knox/ccm/IClientCertificateManager$Stub;->getSlotIdForCaller(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)J

    move-result-wide v12

    .line 274
    .local v12, "_result":J
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 275
    move-object/from16 v0, p3

    invoke-virtual {v0, v12, v13}, Landroid/os/Parcel;->writeLong(J)V

    .line 276
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 269
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":J
    :cond_16
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_17

    .line 280
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_c
    const-string v2, "com.sec.enterprise.knox.ccm.IClientCertificateManager"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 282
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_17

    .line 283
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 289
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_18
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 291
    .restart local v4    # "_arg1":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    .line 292
    .local v5, "_arg2":Ljava/lang/String;
    invoke-virtual {p0, v3, v4, v5}, Lcom/sec/enterprise/knox/ccm/IClientCertificateManager$Stub;->getSlotIdForPackage(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v12

    .line 293
    .restart local v12    # "_result":J
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 294
    move-object/from16 v0, p3

    invoke-virtual {v0, v12, v13}, Landroid/os/Parcel;->writeLong(J)V

    .line 295
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 286
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v5    # "_arg2":Ljava/lang/String;
    .end local v12    # "_result":J
    :cond_17
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_18

    .line 299
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_d
    const-string v2, "com.sec.enterprise.knox.ccm.IClientCertificateManager"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 301
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_18

    .line 302
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 308
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_19
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_19

    .line 309
    sget-object v2, Lcom/sec/enterprise/knox/ccm/CSRProfile;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/enterprise/knox/ccm/CSRProfile;

    .line 314
    .local v4, "_arg1":Lcom/sec/enterprise/knox/ccm/CSRProfile;
    :goto_1a
    invoke-virtual {p0, v3, v4}, Lcom/sec/enterprise/knox/ccm/IClientCertificateManager$Stub;->setCSRProfile(Landroid/app/enterprise/ContextInfo;Lcom/sec/enterprise/knox/ccm/CSRProfile;)Z

    move-result v12

    .line 315
    .local v12, "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 316
    if-eqz v12, :cond_1a

    const/4 v2, 0x1

    :goto_1b
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 317
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 305
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Lcom/sec/enterprise/knox/ccm/CSRProfile;
    .end local v12    # "_result":Z
    :cond_18
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_19

    .line 312
    :cond_19
    const/4 v4, 0x0

    .restart local v4    # "_arg1":Lcom/sec/enterprise/knox/ccm/CSRProfile;
    goto :goto_1a

    .line 316
    .restart local v12    # "_result":Z
    :cond_1a
    const/4 v2, 0x0

    goto :goto_1b

    .line 321
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Lcom/sec/enterprise/knox/ccm/CSRProfile;
    .end local v12    # "_result":Z
    :sswitch_e
    const-string v2, "com.sec.enterprise.knox.ccm.IClientCertificateManager"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 323
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_1b

    .line 324
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 330
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_1c
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 331
    .local v4, "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v3, v4}, Lcom/sec/enterprise/knox/ccm/IClientCertificateManager$Stub;->deleteCSRProfile(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v12

    .line 332
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 333
    if-eqz v12, :cond_1c

    const/4 v2, 0x1

    :goto_1d
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 334
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 327
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":Z
    :cond_1b
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_1c

    .line 333
    .restart local v4    # "_arg1":Ljava/lang/String;
    .restart local v12    # "_result":Z
    :cond_1c
    const/4 v2, 0x0

    goto :goto_1d

    .line 338
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":Z
    :sswitch_f
    const-string v2, "com.sec.enterprise.knox.ccm.IClientCertificateManager"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 340
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_1d

    .line 341
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 347
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_1e
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 349
    .restart local v4    # "_arg1":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    .line 351
    .restart local v5    # "_arg2":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 352
    .restart local v6    # "_arg3":Ljava/lang/String;
    invoke-virtual {p0, v3, v4, v5, v6}, Lcom/sec/enterprise/knox/ccm/IClientCertificateManager$Stub;->generateCSRUsingTemplate(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v12

    .line 353
    .local v12, "_result":[B
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 354
    move-object/from16 v0, p3

    invoke-virtual {v0, v12}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 355
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 344
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v5    # "_arg2":Ljava/lang/String;
    .end local v6    # "_arg3":Ljava/lang/String;
    .end local v12    # "_result":[B
    :cond_1d
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_1e

    .line 359
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_10
    const-string v2, "com.sec.enterprise.knox.ccm.IClientCertificateManager"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 361
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 362
    .local v3, "_arg0":I
    invoke-virtual {p0, v3}, Lcom/sec/enterprise/knox/ccm/IClientCertificateManager$Stub;->isCCMPolicyEnabledByAdmin(I)Z

    move-result v12

    .line 363
    .local v12, "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 364
    if-eqz v12, :cond_1e

    const/4 v2, 0x1

    :goto_1f
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 365
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 364
    :cond_1e
    const/4 v2, 0x0

    goto :goto_1f

    .line 369
    .end local v3    # "_arg0":I
    .end local v12    # "_result":Z
    :sswitch_11
    const-string v2, "com.sec.enterprise.knox.ccm.IClientCertificateManager"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 371
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_1f

    .line 372
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 378
    .local v3, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_20
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 379
    .restart local v4    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v3, v4}, Lcom/sec/enterprise/knox/ccm/IClientCertificateManager$Stub;->addPackageToExemptList(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v12

    .line 380
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 381
    if-eqz v12, :cond_20

    const/4 v2, 0x1

    :goto_21
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 382
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 375
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":Z
    :cond_1f
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_20

    .line 381
    .restart local v4    # "_arg1":Ljava/lang/String;
    .restart local v12    # "_result":Z
    :cond_20
    const/4 v2, 0x0

    goto :goto_21

    .line 386
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":Z
    :sswitch_12
    const-string v2, "com.sec.enterprise.knox.ccm.IClientCertificateManager"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 388
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_21

    .line 389
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 395
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_22
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 396
    .restart local v4    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v3, v4}, Lcom/sec/enterprise/knox/ccm/IClientCertificateManager$Stub;->removePackageFromExemptList(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v12

    .line 397
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 398
    if-eqz v12, :cond_22

    const/4 v2, 0x1

    :goto_23
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 399
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 392
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":Z
    :cond_21
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_22

    .line 398
    .restart local v4    # "_arg1":Ljava/lang/String;
    .restart local v12    # "_result":Z
    :cond_22
    const/4 v2, 0x0

    goto :goto_23

    .line 403
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":Z
    :sswitch_13
    const-string v2, "com.sec.enterprise.knox.ccm.IClientCertificateManager"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 404
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/ccm/IClientCertificateManager$Stub;->getCCMVersion()Ljava/lang/String;

    move-result-object v12

    .line 405
    .local v12, "_result":Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 406
    move-object/from16 v0, p3

    invoke-virtual {v0, v12}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 407
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 411
    .end local v12    # "_result":Ljava/lang/String;
    :sswitch_14
    const-string v2, "com.sec.enterprise.knox.ccm.IClientCertificateManager"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 413
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_23

    .line 414
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 420
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_24
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 422
    .restart local v4    # "_arg1":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    .line 424
    .local v5, "_arg2":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v6

    .line 426
    .local v6, "_arg3":[B
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v7

    .restart local v7    # "_arg4":Ljava/lang/String;
    move-object v2, p0

    .line 427
    invoke-virtual/range {v2 .. v7}, Lcom/sec/enterprise/knox/ccm/IClientCertificateManager$Stub;->installObjectWithType(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;I[BLjava/lang/String;)Z

    move-result v12

    .line 428
    .local v12, "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 429
    if-eqz v12, :cond_24

    const/4 v2, 0x1

    :goto_25
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 430
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 417
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v5    # "_arg2":I
    .end local v6    # "_arg3":[B
    .end local v7    # "_arg4":Ljava/lang/String;
    .end local v12    # "_result":Z
    :cond_23
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_24

    .line 429
    .restart local v4    # "_arg1":Ljava/lang/String;
    .restart local v5    # "_arg2":I
    .restart local v6    # "_arg3":[B
    .restart local v7    # "_arg4":Ljava/lang/String;
    .restart local v12    # "_result":Z
    :cond_24
    const/4 v2, 0x0

    goto :goto_25

    .line 434
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v5    # "_arg2":I
    .end local v6    # "_arg3":[B
    .end local v7    # "_arg4":Ljava/lang/String;
    .end local v12    # "_result":Z
    :sswitch_15
    const-string v2, "com.sec.enterprise.knox.ccm.IClientCertificateManager"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 435
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/ccm/IClientCertificateManager$Stub;->getDefaultCertificateAlias()Ljava/lang/String;

    move-result-object v12

    .line 436
    .local v12, "_result":Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 437
    move-object/from16 v0, p3

    invoke-virtual {v0, v12}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 438
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 442
    .end local v12    # "_result":Ljava/lang/String;
    :sswitch_16
    const-string v2, "com.sec.enterprise.knox.ccm.IClientCertificateManager"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 444
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_25

    .line 445
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 450
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_26
    invoke-virtual {p0, v3}, Lcom/sec/enterprise/knox/ccm/IClientCertificateManager$Stub;->getAliasesForCaller(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;

    move-result-object v14

    .line 451
    .local v14, "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 452
    move-object/from16 v0, p3

    invoke-virtual {v0, v14}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 453
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 448
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v14    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_25
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_26

    .line 457
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_17
    const-string v2, "com.sec.enterprise.knox.ccm.IClientCertificateManager"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 459
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_26

    .line 460
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 465
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_27
    invoke-virtual {p0, v3}, Lcom/sec/enterprise/knox/ccm/IClientCertificateManager$Stub;->isAccessControlMethodPassword(Landroid/app/enterprise/ContextInfo;)Z

    move-result v12

    .line 466
    .local v12, "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 467
    if-eqz v12, :cond_27

    const/4 v2, 0x1

    :goto_28
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 468
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 463
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v12    # "_result":Z
    :cond_26
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_27

    .line 467
    .restart local v12    # "_result":Z
    :cond_27
    const/4 v2, 0x0

    goto :goto_28

    .line 472
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v12    # "_result":Z
    :sswitch_18
    const-string v2, "com.sec.enterprise.knox.ccm.IClientCertificateManager"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 473
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/ccm/IClientCertificateManager$Stub;->getAliasesForWiFi()Ljava/util/List;

    move-result-object v14

    .line 474
    .restart local v14    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 475
    move-object/from16 v0, p3

    invoke-virtual {v0, v14}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 476
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 480
    .end local v14    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :sswitch_19
    const-string v2, "com.sec.enterprise.knox.ccm.IClientCertificateManager"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 482
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_28

    .line 483
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 488
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_29
    invoke-virtual {p0, v3}, Lcom/sec/enterprise/knox/ccm/IClientCertificateManager$Stub;->keychainMarkedReset(Landroid/app/enterprise/ContextInfo;)Z

    move-result v12

    .line 489
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 490
    if-eqz v12, :cond_29

    const/4 v2, 0x1

    :goto_2a
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 491
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 486
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v12    # "_result":Z
    :cond_28
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_29

    .line 490
    .restart local v12    # "_result":Z
    :cond_29
    const/4 v2, 0x0

    goto :goto_2a

    .line 495
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v12    # "_result":Z
    :sswitch_1a
    const-string v2, "com.sec.enterprise.knox.ccm.IClientCertificateManager"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 497
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_2a

    .line 498
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 503
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_2b
    invoke-virtual {p0, v3}, Lcom/sec/enterprise/knox/ccm/IClientCertificateManager$Stub;->isCCMEmptyForKeyChain(Landroid/app/enterprise/ContextInfo;)Z

    move-result v12

    .line 504
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 505
    if-eqz v12, :cond_2b

    const/4 v2, 0x1

    :goto_2c
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 506
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 501
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v12    # "_result":Z
    :cond_2a
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_2b

    .line 505
    .restart local v12    # "_result":Z
    :cond_2b
    const/4 v2, 0x0

    goto :goto_2c

    .line 510
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v12    # "_result":Z
    :sswitch_1b
    const-string v2, "com.sec.enterprise.knox.ccm.IClientCertificateManager"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 512
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_2c

    .line 513
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 518
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_2d
    invoke-virtual {p0, v3}, Lcom/sec/enterprise/knox/ccm/IClientCertificateManager$Stub;->getCertificateAliases(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;

    move-result-object v14

    .line 519
    .restart local v14    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 520
    move-object/from16 v0, p3

    invoke-virtual {v0, v14}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 521
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 516
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v14    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_2c
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_2d

    .line 525
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_1c
    const-string v2, "com.sec.enterprise.knox.ccm.IClientCertificateManager"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 527
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_2d

    .line 528
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 533
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_2e
    invoke-virtual {p0, v3}, Lcom/sec/enterprise/knox/ccm/IClientCertificateManager$Stub;->getCertificateAliasesHavingPrivateKey(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;

    move-result-object v14

    .line 534
    .restart local v14    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 535
    move-object/from16 v0, p3

    invoke-virtual {v0, v14}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 536
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 531
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v14    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_2d
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_2e

    .line 540
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_1d
    const-string v2, "com.sec.enterprise.knox.ccm.IClientCertificateManager"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 542
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_2e

    .line 543
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 549
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_2f
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_2f

    .line 550
    sget-object v2, Lcom/sec/enterprise/knox/ccm/CertificateProfile;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/enterprise/knox/ccm/CertificateProfile;

    .line 556
    .local v4, "_arg1":Lcom/sec/enterprise/knox/ccm/CertificateProfile;
    :goto_30
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    .line 558
    .restart local v5    # "_arg2":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v6

    .line 560
    .restart local v6    # "_arg3":[B
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v7

    .restart local v7    # "_arg4":Ljava/lang/String;
    move-object v2, p0

    .line 561
    invoke-virtual/range {v2 .. v7}, Lcom/sec/enterprise/knox/ccm/IClientCertificateManager$Stub;->installObjectWithProfile(Landroid/app/enterprise/ContextInfo;Lcom/sec/enterprise/knox/ccm/CertificateProfile;I[BLjava/lang/String;)Z

    move-result v12

    .line 562
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 563
    if-eqz v12, :cond_30

    const/4 v2, 0x1

    :goto_31
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 564
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 546
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Lcom/sec/enterprise/knox/ccm/CertificateProfile;
    .end local v5    # "_arg2":I
    .end local v6    # "_arg3":[B
    .end local v7    # "_arg4":Ljava/lang/String;
    .end local v12    # "_result":Z
    :cond_2e
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_2f

    .line 553
    :cond_2f
    const/4 v4, 0x0

    .restart local v4    # "_arg1":Lcom/sec/enterprise/knox/ccm/CertificateProfile;
    goto :goto_30

    .line 563
    .restart local v5    # "_arg2":I
    .restart local v6    # "_arg3":[B
    .restart local v7    # "_arg4":Ljava/lang/String;
    .restart local v12    # "_result":Z
    :cond_30
    const/4 v2, 0x0

    goto :goto_31

    .line 42
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0xe -> :sswitch_e
        0xf -> :sswitch_f
        0x10 -> :sswitch_10
        0x11 -> :sswitch_11
        0x12 -> :sswitch_12
        0x13 -> :sswitch_13
        0x14 -> :sswitch_14
        0x15 -> :sswitch_15
        0x16 -> :sswitch_16
        0x17 -> :sswitch_17
        0x18 -> :sswitch_18
        0x19 -> :sswitch_19
        0x1a -> :sswitch_1a
        0x1b -> :sswitch_1b
        0x1c -> :sswitch_1c
        0x1d -> :sswitch_1d
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
