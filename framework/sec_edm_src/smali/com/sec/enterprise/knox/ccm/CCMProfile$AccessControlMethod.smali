.class public final enum Lcom/sec/enterprise/knox/ccm/CCMProfile$AccessControlMethod;
.super Ljava/lang/Enum;
.source "CCMProfile.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/enterprise/knox/ccm/CCMProfile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AccessControlMethod"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/enterprise/knox/ccm/CCMProfile$AccessControlMethod;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/enterprise/knox/ccm/CCMProfile$AccessControlMethod;

.field public static final enum LOCK_STATE:Lcom/sec/enterprise/knox/ccm/CCMProfile$AccessControlMethod;

.field public static final enum PASSWORD:Lcom/sec/enterprise/knox/ccm/CCMProfile$AccessControlMethod;

.field public static final enum TRUSTED_PINPAD:Lcom/sec/enterprise/knox/ccm/CCMProfile$AccessControlMethod;

.field public static final enum TRUSTED_UI:Lcom/sec/enterprise/knox/ccm/CCMProfile$AccessControlMethod;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 55
    new-instance v0, Lcom/sec/enterprise/knox/ccm/CCMProfile$AccessControlMethod;

    const-string v1, "LOCK_STATE"

    invoke-direct {v0, v1, v2}, Lcom/sec/enterprise/knox/ccm/CCMProfile$AccessControlMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/ccm/CCMProfile$AccessControlMethod;->LOCK_STATE:Lcom/sec/enterprise/knox/ccm/CCMProfile$AccessControlMethod;

    .line 63
    new-instance v0, Lcom/sec/enterprise/knox/ccm/CCMProfile$AccessControlMethod;

    const-string v1, "PASSWORD"

    invoke-direct {v0, v1, v3}, Lcom/sec/enterprise/knox/ccm/CCMProfile$AccessControlMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/ccm/CCMProfile$AccessControlMethod;->PASSWORD:Lcom/sec/enterprise/knox/ccm/CCMProfile$AccessControlMethod;

    .line 69
    new-instance v0, Lcom/sec/enterprise/knox/ccm/CCMProfile$AccessControlMethod;

    const-string v1, "TRUSTED_UI"

    invoke-direct {v0, v1, v4}, Lcom/sec/enterprise/knox/ccm/CCMProfile$AccessControlMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/ccm/CCMProfile$AccessControlMethod;->TRUSTED_UI:Lcom/sec/enterprise/knox/ccm/CCMProfile$AccessControlMethod;

    .line 75
    new-instance v0, Lcom/sec/enterprise/knox/ccm/CCMProfile$AccessControlMethod;

    const-string v1, "TRUSTED_PINPAD"

    invoke-direct {v0, v1, v5}, Lcom/sec/enterprise/knox/ccm/CCMProfile$AccessControlMethod;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/enterprise/knox/ccm/CCMProfile$AccessControlMethod;->TRUSTED_PINPAD:Lcom/sec/enterprise/knox/ccm/CCMProfile$AccessControlMethod;

    .line 47
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/enterprise/knox/ccm/CCMProfile$AccessControlMethod;

    sget-object v1, Lcom/sec/enterprise/knox/ccm/CCMProfile$AccessControlMethod;->LOCK_STATE:Lcom/sec/enterprise/knox/ccm/CCMProfile$AccessControlMethod;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/enterprise/knox/ccm/CCMProfile$AccessControlMethod;->PASSWORD:Lcom/sec/enterprise/knox/ccm/CCMProfile$AccessControlMethod;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/enterprise/knox/ccm/CCMProfile$AccessControlMethod;->TRUSTED_UI:Lcom/sec/enterprise/knox/ccm/CCMProfile$AccessControlMethod;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/enterprise/knox/ccm/CCMProfile$AccessControlMethod;->TRUSTED_PINPAD:Lcom/sec/enterprise/knox/ccm/CCMProfile$AccessControlMethod;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/enterprise/knox/ccm/CCMProfile$AccessControlMethod;->$VALUES:[Lcom/sec/enterprise/knox/ccm/CCMProfile$AccessControlMethod;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/enterprise/knox/ccm/CCMProfile$AccessControlMethod;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 47
    const-class v0, Lcom/sec/enterprise/knox/ccm/CCMProfile$AccessControlMethod;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/ccm/CCMProfile$AccessControlMethod;

    return-object v0
.end method

.method public static values()[Lcom/sec/enterprise/knox/ccm/CCMProfile$AccessControlMethod;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/sec/enterprise/knox/ccm/CCMProfile$AccessControlMethod;->$VALUES:[Lcom/sec/enterprise/knox/ccm/CCMProfile$AccessControlMethod;

    invoke-virtual {v0}, [Lcom/sec/enterprise/knox/ccm/CCMProfile$AccessControlMethod;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/enterprise/knox/ccm/CCMProfile$AccessControlMethod;

    return-object v0
.end method
