.class public Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;
.super Ljava/lang/Object;
.source "ClientCertificateManager.java"


# static fields
.field public static final CCM_CERTIFICATE:I = 0x1

.field public static final CCM_FAILURE:I = -0x1

.field public static final CCM_PRIVATE_KEY:I = 0x3

.field public static final CCM_SUCCESS:I

.field private static TAG:Ljava/lang/String;

.field private static mCCMService:Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mContextInfo:Landroid/app/enterprise/ContextInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 131
    const-string v0, "ClientCertificateManager"

    sput-object v0, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V
    .locals 3
    .param p1, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 173
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 174
    iput-object p1, p0, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    .line 175
    iput-object p2, p0, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->mContext:Landroid/content/Context;

    .line 176
    sget-object v0, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ClientCertificateManager API ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget v2, v2, Landroid/app/enterprise/ContextInfo;->mContainerId:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget v2, v2, Landroid/app/enterprise/ContextInfo;->mCallerUid:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 178
    return-void
.end method

.method static declared-synchronized getCCMService()Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;
    .locals 2

    .prologue
    .line 184
    const-class v1, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->mCCMService:Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;

    if-nez v0, :cond_0

    .line 185
    const-string v0, "knox_ccm_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/ccm/IClientCertificateManager$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->mCCMService:Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;

    .line 189
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->mCCMService:Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 184
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public addPackageToExemptList(Ljava/lang/String;)Z
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 1192
    iget-object v1, p0, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ClientCertificateManager.addPackageToExemptList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1195
    :try_start_0
    invoke-static {}, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->getCCMService()Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1196
    sget-object v1, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->mCCMService:Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;

    iget-object v2, p0, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;->addPackageToExemptList(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1204
    :goto_0
    return v1

    .line 1199
    :catch_0
    move-exception v0

    .line 1200
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->TAG:Ljava/lang/String;

    const-string v2, "Failed at ClientCertificateManager API addPackageToExemptList-Exception"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1204
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public deleteCCMProfile()Z
    .locals 3

    .prologue
    .line 619
    iget-object v1, p0, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ClientCertificateManager.deleteCCMProfile"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 622
    :try_start_0
    invoke-static {}, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->getCCMService()Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 623
    sget-object v1, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->mCCMService:Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;

    iget-object v2, p0, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;->deleteCCMProfile(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 628
    :goto_0
    return v1

    .line 625
    :catch_0
    move-exception v0

    .line 626
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->TAG:Ljava/lang/String;

    const-string v2, "Failed in deleteCCMProfile - Exception "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 628
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public deleteCSRProfile(Ljava/lang/String;)Z
    .locals 3
    .param p1, "templateName"    # Ljava/lang/String;

    .prologue
    .line 1033
    iget-object v1, p0, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ClientCertificateManager.deleteCSRProfile"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1036
    :try_start_0
    invoke-static {}, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->getCCMService()Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1037
    sget-object v1, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->mCCMService:Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;

    iget-object v2, p0, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;->deleteCSRProfile(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1044
    :goto_0
    return v1

    .line 1039
    :catch_0
    move-exception v0

    .line 1040
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->TAG:Ljava/lang/String;

    const-string v2, "Failed at ClientCertificateManager API deleteCSRProfile-Exception "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1044
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public deleteCertificate(Ljava/lang/String;)Z
    .locals 3
    .param p1, "alias"    # Ljava/lang/String;

    .prologue
    .line 772
    iget-object v1, p0, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ClientCertificateManager.deleteCertificate"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 775
    :try_start_0
    invoke-static {}, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->getCCMService()Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 776
    sget-object v1, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->mCCMService:Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;

    iget-object v2, p0, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;->deleteCertificate(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 781
    :goto_0
    return v1

    .line 778
    :catch_0
    move-exception v0

    .line 779
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->TAG:Ljava/lang/String;

    const-string v2, "Failed in deleteCertificate - Exception "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 781
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public generateCSR(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)[B
    .locals 8
    .param p1, "UID"    # I
    .param p2, "tokenName"    # Ljava/lang/String;
    .param p3, "tokenPassword"    # Ljava/lang/String;
    .param p4, "objectAlias"    # Ljava/lang/String;
    .param p5, "isTrustedBootRequired"    # Z

    .prologue
    .line 246
    invoke-static {}, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->getCCMService()Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 247
    iget-object v0, p0, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v1, "ClientCertificateManager.generateCSR"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 250
    :try_start_0
    invoke-static {}, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->getCCMService()Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 251
    sget-object v0, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->mCCMService:Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;

    iget-object v1, p0, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    invoke-interface/range {v0 .. v6}, Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;->generateCSR(Landroid/app/enterprise/ContextInfo;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)[B
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 261
    :goto_0
    return-object v0

    .line 255
    :catch_0
    move-exception v7

    .line 256
    .local v7, "e":Landroid/os/RemoteException;
    sget-object v0, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->TAG:Ljava/lang/String;

    const-string v1, "Failed at ClientCertificateManager API ccmGenerateCSR-Exception"

    invoke-static {v0, v1, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 261
    .end local v7    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public generateCSRUsingTemplate(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[B
    .locals 3
    .param p1, "templateName"    # Ljava/lang/String;
    .param p2, "alias"    # Ljava/lang/String;
    .param p3, "challengePassword"    # Ljava/lang/String;

    .prologue
    .line 1114
    iget-object v1, p0, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ClientCertificateManager.generateCSRUsingTemplate"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1117
    :try_start_0
    invoke-static {}, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->getCCMService()Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1118
    sget-object v1, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->mCCMService:Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;

    iget-object v2, p0, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2, p3}, Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;->generateCSRUsingTemplate(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[B
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1126
    :goto_0
    return-object v1

    .line 1121
    :catch_0
    move-exception v0

    .line 1122
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->TAG:Ljava/lang/String;

    const-string v2, "Failed at ClientCertificateManager API generateCSRUsingTemplate-Exception"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1126
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getCCMProfile()Lcom/sec/enterprise/knox/ccm/CCMProfile;
    .locals 3

    .prologue
    .line 542
    iget-object v1, p0, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ClientCertificateManager.getCCMProfile"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 545
    :try_start_0
    invoke-static {}, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->getCCMService()Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 546
    sget-object v1, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->mCCMService:Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;

    iget-object v2, p0, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;->getCCMProfile(Landroid/app/enterprise/ContextInfo;)Lcom/sec/enterprise/knox/ccm/CCMProfile;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 553
    :goto_0
    return-object v1

    .line 548
    :catch_0
    move-exception v0

    .line 549
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->TAG:Ljava/lang/String;

    const-string v2, "Failed at ClientCertificateManager API getCCMProfile-Exception "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 553
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getCCMProfile(Lcom/sec/enterprise/knox/ccm/CCMProfile;)Z
    .locals 1
    .param p1, "profile"    # Lcom/sec/enterprise/knox/ccm/CCMProfile;

    .prologue
    .line 558
    const/4 v0, 0x0

    return v0
.end method

.method public getCCMVersion()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1328
    iget-object v1, p0, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ClientCertificateManager.getCCMVersion"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1331
    :try_start_0
    invoke-static {}, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->getCCMService()Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1332
    sget-object v1, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->mCCMService:Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;

    invoke-interface {v1}, Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;->getCCMVersion()Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1339
    :goto_0
    return-object v1

    .line 1334
    :catch_0
    move-exception v0

    .line 1335
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->TAG:Ljava/lang/String;

    const-string v2, "Failed at ClientCertificateManager API getCCMVersion-Exception"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1339
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getCertificateAliases()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1452
    iget-object v1, p0, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ClientCertificateManager.getCertificateAliases"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1455
    :try_start_0
    invoke-static {}, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->getCCMService()Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1456
    sget-object v1, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->mCCMService:Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;

    iget-object v2, p0, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;->getCertificateAliases(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1463
    :goto_0
    return-object v1

    .line 1458
    :catch_0
    move-exception v0

    .line 1459
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->TAG:Ljava/lang/String;

    const-string v2, "Failed at ClientCertificateManager API getCertificateAliases-Exception"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1463
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getDefaultCertificateAlias()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1391
    iget-object v1, p0, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ClientCertificateManager.getDefaultCertificateAlias"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1394
    :try_start_0
    invoke-static {}, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->getCCMService()Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1395
    sget-object v1, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->mCCMService:Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;

    invoke-interface {v1}, Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;->getDefaultCertificateAlias()Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1402
    :goto_0
    return-object v1

    .line 1397
    :catch_0
    move-exception v0

    .line 1398
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->TAG:Ljava/lang/String;

    const-string v2, "Failed at ClientCertificateManager API getDefaultCertificateAlias-Exception "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1402
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getSlotIdForCaller(Ljava/lang/String;)J
    .locals 4
    .param p1, "alias"    # Ljava/lang/String;

    .prologue
    .line 885
    sget-object v1, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->TAG:Ljava/lang/String;

    const-string v2, "getSlotIdForCaller"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 888
    :try_start_0
    invoke-static {}, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->getCCMService()Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 889
    sget-object v1, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->mCCMService:Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;

    iget-object v2, p0, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;->getSlotIdForCaller(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 895
    :goto_0
    return-wide v2

    .line 891
    :catch_0
    move-exception v0

    .line 892
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->TAG:Ljava/lang/String;

    const-string v2, "getSlotIdForCaller failed - Exception "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 895
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const-wide/16 v2, -0x1

    goto :goto_0
.end method

.method public getSlotIdForPackage(Ljava/lang/String;Ljava/lang/String;)J
    .locals 4
    .param p1, "alias"    # Ljava/lang/String;
    .param p2, "packageName"    # Ljava/lang/String;

    .prologue
    .line 903
    :try_start_0
    invoke-static {}, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->getCCMService()Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 904
    sget-object v1, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->mCCMService:Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;

    iget-object v2, p0, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p2, p1}, Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;->getSlotIdForPackage(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 911
    :goto_0
    return-wide v2

    .line 907
    :catch_0
    move-exception v0

    .line 908
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->TAG:Ljava/lang/String;

    const-string v2, "getSlotIdForPackage failed - Exception "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 911
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const-wide/16 v2, -0x1

    goto :goto_0
.end method

.method public installCertificate(Lcom/sec/enterprise/knox/ccm/CertificateProfile;[BLjava/lang/String;)Z
    .locals 3
    .param p1, "profile"    # Lcom/sec/enterprise/knox/ccm/CertificateProfile;
    .param p2, "certificateBuffer"    # [B
    .param p3, "privateKeyPassword"    # Ljava/lang/String;

    .prologue
    .line 702
    iget-object v1, p0, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ClientCertificateManager.installCertificate"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 705
    :try_start_0
    invoke-static {}, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->getCCMService()Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 706
    sget-object v1, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->mCCMService:Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;

    iget-object v2, p0, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2, p3}, Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;->installCertificate(Landroid/app/enterprise/ContextInfo;Lcom/sec/enterprise/knox/ccm/CertificateProfile;[BLjava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 714
    :goto_0
    return v1

    .line 709
    :catch_0
    move-exception v0

    .line 710
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->TAG:Ljava/lang/String;

    const-string v2, "Failed at ClientCertificateManager API installCertificate-Exception "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 714
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public installObject(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;I[BLjava/lang/String;Z)I
    .locals 12
    .param p1, "UID"    # I
    .param p2, "tokenName"    # Ljava/lang/String;
    .param p3, "tokenPassword"    # Ljava/lang/String;
    .param p4, "objectAlias"    # Ljava/lang/String;
    .param p5, "objectType"    # I
    .param p6, "objectData"    # [B
    .param p7, "objectPassword"    # Ljava/lang/String;
    .param p8, "isTrustedBootRequired"    # Z

    .prologue
    .line 330
    invoke-static {}, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->getCCMService()Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 331
    const/4 v11, 0x0

    .line 332
    .local v11, "ret":I
    iget-object v0, p0, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v1, "ClientCertificateManager.installObject"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 335
    :try_start_0
    invoke-static {}, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->getCCMService()Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 336
    sget-object v0, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->mCCMService:Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;

    iget-object v1, p0, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move/from16 v9, p8

    invoke-interface/range {v0 .. v9}, Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;->installObject(Landroid/app/enterprise/ContextInfo;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;I[BLjava/lang/String;Z)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 346
    .end local v11    # "ret":I
    :goto_0
    return v0

    .line 340
    .restart local v11    # "ret":I
    :catch_0
    move-exception v10

    .line 341
    .local v10, "e":Landroid/os/RemoteException;
    sget-object v0, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->TAG:Ljava/lang/String;

    const-string v1, "Failed at ClientCertificateManager API ccmInstallObject-Exception"

    invoke-static {v0, v1, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 346
    .end local v10    # "e":Landroid/os/RemoteException;
    .end local v11    # "ret":I
    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public isCCMPolicyEnabledForCaller()Z
    .locals 3

    .prologue
    .line 788
    sget-object v1, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->TAG:Ljava/lang/String;

    const-string v2, "isCCMPolicyEnabledForCaller"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 791
    :try_start_0
    invoke-static {}, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->getCCMService()Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 792
    sget-object v1, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->mCCMService:Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;

    iget-object v2, p0, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;->isCCMPolicyEnabledForCaller(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 799
    :goto_0
    return v1

    .line 794
    :catch_0
    move-exception v0

    .line 795
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->TAG:Ljava/lang/String;

    const-string v2, "Failed at ClientCertificateManager API isCCMPolicyEnabledForCaller-Exception "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 799
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isCCMPolicyEnabledForPackage(Ljava/lang/String;)Z
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 853
    iget-object v2, p0, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "ClientCertificateManager.isCCMPolicyEnabledForPackage"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 856
    if-nez p1, :cond_1

    .line 857
    sget-object v2, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->TAG:Ljava/lang/String;

    const-string v3, "isCCMPolicyEnabledForPackage failed - Invalid arguments"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 878
    :cond_0
    :goto_0
    return v1

    .line 862
    :cond_1
    iget-object v2, p0, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->mContext:Landroid/content/Context;

    if-nez v2, :cond_2

    .line 863
    sget-object v2, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->TAG:Ljava/lang/String;

    const-string v3, "isCCMPolicyEnabledForPackage failed - Invalid Context"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 868
    :cond_2
    :try_start_0
    invoke-static {}, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->getCCMService()Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 869
    sget-object v2, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->mCCMService:Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;

    iget-object v3, p0, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3, p1}, Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;->isCCMPolicyEnabledForPackage(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 872
    :catch_0
    move-exception v0

    .line 873
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->TAG:Ljava/lang/String;

    const-string v3, "Failed at ClientCertificateManager API isCCMPolicyEnabledForPackage-Exception "

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public registerForDefaultCertificate(ILjava/lang/String;Ljava/lang/String;Z)I
    .locals 8
    .param p1, "UID"    # I
    .param p2, "registrationPassword"    # Ljava/lang/String;
    .param p3, "oldPassword"    # Ljava/lang/String;
    .param p4, "isTrustedBootRequired"    # Z

    .prologue
    .line 404
    invoke-static {}, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->getCCMService()Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 405
    const/4 v7, 0x0

    .line 406
    .local v7, "ret":I
    iget-object v0, p0, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v1, "ClientCertificateManager.registerForDefaultCertificate"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 409
    :try_start_0
    invoke-static {}, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->getCCMService()Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 410
    sget-object v0, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->mCCMService:Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;

    iget-object v1, p0, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-interface/range {v0 .. v5}, Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;->registerForDefaultCertificate(Landroid/app/enterprise/ContextInfo;ILjava/lang/String;Ljava/lang/String;Z)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 420
    .end local v7    # "ret":I
    :goto_0
    return v0

    .line 414
    .restart local v7    # "ret":I
    :catch_0
    move-exception v6

    .line 415
    .local v6, "e":Landroid/os/RemoteException;
    sget-object v0, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->TAG:Ljava/lang/String;

    const-string v1, "Failed at ClientCertificateManager API ccmRegisterForDefaultCertificate-Exception"

    invoke-static {v0, v1, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 420
    .end local v6    # "e":Landroid/os/RemoteException;
    .end local v7    # "ret":I
    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public removePackageFromExemptList(Ljava/lang/String;)Z
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 1267
    iget-object v1, p0, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ClientCertificateManager.removePackageFromExemptList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1270
    :try_start_0
    invoke-static {}, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->getCCMService()Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1271
    sget-object v1, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->mCCMService:Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;

    iget-object v2, p0, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;->removePackageFromExemptList(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1279
    :goto_0
    return v1

    .line 1274
    :catch_0
    move-exception v0

    .line 1275
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->TAG:Ljava/lang/String;

    const-string v2, "Failed at ClientCertificateManager API removePackageFromExemptList-Exception"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1279
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setCCMProfile(Lcom/sec/enterprise/knox/ccm/CCMProfile;)Z
    .locals 3
    .param p1, "profile"    # Lcom/sec/enterprise/knox/ccm/CCMProfile;

    .prologue
    .line 480
    iget-object v1, p0, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ClientCertificateManager.setCCMProfile"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 483
    :try_start_0
    invoke-static {}, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->getCCMService()Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 484
    sget-object v1, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->mCCMService:Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;

    iget-object v2, p0, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;->setCCMProfile(Landroid/app/enterprise/ContextInfo;Lcom/sec/enterprise/knox/ccm/CCMProfile;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 491
    :goto_0
    return v1

    .line 486
    :catch_0
    move-exception v0

    .line 487
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->TAG:Ljava/lang/String;

    const-string v2, "Failed at ClientCertificateManager API setCCMProfile-Exception "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 491
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setCSRProfile(Lcom/sec/enterprise/knox/ccm/CSRProfile;)Z
    .locals 3
    .param p1, "profile"    # Lcom/sec/enterprise/knox/ccm/CSRProfile;

    .prologue
    .line 968
    iget-object v1, p0, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ClientCertificateManager.setCSRProfile"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 971
    :try_start_0
    invoke-static {}, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->getCCMService()Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 972
    sget-object v1, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->mCCMService:Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;

    iget-object v2, p0, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Lcom/sec/enterprise/knox/ccm/IClientCertificateManager;->setCSRProfile(Landroid/app/enterprise/ContextInfo;Lcom/sec/enterprise/knox/ccm/CSRProfile;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 979
    :goto_0
    return v1

    .line 974
    :catch_0
    move-exception v0

    .line 975
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->TAG:Ljava/lang/String;

    const-string v2, "Failed at ClientCertificateManager API setCSRProfile-Exception "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 979
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
