.class public Lcom/sec/enterprise/knox/GenericVpnPolicy;
.super Ljava/lang/Object;
.source "GenericVpnPolicy.java"


# static fields
.field private static final CONTAINER_VPN_PERMISSION:Ljava/lang/String; = "com.sec.enterprise.knox.KNOX_CONTAINER_VPN"

.field private static final DBG:Z

.field private static final DEFAULT_VPN_PKG:Ljava/lang/String; = "com.mocana.vpn.android"

.field private static final INVALID_CONTAINER_ID:I = 0x0

.field private static final KNOX_VPN_V1_ENABLED:Z

.field private static final KNOX_VPN_V2_ENABLED:Z

.field private static TAG:Ljava/lang/String; = null

.field private static final VPN_PERMISSION:Ljava/lang/String; = "com.sec.enterprise.knox.KNOX_GENERIC_VPN"

.field public static VPN_RETURN_BOOL_ERROR:Z

.field public static VPN_RETURN_INT_ERROR:I

.field private static genericVpnObjectMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/enterprise/knox/GenericVpnPolicy;",
            ">;"
        }
    .end annotation
.end field

.field private static mEnterpriseDeviceManager:Landroid/app/enterprise/IEnterpriseDeviceManager;

.field private static mEnterpriseVpnPolicyService:Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;

.field private static mKnoxVpnPolicyService:Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;


# instance fields
.field private mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/app/enterprise/EnterpriseResponseData",
            "<*>;"
        }
    .end annotation
.end field

.field private vendorName:Ljava/lang/String;

.field private vpnContext:Lcom/sec/enterprise/knox/KnoxVpnContext;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 137
    const-string v2, "1"

    const-string v3, "ro.config.knox"

    const-string v4, "0"

    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    sput-boolean v2, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V1_ENABLED:Z

    .line 139
    const-string v2, "v30"

    const-string v3, "ro.config.knox"

    const-string v4, "0"

    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    sput-boolean v2, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V2_ENABLED:Z

    .line 143
    const/4 v2, -0x1

    sput v2, Lcom/sec/enterprise/knox/GenericVpnPolicy;->VPN_RETURN_INT_ERROR:I

    .line 145
    sput-boolean v0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->VPN_RETURN_BOOL_ERROR:Z

    .line 147
    const-string v2, "GenericVpnPolicy"

    sput-object v2, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    .line 149
    invoke-static {}, Landroid/os/Debug;->isProductShip()I

    move-result v2

    if-ne v2, v1, :cond_0

    :goto_0
    sput-boolean v0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->DBG:Z

    .line 167
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseDeviceManager:Landroid/app/enterprise/IEnterpriseDeviceManager;

    .line 174
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->genericVpnObjectMap:Ljava/util/HashMap;

    return-void

    :cond_0
    move v0, v1

    .line 149
    goto :goto_0
.end method

.method private constructor <init>(Lcom/sec/enterprise/knox/KnoxVpnContext;)V
    .locals 3
    .param p1, "vpnContext"    # Lcom/sec/enterprise/knox/KnoxVpnContext;

    .prologue
    const/4 v0, 0x0

    .line 233
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 160
    iput-object v0, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->vpnContext:Lcom/sec/enterprise/knox/KnoxVpnContext;

    .line 162
    iput-object v0, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->vendorName:Ljava/lang/String;

    .line 234
    sget-boolean v0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->DBG:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "GenericVpnPolicy ctor : vendorName = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/sec/enterprise/knox/KnoxVpnContext;->vendorName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 235
    :cond_0
    iput-object p1, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->vpnContext:Lcom/sec/enterprise/knox/KnoxVpnContext;

    .line 236
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 3
    .param p1, "vendorName"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 177
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 160
    iput-object v0, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->vpnContext:Lcom/sec/enterprise/knox/KnoxVpnContext;

    .line 162
    iput-object v0, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->vendorName:Ljava/lang/String;

    .line 178
    sget-boolean v0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->DBG:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "GenericVpnPolicy ctor : vendorName = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 179
    :cond_0
    iput-object p1, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->vendorName:Ljava/lang/String;

    .line 180
    return-void
.end method

.method private static checkIfAdminHasVpnPermission(ILjava/lang/String;)Z
    .locals 6
    .param p0, "uid"    # I
    .param p1, "permission"    # Ljava/lang/String;

    .prologue
    .line 326
    const/4 v2, 0x0

    .line 328
    .local v2, "permissionGranted":Z
    :try_start_0
    new-instance v0, Landroid/app/enterprise/ContextInfo;

    invoke-direct {v0, p0}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    .line 329
    .local v0, "cxtInfo":Landroid/app/enterprise/ContextInfo;
    invoke-static {}, Lcom/sec/enterprise/knox/GenericVpnPolicy;->getEnterpriseDeviceManager()Landroid/app/enterprise/IEnterpriseDeviceManager;

    move-result-object v3

    invoke-interface {v3, v0, p1}, Landroid/app/enterprise/IEnterpriseDeviceManager;->enforceActiveAdminPermissionByContext(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Landroid/app/enterprise/ContextInfo;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 330
    const/4 v2, 0x1

    .line 334
    .end local v0    # "cxtInfo":Landroid/app/enterprise/ContextInfo;
    :goto_0
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "The Permision requested is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "and permission granted is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 335
    return v2

    .line 331
    :catch_0
    move-exception v1

    .line 332
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception: checkIfAdminHasVpnPermission "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private static checkIfUserIsPersona(I)Z
    .locals 4
    .param p0, "containerId"    # I

    .prologue
    .line 291
    const/4 v0, 0x0

    .line 292
    .local v0, "isUserPersona":Z
    sget-boolean v1, Lcom/sec/enterprise/knox/GenericVpnPolicy;->DBG:Z

    if-eqz v1, :cond_0

    sget-object v1, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ifUserIsPersona: containerId value is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 305
    :cond_0
    return v0
.end method

.method public static getCidFromTransformedName(Ljava/lang/String;)I
    .locals 3
    .param p0, "vendorNameWithCid"    # Ljava/lang/String;

    .prologue
    .line 2094
    const-string v2, "_"

    invoke-virtual {p0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 2095
    .local v1, "index":I
    const/4 v2, 0x0

    invoke-virtual {p0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 2096
    .local v0, "cid":Ljava/lang/String;
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    return v2
.end method

.method private static getEnterpriseDeviceManager()Landroid/app/enterprise/IEnterpriseDeviceManager;
    .locals 1

    .prologue
    .line 319
    sget-object v0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseDeviceManager:Landroid/app/enterprise/IEnterpriseDeviceManager;

    if-nez v0, :cond_0

    .line 320
    const-string v0, "enterprise_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/app/enterprise/IEnterpriseDeviceManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/IEnterpriseDeviceManager;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseDeviceManager:Landroid/app/enterprise/IEnterpriseDeviceManager;

    .line 322
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseDeviceManager:Landroid/app/enterprise/IEnterpriseDeviceManager;

    return-object v0
.end method

.method private static getEnterpriseVpnPolicyService()Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;
    .locals 3

    .prologue
    .line 225
    sget-object v0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseVpnPolicyService:Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;

    if-nez v0, :cond_0

    .line 226
    const-string v0, "generic_vpn_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseVpnPolicyService:Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;

    .line 229
    :cond_0
    sget-boolean v0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->DBG:Z

    if-eqz v0, :cond_1

    sget-object v0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "GenericVpnPolicy getService : mEnterpriseVpnPolicyService = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseVpnPolicyService:Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 230
    :cond_1
    sget-object v0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseVpnPolicyService:Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;

    return-object v0
.end method

.method public static getInstance()Lcom/sec/enterprise/knox/GenericVpnPolicy;
    .locals 1

    .prologue
    .line 221
    const-string v0, "com.mocana.vpn.android"

    invoke-static {v0}, Lcom/sec/enterprise/knox/GenericVpnPolicy;->getInstance(Ljava/lang/String;)Lcom/sec/enterprise/knox/GenericVpnPolicy;

    move-result-object v0

    return-object v0
.end method

.method public static declared-synchronized getInstance(Lcom/sec/enterprise/knox/KnoxVpnContext;)Lcom/sec/enterprise/knox/GenericVpnPolicy;
    .locals 12
    .param p0, "vpnContext"    # Lcom/sec/enterprise/knox/KnoxVpnContext;

    .prologue
    .line 239
    const-class v8, Lcom/sec/enterprise/knox/GenericVpnPolicy;

    monitor-enter v8

    :try_start_0
    iget-object v5, p0, Lcom/sec/enterprise/knox/KnoxVpnContext;->vendorName:Ljava/lang/String;

    .line 240
    .local v5, "vendorName":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/enterprise/knox/KnoxVpnContext;->vendorName:Ljava/lang/String;

    iget v9, p0, Lcom/sec/enterprise/knox/KnoxVpnContext;->personaId:I

    invoke-static {v7, v9}, Lcom/sec/enterprise/knox/GenericVpnPolicy;->getTransformedVendorName(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v6

    .line 241
    .local v6, "vendorNameWithCid":Ljava/lang/String;
    sget-boolean v7, Lcom/sec/enterprise/knox/GenericVpnPolicy;->DBG:Z

    if-eqz v7, :cond_0

    sget-object v7, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "GenericVpnPolicy getInstance : vendorName = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 242
    :cond_0
    sget-boolean v7, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V2_ENABLED:Z

    if-eqz v7, :cond_1

    .line 243
    sget-object v7, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    const-string v9, "The Knox Version 2 installed in the device."

    invoke-static {v7, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 244
    iget v7, p0, Lcom/sec/enterprise/knox/KnoxVpnContext;->personaId:I

    invoke-static {v7}, Lcom/sec/enterprise/knox/GenericVpnPolicy;->checkIfUserIsPersona(I)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 245
    sget-object v7, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    const-string v9, "checkIfUserIsPersona value is true"

    invoke-static {v7, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 246
    iget v7, p0, Lcom/sec/enterprise/knox/KnoxVpnContext;->adminId:I

    const-string v9, "com.sec.enterprise.knox.KNOX_CONTAINER_VPN"

    invoke-static {v7, v9}, Lcom/sec/enterprise/knox/GenericVpnPolicy;->checkIfAdminHasVpnPermission(ILjava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 252
    :cond_1
    :goto_0
    const/4 v3, 0x0

    .line 254
    .local v3, "eVpn":Lcom/sec/enterprise/knox/GenericVpnPolicy;
    if-eqz v6, :cond_5

    .line 255
    :try_start_1
    const-class v9, Lcom/sec/enterprise/knox/GenericVpnPolicy;

    monitor-enter v9
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 257
    :try_start_2
    sget-object v7, Lcom/sec/enterprise/knox/GenericVpnPolicy;->genericVpnObjectMap:Ljava/util/HashMap;

    invoke-virtual {v7, v6}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 258
    sget-object v7, Lcom/sec/enterprise/knox/GenericVpnPolicy;->genericVpnObjectMap:Ljava/util/HashMap;

    invoke-virtual {v7, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    move-object v0, v7

    check-cast v0, Lcom/sec/enterprise/knox/GenericVpnPolicy;

    move-object v3, v0

    .line 264
    :goto_1
    if-eqz v3, :cond_4

    .line 266
    const/4 v1, 0x0

    .line 268
    .local v1, "bindSuccess":Z
    sget-boolean v7, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V1_ENABLED:Z

    if-eqz v7, :cond_8

    .line 269
    invoke-static {}, Lcom/sec/enterprise/knox/GenericVpnPolicy;->getEnterpriseVpnPolicyService()Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;

    move-result-object v7

    invoke-interface {v7, v6}, Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;->bindKnoxVpnInterface(Ljava/lang/String;)Z

    move-result v1

    .line 273
    :cond_2
    :goto_2
    sget-boolean v7, Lcom/sec/enterprise/knox/GenericVpnPolicy;->DBG:Z

    if-eqz v7, :cond_3

    sget-object v7, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "GenericVpnPolicy getInstance : bindSuccess = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v7, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    :cond_3
    if-nez v1, :cond_4

    .line 275
    sget-object v7, Lcom/sec/enterprise/knox/GenericVpnPolicy;->genericVpnObjectMap:Ljava/util/HashMap;

    invoke-virtual {v7, v6}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 276
    const/4 v3, 0x0

    .line 279
    .end local v1    # "bindSuccess":Z
    :cond_4
    monitor-exit v9
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 285
    :cond_5
    :goto_3
    monitor-exit v8

    return-object v3

    .line 248
    .end local v3    # "eVpn":Lcom/sec/enterprise/knox/GenericVpnPolicy;
    :cond_6
    :try_start_3
    sget-object v7, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    const-string v9, "checkIfUserIsPersona value is false"

    invoke-static {v7, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 249
    iget v7, p0, Lcom/sec/enterprise/knox/KnoxVpnContext;->adminId:I

    const-string v9, "com.sec.enterprise.knox.KNOX_GENERIC_VPN"

    invoke-static {v7, v9}, Lcom/sec/enterprise/knox/GenericVpnPolicy;->checkIfAdminHasVpnPermission(ILjava/lang/String;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 239
    .end local v5    # "vendorName":Ljava/lang/String;
    .end local v6    # "vendorNameWithCid":Ljava/lang/String;
    :catchall_0
    move-exception v7

    monitor-exit v8

    throw v7

    .line 260
    .restart local v3    # "eVpn":Lcom/sec/enterprise/knox/GenericVpnPolicy;
    .restart local v5    # "vendorName":Ljava/lang/String;
    .restart local v6    # "vendorNameWithCid":Ljava/lang/String;
    :cond_7
    :try_start_4
    new-instance v4, Lcom/sec/enterprise/knox/GenericVpnPolicy;

    invoke-direct {v4, p0}, Lcom/sec/enterprise/knox/GenericVpnPolicy;-><init>(Lcom/sec/enterprise/knox/KnoxVpnContext;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 261
    .end local v3    # "eVpn":Lcom/sec/enterprise/knox/GenericVpnPolicy;
    .local v4, "eVpn":Lcom/sec/enterprise/knox/GenericVpnPolicy;
    :try_start_5
    sget-object v7, Lcom/sec/enterprise/knox/GenericVpnPolicy;->genericVpnObjectMap:Ljava/util/HashMap;

    invoke-virtual {v7, v6, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    move-object v3, v4

    .end local v4    # "eVpn":Lcom/sec/enterprise/knox/GenericVpnPolicy;
    .restart local v3    # "eVpn":Lcom/sec/enterprise/knox/GenericVpnPolicy;
    goto :goto_1

    .line 270
    .restart local v1    # "bindSuccess":Z
    :cond_8
    :try_start_6
    sget-boolean v7, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V2_ENABLED:Z

    if-eqz v7, :cond_2

    .line 271
    invoke-static {}, Lcom/sec/enterprise/knox/GenericVpnPolicy;->getKnoxVpnPolicyService()Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;

    move-result-object v7

    invoke-interface {v7, v6}, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;->bindKnoxVpnInterface(Ljava/lang/String;)Z

    move-result v1

    goto :goto_2

    .line 279
    .end local v1    # "bindSuccess":Z
    :catchall_1
    move-exception v7

    :goto_4
    monitor-exit v9
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :try_start_7
    throw v7
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 281
    :catch_0
    move-exception v2

    .line 282
    .local v2, "e":Landroid/os/RemoteException;
    :try_start_8
    sget-object v7, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "GenericVpnPolicy getInstance : returning null for vendorName = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "; Exception = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 283
    const/4 v3, 0x0

    goto :goto_3

    .line 279
    .end local v2    # "e":Landroid/os/RemoteException;
    .end local v3    # "eVpn":Lcom/sec/enterprise/knox/GenericVpnPolicy;
    .restart local v4    # "eVpn":Lcom/sec/enterprise/knox/GenericVpnPolicy;
    :catchall_2
    move-exception v7

    move-object v3, v4

    .end local v4    # "eVpn":Lcom/sec/enterprise/knox/GenericVpnPolicy;
    .restart local v3    # "eVpn":Lcom/sec/enterprise/knox/GenericVpnPolicy;
    goto :goto_4
.end method

.method public static getInstance(Ljava/lang/String;)Lcom/sec/enterprise/knox/GenericVpnPolicy;
    .locals 9
    .param p0, "vendorName"    # Ljava/lang/String;

    .prologue
    .line 183
    sget-boolean v5, Lcom/sec/enterprise/knox/GenericVpnPolicy;->DBG:Z

    if-eqz v5, :cond_0

    sget-object v5, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "GenericVpnPolicy getInstance : vendorName = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    :cond_0
    const/4 v3, 0x0

    .line 186
    .local v3, "eVpn":Lcom/sec/enterprise/knox/GenericVpnPolicy;
    if-eqz p0, :cond_4

    .line 187
    :try_start_0
    const-class v6, Lcom/sec/enterprise/knox/GenericVpnPolicy;

    monitor-enter v6
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 189
    :try_start_1
    sget-object v5, Lcom/sec/enterprise/knox/GenericVpnPolicy;->genericVpnObjectMap:Ljava/util/HashMap;

    invoke-virtual {v5, p0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 190
    sget-object v5, Lcom/sec/enterprise/knox/GenericVpnPolicy;->genericVpnObjectMap:Ljava/util/HashMap;

    invoke-virtual {v5, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    move-object v0, v5

    check-cast v0, Lcom/sec/enterprise/knox/GenericVpnPolicy;

    move-object v3, v0

    .line 196
    :goto_0
    if-eqz v3, :cond_3

    .line 198
    const/4 v1, 0x0

    .line 200
    .local v1, "bindSuccess":Z
    sget-boolean v5, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V1_ENABLED:Z

    if-eqz v5, :cond_6

    .line 201
    invoke-static {}, Lcom/sec/enterprise/knox/GenericVpnPolicy;->getEnterpriseVpnPolicyService()Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;

    move-result-object v5

    invoke-interface {v5, p0}, Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;->bindKnoxVpnInterface(Ljava/lang/String;)Z

    move-result v1

    .line 205
    :cond_1
    :goto_1
    sget-boolean v5, Lcom/sec/enterprise/knox/GenericVpnPolicy;->DBG:Z

    if-eqz v5, :cond_2

    sget-object v5, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "GenericVpnPolicy getInstance : bindSuccess = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 206
    :cond_2
    if-nez v1, :cond_3

    .line 207
    sget-object v5, Lcom/sec/enterprise/knox/GenericVpnPolicy;->genericVpnObjectMap:Ljava/util/HashMap;

    invoke-virtual {v5, p0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 208
    const/4 v3, 0x0

    .line 211
    .end local v1    # "bindSuccess":Z
    :cond_3
    monitor-exit v6

    .line 217
    :cond_4
    :goto_2
    return-object v3

    .line 192
    :cond_5
    new-instance v4, Lcom/sec/enterprise/knox/GenericVpnPolicy;

    invoke-direct {v4, p0}, Lcom/sec/enterprise/knox/GenericVpnPolicy;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 193
    .end local v3    # "eVpn":Lcom/sec/enterprise/knox/GenericVpnPolicy;
    .local v4, "eVpn":Lcom/sec/enterprise/knox/GenericVpnPolicy;
    :try_start_2
    sget-object v5, Lcom/sec/enterprise/knox/GenericVpnPolicy;->genericVpnObjectMap:Ljava/util/HashMap;

    invoke-virtual {v5, p0, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-object v3, v4

    .end local v4    # "eVpn":Lcom/sec/enterprise/knox/GenericVpnPolicy;
    .restart local v3    # "eVpn":Lcom/sec/enterprise/knox/GenericVpnPolicy;
    goto :goto_0

    .line 202
    .restart local v1    # "bindSuccess":Z
    :cond_6
    :try_start_3
    sget-boolean v5, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V2_ENABLED:Z

    if-eqz v5, :cond_1

    .line 203
    invoke-static {}, Lcom/sec/enterprise/knox/GenericVpnPolicy;->getKnoxVpnPolicyService()Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;

    move-result-object v5

    invoke-interface {v5, p0}, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;->bindKnoxVpnInterface(Ljava/lang/String;)Z

    move-result v1

    goto :goto_1

    .line 211
    .end local v1    # "bindSuccess":Z
    :catchall_0
    move-exception v5

    :goto_3
    monitor-exit v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v5
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    .line 213
    :catch_0
    move-exception v2

    .line 214
    .local v2, "e":Ljava/lang/Exception;
    sget-boolean v5, Lcom/sec/enterprise/knox/GenericVpnPolicy;->DBG:Z

    if-eqz v5, :cond_7

    sget-object v5, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "GenericVpnPolicy getInstance : returning null for vendorName = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 215
    :cond_7
    const/4 v3, 0x0

    goto :goto_2

    .line 211
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v3    # "eVpn":Lcom/sec/enterprise/knox/GenericVpnPolicy;
    .restart local v4    # "eVpn":Lcom/sec/enterprise/knox/GenericVpnPolicy;
    :catchall_1
    move-exception v5

    move-object v3, v4

    .end local v4    # "eVpn":Lcom/sec/enterprise/knox/GenericVpnPolicy;
    .restart local v3    # "eVpn":Lcom/sec/enterprise/knox/GenericVpnPolicy;
    goto :goto_3
.end method

.method private static getKnoxVpnPolicyService()Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;
    .locals 3

    .prologue
    .line 339
    sget-object v0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mKnoxVpnPolicyService:Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;

    if-nez v0, :cond_0

    .line 340
    const-string v0, "knox_vpn_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mKnoxVpnPolicyService:Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;

    .line 343
    :cond_0
    sget-boolean v0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->DBG:Z

    if-eqz v0, :cond_1

    sget-object v0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "KnoxVpnPolicy getService : mKnoxVpnPolicyService = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mKnoxVpnPolicyService:Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 344
    :cond_1
    sget-object v0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mKnoxVpnPolicyService:Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;

    return-object v0
.end method

.method public static getTransformedVendorName(Ljava/lang/String;I)Ljava/lang/String;
    .locals 3
    .param p0, "vendorName"    # Ljava/lang/String;
    .param p1, "personaId"    # I

    .prologue
    .line 2088
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 2089
    .local v0, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getVendorNameFromTransformedName(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "vendorNameWithCid"    # Ljava/lang/String;

    .prologue
    .line 2101
    const-string v1, "_"

    invoke-virtual {p0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 2102
    .local v0, "index":I
    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public activateVpnProfile(Ljava/lang/String;Z)I
    .locals 6
    .param p1, "profileName"    # Ljava/lang/String;
    .param p2, "enable"    # Z

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1141
    const/4 v0, -0x1

    .line 1143
    .local v0, "activateValue":I
    const/4 v2, 0x0

    .line 1144
    .local v2, "serviceExist":Z
    :try_start_0
    sget-boolean v5, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V1_ENABLED:Z

    if-eqz v5, :cond_3

    .line 1145
    invoke-static {}, Lcom/sec/enterprise/knox/GenericVpnPolicy;->getEnterpriseVpnPolicyService()Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;

    move-result-object v5

    if-eqz v5, :cond_2

    move v2, v3

    .line 1150
    :cond_0
    :goto_0
    if-eqz v2, :cond_7

    .line 1151
    new-instance v3, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v4

    invoke-direct {v3, v4}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const-string v4, "GenericVpnPolicy.activateVpnProfile"

    invoke-static {v3, v4}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1152
    sget-boolean v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V1_ENABLED:Z

    if-eqz v3, :cond_5

    .line 1153
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseVpnPolicyService:Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;

    iget-object v4, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->vendorName:Ljava/lang/String;

    invoke-interface {v3, v4, p1, p2}, Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;->activateVpnProfile(Ljava/lang/String;Ljava/lang/String;Z)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    .line 1158
    :cond_1
    :goto_1
    iget-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    if-eqz v3, :cond_6

    .line 1159
    iget-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v3}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1172
    :goto_2
    return v0

    :cond_2
    move v2, v4

    .line 1145
    goto :goto_0

    .line 1146
    :cond_3
    sget-boolean v5, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V2_ENABLED:Z

    if-eqz v5, :cond_0

    .line 1147
    invoke-static {}, Lcom/sec/enterprise/knox/GenericVpnPolicy;->getKnoxVpnPolicyService()Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;

    move-result-object v5

    if-eqz v5, :cond_4

    move v2, v3

    :goto_3
    goto :goto_0

    :cond_4
    move v2, v4

    goto :goto_3

    .line 1155
    :cond_5
    sget-boolean v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V2_ENABLED:Z

    if-eqz v3, :cond_1

    .line 1156
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mKnoxVpnPolicyService:Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;

    iget-object v4, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->vpnContext:Lcom/sec/enterprise/knox/KnoxVpnContext;

    invoke-interface {v3, v4, p1, p2}, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;->activateVpnProfile(Lcom/sec/enterprise/knox/KnoxVpnContext;Ljava/lang/String;Z)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1167
    :catch_0
    move-exception v1

    .line 1168
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed at GenericVpnPolicy API activateVpnProfile-Exception"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 1161
    .end local v1    # "e":Landroid/os/RemoteException;
    :cond_6
    :try_start_1
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    const-string v4, "activateVpnProfile >> mEnterpriseResponseData == null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 1164
    :cond_7
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    const-string v4, "activateVpnProfile >> mService == null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method

.method public addAllContainerPackagesToVpn(ILjava/lang/String;)I
    .locals 6
    .param p1, "mContainerId"    # I
    .param p2, "profileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1847
    const/4 v1, -0x1

    .line 1849
    .local v1, "returnValue":I
    const/4 v2, 0x0

    .line 1850
    .local v2, "serviceExist":Z
    :try_start_0
    sget-boolean v5, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V1_ENABLED:Z

    if-eqz v5, :cond_3

    .line 1851
    invoke-static {}, Lcom/sec/enterprise/knox/GenericVpnPolicy;->getEnterpriseVpnPolicyService()Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;

    move-result-object v5

    if-eqz v5, :cond_2

    move v2, v3

    .line 1856
    :cond_0
    :goto_0
    if-eqz v2, :cond_8

    .line 1857
    new-instance v3, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v4

    invoke-direct {v3, v4}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const-string v4, "GenericVpnPolicy.addAllContainerPackagesToVpn"

    invoke-static {v3, v4}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1858
    sget-boolean v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V1_ENABLED:Z

    if-eqz v3, :cond_5

    .line 1859
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseVpnPolicyService:Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;

    iget-object v4, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->vendorName:Ljava/lang/String;

    invoke-interface {v3, v4, p1, p2}, Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;->addAllContainerPackagesToVpn(Ljava/lang/String;ILjava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    .line 1864
    :cond_1
    :goto_1
    iget-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    if-eqz v3, :cond_7

    .line 1865
    iget-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v3}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v3

    const/16 v4, 0xb

    if-ne v3, v4, :cond_6

    .line 1866
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    const-string v4, "The container id entered is invalid and throwing an exception"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1867
    new-instance v3, Ljava/lang/IllegalArgumentException;

    invoke-direct {v3}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v3
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1876
    :catch_0
    move-exception v0

    .line 1877
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed at GenericVpnPolicy API addAllContainerPackagesToVpn-Exception"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1881
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_2
    return v1

    :cond_2
    move v2, v4

    .line 1851
    goto :goto_0

    .line 1852
    :cond_3
    :try_start_1
    sget-boolean v5, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V2_ENABLED:Z

    if-eqz v5, :cond_0

    .line 1853
    invoke-static {}, Lcom/sec/enterprise/knox/GenericVpnPolicy;->getKnoxVpnPolicyService()Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;

    move-result-object v5

    if-eqz v5, :cond_4

    move v2, v3

    :goto_3
    goto :goto_0

    :cond_4
    move v2, v4

    goto :goto_3

    .line 1861
    :cond_5
    sget-boolean v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V2_ENABLED:Z

    if-eqz v3, :cond_1

    .line 1862
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mKnoxVpnPolicyService:Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;

    iget-object v4, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->vpnContext:Lcom/sec/enterprise/knox/KnoxVpnContext;

    invoke-interface {v3, v4, p1, p2}, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;->addAllContainerPackagesToVpn(Lcom/sec/enterprise/knox/KnoxVpnContext;ILjava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    goto :goto_1

    .line 1869
    :cond_6
    iget-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v3}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_2

    .line 1871
    :cond_7
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    const-string v4, "addAllContainerPackagesToVpn > mEnterpriseResponseData == null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 1874
    :cond_8
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    const-string v4, "addAllContainerPackagesToVpn > mService == null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method

.method public addAllPackagesToVpn(Ljava/lang/String;)I
    .locals 6
    .param p1, "profileName"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1996
    const/4 v1, -0x1

    .line 1998
    .local v1, "returnValue":I
    const/4 v2, 0x0

    .line 1999
    .local v2, "serviceExist":Z
    :try_start_0
    sget-boolean v5, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V1_ENABLED:Z

    if-eqz v5, :cond_2

    .line 2000
    invoke-static {}, Lcom/sec/enterprise/knox/GenericVpnPolicy;->getEnterpriseVpnPolicyService()Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;

    move-result-object v5

    if-eqz v5, :cond_1

    move v2, v3

    .line 2005
    :cond_0
    :goto_0
    if-eqz v2, :cond_5

    .line 2006
    new-instance v3, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v4

    invoke-direct {v3, v4}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const-string v4, "GenericVpnPolicy.addAllPackagesToVpn"

    invoke-static {v3, v4}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2007
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mKnoxVpnPolicyService:Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;

    iget-object v4, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->vpnContext:Lcom/sec/enterprise/knox/KnoxVpnContext;

    invoke-interface {v3, v4, p1}, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;->addAllPackagesToVpn(Lcom/sec/enterprise/knox/KnoxVpnContext;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    .line 2008
    iget-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    if-eqz v3, :cond_4

    .line 2009
    iget-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v3}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 2021
    :goto_1
    return v1

    :cond_1
    move v2, v4

    .line 2000
    goto :goto_0

    .line 2001
    :cond_2
    sget-boolean v5, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V2_ENABLED:Z

    if-eqz v5, :cond_0

    .line 2002
    invoke-static {}, Lcom/sec/enterprise/knox/GenericVpnPolicy;->getKnoxVpnPolicyService()Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;

    move-result-object v5

    if-eqz v5, :cond_3

    move v2, v3

    :goto_2
    goto :goto_0

    :cond_3
    move v2, v4

    goto :goto_2

    .line 2011
    :cond_4
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    const-string v4, "addAllPackagesToVpn > mEnterpriseResponseData == null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 2016
    :catch_0
    move-exception v0

    .line 2017
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception at GenericVpnPolicy API addAllPackagesToVpn:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 2014
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_5
    :try_start_1
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    const-string v4, "addAllPackagesToVpn > mService == null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method public addContainerPackagesToVpn(I[Ljava/lang/String;Ljava/lang/String;)I
    .locals 6
    .param p1, "mContainerId"    # I
    .param p2, "packageList"    # [Ljava/lang/String;
    .param p3, "profileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1608
    const/4 v1, -0x1

    .line 1610
    .local v1, "returnValue":I
    const/4 v2, 0x0

    .line 1611
    .local v2, "serviceExist":Z
    :try_start_0
    sget-boolean v5, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V1_ENABLED:Z

    if-eqz v5, :cond_3

    .line 1612
    invoke-static {}, Lcom/sec/enterprise/knox/GenericVpnPolicy;->getEnterpriseVpnPolicyService()Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;

    move-result-object v5

    if-eqz v5, :cond_2

    move v2, v3

    .line 1617
    :cond_0
    :goto_0
    if-eqz v2, :cond_8

    .line 1618
    new-instance v3, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v4

    invoke-direct {v3, v4}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const-string v4, "GenericVpnPolicy.addContainerPackagesToVpn"

    invoke-static {v3, v4}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1619
    sget-boolean v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V1_ENABLED:Z

    if-eqz v3, :cond_5

    .line 1620
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseVpnPolicyService:Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;

    iget-object v4, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->vendorName:Ljava/lang/String;

    invoke-interface {v3, v4, p1, p2, p3}, Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;->addPackagesToVpn(Ljava/lang/String;I[Ljava/lang/String;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    .line 1625
    :cond_1
    :goto_1
    iget-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    if-eqz v3, :cond_7

    .line 1626
    iget-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v3}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v3

    const/16 v4, 0xb

    if-ne v3, v4, :cond_6

    .line 1627
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    const-string v4, "The container id entered is invalid and throwing an exception"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1628
    new-instance v3, Ljava/lang/IllegalArgumentException;

    invoke-direct {v3}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v3
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1637
    :catch_0
    move-exception v0

    .line 1638
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed at GenericVpnPolicy API addContainerPackageToVpn-Exception"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1642
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_2
    return v1

    :cond_2
    move v2, v4

    .line 1612
    goto :goto_0

    .line 1613
    :cond_3
    :try_start_1
    sget-boolean v5, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V2_ENABLED:Z

    if-eqz v5, :cond_0

    .line 1614
    invoke-static {}, Lcom/sec/enterprise/knox/GenericVpnPolicy;->getKnoxVpnPolicyService()Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;

    move-result-object v5

    if-eqz v5, :cond_4

    move v2, v3

    :goto_3
    goto :goto_0

    :cond_4
    move v2, v4

    goto :goto_3

    .line 1622
    :cond_5
    sget-boolean v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V2_ENABLED:Z

    if-eqz v3, :cond_1

    .line 1623
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mKnoxVpnPolicyService:Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;

    iget-object v4, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->vpnContext:Lcom/sec/enterprise/knox/KnoxVpnContext;

    invoke-interface {v3, v4, p1, p2, p3}, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;->addContainerPackagesToVpn(Lcom/sec/enterprise/knox/KnoxVpnContext;I[Ljava/lang/String;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    goto :goto_1

    .line 1630
    :cond_6
    iget-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v3}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_2

    .line 1632
    :cond_7
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    const-string v4, "addContainerPackageToVpn > mEnterpriseResponseData == null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 1635
    :cond_8
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    const-string v4, "addContainerPackageToVpn > mService == null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method

.method public addPackagesToVpn([Ljava/lang/String;Ljava/lang/String;)I
    .locals 6
    .param p1, "packageList"    # [Ljava/lang/String;
    .param p2, "profileName"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 998
    const/4 v1, -0x1

    .line 1000
    .local v1, "returnValue":I
    const/4 v2, 0x0

    .line 1001
    .local v2, "serviceExist":Z
    :try_start_0
    sget-boolean v5, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V1_ENABLED:Z

    if-eqz v5, :cond_3

    .line 1002
    invoke-static {}, Lcom/sec/enterprise/knox/GenericVpnPolicy;->getEnterpriseVpnPolicyService()Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;

    move-result-object v5

    if-eqz v5, :cond_2

    move v2, v3

    .line 1007
    :cond_0
    :goto_0
    if-eqz v2, :cond_7

    .line 1008
    new-instance v3, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v4

    invoke-direct {v3, v4}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const-string v4, "GenericVpnPolicy.addPackagesToVpn"

    invoke-static {v3, v4}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1009
    sget-boolean v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V1_ENABLED:Z

    if-eqz v3, :cond_5

    .line 1010
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseVpnPolicyService:Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;

    iget-object v4, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->vendorName:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5, p1, p2}, Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;->addPackagesToVpn(Ljava/lang/String;I[Ljava/lang/String;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    .line 1015
    :cond_1
    :goto_1
    iget-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    if-eqz v3, :cond_6

    .line 1016
    iget-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v3}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 1028
    :goto_2
    return v1

    :cond_2
    move v2, v4

    .line 1002
    goto :goto_0

    .line 1003
    :cond_3
    sget-boolean v5, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V2_ENABLED:Z

    if-eqz v5, :cond_0

    .line 1004
    invoke-static {}, Lcom/sec/enterprise/knox/GenericVpnPolicy;->getKnoxVpnPolicyService()Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;

    move-result-object v5

    if-eqz v5, :cond_4

    move v2, v3

    :goto_3
    goto :goto_0

    :cond_4
    move v2, v4

    goto :goto_3

    .line 1012
    :cond_5
    sget-boolean v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V2_ENABLED:Z

    if-eqz v3, :cond_1

    .line 1013
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mKnoxVpnPolicyService:Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;

    iget-object v4, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->vpnContext:Lcom/sec/enterprise/knox/KnoxVpnContext;

    invoke-interface {v3, v4, p1, p2}, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;->addPackagesToVpn(Lcom/sec/enterprise/knox/KnoxVpnContext;[Ljava/lang/String;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1023
    :catch_0
    move-exception v0

    .line 1024
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed at GenericVpnPolicy API addPackagetoDatabase-Exception"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 1018
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_6
    :try_start_1
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    const-string v4, "addPackageToVpn > mEnterpriseResponseData == null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 1021
    :cond_7
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    const-string v4, "addPackageToVpn > mService == null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method

.method public createVpnProfile(Ljava/lang/String;)I
    .locals 8
    .param p1, "profileInfo"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 454
    sget-boolean v5, Lcom/sec/enterprise/knox/GenericVpnPolicy;->DBG:Z

    if-eqz v5, :cond_0

    sget-object v5, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " UID in GenericVpnPolicy : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 455
    :cond_0
    sget v0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->VPN_RETURN_INT_ERROR:I

    .line 457
    .local v0, "createVpnProfileValue":I
    const/4 v2, 0x0

    .line 458
    .local v2, "serviceExist":Z
    :try_start_0
    sget-boolean v5, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V1_ENABLED:Z

    if-eqz v5, :cond_4

    .line 459
    invoke-static {}, Lcom/sec/enterprise/knox/GenericVpnPolicy;->getEnterpriseVpnPolicyService()Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;

    move-result-object v5

    if-eqz v5, :cond_3

    move v2, v3

    .line 464
    :cond_1
    :goto_0
    if-eqz v2, :cond_8

    .line 465
    new-instance v3, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v4

    invoke-direct {v3, v4}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const-string v4, "GenericVpnPolicy.createVpnProfile"

    invoke-static {v3, v4}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 466
    sget-boolean v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V1_ENABLED:Z

    if-eqz v3, :cond_6

    .line 467
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseVpnPolicyService:Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;

    iget-object v4, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->vendorName:Ljava/lang/String;

    invoke-interface {v3, v4, p1}, Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;->createVpnProfile(Ljava/lang/String;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    .line 472
    :cond_2
    :goto_1
    iget-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    if-eqz v3, :cond_7

    .line 473
    iget-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v3}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 483
    :goto_2
    return v0

    :cond_3
    move v2, v4

    .line 459
    goto :goto_0

    .line 460
    :cond_4
    sget-boolean v5, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V2_ENABLED:Z

    if-eqz v5, :cond_1

    .line 461
    invoke-static {}, Lcom/sec/enterprise/knox/GenericVpnPolicy;->getKnoxVpnPolicyService()Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;

    move-result-object v5

    if-eqz v5, :cond_5

    move v2, v3

    :goto_3
    goto :goto_0

    :cond_5
    move v2, v4

    goto :goto_3

    .line 469
    :cond_6
    sget-boolean v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V2_ENABLED:Z

    if-eqz v3, :cond_2

    .line 470
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mKnoxVpnPolicyService:Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;

    iget-object v4, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->vpnContext:Lcom/sec/enterprise/knox/KnoxVpnContext;

    invoke-interface {v3, v4, p1}, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;->createVpnProfile(Lcom/sec/enterprise/knox/KnoxVpnContext;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 480
    :catch_0
    move-exception v1

    .line 481
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed at GenericVpnPolicy API createVpnProfile-Exception"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 475
    .end local v1    # "e":Landroid/os/RemoteException;
    :cond_7
    :try_start_1
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    const-string v4, "createVpnProfile Error> mEnterpriseResponseData == null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 478
    :cond_8
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    const-string v4, "createVpnProfile Error > mService == null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method

.method public getAllContainerPackagesInVpnProfile(ILjava/lang/String;)[Ljava/lang/String;
    .locals 5
    .param p1, "mContainerId"    # I
    .param p2, "profileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1763
    const/4 v1, 0x0

    .line 1764
    .local v1, "serviceExist":Z
    :try_start_0
    sget-boolean v4, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V1_ENABLED:Z

    if-eqz v4, :cond_4

    .line 1765
    invoke-static {}, Lcom/sec/enterprise/knox/GenericVpnPolicy;->getEnterpriseVpnPolicyService()Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;

    move-result-object v4

    if-eqz v4, :cond_3

    move v1, v2

    .line 1770
    :cond_0
    :goto_0
    if-eqz v1, :cond_9

    .line 1771
    new-instance v2, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v3

    invoke-direct {v2, v3}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const-string v3, "GenericVpnPolicy.getAllContainerPackagesInVpnProfile"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1772
    sget-boolean v2, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V1_ENABLED:Z

    if-eqz v2, :cond_6

    .line 1773
    sget-object v2, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseVpnPolicyService:Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;

    iget-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->vendorName:Ljava/lang/String;

    invoke-interface {v2, v3, p1, p2}, Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;->getAllPackagesInVpnProfile(Ljava/lang/String;ILjava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    .line 1778
    :cond_1
    :goto_1
    iget-object v2, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    if-eqz v2, :cond_8

    .line 1779
    iget-object v2, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v2}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v2

    const/16 v3, 0xb

    if-ne v2, v3, :cond_7

    .line 1780
    sget-object v2, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    const-string v3, "The container id entered is invalid and throwing an exception"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1781
    new-instance v2, Ljava/lang/IllegalArgumentException;

    invoke-direct {v2}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v2
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1791
    :catch_0
    move-exception v0

    .line 1792
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    const-string v3, "Failed at EnterpriseContainerManager API getAllContainerPackagesInVpnProfile "

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1794
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_2
    :goto_2
    const/4 v2, 0x0

    :goto_3
    return-object v2

    :cond_3
    move v1, v3

    .line 1765
    goto :goto_0

    .line 1766
    :cond_4
    :try_start_1
    sget-boolean v4, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V2_ENABLED:Z

    if-eqz v4, :cond_0

    .line 1767
    invoke-static {}, Lcom/sec/enterprise/knox/GenericVpnPolicy;->getKnoxVpnPolicyService()Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;

    move-result-object v4

    if-eqz v4, :cond_5

    move v1, v2

    :goto_4
    goto :goto_0

    :cond_5
    move v1, v3

    goto :goto_4

    .line 1775
    :cond_6
    sget-boolean v2, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V2_ENABLED:Z

    if-eqz v2, :cond_1

    .line 1776
    sget-object v2, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mKnoxVpnPolicyService:Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;

    iget-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->vpnContext:Lcom/sec/enterprise/knox/KnoxVpnContext;

    invoke-interface {v2, v3, p1, p2}, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;->getAllContainerPackagesInVpnProfile(Lcom/sec/enterprise/knox/KnoxVpnContext;ILjava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    goto :goto_1

    .line 1783
    :cond_7
    iget-object v2, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v2}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v2

    if-nez v2, :cond_2

    .line 1784
    iget-object v2, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v2}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    check-cast v2, [Ljava/lang/String;

    goto :goto_3

    .line 1786
    :cond_8
    sget-object v2, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    const-string v3, "getAllContainerPackagesInVpnProfile > mEnterpriseResponseData == null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 1789
    :cond_9
    sget-object v2, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    const-string v3, "getAllContainerPackagesInVpnProfile > mService == null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method

.method public getAllPackagesInVpnProfile(Ljava/lang/String;)[Ljava/lang/String;
    .locals 5
    .param p1, "profileName"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1203
    const/4 v1, 0x0

    .line 1204
    .local v1, "serviceExist":Z
    :try_start_0
    sget-boolean v4, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V1_ENABLED:Z

    if-eqz v4, :cond_3

    .line 1205
    invoke-static {}, Lcom/sec/enterprise/knox/GenericVpnPolicy;->getEnterpriseVpnPolicyService()Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;

    move-result-object v4

    if-eqz v4, :cond_2

    move v1, v2

    .line 1210
    :cond_0
    :goto_0
    if-eqz v1, :cond_8

    .line 1211
    new-instance v2, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v3

    invoke-direct {v2, v3}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const-string v3, "GenericVpnPolicy.getAllPackagesInVpnProfile"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1212
    sget-boolean v2, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V1_ENABLED:Z

    if-eqz v2, :cond_5

    .line 1213
    sget-object v2, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseVpnPolicyService:Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;

    iget-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->vendorName:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4, p1}, Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;->getAllPackagesInVpnProfile(Ljava/lang/String;ILjava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    .line 1218
    :cond_1
    :goto_1
    iget-object v2, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    if-eqz v2, :cond_7

    .line 1219
    iget-object v2, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v2}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v2

    if-nez v2, :cond_6

    .line 1220
    iget-object v2, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v2}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    check-cast v2, [Ljava/lang/String;

    .line 1230
    :goto_2
    return-object v2

    :cond_2
    move v1, v3

    .line 1205
    goto :goto_0

    .line 1206
    :cond_3
    sget-boolean v4, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V2_ENABLED:Z

    if-eqz v4, :cond_0

    .line 1207
    invoke-static {}, Lcom/sec/enterprise/knox/GenericVpnPolicy;->getKnoxVpnPolicyService()Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;

    move-result-object v4

    if-eqz v4, :cond_4

    move v1, v2

    :goto_3
    goto :goto_0

    :cond_4
    move v1, v3

    goto :goto_3

    .line 1215
    :cond_5
    sget-boolean v2, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V2_ENABLED:Z

    if-eqz v2, :cond_1

    .line 1216
    sget-object v2, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mKnoxVpnPolicyService:Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;

    iget-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->vpnContext:Lcom/sec/enterprise/knox/KnoxVpnContext;

    invoke-interface {v2, v3, p1}, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;->getAllPackagesInVpnProfile(Lcom/sec/enterprise/knox/KnoxVpnContext;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1227
    :catch_0
    move-exception v0

    .line 1228
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    const-string v3, "Failed at EnterpriseContainerManager API getAllPackagesInVpnProfile "

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1230
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_6
    :goto_4
    const/4 v2, 0x0

    goto :goto_2

    .line 1222
    :cond_7
    :try_start_1
    sget-object v2, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    const-string v3, "getAllPackagesInVpnProfile > mEnterpriseResponseData == null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 1225
    :cond_8
    sget-object v2, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    const-string v3, "getAllPackagesInVpnProfile > mService == null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_4
.end method

.method public getAllVpnProfiles()Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 1263
    const/4 v1, 0x0

    .line 1265
    .local v1, "connectionList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v3, 0x0

    .line 1266
    .local v3, "serviceExist":Z
    :try_start_0
    sget-boolean v6, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V1_ENABLED:Z

    if-eqz v6, :cond_4

    .line 1267
    invoke-static {}, Lcom/sec/enterprise/knox/GenericVpnPolicy;->getEnterpriseVpnPolicyService()Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;

    move-result-object v6

    if-eqz v6, :cond_3

    move v3, v4

    .line 1272
    :cond_0
    :goto_0
    if-eqz v3, :cond_8

    .line 1273
    new-instance v4, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v5

    invoke-direct {v4, v5}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const-string v5, "GenericVpnPolicy.getAllVpnProfiles"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1274
    sget-boolean v4, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V1_ENABLED:Z

    if-eqz v4, :cond_6

    .line 1275
    sget-object v4, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseVpnPolicyService:Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;

    iget-object v5, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->vendorName:Ljava/lang/String;

    invoke-interface {v4, v5}, Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;->getAllVpnProfiles(Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    .line 1280
    :cond_1
    :goto_1
    iget-object v4, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    if-eqz v4, :cond_7

    .line 1281
    iget-object v4, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v4}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v4

    if-nez v4, :cond_2

    .line 1282
    iget-object v4, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v4}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Ljava/util/List;

    move-object v1, v0

    .line 1293
    :cond_2
    :goto_2
    return-object v1

    :cond_3
    move v3, v5

    .line 1267
    goto :goto_0

    .line 1268
    :cond_4
    sget-boolean v6, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V2_ENABLED:Z

    if-eqz v6, :cond_0

    .line 1269
    invoke-static {}, Lcom/sec/enterprise/knox/GenericVpnPolicy;->getKnoxVpnPolicyService()Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;

    move-result-object v6

    if-eqz v6, :cond_5

    move v3, v4

    :goto_3
    goto :goto_0

    :cond_5
    move v3, v5

    goto :goto_3

    .line 1277
    :cond_6
    sget-boolean v4, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V2_ENABLED:Z

    if-eqz v4, :cond_1

    .line 1278
    sget-object v4, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mKnoxVpnPolicyService:Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;

    iget-object v5, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->vpnContext:Lcom/sec/enterprise/knox/KnoxVpnContext;

    invoke-interface {v4, v5}, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;->getAllVpnProfiles(Lcom/sec/enterprise/knox/KnoxVpnContext;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1289
    :catch_0
    move-exception v2

    .line 1290
    .local v2, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed at GenericVpnPolicy API getAllVpnProfiles-Exception"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 1284
    .end local v2    # "e":Landroid/os/RemoteException;
    :cond_7
    :try_start_1
    sget-object v4, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    const-string v5, "getAllVpnProfiles > mEnterpriseResponseData == null"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 1287
    :cond_8
    sget-object v4, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    const-string v5, "getAllVpnProfiles > mService == null"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method

.method public getCACertificate(Ljava/lang/String;)Landroid/app/enterprise/CertificateInfo;
    .locals 7
    .param p1, "profileName"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 790
    const/4 v2, 0x0

    .line 792
    .local v2, "getCACertificateValue":Landroid/app/enterprise/CertificateInfo;
    const/4 v3, 0x0

    .line 793
    .local v3, "serviceExist":Z
    :try_start_0
    sget-boolean v6, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V1_ENABLED:Z

    if-eqz v6, :cond_4

    .line 794
    invoke-static {}, Lcom/sec/enterprise/knox/GenericVpnPolicy;->getEnterpriseVpnPolicyService()Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;

    move-result-object v6

    if-eqz v6, :cond_3

    move v3, v4

    .line 799
    :cond_0
    :goto_0
    if-eqz v3, :cond_8

    .line 800
    new-instance v4, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v5

    invoke-direct {v4, v5}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const-string v5, "GenericVpnPolicy.getCACertificate"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 801
    sget-boolean v4, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V1_ENABLED:Z

    if-eqz v4, :cond_6

    .line 802
    sget-object v4, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseVpnPolicyService:Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;

    iget-object v5, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->vendorName:Ljava/lang/String;

    invoke-interface {v4, v5, p1}, Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;->getCACertificate(Ljava/lang/String;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    .line 807
    :cond_1
    :goto_1
    iget-object v4, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    if-eqz v4, :cond_7

    .line 808
    iget-object v4, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v4}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v4

    if-nez v4, :cond_2

    .line 809
    iget-object v4, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v4}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Landroid/app/enterprise/CertificateInfo;

    move-object v2, v0

    .line 821
    :cond_2
    :goto_2
    return-object v2

    :cond_3
    move v3, v5

    .line 794
    goto :goto_0

    .line 795
    :cond_4
    sget-boolean v6, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V2_ENABLED:Z

    if-eqz v6, :cond_0

    .line 796
    invoke-static {}, Lcom/sec/enterprise/knox/GenericVpnPolicy;->getKnoxVpnPolicyService()Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;

    move-result-object v6

    if-eqz v6, :cond_5

    move v3, v4

    :goto_3
    goto :goto_0

    :cond_5
    move v3, v5

    goto :goto_3

    .line 804
    :cond_6
    sget-boolean v4, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V2_ENABLED:Z

    if-eqz v4, :cond_1

    .line 805
    sget-object v4, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mKnoxVpnPolicyService:Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;

    iget-object v5, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->vpnContext:Lcom/sec/enterprise/knox/KnoxVpnContext;

    invoke-interface {v4, v5, p1}, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;->getCACertificate(Lcom/sec/enterprise/knox/KnoxVpnContext;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 816
    :catch_0
    move-exception v1

    .line 817
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed at GenericVpnPolicy API getCACertificate-Exception"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 811
    .end local v1    # "e":Landroid/os/RemoteException;
    :cond_7
    :try_start_1
    sget-object v4, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    const-string v5, "getCACertificate > mEnterpriseResponseData == null"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 814
    :cond_8
    sget-object v4, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    const-string v5, "getCACertificate > mService == null"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method

.method public getErrorString(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "profileName"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 927
    const/4 v2, 0x0

    .line 929
    .local v2, "getErrorValue":Ljava/lang/String;
    const/4 v3, 0x0

    .line 930
    .local v3, "serviceExist":Z
    :try_start_0
    sget-boolean v6, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V1_ENABLED:Z

    if-eqz v6, :cond_4

    .line 931
    invoke-static {}, Lcom/sec/enterprise/knox/GenericVpnPolicy;->getEnterpriseVpnPolicyService()Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;

    move-result-object v6

    if-eqz v6, :cond_3

    move v3, v4

    .line 936
    :cond_0
    :goto_0
    if-eqz v3, :cond_8

    .line 937
    new-instance v4, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v5

    invoke-direct {v4, v5}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const-string v5, "GenericVpnPolicy.getErrorString"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 938
    sget-boolean v4, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V1_ENABLED:Z

    if-eqz v4, :cond_6

    .line 939
    sget-object v4, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseVpnPolicyService:Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;

    iget-object v5, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->vendorName:Ljava/lang/String;

    invoke-interface {v4, v5, p1}, Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;->getErrorString(Ljava/lang/String;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    .line 944
    :cond_1
    :goto_1
    iget-object v4, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    if-eqz v4, :cond_7

    .line 945
    iget-object v4, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v4}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v4

    if-nez v4, :cond_2

    .line 946
    iget-object v4, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v4}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Ljava/lang/String;

    move-object v2, v0

    .line 959
    :cond_2
    :goto_2
    return-object v2

    :cond_3
    move v3, v5

    .line 931
    goto :goto_0

    .line 932
    :cond_4
    sget-boolean v6, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V2_ENABLED:Z

    if-eqz v6, :cond_0

    .line 933
    invoke-static {}, Lcom/sec/enterprise/knox/GenericVpnPolicy;->getKnoxVpnPolicyService()Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;

    move-result-object v6

    if-eqz v6, :cond_5

    move v3, v4

    :goto_3
    goto :goto_0

    :cond_5
    move v3, v5

    goto :goto_3

    .line 941
    :cond_6
    sget-boolean v4, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V2_ENABLED:Z

    if-eqz v4, :cond_1

    .line 942
    sget-object v4, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mKnoxVpnPolicyService:Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;

    iget-object v5, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->vpnContext:Lcom/sec/enterprise/knox/KnoxVpnContext;

    invoke-interface {v4, v5, p1}, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;->getErrorString(Lcom/sec/enterprise/knox/KnoxVpnContext;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 954
    :catch_0
    move-exception v1

    .line 955
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed at GenericVpnPolicy API getErrorString-Exception"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 948
    .end local v1    # "e":Landroid/os/RemoteException;
    :cond_7
    :try_start_1
    sget-object v4, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    const-string v5, "getErrorString > mEnterpriseResponseData == null"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 951
    :cond_8
    sget-object v4, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    const-string v5, "getErrorString > mService == null"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method

.method public getState(Ljava/lang/String;)I
    .locals 6
    .param p1, "profileName"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 864
    const/4 v1, -0x1

    .line 866
    .local v1, "getStateValue":I
    const/4 v2, 0x0

    .line 867
    .local v2, "serviceExist":Z
    :try_start_0
    sget-boolean v5, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V1_ENABLED:Z

    if-eqz v5, :cond_3

    .line 868
    invoke-static {}, Lcom/sec/enterprise/knox/GenericVpnPolicy;->getEnterpriseVpnPolicyService()Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;

    move-result-object v5

    if-eqz v5, :cond_2

    move v2, v3

    .line 873
    :cond_0
    :goto_0
    if-eqz v2, :cond_7

    .line 874
    new-instance v3, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v4

    invoke-direct {v3, v4}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const-string v4, "GenericVpnPolicy.getState"

    invoke-static {v3, v4}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 875
    sget-boolean v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V1_ENABLED:Z

    if-eqz v3, :cond_5

    .line 876
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseVpnPolicyService:Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;

    iget-object v4, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->vendorName:Ljava/lang/String;

    invoke-interface {v3, v4, p1}, Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;->getState(Ljava/lang/String;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    .line 881
    :cond_1
    :goto_1
    iget-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    if-eqz v3, :cond_6

    .line 882
    iget-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v3}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 895
    :goto_2
    return v1

    :cond_2
    move v2, v4

    .line 868
    goto :goto_0

    .line 869
    :cond_3
    sget-boolean v5, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V2_ENABLED:Z

    if-eqz v5, :cond_0

    .line 870
    invoke-static {}, Lcom/sec/enterprise/knox/GenericVpnPolicy;->getKnoxVpnPolicyService()Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;

    move-result-object v5

    if-eqz v5, :cond_4

    move v2, v3

    :goto_3
    goto :goto_0

    :cond_4
    move v2, v4

    goto :goto_3

    .line 878
    :cond_5
    sget-boolean v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V2_ENABLED:Z

    if-eqz v3, :cond_1

    .line 879
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mKnoxVpnPolicyService:Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;

    iget-object v4, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->vpnContext:Lcom/sec/enterprise/knox/KnoxVpnContext;

    invoke-interface {v3, v4, p1}, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;->getState(Lcom/sec/enterprise/knox/KnoxVpnContext;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 890
    :catch_0
    move-exception v0

    .line 891
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed at GenericVpnPolicy API getState-Exception"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 884
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_6
    :try_start_1
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    const-string v4, "getState >> mEnterpriseResponseData == null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 887
    :cond_7
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    const-string v4, "getState >> mService == null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method

.method public getUserCertificate(Ljava/lang/String;)Landroid/app/enterprise/CertificateInfo;
    .locals 7
    .param p1, "profileName"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 653
    const/4 v2, 0x0

    .line 655
    .local v2, "getUserCertificateValue":Landroid/app/enterprise/CertificateInfo;
    const/4 v3, 0x0

    .line 656
    .local v3, "serviceExist":Z
    :try_start_0
    sget-boolean v6, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V1_ENABLED:Z

    if-eqz v6, :cond_4

    .line 657
    invoke-static {}, Lcom/sec/enterprise/knox/GenericVpnPolicy;->getEnterpriseVpnPolicyService()Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;

    move-result-object v6

    if-eqz v6, :cond_3

    move v3, v4

    .line 662
    :cond_0
    :goto_0
    if-eqz v3, :cond_8

    .line 663
    new-instance v4, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v5

    invoke-direct {v4, v5}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const-string v5, "GenericVpnPolicy.getUserCertificate"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 664
    sget-boolean v4, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V1_ENABLED:Z

    if-eqz v4, :cond_6

    .line 665
    sget-object v4, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseVpnPolicyService:Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;

    iget-object v5, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->vendorName:Ljava/lang/String;

    invoke-interface {v4, v5, p1}, Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;->getUserCertificate(Ljava/lang/String;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    .line 670
    :cond_1
    :goto_1
    iget-object v4, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    if-eqz v4, :cond_7

    .line 671
    iget-object v4, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v4}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v4

    if-nez v4, :cond_2

    .line 672
    iget-object v4, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v4}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Landroid/app/enterprise/CertificateInfo;

    move-object v2, v0

    .line 685
    :cond_2
    :goto_2
    return-object v2

    :cond_3
    move v3, v5

    .line 657
    goto :goto_0

    .line 658
    :cond_4
    sget-boolean v6, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V2_ENABLED:Z

    if-eqz v6, :cond_0

    .line 659
    invoke-static {}, Lcom/sec/enterprise/knox/GenericVpnPolicy;->getKnoxVpnPolicyService()Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;

    move-result-object v6

    if-eqz v6, :cond_5

    move v3, v4

    :goto_3
    goto :goto_0

    :cond_5
    move v3, v5

    goto :goto_3

    .line 667
    :cond_6
    sget-boolean v4, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V2_ENABLED:Z

    if-eqz v4, :cond_1

    .line 668
    sget-object v4, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mKnoxVpnPolicyService:Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;

    iget-object v5, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->vpnContext:Lcom/sec/enterprise/knox/KnoxVpnContext;

    invoke-interface {v4, v5, p1}, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;->getUserCertificate(Lcom/sec/enterprise/knox/KnoxVpnContext;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 680
    :catch_0
    move-exception v1

    .line 681
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed at GenericVpnPolicy API getUserCertificate-Exception"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 674
    .end local v1    # "e":Landroid/os/RemoteException;
    :cond_7
    :try_start_1
    sget-object v4, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    const-string v5, "getUserCertificate > mEnterpriseResponseData == null"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 677
    :cond_8
    sget-object v4, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    const-string v5, "getUserCertificate > mService == null"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method

.method public getVpnModeOfOperation(Ljava/lang/String;)I
    .locals 6
    .param p1, "profileName"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1406
    const/4 v1, -0x1

    .line 1408
    .local v1, "getVpnModeOfOperationValue":I
    const/4 v2, 0x0

    .line 1409
    .local v2, "serviceExist":Z
    :try_start_0
    sget-boolean v5, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V1_ENABLED:Z

    if-eqz v5, :cond_4

    .line 1410
    invoke-static {}, Lcom/sec/enterprise/knox/GenericVpnPolicy;->getEnterpriseVpnPolicyService()Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;

    move-result-object v5

    if-eqz v5, :cond_3

    move v2, v3

    .line 1415
    :cond_0
    :goto_0
    if-eqz v2, :cond_8

    .line 1416
    new-instance v3, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v4

    invoke-direct {v3, v4}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const-string v4, "GenericVpnPolicy.getVpnModeOfOperation"

    invoke-static {v3, v4}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1417
    sget-boolean v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V1_ENABLED:Z

    if-eqz v3, :cond_6

    .line 1418
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseVpnPolicyService:Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;

    iget-object v4, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->vendorName:Ljava/lang/String;

    invoke-interface {v3, v4, p1}, Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;->getVpnModeOfOperation(Ljava/lang/String;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    .line 1423
    :cond_1
    :goto_1
    iget-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    if-eqz v3, :cond_7

    .line 1424
    iget-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v3}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v3

    if-nez v3, :cond_2

    .line 1425
    iget-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v3}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 1437
    :cond_2
    :goto_2
    return v1

    :cond_3
    move v2, v4

    .line 1410
    goto :goto_0

    .line 1411
    :cond_4
    sget-boolean v5, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V2_ENABLED:Z

    if-eqz v5, :cond_0

    .line 1412
    invoke-static {}, Lcom/sec/enterprise/knox/GenericVpnPolicy;->getKnoxVpnPolicyService()Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;

    move-result-object v5

    if-eqz v5, :cond_5

    move v2, v3

    :goto_3
    goto :goto_0

    :cond_5
    move v2, v4

    goto :goto_3

    .line 1420
    :cond_6
    sget-boolean v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V2_ENABLED:Z

    if-eqz v3, :cond_1

    .line 1421
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mKnoxVpnPolicyService:Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;

    iget-object v4, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->vpnContext:Lcom/sec/enterprise/knox/KnoxVpnContext;

    invoke-interface {v3, v4, p1}, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;->getVpnModeOfOperation(Lcom/sec/enterprise/knox/KnoxVpnContext;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1432
    :catch_0
    move-exception v0

    .line 1433
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed at GenericVpnPolicy API getVpnModeOfOperation-Exception"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 1427
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_7
    :try_start_1
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    const-string v4, "getVpnModeOfOperation > mEnterpriseResponseData == null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 1430
    :cond_8
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    const-string v4, "getVpnModeOfOperation > mService == null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method

.method public getVpnProfile(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "profileName"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 389
    const/4 v2, 0x0

    .line 391
    .local v2, "getVpnProfileValue":Ljava/lang/String;
    const/4 v3, 0x0

    .line 392
    .local v3, "serviceExist":Z
    :try_start_0
    sget-boolean v6, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V1_ENABLED:Z

    if-eqz v6, :cond_4

    .line 393
    invoke-static {}, Lcom/sec/enterprise/knox/GenericVpnPolicy;->getEnterpriseVpnPolicyService()Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;

    move-result-object v6

    if-eqz v6, :cond_3

    move v3, v4

    .line 398
    :cond_0
    :goto_0
    if-eqz v3, :cond_8

    .line 399
    new-instance v4, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v5

    invoke-direct {v4, v5}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const-string v5, "GenericVpnPolicy.getVpnProfile"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 400
    sget-boolean v4, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V1_ENABLED:Z

    if-eqz v4, :cond_6

    .line 401
    sget-object v4, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseVpnPolicyService:Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;

    iget-object v5, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->vendorName:Ljava/lang/String;

    invoke-interface {v4, v5, p1}, Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;->getVpnProfile(Ljava/lang/String;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    .line 406
    :cond_1
    :goto_1
    iget-object v4, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    if-eqz v4, :cond_7

    .line 407
    iget-object v4, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v4}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v4

    if-nez v4, :cond_2

    .line 408
    iget-object v4, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v4}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Ljava/lang/String;

    move-object v2, v0

    .line 418
    :cond_2
    :goto_2
    return-object v2

    :cond_3
    move v3, v5

    .line 393
    goto :goto_0

    .line 394
    :cond_4
    sget-boolean v6, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V2_ENABLED:Z

    if-eqz v6, :cond_0

    .line 395
    invoke-static {}, Lcom/sec/enterprise/knox/GenericVpnPolicy;->getKnoxVpnPolicyService()Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;

    move-result-object v6

    if-eqz v6, :cond_5

    move v3, v4

    :goto_3
    goto :goto_0

    :cond_5
    move v3, v5

    goto :goto_3

    .line 403
    :cond_6
    sget-boolean v4, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V2_ENABLED:Z

    if-eqz v4, :cond_1

    .line 404
    sget-object v4, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mKnoxVpnPolicyService:Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;

    iget-object v5, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->vpnContext:Lcom/sec/enterprise/knox/KnoxVpnContext;

    invoke-interface {v4, v5, p1}, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;->getVpnProfile(Lcom/sec/enterprise/knox/KnoxVpnContext;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 415
    :catch_0
    move-exception v1

    .line 416
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed at GenericVpnPolicy API getVpnProfile-Exception"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 410
    .end local v1    # "e":Landroid/os/RemoteException;
    :cond_7
    :try_start_1
    sget-object v4, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    const-string v5, "getVpnProfile Error> mEnterpriseResponseData == null"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 413
    :cond_8
    sget-object v4, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    const-string v5, "getVpnProfile Error > mService == null"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method

.method public removeAllContainerPackagesFromVpn(ILjava/lang/String;)I
    .locals 6
    .param p1, "mContainerId"    # I
    .param p2, "profileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1927
    const/4 v1, -0x1

    .line 1929
    .local v1, "returnValue":I
    const/4 v2, 0x0

    .line 1930
    .local v2, "serviceExist":Z
    :try_start_0
    sget-boolean v5, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V1_ENABLED:Z

    if-eqz v5, :cond_3

    .line 1931
    invoke-static {}, Lcom/sec/enterprise/knox/GenericVpnPolicy;->getEnterpriseVpnPolicyService()Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;

    move-result-object v5

    if-eqz v5, :cond_2

    move v2, v3

    .line 1936
    :cond_0
    :goto_0
    if-eqz v2, :cond_8

    .line 1937
    new-instance v3, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v4

    invoke-direct {v3, v4}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const-string v4, "GenericVpnPolicy.removeAllContainerPackagesFromVpn"

    invoke-static {v3, v4}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1938
    sget-boolean v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V1_ENABLED:Z

    if-eqz v3, :cond_5

    .line 1939
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseVpnPolicyService:Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;

    iget-object v4, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->vendorName:Ljava/lang/String;

    invoke-interface {v3, v4, p1, p2}, Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;->removeAllContainerPackagesFromVpn(Ljava/lang/String;ILjava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    .line 1944
    :cond_1
    :goto_1
    iget-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    if-eqz v3, :cond_7

    .line 1945
    iget-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v3}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v3

    const/16 v4, 0xb

    if-ne v3, v4, :cond_6

    .line 1946
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    const-string v4, "The container id entered is invalid and throwing an exception"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1947
    new-instance v3, Ljava/lang/IllegalArgumentException;

    invoke-direct {v3}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v3
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1956
    :catch_0
    move-exception v0

    .line 1957
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception at GenericVpnPolicy API removeAllContainerPackagesFromVpn:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1961
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_2
    return v1

    :cond_2
    move v2, v4

    .line 1931
    goto :goto_0

    .line 1932
    :cond_3
    :try_start_1
    sget-boolean v5, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V2_ENABLED:Z

    if-eqz v5, :cond_0

    .line 1933
    invoke-static {}, Lcom/sec/enterprise/knox/GenericVpnPolicy;->getKnoxVpnPolicyService()Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;

    move-result-object v5

    if-eqz v5, :cond_4

    move v2, v3

    :goto_3
    goto :goto_0

    :cond_4
    move v2, v4

    goto :goto_3

    .line 1941
    :cond_5
    sget-boolean v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V2_ENABLED:Z

    if-eqz v3, :cond_1

    .line 1942
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mKnoxVpnPolicyService:Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;

    iget-object v4, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->vpnContext:Lcom/sec/enterprise/knox/KnoxVpnContext;

    invoke-interface {v3, v4, p1, p2}, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;->removeAllContainerPackagesFromVpn(Lcom/sec/enterprise/knox/KnoxVpnContext;ILjava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    goto :goto_1

    .line 1949
    :cond_6
    iget-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v3}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_2

    .line 1951
    :cond_7
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    const-string v4, "removeAllContainerPackagesFromVpn > mEnterpriseResponseData == null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 1954
    :cond_8
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    const-string v4, "removeAllContainerPackagesFromVpn > mService == null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method

.method public removeAllPackagesFromVpn(Ljava/lang/String;)I
    .locals 6
    .param p1, "profileName"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 2058
    const/4 v1, -0x1

    .line 2060
    .local v1, "returnValue":I
    const/4 v2, 0x0

    .line 2061
    .local v2, "serviceExist":Z
    :try_start_0
    sget-boolean v5, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V1_ENABLED:Z

    if-eqz v5, :cond_2

    .line 2062
    invoke-static {}, Lcom/sec/enterprise/knox/GenericVpnPolicy;->getEnterpriseVpnPolicyService()Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;

    move-result-object v5

    if-eqz v5, :cond_1

    move v2, v3

    .line 2067
    :cond_0
    :goto_0
    if-eqz v2, :cond_5

    .line 2068
    new-instance v3, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v4

    invoke-direct {v3, v4}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const-string v4, "GenericVpnPolicy.removeAllPackagesFromVpn"

    invoke-static {v3, v4}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2069
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mKnoxVpnPolicyService:Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;

    iget-object v4, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->vpnContext:Lcom/sec/enterprise/knox/KnoxVpnContext;

    invoke-interface {v3, v4, p1}, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;->removeAllPackagesFromVpn(Lcom/sec/enterprise/knox/KnoxVpnContext;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    .line 2070
    iget-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    if-eqz v3, :cond_4

    .line 2071
    iget-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v3}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 2083
    :goto_1
    return v1

    :cond_1
    move v2, v4

    .line 2062
    goto :goto_0

    .line 2063
    :cond_2
    sget-boolean v5, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V2_ENABLED:Z

    if-eqz v5, :cond_0

    .line 2064
    invoke-static {}, Lcom/sec/enterprise/knox/GenericVpnPolicy;->getKnoxVpnPolicyService()Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;

    move-result-object v5

    if-eqz v5, :cond_3

    move v2, v3

    :goto_2
    goto :goto_0

    :cond_3
    move v2, v4

    goto :goto_2

    .line 2073
    :cond_4
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    const-string v4, "removeAllPackagesFromVpn > mEnterpriseResponseData == null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 2078
    :catch_0
    move-exception v0

    .line 2079
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception at GenericVpnPolicy API removeAllPackagesFromVpn:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 2076
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_5
    :try_start_1
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    const-string v4, "removeAllPackagesFromVpn > mService == null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method public removeContainerPackagesFromVpn(I[Ljava/lang/String;Ljava/lang/String;)I
    .locals 6
    .param p1, "mContainerId"    # I
    .param p2, "packageList"    # [Ljava/lang/String;
    .param p3, "profileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1688
    const/4 v1, -0x1

    .line 1690
    .local v1, "returnValue":I
    const/4 v2, 0x0

    .line 1691
    .local v2, "serviceExist":Z
    :try_start_0
    sget-boolean v5, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V1_ENABLED:Z

    if-eqz v5, :cond_3

    .line 1692
    invoke-static {}, Lcom/sec/enterprise/knox/GenericVpnPolicy;->getEnterpriseVpnPolicyService()Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;

    move-result-object v5

    if-eqz v5, :cond_2

    move v2, v3

    .line 1697
    :cond_0
    :goto_0
    if-eqz v2, :cond_8

    .line 1698
    new-instance v3, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v4

    invoke-direct {v3, v4}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const-string v4, "GenericVpnPolicy.removeContainerPackagesFromVpn"

    invoke-static {v3, v4}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1699
    sget-boolean v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V1_ENABLED:Z

    if-eqz v3, :cond_5

    .line 1700
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseVpnPolicyService:Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;

    iget-object v4, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->vendorName:Ljava/lang/String;

    invoke-interface {v3, v4, p1, p2, p3}, Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;->removePackagesFromVpn(Ljava/lang/String;I[Ljava/lang/String;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    .line 1705
    :cond_1
    :goto_1
    iget-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    if-eqz v3, :cond_7

    .line 1706
    iget-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v3}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v3

    const/16 v4, 0xb

    if-ne v3, v4, :cond_6

    .line 1707
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    const-string v4, "The container id entered is invalid and throwing an exception"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1708
    new-instance v3, Ljava/lang/IllegalArgumentException;

    invoke-direct {v3}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v3
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1717
    :catch_0
    move-exception v0

    .line 1718
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception at GenericVpnPolicy API removeContainerPackageFromVpn:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1722
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_2
    return v1

    :cond_2
    move v2, v4

    .line 1692
    goto :goto_0

    .line 1693
    :cond_3
    :try_start_1
    sget-boolean v5, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V2_ENABLED:Z

    if-eqz v5, :cond_0

    .line 1694
    invoke-static {}, Lcom/sec/enterprise/knox/GenericVpnPolicy;->getKnoxVpnPolicyService()Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;

    move-result-object v5

    if-eqz v5, :cond_4

    move v2, v3

    :goto_3
    goto :goto_0

    :cond_4
    move v2, v4

    goto :goto_3

    .line 1702
    :cond_5
    sget-boolean v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V2_ENABLED:Z

    if-eqz v3, :cond_1

    .line 1703
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mKnoxVpnPolicyService:Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;

    iget-object v4, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->vpnContext:Lcom/sec/enterprise/knox/KnoxVpnContext;

    invoke-interface {v3, v4, p1, p2, p3}, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;->removeContainerPackagesFromVpn(Lcom/sec/enterprise/knox/KnoxVpnContext;I[Ljava/lang/String;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    goto :goto_1

    .line 1710
    :cond_6
    iget-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v3}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_2

    .line 1712
    :cond_7
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    const-string v4, "removeContainerPackageFromVpn > mEnterpriseResponseData == null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 1715
    :cond_8
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    const-string v4, "removeContainerPackageFromVpn > mService == null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method

.method public removePackagesFromVpn([Ljava/lang/String;Ljava/lang/String;)I
    .locals 6
    .param p1, "packageList"    # [Ljava/lang/String;
    .param p2, "profileName"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1065
    const/4 v1, -0x1

    .line 1067
    .local v1, "returnValue":I
    const/4 v2, 0x0

    .line 1068
    .local v2, "serviceExist":Z
    :try_start_0
    sget-boolean v5, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V1_ENABLED:Z

    if-eqz v5, :cond_3

    .line 1069
    invoke-static {}, Lcom/sec/enterprise/knox/GenericVpnPolicy;->getEnterpriseVpnPolicyService()Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;

    move-result-object v5

    if-eqz v5, :cond_2

    move v2, v3

    .line 1074
    :cond_0
    :goto_0
    if-eqz v2, :cond_7

    .line 1075
    new-instance v3, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v4

    invoke-direct {v3, v4}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const-string v4, "GenericVpnPolicy.removePackagesFromVpn"

    invoke-static {v3, v4}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1076
    sget-boolean v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V1_ENABLED:Z

    if-eqz v3, :cond_5

    .line 1077
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseVpnPolicyService:Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;

    iget-object v4, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->vendorName:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5, p1, p2}, Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;->removePackagesFromVpn(Ljava/lang/String;I[Ljava/lang/String;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    .line 1082
    :cond_1
    :goto_1
    iget-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    if-eqz v3, :cond_6

    .line 1083
    iget-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v3}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 1095
    :goto_2
    return v1

    :cond_2
    move v2, v4

    .line 1069
    goto :goto_0

    .line 1070
    :cond_3
    sget-boolean v5, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V2_ENABLED:Z

    if-eqz v5, :cond_0

    .line 1071
    invoke-static {}, Lcom/sec/enterprise/knox/GenericVpnPolicy;->getKnoxVpnPolicyService()Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;

    move-result-object v5

    if-eqz v5, :cond_4

    move v2, v3

    :goto_3
    goto :goto_0

    :cond_4
    move v2, v4

    goto :goto_3

    .line 1079
    :cond_5
    sget-boolean v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V2_ENABLED:Z

    if-eqz v3, :cond_1

    .line 1080
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mKnoxVpnPolicyService:Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;

    iget-object v4, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->vpnContext:Lcom/sec/enterprise/knox/KnoxVpnContext;

    invoke-interface {v3, v4, p1, p2}, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;->removePackagesFromVpn(Lcom/sec/enterprise/knox/KnoxVpnContext;[Ljava/lang/String;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1090
    :catch_0
    move-exception v0

    .line 1091
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception at GenericVpnPolicy API removePackageFromVpn:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 1085
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_6
    :try_start_1
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    const-string v4, "removePackageFromVpn > mEnterpriseResponseData == null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 1088
    :cond_7
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    const-string v4, "removePackageFromVpn > mService == null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method

.method public removeVpnProfile(Ljava/lang/String;)I
    .locals 6
    .param p1, "profileName"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 516
    sget v1, Lcom/sec/enterprise/knox/GenericVpnPolicy;->VPN_RETURN_INT_ERROR:I

    .line 518
    .local v1, "removeVpnProfileValue":I
    const/4 v2, 0x0

    .line 519
    .local v2, "serviceExist":Z
    :try_start_0
    sget-boolean v5, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V1_ENABLED:Z

    if-eqz v5, :cond_3

    .line 520
    invoke-static {}, Lcom/sec/enterprise/knox/GenericVpnPolicy;->getEnterpriseVpnPolicyService()Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;

    move-result-object v5

    if-eqz v5, :cond_2

    move v2, v3

    .line 525
    :cond_0
    :goto_0
    if-eqz v2, :cond_7

    .line 526
    new-instance v3, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v4

    invoke-direct {v3, v4}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const-string v4, "GenericVpnPolicy.removeVpnProfile"

    invoke-static {v3, v4}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 527
    sget-boolean v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V1_ENABLED:Z

    if-eqz v3, :cond_5

    .line 528
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseVpnPolicyService:Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;

    iget-object v4, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->vendorName:Ljava/lang/String;

    invoke-interface {v3, v4, p1}, Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;->removeVpnProfile(Ljava/lang/String;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    .line 533
    :cond_1
    :goto_1
    iget-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    if-eqz v3, :cond_6

    .line 534
    iget-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v3}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 544
    :goto_2
    return v1

    :cond_2
    move v2, v4

    .line 520
    goto :goto_0

    .line 521
    :cond_3
    sget-boolean v5, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V2_ENABLED:Z

    if-eqz v5, :cond_0

    .line 522
    invoke-static {}, Lcom/sec/enterprise/knox/GenericVpnPolicy;->getKnoxVpnPolicyService()Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;

    move-result-object v5

    if-eqz v5, :cond_4

    move v2, v3

    :goto_3
    goto :goto_0

    :cond_4
    move v2, v4

    goto :goto_3

    .line 530
    :cond_5
    sget-boolean v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V2_ENABLED:Z

    if-eqz v3, :cond_1

    .line 531
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mKnoxVpnPolicyService:Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;

    iget-object v4, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->vpnContext:Lcom/sec/enterprise/knox/KnoxVpnContext;

    invoke-interface {v3, v4, p1}, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;->removeVpnProfile(Lcom/sec/enterprise/knox/KnoxVpnContext;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 541
    :catch_0
    move-exception v0

    .line 542
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed at GenericVpnPolicy API removeVpnProfile\u00a0-Exception"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 536
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_6
    :try_start_1
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    const-string v4, "removeVpnProfile\u00a0 Error> mEnterpriseResponseData == null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 539
    :cond_7
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    const-string v4, "removeVpnProfile\u00a0 Error > mService == null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method

.method public setAutoRetryOnConnectionError(Ljava/lang/String;Z)Z
    .locals 6
    .param p1, "profileName"    # Ljava/lang/String;
    .param p2, "enable"    # Z

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1467
    const/4 v2, 0x0

    .line 1469
    .local v2, "setAutoRetryOnConnectionErrorValue":Z
    const/4 v1, 0x0

    .line 1470
    .local v1, "serviceExist":Z
    :try_start_0
    sget-boolean v5, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V1_ENABLED:Z

    if-eqz v5, :cond_4

    .line 1471
    invoke-static {}, Lcom/sec/enterprise/knox/GenericVpnPolicy;->getEnterpriseVpnPolicyService()Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;

    move-result-object v5

    if-eqz v5, :cond_3

    move v1, v3

    .line 1476
    :cond_0
    :goto_0
    if-eqz v1, :cond_8

    .line 1477
    new-instance v3, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v4

    invoke-direct {v3, v4}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const-string v4, "GenericVpnPolicy.setAutoRetryOnConnectionError"

    invoke-static {v3, v4}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1478
    sget-boolean v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V1_ENABLED:Z

    if-eqz v3, :cond_6

    .line 1479
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseVpnPolicyService:Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;

    iget-object v4, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->vendorName:Ljava/lang/String;

    invoke-interface {v3, v4, p1, p2}, Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;->setAutoRetryOnConnectionError(Ljava/lang/String;Ljava/lang/String;Z)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    .line 1484
    :cond_1
    :goto_1
    iget-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    if-eqz v3, :cond_7

    .line 1485
    iget-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v3}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v3

    if-nez v3, :cond_2

    .line 1486
    iget-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v3}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 1496
    :cond_2
    :goto_2
    return v2

    :cond_3
    move v1, v4

    .line 1471
    goto :goto_0

    .line 1472
    :cond_4
    sget-boolean v5, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V2_ENABLED:Z

    if-eqz v5, :cond_0

    .line 1473
    invoke-static {}, Lcom/sec/enterprise/knox/GenericVpnPolicy;->getKnoxVpnPolicyService()Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;

    move-result-object v5

    if-eqz v5, :cond_5

    move v1, v3

    :goto_3
    goto :goto_0

    :cond_5
    move v1, v4

    goto :goto_3

    .line 1481
    :cond_6
    sget-boolean v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V2_ENABLED:Z

    if-eqz v3, :cond_1

    .line 1482
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mKnoxVpnPolicyService:Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;

    iget-object v4, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->vpnContext:Lcom/sec/enterprise/knox/KnoxVpnContext;

    invoke-interface {v3, v4, p1, p2}, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;->setAutoRetryOnConnectionError(Lcom/sec/enterprise/knox/KnoxVpnContext;Ljava/lang/String;Z)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1493
    :catch_0
    move-exception v0

    .line 1494
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed at GenericVpnPolicy API setAutoRetryOnConnectionError-Exception"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 1488
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_7
    :try_start_1
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    const-string v4, "setAutoRetryOnConnection Error > mEnterpriseResponseData == null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 1491
    :cond_8
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    const-string v4, "setAutoRetryOnConnection Error > mService == null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method

.method public setCACertificate(Ljava/lang/String;[B)Z
    .locals 6
    .param p1, "profileName"    # Ljava/lang/String;
    .param p2, "certificateBlob"    # [B

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 725
    const/4 v2, 0x0

    .line 727
    .local v2, "setCACertificateValue":Z
    const/4 v1, 0x0

    .line 728
    .local v1, "serviceExist":Z
    :try_start_0
    sget-boolean v5, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V1_ENABLED:Z

    if-eqz v5, :cond_4

    .line 729
    invoke-static {}, Lcom/sec/enterprise/knox/GenericVpnPolicy;->getEnterpriseVpnPolicyService()Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;

    move-result-object v5

    if-eqz v5, :cond_3

    move v1, v3

    .line 734
    :cond_0
    :goto_0
    if-eqz v1, :cond_8

    .line 735
    new-instance v3, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v4

    invoke-direct {v3, v4}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const-string v4, "GenericVpnPolicy.setCACertificate"

    invoke-static {v3, v4}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 736
    sget-boolean v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V1_ENABLED:Z

    if-eqz v3, :cond_6

    .line 737
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseVpnPolicyService:Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;

    iget-object v4, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->vendorName:Ljava/lang/String;

    invoke-interface {v3, v4, p1, p2}, Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;->setCACertificate(Ljava/lang/String;Ljava/lang/String;[B)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    .line 742
    :cond_1
    :goto_1
    iget-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    if-eqz v3, :cond_7

    .line 743
    iget-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v3}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v3

    if-nez v3, :cond_2

    .line 744
    iget-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v3}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 757
    :cond_2
    :goto_2
    return v2

    :cond_3
    move v1, v4

    .line 729
    goto :goto_0

    .line 730
    :cond_4
    sget-boolean v5, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V2_ENABLED:Z

    if-eqz v5, :cond_0

    .line 731
    invoke-static {}, Lcom/sec/enterprise/knox/GenericVpnPolicy;->getKnoxVpnPolicyService()Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;

    move-result-object v5

    if-eqz v5, :cond_5

    move v1, v3

    :goto_3
    goto :goto_0

    :cond_5
    move v1, v4

    goto :goto_3

    .line 739
    :cond_6
    sget-boolean v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V2_ENABLED:Z

    if-eqz v3, :cond_1

    .line 740
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mKnoxVpnPolicyService:Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;

    iget-object v4, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->vpnContext:Lcom/sec/enterprise/knox/KnoxVpnContext;

    invoke-interface {v3, v4, p1, p2}, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;->setCACertificate(Lcom/sec/enterprise/knox/KnoxVpnContext;Ljava/lang/String;[B)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 752
    :catch_0
    move-exception v0

    .line 753
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed at GenericVpnPolicy API setCACertificate-Exception"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 746
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_7
    :try_start_1
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    const-string v4, "setCACertificate > mEnterpriseResponseData == null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 750
    :cond_8
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    const-string v4, "setCACertificate > mService == null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method

.method public setServerCertValidationUserAcceptanceCriteria(Ljava/lang/String;ZLjava/util/List;I)Z
    .locals 9
    .param p1, "profileName"    # Ljava/lang/String;
    .param p2, "enableValidation"    # Z
    .param p4, "frequency"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;I)Z"
        }
    .end annotation

    .prologue
    .local p3, "condition":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1530
    const/4 v8, 0x0

    .line 1532
    .local v8, "setServerCertValidationUserAcceptanceCriteriaValue":Z
    const/4 v7, 0x0

    .line 1533
    .local v7, "serviceExist":Z
    :try_start_0
    sget-boolean v2, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V1_ENABLED:Z

    if-eqz v2, :cond_4

    .line 1534
    invoke-static {}, Lcom/sec/enterprise/knox/GenericVpnPolicy;->getEnterpriseVpnPolicyService()Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;

    move-result-object v2

    if-eqz v2, :cond_3

    move v7, v0

    .line 1539
    :cond_0
    :goto_0
    if-eqz v7, :cond_8

    .line 1540
    new-instance v0, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    invoke-direct {v0, v1}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const-string v1, "GenericVpnPolicy.setServerCertValidationUserAcceptanceCriteria"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1541
    sget-boolean v0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V1_ENABLED:Z

    if-eqz v0, :cond_6

    .line 1542
    sget-object v0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseVpnPolicyService:Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;

    iget-object v1, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->vendorName:Ljava/lang/String;

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    move v5, p4

    invoke-interface/range {v0 .. v5}, Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;->setServerCertValidationUserAcceptanceCriteria(Ljava/lang/String;Ljava/lang/String;ZLjava/util/List;I)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    .line 1547
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    if-eqz v0, :cond_7

    .line 1548
    iget-object v0, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v0}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v0

    if-nez v0, :cond_2

    .line 1549
    iget-object v0, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v0}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    .line 1559
    :cond_2
    :goto_2
    return v8

    :cond_3
    move v7, v1

    .line 1534
    goto :goto_0

    .line 1535
    :cond_4
    sget-boolean v2, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V2_ENABLED:Z

    if-eqz v2, :cond_0

    .line 1536
    invoke-static {}, Lcom/sec/enterprise/knox/GenericVpnPolicy;->getKnoxVpnPolicyService()Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;

    move-result-object v2

    if-eqz v2, :cond_5

    move v7, v0

    :goto_3
    goto :goto_0

    :cond_5
    move v7, v1

    goto :goto_3

    .line 1544
    :cond_6
    sget-boolean v0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V2_ENABLED:Z

    if-eqz v0, :cond_1

    .line 1545
    sget-object v0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mKnoxVpnPolicyService:Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;

    iget-object v1, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->vpnContext:Lcom/sec/enterprise/knox/KnoxVpnContext;

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    move v5, p4

    invoke-interface/range {v0 .. v5}, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;->setServerCertValidationUserAcceptanceCriteria(Lcom/sec/enterprise/knox/KnoxVpnContext;Ljava/lang/String;ZLjava/util/List;I)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1556
    :catch_0
    move-exception v6

    .line 1557
    .local v6, "e":Landroid/os/RemoteException;
    sget-object v0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed at GenericVpnPolicy API setServerCertValidationUserAcceptanceCriteria-Exception"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v6}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 1551
    .end local v6    # "e":Landroid/os/RemoteException;
    :cond_7
    :try_start_1
    sget-object v0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    const-string v1, "setServerCertValidationUserAcceptanceCriteria Error > mEnterpriseResponseData == null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 1554
    :cond_8
    sget-object v0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    const-string v1, "setServerCertValidationUserAcceptanceCriteria Error > mService == null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method

.method public setUserCertificate(Ljava/lang/String;[BLjava/lang/String;)Z
    .locals 6
    .param p1, "profileName"    # Ljava/lang/String;
    .param p2, "pkcs12Blob"    # [B
    .param p3, "password"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 589
    const/4 v2, 0x0

    .line 591
    .local v2, "setUserCertificateValue":Z
    const/4 v1, 0x0

    .line 592
    .local v1, "serviceExist":Z
    :try_start_0
    sget-boolean v5, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V1_ENABLED:Z

    if-eqz v5, :cond_4

    .line 593
    invoke-static {}, Lcom/sec/enterprise/knox/GenericVpnPolicy;->getEnterpriseVpnPolicyService()Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;

    move-result-object v5

    if-eqz v5, :cond_3

    move v1, v3

    .line 598
    :cond_0
    :goto_0
    if-eqz v1, :cond_8

    .line 599
    new-instance v3, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v4

    invoke-direct {v3, v4}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const-string v4, "GenericVpnPolicy.setUserCertificate"

    invoke-static {v3, v4}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 600
    sget-boolean v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V1_ENABLED:Z

    if-eqz v3, :cond_6

    .line 601
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseVpnPolicyService:Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;

    iget-object v4, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->vendorName:Ljava/lang/String;

    invoke-interface {v3, v4, p1, p2, p3}, Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;->setUserCertificate(Ljava/lang/String;Ljava/lang/String;[BLjava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    .line 606
    :cond_1
    :goto_1
    iget-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    if-eqz v3, :cond_7

    .line 607
    iget-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v3}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v3

    if-nez v3, :cond_2

    .line 608
    iget-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v3}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 620
    :cond_2
    :goto_2
    return v2

    :cond_3
    move v1, v4

    .line 593
    goto :goto_0

    .line 594
    :cond_4
    sget-boolean v5, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V2_ENABLED:Z

    if-eqz v5, :cond_0

    .line 595
    invoke-static {}, Lcom/sec/enterprise/knox/GenericVpnPolicy;->getKnoxVpnPolicyService()Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;

    move-result-object v5

    if-eqz v5, :cond_5

    move v1, v3

    :goto_3
    goto :goto_0

    :cond_5
    move v1, v4

    goto :goto_3

    .line 603
    :cond_6
    sget-boolean v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V2_ENABLED:Z

    if-eqz v3, :cond_1

    .line 604
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mKnoxVpnPolicyService:Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;

    iget-object v4, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->vpnContext:Lcom/sec/enterprise/knox/KnoxVpnContext;

    invoke-interface {v3, v4, p1, p2, p3}, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;->setUserCertificate(Lcom/sec/enterprise/knox/KnoxVpnContext;Ljava/lang/String;[BLjava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 615
    :catch_0
    move-exception v0

    .line 616
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed at GenericVpnPolicy API setUserCertificate-Exception"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 610
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_7
    :try_start_1
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    const-string v4, "setUserCertificate > mEnterpriseResponseData == null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 613
    :cond_8
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    const-string v4, "setUserCertificate > mService == null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method

.method protected setVpnFrameworkSystemProperty(Ljava/lang/String;)V
    .locals 4
    .param p1, "currentFramework"    # Ljava/lang/String;

    .prologue
    .line 353
    :try_start_0
    sget-boolean v1, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V1_ENABLED:Z

    if-eqz v1, :cond_1

    .line 354
    invoke-static {}, Lcom/sec/enterprise/knox/GenericVpnPolicy;->getEnterpriseVpnPolicyService()Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;->setVpnFrameworkSystemProperty(Ljava/lang/String;)V

    .line 361
    :cond_0
    :goto_0
    return-void

    .line 355
    :cond_1
    sget-boolean v1, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V2_ENABLED:Z

    if-eqz v1, :cond_0

    .line 356
    invoke-static {}, Lcom/sec/enterprise/knox/GenericVpnPolicy;->getKnoxVpnPolicyService()Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;->setVpnFrameworkSystemProperty(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 358
    :catch_0
    move-exception v0

    .line 359
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception at setVpnFrameworkProperty in GenericVpnPolicy"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setVpnModeOfOperation(Ljava/lang/String;I)I
    .locals 6
    .param p1, "profileName"    # Ljava/lang/String;
    .param p2, "vpnMode"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1341
    const/4 v2, -0x1

    .line 1343
    .local v2, "setVpnModeOfOperationValue":I
    const/4 v1, 0x0

    .line 1344
    .local v1, "serviceExist":Z
    :try_start_0
    sget-boolean v5, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V1_ENABLED:Z

    if-eqz v5, :cond_4

    .line 1345
    invoke-static {}, Lcom/sec/enterprise/knox/GenericVpnPolicy;->getEnterpriseVpnPolicyService()Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;

    move-result-object v5

    if-eqz v5, :cond_3

    move v1, v3

    .line 1350
    :cond_0
    :goto_0
    if-eqz v1, :cond_8

    .line 1351
    new-instance v3, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v4

    invoke-direct {v3, v4}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const-string v4, "GenericVpnPolicy.setVpnModeOfOperation"

    invoke-static {v3, v4}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1352
    sget-boolean v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V1_ENABLED:Z

    if-eqz v3, :cond_6

    .line 1353
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseVpnPolicyService:Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;

    iget-object v4, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->vendorName:Ljava/lang/String;

    invoke-interface {v3, v4, p1, p2}, Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;->setVpnModeOfOperation(Ljava/lang/String;Ljava/lang/String;I)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    .line 1358
    :cond_1
    :goto_1
    iget-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    if-eqz v3, :cond_7

    .line 1359
    iget-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v3}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v3

    if-nez v3, :cond_2

    .line 1360
    iget-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v3}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 1372
    :cond_2
    :goto_2
    return v2

    :cond_3
    move v1, v4

    .line 1345
    goto :goto_0

    .line 1346
    :cond_4
    sget-boolean v5, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V2_ENABLED:Z

    if-eqz v5, :cond_0

    .line 1347
    invoke-static {}, Lcom/sec/enterprise/knox/GenericVpnPolicy;->getKnoxVpnPolicyService()Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;

    move-result-object v5

    if-eqz v5, :cond_5

    move v1, v3

    :goto_3
    goto :goto_0

    :cond_5
    move v1, v4

    goto :goto_3

    .line 1355
    :cond_6
    sget-boolean v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->KNOX_VPN_V2_ENABLED:Z

    if-eqz v3, :cond_1

    .line 1356
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mKnoxVpnPolicyService:Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;

    iget-object v4, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->vpnContext:Lcom/sec/enterprise/knox/KnoxVpnContext;

    invoke-interface {v3, v4, p1, p2}, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;->setVpnModeOfOperation(Lcom/sec/enterprise/knox/KnoxVpnContext;Ljava/lang/String;I)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/enterprise/knox/GenericVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1367
    :catch_0
    move-exception v0

    .line 1368
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed at GenericVpnPolicy API setVpnModeOfOperation-Exception"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 1362
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_7
    :try_start_1
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    const-string v4, "setVpnModeOfOperation > mEnterpriseResponseData == null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 1365
    :cond_8
    sget-object v3, Lcom/sec/enterprise/knox/GenericVpnPolicy;->TAG:Ljava/lang/String;

    const-string v4, "setVpnModeOfOperation > mService == null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method
