.class public final Lcom/sec/enterprise/internal/EDMNativeHelper;
.super Ljava/lang/Object;
.source "EDMNativeHelper.java"


# static fields
.field public static TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const-string v0, "EDMNativeHelper"

    sput-object v0, Lcom/sec/enterprise/internal/EDMNativeHelper;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static isAVRCPProfileEnabled()Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 126
    const-string v3, "bluetooth_policy"

    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v3

    invoke-static {v3}, Landroid/app/enterprise/IBluetoothPolicy$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/IBluetoothPolicy;

    move-result-object v1

    .line 129
    .local v1, "service":Landroid/app/enterprise/IBluetoothPolicy;
    if-eqz v1, :cond_0

    .line 131
    const/16 v3, 0x10

    const/4 v4, 0x1

    :try_start_0
    invoke-interface {v1, v3, v4}, Landroid/app/enterprise/IBluetoothPolicy;->isProfileEnabledInternal(IZ)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 137
    :cond_0
    :goto_0
    return v2

    .line 132
    :catch_0
    move-exception v0

    .line 133
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static isAuditLogEnabled()Z
    .locals 3

    .prologue
    .line 172
    const-string v2, "auditlog"

    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/enterprise/knox/auditlog/IAuditLog$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/enterprise/knox/auditlog/IAuditLog;

    move-result-object v1

    .line 175
    .local v1, "service":Lcom/sec/enterprise/knox/auditlog/IAuditLog;
    if-eqz v1, :cond_0

    .line 177
    :try_start_0
    invoke-interface {v1}, Lcom/sec/enterprise/knox/auditlog/IAuditLog;->isAuditServiceRunning()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 183
    :goto_0
    return v2

    .line 178
    :catch_0
    move-exception v0

    .line 179
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 183
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static isBTOutgoingCallEnabled()Z
    .locals 3

    .prologue
    .line 141
    const-string v2, "bluetooth_policy"

    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Landroid/app/enterprise/IBluetoothPolicy$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/IBluetoothPolicy;

    move-result-object v1

    .line 144
    .local v1, "service":Landroid/app/enterprise/IBluetoothPolicy;
    if-eqz v1, :cond_0

    .line 146
    :try_start_0
    new-instance v2, Landroid/app/enterprise/ContextInfo;

    invoke-direct {v2}, Landroid/app/enterprise/ContextInfo;-><init>()V

    invoke-interface {v1, v2}, Landroid/app/enterprise/IBluetoothPolicy;->isOutgoingCallsAllowed(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 152
    :goto_0
    return v2

    .line 147
    :catch_0
    move-exception v0

    .line 148
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 152
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public static isCameraEnabled(I)Z
    .locals 7
    .param p0, "uid"    # I

    .prologue
    const/4 v4, 0x1

    .line 84
    const-string v5, "restriction_policy"

    invoke-static {v5}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v5

    invoke-static {v5}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v3

    .line 87
    .local v3, "service":Landroid/app/enterprise/IRestrictionPolicy;
    if-eqz v3, :cond_1

    .line 89
    :try_start_0
    new-instance v5, Landroid/app/enterprise/ContextInfo;

    invoke-direct {v5, p0}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const/4 v6, 0x1

    invoke-interface {v3, v5, v6}, Landroid/app/enterprise/IRestrictionPolicy;->isCameraEnabled(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 107
    :cond_0
    :goto_0
    return v0

    .line 90
    :catch_0
    move-exception v2

    .line 91
    .local v2, "e":Landroid/os/RemoteException;
    invoke-virtual {v2}, Landroid/os/RemoteException;->printStackTrace()V

    .line 95
    .end local v2    # "e":Landroid/os/RemoteException;
    :cond_1
    const/4 v0, 0x1

    .line 97
    .local v0, "cameraEnabled":Z
    const-string v5, "device_policy"

    invoke-static {v5}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v5

    invoke-static {v5}, Landroid/app/admin/IDevicePolicyManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/admin/IDevicePolicyManager;

    move-result-object v1

    .line 99
    .local v1, "dpmService":Landroid/app/admin/IDevicePolicyManager;
    if-eqz v1, :cond_0

    .line 101
    const/4 v5, 0x0

    :try_start_1
    invoke-static {p0}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v6

    invoke-interface {v1, v5, v6}, Landroid/app/admin/IDevicePolicyManager;->getCameraDisabled(Landroid/content/ComponentName;I)Z
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v5

    if-nez v5, :cond_2

    move v0, v4

    :goto_1
    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 102
    :catch_1
    move-exception v2

    .line 103
    .restart local v2    # "e":Landroid/os/RemoteException;
    invoke-virtual {v2}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static isMicrophoneEnabled(I)Z
    .locals 5
    .param p0, "uid"    # I

    .prologue
    const/4 v2, 0x1

    .line 156
    const-string v3, "restriction_policy"

    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v3

    invoke-static {v3}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    .line 159
    .local v1, "service":Landroid/app/enterprise/IRestrictionPolicy;
    if-eqz v1, :cond_0

    .line 161
    :try_start_0
    new-instance v3, Landroid/app/enterprise/ContextInfo;

    invoke-direct {v3, p0}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const/4 v4, 0x1

    invoke-interface {v1, v3, v4}, Landroid/app/enterprise/IRestrictionPolicy;->isMicrophoneEnabled(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 167
    :cond_0
    :goto_0
    return v2

    .line 162
    :catch_0
    move-exception v0

    .line 163
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static isScreenCaptureEnabled()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 111
    const-string v3, "restriction_policy"

    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v3

    invoke-static {v3}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    .line 114
    .local v1, "service":Landroid/app/enterprise/IRestrictionPolicy;
    if-eqz v1, :cond_0

    .line 116
    const/4 v3, 0x1

    :try_start_0
    invoke-interface {v1, v3}, Landroid/app/enterprise/IRestrictionPolicy;->isScreenCaptureEnabledInternal(Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 122
    :cond_0
    :goto_0
    return v2

    .line 117
    :catch_0
    move-exception v0

    .line 118
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static nativeLogger(IIIILjava/lang/String;[B)V
    .locals 10
    .param p0, "severityGrade"    # I
    .param p1, "moduleGroup"    # I
    .param p2, "outcome"    # I
    .param p3, "uid"    # I
    .param p4, "swComponent"    # Ljava/lang/String;
    .param p5, "logMessage"    # [B

    .prologue
    .line 71
    :try_start_0
    new-instance v9, Ljava/lang/String;

    const-string v0, "UTF-8"

    invoke-direct {v9, p5, v0}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 72
    .local v9, "logs":Ljava/lang/String;
    const-string v0, "\\n"

    invoke-virtual {v9, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 73
    .local v8, "inputSplitNewLine":[Ljava/lang/String;
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    array-length v0, v8

    if-ge v7, v0, :cond_0

    .line 74
    const/4 v2, 0x1

    aget-object v5, v8, v7

    move v0, p0

    move v1, p1

    move v3, p3

    move-object v4, p4

    invoke-static/range {v0 .. v5}, Landroid/sec/enterprise/auditlog/AuditLog;->log(IIZILjava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 73
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 77
    .end local v7    # "i":I
    .end local v8    # "inputSplitNewLine":[Ljava/lang/String;
    .end local v9    # "logs":Ljava/lang/String;
    :catch_0
    move-exception v6

    .line 78
    .local v6, "e":Ljava/io/UnsupportedEncodingException;
    sget-object v0, Lcom/sec/enterprise/internal/EDMNativeHelper;->TAG:Ljava/lang/String;

    const-string v1, "Unsupported conversion"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    .end local v6    # "e":Ljava/io/UnsupportedEncodingException;
    :cond_0
    return-void
.end method

.method public static nativeLogger(IIZILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "severityGrade"    # I
    .param p1, "moduleGroup"    # I
    .param p2, "outcome"    # Z
    .param p3, "uid"    # I
    .param p4, "swComponent"    # Ljava/lang/String;
    .param p5, "logMessage"    # Ljava/lang/String;

    .prologue
    .line 65
    invoke-static/range {p0 .. p5}, Landroid/sec/enterprise/auditlog/AuditLog;->log(IIZILjava/lang/String;Ljava/lang/String;)V

    .line 66
    return-void
.end method

.method public static sendIntent(I)V
    .locals 3
    .param p0, "restriction"    # I

    .prologue
    .line 50
    const-string v2, "enterprise_policy"

    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Landroid/app/enterprise/IEnterpriseDeviceManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/IEnterpriseDeviceManager;

    move-result-object v1

    .line 53
    .local v1, "service":Landroid/app/enterprise/IEnterpriseDeviceManager;
    if-eqz v1, :cond_0

    .line 55
    :try_start_0
    invoke-interface {v1, p0}, Landroid/app/enterprise/IEnterpriseDeviceManager;->sendIntent(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 61
    :cond_0
    :goto_0
    return-void

    .line 56
    :catch_0
    move-exception v0

    .line 57
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public static native setAuditEnabled(Z)V
.end method

.method public static updateRemoteScreenDimensionsAndCallerUid(III)V
    .locals 3
    .param p0, "width"    # I
    .param p1, "height"    # I
    .param p2, "uid"    # I

    .prologue
    .line 187
    const-string v2, "remoteinjection"

    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Landroid/app/enterprise/remotecontrol/IRemoteInjection$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/remotecontrol/IRemoteInjection;

    move-result-object v1

    .line 190
    .local v1, "service":Landroid/app/enterprise/remotecontrol/IRemoteInjection;
    if-eqz v1, :cond_0

    .line 192
    :try_start_0
    invoke-interface {v1, p0, p1, p2}, Landroid/app/enterprise/remotecontrol/IRemoteInjection;->updateRemoteScreenDimensionsAndCallerUid(III)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 197
    :cond_0
    :goto_0
    return-void

    .line 193
    :catch_0
    move-exception v0

    .line 194
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method
