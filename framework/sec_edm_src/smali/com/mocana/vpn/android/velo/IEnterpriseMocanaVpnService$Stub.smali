.class public abstract Lcom/mocana/vpn/android/velo/IEnterpriseMocanaVpnService$Stub;
.super Landroid/os/Binder;
.source "IEnterpriseMocanaVpnService.java"

# interfaces
.implements Lcom/mocana/vpn/android/velo/IEnterpriseMocanaVpnService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mocana/vpn/android/velo/IEnterpriseMocanaVpnService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mocana/vpn/android/velo/IEnterpriseMocanaVpnService$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.mocana.vpn.android.velo.IEnterpriseMocanaVpnService"

.field static final TRANSACTION_createConnection:I = 0x1

.field static final TRANSACTION_enableDefaultRoute:I = 0x30

.field static final TRANSACTION_getAllConnections:I = 0x3

.field static final TRANSACTION_getAuthMethod:I = 0x12

.field static final TRANSACTION_getBackupServerName:I = 0x29

.field static final TRANSACTION_getCACertificate:I = 0x8

.field static final TRANSACTION_getConnection:I = 0x4

.field static final TRANSACTION_getDHGroup:I = 0x1a

.field static final TRANSACTION_getDeadPeerDetection:I = 0x2b

.field static final TRANSACTION_getErrorString:I = 0x22

.field static final TRANSACTION_getFIPSMode:I = 0x35

.field static final TRANSACTION_getForwardRoutes:I = 0x25

.field static final TRANSACTION_getIKEVersion:I = 0x20

.field static final TRANSACTION_getIPSecIdentfier:I = 0x1e

.field static final TRANSACTION_getIPsecIdentifierType:I = 0x1d

.field static final TRANSACTION_getMobike:I = 0x2d

.field static final TRANSACTION_getP1Mode:I = 0x16

.field static final TRANSACTION_getPFS:I = 0x18

.field static final TRANSACTION_getPresharedKey:I = 0x14

.field static final TRANSACTION_getServerName:I = 0xa

.field static final TRANSACTION_getSplitTunnelType:I = 0x27

.field static final TRANSACTION_getState:I = 0x21

.field static final TRANSACTION_getSuiteBType:I = 0x2f

.field static final TRANSACTION_getUserCertificate:I = 0x7

.field static final TRANSACTION_getUserName:I = 0x10

.field static final TRANSACTION_getUserPassword:I = 0x11

.field static final TRANSACTION_isAdminProfile:I = 0x23

.field static final TRANSACTION_isDefaultRouteEnabled:I = 0x31

.field static final TRANSACTION_isUserAuthEnabled:I = 0x33

.field static final TRANSACTION_removeConnection:I = 0x2

.field static final TRANSACTION_setAuthMethod:I = 0xf

.field static final TRANSACTION_setBackupServerName:I = 0x28

.field static final TRANSACTION_setCACertificate:I = 0x6

.field static final TRANSACTION_setDHGroup:I = 0x19

.field static final TRANSACTION_setDeadPeerDetection:I = 0x2a

.field static final TRANSACTION_setForwardRoutes:I = 0x24

.field static final TRANSACTION_setIKEVersion:I = 0x1f

.field static final TRANSACTION_setIPSecIdentfier:I = 0x1c

.field static final TRANSACTION_setIPsecIdentifierType:I = 0x1b

.field static final TRANSACTION_setMobike:I = 0x2c

.field static final TRANSACTION_setP1Mode:I = 0x15

.field static final TRANSACTION_setPFS:I = 0x17

.field static final TRANSACTION_setPresharedKey:I = 0x13

.field static final TRANSACTION_setServerName:I = 0x9

.field static final TRANSACTION_setSplitTunnelType:I = 0x26

.field static final TRANSACTION_setSuiteBType:I = 0x2e

.field static final TRANSACTION_setUserAuth:I = 0x32

.field static final TRANSACTION_setUserCertificate:I = 0x5

.field static final TRANSACTION_setUserName:I = 0xd

.field static final TRANSACTION_setUserPassword:I = 0xe

.field static final TRANSACTION_startConnection:I = 0xb

.field static final TRANSACTION_stopConnection:I = 0xc

.field static final TRANSACTION_switchFIPSMode:I = 0x34


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 15
    const-string v0, "com.mocana.vpn.android.velo.IEnterpriseMocanaVpnService"

    invoke-virtual {p0, p0, v0}, Lcom/mocana/vpn/android/velo/IEnterpriseMocanaVpnService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 16
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/mocana/vpn/android/velo/IEnterpriseMocanaVpnService;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 23
    if-nez p0, :cond_0

    .line 24
    const/4 v0, 0x0

    .line 30
    :goto_0
    return-object v0

    .line 26
    :cond_0
    const-string v1, "com.mocana.vpn.android.velo.IEnterpriseMocanaVpnService"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 27
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/mocana/vpn/android/velo/IEnterpriseMocanaVpnService;

    if-eqz v1, :cond_1

    .line 28
    check-cast v0, Lcom/mocana/vpn/android/velo/IEnterpriseMocanaVpnService;

    goto :goto_0

    .line 30
    :cond_1
    new-instance v0, Lcom/mocana/vpn/android/velo/IEnterpriseMocanaVpnService$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/mocana/vpn/android/velo/IEnterpriseMocanaVpnService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 11
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    const/4 v9, 0x1

    .line 38
    sparse-switch p1, :sswitch_data_0

    .line 749
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v9

    :goto_0
    return v9

    .line 42
    :sswitch_0
    const-string v8, "com.mocana.vpn.android.velo.IEnterpriseMocanaVpnService"

    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 47
    :sswitch_1
    const-string v10, "com.mocana.vpn.android.velo.IEnterpriseMocanaVpnService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 49
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_1

    .line 50
    sget-object v10, Lcom/sec/enterprise/knox/EnterprisePremiumVpnConnection;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnConnection;

    .line 56
    .local v0, "_arg0":Lcom/sec/enterprise/knox/EnterprisePremiumVpnConnection;
    :goto_1
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 58
    .local v1, "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 59
    .local v3, "_arg2":I
    invoke-virtual {p0, v0, v1, v3}, Lcom/mocana/vpn/android/velo/IEnterpriseMocanaVpnService$Stub;->createConnection(Lcom/sec/enterprise/knox/EnterprisePremiumVpnConnection;Ljava/lang/String;I)Z

    move-result v5

    .line 60
    .local v5, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 61
    if-eqz v5, :cond_0

    move v8, v9

    :cond_0
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 53
    .end local v0    # "_arg0":Lcom/sec/enterprise/knox/EnterprisePremiumVpnConnection;
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":I
    .end local v5    # "_result":Z
    :cond_1
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Lcom/sec/enterprise/knox/EnterprisePremiumVpnConnection;
    goto :goto_1

    .line 66
    .end local v0    # "_arg0":Lcom/sec/enterprise/knox/EnterprisePremiumVpnConnection;
    :sswitch_2
    const-string v10, "com.mocana.vpn.android.velo.IEnterpriseMocanaVpnService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 68
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 70
    .local v0, "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v10

    invoke-static {v10}, Lcom/mocana/vpn/android/velo/IEnterpriseStatusCallback$Stub;->asInterface(Landroid/os/IBinder;)Lcom/mocana/vpn/android/velo/IEnterpriseStatusCallback;

    move-result-object v1

    .line 72
    .local v1, "_arg1":Lcom/mocana/vpn/android/velo/IEnterpriseStatusCallback;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 73
    .restart local v3    # "_arg2":I
    invoke-virtual {p0, v0, v1, v3}, Lcom/mocana/vpn/android/velo/IEnterpriseMocanaVpnService$Stub;->removeConnection(Ljava/lang/String;Lcom/mocana/vpn/android/velo/IEnterpriseStatusCallback;I)Z

    move-result v5

    .line 74
    .restart local v5    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 75
    if-eqz v5, :cond_2

    move v8, v9

    :cond_2
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 80
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":Lcom/mocana/vpn/android/velo/IEnterpriseStatusCallback;
    .end local v3    # "_arg2":I
    .end local v5    # "_result":Z
    :sswitch_3
    const-string v8, "com.mocana.vpn.android.velo.IEnterpriseMocanaVpnService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 82
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 83
    .local v0, "_arg0":I
    invoke-virtual {p0, v0}, Lcom/mocana/vpn/android/velo/IEnterpriseMocanaVpnService$Stub;->getAllConnections(I)Ljava/util/List;

    move-result-object v6

    .line 84
    .local v6, "_result":Ljava/util/List;, "Ljava/util/List<Lcom/sec/enterprise/knox/EnterprisePremiumVpnConnection;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 85
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    goto :goto_0

    .line 90
    .end local v0    # "_arg0":I
    .end local v6    # "_result":Ljava/util/List;, "Ljava/util/List<Lcom/sec/enterprise/knox/EnterprisePremiumVpnConnection;>;"
    :sswitch_4
    const-string v10, "com.mocana.vpn.android.velo.IEnterpriseMocanaVpnService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 92
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 94
    .local v0, "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 95
    .local v1, "_arg1":I
    invoke-virtual {p0, v0, v1}, Lcom/mocana/vpn/android/velo/IEnterpriseMocanaVpnService$Stub;->getConnection(Ljava/lang/String;I)Lcom/sec/enterprise/knox/EnterprisePremiumVpnConnection;

    move-result-object v5

    .line 96
    .local v5, "_result":Lcom/sec/enterprise/knox/EnterprisePremiumVpnConnection;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 97
    if-eqz v5, :cond_3

    .line 98
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 99
    invoke-virtual {v5, p3, v9}, Lcom/sec/enterprise/knox/EnterprisePremiumVpnConnection;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 102
    :cond_3
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 108
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":I
    .end local v5    # "_result":Lcom/sec/enterprise/knox/EnterprisePremiumVpnConnection;
    :sswitch_5
    const-string v10, "com.mocana.vpn.android.velo.IEnterpriseMocanaVpnService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 110
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 112
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v1

    .line 114
    .local v1, "_arg1":[B
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 116
    .local v3, "_arg2":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 117
    .local v4, "_arg3":I
    invoke-virtual {p0, v0, v1, v3, v4}, Lcom/mocana/vpn/android/velo/IEnterpriseMocanaVpnService$Stub;->setUserCertificate(Ljava/lang/String;[BLjava/lang/String;I)Z

    move-result v5

    .line 118
    .local v5, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 119
    if-eqz v5, :cond_4

    move v8, v9

    :cond_4
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 124
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":[B
    .end local v3    # "_arg2":Ljava/lang/String;
    .end local v4    # "_arg3":I
    .end local v5    # "_result":Z
    :sswitch_6
    const-string v10, "com.mocana.vpn.android.velo.IEnterpriseMocanaVpnService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 126
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 128
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v1

    .line 130
    .restart local v1    # "_arg1":[B
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 132
    .restart local v3    # "_arg2":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 133
    .restart local v4    # "_arg3":I
    invoke-virtual {p0, v0, v1, v3, v4}, Lcom/mocana/vpn/android/velo/IEnterpriseMocanaVpnService$Stub;->setCACertificate(Ljava/lang/String;[BLjava/lang/String;I)Z

    move-result v5

    .line 134
    .restart local v5    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 135
    if-eqz v5, :cond_5

    move v8, v9

    :cond_5
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 140
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":[B
    .end local v3    # "_arg2":Ljava/lang/String;
    .end local v4    # "_arg3":I
    .end local v5    # "_result":Z
    :sswitch_7
    const-string v10, "com.mocana.vpn.android.velo.IEnterpriseMocanaVpnService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 142
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 144
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 145
    .local v1, "_arg1":I
    invoke-virtual {p0, v0, v1}, Lcom/mocana/vpn/android/velo/IEnterpriseMocanaVpnService$Stub;->getUserCertificate(Ljava/lang/String;I)Landroid/app/enterprise/CertificateInfo;

    move-result-object v5

    .line 146
    .local v5, "_result":Landroid/app/enterprise/CertificateInfo;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 147
    if-eqz v5, :cond_6

    .line 148
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 149
    invoke-virtual {v5, p3, v9}, Landroid/app/enterprise/CertificateInfo;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 152
    :cond_6
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 158
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":I
    .end local v5    # "_result":Landroid/app/enterprise/CertificateInfo;
    :sswitch_8
    const-string v10, "com.mocana.vpn.android.velo.IEnterpriseMocanaVpnService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 160
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 162
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 163
    .restart local v1    # "_arg1":I
    invoke-virtual {p0, v0, v1}, Lcom/mocana/vpn/android/velo/IEnterpriseMocanaVpnService$Stub;->getCACertificate(Ljava/lang/String;I)Landroid/app/enterprise/CertificateInfo;

    move-result-object v5

    .line 164
    .restart local v5    # "_result":Landroid/app/enterprise/CertificateInfo;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 165
    if-eqz v5, :cond_7

    .line 166
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 167
    invoke-virtual {v5, p3, v9}, Landroid/app/enterprise/CertificateInfo;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 170
    :cond_7
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 176
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":I
    .end local v5    # "_result":Landroid/app/enterprise/CertificateInfo;
    :sswitch_9
    const-string v10, "com.mocana.vpn.android.velo.IEnterpriseMocanaVpnService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 178
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 180
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 182
    .local v1, "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 183
    .local v3, "_arg2":I
    invoke-virtual {p0, v0, v1, v3}, Lcom/mocana/vpn/android/velo/IEnterpriseMocanaVpnService$Stub;->setServerName(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v5

    .line 184
    .local v5, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 185
    if-eqz v5, :cond_8

    move v8, v9

    :cond_8
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 190
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":I
    .end local v5    # "_result":Z
    :sswitch_a
    const-string v8, "com.mocana.vpn.android.velo.IEnterpriseMocanaVpnService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 192
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 194
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 195
    .local v1, "_arg1":I
    invoke-virtual {p0, v0, v1}, Lcom/mocana/vpn/android/velo/IEnterpriseMocanaVpnService$Stub;->getServerName(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v5

    .line 196
    .local v5, "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 197
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 202
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":I
    .end local v5    # "_result":Ljava/lang/String;
    :sswitch_b
    const-string v10, "com.mocana.vpn.android.velo.IEnterpriseMocanaVpnService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 204
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 206
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v10

    invoke-static {v10}, Lcom/mocana/vpn/android/velo/IEnterpriseStatusCallback$Stub;->asInterface(Landroid/os/IBinder;)Lcom/mocana/vpn/android/velo/IEnterpriseStatusCallback;

    move-result-object v1

    .line 208
    .local v1, "_arg1":Lcom/mocana/vpn/android/velo/IEnterpriseStatusCallback;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 209
    .restart local v3    # "_arg2":I
    invoke-virtual {p0, v0, v1, v3}, Lcom/mocana/vpn/android/velo/IEnterpriseMocanaVpnService$Stub;->startConnection(Ljava/lang/String;Lcom/mocana/vpn/android/velo/IEnterpriseStatusCallback;I)Z

    move-result v5

    .line 210
    .local v5, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 211
    if-eqz v5, :cond_9

    move v8, v9

    :cond_9
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 216
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":Lcom/mocana/vpn/android/velo/IEnterpriseStatusCallback;
    .end local v3    # "_arg2":I
    .end local v5    # "_result":Z
    :sswitch_c
    const-string v10, "com.mocana.vpn.android.velo.IEnterpriseMocanaVpnService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 218
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 220
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v10

    invoke-static {v10}, Lcom/mocana/vpn/android/velo/IEnterpriseStatusCallback$Stub;->asInterface(Landroid/os/IBinder;)Lcom/mocana/vpn/android/velo/IEnterpriseStatusCallback;

    move-result-object v1

    .line 222
    .restart local v1    # "_arg1":Lcom/mocana/vpn/android/velo/IEnterpriseStatusCallback;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 223
    .restart local v3    # "_arg2":I
    invoke-virtual {p0, v0, v1, v3}, Lcom/mocana/vpn/android/velo/IEnterpriseMocanaVpnService$Stub;->stopConnection(Ljava/lang/String;Lcom/mocana/vpn/android/velo/IEnterpriseStatusCallback;I)Z

    move-result v5

    .line 224
    .restart local v5    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 225
    if-eqz v5, :cond_a

    move v8, v9

    :cond_a
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 230
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":Lcom/mocana/vpn/android/velo/IEnterpriseStatusCallback;
    .end local v3    # "_arg2":I
    .end local v5    # "_result":Z
    :sswitch_d
    const-string v10, "com.mocana.vpn.android.velo.IEnterpriseMocanaVpnService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 232
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 234
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 236
    .local v1, "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 237
    .restart local v3    # "_arg2":I
    invoke-virtual {p0, v0, v1, v3}, Lcom/mocana/vpn/android/velo/IEnterpriseMocanaVpnService$Stub;->setUserName(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v5

    .line 238
    .restart local v5    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 239
    if-eqz v5, :cond_b

    move v8, v9

    :cond_b
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 244
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":I
    .end local v5    # "_result":Z
    :sswitch_e
    const-string v10, "com.mocana.vpn.android.velo.IEnterpriseMocanaVpnService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 246
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 248
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 250
    .restart local v1    # "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 251
    .restart local v3    # "_arg2":I
    invoke-virtual {p0, v0, v1, v3}, Lcom/mocana/vpn/android/velo/IEnterpriseMocanaVpnService$Stub;->setUserPassword(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v5

    .line 252
    .restart local v5    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 253
    if-eqz v5, :cond_c

    move v8, v9

    :cond_c
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 258
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":I
    .end local v5    # "_result":Z
    :sswitch_f
    const-string v10, "com.mocana.vpn.android.velo.IEnterpriseMocanaVpnService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 260
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 262
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 264
    .local v1, "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 265
    .restart local v3    # "_arg2":I
    invoke-virtual {p0, v0, v1, v3}, Lcom/mocana/vpn/android/velo/IEnterpriseMocanaVpnService$Stub;->setAuthMethod(Ljava/lang/String;II)Z

    move-result v5

    .line 266
    .restart local v5    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 267
    if-eqz v5, :cond_d

    move v8, v9

    :cond_d
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 272
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":I
    .end local v3    # "_arg2":I
    .end local v5    # "_result":Z
    :sswitch_10
    const-string v8, "com.mocana.vpn.android.velo.IEnterpriseMocanaVpnService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 274
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 276
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 277
    .restart local v1    # "_arg1":I
    invoke-virtual {p0, v0, v1}, Lcom/mocana/vpn/android/velo/IEnterpriseMocanaVpnService$Stub;->getUserName(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v5

    .line 278
    .local v5, "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 279
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 284
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":I
    .end local v5    # "_result":Ljava/lang/String;
    :sswitch_11
    const-string v8, "com.mocana.vpn.android.velo.IEnterpriseMocanaVpnService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 286
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 288
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 289
    .restart local v1    # "_arg1":I
    invoke-virtual {p0, v0, v1}, Lcom/mocana/vpn/android/velo/IEnterpriseMocanaVpnService$Stub;->getUserPassword(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v5

    .line 290
    .restart local v5    # "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 291
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 296
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":I
    .end local v5    # "_result":Ljava/lang/String;
    :sswitch_12
    const-string v8, "com.mocana.vpn.android.velo.IEnterpriseMocanaVpnService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 298
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 300
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 301
    .restart local v1    # "_arg1":I
    invoke-virtual {p0, v0, v1}, Lcom/mocana/vpn/android/velo/IEnterpriseMocanaVpnService$Stub;->getAuthMethod(Ljava/lang/String;I)I

    move-result v5

    .line 302
    .local v5, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 303
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 308
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":I
    .end local v5    # "_result":I
    :sswitch_13
    const-string v10, "com.mocana.vpn.android.velo.IEnterpriseMocanaVpnService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 310
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 312
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 314
    .local v1, "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 315
    .restart local v3    # "_arg2":I
    invoke-virtual {p0, v0, v1, v3}, Lcom/mocana/vpn/android/velo/IEnterpriseMocanaVpnService$Stub;->setPresharedKey(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v5

    .line 316
    .local v5, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 317
    if-eqz v5, :cond_e

    move v8, v9

    :cond_e
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 322
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":I
    .end local v5    # "_result":Z
    :sswitch_14
    const-string v8, "com.mocana.vpn.android.velo.IEnterpriseMocanaVpnService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 324
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 326
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 327
    .local v1, "_arg1":I
    invoke-virtual {p0, v0, v1}, Lcom/mocana/vpn/android/velo/IEnterpriseMocanaVpnService$Stub;->getPresharedKey(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v5

    .line 328
    .local v5, "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 329
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 334
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":I
    .end local v5    # "_result":Ljava/lang/String;
    :sswitch_15
    const-string v10, "com.mocana.vpn.android.velo.IEnterpriseMocanaVpnService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 336
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 338
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 340
    .restart local v1    # "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 341
    .restart local v3    # "_arg2":I
    invoke-virtual {p0, v0, v1, v3}, Lcom/mocana/vpn/android/velo/IEnterpriseMocanaVpnService$Stub;->setP1Mode(Ljava/lang/String;II)Z

    move-result v5

    .line 342
    .local v5, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 343
    if-eqz v5, :cond_f

    move v8, v9

    :cond_f
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 348
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":I
    .end local v3    # "_arg2":I
    .end local v5    # "_result":Z
    :sswitch_16
    const-string v8, "com.mocana.vpn.android.velo.IEnterpriseMocanaVpnService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 350
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 352
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 353
    .restart local v1    # "_arg1":I
    invoke-virtual {p0, v0, v1}, Lcom/mocana/vpn/android/velo/IEnterpriseMocanaVpnService$Stub;->getP1Mode(Ljava/lang/String;I)I

    move-result v5

    .line 354
    .local v5, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 355
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 360
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":I
    .end local v5    # "_result":I
    :sswitch_17
    const-string v10, "com.mocana.vpn.android.velo.IEnterpriseMocanaVpnService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 362
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 364
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_11

    move v1, v9

    .line 366
    .local v1, "_arg1":Z
    :goto_2
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 367
    .restart local v3    # "_arg2":I
    invoke-virtual {p0, v0, v1, v3}, Lcom/mocana/vpn/android/velo/IEnterpriseMocanaVpnService$Stub;->setPFS(Ljava/lang/String;ZI)Z

    move-result v5

    .line 368
    .local v5, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 369
    if-eqz v5, :cond_10

    move v8, v9

    :cond_10
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .end local v1    # "_arg1":Z
    .end local v3    # "_arg2":I
    .end local v5    # "_result":Z
    :cond_11
    move v1, v8

    .line 364
    goto :goto_2

    .line 374
    .end local v0    # "_arg0":Ljava/lang/String;
    :sswitch_18
    const-string v8, "com.mocana.vpn.android.velo.IEnterpriseMocanaVpnService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 376
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 378
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 379
    .local v1, "_arg1":I
    invoke-virtual {p0, v0, v1}, Lcom/mocana/vpn/android/velo/IEnterpriseMocanaVpnService$Stub;->getPFS(Ljava/lang/String;I)I

    move-result v5

    .line 380
    .local v5, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 381
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 386
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":I
    .end local v5    # "_result":I
    :sswitch_19
    const-string v10, "com.mocana.vpn.android.velo.IEnterpriseMocanaVpnService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 388
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 390
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 392
    .restart local v1    # "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 393
    .restart local v3    # "_arg2":I
    invoke-virtual {p0, v0, v1, v3}, Lcom/mocana/vpn/android/velo/IEnterpriseMocanaVpnService$Stub;->setDHGroup(Ljava/lang/String;II)Z

    move-result v5

    .line 394
    .local v5, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 395
    if-eqz v5, :cond_12

    move v8, v9

    :cond_12
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 400
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":I
    .end local v3    # "_arg2":I
    .end local v5    # "_result":Z
    :sswitch_1a
    const-string v8, "com.mocana.vpn.android.velo.IEnterpriseMocanaVpnService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 402
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 404
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 405
    .restart local v1    # "_arg1":I
    invoke-virtual {p0, v0, v1}, Lcom/mocana/vpn/android/velo/IEnterpriseMocanaVpnService$Stub;->getDHGroup(Ljava/lang/String;I)I

    move-result v5

    .line 406
    .local v5, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 407
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 412
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":I
    .end local v5    # "_result":I
    :sswitch_1b
    const-string v10, "com.mocana.vpn.android.velo.IEnterpriseMocanaVpnService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 414
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 416
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 418
    .restart local v1    # "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 419
    .restart local v3    # "_arg2":I
    invoke-virtual {p0, v0, v1, v3}, Lcom/mocana/vpn/android/velo/IEnterpriseMocanaVpnService$Stub;->setIPsecIdentifierType(Ljava/lang/String;II)Z

    move-result v5

    .line 420
    .local v5, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 421
    if-eqz v5, :cond_13

    move v8, v9

    :cond_13
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 426
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":I
    .end local v3    # "_arg2":I
    .end local v5    # "_result":Z
    :sswitch_1c
    const-string v10, "com.mocana.vpn.android.velo.IEnterpriseMocanaVpnService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 428
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 430
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 432
    .local v1, "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 433
    .restart local v3    # "_arg2":I
    invoke-virtual {p0, v0, v1, v3}, Lcom/mocana/vpn/android/velo/IEnterpriseMocanaVpnService$Stub;->setIPSecIdentfier(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v5

    .line 434
    .restart local v5    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 435
    if-eqz v5, :cond_14

    move v8, v9

    :cond_14
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 440
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":I
    .end local v5    # "_result":Z
    :sswitch_1d
    const-string v8, "com.mocana.vpn.android.velo.IEnterpriseMocanaVpnService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 442
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 444
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 445
    .local v1, "_arg1":I
    invoke-virtual {p0, v0, v1}, Lcom/mocana/vpn/android/velo/IEnterpriseMocanaVpnService$Stub;->getIPsecIdentifierType(Ljava/lang/String;I)I

    move-result v5

    .line 446
    .local v5, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 447
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 452
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":I
    .end local v5    # "_result":I
    :sswitch_1e
    const-string v8, "com.mocana.vpn.android.velo.IEnterpriseMocanaVpnService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 454
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 456
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 457
    .restart local v1    # "_arg1":I
    invoke-virtual {p0, v0, v1}, Lcom/mocana/vpn/android/velo/IEnterpriseMocanaVpnService$Stub;->getIPSecIdentfier(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v5

    .line 458
    .local v5, "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 459
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 464
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":I
    .end local v5    # "_result":Ljava/lang/String;
    :sswitch_1f
    const-string v10, "com.mocana.vpn.android.velo.IEnterpriseMocanaVpnService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 466
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 468
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 470
    .restart local v1    # "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 471
    .restart local v3    # "_arg2":I
    invoke-virtual {p0, v0, v1, v3}, Lcom/mocana/vpn/android/velo/IEnterpriseMocanaVpnService$Stub;->setIKEVersion(Ljava/lang/String;II)Z

    move-result v5

    .line 472
    .local v5, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 473
    if-eqz v5, :cond_15

    move v8, v9

    :cond_15
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 478
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":I
    .end local v3    # "_arg2":I
    .end local v5    # "_result":Z
    :sswitch_20
    const-string v8, "com.mocana.vpn.android.velo.IEnterpriseMocanaVpnService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 480
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 482
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 483
    .restart local v1    # "_arg1":I
    invoke-virtual {p0, v0, v1}, Lcom/mocana/vpn/android/velo/IEnterpriseMocanaVpnService$Stub;->getIKEVersion(Ljava/lang/String;I)I

    move-result v5

    .line 484
    .local v5, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 485
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 490
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":I
    .end local v5    # "_result":I
    :sswitch_21
    const-string v8, "com.mocana.vpn.android.velo.IEnterpriseMocanaVpnService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 492
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 494
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 495
    .restart local v1    # "_arg1":I
    invoke-virtual {p0, v0, v1}, Lcom/mocana/vpn/android/velo/IEnterpriseMocanaVpnService$Stub;->getState(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v5

    .line 496
    .local v5, "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 497
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 502
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":I
    .end local v5    # "_result":Ljava/lang/String;
    :sswitch_22
    const-string v8, "com.mocana.vpn.android.velo.IEnterpriseMocanaVpnService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 504
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 506
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 507
    .restart local v1    # "_arg1":I
    invoke-virtual {p0, v0, v1}, Lcom/mocana/vpn/android/velo/IEnterpriseMocanaVpnService$Stub;->getErrorString(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v5

    .line 508
    .restart local v5    # "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 509
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 514
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":I
    .end local v5    # "_result":Ljava/lang/String;
    :sswitch_23
    const-string v8, "com.mocana.vpn.android.velo.IEnterpriseMocanaVpnService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 516
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 518
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 519
    .restart local v1    # "_arg1":I
    invoke-virtual {p0, v0, v1}, Lcom/mocana/vpn/android/velo/IEnterpriseMocanaVpnService$Stub;->isAdminProfile(Ljava/lang/String;I)I

    move-result v5

    .line 520
    .local v5, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 521
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 526
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":I
    .end local v5    # "_result":I
    :sswitch_24
    const-string v10, "com.mocana.vpn.android.velo.IEnterpriseMocanaVpnService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 528
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 530
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v2

    .line 532
    .local v2, "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 533
    .restart local v3    # "_arg2":I
    invoke-virtual {p0, v0, v2, v3}, Lcom/mocana/vpn/android/velo/IEnterpriseMocanaVpnService$Stub;->setForwardRoutes(Ljava/lang/String;Ljava/util/List;I)Z

    move-result v5

    .line 534
    .local v5, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 535
    if-eqz v5, :cond_16

    move v8, v9

    :cond_16
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 540
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v3    # "_arg2":I
    .end local v5    # "_result":Z
    :sswitch_25
    const-string v8, "com.mocana.vpn.android.velo.IEnterpriseMocanaVpnService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 542
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 544
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 545
    .restart local v1    # "_arg1":I
    invoke-virtual {p0, v0, v1}, Lcom/mocana/vpn/android/velo/IEnterpriseMocanaVpnService$Stub;->getForwardRoutes(Ljava/lang/String;I)Ljava/util/List;

    move-result-object v7

    .line 546
    .local v7, "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 547
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    goto/16 :goto_0

    .line 552
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":I
    .end local v7    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :sswitch_26
    const-string v10, "com.mocana.vpn.android.velo.IEnterpriseMocanaVpnService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 554
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 556
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 558
    .restart local v1    # "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 559
    .restart local v3    # "_arg2":I
    invoke-virtual {p0, v0, v1, v3}, Lcom/mocana/vpn/android/velo/IEnterpriseMocanaVpnService$Stub;->setSplitTunnelType(Ljava/lang/String;II)Z

    move-result v5

    .line 560
    .restart local v5    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 561
    if-eqz v5, :cond_17

    move v8, v9

    :cond_17
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 566
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":I
    .end local v3    # "_arg2":I
    .end local v5    # "_result":Z
    :sswitch_27
    const-string v8, "com.mocana.vpn.android.velo.IEnterpriseMocanaVpnService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 568
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 570
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 571
    .restart local v1    # "_arg1":I
    invoke-virtual {p0, v0, v1}, Lcom/mocana/vpn/android/velo/IEnterpriseMocanaVpnService$Stub;->getSplitTunnelType(Ljava/lang/String;I)I

    move-result v5

    .line 572
    .local v5, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 573
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 578
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":I
    .end local v5    # "_result":I
    :sswitch_28
    const-string v10, "com.mocana.vpn.android.velo.IEnterpriseMocanaVpnService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 580
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 582
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 584
    .local v1, "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 585
    .restart local v3    # "_arg2":I
    invoke-virtual {p0, v0, v1, v3}, Lcom/mocana/vpn/android/velo/IEnterpriseMocanaVpnService$Stub;->setBackupServerName(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v5

    .line 586
    .local v5, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 587
    if-eqz v5, :cond_18

    move v8, v9

    :cond_18
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 592
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":I
    .end local v5    # "_result":Z
    :sswitch_29
    const-string v8, "com.mocana.vpn.android.velo.IEnterpriseMocanaVpnService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 594
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 596
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 597
    .local v1, "_arg1":I
    invoke-virtual {p0, v0, v1}, Lcom/mocana/vpn/android/velo/IEnterpriseMocanaVpnService$Stub;->getBackupServerName(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v5

    .line 598
    .local v5, "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 599
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 604
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":I
    .end local v5    # "_result":Ljava/lang/String;
    :sswitch_2a
    const-string v10, "com.mocana.vpn.android.velo.IEnterpriseMocanaVpnService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 606
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 608
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_1a

    move v1, v9

    .line 610
    .local v1, "_arg1":Z
    :goto_3
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 611
    .restart local v3    # "_arg2":I
    invoke-virtual {p0, v0, v1, v3}, Lcom/mocana/vpn/android/velo/IEnterpriseMocanaVpnService$Stub;->setDeadPeerDetection(Ljava/lang/String;ZI)Z

    move-result v5

    .line 612
    .local v5, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 613
    if-eqz v5, :cond_19

    move v8, v9

    :cond_19
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .end local v1    # "_arg1":Z
    .end local v3    # "_arg2":I
    .end local v5    # "_result":Z
    :cond_1a
    move v1, v8

    .line 608
    goto :goto_3

    .line 618
    .end local v0    # "_arg0":Ljava/lang/String;
    :sswitch_2b
    const-string v8, "com.mocana.vpn.android.velo.IEnterpriseMocanaVpnService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 620
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 622
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 623
    .local v1, "_arg1":I
    invoke-virtual {p0, v0, v1}, Lcom/mocana/vpn/android/velo/IEnterpriseMocanaVpnService$Stub;->getDeadPeerDetection(Ljava/lang/String;I)I

    move-result v5

    .line 624
    .local v5, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 625
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 630
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":I
    .end local v5    # "_result":I
    :sswitch_2c
    const-string v10, "com.mocana.vpn.android.velo.IEnterpriseMocanaVpnService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 632
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 634
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_1c

    move v1, v9

    .line 636
    .local v1, "_arg1":Z
    :goto_4
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 637
    .restart local v3    # "_arg2":I
    invoke-virtual {p0, v0, v1, v3}, Lcom/mocana/vpn/android/velo/IEnterpriseMocanaVpnService$Stub;->setMobike(Ljava/lang/String;ZI)Z

    move-result v5

    .line 638
    .local v5, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 639
    if-eqz v5, :cond_1b

    move v8, v9

    :cond_1b
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .end local v1    # "_arg1":Z
    .end local v3    # "_arg2":I
    .end local v5    # "_result":Z
    :cond_1c
    move v1, v8

    .line 634
    goto :goto_4

    .line 644
    .end local v0    # "_arg0":Ljava/lang/String;
    :sswitch_2d
    const-string v8, "com.mocana.vpn.android.velo.IEnterpriseMocanaVpnService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 646
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 648
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 649
    .local v1, "_arg1":I
    invoke-virtual {p0, v0, v1}, Lcom/mocana/vpn/android/velo/IEnterpriseMocanaVpnService$Stub;->getMobike(Ljava/lang/String;I)I

    move-result v5

    .line 650
    .local v5, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 651
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 656
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":I
    .end local v5    # "_result":I
    :sswitch_2e
    const-string v10, "com.mocana.vpn.android.velo.IEnterpriseMocanaVpnService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 658
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 660
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 662
    .restart local v1    # "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 663
    .restart local v3    # "_arg2":I
    invoke-virtual {p0, v0, v1, v3}, Lcom/mocana/vpn/android/velo/IEnterpriseMocanaVpnService$Stub;->setSuiteBType(Ljava/lang/String;II)Z

    move-result v5

    .line 664
    .local v5, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 665
    if-eqz v5, :cond_1d

    move v8, v9

    :cond_1d
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 670
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":I
    .end local v3    # "_arg2":I
    .end local v5    # "_result":Z
    :sswitch_2f
    const-string v8, "com.mocana.vpn.android.velo.IEnterpriseMocanaVpnService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 672
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 674
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 675
    .restart local v1    # "_arg1":I
    invoke-virtual {p0, v0, v1}, Lcom/mocana/vpn/android/velo/IEnterpriseMocanaVpnService$Stub;->getSuiteBType(Ljava/lang/String;I)I

    move-result v5

    .line 676
    .local v5, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 677
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 682
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":I
    .end local v5    # "_result":I
    :sswitch_30
    const-string v10, "com.mocana.vpn.android.velo.IEnterpriseMocanaVpnService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 684
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 686
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_1f

    move v1, v9

    .line 688
    .local v1, "_arg1":Z
    :goto_5
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 689
    .restart local v3    # "_arg2":I
    invoke-virtual {p0, v0, v1, v3}, Lcom/mocana/vpn/android/velo/IEnterpriseMocanaVpnService$Stub;->enableDefaultRoute(Ljava/lang/String;ZI)Z

    move-result v5

    .line 690
    .local v5, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 691
    if-eqz v5, :cond_1e

    move v8, v9

    :cond_1e
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .end local v1    # "_arg1":Z
    .end local v3    # "_arg2":I
    .end local v5    # "_result":Z
    :cond_1f
    move v1, v8

    .line 686
    goto :goto_5

    .line 696
    .end local v0    # "_arg0":Ljava/lang/String;
    :sswitch_31
    const-string v8, "com.mocana.vpn.android.velo.IEnterpriseMocanaVpnService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 698
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 700
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 701
    .local v1, "_arg1":I
    invoke-virtual {p0, v0, v1}, Lcom/mocana/vpn/android/velo/IEnterpriseMocanaVpnService$Stub;->isDefaultRouteEnabled(Ljava/lang/String;I)I

    move-result v5

    .line 702
    .local v5, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 703
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 708
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":I
    .end local v5    # "_result":I
    :sswitch_32
    const-string v10, "com.mocana.vpn.android.velo.IEnterpriseMocanaVpnService"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 710
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 712
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_21

    move v1, v9

    .line 714
    .local v1, "_arg1":Z
    :goto_6
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 715
    .restart local v3    # "_arg2":I
    invoke-virtual {p0, v0, v1, v3}, Lcom/mocana/vpn/android/velo/IEnterpriseMocanaVpnService$Stub;->setUserAuth(Ljava/lang/String;ZI)Z

    move-result v5

    .line 716
    .local v5, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 717
    if-eqz v5, :cond_20

    move v8, v9

    :cond_20
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .end local v1    # "_arg1":Z
    .end local v3    # "_arg2":I
    .end local v5    # "_result":Z
    :cond_21
    move v1, v8

    .line 712
    goto :goto_6

    .line 722
    .end local v0    # "_arg0":Ljava/lang/String;
    :sswitch_33
    const-string v8, "com.mocana.vpn.android.velo.IEnterpriseMocanaVpnService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 724
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 726
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 727
    .local v1, "_arg1":I
    invoke-virtual {p0, v0, v1}, Lcom/mocana/vpn/android/velo/IEnterpriseMocanaVpnService$Stub;->isUserAuthEnabled(Ljava/lang/String;I)I

    move-result v5

    .line 728
    .local v5, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 729
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 734
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":I
    .end local v5    # "_result":I
    :sswitch_34
    const-string v8, "com.mocana.vpn.android.velo.IEnterpriseMocanaVpnService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 735
    invoke-virtual {p0}, Lcom/mocana/vpn/android/velo/IEnterpriseMocanaVpnService$Stub;->switchFIPSMode()I

    move-result v5

    .line 736
    .restart local v5    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 737
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 742
    .end local v5    # "_result":I
    :sswitch_35
    const-string v8, "com.mocana.vpn.android.velo.IEnterpriseMocanaVpnService"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 743
    invoke-virtual {p0}, Lcom/mocana/vpn/android/velo/IEnterpriseMocanaVpnService$Stub;->getFIPSMode()I

    move-result v5

    .line 744
    .restart local v5    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 745
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 38
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0xe -> :sswitch_e
        0xf -> :sswitch_f
        0x10 -> :sswitch_10
        0x11 -> :sswitch_11
        0x12 -> :sswitch_12
        0x13 -> :sswitch_13
        0x14 -> :sswitch_14
        0x15 -> :sswitch_15
        0x16 -> :sswitch_16
        0x17 -> :sswitch_17
        0x18 -> :sswitch_18
        0x19 -> :sswitch_19
        0x1a -> :sswitch_1a
        0x1b -> :sswitch_1b
        0x1c -> :sswitch_1c
        0x1d -> :sswitch_1d
        0x1e -> :sswitch_1e
        0x1f -> :sswitch_1f
        0x20 -> :sswitch_20
        0x21 -> :sswitch_21
        0x22 -> :sswitch_22
        0x23 -> :sswitch_23
        0x24 -> :sswitch_24
        0x25 -> :sswitch_25
        0x26 -> :sswitch_26
        0x27 -> :sswitch_27
        0x28 -> :sswitch_28
        0x29 -> :sswitch_29
        0x2a -> :sswitch_2a
        0x2b -> :sswitch_2b
        0x2c -> :sswitch_2c
        0x2d -> :sswitch_2d
        0x2e -> :sswitch_2e
        0x2f -> :sswitch_2f
        0x30 -> :sswitch_30
        0x31 -> :sswitch_31
        0x32 -> :sswitch_32
        0x33 -> :sswitch_33
        0x34 -> :sswitch_34
        0x35 -> :sswitch_35
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
