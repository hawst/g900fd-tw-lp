.class public Lorg/simalliance/openmobileapi/Channel;
.super Ljava/lang/Object;
.source "Channel.java"


# instance fields
.field private final mChannel:Lorg/simalliance/openmobileapi/service/ISmartcardServiceChannel;

.field private final mLock:Ljava/lang/Object;

.field private final mService:Lorg/simalliance/openmobileapi/SEService;

.field private mSession:Lorg/simalliance/openmobileapi/Session;


# direct methods
.method constructor <init>(Lorg/simalliance/openmobileapi/SEService;Lorg/simalliance/openmobileapi/Session;Lorg/simalliance/openmobileapi/service/ISmartcardServiceChannel;)V
    .locals 1
    .param p1, "service"    # Lorg/simalliance/openmobileapi/SEService;
    .param p2, "session"    # Lorg/simalliance/openmobileapi/Session;
    .param p3, "channel"    # Lorg/simalliance/openmobileapi/service/ISmartcardServiceChannel;

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lorg/simalliance/openmobileapi/Channel;->mLock:Ljava/lang/Object;

    .line 55
    iput-object p1, p0, Lorg/simalliance/openmobileapi/Channel;->mService:Lorg/simalliance/openmobileapi/SEService;

    .line 56
    iput-object p2, p0, Lorg/simalliance/openmobileapi/Channel;->mSession:Lorg/simalliance/openmobileapi/Session;

    .line 57
    iput-object p3, p0, Lorg/simalliance/openmobileapi/Channel;->mChannel:Lorg/simalliance/openmobileapi/service/ISmartcardServiceChannel;

    .line 58
    return-void
.end method


# virtual methods
.method public close()V
    .locals 3

    .prologue
    .line 66
    iget-object v1, p0, Lorg/simalliance/openmobileapi/Channel;->mService:Lorg/simalliance/openmobileapi/SEService;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/simalliance/openmobileapi/Channel;->mService:Lorg/simalliance/openmobileapi/SEService;

    invoke-virtual {v1}, Lorg/simalliance/openmobileapi/SEService;->isConnected()Z

    move-result v1

    if-nez v1, :cond_1

    .line 67
    :cond_0
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "service not connected to system"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 69
    :cond_1
    iget-object v1, p0, Lorg/simalliance/openmobileapi/Channel;->mChannel:Lorg/simalliance/openmobileapi/service/ISmartcardServiceChannel;

    if-nez v1, :cond_2

    .line 70
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "channel must not be null"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 72
    :cond_2
    new-instance v0, Lorg/simalliance/openmobileapi/service/SmartcardError;

    invoke-direct {v0}, Lorg/simalliance/openmobileapi/service/SmartcardError;-><init>()V

    .line 75
    .local v0, "error":Lorg/simalliance/openmobileapi/service/SmartcardError;
    :try_start_0
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/Channel;->isClosed()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 83
    :goto_0
    return-void

    .line 79
    :cond_3
    iget-object v1, p0, Lorg/simalliance/openmobileapi/Channel;->mChannel:Lorg/simalliance/openmobileapi/service/ISmartcardServiceChannel;

    invoke-interface {v1, v0}, Lorg/simalliance/openmobileapi/service/ISmartcardServiceChannel;->close(Lorg/simalliance/openmobileapi/service/SmartcardError;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 82
    :goto_1
    invoke-static {v0}, Lorg/simalliance/openmobileapi/SEService;->checkForException(Lorg/simalliance/openmobileapi/service/SmartcardError;)V

    goto :goto_0

    .line 80
    :catch_0
    move-exception v1

    goto :goto_1
.end method

.method public getSelectResponse()[B
    .locals 5

    .prologue
    .line 216
    iget-object v3, p0, Lorg/simalliance/openmobileapi/Channel;->mService:Lorg/simalliance/openmobileapi/SEService;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lorg/simalliance/openmobileapi/Channel;->mService:Lorg/simalliance/openmobileapi/SEService;

    invoke-virtual {v3}, Lorg/simalliance/openmobileapi/SEService;->isConnected()Z

    move-result v3

    if-nez v3, :cond_1

    .line 217
    :cond_0
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "service not connected to system"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 219
    :cond_1
    iget-object v3, p0, Lorg/simalliance/openmobileapi/Channel;->mChannel:Lorg/simalliance/openmobileapi/service/ISmartcardServiceChannel;

    if-nez v3, :cond_2

    .line 220
    new-instance v3, Ljava/lang/NullPointerException;

    const-string v4, "channel must not be null"

    invoke-direct {v3, v4}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 223
    :cond_2
    :try_start_0
    iget-object v3, p0, Lorg/simalliance/openmobileapi/Channel;->mChannel:Lorg/simalliance/openmobileapi/service/ISmartcardServiceChannel;

    invoke-interface {v3}, Lorg/simalliance/openmobileapi/service/ISmartcardServiceChannel;->isClosed()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 224
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "channel is closed"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 226
    :catch_0
    move-exception v1

    .line 227
    .local v1, "e1":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/RuntimeException;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 232
    .end local v1    # "e1":Ljava/lang/Exception;
    :cond_3
    :try_start_1
    iget-object v3, p0, Lorg/simalliance/openmobileapi/Channel;->mChannel:Lorg/simalliance/openmobileapi/service/ISmartcardServiceChannel;

    invoke-interface {v3}, Lorg/simalliance/openmobileapi/service/ISmartcardServiceChannel;->getSelectResponse()[B
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v2

    .line 237
    .local v2, "response":[B
    if-eqz v2, :cond_4

    array-length v3, v2

    if-nez v3, :cond_4

    .line 238
    const/4 v2, 0x0

    .line 239
    :cond_4
    return-object v2

    .line 233
    .end local v2    # "response":[B
    :catch_1
    move-exception v0

    .line 234
    .local v0, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/RuntimeException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method public getSession()Lorg/simalliance/openmobileapi/Session;
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lorg/simalliance/openmobileapi/Channel;->mSession:Lorg/simalliance/openmobileapi/Session;

    return-object v0
.end method

.method public isBasicChannel()Z
    .locals 3

    .prologue
    .line 131
    iget-object v1, p0, Lorg/simalliance/openmobileapi/Channel;->mService:Lorg/simalliance/openmobileapi/SEService;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/simalliance/openmobileapi/Channel;->mService:Lorg/simalliance/openmobileapi/SEService;

    invoke-virtual {v1}, Lorg/simalliance/openmobileapi/SEService;->isConnected()Z

    move-result v1

    if-nez v1, :cond_1

    .line 132
    :cond_0
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "service not connected to system"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 134
    :cond_1
    iget-object v1, p0, Lorg/simalliance/openmobileapi/Channel;->mChannel:Lorg/simalliance/openmobileapi/service/ISmartcardServiceChannel;

    if-nez v1, :cond_2

    .line 135
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "channel must not be null"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 138
    :cond_2
    :try_start_0
    iget-object v1, p0, Lorg/simalliance/openmobileapi/Channel;->mChannel:Lorg/simalliance/openmobileapi/service/ISmartcardServiceChannel;

    invoke-interface {v1}, Lorg/simalliance/openmobileapi/service/ISmartcardServiceChannel;->isBasicChannel()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    return v1

    .line 139
    :catch_0
    move-exception v0

    .line 140
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public isClosed()Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 92
    const-string v3, "ro.csc.sales_code"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 93
    .local v1, "salesCode":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v3

    const-string v4, "CscFeature_NFC_EnableFelica"

    invoke-virtual {v3, v4}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "KDI"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 97
    :cond_0
    iget-object v3, p0, Lorg/simalliance/openmobileapi/Channel;->mService:Lorg/simalliance/openmobileapi/SEService;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lorg/simalliance/openmobileapi/Channel;->mService:Lorg/simalliance/openmobileapi/SEService;

    invoke-virtual {v3}, Lorg/simalliance/openmobileapi/SEService;->isConnected()Z

    move-result v3

    if-nez v3, :cond_2

    .line 119
    :cond_1
    :goto_0
    return v2

    .line 108
    :cond_2
    iget-object v3, p0, Lorg/simalliance/openmobileapi/Channel;->mChannel:Lorg/simalliance/openmobileapi/service/ISmartcardServiceChannel;

    if-eqz v3, :cond_1

    .line 115
    :try_start_0
    iget-object v3, p0, Lorg/simalliance/openmobileapi/Channel;->mChannel:Lorg/simalliance/openmobileapi/service/ISmartcardServiceChannel;

    invoke-interface {v3}, Lorg/simalliance/openmobileapi/service/ISmartcardServiceChannel;->isClosed()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    goto :goto_0

    .line 116
    :catch_0
    move-exception v0

    .line 119
    .local v0, "e":Ljava/lang/Exception;
    goto :goto_0
.end method

.method public selectNext()Z
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 259
    iget-object v4, p0, Lorg/simalliance/openmobileapi/Channel;->mService:Lorg/simalliance/openmobileapi/SEService;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lorg/simalliance/openmobileapi/Channel;->mService:Lorg/simalliance/openmobileapi/SEService;

    invoke-virtual {v4}, Lorg/simalliance/openmobileapi/SEService;->isConnected()Z

    move-result v4

    if-nez v4, :cond_1

    .line 260
    :cond_0
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "service not connected to system"

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 262
    :cond_1
    iget-object v4, p0, Lorg/simalliance/openmobileapi/Channel;->mChannel:Lorg/simalliance/openmobileapi/service/ISmartcardServiceChannel;

    if-nez v4, :cond_2

    .line 263
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "channel must not be null"

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 266
    :cond_2
    :try_start_0
    iget-object v4, p0, Lorg/simalliance/openmobileapi/Channel;->mChannel:Lorg/simalliance/openmobileapi/service/ISmartcardServiceChannel;

    invoke-interface {v4}, Lorg/simalliance/openmobileapi/service/ISmartcardServiceChannel;->isClosed()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 267
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "channel is closed"

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 269
    :catch_0
    move-exception v1

    .line 270
    .local v1, "e1":Ljava/lang/Exception;
    new-instance v4, Ljava/lang/RuntimeException;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 273
    .end local v1    # "e1":Ljava/lang/Exception;
    :cond_3
    const/4 v3, 0x0

    .line 274
    .local v3, "response":Z
    iget-object v5, p0, Lorg/simalliance/openmobileapi/Channel;->mLock:Ljava/lang/Object;

    monitor-enter v5

    .line 275
    :try_start_1
    new-instance v2, Lorg/simalliance/openmobileapi/service/SmartcardError;

    invoke-direct {v2}, Lorg/simalliance/openmobileapi/service/SmartcardError;-><init>()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 277
    .local v2, "error":Lorg/simalliance/openmobileapi/service/SmartcardError;
    :try_start_2
    iget-object v4, p0, Lorg/simalliance/openmobileapi/Channel;->mChannel:Lorg/simalliance/openmobileapi/service/ISmartcardServiceChannel;

    invoke-interface {v4, v2}, Lorg/simalliance/openmobileapi/service/ISmartcardServiceChannel;->selectNext(Lorg/simalliance/openmobileapi/service/SmartcardError;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v3

    .line 281
    :try_start_3
    invoke-static {v2}, Lorg/simalliance/openmobileapi/SEService;->checkForException(Lorg/simalliance/openmobileapi/service/SmartcardError;)V

    .line 282
    monitor-exit v5

    .line 283
    return v3

    .line 278
    :catch_1
    move-exception v0

    .line 279
    .local v0, "e":Ljava/lang/Exception;
    new-instance v4, Ljava/io/IOException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 282
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v2    # "error":Lorg/simalliance/openmobileapi/service/SmartcardError;
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v4
.end method

.method public transmit([B)[B
    .locals 6
    .param p1, "command"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 176
    iget-object v3, p0, Lorg/simalliance/openmobileapi/Channel;->mService:Lorg/simalliance/openmobileapi/SEService;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lorg/simalliance/openmobileapi/Channel;->mService:Lorg/simalliance/openmobileapi/SEService;

    invoke-virtual {v3}, Lorg/simalliance/openmobileapi/SEService;->isConnected()Z

    move-result v3

    if-nez v3, :cond_1

    .line 177
    :cond_0
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "service not connected to system"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 179
    :cond_1
    iget-object v3, p0, Lorg/simalliance/openmobileapi/Channel;->mChannel:Lorg/simalliance/openmobileapi/service/ISmartcardServiceChannel;

    if-nez v3, :cond_2

    .line 180
    new-instance v3, Ljava/lang/NullPointerException;

    const-string v4, "channel must not be null"

    invoke-direct {v3, v4}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 184
    :cond_2
    iget-object v4, p0, Lorg/simalliance/openmobileapi/Channel;->mLock:Ljava/lang/Object;

    monitor-enter v4

    .line 185
    :try_start_0
    new-instance v1, Lorg/simalliance/openmobileapi/service/SmartcardError;

    invoke-direct {v1}, Lorg/simalliance/openmobileapi/service/SmartcardError;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 187
    .local v1, "error":Lorg/simalliance/openmobileapi/service/SmartcardError;
    :try_start_1
    iget-object v3, p0, Lorg/simalliance/openmobileapi/Channel;->mChannel:Lorg/simalliance/openmobileapi/service/ISmartcardServiceChannel;

    invoke-interface {v3, p1, v1}, Lorg/simalliance/openmobileapi/service/ISmartcardServiceChannel;->transmit([BLorg/simalliance/openmobileapi/service/SmartcardError;)[B
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    .line 191
    .local v2, "response":[B
    :try_start_2
    invoke-static {v1}, Lorg/simalliance/openmobileapi/SEService;->checkForException(Lorg/simalliance/openmobileapi/service/SmartcardError;)V

    .line 192
    monitor-exit v4

    .line 193
    return-object v2

    .line 188
    .end local v2    # "response":[B
    :catch_0
    move-exception v0

    .line 189
    .local v0, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/io/IOException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 192
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "error":Lorg/simalliance/openmobileapi/service/SmartcardError;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3
.end method
