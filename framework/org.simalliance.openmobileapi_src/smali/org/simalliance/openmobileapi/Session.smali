.class public Lorg/simalliance/openmobileapi/Session;
.super Ljava/lang/Object;
.source "Session.java"


# instance fields
.field private final mLock:Ljava/lang/Object;

.field private final mReader:Lorg/simalliance/openmobileapi/Reader;

.field private final mService:Lorg/simalliance/openmobileapi/SEService;

.field private final mSession:Lorg/simalliance/openmobileapi/service/ISmartcardServiceSession;

.field final salesCode:Ljava/lang/String;


# direct methods
.method constructor <init>(Lorg/simalliance/openmobileapi/SEService;Lorg/simalliance/openmobileapi/service/ISmartcardServiceSession;Lorg/simalliance/openmobileapi/Reader;)V
    .locals 1
    .param p1, "service"    # Lorg/simalliance/openmobileapi/SEService;
    .param p2, "session"    # Lorg/simalliance/openmobileapi/service/ISmartcardServiceSession;
    .param p3, "reader"    # Lorg/simalliance/openmobileapi/Reader;

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lorg/simalliance/openmobileapi/Session;->mLock:Ljava/lang/Object;

    .line 52
    const-string v0, "ro.csc.sales_code"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/simalliance/openmobileapi/Session;->salesCode:Ljava/lang/String;

    .line 55
    iput-object p1, p0, Lorg/simalliance/openmobileapi/Session;->mService:Lorg/simalliance/openmobileapi/SEService;

    .line 56
    iput-object p3, p0, Lorg/simalliance/openmobileapi/Session;->mReader:Lorg/simalliance/openmobileapi/Reader;

    .line 57
    iput-object p2, p0, Lorg/simalliance/openmobileapi/Session;->mSession:Lorg/simalliance/openmobileapi/service/ISmartcardServiceSession;

    .line 58
    return-void
.end method

.method private basicChannelInUse(Lorg/simalliance/openmobileapi/service/SmartcardError;)Z
    .locals 3
    .param p1, "error"    # Lorg/simalliance/openmobileapi/service/SmartcardError;

    .prologue
    .line 347
    invoke-virtual {p1}, Lorg/simalliance/openmobileapi/service/SmartcardError;->createException()Ljava/lang/Exception;

    move-result-object v0

    .line 348
    .local v0, "exp":Ljava/lang/Exception;
    if-eqz v0, :cond_0

    .line 349
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    .line 350
    .local v1, "msg":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 351
    const-string v2, "basic channel in use"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 352
    const/4 v2, 0x1

    .line 356
    .end local v1    # "msg":Ljava/lang/String;
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private channelCannotBeEstablished(Lorg/simalliance/openmobileapi/service/SmartcardError;)Z
    .locals 4
    .param p1, "error"    # Lorg/simalliance/openmobileapi/service/SmartcardError;

    .prologue
    const/4 v2, 0x1

    .line 360
    invoke-virtual {p1}, Lorg/simalliance/openmobileapi/service/SmartcardError;->createException()Ljava/lang/Exception;

    move-result-object v0

    .line 361
    .local v0, "exp":Ljava/lang/Exception;
    if-eqz v0, :cond_2

    .line 362
    instance-of v3, v0, Ljava/util/MissingResourceException;

    if-eqz v3, :cond_1

    .line 381
    :cond_0
    :goto_0
    return v2

    .line 365
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    .line 366
    .local v1, "msg":Ljava/lang/String;
    if-eqz v1, :cond_2

    .line 367
    const-string v3, "channel in use"

    invoke-virtual {v1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 370
    const-string v3, "open channel failed"

    invoke-virtual {v1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 373
    const-string v3, "out of channels"

    invoke-virtual {v1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 376
    const-string v3, "MANAGE CHANNEL"

    invoke-virtual {v1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 381
    .end local v1    # "msg":Ljava/lang/String;
    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private checkIfAppletAvailable(Lorg/simalliance/openmobileapi/service/SmartcardError;)V
    .locals 3
    .param p1, "error"    # Lorg/simalliance/openmobileapi/service/SmartcardError;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/util/NoSuchElementException;
        }
    .end annotation

    .prologue
    .line 385
    invoke-virtual {p1}, Lorg/simalliance/openmobileapi/service/SmartcardError;->createException()Ljava/lang/Exception;

    move-result-object v0

    .line 386
    .local v0, "exp":Ljava/lang/Exception;
    if-eqz v0, :cond_0

    .line 387
    instance-of v1, v0, Ljava/util/NoSuchElementException;

    if-eqz v1, :cond_0

    .line 388
    new-instance v1, Ljava/util/NoSuchElementException;

    const-string v2, "Applet with the defined aid does not exist in the SE"

    invoke-direct {v1, v2}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 391
    :cond_0
    return-void
.end method

.method private isDefaultApplicationSelected(Lorg/simalliance/openmobileapi/service/SmartcardError;)Z
    .locals 3
    .param p1, "error"    # Lorg/simalliance/openmobileapi/service/SmartcardError;

    .prologue
    .line 334
    invoke-virtual {p1}, Lorg/simalliance/openmobileapi/service/SmartcardError;->createException()Ljava/lang/Exception;

    move-result-object v0

    .line 335
    .local v0, "exp":Ljava/lang/Exception;
    if-eqz v0, :cond_0

    .line 336
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    .line 337
    .local v1, "msg":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 338
    const-string v2, "default application is not selected"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 339
    const/4 v2, 0x0

    .line 343
    .end local v1    # "msg":Ljava/lang/String;
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x1

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 5

    .prologue
    .line 103
    iget-object v2, p0, Lorg/simalliance/openmobileapi/Session;->mService:Lorg/simalliance/openmobileapi/SEService;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lorg/simalliance/openmobileapi/Session;->mService:Lorg/simalliance/openmobileapi/SEService;

    invoke-virtual {v2}, Lorg/simalliance/openmobileapi/SEService;->isConnected()Z

    move-result v2

    if-nez v2, :cond_0

    .line 106
    :cond_0
    iget-object v2, p0, Lorg/simalliance/openmobileapi/Session;->mSession:Lorg/simalliance/openmobileapi/service/ISmartcardServiceSession;

    if-eqz v2, :cond_1

    .line 107
    iget-object v3, p0, Lorg/simalliance/openmobileapi/Session;->mLock:Ljava/lang/Object;

    monitor-enter v3

    .line 108
    :try_start_0
    new-instance v1, Lorg/simalliance/openmobileapi/service/SmartcardError;

    invoke-direct {v1}, Lorg/simalliance/openmobileapi/service/SmartcardError;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 110
    .local v1, "error":Lorg/simalliance/openmobileapi/service/SmartcardError;
    :try_start_1
    iget-object v2, p0, Lorg/simalliance/openmobileapi/Session;->mSession:Lorg/simalliance/openmobileapi/service/ISmartcardServiceSession;

    invoke-interface {v2, v1}, Lorg/simalliance/openmobileapi/service/ISmartcardServiceSession;->close(Lorg/simalliance/openmobileapi/service/SmartcardError;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 114
    :try_start_2
    invoke-static {v1}, Lorg/simalliance/openmobileapi/SEService;->checkForException(Lorg/simalliance/openmobileapi/service/SmartcardError;)V

    .line 115
    monitor-exit v3

    .line 117
    .end local v1    # "error":Lorg/simalliance/openmobileapi/service/SmartcardError;
    :cond_1
    return-void

    .line 111
    .restart local v1    # "error":Lorg/simalliance/openmobileapi/service/SmartcardError;
    :catch_0
    move-exception v0

    .line 112
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 115
    .end local v0    # "e":Landroid/os/RemoteException;
    .end local v1    # "error":Lorg/simalliance/openmobileapi/service/SmartcardError;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2
.end method

.method public closeChannels()V
    .locals 5

    .prologue
    .line 141
    iget-object v2, p0, Lorg/simalliance/openmobileapi/Session;->mService:Lorg/simalliance/openmobileapi/SEService;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lorg/simalliance/openmobileapi/Session;->mService:Lorg/simalliance/openmobileapi/SEService;

    invoke-virtual {v2}, Lorg/simalliance/openmobileapi/SEService;->isConnected()Z

    move-result v2

    if-nez v2, :cond_0

    .line 145
    :cond_0
    iget-object v2, p0, Lorg/simalliance/openmobileapi/Session;->mSession:Lorg/simalliance/openmobileapi/service/ISmartcardServiceSession;

    if-eqz v2, :cond_1

    .line 146
    iget-object v3, p0, Lorg/simalliance/openmobileapi/Session;->mLock:Ljava/lang/Object;

    monitor-enter v3

    .line 147
    :try_start_0
    new-instance v1, Lorg/simalliance/openmobileapi/service/SmartcardError;

    invoke-direct {v1}, Lorg/simalliance/openmobileapi/service/SmartcardError;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 149
    .local v1, "error":Lorg/simalliance/openmobileapi/service/SmartcardError;
    :try_start_1
    iget-object v2, p0, Lorg/simalliance/openmobileapi/Session;->mSession:Lorg/simalliance/openmobileapi/service/ISmartcardServiceSession;

    invoke-interface {v2, v1}, Lorg/simalliance/openmobileapi/service/ISmartcardServiceSession;->closeChannels(Lorg/simalliance/openmobileapi/service/SmartcardError;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 153
    :try_start_2
    invoke-static {v1}, Lorg/simalliance/openmobileapi/SEService;->checkForException(Lorg/simalliance/openmobileapi/service/SmartcardError;)V

    .line 154
    monitor-exit v3

    .line 156
    .end local v1    # "error":Lorg/simalliance/openmobileapi/service/SmartcardError;
    :cond_1
    return-void

    .line 150
    .restart local v1    # "error":Lorg/simalliance/openmobileapi/service/SmartcardError;
    :catch_0
    move-exception v0

    .line 151
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 154
    .end local v0    # "e":Landroid/os/RemoteException;
    .end local v1    # "error":Lorg/simalliance/openmobileapi/service/SmartcardError;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2
.end method

.method public getATR()[B
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 77
    iget-object v2, p0, Lorg/simalliance/openmobileapi/Session;->mService:Lorg/simalliance/openmobileapi/SEService;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lorg/simalliance/openmobileapi/Session;->mService:Lorg/simalliance/openmobileapi/SEService;

    invoke-virtual {v2}, Lorg/simalliance/openmobileapi/SEService;->isConnected()Z

    move-result v2

    if-nez v2, :cond_1

    .line 78
    :cond_0
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "service not connected to system"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 80
    :cond_1
    iget-object v2, p0, Lorg/simalliance/openmobileapi/Session;->mSession:Lorg/simalliance/openmobileapi/service/ISmartcardServiceSession;

    if-nez v2, :cond_2

    .line 81
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "service session is null"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 86
    :cond_2
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v2

    const-string v3, "CscFeature_NFC_EnableFelica"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "KDI"

    iget-object v3, p0, Lorg/simalliance/openmobileapi/Session;->salesCode:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 94
    :goto_0
    return-object v1

    .line 92
    :cond_3
    iget-object v2, p0, Lorg/simalliance/openmobileapi/Session;->mSession:Lorg/simalliance/openmobileapi/service/ISmartcardServiceSession;

    invoke-interface {v2}, Lorg/simalliance/openmobileapi/service/ISmartcardServiceSession;->getAtr()[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 93
    :catch_0
    move-exception v0

    .line 94
    .local v0, "e":Ljava/lang/Exception;
    goto :goto_0
.end method

.method public getReader()Lorg/simalliance/openmobileapi/Reader;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lorg/simalliance/openmobileapi/Session;->mReader:Lorg/simalliance/openmobileapi/Reader;

    return-object v0
.end method

.method public isClosed()Z
    .locals 3

    .prologue
    .line 126
    :try_start_0
    iget-object v1, p0, Lorg/simalliance/openmobileapi/Session;->mSession:Lorg/simalliance/openmobileapi/service/ISmartcardServiceSession;

    if-nez v1, :cond_0

    .line 127
    const/4 v1, 0x1

    .line 129
    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lorg/simalliance/openmobileapi/Session;->mSession:Lorg/simalliance/openmobileapi/service/ISmartcardServiceSession;

    invoke-interface {v1}, Lorg/simalliance/openmobileapi/service/ISmartcardServiceSession;->isClosed()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 130
    :catch_0
    move-exception v0

    .line 131
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public openBasicChannel([B)Lorg/simalliance/openmobileapi/Channel;
    .locals 8
    .param p1, "aid"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 197
    iget-object v5, p0, Lorg/simalliance/openmobileapi/Session;->mService:Lorg/simalliance/openmobileapi/SEService;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lorg/simalliance/openmobileapi/Session;->mService:Lorg/simalliance/openmobileapi/SEService;

    invoke-virtual {v5}, Lorg/simalliance/openmobileapi/SEService;->isConnected()Z

    move-result v5

    if-nez v5, :cond_1

    .line 198
    :cond_0
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "service not connected to system"

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 200
    :cond_1
    iget-object v5, p0, Lorg/simalliance/openmobileapi/Session;->mSession:Lorg/simalliance/openmobileapi/service/ISmartcardServiceSession;

    if-nez v5, :cond_2

    .line 201
    new-instance v4, Ljava/lang/NullPointerException;

    const-string v5, "service session is null"

    invoke-direct {v4, v5}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 203
    :cond_2
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/Session;->getReader()Lorg/simalliance/openmobileapi/Reader;

    move-result-object v5

    if-nez v5, :cond_3

    .line 204
    new-instance v4, Ljava/lang/NullPointerException;

    const-string v5, "reader must not be null"

    invoke-direct {v4, v5}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 209
    :cond_3
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v5

    const-string v6, "CscFeature_NFC_EnableFelica"

    invoke-virtual {v5, v6}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    const-string v5, "KDI"

    iget-object v6, p0, Lorg/simalliance/openmobileapi/Session;->salesCode:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 256
    :goto_0
    return-object v4

    .line 216
    :cond_4
    iget-object v5, p0, Lorg/simalliance/openmobileapi/Session;->mLock:Ljava/lang/Object;

    monitor-enter v5

    .line 218
    :try_start_0
    new-instance v3, Lorg/simalliance/openmobileapi/service/SmartcardError;

    invoke-direct {v3}, Lorg/simalliance/openmobileapi/service/SmartcardError;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 220
    .local v3, "error":Lorg/simalliance/openmobileapi/service/SmartcardError;
    :try_start_1
    iget-object v6, p0, Lorg/simalliance/openmobileapi/Session;->mSession:Lorg/simalliance/openmobileapi/service/ISmartcardServiceSession;

    iget-object v7, p0, Lorg/simalliance/openmobileapi/Session;->mService:Lorg/simalliance/openmobileapi/SEService;

    invoke-virtual {v7}, Lorg/simalliance/openmobileapi/SEService;->getCallback()Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;

    move-result-object v7

    invoke-interface {v6, p1, v7, v3}, Lorg/simalliance/openmobileapi/service/ISmartcardServiceSession;->openBasicChannelAid([BLorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;Lorg/simalliance/openmobileapi/service/SmartcardError;)Lorg/simalliance/openmobileapi/service/ISmartcardServiceChannel;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    .line 227
    .local v1, "channel":Lorg/simalliance/openmobileapi/service/ISmartcardServiceChannel;
    :try_start_2
    invoke-static {v3}, Lorg/simalliance/openmobileapi/SEService;->checkForException(Lorg/simalliance/openmobileapi/service/SmartcardError;)V

    .line 228
    invoke-virtual {v3}, Lorg/simalliance/openmobileapi/service/SmartcardError;->clear()V

    .line 229
    invoke-direct {p0, v3}, Lorg/simalliance/openmobileapi/Session;->basicChannelInUse(Lorg/simalliance/openmobileapi/service/SmartcardError;)Z

    move-result v0

    .line 230
    .local v0, "b":Z
    invoke-static {v3}, Lorg/simalliance/openmobileapi/SEService;->checkForException(Lorg/simalliance/openmobileapi/service/SmartcardError;)V

    .line 231
    if-eqz v0, :cond_5

    .line 232
    monitor-exit v5

    goto :goto_0

    .line 257
    .end local v0    # "b":Z
    .end local v1    # "channel":Lorg/simalliance/openmobileapi/service/ISmartcardServiceChannel;
    .end local v3    # "error":Lorg/simalliance/openmobileapi/service/SmartcardError;
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v4

    .line 224
    .restart local v3    # "error":Lorg/simalliance/openmobileapi/service/SmartcardError;
    :catch_0
    move-exception v2

    .line 225
    .local v2, "e":Ljava/lang/Exception;
    :try_start_3
    new-instance v4, Ljava/io/IOException;

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 234
    .end local v2    # "e":Ljava/lang/Exception;
    .restart local v0    # "b":Z
    .restart local v1    # "channel":Lorg/simalliance/openmobileapi/service/ISmartcardServiceChannel;
    :cond_5
    invoke-virtual {v3}, Lorg/simalliance/openmobileapi/service/SmartcardError;->clear()V

    .line 235
    invoke-direct {p0, v3}, Lorg/simalliance/openmobileapi/Session;->channelCannotBeEstablished(Lorg/simalliance/openmobileapi/service/SmartcardError;)Z

    move-result v0

    .line 236
    invoke-static {v3}, Lorg/simalliance/openmobileapi/SEService;->checkForException(Lorg/simalliance/openmobileapi/service/SmartcardError;)V

    .line 237
    if-eqz v0, :cond_6

    .line 238
    monitor-exit v5

    goto :goto_0

    .line 240
    :cond_6
    if-eqz p1, :cond_7

    array-length v6, p1

    if-nez v6, :cond_8

    .line 242
    :cond_7
    invoke-virtual {v3}, Lorg/simalliance/openmobileapi/service/SmartcardError;->clear()V

    .line 243
    invoke-direct {p0, v3}, Lorg/simalliance/openmobileapi/Session;->isDefaultApplicationSelected(Lorg/simalliance/openmobileapi/service/SmartcardError;)Z

    move-result v0

    .line 244
    invoke-static {v3}, Lorg/simalliance/openmobileapi/SEService;->checkForException(Lorg/simalliance/openmobileapi/service/SmartcardError;)V

    .line 245
    if-nez v0, :cond_8

    .line 246
    monitor-exit v5

    goto :goto_0

    .line 249
    :cond_8
    invoke-virtual {v3}, Lorg/simalliance/openmobileapi/service/SmartcardError;->clear()V

    .line 250
    invoke-direct {p0, v3}, Lorg/simalliance/openmobileapi/Session;->checkIfAppletAvailable(Lorg/simalliance/openmobileapi/service/SmartcardError;)V

    .line 251
    invoke-static {v3}, Lorg/simalliance/openmobileapi/SEService;->checkForException(Lorg/simalliance/openmobileapi/service/SmartcardError;)V

    .line 253
    if-nez v1, :cond_9

    .line 254
    monitor-exit v5

    goto :goto_0

    .line 256
    :cond_9
    new-instance v4, Lorg/simalliance/openmobileapi/Channel;

    iget-object v6, p0, Lorg/simalliance/openmobileapi/Session;->mService:Lorg/simalliance/openmobileapi/SEService;

    invoke-direct {v4, v6, p0, v1}, Lorg/simalliance/openmobileapi/Channel;-><init>(Lorg/simalliance/openmobileapi/SEService;Lorg/simalliance/openmobileapi/Session;Lorg/simalliance/openmobileapi/service/ISmartcardServiceChannel;)V

    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method public openLogicalChannel([B)Lorg/simalliance/openmobileapi/Channel;
    .locals 8
    .param p1, "aid"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 289
    iget-object v5, p0, Lorg/simalliance/openmobileapi/Session;->mService:Lorg/simalliance/openmobileapi/SEService;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lorg/simalliance/openmobileapi/Session;->mService:Lorg/simalliance/openmobileapi/SEService;

    invoke-virtual {v5}, Lorg/simalliance/openmobileapi/SEService;->isConnected()Z

    move-result v5

    if-nez v5, :cond_1

    .line 290
    :cond_0
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "service not connected to system"

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 292
    :cond_1
    iget-object v5, p0, Lorg/simalliance/openmobileapi/Session;->mSession:Lorg/simalliance/openmobileapi/service/ISmartcardServiceSession;

    if-nez v5, :cond_2

    .line 293
    new-instance v4, Ljava/lang/NullPointerException;

    const-string v5, "service session is null"

    invoke-direct {v4, v5}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 295
    :cond_2
    invoke-virtual {p0}, Lorg/simalliance/openmobileapi/Session;->getReader()Lorg/simalliance/openmobileapi/Reader;

    move-result-object v5

    if-nez v5, :cond_3

    .line 296
    new-instance v4, Ljava/lang/NullPointerException;

    const-string v5, "reader must not be null"

    invoke-direct {v4, v5}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 298
    :cond_3
    iget-object v5, p0, Lorg/simalliance/openmobileapi/Session;->mLock:Ljava/lang/Object;

    monitor-enter v5

    .line 299
    :try_start_0
    new-instance v3, Lorg/simalliance/openmobileapi/service/SmartcardError;

    invoke-direct {v3}, Lorg/simalliance/openmobileapi/service/SmartcardError;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 302
    .local v3, "error":Lorg/simalliance/openmobileapi/service/SmartcardError;
    :try_start_1
    iget-object v6, p0, Lorg/simalliance/openmobileapi/Session;->mSession:Lorg/simalliance/openmobileapi/service/ISmartcardServiceSession;

    iget-object v7, p0, Lorg/simalliance/openmobileapi/Session;->mService:Lorg/simalliance/openmobileapi/SEService;

    invoke-virtual {v7}, Lorg/simalliance/openmobileapi/SEService;->getCallback()Lorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;

    move-result-object v7

    invoke-interface {v6, p1, v7, v3}, Lorg/simalliance/openmobileapi/service/ISmartcardServiceSession;->openLogicalChannel([BLorg/simalliance/openmobileapi/service/ISmartcardServiceCallback;Lorg/simalliance/openmobileapi/service/SmartcardError;)Lorg/simalliance/openmobileapi/service/ISmartcardServiceChannel;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    .line 309
    .local v1, "channel":Lorg/simalliance/openmobileapi/service/ISmartcardServiceChannel;
    :try_start_2
    invoke-static {v3}, Lorg/simalliance/openmobileapi/SEService;->checkForException(Lorg/simalliance/openmobileapi/service/SmartcardError;)V

    .line 310
    invoke-virtual {v3}, Lorg/simalliance/openmobileapi/service/SmartcardError;->clear()V

    .line 311
    invoke-direct {p0, v3}, Lorg/simalliance/openmobileapi/Session;->channelCannotBeEstablished(Lorg/simalliance/openmobileapi/service/SmartcardError;)Z

    move-result v0

    .line 312
    .local v0, "b":Z
    invoke-static {v3}, Lorg/simalliance/openmobileapi/SEService;->checkForException(Lorg/simalliance/openmobileapi/service/SmartcardError;)V

    .line 313
    if-eqz v0, :cond_4

    .line 314
    monitor-exit v5

    .line 323
    :goto_0
    return-object v4

    .line 306
    .end local v0    # "b":Z
    .end local v1    # "channel":Lorg/simalliance/openmobileapi/service/ISmartcardServiceChannel;
    :catch_0
    move-exception v2

    .line 307
    .local v2, "e":Ljava/lang/Exception;
    new-instance v4, Ljava/io/IOException;

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 324
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v3    # "error":Lorg/simalliance/openmobileapi/service/SmartcardError;
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v4

    .line 316
    .restart local v0    # "b":Z
    .restart local v1    # "channel":Lorg/simalliance/openmobileapi/service/ISmartcardServiceChannel;
    .restart local v3    # "error":Lorg/simalliance/openmobileapi/service/SmartcardError;
    :cond_4
    :try_start_3
    invoke-virtual {v3}, Lorg/simalliance/openmobileapi/service/SmartcardError;->clear()V

    .line 317
    invoke-direct {p0, v3}, Lorg/simalliance/openmobileapi/Session;->checkIfAppletAvailable(Lorg/simalliance/openmobileapi/service/SmartcardError;)V

    .line 318
    invoke-static {v3}, Lorg/simalliance/openmobileapi/SEService;->checkForException(Lorg/simalliance/openmobileapi/service/SmartcardError;)V

    .line 320
    if-nez v1, :cond_5

    .line 321
    monitor-exit v5

    goto :goto_0

    .line 323
    :cond_5
    new-instance v4, Lorg/simalliance/openmobileapi/Channel;

    iget-object v6, p0, Lorg/simalliance/openmobileapi/Session;->mService:Lorg/simalliance/openmobileapi/SEService;

    invoke-direct {v4, v6, p0, v1}, Lorg/simalliance/openmobileapi/Channel;-><init>(Lorg/simalliance/openmobileapi/SEService;Lorg/simalliance/openmobileapi/Session;Lorg/simalliance/openmobileapi/service/ISmartcardServiceChannel;)V

    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method
