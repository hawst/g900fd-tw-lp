.class public final enum Lcom/sec/ims/csh/CshErrorReason;
.super Ljava/lang/Enum;
.source "CshErrorReason.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/ims/csh/CshErrorReason;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/ims/csh/CshErrorReason;

.field public static final enum ACK_TIMED_OUT:Lcom/sec/ims/csh/CshErrorReason;

.field public static final enum ACK_TIMEOUT:Lcom/sec/ims/csh/CshErrorReason;

.field public static final enum BEARER_LOST:Lcom/sec/ims/csh/CshErrorReason;

.field public static final enum CANCELED:Lcom/sec/ims/csh/CshErrorReason;

.field public static final enum ENGINE_ERROR:Lcom/sec/ims/csh/CshErrorReason;

.field public static final enum FILE_IO:Lcom/sec/ims/csh/CshErrorReason;

.field public static final enum FORBIDDEN:Lcom/sec/ims/csh/CshErrorReason;

.field public static final enum FORMAT_NOT_SUPPORTED:Lcom/sec/ims/csh/CshErrorReason;

.field public static final enum NONE:Lcom/sec/ims/csh/CshErrorReason;

.field public static final enum NORMAL:Lcom/sec/ims/csh/CshErrorReason;

.field public static final enum NOT_REACHABLE:Lcom/sec/ims/csh/CshErrorReason;

.field public static final enum REJECTED:Lcom/sec/ims/csh/CshErrorReason;

.field public static final enum REMOTE_CONNECTION_CLOSED:Lcom/sec/ims/csh/CshErrorReason;

.field public static final enum REQUEST_TIMED_OUT:Lcom/sec/ims/csh/CshErrorReason;

.field public static final enum SUCCESS:Lcom/sec/ims/csh/CshErrorReason;

.field public static final enum TEMPORAIRLY_NOT_AVAILABLE:Lcom/sec/ims/csh/CshErrorReason;

.field public static final enum UNKNOWN:Lcom/sec/ims/csh/CshErrorReason;

.field public static final enum USER_BUSY:Lcom/sec/ims/csh/CshErrorReason;

.field public static final enum USER_NOT_FOUND:Lcom/sec/ims/csh/CshErrorReason;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 16
    new-instance v0, Lcom/sec/ims/csh/CshErrorReason;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v3}, Lcom/sec/ims/csh/CshErrorReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/csh/CshErrorReason;->SUCCESS:Lcom/sec/ims/csh/CshErrorReason;

    .line 17
    new-instance v0, Lcom/sec/ims/csh/CshErrorReason;

    const-string v1, "USER_BUSY"

    invoke-direct {v0, v1, v4}, Lcom/sec/ims/csh/CshErrorReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/csh/CshErrorReason;->USER_BUSY:Lcom/sec/ims/csh/CshErrorReason;

    .line 18
    new-instance v0, Lcom/sec/ims/csh/CshErrorReason;

    const-string v1, "TEMPORAIRLY_NOT_AVAILABLE"

    invoke-direct {v0, v1, v5}, Lcom/sec/ims/csh/CshErrorReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/csh/CshErrorReason;->TEMPORAIRLY_NOT_AVAILABLE:Lcom/sec/ims/csh/CshErrorReason;

    .line 19
    new-instance v0, Lcom/sec/ims/csh/CshErrorReason;

    const-string v1, "REQUEST_TIMED_OUT"

    invoke-direct {v0, v1, v6}, Lcom/sec/ims/csh/CshErrorReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/csh/CshErrorReason;->REQUEST_TIMED_OUT:Lcom/sec/ims/csh/CshErrorReason;

    .line 20
    new-instance v0, Lcom/sec/ims/csh/CshErrorReason;

    const-string v1, "USER_NOT_FOUND"

    invoke-direct {v0, v1, v7}, Lcom/sec/ims/csh/CshErrorReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/csh/CshErrorReason;->USER_NOT_FOUND:Lcom/sec/ims/csh/CshErrorReason;

    .line 21
    new-instance v0, Lcom/sec/ims/csh/CshErrorReason;

    const-string v1, "CANCELED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/csh/CshErrorReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/csh/CshErrorReason;->CANCELED:Lcom/sec/ims/csh/CshErrorReason;

    .line 22
    new-instance v0, Lcom/sec/ims/csh/CshErrorReason;

    const-string v1, "REJECTED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/csh/CshErrorReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/csh/CshErrorReason;->REJECTED:Lcom/sec/ims/csh/CshErrorReason;

    .line 23
    new-instance v0, Lcom/sec/ims/csh/CshErrorReason;

    const-string v1, "ENGINE_ERROR"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/csh/CshErrorReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/csh/CshErrorReason;->ENGINE_ERROR:Lcom/sec/ims/csh/CshErrorReason;

    .line 24
    new-instance v0, Lcom/sec/ims/csh/CshErrorReason;

    const-string v1, "FILE_IO"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/csh/CshErrorReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/csh/CshErrorReason;->FILE_IO:Lcom/sec/ims/csh/CshErrorReason;

    .line 25
    new-instance v0, Lcom/sec/ims/csh/CshErrorReason;

    const-string v1, "REMOTE_CONNECTION_CLOSED"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/csh/CshErrorReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/csh/CshErrorReason;->REMOTE_CONNECTION_CLOSED:Lcom/sec/ims/csh/CshErrorReason;

    .line 26
    new-instance v0, Lcom/sec/ims/csh/CshErrorReason;

    const-string v1, "FORMAT_NOT_SUPPORTED"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/csh/CshErrorReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/csh/CshErrorReason;->FORMAT_NOT_SUPPORTED:Lcom/sec/ims/csh/CshErrorReason;

    .line 27
    new-instance v0, Lcom/sec/ims/csh/CshErrorReason;

    const-string v1, "ACK_TIMEOUT"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/csh/CshErrorReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/csh/CshErrorReason;->ACK_TIMEOUT:Lcom/sec/ims/csh/CshErrorReason;

    .line 28
    new-instance v0, Lcom/sec/ims/csh/CshErrorReason;

    const-string v1, "FORBIDDEN"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/csh/CshErrorReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/csh/CshErrorReason;->FORBIDDEN:Lcom/sec/ims/csh/CshErrorReason;

    .line 29
    new-instance v0, Lcom/sec/ims/csh/CshErrorReason;

    const-string v1, "NOT_REACHABLE"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/csh/CshErrorReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/csh/CshErrorReason;->NOT_REACHABLE:Lcom/sec/ims/csh/CshErrorReason;

    .line 30
    new-instance v0, Lcom/sec/ims/csh/CshErrorReason;

    const-string v1, "BEARER_LOST"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/csh/CshErrorReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/csh/CshErrorReason;->BEARER_LOST:Lcom/sec/ims/csh/CshErrorReason;

    .line 31
    new-instance v0, Lcom/sec/ims/csh/CshErrorReason;

    const-string v1, "ACK_TIMED_OUT"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/csh/CshErrorReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/csh/CshErrorReason;->ACK_TIMED_OUT:Lcom/sec/ims/csh/CshErrorReason;

    .line 32
    new-instance v0, Lcom/sec/ims/csh/CshErrorReason;

    const-string v1, "NORMAL"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/csh/CshErrorReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/csh/CshErrorReason;->NORMAL:Lcom/sec/ims/csh/CshErrorReason;

    .line 33
    new-instance v0, Lcom/sec/ims/csh/CshErrorReason;

    const-string v1, "UNKNOWN"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/csh/CshErrorReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/csh/CshErrorReason;->UNKNOWN:Lcom/sec/ims/csh/CshErrorReason;

    .line 35
    new-instance v0, Lcom/sec/ims/csh/CshErrorReason;

    const-string v1, "NONE"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/csh/CshErrorReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/csh/CshErrorReason;->NONE:Lcom/sec/ims/csh/CshErrorReason;

    .line 15
    const/16 v0, 0x13

    new-array v0, v0, [Lcom/sec/ims/csh/CshErrorReason;

    sget-object v1, Lcom/sec/ims/csh/CshErrorReason;->SUCCESS:Lcom/sec/ims/csh/CshErrorReason;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/ims/csh/CshErrorReason;->USER_BUSY:Lcom/sec/ims/csh/CshErrorReason;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/ims/csh/CshErrorReason;->TEMPORAIRLY_NOT_AVAILABLE:Lcom/sec/ims/csh/CshErrorReason;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/ims/csh/CshErrorReason;->REQUEST_TIMED_OUT:Lcom/sec/ims/csh/CshErrorReason;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/ims/csh/CshErrorReason;->USER_NOT_FOUND:Lcom/sec/ims/csh/CshErrorReason;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/ims/csh/CshErrorReason;->CANCELED:Lcom/sec/ims/csh/CshErrorReason;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/ims/csh/CshErrorReason;->REJECTED:Lcom/sec/ims/csh/CshErrorReason;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/ims/csh/CshErrorReason;->ENGINE_ERROR:Lcom/sec/ims/csh/CshErrorReason;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/ims/csh/CshErrorReason;->FILE_IO:Lcom/sec/ims/csh/CshErrorReason;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/ims/csh/CshErrorReason;->REMOTE_CONNECTION_CLOSED:Lcom/sec/ims/csh/CshErrorReason;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/ims/csh/CshErrorReason;->FORMAT_NOT_SUPPORTED:Lcom/sec/ims/csh/CshErrorReason;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/sec/ims/csh/CshErrorReason;->ACK_TIMEOUT:Lcom/sec/ims/csh/CshErrorReason;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/sec/ims/csh/CshErrorReason;->FORBIDDEN:Lcom/sec/ims/csh/CshErrorReason;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/sec/ims/csh/CshErrorReason;->NOT_REACHABLE:Lcom/sec/ims/csh/CshErrorReason;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/sec/ims/csh/CshErrorReason;->BEARER_LOST:Lcom/sec/ims/csh/CshErrorReason;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/sec/ims/csh/CshErrorReason;->ACK_TIMED_OUT:Lcom/sec/ims/csh/CshErrorReason;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/sec/ims/csh/CshErrorReason;->NORMAL:Lcom/sec/ims/csh/CshErrorReason;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/sec/ims/csh/CshErrorReason;->UNKNOWN:Lcom/sec/ims/csh/CshErrorReason;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/sec/ims/csh/CshErrorReason;->NONE:Lcom/sec/ims/csh/CshErrorReason;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/ims/csh/CshErrorReason;->$VALUES:[Lcom/sec/ims/csh/CshErrorReason;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 15
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/ims/csh/CshErrorReason;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 15
    const-class v0, Lcom/sec/ims/csh/CshErrorReason;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/ims/csh/CshErrorReason;

    return-object v0
.end method

.method public static values()[Lcom/sec/ims/csh/CshErrorReason;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/sec/ims/csh/CshErrorReason;->$VALUES:[Lcom/sec/ims/csh/CshErrorReason;

    invoke-virtual {v0}, [Lcom/sec/ims/csh/CshErrorReason;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/ims/csh/CshErrorReason;

    return-object v0
.end method
