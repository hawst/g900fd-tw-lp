.class public final enum Lcom/sec/ims/csh/CshRejectReason;
.super Ljava/lang/Enum;
.source "CshRejectReason.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/ims/csh/CshRejectReason;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/ims/csh/CshRejectReason;

.field public static final enum DEVICE_FAILED:Lcom/sec/ims/csh/CshRejectReason;

.field public static final enum NO_ANSWER:Lcom/sec/ims/csh/CshRejectReason;

.field public static final enum USER:Lcom/sec/ims/csh/CshRejectReason;

.field public static final enum USER_BUSY:Lcom/sec/ims/csh/CshRejectReason;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 21
    new-instance v0, Lcom/sec/ims/csh/CshRejectReason;

    const-string v1, "USER"

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/csh/CshRejectReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/csh/CshRejectReason;->USER:Lcom/sec/ims/csh/CshRejectReason;

    .line 22
    new-instance v0, Lcom/sec/ims/csh/CshRejectReason;

    const-string v1, "NO_ANSWER"

    invoke-direct {v0, v1, v3}, Lcom/sec/ims/csh/CshRejectReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/csh/CshRejectReason;->NO_ANSWER:Lcom/sec/ims/csh/CshRejectReason;

    .line 23
    new-instance v0, Lcom/sec/ims/csh/CshRejectReason;

    const-string v1, "USER_BUSY"

    invoke-direct {v0, v1, v4}, Lcom/sec/ims/csh/CshRejectReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/csh/CshRejectReason;->USER_BUSY:Lcom/sec/ims/csh/CshRejectReason;

    .line 24
    new-instance v0, Lcom/sec/ims/csh/CshRejectReason;

    const-string v1, "DEVICE_FAILED"

    invoke-direct {v0, v1, v5}, Lcom/sec/ims/csh/CshRejectReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/csh/CshRejectReason;->DEVICE_FAILED:Lcom/sec/ims/csh/CshRejectReason;

    .line 20
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/ims/csh/CshRejectReason;

    sget-object v1, Lcom/sec/ims/csh/CshRejectReason;->USER:Lcom/sec/ims/csh/CshRejectReason;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/ims/csh/CshRejectReason;->NO_ANSWER:Lcom/sec/ims/csh/CshRejectReason;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/ims/csh/CshRejectReason;->USER_BUSY:Lcom/sec/ims/csh/CshRejectReason;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/ims/csh/CshRejectReason;->DEVICE_FAILED:Lcom/sec/ims/csh/CshRejectReason;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/ims/csh/CshRejectReason;->$VALUES:[Lcom/sec/ims/csh/CshRejectReason;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/ims/csh/CshRejectReason;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 20
    const-class v0, Lcom/sec/ims/csh/CshRejectReason;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/ims/csh/CshRejectReason;

    return-object v0
.end method

.method public static values()[Lcom/sec/ims/csh/CshRejectReason;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/sec/ims/csh/CshRejectReason;->$VALUES:[Lcom/sec/ims/csh/CshRejectReason;

    invoke-virtual {v0}, [Lcom/sec/ims/csh/CshRejectReason;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/ims/csh/CshRejectReason;

    return-object v0
.end method
