.class public final enum Lcom/sec/ims/csh/CshServiceStatus;
.super Ljava/lang/Enum;
.source "CshServiceStatus.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/ims/csh/CshServiceStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/ims/csh/CshServiceStatus;

.field public static final enum BIDIRECTIONAL_NOT_READY:Lcom/sec/ims/csh/CshServiceStatus;

.field public static final enum BIDIRECTIONAL_READY:Lcom/sec/ims/csh/CshServiceStatus;

.field public static final enum ONE_WAY_NOT_READY:Lcom/sec/ims/csh/CshServiceStatus;

.field public static final enum ONE_WAY_READY:Lcom/sec/ims/csh/CshServiceStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 21
    new-instance v0, Lcom/sec/ims/csh/CshServiceStatus;

    const-string v1, "ONE_WAY_READY"

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/csh/CshServiceStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/csh/CshServiceStatus;->ONE_WAY_READY:Lcom/sec/ims/csh/CshServiceStatus;

    .line 22
    new-instance v0, Lcom/sec/ims/csh/CshServiceStatus;

    const-string v1, "ONE_WAY_NOT_READY"

    invoke-direct {v0, v1, v3}, Lcom/sec/ims/csh/CshServiceStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/csh/CshServiceStatus;->ONE_WAY_NOT_READY:Lcom/sec/ims/csh/CshServiceStatus;

    .line 23
    new-instance v0, Lcom/sec/ims/csh/CshServiceStatus;

    const-string v1, "BIDIRECTIONAL_READY"

    invoke-direct {v0, v1, v4}, Lcom/sec/ims/csh/CshServiceStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/csh/CshServiceStatus;->BIDIRECTIONAL_READY:Lcom/sec/ims/csh/CshServiceStatus;

    .line 24
    new-instance v0, Lcom/sec/ims/csh/CshServiceStatus;

    const-string v1, "BIDIRECTIONAL_NOT_READY"

    invoke-direct {v0, v1, v5}, Lcom/sec/ims/csh/CshServiceStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/csh/CshServiceStatus;->BIDIRECTIONAL_NOT_READY:Lcom/sec/ims/csh/CshServiceStatus;

    .line 20
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/ims/csh/CshServiceStatus;

    sget-object v1, Lcom/sec/ims/csh/CshServiceStatus;->ONE_WAY_READY:Lcom/sec/ims/csh/CshServiceStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/ims/csh/CshServiceStatus;->ONE_WAY_NOT_READY:Lcom/sec/ims/csh/CshServiceStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/ims/csh/CshServiceStatus;->BIDIRECTIONAL_READY:Lcom/sec/ims/csh/CshServiceStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/ims/csh/CshServiceStatus;->BIDIRECTIONAL_NOT_READY:Lcom/sec/ims/csh/CshServiceStatus;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/ims/csh/CshServiceStatus;->$VALUES:[Lcom/sec/ims/csh/CshServiceStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/ims/csh/CshServiceStatus;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 20
    const-class v0, Lcom/sec/ims/csh/CshServiceStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/ims/csh/CshServiceStatus;

    return-object v0
.end method

.method public static values()[Lcom/sec/ims/csh/CshServiceStatus;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/sec/ims/csh/CshServiceStatus;->$VALUES:[Lcom/sec/ims/csh/CshServiceStatus;

    invoke-virtual {v0}, [Lcom/sec/ims/csh/CshServiceStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/ims/csh/CshServiceStatus;

    return-object v0
.end method
