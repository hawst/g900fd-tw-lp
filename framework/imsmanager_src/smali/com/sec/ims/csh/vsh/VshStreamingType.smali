.class public final enum Lcom/sec/ims/csh/vsh/VshStreamingType;
.super Ljava/lang/Enum;
.source "VshStreamingType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/ims/csh/vsh/VshStreamingType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/ims/csh/vsh/VshStreamingType;

.field public static final enum VSH_LIVE:Lcom/sec/ims/csh/vsh/VshStreamingType;

.field public static final enum VSH_RECORDED:Lcom/sec/ims/csh/vsh/VshStreamingType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 21
    new-instance v0, Lcom/sec/ims/csh/vsh/VshStreamingType;

    const-string v1, "VSH_LIVE"

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/csh/vsh/VshStreamingType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/csh/vsh/VshStreamingType;->VSH_LIVE:Lcom/sec/ims/csh/vsh/VshStreamingType;

    .line 22
    new-instance v0, Lcom/sec/ims/csh/vsh/VshStreamingType;

    const-string v1, "VSH_RECORDED"

    invoke-direct {v0, v1, v3}, Lcom/sec/ims/csh/vsh/VshStreamingType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/csh/vsh/VshStreamingType;->VSH_RECORDED:Lcom/sec/ims/csh/vsh/VshStreamingType;

    .line 20
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/ims/csh/vsh/VshStreamingType;

    sget-object v1, Lcom/sec/ims/csh/vsh/VshStreamingType;->VSH_LIVE:Lcom/sec/ims/csh/vsh/VshStreamingType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/ims/csh/vsh/VshStreamingType;->VSH_RECORDED:Lcom/sec/ims/csh/vsh/VshStreamingType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/ims/csh/vsh/VshStreamingType;->$VALUES:[Lcom/sec/ims/csh/vsh/VshStreamingType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/ims/csh/vsh/VshStreamingType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 20
    const-class v0, Lcom/sec/ims/csh/vsh/VshStreamingType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/ims/csh/vsh/VshStreamingType;

    return-object v0
.end method

.method public static values()[Lcom/sec/ims/csh/vsh/VshStreamingType;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/sec/ims/csh/vsh/VshStreamingType;->$VALUES:[Lcom/sec/ims/csh/vsh/VshStreamingType;

    invoke-virtual {v0}, [Lcom/sec/ims/csh/vsh/VshStreamingType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/ims/csh/vsh/VshStreamingType;

    return-object v0
.end method
