.class public Lcom/sec/ims/csh/vsh/VideoDisplay;
.super Ljava/lang/Object;
.source "VideoDisplay.java"

# interfaces
.implements Lcom/sec/ims/csh/vsh/IVideoDisplay;


# instance fields
.field private final mHeight:I

.field private final mOrientation:Lcom/sec/ims/csh/vsh/VshOrientation;

.field private final mWidth:I

.field private final mWindowHandle:Landroid/view/Surface;


# direct methods
.method public constructor <init>(Lcom/sec/ims/csh/vsh/VshOrientation;IILandroid/view/Surface;)V
    .locals 0
    .param p1, "orientation"    # Lcom/sec/ims/csh/vsh/VshOrientation;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "windowHandle"    # Landroid/view/Surface;

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/sec/ims/csh/vsh/VideoDisplay;->mOrientation:Lcom/sec/ims/csh/vsh/VshOrientation;

    .line 39
    iput p2, p0, Lcom/sec/ims/csh/vsh/VideoDisplay;->mWidth:I

    .line 40
    iput p3, p0, Lcom/sec/ims/csh/vsh/VideoDisplay;->mHeight:I

    .line 41
    iput-object p4, p0, Lcom/sec/ims/csh/vsh/VideoDisplay;->mWindowHandle:Landroid/view/Surface;

    .line 42
    return-void
.end method


# virtual methods
.method public getHeight()I
    .locals 1

    .prologue
    .line 65
    iget v0, p0, Lcom/sec/ims/csh/vsh/VideoDisplay;->mHeight:I

    return v0
.end method

.method public getOrientation()Lcom/sec/ims/csh/vsh/VshOrientation;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/ims/csh/vsh/VideoDisplay;->mOrientation:Lcom/sec/ims/csh/vsh/VshOrientation;

    return-object v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 57
    iget v0, p0, Lcom/sec/ims/csh/vsh/VideoDisplay;->mWidth:I

    return v0
.end method

.method public getWindowHandle()Landroid/view/Surface;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/ims/csh/vsh/VideoDisplay;->mWindowHandle:Landroid/view/Surface;

    return-object v0
.end method
