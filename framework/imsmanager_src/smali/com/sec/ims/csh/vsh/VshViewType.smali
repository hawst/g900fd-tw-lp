.class public final enum Lcom/sec/ims/csh/vsh/VshViewType;
.super Ljava/lang/Enum;
.source "VshViewType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/ims/csh/vsh/VshViewType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/ims/csh/vsh/VshViewType;

.field public static final enum LOCAL:Lcom/sec/ims/csh/vsh/VshViewType;

.field public static final enum REMOTE:Lcom/sec/ims/csh/vsh/VshViewType;


# instance fields
.field private final mType:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 21
    new-instance v0, Lcom/sec/ims/csh/vsh/VshViewType;

    const-string v1, "LOCAL"

    invoke-direct {v0, v1, v3, v2}, Lcom/sec/ims/csh/vsh/VshViewType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/ims/csh/vsh/VshViewType;->LOCAL:Lcom/sec/ims/csh/vsh/VshViewType;

    .line 22
    new-instance v0, Lcom/sec/ims/csh/vsh/VshViewType;

    const-string v1, "REMOTE"

    invoke-direct {v0, v1, v2, v4}, Lcom/sec/ims/csh/vsh/VshViewType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/ims/csh/vsh/VshViewType;->REMOTE:Lcom/sec/ims/csh/vsh/VshViewType;

    .line 20
    new-array v0, v4, [Lcom/sec/ims/csh/vsh/VshViewType;

    sget-object v1, Lcom/sec/ims/csh/vsh/VshViewType;->LOCAL:Lcom/sec/ims/csh/vsh/VshViewType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/ims/csh/vsh/VshViewType;->REMOTE:Lcom/sec/ims/csh/vsh/VshViewType;

    aput-object v1, v0, v2

    sput-object v0, Lcom/sec/ims/csh/vsh/VshViewType;->$VALUES:[Lcom/sec/ims/csh/vsh/VshViewType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "i"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 27
    iput p3, p0, Lcom/sec/ims/csh/vsh/VshViewType;->mType:I

    .line 28
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/ims/csh/vsh/VshViewType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 20
    const-class v0, Lcom/sec/ims/csh/vsh/VshViewType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/ims/csh/vsh/VshViewType;

    return-object v0
.end method

.method public static values()[Lcom/sec/ims/csh/vsh/VshViewType;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/sec/ims/csh/vsh/VshViewType;->$VALUES:[Lcom/sec/ims/csh/vsh/VshViewType;

    invoke-virtual {v0}, [Lcom/sec/ims/csh/vsh/VshViewType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/ims/csh/vsh/VshViewType;

    return-object v0
.end method


# virtual methods
.method public toInt()I
    .locals 1

    .prologue
    .line 31
    iget v0, p0, Lcom/sec/ims/csh/vsh/VshViewType;->mType:I

    return v0
.end method
