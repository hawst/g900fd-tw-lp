.class public final enum Lcom/sec/ims/csh/vsh/VshOrientation;
.super Ljava/lang/Enum;
.source "VshOrientation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/ims/csh/vsh/VshOrientation;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/ims/csh/vsh/VshOrientation;

.field public static final enum FLIPPED_LANDSCAPE:Lcom/sec/ims/csh/vsh/VshOrientation;

.field public static final enum LANDSCAPE:Lcom/sec/ims/csh/vsh/VshOrientation;

.field public static final enum PORTRAIT:Lcom/sec/ims/csh/vsh/VshOrientation;

.field public static final enum REVERSE_PORTRAIT:Lcom/sec/ims/csh/vsh/VshOrientation;


# instance fields
.field private final mAngle:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 22
    new-instance v0, Lcom/sec/ims/csh/vsh/VshOrientation;

    const-string v1, "LANDSCAPE"

    invoke-direct {v0, v1, v3, v3}, Lcom/sec/ims/csh/vsh/VshOrientation;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/ims/csh/vsh/VshOrientation;->LANDSCAPE:Lcom/sec/ims/csh/vsh/VshOrientation;

    .line 23
    new-instance v0, Lcom/sec/ims/csh/vsh/VshOrientation;

    const-string v1, "PORTRAIT"

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v4, v2}, Lcom/sec/ims/csh/vsh/VshOrientation;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/ims/csh/vsh/VshOrientation;->PORTRAIT:Lcom/sec/ims/csh/vsh/VshOrientation;

    .line 24
    new-instance v0, Lcom/sec/ims/csh/vsh/VshOrientation;

    const-string v1, "FLIPPED_LANDSCAPE"

    const/16 v2, 0xb4

    invoke-direct {v0, v1, v5, v2}, Lcom/sec/ims/csh/vsh/VshOrientation;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/ims/csh/vsh/VshOrientation;->FLIPPED_LANDSCAPE:Lcom/sec/ims/csh/vsh/VshOrientation;

    .line 25
    new-instance v0, Lcom/sec/ims/csh/vsh/VshOrientation;

    const-string v1, "REVERSE_PORTRAIT"

    const/16 v2, 0x10e

    invoke-direct {v0, v1, v6, v2}, Lcom/sec/ims/csh/vsh/VshOrientation;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/ims/csh/vsh/VshOrientation;->REVERSE_PORTRAIT:Lcom/sec/ims/csh/vsh/VshOrientation;

    .line 21
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/ims/csh/vsh/VshOrientation;

    sget-object v1, Lcom/sec/ims/csh/vsh/VshOrientation;->LANDSCAPE:Lcom/sec/ims/csh/vsh/VshOrientation;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/ims/csh/vsh/VshOrientation;->PORTRAIT:Lcom/sec/ims/csh/vsh/VshOrientation;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/ims/csh/vsh/VshOrientation;->FLIPPED_LANDSCAPE:Lcom/sec/ims/csh/vsh/VshOrientation;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/ims/csh/vsh/VshOrientation;->REVERSE_PORTRAIT:Lcom/sec/ims/csh/vsh/VshOrientation;

    aput-object v1, v0, v6

    sput-object v0, Lcom/sec/ims/csh/vsh/VshOrientation;->$VALUES:[Lcom/sec/ims/csh/vsh/VshOrientation;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "i"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 30
    iput p3, p0, Lcom/sec/ims/csh/vsh/VshOrientation;->mAngle:I

    .line 31
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/ims/csh/vsh/VshOrientation;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 21
    const-class v0, Lcom/sec/ims/csh/vsh/VshOrientation;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/ims/csh/vsh/VshOrientation;

    return-object v0
.end method

.method public static values()[Lcom/sec/ims/csh/vsh/VshOrientation;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/sec/ims/csh/vsh/VshOrientation;->$VALUES:[Lcom/sec/ims/csh/vsh/VshOrientation;

    invoke-virtual {v0}, [Lcom/sec/ims/csh/vsh/VshOrientation;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/ims/csh/vsh/VshOrientation;

    return-object v0
.end method


# virtual methods
.method public toInt()I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lcom/sec/ims/csh/vsh/VshOrientation;->mAngle:I

    return v0
.end method
