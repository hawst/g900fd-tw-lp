.class public final enum Lcom/sec/ims/csh/vsh/VshResolution;
.super Ljava/lang/Enum;
.source "VshResolution.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/ims/csh/vsh/VshResolution;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/ims/csh/vsh/VshResolution;

.field public static final enum CIF:Lcom/sec/ims/csh/vsh/VshResolution;

.field public static final enum CIF_PORTRAIT:Lcom/sec/ims/csh/vsh/VshResolution;

.field public static final enum NONE:Lcom/sec/ims/csh/vsh/VshResolution;

.field public static final enum QCIF:Lcom/sec/ims/csh/vsh/VshResolution;

.field public static final enum QCIF_PORTRAIT:Lcom/sec/ims/csh/vsh/VshResolution;

.field public static final enum QVGA:Lcom/sec/ims/csh/vsh/VshResolution;

.field public static final enum QVGA_PORTRAIT:Lcom/sec/ims/csh/vsh/VshResolution;

.field public static final enum VGA:Lcom/sec/ims/csh/vsh/VshResolution;

.field public static final enum VGA_PORTRAIT:Lcom/sec/ims/csh/vsh/VshResolution;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 21
    new-instance v0, Lcom/sec/ims/csh/vsh/VshResolution;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v3}, Lcom/sec/ims/csh/vsh/VshResolution;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/csh/vsh/VshResolution;->NONE:Lcom/sec/ims/csh/vsh/VshResolution;

    .line 22
    new-instance v0, Lcom/sec/ims/csh/vsh/VshResolution;

    const-string v1, "QCIF"

    invoke-direct {v0, v1, v4}, Lcom/sec/ims/csh/vsh/VshResolution;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/csh/vsh/VshResolution;->QCIF:Lcom/sec/ims/csh/vsh/VshResolution;

    .line 23
    new-instance v0, Lcom/sec/ims/csh/vsh/VshResolution;

    const-string v1, "QVGA"

    invoke-direct {v0, v1, v5}, Lcom/sec/ims/csh/vsh/VshResolution;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/csh/vsh/VshResolution;->QVGA:Lcom/sec/ims/csh/vsh/VshResolution;

    .line 24
    new-instance v0, Lcom/sec/ims/csh/vsh/VshResolution;

    const-string v1, "VGA"

    invoke-direct {v0, v1, v6}, Lcom/sec/ims/csh/vsh/VshResolution;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/csh/vsh/VshResolution;->VGA:Lcom/sec/ims/csh/vsh/VshResolution;

    .line 25
    new-instance v0, Lcom/sec/ims/csh/vsh/VshResolution;

    const-string v1, "CIF"

    invoke-direct {v0, v1, v7}, Lcom/sec/ims/csh/vsh/VshResolution;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/csh/vsh/VshResolution;->CIF:Lcom/sec/ims/csh/vsh/VshResolution;

    .line 26
    new-instance v0, Lcom/sec/ims/csh/vsh/VshResolution;

    const-string v1, "QCIF_PORTRAIT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/csh/vsh/VshResolution;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/csh/vsh/VshResolution;->QCIF_PORTRAIT:Lcom/sec/ims/csh/vsh/VshResolution;

    .line 27
    new-instance v0, Lcom/sec/ims/csh/vsh/VshResolution;

    const-string v1, "QVGA_PORTRAIT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/csh/vsh/VshResolution;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/csh/vsh/VshResolution;->QVGA_PORTRAIT:Lcom/sec/ims/csh/vsh/VshResolution;

    .line 28
    new-instance v0, Lcom/sec/ims/csh/vsh/VshResolution;

    const-string v1, "VGA_PORTRAIT"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/csh/vsh/VshResolution;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/csh/vsh/VshResolution;->VGA_PORTRAIT:Lcom/sec/ims/csh/vsh/VshResolution;

    .line 29
    new-instance v0, Lcom/sec/ims/csh/vsh/VshResolution;

    const-string v1, "CIF_PORTRAIT"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/csh/vsh/VshResolution;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/csh/vsh/VshResolution;->CIF_PORTRAIT:Lcom/sec/ims/csh/vsh/VshResolution;

    .line 20
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/sec/ims/csh/vsh/VshResolution;

    sget-object v1, Lcom/sec/ims/csh/vsh/VshResolution;->NONE:Lcom/sec/ims/csh/vsh/VshResolution;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/ims/csh/vsh/VshResolution;->QCIF:Lcom/sec/ims/csh/vsh/VshResolution;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/ims/csh/vsh/VshResolution;->QVGA:Lcom/sec/ims/csh/vsh/VshResolution;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/ims/csh/vsh/VshResolution;->VGA:Lcom/sec/ims/csh/vsh/VshResolution;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/ims/csh/vsh/VshResolution;->CIF:Lcom/sec/ims/csh/vsh/VshResolution;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/ims/csh/vsh/VshResolution;->QCIF_PORTRAIT:Lcom/sec/ims/csh/vsh/VshResolution;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/ims/csh/vsh/VshResolution;->QVGA_PORTRAIT:Lcom/sec/ims/csh/vsh/VshResolution;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/ims/csh/vsh/VshResolution;->VGA_PORTRAIT:Lcom/sec/ims/csh/vsh/VshResolution;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/ims/csh/vsh/VshResolution;->CIF_PORTRAIT:Lcom/sec/ims/csh/vsh/VshResolution;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/ims/csh/vsh/VshResolution;->$VALUES:[Lcom/sec/ims/csh/vsh/VshResolution;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/ims/csh/vsh/VshResolution;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 20
    const-class v0, Lcom/sec/ims/csh/vsh/VshResolution;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/ims/csh/vsh/VshResolution;

    return-object v0
.end method

.method public static values()[Lcom/sec/ims/csh/vsh/VshResolution;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/sec/ims/csh/vsh/VshResolution;->$VALUES:[Lcom/sec/ims/csh/vsh/VshResolution;

    invoke-virtual {v0}, [Lcom/sec/ims/csh/vsh/VshResolution;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/ims/csh/vsh/VshResolution;

    return-object v0
.end method
