.class public final enum Lcom/sec/ims/csh/vsh/VshRotationDirection;
.super Ljava/lang/Enum;
.source "VshRotationDirection.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/ims/csh/vsh/VshRotationDirection;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/ims/csh/vsh/VshRotationDirection;

.field public static final enum ANTI_CLOCKWISE:Lcom/sec/ims/csh/vsh/VshRotationDirection;

.field public static final enum CLOCKWISE:Lcom/sec/ims/csh/vsh/VshRotationDirection;

.field public static final enum NONE:Lcom/sec/ims/csh/vsh/VshRotationDirection;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 21
    new-instance v0, Lcom/sec/ims/csh/vsh/VshRotationDirection;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/csh/vsh/VshRotationDirection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/csh/vsh/VshRotationDirection;->NONE:Lcom/sec/ims/csh/vsh/VshRotationDirection;

    .line 22
    new-instance v0, Lcom/sec/ims/csh/vsh/VshRotationDirection;

    const-string v1, "CLOCKWISE"

    invoke-direct {v0, v1, v3}, Lcom/sec/ims/csh/vsh/VshRotationDirection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/csh/vsh/VshRotationDirection;->CLOCKWISE:Lcom/sec/ims/csh/vsh/VshRotationDirection;

    .line 23
    new-instance v0, Lcom/sec/ims/csh/vsh/VshRotationDirection;

    const-string v1, "ANTI_CLOCKWISE"

    invoke-direct {v0, v1, v4}, Lcom/sec/ims/csh/vsh/VshRotationDirection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/csh/vsh/VshRotationDirection;->ANTI_CLOCKWISE:Lcom/sec/ims/csh/vsh/VshRotationDirection;

    .line 20
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/ims/csh/vsh/VshRotationDirection;

    sget-object v1, Lcom/sec/ims/csh/vsh/VshRotationDirection;->NONE:Lcom/sec/ims/csh/vsh/VshRotationDirection;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/ims/csh/vsh/VshRotationDirection;->CLOCKWISE:Lcom/sec/ims/csh/vsh/VshRotationDirection;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/ims/csh/vsh/VshRotationDirection;->ANTI_CLOCKWISE:Lcom/sec/ims/csh/vsh/VshRotationDirection;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/ims/csh/vsh/VshRotationDirection;->$VALUES:[Lcom/sec/ims/csh/vsh/VshRotationDirection;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/ims/csh/vsh/VshRotationDirection;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 20
    const-class v0, Lcom/sec/ims/csh/vsh/VshRotationDirection;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/ims/csh/vsh/VshRotationDirection;

    return-object v0
.end method

.method public static values()[Lcom/sec/ims/csh/vsh/VshRotationDirection;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/sec/ims/csh/vsh/VshRotationDirection;->$VALUES:[Lcom/sec/ims/csh/vsh/VshRotationDirection;

    invoke-virtual {v0}, [Lcom/sec/ims/csh/vsh/VshRotationDirection;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/ims/csh/vsh/VshRotationDirection;

    return-object v0
.end method
