.class public Lcom/sec/ims/csh/ish/IshFileTransfer;
.super Lcom/sec/ims/csh/ish/IshFile;
.source "IshFileTransfer.java"


# instance fields
.field private mTransmittedBytes:I


# direct methods
.method public constructor <init>(Lcom/sec/ims/csh/ish/IshFile;)V
    .locals 1
    .param p1, "ishFile"    # Lcom/sec/ims/csh/ish/IshFile;

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/sec/ims/csh/ish/IshFile;-><init>()V

    .line 22
    invoke-static {p1}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 24
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/ims/csh/ish/IshFileTransfer;->mTransmittedBytes:I

    .line 25
    invoke-virtual {p1}, Lcom/sec/ims/csh/ish/IshFile;->getPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/csh/ish/IshFileTransfer;->mPath:Ljava/lang/String;

    .line 26
    invoke-virtual {p1}, Lcom/sec/ims/csh/ish/IshFile;->getSize()I

    move-result v0

    iput v0, p0, Lcom/sec/ims/csh/ish/IshFileTransfer;->mSize:I

    .line 27
    invoke-virtual {p1}, Lcom/sec/ims/csh/ish/IshFile;->getMimeType()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/csh/ish/IshFileTransfer;->mMimeType:Ljava/lang/String;

    .line 28
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 2
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "size"    # I
    .param p3, "mimeType"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 30
    invoke-direct {p0}, Lcom/sec/ims/csh/ish/IshFile;-><init>()V

    .line 31
    const-string v0, "path can\'t be NULL"

    invoke-static {p1, v0}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    if-ltz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/android/internal/util/Preconditions;->checkState(Z)V

    .line 33
    const-string v0, "mimeType can\'t be NULL"

    invoke-static {p3, v0}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    iput v1, p0, Lcom/sec/ims/csh/ish/IshFileTransfer;->mTransmittedBytes:I

    .line 37
    iput-object p1, p0, Lcom/sec/ims/csh/ish/IshFileTransfer;->mPath:Ljava/lang/String;

    .line 38
    iput p2, p0, Lcom/sec/ims/csh/ish/IshFileTransfer;->mSize:I

    .line 39
    iput-object p3, p0, Lcom/sec/ims/csh/ish/IshFileTransfer;->mMimeType:Ljava/lang/String;

    .line 40
    return-void

    :cond_0
    move v0, v1

    .line 32
    goto :goto_0
.end method


# virtual methods
.method public getMimeType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/ims/csh/ish/IshFileTransfer;->mMimeType:Ljava/lang/String;

    return-object v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/ims/csh/ish/IshFileTransfer;->mPath:Ljava/lang/String;

    return-object v0
.end method

.method public getSize()I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lcom/sec/ims/csh/ish/IshFileTransfer;->mSize:I

    return v0
.end method

.method public getTransmittedBytes()I
    .locals 1

    .prologue
    .line 64
    iget v0, p0, Lcom/sec/ims/csh/ish/IshFileTransfer;->mTransmittedBytes:I

    return v0
.end method

.method public setPath(Ljava/lang/String;)V
    .locals 0
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 50
    iput-object p1, p0, Lcom/sec/ims/csh/ish/IshFileTransfer;->mPath:Ljava/lang/String;

    .line 51
    return-void
.end method

.method public updateTransmittedBytes(I)V
    .locals 3
    .param p1, "bytes"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 74
    if-ltz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/android/internal/util/Preconditions;->checkState(Z)V

    .line 75
    iget v0, p0, Lcom/sec/ims/csh/ish/IshFileTransfer;->mTransmittedBytes:I

    if-lt p1, v0, :cond_1

    :goto_1
    invoke-static {v1}, Lcom/android/internal/util/Preconditions;->checkState(Z)V

    .line 77
    iput p1, p0, Lcom/sec/ims/csh/ish/IshFileTransfer;->mTransmittedBytes:I

    .line 78
    return-void

    :cond_0
    move v0, v2

    .line 74
    goto :goto_0

    :cond_1
    move v1, v2

    .line 75
    goto :goto_1
.end method
