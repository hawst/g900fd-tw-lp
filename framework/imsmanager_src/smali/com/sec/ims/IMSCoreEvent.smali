.class public Lcom/sec/ims/IMSCoreEvent;
.super Ljava/lang/Object;
.source "IMSCoreEvent.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/ims/IMSCoreEvent;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final bearerType:I

.field private final ecmpStatus:I

.field private final imsEventType:I

.field private final imsRegistrationErrorCode:I

.field private final imsRegistrationErrorDescription:Ljava/lang/String;

.field private final imsRegistrationStatus:I

.field private final isImsiBasedUriRegistration:I

.field private final mRegExpiryStatus:I

.field private final videoEnabledStatus:I

.field private final volteEnabledStatus:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 112
    new-instance v0, Lcom/sec/ims/IMSCoreEvent$1;

    invoke-direct {v0}, Lcom/sec/ims/IMSCoreEvent$1;-><init>()V

    sput-object v0, Lcom/sec/ims/IMSCoreEvent;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(IIILjava/lang/String;IIIIII)V
    .locals 0
    .param p1, "bearertype"    # I
    .param p2, "regStatus"    # I
    .param p3, "errorCode"    # I
    .param p4, "errorDesc"    # Ljava/lang/String;
    .param p5, "ecmpstatus"    # I
    .param p6, "regExpiryStatus"    # I
    .param p7, "volteStatus"    # I
    .param p8, "videoStatus"    # I
    .param p9, "imsEvent"    # I
    .param p10, "isImsiBasedUriRegistration"    # I

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput p1, p0, Lcom/sec/ims/IMSCoreEvent;->bearerType:I

    .line 42
    iput p2, p0, Lcom/sec/ims/IMSCoreEvent;->imsRegistrationStatus:I

    .line 43
    iput p3, p0, Lcom/sec/ims/IMSCoreEvent;->imsRegistrationErrorCode:I

    .line 44
    iput-object p4, p0, Lcom/sec/ims/IMSCoreEvent;->imsRegistrationErrorDescription:Ljava/lang/String;

    .line 45
    iput p5, p0, Lcom/sec/ims/IMSCoreEvent;->ecmpStatus:I

    .line 46
    iput p6, p0, Lcom/sec/ims/IMSCoreEvent;->mRegExpiryStatus:I

    .line 47
    iput p7, p0, Lcom/sec/ims/IMSCoreEvent;->volteEnabledStatus:I

    .line 48
    iput p8, p0, Lcom/sec/ims/IMSCoreEvent;->videoEnabledStatus:I

    .line 49
    iput p9, p0, Lcom/sec/ims/IMSCoreEvent;->imsEventType:I

    .line 50
    iput p10, p0, Lcom/sec/ims/IMSCoreEvent;->isImsiBasedUriRegistration:I

    .line 51
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 126
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/ims/IMSCoreEvent;->bearerType:I

    .line 127
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/ims/IMSCoreEvent;->imsRegistrationStatus:I

    .line 128
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/ims/IMSCoreEvent;->imsRegistrationErrorCode:I

    .line 129
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/IMSCoreEvent;->imsRegistrationErrorDescription:Ljava/lang/String;

    .line 130
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/ims/IMSCoreEvent;->ecmpStatus:I

    .line 131
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/ims/IMSCoreEvent;->mRegExpiryStatus:I

    .line 132
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/ims/IMSCoreEvent;->volteEnabledStatus:I

    .line 133
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/ims/IMSCoreEvent;->videoEnabledStatus:I

    .line 134
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/ims/IMSCoreEvent;->imsEventType:I

    .line 135
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/ims/IMSCoreEvent;->isImsiBasedUriRegistration:I

    .line 136
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/ims/IMSCoreEvent$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/sec/ims/IMSCoreEvent$1;

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/sec/ims/IMSCoreEvent;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 95
    const/4 v0, 0x0

    return v0
.end method

.method public getBearerType()I
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lcom/sec/ims/IMSCoreEvent;->bearerType:I

    return v0
.end method

.method public getECMPStatus()I
    .locals 1

    .prologue
    .line 70
    iget v0, p0, Lcom/sec/ims/IMSCoreEvent;->ecmpStatus:I

    return v0
.end method

.method public getIMSRegistrationErrorCode()I
    .locals 1

    .prologue
    .line 62
    iget v0, p0, Lcom/sec/ims/IMSCoreEvent;->imsRegistrationErrorCode:I

    return v0
.end method

.method public getIMSRegistrationErrorDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/ims/IMSCoreEvent;->imsRegistrationErrorDescription:Ljava/lang/String;

    return-object v0
.end method

.method public getIMSRegistrationStatus()I
    .locals 1

    .prologue
    .line 58
    iget v0, p0, Lcom/sec/ims/IMSCoreEvent;->imsRegistrationStatus:I

    return v0
.end method

.method public getImsEventType()I
    .locals 1

    .prologue
    .line 86
    iget v0, p0, Lcom/sec/ims/IMSCoreEvent;->imsEventType:I

    return v0
.end method

.method public getIsImsiBasedRegistration()I
    .locals 1

    .prologue
    .line 90
    iget v0, p0, Lcom/sec/ims/IMSCoreEvent;->isImsiBasedUriRegistration:I

    return v0
.end method

.method public getRegExpiryStatus()I
    .locals 1

    .prologue
    .line 74
    iget v0, p0, Lcom/sec/ims/IMSCoreEvent;->mRegExpiryStatus:I

    return v0
.end method

.method public getVideoEnabledStatus()I
    .locals 1

    .prologue
    .line 82
    iget v0, p0, Lcom/sec/ims/IMSCoreEvent;->videoEnabledStatus:I

    return v0
.end method

.method public getVolteEnabledStatus()I
    .locals 1

    .prologue
    .line 78
    iget v0, p0, Lcom/sec/ims/IMSCoreEvent;->volteEnabledStatus:I

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 100
    iget v0, p0, Lcom/sec/ims/IMSCoreEvent;->bearerType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 101
    iget v0, p0, Lcom/sec/ims/IMSCoreEvent;->imsRegistrationStatus:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 102
    iget v0, p0, Lcom/sec/ims/IMSCoreEvent;->imsRegistrationErrorCode:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 103
    iget-object v0, p0, Lcom/sec/ims/IMSCoreEvent;->imsRegistrationErrorDescription:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 104
    iget v0, p0, Lcom/sec/ims/IMSCoreEvent;->ecmpStatus:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 105
    iget v0, p0, Lcom/sec/ims/IMSCoreEvent;->mRegExpiryStatus:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 106
    iget v0, p0, Lcom/sec/ims/IMSCoreEvent;->volteEnabledStatus:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 107
    iget v0, p0, Lcom/sec/ims/IMSCoreEvent;->videoEnabledStatus:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 108
    iget v0, p0, Lcom/sec/ims/IMSCoreEvent;->imsEventType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 109
    iget v0, p0, Lcom/sec/ims/IMSCoreEvent;->isImsiBasedUriRegistration:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 110
    return-void
.end method
