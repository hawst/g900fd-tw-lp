.class public Lcom/sec/ims/contact/EnContactsEngineEvent;
.super Lcom/sec/ims/contact/EnContactsEngineRawEvent;
.source "EnContactsEngineEvent.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/ims/contact/EnContactsEngineEvent;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mData:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    new-instance v0, Lcom/sec/ims/contact/EnContactsEngineEvent$1;

    invoke-direct {v0}, Lcom/sec/ims/contact/EnContactsEngineEvent$1;-><init>()V

    sput-object v0, Lcom/sec/ims/contact/EnContactsEngineEvent;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(IIIILjava/lang/Object;)V
    .locals 0
    .param p1, "appType"    # I
    .param p2, "evntType"    # I
    .param p3, "sessionID"    # I
    .param p4, "subEventType"    # I
    .param p5, "data"    # Ljava/lang/Object;

    .prologue
    .line 23
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/ims/contact/EnContactsEngineRawEvent;-><init>(IIII)V

    .line 24
    check-cast p5, Ljava/lang/String;

    .end local p5    # "data":Ljava/lang/Object;
    iput-object p5, p0, Lcom/sec/ims/contact/EnContactsEngineEvent;->mData:Ljava/lang/String;

    .line 25
    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/sec/ims/contact/EnContactsEngineRawEvent;-><init>(Landroid/os/Parcel;)V

    .line 33
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/contact/EnContactsEngineEvent;->mData:Ljava/lang/String;

    .line 34
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x0

    return v0
.end method

.method public getEabData()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/ims/contact/EnContactsEngineEvent;->mData:Ljava/lang/String;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 43
    invoke-super {p0, p1, p2}, Lcom/sec/ims/contact/EnContactsEngineRawEvent;->writeToParcel(Landroid/os/Parcel;I)V

    .line 44
    iget-object v0, p0, Lcom/sec/ims/contact/EnContactsEngineEvent;->mData:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 45
    return-void
.end method
