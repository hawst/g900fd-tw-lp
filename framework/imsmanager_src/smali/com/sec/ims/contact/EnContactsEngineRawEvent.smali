.class public abstract Lcom/sec/ims/contact/EnContactsEngineRawEvent;
.super Ljava/lang/Object;
.source "EnContactsEngineRawEvent.java"

# interfaces
.implements Landroid/os/Parcelable;


# instance fields
.field private final mAppType:I

.field private final mEventType:I

.field private final mSessionID:I

.field private final mSubEventType:I


# direct methods
.method protected constructor <init>(IIII)V
    .locals 0
    .param p1, "appType"    # I
    .param p2, "evntType"    # I
    .param p3, "sessionID"    # I
    .param p4, "subEventType"    # I

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput p1, p0, Lcom/sec/ims/contact/EnContactsEngineRawEvent;->mAppType:I

    .line 27
    iput p2, p0, Lcom/sec/ims/contact/EnContactsEngineRawEvent;->mEventType:I

    .line 28
    iput p3, p0, Lcom/sec/ims/contact/EnContactsEngineRawEvent;->mSessionID:I

    .line 29
    iput p4, p0, Lcom/sec/ims/contact/EnContactsEngineRawEvent;->mSubEventType:I

    .line 30
    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/ims/contact/EnContactsEngineRawEvent;->mAppType:I

    .line 50
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/ims/contact/EnContactsEngineRawEvent;->mEventType:I

    .line 51
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/ims/contact/EnContactsEngineRawEvent;->mSessionID:I

    .line 52
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/ims/contact/EnContactsEngineRawEvent;->mSubEventType:I

    .line 53
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 57
    const/4 v0, 0x0

    return v0
.end method

.method public getAppType()I
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lcom/sec/ims/contact/EnContactsEngineRawEvent;->mAppType:I

    return v0
.end method

.method public getEventType()I
    .locals 1

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/ims/contact/EnContactsEngineRawEvent;->mEventType:I

    return v0
.end method

.method public getSessionID()I
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Lcom/sec/ims/contact/EnContactsEngineRawEvent;->mSessionID:I

    return v0
.end method

.method public getSubEventType()I
    .locals 1

    .prologue
    .line 45
    iget v0, p0, Lcom/sec/ims/contact/EnContactsEngineRawEvent;->mSubEventType:I

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 62
    iget v0, p0, Lcom/sec/ims/contact/EnContactsEngineRawEvent;->mAppType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 63
    iget v0, p0, Lcom/sec/ims/contact/EnContactsEngineRawEvent;->mEventType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 64
    iget v0, p0, Lcom/sec/ims/contact/EnContactsEngineRawEvent;->mSessionID:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 65
    iget v0, p0, Lcom/sec/ims/contact/EnContactsEngineRawEvent;->mSubEventType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 66
    return-void
.end method
