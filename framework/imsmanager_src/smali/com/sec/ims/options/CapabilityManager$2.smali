.class Lcom/sec/ims/options/CapabilityManager$2;
.super Lcom/sec/ims/options/ICapabilityServiceEventListener$Stub;
.source "CapabilityManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/ims/options/CapabilityManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/ims/options/CapabilityManager;


# direct methods
.method constructor <init>(Lcom/sec/ims/options/CapabilityManager;)V
    .locals 0

    .prologue
    .line 180
    iput-object p1, p0, Lcom/sec/ims/options/CapabilityManager$2;->this$0:Lcom/sec/ims/options/CapabilityManager;

    invoke-direct {p0}, Lcom/sec/ims/options/ICapabilityServiceEventListener$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onCapabilitiesChanged(Ljava/util/List;Lcom/sec/ims/options/Capabilities;)V
    .locals 2
    .param p2, "capa"    # Lcom/sec/ims/options/Capabilities;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Lcom/sec/ims/options/Capabilities;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 193
    .local p1, "uris":Ljava/util/List;, "Ljava/util/List<Landroid/net/Uri;>;"
    iget-object v0, p0, Lcom/sec/ims/options/CapabilityManager$2;->this$0:Lcom/sec/ims/options/CapabilityManager;

    iget-object v0, v0, Lcom/sec/ims/options/CapabilityManager;->mRelay:Lcom/sec/ims/options/CapabilityManager$CapabilityEventRelay;

    if-nez v0, :cond_0

    .line 194
    const-string v0, "CapabilityManager"

    const-string v1, "no listener for ICapabilityServiceEventListener"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 195
    new-instance v0, Landroid/os/RemoteException;

    invoke-direct {v0}, Landroid/os/RemoteException;-><init>()V

    throw v0

    .line 197
    :cond_0
    iget-object v0, p0, Lcom/sec/ims/options/CapabilityManager$2;->this$0:Lcom/sec/ims/options/CapabilityManager;

    iget-object v0, v0, Lcom/sec/ims/options/CapabilityManager;->mRelay:Lcom/sec/ims/options/CapabilityManager$CapabilityEventRelay;

    invoke-interface {v0, p1, p2}, Lcom/sec/ims/options/CapabilityManager$CapabilityEventRelay;->onCapabilitiesChanged(Ljava/util/List;Lcom/sec/ims/options/Capabilities;)V

    .line 199
    return-void
.end method

.method public onMultipleCapabilitiesChanged(Ljava/util/List;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/ims/options/Capabilities;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 204
    .local p1, "uris":Ljava/util/List;, "Ljava/util/List<Landroid/net/Uri;>;"
    .local p2, "capaList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/ims/options/Capabilities;>;"
    iget-object v0, p0, Lcom/sec/ims/options/CapabilityManager$2;->this$0:Lcom/sec/ims/options/CapabilityManager;

    iget-object v0, v0, Lcom/sec/ims/options/CapabilityManager;->mRelay:Lcom/sec/ims/options/CapabilityManager$CapabilityEventRelay;

    if-nez v0, :cond_0

    .line 205
    const-string v0, "CapabilityManager"

    const-string v1, "no listener for ICapabilityServiceEventListener"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 206
    new-instance v0, Landroid/os/RemoteException;

    invoke-direct {v0}, Landroid/os/RemoteException;-><init>()V

    throw v0

    .line 208
    :cond_0
    iget-object v0, p0, Lcom/sec/ims/options/CapabilityManager$2;->this$0:Lcom/sec/ims/options/CapabilityManager;

    iget-object v0, v0, Lcom/sec/ims/options/CapabilityManager;->mRelay:Lcom/sec/ims/options/CapabilityManager$CapabilityEventRelay;

    invoke-interface {v0, p1, p2}, Lcom/sec/ims/options/CapabilityManager$CapabilityEventRelay;->onMultipleCapabilitiesChanged(Ljava/util/List;Ljava/util/List;)V

    .line 210
    return-void
.end method

.method public onOwnCapabilitiesChanged()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 183
    iget-object v0, p0, Lcom/sec/ims/options/CapabilityManager$2;->this$0:Lcom/sec/ims/options/CapabilityManager;

    iget-object v0, v0, Lcom/sec/ims/options/CapabilityManager;->mRelay:Lcom/sec/ims/options/CapabilityManager$CapabilityEventRelay;

    if-nez v0, :cond_0

    .line 184
    const-string v0, "CapabilityManager"

    const-string v1, "no listener for ICapabilityServiceEventListener"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    new-instance v0, Landroid/os/RemoteException;

    invoke-direct {v0}, Landroid/os/RemoteException;-><init>()V

    throw v0

    .line 187
    :cond_0
    iget-object v0, p0, Lcom/sec/ims/options/CapabilityManager$2;->this$0:Lcom/sec/ims/options/CapabilityManager;

    iget-object v0, v0, Lcom/sec/ims/options/CapabilityManager;->mRelay:Lcom/sec/ims/options/CapabilityManager$CapabilityEventRelay;

    invoke-interface {v0}, Lcom/sec/ims/options/CapabilityManager$CapabilityEventRelay;->onOwnCapabilitiesChanged()V

    .line 189
    return-void
.end method
