.class public interface abstract Lcom/sec/ims/options/IEABServiceEventListener;
.super Ljava/lang/Object;
.source "IEABServiceEventListener.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/ims/options/IEABServiceEventListener$Stub;
    }
.end annotation


# virtual methods
.method public abstract onCapabilityAndAvailabilityPublished(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract onCapabilityAndAvailabilityReceived(ILjava/lang/String;[Lcom/sec/ims/options/SipRemoteProfile;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
