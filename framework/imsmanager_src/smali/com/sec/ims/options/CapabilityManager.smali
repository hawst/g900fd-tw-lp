.class public Lcom/sec/ims/options/CapabilityManager;
.super Ljava/lang/Object;
.source "CapabilityManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/ims/options/CapabilityManager$CapabilityEventRelay;
    }
.end annotation


# static fields
.field public static final LOG_TAG:Ljava/lang/String; = "CapabilityManager"

.field public static mImsCapabilityService:Lcom/sec/ims/options/ICapabilityService;


# instance fields
.field mEventProxy:Lcom/sec/ims/options/ICapabilityServiceEventListener$Stub;

.field mRelay:Lcom/sec/ims/options/CapabilityManager$CapabilityEventRelay;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/ims/options/CapabilityManager;->mImsCapabilityService:Lcom/sec/ims/options/ICapabilityService;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 179
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/ims/options/CapabilityManager;->mRelay:Lcom/sec/ims/options/CapabilityManager$CapabilityEventRelay;

    .line 180
    new-instance v1, Lcom/sec/ims/options/CapabilityManager$2;

    invoke-direct {v1, p0}, Lcom/sec/ims/options/CapabilityManager$2;-><init>(Lcom/sec/ims/options/CapabilityManager;)V

    iput-object v1, p0, Lcom/sec/ims/options/CapabilityManager;->mEventProxy:Lcom/sec/ims/options/ICapabilityServiceEventListener$Stub;

    .line 31
    const-string v1, "CapabilityManager"

    const-string v2, "Connecting to CapabilityDiscoveryService..."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 33
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 34
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.sec.imsservice"

    const-string v2, "com.sec.internal.ims.imsservice.CapabilityService"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 36
    new-instance v1, Lcom/sec/ims/options/CapabilityManager$1;

    invoke-direct {v1, p0}, Lcom/sec/ims/options/CapabilityManager$1;-><init>(Lcom/sec/ims/options/CapabilityManager;)V

    const/4 v2, 0x1

    sget-object v3, Landroid/os/UserHandle;->CURRENT_OR_SELF:Landroid/os/UserHandle;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/content/Context;->bindServiceAsUser(Landroid/content/Intent;Landroid/content/ServiceConnection;ILandroid/os/UserHandle;)Z

    .line 48
    return-void
.end method


# virtual methods
.method public addFakeCapabilityInfo(Ljava/util/List;Z)V
    .locals 1
    .param p2, "feature"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;Z)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 176
    .local p1, "uris":Ljava/util/List;, "Ljava/util/List<Landroid/net/Uri;>;"
    sget-object v0, Lcom/sec/ims/options/CapabilityManager;->mImsCapabilityService:Lcom/sec/ims/options/ICapabilityService;

    invoke-interface {v0, p1, p2}, Lcom/sec/ims/options/ICapabilityService;->addFakeCapabilityInfo(Ljava/util/List;Z)V

    .line 177
    return-void
.end method

.method public getAllCapabilities()[Lcom/sec/ims/options/Capabilities;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 141
    sget-object v0, Lcom/sec/ims/options/CapabilityManager;->mImsCapabilityService:Lcom/sec/ims/options/ICapabilityService;

    invoke-interface {v0}, Lcom/sec/ims/options/ICapabilityService;->getAllCapabilities()[Lcom/sec/ims/options/Capabilities;

    move-result-object v0

    return-object v0
.end method

.method public getCapabilities(Landroid/net/Uri;I)Lcom/sec/ims/options/Capabilities;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "refreshType"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 68
    sget-object v0, Lcom/sec/ims/options/CapabilityManager;->mImsCapabilityService:Lcom/sec/ims/options/ICapabilityService;

    invoke-interface {v0, p1, p2}, Lcom/sec/ims/options/ICapabilityService;->getCapabilities(Landroid/net/Uri;I)Lcom/sec/ims/options/Capabilities;

    move-result-object v0

    return-object v0
.end method

.method public getCapabilitiesByContactId(Ljava/lang/String;I)[Lcom/sec/ims/options/Capabilities;
    .locals 1
    .param p1, "contactId"    # Ljava/lang/String;
    .param p2, "refreshType"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 132
    sget-object v0, Lcom/sec/ims/options/CapabilityManager;->mImsCapabilityService:Lcom/sec/ims/options/ICapabilityService;

    invoke-interface {v0, p1, p2}, Lcom/sec/ims/options/ICapabilityService;->getCapabilitiesByContactId(Ljava/lang/String;I)[Lcom/sec/ims/options/Capabilities;

    move-result-object v0

    return-object v0
.end method

.method public getCapabilitiesById(I)Lcom/sec/ims/options/Capabilities;
    .locals 1
    .param p1, "id"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 118
    sget-object v0, Lcom/sec/ims/options/CapabilityManager;->mImsCapabilityService:Lcom/sec/ims/options/ICapabilityService;

    invoke-interface {v0, p1}, Lcom/sec/ims/options/ICapabilityService;->getCapabilitiesById(I)Lcom/sec/ims/options/Capabilities;

    move-result-object v0

    return-object v0
.end method

.method public getCapabilitiesByNumber(Ljava/lang/String;I)Lcom/sec/ims/options/Capabilities;
    .locals 1
    .param p1, "number"    # Ljava/lang/String;
    .param p2, "refreshType"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 81
    sget-object v0, Lcom/sec/ims/options/CapabilityManager;->mImsCapabilityService:Lcom/sec/ims/options/ICapabilityService;

    invoke-interface {v0, p1, p2}, Lcom/sec/ims/options/ICapabilityService;->getCapabilitiesByNumber(Ljava/lang/String;I)Lcom/sec/ims/options/Capabilities;

    move-result-object v0

    return-object v0
.end method

.method public getCapabilitiesWithDelay(Ljava/lang/String;I)Lcom/sec/ims/options/Capabilities;
    .locals 1
    .param p1, "number"    # Ljava/lang/String;
    .param p2, "refreshType"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 95
    sget-object v0, Lcom/sec/ims/options/CapabilityManager;->mImsCapabilityService:Lcom/sec/ims/options/ICapabilityService;

    invoke-interface {v0, p1, p2}, Lcom/sec/ims/options/ICapabilityService;->getCapabilitiesWithDelay(Ljava/lang/String;I)Lcom/sec/ims/options/Capabilities;

    move-result-object v0

    return-object v0
.end method

.method public getCapabilitiesWithFeature(Ljava/lang/String;I)Lcom/sec/ims/options/Capabilities;
    .locals 1
    .param p1, "number"    # Ljava/lang/String;
    .param p2, "feature"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 108
    sget-object v0, Lcom/sec/ims/options/CapabilityManager;->mImsCapabilityService:Lcom/sec/ims/options/ICapabilityService;

    invoke-interface {v0, p1, p2}, Lcom/sec/ims/options/ICapabilityService;->getCapabilitiesWithFeature(Ljava/lang/String;I)Lcom/sec/ims/options/Capabilities;

    move-result-object v0

    return-object v0
.end method

.method public getOwnCapabilities()Lcom/sec/ims/options/Capabilities;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 56
    sget-object v0, Lcom/sec/ims/options/CapabilityManager;->mImsCapabilityService:Lcom/sec/ims/options/ICapabilityService;

    invoke-interface {v0}, Lcom/sec/ims/options/ICapabilityService;->getOwnCapabilities()Lcom/sec/ims/options/Capabilities;

    move-result-object v0

    return-object v0
.end method

.method public registerListener(Lcom/sec/ims/options/CapabilityManager$CapabilityEventRelay;)V
    .locals 2
    .param p1, "listener"    # Lcom/sec/ims/options/CapabilityManager$CapabilityEventRelay;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 152
    iput-object p1, p0, Lcom/sec/ims/options/CapabilityManager;->mRelay:Lcom/sec/ims/options/CapabilityManager$CapabilityEventRelay;

    .line 153
    sget-object v0, Lcom/sec/ims/options/CapabilityManager;->mImsCapabilityService:Lcom/sec/ims/options/ICapabilityService;

    iget-object v1, p0, Lcom/sec/ims/options/CapabilityManager;->mEventProxy:Lcom/sec/ims/options/ICapabilityServiceEventListener$Stub;

    invoke-interface {v0, v1}, Lcom/sec/ims/options/ICapabilityService;->registerListener(Lcom/sec/ims/options/ICapabilityServiceEventListener;)V

    .line 154
    return-void
.end method

.method public unregisterListener(Lcom/sec/ims/options/CapabilityManager$CapabilityEventRelay;)V
    .locals 2
    .param p1, "listener"    # Lcom/sec/ims/options/CapabilityManager$CapabilityEventRelay;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 164
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/ims/options/CapabilityManager;->mRelay:Lcom/sec/ims/options/CapabilityManager$CapabilityEventRelay;

    .line 165
    sget-object v0, Lcom/sec/ims/options/CapabilityManager;->mImsCapabilityService:Lcom/sec/ims/options/ICapabilityService;

    iget-object v1, p0, Lcom/sec/ims/options/CapabilityManager;->mEventProxy:Lcom/sec/ims/options/ICapabilityServiceEventListener$Stub;

    invoke-interface {v0, v1}, Lcom/sec/ims/options/ICapabilityService;->unregisterListener(Lcom/sec/ims/options/ICapabilityServiceEventListener;)V

    .line 166
    return-void
.end method
