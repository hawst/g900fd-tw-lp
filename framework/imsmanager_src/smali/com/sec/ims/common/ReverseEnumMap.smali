.class public Lcom/sec/ims/common/ReverseEnumMap;
.super Ljava/lang/Object;
.source "ReverseEnumMap.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Enum",
        "<TE;>;:",
        "Lcom/sec/ims/common/IEnumerationWithId",
        "<TE;>;>",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final map:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Class;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 26
    .local p0, "this":Lcom/sec/ims/common/ReverseEnumMap;, "Lcom/sec/ims/common/ReverseEnumMap<TE;>;"
    .local p1, "type":Ljava/lang/Class;, "Ljava/lang/Class<TE;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-instance v4, Landroid/util/SparseArray;

    invoke-direct {v4}, Landroid/util/SparseArray;-><init>()V

    iput-object v4, p0, Lcom/sec/ims/common/ReverseEnumMap;->map:Landroid/util/SparseArray;

    .line 27
    invoke-virtual {p1}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_0

    .line 28
    new-instance v4, Ljava/lang/IllegalStateException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Trying to make ReverseEnumMap with non-enum class: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 32
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Enum;

    .local v0, "arr$":[Ljava/lang/Enum;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_2

    aget-object v1, v0, v2

    .line 33
    .local v1, "enumConstant":Ljava/lang/Enum;, "TE;"
    iget-object v5, p0, Lcom/sec/ims/common/ReverseEnumMap;->map:Landroid/util/SparseArray;

    move-object v4, v1

    check-cast v4, Lcom/sec/ims/common/IEnumerationWithId;

    invoke-interface {v4}, Lcom/sec/ims/common/IEnumerationWithId;->getId()I

    move-result v4

    invoke-virtual {v5, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 34
    new-instance v5, Ljava/lang/IllegalStateException;

    const-string v6, "Id %d already set to constant %s"

    const/4 v4, 0x2

    new-array v7, v4, [Ljava/lang/Object;

    const/4 v8, 0x0

    move-object v4, v1

    check-cast v4, Lcom/sec/ims/common/IEnumerationWithId;

    invoke-interface {v4}, Lcom/sec/ims/common/IEnumerationWithId;->getId()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v7, v8

    const/4 v8, 0x1

    iget-object v4, p0, Lcom/sec/ims/common/ReverseEnumMap;->map:Landroid/util/SparseArray;

    check-cast v1, Lcom/sec/ims/common/IEnumerationWithId;

    .end local v1    # "enumConstant":Ljava/lang/Enum;, "TE;"
    invoke-interface {v1}, Lcom/sec/ims/common/IEnumerationWithId;->getId()I

    move-result v9

    invoke-virtual {v4, v9}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Enum;

    invoke-virtual {v4}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 37
    .restart local v1    # "enumConstant":Ljava/lang/Enum;, "TE;"
    :cond_1
    iget-object v5, p0, Lcom/sec/ims/common/ReverseEnumMap;->map:Landroid/util/SparseArray;

    move-object v4, v1

    check-cast v4, Lcom/sec/ims/common/IEnumerationWithId;

    invoke-interface {v4}, Lcom/sec/ims/common/IEnumerationWithId;->getId()I

    move-result v4

    invoke-virtual {v5, v4, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 32
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 39
    .end local v1    # "enumConstant":Ljava/lang/Enum;, "TE;"
    :cond_2
    return-void
.end method


# virtual methods
.method public get(Ljava/lang/Integer;)Ljava/lang/Enum;
    .locals 4
    .param p1, "id"    # Ljava/lang/Integer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Integer;",
            ")TE;"
        }
    .end annotation

    .prologue
    .line 42
    .local p0, "this":Lcom/sec/ims/common/ReverseEnumMap;, "Lcom/sec/ims/common/ReverseEnumMap<TE;>;"
    iget-object v0, p0, Lcom/sec/ims/common/ReverseEnumMap;->map:Landroid/util/SparseArray;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 43
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "ID %d unknown in reverse enumeration map"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 46
    :cond_0
    iget-object v0, p0, Lcom/sec/ims/common/ReverseEnumMap;->map:Landroid/util/SparseArray;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Enum;

    return-object v0
.end method
