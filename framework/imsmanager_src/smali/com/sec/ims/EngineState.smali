.class public final enum Lcom/sec/ims/EngineState;
.super Ljava/lang/Enum;
.source "EngineState.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/ims/EngineState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/ims/EngineState;

.field public static final enum AWAITING_BEARER:Lcom/sec/ims/EngineState;

.field public static final enum BOOTED:Lcom/sec/ims/EngineState;

.field public static final enum BOOTING:Lcom/sec/ims/EngineState;

.field public static final enum MODIFYING:Lcom/sec/ims/EngineState;

.field public static final enum OFF:Lcom/sec/ims/EngineState;

.field public static final enum REGISTERED:Lcom/sec/ims/EngineState;

.field public static final enum SHUTTING_DOWN:Lcom/sec/ims/EngineState;

.field public static final enum UNREGISTERING:Lcom/sec/ims/EngineState;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 26
    new-instance v0, Lcom/sec/ims/EngineState;

    const-string v1, "OFF"

    invoke-direct {v0, v1, v3}, Lcom/sec/ims/EngineState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/EngineState;->OFF:Lcom/sec/ims/EngineState;

    .line 27
    new-instance v0, Lcom/sec/ims/EngineState;

    const-string v1, "SHUTTING_DOWN"

    invoke-direct {v0, v1, v4}, Lcom/sec/ims/EngineState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/EngineState;->SHUTTING_DOWN:Lcom/sec/ims/EngineState;

    .line 28
    new-instance v0, Lcom/sec/ims/EngineState;

    const-string v1, "BOOTING"

    invoke-direct {v0, v1, v5}, Lcom/sec/ims/EngineState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/EngineState;->BOOTING:Lcom/sec/ims/EngineState;

    .line 29
    new-instance v0, Lcom/sec/ims/EngineState;

    const-string v1, "BOOTED"

    invoke-direct {v0, v1, v6}, Lcom/sec/ims/EngineState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/EngineState;->BOOTED:Lcom/sec/ims/EngineState;

    .line 30
    new-instance v0, Lcom/sec/ims/EngineState;

    const-string v1, "MODIFYING"

    invoke-direct {v0, v1, v7}, Lcom/sec/ims/EngineState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/EngineState;->MODIFYING:Lcom/sec/ims/EngineState;

    .line 31
    new-instance v0, Lcom/sec/ims/EngineState;

    const-string v1, "REGISTERED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/EngineState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/EngineState;->REGISTERED:Lcom/sec/ims/EngineState;

    .line 32
    new-instance v0, Lcom/sec/ims/EngineState;

    const-string v1, "AWAITING_BEARER"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/EngineState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/EngineState;->AWAITING_BEARER:Lcom/sec/ims/EngineState;

    .line 33
    new-instance v0, Lcom/sec/ims/EngineState;

    const-string v1, "UNREGISTERING"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/EngineState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/EngineState;->UNREGISTERING:Lcom/sec/ims/EngineState;

    .line 25
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/sec/ims/EngineState;

    sget-object v1, Lcom/sec/ims/EngineState;->OFF:Lcom/sec/ims/EngineState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/ims/EngineState;->SHUTTING_DOWN:Lcom/sec/ims/EngineState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/ims/EngineState;->BOOTING:Lcom/sec/ims/EngineState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/ims/EngineState;->BOOTED:Lcom/sec/ims/EngineState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/ims/EngineState;->MODIFYING:Lcom/sec/ims/EngineState;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/ims/EngineState;->REGISTERED:Lcom/sec/ims/EngineState;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/ims/EngineState;->AWAITING_BEARER:Lcom/sec/ims/EngineState;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/ims/EngineState;->UNREGISTERING:Lcom/sec/ims/EngineState;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/ims/EngineState;->$VALUES:[Lcom/sec/ims/EngineState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/ims/EngineState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 25
    const-class v0, Lcom/sec/ims/EngineState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/ims/EngineState;

    return-object v0
.end method

.method public static values()[Lcom/sec/ims/EngineState;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/sec/ims/EngineState;->$VALUES:[Lcom/sec/ims/EngineState;

    invoke-virtual {v0}, [Lcom/sec/ims/EngineState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/ims/EngineState;

    return-object v0
.end method
