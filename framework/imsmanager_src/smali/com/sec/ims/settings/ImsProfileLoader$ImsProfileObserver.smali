.class Lcom/sec/ims/settings/ImsProfileLoader$ImsProfileObserver;
.super Landroid/database/ContentObserver;
.source "ImsProfileLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/ims/settings/ImsProfileLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ImsProfileObserver"
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/os/Handler;Landroid/content/Context;)V
    .locals 0
    .param p1, "handler"    # Landroid/os/Handler;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 534
    invoke-direct {p0, p1}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 535
    iput-object p2, p0, Lcom/sec/ims/settings/ImsProfileLoader$ImsProfileObserver;->mContext:Landroid/content/Context;

    .line 536
    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 1
    .param p1, "selfChange"    # Z

    .prologue
    .line 540
    iget-object v0, p0, Lcom/sec/ims/settings/ImsProfileLoader$ImsProfileObserver;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/ims/settings/ImsProfileLoader;->refreshImsProfileMap(Landroid/content/Context;)V

    .line 541
    return-void
.end method
