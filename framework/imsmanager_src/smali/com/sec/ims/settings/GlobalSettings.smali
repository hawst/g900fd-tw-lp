.class public Lcom/sec/ims/settings/GlobalSettings;
.super Ljava/lang/Object;
.source "GlobalSettings.java"


# static fields
.field public static final AUDIO_CAPABILITIES_NB_ONLY:I = 0x3

.field public static final AUDIO_CAPABILITIES_NB_PREF:I = 0x1

.field public static final AUDIO_CAPABILITIES_WB_ONLY:I = 0x2

.field public static final AUDIO_CAPABILITIES_WB_PREF:I = 0x0

.field public static final AUDIO_CODEC_BANDWIDTH_EFFICIENT:I = 0x0

.field public static final AUDIO_CODEC_BANDWIDTH_PREF:I = 0x2

.field public static final AUDIO_CODEC_MANUAL:I = 0x4

.field public static final AUDIO_CODEC_OCTET_ALIGNED:I = 0x1

.field public static final AUDIO_CODEC_OCTET_ALIGNED_PREF:I = 0x3

.field private static final AllEnabledLogLevel:Ljava/lang/String; = "1111111"

.field public static final DEFAULT_VOLTE_REGI_ICON_ID:Ljava/lang/String; = "stat_notify_volte_service_avaliable"

.field public static final DTMF_CODEC_ENABLED:I = 0x0

.field public static final DTMF_IN_BAND:I = 0x1

.field private static final DefaultEngLogLevel:Ljava/lang/String; = "1011101"

.field private static final DefaultUserLogLevel:Ljava/lang/String; = "0001100"

.field private static final DefaultmenablersLogLevel:Ljava/lang/StringBuilder;

.field private static final DefaultmframeWorkLogLevel:Ljava/lang/StringBuilder;

.field private static final DefaultmmediaLogLevel:Ljava/lang/StringBuilder;

.field private static final DefaultmmsrpLogLevel:Ljava/lang/StringBuilder;

.field private static final DefaultmplatformLogLevel:Ljava/lang/StringBuilder;

.field private static final DefaultmsipStackLogLevel:Ljava/lang/StringBuilder;

.field private static final ENABLERS:Ljava/lang/String; = "EnablerSvc"

.field private static final ENDDELEMETER:Ljava/lang/String; = ";"

.field private static final EQUALTO:Ljava/lang/String; = "-"

.field private static final FRAMEWORK:Ljava/lang/String; = "Framework"

.field public static final LOG_TAG:Ljava/lang/String; = "GlobalSettings"

.field private static final MEDIA:Ljava/lang/String; = "Media"

.field private static final MSRP:Ljava/lang/String; = "MSRP"

.field private static final PLATFORM:Ljava/lang/String; = "Platform"

.field public static final RCS_CONFIG_SOURCE_LOCAL_FILE:I = 0x2

.field public static final RCS_CONFIG_SOURCE_LOCAL_SERVER:I = 0x1

.field public static final RCS_CONFIG_SOURCE_REMOTE_SERVER:I = 0x0

.field public static final RCS_PREF_ALWAYS_ASK:I = 0x2

.field public static final RCS_PREF_ALWAYS_CONNECT:I = 0x1

.field public static final RCS_PREF_NEVER:I = 0x0

.field private static final SIPSTACK:Ljava/lang/String; = "SIPStack"

.field public static final URI:Ljava/lang/String; = "content://com.sec.ims.settings/global"

.field public static final XCAP_ROOT_URI_PREF_DB:I = 0x0

.field public static final XCAP_ROOT_URI_PREF_IMSI:I = 0x2

.field public static final XCAP_ROOT_URI_PREF_MSISDN:I = 0x1

.field public static final XCAP_ROOT_URI_PREF_MSISDN_WITH_DOMAIN:I = 0x3

.field private static final mAllEnabledStackLogLevel:Ljava/lang/String; = "EnablerSvc-1111111;Framework-1111111;SIPStack-1111111;MSRP-1111111;Media-1111111;Platform-1111111"

.field private static final mDefaultStackLogLevel:Ljava/lang/String;


# instance fields
.field private mAMRNBBEPayloadVal:Ljava/lang/Integer;

.field private mAMRNBOAPayloadVal:Ljava/lang/Integer;

.field private mAMRWBBEPayloadVal:Ljava/lang/Integer;

.field private mAMRWBOAPayloadVal:Ljava/lang/Integer;

.field private mAddPhoneExtension:I

.field private mAddPresListDummy:Ljava/lang/String;

.field private mAggProxyIp:Ljava/lang/String;

.field private mAmrBundleRate:Ljava/lang/Integer;

.field private mAmrNbMode:Ljava/lang/String;

.field private mAmrWbMode:Ljava/lang/String;

.field private mAnonymousFetch:Z

.field private mApnSelection:Ljava/lang/String;

.field private mAudioAS:Ljava/lang/Integer;

.field private mAudioCapabilities:Ljava/lang/String;

.field private mAudioCodecModeVal:Ljava/lang/String;

.field private mAudioCodecVal:Ljava/lang/String;

.field private mAudioPortEnd:Ljava/lang/Integer;

.field private mAudioPortStart:Ljava/lang/Integer;

.field private mAudioRR:Ljava/lang/Integer;

.field private mAudioRS:Ljava/lang/Integer;

.field private mAudioVideoTxVal:Ljava/lang/Integer;

.field private mAuthProxyIp:Ljava/lang/String;

.field private mAuthProxyPort:Ljava/lang/Integer;

.field private mBadEventExpiry:I

.field private mBitRate:Ljava/lang/Integer;

.field private mBsfIp:Ljava/lang/String;

.field private mBsfPort:Ljava/lang/String;

.field private mCapCacheExpiry:I

.field private mClosedGroupChat:Z

.field private mConfDialogType:Ljava/lang/String;

.field private mConfSubscribe:Ljava/lang/String;

.field private mConfUri:Ljava/lang/String;

.field private mCurrDispFormat:Ljava/lang/String;

.field private mDtmfCodecMode:Ljava/lang/String;

.field private mDtmfMode:Ljava/lang/Integer;

.field private mDtmfNBPayloadVal:Ljava/lang/Integer;

.field private mDtmfWBPayloadVal:Ljava/lang/Integer;

.field private mEnableAllLog:Z

.field private mEnableBwm:Ljava/lang/String;

.field private mEnableBwmDefault:Ljava/lang/String;

.field private mEnableCiq:Ljava/lang/Integer;

.field private mEnableCpAudioPath:Ljava/lang/Integer;

.field private mEnableG711Codec:Ljava/lang/String;

.field private mEnableGba:Ljava/lang/Integer;

.field private mEnableGruu:Ljava/lang/Integer;

.field private mEnableHasati:Ljava/lang/Integer;

.field private mEnableMwi:Ljava/lang/Integer;

.field private mEnforceGbaSupport:Z

.field private mExpireHeader:I

.field private mExtendedPublishTimer:I

.field private mFrameRate:Ljava/lang/Integer;

.field private mH264QvgaPayloadVal:Ljava/lang/Integer;

.field private mH264VgaLPayloadVal:Ljava/lang/Integer;

.field private mH264VgaPayloadVal:Ljava/lang/Integer;

.field private mHttpPassword:Ljava/lang/String;

.field private mHttpUserName:Ljava/lang/String;

.field private mId:I

.field private mImsConfigName:Ljava/lang/String;

.field private mImsTestMode:Z

.field private mInviteTimeout:Ljava/lang/Integer;

.field private mIsAVPFEnabled:Ljava/lang/Integer;

.field private mIsDispFpsBitrateEnabled:Ljava/lang/Integer;

.field private mIsEabEnabled:Z

.field private mIsEabPubDone:Z

.field private mIsEabPubFailed:Z

.field private mIsEnableEMM:Ljava/lang/Integer;

.field private mIsEnableLoopBack:Ljava/lang/Integer;

.field private mIsEnableSRTP:Ljava/lang/Integer;

.field private mIsExternalGTTY:Ljava/lang/Integer;

.field private mIsGZipEnable:I

.field private mIsLvcEnabled:Z

.field private mIsRcsEnabled:Z

.field private mIsVolteEnabled:Z

.field private mJitterBufferSettings:Ljava/lang/Integer;

.field private mLocalUriType:Ljava/lang/String;

.field private mLogLevel:I

.field private mLogRtpPacketsToFile:Ljava/lang/Integer;

.field private mMwiSubscribeExpiry:Ljava/lang/Integer;

.field private mName:Ljava/lang/String;

.field private mNetwork:Ljava/lang/String;

.field private mPacketizatoinMode:Ljava/lang/String;

.field private mPollListSubExpiry:I

.field private mPresenceListMaxEntries:Landroid/net/Uri;

.field private mPublishErrRetry:I

.field private mPublishExpiry:I

.field private mPublishTimer:I

.field private mRcsAutoconfigSource:I

.field private mRcsHomePreference:Ljava/lang/Integer;

.field private mRcsRoamingPreference:Ljava/lang/Integer;

.field private mRcsVersion:I

.field private mRegExpire:Ljava/lang/Integer;

.field private mRegRetryBaseTime:Ljava/lang/Integer;

.field private mRegRetryMaxTime:Ljava/lang/Integer;

.field private mRemoteUriType:Ljava/lang/String;

.field private mRingbackTimer:Ljava/lang/Integer;

.field private mRingingTimer:Ljava/lang/Integer;

.field private mRtcpTimeout:Ljava/lang/Integer;

.field private mRtpTimeout:Ljava/lang/Integer;

.field private mSelfPort:Ljava/lang/String;

.field private mSessionExpire:Ljava/lang/Integer;

.field private mSetDefaultUri:I

.field private mShowRegIcon:Z

.field private mSipUriOnly:Z

.field private mSipUserAgent:Ljava/lang/String;

.field private mSlickConfigName:Ljava/lang/String;

.field private mSmsFormat:Ljava/lang/String;

.field private mSmsOverIp:Ljava/lang/String;

.field private mSmsWriteUicc:Ljava/lang/String;

.field private mSubscribeForReg:Ljava/lang/Integer;

.field private mSubscribeListSyncEnabled:I

.field private mSubscriberTimer:Ljava/lang/Integer;

.field private mSubscriptionExpiry:I

.field private mSupportImsInRoaming:Z

.field private mTDelay:Ljava/lang/String;

.field private mTVolteHysTimer:Ljava/lang/Integer;

.field private mTextPort:Ljava/lang/Integer;

.field private mTransportInContact:Ljava/lang/Integer;

.field private mUpdateAutoConfig:Z

.field private mUsePrecondition:Ljava/lang/Integer;

.field private mUtApnName:Ljava/lang/String;

.field private mUtPdnType:Ljava/lang/String;

.field private mValues:Landroid/content/ContentValues;

.field private mVideoAS:Ljava/lang/Integer;

.field private mVideoCodecVal:Ljava/lang/String;

.field private mVideoPortEnd:Ljava/lang/Integer;

.field private mVideoPortStart:Ljava/lang/Integer;

.field private mVideoRR:Ljava/lang/Integer;

.field private mVideoRS:Ljava/lang/Integer;

.field private mVolteRegIconId:Ljava/lang/String;

.field private mVzwTimer:Ljava/lang/String;

.field private mWifiSelfPort:Ljava/lang/String;

.field private mXcapRootUri:Ljava/lang/String;

.field private mXcapRootUriPref:Ljava/lang/Integer;

.field private mXdmEnable:Ljava/lang/Integer;

.field private mXdmUserAgent:Landroid/net/Uri;

.field private mXdmUserIdDomain:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 233
    const-string v0, "eng"

    sget-object v1, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "1011101"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    :goto_0
    sput-object v0, Lcom/sec/ims/settings/GlobalSettings;->DefaultmenablersLogLevel:Ljava/lang/StringBuilder;

    .line 234
    const-string v0, "eng"

    sget-object v1, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "1011101"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    :goto_1
    sput-object v0, Lcom/sec/ims/settings/GlobalSettings;->DefaultmframeWorkLogLevel:Ljava/lang/StringBuilder;

    .line 235
    const-string v0, "eng"

    sget-object v1, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "1011101"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    :goto_2
    sput-object v0, Lcom/sec/ims/settings/GlobalSettings;->DefaultmsipStackLogLevel:Ljava/lang/StringBuilder;

    .line 236
    const-string v0, "eng"

    sget-object v1, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "1011101"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    :goto_3
    sput-object v0, Lcom/sec/ims/settings/GlobalSettings;->DefaultmmsrpLogLevel:Ljava/lang/StringBuilder;

    .line 237
    const-string v0, "eng"

    sget-object v1, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "1011101"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    :goto_4
    sput-object v0, Lcom/sec/ims/settings/GlobalSettings;->DefaultmmediaLogLevel:Ljava/lang/StringBuilder;

    .line 238
    const-string v0, "eng"

    sget-object v1, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "1011101"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    :goto_5
    sput-object v0, Lcom/sec/ims/settings/GlobalSettings;->DefaultmplatformLogLevel:Ljava/lang/StringBuilder;

    .line 249
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "EnablerSvc-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/sec/ims/settings/GlobalSettings;->DefaultmenablersLogLevel:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Framework"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/sec/ims/settings/GlobalSettings;->DefaultmframeWorkLogLevel:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "SIPStack"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/sec/ims/settings/GlobalSettings;->DefaultmsipStackLogLevel:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "MSRP"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/sec/ims/settings/GlobalSettings;->DefaultmmsrpLogLevel:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Media"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/sec/ims/settings/GlobalSettings;->DefaultmmediaLogLevel:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Platform"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/sec/ims/settings/GlobalSettings;->DefaultmplatformLogLevel:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/ims/settings/GlobalSettings;->mDefaultStackLogLevel:Ljava/lang/String;

    return-void

    .line 233
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "0001100"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 234
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "0001100"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 235
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "0001100"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 236
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "0001100"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 237
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "0001100"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    goto/16 :goto_4

    .line 238
    :cond_5
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "0001100"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    goto/16 :goto_5
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/16 v3, 0xe10

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 265
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 136
    iput-boolean v1, p0, Lcom/sec/ims/settings/GlobalSettings;->mImsTestMode:Z

    .line 196
    iput-boolean v2, p0, Lcom/sec/ims/settings/GlobalSettings;->mIsVolteEnabled:Z

    .line 197
    iput-boolean v2, p0, Lcom/sec/ims/settings/GlobalSettings;->mIsLvcEnabled:Z

    .line 198
    iput-boolean v2, p0, Lcom/sec/ims/settings/GlobalSettings;->mIsEabEnabled:Z

    .line 199
    iput-boolean v1, p0, Lcom/sec/ims/settings/GlobalSettings;->mIsEabPubFailed:Z

    .line 200
    iput-boolean v1, p0, Lcom/sec/ims/settings/GlobalSettings;->mIsEabPubDone:Z

    .line 266
    iput-boolean v2, p0, Lcom/sec/ims/settings/GlobalSettings;->mIsRcsEnabled:Z

    .line 267
    const/16 v0, 0x270f

    iput v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mRcsVersion:I

    .line 268
    iput v1, p0, Lcom/sec/ims/settings/GlobalSettings;->mRcsAutoconfigSource:I

    .line 269
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mRcsHomePreference:Ljava/lang/Integer;

    .line 270
    const/4 v0, 0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mRcsRoamingPreference:Ljava/lang/Integer;

    .line 271
    iput-boolean v1, p0, Lcom/sec/ims/settings/GlobalSettings;->mClosedGroupChat:Z

    .line 272
    const-string v0, "sip"

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mLocalUriType:Ljava/lang/String;

    .line 273
    const-string v0, "sip"

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mRemoteUriType:Ljava/lang/String;

    .line 274
    const/16 v0, 0xff

    iput v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mLogLevel:I

    .line 275
    iput v3, p0, Lcom/sec/ims/settings/GlobalSettings;->mPublishExpiry:I

    .line 276
    const v0, 0x15180

    iput v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mExtendedPublishTimer:I

    .line 277
    iput v2, p0, Lcom/sec/ims/settings/GlobalSettings;->mIsGZipEnable:I

    .line 278
    iput v3, p0, Lcom/sec/ims/settings/GlobalSettings;->mSubscriptionExpiry:I

    .line 279
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mRegExpire:Ljava/lang/Integer;

    .line 280
    const/16 v0, 0xb4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mInviteTimeout:Ljava/lang/Integer;

    .line 281
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mDtmfMode:Ljava/lang/Integer;

    .line 282
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mEnableMwi:Ljava/lang/Integer;

    .line 283
    const/16 v0, 0x1c20

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mMwiSubscribeExpiry:Ljava/lang/Integer;

    .line 284
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mEnableGruu:Ljava/lang/Integer;

    .line 285
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mLogRtpPacketsToFile:Ljava/lang/Integer;

    .line 286
    iput v1, p0, Lcom/sec/ims/settings/GlobalSettings;->mBadEventExpiry:I

    .line 287
    iput v1, p0, Lcom/sec/ims/settings/GlobalSettings;->mCapCacheExpiry:I

    .line 288
    iput-boolean v1, p0, Lcom/sec/ims/settings/GlobalSettings;->mAnonymousFetch:Z

    .line 289
    iput v1, p0, Lcom/sec/ims/settings/GlobalSettings;->mPublishTimer:I

    .line 290
    iput-boolean v1, p0, Lcom/sec/ims/settings/GlobalSettings;->mUpdateAutoConfig:Z

    .line 291
    iput v1, p0, Lcom/sec/ims/settings/GlobalSettings;->mPublishErrRetry:I

    .line 292
    return-void
.end method

.method public constructor <init>(Landroid/content/ContentValues;)V
    .locals 4
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 295
    invoke-direct {p0}, Lcom/sec/ims/settings/GlobalSettings;-><init>()V

    .line 296
    iput-object p1, p0, Lcom/sec/ims/settings/GlobalSettings;->mValues:Landroid/content/ContentValues;

    .line 298
    const-string v0, "_id"

    const/4 v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mId:I

    .line 299
    const-string v0, "name"

    invoke-direct {p0, p1, v0}, Lcom/sec/ims/settings/GlobalSettings;->getStringValue(Landroid/content/ContentValues;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mName:Ljava/lang/String;

    .line 300
    const-string v0, "network"

    iget-object v1, p0, Lcom/sec/ims/settings/GlobalSettings;->mNetwork:Ljava/lang/String;

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/ims/settings/GlobalSettings;->getStringValue(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mNetwork:Ljava/lang/String;

    .line 301
    const-string v0, "rcs"

    iget-boolean v1, p0, Lcom/sec/ims/settings/GlobalSettings;->mIsRcsEnabled:Z

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/ims/settings/GlobalSettings;->getBooleanValue(Landroid/content/ContentValues;Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mIsRcsEnabled:Z

    .line 302
    const-string v0, "rcs_ver"

    iget v1, p0, Lcom/sec/ims/settings/GlobalSettings;->mRcsVersion:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mRcsVersion:I

    .line 303
    const-string v0, "rcs_local_config_server"

    iget v1, p0, Lcom/sec/ims/settings/GlobalSettings;->mRcsAutoconfigSource:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mRcsAutoconfigSource:I

    .line 304
    const-string v0, "rcs_home_pref"

    iget-object v1, p0, Lcom/sec/ims/settings/GlobalSettings;->mRcsHomePreference:Ljava/lang/Integer;

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mRcsHomePreference:Ljava/lang/Integer;

    .line 305
    const-string v0, "rcs_roaming_pref"

    iget-object v1, p0, Lcom/sec/ims/settings/GlobalSettings;->mRcsRoamingPreference:Ljava/lang/Integer;

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mRcsRoamingPreference:Ljava/lang/Integer;

    .line 306
    const-string v0, "closed_group_chat"

    iget-boolean v1, p0, Lcom/sec/ims/settings/GlobalSettings;->mClosedGroupChat:Z

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/ims/settings/GlobalSettings;->getBooleanValue(Landroid/content/ContentValues;Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mClosedGroupChat:Z

    .line 307
    const-string v0, "local_uri_type"

    iget-object v1, p0, Lcom/sec/ims/settings/GlobalSettings;->mLocalUriType:Ljava/lang/String;

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/ims/settings/GlobalSettings;->getStringValue(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mLocalUriType:Ljava/lang/String;

    .line 308
    const-string v0, "remote_uri_type"

    iget-object v1, p0, Lcom/sec/ims/settings/GlobalSettings;->mRemoteUriType:Ljava/lang/String;

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/ims/settings/GlobalSettings;->getStringValue(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mRemoteUriType:Ljava/lang/String;

    .line 309
    const-string v0, "useragent"

    iget-object v1, p0, Lcom/sec/ims/settings/GlobalSettings;->mSipUserAgent:Ljava/lang/String;

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/ims/settings/GlobalSettings;->getStringValue(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mSipUserAgent:Ljava/lang/String;

    .line 310
    const-string v0, "subscribe_for_reg"

    invoke-direct {p0, p1, v0}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mSubscribeForReg:Ljava/lang/Integer;

    .line 311
    const-string v0, "use_precondition"

    invoke-direct {p0, p1, v0}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mUsePrecondition:Ljava/lang/Integer;

    .line 312
    const-string v0, "log_level"

    iget v1, p0, Lcom/sec/ims/settings/GlobalSettings;->mLogLevel:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mLogLevel:I

    .line 313
    const-string v0, "publish_expiry"

    iget v1, p0, Lcom/sec/ims/settings/GlobalSettings;->mPublishExpiry:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mPublishExpiry:I

    .line 314
    const-string v0, "extended_publish_timer"

    iget v1, p0, Lcom/sec/ims/settings/GlobalSettings;->mExtendedPublishTimer:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mExtendedPublishTimer:I

    .line 315
    const-string v0, "enable_gzip"

    iget v1, p0, Lcom/sec/ims/settings/GlobalSettings;->mIsGZipEnable:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mIsGZipEnable:I

    .line 316
    const-string v0, "expire_header"

    iget v1, p0, Lcom/sec/ims/settings/GlobalSettings;->mExpireHeader:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mExpireHeader:I

    .line 317
    const-string v0, "set_default_uri"

    iget v1, p0, Lcom/sec/ims/settings/GlobalSettings;->mSetDefaultUri:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mSetDefaultUri:I

    .line 318
    const-string v0, "subscribe_list_sync_enabled"

    iget v1, p0, Lcom/sec/ims/settings/GlobalSettings;->mSubscribeListSyncEnabled:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mSubscribeListSyncEnabled:I

    .line 320
    const-string v0, "add_phone_extension"

    iget v1, p0, Lcom/sec/ims/settings/GlobalSettings;->mAddPhoneExtension:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mAddPhoneExtension:I

    .line 321
    const-string v0, "add_pres_list_dummy"

    invoke-direct {p0, p1, v0}, Lcom/sec/ims/settings/GlobalSettings;->getStringValue(Landroid/content/ContentValues;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mAddPresListDummy:Ljava/lang/String;

    .line 322
    const-string v0, "subscription_expiry"

    iget v1, p0, Lcom/sec/ims/settings/GlobalSettings;->mSubscriptionExpiry:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mSubscriptionExpiry:I

    .line 323
    const-string v0, "anonymous_fetch"

    iget-boolean v1, p0, Lcom/sec/ims/settings/GlobalSettings;->mAnonymousFetch:Z

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/ims/settings/GlobalSettings;->getBooleanValue(Landroid/content/ContentValues;Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mAnonymousFetch:Z

    .line 324
    const-string v0, "bad_event_expiry"

    iget v1, p0, Lcom/sec/ims/settings/GlobalSettings;->mBadEventExpiry:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mBadEventExpiry:I

    .line 325
    const-string v0, "publish_timer"

    iget v1, p0, Lcom/sec/ims/settings/GlobalSettings;->mPublishTimer:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mPublishTimer:I

    .line 326
    const-string v0, "update_autoconfig"

    iget-boolean v1, p0, Lcom/sec/ims/settings/GlobalSettings;->mUpdateAutoConfig:Z

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/ims/settings/GlobalSettings;->getBooleanValue(Landroid/content/ContentValues;Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mUpdateAutoConfig:Z

    .line 327
    const-string v0, "cap_cache_exp"

    iget v1, p0, Lcom/sec/ims/settings/GlobalSettings;->mCapCacheExpiry:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mCapCacheExpiry:I

    .line 328
    const-string v0, "publish_err_retry_timer"

    iget v1, p0, Lcom/sec/ims/settings/GlobalSettings;->mPublishErrRetry:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mPublishErrRetry:I

    .line 329
    const-string v0, "poll_list_sub_exp"

    iget v1, p0, Lcom/sec/ims/settings/GlobalSettings;->mPollListSubExpiry:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mPollListSubExpiry:I

    .line 331
    const-string v0, "self_port"

    const-string v1, "5060"

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/ims/settings/GlobalSettings;->getStringValue(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mSelfPort:Ljava/lang/String;

    .line 332
    const-string v0, "wifi_self_port"

    const-string v1, "5060"

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/ims/settings/GlobalSettings;->getStringValue(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mWifiSelfPort:Ljava/lang/String;

    .line 334
    const-string v0, "reg_expires"

    iget-object v1, p0, Lcom/sec/ims/settings/GlobalSettings;->mRegExpire:Ljava/lang/Integer;

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mRegExpire:Ljava/lang/Integer;

    .line 335
    const-string v0, "xdm_enable"

    iget-object v1, p0, Lcom/sec/ims/settings/GlobalSettings;->mXdmEnable:Ljava/lang/Integer;

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mXdmEnable:Ljava/lang/Integer;

    .line 336
    const-string v0, "http_username"

    invoke-direct {p0, p1, v0, v2}, Lcom/sec/ims/settings/GlobalSettings;->getStringValue(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mHttpUserName:Ljava/lang/String;

    .line 337
    const-string v0, "http_password"

    invoke-direct {p0, p1, v0, v2}, Lcom/sec/ims/settings/GlobalSettings;->getStringValue(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mHttpPassword:Ljava/lang/String;

    .line 338
    const-string v0, "xcap_root_uri"

    invoke-direct {p0, p1, v0, v2}, Lcom/sec/ims/settings/GlobalSettings;->getStringValue(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mXcapRootUri:Ljava/lang/String;

    .line 339
    const-string v0, "agg_proxy_ip"

    invoke-direct {p0, p1, v0, v2}, Lcom/sec/ims/settings/GlobalSettings;->getStringValue(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mAggProxyIp:Ljava/lang/String;

    .line 340
    const-string v0, "auth_proxy_ip"

    invoke-direct {p0, p1, v0, v2}, Lcom/sec/ims/settings/GlobalSettings;->getStringValue(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mAuthProxyIp:Ljava/lang/String;

    .line 341
    const-string v0, "auth_proxy_port"

    invoke-direct {p0, p1, v0}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mAuthProxyPort:Ljava/lang/Integer;

    .line 342
    const-string v0, "apn_selection"

    invoke-direct {p0, p1, v0, v2}, Lcom/sec/ims/settings/GlobalSettings;->getStringValue(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mApnSelection:Ljava/lang/String;

    .line 343
    const-string v0, "xcap_root_uri_pref"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mXcapRootUriPref:Ljava/lang/Integer;

    .line 344
    const-string v0, "xdm_user_id_domain"

    invoke-direct {p0, p1, v0}, Lcom/sec/ims/settings/GlobalSettings;->getStringValue(Landroid/content/ContentValues;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mXdmUserIdDomain:Ljava/lang/String;

    .line 345
    const-string v0, "xdm_user_agent"

    invoke-direct {p0, p1, v0, v2}, Lcom/sec/ims/settings/GlobalSettings;->getUriValue(Landroid/content/ContentValues;Ljava/lang/String;Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mXdmUserAgent:Landroid/net/Uri;

    .line 346
    const-string v0, "bsf_ip"

    invoke-direct {p0, p1, v0, v2}, Lcom/sec/ims/settings/GlobalSettings;->getStringValue(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mBsfIp:Ljava/lang/String;

    .line 347
    const-string v0, "bsf_port"

    invoke-direct {p0, p1, v0, v2}, Lcom/sec/ims/settings/GlobalSettings;->getStringValue(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mBsfPort:Ljava/lang/String;

    .line 348
    const-string v0, "enable_gba"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mEnableGba:Ljava/lang/Integer;

    .line 350
    const-string v0, "conference_uri"

    invoke-direct {p0, p1, v0}, Lcom/sec/ims/settings/GlobalSettings;->getStringValue(Landroid/content/ContentValues;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mConfUri:Ljava/lang/String;

    .line 351
    const-string v0, "conference_subscribe"

    invoke-direct {p0, p1, v0}, Lcom/sec/ims/settings/GlobalSettings;->getStringValue(Landroid/content/ContentValues;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mConfSubscribe:Ljava/lang/String;

    .line 352
    const-string v0, "conference_dialog_type"

    invoke-direct {p0, p1, v0}, Lcom/sec/ims/settings/GlobalSettings;->getStringValue(Landroid/content/ContentValues;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mConfDialogType:Ljava/lang/String;

    .line 353
    const-string v0, "reg_retry_base_time"

    const/16 v1, 0x1e

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mRegRetryBaseTime:Ljava/lang/Integer;

    .line 354
    const-string v0, "reg_retry_max_time"

    const/16 v1, 0x708

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mRegRetryMaxTime:Ljava/lang/Integer;

    .line 356
    const-string v0, "session_expires"

    invoke-direct {p0, p1, v0}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mSessionExpire:Ljava/lang/Integer;

    .line 357
    const-string v0, "ringing_timer"

    invoke-direct {p0, p1, v0}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mRingingTimer:Ljava/lang/Integer;

    .line 358
    const-string v0, "ringback_timer"

    invoke-direct {p0, p1, v0}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mRingbackTimer:Ljava/lang/Integer;

    .line 359
    const-string v0, "subscriber_timer"

    invoke-direct {p0, p1, v0}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mSubscriberTimer:Ljava/lang/Integer;

    .line 360
    const-string v0, "transport_in_contact"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mTransportInContact:Ljava/lang/Integer;

    .line 362
    const-string v0, "rtp_timeout"

    invoke-direct {p0, p1, v0}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mRtpTimeout:Ljava/lang/Integer;

    .line 363
    const-string v0, "rtcp_timeout"

    invoke-direct {p0, p1, v0}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mRtcpTimeout:Ljava/lang/Integer;

    .line 364
    const-string v0, "enable_cp_audio_path"

    invoke-direct {p0, p1, v0}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mEnableCpAudioPath:Ljava/lang/Integer;

    .line 365
    const-string v0, "amr_bundle_rate"

    invoke-direct {p0, p1, v0}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mAmrBundleRate:Ljava/lang/Integer;

    .line 366
    const-string v0, "log_rtppackets_to_file"

    invoke-direct {p0, p1, v0}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mLogRtpPacketsToFile:Ljava/lang/Integer;

    .line 367
    const-string v0, "bitrate"

    invoke-direct {p0, p1, v0}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mBitRate:Ljava/lang/Integer;

    .line 368
    const-string v0, "audio_codec_mode"

    invoke-direct {p0, p1, v0}, Lcom/sec/ims/settings/GlobalSettings;->getStringValue(Landroid/content/ContentValues;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mAudioCodecModeVal:Ljava/lang/String;

    .line 369
    const-string v0, "audio_video_tx"

    invoke-direct {p0, p1, v0}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mAudioVideoTxVal:Ljava/lang/Integer;

    .line 370
    const-string v0, "amrnb_mode"

    invoke-direct {p0, p1, v0}, Lcom/sec/ims/settings/GlobalSettings;->getStringValue(Landroid/content/ContentValues;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mAmrNbMode:Ljava/lang/String;

    .line 371
    const-string v0, "amrwb_mode"

    invoke-direct {p0, p1, v0}, Lcom/sec/ims/settings/GlobalSettings;->getStringValue(Landroid/content/ContentValues;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mAmrWbMode:Ljava/lang/String;

    .line 372
    const-string v0, "framerate"

    invoke-direct {p0, p1, v0}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mFrameRate:Ljava/lang/Integer;

    .line 373
    const-string v0, "display_format"

    invoke-direct {p0, p1, v0}, Lcom/sec/ims/settings/GlobalSettings;->getStringValue(Landroid/content/ContentValues;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mCurrDispFormat:Ljava/lang/String;

    .line 374
    const-string v0, "audio_codec"

    invoke-direct {p0, p1, v0}, Lcom/sec/ims/settings/GlobalSettings;->getStringValue(Landroid/content/ContentValues;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mAudioCodecVal:Ljava/lang/String;

    .line 375
    const-string v0, "video_codec"

    invoke-direct {p0, p1, v0}, Lcom/sec/ims/settings/GlobalSettings;->getStringValue(Landroid/content/ContentValues;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mVideoCodecVal:Ljava/lang/String;

    .line 376
    const-string v0, "audio_port_start"

    invoke-direct {p0, p1, v0}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mAudioPortStart:Ljava/lang/Integer;

    .line 377
    const-string v0, "audio_port_end"

    invoke-direct {p0, p1, v0}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mAudioPortEnd:Ljava/lang/Integer;

    .line 378
    const-string v0, "video_port_start"

    invoke-direct {p0, p1, v0}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mVideoPortStart:Ljava/lang/Integer;

    .line 379
    const-string v0, "video_port_end"

    invoke-direct {p0, p1, v0}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mVideoPortEnd:Ljava/lang/Integer;

    .line 381
    const-string v0, "text_port"

    invoke-direct {p0, p1, v0}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mTextPort:Ljava/lang/Integer;

    .line 382
    const-string v0, "jitter_buffer"

    invoke-direct {p0, p1, v0}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mJitterBufferSettings:Ljava/lang/Integer;

    .line 383
    const-string v0, "presence_list_max_entries"

    invoke-direct {p0, p1, v0, v2}, Lcom/sec/ims/settings/GlobalSettings;->getUriValue(Landroid/content/ContentValues;Ljava/lang/String;Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mPresenceListMaxEntries:Landroid/net/Uri;

    .line 384
    const-string v0, "emm"

    invoke-direct {p0, p1, v0}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mIsEnableEMM:Ljava/lang/Integer;

    .line 385
    const-string v0, "loopback"

    invoke-direct {p0, p1, v0}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mIsEnableLoopBack:Ljava/lang/Integer;

    .line 386
    const-string v0, "external_gtty"

    invoke-direct {p0, p1, v0}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mIsExternalGTTY:Ljava/lang/Integer;

    .line 387
    const-string v0, "avpf"

    invoke-direct {p0, p1, v0}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mIsAVPFEnabled:Ljava/lang/Integer;

    .line 388
    const-string v0, "srtp"

    invoke-direct {p0, p1, v0}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mIsEnableSRTP:Ljava/lang/Integer;

    .line 389
    const-string v0, "display_fps_bitrate"

    invoke-direct {p0, p1, v0}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mIsDispFpsBitrateEnabled:Ljava/lang/Integer;

    .line 390
    const-string v0, "audio_as"

    invoke-direct {p0, p1, v0}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mAudioAS:Ljava/lang/Integer;

    .line 391
    const-string v0, "audio_rs"

    invoke-direct {p0, p1, v0}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mAudioRS:Ljava/lang/Integer;

    .line 392
    const-string v0, "audio_rr"

    invoke-direct {p0, p1, v0}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mAudioRR:Ljava/lang/Integer;

    .line 393
    const-string v0, "video_as"

    invoke-direct {p0, p1, v0}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mVideoAS:Ljava/lang/Integer;

    .line 394
    const-string v0, "video_rs"

    invoke-direct {p0, p1, v0}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mVideoRS:Ljava/lang/Integer;

    .line 395
    const-string v0, "video_rr"

    invoke-direct {p0, p1, v0}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mVideoRR:Ljava/lang/Integer;

    .line 396
    const-string v0, "amrnbbe_payload"

    invoke-direct {p0, p1, v0}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mAMRNBBEPayloadVal:Ljava/lang/Integer;

    .line 397
    const-string v0, "amrnboa_payload"

    invoke-direct {p0, p1, v0}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mAMRNBOAPayloadVal:Ljava/lang/Integer;

    .line 398
    const-string v0, "amrwbbe_payload"

    invoke-direct {p0, p1, v0}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mAMRWBBEPayloadVal:Ljava/lang/Integer;

    .line 399
    const-string v0, "amrwboa_payload"

    invoke-direct {p0, p1, v0}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mAMRWBOAPayloadVal:Ljava/lang/Integer;

    .line 400
    const-string v0, "h264_qvga_payload"

    invoke-direct {p0, p1, v0}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mH264QvgaPayloadVal:Ljava/lang/Integer;

    .line 401
    const-string v0, "h264_vga_payload"

    invoke-direct {p0, p1, v0}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mH264VgaPayloadVal:Ljava/lang/Integer;

    .line 402
    const-string v0, "h264_vgal_payload"

    invoke-direct {p0, p1, v0}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mH264VgaLPayloadVal:Ljava/lang/Integer;

    .line 403
    const-string v0, "dtmf_nb_payload"

    invoke-direct {p0, p1, v0}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mDtmfNBPayloadVal:Ljava/lang/Integer;

    .line 404
    const-string v0, "dtmf_wb_payload"

    invoke-direct {p0, p1, v0}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mDtmfWBPayloadVal:Ljava/lang/Integer;

    .line 405
    const-string v0, "packetization_mode"

    invoke-direct {p0, p1, v0}, Lcom/sec/ims/settings/GlobalSettings;->getStringValue(Landroid/content/ContentValues;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mPacketizatoinMode:Ljava/lang/String;

    .line 406
    const-string v0, "enable_g711"

    const-string v1, "false"

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/ims/settings/GlobalSettings;->getStringValue(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mEnableG711Codec:Ljava/lang/String;

    .line 407
    const-string v0, "dtmf_codec_mode"

    invoke-direct {p0, p1, v0}, Lcom/sec/ims/settings/GlobalSettings;->getStringValue(Landroid/content/ContentValues;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mDtmfCodecMode:Ljava/lang/String;

    .line 408
    const-string v0, "enable_bwm"

    const-string v1, "1"

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/ims/settings/GlobalSettings;->getStringValue(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mEnableBwm:Ljava/lang/String;

    .line 409
    const-string v0, "enable_bwm_default"

    const-string v1, "1"

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/ims/settings/GlobalSettings;->getStringValue(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mEnableBwmDefault:Ljava/lang/String;

    .line 410
    const-string v0, "audio_capabilities"

    const-string v1, "0"

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/ims/settings/GlobalSettings;->getStringValue(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mAudioCapabilities:Ljava/lang/String;

    .line 412
    const-string v0, "invite_timeout"

    iget-object v1, p0, Lcom/sec/ims/settings/GlobalSettings;->mInviteTimeout:Ljava/lang/Integer;

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mInviteTimeout:Ljava/lang/Integer;

    .line 413
    const-string v0, "dtmf_mode"

    iget-object v1, p0, Lcom/sec/ims/settings/GlobalSettings;->mDtmfMode:Ljava/lang/Integer;

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mDtmfMode:Ljava/lang/Integer;

    .line 414
    const-string v0, "enable_mwi"

    iget-object v1, p0, Lcom/sec/ims/settings/GlobalSettings;->mEnableMwi:Ljava/lang/Integer;

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mEnableMwi:Ljava/lang/Integer;

    .line 415
    const-string v0, "mwi_subscribe_expiry"

    iget-object v1, p0, Lcom/sec/ims/settings/GlobalSettings;->mMwiSubscribeExpiry:Ljava/lang/Integer;

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mMwiSubscribeExpiry:Ljava/lang/Integer;

    .line 416
    const-string v0, "enable_ciq"

    iget-object v1, p0, Lcom/sec/ims/settings/GlobalSettings;->mEnableCiq:Ljava/lang/Integer;

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mEnableCiq:Ljava/lang/Integer;

    .line 417
    const-string v0, "enable_hasati"

    iget-object v1, p0, Lcom/sec/ims/settings/GlobalSettings;->mEnableHasati:Ljava/lang/Integer;

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mEnableHasati:Ljava/lang/Integer;

    .line 418
    const-string v0, "enable_gruu"

    iget-object v1, p0, Lcom/sec/ims/settings/GlobalSettings;->mEnableGruu:Ljava/lang/Integer;

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mEnableGruu:Ljava/lang/Integer;

    .line 419
    const-string v0, "ims_test_mode"

    invoke-direct {p0, p1, v0, v3}, Lcom/sec/ims/settings/GlobalSettings;->getBooleanValue(Landroid/content/ContentValues;Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mImsTestMode:Z

    .line 420
    const-string v0, "enable_all_logs"

    invoke-direct {p0, p1, v0, v3}, Lcom/sec/ims/settings/GlobalSettings;->getBooleanValue(Landroid/content/ContentValues;Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mEnableAllLog:Z

    .line 421
    const-string v0, "sip_uri_only"

    invoke-direct {p0, p1, v0, v3}, Lcom/sec/ims/settings/GlobalSettings;->getBooleanValue(Landroid/content/ContentValues;Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mSipUriOnly:Z

    .line 422
    const-string v0, "enforce_gba_support"

    invoke-direct {p0, p1, v0, v3}, Lcom/sec/ims/settings/GlobalSettings;->getBooleanValue(Landroid/content/ContentValues;Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mEnforceGbaSupport:Z

    .line 423
    const-string v0, "volte_enabled"

    iget-boolean v1, p0, Lcom/sec/ims/settings/GlobalSettings;->mIsVolteEnabled:Z

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/ims/settings/GlobalSettings;->getBooleanValue(Landroid/content/ContentValues;Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mIsVolteEnabled:Z

    .line 424
    const-string v0, "lvc_enabled"

    iget-boolean v1, p0, Lcom/sec/ims/settings/GlobalSettings;->mIsLvcEnabled:Z

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/ims/settings/GlobalSettings;->getBooleanValue(Landroid/content/ContentValues;Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mIsLvcEnabled:Z

    .line 425
    const-string v0, "eab_enabled"

    iget-boolean v1, p0, Lcom/sec/ims/settings/GlobalSettings;->mIsEabEnabled:Z

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/ims/settings/GlobalSettings;->getBooleanValue(Landroid/content/ContentValues;Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mIsEabEnabled:Z

    .line 426
    const-string v0, "vzw_eab_publish_fail"

    iget-boolean v1, p0, Lcom/sec/ims/settings/GlobalSettings;->mIsEabPubFailed:Z

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/ims/settings/GlobalSettings;->getBooleanValue(Landroid/content/ContentValues;Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mIsEabPubFailed:Z

    .line 427
    const-string v0, "vzw_eab_publish_done"

    iget-boolean v1, p0, Lcom/sec/ims/settings/GlobalSettings;->mIsEabPubDone:Z

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/ims/settings/GlobalSettings;->getBooleanValue(Landroid/content/ContentValues;Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mIsEabPubDone:Z

    .line 428
    const-string v0, "show_volte_regi_icon"

    invoke-direct {p0, p1, v0, v3}, Lcom/sec/ims/settings/GlobalSettings;->getBooleanValue(Landroid/content/ContentValues;Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mShowRegIcon:Z

    .line 429
    const-string v0, "volte_regi_icon_id"

    const-string v1, "stat_notify_volte_service_avaliable"

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/ims/settings/GlobalSettings;->getStringValue(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mVolteRegIconId:Ljava/lang/String;

    .line 430
    const-string v0, "ut_pdn"

    iget-object v1, p0, Lcom/sec/ims/settings/GlobalSettings;->mUtPdnType:Ljava/lang/String;

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/ims/settings/GlobalSettings;->getStringValue(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mUtPdnType:Ljava/lang/String;

    .line 431
    const-string v0, "ut_apn_name"

    iget-object v1, p0, Lcom/sec/ims/settings/GlobalSettings;->mUtApnName:Ljava/lang/String;

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/ims/settings/GlobalSettings;->getStringValue(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mUtApnName:Ljava/lang/String;

    .line 432
    const-string v0, "sms_format"

    const-string v1, "3GPP"

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/ims/settings/GlobalSettings;->getStringValue(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mSmsFormat:Ljava/lang/String;

    .line 433
    const-string v0, "sms_write_uicc"

    const-string v1, "0"

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/ims/settings/GlobalSettings;->getStringValue(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mSmsWriteUicc:Ljava/lang/String;

    .line 434
    const-string v0, "sms_over_ip_indication"

    const-string v1, "1"

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/ims/settings/GlobalSettings;->getStringValue(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mSmsOverIp:Ljava/lang/String;

    .line 435
    const-string v0, "timer_vzw"

    const-string v1, "6"

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/ims/settings/GlobalSettings;->getStringValue(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mVzwTimer:Ljava/lang/String;

    .line 436
    const-string v0, "t_delay"

    const-string v1, "0"

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/ims/settings/GlobalSettings;->getStringValue(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mTDelay:Ljava/lang/String;

    .line 437
    const-string v0, "support_ims_in_roaming"

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/ims/settings/GlobalSettings;->getBooleanValue(Landroid/content/ContentValues;Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mSupportImsInRoaming:Z

    .line 438
    const-string v0, "tvolte_hys_timer"

    const/16 v1, 0x3c

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mTVolteHysTimer:Ljava/lang/Integer;

    .line 440
    const-string v0, "ims_config_name"

    const-string v1, "Default"

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/ims/settings/GlobalSettings;->getStringValue(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mImsConfigName:Ljava/lang/String;

    .line 441
    const-string v0, "slick_config_name"

    const-string v1, ""

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/ims/settings/GlobalSettings;->getStringValue(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mSlickConfigName:Ljava/lang/String;

    .line 443
    return-void
.end method

.method public static _getInstance(Landroid/content/Context;)Lcom/sec/ims/settings/GlobalSettings;
    .locals 9
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    .line 492
    const/4 v2, 0x0

    .line 493
    .local v2, "name":Ljava/lang/String;
    const/4 v3, 0x0

    .line 496
    .local v3, "network":Ljava/lang/String;
    const-string v6, "phone"

    invoke-virtual {p0, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/telephony/TelephonyManager;

    .line 497
    .local v5, "tm":Landroid/telephony/TelephonyManager;
    invoke-virtual {v5}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    move-result v6

    invoke-static {v6}, Lcom/sec/ims/settings/ImsProfile;->getNetworkName(I)Ljava/lang/String;

    move-result-object v3

    .line 499
    invoke-static {p0}, Lcom/sec/ims/settings/ImsProfileLoader;->getProfileList(Landroid/content/Context;)Ljava/util/List;

    move-result-object v1

    .line 502
    .local v1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/sec/ims/settings/ImsProfile;>;"
    if-eqz v1, :cond_2

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v6

    if-lez v6, :cond_2

    .line 503
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_1

    .line 504
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/ims/settings/ImsProfile;

    .line 505
    .local v4, "profile":Lcom/sec/ims/settings/ImsProfile;
    invoke-virtual {v4}, Lcom/sec/ims/settings/ImsProfile;->getNetworkNameSet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 506
    invoke-virtual {v4}, Lcom/sec/ims/settings/ImsProfile;->getGlobalSettingsName()Ljava/lang/String;

    move-result-object v2

    .line 512
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v4    # "profile":Lcom/sec/ims/settings/ImsProfile;
    :cond_1
    if-nez v2, :cond_2

    .line 513
    const/4 v6, 0x0

    invoke-interface {v1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/ims/settings/ImsProfile;

    invoke-virtual {v6}, Lcom/sec/ims/settings/ImsProfile;->getGlobalSettingsName()Ljava/lang/String;

    move-result-object v2

    .line 517
    :cond_2
    const-string v6, "GlobalSettings"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "global settings:  name "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " network "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 519
    invoke-static {p0, v2}, Lcom/sec/ims/settings/GlobalSettings;->_getInstance(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/ims/settings/GlobalSettings;

    move-result-object v6

    return-object v6
.end method

.method public static _getInstance(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/ims/settings/GlobalSettings;
    .locals 3
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 524
    invoke-static {p0, p1}, Lcom/sec/ims/settings/GlobalSettings;->loadSettings(Landroid/content/Context;Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v0

    .line 526
    .local v0, "values":Landroid/content/ContentValues;
    if-nez v0, :cond_0

    .line 527
    invoke-static {p0}, Lcom/sec/ims/settings/GlobalSettings;->getDefault(Landroid/content/Context;)Landroid/content/ContentValues;

    move-result-object v0

    .line 530
    :cond_0
    if-nez v0, :cond_1

    .line 531
    const-string v1, "GlobalSettings"

    const-string v2, "global settings is null"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 532
    const/4 v1, 0x0

    .line 535
    :goto_0
    return-object v1

    :cond_1
    new-instance v1, Lcom/sec/ims/settings/GlobalSettings;

    invoke-direct {v1, v0}, Lcom/sec/ims/settings/GlobalSettings;-><init>(Landroid/content/ContentValues;)V

    goto :goto_0
.end method

.method private static varargs flatten(Landroid/content/ContentValues;[Landroid/content/ContentValues;)Landroid/content/ContentValues;
    .locals 11
    .param p0, "def"    # Landroid/content/ContentValues;
    .param p1, "overrides"    # [Landroid/content/ContentValues;

    .prologue
    .line 541
    move-object v7, p0

    .line 543
    .local v7, "values":Landroid/content/ContentValues;
    if-nez v7, :cond_0

    .line 544
    new-instance v7, Landroid/content/ContentValues;

    .end local v7    # "values":Landroid/content/ContentValues;
    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    .line 547
    .restart local v7    # "values":Landroid/content/ContentValues;
    :cond_0
    if-nez p1, :cond_2

    .line 573
    :cond_1
    return-object v7

    .line 551
    :cond_2
    move-object v0, p1

    .local v0, "arr$":[Landroid/content/ContentValues;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    move v3, v2

    .end local v2    # "i$":I
    .local v3, "i$":I
    :goto_0
    if-ge v3, v5, :cond_1

    aget-object v1, v0, v3

    .line 552
    .local v1, "cv":Landroid/content/ContentValues;
    if-nez v1, :cond_4

    .line 551
    .end local v3    # "i$":I
    :cond_3
    add-int/lit8 v2, v3, 0x1

    .restart local v2    # "i$":I
    move v3, v2

    .end local v2    # "i$":I
    .restart local v3    # "i$":I
    goto :goto_0

    .line 555
    :cond_4
    const-string v8, "GlobalSettings"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "flatten: override with GlobalSettings - "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "name"

    invoke-virtual {v1, v10}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 556
    invoke-virtual {v7}, Landroid/content/ContentValues;->keySet()Ljava/util/Set;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .end local v3    # "i$":I
    .local v2, "i$":Ljava/util/Iterator;
    :cond_5
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 561
    .local v4, "key":Ljava/lang/String;
    const-string v8, "_id"

    invoke-virtual {v4, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_5

    .line 564
    invoke-virtual {v1, v4}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 565
    invoke-virtual {v1, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 566
    .local v6, "obj":Ljava/lang/String;
    if-eqz v6, :cond_5

    .line 567
    invoke-virtual {v7, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static getAllEnabledStackLogLevelValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1121
    const-string v0, "EnablerSvc-1111111;Framework-1111111;SIPStack-1111111;MSRP-1111111;Media-1111111;Platform-1111111"

    return-object v0
.end method

.method public static getAllSettings(Landroid/content/Context;)Ljava/util/List;
    .locals 9
    .param p0, "ctx"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/sec/ims/settings/GlobalSettings;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 616
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 617
    .local v7, "list":Ljava/util/List;, "Ljava/util/List<Lcom/sec/ims/settings/GlobalSettings;>;"
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://com.sec.ims.settings/global"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 620
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_1

    .line 621
    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 622
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    .line 623
    .local v8, "values":Landroid/content/ContentValues;
    invoke-static {v6, v8}, Landroid/database/DatabaseUtils;->cursorRowToContentValues(Landroid/database/Cursor;Landroid/content/ContentValues;)V

    .line 624
    new-instance v0, Lcom/sec/ims/settings/GlobalSettings;

    invoke-direct {v0, v8}, Lcom/sec/ims/settings/GlobalSettings;-><init>(Landroid/content/ContentValues;)V

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 626
    .end local v8    # "values":Landroid/content/ContentValues;
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 628
    :cond_1
    return-object v7
.end method

.method private getBooleanValue(Landroid/content/ContentValues;Ljava/lang/String;Z)Z
    .locals 2
    .param p1, "values"    # Landroid/content/ContentValues;
    .param p2, "field"    # Ljava/lang/String;
    .param p3, "defaultVal"    # Z

    .prologue
    .line 446
    invoke-virtual {p1, p2}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    .line 447
    .local v0, "obj":Ljava/lang/Integer;
    if-nez v0, :cond_0

    .line 453
    .end local p3    # "defaultVal":Z
    :goto_0
    return p3

    .line 450
    .restart local p3    # "defaultVal":Z
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-nez v1, :cond_1

    .line 451
    const/4 p3, 0x0

    goto :goto_0

    .line 453
    :cond_1
    const/4 p3, 0x1

    goto :goto_0
.end method

.method public static getDefault(Landroid/content/Context;)Landroid/content/ContentValues;
    .locals 1
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    .line 612
    const-string v0, "default"

    invoke-static {p0, v0}, Lcom/sec/ims/settings/GlobalSettings;->loadSettings(Landroid/content/Context;Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v0

    return-object v0
.end method

.method public static getDefaultStackLogLevelValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1117
    sget-object v0, Lcom/sec/ims/settings/GlobalSettings;->mDefaultStackLogLevel:Ljava/lang/String;

    return-object v0
.end method

.method private getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;)Ljava/lang/Integer;
    .locals 1
    .param p1, "values"    # Landroid/content/ContentValues;
    .param p2, "field"    # Ljava/lang/String;

    .prologue
    .line 457
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/ims/settings/GlobalSettings;->getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method private getIntegerValue(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 1
    .param p1, "values"    # Landroid/content/ContentValues;
    .param p2, "field"    # Ljava/lang/String;
    .param p3, "defaultVal"    # Ljava/lang/Integer;

    .prologue
    .line 461
    invoke-virtual {p1, p2}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    .line 462
    .local v0, "obj":Ljava/lang/Integer;
    if-nez v0, :cond_0

    .line 465
    .end local p3    # "defaultVal":Ljava/lang/Integer;
    :goto_0
    return-object p3

    .restart local p3    # "defaultVal":Ljava/lang/Integer;
    :cond_0
    move-object p3, v0

    goto :goto_0
.end method

.method private getStringValue(Landroid/content/ContentValues;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "values"    # Landroid/content/ContentValues;
    .param p2, "field"    # Ljava/lang/String;

    .prologue
    .line 469
    const-string v0, ""

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/ims/settings/GlobalSettings;->getStringValue(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getStringValue(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "values"    # Landroid/content/ContentValues;
    .param p2, "field"    # Ljava/lang/String;
    .param p3, "defaultVal"    # Ljava/lang/String;

    .prologue
    .line 473
    invoke-virtual {p1, p2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 474
    .local v0, "obj":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 477
    .end local p3    # "defaultVal":Ljava/lang/String;
    :goto_0
    return-object p3

    .restart local p3    # "defaultVal":Ljava/lang/String;
    :cond_0
    move-object p3, v0

    goto :goto_0
.end method

.method private getUriValue(Landroid/content/ContentValues;Ljava/lang/String;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 1
    .param p1, "values"    # Landroid/content/ContentValues;
    .param p2, "field"    # Ljava/lang/String;
    .param p3, "defaultVal"    # Landroid/net/Uri;

    .prologue
    .line 481
    invoke-virtual {p1, p2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 482
    .local v0, "obj":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 485
    .end local p3    # "defaultVal":Landroid/net/Uri;
    :goto_0
    return-object p3

    .restart local p3    # "defaultVal":Landroid/net/Uri;
    :cond_0
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p3

    goto :goto_0
.end method

.method private static loadSettings(Landroid/content/Context;Ljava/lang/String;)Landroid/content/ContentValues;
    .locals 1
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 577
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/sec/ims/settings/GlobalSettings;->loadSettings(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v0

    return-object v0
.end method

.method private static loadSettings(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/ContentValues;
    .locals 8
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "network"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 581
    const/4 v7, 0x0

    .line 583
    .local v7, "values":Landroid/content/ContentValues;
    if-nez p2, :cond_0

    .line 584
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "name=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 589
    .local v3, "selection":Ljava/lang/String;
    :goto_0
    const-string v0, "GlobalSettings"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "selection="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 591
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://com.sec.ims.settings/global"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 595
    .local v6, "cursor":Landroid/database/Cursor;
    if-nez v6, :cond_1

    .line 608
    :goto_1
    return-object v2

    .line 586
    .end local v3    # "selection":Ljava/lang/String;
    .end local v6    # "cursor":Landroid/database/Cursor;
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "name=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\" AND network=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .restart local v3    # "selection":Ljava/lang/String;
    goto :goto_0

    .line 599
    .restart local v6    # "cursor":Landroid/database/Cursor;
    :cond_1
    const-string v0, "GlobalSettings"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "loadSettings: count "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 601
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 602
    new-instance v7, Landroid/content/ContentValues;

    .end local v7    # "values":Landroid/content/ContentValues;
    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    .line 603
    .restart local v7    # "values":Landroid/content/ContentValues;
    invoke-static {v6, v7}, Landroid/database/DatabaseUtils;->cursorRowToContentValues(Landroid/database/Cursor;Landroid/content/ContentValues;)V

    .line 606
    :cond_2
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move-object v2, v7

    .line 608
    goto :goto_1
.end method


# virtual methods
.method public IsAVPFEnabled()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 960
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mIsAVPFEnabled:Ljava/lang/Integer;

    return-object v0
.end method

.method public getAMRBundlingRate()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 876
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mAmrBundleRate:Ljava/lang/Integer;

    return-object v0
.end method

.method public getAMRNBBEPayloadVal()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 996
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mAMRNBBEPayloadVal:Ljava/lang/Integer;

    return-object v0
.end method

.method public getAMRNBOAPayloadVal()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 1000
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mAMRNBOAPayloadVal:Ljava/lang/Integer;

    return-object v0
.end method

.method public getAMRWBBEPayloadVal()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 1004
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mAMRWBBEPayloadVal:Ljava/lang/Integer;

    return-object v0
.end method

.method public getAMRWBOAPayloadVal()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 1008
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mAMRWBOAPayloadVal:Ljava/lang/Integer;

    return-object v0
.end method

.method public getAddPhoneExtension()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 1101
    iget v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mAddPhoneExtension:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getAddPresListDummy()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1105
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mAddPresListDummy:Ljava/lang/String;

    return-object v0
.end method

.method public getAggProxyIp()Ljava/lang/String;
    .locals 1

    .prologue
    .line 764
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mAggProxyIp:Ljava/lang/String;

    return-object v0
.end method

.method public getApnSelection()Ljava/lang/String;
    .locals 1

    .prologue
    .line 780
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mApnSelection:Ljava/lang/String;

    return-object v0
.end method

.method public getApnSelectionAsIntType()Ljava/lang/Integer;
    .locals 2

    .prologue
    .line 784
    const-string v0, "Inet"

    iget-object v1, p0, Lcom/sec/ims/settings/GlobalSettings;->mApnSelection:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 785
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 790
    :goto_0
    return-object v0

    .line 786
    :cond_0
    const-string v0, "Wifi"

    iget-object v1, p0, Lcom/sec/ims/settings/GlobalSettings;->mApnSelection:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 787
    const/4 v0, 0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 790
    :cond_1
    const/4 v0, 0x3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method public getAudioAS()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 972
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mAudioAS:Ljava/lang/Integer;

    return-object v0
.end method

.method public getAudioCapabilities()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1056
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mAudioCapabilities:Ljava/lang/String;

    return-object v0
.end method

.method public getAudioCodecMode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 888
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mAudioCodecModeVal:Ljava/lang/String;

    return-object v0
.end method

.method public getAudioCodecVal()Ljava/lang/String;
    .locals 1

    .prologue
    .line 912
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mAudioCodecVal:Ljava/lang/String;

    return-object v0
.end method

.method public getAudioModeSetNB()Ljava/lang/String;
    .locals 1

    .prologue
    .line 896
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mAmrNbMode:Ljava/lang/String;

    return-object v0
.end method

.method public getAudioModeSetWB()Ljava/lang/String;
    .locals 1

    .prologue
    .line 900
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mAmrWbMode:Ljava/lang/String;

    return-object v0
.end method

.method public getAudioPortEnd()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 932
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mAudioPortEnd:Ljava/lang/Integer;

    return-object v0
.end method

.method public getAudioRR()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 980
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mAudioRR:Ljava/lang/Integer;

    return-object v0
.end method

.method public getAudioRS()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 976
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mAudioRS:Ljava/lang/Integer;

    return-object v0
.end method

.method public getAudioVideoTxVal()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 892
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mAudioVideoTxVal:Ljava/lang/Integer;

    return-object v0
.end method

.method public getAuthProxyIp()Ljava/lang/String;
    .locals 1

    .prologue
    .line 768
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mAuthProxyIp:Ljava/lang/String;

    return-object v0
.end method

.method public getAuthProxyPort()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 772
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mAuthProxyPort:Ljava/lang/Integer;

    return-object v0
.end method

.method public getBadEventExpiry()I
    .locals 1

    .prologue
    .line 719
    iget v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mBadEventExpiry:I

    return v0
.end method

.method public getBsfIp()Ljava/lang/String;
    .locals 1

    .prologue
    .line 802
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mBsfIp:Ljava/lang/String;

    return-object v0
.end method

.method public getBsfPort()Ljava/lang/String;
    .locals 1

    .prologue
    .line 806
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mBsfPort:Ljava/lang/String;

    return-object v0
.end method

.method public getCapCacheExpiry()I
    .locals 1

    .prologue
    .line 723
    iget v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mCapCacheExpiry:I

    return v0
.end method

.method public getConfDialogType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 822
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mConfDialogType:Ljava/lang/String;

    return-object v0
.end method

.method public getConfSubscribe()Ljava/lang/String;
    .locals 1

    .prologue
    .line 818
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mConfSubscribe:Ljava/lang/String;

    return-object v0
.end method

.method public getConfUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 814
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mConfUri:Ljava/lang/String;

    return-object v0
.end method

.method public getCurrDispFormat()Ljava/lang/String;
    .locals 1

    .prologue
    .line 908
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mCurrDispFormat:Ljava/lang/String;

    return-object v0
.end method

.method public getDefaultUri()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 1093
    iget v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mSetDefaultUri:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getDtmfCodecMode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1040
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mDtmfCodecMode:Ljava/lang/String;

    return-object v0
.end method

.method public getDtmfMode()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 1065
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mDtmfMode:Ljava/lang/Integer;

    return-object v0
.end method

.method public getDtmfNBPayloadVal()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 1024
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mDtmfNBPayloadVal:Ljava/lang/Integer;

    return-object v0
.end method

.method public getDtmfWBPayloadVal()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 1028
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mDtmfWBPayloadVal:Ljava/lang/Integer;

    return-object v0
.end method

.method public getDynamicBitRateVal()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 884
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mBitRate:Ljava/lang/Integer;

    return-object v0
.end method

.method public getEnableAudioonCPVal()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 872
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mEnableCpAudioPath:Ljava/lang/Integer;

    return-object v0
.end method

.method public getEnableBwm()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1048
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mEnableBwm:Ljava/lang/String;

    return-object v0
.end method

.method public getEnableBwmDefault()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1052
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mEnableBwmDefault:Ljava/lang/String;

    return-object v0
.end method

.method public getEnableGba()I
    .locals 1

    .prologue
    .line 810
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mEnableGba:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getEnableMwi()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 1069
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mEnableMwi:Ljava/lang/Integer;

    return-object v0
.end method

.method public getExpireHeader()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 1089
    iget v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mExpireHeader:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getExtendedPublishTimer()I
    .locals 1

    .prologue
    .line 707
    iget v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mExtendedPublishTimer:I

    return v0
.end method

.method public getFrameRate()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 904
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mFrameRate:Ljava/lang/Integer;

    return-object v0
.end method

.method public getGZipEnable()I
    .locals 1

    .prologue
    .line 711
    iget v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mIsGZipEnable:I

    return v0
.end method

.method public getH264QvgaPayloadVal()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 1012
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mH264QvgaPayloadVal:Ljava/lang/Integer;

    return-object v0
.end method

.method public getH264VgaLPayloadVal()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 1020
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mH264VgaLPayloadVal:Ljava/lang/Integer;

    return-object v0
.end method

.method public getH264VgaPayloadVal()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 1016
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mH264VgaPayloadVal:Ljava/lang/Integer;

    return-object v0
.end method

.method public getHttpPassword()Ljava/lang/String;
    .locals 1

    .prologue
    .line 756
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mHttpPassword:Ljava/lang/String;

    return-object v0
.end method

.method public getHttpUserName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 752
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mHttpUserName:Ljava/lang/String;

    return-object v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 632
    iget v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mId:I

    return v0
.end method

.method public getImsConfigName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1174
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mImsConfigName:Ljava/lang/String;

    return-object v0
.end method

.method public getInviteTimeout()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 1061
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mInviteTimeout:Ljava/lang/Integer;

    return-object v0
.end method

.method public getJitterBufferSettings()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 940
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mJitterBufferSettings:Ljava/lang/Integer;

    return-object v0
.end method

.method public getLocalUriType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 682
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mLocalUriType:Ljava/lang/String;

    return-object v0
.end method

.method public getLogLevel()I
    .locals 1

    .prologue
    .line 694
    iget v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mLogLevel:I

    return v0
.end method

.method public getLogRTPPacketsToFile()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 880
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mLogRtpPacketsToFile:Ljava/lang/Integer;

    return-object v0
.end method

.method public getMwiSubscribeExpiry()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 1073
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mMwiSubscribeExpiry:Ljava/lang/Integer;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 636
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getNetwork()Ljava/lang/String;
    .locals 1

    .prologue
    .line 640
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mNetwork:Ljava/lang/String;

    return-object v0
.end method

.method public getPacketizationMode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1032
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mPacketizatoinMode:Ljava/lang/String;

    return-object v0
.end method

.method public getPollListSubExpiry()I
    .locals 1

    .prologue
    .line 739
    iget v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mPollListSubExpiry:I

    return v0
.end method

.method public getPresenceListMaxEntries()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 944
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mPresenceListMaxEntries:Landroid/net/Uri;

    return-object v0
.end method

.method public getPublishErrRetry()I
    .locals 1

    .prologue
    .line 735
    iget v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mPublishErrRetry:I

    return v0
.end method

.method public getPublishExpiry()I
    .locals 1

    .prologue
    .line 703
    iget v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mPublishExpiry:I

    return v0
.end method

.method public getPublishTimer()I
    .locals 1

    .prologue
    .line 731
    iget v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mPublishTimer:I

    return v0
.end method

.method public getRTCPTimer()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 868
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mRtcpTimeout:Ljava/lang/Integer;

    return-object v0
.end method

.method public getRTPAudioPort()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 920
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mAudioPortStart:Ljava/lang/Integer;

    return-object v0
.end method

.method public getRTPTextPort()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 928
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mTextPort:Ljava/lang/Integer;

    return-object v0
.end method

.method public getRTPTimer()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 864
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mRtpTimeout:Ljava/lang/Integer;

    return-object v0
.end method

.method public getRTPVideoPort()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 924
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mVideoPortStart:Ljava/lang/Integer;

    return-object v0
.end method

.method public getRcsAutoconfigurationSource()I
    .locals 1

    .prologue
    .line 657
    iget v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mRcsAutoconfigSource:I

    return v0
.end method

.method public getRcsHomePreference()I
    .locals 1

    .prologue
    .line 661
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mRcsHomePreference:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getRcsRoamingPreference()I
    .locals 1

    .prologue
    .line 665
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mRcsRoamingPreference:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getRcsVersion()I
    .locals 1

    .prologue
    .line 653
    iget v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mRcsVersion:I

    return v0
.end method

.method public getRegExpire()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 859
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mRegExpire:Ljava/lang/Integer;

    return-object v0
.end method

.method public getRegRetryBaseTime()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 826
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mRegRetryBaseTime:Ljava/lang/Integer;

    return-object v0
.end method

.method public getRegRetryMaxTime()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 830
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mRegRetryMaxTime:Ljava/lang/Integer;

    return-object v0
.end method

.method public getRemoteUriType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 686
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mRemoteUriType:Ljava/lang/String;

    return-object v0
.end method

.method public getRingbackTimer()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 843
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mRingbackTimer:Ljava/lang/Integer;

    return-object v0
.end method

.method public getRingingTimer()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 839
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mRingingTimer:Ljava/lang/Integer;

    return-object v0
.end method

.method public getSelfPort()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1202
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mSelfPort:Ljava/lang/String;

    return-object v0
.end method

.method public getSessionExpire()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 835
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mSessionExpire:Ljava/lang/Integer;

    return-object v0
.end method

.method public getSipUserAgent()Ljava/lang/String;
    .locals 1

    .prologue
    .line 690
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mSipUserAgent:Ljava/lang/String;

    return-object v0
.end method

.method public getSlickConfigName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1178
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mSlickConfigName:Ljava/lang/String;

    return-object v0
.end method

.method public getSmsFormat()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1182
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mSmsFormat:Ljava/lang/String;

    return-object v0
.end method

.method public getStackLogLevelValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1125
    iget-boolean v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mEnableAllLog:Z

    if-eqz v0, :cond_0

    .line 1126
    const-string v0, "EnablerSvc-1111111;Framework-1111111;SIPStack-1111111;MSRP-1111111;Media-1111111;Platform-1111111"

    .line 1128
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/sec/ims/settings/GlobalSettings;->mDefaultStackLogLevel:Ljava/lang/String;

    goto :goto_0
.end method

.method public getSubscribeListSyncEnabled()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 1097
    iget v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mSubscribeListSyncEnabled:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getSubscriberTimer()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 847
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mSubscriberTimer:Ljava/lang/Integer;

    return-object v0
.end method

.method public getSubscriptionExpiry()I
    .locals 1

    .prologue
    .line 715
    iget v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mSubscriptionExpiry:I

    return v0
.end method

.method public getTDelay()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1198
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mTDelay:Ljava/lang/String;

    return-object v0
.end method

.method public getTVolteHysTimer()I
    .locals 1

    .prologue
    .line 1210
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mTVolteHysTimer:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getTransportInContact()I
    .locals 1

    .prologue
    .line 698
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mTransportInContact:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getUtApnName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1113
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mUtApnName:Ljava/lang/String;

    return-object v0
.end method

.method public getUtPdnType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1109
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mUtPdnType:Ljava/lang/String;

    return-object v0
.end method

.method public getValues()Landroid/content/ContentValues;
    .locals 1

    .prologue
    .line 644
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mValues:Landroid/content/ContentValues;

    return-object v0
.end method

.method public getVideoAS()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 984
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mVideoAS:Ljava/lang/Integer;

    return-object v0
.end method

.method public getVideoCodecVal()Ljava/lang/String;
    .locals 1

    .prologue
    .line 916
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mVideoCodecVal:Ljava/lang/String;

    return-object v0
.end method

.method public getVideoPortEnd()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 936
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mVideoPortEnd:Ljava/lang/Integer;

    return-object v0
.end method

.method public getVideoRR()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 992
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mVideoRR:Ljava/lang/Integer;

    return-object v0
.end method

.method public getVideoRS()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 988
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mVideoRS:Ljava/lang/Integer;

    return-object v0
.end method

.method public getVolteRegIconId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1170
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mVolteRegIconId:Ljava/lang/String;

    return-object v0
.end method

.method public getVzwTimer()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1194
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mVzwTimer:Ljava/lang/String;

    return-object v0
.end method

.method public getWifiSelfPort()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1206
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mWifiSelfPort:Ljava/lang/String;

    return-object v0
.end method

.method public getXcapRootUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 760
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mXcapRootUri:Ljava/lang/String;

    return-object v0
.end method

.method public getXcapRootUriPref()I
    .locals 1

    .prologue
    .line 794
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mXcapRootUriPref:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getXdmEnable()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 748
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mXdmEnable:Ljava/lang/Integer;

    return-object v0
.end method

.method public getXdmUserAgent()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 776
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mXdmUserAgent:Landroid/net/Uri;

    return-object v0
.end method

.method public getXdmUserIdDomain()Ljava/lang/String;
    .locals 1

    .prologue
    .line 798
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mXdmUserIdDomain:Ljava/lang/String;

    return-object v0
.end method

.method public isAnonymousFetch()Z
    .locals 1

    .prologue
    .line 727
    iget-boolean v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mAnonymousFetch:Z

    return v0
.end method

.method public isClosedGroupChat()Z
    .locals 1

    .prologue
    .line 669
    iget-boolean v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mClosedGroupChat:Z

    return v0
.end method

.method public isDispFpsBitrateEnabled()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 968
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mIsDispFpsBitrateEnabled:Ljava/lang/Integer;

    return-object v0
.end method

.method public isEabEnabled()Z
    .locals 1

    .prologue
    .line 1154
    iget-boolean v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mIsEabEnabled:Z

    return v0
.end method

.method public isEabPubDone()Z
    .locals 1

    .prologue
    .line 1162
    iget-boolean v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mIsEabPubDone:Z

    return v0
.end method

.method public isEabPubFailed()Z
    .locals 1

    .prologue
    .line 1158
    iget-boolean v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mIsEabPubFailed:Z

    return v0
.end method

.method public isEnableCiq()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 1077
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mEnableCiq:Ljava/lang/Integer;

    return-object v0
.end method

.method public isEnableEMM()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 948
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mIsEnableEMM:Ljava/lang/Integer;

    return-object v0
.end method

.method public isEnableG711Codec()Z
    .locals 2

    .prologue
    .line 1036
    const-string v0, "1"

    iget-object v1, p0, Lcom/sec/ims/settings/GlobalSettings;->mEnableG711Codec:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isEnableGruu()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 1085
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mEnableGruu:Ljava/lang/Integer;

    return-object v0
.end method

.method public isEnableHasati()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 1081
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mEnableHasati:Ljava/lang/Integer;

    return-object v0
.end method

.method public isEnableLoopBack()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 952
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mIsEnableLoopBack:Ljava/lang/Integer;

    return-object v0
.end method

.method public isEnableSRTP()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 964
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mIsEnableSRTP:Ljava/lang/Integer;

    return-object v0
.end method

.method public isExternalGTTY()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 956
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mIsExternalGTTY:Ljava/lang/Integer;

    return-object v0
.end method

.method public isGbaSupportEnforced()Z
    .locals 1

    .prologue
    .line 1142
    iget-boolean v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mEnforceGbaSupport:Z

    return v0
.end method

.method public isImsSupportedInRoaming()Z
    .locals 1

    .prologue
    .line 1137
    iget-boolean v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mSupportImsInRoaming:Z

    return v0
.end method

.method public isImsTestMode()Z
    .locals 1

    .prologue
    .line 1044
    iget-boolean v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mImsTestMode:Z

    return v0
.end method

.method public isLvcEnabled()Z
    .locals 1

    .prologue
    .line 1150
    iget-boolean v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mIsLvcEnabled:Z

    return v0
.end method

.method public isPrecondEnabled()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 855
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mUsePrecondition:Ljava/lang/Integer;

    return-object v0
.end method

.method public isRcsEnabled()Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 649
    iget-boolean v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mIsRcsEnabled:Z

    return v0
.end method

.method public isSipUriOnly()Z
    .locals 1

    .prologue
    .line 1132
    iget-boolean v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mSipUriOnly:Z

    return v0
.end method

.method public isSmsOverIp()Z
    .locals 2

    .prologue
    .line 1190
    const-string v0, "0"

    iget-object v1, p0, Lcom/sec/ims/settings/GlobalSettings;->mSmsOverIp:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isSmsWriteUicc()Z
    .locals 2

    .prologue
    .line 1186
    const-string v0, "0"

    iget-object v1, p0, Lcom/sec/ims/settings/GlobalSettings;->mSmsWriteUicc:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isSubscribeEnabled()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 851
    iget-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mSubscribeForReg:Ljava/lang/Integer;

    return-object v0
.end method

.method public isVolteEnabled()Z
    .locals 1

    .prologue
    .line 1146
    iget-boolean v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mIsVolteEnabled:Z

    return v0
.end method

.method public setRcsHomePreference(I)V
    .locals 1
    .param p1, "pref"    # I

    .prologue
    .line 673
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mRcsHomePreference:Ljava/lang/Integer;

    .line 674
    return-void
.end method

.method public setRcsRoamingPreference(I)V
    .locals 1
    .param p1, "pref"    # I

    .prologue
    .line 677
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mRcsRoamingPreference:Ljava/lang/Integer;

    .line 678
    return-void
.end method

.method public shouldUpdateAutoConfig()Z
    .locals 1

    .prologue
    .line 743
    iget-boolean v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mUpdateAutoConfig:Z

    return v0
.end method

.method public showRegiIcon()Z
    .locals 1

    .prologue
    .line 1166
    iget-boolean v0, p0, Lcom/sec/ims/settings/GlobalSettings;->mShowRegIcon:Z

    return v0
.end method
