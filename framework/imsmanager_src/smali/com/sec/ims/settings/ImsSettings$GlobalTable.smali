.class public final Lcom/sec/ims/settings/ImsSettings$GlobalTable;
.super Ljava/lang/Object;
.source "ImsSettings.java"

# interfaces
.implements Landroid/provider/BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/ims/settings/ImsSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GlobalTable"
.end annotation


# static fields
.field public static final ADD_PHONE_EXTENSION:Ljava/lang/String; = "add_phone_extension"

.field public static final ADD_PRES_LIST_DUMMY:Ljava/lang/String; = "add_pres_list_dummy"

.field public static final AGG_PROXY_IP:Ljava/lang/String; = "agg_proxy_ip"

.field public static final AMRNBBE_PAYLOAD:Ljava/lang/String; = "amrnbbe_payload"

.field public static final AMRNBOA_PAYLOAD:Ljava/lang/String; = "amrnboa_payload"

.field public static final AMRNB_MODE:Ljava/lang/String; = "amrnb_mode"

.field public static final AMRWBBE_PAYLOAD:Ljava/lang/String; = "amrwbbe_payload"

.field public static final AMRWBOA_PAYLOAD:Ljava/lang/String; = "amrwboa_payload"

.field public static final AMRWB_MODE:Ljava/lang/String; = "amrwb_mode"

.field public static final AMR_BUNDLE_RATE:Ljava/lang/String; = "amr_bundle_rate"

.field public static final AMR_WB:Ljava/lang/String; = "amr_wb"

.field public static final ANONYMOUS_FETCH:Ljava/lang/String; = "anonymous_fetch"

.field public static final APCS_SERVICE_STATUS:Ljava/lang/String; = "apcs_service_status"

.field public static final APN_SELECTION:Ljava/lang/String; = "apn_selection"

.field public static final AUDIO_AS:Ljava/lang/String; = "audio_as"

.field public static final AUDIO_CAPABILITIES:Ljava/lang/String; = "audio_capabilities"

.field public static final AUDIO_CODEC:Ljava/lang/String; = "audio_codec"

.field public static final AUDIO_CODEC_MODE:Ljava/lang/String; = "audio_codec_mode"

.field public static final AUDIO_PORT_END:Ljava/lang/String; = "audio_port_end"

.field public static final AUDIO_PORT_START:Ljava/lang/String; = "audio_port_start"

.field public static final AUDIO_RR:Ljava/lang/String; = "audio_rr"

.field public static final AUDIO_RS:Ljava/lang/String; = "audio_rs"

.field public static final AUDIO_VIDEO_TX:Ljava/lang/String; = "audio_video_tx"

.field public static final AUTH_PROXY_IP:Ljava/lang/String; = "auth_proxy_ip"

.field public static final AUTH_PROXY_PORT:Ljava/lang/String; = "auth_proxy_port"

.field public static final AVAIL_CACHE_EXP:Ljava/lang/String; = "avail_cache_exp"

.field public static final AVPF:Ljava/lang/String; = "avpf"

.field public static final BAD_EVENT_EXPIRY:Ljava/lang/String; = "bad_event_expiry"

.field public static final BITRATE:Ljava/lang/String; = "bitrate"

.field public static final BSF_IP:Ljava/lang/String; = "bsf_ip"

.field public static final BSF_PORT:Ljava/lang/String; = "bsf_port"

.field public static final CAP_CACHE_EXP:Ljava/lang/String; = "cap_cache_exp"

.field public static final CAP_DISCOVERY:Ljava/lang/String; = "cap_discovery"

.field public static final CAP_POLL_INTERVAL:Ljava/lang/String; = "cap_poll_interval"

.field public static final CLOSED_GROUP_CHAT:Ljava/lang/String; = "closed_group_chat"

.field public static final CONFERENCE_DIALOG_TYPE:Ljava/lang/String; = "conference_dialog_type"

.field public static final CONFERENCE_SUBSCRIBE:Ljava/lang/String; = "conference_subscribe"

.field public static final CONFERENCE_URI:Ljava/lang/String; = "conference_uri"

.field public static final CONF_FACTORY_URI:Ljava/lang/String; = "conf_factory_uri"

.field public static final CONF_FACTORY_URI_SHOW:Ljava/lang/String; = "conf_factory_uri_show"

.field public static final CONTENT_URI:Landroid/net/Uri;

.field public static final CUSTOM_CODECS:Ljava/lang/String; = "custom_codecs"

.field public static final DCN_NUMBER:Ljava/lang/String; = "dcn_number"

.field public static final DISPLAY_FORMAT:Ljava/lang/String; = "display_format"

.field public static final DISPLAY_FPS_BITRATE:Ljava/lang/String; = "display_fps_bitrate"

.field public static final DM_APP_ID:Ljava/lang/String; = "dm_app_id"

.field public static final DM_CON_REF:Ljava/lang/String; = "dm_con_ref"

.field public static final DM_POLLING_PERIOD:Ljava/lang/String; = "dm_polling_period"

.field public static final DM_USER_DISP_NAME:Ljava/lang/String; = "dm_user_disp_name"

.field public static final DOMAIN_PUI:Ljava/lang/String; = "domain_pui"

.field public static final DTMF_CODEC_MODE:Ljava/lang/String; = "dtmf_codec_mode"

.field public static final DTMF_MODE:Ljava/lang/String; = "dtmf_mode"

.field public static final DTMF_NB_PAYLOAD:Ljava/lang/String; = "dtmf_nb_payload"

.field public static final DTMF_WB_PAYLOAD:Ljava/lang/String; = "dtmf_wb_payload"

.field public static final EAB_SETTING:Ljava/lang/String; = "dm_eab_enabled"

.field public static final EAB_SETTING_BY_USER:Ljava/lang/String; = "eab_enabled"

.field public static final EHRPD_ENABLED:Ljava/lang/String; = "ehrpd_enabled"

.field public static final EMERGENCY_DOMAIN_SETTING:Ljava/lang/String; = "emergency_domain_setting"

.field public static final EMM:Ljava/lang/String; = "emm"

.field public static final ENABLE_ALL_LOGS:Ljava/lang/String; = "enable_all_logs"

.field public static final ENABLE_BWM:Ljava/lang/String; = "enable_bwm"

.field public static final ENABLE_BWM_DEFAULT:Ljava/lang/String; = "enable_bwm_default"

.field public static final ENABLE_CIQ:Ljava/lang/String; = "enable_ciq"

.field public static final ENABLE_CP_AUDIO_PATH:Ljava/lang/String; = "enable_cp_audio_path"

.field public static final ENABLE_G711:Ljava/lang/String; = "enable_g711"

.field public static final ENABLE_GBA:Ljava/lang/String; = "enable_gba"

.field public static final ENABLE_GRUU:Ljava/lang/String; = "enable_gruu"

.field public static final ENABLE_GZIP:Ljava/lang/String; = "enable_gzip"

.field public static final ENABLE_HASATI:Ljava/lang/String; = "enable_hasati"

.field public static final ENABLE_MWI:Ljava/lang/String; = "enable_mwi"

.field public static final ENFORCE_GBA_SUPPORT:Ljava/lang/String; = "enforce_gba_support"

.field public static final EPDG_ENABLED:Ljava/lang/String; = "epdg_enabled"

.field public static final EXPIRE_HEADER:Ljava/lang/String; = "expire_header"

.field public static final EXTENDED_PUBLISH_TIMER:Ljava/lang/String; = "extended_publish_timer"

.field public static final EXTERNAL_GTTY:Ljava/lang/String; = "external_gtty"

.field public static final FRAMERATE:Ljava/lang/String; = "framerate"

.field public static final H264_LVGA_PAYLOAD:Ljava/lang/String; = "h264_lvga_payload"

.field public static final H264_QVGA_PAYLOAD:Ljava/lang/String; = "h264_qvga_payload"

.field public static final H264_VGAL_PAYLOAD:Ljava/lang/String; = "h264_vgal_payload"

.field public static final H264_VGA_PAYLOAD:Ljava/lang/String; = "h264_vga_payload"

.field public static final HD_VOICE:Ljava/lang/String; = "hd_voice"

.field public static final HOME_DOMAIN:Ljava/lang/String; = "home_domain"

.field public static final HTTP_PASSWORD:Ljava/lang/String; = "http_password"

.field public static final HTTP_USERNAME:Ljava/lang/String; = "http_username"

.field public static final ICSI:Ljava/lang/String; = "icsi"

.field public static final ICSI_RSC_ALLOC_MODE:Ljava/lang/String; = "icsi_rsc_alloc_mode"

.field public static final IMS_CONFIG_NAME:Ljava/lang/String; = "ims_config_name"

.field public static final IMS_ENABLED:Ljava/lang/String; = "ims_enabled"

.field public static final IMS_TEST_MODE:Ljava/lang/String; = "ims_test_mode"

.field public static final IMS_VOICE_TERMINATION:Ljava/lang/String; = "ims_voice_termination"

.field public static final INVITE_TIME_OUT:Ljava/lang/String; = "invite_timeout"

.field public static final IS_PARENT:Ljava/lang/String; = "is_parent"

.field public static final JITTER_BUFFER:Ljava/lang/String; = "jitter_buffer"

.field public static final LOCAL_URI_TYPE:Ljava/lang/String; = "local_uri_type"

.field public static final LOG_LEVEL:Ljava/lang/String; = "log_level"

.field public static final LOG_RTPPACKETS_TO_FILE:Ljava/lang/String; = "log_rtppackets_to_file"

.field public static final LOOPBACK:Ljava/lang/String; = "loopback"

.field public static final LVC_BETA_SETTING:Ljava/lang/String; = "lvc_beta_setting"

.field public static final LVC_ENABLED:Ljava/lang/String; = "dm_lvc_enabled"

.field public static final LVC_ENABLED_BY_USER:Ljava/lang/String; = "lvc_enabled"

.field public static final MIN_SE:Ljava/lang/String; = "min_se"

.field public static final MWI_SUBSCRIBE_EXPIRY:Ljava/lang/String; = "mwi_subscribe_expiry"

.field public static final NAME:Ljava/lang/String; = "name"

.field public static final NETWORK:Ljava/lang/String; = "network"

.field public static final PACKETIZAION_MODE:Ljava/lang/String; = "packetization_mode"

.field public static final PARENT:Ljava/lang/String; = "parent"

.field public static final PCSCF_ADDRESS:Ljava/lang/String; = "pcscf_addr"

.field public static final PDP_CONTEXT_PREF:Ljava/lang/String; = "pdp_context_pref"

.field public static final PHONE_CONTEXT_URI:Ljava/lang/String; = "phone_context_uri"

.field public static final PIP:Ljava/lang/String; = "pip"

.field public static final POLL_LIST_SUB_EXP:Ljava/lang/String; = "poll_list_sub_exp"

.field public static final PRESENCE_LIST_MAX_ENTRIES:Ljava/lang/String; = "presence_list_max_entries"

.field public static final PUBLIC_USER_ID:Ljava/lang/String; = "public_user_id"

.field public static final PUBLISH_ERR_RETRY_TIMER:Ljava/lang/String; = "publish_err_retry_timer"

.field public static final PUBLISH_EXPIRY:Ljava/lang/String; = "publish_expiry"

.field public static final PUBLISH_TIMER:Ljava/lang/String; = "publish_timer"

.field public static final RCS:Ljava/lang/String; = "rcs"

.field public static final RCS_HOME_PREF:Ljava/lang/String; = "rcs_home_pref"

.field public static final RCS_LOCAL_CONFIG_SERVER:Ljava/lang/String; = "rcs_local_config_server"

.field public static final RCS_ROAMING_PREF:Ljava/lang/String; = "rcs_roaming_pref"

.field public static final RCS_VER:Ljava/lang/String; = "rcs_ver"

.field public static final REG_EXPIRES:Ljava/lang/String; = "reg_expires"

.field public static final REG_RETRY_BASE_TIME:Ljava/lang/String; = "reg_retry_base_time"

.field public static final REG_RETRY_MAX_TIME:Ljava/lang/String; = "reg_retry_max_time"

.field public static final REMOTE_URI_TYPE:Ljava/lang/String; = "remote_uri_type"

.field public static final RINGBACK_TIMER:Ljava/lang/String; = "ringback_timer"

.field public static final RINGING_TIMER:Ljava/lang/String; = "ringing_timer"

.field public static final RSC_ALLOC_MODE:Ljava/lang/String; = "rsc_alloc_mode"

.field public static final RTCP_TIMEOUT:Ljava/lang/String; = "rtcp_timeout"

.field public static final RTP_TIMEOUT:Ljava/lang/String; = "rtp_timeout"

.field public static final SELF_PORT:Ljava/lang/String; = "self_port"

.field public static final SESSION_EXPIRES:Ljava/lang/String; = "session_expires"

.field public static final SET_DEFAULT_URI:Ljava/lang/String; = "set_default_uri"

.field public static final SHOW_REGI_ICON:Ljava/lang/String; = "show_volte_regi_icon"

.field public static final SHOW_REG_INFO_TOSETTING_APP:Ljava/lang/String; = "show_reg_info_to_setting_app"

.field public static final SIP_URI_ONLY:Ljava/lang/String; = "sip_uri_only"

.field public static final SIP_USERAGENT:Ljava/lang/String; = "useragent"

.field public static final SLICK_CONFIG_NAME:Ljava/lang/String; = "slick_config_name"

.field public static final SMS_DOMAIN_UI_SHOW:Ljava/lang/String; = "sms_domain_ui_show"

.field public static final SMS_FORMAT:Ljava/lang/String; = "sms_format"

.field public static final SMS_OVER_IMS:Ljava/lang/String; = "sms_over_ip_indication"

.field public static final SMS_PSI:Ljava/lang/String; = "sms_psi"

.field public static final SMS_WRITE_UICC:Ljava/lang/String; = "sms_write_uicc"

.field public static final SPEAKER_DEFAULT_VIDEO:Ljava/lang/String; = "speaker_default_video"

.field public static final SRC_AMR:Ljava/lang/String; = "src_amr"

.field public static final SRC_AMR_WB:Ljava/lang/String; = "src_amr_wb"

.field public static final SRC_THROTTLE_PUBLISH:Ljava/lang/String; = "src_throttle_publish"

.field public static final SRTP:Ljava/lang/String; = "srtp"

.field public static final SS_CONTROL_PREF:Ljava/lang/String; = "ss_control_pref"

.field public static final SS_CSFB_WITH_IMSERROR:Ljava/lang/String; = "ss_csfb_with_imserror"

.field public static final SS_DOMAIN_SETTING:Ljava/lang/String; = "ss_domain_setting"

.field public static final STACK_LOG_LEVEL:Ljava/lang/String; = "stack_log_level"

.field public static final SUBSCRIBER_TIMER:Ljava/lang/String; = "subscriber_timer"

.field public static final SUBSCRIBE_FOR_REG:Ljava/lang/String; = "subscribe_for_reg"

.field public static final SUBSCRIBE_LIST_SYNC_ENABLED:Ljava/lang/String; = "subscribe_list_sync_enabled"

.field public static final SUBSCRIBE_MAX_ENTRY:Ljava/lang/String; = "subscribe_max_entry"

.field public static final SUBSCRIPTION_EXPIRY:Ljava/lang/String; = "subscription_expiry"

.field public static final SUPPORT_IMS_IN_ROAMING:Ljava/lang/String; = "support_ims_in_roaming"

.field public static final TABLE_NAME:Ljava/lang/String; = "global"

.field public static final TEXT_PORT:Ljava/lang/String; = "text_port"

.field public static final TIMER_VZW:Ljava/lang/String; = "timer_vzw"

.field public static final TRANSPORT_IN_CONTACT:Ljava/lang/String; = "transport_in_contact"

.field public static final TVOLTE_HYS_TIMER:Ljava/lang/String; = "tvolte_hys_timer"

.field public static final T_DELAY:Ljava/lang/String; = "t_delay"

.field public static final T_LTE_911_FAIL:Ljava/lang/String; = "t_lte_911_fail"

.field public static final UDP_KEEP_ALIVE:Ljava/lang/String; = "udp_keep_alive"

.field public static final UPDATE_AUTOCONFIG:Ljava/lang/String; = "update_autoconfig"

.field public static final URI_MEDIA_RSC_SERV_3WAY_CALL:Ljava/lang/String; = "uri_media_rsc_serv_3way_call"

.field public static final USE_APCS:Ljava/lang/String; = "use_apcs"

.field public static final USE_PRECONDITION:Ljava/lang/String; = "use_precondition"

.field public static final USSD_DOMAIN_SETTING:Ljava/lang/String; = "ussd_domain_setting"

.field public static final UT_APN_NAME:Ljava/lang/String; = "ut_apn_name"

.field public static final UT_APN_SETTING_UI_SHOW:Ljava/lang/String; = "ut_apn_setting_ui_show"

.field public static final UT_PDN:Ljava/lang/String; = "ut_pdn"

.field public static final VIDEO_AS:Ljava/lang/String; = "video_as"

.field public static final VIDEO_CAPABILITIES:Ljava/lang/String; = "video_capabilities"

.field public static final VIDEO_CODEC:Ljava/lang/String; = "video_codec"

.field public static final VIDEO_PORT_END:Ljava/lang/String; = "video_port_end"

.field public static final VIDEO_PORT_START:Ljava/lang/String; = "video_port_start"

.field public static final VIDEO_RR:Ljava/lang/String; = "video_rr"

.field public static final VIDEO_RS:Ljava/lang/String; = "video_rs"

.field public static final VOICE_DOMAIN_PREF_EUTRAN:Ljava/lang/String; = "voice_domain_pref_eutran"

.field public static final VOICE_DOMAIN_PREF_UTRAN:Ljava/lang/String; = "voice_domain_pref_utran"

.field public static final VOLTE_DOMAIN_UI_SHOW:Ljava/lang/String; = "volte_domain_ui_show"

.field public static final VOLTE_ENABLED:Ljava/lang/String; = "dm_volte_enabled"

.field public static final VOLTE_ENABLED_BY_USER:Ljava/lang/String; = "volte_enabled"

.field public static final VOLTE_REGI_ICON_ID:Ljava/lang/String; = "volte_regi_icon_id"

.field public static final VZW_EAB_PUBLISH_DONE:Ljava/lang/String; = "vzw_eab_publish_done"

.field public static final VZW_EAB_PUBLISH_FAIL:Ljava/lang/String; = "vzw_eab_publish_fail"

.field public static final WIFI_SELF_PORT:Ljava/lang/String; = "wifi_self_port"

.field public static final XCAP_ROOT_URI:Ljava/lang/String; = "xcap_root_uri"

.field public static final XCAP_ROOT_URI_PREF:Ljava/lang/String; = "xcap_root_uri_pref"

.field public static final XDM_ENABLE:Ljava/lang/String; = "xdm_enable"

.field public static final XDM_USER_AGENT:Ljava/lang/String; = "xdm_user_agent"

.field public static final XDM_USER_ID_DOMAIN:Ljava/lang/String; = "xdm_user_id_domain"

.field private static final mTableMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 167
    const-string v0, "content://com.sec.ims.settings/global"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->CONTENT_URI:Landroid/net/Uri;

    .line 380
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    .line 382
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "_id"

    const-string v2, "INTEGER PRIMARY KEY"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 383
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "name"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 384
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "network"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 386
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "rcs"

    const-string v2, "INT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 387
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "rcs_ver"

    const-string v2, "INT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 388
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "rcs_local_config_server"

    const-string v2, "INT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 389
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "rcs_home_pref"

    const-string v2, "INT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 390
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "rcs_roaming_pref"

    const-string v2, "INT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 391
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "closed_group_chat"

    const-string v2, "INT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 393
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "local_uri_type"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 394
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "remote_uri_type"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 395
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "subscribe_for_reg"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 396
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "use_precondition"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 397
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "session_expires"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 398
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "ringing_timer"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 399
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "ringback_timer"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 400
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "subscriber_timer"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 401
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "useragent"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 402
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "use_apcs"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 403
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "reg_expires"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 404
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "invite_timeout"

    const-string v2, "INT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 405
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "enable_mwi"

    const-string v2, "INT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 406
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "mwi_subscribe_expiry"

    const-string v2, "INT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 407
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "enable_ciq"

    const-string v2, "INT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 408
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "enable_hasati"

    const-string v2, "INT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 409
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "enable_gruu"

    const-string v2, "INT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 410
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "self_port"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 411
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "wifi_self_port"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 412
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "transport_in_contact"

    const-string v2, "INT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 414
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "conference_uri"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 415
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "conference_subscribe"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "conference_dialog_type"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 417
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "reg_retry_base_time"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 418
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "reg_retry_max_time"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 420
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "update_autoconfig"

    const-string v2, "INT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 421
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "publish_timer"

    const-string v2, "INT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 422
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "publish_expiry"

    const-string v2, "INT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 423
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "subscription_expiry"

    const-string v2, "INT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 424
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "extended_publish_timer"

    const-string v2, "INT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 425
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "enable_gzip"

    const-string v2, "INT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 426
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "expire_header"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 427
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "set_default_uri"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 428
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "subscribe_list_sync_enabled"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 429
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "add_phone_extension"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 430
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "add_pres_list_dummy"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 431
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "anonymous_fetch"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 432
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "bad_event_expiry"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 433
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "cap_cache_exp"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 434
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "cap_poll_interval"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 435
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "src_throttle_publish"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 436
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "subscribe_max_entry"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 437
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "avail_cache_exp"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 438
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "publish_err_retry_timer"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 439
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "poll_list_sub_exp"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 440
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "cap_discovery"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 442
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "xdm_enable"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 443
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "http_username"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 444
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "http_password"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 445
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "xcap_root_uri"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 446
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "agg_proxy_ip"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 447
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "auth_proxy_ip"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 448
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "auth_proxy_port"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 449
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "xdm_user_agent"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 450
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "apn_selection"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 451
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "xcap_root_uri_pref"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 452
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "xdm_user_id_domain"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 453
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "bsf_ip"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 454
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "bsf_port"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 455
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "enable_gba"

    const-string v2, "INT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 457
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "audio_capabilities"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 458
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "audio_codec_mode"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 459
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "dtmf_mode"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 460
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "video_capabilities"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 461
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "enable_g711"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 462
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "custom_codecs"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 463
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "display_format"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 464
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "framerate"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 465
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "log_rtppackets_to_file"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 466
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "bitrate"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 467
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "rtp_timeout"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 468
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "rtcp_timeout"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 469
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "audio_port_start"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 470
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "audio_port_end"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 471
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "video_port_start"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 472
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "video_port_end"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 473
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "text_port"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 474
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "amrnb_mode"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 475
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "amrwb_mode"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 476
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "amr_bundle_rate"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 477
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "enable_cp_audio_path"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 478
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "audio_video_tx"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 479
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "jitter_buffer"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 480
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "presence_list_max_entries"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 481
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "emm"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 482
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "loopback"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 483
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "external_gtty"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 484
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "avpf"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 485
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "srtp"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 486
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "display_fps_bitrate"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 487
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "audio_as"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 488
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "audio_rs"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 489
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "audio_rr"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 490
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "video_as"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 491
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "video_rs"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 492
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "video_rr"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 493
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "amrnbbe_payload"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 494
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "amrnboa_payload"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 495
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "amrwbbe_payload"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 496
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "amrwboa_payload"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 497
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "h264_qvga_payload"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 498
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "h264_vga_payload"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 499
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "h264_lvga_payload"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 500
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "h264_vgal_payload"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 501
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "dtmf_nb_payload"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 502
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "dtmf_wb_payload"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 503
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "video_codec"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 504
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "audio_codec"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 505
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "packetization_mode"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 506
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "dtmf_codec_mode"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 507
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "enable_bwm"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 508
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "enable_bwm_default"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 511
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "support_ims_in_roaming"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 514
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "ims_enabled"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 515
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "apcs_service_status"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 516
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "sms_psi"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 517
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "lvc_beta_setting"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 518
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "dm_eab_enabled"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 519
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "dm_app_id"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 520
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "dm_user_disp_name"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 521
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "dm_con_ref"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 522
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "pdp_context_pref"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 523
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "pcscf_addr"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 524
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "public_user_id"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 525
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "icsi"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 526
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "icsi_rsc_alloc_mode"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 527
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "rsc_alloc_mode"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 528
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "voice_domain_pref_eutran"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 529
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "voice_domain_pref_utran"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 530
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "sms_over_ip_indication"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 531
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "udp_keep_alive"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 532
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "ims_voice_termination"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 533
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "ss_domain_setting"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 534
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "ss_control_pref"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 535
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "dm_polling_period"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 536
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "conf_factory_uri"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 537
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "sms_format"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 538
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "sms_write_uicc"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 539
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "min_se"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 540
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "dcn_number"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 541
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "t_lte_911_fail"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 542
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "domain_pui"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 543
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "uri_media_rsc_serv_3way_call"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 544
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "phone_context_uri"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 545
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "amr_wb"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 546
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "src_amr"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 547
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "src_amr_wb"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 548
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "hd_voice"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 549
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "speaker_default_video"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 550
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "dm_volte_enabled"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 551
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "dm_lvc_enabled"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 552
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "eab_enabled"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 553
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "volte_enabled"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 554
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "lvc_enabled"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 555
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "ussd_domain_setting"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 556
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "emergency_domain_setting"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 557
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "ut_pdn"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 558
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "ut_apn_name"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 559
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "volte_domain_ui_show"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 560
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "sms_domain_ui_show"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 561
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "ut_apn_setting_ui_show"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 562
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "conf_factory_uri_show"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 563
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "enforce_gba_support"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 564
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "tvolte_hys_timer"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 565
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "timer_vzw"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 566
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "t_delay"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 567
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "pip"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 568
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "vzw_eab_publish_fail"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 569
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "vzw_eab_publish_done"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 570
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "home_domain"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 571
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "epdg_enabled"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 572
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "ehrpd_enabled"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 573
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "ss_csfb_with_imserror"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 576
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "ims_config_name"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 577
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "slick_config_name"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 578
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "ims_test_mode"

    const-string v2, "INT DEFAULT 0"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 579
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "enable_all_logs"

    const-string v2, "INT DEFAULT 0"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 580
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "sip_uri_only"

    const-string v2, "INT DEFAULT 0"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 581
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "show_volte_regi_icon"

    const-string v2, "INT DEFAULT 0"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 582
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "show_reg_info_to_setting_app"

    const-string v2, "INT DEFAULT 0"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 583
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "volte_regi_icon_id"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 584
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "log_level"

    const-string v2, "INT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 585
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 164
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 165
    return-void
.end method

.method public static getTableMap()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 588
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$GlobalTable;->mTableMap:Ljava/util/HashMap;

    return-object v0
.end method
