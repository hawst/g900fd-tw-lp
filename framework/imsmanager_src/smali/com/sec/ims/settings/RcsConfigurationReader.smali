.class public Lcom/sec/ims/settings/RcsConfigurationReader;
.super Ljava/lang/Object;
.source "RcsConfigurationReader.java"


# static fields
.field public static final APPAUTH_PATH:Ljava/lang/String; = "root/application/0/appauth/"

.field public static final AUTHORITY:Ljava/lang/String; = "com.samsung.rcs.autoconfigurationprovider"

.field public static final AUTOCONFIG_COMPLETED:Ljava/lang/String; = "info/completed"

.field public static final AUTO_CONFIGURATION_URI:Landroid/net/Uri;

.field public static final AUT_ACCEPT:Ljava/lang/String; = "root/application/1/im/autaccept"

.field public static final AUT_ACCEPT_GROUP_CHAT:Ljava/lang/String; = "root/application/1/im/autacceptgroupchat"

.field public static final CAPDISCOVERY_CAPINFO_EXPIRY:Ljava/lang/String; = "root/application/1/capdiscovery/capinfoexpiry"

.field public static final CAPDISCOVERY_CHARACTERISTIC_PATH:Ljava/lang/String; = "root/application/1/capdiscovery/"

.field public static final CAPDISCOVERY_DEFAULT_DISC:Ljava/lang/String; = "root/application/1/capdiscovery/defaultdisc"

.field public static final CAPDISCOVERY_MAX_ENTRIES_IN_LIST:Ljava/lang/String; = "root/application/1/capdiscovery/maxentriesinlist"

.field public static final CAPDISCOVERY_POLLING_PERIOD:Ljava/lang/String; = "root/application/1/capdiscovery/pollingperiod"

.field public static final CAPDISCOVERY_POLLING_RATE:Ljava/lang/String; = "root/application/1/capdiscovery/pollingrate"

.field public static final CAPDISCOVERY_POLLING_RATE_PERIOD:Ljava/lang/String; = "root/application/1/capdiscovery/pollingrateperiod"

.field public static final CHAT_ENABLED:Ljava/lang/String; = "root/application/1/services/ChatAuth"

.field public static final CONFIG_VERSION:Ljava/lang/String; = "root/vers/version"

.field public static final CONF_FCTY_URI:Ljava/lang/String; = "root/application/1/im/conf-fcty-uri"

.field public static final CPM_CNTRL_FNCN_URI:Ljava/lang/String; = "root/application/1/im/CPMControllingFuncUri"

.field public static final DEFERRED_MSG_FUNC_URI:Ljava/lang/String; = "root/application/1/im/deferred-msg-func-uri"

.field public static final EXPLODER_URI:Ljava/lang/String; = "root/application/1/im/exploder-uri"

.field public static final FIRST_MSG_INVITE:Ljava/lang/String; = "root/application/1/im/firstMessageInvite"

.field public static final FT_AUT_ACCEPT:Ljava/lang/String; = "root/application/1/im/ftautaccept"

.field public static final FT_CAP_ALWAYS_ON:Ljava/lang/String; = "root/application/1/im/ftCapalwaysON"

.field public static final FT_DEFAULT_MECH:Ljava/lang/String; = "root/application/1/im/ftDefaultMech"

.field public static final FT_ENABLED:Ljava/lang/String; = "root/application/1/services/ftAuth"

.field public static final FT_HTTP_CS_PWD:Ljava/lang/String; = "root/application/1/im/ftHTTPCSPwd"

.field public static final FT_HTTP_CS_URI:Ljava/lang/String; = "root/application/1/im/ftHTTPCSURI"

.field public static final FT_HTTP_CS_USER:Ljava/lang/String; = "root/application/1/im/ftHTTPCSUser"

.field public static final FT_ST_AND_FW_ENABLED:Ljava/lang/String; = "root/application/1/im/ftStAnd"

.field public static final FT_THUMB:Ljava/lang/String; = "root/application/1/im/ftThumb"

.field public static final FT_WARN_SIZE:Ljava/lang/String; = "root/application/1/im/ftWarnSize"

.field public static final GEOPULL_ENABLED:Ljava/lang/String; = "root/application/1/services/geolocPullAuth"

.field public static final GEOPUSH_ENABLED:Ljava/lang/String; = "root/application/1/services/geolocPushAuth"

.field public static final GROUP_CHAT_ENABLED:Ljava/lang/String; = "root/application/1/services/GroupChatAuth"

.field public static final GROUP_CHAT_FULL_STAND_FWD:Ljava/lang/String; = "root/application/1/im/GroupChatFullStandFwd"

.field public static final GROUP_CHAT_ONLY_F_STAND_FWD:Ljava/lang/String; = "root/application/1/im/GroupChatOnlyFStandFwd"

.field public static final HOME_NETWORK_DOMAIN_NAME:Ljava/lang/String; = "root/application/0home_network_domain_name"

.field public static final IMS_APPLICATION_CHARACTERISTIC_PATH:Ljava/lang/String; = "root/application/0"

.field public static final IMS_APPLICATION_EXT_CHARACTERISTIC_PATH:Ljava/lang/String; = "root/application/0/ext/"

.field public static final IM_CAP_ALWAYS_ON:Ljava/lang/String; = "root/application/1/im/imcapalwayson"

.field public static final IM_CAP_NON_RCS:Ljava/lang/String; = "root/application/1/im/imCapNonRCS"

.field public static final IM_CHARACTERISTIC_PATH:Ljava/lang/String; = "root/application/1/im"

.field public static final IM_MAX_SIZE_1_TO_1:Ljava/lang/String; = "root/application/1/im/maxsize1to1"

.field public static final IM_MAX_SIZE_1_TO_M:Ljava/lang/String; = "root/application/1/im/maxsize1tom"

.field public static final IM_MSG_TECH:Ljava/lang/String; = "root/application/1/im/imMsgTech"

.field public static final IM_SESSION_START:Ljava/lang/String; = "root/application/1/im/imsessionstart"

.field public static final IM_WARN_IW:Ljava/lang/String; = "root/application/1/im/imwarniw"

.field public static final IM_WARN_SF:Ljava/lang/String; = "root/application/1/im/imWarnSF"

.field public static final INT_URL_FORMAT:Ljava/lang/String; = "root/application/0/ext/inturlfmt"

.field public static final IS_ENABLED:Ljava/lang/String; = "root/application/1/services/isAuth"

.field public static final LBO_PCSCF_ADDRESS:Ljava/lang/String; = "root/application/0/lbo_p-cscf_address/0/address"

.field public static final LBO_PCSCF_ADDRESS_TYPE:Ljava/lang/String; = "root/application/0/lbo_p-cscf_address/0/addresstype"

.field public static final MAX_ADHOC_GROUP_SIZE:Ljava/lang/String; = "root/application/1/im/max_adhoc_group_size"

.field public static final MAX_CONCURRENT_SESSION:Ljava/lang/String; = "root/application/1/im/maxConcurrentSession"

.field public static final MAX_SIZE_FILE_TR:Ljava/lang/String; = "root/application/1/im/MaxSizeFileTr"

.field public static final MAX_SIZE_IMAGE_SHARE:Ljava/lang/String; = "root/application/0/ext/maxsizeimageshare"

.field public static final MAX_TIME_VIDEO_SHARE:Ljava/lang/String; = "root/application/0/ext/maxtimevideoshare"

.field public static final MULTIMEDIA_CHAT:Ljava/lang/String; = "root/application/1/im/multiMediaChat"

.field public static final OTHER_CHARACTERISTIC_PATH:Ljava/lang/String; = "root/application/1/other/"

.field public static final PRESENCE_CHARACTERISTIC_PATH:Ljava/lang/String; = "root/application/1/presence/"

.field public static final PRESENCE_MAX_SUBSCRIPTION_URI:Ljava/lang/String; = "root/application/1/presence/max-number-ofsubscriptions-inpresence-list"

.field public static final PRESENCE_PUBLISH_TIMER:Ljava/lang/String; = "root/application/1/presence/PublishTimer"

.field public static final PRESENCE_RLS_URI:Ljava/lang/String; = "root/application/1/presence/RLS-URI"

.field public static final PRESENCE_THROTTLE_PUBLISH:Ljava/lang/String; = "root/application/1/presence/source-throttlepublish"

.field public static final PRES_SRV_CAP:Ljava/lang/String; = "root/application/1/im/pres-srv-cap"

.field public static final PRIVATE_USER_IDENTITY:Ljava/lang/String; = "root/application/0/private_user_identity"

.field public static final PS_MEDIA:Ljava/lang/String; = "root/application/1/other/transportproto/psmedia"

.field public static final PS_SIGNALLING:Ljava/lang/String; = "root/application/1/other/transportproto/pssignalling"

.field public static final PUBLIC_USER_IDENTITY_LIST:Ljava/lang/String; = "root/application/0/public_user_identity_list"

.field public static final Q_VALUE:Ljava/lang/String; = "root/application/0/ext/q-value"

.field public static final RCS_VIDEO_ENABLED:Ljava/lang/String; = "root/application/1/services/rcsIPVideoCallAuth"

.field public static final RCS_VOICE_ENABLED:Ljava/lang/String; = "root/application/1/services/rcsIPVoiceCallAuth"

.field public static final REALM:Ljava/lang/String; = "root/application/0/appauth/realm"

.field public static final SERVICE_CHRACTERISTIC_PATH:Ljava/lang/String; = "root/application/1/services/"

.field public static final SLM_CHARACTERISTIC_PATH:Ljava/lang/String; = "root/application/1/cpm/standalonemsg"

.field public static final SLM_ENABLED:Ljava/lang/String; = "root/application/1/services/standaloneMsgAuth"

.field public static final SLM_MAX_MSG_SIZE:Ljava/lang/String; = "root/application/1/cpm/standalonemsg/SLMMaxMsgSize"

.field public static final SMS_FALLBACK_AUTH:Ljava/lang/String; = "root/application/1/im/smsfallbackauth"

.field public static final SOCIAL_PRESENCE_ENABLED:Ljava/lang/String; = "root/application/1/services/presencePrfl"

.field private static final TAG:Ljava/lang/String;

.field public static final TIMER_IDLE:Ljava/lang/String; = "root/application/1/im/TimerIdle"

.field public static final TRANSPORT_PROTO_CHARACTERISTIC_PATH:Ljava/lang/String; = "root/application/1/other/transportproto/"

.field public static final USERPWD:Ljava/lang/String; = "root/application/0/appauth/userpwd"

.field public static final USER_NAME:Ljava/lang/String; = "root/application/0/appauth/UserName"

.field public static final USER_PWD:Ljava/lang/String; = "root/application/0/appauth/UserPwd"

.field public static final VS_ENABLED:Ljava/lang/String; = "root/application/1/services/vsAuth"

.field public static final WARN_SIZE_IMAGE_SHARE:Ljava/lang/String; = "root/application/1/other/warnsizeimageshare"

.field public static final WIFI_MEDIA:Ljava/lang/String; = "root/application/1/other/transportproto/wifimedia"

.field public static final WIFI_SIGNALLING:Ljava/lang/String; = "root/application/1/other/transportproto/wifisignalling"

.field public static final XCAP_ROOT_URI:Ljava/lang/String; = "root/application/1/xdms/xcaprooturi"

.field public static final XDMS_PATH:Ljava/lang/String; = "root/application/1/xdms/"


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/sec/ims/settings/RcsConfigurationReader;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/ims/settings/RcsConfigurationReader;->TAG:Ljava/lang/String;

    .line 26
    const-string v0, "content://com.samsung.rcs.autoconfigurationprovider/"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/ims/settings/RcsConfigurationReader;->AUTO_CONFIGURATION_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 159
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 160
    iput-object p1, p0, Lcom/sec/ims/settings/RcsConfigurationReader;->mContext:Landroid/content/Context;

    .line 161
    return-void
.end method

.method private getValue(Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 164
    sget-object v0, Lcom/sec/ims/settings/RcsConfigurationReader;->AUTO_CONFIGURATION_URI:Landroid/net/Uri;

    invoke-static {v0, p1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 165
    .local v1, "uri":Landroid/net/Uri;
    const/4 v8, 0x0

    .line 167
    .local v8, "value":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/ims/settings/RcsConfigurationReader;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 168
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 169
    invoke-interface {v6, p1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    .line 170
    .local v7, "index":I
    invoke-interface {v6, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 173
    .end local v7    # "index":I
    :cond_0
    if-eqz v6, :cond_1

    .line 174
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 177
    :cond_1
    return-object v8
.end method


# virtual methods
.method public getBoolean(Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 5
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 217
    invoke-direct {p0, p1}, Lcom/sec/ims/settings/RcsConfigurationReader;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 219
    .local v0, "BooleanValue":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 221
    :try_start_0
    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 228
    :goto_0
    return-object v2

    .line 222
    :catch_0
    move-exception v1

    .line 223
    .local v1, "e":Ljava/lang/NumberFormatException;
    sget-object v2, Lcom/sec/ims/settings/RcsConfigurationReader;->TAG:Ljava/lang/String;

    const-string v3, "Error while parsing integer in parseBoolean() - NumberFormatException"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 224
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto :goto_0

    .line 227
    .end local v1    # "e":Ljava/lang/NumberFormatException;
    :cond_0
    sget-object v2, Lcom/sec/ims/settings/RcsConfigurationReader;->TAG:Ljava/lang/String;

    const-string v3, "getIntValue: String value used as Boolean is null. Returning -1 instead."

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 228
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto :goto_0
.end method

.method public getInt(Ljava/lang/String;)I
    .locals 5
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    const/4 v2, -0x1

    .line 185
    invoke-direct {p0, p1}, Lcom/sec/ims/settings/RcsConfigurationReader;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 187
    .local v1, "intValue":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 189
    :try_start_0
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 196
    :goto_0
    return v2

    .line 190
    :catch_0
    move-exception v0

    .line 191
    .local v0, "e":Ljava/lang/NumberFormatException;
    sget-object v3, Lcom/sec/ims/settings/RcsConfigurationReader;->TAG:Ljava/lang/String;

    const-string v4, "Error while parsing integer in getIntValue() - NumberFormatException"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 195
    .end local v0    # "e":Ljava/lang/NumberFormatException;
    :cond_0
    sget-object v3, Lcom/sec/ims/settings/RcsConfigurationReader;->TAG:Ljava/lang/String;

    const-string v4, "getIntValue: String value used as integer is null. Returning -1 instead."

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getLong(Ljava/lang/String;)J
    .locals 6
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    const-wide/16 v2, -0x1

    .line 201
    invoke-direct {p0, p1}, Lcom/sec/ims/settings/RcsConfigurationReader;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 203
    .local v1, "longValue":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 205
    :try_start_0
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 212
    :goto_0
    return-wide v2

    .line 206
    :catch_0
    move-exception v0

    .line 207
    .local v0, "e":Ljava/lang/NumberFormatException;
    sget-object v4, Lcom/sec/ims/settings/RcsConfigurationReader;->TAG:Ljava/lang/String;

    const-string v5, "Error while parsing integer in parseLong() - NumberFormatException"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 211
    .end local v0    # "e":Ljava/lang/NumberFormatException;
    :cond_0
    sget-object v4, Lcom/sec/ims/settings/RcsConfigurationReader;->TAG:Ljava/lang/String;

    const-string v5, "getIntValue: String value used as long is null. Returning -1 instead."

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 181
    invoke-direct {p0, p1}, Lcom/sec/ims/settings/RcsConfigurationReader;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
