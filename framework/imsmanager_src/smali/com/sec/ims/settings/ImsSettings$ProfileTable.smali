.class public final Lcom/sec/ims/settings/ImsSettings$ProfileTable;
.super Ljava/lang/Object;
.source "ImsSettings.java"

# interfaces
.implements Landroid/provider/BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/ims/settings/ImsSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ProfileTable"
.end annotation


# static fields
.field public static final AUTHNAME:Ljava/lang/String; = "authname"

.field public static final AUTH_ALGO:Ljava/lang/String; = "auth_algo"

.field public static final CONTENT_URI:Landroid/net/Uri;

.field public static final DEFAULT_MCCMNC:Ljava/lang/String; = "default_mccmnc"

.field public static final DOMAIN:Ljava/lang/String; = "domain"

.field public static final EMERGENCY_SUPPORT:Ljava/lang/String; = "emergency_support"

.field public static final ENABLED:Ljava/lang/String; = "enabled"

.field public static final ENC_ALGO:Ljava/lang/String; = "enc_algo"

.field public static final GLOBAL:Ljava/lang/String; = "global"

.field public static final IMPI:Ljava/lang/String; = "impi"

.field public static final IMPU:Ljava/lang/String; = "impu"

.field public static final IPSEC:Ljava/lang/String; = "ipsec"

.field public static final IP_VERSION:Ljava/lang/String; = "ipver"

.field public static final MCCMNC:Ljava/lang/String; = "mccmnc"

.field public static final NAME:Ljava/lang/String; = "name"

.field public static final NETWORK:Ljava/lang/String; = "network"

.field public static final NETWORK_ENABLED:Ljava/lang/String; = "network_enabled"

.field public static final PASSWORD:Ljava/lang/String; = "password"

.field public static final PCSCF:Ljava/lang/String; = "pcscf"

.field public static final PCSCF_PREF:Ljava/lang/String; = "pcscf_pref"

.field public static final PDN:Ljava/lang/String; = "pdn"

.field public static final PRIORITY:Ljava/lang/String; = "priority"

.field public static final REGI_ALGO:Ljava/lang/String; = "regi_algo"

.field public static final REG_ENALBED:Ljava/lang/String; = "reg_enabled"

.field public static final ROAMING:Ljava/lang/String; = "roaming"

.field public static final RPORT:Ljava/lang/String; = "rport"

.field public static final SECURE_CLIENT_PORT:Ljava/lang/String; = "secure_client_port"

.field public static final SECURE_SERVER_PORT:Ljava/lang/String; = "secure_server_port"

.field public static final SERVICE:Ljava/lang/String; = "service"

.field public static final SIP_PORT:Ljava/lang/String; = "sip_port"

.field public static final SIP_TIMER_1:Ljava/lang/String; = "sip_timer_1"

.field public static final SIP_TIMER_2:Ljava/lang/String; = "sip_timer_2"

.field public static final SIP_TIMER_4:Ljava/lang/String; = "sip_timer_4"

.field public static final SIP_TIMER_A:Ljava/lang/String; = "sip_timer_a"

.field public static final SIP_TIMER_B:Ljava/lang/String; = "sip_timer_b"

.field public static final SIP_TIMER_C:Ljava/lang/String; = "sip_timer_c"

.field public static final SIP_TIMER_D:Ljava/lang/String; = "sip_timer_d"

.field public static final SIP_TIMER_E:Ljava/lang/String; = "sip_timer_e"

.field public static final SIP_TIMER_F:Ljava/lang/String; = "sip_timer_f"

.field public static final SIP_TIMER_G:Ljava/lang/String; = "sip_timer_g"

.field public static final SIP_TIMER_H:Ljava/lang/String; = "sip_timer_h"

.field public static final SIP_TIMER_I:Ljava/lang/String; = "sip_timer_i"

.field public static final SIP_TIMER_J:Ljava/lang/String; = "sip_timer_j"

.field public static final SIP_TIMER_K:Ljava/lang/String; = "sip_timer_k"

.field public static final TABLE_NAME:Ljava/lang/String; = "profile"

.field public static final TRANSPORT:Ljava/lang/String; = "transport"

.field public static final _ID:Ljava/lang/String; = "id"

.field private static final mTableMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 56
    const-string v0, "content://com.sec.ims.settings/profile"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/ims/settings/ImsSettings$ProfileTable;->CONTENT_URI:Landroid/net/Uri;

    .line 109
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/ims/settings/ImsSettings$ProfileTable;->mTableMap:Ljava/util/HashMap;

    .line 111
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$ProfileTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "id"

    const-string v2, "INTEGER PRIMARY KEY"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 112
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$ProfileTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "enabled"

    const-string v2, "INT DEFAULT 2"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$ProfileTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "name"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$ProfileTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "impi"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 115
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$ProfileTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "impu"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$ProfileTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "domain"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$ProfileTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "mccmnc"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$ProfileTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "default_mccmnc"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 119
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$ProfileTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "pdn"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 120
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$ProfileTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "network"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 121
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$ProfileTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "service"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 122
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$ProfileTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "network_enabled"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 123
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$ProfileTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "priority"

    const-string v2, "INT DEFAULT 0"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$ProfileTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "authname"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$ProfileTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "password"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$ProfileTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "pcscf"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 127
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$ProfileTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "pcscf_pref"

    const-string v2, "INT DEFAULT 0"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$ProfileTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "roaming"

    const-string v2, "INT DEFAULT 0"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 129
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$ProfileTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "sip_port"

    const-string v2, "INT DEFAULT 5060"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 130
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$ProfileTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "transport"

    const-string v2, "TEXT DEFAULT \'udp\'"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 131
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$ProfileTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "ipver"

    const-string v2, "TEXT DEFAULT \'ipv4v6\'"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$ProfileTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "regi_algo"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 133
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$ProfileTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "sip_timer_1"

    const-string v2, "INT DEFAULT 500"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 134
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$ProfileTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "sip_timer_2"

    const-string v2, "INT DEFAULT 4000"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$ProfileTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "sip_timer_4"

    const-string v2, "INT DEFAULT 5000"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 136
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$ProfileTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "sip_timer_a"

    const-string v2, "INT DEFAULT 500"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$ProfileTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "sip_timer_b"

    const-string v2, "INT DEFAULT 32000"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$ProfileTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "sip_timer_c"

    const-string v2, "INT DEFAULT 72000"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$ProfileTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "sip_timer_d"

    const-string v2, "INT DEFAULT 32000"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 140
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$ProfileTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "sip_timer_e"

    const-string v2, "INT DEFAULT 500"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 141
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$ProfileTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "sip_timer_f"

    const-string v2, "INT DEFAULT 32000"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 142
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$ProfileTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "sip_timer_g"

    const-string v2, "INT DEFAULT 500"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 143
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$ProfileTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "sip_timer_h"

    const-string v2, "INT DEFAULT 32000"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 144
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$ProfileTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "sip_timer_i"

    const-string v2, "INT DEFAULT 5000"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 145
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$ProfileTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "sip_timer_j"

    const-string v2, "INT DEFAULT 32000"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 146
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$ProfileTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "sip_timer_k"

    const-string v2, "INT DEFAULT 5000"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 147
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$ProfileTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "ipsec"

    const-string v2, "INT DEFAULT 0"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 148
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$ProfileTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "secure_client_port"

    const-string v2, "INT DEFAULT 5000"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 149
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$ProfileTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "secure_server_port"

    const-string v2, "INT DEFAULT 6000"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 150
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$ProfileTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "enc_algo"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 151
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$ProfileTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "auth_algo"

    const-string v2, "TEXT"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 152
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$ProfileTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "emergency_support"

    const-string v2, "INT DEFAULT 0"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 153
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$ProfileTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "rport"

    const-string v2, "INT DEFAULT 1"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 154
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$ProfileTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "reg_enabled"

    const-string v2, "INT DEFAULT 0"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 155
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$ProfileTable;->mTableMap:Ljava/util/HashMap;

    const-string v1, "global"

    const-string v2, "TEXT DEFAULT \'default\'"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 156
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    return-void
.end method

.method public static getTableMap()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 159
    sget-object v0, Lcom/sec/ims/settings/ImsSettings$ProfileTable;->mTableMap:Ljava/util/HashMap;

    return-object v0
.end method
