.class public Lcom/sec/ims/settings/ImsProfile;
.super Ljava/lang/Object;
.source "ImsProfile.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/ims/settings/ImsProfile$2;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/ims/settings/ImsProfile;",
            ">;"
        }
    .end annotation
.end field

.field public static final ENABLE_STATUS_MANUAL:I = 0x1

.field public static final ENABLE_STATUS_OFF:I = 0x0

.field public static final ENABLE_STATUS_ON:I = 0x2

.field public static final IP_TYPE_IPV4:I = 0x1

.field public static final IP_TYPE_IPV4V6:I = 0x3

.field public static final IP_TYPE_IPV6:I = 0x2

.field public static final LOG_TAG:Ljava/lang/String; = "ImsProfile"

.field public static final PCSCF_PREF_ISIM:I = 0x1

.field public static final PCSCF_PREF_MANUAL:I = 0x2

.field public static final PCSCF_PREF_PCO:I = 0x0

.field public static final PDN_EMERGENCY:Ljava/lang/String; = "emergency"

.field public static final PDN_IMS:Ljava/lang/String; = "ims"

.field public static final PDN_INTERNET:Ljava/lang/String; = "internet"

.field public static final PDN_WIFI:Ljava/lang/String; = "wifi"

.field public static final SERVICE_EUC:Ljava/lang/String; = "euc"

.field public static final SERVICE_FT:Ljava/lang/String; = "ft"

.field public static final SERVICE_FT_HTTP:Ljava/lang/String; = "ft_http"

.field public static final SERVICE_IM:Ljava/lang/String; = "im"

.field public static final SERVICE_IS:Ljava/lang/String; = "is"

.field public static final SERVICE_MMTEL_VOICE:Ljava/lang/String; = "mmtel"

.field public static final SERVICE_MMTEL_VOICE_VIDEO:Ljava/lang/String; = "mmtel-video"

.field public static final SERVICE_OPTIONS:Ljava/lang/String; = "options"

.field public static final SERVICE_PRESENCE:Ljava/lang/String; = "presence"

.field public static final SERVICE_SLM:Ljava/lang/String; = "slm"

.field public static final SERVICE_SMSIP:Ljava/lang/String; = "smsip"

.field public static final SERVICE_SS:Ljava/lang/String; = "ss"

.field public static final SERVICE_VS:Ljava/lang/String; = "vs"

.field public static final SERVICE_XDM:Ljava/lang/String; = "xdm"

.field public static final TIMER_NAME_1:Ljava/lang/String; = "1"

.field public static final TIMER_NAME_2:Ljava/lang/String; = "2"

.field public static final TIMER_NAME_4:Ljava/lang/String; = "4"

.field public static final TIMER_NAME_A:Ljava/lang/String; = "A"

.field public static final TIMER_NAME_B:Ljava/lang/String; = "B"

.field public static final TIMER_NAME_C:Ljava/lang/String; = "C"

.field public static final TIMER_NAME_D:Ljava/lang/String; = "D"

.field public static final TIMER_NAME_E:Ljava/lang/String; = "E"

.field public static final TIMER_NAME_F:Ljava/lang/String; = "F"

.field public static final TIMER_NAME_G:Ljava/lang/String; = "G"

.field public static final TIMER_NAME_H:Ljava/lang/String; = "H"

.field public static final TIMER_NAME_I:Ljava/lang/String; = "I"

.field public static final TIMER_NAME_J:Ljava/lang/String; = "J"

.field public static final TIMER_NAME_K:Ljava/lang/String; = "K"

.field private static final rcsServices:[Ljava/lang/String;

.field private static final volteServices:[Ljava/lang/String;


# instance fields
.field private mActiveNetwork:Ljava/lang/Integer;

.field private mAuthAlgorithm:Ljava/lang/String;

.field private mAuthName:Ljava/lang/String;

.field private mDefaultMcc:Ljava/lang/String;

.field private mDefaultMnc:Ljava/lang/String;

.field private mDomain:Ljava/lang/String;

.field private mEnableStatus:I

.field private mEncAlgorithm:Ljava/lang/String;

.field private mGlobalSettingsName:Ljava/lang/String;

.field private mHasRoamingSupport:Z

.field private mId:I

.field private mImpi:Ljava/lang/String;

.field private mImpuList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mIpVer:I

.field private mIsEmergencySupport:Z

.field private mIsIpSecEnabled:Z

.field private mIsRegEnabled:Z

.field private mMcc:Ljava/lang/String;

.field private mMnc:Ljava/lang/String;

.field private mName:Ljava/lang/String;

.field private final mNetworkEnabledMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private mPassword:Ljava/lang/String;

.field private mPcscfList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mPcscfPreference:I

.field private mPdn:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mPriority:I

.field private mRPort:I

.field private mRegistrationAlgorithm:Ljava/lang/String;

.field private mSaClientPort:I

.field private mSaServerPort:I

.field private final mServiceSetMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private mSipPort:I

.field private mTimer1:I

.field private mTimer2:I

.field private mTimer4:I

.field private mTimerA:I

.field private mTimerB:I

.field private mTimerC:I

.field private mTimerD:I

.field private mTimerE:I

.field private mTimerF:I

.field private mTimerG:I

.field private mTimerH:I

.field private mTimerI:I

.field private mTimerJ:I

.field private mTimerK:I

.field private mTransport:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 183
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "mmtel"

    aput-object v1, v0, v2

    const-string v1, "mmtel-video"

    aput-object v1, v0, v3

    const-string v1, "smsip"

    aput-object v1, v0, v4

    const-string v1, "ss"

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/ims/settings/ImsProfile;->volteServices:[Ljava/lang/String;

    .line 190
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "options"

    aput-object v1, v0, v2

    const-string v1, "presence"

    aput-object v1, v0, v3

    const-string v1, "xdm"

    aput-object v1, v0, v4

    const-string v1, "im"

    aput-object v1, v0, v5

    const-string v1, "ft"

    aput-object v1, v0, v6

    const/4 v1, 0x5

    const-string v2, "slm"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "is"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "vs"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "euc"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/ims/settings/ImsProfile;->rcsServices:[Ljava/lang/String;

    .line 1265
    new-instance v0, Lcom/sec/ims/settings/ImsProfile$1;

    invoke-direct {v0}, Lcom/sec/ims/settings/ImsProfile$1;-><init>()V

    sput-object v0, Lcom/sec/ims/settings/ImsProfile;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    .prologue
    const/16 v4, 0x1388

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 220
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 120
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/ims/settings/ImsProfile;->mId:I

    .line 121
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/ims/settings/ImsProfile;->mEnableStatus:I

    .line 123
    iput-object v1, p0, Lcom/sec/ims/settings/ImsProfile;->mName:Ljava/lang/String;

    .line 124
    iput-object v1, p0, Lcom/sec/ims/settings/ImsProfile;->mMcc:Ljava/lang/String;

    .line 125
    iput-object v1, p0, Lcom/sec/ims/settings/ImsProfile;->mMnc:Ljava/lang/String;

    .line 126
    iput-object v1, p0, Lcom/sec/ims/settings/ImsProfile;->mDefaultMcc:Ljava/lang/String;

    .line 127
    iput-object v1, p0, Lcom/sec/ims/settings/ImsProfile;->mDefaultMnc:Ljava/lang/String;

    .line 128
    iput-object v1, p0, Lcom/sec/ims/settings/ImsProfile;->mImpi:Ljava/lang/String;

    .line 129
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mImpuList:Ljava/util/List;

    .line 130
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mPdn:Ljava/util/List;

    .line 131
    iput-object v1, p0, Lcom/sec/ims/settings/ImsProfile;->mActiveNetwork:Ljava/lang/Integer;

    .line 132
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mServiceSetMap:Ljava/util/Map;

    .line 133
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mNetworkEnabledMap:Ljava/util/Map;

    .line 134
    iput v2, p0, Lcom/sec/ims/settings/ImsProfile;->mPriority:I

    .line 135
    iput-object v1, p0, Lcom/sec/ims/settings/ImsProfile;->mAuthName:Ljava/lang/String;

    .line 136
    iput-object v1, p0, Lcom/sec/ims/settings/ImsProfile;->mPassword:Ljava/lang/String;

    .line 137
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mPcscfList:Ljava/util/List;

    .line 138
    iput v2, p0, Lcom/sec/ims/settings/ImsProfile;->mPcscfPreference:I

    .line 140
    const/16 v0, 0x13c4

    iput v0, p0, Lcom/sec/ims/settings/ImsProfile;->mSipPort:I

    .line 141
    iput v3, p0, Lcom/sec/ims/settings/ImsProfile;->mTransport:I

    .line 142
    iput v3, p0, Lcom/sec/ims/settings/ImsProfile;->mIpVer:I

    .line 143
    const-string v0, "MD5"

    iput-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mRegistrationAlgorithm:Ljava/lang/String;

    .line 145
    iput-boolean v2, p0, Lcom/sec/ims/settings/ImsProfile;->mIsIpSecEnabled:Z

    .line 146
    iput v4, p0, Lcom/sec/ims/settings/ImsProfile;->mSaClientPort:I

    .line 147
    const/16 v0, 0x1770

    iput v0, p0, Lcom/sec/ims/settings/ImsProfile;->mSaServerPort:I

    .line 148
    const-string v0, "aes-cbc"

    iput-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mEncAlgorithm:Ljava/lang/String;

    .line 149
    const-string v0, "hmac-md5-96"

    iput-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mAuthAlgorithm:Ljava/lang/String;

    .line 150
    iput-boolean v2, p0, Lcom/sec/ims/settings/ImsProfile;->mIsEmergencySupport:Z

    .line 151
    iput-object v1, p0, Lcom/sec/ims/settings/ImsProfile;->mGlobalSettingsName:Ljava/lang/String;

    .line 154
    const/16 v0, 0x1f4

    iput v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimer1:I

    .line 155
    const/16 v0, 0xfa0

    iput v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimer2:I

    .line 156
    iput v4, p0, Lcom/sec/ims/settings/ImsProfile;->mTimer4:I

    .line 159
    iget v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimer1:I

    iput v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerA:I

    .line 160
    iget v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimer1:I

    mul-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerB:I

    .line 161
    const v0, 0x2bf20

    iput v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerC:I

    .line 162
    const/16 v0, 0x7d00

    iput v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerD:I

    .line 163
    iget v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimer1:I

    iput v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerE:I

    .line 164
    iget v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimer1:I

    mul-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerF:I

    .line 165
    iget v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimer1:I

    iput v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerG:I

    .line 166
    iget v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimer1:I

    mul-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerH:I

    .line 167
    iget v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimer4:I

    iput v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerI:I

    .line 168
    iget v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimer1:I

    mul-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerJ:I

    .line 169
    iget v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimer4:I

    iput v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerK:I

    .line 171
    iput v3, p0, Lcom/sec/ims/settings/ImsProfile;->mRPort:I

    .line 172
    iput-boolean v2, p0, Lcom/sec/ims/settings/ImsProfile;->mIsRegEnabled:Z

    .line 174
    iput-object v1, p0, Lcom/sec/ims/settings/ImsProfile;->mDomain:Ljava/lang/String;

    .line 222
    return-void
.end method

.method public constructor <init>(IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;IZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZIILjava/lang/String;Ljava/lang/String;ZIZLjava/lang/String;)V
    .locals 3
    .param p1, "id"    # I
    .param p2, "enabled"    # I
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "mccmnc"    # Ljava/lang/String;
    .param p5, "defaultMccMnc"    # Ljava/lang/String;
    .param p6, "impi"    # Ljava/lang/String;
    .param p7, "impu"    # Ljava/lang/String;
    .param p8, "domain"    # Ljava/lang/String;
    .param p9, "pdn"    # Ljava/lang/String;
    .param p10, "network"    # Ljava/lang/String;
    .param p11, "service"    # Ljava/lang/String;
    .param p12, "networkEnabled"    # Ljava/lang/String;
    .param p13, "priority"    # I
    .param p14, "authname"    # Ljava/lang/String;
    .param p15, "password"    # Ljava/lang/String;
    .param p16, "pcscf"    # Ljava/lang/String;
    .param p17, "pcscfPref"    # I
    .param p18, "roaming"    # Z
    .param p19, "port"    # I
    .param p20, "transport"    # Ljava/lang/String;
    .param p21, "ipver"    # Ljava/lang/String;
    .param p22, "regiAlgo"    # Ljava/lang/String;
    .param p23, "ipsec"    # Z
    .param p24, "saPortC"    # I
    .param p25, "saPortS"    # I
    .param p26, "encAlgo"    # Ljava/lang/String;
    .param p27, "authAlgo"    # Ljava/lang/String;
    .param p28, "isEmergencySupport"    # Z
    .param p29, "rport"    # I
    .param p30, "isRegEnabled"    # Z
    .param p31, "globalSettings"    # Ljava/lang/String;

    .prologue
    .line 230
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 120
    const/4 v1, -0x1

    iput v1, p0, Lcom/sec/ims/settings/ImsProfile;->mId:I

    .line 121
    const/4 v1, 0x2

    iput v1, p0, Lcom/sec/ims/settings/ImsProfile;->mEnableStatus:I

    .line 123
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/ims/settings/ImsProfile;->mName:Ljava/lang/String;

    .line 124
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/ims/settings/ImsProfile;->mMcc:Ljava/lang/String;

    .line 125
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/ims/settings/ImsProfile;->mMnc:Ljava/lang/String;

    .line 126
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/ims/settings/ImsProfile;->mDefaultMcc:Ljava/lang/String;

    .line 127
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/ims/settings/ImsProfile;->mDefaultMnc:Ljava/lang/String;

    .line 128
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/ims/settings/ImsProfile;->mImpi:Ljava/lang/String;

    .line 129
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/ims/settings/ImsProfile;->mImpuList:Ljava/util/List;

    .line 130
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/ims/settings/ImsProfile;->mPdn:Ljava/util/List;

    .line 131
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/ims/settings/ImsProfile;->mActiveNetwork:Ljava/lang/Integer;

    .line 132
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/sec/ims/settings/ImsProfile;->mServiceSetMap:Ljava/util/Map;

    .line 133
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/sec/ims/settings/ImsProfile;->mNetworkEnabledMap:Ljava/util/Map;

    .line 134
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/ims/settings/ImsProfile;->mPriority:I

    .line 135
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/ims/settings/ImsProfile;->mAuthName:Ljava/lang/String;

    .line 136
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/ims/settings/ImsProfile;->mPassword:Ljava/lang/String;

    .line 137
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/ims/settings/ImsProfile;->mPcscfList:Ljava/util/List;

    .line 138
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/ims/settings/ImsProfile;->mPcscfPreference:I

    .line 140
    const/16 v1, 0x13c4

    iput v1, p0, Lcom/sec/ims/settings/ImsProfile;->mSipPort:I

    .line 141
    const/4 v1, 0x1

    iput v1, p0, Lcom/sec/ims/settings/ImsProfile;->mTransport:I

    .line 142
    const/4 v1, 0x1

    iput v1, p0, Lcom/sec/ims/settings/ImsProfile;->mIpVer:I

    .line 143
    const-string v1, "MD5"

    iput-object v1, p0, Lcom/sec/ims/settings/ImsProfile;->mRegistrationAlgorithm:Ljava/lang/String;

    .line 145
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/ims/settings/ImsProfile;->mIsIpSecEnabled:Z

    .line 146
    const/16 v1, 0x1388

    iput v1, p0, Lcom/sec/ims/settings/ImsProfile;->mSaClientPort:I

    .line 147
    const/16 v1, 0x1770

    iput v1, p0, Lcom/sec/ims/settings/ImsProfile;->mSaServerPort:I

    .line 148
    const-string v1, "aes-cbc"

    iput-object v1, p0, Lcom/sec/ims/settings/ImsProfile;->mEncAlgorithm:Ljava/lang/String;

    .line 149
    const-string v1, "hmac-md5-96"

    iput-object v1, p0, Lcom/sec/ims/settings/ImsProfile;->mAuthAlgorithm:Ljava/lang/String;

    .line 150
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/ims/settings/ImsProfile;->mIsEmergencySupport:Z

    .line 151
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/ims/settings/ImsProfile;->mGlobalSettingsName:Ljava/lang/String;

    .line 154
    const/16 v1, 0x1f4

    iput v1, p0, Lcom/sec/ims/settings/ImsProfile;->mTimer1:I

    .line 155
    const/16 v1, 0xfa0

    iput v1, p0, Lcom/sec/ims/settings/ImsProfile;->mTimer2:I

    .line 156
    const/16 v1, 0x1388

    iput v1, p0, Lcom/sec/ims/settings/ImsProfile;->mTimer4:I

    .line 159
    iget v1, p0, Lcom/sec/ims/settings/ImsProfile;->mTimer1:I

    iput v1, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerA:I

    .line 160
    iget v1, p0, Lcom/sec/ims/settings/ImsProfile;->mTimer1:I

    mul-int/lit8 v1, v1, 0x40

    iput v1, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerB:I

    .line 161
    const v1, 0x2bf20

    iput v1, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerC:I

    .line 162
    const/16 v1, 0x7d00

    iput v1, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerD:I

    .line 163
    iget v1, p0, Lcom/sec/ims/settings/ImsProfile;->mTimer1:I

    iput v1, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerE:I

    .line 164
    iget v1, p0, Lcom/sec/ims/settings/ImsProfile;->mTimer1:I

    mul-int/lit8 v1, v1, 0x40

    iput v1, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerF:I

    .line 165
    iget v1, p0, Lcom/sec/ims/settings/ImsProfile;->mTimer1:I

    iput v1, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerG:I

    .line 166
    iget v1, p0, Lcom/sec/ims/settings/ImsProfile;->mTimer1:I

    mul-int/lit8 v1, v1, 0x40

    iput v1, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerH:I

    .line 167
    iget v1, p0, Lcom/sec/ims/settings/ImsProfile;->mTimer4:I

    iput v1, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerI:I

    .line 168
    iget v1, p0, Lcom/sec/ims/settings/ImsProfile;->mTimer1:I

    mul-int/lit8 v1, v1, 0x40

    iput v1, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerJ:I

    .line 169
    iget v1, p0, Lcom/sec/ims/settings/ImsProfile;->mTimer4:I

    iput v1, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerK:I

    .line 171
    const/4 v1, 0x1

    iput v1, p0, Lcom/sec/ims/settings/ImsProfile;->mRPort:I

    .line 172
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/ims/settings/ImsProfile;->mIsRegEnabled:Z

    .line 174
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/ims/settings/ImsProfile;->mDomain:Ljava/lang/String;

    .line 231
    iput p1, p0, Lcom/sec/ims/settings/ImsProfile;->mId:I

    .line 232
    iput p2, p0, Lcom/sec/ims/settings/ImsProfile;->mEnableStatus:I

    .line 234
    iput-object p3, p0, Lcom/sec/ims/settings/ImsProfile;->mName:Ljava/lang/String;

    .line 235
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p4}, Lcom/sec/ims/settings/ImsProfileLoader;->isNumeric(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p4}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x5

    if-ge v1, v2, :cond_f

    .line 236
    :cond_0
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/ims/settings/ImsProfile;->mMcc:Ljava/lang/String;

    .line 237
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/ims/settings/ImsProfile;->mMnc:Ljava/lang/String;

    .line 238
    if-eqz p4, :cond_1

    const-string v1, "nosim"

    invoke-virtual {p4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 239
    iput-object p4, p0, Lcom/sec/ims/settings/ImsProfile;->mMcc:Ljava/lang/String;

    .line 245
    :cond_1
    :goto_0
    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-static {p5}, Lcom/sec/ims/settings/ImsProfileLoader;->isNumeric(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p4}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x5

    if-ge v1, v2, :cond_10

    .line 246
    :cond_2
    iget-object v1, p0, Lcom/sec/ims/settings/ImsProfile;->mMcc:Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/ims/settings/ImsProfile;->mDefaultMcc:Ljava/lang/String;

    .line 247
    iget-object v1, p0, Lcom/sec/ims/settings/ImsProfile;->mMnc:Ljava/lang/String;

    iput-object v1, p0, Lcom/sec/ims/settings/ImsProfile;->mDefaultMnc:Ljava/lang/String;

    .line 248
    if-eqz p5, :cond_3

    const-string v1, "nosim"

    invoke-virtual {p5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 249
    iput-object p5, p0, Lcom/sec/ims/settings/ImsProfile;->mDefaultMcc:Ljava/lang/String;

    .line 256
    :cond_3
    :goto_1
    if-eqz p6, :cond_4

    .line 257
    iput-object p6, p0, Lcom/sec/ims/settings/ImsProfile;->mImpi:Ljava/lang/String;

    .line 259
    :cond_4
    if-eqz p7, :cond_5

    .line 260
    invoke-virtual {p0, p7}, Lcom/sec/ims/settings/ImsProfile;->setImpuList(Ljava/lang/String;)V

    .line 263
    :cond_5
    iput-object p8, p0, Lcom/sec/ims/settings/ImsProfile;->mDomain:Ljava/lang/String;

    .line 265
    if-eqz p9, :cond_6

    .line 266
    invoke-virtual {p0, p9}, Lcom/sec/ims/settings/ImsProfile;->setPdn(Ljava/lang/String;)V

    .line 269
    :cond_6
    if-eqz p10, :cond_7

    .line 270
    invoke-virtual {p0, p10}, Lcom/sec/ims/settings/ImsProfile;->setNetworkList(Ljava/lang/String;)V

    .line 273
    :cond_7
    invoke-direct {p0, p11}, Lcom/sec/ims/settings/ImsProfile;->unserializeServiceSet(Ljava/lang/String;)V

    .line 274
    iget-object v1, p0, Lcom/sec/ims/settings/ImsProfile;->mNetworkEnabledMap:Ljava/util/Map;

    invoke-direct {p0, p12, v1}, Lcom/sec/ims/settings/ImsProfile;->unserializeNetworkDataValue(Ljava/lang/String;Ljava/util/Map;)V

    .line 276
    move/from16 v0, p13

    iput v0, p0, Lcom/sec/ims/settings/ImsProfile;->mPriority:I

    .line 277
    if-eqz p14, :cond_8

    invoke-virtual/range {p14 .. p14}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_8

    .line 278
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mAuthName:Ljava/lang/String;

    .line 281
    :cond_8
    if-eqz p15, :cond_9

    invoke-virtual/range {p15 .. p15}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_9

    .line 282
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mPassword:Ljava/lang/String;

    .line 285
    :cond_9
    if-eqz p16, :cond_a

    .line 286
    const-string v1, "\\s*,\\s*"

    move-object/from16 v0, p16

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/ims/settings/ImsProfile;->mPcscfList:Ljava/util/List;

    .line 289
    :cond_a
    move/from16 v0, p17

    iput v0, p0, Lcom/sec/ims/settings/ImsProfile;->mPcscfPreference:I

    .line 290
    move/from16 v0, p18

    iput-boolean v0, p0, Lcom/sec/ims/settings/ImsProfile;->mHasRoamingSupport:Z

    .line 292
    move/from16 v0, p19

    iput v0, p0, Lcom/sec/ims/settings/ImsProfile;->mSipPort:I

    .line 294
    if-eqz p20, :cond_b

    .line 295
    const-string v1, "udp"

    move-object/from16 v0, p20

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 296
    const/4 v1, 0x1

    iput v1, p0, Lcom/sec/ims/settings/ImsProfile;->mTransport:I

    .line 304
    :cond_b
    :goto_2
    if-eqz p21, :cond_c

    .line 305
    const-string v1, "ipv4"

    move-object/from16 v0, p21

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 306
    const/4 v1, 0x1

    iput v1, p0, Lcom/sec/ims/settings/ImsProfile;->mIpVer:I

    .line 314
    :cond_c
    :goto_3
    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mRegistrationAlgorithm:Ljava/lang/String;

    .line 316
    move/from16 v0, p23

    iput-boolean v0, p0, Lcom/sec/ims/settings/ImsProfile;->mIsIpSecEnabled:Z

    .line 317
    move/from16 v0, p24

    iput v0, p0, Lcom/sec/ims/settings/ImsProfile;->mSaClientPort:I

    .line 318
    move/from16 v0, p25

    iput v0, p0, Lcom/sec/ims/settings/ImsProfile;->mSaServerPort:I

    .line 320
    if-eqz p26, :cond_d

    .line 321
    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mEncAlgorithm:Ljava/lang/String;

    .line 324
    :cond_d
    if-eqz p27, :cond_e

    .line 325
    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mAuthAlgorithm:Ljava/lang/String;

    .line 327
    :cond_e
    move/from16 v0, p28

    iput-boolean v0, p0, Lcom/sec/ims/settings/ImsProfile;->mIsEmergencySupport:Z

    .line 328
    move/from16 v0, p29

    iput v0, p0, Lcom/sec/ims/settings/ImsProfile;->mRPort:I

    .line 329
    move/from16 v0, p30

    iput-boolean v0, p0, Lcom/sec/ims/settings/ImsProfile;->mIsRegEnabled:Z

    .line 330
    move-object/from16 v0, p31

    iput-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mGlobalSettingsName:Ljava/lang/String;

    .line 331
    return-void

    .line 242
    :cond_f
    const/4 v1, 0x0

    const/4 v2, 0x3

    invoke-virtual {p4, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/ims/settings/ImsProfile;->mMcc:Ljava/lang/String;

    .line 243
    const/4 v1, 0x3

    invoke-virtual {p4, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/ims/settings/ImsProfile;->mMnc:Ljava/lang/String;

    goto/16 :goto_0

    .line 252
    :cond_10
    const/4 v1, 0x0

    const/4 v2, 0x3

    invoke-virtual {p5, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/ims/settings/ImsProfile;->mDefaultMcc:Ljava/lang/String;

    .line 253
    const/4 v1, 0x3

    invoke-virtual {p5, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/ims/settings/ImsProfile;->mDefaultMnc:Ljava/lang/String;

    goto/16 :goto_1

    .line 297
    :cond_11
    const-string v1, "tcp"

    move-object/from16 v0, p20

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 298
    const/4 v1, 0x2

    iput v1, p0, Lcom/sec/ims/settings/ImsProfile;->mTransport:I

    goto :goto_2

    .line 299
    :cond_12
    const-string v1, "tls"

    move-object/from16 v0, p20

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 300
    const/4 v1, 0x3

    iput v1, p0, Lcom/sec/ims/settings/ImsProfile;->mTransport:I

    goto :goto_2

    .line 307
    :cond_13
    const-string v1, "ipv6"

    move-object/from16 v0, p21

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_14

    .line 308
    const/4 v1, 0x2

    iput v1, p0, Lcom/sec/ims/settings/ImsProfile;->mIpVer:I

    goto :goto_3

    .line 309
    :cond_14
    const-string v1, "ipv4v6"

    move-object/from16 v0, p21

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 310
    const/4 v1, 0x3

    iput v1, p0, Lcom/sec/ims/settings/ImsProfile;->mIpVer:I

    goto/16 :goto_3
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 5
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/16 v4, 0x1388

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v1, 0x1

    .line 1278
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 120
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/ims/settings/ImsProfile;->mId:I

    .line 121
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/ims/settings/ImsProfile;->mEnableStatus:I

    .line 123
    iput-object v3, p0, Lcom/sec/ims/settings/ImsProfile;->mName:Ljava/lang/String;

    .line 124
    iput-object v3, p0, Lcom/sec/ims/settings/ImsProfile;->mMcc:Ljava/lang/String;

    .line 125
    iput-object v3, p0, Lcom/sec/ims/settings/ImsProfile;->mMnc:Ljava/lang/String;

    .line 126
    iput-object v3, p0, Lcom/sec/ims/settings/ImsProfile;->mDefaultMcc:Ljava/lang/String;

    .line 127
    iput-object v3, p0, Lcom/sec/ims/settings/ImsProfile;->mDefaultMnc:Ljava/lang/String;

    .line 128
    iput-object v3, p0, Lcom/sec/ims/settings/ImsProfile;->mImpi:Ljava/lang/String;

    .line 129
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mImpuList:Ljava/util/List;

    .line 130
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mPdn:Ljava/util/List;

    .line 131
    iput-object v3, p0, Lcom/sec/ims/settings/ImsProfile;->mActiveNetwork:Ljava/lang/Integer;

    .line 132
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mServiceSetMap:Ljava/util/Map;

    .line 133
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mNetworkEnabledMap:Ljava/util/Map;

    .line 134
    iput v2, p0, Lcom/sec/ims/settings/ImsProfile;->mPriority:I

    .line 135
    iput-object v3, p0, Lcom/sec/ims/settings/ImsProfile;->mAuthName:Ljava/lang/String;

    .line 136
    iput-object v3, p0, Lcom/sec/ims/settings/ImsProfile;->mPassword:Ljava/lang/String;

    .line 137
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mPcscfList:Ljava/util/List;

    .line 138
    iput v2, p0, Lcom/sec/ims/settings/ImsProfile;->mPcscfPreference:I

    .line 140
    const/16 v0, 0x13c4

    iput v0, p0, Lcom/sec/ims/settings/ImsProfile;->mSipPort:I

    .line 141
    iput v1, p0, Lcom/sec/ims/settings/ImsProfile;->mTransport:I

    .line 142
    iput v1, p0, Lcom/sec/ims/settings/ImsProfile;->mIpVer:I

    .line 143
    const-string v0, "MD5"

    iput-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mRegistrationAlgorithm:Ljava/lang/String;

    .line 145
    iput-boolean v2, p0, Lcom/sec/ims/settings/ImsProfile;->mIsIpSecEnabled:Z

    .line 146
    iput v4, p0, Lcom/sec/ims/settings/ImsProfile;->mSaClientPort:I

    .line 147
    const/16 v0, 0x1770

    iput v0, p0, Lcom/sec/ims/settings/ImsProfile;->mSaServerPort:I

    .line 148
    const-string v0, "aes-cbc"

    iput-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mEncAlgorithm:Ljava/lang/String;

    .line 149
    const-string v0, "hmac-md5-96"

    iput-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mAuthAlgorithm:Ljava/lang/String;

    .line 150
    iput-boolean v2, p0, Lcom/sec/ims/settings/ImsProfile;->mIsEmergencySupport:Z

    .line 151
    iput-object v3, p0, Lcom/sec/ims/settings/ImsProfile;->mGlobalSettingsName:Ljava/lang/String;

    .line 154
    const/16 v0, 0x1f4

    iput v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimer1:I

    .line 155
    const/16 v0, 0xfa0

    iput v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimer2:I

    .line 156
    iput v4, p0, Lcom/sec/ims/settings/ImsProfile;->mTimer4:I

    .line 159
    iget v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimer1:I

    iput v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerA:I

    .line 160
    iget v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimer1:I

    mul-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerB:I

    .line 161
    const v0, 0x2bf20

    iput v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerC:I

    .line 162
    const/16 v0, 0x7d00

    iput v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerD:I

    .line 163
    iget v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimer1:I

    iput v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerE:I

    .line 164
    iget v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimer1:I

    mul-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerF:I

    .line 165
    iget v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimer1:I

    iput v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerG:I

    .line 166
    iget v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimer1:I

    mul-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerH:I

    .line 167
    iget v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimer4:I

    iput v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerI:I

    .line 168
    iget v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimer1:I

    mul-int/lit8 v0, v0, 0x40

    iput v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerJ:I

    .line 169
    iget v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimer4:I

    iput v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerK:I

    .line 171
    iput v1, p0, Lcom/sec/ims/settings/ImsProfile;->mRPort:I

    .line 172
    iput-boolean v2, p0, Lcom/sec/ims/settings/ImsProfile;->mIsRegEnabled:Z

    .line 174
    iput-object v3, p0, Lcom/sec/ims/settings/ImsProfile;->mDomain:Ljava/lang/String;

    .line 1279
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/ims/settings/ImsProfile;->mId:I

    .line 1280
    invoke-direct {p0, p1}, Lcom/sec/ims/settings/ImsProfile;->readStringFromParcel(Landroid/os/Parcel;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mName:Ljava/lang/String;

    .line 1281
    invoke-direct {p0, p1}, Lcom/sec/ims/settings/ImsProfile;->readStringFromParcel(Landroid/os/Parcel;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mMcc:Ljava/lang/String;

    .line 1282
    invoke-direct {p0, p1}, Lcom/sec/ims/settings/ImsProfile;->readStringFromParcel(Landroid/os/Parcel;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mMnc:Ljava/lang/String;

    .line 1283
    invoke-direct {p0, p1}, Lcom/sec/ims/settings/ImsProfile;->readStringFromParcel(Landroid/os/Parcel;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mImpi:Ljava/lang/String;

    .line 1284
    iget-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mImpuList:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readStringList(Ljava/util/List;)V

    .line 1285
    invoke-direct {p0, p1}, Lcom/sec/ims/settings/ImsProfile;->readStringFromParcel(Landroid/os/Parcel;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mDomain:Ljava/lang/String;

    .line 1286
    iget-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mPdn:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readStringList(Ljava/util/List;)V

    .line 1287
    const-class v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    iput-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mActiveNetwork:Ljava/lang/Integer;

    .line 1288
    iget-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mServiceSetMap:Ljava/util/Map;

    iget-object v3, p0, Lcom/sec/ims/settings/ImsProfile;->mServiceSetMap:Ljava/util/Map;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    invoke-virtual {p1, v0, v3}, Landroid/os/Parcel;->readMap(Ljava/util/Map;Ljava/lang/ClassLoader;)V

    .line 1289
    iget-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mNetworkEnabledMap:Ljava/util/Map;

    iget-object v3, p0, Lcom/sec/ims/settings/ImsProfile;->mNetworkEnabledMap:Ljava/util/Map;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    invoke-virtual {p1, v0, v3}, Landroid/os/Parcel;->readMap(Ljava/util/Map;Ljava/lang/ClassLoader;)V

    .line 1290
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/ims/settings/ImsProfile;->mPriority:I

    .line 1291
    invoke-direct {p0, p1}, Lcom/sec/ims/settings/ImsProfile;->readStringFromParcel(Landroid/os/Parcel;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mAuthName:Ljava/lang/String;

    .line 1292
    invoke-direct {p0, p1}, Lcom/sec/ims/settings/ImsProfile;->readStringFromParcel(Landroid/os/Parcel;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mPassword:Ljava/lang/String;

    .line 1293
    iget-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mPcscfList:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readStringList(Ljava/util/List;)V

    .line 1294
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/ims/settings/ImsProfile;->mPcscfPreference:I

    .line 1295
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/ims/settings/ImsProfile;->mHasRoamingSupport:Z

    .line 1296
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/ims/settings/ImsProfile;->mSipPort:I

    .line 1297
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTransport:I

    .line 1298
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/ims/settings/ImsProfile;->mIpVer:I

    .line 1299
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mRegistrationAlgorithm:Ljava/lang/String;

    .line 1300
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/sec/ims/settings/ImsProfile;->mIsIpSecEnabled:Z

    .line 1301
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/ims/settings/ImsProfile;->mSaClientPort:I

    .line 1302
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/ims/settings/ImsProfile;->mSaServerPort:I

    .line 1303
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mEncAlgorithm:Ljava/lang/String;

    .line 1304
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mAuthAlgorithm:Ljava/lang/String;

    .line 1305
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimer1:I

    .line 1306
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimer2:I

    .line 1307
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimer4:I

    .line 1308
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerA:I

    .line 1309
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerB:I

    .line 1310
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerC:I

    .line 1311
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerD:I

    .line 1312
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerE:I

    .line 1313
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerF:I

    .line 1314
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerG:I

    .line 1315
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerH:I

    .line 1316
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerI:I

    .line 1317
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerJ:I

    .line 1318
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerK:I

    .line 1319
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/sec/ims/settings/ImsProfile;->mIsEmergencySupport:Z

    .line 1320
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/ims/settings/ImsProfile;->mRPort:I

    .line 1321
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_3

    :goto_3
    iput-boolean v1, p0, Lcom/sec/ims/settings/ImsProfile;->mIsRegEnabled:Z

    .line 1322
    return-void

    :cond_0
    move v0, v2

    .line 1295
    goto/16 :goto_0

    :cond_1
    move v0, v2

    .line 1300
    goto/16 :goto_1

    :cond_2
    move v0, v2

    .line 1319
    goto :goto_2

    :cond_3
    move v1, v2

    .line 1321
    goto :goto_3
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/ims/settings/ImsProfile$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/sec/ims/settings/ImsProfile$1;

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/sec/ims/settings/ImsProfile;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public static getNetworkName(I)Ljava/lang/String;
    .locals 2
    .param p0, "type"    # I

    .prologue
    .line 1126
    sget-object v0, Lcom/sec/ims/settings/ImsProfile$2;->$SwitchMap$com$sec$ims$util$TelephonyManagerExt$NetworkTypeExt:[I

    invoke-static {p0}, Lcom/sec/ims/util/TelephonyManagerExt;->getNetworkEnumType(I)Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1156
    const-string v0, ""

    :goto_0
    return-object v0

    .line 1128
    :pswitch_0
    const-string v0, "lte"

    goto :goto_0

    .line 1130
    :pswitch_1
    const-string v0, "edge"

    goto :goto_0

    .line 1132
    :pswitch_2
    const-string v0, "umts"

    goto :goto_0

    .line 1134
    :pswitch_3
    const-string v0, "gsm"

    goto :goto_0

    .line 1136
    :pswitch_4
    const-string v0, "tdlte"

    goto :goto_0

    .line 1138
    :pswitch_5
    const-string v0, "tdscdma"

    goto :goto_0

    .line 1140
    :pswitch_6
    const-string v0, "cdma"

    goto :goto_0

    .line 1142
    :pswitch_7
    const-string v0, "ehrpd"

    goto :goto_0

    .line 1144
    :pswitch_8
    const-string v0, "wifi"

    goto :goto_0

    .line 1146
    :pswitch_9
    const-string v0, "hspa"

    goto :goto_0

    .line 1148
    :pswitch_a
    const-string v0, "hspa+"

    goto :goto_0

    .line 1150
    :pswitch_b
    const-string v0, "hsdpa"

    goto :goto_0

    .line 1152
    :pswitch_c
    const-string v0, "hsupa"

    goto :goto_0

    .line 1154
    :pswitch_d
    const-string v0, "gprs"

    goto :goto_0

    .line 1126
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
    .end packed-switch
.end method

.method public static getNetworkType(Ljava/lang/String;)I
    .locals 2
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 1162
    const/4 v0, -0x1

    .line 1164
    .local v0, "type":I
    const-string v1, "LTE"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1165
    const/16 v0, 0xd

    .line 1193
    :cond_0
    :goto_0
    return v0

    .line 1166
    :cond_1
    const-string v1, "EDGE"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1167
    const/4 v0, 0x2

    goto :goto_0

    .line 1168
    :cond_2
    const-string v1, "UMTS"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1169
    const/4 v0, 0x3

    goto :goto_0

    .line 1170
    :cond_3
    const-string v1, "GSM"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1171
    const/16 v0, 0x10

    goto :goto_0

    .line 1172
    :cond_4
    const-string v1, "TDLTE"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1173
    sget v0, Lcom/sec/ims/util/TelephonyManagerExt;->NETWORK_TYPE_TDLTE:I

    goto :goto_0

    .line 1174
    :cond_5
    const-string v1, "TDSCDMA"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1175
    const/16 v0, 0x11

    goto :goto_0

    .line 1176
    :cond_6
    const-string v1, "CDMA"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1177
    const/4 v0, 0x4

    goto :goto_0

    .line 1178
    :cond_7
    const-string v1, "EHRPD"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 1179
    const/16 v0, 0xe

    goto :goto_0

    .line 1180
    :cond_8
    const-string v1, "WIFI"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1181
    sget v0, Lcom/sec/ims/util/TelephonyManagerExt;->NETWORK_TYPE_IWLAN:I

    goto :goto_0

    .line 1182
    :cond_9
    const-string v1, "HSPA"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 1183
    const/16 v0, 0xa

    goto :goto_0

    .line 1184
    :cond_a
    const-string v1, "HSPA+"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 1185
    const/16 v0, 0xf

    goto :goto_0

    .line 1186
    :cond_b
    const-string v1, "HSDPA"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 1187
    const/16 v0, 0x8

    goto :goto_0

    .line 1188
    :cond_c
    const-string v1, "HSUPA"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 1189
    const/16 v0, 0x9

    goto/16 :goto_0

    .line 1190
    :cond_d
    const-string v1, "GPRS"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1191
    const/4 v0, 0x1

    goto/16 :goto_0
.end method

.method public static getRcsServiceList()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 217
    sget-object v0, Lcom/sec/ims/settings/ImsProfile;->rcsServices:[Ljava/lang/String;

    return-object v0
.end method

.method public static getVoLteServiceList()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 208
    sget-object v0, Lcom/sec/ims/settings/ImsProfile;->volteServices:[Ljava/lang/String;

    return-object v0
.end method

.method private readStringFromParcel(Landroid/os/Parcel;)Ljava/lang/String;
    .locals 2
    .param p1, "p"    # Landroid/os/Parcel;

    .prologue
    const/4 v0, 0x1

    .line 1333
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v1

    if-ne v1, v0, :cond_0

    .line 1334
    .local v0, "isPresent":Z
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    :goto_1
    return-object v1

    .line 1333
    .end local v0    # "isPresent":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1334
    .restart local v0    # "isPresent":Z
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private unserializeNetworkDataValue(Ljava/lang/String;Ljava/util/Map;)V
    .locals 11
    .param p1, "networkData"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/lang/Boolean;>;"
    const/4 v10, 0x0

    .line 397
    const-string v7, "ImsProfile"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "unserializeNetworkDataValue ["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 398
    const-string v7, ","

    invoke-virtual {p1, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 399
    .local v6, "serviceSets":[Ljava/lang/String;
    const/4 v1, 0x0

    .line 400
    .local v1, "dataValue":Z
    move-object v0, v6

    .local v0, "arr$":[Ljava/lang/String;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v5, v0, v3

    .line 401
    .local v5, "serviceSet":Ljava/lang/String;
    const-string v7, ":"

    invoke-virtual {v5, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 402
    .local v2, "fields":[Ljava/lang/String;
    array-length v7, v2

    const/4 v8, 0x2

    if-eq v7, v8, :cond_0

    .line 403
    const-string v7, "ImsProfile"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "unserializeNetworkDataValue. No specified for network "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    aget-object v9, v2, v10

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 404
    const/4 v1, 0x0

    .line 408
    :goto_1
    aget-object v7, v2, v10

    invoke-static {v7}, Lcom/sec/ims/settings/ImsProfile;->getNetworkType(Ljava/lang/String;)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-interface {p2, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 400
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 406
    :cond_0
    const/4 v7, 0x1

    aget-object v7, v2, v7

    invoke-static {v7}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v1

    goto :goto_1

    .line 410
    .end local v2    # "fields":[Ljava/lang/String;
    .end local v5    # "serviceSet":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method private unserializeServiceSet(Ljava/lang/String;)V
    .locals 12
    .param p1, "service"    # Ljava/lang/String;

    .prologue
    const/4 v11, 0x0

    .line 356
    const-string v8, "ImsProfile"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "service : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 357
    const-string v8, ","

    invoke-virtual {p1, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 358
    .local v5, "serviceSets":[Ljava/lang/String;
    move-object v0, v5

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v4, v0, v2

    .line 359
    .local v4, "serviceSet":Ljava/lang/String;
    const-string v8, ":"

    invoke-virtual {v4, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 361
    .local v1, "fields":[Ljava/lang/String;
    array-length v8, v1

    const/4 v9, 0x2

    if-eq v8, v9, :cond_0

    .line 362
    const-string v8, "ImsProfile"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "No service specified for network "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    aget-object v10, v1, v11

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 363
    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    .line 368
    .local v7, "svcSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :goto_1
    aget-object v8, v1, v11

    invoke-static {v8}, Lcom/sec/ims/settings/ImsProfile;->getNetworkType(Ljava/lang/String;)I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {p0, v8, v7}, Lcom/sec/ims/settings/ImsProfile;->setServiceSet(Ljava/lang/Integer;Ljava/util/Set;)V

    .line 358
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 365
    .end local v7    # "svcSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :cond_0
    const/4 v8, 0x1

    aget-object v8, v1, v8

    const-string v9, "/"

    invoke-virtual {v8, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 366
    .local v6, "services":[Ljava/lang/String;
    new-instance v7, Ljava/util/HashSet;

    invoke-static {v6}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .restart local v7    # "svcSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    goto :goto_1

    .line 370
    .end local v1    # "fields":[Ljava/lang/String;
    .end local v4    # "serviceSet":Ljava/lang/String;
    .end local v6    # "services":[Ljava/lang/String;
    .end local v7    # "svcSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :cond_1
    return-void
.end method

.method private writeStringToParcel(Landroid/os/Parcel;Ljava/lang/String;)V
    .locals 3
    .param p1, "p"    # Landroid/os/Parcel;
    .param p2, "s"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    .line 1325
    if-eqz p2, :cond_1

    move v1, v2

    :goto_0
    int-to-byte v0, v1

    .line 1326
    .local v0, "isPresent":B
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 1327
    if-ne v0, v2, :cond_0

    .line 1328
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1330
    :cond_0
    return-void

    .line 1325
    .end local v0    # "isPresent":B
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 1213
    const/4 v0, 0x0

    return v0
.end method

.method public enable(I)V
    .locals 0
    .param p1, "enable"    # I

    .prologue
    .line 424
    iput p1, p0, Lcom/sec/ims/settings/ImsProfile;->mEnableStatus:I

    .line 425
    return-void
.end method

.method public getActiveNetwork()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 614
    iget-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mActiveNetwork:Ljava/lang/Integer;

    return-object v0
.end method

.method public getActiveServiceSet()Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 619
    iget-object v1, p0, Lcom/sec/ims/settings/ImsProfile;->mActiveNetwork:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 620
    iget-object v1, p0, Lcom/sec/ims/settings/ImsProfile;->mActiveNetwork:Ljava/lang/Integer;

    invoke-virtual {p0, v1}, Lcom/sec/ims/settings/ImsProfile;->getServiceSet(Ljava/lang/Integer;)Ljava/util/Set;

    move-result-object v0

    .line 622
    .local v0, "result":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 623
    const-string v1, "ImsProfile"

    const-string v2, "Empty service set for active network!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 630
    .end local v0    # "result":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :cond_0
    :goto_0
    return-object v0

    .line 629
    :cond_1
    const-string v1, "ImsProfile"

    const-string v2, "Active network is not set!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 630
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    goto :goto_0
.end method

.method public getAllNetworkEnabled()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 705
    iget-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mNetworkEnabledMap:Ljava/util/Map;

    return-object v0
.end method

.method public getAllServiceSet()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 604
    iget-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mServiceSetMap:Ljava/util/Map;

    return-object v0
.end method

.method public getAllServiceSetFromAllNetwork()Ljava/util/Set;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 588
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 589
    .local v2, "retSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const/4 v3, 0x0

    .line 590
    .local v3, "set":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    iget-object v4, p0, Lcom/sec/ims/settings/ImsProfile;->mServiceSetMap:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 591
    .local v1, "key":Ljava/lang/Integer;
    if-eqz v1, :cond_0

    .line 594
    iget-object v4, p0, Lcom/sec/ims/settings/ImsProfile;->mServiceSetMap:Ljava/util/Map;

    invoke-interface {v4, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "set":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    check-cast v3, Ljava/util/Set;

    .line 595
    .restart local v3    # "set":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-eqz v3, :cond_0

    invoke-interface {v3}, Ljava/util/Set;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 598
    invoke-interface {v2, v3}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 600
    .end local v1    # "key":Ljava/lang/Integer;
    :cond_1
    return-object v2
.end method

.method public getAuthAlgorithm()Ljava/lang/String;
    .locals 1

    .prologue
    .line 891
    iget-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mAuthAlgorithm:Ljava/lang/String;

    return-object v0
.end method

.method public getAuthName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 720
    iget-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mAuthName:Ljava/lang/String;

    return-object v0
.end method

.method public getDefaultMcc()Ljava/lang/String;
    .locals 1

    .prologue
    .line 459
    iget-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mDefaultMcc:Ljava/lang/String;

    return-object v0
.end method

.method public getDefaultMnc()Ljava/lang/String;
    .locals 1

    .prologue
    .line 469
    iget-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mDefaultMnc:Ljava/lang/String;

    return-object v0
.end method

.method public getDomain()Ljava/lang/String;
    .locals 1

    .prologue
    .line 505
    iget-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mDomain:Ljava/lang/String;

    return-object v0
.end method

.method public getEnableStatus()I
    .locals 1

    .prologue
    .line 419
    iget v0, p0, Lcom/sec/ims/settings/ImsProfile;->mEnableStatus:I

    return v0
.end method

.method public getEncAlgorithm()Ljava/lang/String;
    .locals 1

    .prologue
    .line 881
    iget-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mEncAlgorithm:Ljava/lang/String;

    return-object v0
.end method

.method public getGlobalSettingsName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 906
    iget-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mGlobalSettingsName:Ljava/lang/String;

    return-object v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 414
    iget v0, p0, Lcom/sec/ims/settings/ImsProfile;->mId:I

    return v0
.end method

.method public getImpi()Ljava/lang/String;
    .locals 1

    .prologue
    .line 479
    iget-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mImpi:Ljava/lang/String;

    return-object v0
.end method

.method public getImpuList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 489
    iget-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mImpuList:Ljava/util/List;

    return-object v0
.end method

.method public getIpVer()I
    .locals 1

    .prologue
    .line 803
    iget v0, p0, Lcom/sec/ims/settings/ImsProfile;->mIpVer:I

    return v0
.end method

.method public getIpVersionName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 808
    iget v0, p0, Lcom/sec/ims/settings/ImsProfile;->mIpVer:I

    packed-switch v0, :pswitch_data_0

    .line 816
    const-string v0, "ipv4"

    :goto_0
    return-object v0

    .line 810
    :pswitch_0
    const-string v0, "ipv4"

    goto :goto_0

    .line 812
    :pswitch_1
    const-string v0, "ipv6"

    goto :goto_0

    .line 814
    :pswitch_2
    const-string v0, "ipv4v6"

    goto :goto_0

    .line 808
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getIpVersionNameForStack()Ljava/lang/String;
    .locals 1

    .prologue
    .line 822
    iget v0, p0, Lcom/sec/ims/settings/ImsProfile;->mIpVer:I

    packed-switch v0, :pswitch_data_0

    .line 830
    const-string v0, "IPv4"

    :goto_0
    return-object v0

    .line 824
    :pswitch_0
    const-string v0, "IPv4"

    goto :goto_0

    .line 826
    :pswitch_1
    const-string v0, "IPv6"

    goto :goto_0

    .line 828
    :pswitch_2
    const-string v0, "IPv4v6"

    goto :goto_0

    .line 822
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getMcc()Ljava/lang/String;
    .locals 1

    .prologue
    .line 439
    iget-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mMcc:Ljava/lang/String;

    return-object v0
.end method

.method public getMnc()Ljava/lang/String;
    .locals 1

    .prologue
    .line 449
    iget-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mMnc:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 429
    iget-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getNetworkNameSet()Ljava/util/Set;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 557
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 559
    .local v2, "list":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-virtual {p0}, Lcom/sec/ims/settings/ImsProfile;->getNetworkSet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 560
    .local v0, "i":Ljava/lang/Integer;
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v3}, Lcom/sec/ims/settings/ImsProfile;->getNetworkName(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 563
    .end local v0    # "i":Ljava/lang/Integer;
    :cond_0
    return-object v2
.end method

.method public getNetworkSet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 552
    iget-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mServiceSetMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public getPassword()Ljava/lang/String;
    .locals 1

    .prologue
    .line 730
    iget-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mPassword:Ljava/lang/String;

    return-object v0
.end method

.method public getPcscfList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 740
    iget-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mPcscfList:Ljava/util/List;

    return-object v0
.end method

.method public getPcscfPreference()I
    .locals 1

    .prologue
    .line 750
    iget v0, p0, Lcom/sec/ims/settings/ImsProfile;->mPcscfPreference:I

    return v0
.end method

.method public getPdn()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 515
    iget-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mPdn:Ljava/util/List;

    return-object v0
.end method

.method public getPdnType()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 525
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 527
    .local v1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    iget-object v3, p0, Lcom/sec/ims/settings/ImsProfile;->mPdn:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 528
    .local v2, "pdn":Ljava/lang/String;
    const-string v3, "ims"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 529
    const/16 v3, 0xb

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 530
    :cond_1
    const-string v3, "internet"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 531
    const/4 v3, 0x5

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 532
    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 533
    :cond_2
    const-string v3, "wifi"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 534
    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 535
    :cond_3
    const-string v3, "emergency"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_0

    .line 542
    .end local v2    # "pdn":Ljava/lang/String;
    :cond_4
    return-object v1
.end method

.method public getPriority()I
    .locals 1

    .prologue
    .line 710
    iget v0, p0, Lcom/sec/ims/settings/ImsProfile;->mPriority:I

    return v0
.end method

.method public getRPort()I
    .locals 1

    .prologue
    .line 1094
    iget v0, p0, Lcom/sec/ims/settings/ImsProfile;->mRPort:I

    return v0
.end method

.method public getRegistrationAlgorithm()Ljava/lang/String;
    .locals 1

    .prologue
    .line 841
    iget-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mRegistrationAlgorithm:Ljava/lang/String;

    return-object v0
.end method

.method public getSaClientPort()I
    .locals 1

    .prologue
    .line 861
    iget v0, p0, Lcom/sec/ims/settings/ImsProfile;->mSaClientPort:I

    return v0
.end method

.method public getSaServerPort()I
    .locals 1

    .prologue
    .line 871
    iget v0, p0, Lcom/sec/ims/settings/ImsProfile;->mSaServerPort:I

    return v0
.end method

.method public getServiceSet(Ljava/lang/Integer;)Ljava/util/Set;
    .locals 1
    .param p1, "network"    # Ljava/lang/Integer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Integer;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 635
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/sec/ims/settings/ImsProfile;->getServiceSet(Ljava/lang/Integer;Z)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public getServiceSet(Ljava/lang/Integer;Z)Ljava/util/Set;
    .locals 5
    .param p1, "network"    # Ljava/lang/Integer;
    .param p2, "forceToGetAllService"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Integer;",
            "Z)",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 640
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 641
    .local v1, "retVal":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-virtual {p0, p1}, Lcom/sec/ims/settings/ImsProfile;->isNetworkEnabled(Ljava/lang/Integer;)Z

    move-result v0

    .line 643
    .local v0, "isNetworkEnabled":Z
    const-string v2, "ImsProfile"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getServiceSet : network ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "isNetworkEnabled"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "forceToGetAllService"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 647
    if-nez p2, :cond_0

    if-eqz v0, :cond_1

    .line 648
    :cond_0
    iget-object v2, p0, Lcom/sec/ims/settings/ImsProfile;->mServiceSetMap:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 649
    iget-object v2, p0, Lcom/sec/ims/settings/ImsProfile;->mServiceSetMap:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Collection;

    invoke-interface {v1, v2}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 653
    :cond_1
    return-object v1
.end method

.method public getSipPort()I
    .locals 1

    .prologue
    .line 770
    iget v0, p0, Lcom/sec/ims/settings/ImsProfile;->mSipPort:I

    return v0
.end method

.method public getTDelay()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 1121
    const/4 v0, 0x0

    return-object v0
.end method

.method public getTimer1()I
    .locals 1

    .prologue
    .line 911
    iget v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimer1:I

    return v0
.end method

.method public getTimer2()I
    .locals 1

    .prologue
    .line 921
    iget v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimer2:I

    return v0
.end method

.method public getTimer4()I
    .locals 1

    .prologue
    .line 931
    iget v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimer4:I

    return v0
.end method

.method public getTimerA()I
    .locals 1

    .prologue
    .line 941
    iget v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerA:I

    return v0
.end method

.method public getTimerB()I
    .locals 1

    .prologue
    .line 951
    iget v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerB:I

    return v0
.end method

.method public getTimerC()I
    .locals 1

    .prologue
    .line 961
    iget v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerC:I

    return v0
.end method

.method public getTimerD()I
    .locals 1

    .prologue
    .line 971
    iget v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerD:I

    return v0
.end method

.method public getTimerE()I
    .locals 1

    .prologue
    .line 981
    iget v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerE:I

    return v0
.end method

.method public getTimerF()I
    .locals 1

    .prologue
    .line 991
    iget v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerF:I

    return v0
.end method

.method public getTimerG()I
    .locals 1

    .prologue
    .line 1001
    iget v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerG:I

    return v0
.end method

.method public getTimerH()I
    .locals 1

    .prologue
    .line 1011
    iget v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerH:I

    return v0
.end method

.method public getTimerI()I
    .locals 1

    .prologue
    .line 1021
    iget v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerI:I

    return v0
.end method

.method public getTimerJ()I
    .locals 1

    .prologue
    .line 1031
    iget v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerJ:I

    return v0
.end method

.method public getTimerK()I
    .locals 1

    .prologue
    .line 1041
    iget v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerK:I

    return v0
.end method

.method public getTimer_VZW()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 1115
    const/4 v0, 0x0

    return-object v0
.end method

.method public getTransport()I
    .locals 1

    .prologue
    .line 780
    iget v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTransport:I

    return v0
.end method

.method public getTransportName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 785
    iget v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTransport:I

    packed-switch v0, :pswitch_data_0

    .line 793
    const-string v0, ""

    :goto_0
    return-object v0

    .line 787
    :pswitch_0
    const-string v0, "udp"

    goto :goto_0

    .line 789
    :pswitch_1
    const-string v0, "tcp"

    goto :goto_0

    .line 791
    :pswitch_2
    const-string v0, "tls"

    goto :goto_0

    .line 785
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public hasRoamingSupport()Z
    .locals 1

    .prologue
    .line 760
    iget-boolean v0, p0, Lcom/sec/ims/settings/ImsProfile;->mHasRoamingSupport:Z

    return v0
.end method

.method public hasService(Ljava/lang/String;)Z
    .locals 3
    .param p1, "service"    # Ljava/lang/String;

    .prologue
    .line 662
    iget-object v2, p0, Lcom/sec/ims/settings/ImsProfile;->mServiceSetMap:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Set;

    .line 663
    .local v1, "s":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v1, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 664
    const/4 v2, 0x1

    .line 667
    .end local v1    # "s":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public hasService(Ljava/lang/String;Ljava/lang/Integer;)Z
    .locals 2
    .param p1, "service"    # Ljava/lang/String;
    .param p2, "network"    # Ljava/lang/Integer;

    .prologue
    .line 672
    iget-object v1, p0, Lcom/sec/ims/settings/ImsProfile;->mServiceSetMap:Ljava/util/Map;

    invoke-interface {v1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 674
    .local v0, "s":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 675
    const/4 v1, 0x1

    .line 678
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isEmergency()Z
    .locals 2

    .prologue
    .line 520
    invoke-virtual {p0}, Lcom/sec/ims/settings/ImsProfile;->getPdn()Ljava/util/List;

    move-result-object v0

    const-string v1, "emergency"

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isEmergencySupport()Z
    .locals 1

    .prologue
    .line 1084
    iget-boolean v0, p0, Lcom/sec/ims/settings/ImsProfile;->mIsEmergencySupport:Z

    return v0
.end method

.method public isEpdgSupported()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1198
    iget-object v2, p0, Lcom/sec/ims/settings/ImsProfile;->mPdn:Ljava/util/List;

    const-string v3, "ims"

    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1199
    iget-object v2, p0, Lcom/sec/ims/settings/ImsProfile;->mServiceSetMap:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 1200
    .local v0, "networkSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v2

    if-gtz v2, :cond_1

    .line 1207
    .end local v0    # "networkSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    :cond_0
    :goto_0
    return v1

    .line 1203
    .restart local v0    # "networkSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    :cond_1
    const/16 v2, 0x12

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1204
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isIpSecEnabled()Z
    .locals 1

    .prologue
    .line 851
    iget-boolean v0, p0, Lcom/sec/ims/settings/ImsProfile;->mIsIpSecEnabled:Z

    return v0
.end method

.method public isNetworkEnabled(Ljava/lang/Integer;)Z
    .locals 2
    .param p1, "network"    # Ljava/lang/Integer;

    .prologue
    .line 683
    iget-object v1, p0, Lcom/sec/ims/settings/ImsProfile;->mNetworkEnabledMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 684
    .local v0, "isManual":Ljava/lang/Boolean;
    if-nez v0, :cond_0

    .line 685
    const/4 v1, 0x0

    .line 687
    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    goto :goto_0
.end method

.method public isRegEnabled()Z
    .locals 1

    .prologue
    .line 1104
    iget-boolean v0, p0, Lcom/sec/ims/settings/ImsProfile;->mIsRegEnabled:Z

    return v0
.end method

.method public serializeNetworkDataValue(Ljava/util/Map;)Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 379
    .local p1, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/lang/Boolean;>;"
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 380
    .local v3, "networkDataValueStringBuilder":Ljava/lang/StringBuilder;
    const/4 v1, 0x1

    .line 382
    .local v1, "first":Z
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 383
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Boolean;>;"
    if-nez v1, :cond_0

    .line 384
    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 388
    :goto_1
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-static {v4}, Lcom/sec/ims/settings/ImsProfile;->getNetworkName(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 389
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 390
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 386
    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    .line 393
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/lang/Boolean;>;"
    :cond_1
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method public serializeNetworkEnabled()Ljava/lang/String;
    .locals 1

    .prologue
    .line 374
    iget-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mNetworkEnabledMap:Ljava/util/Map;

    invoke-virtual {p0, v0}, Lcom/sec/ims/settings/ImsProfile;->serializeNetworkDataValue(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public serializeServiceSet()Ljava/lang/String;
    .locals 6

    .prologue
    .line 335
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 336
    .local v3, "serviceStringBuilder":Ljava/lang/StringBuilder;
    const/4 v1, 0x1

    .line 338
    .local v1, "first":Z
    iget-object v4, p0, Lcom/sec/ims/settings/ImsProfile;->mServiceSetMap:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 339
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/util/Set<Ljava/lang/String;>;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 342
    if-nez v1, :cond_1

    .line 343
    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 347
    :goto_1
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-static {v4}, Lcom/sec/ims/settings/ImsProfile;->getNetworkName(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 348
    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 349
    const-string v5, "/"

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Iterable;

    invoke-static {v5, v4}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 345
    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 352
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/util/Set<Ljava/lang/String;>;>;"
    :cond_2
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method public setActiveNetwork(Ljava/lang/Integer;)V
    .locals 0
    .param p1, "network"    # Ljava/lang/Integer;

    .prologue
    .line 609
    iput-object p1, p0, Lcom/sec/ims/settings/ImsProfile;->mActiveNetwork:Ljava/lang/Integer;

    .line 610
    return-void
.end method

.method public setAuthAlgorithm(Ljava/lang/String;)V
    .locals 0
    .param p1, "auth"    # Ljava/lang/String;

    .prologue
    .line 896
    iput-object p1, p0, Lcom/sec/ims/settings/ImsProfile;->mAuthAlgorithm:Ljava/lang/String;

    .line 897
    return-void
.end method

.method public setAuthName(Ljava/lang/String;)V
    .locals 0
    .param p1, "mAuthName"    # Ljava/lang/String;

    .prologue
    .line 725
    iput-object p1, p0, Lcom/sec/ims/settings/ImsProfile;->mAuthName:Ljava/lang/String;

    .line 726
    return-void
.end method

.method public setDefaultMcc(Ljava/lang/String;)V
    .locals 0
    .param p1, "mcc"    # Ljava/lang/String;

    .prologue
    .line 464
    iput-object p1, p0, Lcom/sec/ims/settings/ImsProfile;->mDefaultMcc:Ljava/lang/String;

    .line 465
    return-void
.end method

.method public setDefaultMnc(Ljava/lang/String;)V
    .locals 0
    .param p1, "mnc"    # Ljava/lang/String;

    .prologue
    .line 474
    iput-object p1, p0, Lcom/sec/ims/settings/ImsProfile;->mDefaultMnc:Ljava/lang/String;

    .line 475
    return-void
.end method

.method public setDomain(Ljava/lang/String;)V
    .locals 0
    .param p1, "mDomain"    # Ljava/lang/String;

    .prologue
    .line 510
    iput-object p1, p0, Lcom/sec/ims/settings/ImsProfile;->mDomain:Ljava/lang/String;

    .line 511
    return-void
.end method

.method public setEmergencySupport(Z)V
    .locals 0
    .param p1, "isEmergencySupport"    # Z

    .prologue
    .line 1089
    iput-boolean p1, p0, Lcom/sec/ims/settings/ImsProfile;->mIsEmergencySupport:Z

    .line 1090
    return-void
.end method

.method public setEncAlgorithm(Ljava/lang/String;)V
    .locals 0
    .param p1, "enc"    # Ljava/lang/String;

    .prologue
    .line 886
    iput-object p1, p0, Lcom/sec/ims/settings/ImsProfile;->mEncAlgorithm:Ljava/lang/String;

    .line 887
    return-void
.end method

.method public setGlobalSettingsName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 901
    iput-object p1, p0, Lcom/sec/ims/settings/ImsProfile;->mGlobalSettingsName:Ljava/lang/String;

    .line 902
    return-void
.end method

.method public setHasRoamingSupport(Z)V
    .locals 0
    .param p1, "hasRoamingSupport"    # Z

    .prologue
    .line 765
    iput-boolean p1, p0, Lcom/sec/ims/settings/ImsProfile;->mHasRoamingSupport:Z

    .line 766
    return-void
.end method

.method public setImpi(Ljava/lang/String;)V
    .locals 0
    .param p1, "mImpi"    # Ljava/lang/String;

    .prologue
    .line 484
    iput-object p1, p0, Lcom/sec/ims/settings/ImsProfile;->mImpi:Ljava/lang/String;

    .line 485
    return-void
.end method

.method public setImpuList(Ljava/lang/String;)V
    .locals 3
    .param p1, "impu"    # Ljava/lang/String;

    .prologue
    .line 494
    const-string v0, "ImsProfile"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setImpuList: impu "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 496
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 497
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mImpuList:Ljava/util/List;

    .line 501
    :goto_0
    return-void

    .line 499
    :cond_0
    const-string v0, "\\s*,\\s*"

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mImpuList:Ljava/util/List;

    goto :goto_0
.end method

.method public setIpSpecEnabled(Z)V
    .locals 0
    .param p1, "mIsIpSecEnabled"    # Z

    .prologue
    .line 856
    iput-boolean p1, p0, Lcom/sec/ims/settings/ImsProfile;->mIsIpSecEnabled:Z

    .line 857
    return-void
.end method

.method public setIpVer(I)V
    .locals 0
    .param p1, "mIpVer"    # I

    .prologue
    .line 836
    iput p1, p0, Lcom/sec/ims/settings/ImsProfile;->mIpVer:I

    .line 837
    return-void
.end method

.method public setMcc(Ljava/lang/String;)V
    .locals 0
    .param p1, "mMcc"    # Ljava/lang/String;

    .prologue
    .line 444
    iput-object p1, p0, Lcom/sec/ims/settings/ImsProfile;->mMcc:Ljava/lang/String;

    .line 445
    return-void
.end method

.method public setMnc(Ljava/lang/String;)V
    .locals 0
    .param p1, "mMnc"    # Ljava/lang/String;

    .prologue
    .line 454
    iput-object p1, p0, Lcom/sec/ims/settings/ImsProfile;->mMnc:Ljava/lang/String;

    .line 455
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "mName"    # Ljava/lang/String;

    .prologue
    .line 434
    iput-object p1, p0, Lcom/sec/ims/settings/ImsProfile;->mName:Ljava/lang/String;

    .line 435
    return-void
.end method

.method public setNetworkEnabled(Ljava/lang/Integer;Z)V
    .locals 3
    .param p1, "network"    # Ljava/lang/Integer;
    .param p2, "enabled"    # Z

    .prologue
    .line 692
    const-string v0, "ImsProfile"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setNetworkEnabled : network ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "enabled"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 695
    iget-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mNetworkEnabledMap:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 696
    return-void
.end method

.method public setNetworkEnabled(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "network"    # Ljava/lang/String;
    .param p2, "enabled"    # Z

    .prologue
    .line 700
    invoke-static {p1}, Lcom/sec/ims/settings/ImsProfile;->getNetworkType(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/sec/ims/settings/ImsProfile;->setNetworkEnabled(Ljava/lang/Integer;Z)V

    .line 701
    return-void
.end method

.method public setNetworkList(Ljava/lang/String;)V
    .locals 6
    .param p1, "networkList"    # Ljava/lang/String;

    .prologue
    .line 568
    const-string v5, "\\s*,\\s*"

    invoke-virtual {p1, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    .line 570
    .local v3, "newNetStrList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v5, p0, Lcom/sec/ims/settings/ImsProfile;->mServiceSetMap:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 571
    .local v4, "svcSetItr":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 572
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 573
    .local v2, "netType":Ljava/lang/Integer;
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-static {v5}, Lcom/sec/ims/settings/ImsProfile;->getNetworkName(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 574
    invoke-interface {v4}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 578
    .end local v2    # "netType":Ljava/lang/Integer;
    :cond_1
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 579
    .local v1, "netStr":Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/ims/settings/ImsProfile;->getNetworkType(Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 581
    .restart local v2    # "netType":Ljava/lang/Integer;
    invoke-virtual {p0, v2}, Lcom/sec/ims/settings/ImsProfile;->getServiceSet(Ljava/lang/Integer;)Ljava/util/Set;

    move-result-object v5

    if-eqz v5, :cond_3

    invoke-virtual {p0, v2}, Lcom/sec/ims/settings/ImsProfile;->getServiceSet(Ljava/lang/Integer;)Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 582
    :cond_3
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    invoke-virtual {p0, v2, v5}, Lcom/sec/ims/settings/ImsProfile;->setServiceSet(Ljava/lang/Integer;Ljava/util/Set;)V

    goto :goto_1

    .line 585
    .end local v1    # "netStr":Ljava/lang/String;
    .end local v2    # "netType":Ljava/lang/Integer;
    :cond_4
    return-void
.end method

.method public setPassword(Ljava/lang/String;)V
    .locals 0
    .param p1, "mPassword"    # Ljava/lang/String;

    .prologue
    .line 735
    iput-object p1, p0, Lcom/sec/ims/settings/ImsProfile;->mPassword:Ljava/lang/String;

    .line 736
    return-void
.end method

.method public setPcscfList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 745
    .local p1, "mPcscfList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/sec/ims/settings/ImsProfile;->mPcscfList:Ljava/util/List;

    .line 746
    return-void
.end method

.method public setPcscfPreference(I)V
    .locals 0
    .param p1, "mPcscfPreference"    # I

    .prologue
    .line 755
    iput p1, p0, Lcom/sec/ims/settings/ImsProfile;->mPcscfPreference:I

    .line 756
    return-void
.end method

.method public setPdn(Ljava/lang/String;)V
    .locals 1
    .param p1, "pdn"    # Ljava/lang/String;

    .prologue
    .line 547
    const-string v0, "\\s*,\\s*"

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mPdn:Ljava/util/List;

    .line 548
    return-void
.end method

.method public setPriority(I)V
    .locals 0
    .param p1, "mPriority"    # I

    .prologue
    .line 715
    iput p1, p0, Lcom/sec/ims/settings/ImsProfile;->mPriority:I

    .line 716
    return-void
.end method

.method public setRPort(I)V
    .locals 0
    .param p1, "RPort"    # I

    .prologue
    .line 1099
    iput p1, p0, Lcom/sec/ims/settings/ImsProfile;->mRPort:I

    .line 1100
    return-void
.end method

.method public setRegEnabled(Z)V
    .locals 0
    .param p1, "isRegEnabled"    # Z

    .prologue
    .line 1109
    iput-boolean p1, p0, Lcom/sec/ims/settings/ImsProfile;->mIsRegEnabled:Z

    .line 1110
    return-void
.end method

.method public setRegistrationAlgorithm(Ljava/lang/String;)V
    .locals 0
    .param p1, "mRegistrationAlgorithm"    # Ljava/lang/String;

    .prologue
    .line 846
    iput-object p1, p0, Lcom/sec/ims/settings/ImsProfile;->mRegistrationAlgorithm:Ljava/lang/String;

    .line 847
    return-void
.end method

.method public setSaClientPort(I)V
    .locals 0
    .param p1, "mSaClientPort"    # I

    .prologue
    .line 866
    iput p1, p0, Lcom/sec/ims/settings/ImsProfile;->mSaClientPort:I

    .line 867
    return-void
.end method

.method public setSaServerPort(I)V
    .locals 0
    .param p1, "mSaServerPort"    # I

    .prologue
    .line 876
    iput p1, p0, Lcom/sec/ims/settings/ImsProfile;->mSaServerPort:I

    .line 877
    return-void
.end method

.method public setServiceSet(Ljava/lang/Integer;Ljava/util/Set;)V
    .locals 1
    .param p1, "network"    # Ljava/lang/Integer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Integer;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 658
    .local p2, "serviceSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mServiceSetMap:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 659
    return-void
.end method

.method public setSipPort(I)V
    .locals 0
    .param p1, "mSipPort"    # I

    .prologue
    .line 775
    iput p1, p0, Lcom/sec/ims/settings/ImsProfile;->mSipPort:I

    .line 776
    return-void
.end method

.method public setTimer(Ljava/lang/String;I)V
    .locals 1
    .param p1, "timerName"    # Ljava/lang/String;
    .param p2, "value"    # I

    .prologue
    .line 1051
    const-string v0, "1"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1052
    iput p2, p0, Lcom/sec/ims/settings/ImsProfile;->mTimer1:I

    .line 1080
    :cond_0
    :goto_0
    return-void

    .line 1053
    :cond_1
    const-string v0, "2"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1054
    iput p2, p0, Lcom/sec/ims/settings/ImsProfile;->mTimer2:I

    goto :goto_0

    .line 1055
    :cond_2
    const-string v0, "4"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1056
    iput p2, p0, Lcom/sec/ims/settings/ImsProfile;->mTimer4:I

    goto :goto_0

    .line 1057
    :cond_3
    const-string v0, "A"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1058
    iput p2, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerA:I

    goto :goto_0

    .line 1059
    :cond_4
    const-string v0, "B"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1060
    iput p2, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerB:I

    goto :goto_0

    .line 1061
    :cond_5
    const-string v0, "C"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1062
    iput p2, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerC:I

    goto :goto_0

    .line 1063
    :cond_6
    const-string v0, "D"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1064
    iput p2, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerD:I

    goto :goto_0

    .line 1065
    :cond_7
    const-string v0, "E"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1066
    iput p2, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerE:I

    goto :goto_0

    .line 1067
    :cond_8
    const-string v0, "F"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1068
    iput p2, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerF:I

    goto :goto_0

    .line 1069
    :cond_9
    const-string v0, "G"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1070
    iput p2, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerG:I

    goto :goto_0

    .line 1071
    :cond_a
    const-string v0, "H"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 1072
    iput p2, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerH:I

    goto :goto_0

    .line 1073
    :cond_b
    const-string v0, "I"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 1074
    iput p2, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerI:I

    goto :goto_0

    .line 1075
    :cond_c
    const-string v0, "J"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 1076
    iput p2, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerJ:I

    goto/16 :goto_0

    .line 1077
    :cond_d
    const-string v0, "K"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1078
    iput p2, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerK:I

    goto/16 :goto_0
.end method

.method public setTimer1(I)V
    .locals 0
    .param p1, "mTimer1"    # I

    .prologue
    .line 916
    iput p1, p0, Lcom/sec/ims/settings/ImsProfile;->mTimer1:I

    .line 917
    return-void
.end method

.method public setTimer2(I)V
    .locals 0
    .param p1, "mTimer2"    # I

    .prologue
    .line 926
    iput p1, p0, Lcom/sec/ims/settings/ImsProfile;->mTimer2:I

    .line 927
    return-void
.end method

.method public setTimer4(I)V
    .locals 0
    .param p1, "mTimer4"    # I

    .prologue
    .line 936
    iput p1, p0, Lcom/sec/ims/settings/ImsProfile;->mTimer4:I

    .line 937
    return-void
.end method

.method public setTimerA(I)V
    .locals 0
    .param p1, "mTimerA"    # I

    .prologue
    .line 946
    iput p1, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerA:I

    .line 947
    return-void
.end method

.method public setTimerB(I)V
    .locals 0
    .param p1, "mTimerB"    # I

    .prologue
    .line 956
    iput p1, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerB:I

    .line 957
    return-void
.end method

.method public setTimerC(I)V
    .locals 0
    .param p1, "mTimerC"    # I

    .prologue
    .line 966
    iput p1, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerC:I

    .line 967
    return-void
.end method

.method public setTimerD(I)V
    .locals 0
    .param p1, "mTimerD"    # I

    .prologue
    .line 976
    iput p1, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerD:I

    .line 977
    return-void
.end method

.method public setTimerE(I)V
    .locals 0
    .param p1, "mTimerE"    # I

    .prologue
    .line 986
    iput p1, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerE:I

    .line 987
    return-void
.end method

.method public setTimerF(I)V
    .locals 0
    .param p1, "mTimerF"    # I

    .prologue
    .line 996
    iput p1, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerF:I

    .line 997
    return-void
.end method

.method public setTimerG(I)V
    .locals 0
    .param p1, "mTimerG"    # I

    .prologue
    .line 1006
    iput p1, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerG:I

    .line 1007
    return-void
.end method

.method public setTimerH(I)V
    .locals 0
    .param p1, "mTimerH"    # I

    .prologue
    .line 1016
    iput p1, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerH:I

    .line 1017
    return-void
.end method

.method public setTimerI(I)V
    .locals 0
    .param p1, "mTimerI"    # I

    .prologue
    .line 1026
    iput p1, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerI:I

    .line 1027
    return-void
.end method

.method public setTimerJ(I)V
    .locals 0
    .param p1, "mTimerJ"    # I

    .prologue
    .line 1036
    iput p1, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerJ:I

    .line 1037
    return-void
.end method

.method public setTimerK(I)V
    .locals 0
    .param p1, "mTimerK"    # I

    .prologue
    .line 1046
    iput p1, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerK:I

    .line 1047
    return-void
.end method

.method public setTransport(I)V
    .locals 0
    .param p1, "mTransport"    # I

    .prologue
    .line 798
    iput p1, p0, Lcom/sec/ims/settings/ImsProfile;->mTransport:I

    .line 799
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1339
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ImsProfile [mId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/ims/settings/ImsProfile;->mId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mEnableStatus="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/ims/settings/ImsProfile;->mEnableStatus:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/ims/settings/ImsProfile;->mName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mMcc="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/ims/settings/ImsProfile;->mMcc:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mMnc="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/ims/settings/ImsProfile;->mMnc:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mImpi="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/ims/settings/ImsProfile;->mImpi:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mImpuList="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/ims/settings/ImsProfile;->mImpuList:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mPdn="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/ims/settings/ImsProfile;->mPdn:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mActiveNetwork="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/ims/settings/ImsProfile;->mActiveNetwork:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mServiceSetMap="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/ims/settings/ImsProfile;->mServiceSetMap:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mNetworkEnabledMap="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/ims/settings/ImsProfile;->mNetworkEnabledMap:Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mPriority="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/ims/settings/ImsProfile;->mPriority:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mAuthName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/ims/settings/ImsProfile;->mAuthName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mPassword="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/ims/settings/ImsProfile;->mPassword:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mPcscfList="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/ims/settings/ImsProfile;->mPcscfList:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mPcscfPreference="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/ims/settings/ImsProfile;->mPcscfPreference:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mHasRoamingSupport="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/sec/ims/settings/ImsProfile;->mHasRoamingSupport:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mSipPort="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/ims/settings/ImsProfile;->mSipPort:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mTransport="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/ims/settings/ImsProfile;->mTransport:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mIpVer="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/ims/settings/ImsProfile;->mIpVer:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mRegistrationAlgorithm="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/ims/settings/ImsProfile;->mRegistrationAlgorithm:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mSaClientPort="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/ims/settings/ImsProfile;->mSaClientPort:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mSaServerPort="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/ims/settings/ImsProfile;->mSaServerPort:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mTimer1="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/ims/settings/ImsProfile;->mTimer1:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mTimer2="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/ims/settings/ImsProfile;->mTimer2:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mTimer4="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/ims/settings/ImsProfile;->mTimer4:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mTimerF="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerF:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "defaultMccMnc ["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/ims/settings/ImsProfile;->mDefaultMcc:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/ims/settings/ImsProfile;->mDefaultMnc:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1219
    iget v0, p0, Lcom/sec/ims/settings/ImsProfile;->mId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1220
    iget-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mName:Ljava/lang/String;

    invoke-direct {p0, p1, v0}, Lcom/sec/ims/settings/ImsProfile;->writeStringToParcel(Landroid/os/Parcel;Ljava/lang/String;)V

    .line 1221
    iget-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mMcc:Ljava/lang/String;

    invoke-direct {p0, p1, v0}, Lcom/sec/ims/settings/ImsProfile;->writeStringToParcel(Landroid/os/Parcel;Ljava/lang/String;)V

    .line 1222
    iget-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mMnc:Ljava/lang/String;

    invoke-direct {p0, p1, v0}, Lcom/sec/ims/settings/ImsProfile;->writeStringToParcel(Landroid/os/Parcel;Ljava/lang/String;)V

    .line 1223
    iget-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mImpi:Ljava/lang/String;

    invoke-direct {p0, p1, v0}, Lcom/sec/ims/settings/ImsProfile;->writeStringToParcel(Landroid/os/Parcel;Ljava/lang/String;)V

    .line 1224
    iget-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mImpuList:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 1225
    iget-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mDomain:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1226
    iget-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mPdn:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 1227
    iget-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mActiveNetwork:Ljava/lang/Integer;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 1228
    iget-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mServiceSetMap:Ljava/util/Map;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V

    .line 1229
    iget-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mNetworkEnabledMap:Ljava/util/Map;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V

    .line 1230
    iget v0, p0, Lcom/sec/ims/settings/ImsProfile;->mPriority:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1231
    iget-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mAuthName:Ljava/lang/String;

    invoke-direct {p0, p1, v0}, Lcom/sec/ims/settings/ImsProfile;->writeStringToParcel(Landroid/os/Parcel;Ljava/lang/String;)V

    .line 1232
    iget-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mPassword:Ljava/lang/String;

    invoke-direct {p0, p1, v0}, Lcom/sec/ims/settings/ImsProfile;->writeStringToParcel(Landroid/os/Parcel;Ljava/lang/String;)V

    .line 1233
    iget-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mPcscfList:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 1234
    iget v0, p0, Lcom/sec/ims/settings/ImsProfile;->mPcscfPreference:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1235
    iget-boolean v0, p0, Lcom/sec/ims/settings/ImsProfile;->mHasRoamingSupport:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1236
    iget v0, p0, Lcom/sec/ims/settings/ImsProfile;->mSipPort:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1237
    iget v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTransport:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1238
    iget v0, p0, Lcom/sec/ims/settings/ImsProfile;->mIpVer:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1239
    iget-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mRegistrationAlgorithm:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1240
    iget-boolean v0, p0, Lcom/sec/ims/settings/ImsProfile;->mIsIpSecEnabled:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1241
    iget v0, p0, Lcom/sec/ims/settings/ImsProfile;->mSaClientPort:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1242
    iget v0, p0, Lcom/sec/ims/settings/ImsProfile;->mSaServerPort:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1243
    iget-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mEncAlgorithm:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1244
    iget-object v0, p0, Lcom/sec/ims/settings/ImsProfile;->mAuthAlgorithm:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1245
    iget v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimer1:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1246
    iget v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimer2:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1247
    iget v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimer4:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1248
    iget v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerA:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1249
    iget v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerB:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1250
    iget v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerC:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1251
    iget v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerD:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1252
    iget v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerE:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1253
    iget v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerF:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1254
    iget v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerG:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1255
    iget v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerH:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1256
    iget v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerI:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1257
    iget v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerJ:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1258
    iget v0, p0, Lcom/sec/ims/settings/ImsProfile;->mTimerK:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1259
    iget-boolean v0, p0, Lcom/sec/ims/settings/ImsProfile;->mIsEmergencySupport:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1260
    iget v0, p0, Lcom/sec/ims/settings/ImsProfile;->mRPort:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1261
    iget-boolean v0, p0, Lcom/sec/ims/settings/ImsProfile;->mIsRegEnabled:Z

    if-eqz v0, :cond_3

    :goto_3
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1262
    return-void

    :cond_0
    move v0, v2

    .line 1235
    goto/16 :goto_0

    :cond_1
    move v0, v2

    .line 1240
    goto :goto_1

    :cond_2
    move v0, v2

    .line 1259
    goto :goto_2

    :cond_3
    move v1, v2

    .line 1261
    goto :goto_3
.end method
