.class public Lcom/sec/ims/settings/ImsSettings;
.super Ljava/lang/Object;
.source "ImsSettings.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/ims/settings/ImsSettings$CscSettingTable;,
        Lcom/sec/ims/settings/ImsSettings$CscProfileTable;,
        Lcom/sec/ims/settings/ImsSettings$GlobalTable;,
        Lcom/sec/ims/settings/ImsSettings$ProfileTable;,
        Lcom/sec/ims/settings/ImsSettings$ProfileIdTable;,
        Lcom/sec/ims/settings/ImsSettings$ImsServiceSwitchTable;
    }
.end annotation


# static fields
.field public static final AUTHORITY:Ljava/lang/String; = "com.sec.ims.settings"

.field public static final DATABASE_NAME:Ljava/lang/String; = "ims_config_data.db"

.field public static final DATABASE_VERSION:I = 0x22

.field public static final TYPE_INT:Ljava/lang/String; = "INT"

.field public static final TYPE_TEXT:Ljava/lang/String; = "TEXT"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 608
    return-void
.end method
