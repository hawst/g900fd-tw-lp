.class public Lcom/sec/ims/ImsManager;
.super Ljava/lang/Object;
.source "ImsManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/ims/ImsManager$ConnectionListener;
    }
.end annotation


# static fields
.field static final LOG_TAG:Ljava/lang/String; = "ImsManager"


# instance fields
.field private final mConnection:Landroid/content/ServiceConnection;

.field private mContext:Landroid/content/Context;

.field private mImsService:Lcom/sec/ims/IImsService;

.field private mListener:Lcom/sec/ims/ImsManager$ConnectionListener;

.field private final mOwner:Ljava/lang/String;

.field private final mPreConnectionRegListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/ims/IImsRegistrationListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/ims/ImsManager$ConnectionListener;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/sec/ims/ImsManager$ConnectionListener;

    .prologue
    const/4 v0, 0x0

    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object v0, p0, Lcom/sec/ims/ImsManager;->mContext:Landroid/content/Context;

    .line 38
    iput-object v0, p0, Lcom/sec/ims/ImsManager;->mImsService:Lcom/sec/ims/IImsService;

    .line 39
    iput-object v0, p0, Lcom/sec/ims/ImsManager;->mListener:Lcom/sec/ims/ImsManager$ConnectionListener;

    .line 40
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/ims/ImsManager;->mOwner:Ljava/lang/String;

    .line 42
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/ims/ImsManager;->mPreConnectionRegListeners:Ljava/util/List;

    .line 44
    new-instance v0, Lcom/sec/ims/ImsManager$1;

    invoke-direct {v0, p0}, Lcom/sec/ims/ImsManager$1;-><init>(Lcom/sec/ims/ImsManager;)V

    iput-object v0, p0, Lcom/sec/ims/ImsManager;->mConnection:Landroid/content/ServiceConnection;

    .line 85
    iput-object p1, p0, Lcom/sec/ims/ImsManager;->mContext:Landroid/content/Context;

    .line 86
    iput-object p2, p0, Lcom/sec/ims/ImsManager;->mListener:Lcom/sec/ims/ImsManager$ConnectionListener;

    .line 87
    return-void
.end method

.method static synthetic access$000(Lcom/sec/ims/ImsManager;)Lcom/sec/ims/IImsService;
    .locals 1
    .param p0, "x0"    # Lcom/sec/ims/ImsManager;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/ims/ImsManager;->mImsService:Lcom/sec/ims/IImsService;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/ims/ImsManager;Lcom/sec/ims/IImsService;)Lcom/sec/ims/IImsService;
    .locals 0
    .param p0, "x0"    # Lcom/sec/ims/ImsManager;
    .param p1, "x1"    # Lcom/sec/ims/IImsService;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/sec/ims/ImsManager;->mImsService:Lcom/sec/ims/IImsService;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/ims/ImsManager;)Lcom/sec/ims/ImsManager$ConnectionListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/ims/ImsManager;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/ims/ImsManager;->mListener:Lcom/sec/ims/ImsManager$ConnectionListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/ims/ImsManager;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/sec/ims/ImsManager;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/ims/ImsManager;->mPreConnectionRegListeners:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public connectService()V
    .locals 5

    .prologue
    .line 91
    iget-object v1, p0, Lcom/sec/ims/ImsManager;->mImsService:Lcom/sec/ims/IImsService;

    if-nez v1, :cond_0

    .line 92
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 93
    .local v0, "serviceIntent":Landroid/content/Intent;
    const-string v1, "com.sec.imsservice"

    const-string v2, "com.sec.internal.ims.imsservice.ImsService"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 95
    iget-object v1, p0, Lcom/sec/ims/ImsManager;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/ims/ImsManager;->mConnection:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    sget-object v4, Landroid/os/UserHandle;->CURRENT_OR_SELF:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2, v3, v4}, Landroid/content/Context;->bindServiceAsUser(Landroid/content/Intent;Landroid/content/ServiceConnection;ILandroid/os/UserHandle;)Z

    .line 100
    .end local v0    # "serviceIntent":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method public deregisterProfile(Ljava/util/List;Z)V
    .locals 2
    .param p2, "disconnectPdn"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 495
    .local p1, "profileIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :try_start_0
    iget-object v1, p0, Lcom/sec/ims/ImsManager;->mImsService:Lcom/sec/ims/IImsService;

    invoke-interface {v1, p1, p2}, Lcom/sec/ims/IImsService;->deregisterProfile(Ljava/util/List;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 499
    :goto_0
    return-void

    .line 496
    :catch_0
    move-exception v0

    .line 497
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public disconnectService()V
    .locals 2

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/ims/ImsManager;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Lcom/sec/ims/ImsManager;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/ims/ImsManager;->mConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 107
    :cond_0
    return-void
.end method

.method public enableRcs(Z)V
    .locals 4
    .param p1, "enable"    # Z

    .prologue
    .line 281
    const-string v1, "ImsManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "enableRcs: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 283
    iget-object v1, p0, Lcom/sec/ims/ImsManager;->mImsService:Lcom/sec/ims/IImsService;

    if-nez v1, :cond_0

    .line 284
    const-string v1, "ImsManager"

    const-string v2, "enableRcs: not connected."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 293
    :goto_0
    return-void

    .line 289
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/ims/ImsManager;->mImsService:Lcom/sec/ims/IImsService;

    invoke-interface {v1, p1}, Lcom/sec/ims/IImsService;->enableRcs(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 290
    :catch_0
    move-exception v0

    .line 291
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public enableService(Ljava/lang/String;Z)V
    .locals 4
    .param p1, "service"    # Ljava/lang/String;
    .param p2, "enable"    # Z

    .prologue
    .line 325
    const-string v1, "ImsManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "enableService: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 327
    iget-object v1, p0, Lcom/sec/ims/ImsManager;->mImsService:Lcom/sec/ims/IImsService;

    if-nez v1, :cond_0

    .line 328
    const-string v1, "ImsManager"

    const-string v2, "enableService: not connected."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 337
    :goto_0
    return-void

    .line 333
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/ims/ImsManager;->mImsService:Lcom/sec/ims/IImsService;

    invoke-interface {v1, p1, p2}, Lcom/sec/ims/IImsService;->enableService(Ljava/lang/String;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 334
    :catch_0
    move-exception v0

    .line 335
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public enableVoLte(Z)V
    .locals 4
    .param p1, "enable"    # Z

    .prologue
    .line 215
    const-string v1, "ImsManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "enableVoLte: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 217
    iget-object v1, p0, Lcom/sec/ims/ImsManager;->mImsService:Lcom/sec/ims/IImsService;

    if-nez v1, :cond_0

    .line 218
    const-string v1, "ImsManager"

    const-string v2, "enableVoLte: not connected."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 226
    :goto_0
    return-void

    .line 222
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/ims/ImsManager;->mImsService:Lcom/sec/ims/IImsService;

    invoke-interface {v1, p1}, Lcom/sec/ims/IImsService;->enableVoLte(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 223
    :catch_0
    move-exception v0

    .line 224
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getCurrentProfile()[Lcom/sec/ims/settings/ImsProfile;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 462
    const-string v1, "ImsManager"

    const-string v2, "getCurrentProfile"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 464
    iget-object v1, p0, Lcom/sec/ims/ImsManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/ims/settings/ImsProfileLoader;->getProfileList(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    .line 465
    .local v0, "profiles":Ljava/util/List;, "Ljava/util/List<Lcom/sec/ims/settings/ImsProfile;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    .line 466
    :cond_0
    const-string v1, "ImsManager"

    const-string v2, "getCurrentProfile() no profile is selected!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 467
    const/4 v1, 0x0

    .line 469
    :goto_0
    return-object v1

    :cond_1
    new-array v1, v3, [Lcom/sec/ims/settings/ImsProfile;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/sec/ims/settings/ImsProfile;

    goto :goto_0
.end method

.method public getFeatureValue(IILcom/sec/ims/ImsConfigListener;)V
    .locals 3
    .param p1, "feature"    # I
    .param p2, "network"    # I
    .param p3, "listener"    # Lcom/sec/ims/ImsConfigListener;

    .prologue
    .line 401
    iget-object v1, p0, Lcom/sec/ims/ImsManager;->mImsService:Lcom/sec/ims/IImsService;

    if-nez v1, :cond_0

    .line 402
    const-string v1, "ImsManager"

    const-string v2, "getFeatureValue: not connected."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 410
    :goto_0
    return-void

    .line 406
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/ims/ImsManager;->mImsService:Lcom/sec/ims/IImsService;

    invoke-interface {v1, p1, p2, p3}, Lcom/sec/ims/IImsService;->getFeatureValue(IILcom/sec/ims/ImsConfigListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 407
    :catch_0
    move-exception v0

    .line 408
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getMasterStringValue(I)Ljava/lang/String;
    .locals 5
    .param p1, "item"    # I

    .prologue
    .line 360
    const-string v1, ""

    .line 361
    .local v1, "value":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/ims/ImsManager;->mImsService:Lcom/sec/ims/IImsService;

    if-nez v3, :cond_0

    .line 362
    const-string v3, "ImsManager"

    const-string v4, "getMasterStringValue: not connected."

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v2, v1

    .line 370
    .end local v1    # "value":Ljava/lang/String;
    .local v2, "value":Ljava/lang/String;
    :goto_0
    return-object v2

    .line 366
    .end local v2    # "value":Ljava/lang/String;
    .restart local v1    # "value":Ljava/lang/String;
    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/sec/ims/ImsManager;->mImsService:Lcom/sec/ims/IImsService;

    invoke-interface {v3, p1}, Lcom/sec/ims/IImsService;->getMasterStringValue(I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_1
    move-object v2, v1

    .line 370
    .end local v1    # "value":Ljava/lang/String;
    .restart local v2    # "value":Ljava/lang/String;
    goto :goto_0

    .line 367
    .end local v2    # "value":Ljava/lang/String;
    .restart local v1    # "value":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 368
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method

.method public getMasterValue(I)I
    .locals 5
    .param p1, "item"    # I

    .prologue
    .line 345
    const/4 v1, -0x1

    .line 346
    .local v1, "value":I
    iget-object v3, p0, Lcom/sec/ims/ImsManager;->mImsService:Lcom/sec/ims/IImsService;

    if-nez v3, :cond_0

    .line 347
    const-string v3, "ImsManager"

    const-string v4, "getMasterValue: not connected."

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v1

    .line 355
    .end local v1    # "value":I
    .local v2, "value":I
    :goto_0
    return v2

    .line 351
    .end local v2    # "value":I
    .restart local v1    # "value":I
    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/sec/ims/ImsManager;->mImsService:Lcom/sec/ims/IImsService;

    invoke-interface {v3, p1}, Lcom/sec/ims/IImsService;->getMasterValue(I)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_1
    move v2, v1

    .line 355
    .end local v1    # "value":I
    .restart local v2    # "value":I
    goto :goto_0

    .line 352
    .end local v2    # "value":I
    .restart local v1    # "value":I
    :catch_0
    move-exception v0

    .line 353
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method

.method public getRegistrationInfo()[Lcom/sec/ims/ImsRegistration;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 151
    const-string v2, "ImsManager"

    const-string v3, "getRegistrationInfo"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 152
    iget-object v2, p0, Lcom/sec/ims/ImsManager;->mImsService:Lcom/sec/ims/IImsService;

    if-nez v2, :cond_0

    .line 153
    const-string v2, "ImsManager"

    const-string v3, "getRegistrationInfo: Not initialized."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 162
    :goto_0
    return-object v1

    .line 158
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/sec/ims/ImsManager;->mImsService:Lcom/sec/ims/IImsService;

    invoke-interface {v2}, Lcom/sec/ims/IImsService;->getRegistrationInfo()[Lcom/sec/ims/ImsRegistration;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 159
    :catch_0
    move-exception v0

    .line 160
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public hasService(Ljava/lang/String;)Z
    .locals 5
    .param p1, "service"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 474
    const-string v2, "ImsManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "hasService ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 475
    iget-object v2, p0, Lcom/sec/ims/ImsManager;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/ims/settings/ImsProfileLoader;->getProfileList(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    .line 476
    .local v0, "profiles":Ljava/util/List;, "Ljava/util/List<Lcom/sec/ims/settings/ImsProfile;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_1

    .line 477
    :cond_0
    const-string v2, "ImsManager"

    const-string v3, "hasService() no profiles!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 480
    :goto_0
    return v1

    :cond_1
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/ims/settings/ImsProfile;

    invoke-virtual {v1, p1}, Lcom/sec/ims/settings/ImsProfile;->hasService(Ljava/lang/String;)Z

    move-result v1

    goto :goto_0
.end method

.method public isImsEnabled()Z
    .locals 4

    .prologue
    .line 171
    const/4 v1, 0x0

    .line 173
    .local v1, "enabled":Z
    iget-object v2, p0, Lcom/sec/ims/ImsManager;->mImsService:Lcom/sec/ims/IImsService;

    if-nez v2, :cond_0

    .line 174
    const-string v2, "ImsManager"

    const-string v3, "isVoLteEnabled: not connected."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    const/4 v2, 0x0

    .line 184
    :goto_0
    return v2

    .line 179
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/sec/ims/ImsManager;->mImsService:Lcom/sec/ims/IImsService;

    invoke-interface {v2}, Lcom/sec/ims/IImsService;->isImsEnabled()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_1
    move v2, v1

    .line 184
    goto :goto_0

    .line 180
    :catch_0
    move-exception v0

    .line 181
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method

.method public isRcsEnabled()Z
    .locals 14

    .prologue
    const/4 v3, 0x0

    const/4 v12, 0x1

    const/4 v13, 0x0

    .line 235
    new-instance v9, Lcom/sec/ims/settings/RcsConfigurationReader;

    iget-object v0, p0, Lcom/sec/ims/ImsManager;->mContext:Landroid/content/Context;

    invoke-direct {v9, v0}, Lcom/sec/ims/settings/RcsConfigurationReader;-><init>(Landroid/content/Context;)V

    .line 236
    .local v9, "rcsConfig":Lcom/sec/ims/settings/RcsConfigurationReader;
    const/4 v11, 0x0

    .line 237
    .local v11, "version":I
    const/4 v10, 0x0

    .line 238
    .local v10, "rcsEnabled":Z
    const/4 v6, 0x0

    .line 240
    .local v6, "autoconfCompleted":Z
    iget-object v0, p0, Lcom/sec/ims/ImsManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://com.sec.ims.settings/imsswitch/rcs"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/sec/ims/settings/ImsSettings$ImsServiceSwitchTable;->PROJECTION:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 244
    .local v7, "c":Landroid/database/Cursor;
    if-eqz v7, :cond_0

    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_3

    .line 245
    :cond_0
    const-string v0, "ImsManager"

    const-string v1, "isRcsEnabled: not found"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 246
    if-eqz v7, :cond_1

    .line 247
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_1
    move v12, v13

    .line 272
    :cond_2
    :goto_0
    return v12

    .line 252
    :cond_3
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 253
    const-string v0, "ImsManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isRcsEnabled: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "name"

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "enabled"

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 256
    const-string v0, "enabled"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v12, :cond_5

    move v10, v12

    .line 258
    :goto_1
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 261
    :try_start_0
    const-string v0, "root/vers/version"

    invoke-virtual {v9, v0}, Lcom/sec/ims/settings/RcsConfigurationReader;->getInt(Ljava/lang/String;)I

    move-result v11

    .line 262
    const-string v0, "true"

    const-string v1, "info/completed"

    invoke-virtual {v9, v1}, Lcom/sec/ims/settings/RcsConfigurationReader;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    .line 265
    const-string v0, "ImsManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isRcsEnabled: version "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " autoConfigComplete "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 272
    :goto_2
    if-eqz v10, :cond_4

    if-eqz v6, :cond_2

    if-gtz v11, :cond_2

    :cond_4
    move v12, v13

    goto/16 :goto_0

    :cond_5
    move v10, v13

    .line 256
    goto :goto_1

    .line 267
    :catch_0
    move-exception v8

    .line 268
    .local v8, "e":Ljava/lang/IllegalStateException;
    const-string v0, "ImsManager"

    const-string v1, "isRcsEnabled: AutoConfiguration is not completed."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 269
    invoke-virtual {v8}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_2
.end method

.method public isServiceEnabled(Ljava/lang/String;)Z
    .locals 4
    .param p1, "service"    # Ljava/lang/String;

    .prologue
    .line 302
    const/4 v1, 0x0

    .line 304
    .local v1, "enabled":Z
    iget-object v2, p0, Lcom/sec/ims/ImsManager;->mImsService:Lcom/sec/ims/IImsService;

    if-nez v2, :cond_0

    .line 305
    const-string v2, "ImsManager"

    const-string v3, "isServiceEnabled: not connected."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 306
    const/4 v2, 0x0

    .line 315
    :goto_0
    return v2

    .line 310
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/sec/ims/ImsManager;->mImsService:Lcom/sec/ims/IImsService;

    invoke-interface {v2, p1}, Lcom/sec/ims/IImsService;->isServiceEnabled(Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_1
    move v2, v1

    .line 315
    goto :goto_0

    .line 311
    :catch_0
    move-exception v0

    .line 312
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method

.method public isVoLteEnabled()Z
    .locals 4

    .prologue
    .line 193
    const/4 v1, 0x0

    .line 195
    .local v1, "enabled":Z
    iget-object v2, p0, Lcom/sec/ims/ImsManager;->mImsService:Lcom/sec/ims/IImsService;

    if-nez v2, :cond_0

    .line 196
    const-string v2, "ImsManager"

    const-string v3, "isVoLteEnabled: not connected."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    const/4 v2, 0x0

    .line 206
    :goto_0
    return v2

    .line 201
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/sec/ims/ImsManager;->mImsService:Lcom/sec/ims/IImsService;

    invoke-interface {v2}, Lcom/sec/ims/IImsService;->isVoLteEnabled()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_1
    move v2, v1

    .line 206
    goto :goto_0

    .line 202
    :catch_0
    move-exception v0

    .line 203
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method

.method public declared-synchronized registerImsRegistrationListener(Lcom/sec/ims/IImsRegistrationListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/sec/ims/IImsRegistrationListener;

    .prologue
    .line 431
    monitor-enter p0

    :try_start_0
    const-string v1, "ImsManager"

    const-string v2, "registerImsRegistrationListener"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 432
    iget-object v1, p0, Lcom/sec/ims/ImsManager;->mImsService:Lcom/sec/ims/IImsService;

    if-nez v1, :cond_0

    .line 433
    const-string v1, "ImsManager"

    const-string v2, "Not initialized."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 434
    iget-object v1, p0, Lcom/sec/ims/ImsManager;->mPreConnectionRegListeners:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 443
    :goto_0
    monitor-exit p0

    return-void

    .line 439
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/sec/ims/ImsManager;->mImsService:Lcom/sec/ims/IImsService;

    invoke-interface {v1, p1}, Lcom/sec/ims/IImsService;->registerImsRegistrationListener(Lcom/sec/ims/IImsRegistrationListener;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 440
    :catch_0
    move-exception v0

    .line 441
    .local v0, "e":Landroid/os/RemoteException;
    :try_start_2
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 431
    .end local v0    # "e":Landroid/os/RemoteException;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public registerListener(Lcom/sec/ims/ImsEventListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/sec/ims/ImsEventListener;

    .prologue
    .line 111
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/sec/ims/ImsManager;->registerListener(Lcom/sec/ims/ImsEventListener;Ljava/lang/String;)V

    .line 112
    return-void
.end method

.method public registerListener(Lcom/sec/ims/ImsEventListener;Ljava/lang/String;)V
    .locals 3
    .param p1, "listener"    # Lcom/sec/ims/ImsEventListener;
    .param p2, "caller"    # Ljava/lang/String;

    .prologue
    .line 116
    iget-object v1, p0, Lcom/sec/ims/ImsManager;->mImsService:Lcom/sec/ims/IImsService;

    if-nez v1, :cond_0

    .line 117
    const-string v1, "ImsManager"

    const-string v2, "Not initialized."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    :goto_0
    return-void

    .line 121
    :cond_0
    if-nez p2, :cond_1

    .line 122
    const-string p2, ""

    .line 126
    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/sec/ims/ImsManager;->mImsService:Lcom/sec/ims/IImsService;

    invoke-interface {v1, p1, p2}, Lcom/sec/ims/IImsService;->registerCallback(Lcom/sec/ims/ImsEventListener;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 127
    :catch_0
    move-exception v0

    .line 129
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public registerProfile(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 486
    .local p1, "profileIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :try_start_0
    iget-object v1, p0, Lcom/sec/ims/ImsManager;->mImsService:Lcom/sec/ims/IImsService;

    invoke-interface {v1, p1}, Lcom/sec/ims/IImsService;->registerProfile(Ljava/util/List;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 490
    :goto_0
    return-void

    .line 487
    :catch_0
    move-exception v0

    .line 488
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public setFeatureValue(IIILcom/sec/ims/ImsConfigListener;)V
    .locals 3
    .param p1, "feature"    # I
    .param p2, "network"    # I
    .param p3, "value"    # I
    .param p4, "listener"    # Lcom/sec/ims/ImsConfigListener;

    .prologue
    .line 414
    iget-object v1, p0, Lcom/sec/ims/ImsManager;->mImsService:Lcom/sec/ims/IImsService;

    if-nez v1, :cond_0

    .line 415
    const-string v1, "ImsManager"

    const-string v2, "setFeatureValue: not connected."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 423
    :goto_0
    return-void

    .line 419
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/ims/ImsManager;->mImsService:Lcom/sec/ims/IImsService;

    invoke-interface {v1, p1, p2, p3, p4}, Lcom/sec/ims/IImsService;->setFeatureValue(IIILcom/sec/ims/ImsConfigListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 420
    :catch_0
    move-exception v0

    .line 421
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public setProvisionedStringValue(ILjava/lang/String;)V
    .locals 3
    .param p1, "item"    # I
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 388
    iget-object v1, p0, Lcom/sec/ims/ImsManager;->mImsService:Lcom/sec/ims/IImsService;

    if-nez v1, :cond_0

    .line 389
    const-string v1, "ImsManager"

    const-string v2, "setProvisionedStringValue: not connected."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 397
    :goto_0
    return-void

    .line 393
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/ims/ImsManager;->mImsService:Lcom/sec/ims/IImsService;

    invoke-interface {v1, p1, p2}, Lcom/sec/ims/IImsService;->setProvisionedStringValue(ILjava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 394
    :catch_0
    move-exception v0

    .line 395
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public setProvisionedValue(II)V
    .locals 3
    .param p1, "item"    # I
    .param p2, "value"    # I

    .prologue
    .line 375
    iget-object v1, p0, Lcom/sec/ims/ImsManager;->mImsService:Lcom/sec/ims/IImsService;

    if-nez v1, :cond_0

    .line 376
    const-string v1, "ImsManager"

    const-string v2, "setProvisionedValue: not connected."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 384
    :goto_0
    return-void

    .line 380
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/ims/ImsManager;->mImsService:Lcom/sec/ims/IImsService;

    invoke-interface {v1, p1, p2}, Lcom/sec/ims/IImsService;->setProvisionedValue(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 381
    :catch_0
    move-exception v0

    .line 382
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public declared-synchronized unregisterImsRegistrationListener(Lcom/sec/ims/IImsRegistrationListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/sec/ims/IImsRegistrationListener;

    .prologue
    .line 447
    monitor-enter p0

    :try_start_0
    const-string v1, "ImsManager"

    const-string v2, "unregisterImsRegistrationListener"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 448
    iget-object v1, p0, Lcom/sec/ims/ImsManager;->mImsService:Lcom/sec/ims/IImsService;

    if-nez v1, :cond_0

    .line 449
    const-string v1, "ImsManager"

    const-string v2, "Not initialized."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 450
    iget-object v1, p0, Lcom/sec/ims/ImsManager;->mPreConnectionRegListeners:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 459
    :goto_0
    monitor-exit p0

    return-void

    .line 455
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/sec/ims/ImsManager;->mImsService:Lcom/sec/ims/IImsService;

    invoke-interface {v1, p1}, Lcom/sec/ims/IImsService;->unregisterImsRegistrationListener(Lcom/sec/ims/IImsRegistrationListener;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 456
    :catch_0
    move-exception v0

    .line 457
    .local v0, "e":Landroid/os/RemoteException;
    :try_start_2
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 447
    .end local v0    # "e":Landroid/os/RemoteException;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public unregisterListener(Lcom/sec/ims/ImsEventListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/sec/ims/ImsEventListener;

    .prologue
    .line 135
    iget-object v1, p0, Lcom/sec/ims/ImsManager;->mImsService:Lcom/sec/ims/IImsService;

    if-nez v1, :cond_0

    .line 136
    const-string v1, "ImsManager"

    const-string v2, "Not initialized."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    :goto_0
    return-void

    .line 141
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/ims/ImsManager;->mImsService:Lcom/sec/ims/IImsService;

    invoke-interface {v1, p1}, Lcom/sec/ims/IImsService;->unregisterCallback(Lcom/sec/ims/ImsEventListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 142
    :catch_0
    move-exception v0

    .line 144
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method
