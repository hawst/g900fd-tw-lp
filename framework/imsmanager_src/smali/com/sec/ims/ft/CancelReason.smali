.class public final enum Lcom/sec/ims/ft/CancelReason;
.super Ljava/lang/Enum;
.source "CancelReason.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/ims/ft/CancelReason;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/ims/ft/CancelReason;

.field public static final enum CANCELED_BY_REMOTE:Lcom/sec/ims/ft/CancelReason;

.field public static final enum CANCELED_BY_SYSTEM:Lcom/sec/ims/ft/CancelReason;

.field public static final enum CANCELED_BY_USER:Lcom/sec/ims/ft/CancelReason;

.field public static final enum CONNECTION_RELEASED:Lcom/sec/ims/ft/CancelReason;

.field public static final enum CONTENT_REACHED_DOWNSIZE:Lcom/sec/ims/ft/CancelReason;

.field public static final enum DEDICATED_BEARER_UNAVAILABLE_TIMEOUT:Lcom/sec/ims/ft/CancelReason;

.field public static final enum DEVICE_UNREGISTERED:Lcom/sec/ims/ft/CancelReason;

.field public static final enum ERROR:Lcom/sec/ims/ft/CancelReason;

.field public static final enum FORBIDDEN_NO_RETRY_FALLBACK:Lcom/sec/ims/ft/CancelReason;

.field public static final enum INVALID_REQUEST:Lcom/sec/ims/ft/CancelReason;

.field public static final enum LOCALLY_ABORTED:Lcom/sec/ims/ft/CancelReason;

.field public static final enum LOW_MEMORY:Lcom/sec/ims/ft/CancelReason;

.field public static final enum NOT_AUTHORIZED:Lcom/sec/ims/ft/CancelReason;

.field public static final enum NO_RESPONSE:Lcom/sec/ims/ft/CancelReason;

.field public static final enum REJECTED_BY_REMOTE:Lcom/sec/ims/ft/CancelReason;

.field public static final enum REJECTED_BY_USER:Lcom/sec/ims/ft/CancelReason;

.field public static final enum REMOTE_BLOCKED:Lcom/sec/ims/ft/CancelReason;

.field public static final enum REMOTE_TEMPORARILY_UNAVAILABLE:Lcom/sec/ims/ft/CancelReason;

.field public static final enum REMOTE_USER_INVALID:Lcom/sec/ims/ft/CancelReason;

.field public static final enum TIME_OUT:Lcom/sec/ims/ft/CancelReason;

.field public static final enum TOO_LARGE:Lcom/sec/ims/ft/CancelReason;

.field public static final enum UNKNOWN:Lcom/sec/ims/ft/CancelReason;

.field public static final enum VALIDITY_EXPIRED:Lcom/sec/ims/ft/CancelReason;


# instance fields
.field private final mId:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 20
    new-instance v0, Lcom/sec/ims/ft/CancelReason;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v4, v4}, Lcom/sec/ims/ft/CancelReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/ims/ft/CancelReason;->UNKNOWN:Lcom/sec/ims/ft/CancelReason;

    .line 25
    new-instance v0, Lcom/sec/ims/ft/CancelReason;

    const-string v1, "CANCELED_BY_USER"

    invoke-direct {v0, v1, v5, v5}, Lcom/sec/ims/ft/CancelReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/ims/ft/CancelReason;->CANCELED_BY_USER:Lcom/sec/ims/ft/CancelReason;

    .line 30
    new-instance v0, Lcom/sec/ims/ft/CancelReason;

    const-string v1, "CANCELED_BY_REMOTE"

    invoke-direct {v0, v1, v6, v6}, Lcom/sec/ims/ft/CancelReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/ims/ft/CancelReason;->CANCELED_BY_REMOTE:Lcom/sec/ims/ft/CancelReason;

    .line 35
    new-instance v0, Lcom/sec/ims/ft/CancelReason;

    const-string v1, "CANCELED_BY_SYSTEM"

    invoke-direct {v0, v1, v7, v7}, Lcom/sec/ims/ft/CancelReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/ims/ft/CancelReason;->CANCELED_BY_SYSTEM:Lcom/sec/ims/ft/CancelReason;

    .line 40
    new-instance v0, Lcom/sec/ims/ft/CancelReason;

    const-string v1, "REJECTED_BY_USER"

    invoke-direct {v0, v1, v8, v8}, Lcom/sec/ims/ft/CancelReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/ims/ft/CancelReason;->REJECTED_BY_USER:Lcom/sec/ims/ft/CancelReason;

    .line 45
    new-instance v0, Lcom/sec/ims/ft/CancelReason;

    const-string v1, "REJECTED_BY_REMOTE"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/ims/ft/CancelReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/ims/ft/CancelReason;->REJECTED_BY_REMOTE:Lcom/sec/ims/ft/CancelReason;

    .line 50
    new-instance v0, Lcom/sec/ims/ft/CancelReason;

    const-string v1, "TIME_OUT"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/ims/ft/CancelReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/ims/ft/CancelReason;->TIME_OUT:Lcom/sec/ims/ft/CancelReason;

    .line 55
    new-instance v0, Lcom/sec/ims/ft/CancelReason;

    const-string v1, "LOW_MEMORY"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/ims/ft/CancelReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/ims/ft/CancelReason;->LOW_MEMORY:Lcom/sec/ims/ft/CancelReason;

    .line 60
    new-instance v0, Lcom/sec/ims/ft/CancelReason;

    const-string v1, "TOO_LARGE"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/ims/ft/CancelReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/ims/ft/CancelReason;->TOO_LARGE:Lcom/sec/ims/ft/CancelReason;

    .line 65
    new-instance v0, Lcom/sec/ims/ft/CancelReason;

    const-string v1, "NOT_AUTHORIZED"

    const/16 v2, 0x9

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/ims/ft/CancelReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/ims/ft/CancelReason;->NOT_AUTHORIZED:Lcom/sec/ims/ft/CancelReason;

    .line 70
    new-instance v0, Lcom/sec/ims/ft/CancelReason;

    const-string v1, "REMOTE_BLOCKED"

    const/16 v2, 0xa

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/ims/ft/CancelReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/ims/ft/CancelReason;->REMOTE_BLOCKED:Lcom/sec/ims/ft/CancelReason;

    .line 75
    new-instance v0, Lcom/sec/ims/ft/CancelReason;

    const-string v1, "VALIDITY_EXPIRED"

    const/16 v2, 0xb

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/ims/ft/CancelReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/ims/ft/CancelReason;->VALIDITY_EXPIRED:Lcom/sec/ims/ft/CancelReason;

    .line 80
    new-instance v0, Lcom/sec/ims/ft/CancelReason;

    const-string v1, "REMOTE_TEMPORARILY_UNAVAILABLE"

    const/16 v2, 0xc

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/ims/ft/CancelReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/ims/ft/CancelReason;->REMOTE_TEMPORARILY_UNAVAILABLE:Lcom/sec/ims/ft/CancelReason;

    .line 85
    new-instance v0, Lcom/sec/ims/ft/CancelReason;

    const-string v1, "ERROR"

    const/16 v2, 0xd

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/ims/ft/CancelReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/ims/ft/CancelReason;->ERROR:Lcom/sec/ims/ft/CancelReason;

    .line 90
    new-instance v0, Lcom/sec/ims/ft/CancelReason;

    const-string v1, "INVALID_REQUEST"

    const/16 v2, 0xe

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/ims/ft/CancelReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/ims/ft/CancelReason;->INVALID_REQUEST:Lcom/sec/ims/ft/CancelReason;

    .line 95
    new-instance v0, Lcom/sec/ims/ft/CancelReason;

    const-string v1, "REMOTE_USER_INVALID"

    const/16 v2, 0xf

    const/16 v3, 0xf

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/ims/ft/CancelReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/ims/ft/CancelReason;->REMOTE_USER_INVALID:Lcom/sec/ims/ft/CancelReason;

    .line 100
    new-instance v0, Lcom/sec/ims/ft/CancelReason;

    const-string v1, "FORBIDDEN_NO_RETRY_FALLBACK"

    const/16 v2, 0x10

    const/16 v3, 0x10

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/ims/ft/CancelReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/ims/ft/CancelReason;->FORBIDDEN_NO_RETRY_FALLBACK:Lcom/sec/ims/ft/CancelReason;

    .line 105
    new-instance v0, Lcom/sec/ims/ft/CancelReason;

    const-string v1, "CONTENT_REACHED_DOWNSIZE"

    const/16 v2, 0x11

    const/16 v3, 0x11

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/ims/ft/CancelReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/ims/ft/CancelReason;->CONTENT_REACHED_DOWNSIZE:Lcom/sec/ims/ft/CancelReason;

    .line 110
    new-instance v0, Lcom/sec/ims/ft/CancelReason;

    const-string v1, "NO_RESPONSE"

    const/16 v2, 0x12

    const/16 v3, 0x12

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/ims/ft/CancelReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/ims/ft/CancelReason;->NO_RESPONSE:Lcom/sec/ims/ft/CancelReason;

    .line 116
    new-instance v0, Lcom/sec/ims/ft/CancelReason;

    const-string v1, "LOCALLY_ABORTED"

    const/16 v2, 0x13

    const/16 v3, 0x13

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/ims/ft/CancelReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/ims/ft/CancelReason;->LOCALLY_ABORTED:Lcom/sec/ims/ft/CancelReason;

    .line 121
    new-instance v0, Lcom/sec/ims/ft/CancelReason;

    const-string v1, "CONNECTION_RELEASED"

    const/16 v2, 0x14

    const/16 v3, 0x14

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/ims/ft/CancelReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/ims/ft/CancelReason;->CONNECTION_RELEASED:Lcom/sec/ims/ft/CancelReason;

    .line 126
    new-instance v0, Lcom/sec/ims/ft/CancelReason;

    const-string v1, "DEVICE_UNREGISTERED"

    const/16 v2, 0x15

    const/16 v3, 0x15

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/ims/ft/CancelReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/ims/ft/CancelReason;->DEVICE_UNREGISTERED:Lcom/sec/ims/ft/CancelReason;

    .line 131
    new-instance v0, Lcom/sec/ims/ft/CancelReason;

    const-string v1, "DEDICATED_BEARER_UNAVAILABLE_TIMEOUT"

    const/16 v2, 0x16

    const/16 v3, 0x16

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/ims/ft/CancelReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/ims/ft/CancelReason;->DEDICATED_BEARER_UNAVAILABLE_TIMEOUT:Lcom/sec/ims/ft/CancelReason;

    .line 15
    const/16 v0, 0x17

    new-array v0, v0, [Lcom/sec/ims/ft/CancelReason;

    sget-object v1, Lcom/sec/ims/ft/CancelReason;->UNKNOWN:Lcom/sec/ims/ft/CancelReason;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/ims/ft/CancelReason;->CANCELED_BY_USER:Lcom/sec/ims/ft/CancelReason;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/ims/ft/CancelReason;->CANCELED_BY_REMOTE:Lcom/sec/ims/ft/CancelReason;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/ims/ft/CancelReason;->CANCELED_BY_SYSTEM:Lcom/sec/ims/ft/CancelReason;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sec/ims/ft/CancelReason;->REJECTED_BY_USER:Lcom/sec/ims/ft/CancelReason;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/ims/ft/CancelReason;->REJECTED_BY_REMOTE:Lcom/sec/ims/ft/CancelReason;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/ims/ft/CancelReason;->TIME_OUT:Lcom/sec/ims/ft/CancelReason;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/ims/ft/CancelReason;->LOW_MEMORY:Lcom/sec/ims/ft/CancelReason;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/ims/ft/CancelReason;->TOO_LARGE:Lcom/sec/ims/ft/CancelReason;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/ims/ft/CancelReason;->NOT_AUTHORIZED:Lcom/sec/ims/ft/CancelReason;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/ims/ft/CancelReason;->REMOTE_BLOCKED:Lcom/sec/ims/ft/CancelReason;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/sec/ims/ft/CancelReason;->VALIDITY_EXPIRED:Lcom/sec/ims/ft/CancelReason;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/sec/ims/ft/CancelReason;->REMOTE_TEMPORARILY_UNAVAILABLE:Lcom/sec/ims/ft/CancelReason;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/sec/ims/ft/CancelReason;->ERROR:Lcom/sec/ims/ft/CancelReason;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/sec/ims/ft/CancelReason;->INVALID_REQUEST:Lcom/sec/ims/ft/CancelReason;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/sec/ims/ft/CancelReason;->REMOTE_USER_INVALID:Lcom/sec/ims/ft/CancelReason;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/sec/ims/ft/CancelReason;->FORBIDDEN_NO_RETRY_FALLBACK:Lcom/sec/ims/ft/CancelReason;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/sec/ims/ft/CancelReason;->CONTENT_REACHED_DOWNSIZE:Lcom/sec/ims/ft/CancelReason;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/sec/ims/ft/CancelReason;->NO_RESPONSE:Lcom/sec/ims/ft/CancelReason;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/sec/ims/ft/CancelReason;->LOCALLY_ABORTED:Lcom/sec/ims/ft/CancelReason;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/sec/ims/ft/CancelReason;->CONNECTION_RELEASED:Lcom/sec/ims/ft/CancelReason;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/sec/ims/ft/CancelReason;->DEVICE_UNREGISTERED:Lcom/sec/ims/ft/CancelReason;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/sec/ims/ft/CancelReason;->DEDICATED_BEARER_UNAVAILABLE_TIMEOUT:Lcom/sec/ims/ft/CancelReason;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/ims/ft/CancelReason;->$VALUES:[Lcom/sec/ims/ft/CancelReason;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "id"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 134
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 135
    iput p3, p0, Lcom/sec/ims/ft/CancelReason;->mId:I

    .line 136
    return-void
.end method

.method public static valueOf(I)Lcom/sec/ims/ft/CancelReason;
    .locals 6
    .param p0, "id"    # I

    .prologue
    .line 144
    const/4 v1, 0x0

    .line 146
    .local v1, "cancelReason":Lcom/sec/ims/ft/CancelReason;
    invoke-static {}, Lcom/sec/ims/ft/CancelReason;->values()[Lcom/sec/ims/ft/CancelReason;

    move-result-object v0

    .local v0, "arr$":[Lcom/sec/ims/ft/CancelReason;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v4, v0, v2

    .line 147
    .local v4, "r":Lcom/sec/ims/ft/CancelReason;
    iget v5, v4, Lcom/sec/ims/ft/CancelReason;->mId:I

    if-ne v5, p0, :cond_1

    .line 148
    move-object v1, v4

    .line 153
    .end local v4    # "r":Lcom/sec/ims/ft/CancelReason;
    :cond_0
    return-object v1

    .line 146
    .restart local v4    # "r":Lcom/sec/ims/ft/CancelReason;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/ims/ft/CancelReason;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 15
    const-class v0, Lcom/sec/ims/ft/CancelReason;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/ims/ft/CancelReason;

    return-object v0
.end method

.method public static values()[Lcom/sec/ims/ft/CancelReason;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/sec/ims/ft/CancelReason;->$VALUES:[Lcom/sec/ims/ft/CancelReason;

    invoke-virtual {v0}, [Lcom/sec/ims/ft/CancelReason;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/ims/ft/CancelReason;

    return-object v0
.end method


# virtual methods
.method public getId()I
    .locals 1

    .prologue
    .line 139
    iget v0, p0, Lcom/sec/ims/ft/CancelReason;->mId:I

    return v0
.end method
