.class public final enum Lcom/sec/ims/ft/RejectReason;
.super Ljava/lang/Enum;
.source "RejectReason.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/ims/ft/RejectReason;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/ims/ft/RejectReason;

.field public static final enum DEDICATED_BEARER_UNAVAILABLE_TIMEOUT:Lcom/sec/ims/ft/RejectReason;

.field public static final enum REJECTED_BY_LOCAL_CHOICE:Lcom/sec/ims/ft/RejectReason;

.field public static final enum REJECTED_CORRUPTED_INVITATION_PARAMETERS:Lcom/sec/ims/ft/RejectReason;

.field public static final enum REJECTED_INTERNAL_ERROR:Lcom/sec/ims/ft/RejectReason;

.field public static final enum REJECTED_MAX_SIZE_EXCEEDED:Lcom/sec/ims/ft/RejectReason;

.field public static final enum REJECTED_NO_AVAILABLE_DISK_SPACE:Lcom/sec/ims/ft/RejectReason;

.field public static final enum REJECTED_SENDER_ISBLOCKED:Lcom/sec/ims/ft/RejectReason;


# instance fields
.field private final mId:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 20
    new-instance v0, Lcom/sec/ims/ft/RejectReason;

    const-string v1, "REJECTED_MAX_SIZE_EXCEEDED"

    const/16 v2, 0xc8

    invoke-direct {v0, v1, v4, v2}, Lcom/sec/ims/ft/RejectReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/ims/ft/RejectReason;->REJECTED_MAX_SIZE_EXCEEDED:Lcom/sec/ims/ft/RejectReason;

    .line 25
    new-instance v0, Lcom/sec/ims/ft/RejectReason;

    const-string v1, "REJECTED_NO_AVAILABLE_DISK_SPACE"

    const/16 v2, 0xc9

    invoke-direct {v0, v1, v5, v2}, Lcom/sec/ims/ft/RejectReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/ims/ft/RejectReason;->REJECTED_NO_AVAILABLE_DISK_SPACE:Lcom/sec/ims/ft/RejectReason;

    .line 30
    new-instance v0, Lcom/sec/ims/ft/RejectReason;

    const-string v1, "REJECTED_BY_LOCAL_CHOICE"

    const/16 v2, 0xca

    invoke-direct {v0, v1, v6, v2}, Lcom/sec/ims/ft/RejectReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/ims/ft/RejectReason;->REJECTED_BY_LOCAL_CHOICE:Lcom/sec/ims/ft/RejectReason;

    .line 36
    new-instance v0, Lcom/sec/ims/ft/RejectReason;

    const-string v1, "REJECTED_CORRUPTED_INVITATION_PARAMETERS"

    const/16 v2, 0xcb

    invoke-direct {v0, v1, v7, v2}, Lcom/sec/ims/ft/RejectReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/ims/ft/RejectReason;->REJECTED_CORRUPTED_INVITATION_PARAMETERS:Lcom/sec/ims/ft/RejectReason;

    .line 41
    new-instance v0, Lcom/sec/ims/ft/RejectReason;

    const-string v1, "REJECTED_INTERNAL_ERROR"

    const/16 v2, 0xcc

    invoke-direct {v0, v1, v8, v2}, Lcom/sec/ims/ft/RejectReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/ims/ft/RejectReason;->REJECTED_INTERNAL_ERROR:Lcom/sec/ims/ft/RejectReason;

    .line 46
    new-instance v0, Lcom/sec/ims/ft/RejectReason;

    const-string v1, "REJECTED_SENDER_ISBLOCKED"

    const/4 v2, 0x5

    const/16 v3, 0xcd

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/ims/ft/RejectReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/ims/ft/RejectReason;->REJECTED_SENDER_ISBLOCKED:Lcom/sec/ims/ft/RejectReason;

    .line 51
    new-instance v0, Lcom/sec/ims/ft/RejectReason;

    const-string v1, "DEDICATED_BEARER_UNAVAILABLE_TIMEOUT"

    const/4 v2, 0x6

    const/16 v3, 0xce

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/ims/ft/RejectReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/ims/ft/RejectReason;->DEDICATED_BEARER_UNAVAILABLE_TIMEOUT:Lcom/sec/ims/ft/RejectReason;

    .line 15
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/sec/ims/ft/RejectReason;

    sget-object v1, Lcom/sec/ims/ft/RejectReason;->REJECTED_MAX_SIZE_EXCEEDED:Lcom/sec/ims/ft/RejectReason;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/ims/ft/RejectReason;->REJECTED_NO_AVAILABLE_DISK_SPACE:Lcom/sec/ims/ft/RejectReason;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/ims/ft/RejectReason;->REJECTED_BY_LOCAL_CHOICE:Lcom/sec/ims/ft/RejectReason;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/ims/ft/RejectReason;->REJECTED_CORRUPTED_INVITATION_PARAMETERS:Lcom/sec/ims/ft/RejectReason;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sec/ims/ft/RejectReason;->REJECTED_INTERNAL_ERROR:Lcom/sec/ims/ft/RejectReason;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/ims/ft/RejectReason;->REJECTED_SENDER_ISBLOCKED:Lcom/sec/ims/ft/RejectReason;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/ims/ft/RejectReason;->DEDICATED_BEARER_UNAVAILABLE_TIMEOUT:Lcom/sec/ims/ft/RejectReason;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/ims/ft/RejectReason;->$VALUES:[Lcom/sec/ims/ft/RejectReason;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "id"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 57
    iput p3, p0, Lcom/sec/ims/ft/RejectReason;->mId:I

    .line 58
    return-void
.end method

.method public static valueOf(I)Lcom/sec/ims/ft/RejectReason;
    .locals 6
    .param p0, "id"    # I

    .prologue
    .line 66
    const/4 v4, 0x0

    .line 68
    .local v4, "rejectReason":Lcom/sec/ims/ft/RejectReason;
    invoke-static {}, Lcom/sec/ims/ft/RejectReason;->values()[Lcom/sec/ims/ft/RejectReason;

    move-result-object v0

    .local v0, "arr$":[Lcom/sec/ims/ft/RejectReason;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 69
    .local v3, "r":Lcom/sec/ims/ft/RejectReason;
    iget v5, v3, Lcom/sec/ims/ft/RejectReason;->mId:I

    if-ne v5, p0, :cond_1

    .line 70
    move-object v4, v3

    .line 75
    .end local v3    # "r":Lcom/sec/ims/ft/RejectReason;
    :cond_0
    return-object v4

    .line 68
    .restart local v3    # "r":Lcom/sec/ims/ft/RejectReason;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/ims/ft/RejectReason;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 15
    const-class v0, Lcom/sec/ims/ft/RejectReason;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/ims/ft/RejectReason;

    return-object v0
.end method

.method public static values()[Lcom/sec/ims/ft/RejectReason;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/sec/ims/ft/RejectReason;->$VALUES:[Lcom/sec/ims/ft/RejectReason;

    invoke-virtual {v0}, [Lcom/sec/ims/ft/RejectReason;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/ims/ft/RejectReason;

    return-object v0
.end method


# virtual methods
.method public getId()I
    .locals 1

    .prologue
    .line 61
    iget v0, p0, Lcom/sec/ims/ft/RejectReason;->mId:I

    return v0
.end method
