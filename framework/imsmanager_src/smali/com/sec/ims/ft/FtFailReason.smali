.class public final enum Lcom/sec/ims/ft/FtFailReason;
.super Ljava/lang/Enum;
.source "FtFailReason.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/ims/ft/FtFailReason;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/ims/ft/FtFailReason;

.field public static final enum ADDRESS_INCOMPLETE:Lcom/sec/ims/ft/FtFailReason;

.field public static final enum ALTERNATE_SERVICE:Lcom/sec/ims/ft/FtFailReason;

.field public static final enum AMBIGUOUS:Lcom/sec/ims/ft/FtFailReason;

.field public static final enum BAD_EXTENSION:Lcom/sec/ims/ft/FtFailReason;

.field public static final enum BAD_GATEWAY:Lcom/sec/ims/ft/FtFailReason;

.field public static final enum BUSY_EVERYWHERE:Lcom/sec/ims/ft/FtFailReason;

.field public static final enum CANCELED_BY_LOCAL:Lcom/sec/ims/ft/FtFailReason;

.field public static final enum COMPLETE_DATA_RECEIVED:Lcom/sec/ims/ft/FtFailReason;

.field public static final enum CONNECTION_RELEASED:Lcom/sec/ims/ft/FtFailReason;

.field public static final enum CONTENT_REACHED_DOWNSIZE:Lcom/sec/ims/ft/FtFailReason;

.field public static final enum CONTINUED_ON_ANOTHER_DEVICE:Lcom/sec/ims/ft/FtFailReason;

.field public static final enum DEDICATED_BEARER_AVAILABLE:Lcom/sec/ims/ft/FtFailReason;

.field public static final enum DEDICATED_BEARER_FALLBACK:Lcom/sec/ims/ft/FtFailReason;

.field public static final enum DEDICATED_BEARER_UNAVAILABLE:Lcom/sec/ims/ft/FtFailReason;

.field public static final enum DEVICE_UNREGISTERED:Lcom/sec/ims/ft/FtFailReason;

.field public static final enum ENGINE_ERROR:Lcom/sec/ims/ft/FtFailReason;

.field public static final enum EXTENSION_REQUIRED:Lcom/sec/ims/ft/FtFailReason;

.field public static final enum FILE_IO_ERROR:Lcom/sec/ims/ft/FtFailReason;

.field public static final enum FORBIDDEN_NO_RETRY_FALLBACK:Lcom/sec/ims/ft/FtFailReason;

.field public static final enum GONE:Lcom/sec/ims/ft/FtFailReason;

.field public static final enum INTERNAL_SERVER_ERROR:Lcom/sec/ims/ft/FtFailReason;

.field public static final enum INVALID_REQUEST:Lcom/sec/ims/ft/FtFailReason;

.field public static final enum LOOP_DETECTED:Lcom/sec/ims/ft/FtFailReason;

.field public static final enum MAX_SIZE_EXCEEDED:Lcom/sec/ims/ft/FtFailReason;

.field public static final enum MESSAGE_TOO_LARGE:Lcom/sec/ims/ft/FtFailReason;

.field public static final enum METHOD_NOT_ALLOWED:Lcom/sec/ims/ft/FtFailReason;

.field public static final enum MOVED_PERMANENTLY:Lcom/sec/ims/ft/FtFailReason;

.field public static final enum MOVED_TEMPORARILY:Lcom/sec/ims/ft/FtFailReason;

.field public static final enum MULTIPLE_CHOICES:Lcom/sec/ims/ft/FtFailReason;

.field public static final enum NETWORK_ERROR:Lcom/sec/ims/ft/FtFailReason;

.field public static final enum NOTEXIST_ANYWHERE:Lcom/sec/ims/ft/FtFailReason;

.field public static final enum NOT_ACCEPTABLE:Lcom/sec/ims/ft/FtFailReason;

.field public static final enum NOT_AUTHORIZED:Lcom/sec/ims/ft/FtFailReason;

.field public static final enum NOT_IMPLEMENTED:Lcom/sec/ims/ft/FtFailReason;

.field public static final enum NO_RESPONSE:Lcom/sec/ims/ft/FtFailReason;

.field public static final enum REMOTE_PARTY_CANCELED:Lcom/sec/ims/ft/FtFailReason;

.field public static final enum REMOTE_PARTY_CLOSED:Lcom/sec/ims/ft/FtFailReason;

.field public static final enum REMOTE_PARTY_DECLINED:Lcom/sec/ims/ft/FtFailReason;

.field public static final enum REMOTE_PARTY_REJECTED:Lcom/sec/ims/ft/FtFailReason;

.field public static final enum REMOTE_TEMPORARILY_UNAVAILABLE:Lcom/sec/ims/ft/FtFailReason;

.field public static final enum REMOTE_USER_INVALID:Lcom/sec/ims/ft/FtFailReason;

.field public static final enum REQEUST_ENTITY_TOO_LARGE:Lcom/sec/ims/ft/FtFailReason;

.field public static final enum REQUEST_PENDING:Lcom/sec/ims/ft/FtFailReason;

.field public static final enum REQUEST_URI_TOO_LARGE:Lcom/sec/ims/ft/FtFailReason;

.field public static final enum SECURITY_AGREEMENT_REQD:Lcom/sec/ims/ft/FtFailReason;

.field public static final enum SERVER_NOT_ACCEPTABLE:Lcom/sec/ims/ft/FtFailReason;

.field public static final enum SERVER_TIMEOUT:Lcom/sec/ims/ft/FtFailReason;

.field public static final enum SERVICE_UNAVAILABLE:Lcom/sec/ims/ft/FtFailReason;

.field public static final enum SESSION_DOESNT_EXIST:Lcom/sec/ims/ft/FtFailReason;

.field public static final enum SESSION_RSRC_UNAVAILABLE:Lcom/sec/ims/ft/FtFailReason;

.field public static final enum SESSION_TIMED_OUT:Lcom/sec/ims/ft/FtFailReason;

.field public static final enum SIP_VERSION_NOT_SUPPORTED:Lcom/sec/ims/ft/FtFailReason;

.field public static final enum SUCCESS:Lcom/sec/ims/ft/FtFailReason;

.field public static final enum TOO_MANY_HOPS:Lcom/sec/ims/ft/FtFailReason;

.field public static final enum UNDECEIPHERABLE:Lcom/sec/ims/ft/FtFailReason;

.field public static final enum UNSUPPORTED_MEDIA_TYPE:Lcom/sec/ims/ft/FtFailReason;

.field public static final enum UNSUPPORTED_URI_SCHEME:Lcom/sec/ims/ft/FtFailReason;

.field public static final enum USE_PROXY:Lcom/sec/ims/ft/FtFailReason;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 16
    new-instance v0, Lcom/sec/ims/ft/FtFailReason;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v3}, Lcom/sec/ims/ft/FtFailReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/ft/FtFailReason;->SUCCESS:Lcom/sec/ims/ft/FtFailReason;

    .line 17
    new-instance v0, Lcom/sec/ims/ft/FtFailReason;

    const-string v1, "DEDICATED_BEARER_AVAILABLE"

    invoke-direct {v0, v1, v4}, Lcom/sec/ims/ft/FtFailReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/ft/FtFailReason;->DEDICATED_BEARER_AVAILABLE:Lcom/sec/ims/ft/FtFailReason;

    .line 18
    new-instance v0, Lcom/sec/ims/ft/FtFailReason;

    const-string v1, "DEDICATED_BEARER_UNAVAILABLE"

    invoke-direct {v0, v1, v5}, Lcom/sec/ims/ft/FtFailReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/ft/FtFailReason;->DEDICATED_BEARER_UNAVAILABLE:Lcom/sec/ims/ft/FtFailReason;

    .line 19
    new-instance v0, Lcom/sec/ims/ft/FtFailReason;

    const-string v1, "DEDICATED_BEARER_FALLBACK"

    invoke-direct {v0, v1, v6}, Lcom/sec/ims/ft/FtFailReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/ft/FtFailReason;->DEDICATED_BEARER_FALLBACK:Lcom/sec/ims/ft/FtFailReason;

    .line 20
    new-instance v0, Lcom/sec/ims/ft/FtFailReason;

    const-string v1, "REMOTE_USER_INVALID"

    invoke-direct {v0, v1, v7}, Lcom/sec/ims/ft/FtFailReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/ft/FtFailReason;->REMOTE_USER_INVALID:Lcom/sec/ims/ft/FtFailReason;

    .line 21
    new-instance v0, Lcom/sec/ims/ft/FtFailReason;

    const-string v1, "NOT_AUTHORIZED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/ft/FtFailReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/ft/FtFailReason;->NOT_AUTHORIZED:Lcom/sec/ims/ft/FtFailReason;

    .line 22
    new-instance v0, Lcom/sec/ims/ft/FtFailReason;

    const-string v1, "SESSION_TIMED_OUT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/ft/FtFailReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/ft/FtFailReason;->SESSION_TIMED_OUT:Lcom/sec/ims/ft/FtFailReason;

    .line 23
    new-instance v0, Lcom/sec/ims/ft/FtFailReason;

    const-string v1, "REMOTE_PARTY_REJECTED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/ft/FtFailReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/ft/FtFailReason;->REMOTE_PARTY_REJECTED:Lcom/sec/ims/ft/FtFailReason;

    .line 24
    new-instance v0, Lcom/sec/ims/ft/FtFailReason;

    const-string v1, "REMOTE_PARTY_DECLINED"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/ft/FtFailReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/ft/FtFailReason;->REMOTE_PARTY_DECLINED:Lcom/sec/ims/ft/FtFailReason;

    .line 25
    new-instance v0, Lcom/sec/ims/ft/FtFailReason;

    const-string v1, "REMOTE_PARTY_CANCELED"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/ft/FtFailReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/ft/FtFailReason;->REMOTE_PARTY_CANCELED:Lcom/sec/ims/ft/FtFailReason;

    .line 26
    new-instance v0, Lcom/sec/ims/ft/FtFailReason;

    const-string v1, "REMOTE_PARTY_CLOSED"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/ft/FtFailReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/ft/FtFailReason;->REMOTE_PARTY_CLOSED:Lcom/sec/ims/ft/FtFailReason;

    .line 27
    new-instance v0, Lcom/sec/ims/ft/FtFailReason;

    const-string v1, "INVALID_REQUEST"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/ft/FtFailReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/ft/FtFailReason;->INVALID_REQUEST:Lcom/sec/ims/ft/FtFailReason;

    .line 28
    new-instance v0, Lcom/sec/ims/ft/FtFailReason;

    const-string v1, "FILE_IO_ERROR"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/ft/FtFailReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/ft/FtFailReason;->FILE_IO_ERROR:Lcom/sec/ims/ft/FtFailReason;

    .line 29
    new-instance v0, Lcom/sec/ims/ft/FtFailReason;

    const-string v1, "NETWORK_ERROR"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/ft/FtFailReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/ft/FtFailReason;->NETWORK_ERROR:Lcom/sec/ims/ft/FtFailReason;

    .line 30
    new-instance v0, Lcom/sec/ims/ft/FtFailReason;

    const-string v1, "MAX_SIZE_EXCEEDED"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/ft/FtFailReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/ft/FtFailReason;->MAX_SIZE_EXCEEDED:Lcom/sec/ims/ft/FtFailReason;

    .line 31
    new-instance v0, Lcom/sec/ims/ft/FtFailReason;

    const-string v1, "COMPLETE_DATA_RECEIVED"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/ft/FtFailReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/ft/FtFailReason;->COMPLETE_DATA_RECEIVED:Lcom/sec/ims/ft/FtFailReason;

    .line 32
    new-instance v0, Lcom/sec/ims/ft/FtFailReason;

    const-string v1, "ENGINE_ERROR"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/ft/FtFailReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/ft/FtFailReason;->ENGINE_ERROR:Lcom/sec/ims/ft/FtFailReason;

    .line 33
    new-instance v0, Lcom/sec/ims/ft/FtFailReason;

    const-string v1, "CONTINUED_ON_ANOTHER_DEVICE"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/ft/FtFailReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/ft/FtFailReason;->CONTINUED_ON_ANOTHER_DEVICE:Lcom/sec/ims/ft/FtFailReason;

    .line 34
    new-instance v0, Lcom/sec/ims/ft/FtFailReason;

    const-string v1, "FORBIDDEN_NO_RETRY_FALLBACK"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/ft/FtFailReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/ft/FtFailReason;->FORBIDDEN_NO_RETRY_FALLBACK:Lcom/sec/ims/ft/FtFailReason;

    .line 35
    new-instance v0, Lcom/sec/ims/ft/FtFailReason;

    const-string v1, "DEVICE_UNREGISTERED"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/ft/FtFailReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/ft/FtFailReason;->DEVICE_UNREGISTERED:Lcom/sec/ims/ft/FtFailReason;

    .line 36
    new-instance v0, Lcom/sec/ims/ft/FtFailReason;

    const-string v1, "CONTENT_REACHED_DOWNSIZE"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/ft/FtFailReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/ft/FtFailReason;->CONTENT_REACHED_DOWNSIZE:Lcom/sec/ims/ft/FtFailReason;

    .line 37
    new-instance v0, Lcom/sec/ims/ft/FtFailReason;

    const-string v1, "CANCELED_BY_LOCAL"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/ft/FtFailReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/ft/FtFailReason;->CANCELED_BY_LOCAL:Lcom/sec/ims/ft/FtFailReason;

    .line 38
    new-instance v0, Lcom/sec/ims/ft/FtFailReason;

    const-string v1, "REMOTE_TEMPORARILY_UNAVAILABLE"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/ft/FtFailReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/ft/FtFailReason;->REMOTE_TEMPORARILY_UNAVAILABLE:Lcom/sec/ims/ft/FtFailReason;

    .line 39
    new-instance v0, Lcom/sec/ims/ft/FtFailReason;

    const-string v1, "ALTERNATE_SERVICE"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/ft/FtFailReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/ft/FtFailReason;->ALTERNATE_SERVICE:Lcom/sec/ims/ft/FtFailReason;

    .line 40
    new-instance v0, Lcom/sec/ims/ft/FtFailReason;

    const-string v1, "METHOD_NOT_ALLOWED"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/ft/FtFailReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/ft/FtFailReason;->METHOD_NOT_ALLOWED:Lcom/sec/ims/ft/FtFailReason;

    .line 41
    new-instance v0, Lcom/sec/ims/ft/FtFailReason;

    const-string v1, "NOT_ACCEPTABLE"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/ft/FtFailReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/ft/FtFailReason;->NOT_ACCEPTABLE:Lcom/sec/ims/ft/FtFailReason;

    .line 42
    new-instance v0, Lcom/sec/ims/ft/FtFailReason;

    const-string v1, "GONE"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/ft/FtFailReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/ft/FtFailReason;->GONE:Lcom/sec/ims/ft/FtFailReason;

    .line 43
    new-instance v0, Lcom/sec/ims/ft/FtFailReason;

    const-string v1, "REQEUST_ENTITY_TOO_LARGE"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/ft/FtFailReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/ft/FtFailReason;->REQEUST_ENTITY_TOO_LARGE:Lcom/sec/ims/ft/FtFailReason;

    .line 44
    new-instance v0, Lcom/sec/ims/ft/FtFailReason;

    const-string v1, "REQUEST_URI_TOO_LARGE"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/ft/FtFailReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/ft/FtFailReason;->REQUEST_URI_TOO_LARGE:Lcom/sec/ims/ft/FtFailReason;

    .line 45
    new-instance v0, Lcom/sec/ims/ft/FtFailReason;

    const-string v1, "UNSUPPORTED_MEDIA_TYPE"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/ft/FtFailReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/ft/FtFailReason;->UNSUPPORTED_MEDIA_TYPE:Lcom/sec/ims/ft/FtFailReason;

    .line 46
    new-instance v0, Lcom/sec/ims/ft/FtFailReason;

    const-string v1, "UNSUPPORTED_URI_SCHEME"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/ft/FtFailReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/ft/FtFailReason;->UNSUPPORTED_URI_SCHEME:Lcom/sec/ims/ft/FtFailReason;

    .line 47
    new-instance v0, Lcom/sec/ims/ft/FtFailReason;

    const-string v1, "BUSY_EVERYWHERE"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/ft/FtFailReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/ft/FtFailReason;->BUSY_EVERYWHERE:Lcom/sec/ims/ft/FtFailReason;

    .line 48
    new-instance v0, Lcom/sec/ims/ft/FtFailReason;

    const-string v1, "NOTEXIST_ANYWHERE"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/ft/FtFailReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/ft/FtFailReason;->NOTEXIST_ANYWHERE:Lcom/sec/ims/ft/FtFailReason;

    .line 49
    new-instance v0, Lcom/sec/ims/ft/FtFailReason;

    const-string v1, "SERVER_NOT_ACCEPTABLE"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/ft/FtFailReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/ft/FtFailReason;->SERVER_NOT_ACCEPTABLE:Lcom/sec/ims/ft/FtFailReason;

    .line 50
    new-instance v0, Lcom/sec/ims/ft/FtFailReason;

    const-string v1, "NOT_IMPLEMENTED"

    const/16 v2, 0x22

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/ft/FtFailReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/ft/FtFailReason;->NOT_IMPLEMENTED:Lcom/sec/ims/ft/FtFailReason;

    .line 51
    new-instance v0, Lcom/sec/ims/ft/FtFailReason;

    const-string v1, "BAD_GATEWAY"

    const/16 v2, 0x23

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/ft/FtFailReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/ft/FtFailReason;->BAD_GATEWAY:Lcom/sec/ims/ft/FtFailReason;

    .line 52
    new-instance v0, Lcom/sec/ims/ft/FtFailReason;

    const-string v1, "SESSION_RSRC_UNAVAILABLE"

    const/16 v2, 0x24

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/ft/FtFailReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/ft/FtFailReason;->SESSION_RSRC_UNAVAILABLE:Lcom/sec/ims/ft/FtFailReason;

    .line 53
    new-instance v0, Lcom/sec/ims/ft/FtFailReason;

    const-string v1, "SERVER_TIMEOUT"

    const/16 v2, 0x25

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/ft/FtFailReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/ft/FtFailReason;->SERVER_TIMEOUT:Lcom/sec/ims/ft/FtFailReason;

    .line 54
    new-instance v0, Lcom/sec/ims/ft/FtFailReason;

    const-string v1, "SIP_VERSION_NOT_SUPPORTED"

    const/16 v2, 0x26

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/ft/FtFailReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/ft/FtFailReason;->SIP_VERSION_NOT_SUPPORTED:Lcom/sec/ims/ft/FtFailReason;

    .line 55
    new-instance v0, Lcom/sec/ims/ft/FtFailReason;

    const-string v1, "MESSAGE_TOO_LARGE"

    const/16 v2, 0x27

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/ft/FtFailReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/ft/FtFailReason;->MESSAGE_TOO_LARGE:Lcom/sec/ims/ft/FtFailReason;

    .line 56
    new-instance v0, Lcom/sec/ims/ft/FtFailReason;

    const-string v1, "SESSION_DOESNT_EXIST"

    const/16 v2, 0x28

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/ft/FtFailReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/ft/FtFailReason;->SESSION_DOESNT_EXIST:Lcom/sec/ims/ft/FtFailReason;

    .line 57
    new-instance v0, Lcom/sec/ims/ft/FtFailReason;

    const-string v1, "NO_RESPONSE"

    const/16 v2, 0x29

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/ft/FtFailReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/ft/FtFailReason;->NO_RESPONSE:Lcom/sec/ims/ft/FtFailReason;

    .line 58
    new-instance v0, Lcom/sec/ims/ft/FtFailReason;

    const-string v1, "CONNECTION_RELEASED"

    const/16 v2, 0x2a

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/ft/FtFailReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/ft/FtFailReason;->CONNECTION_RELEASED:Lcom/sec/ims/ft/FtFailReason;

    .line 59
    new-instance v0, Lcom/sec/ims/ft/FtFailReason;

    const-string v1, "LOOP_DETECTED"

    const/16 v2, 0x2b

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/ft/FtFailReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/ft/FtFailReason;->LOOP_DETECTED:Lcom/sec/ims/ft/FtFailReason;

    .line 60
    new-instance v0, Lcom/sec/ims/ft/FtFailReason;

    const-string v1, "TOO_MANY_HOPS"

    const/16 v2, 0x2c

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/ft/FtFailReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/ft/FtFailReason;->TOO_MANY_HOPS:Lcom/sec/ims/ft/FtFailReason;

    .line 61
    new-instance v0, Lcom/sec/ims/ft/FtFailReason;

    const-string v1, "ADDRESS_INCOMPLETE"

    const/16 v2, 0x2d

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/ft/FtFailReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/ft/FtFailReason;->ADDRESS_INCOMPLETE:Lcom/sec/ims/ft/FtFailReason;

    .line 62
    new-instance v0, Lcom/sec/ims/ft/FtFailReason;

    const-string v1, "AMBIGUOUS"

    const/16 v2, 0x2e

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/ft/FtFailReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/ft/FtFailReason;->AMBIGUOUS:Lcom/sec/ims/ft/FtFailReason;

    .line 63
    new-instance v0, Lcom/sec/ims/ft/FtFailReason;

    const-string v1, "REQUEST_PENDING"

    const/16 v2, 0x2f

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/ft/FtFailReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/ft/FtFailReason;->REQUEST_PENDING:Lcom/sec/ims/ft/FtFailReason;

    .line 64
    new-instance v0, Lcom/sec/ims/ft/FtFailReason;

    const-string v1, "UNDECEIPHERABLE"

    const/16 v2, 0x30

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/ft/FtFailReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/ft/FtFailReason;->UNDECEIPHERABLE:Lcom/sec/ims/ft/FtFailReason;

    .line 65
    new-instance v0, Lcom/sec/ims/ft/FtFailReason;

    const-string v1, "MULTIPLE_CHOICES"

    const/16 v2, 0x31

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/ft/FtFailReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/ft/FtFailReason;->MULTIPLE_CHOICES:Lcom/sec/ims/ft/FtFailReason;

    .line 66
    new-instance v0, Lcom/sec/ims/ft/FtFailReason;

    const-string v1, "MOVED_PERMANENTLY"

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/ft/FtFailReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/ft/FtFailReason;->MOVED_PERMANENTLY:Lcom/sec/ims/ft/FtFailReason;

    .line 67
    new-instance v0, Lcom/sec/ims/ft/FtFailReason;

    const-string v1, "MOVED_TEMPORARILY"

    const/16 v2, 0x33

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/ft/FtFailReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/ft/FtFailReason;->MOVED_TEMPORARILY:Lcom/sec/ims/ft/FtFailReason;

    .line 68
    new-instance v0, Lcom/sec/ims/ft/FtFailReason;

    const-string v1, "USE_PROXY"

    const/16 v2, 0x34

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/ft/FtFailReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/ft/FtFailReason;->USE_PROXY:Lcom/sec/ims/ft/FtFailReason;

    .line 69
    new-instance v0, Lcom/sec/ims/ft/FtFailReason;

    const-string v1, "BAD_EXTENSION"

    const/16 v2, 0x35

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/ft/FtFailReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/ft/FtFailReason;->BAD_EXTENSION:Lcom/sec/ims/ft/FtFailReason;

    .line 70
    new-instance v0, Lcom/sec/ims/ft/FtFailReason;

    const-string v1, "EXTENSION_REQUIRED"

    const/16 v2, 0x36

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/ft/FtFailReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/ft/FtFailReason;->EXTENSION_REQUIRED:Lcom/sec/ims/ft/FtFailReason;

    .line 71
    new-instance v0, Lcom/sec/ims/ft/FtFailReason;

    const-string v1, "SECURITY_AGREEMENT_REQD"

    const/16 v2, 0x37

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/ft/FtFailReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/ft/FtFailReason;->SECURITY_AGREEMENT_REQD:Lcom/sec/ims/ft/FtFailReason;

    .line 72
    new-instance v0, Lcom/sec/ims/ft/FtFailReason;

    const-string v1, "INTERNAL_SERVER_ERROR"

    const/16 v2, 0x38

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/ft/FtFailReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/ft/FtFailReason;->INTERNAL_SERVER_ERROR:Lcom/sec/ims/ft/FtFailReason;

    .line 73
    new-instance v0, Lcom/sec/ims/ft/FtFailReason;

    const-string v1, "SERVICE_UNAVAILABLE"

    const/16 v2, 0x39

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/ft/FtFailReason;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/ft/FtFailReason;->SERVICE_UNAVAILABLE:Lcom/sec/ims/ft/FtFailReason;

    .line 15
    const/16 v0, 0x3a

    new-array v0, v0, [Lcom/sec/ims/ft/FtFailReason;

    sget-object v1, Lcom/sec/ims/ft/FtFailReason;->SUCCESS:Lcom/sec/ims/ft/FtFailReason;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/ims/ft/FtFailReason;->DEDICATED_BEARER_AVAILABLE:Lcom/sec/ims/ft/FtFailReason;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/ims/ft/FtFailReason;->DEDICATED_BEARER_UNAVAILABLE:Lcom/sec/ims/ft/FtFailReason;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/ims/ft/FtFailReason;->DEDICATED_BEARER_FALLBACK:Lcom/sec/ims/ft/FtFailReason;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/ims/ft/FtFailReason;->REMOTE_USER_INVALID:Lcom/sec/ims/ft/FtFailReason;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/ims/ft/FtFailReason;->NOT_AUTHORIZED:Lcom/sec/ims/ft/FtFailReason;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/ims/ft/FtFailReason;->SESSION_TIMED_OUT:Lcom/sec/ims/ft/FtFailReason;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/ims/ft/FtFailReason;->REMOTE_PARTY_REJECTED:Lcom/sec/ims/ft/FtFailReason;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/ims/ft/FtFailReason;->REMOTE_PARTY_DECLINED:Lcom/sec/ims/ft/FtFailReason;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/ims/ft/FtFailReason;->REMOTE_PARTY_CANCELED:Lcom/sec/ims/ft/FtFailReason;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/ims/ft/FtFailReason;->REMOTE_PARTY_CLOSED:Lcom/sec/ims/ft/FtFailReason;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/sec/ims/ft/FtFailReason;->INVALID_REQUEST:Lcom/sec/ims/ft/FtFailReason;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/sec/ims/ft/FtFailReason;->FILE_IO_ERROR:Lcom/sec/ims/ft/FtFailReason;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/sec/ims/ft/FtFailReason;->NETWORK_ERROR:Lcom/sec/ims/ft/FtFailReason;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/sec/ims/ft/FtFailReason;->MAX_SIZE_EXCEEDED:Lcom/sec/ims/ft/FtFailReason;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/sec/ims/ft/FtFailReason;->COMPLETE_DATA_RECEIVED:Lcom/sec/ims/ft/FtFailReason;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/sec/ims/ft/FtFailReason;->ENGINE_ERROR:Lcom/sec/ims/ft/FtFailReason;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/sec/ims/ft/FtFailReason;->CONTINUED_ON_ANOTHER_DEVICE:Lcom/sec/ims/ft/FtFailReason;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/sec/ims/ft/FtFailReason;->FORBIDDEN_NO_RETRY_FALLBACK:Lcom/sec/ims/ft/FtFailReason;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/sec/ims/ft/FtFailReason;->DEVICE_UNREGISTERED:Lcom/sec/ims/ft/FtFailReason;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/sec/ims/ft/FtFailReason;->CONTENT_REACHED_DOWNSIZE:Lcom/sec/ims/ft/FtFailReason;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/sec/ims/ft/FtFailReason;->CANCELED_BY_LOCAL:Lcom/sec/ims/ft/FtFailReason;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/sec/ims/ft/FtFailReason;->REMOTE_TEMPORARILY_UNAVAILABLE:Lcom/sec/ims/ft/FtFailReason;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/sec/ims/ft/FtFailReason;->ALTERNATE_SERVICE:Lcom/sec/ims/ft/FtFailReason;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/sec/ims/ft/FtFailReason;->METHOD_NOT_ALLOWED:Lcom/sec/ims/ft/FtFailReason;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/sec/ims/ft/FtFailReason;->NOT_ACCEPTABLE:Lcom/sec/ims/ft/FtFailReason;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/sec/ims/ft/FtFailReason;->GONE:Lcom/sec/ims/ft/FtFailReason;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/sec/ims/ft/FtFailReason;->REQEUST_ENTITY_TOO_LARGE:Lcom/sec/ims/ft/FtFailReason;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/sec/ims/ft/FtFailReason;->REQUEST_URI_TOO_LARGE:Lcom/sec/ims/ft/FtFailReason;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/sec/ims/ft/FtFailReason;->UNSUPPORTED_MEDIA_TYPE:Lcom/sec/ims/ft/FtFailReason;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/sec/ims/ft/FtFailReason;->UNSUPPORTED_URI_SCHEME:Lcom/sec/ims/ft/FtFailReason;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/sec/ims/ft/FtFailReason;->BUSY_EVERYWHERE:Lcom/sec/ims/ft/FtFailReason;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/sec/ims/ft/FtFailReason;->NOTEXIST_ANYWHERE:Lcom/sec/ims/ft/FtFailReason;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/sec/ims/ft/FtFailReason;->SERVER_NOT_ACCEPTABLE:Lcom/sec/ims/ft/FtFailReason;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/sec/ims/ft/FtFailReason;->NOT_IMPLEMENTED:Lcom/sec/ims/ft/FtFailReason;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/sec/ims/ft/FtFailReason;->BAD_GATEWAY:Lcom/sec/ims/ft/FtFailReason;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/sec/ims/ft/FtFailReason;->SESSION_RSRC_UNAVAILABLE:Lcom/sec/ims/ft/FtFailReason;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/sec/ims/ft/FtFailReason;->SERVER_TIMEOUT:Lcom/sec/ims/ft/FtFailReason;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/sec/ims/ft/FtFailReason;->SIP_VERSION_NOT_SUPPORTED:Lcom/sec/ims/ft/FtFailReason;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/sec/ims/ft/FtFailReason;->MESSAGE_TOO_LARGE:Lcom/sec/ims/ft/FtFailReason;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/sec/ims/ft/FtFailReason;->SESSION_DOESNT_EXIST:Lcom/sec/ims/ft/FtFailReason;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/sec/ims/ft/FtFailReason;->NO_RESPONSE:Lcom/sec/ims/ft/FtFailReason;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/sec/ims/ft/FtFailReason;->CONNECTION_RELEASED:Lcom/sec/ims/ft/FtFailReason;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/sec/ims/ft/FtFailReason;->LOOP_DETECTED:Lcom/sec/ims/ft/FtFailReason;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/sec/ims/ft/FtFailReason;->TOO_MANY_HOPS:Lcom/sec/ims/ft/FtFailReason;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/sec/ims/ft/FtFailReason;->ADDRESS_INCOMPLETE:Lcom/sec/ims/ft/FtFailReason;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lcom/sec/ims/ft/FtFailReason;->AMBIGUOUS:Lcom/sec/ims/ft/FtFailReason;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Lcom/sec/ims/ft/FtFailReason;->REQUEST_PENDING:Lcom/sec/ims/ft/FtFailReason;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Lcom/sec/ims/ft/FtFailReason;->UNDECEIPHERABLE:Lcom/sec/ims/ft/FtFailReason;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Lcom/sec/ims/ft/FtFailReason;->MULTIPLE_CHOICES:Lcom/sec/ims/ft/FtFailReason;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, Lcom/sec/ims/ft/FtFailReason;->MOVED_PERMANENTLY:Lcom/sec/ims/ft/FtFailReason;

    aput-object v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, Lcom/sec/ims/ft/FtFailReason;->MOVED_TEMPORARILY:Lcom/sec/ims/ft/FtFailReason;

    aput-object v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, Lcom/sec/ims/ft/FtFailReason;->USE_PROXY:Lcom/sec/ims/ft/FtFailReason;

    aput-object v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, Lcom/sec/ims/ft/FtFailReason;->BAD_EXTENSION:Lcom/sec/ims/ft/FtFailReason;

    aput-object v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, Lcom/sec/ims/ft/FtFailReason;->EXTENSION_REQUIRED:Lcom/sec/ims/ft/FtFailReason;

    aput-object v2, v0, v1

    const/16 v1, 0x37

    sget-object v2, Lcom/sec/ims/ft/FtFailReason;->SECURITY_AGREEMENT_REQD:Lcom/sec/ims/ft/FtFailReason;

    aput-object v2, v0, v1

    const/16 v1, 0x38

    sget-object v2, Lcom/sec/ims/ft/FtFailReason;->INTERNAL_SERVER_ERROR:Lcom/sec/ims/ft/FtFailReason;

    aput-object v2, v0, v1

    const/16 v1, 0x39

    sget-object v2, Lcom/sec/ims/ft/FtFailReason;->SERVICE_UNAVAILABLE:Lcom/sec/ims/ft/FtFailReason;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/ims/ft/FtFailReason;->$VALUES:[Lcom/sec/ims/ft/FtFailReason;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 15
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/ims/ft/FtFailReason;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 15
    const-class v0, Lcom/sec/ims/ft/FtFailReason;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/ims/ft/FtFailReason;

    return-object v0
.end method

.method public static values()[Lcom/sec/ims/ft/FtFailReason;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/sec/ims/ft/FtFailReason;->$VALUES:[Lcom/sec/ims/ft/FtFailReason;

    invoke-virtual {v0}, [Lcom/sec/ims/ft/FtFailReason;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/ims/ft/FtFailReason;

    return-object v0
.end method
