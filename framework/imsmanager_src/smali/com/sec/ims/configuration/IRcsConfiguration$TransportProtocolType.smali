.class public final enum Lcom/sec/ims/configuration/IRcsConfiguration$TransportProtocolType;
.super Ljava/lang/Enum;
.source "IRcsConfiguration.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/ims/configuration/IRcsConfiguration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TransportProtocolType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/ims/configuration/IRcsConfiguration$TransportProtocolType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/ims/configuration/IRcsConfiguration$TransportProtocolType;

.field public static final enum SIPoTCP:Lcom/sec/ims/configuration/IRcsConfiguration$TransportProtocolType;

.field public static final enum SIPoTLS:Lcom/sec/ims/configuration/IRcsConfiguration$TransportProtocolType;

.field public static final enum SIPoUDP:Lcom/sec/ims/configuration/IRcsConfiguration$TransportProtocolType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 132
    new-instance v0, Lcom/sec/ims/configuration/IRcsConfiguration$TransportProtocolType;

    const-string v1, "SIPoUDP"

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/configuration/IRcsConfiguration$TransportProtocolType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/configuration/IRcsConfiguration$TransportProtocolType;->SIPoUDP:Lcom/sec/ims/configuration/IRcsConfiguration$TransportProtocolType;

    .line 133
    new-instance v0, Lcom/sec/ims/configuration/IRcsConfiguration$TransportProtocolType;

    const-string v1, "SIPoTCP"

    invoke-direct {v0, v1, v3}, Lcom/sec/ims/configuration/IRcsConfiguration$TransportProtocolType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/configuration/IRcsConfiguration$TransportProtocolType;->SIPoTCP:Lcom/sec/ims/configuration/IRcsConfiguration$TransportProtocolType;

    .line 134
    new-instance v0, Lcom/sec/ims/configuration/IRcsConfiguration$TransportProtocolType;

    const-string v1, "SIPoTLS"

    invoke-direct {v0, v1, v4}, Lcom/sec/ims/configuration/IRcsConfiguration$TransportProtocolType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/configuration/IRcsConfiguration$TransportProtocolType;->SIPoTLS:Lcom/sec/ims/configuration/IRcsConfiguration$TransportProtocolType;

    .line 131
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/ims/configuration/IRcsConfiguration$TransportProtocolType;

    sget-object v1, Lcom/sec/ims/configuration/IRcsConfiguration$TransportProtocolType;->SIPoUDP:Lcom/sec/ims/configuration/IRcsConfiguration$TransportProtocolType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/ims/configuration/IRcsConfiguration$TransportProtocolType;->SIPoTCP:Lcom/sec/ims/configuration/IRcsConfiguration$TransportProtocolType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/ims/configuration/IRcsConfiguration$TransportProtocolType;->SIPoTLS:Lcom/sec/ims/configuration/IRcsConfiguration$TransportProtocolType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/ims/configuration/IRcsConfiguration$TransportProtocolType;->$VALUES:[Lcom/sec/ims/configuration/IRcsConfiguration$TransportProtocolType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 131
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/ims/configuration/IRcsConfiguration$TransportProtocolType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 131
    const-class v0, Lcom/sec/ims/configuration/IRcsConfiguration$TransportProtocolType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/ims/configuration/IRcsConfiguration$TransportProtocolType;

    return-object v0
.end method

.method public static values()[Lcom/sec/ims/configuration/IRcsConfiguration$TransportProtocolType;
    .locals 1

    .prologue
    .line 131
    sget-object v0, Lcom/sec/ims/configuration/IRcsConfiguration$TransportProtocolType;->$VALUES:[Lcom/sec/ims/configuration/IRcsConfiguration$TransportProtocolType;

    invoke-virtual {v0}, [Lcom/sec/ims/configuration/IRcsConfiguration$TransportProtocolType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/ims/configuration/IRcsConfiguration$TransportProtocolType;

    return-object v0
.end method
