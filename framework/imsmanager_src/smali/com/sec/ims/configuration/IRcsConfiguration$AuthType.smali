.class public final enum Lcom/sec/ims/configuration/IRcsConfiguration$AuthType;
.super Ljava/lang/Enum;
.source "IRcsConfiguration.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/ims/configuration/IRcsConfiguration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AuthType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/ims/configuration/IRcsConfiguration$AuthType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/ims/configuration/IRcsConfiguration$AuthType;

.field public static final enum AKA:Lcom/sec/ims/configuration/IRcsConfiguration$AuthType;

.field public static final enum DIGEST:Lcom/sec/ims/configuration/IRcsConfiguration$AuthType;

.field public static final enum EARLY_IMS:Lcom/sec/ims/configuration/IRcsConfiguration$AuthType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 121
    new-instance v0, Lcom/sec/ims/configuration/IRcsConfiguration$AuthType;

    const-string v1, "EARLY_IMS"

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/configuration/IRcsConfiguration$AuthType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/configuration/IRcsConfiguration$AuthType;->EARLY_IMS:Lcom/sec/ims/configuration/IRcsConfiguration$AuthType;

    .line 122
    new-instance v0, Lcom/sec/ims/configuration/IRcsConfiguration$AuthType;

    const-string v1, "AKA"

    invoke-direct {v0, v1, v3}, Lcom/sec/ims/configuration/IRcsConfiguration$AuthType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/configuration/IRcsConfiguration$AuthType;->AKA:Lcom/sec/ims/configuration/IRcsConfiguration$AuthType;

    .line 123
    new-instance v0, Lcom/sec/ims/configuration/IRcsConfiguration$AuthType;

    const-string v1, "DIGEST"

    invoke-direct {v0, v1, v4}, Lcom/sec/ims/configuration/IRcsConfiguration$AuthType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/configuration/IRcsConfiguration$AuthType;->DIGEST:Lcom/sec/ims/configuration/IRcsConfiguration$AuthType;

    .line 120
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/ims/configuration/IRcsConfiguration$AuthType;

    sget-object v1, Lcom/sec/ims/configuration/IRcsConfiguration$AuthType;->EARLY_IMS:Lcom/sec/ims/configuration/IRcsConfiguration$AuthType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/ims/configuration/IRcsConfiguration$AuthType;->AKA:Lcom/sec/ims/configuration/IRcsConfiguration$AuthType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/ims/configuration/IRcsConfiguration$AuthType;->DIGEST:Lcom/sec/ims/configuration/IRcsConfiguration$AuthType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/ims/configuration/IRcsConfiguration$AuthType;->$VALUES:[Lcom/sec/ims/configuration/IRcsConfiguration$AuthType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 120
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/ims/configuration/IRcsConfiguration$AuthType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 120
    const-class v0, Lcom/sec/ims/configuration/IRcsConfiguration$AuthType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/ims/configuration/IRcsConfiguration$AuthType;

    return-object v0
.end method

.method public static values()[Lcom/sec/ims/configuration/IRcsConfiguration$AuthType;
    .locals 1

    .prologue
    .line 120
    sget-object v0, Lcom/sec/ims/configuration/IRcsConfiguration$AuthType;->$VALUES:[Lcom/sec/ims/configuration/IRcsConfiguration$AuthType;

    invoke-virtual {v0}, [Lcom/sec/ims/configuration/IRcsConfiguration$AuthType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/ims/configuration/IRcsConfiguration$AuthType;

    return-object v0
.end method
