.class public interface abstract Lcom/sec/ims/configuration/IRcsRegistrationSettings;
.super Ljava/lang/Object;
.source "IRcsRegistrationSettings.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/ims/configuration/IRcsRegistrationSettings$INonMobileDisabledRegistrationEvent;,
        Lcom/sec/ims/configuration/IRcsRegistrationSettings$IImsApnHomeNetworkDisabledRegistrationEvent;,
        Lcom/sec/ims/configuration/IRcsRegistrationSettings$IHomeNetworkDisabledRegistrationEvent;,
        Lcom/sec/ims/configuration/IRcsRegistrationSettings$IImsApnRoamingDisabledRegistrationEvent;,
        Lcom/sec/ims/configuration/IRcsRegistrationSettings$IRoamingDisabledRegistrationEvent;
    }
.end annotation


# virtual methods
.method public abstract setPreferImsApn(Z)V
.end method

.method public abstract setRcsRegistrationInImsApnHomeNetwork(Z)V
.end method

.method public abstract setRcsRegistrationInImsApnRoaming(Z)V
.end method

.method public abstract setRcsRegistrationInMobileHomeNetwork(Z)V
.end method

.method public abstract setRcsRegistrationInMobileRoaming(Z)V
.end method

.method public abstract setRcsRegistrationInNonMobileNetwork(Z)V
.end method
