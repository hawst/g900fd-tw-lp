.class public final enum Lcom/sec/ims/configuration/IRcsConfiguration$MediaProtocolType;
.super Ljava/lang/Enum;
.source "IRcsConfiguration.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/ims/configuration/IRcsConfiguration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MediaProtocolType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/ims/configuration/IRcsConfiguration$MediaProtocolType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/ims/configuration/IRcsConfiguration$MediaProtocolType;

.field public static final enum MSRP:Lcom/sec/ims/configuration/IRcsConfiguration$MediaProtocolType;

.field public static final enum MSRPoTLS:Lcom/sec/ims/configuration/IRcsConfiguration$MediaProtocolType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 127
    new-instance v0, Lcom/sec/ims/configuration/IRcsConfiguration$MediaProtocolType;

    const-string v1, "MSRP"

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/configuration/IRcsConfiguration$MediaProtocolType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/configuration/IRcsConfiguration$MediaProtocolType;->MSRP:Lcom/sec/ims/configuration/IRcsConfiguration$MediaProtocolType;

    .line 128
    new-instance v0, Lcom/sec/ims/configuration/IRcsConfiguration$MediaProtocolType;

    const-string v1, "MSRPoTLS"

    invoke-direct {v0, v1, v3}, Lcom/sec/ims/configuration/IRcsConfiguration$MediaProtocolType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/configuration/IRcsConfiguration$MediaProtocolType;->MSRPoTLS:Lcom/sec/ims/configuration/IRcsConfiguration$MediaProtocolType;

    .line 126
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/ims/configuration/IRcsConfiguration$MediaProtocolType;

    sget-object v1, Lcom/sec/ims/configuration/IRcsConfiguration$MediaProtocolType;->MSRP:Lcom/sec/ims/configuration/IRcsConfiguration$MediaProtocolType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/ims/configuration/IRcsConfiguration$MediaProtocolType;->MSRPoTLS:Lcom/sec/ims/configuration/IRcsConfiguration$MediaProtocolType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/ims/configuration/IRcsConfiguration$MediaProtocolType;->$VALUES:[Lcom/sec/ims/configuration/IRcsConfiguration$MediaProtocolType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 126
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/ims/configuration/IRcsConfiguration$MediaProtocolType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 126
    const-class v0, Lcom/sec/ims/configuration/IRcsConfiguration$MediaProtocolType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/ims/configuration/IRcsConfiguration$MediaProtocolType;

    return-object v0
.end method

.method public static values()[Lcom/sec/ims/configuration/IRcsConfiguration$MediaProtocolType;
    .locals 1

    .prologue
    .line 126
    sget-object v0, Lcom/sec/ims/configuration/IRcsConfiguration$MediaProtocolType;->$VALUES:[Lcom/sec/ims/configuration/IRcsConfiguration$MediaProtocolType;

    invoke-virtual {v0}, [Lcom/sec/ims/configuration/IRcsConfiguration$MediaProtocolType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/ims/configuration/IRcsConfiguration$MediaProtocolType;

    return-object v0
.end method
