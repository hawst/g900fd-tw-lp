.class public interface abstract Lcom/sec/ims/configuration/IRcsConfiguration;
.super Ljava/lang/Object;
.source "IRcsConfiguration.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/ims/configuration/IRcsConfiguration$LboPcscfAddressPair;,
        Lcom/sec/ims/configuration/IRcsConfiguration$TransportProtocolType;,
        Lcom/sec/ims/configuration/IRcsConfiguration$MediaProtocolType;,
        Lcom/sec/ims/configuration/IRcsConfiguration$AuthType;
    }
.end annotation


# virtual methods
.method public abstract getAuthType()Lcom/sec/ims/configuration/IRcsConfiguration$AuthType;
.end method

.method public abstract getCapInfoExpiry()Ljava/lang/Integer;
.end method

.method public abstract getConfFctyUri()Landroid/net/Uri;
.end method

.method public abstract getContentServerUri()Ljava/lang/String;
.end method

.method public abstract getDeferredMsgFuncUri()Landroid/net/Uri;
.end method

.method public abstract getExploderUri()Landroid/net/Uri;
.end method

.method public abstract getFtWarnSize()Ljava/lang/Long;
.end method

.method public abstract getHomeNetworkDomainName()Ljava/lang/String;
.end method

.method public abstract getLboPcscfAddressPairList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/ims/configuration/IRcsConfiguration$LboPcscfAddressPair;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getMaxAdHocGroupSize()Ljava/lang/Integer;
.end method

.method public abstract getMaxSizeFileTr()Ljava/lang/Long;
.end method

.method public abstract getMaxSizeImageShare()Ljava/lang/Long;
.end method

.method public abstract getMaxTimeVideoShare()Ljava/lang/Long;
.end method

.method public abstract getMcc()Ljava/lang/String;
.end method

.method public abstract getMnc()Ljava/lang/String;
.end method

.method public abstract getNoteMaxSize()Ljava/lang/Integer;
.end method

.method public abstract getPollingPeriod()I
.end method

.method public abstract getPrivateUserIdentity()Ljava/lang/String;
.end method

.method public abstract getPsMedia()Lcom/sec/ims/configuration/IRcsConfiguration$MediaProtocolType;
.end method

.method public abstract getPsSignalling()Lcom/sec/ims/configuration/IRcsConfiguration$TransportProtocolType;
.end method

.method public abstract getPuiList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getQValue()Ljava/lang/String;
.end method

.method public abstract getRealm()Landroid/net/Uri;
.end method

.method public abstract getTimerT1()Ljava/lang/Long;
.end method

.method public abstract getTimerT2()Ljava/lang/Long;
.end method

.method public abstract getTimerT4()Ljava/lang/Long;
.end method

.method public abstract getUserName()Ljava/lang/String;
.end method

.method public abstract getUserPwd()Ljava/lang/String;
.end method

.method public abstract getUuidValue()Ljava/lang/String;
.end method

.method public abstract getWarnSizeImageShare()Ljava/lang/Long;
.end method

.method public abstract getWiFiMedia()Lcom/sec/ims/configuration/IRcsConfiguration$MediaProtocolType;
.end method

.method public abstract getWiFiSignalling()Lcom/sec/ims/configuration/IRcsConfiguration$TransportProtocolType;
.end method

.method public abstract getXcapRootUri()Ljava/lang/String;
.end method

.method public abstract isFtHttpEnabled()Z
.end method

.method public abstract isImCapAlwaysOn()Ljava/lang/Boolean;
.end method

.method public abstract isImWarnSf()Ljava/lang/Boolean;
.end method

.method public abstract isPresenceDisc()Ljava/lang/Boolean;
.end method

.method public abstract isRcsEnabled()Z
.end method

.method public abstract isUsePresence()Z
.end method
