.class public Lcom/sec/ims/configuration/IRcsConfiguration$LboPcscfAddressPair;
.super Ljava/lang/Object;
.source "IRcsConfiguration.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/ims/configuration/IRcsConfiguration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LboPcscfAddressPair"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/ims/configuration/IRcsConfiguration$LboPcscfAddressPair$LboPcscfAddressType;
    }
.end annotation


# instance fields
.field private final mAddress:Ljava/lang/String;

.field private final mAddressType:Lcom/sec/ims/configuration/IRcsConfiguration$LboPcscfAddressPair$LboPcscfAddressType;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/sec/ims/configuration/IRcsConfiguration$LboPcscfAddressPair$LboPcscfAddressType;)V
    .locals 0
    .param p1, "address"    # Ljava/lang/String;
    .param p2, "addressType"    # Lcom/sec/ims/configuration/IRcsConfiguration$LboPcscfAddressPair$LboPcscfAddressType;

    .prologue
    .line 141
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 142
    iput-object p1, p0, Lcom/sec/ims/configuration/IRcsConfiguration$LboPcscfAddressPair;->mAddress:Ljava/lang/String;

    .line 143
    iput-object p2, p0, Lcom/sec/ims/configuration/IRcsConfiguration$LboPcscfAddressPair;->mAddressType:Lcom/sec/ims/configuration/IRcsConfiguration$LboPcscfAddressPair$LboPcscfAddressType;

    .line 144
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 161
    if-ne p0, p1, :cond_1

    .line 171
    :cond_0
    :goto_0
    return v1

    .line 165
    :cond_1
    instance-of v3, p1, Lcom/sec/ims/configuration/IRcsConfiguration$LboPcscfAddressPair;

    if-eqz v3, :cond_3

    move-object v0, p1

    .line 166
    check-cast v0, Lcom/sec/ims/configuration/IRcsConfiguration$LboPcscfAddressPair;

    .line 168
    .local v0, "other":Lcom/sec/ims/configuration/IRcsConfiguration$LboPcscfAddressPair;
    iget-object v3, p0, Lcom/sec/ims/configuration/IRcsConfiguration$LboPcscfAddressPair;->mAddress:Ljava/lang/String;

    iget-object v4, v0, Lcom/sec/ims/configuration/IRcsConfiguration$LboPcscfAddressPair;->mAddress:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/ims/configuration/IRcsConfiguration$LboPcscfAddressPair;->mAddressType:Lcom/sec/ims/configuration/IRcsConfiguration$LboPcscfAddressPair$LboPcscfAddressType;

    iget-object v4, v0, Lcom/sec/ims/configuration/IRcsConfiguration$LboPcscfAddressPair;->mAddressType:Lcom/sec/ims/configuration/IRcsConfiguration$LboPcscfAddressPair$LboPcscfAddressType;

    invoke-virtual {v3, v4}, Lcom/sec/ims/configuration/IRcsConfiguration$LboPcscfAddressPair$LboPcscfAddressType;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_2
    move v1, v2

    goto :goto_0

    .end local v0    # "other":Lcom/sec/ims/configuration/IRcsConfiguration$LboPcscfAddressPair;
    :cond_3
    move v1, v2

    .line 171
    goto :goto_0
.end method

.method public getAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/sec/ims/configuration/IRcsConfiguration$LboPcscfAddressPair;->mAddress:Ljava/lang/String;

    return-object v0
.end method

.method public getAddressType()Lcom/sec/ims/configuration/IRcsConfiguration$LboPcscfAddressPair$LboPcscfAddressType;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/sec/ims/configuration/IRcsConfiguration$LboPcscfAddressPair;->mAddressType:Lcom/sec/ims/configuration/IRcsConfiguration$LboPcscfAddressPair$LboPcscfAddressType;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 156
    iget-object v0, p0, Lcom/sec/ims/configuration/IRcsConfiguration$LboPcscfAddressPair;->mAddress:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    iget-object v1, p0, Lcom/sec/ims/configuration/IRcsConfiguration$LboPcscfAddressPair;->mAddressType:Lcom/sec/ims/configuration/IRcsConfiguration$LboPcscfAddressPair$LboPcscfAddressType;

    invoke-virtual {v1}, Lcom/sec/ims/configuration/IRcsConfiguration$LboPcscfAddressPair$LboPcscfAddressType;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
