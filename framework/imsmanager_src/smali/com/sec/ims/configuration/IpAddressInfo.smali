.class public Lcom/sec/ims/configuration/IpAddressInfo;
.super Ljava/lang/Object;
.source "IpAddressInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/ims/configuration/IpAddressInfo$IpAddressType;
    }
.end annotation


# instance fields
.field private final mIpAddress:Ljava/lang/String;

.field private final mIpAddressType:Lcom/sec/ims/configuration/IpAddressInfo$IpAddressType;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/sec/ims/configuration/IpAddressInfo$IpAddressType;)V
    .locals 0
    .param p1, "ipAddress"    # Ljava/lang/String;
    .param p2, "ipAddressType"    # Lcom/sec/ims/configuration/IpAddressInfo$IpAddressType;

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    invoke-static {p1}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    invoke-static {p2}, Lcom/android/internal/util/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    iput-object p1, p0, Lcom/sec/ims/configuration/IpAddressInfo;->mIpAddress:Ljava/lang/String;

    .line 34
    iput-object p2, p0, Lcom/sec/ims/configuration/IpAddressInfo;->mIpAddressType:Lcom/sec/ims/configuration/IpAddressInfo$IpAddressType;

    .line 35
    return-void
.end method


# virtual methods
.method public getIpAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/ims/configuration/IpAddressInfo;->mIpAddress:Ljava/lang/String;

    return-object v0
.end method

.method public getIpAddressType()Lcom/sec/ims/configuration/IpAddressInfo$IpAddressType;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/ims/configuration/IpAddressInfo;->mIpAddressType:Lcom/sec/ims/configuration/IpAddressInfo$IpAddressType;

    return-object v0
.end method

.method public getIsFqdn()Z
    .locals 2

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/ims/configuration/IpAddressInfo;->mIpAddressType:Lcom/sec/ims/configuration/IpAddressInfo$IpAddressType;

    sget-object v1, Lcom/sec/ims/configuration/IpAddressInfo$IpAddressType;->FQDN:Lcom/sec/ims/configuration/IpAddressInfo$IpAddressType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
