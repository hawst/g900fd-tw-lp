.class public interface abstract Lcom/sec/ims/configuration/IEnabledStateCondition;
.super Ljava/lang/Object;
.source "IEnabledStateCondition.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/ims/configuration/IEnabledStateCondition$IEnabledChangeEvent;
    }
.end annotation


# virtual methods
.method public abstract attach()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method public abstract detach()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method public abstract isEnabled()Z
.end method
