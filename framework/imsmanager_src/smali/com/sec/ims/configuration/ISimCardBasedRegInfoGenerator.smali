.class public interface abstract Lcom/sec/ims/configuration/ISimCardBasedRegInfoGenerator;
.super Ljava/lang/Object;
.source "ISimCardBasedRegInfoGenerator.java"


# static fields
.field public static final AUTH_TYPE_AKA:Ljava/lang/String; = "AKA"

.field public static final AUTH_TYPE_AKAv1:Ljava/lang/String; = "AKAv1-MD5"

.field public static final AUTH_TYPE_AKAv2:Ljava/lang/String; = "AKAv2-MD5"

.field public static final AUTH_TYPE_DIGEST:Ljava/lang/String; = "Digest"


# virtual methods
.method public abstract generateAuthType(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract generateHomeNetworkDomainName(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract generateImsiBasedPublicUri(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract generateSipPublicUri(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract generateTelPublicUri(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract generateXCAPDomainName(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract isImsiBasedUriCondition()Z
.end method
