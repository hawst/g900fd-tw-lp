.class public Lcom/sec/ims/sms/SmsEvent;
.super Ljava/lang/Object;
.source "SmsEvent.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/ims/sms/SmsEvent$State;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/ims/sms/SmsEvent;",
            ">;"
        }
    .end annotation
.end field

.field public static final SMSIP_CST_NOTI_INFO:I = 0xc

.field public static final SMSIP_CST_RECEIVED:I = 0xb


# instance fields
.field private mCallID:Ljava/lang/String;

.field private mContentType:Ljava/lang/String;

.field private mData:[B

.field private mEventType:I

.field private mMessageID:I

.field private mReasonCode:I

.field private mRemoteAddr:Ljava/lang/String;

.field private mRetryAfter:I

.field private mRpRef:I

.field private mState:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 184
    new-instance v0, Lcom/sec/ims/sms/SmsEvent$1;

    invoke-direct {v0}, Lcom/sec/ims/sms/SmsEvent$1;-><init>()V

    sput-object v0, Lcom/sec/ims/sms/SmsEvent;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object v1, p0, Lcom/sec/ims/sms/SmsEvent;->mData:[B

    .line 23
    iput v0, p0, Lcom/sec/ims/sms/SmsEvent;->mEventType:I

    .line 24
    iput v0, p0, Lcom/sec/ims/sms/SmsEvent;->mMessageID:I

    .line 25
    iput v0, p0, Lcom/sec/ims/sms/SmsEvent;->mReasonCode:I

    .line 26
    iput-object v1, p0, Lcom/sec/ims/sms/SmsEvent;->mContentType:Ljava/lang/String;

    .line 27
    iput-object v1, p0, Lcom/sec/ims/sms/SmsEvent;->mCallID:Ljava/lang/String;

    .line 28
    iput-object v1, p0, Lcom/sec/ims/sms/SmsEvent;->mRemoteAddr:Ljava/lang/String;

    .line 29
    iput v0, p0, Lcom/sec/ims/sms/SmsEvent;->mRetryAfter:I

    .line 31
    iput v2, p0, Lcom/sec/ims/sms/SmsEvent;->mState:I

    .line 32
    iput v0, p0, Lcom/sec/ims/sms/SmsEvent;->mRpRef:I

    .line 78
    invoke-virtual {p0, v2}, Lcom/sec/ims/sms/SmsEvent;->setState(I)V

    .line 79
    iput v0, p0, Lcom/sec/ims/sms/SmsEvent;->mRpRef:I

    .line 80
    return-void
.end method

.method public constructor <init>(IIII[BLjava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 2
    .param p1, "appType"    # I
    .param p2, "eventType"    # I
    .param p3, "messageID"    # I
    .param p4, "reasonCode"    # I
    .param p5, "data"    # [B
    .param p6, "contentType"    # Ljava/lang/String;
    .param p7, "callId"    # Ljava/lang/String;
    .param p8, "remoteAddr"    # Ljava/lang/String;
    .param p9, "retryAfter"    # I

    .prologue
    const/4 v0, 0x0

    const/4 v1, -0x1

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object v0, p0, Lcom/sec/ims/sms/SmsEvent;->mData:[B

    .line 23
    iput v1, p0, Lcom/sec/ims/sms/SmsEvent;->mEventType:I

    .line 24
    iput v1, p0, Lcom/sec/ims/sms/SmsEvent;->mMessageID:I

    .line 25
    iput v1, p0, Lcom/sec/ims/sms/SmsEvent;->mReasonCode:I

    .line 26
    iput-object v0, p0, Lcom/sec/ims/sms/SmsEvent;->mContentType:Ljava/lang/String;

    .line 27
    iput-object v0, p0, Lcom/sec/ims/sms/SmsEvent;->mCallID:Ljava/lang/String;

    .line 28
    iput-object v0, p0, Lcom/sec/ims/sms/SmsEvent;->mRemoteAddr:Ljava/lang/String;

    .line 29
    iput v1, p0, Lcom/sec/ims/sms/SmsEvent;->mRetryAfter:I

    .line 31
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/ims/sms/SmsEvent;->mState:I

    .line 32
    iput v1, p0, Lcom/sec/ims/sms/SmsEvent;->mRpRef:I

    .line 49
    iput-object p5, p0, Lcom/sec/ims/sms/SmsEvent;->mData:[B

    .line 50
    iput p2, p0, Lcom/sec/ims/sms/SmsEvent;->mEventType:I

    .line 51
    iput p3, p0, Lcom/sec/ims/sms/SmsEvent;->mMessageID:I

    .line 52
    iput p4, p0, Lcom/sec/ims/sms/SmsEvent;->mReasonCode:I

    .line 53
    iput-object p6, p0, Lcom/sec/ims/sms/SmsEvent;->mContentType:Ljava/lang/String;

    .line 54
    iput-object p7, p0, Lcom/sec/ims/sms/SmsEvent;->mCallID:Ljava/lang/String;

    .line 55
    iput-object p8, p0, Lcom/sec/ims/sms/SmsEvent;->mRemoteAddr:Ljava/lang/String;

    .line 56
    iput p9, p0, Lcom/sec/ims/sms/SmsEvent;->mRetryAfter:I

    .line 57
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 5
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object v4, p0, Lcom/sec/ims/sms/SmsEvent;->mData:[B

    .line 23
    iput v3, p0, Lcom/sec/ims/sms/SmsEvent;->mEventType:I

    .line 24
    iput v3, p0, Lcom/sec/ims/sms/SmsEvent;->mMessageID:I

    .line 25
    iput v3, p0, Lcom/sec/ims/sms/SmsEvent;->mReasonCode:I

    .line 26
    iput-object v4, p0, Lcom/sec/ims/sms/SmsEvent;->mContentType:Ljava/lang/String;

    .line 27
    iput-object v4, p0, Lcom/sec/ims/sms/SmsEvent;->mCallID:Ljava/lang/String;

    .line 28
    iput-object v4, p0, Lcom/sec/ims/sms/SmsEvent;->mRemoteAddr:Ljava/lang/String;

    .line 29
    iput v3, p0, Lcom/sec/ims/sms/SmsEvent;->mRetryAfter:I

    .line 31
    const/4 v2, 0x0

    iput v2, p0, Lcom/sec/ims/sms/SmsEvent;->mState:I

    .line 32
    iput v3, p0, Lcom/sec/ims/sms/SmsEvent;->mRpRef:I

    .line 60
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 61
    .local v0, "mDataLen":I
    if-lez v0, :cond_0

    .line 62
    new-array v1, v0, [B

    .line 63
    .local v1, "mDataTemp":[B
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readByteArray([B)V

    .line 64
    iput-object v1, p0, Lcom/sec/ims/sms/SmsEvent;->mData:[B

    .line 68
    .end local v1    # "mDataTemp":[B
    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iput v2, p0, Lcom/sec/ims/sms/SmsEvent;->mEventType:I

    .line 69
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iput v2, p0, Lcom/sec/ims/sms/SmsEvent;->mMessageID:I

    .line 70
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iput v2, p0, Lcom/sec/ims/sms/SmsEvent;->mReasonCode:I

    .line 71
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/ims/sms/SmsEvent;->mContentType:Ljava/lang/String;

    .line 72
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/ims/sms/SmsEvent;->mCallID:Ljava/lang/String;

    .line 73
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/ims/sms/SmsEvent;->mRemoteAddr:Ljava/lang/String;

    .line 74
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iput v2, p0, Lcom/sec/ims/sms/SmsEvent;->mRetryAfter:I

    .line 75
    return-void

    .line 66
    :cond_0
    iput-object v4, p0, Lcom/sec/ims/sms/SmsEvent;->mData:[B

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 164
    const/4 v0, 0x0

    return v0
.end method

.method public getCallID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/sec/ims/sms/SmsEvent;->mCallID:Ljava/lang/String;

    return-object v0
.end method

.method public getContentType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/sec/ims/sms/SmsEvent;->mContentType:Ljava/lang/String;

    return-object v0
.end method

.method public getData()[B
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/sec/ims/sms/SmsEvent;->mData:[B

    return-object v0
.end method

.method public getEventType()I
    .locals 1

    .prologue
    .line 95
    iget v0, p0, Lcom/sec/ims/sms/SmsEvent;->mEventType:I

    return v0
.end method

.method public getMessageID()I
    .locals 1

    .prologue
    .line 99
    iget v0, p0, Lcom/sec/ims/sms/SmsEvent;->mMessageID:I

    return v0
.end method

.method public getReasonCode()I
    .locals 1

    .prologue
    .line 103
    iget v0, p0, Lcom/sec/ims/sms/SmsEvent;->mReasonCode:I

    return v0
.end method

.method public getRemoteAddr()Ljava/lang/String;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/sec/ims/sms/SmsEvent;->mRemoteAddr:Ljava/lang/String;

    return-object v0
.end method

.method public getRetryAfter()I
    .locals 1

    .prologue
    .line 119
    iget v0, p0, Lcom/sec/ims/sms/SmsEvent;->mRetryAfter:I

    return v0
.end method

.method public getRpRef()I
    .locals 1

    .prologue
    .line 87
    iget v0, p0, Lcom/sec/ims/sms/SmsEvent;->mRpRef:I

    return v0
.end method

.method public getState()I
    .locals 1

    .prologue
    .line 83
    iget v0, p0, Lcom/sec/ims/sms/SmsEvent;->mState:I

    return v0
.end method

.method public setCallID(Ljava/lang/String;)V
    .locals 0
    .param p1, "callId"    # Ljava/lang/String;

    .prologue
    .line 151
    iput-object p1, p0, Lcom/sec/ims/sms/SmsEvent;->mCallID:Ljava/lang/String;

    .line 152
    return-void
.end method

.method public setContentType(Ljava/lang/String;)V
    .locals 0
    .param p1, "contentType"    # Ljava/lang/String;

    .prologue
    .line 147
    iput-object p1, p0, Lcom/sec/ims/sms/SmsEvent;->mContentType:Ljava/lang/String;

    .line 148
    return-void
.end method

.method public setData([B)V
    .locals 0
    .param p1, "data"    # [B

    .prologue
    .line 131
    iput-object p1, p0, Lcom/sec/ims/sms/SmsEvent;->mData:[B

    .line 132
    return-void
.end method

.method public setEventType(I)V
    .locals 0
    .param p1, "eventType"    # I

    .prologue
    .line 135
    iput p1, p0, Lcom/sec/ims/sms/SmsEvent;->mEventType:I

    .line 136
    return-void
.end method

.method public setMessageID(I)V
    .locals 0
    .param p1, "messageId"    # I

    .prologue
    .line 139
    iput p1, p0, Lcom/sec/ims/sms/SmsEvent;->mMessageID:I

    .line 140
    return-void
.end method

.method public setReasonCode(I)V
    .locals 0
    .param p1, "reasonCode"    # I

    .prologue
    .line 143
    iput p1, p0, Lcom/sec/ims/sms/SmsEvent;->mReasonCode:I

    .line 144
    return-void
.end method

.method public setRemoteAddr(Ljava/lang/String;)V
    .locals 0
    .param p1, "remoteAddr"    # Ljava/lang/String;

    .prologue
    .line 155
    iput-object p1, p0, Lcom/sec/ims/sms/SmsEvent;->mRemoteAddr:Ljava/lang/String;

    .line 156
    return-void
.end method

.method public setRetryAfter(I)V
    .locals 0
    .param p1, "retryAfter"    # I

    .prologue
    .line 159
    iput p1, p0, Lcom/sec/ims/sms/SmsEvent;->mRetryAfter:I

    .line 160
    return-void
.end method

.method public setRpRef(I)V
    .locals 0
    .param p1, "rpRef"    # I

    .prologue
    .line 127
    iput p1, p0, Lcom/sec/ims/sms/SmsEvent;->mRpRef:I

    .line 128
    return-void
.end method

.method public setState(I)V
    .locals 0
    .param p1, "state"    # I

    .prologue
    .line 123
    iput p1, p0, Lcom/sec/ims/sms/SmsEvent;->mState:I

    .line 124
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 198
    const-string v0, ""

    .line 200
    .local v0, "str":Ljava/lang/String;
    iget v1, p0, Lcom/sec/ims/sms/SmsEvent;->mState:I

    packed-switch v1, :pswitch_data_0

    .line 223
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "[NONE] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 226
    :goto_0
    iget-object v1, p0, Lcom/sec/ims/sms/SmsEvent;->mContentType:Ljava/lang/String;

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "contentType ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/ims/sms/SmsEvent;->mContentType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 227
    :cond_0
    iget v1, p0, Lcom/sec/ims/sms/SmsEvent;->mMessageID:I

    if-ltz v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "messageID ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/ims/sms/SmsEvent;->mMessageID:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 228
    :cond_1
    iget v1, p0, Lcom/sec/ims/sms/SmsEvent;->mRpRef:I

    if-ltz v1, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "rpRef ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/ims/sms/SmsEvent;->mRpRef:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 229
    :cond_2
    iget v1, p0, Lcom/sec/ims/sms/SmsEvent;->mReasonCode:I

    if-ltz v1, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "reasonCode ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/ims/sms/SmsEvent;->mReasonCode:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 230
    :cond_3
    iget-object v1, p0, Lcom/sec/ims/sms/SmsEvent;->mCallID:Ljava/lang/String;

    if-eqz v1, :cond_4

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "callID ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/ims/sms/SmsEvent;->mCallID:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 231
    :cond_4
    iget-object v1, p0, Lcom/sec/ims/sms/SmsEvent;->mRemoteAddr:Ljava/lang/String;

    if-eqz v1, :cond_5

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "remoteAddr ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/ims/sms/SmsEvent;->mRemoteAddr:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 232
    :cond_5
    return-object v0

    .line 202
    :pswitch_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "[OUTGOING] state MO_SENDING_START "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 203
    goto/16 :goto_0

    .line 205
    :pswitch_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "[OUTGOING] state MO_RECEIVING_CALLID "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 206
    goto/16 :goto_0

    .line 208
    :pswitch_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "[OUTGOING] state MO_RECEIVING_202_ACCEPTED "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 209
    goto/16 :goto_0

    .line 211
    :pswitch_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "[INCOMING] state MT_RECEIVING_STATUS_REPORT "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 212
    goto/16 :goto_0

    .line 214
    :pswitch_4
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "[INCOMING] state MT_RECEIVING_INCOMING_SMS "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 215
    goto/16 :goto_0

    .line 217
    :pswitch_5
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "[INCOMING] state MT_SENDING_DELIVER_REPORT "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 218
    goto/16 :goto_0

    .line 220
    :pswitch_6
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "[INCOMING] state MT_RECEIVING_DELIVER_REPORT_ACK "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 221
    goto/16 :goto_0

    .line 200
    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_3
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 169
    iget-object v0, p0, Lcom/sec/ims/sms/SmsEvent;->mData:[B

    if-eqz v0, :cond_0

    .line 170
    iget-object v0, p0, Lcom/sec/ims/sms/SmsEvent;->mData:[B

    array-length v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 171
    iget-object v0, p0, Lcom/sec/ims/sms/SmsEvent;->mData:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 175
    :goto_0
    iget v0, p0, Lcom/sec/ims/sms/SmsEvent;->mEventType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 176
    iget v0, p0, Lcom/sec/ims/sms/SmsEvent;->mMessageID:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 177
    iget v0, p0, Lcom/sec/ims/sms/SmsEvent;->mReasonCode:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 178
    iget-object v0, p0, Lcom/sec/ims/sms/SmsEvent;->mContentType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 179
    iget-object v0, p0, Lcom/sec/ims/sms/SmsEvent;->mCallID:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 180
    iget-object v0, p0, Lcom/sec/ims/sms/SmsEvent;->mRemoteAddr:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 181
    iget v0, p0, Lcom/sec/ims/sms/SmsEvent;->mRetryAfter:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 182
    return-void

    .line 173
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0
.end method
