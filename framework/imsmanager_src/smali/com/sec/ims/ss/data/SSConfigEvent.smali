.class public Lcom/sec/ims/ss/data/SSConfigEvent;
.super Ljava/lang/Object;
.source "SSConfigEvent.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;
    }
.end annotation


# instance fields
.field private mCBType:[I

.field private mCFType:[I

.field private mCFURI:[Ljava/lang/String;

.field private mCFURILength:[I

.field private mCFURIType:[I

.field private mData:[B

.field private mErrorMessage:Ljava/lang/String;

.field private mErrorMessages:[Ljava/lang/String;

.field private mEventType:I

.field private mNoReplyTime:[I

.field private mNumberOfClasses:I

.field private mResult:Z

.field private mSSClass:[I

.field private mSSStatus:[I

.field private mSessionID:I

.field private mState:Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;

.field private mSubEventType:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    sget-object v0, Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;->NOT_INITIALIZED:Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;

    iput-object v0, p0, Lcom/sec/ims/ss/data/SSConfigEvent;->mState:Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;

    .line 90
    return-void
.end method

.method public constructor <init>(I)V
    .locals 2
    .param p1, "eventType"    # I

    .prologue
    const/4 v1, -0x1

    .line 100
    const/4 v0, 0x0

    invoke-direct {p0, p1, v1, v1, v0}, Lcom/sec/ims/ss/data/SSConfigEvent;-><init>(III[B)V

    .line 102
    return-void
.end method

.method public constructor <init>(III[B)V
    .locals 1
    .param p1, "evntType"    # I
    .param p2, "sessionID"    # I
    .param p3, "subEventType"    # I
    .param p4, "data"    # [B

    .prologue
    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    sget-object v0, Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;->NOT_INITIALIZED:Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;

    iput-object v0, p0, Lcom/sec/ims/ss/data/SSConfigEvent;->mState:Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;

    .line 93
    iput p1, p0, Lcom/sec/ims/ss/data/SSConfigEvent;->mEventType:I

    .line 94
    iput p2, p0, Lcom/sec/ims/ss/data/SSConfigEvent;->mSessionID:I

    .line 95
    iput p3, p0, Lcom/sec/ims/ss/data/SSConfigEvent;->mSubEventType:I

    .line 96
    iput-object p4, p0, Lcom/sec/ims/ss/data/SSConfigEvent;->mData:[B

    .line 97
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    sget-object v0, Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;->NOT_INITIALIZED:Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;

    iput-object v0, p0, Lcom/sec/ims/ss/data/SSConfigEvent;->mState:Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;

    .line 105
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/ims/ss/data/SSConfigEvent;->mEventType:I

    .line 106
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/ims/ss/data/SSConfigEvent;->mSessionID:I

    .line 107
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/ims/ss/data/SSConfigEvent;->mSubEventType:I

    .line 108
    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/ss/data/SSConfigEvent;->mData:[B

    .line 109
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 113
    const/4 v0, 0x0

    return v0
.end method

.method public getCBType()[I
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/sec/ims/ss/data/SSConfigEvent;->mCBType:[I

    return-object v0
.end method

.method public getCFType()[I
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lcom/sec/ims/ss/data/SSConfigEvent;->mCFType:[I

    return-object v0
.end method

.method public getCFURI()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 249
    iget-object v0, p0, Lcom/sec/ims/ss/data/SSConfigEvent;->mCFURI:[Ljava/lang/String;

    return-object v0
.end method

.method public getCFURILength()[I
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, Lcom/sec/ims/ss/data/SSConfigEvent;->mCFURILength:[I

    return-object v0
.end method

.method public getCFURIType()[I
    .locals 1

    .prologue
    .line 241
    iget-object v0, p0, Lcom/sec/ims/ss/data/SSConfigEvent;->mCFURIType:[I

    return-object v0
.end method

.method public getData()[B
    .locals 1

    .prologue
    .line 257
    iget-object v0, p0, Lcom/sec/ims/ss/data/SSConfigEvent;->mData:[B

    return-object v0
.end method

.method public getErrorMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/sec/ims/ss/data/SSConfigEvent;->mErrorMessage:Ljava/lang/String;

    return-object v0
.end method

.method public getErrorMessages()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/sec/ims/ss/data/SSConfigEvent;->mErrorMessages:[Ljava/lang/String;

    return-object v0
.end method

.method public getEventType()I
    .locals 1

    .prologue
    .line 177
    iget v0, p0, Lcom/sec/ims/ss/data/SSConfigEvent;->mEventType:I

    return v0
.end method

.method public getNoReplyTime()[I
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lcom/sec/ims/ss/data/SSConfigEvent;->mNoReplyTime:[I

    return-object v0
.end method

.method public getNumberOfClasses()I
    .locals 1

    .prologue
    .line 161
    iget v0, p0, Lcom/sec/ims/ss/data/SSConfigEvent;->mNumberOfClasses:I

    return v0
.end method

.method public getResult()Z
    .locals 1

    .prologue
    .line 137
    iget-boolean v0, p0, Lcom/sec/ims/ss/data/SSConfigEvent;->mResult:Z

    return v0
.end method

.method public getSSClass()[I
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcom/sec/ims/ss/data/SSConfigEvent;->mSSClass:[I

    return-object v0
.end method

.method public getSSStatus()[I
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/sec/ims/ss/data/SSConfigEvent;->mSSStatus:[I

    return-object v0
.end method

.method public getSessionID()I
    .locals 1

    .prologue
    .line 169
    iget v0, p0, Lcom/sec/ims/ss/data/SSConfigEvent;->mSessionID:I

    return v0
.end method

.method public getState()Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/sec/ims/ss/data/SSConfigEvent;->mState:Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;

    return-object v0
.end method

.method public getSubEventType()I
    .locals 1

    .prologue
    .line 185
    iget v0, p0, Lcom/sec/ims/ss/data/SSConfigEvent;->mSubEventType:I

    return v0
.end method

.method public setCBType([I)V
    .locals 0
    .param p1, "cbType"    # [I

    .prologue
    .line 197
    iput-object p1, p0, Lcom/sec/ims/ss/data/SSConfigEvent;->mCBType:[I

    .line 198
    return-void
.end method

.method public setCFType([I)V
    .locals 0
    .param p1, "cfType"    # [I

    .prologue
    .line 189
    iput-object p1, p0, Lcom/sec/ims/ss/data/SSConfigEvent;->mCFType:[I

    .line 190
    return-void
.end method

.method public setCFURI([Ljava/lang/String;)V
    .locals 0
    .param p1, "cfURI"    # [Ljava/lang/String;

    .prologue
    .line 245
    iput-object p1, p0, Lcom/sec/ims/ss/data/SSConfigEvent;->mCFURI:[Ljava/lang/String;

    .line 246
    return-void
.end method

.method public setCFURILength([I)V
    .locals 0
    .param p1, "cfURILength"    # [I

    .prologue
    .line 229
    iput-object p1, p0, Lcom/sec/ims/ss/data/SSConfigEvent;->mCFURILength:[I

    .line 230
    return-void
.end method

.method public setCFURIType([I)V
    .locals 0
    .param p1, "cfURIType"    # [I

    .prologue
    .line 237
    iput-object p1, p0, Lcom/sec/ims/ss/data/SSConfigEvent;->mCFURIType:[I

    .line 238
    return-void
.end method

.method public setData([B)V
    .locals 0
    .param p1, "data"    # [B

    .prologue
    .line 253
    iput-object p1, p0, Lcom/sec/ims/ss/data/SSConfigEvent;->mData:[B

    .line 254
    return-void
.end method

.method public setErrorMessage(Ljava/lang/String;)V
    .locals 0
    .param p1, "errorMessage"    # Ljava/lang/String;

    .prologue
    .line 141
    iput-object p1, p0, Lcom/sec/ims/ss/data/SSConfigEvent;->mErrorMessage:Ljava/lang/String;

    .line 142
    return-void
.end method

.method public setErrorMessages([Ljava/lang/String;)V
    .locals 0
    .param p1, "errorMessages"    # [Ljava/lang/String;

    .prologue
    .line 149
    iput-object p1, p0, Lcom/sec/ims/ss/data/SSConfigEvent;->mErrorMessages:[Ljava/lang/String;

    .line 150
    return-void
.end method

.method public setEventType(I)V
    .locals 0
    .param p1, "eventType"    # I

    .prologue
    .line 173
    iput p1, p0, Lcom/sec/ims/ss/data/SSConfigEvent;->mEventType:I

    .line 174
    return-void
.end method

.method public setNoReplyTime([I)V
    .locals 0
    .param p1, "noReplyTime"    # [I

    .prologue
    .line 221
    iput-object p1, p0, Lcom/sec/ims/ss/data/SSConfigEvent;->mNoReplyTime:[I

    .line 222
    return-void
.end method

.method public setNumberOfClasses(I)V
    .locals 0
    .param p1, "numberOfClasses"    # I

    .prologue
    .line 157
    iput p1, p0, Lcom/sec/ims/ss/data/SSConfigEvent;->mNumberOfClasses:I

    .line 158
    return-void
.end method

.method public setResult(Z)V
    .locals 0
    .param p1, "result"    # Z

    .prologue
    .line 133
    iput-boolean p1, p0, Lcom/sec/ims/ss/data/SSConfigEvent;->mResult:Z

    .line 134
    return-void
.end method

.method public setSSClass([I)V
    .locals 0
    .param p1, "ssClass"    # [I

    .prologue
    .line 205
    iput-object p1, p0, Lcom/sec/ims/ss/data/SSConfigEvent;->mSSClass:[I

    .line 206
    return-void
.end method

.method public setSSStatus([I)V
    .locals 0
    .param p1, "ssStatus"    # [I

    .prologue
    .line 213
    iput-object p1, p0, Lcom/sec/ims/ss/data/SSConfigEvent;->mSSStatus:[I

    .line 214
    return-void
.end method

.method public setSessionID(I)V
    .locals 0
    .param p1, "sessionID"    # I

    .prologue
    .line 165
    iput p1, p0, Lcom/sec/ims/ss/data/SSConfigEvent;->mSessionID:I

    .line 166
    return-void
.end method

.method public setState(Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;)V
    .locals 0
    .param p1, "state"    # Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;

    .prologue
    .line 125
    iput-object p1, p0, Lcom/sec/ims/ss/data/SSConfigEvent;->mState:Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;

    .line 126
    return-void
.end method

.method public setSubEventType(I)V
    .locals 0
    .param p1, "subEventType"    # I

    .prologue
    .line 181
    iput p1, p0, Lcom/sec/ims/ss/data/SSConfigEvent;->mSubEventType:I

    .line 182
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 118
    iget v0, p0, Lcom/sec/ims/ss/data/SSConfigEvent;->mEventType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 119
    iget v0, p0, Lcom/sec/ims/ss/data/SSConfigEvent;->mSessionID:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 120
    iget v0, p0, Lcom/sec/ims/ss/data/SSConfigEvent;->mSubEventType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 121
    iget-object v0, p0, Lcom/sec/ims/ss/data/SSConfigEvent;->mData:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 122
    return-void
.end method
