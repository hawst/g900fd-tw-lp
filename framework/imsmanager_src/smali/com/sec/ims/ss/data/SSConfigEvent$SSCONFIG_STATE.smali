.class public final enum Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;
.super Ljava/lang/Enum;
.source "SSConfigEvent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/ims/ss/data/SSConfigEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SSCONFIG_STATE"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;

.field public static final enum GET_CALL_BARRING:Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;

.field public static final enum GET_CALL_FORWARDING:Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;

.field public static final enum GET_CALL_WAITING:Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;

.field public static final enum NOT_INITIALIZED:Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;

.field public static final enum SET_CALL_BARRING:Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;

.field public static final enum SET_CALL_FORWARDING:Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;

.field public static final enum SET_CALL_WAITING:Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 41
    new-instance v0, Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;

    const-string v1, "NOT_INITIALIZED"

    invoke-direct {v0, v1, v3}, Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;->NOT_INITIALIZED:Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;

    .line 46
    new-instance v0, Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;

    const-string v1, "SET_CALL_WAITING"

    invoke-direct {v0, v1, v4}, Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;->SET_CALL_WAITING:Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;

    .line 54
    new-instance v0, Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;

    const-string v1, "GET_CALL_WAITING"

    invoke-direct {v0, v1, v5}, Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;->GET_CALL_WAITING:Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;

    .line 59
    new-instance v0, Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;

    const-string v1, "SET_CALL_FORWARDING"

    invoke-direct {v0, v1, v6}, Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;->SET_CALL_FORWARDING:Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;

    .line 72
    new-instance v0, Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;

    const-string v1, "GET_CALL_FORWARDING"

    invoke-direct {v0, v1, v7}, Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;->GET_CALL_FORWARDING:Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;

    .line 77
    new-instance v0, Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;

    const-string v1, "SET_CALL_BARRING"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;->SET_CALL_BARRING:Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;

    .line 86
    new-instance v0, Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;

    const-string v1, "GET_CALL_BARRING"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;->GET_CALL_BARRING:Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;

    .line 40
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;

    sget-object v1, Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;->NOT_INITIALIZED:Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;->SET_CALL_WAITING:Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;->GET_CALL_WAITING:Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;->SET_CALL_FORWARDING:Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;->GET_CALL_FORWARDING:Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;->SET_CALL_BARRING:Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;->GET_CALL_BARRING:Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;->$VALUES:[Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 40
    const-class v0, Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;

    return-object v0
.end method

.method public static values()[Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;->$VALUES:[Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;

    invoke-virtual {v0}, [Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/ims/ss/data/SSConfigEvent$SSCONFIG_STATE;

    return-object v0
.end method
