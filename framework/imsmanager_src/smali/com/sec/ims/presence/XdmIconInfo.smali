.class public Lcom/sec/ims/presence/XdmIconInfo;
.super Ljava/lang/Object;
.source "XdmIconInfo.java"


# instance fields
.field private pi8Entity:Ljava/lang/String;

.field private pi8Etag:Ljava/lang/String;

.field private pi8IconLocalPath:Ljava/lang/String;

.field private pi8IconUri:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "entity"    # Ljava/lang/String;
    .param p2, "iconUri"    # Ljava/lang/String;
    .param p3, "etag"    # Ljava/lang/String;
    .param p4, "iconLoclPath"    # Ljava/lang/String;

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    invoke-direct {p0, p1}, Lcom/sec/ims/presence/XdmIconInfo;->setPi8Entity(Ljava/lang/String;)V

    .line 23
    invoke-direct {p0, p2}, Lcom/sec/ims/presence/XdmIconInfo;->setPi8IconUri(Ljava/lang/String;)V

    .line 24
    invoke-direct {p0, p3}, Lcom/sec/ims/presence/XdmIconInfo;->setPi8Etag(Ljava/lang/String;)V

    .line 25
    invoke-direct {p0, p4}, Lcom/sec/ims/presence/XdmIconInfo;->setPi8IconLocalPath(Ljava/lang/String;)V

    .line 26
    return-void
.end method

.method private setPi8Entity(Ljava/lang/String;)V
    .locals 0
    .param p1, "pi8Entity"    # Ljava/lang/String;

    .prologue
    .line 33
    iput-object p1, p0, Lcom/sec/ims/presence/XdmIconInfo;->pi8Entity:Ljava/lang/String;

    .line 34
    return-void
.end method

.method private setPi8Etag(Ljava/lang/String;)V
    .locals 0
    .param p1, "pi8Etag"    # Ljava/lang/String;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/sec/ims/presence/XdmIconInfo;->pi8Etag:Ljava/lang/String;

    .line 50
    return-void
.end method

.method private setPi8IconLocalPath(Ljava/lang/String;)V
    .locals 0
    .param p1, "pi8IconLocalPath"    # Ljava/lang/String;

    .prologue
    .line 57
    iput-object p1, p0, Lcom/sec/ims/presence/XdmIconInfo;->pi8IconLocalPath:Ljava/lang/String;

    .line 58
    return-void
.end method

.method private setPi8IconUri(Ljava/lang/String;)V
    .locals 0
    .param p1, "pi8IconUri"    # Ljava/lang/String;

    .prologue
    .line 41
    iput-object p1, p0, Lcom/sec/ims/presence/XdmIconInfo;->pi8IconUri:Ljava/lang/String;

    .line 42
    return-void
.end method


# virtual methods
.method public getPi8Entity()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/ims/presence/XdmIconInfo;->pi8Entity:Ljava/lang/String;

    return-object v0
.end method

.method public getPi8Etag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/ims/presence/XdmIconInfo;->pi8Etag:Ljava/lang/String;

    return-object v0
.end method

.method public getPi8IconLocalPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/ims/presence/XdmIconInfo;->pi8IconLocalPath:Ljava/lang/String;

    return-object v0
.end method

.method public getPi8IconUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/ims/presence/XdmIconInfo;->pi8IconUri:Ljava/lang/String;

    return-object v0
.end method
