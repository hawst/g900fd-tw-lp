.class public Lcom/sec/ims/presence/PresenceInfo$Builder;
.super Ljava/lang/Object;
.source "PresenceInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/ims/presence/PresenceInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field activities:Ljava/lang/String;

.field avatar_uri:Ljava/lang/String;

.field birthday:Ljava/lang/String;

.field cyworld:Ljava/lang/String;

.field display_name:Ljava/lang/String;

.field email:Ljava/lang/String;

.field facebook:Ljava/lang/String;

.field homepage:Ljava/lang/String;

.field hyper:I

.field mood_text:Ljava/lang/String;

.field phone_number:Ljava/lang/String;

.field state:I

.field tel_uri:Ljava/lang/String;

.field timestamp:J

.field twitter:Ljava/lang/String;

.field uri:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 337
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public activities(Ljava/lang/String;)Lcom/sec/ims/presence/PresenceInfo$Builder;
    .locals 0
    .param p1, "activities"    # Ljava/lang/String;

    .prologue
    .line 431
    iput-object p1, p0, Lcom/sec/ims/presence/PresenceInfo$Builder;->activities:Ljava/lang/String;

    .line 432
    return-object p0
.end method

.method public avatar_uri(Ljava/lang/String;)Lcom/sec/ims/presence/PresenceInfo$Builder;
    .locals 0
    .param p1, "avatar_uri"    # Ljava/lang/String;

    .prologue
    .line 366
    iput-object p1, p0, Lcom/sec/ims/presence/PresenceInfo$Builder;->avatar_uri:Ljava/lang/String;

    .line 367
    return-object p0
.end method

.method public birthday(Ljava/lang/String;)Lcom/sec/ims/presence/PresenceInfo$Builder;
    .locals 0
    .param p1, "birthday"    # Ljava/lang/String;

    .prologue
    .line 406
    iput-object p1, p0, Lcom/sec/ims/presence/PresenceInfo$Builder;->birthday:Ljava/lang/String;

    .line 407
    return-object p0
.end method

.method public build()Lcom/sec/ims/presence/PresenceInfo;
    .locals 1

    .prologue
    .line 436
    new-instance v0, Lcom/sec/ims/presence/PresenceInfo;

    invoke-direct {v0, p0}, Lcom/sec/ims/presence/PresenceInfo;-><init>(Lcom/sec/ims/presence/PresenceInfo$Builder;)V

    return-object v0
.end method

.method public cyworld(Ljava/lang/String;)Lcom/sec/ims/presence/PresenceInfo$Builder;
    .locals 0
    .param p1, "cyworld"    # Ljava/lang/String;

    .prologue
    .line 416
    iput-object p1, p0, Lcom/sec/ims/presence/PresenceInfo$Builder;->cyworld:Ljava/lang/String;

    .line 417
    return-object p0
.end method

.method public display_name(Ljava/lang/String;)Lcom/sec/ims/presence/PresenceInfo$Builder;
    .locals 0
    .param p1, "display_name"    # Ljava/lang/String;

    .prologue
    .line 381
    iput-object p1, p0, Lcom/sec/ims/presence/PresenceInfo$Builder;->display_name:Ljava/lang/String;

    .line 382
    return-object p0
.end method

.method public email(Ljava/lang/String;)Lcom/sec/ims/presence/PresenceInfo$Builder;
    .locals 0
    .param p1, "email"    # Ljava/lang/String;

    .prologue
    .line 401
    iput-object p1, p0, Lcom/sec/ims/presence/PresenceInfo$Builder;->email:Ljava/lang/String;

    .line 402
    return-object p0
.end method

.method public facebook(Ljava/lang/String;)Lcom/sec/ims/presence/PresenceInfo$Builder;
    .locals 0
    .param p1, "facebook"    # Ljava/lang/String;

    .prologue
    .line 426
    iput-object p1, p0, Lcom/sec/ims/presence/PresenceInfo$Builder;->facebook:Ljava/lang/String;

    .line 427
    return-object p0
.end method

.method public homepage(Ljava/lang/String;)Lcom/sec/ims/presence/PresenceInfo$Builder;
    .locals 0
    .param p1, "homepage"    # Ljava/lang/String;

    .prologue
    .line 396
    iput-object p1, p0, Lcom/sec/ims/presence/PresenceInfo$Builder;->homepage:Ljava/lang/String;

    .line 397
    return-object p0
.end method

.method public hyper(I)Lcom/sec/ims/presence/PresenceInfo$Builder;
    .locals 0
    .param p1, "hyper"    # I

    .prologue
    .line 386
    iput p1, p0, Lcom/sec/ims/presence/PresenceInfo$Builder;->hyper:I

    .line 387
    return-object p0
.end method

.method public mood_text(Ljava/lang/String;)Lcom/sec/ims/presence/PresenceInfo$Builder;
    .locals 0
    .param p1, "mood_text"    # Ljava/lang/String;

    .prologue
    .line 391
    iput-object p1, p0, Lcom/sec/ims/presence/PresenceInfo$Builder;->mood_text:Ljava/lang/String;

    .line 392
    return-object p0
.end method

.method public phone_number(Ljava/lang/String;)Lcom/sec/ims/presence/PresenceInfo$Builder;
    .locals 0
    .param p1, "phone_number"    # Ljava/lang/String;

    .prologue
    .line 411
    iput-object p1, p0, Lcom/sec/ims/presence/PresenceInfo$Builder;->phone_number:Ljava/lang/String;

    .line 412
    return-object p0
.end method

.method public state(I)Lcom/sec/ims/presence/PresenceInfo$Builder;
    .locals 0
    .param p1, "state"    # I

    .prologue
    .line 371
    iput p1, p0, Lcom/sec/ims/presence/PresenceInfo$Builder;->state:I

    .line 372
    return-object p0
.end method

.method public tel_uri(Ljava/lang/String;)Lcom/sec/ims/presence/PresenceInfo$Builder;
    .locals 0
    .param p1, "telUri"    # Ljava/lang/String;

    .prologue
    .line 356
    iput-object p1, p0, Lcom/sec/ims/presence/PresenceInfo$Builder;->tel_uri:Ljava/lang/String;

    .line 357
    return-object p0
.end method

.method public timestamp(J)Lcom/sec/ims/presence/PresenceInfo$Builder;
    .locals 1
    .param p1, "timestamp"    # J

    .prologue
    .line 376
    iput-wide p1, p0, Lcom/sec/ims/presence/PresenceInfo$Builder;->timestamp:J

    .line 377
    return-object p0
.end method

.method public twitter(Ljava/lang/String;)Lcom/sec/ims/presence/PresenceInfo$Builder;
    .locals 0
    .param p1, "twitter"    # Ljava/lang/String;

    .prologue
    .line 421
    iput-object p1, p0, Lcom/sec/ims/presence/PresenceInfo$Builder;->twitter:Ljava/lang/String;

    .line 422
    return-object p0
.end method

.method public uri(Ljava/lang/String;)Lcom/sec/ims/presence/PresenceInfo$Builder;
    .locals 0
    .param p1, "uri"    # Ljava/lang/String;

    .prologue
    .line 361
    iput-object p1, p0, Lcom/sec/ims/presence/PresenceInfo$Builder;->uri:Ljava/lang/String;

    .line 362
    return-object p0
.end method
