.class public Lcom/sec/ims/presence/XdmResponse;
.super Ljava/lang/Object;
.source "XdmResponse.java"


# instance fields
.field protected mListType:Lcom/sec/ims/presence/Constants$ListType;

.field protected mListUpdateType:Lcom/sec/ims/presence/Constants$ListUpdateType;

.field protected mSuccess:Z

.field protected mUri:Landroid/net/Uri;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/net/Uri;ZLcom/sec/ims/presence/Constants$ListType;Lcom/sec/ims/presence/Constants$ListUpdateType;)V
    .locals 0
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "success"    # Z
    .param p3, "listType"    # Lcom/sec/ims/presence/Constants$ListType;
    .param p4, "listUpdateType"    # Lcom/sec/ims/presence/Constants$ListUpdateType;

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/sec/ims/presence/XdmResponse;->mUri:Landroid/net/Uri;

    .line 30
    iput-boolean p2, p0, Lcom/sec/ims/presence/XdmResponse;->mSuccess:Z

    .line 31
    iput-object p3, p0, Lcom/sec/ims/presence/XdmResponse;->mListType:Lcom/sec/ims/presence/Constants$ListType;

    .line 32
    iput-object p4, p0, Lcom/sec/ims/presence/XdmResponse;->mListUpdateType:Lcom/sec/ims/presence/Constants$ListUpdateType;

    .line 33
    return-void
.end method


# virtual methods
.method public getListType()Lcom/sec/ims/presence/Constants$ListType;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/ims/presence/XdmResponse;->mListType:Lcom/sec/ims/presence/Constants$ListType;

    return-object v0
.end method

.method public getListUpdateType()Lcom/sec/ims/presence/Constants$ListUpdateType;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/ims/presence/XdmResponse;->mListUpdateType:Lcom/sec/ims/presence/Constants$ListUpdateType;

    return-object v0
.end method

.method public getUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/ims/presence/XdmResponse;->mUri:Landroid/net/Uri;

    return-object v0
.end method

.method public isSuccess()Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/sec/ims/presence/XdmResponse;->mSuccess:Z

    return v0
.end method
