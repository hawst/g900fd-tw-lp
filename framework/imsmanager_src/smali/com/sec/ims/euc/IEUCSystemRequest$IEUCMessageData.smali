.class public Lcom/sec/ims/euc/IEUCSystemRequest$IEUCMessageData;
.super Ljava/lang/Object;
.source "IEUCSystemRequest.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/ims/euc/IEUCSystemRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "IEUCMessageData"
.end annotation


# instance fields
.field public final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/ims/euc/IEUCSystemRequest$IEUCMessageData;",
            ">;"
        }
    .end annotation
.end field

.field private mData:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    new-instance v0, Lcom/sec/ims/euc/IEUCSystemRequest$IEUCMessageData$1;

    invoke-direct {v0, p0}, Lcom/sec/ims/euc/IEUCSystemRequest$IEUCMessageData$1;-><init>(Lcom/sec/ims/euc/IEUCSystemRequest$IEUCMessageData;)V

    iput-object v0, p0, Lcom/sec/ims/euc/IEUCSystemRequest$IEUCMessageData;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 72
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    new-instance v0, Lcom/sec/ims/euc/IEUCSystemRequest$IEUCMessageData$1;

    invoke-direct {v0, p0}, Lcom/sec/ims/euc/IEUCSystemRequest$IEUCMessageData$1;-><init>(Lcom/sec/ims/euc/IEUCSystemRequest$IEUCMessageData;)V

    iput-object v0, p0, Lcom/sec/ims/euc/IEUCSystemRequest$IEUCMessageData;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 102
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/euc/IEUCSystemRequest$IEUCMessageData;->mData:Ljava/lang/String;

    .line 103
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/ims/euc/IEUCSystemRequest$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/sec/ims/euc/IEUCSystemRequest$1;

    .prologue
    .line 63
    invoke-direct {p0, p1}, Lcom/sec/ims/euc/IEUCSystemRequest$IEUCMessageData;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 81
    const/4 v0, 0x0

    return v0
.end method

.method public getData()Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/ims/euc/IEUCSystemRequest$IEUCMessageData;->mData:Ljava/lang/String;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/ims/euc/IEUCSystemRequest$IEUCMessageData;->mData:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 87
    return-void
.end method
