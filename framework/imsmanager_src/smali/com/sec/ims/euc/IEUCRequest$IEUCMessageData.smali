.class public Lcom/sec/ims/euc/IEUCRequest$IEUCMessageData;
.super Ljava/lang/Object;
.source "IEUCRequest.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/ims/euc/IEUCRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "IEUCMessageData"
.end annotation


# instance fields
.field public final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/ims/euc/IEUCRequest$IEUCMessageData;",
            ">;"
        }
    .end annotation
.end field

.field mAcceptButtonText:Ljava/lang/String;

.field mRejectButtonText:Ljava/lang/String;

.field mSubject:Ljava/lang/String;

.field mText:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 135
    new-instance v0, Lcom/sec/ims/euc/IEUCRequest$IEUCMessageData$1;

    invoke-direct {v0, p0}, Lcom/sec/ims/euc/IEUCRequest$IEUCMessageData$1;-><init>(Lcom/sec/ims/euc/IEUCRequest$IEUCMessageData;)V

    iput-object v0, p0, Lcom/sec/ims/euc/IEUCRequest$IEUCMessageData;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 79
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 147
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 135
    new-instance v0, Lcom/sec/ims/euc/IEUCRequest$IEUCMessageData$1;

    invoke-direct {v0, p0}, Lcom/sec/ims/euc/IEUCRequest$IEUCMessageData$1;-><init>(Lcom/sec/ims/euc/IEUCRequest$IEUCMessageData;)V

    iput-object v0, p0, Lcom/sec/ims/euc/IEUCRequest$IEUCMessageData;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 148
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/euc/IEUCRequest$IEUCMessageData;->mSubject:Ljava/lang/String;

    .line 149
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/euc/IEUCRequest$IEUCMessageData;->mText:Ljava/lang/String;

    .line 150
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/euc/IEUCRequest$IEUCMessageData;->mAcceptButtonText:Ljava/lang/String;

    .line 151
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/euc/IEUCRequest$IEUCMessageData;->mRejectButtonText:Ljava/lang/String;

    .line 152
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/ims/euc/IEUCRequest$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/sec/ims/euc/IEUCRequest$1;

    .prologue
    .line 71
    invoke-direct {p0, p1}, Lcom/sec/ims/euc/IEUCRequest$IEUCMessageData;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 124
    const/4 v0, 0x0

    return v0
.end method

.method public getAcceptButton()Ljava/lang/String;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/sec/ims/euc/IEUCRequest$IEUCMessageData;->mAcceptButtonText:Ljava/lang/String;

    return-object v0
.end method

.method public getRejectButton()Ljava/lang/String;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/ims/euc/IEUCRequest$IEUCMessageData;->mRejectButtonText:Ljava/lang/String;

    return-object v0
.end method

.method public getSubject()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/ims/euc/IEUCRequest$IEUCMessageData;->mSubject:Ljava/lang/String;

    return-object v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/ims/euc/IEUCRequest$IEUCMessageData;->mText:Ljava/lang/String;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 129
    iget-object v0, p0, Lcom/sec/ims/euc/IEUCRequest$IEUCMessageData;->mSubject:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 130
    iget-object v0, p0, Lcom/sec/ims/euc/IEUCRequest$IEUCMessageData;->mText:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 131
    iget-object v0, p0, Lcom/sec/ims/euc/IEUCRequest$IEUCMessageData;->mAcceptButtonText:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 132
    iget-object v0, p0, Lcom/sec/ims/euc/IEUCRequest$IEUCMessageData;->mRejectButtonText:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 133
    return-void
.end method
