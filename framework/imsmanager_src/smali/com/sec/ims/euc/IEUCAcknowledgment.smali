.class public Lcom/sec/ims/euc/IEUCAcknowledgment;
.super Lcom/sec/ims/euc/IEUC;
.source "IEUCAcknowledgment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/ims/euc/IEUCAcknowledgment$IEUCMessageData;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sec/ims/euc/IEUC",
        "<",
        "Lcom/sec/ims/euc/IEUCAcknowledgment$IEUCMessageData;",
        ">;"
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/ims/euc/IEUCAcknowledgment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 117
    new-instance v0, Lcom/sec/ims/euc/IEUCAcknowledgment$1;

    invoke-direct {v0}, Lcom/sec/ims/euc/IEUCAcknowledgment$1;-><init>()V

    sput-object v0, Lcom/sec/ims/euc/IEUCAcknowledgment;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 133
    invoke-direct {p0}, Lcom/sec/ims/euc/IEUC;-><init>()V

    .line 135
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 129
    invoke-direct {p0}, Lcom/sec/ims/euc/IEUC;-><init>()V

    .line 130
    invoke-virtual {p0, p1}, Lcom/sec/ims/euc/IEUCAcknowledgment;->readFromParcel(Landroid/os/Parcel;)V

    .line 131
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/ims/euc/IEUCAcknowledgment$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/sec/ims/euc/IEUCAcknowledgment$1;

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/sec/ims/euc/IEUCAcknowledgment;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 109
    const/4 v0, 0x0

    return v0
.end method

.method public getDefaultData()Lcom/sec/ims/euc/IEUCAcknowledgment$IEUCMessageData;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/sec/ims/euc/IEUCAcknowledgment;->mDefaultMessage:Ljava/lang/Object;

    check-cast v0, Lcom/sec/ims/euc/IEUCAcknowledgment$IEUCMessageData;

    return-object v0
.end method

.method public bridge synthetic getDefaultData()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 25
    invoke-virtual {p0}, Lcom/sec/ims/euc/IEUCAcknowledgment;->getDefaultData()Lcom/sec/ims/euc/IEUCAcknowledgment$IEUCMessageData;

    move-result-object v0

    return-object v0
.end method

.method protected readMessageData(Landroid/os/Parcel;)V
    .locals 6
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 146
    const-class v3, Lcom/sec/ims/euc/IEUCAcknowledgment$IEUCMessageData;

    invoke-virtual {v3}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/ims/euc/IEUCAcknowledgment;->mDefaultMessage:Ljava/lang/Object;

    .line 147
    invoke-virtual {p1}, Landroid/os/Parcel;->readStringArray()[Ljava/lang/String;

    move-result-object v1

    .line 148
    .local v1, "lang":[Ljava/lang/String;
    const-class v3, Lcom/sec/ims/euc/IEUCAcknowledgment$IEUCMessageData;

    invoke-virtual {v3}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->readParcelableArray(Ljava/lang/ClassLoader;)[Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, [Lcom/sec/ims/euc/IEUCAcknowledgment$IEUCMessageData;

    move-object v2, v3

    check-cast v2, [Lcom/sec/ims/euc/IEUCAcknowledgment$IEUCMessageData;

    .line 151
    .local v2, "messages":[Lcom/sec/ims/euc/IEUCAcknowledgment$IEUCMessageData;
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    iput-object v3, p0, Lcom/sec/ims/euc/IEUCAcknowledgment;->mMessages:Ljava/util/Map;

    .line 152
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, v1

    if-ge v0, v3, :cond_0

    .line 153
    iget-object v3, p0, Lcom/sec/ims/euc/IEUCAcknowledgment;->mMessages:Ljava/util/Map;

    aget-object v4, v1, v0

    aget-object v5, v2, v0

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 152
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 155
    :cond_0
    return-void
.end method

.method protected writeMessageData(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "out"    # Landroid/os/Parcel;

    .prologue
    const/4 v2, 0x0

    .line 139
    iget-object v0, p0, Lcom/sec/ims/euc/IEUCAcknowledgment;->mDefaultMessage:Ljava/lang/Object;

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 140
    iget-object v0, p0, Lcom/sec/ims/euc/IEUCAcknowledgment;->mMessages:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    new-array v1, v2, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    .line 141
    iget-object v0, p0, Lcom/sec/ims/euc/IEUCAcknowledgment;->mMessages:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    new-array v1, v2, [Lcom/sec/ims/euc/IEUCAcknowledgment$IEUCMessageData;

    invoke-interface {v0, v1}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/os/Parcelable;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelableArray([Landroid/os/Parcelable;I)V

    .line 142
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 114
    invoke-super {p0, p1, p2}, Lcom/sec/ims/euc/IEUC;->writeToParcel(Landroid/os/Parcel;I)V

    .line 115
    return-void
.end method
