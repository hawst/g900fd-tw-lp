.class public Lcom/sec/ims/euc/IEUCNotification$IEUCMessageData;
.super Ljava/lang/Object;
.source "IEUCNotification.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/ims/euc/IEUCNotification;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "IEUCMessageData"
.end annotation


# instance fields
.field public final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/ims/euc/IEUCNotification$IEUCMessageData;",
            ">;"
        }
    .end annotation
.end field

.field private mOkButton:Ljava/lang/String;

.field private mSubject:Ljava/lang/String;

.field private mText:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    new-instance v0, Lcom/sec/ims/euc/IEUCNotification$IEUCMessageData$1;

    invoke-direct {v0, p0}, Lcom/sec/ims/euc/IEUCNotification$IEUCMessageData$1;-><init>(Lcom/sec/ims/euc/IEUCNotification$IEUCMessageData;)V

    iput-object v0, p0, Lcom/sec/ims/euc/IEUCNotification$IEUCMessageData;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 40
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    new-instance v0, Lcom/sec/ims/euc/IEUCNotification$IEUCMessageData$1;

    invoke-direct {v0, p0}, Lcom/sec/ims/euc/IEUCNotification$IEUCMessageData$1;-><init>(Lcom/sec/ims/euc/IEUCNotification$IEUCMessageData;)V

    iput-object v0, p0, Lcom/sec/ims/euc/IEUCNotification$IEUCMessageData;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 97
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/euc/IEUCNotification$IEUCMessageData;->mSubject:Ljava/lang/String;

    .line 98
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/euc/IEUCNotification$IEUCMessageData;->mText:Ljava/lang/String;

    .line 99
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/euc/IEUCNotification$IEUCMessageData;->mOkButton:Ljava/lang/String;

    .line 100
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/ims/euc/IEUCNotification$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/sec/ims/euc/IEUCNotification$1;

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/sec/ims/euc/IEUCNotification$IEUCMessageData;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 74
    const/4 v0, 0x0

    return v0
.end method

.method public getOKButton()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/ims/euc/IEUCNotification$IEUCMessageData;->mOkButton:Ljava/lang/String;

    return-object v0
.end method

.method public getSubject()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/ims/euc/IEUCNotification$IEUCMessageData;->mSubject:Ljava/lang/String;

    return-object v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/ims/euc/IEUCNotification$IEUCMessageData;->mText:Ljava/lang/String;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/ims/euc/IEUCNotification$IEUCMessageData;->mSubject:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 80
    iget-object v0, p0, Lcom/sec/ims/euc/IEUCNotification$IEUCMessageData;->mText:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 81
    iget-object v0, p0, Lcom/sec/ims/euc/IEUCNotification$IEUCMessageData;->mOkButton:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 82
    return-void
.end method
