.class public Lcom/sec/ims/euc/IEUCRequest;
.super Lcom/sec/ims/euc/IEUC;
.source "IEUCRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/ims/euc/IEUCRequest$IEUCMessageData;,
        Lcom/sec/ims/euc/IEUCRequest$EUCRequestType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sec/ims/euc/IEUC",
        "<",
        "Lcom/sec/ims/euc/IEUCRequest$IEUCMessageData;",
        ">;"
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/ims/euc/IEUCRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_TIMEOUT:J = 0x7d00L


# instance fields
.field private mIsExternal:Z

.field private mIsPinRequested:Z

.field private mTimeout:J

.field private mType:Lcom/sec/ims/euc/IEUCRequest$EUCRequestType;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 216
    new-instance v0, Lcom/sec/ims/euc/IEUCRequest$1;

    invoke-direct {v0}, Lcom/sec/ims/euc/IEUCRequest$1;-><init>()V

    sput-object v0, Lcom/sec/ims/euc/IEUCRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/sec/ims/euc/IEUC;-><init>()V

    .line 63
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 4
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 228
    invoke-direct {p0}, Lcom/sec/ims/euc/IEUC;-><init>()V

    .line 229
    invoke-virtual {p0, p1}, Lcom/sec/ims/euc/IEUCRequest;->readFromParcel(Landroid/os/Parcel;)V

    .line 230
    invoke-static {}, Lcom/sec/ims/euc/IEUCRequest$EUCRequestType;->values()[Lcom/sec/ims/euc/IEUCRequest$EUCRequestType;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    aget-object v0, v0, v3

    iput-object v0, p0, Lcom/sec/ims/euc/IEUCRequest;->mType:Lcom/sec/ims/euc/IEUCRequest$EUCRequestType;

    .line 231
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/ims/euc/IEUCRequest;->mIsPinRequested:Z

    .line 232
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/sec/ims/euc/IEUCRequest;->mIsExternal:Z

    .line 233
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/ims/euc/IEUCRequest;->mTimeout:J

    .line 234
    return-void

    :cond_0
    move v0, v2

    .line 231
    goto :goto_0

    :cond_1
    move v1, v2

    .line 232
    goto :goto_1
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/ims/euc/IEUCRequest$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/sec/ims/euc/IEUCRequest$1;

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/sec/ims/euc/IEUCRequest;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 204
    const/4 v0, 0x0

    return v0
.end method

.method public getTimeOut()Ljava/lang/Long;
    .locals 2

    .prologue
    .line 196
    iget-wide v0, p0, Lcom/sec/ims/euc/IEUCRequest;->mTimeout:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public getType()Lcom/sec/ims/euc/IEUCRequest$EUCRequestType;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/sec/ims/euc/IEUCRequest;->mType:Lcom/sec/ims/euc/IEUCRequest$EUCRequestType;

    return-object v0
.end method

.method public isExternal()Z
    .locals 1

    .prologue
    .line 184
    iget-boolean v0, p0, Lcom/sec/ims/euc/IEUCRequest;->mIsExternal:Z

    return v0
.end method

.method public isPinRequested()Z
    .locals 1

    .prologue
    .line 173
    iget-boolean v0, p0, Lcom/sec/ims/euc/IEUCRequest;->mIsPinRequested:Z

    return v0
.end method

.method protected readMessageData(Landroid/os/Parcel;)V
    .locals 6
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 245
    const-class v3, Lcom/sec/ims/euc/IEUCRequest$IEUCMessageData;

    invoke-virtual {v3}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/ims/euc/IEUCRequest;->mDefaultMessage:Ljava/lang/Object;

    .line 246
    invoke-virtual {p1}, Landroid/os/Parcel;->readStringArray()[Ljava/lang/String;

    move-result-object v1

    .line 247
    .local v1, "lang":[Ljava/lang/String;
    const-class v3, Lcom/sec/ims/euc/IEUCRequest$IEUCMessageData;

    invoke-virtual {v3}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->readParcelableArray(Ljava/lang/ClassLoader;)[Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, [Lcom/sec/ims/euc/IEUCRequest$IEUCMessageData;

    move-object v2, v3

    check-cast v2, [Lcom/sec/ims/euc/IEUCRequest$IEUCMessageData;

    .line 250
    .local v2, "messages":[Lcom/sec/ims/euc/IEUCRequest$IEUCMessageData;
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    iput-object v3, p0, Lcom/sec/ims/euc/IEUCRequest;->mMessages:Ljava/util/Map;

    .line 251
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, v1

    if-ge v0, v3, :cond_0

    .line 252
    iget-object v3, p0, Lcom/sec/ims/euc/IEUCRequest;->mMessages:Ljava/util/Map;

    aget-object v4, v1, v0

    aget-object v5, v2, v0

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 251
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 254
    :cond_0
    return-void
.end method

.method protected writeMessageData(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "out"    # Landroid/os/Parcel;

    .prologue
    const/4 v2, 0x0

    .line 238
    iget-object v0, p0, Lcom/sec/ims/euc/IEUCRequest;->mDefaultMessage:Ljava/lang/Object;

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 239
    iget-object v0, p0, Lcom/sec/ims/euc/IEUCRequest;->mMessages:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    new-array v1, v2, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    .line 240
    iget-object v0, p0, Lcom/sec/ims/euc/IEUCRequest;->mMessages:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    new-array v1, v2, [Lcom/sec/ims/euc/IEUCRequest$IEUCMessageData;

    invoke-interface {v0, v1}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/os/Parcelable;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelableArray([Landroid/os/Parcelable;I)V

    .line 241
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 209
    invoke-super {p0, p1, p2}, Lcom/sec/ims/euc/IEUC;->writeToParcel(Landroid/os/Parcel;I)V

    .line 210
    iget-object v0, p0, Lcom/sec/ims/euc/IEUCRequest;->mType:Lcom/sec/ims/euc/IEUCRequest$EUCRequestType;

    invoke-virtual {v0}, Lcom/sec/ims/euc/IEUCRequest$EUCRequestType;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 211
    iget-boolean v0, p0, Lcom/sec/ims/euc/IEUCRequest;->mIsPinRequested:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 212
    iget-boolean v0, p0, Lcom/sec/ims/euc/IEUCRequest;->mIsExternal:Z

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeByte(B)V

    .line 213
    iget-wide v0, p0, Lcom/sec/ims/euc/IEUCRequest;->mTimeout:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 214
    return-void

    :cond_0
    move v0, v2

    .line 211
    goto :goto_0

    :cond_1
    move v1, v2

    .line 212
    goto :goto_1
.end method
