.class public abstract Lcom/sec/ims/euc/IEUC;
.super Ljava/lang/Object;
.source "IEUC.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<MessageDataType:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# instance fields
.field protected mDefaultMessage:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TMessageDataType;"
        }
    .end annotation
.end field

.field protected mEUCId:Ljava/lang/String;

.field protected mFrom:Landroid/net/Uri;

.field protected mMessages:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "TMessageDataType;>;"
        }
    .end annotation
.end field

.field protected mTo:Landroid/net/Uri;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    .local p0, "this":Lcom/sec/ims/euc/IEUC;, "Lcom/sec/ims/euc/IEUC<TMessageDataType;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getDefaultData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TMessageDataType;"
        }
    .end annotation

    .prologue
    .line 58
    .local p0, "this":Lcom/sec/ims/euc/IEUC;, "Lcom/sec/ims/euc/IEUC<TMessageDataType;>;"
    iget-object v0, p0, Lcom/sec/ims/euc/IEUC;->mDefaultMessage:Ljava/lang/Object;

    return-object v0
.end method

.method public getEUCId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    .local p0, "this":Lcom/sec/ims/euc/IEUC;, "Lcom/sec/ims/euc/IEUC<TMessageDataType;>;"
    iget-object v0, p0, Lcom/sec/ims/euc/IEUC;->mEUCId:Ljava/lang/String;

    return-object v0
.end method

.method public getFromHeader()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 76
    .local p0, "this":Lcom/sec/ims/euc/IEUC;, "Lcom/sec/ims/euc/IEUC<TMessageDataType;>;"
    iget-object v0, p0, Lcom/sec/ims/euc/IEUC;->mFrom:Landroid/net/Uri;

    return-object v0
.end method

.method public getLanguageMapping()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "TMessageDataType;>;"
        }
    .end annotation

    .prologue
    .line 47
    .local p0, "this":Lcom/sec/ims/euc/IEUC;, "Lcom/sec/ims/euc/IEUC<TMessageDataType;>;"
    iget-object v0, p0, Lcom/sec/ims/euc/IEUC;->mMessages:Ljava/util/Map;

    return-object v0
.end method

.method public getToHeader()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 85
    .local p0, "this":Lcom/sec/ims/euc/IEUC;, "Lcom/sec/ims/euc/IEUC<TMessageDataType;>;"
    iget-object v0, p0, Lcom/sec/ims/euc/IEUC;->mTo:Landroid/net/Uri;

    return-object v0
.end method

.method protected readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 96
    .local p0, "this":Lcom/sec/ims/euc/IEUC;, "Lcom/sec/ims/euc/IEUC<TMessageDataType;>;"
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/euc/IEUC;->mEUCId:Ljava/lang/String;

    .line 97
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/sec/ims/euc/IEUC;->mFrom:Landroid/net/Uri;

    .line 98
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/sec/ims/euc/IEUC;->mTo:Landroid/net/Uri;

    .line 99
    invoke-virtual {p0, p1}, Lcom/sec/ims/euc/IEUC;->readMessageData(Landroid/os/Parcel;)V

    .line 100
    return-void
.end method

.method protected abstract readMessageData(Landroid/os/Parcel;)V
.end method

.method protected abstract writeMessageData(Landroid/os/Parcel;)V
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 89
    .local p0, "this":Lcom/sec/ims/euc/IEUC;, "Lcom/sec/ims/euc/IEUC<TMessageDataType;>;"
    iget-object v0, p0, Lcom/sec/ims/euc/IEUC;->mEUCId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 90
    iget-object v0, p0, Lcom/sec/ims/euc/IEUC;->mFrom:Landroid/net/Uri;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 91
    iget-object v0, p0, Lcom/sec/ims/euc/IEUC;->mTo:Landroid/net/Uri;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 92
    invoke-virtual {p0, p1}, Lcom/sec/ims/euc/IEUC;->writeMessageData(Landroid/os/Parcel;)V

    .line 93
    return-void
.end method
