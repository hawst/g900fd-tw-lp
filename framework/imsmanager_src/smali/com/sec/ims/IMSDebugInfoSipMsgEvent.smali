.class public Lcom/sec/ims/IMSDebugInfoSipMsgEvent;
.super Ljava/lang/Object;
.source "IMSDebugInfoSipMsgEvent.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/ims/IMSDebugInfoSipMsgEvent;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mDirection:I

.field private final mLocalAddr:Ljava/lang/String;

.field private final mRemoteAddr:Ljava/lang/String;

.field private final mSipMsg:Ljava/lang/String;

.field private final mSipType:I

.field private final mTimestamp:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 86
    new-instance v0, Lcom/sec/ims/IMSDebugInfoSipMsgEvent$1;

    invoke-direct {v0}, Lcom/sec/ims/IMSDebugInfoSipMsgEvent$1;-><init>()V

    sput-object v0, Lcom/sec/ims/IMSDebugInfoSipMsgEvent;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/IMSDebugInfoSipMsgEvent;->mSipMsg:Ljava/lang/String;

    .line 39
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/ims/IMSDebugInfoSipMsgEvent;->mSipType:I

    .line 40
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/ims/IMSDebugInfoSipMsgEvent;->mDirection:I

    .line 41
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/IMSDebugInfoSipMsgEvent;->mTimestamp:Ljava/lang/String;

    .line 42
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/IMSDebugInfoSipMsgEvent;->mLocalAddr:Ljava/lang/String;

    .line 43
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/IMSDebugInfoSipMsgEvent;->mRemoteAddr:Ljava/lang/String;

    .line 44
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "sip"    # Ljava/lang/String;
    .param p2, "type"    # I
    .param p3, "direction"    # I
    .param p4, "timestamp"    # Ljava/lang/String;
    .param p5, "localAddr"    # Ljava/lang/String;
    .param p6, "remoteAddr"    # Ljava/lang/String;

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/sec/ims/IMSDebugInfoSipMsgEvent;->mSipMsg:Ljava/lang/String;

    .line 30
    iput p2, p0, Lcom/sec/ims/IMSDebugInfoSipMsgEvent;->mSipType:I

    .line 31
    iput p3, p0, Lcom/sec/ims/IMSDebugInfoSipMsgEvent;->mDirection:I

    .line 32
    iput-object p4, p0, Lcom/sec/ims/IMSDebugInfoSipMsgEvent;->mTimestamp:Ljava/lang/String;

    .line 33
    iput-object p5, p0, Lcom/sec/ims/IMSDebugInfoSipMsgEvent;->mLocalAddr:Ljava/lang/String;

    .line 34
    iput-object p6, p0, Lcom/sec/ims/IMSDebugInfoSipMsgEvent;->mRemoteAddr:Ljava/lang/String;

    .line 35
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 72
    const/4 v0, 0x0

    return v0
.end method

.method public getLocalAddr()Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/ims/IMSDebugInfoSipMsgEvent;->mLocalAddr:Ljava/lang/String;

    return-object v0
.end method

.method public getRemoteAddr()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/ims/IMSDebugInfoSipMsgEvent;->mRemoteAddr:Ljava/lang/String;

    return-object v0
.end method

.method public getSipDirection()I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lcom/sec/ims/IMSDebugInfoSipMsgEvent;->mDirection:I

    return v0
.end method

.method public getSipMsgInfo()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/ims/IMSDebugInfoSipMsgEvent;->mSipMsg:Ljava/lang/String;

    return-object v0
.end method

.method public getSipTimestamp()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/ims/IMSDebugInfoSipMsgEvent;->mTimestamp:Ljava/lang/String;

    return-object v0
.end method

.method public getSipType()I
    .locals 1

    .prologue
    .line 51
    iget v0, p0, Lcom/sec/ims/IMSDebugInfoSipMsgEvent;->mSipType:I

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/ims/IMSDebugInfoSipMsgEvent;->mSipMsg:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 78
    iget v0, p0, Lcom/sec/ims/IMSDebugInfoSipMsgEvent;->mSipType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 79
    iget v0, p0, Lcom/sec/ims/IMSDebugInfoSipMsgEvent;->mDirection:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 80
    iget-object v0, p0, Lcom/sec/ims/IMSDebugInfoSipMsgEvent;->mTimestamp:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 81
    iget-object v0, p0, Lcom/sec/ims/IMSDebugInfoSipMsgEvent;->mLocalAddr:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 82
    iget-object v0, p0, Lcom/sec/ims/IMSDebugInfoSipMsgEvent;->mRemoteAddr:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 83
    return-void
.end method
