.class public final enum Lcom/sec/ims/im/ImDirection;
.super Ljava/lang/Enum;
.source "ImDirection.java"

# interfaces
.implements Lcom/sec/ims/common/IEnumerationWithId;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/ims/im/ImDirection;",
        ">;",
        "Lcom/sec/ims/common/IEnumerationWithId",
        "<",
        "Lcom/sec/ims/im/ImDirection;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/ims/im/ImDirection;

.field public static final enum INCOMING:Lcom/sec/ims/im/ImDirection;

.field public static final enum IRRELEVANT:Lcom/sec/ims/im/ImDirection;

.field public static final enum OUTGOING:Lcom/sec/ims/im/ImDirection;

.field private static final map:Lcom/sec/ims/common/ReverseEnumMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/sec/ims/common/ReverseEnumMap",
            "<",
            "Lcom/sec/ims/im/ImDirection;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final id:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 25
    new-instance v0, Lcom/sec/ims/im/ImDirection;

    const-string v1, "INCOMING"

    invoke-direct {v0, v1, v2, v2}, Lcom/sec/ims/im/ImDirection;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/ims/im/ImDirection;->INCOMING:Lcom/sec/ims/im/ImDirection;

    .line 30
    new-instance v0, Lcom/sec/ims/im/ImDirection;

    const-string v1, "OUTGOING"

    invoke-direct {v0, v1, v3, v3}, Lcom/sec/ims/im/ImDirection;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/ims/im/ImDirection;->OUTGOING:Lcom/sec/ims/im/ImDirection;

    .line 35
    new-instance v0, Lcom/sec/ims/im/ImDirection;

    const-string v1, "IRRELEVANT"

    invoke-direct {v0, v1, v4, v4}, Lcom/sec/ims/im/ImDirection;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/ims/im/ImDirection;->IRRELEVANT:Lcom/sec/ims/im/ImDirection;

    .line 21
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/ims/im/ImDirection;

    sget-object v1, Lcom/sec/ims/im/ImDirection;->INCOMING:Lcom/sec/ims/im/ImDirection;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/ims/im/ImDirection;->OUTGOING:Lcom/sec/ims/im/ImDirection;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/ims/im/ImDirection;->IRRELEVANT:Lcom/sec/ims/im/ImDirection;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/ims/im/ImDirection;->$VALUES:[Lcom/sec/ims/im/ImDirection;

    .line 39
    new-instance v0, Lcom/sec/ims/common/ReverseEnumMap;

    const-class v1, Lcom/sec/ims/im/ImDirection;

    invoke-direct {v0, v1}, Lcom/sec/ims/common/ReverseEnumMap;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/sec/ims/im/ImDirection;->map:Lcom/sec/ims/common/ReverseEnumMap;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "id"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 43
    iput p3, p0, Lcom/sec/ims/im/ImDirection;->id:I

    .line 44
    return-void
.end method

.method public static fromId(I)Lcom/sec/ims/im/ImDirection;
    .locals 2
    .param p0, "id"    # I

    .prologue
    .line 57
    sget-object v0, Lcom/sec/ims/im/ImDirection;->map:Lcom/sec/ims/common/ReverseEnumMap;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/ims/common/ReverseEnumMap;->get(Ljava/lang/Integer;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/ims/im/ImDirection;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/ims/im/ImDirection;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 21
    const-class v0, Lcom/sec/ims/im/ImDirection;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/ims/im/ImDirection;

    return-object v0
.end method

.method public static values()[Lcom/sec/ims/im/ImDirection;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/sec/ims/im/ImDirection;->$VALUES:[Lcom/sec/ims/im/ImDirection;

    invoke-virtual {v0}, [Lcom/sec/ims/im/ImDirection;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/ims/im/ImDirection;

    return-object v0
.end method


# virtual methods
.method public getFromId(I)Lcom/sec/ims/im/ImDirection;
    .locals 2
    .param p1, "id"    # I

    .prologue
    .line 53
    sget-object v0, Lcom/sec/ims/im/ImDirection;->map:Lcom/sec/ims/common/ReverseEnumMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/ims/common/ReverseEnumMap;->get(Ljava/lang/Integer;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/ims/im/ImDirection;

    return-object v0
.end method

.method public bridge synthetic getFromId(I)Ljava/lang/Enum;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 21
    invoke-virtual {p0, p1}, Lcom/sec/ims/im/ImDirection;->getFromId(I)Lcom/sec/ims/im/ImDirection;

    move-result-object v0

    return-object v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lcom/sec/ims/im/ImDirection;->id:I

    return v0
.end method
