.class public interface abstract Lcom/sec/ims/im/ImConstants;
.super Ljava/lang/Object;
.source "ImConstants.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/ims/im/ImConstants$Config;,
        Lcom/sec/ims/im/ImConstants$Settings;,
        Lcom/sec/ims/im/ImConstants$AutoAcceptFt;,
        Lcom/sec/ims/im/ImConstants$TransferMech;,
        Lcom/sec/ims/im/ImConstants$ServiceTag;,
        Lcom/sec/ims/im/ImConstants$TransferState;,
        Lcom/sec/ims/im/ImConstants$ErrorReason;,
        Lcom/sec/ims/im/ImConstants$SessionState;,
        Lcom/sec/ims/im/ImConstants$SessionType;,
        Lcom/sec/ims/im/ImConstants$ChatState;,
        Lcom/sec/ims/im/ImConstants$ParticipantStatus;,
        Lcom/sec/ims/im/ImConstants$ParticipantType;,
        Lcom/sec/ims/im/ImConstants$MessageStatus;,
        Lcom/sec/ims/im/ImConstants$MessageDispositionNotification;,
        Lcom/sec/ims/im/ImConstants$MessageDisplayedNotificationStatus;,
        Lcom/sec/ims/im/ImConstants$MessageDeliveryNotificationStatus;,
        Lcom/sec/ims/im/ImConstants$MessageNotificationStatus;,
        Lcom/sec/ims/im/ImConstants$MessageType;,
        Lcom/sec/ims/im/ImConstants$ChatDirection;
    }
.end annotation
