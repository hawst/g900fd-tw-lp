.class public Lcom/sec/ims/im/ImIntent$Extras;
.super Ljava/lang/Object;
.source "ImIntent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/ims/im/ImIntent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Extras"
.end annotation


# static fields
.field public static final BOOLEAN_ANSWER:Ljava/lang/String; = "invitation_answer"

.field public static final CHATS_LIST:Ljava/lang/String; = "chats_list"

.field public static final CHAT_ID:Ljava/lang/String; = "chat_id"

.field public static final CHAT_STATUS:Ljava/lang/String; = "chat_status"

.field public static final COMPOSING_URI_LIST:Ljava/lang/String; = "composing_uri_list"

.field public static final CONTENT_TYPE:Ljava/lang/String; = "content_type"

.field public static final DISPOSITION_NOTIFICATION:Ljava/lang/String; = "disposition_notification"

.field public static final ERROR_REASON:Ljava/lang/String; = "error_reason"

.field public static final INTERVAL:Ljava/lang/String; = "interval"

.field public static final IS_AVAILABLE:Ljava/lang/String; = "is_available"

.field public static final IS_GROUP_CHAT:Ljava/lang/String; = "is_group_chat"

.field public static final IS_TYPING:Ljava/lang/String; = "is_typing"

.field public static final LAST_SENT_MESSAGES_STATUS:Ljava/lang/String; = "last_sent_messages_status"

.field public static final MESSAGES_LIMIT:Ljava/lang/String; = "messages_limit"

.field public static final MESSAGES_LIST:Ljava/lang/String; = "messages_list"

.field public static final MESSAGE_BODY:Ljava/lang/String; = "message_body"

.field public static final MESSAGE_DIRECTION:Ljava/lang/String; = "message_direction"

.field public static final MESSAGE_ID:Ljava/lang/String; = "message_id"

.field public static final MESSAGE_IMDN:Ljava/lang/String; = "message_imdn"

.field public static final MESSAGE_NOTIFICATION_STATUS:Ljava/lang/String; = "message_notification_status"

.field public static final MESSAGE_SERVICE:Ljava/lang/String; = "message_service"

.field public static final MESSAGE_SOURCE:Ljava/lang/String; = "message_source"

.field public static final MESSAGE_TYPE:Ljava/lang/String; = "message_type"

.field public static final MUTE_CHAT:Ljava/lang/String; = "mute_chat"

.field public static final PARTICIPANTS_LIST:Ljava/lang/String; = "participants_list"

.field public static final PARTICIPANT_ID:Ljava/lang/String; = "participant_id"

.field public static final PARTICIPANT_STATUS:Ljava/lang/String; = "participant_status"

.field public static final PARTICIPANT_URI:Ljava/lang/String; = "participant"

.field public static final PROGRESS_FAILED_LIST:Ljava/lang/String; = "progress_failed_list"

.field public static final REMOTE_URI:Ljava/lang/String; = "remote_uri"

.field public static final REQUEST_ID:Ljava/lang/String; = "request_id"

.field public static final REQUEST_MESSAGE_ID:Ljava/lang/String; = "request_message_id"

.field public static final REQUEST_SUCCESS:Ljava/lang/String; = "response_status"

.field public static final REQUEST_THREAD_ID:Ljava/lang/String; = "request_thread_id"

.field public static final SUBJECT:Ljava/lang/String; = "subject"


# instance fields
.field SETTING_NAME:Ljava/lang/String;

.field SETTING_STRING_VALUE:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 532
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 702
    const-string v0, "setting_name"

    iput-object v0, p0, Lcom/sec/ims/im/ImIntent$Extras;->SETTING_NAME:Ljava/lang/String;

    .line 710
    const-string v0, "setting_str_val"

    iput-object v0, p0, Lcom/sec/ims/im/ImIntent$Extras;->SETTING_STRING_VALUE:Ljava/lang/String;

    return-void
.end method
