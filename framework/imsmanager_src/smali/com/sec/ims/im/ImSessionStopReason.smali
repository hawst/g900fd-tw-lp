.class public final enum Lcom/sec/ims/im/ImSessionStopReason;
.super Ljava/lang/Enum;
.source "ImSessionStopReason.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/ims/im/ImSessionStopReason;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/ims/im/ImSessionStopReason;

.field public static final enum DEDICATED_BEARER_UNAVAILABLE_TIMEOUT:Lcom/sec/ims/im/ImSessionStopReason;

.field public static final enum INVOLUNTARILY:Lcom/sec/ims/im/ImSessionStopReason;

.field public static final enum VOLUNTARILY:Lcom/sec/ims/im/ImSessionStopReason;


# instance fields
.field private final mId:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 17
    new-instance v0, Lcom/sec/ims/im/ImSessionStopReason;

    const-string v1, "VOLUNTARILY"

    invoke-direct {v0, v1, v2, v2}, Lcom/sec/ims/im/ImSessionStopReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/ims/im/ImSessionStopReason;->VOLUNTARILY:Lcom/sec/ims/im/ImSessionStopReason;

    .line 19
    new-instance v0, Lcom/sec/ims/im/ImSessionStopReason;

    const-string v1, "INVOLUNTARILY"

    invoke-direct {v0, v1, v3, v3}, Lcom/sec/ims/im/ImSessionStopReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/ims/im/ImSessionStopReason;->INVOLUNTARILY:Lcom/sec/ims/im/ImSessionStopReason;

    .line 21
    new-instance v0, Lcom/sec/ims/im/ImSessionStopReason;

    const-string v1, "DEDICATED_BEARER_UNAVAILABLE_TIMEOUT"

    invoke-direct {v0, v1, v4, v4}, Lcom/sec/ims/im/ImSessionStopReason;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/ims/im/ImSessionStopReason;->DEDICATED_BEARER_UNAVAILABLE_TIMEOUT:Lcom/sec/ims/im/ImSessionStopReason;

    .line 14
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/ims/im/ImSessionStopReason;

    sget-object v1, Lcom/sec/ims/im/ImSessionStopReason;->VOLUNTARILY:Lcom/sec/ims/im/ImSessionStopReason;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/ims/im/ImSessionStopReason;->INVOLUNTARILY:Lcom/sec/ims/im/ImSessionStopReason;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/ims/im/ImSessionStopReason;->DEDICATED_BEARER_UNAVAILABLE_TIMEOUT:Lcom/sec/ims/im/ImSessionStopReason;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/ims/im/ImSessionStopReason;->$VALUES:[Lcom/sec/ims/im/ImSessionStopReason;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "id"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 26
    iput p3, p0, Lcom/sec/ims/im/ImSessionStopReason;->mId:I

    .line 27
    return-void
.end method

.method public static valueOf(I)Lcom/sec/ims/im/ImSessionStopReason;
    .locals 6
    .param p0, "id"    # I

    .prologue
    .line 35
    const/4 v4, 0x0

    .line 37
    .local v4, "sessionclosereason":Lcom/sec/ims/im/ImSessionStopReason;
    invoke-static {}, Lcom/sec/ims/im/ImSessionStopReason;->values()[Lcom/sec/ims/im/ImSessionStopReason;

    move-result-object v0

    .local v0, "arr$":[Lcom/sec/ims/im/ImSessionStopReason;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 38
    .local v3, "r":Lcom/sec/ims/im/ImSessionStopReason;
    iget v5, v3, Lcom/sec/ims/im/ImSessionStopReason;->mId:I

    if-ne v5, p0, :cond_1

    .line 39
    move-object v4, v3

    .line 44
    .end local v3    # "r":Lcom/sec/ims/im/ImSessionStopReason;
    :cond_0
    return-object v4

    .line 37
    .restart local v3    # "r":Lcom/sec/ims/im/ImSessionStopReason;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/ims/im/ImSessionStopReason;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 14
    const-class v0, Lcom/sec/ims/im/ImSessionStopReason;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/ims/im/ImSessionStopReason;

    return-object v0
.end method

.method public static values()[Lcom/sec/ims/im/ImSessionStopReason;
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/sec/ims/im/ImSessionStopReason;->$VALUES:[Lcom/sec/ims/im/ImSessionStopReason;

    invoke-virtual {v0}, [Lcom/sec/ims/im/ImSessionStopReason;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/ims/im/ImSessionStopReason;

    return-object v0
.end method


# virtual methods
.method public getId()I
    .locals 1

    .prologue
    .line 30
    iget v0, p0, Lcom/sec/ims/im/ImSessionStopReason;->mId:I

    return v0
.end method
