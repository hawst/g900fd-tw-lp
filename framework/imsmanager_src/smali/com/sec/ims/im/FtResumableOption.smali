.class public final enum Lcom/sec/ims/im/FtResumableOption;
.super Ljava/lang/Enum;
.source "FtResumableOption.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/ims/im/FtResumableOption;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/ims/im/FtResumableOption;

.field public static final enum MANUALLY_AUTOMATICALLY_RESUMABLE:Lcom/sec/ims/im/FtResumableOption;

.field public static final enum MANUALLY_RESUMABLE_ONLY:Lcom/sec/ims/im/FtResumableOption;

.field public static final enum NOTRESUMABLE:Lcom/sec/ims/im/FtResumableOption;


# instance fields
.field private final mId:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 20
    new-instance v0, Lcom/sec/ims/im/FtResumableOption;

    const-string v1, "NOTRESUMABLE"

    invoke-direct {v0, v1, v2, v2}, Lcom/sec/ims/im/FtResumableOption;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/ims/im/FtResumableOption;->NOTRESUMABLE:Lcom/sec/ims/im/FtResumableOption;

    .line 24
    new-instance v0, Lcom/sec/ims/im/FtResumableOption;

    const-string v1, "MANUALLY_RESUMABLE_ONLY"

    invoke-direct {v0, v1, v3, v3}, Lcom/sec/ims/im/FtResumableOption;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/ims/im/FtResumableOption;->MANUALLY_RESUMABLE_ONLY:Lcom/sec/ims/im/FtResumableOption;

    .line 28
    new-instance v0, Lcom/sec/ims/im/FtResumableOption;

    const-string v1, "MANUALLY_AUTOMATICALLY_RESUMABLE"

    invoke-direct {v0, v1, v4, v4}, Lcom/sec/ims/im/FtResumableOption;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/ims/im/FtResumableOption;->MANUALLY_AUTOMATICALLY_RESUMABLE:Lcom/sec/ims/im/FtResumableOption;

    .line 15
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/ims/im/FtResumableOption;

    sget-object v1, Lcom/sec/ims/im/FtResumableOption;->NOTRESUMABLE:Lcom/sec/ims/im/FtResumableOption;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/ims/im/FtResumableOption;->MANUALLY_RESUMABLE_ONLY:Lcom/sec/ims/im/FtResumableOption;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/ims/im/FtResumableOption;->MANUALLY_AUTOMATICALLY_RESUMABLE:Lcom/sec/ims/im/FtResumableOption;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/ims/im/FtResumableOption;->$VALUES:[Lcom/sec/ims/im/FtResumableOption;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "id"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 33
    iput p3, p0, Lcom/sec/ims/im/FtResumableOption;->mId:I

    .line 34
    return-void
.end method

.method public static valueOf(I)Lcom/sec/ims/im/FtResumableOption;
    .locals 6
    .param p0, "id"    # I

    .prologue
    .line 42
    const/4 v4, 0x0

    .line 44
    .local v4, "resumableOption":Lcom/sec/ims/im/FtResumableOption;
    invoke-static {}, Lcom/sec/ims/im/FtResumableOption;->values()[Lcom/sec/ims/im/FtResumableOption;

    move-result-object v0

    .local v0, "arr$":[Lcom/sec/ims/im/FtResumableOption;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 45
    .local v3, "r":Lcom/sec/ims/im/FtResumableOption;
    iget v5, v3, Lcom/sec/ims/im/FtResumableOption;->mId:I

    if-ne v5, p0, :cond_1

    .line 46
    move-object v4, v3

    .line 51
    .end local v3    # "r":Lcom/sec/ims/im/FtResumableOption;
    :cond_0
    return-object v4

    .line 44
    .restart local v3    # "r":Lcom/sec/ims/im/FtResumableOption;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/ims/im/FtResumableOption;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 15
    const-class v0, Lcom/sec/ims/im/FtResumableOption;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/ims/im/FtResumableOption;

    return-object v0
.end method

.method public static values()[Lcom/sec/ims/im/FtResumableOption;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/sec/ims/im/FtResumableOption;->$VALUES:[Lcom/sec/ims/im/FtResumableOption;

    invoke-virtual {v0}, [Lcom/sec/ims/im/FtResumableOption;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/ims/im/FtResumableOption;

    return-object v0
.end method


# virtual methods
.method public getId()I
    .locals 1

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/ims/im/FtResumableOption;->mId:I

    return v0
.end method
