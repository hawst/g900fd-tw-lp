.class Lcom/sec/ims/ImsManager$1;
.super Ljava/lang/Object;
.source "ImsManager.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/ims/ImsManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/ims/ImsManager;


# direct methods
.method constructor <init>(Lcom/sec/ims/ImsManager;)V
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lcom/sec/ims/ImsManager$1;->this$0:Lcom/sec/ims/ImsManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 5
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 48
    const-string v3, "ImsManager"

    const-string v4, "Connected"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    iget-object v3, p0, Lcom/sec/ims/ImsManager$1;->this$0:Lcom/sec/ims/ImsManager;

    invoke-static {p2}, Lcom/sec/ims/IImsService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/ims/IImsService;

    move-result-object v4

    # setter for: Lcom/sec/ims/ImsManager;->mImsService:Lcom/sec/ims/IImsService;
    invoke-static {v3, v4}, Lcom/sec/ims/ImsManager;->access$002(Lcom/sec/ims/ImsManager;Lcom/sec/ims/IImsService;)Lcom/sec/ims/IImsService;

    .line 50
    iget-object v3, p0, Lcom/sec/ims/ImsManager$1;->this$0:Lcom/sec/ims/ImsManager;

    # getter for: Lcom/sec/ims/ImsManager;->mListener:Lcom/sec/ims/ImsManager$ConnectionListener;
    invoke-static {v3}, Lcom/sec/ims/ImsManager;->access$100(Lcom/sec/ims/ImsManager;)Lcom/sec/ims/ImsManager$ConnectionListener;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 51
    iget-object v3, p0, Lcom/sec/ims/ImsManager$1;->this$0:Lcom/sec/ims/ImsManager;

    # getter for: Lcom/sec/ims/ImsManager;->mListener:Lcom/sec/ims/ImsManager$ConnectionListener;
    invoke-static {v3}, Lcom/sec/ims/ImsManager;->access$100(Lcom/sec/ims/ImsManager;)Lcom/sec/ims/ImsManager$ConnectionListener;

    move-result-object v3

    invoke-interface {v3}, Lcom/sec/ims/ImsManager$ConnectionListener;->onConnected()V

    .line 54
    :cond_0
    iget-object v4, p0, Lcom/sec/ims/ImsManager$1;->this$0:Lcom/sec/ims/ImsManager;

    monitor-enter v4

    .line 55
    :try_start_0
    iget-object v3, p0, Lcom/sec/ims/ImsManager$1;->this$0:Lcom/sec/ims/ImsManager;

    # getter for: Lcom/sec/ims/ImsManager;->mPreConnectionRegListeners:Ljava/util/List;
    invoke-static {v3}, Lcom/sec/ims/ImsManager;->access$200(Lcom/sec/ims/ImsManager;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/ims/IImsRegistrationListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 57
    .local v2, "tempListener":Lcom/sec/ims/IImsRegistrationListener;
    :try_start_1
    iget-object v3, p0, Lcom/sec/ims/ImsManager$1;->this$0:Lcom/sec/ims/ImsManager;

    # getter for: Lcom/sec/ims/ImsManager;->mImsService:Lcom/sec/ims/IImsService;
    invoke-static {v3}, Lcom/sec/ims/ImsManager;->access$000(Lcom/sec/ims/ImsManager;)Lcom/sec/ims/IImsService;

    move-result-object v3

    invoke-interface {v3, v2}, Lcom/sec/ims/IImsService;->registerImsRegistrationListener(Lcom/sec/ims/IImsRegistrationListener;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 58
    :catch_0
    move-exception v0

    .line 59
    .local v0, "e":Landroid/os/RemoteException;
    :try_start_2
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 64
    .end local v0    # "e":Landroid/os/RemoteException;
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "tempListener":Lcom/sec/ims/IImsRegistrationListener;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3

    .line 63
    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_1
    :try_start_3
    iget-object v3, p0, Lcom/sec/ims/ImsManager$1;->this$0:Lcom/sec/ims/ImsManager;

    # getter for: Lcom/sec/ims/ImsManager;->mPreConnectionRegListeners:Ljava/util/List;
    invoke-static {v3}, Lcom/sec/ims/ImsManager;->access$200(Lcom/sec/ims/ImsManager;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 64
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 65
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 69
    const-string v0, "ImsManager"

    const-string v1, "Disconnected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    iget-object v0, p0, Lcom/sec/ims/ImsManager$1;->this$0:Lcom/sec/ims/ImsManager;

    const/4 v1, 0x0

    # setter for: Lcom/sec/ims/ImsManager;->mImsService:Lcom/sec/ims/IImsService;
    invoke-static {v0, v1}, Lcom/sec/ims/ImsManager;->access$002(Lcom/sec/ims/ImsManager;Lcom/sec/ims/IImsService;)Lcom/sec/ims/IImsService;

    .line 71
    iget-object v0, p0, Lcom/sec/ims/ImsManager$1;->this$0:Lcom/sec/ims/ImsManager;

    # getter for: Lcom/sec/ims/ImsManager;->mListener:Lcom/sec/ims/ImsManager$ConnectionListener;
    invoke-static {v0}, Lcom/sec/ims/ImsManager;->access$100(Lcom/sec/ims/ImsManager;)Lcom/sec/ims/ImsManager$ConnectionListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lcom/sec/ims/ImsManager$1;->this$0:Lcom/sec/ims/ImsManager;

    # getter for: Lcom/sec/ims/ImsManager;->mListener:Lcom/sec/ims/ImsManager$ConnectionListener;
    invoke-static {v0}, Lcom/sec/ims/ImsManager;->access$100(Lcom/sec/ims/ImsManager;)Lcom/sec/ims/ImsManager$ConnectionListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/ims/ImsManager$ConnectionListener;->onDisconnected()V

    .line 74
    :cond_0
    return-void
.end method
