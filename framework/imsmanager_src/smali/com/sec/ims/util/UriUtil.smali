.class public Lcom/sec/ims/util/UriUtil;
.super Ljava/lang/Object;
.source "UriUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/ims/util/UriUtil$UriType;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    return-void
.end method

.method public static getNetworkDomain(Landroid/net/Uri;)Ljava/lang/String;
    .locals 6
    .param p0, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v3, 0x0

    .line 59
    if-nez p0, :cond_1

    .line 75
    :cond_0
    :goto_0
    return-object v3

    .line 63
    :cond_1
    invoke-static {p0}, Lcom/sec/ims/util/UriUtil;->getUriType(Landroid/net/Uri;)Lcom/sec/ims/util/UriUtil$UriType;

    move-result-object v4

    sget-object v5, Lcom/sec/ims/util/UriUtil$UriType;->TEL_URI:Lcom/sec/ims/util/UriUtil$UriType;

    if-eq v4, v5, :cond_0

    .line 67
    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    .line 69
    .local v2, "uriStr":Ljava/lang/String;
    const/16 v3, 0x40

    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 70
    .local v1, "start":I
    const/16 v3, 0x3b

    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 72
    .local v0, "end":I
    if-gez v0, :cond_2

    .line 73
    add-int/lit8 v3, v1, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 75
    :cond_2
    add-int/lit8 v3, v1, 0x1

    invoke-virtual {v2, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method public static getUriType(Landroid/net/Uri;)Lcom/sec/ims/util/UriUtil$UriType;
    .locals 2
    .param p0, "uri"    # Landroid/net/Uri;

    .prologue
    .line 28
    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    .line 30
    .local v0, "scheme":Ljava/lang/String;
    const-string v1, "sip"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 31
    sget-object v1, Lcom/sec/ims/util/UriUtil$UriType;->SIP_URI:Lcom/sec/ims/util/UriUtil$UriType;

    .line 36
    :goto_0
    return-object v1

    .line 32
    :cond_0
    const-string v1, "tel"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 33
    sget-object v1, Lcom/sec/ims/util/UriUtil$UriType;->TEL_URI:Lcom/sec/ims/util/UriUtil$UriType;

    goto :goto_0

    .line 36
    :cond_1
    sget-object v1, Lcom/sec/ims/util/UriUtil$UriType;->TEL_URI:Lcom/sec/ims/util/UriUtil$UriType;

    goto :goto_0
.end method

.method public static getUserInfo(Landroid/net/Uri;)Ljava/lang/String;
    .locals 5
    .param p0, "uri"    # Landroid/net/Uri;

    .prologue
    .line 40
    if-nez p0, :cond_0

    .line 41
    new-instance v3, Ljava/lang/String;

    const-string v4, ""

    invoke-direct {v3, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 55
    :goto_0
    return-object v3

    .line 44
    :cond_0
    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    .line 48
    .local v1, "s":Ljava/lang/String;
    const/16 v3, 0x3a

    invoke-virtual {v1, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    add-int/lit8 v2, v3, 0x1

    .line 49
    .local v2, "start":I
    const/16 v3, 0x40

    invoke-virtual {v1, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    .line 51
    .local v0, "end":I
    if-ltz v2, :cond_1

    if-gez v0, :cond_2

    .line 52
    :cond_1
    new-instance v3, Ljava/lang/String;

    const-string v4, ""

    invoke-direct {v3, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 55
    :cond_2
    invoke-virtual {v1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method
