.class public Lcom/sec/ims/util/NetworkUtil;
.super Ljava/lang/Object;
.source "NetworkUtil.java"


# static fields
.field private static VALID_IPV4_PATTERN:Ljava/util/regex/Pattern; = null

.field private static VALID_IPV6_PATTERN:Ljava/util/regex/Pattern; = null

.field private static final ipv4Pattern:Ljava/lang/String; = "(([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.){3}([01]?\\d\\d?|2[0-4]\\d|25[0-5])"

.field private static final ipv6Pattern:Ljava/lang/String; = "([0-9a-f]{1,4}:){7}([0-9a-f]){1,4}"


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 20
    sput-object v0, Lcom/sec/ims/util/NetworkUtil;->VALID_IPV4_PATTERN:Ljava/util/regex/Pattern;

    .line 21
    sput-object v0, Lcom/sec/ims/util/NetworkUtil;->VALID_IPV6_PATTERN:Ljava/util/regex/Pattern;

    .line 27
    :try_start_0
    const-string v0, "(([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.){3}([01]?\\d\\d?|2[0-4]\\d|25[0-5])"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/sec/ims/util/NetworkUtil;->VALID_IPV4_PATTERN:Ljava/util/regex/Pattern;

    .line 28
    const-string v0, "([0-9a-f]{1,4}:){7}([0-9a-f]){1,4}"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/sec/ims/util/NetworkUtil;->VALID_IPV6_PATTERN:Ljava/util/regex/Pattern;
    :try_end_0
    .catch Ljava/util/regex/PatternSyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    .line 32
    :goto_0
    return-void

    .line 29
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static isIpv4Address(Ljava/lang/String;)Z
    .locals 2
    .param p0, "ipAddress"    # Ljava/lang/String;

    .prologue
    .line 35
    sget-object v1, Lcom/sec/ims/util/NetworkUtil;->VALID_IPV4_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v1, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 36
    .local v0, "m1":Ljava/util/regex/Matcher;
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    return v1
.end method

.method public static isIpv6Address(Ljava/lang/String;)Z
    .locals 2
    .param p0, "ipAddress"    # Ljava/lang/String;

    .prologue
    .line 40
    sget-object v1, Lcom/sec/ims/util/NetworkUtil;->VALID_IPV6_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v1, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 41
    .local v0, "m2":Ljava/util/regex/Matcher;
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    return v1
.end method
