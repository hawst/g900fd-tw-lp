.class public final enum Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;
.super Ljava/lang/Enum;
.source "TelephonyManagerExt.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/ims/util/TelephonyManagerExt;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "NetworkTypeExt"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

.field public static final enum NETWORK_TYPE_1xRTT:Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

.field public static final enum NETWORK_TYPE_CDMA:Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

.field public static final enum NETWORK_TYPE_DC:Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

.field public static final enum NETWORK_TYPE_EDGE:Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

.field public static final enum NETWORK_TYPE_EHRPD:Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

.field public static final enum NETWORK_TYPE_EVDO_0:Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

.field public static final enum NETWORK_TYPE_EVDO_A:Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

.field public static final enum NETWORK_TYPE_EVDO_B:Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

.field public static final enum NETWORK_TYPE_GPRS:Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

.field public static final enum NETWORK_TYPE_GSM:Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

.field public static final enum NETWORK_TYPE_HSDPA:Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

.field public static final enum NETWORK_TYPE_HSPA:Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

.field public static final enum NETWORK_TYPE_HSPAP:Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

.field public static final enum NETWORK_TYPE_HSUPA:Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

.field public static final enum NETWORK_TYPE_IDEN:Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

.field public static final enum NETWORK_TYPE_IWLAN:Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

.field public static final enum NETWORK_TYPE_LTE:Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

.field public static final enum NETWORK_TYPE_TDLTE:Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

.field public static final enum NETWORK_TYPE_TD_SCDMA:Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

.field public static final enum NETWORK_TYPE_UMTS:Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

.field public static final enum NETWORK_TYPE_UNKNOWN:Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;


# instance fields
.field private final mValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 27
    new-instance v0, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    const-string v1, "NETWORK_TYPE_UNKNOWN"

    invoke-direct {v0, v1, v4, v4}, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;->NETWORK_TYPE_UNKNOWN:Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    .line 28
    new-instance v0, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    const-string v1, "NETWORK_TYPE_GPRS"

    invoke-direct {v0, v1, v5, v5}, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;->NETWORK_TYPE_GPRS:Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    .line 29
    new-instance v0, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    const-string v1, "NETWORK_TYPE_EDGE"

    invoke-direct {v0, v1, v6, v6}, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;->NETWORK_TYPE_EDGE:Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    .line 30
    new-instance v0, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    const-string v1, "NETWORK_TYPE_UMTS"

    invoke-direct {v0, v1, v7, v7}, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;->NETWORK_TYPE_UMTS:Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    .line 31
    new-instance v0, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    const-string v1, "NETWORK_TYPE_CDMA"

    invoke-direct {v0, v1, v8, v8}, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;->NETWORK_TYPE_CDMA:Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    .line 32
    new-instance v0, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    const-string v1, "NETWORK_TYPE_EVDO_0"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;->NETWORK_TYPE_EVDO_0:Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    .line 33
    new-instance v0, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    const-string v1, "NETWORK_TYPE_EVDO_A"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;->NETWORK_TYPE_EVDO_A:Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    .line 34
    new-instance v0, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    const-string v1, "NETWORK_TYPE_1xRTT"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;->NETWORK_TYPE_1xRTT:Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    .line 35
    new-instance v0, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    const-string v1, "NETWORK_TYPE_HSDPA"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;->NETWORK_TYPE_HSDPA:Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    .line 36
    new-instance v0, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    const-string v1, "NETWORK_TYPE_HSUPA"

    const/16 v2, 0x9

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;->NETWORK_TYPE_HSUPA:Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    .line 37
    new-instance v0, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    const-string v1, "NETWORK_TYPE_HSPA"

    const/16 v2, 0xa

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;->NETWORK_TYPE_HSPA:Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    .line 38
    new-instance v0, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    const-string v1, "NETWORK_TYPE_IDEN"

    const/16 v2, 0xb

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;->NETWORK_TYPE_IDEN:Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    .line 39
    new-instance v0, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    const-string v1, "NETWORK_TYPE_EVDO_B"

    const/16 v2, 0xc

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;->NETWORK_TYPE_EVDO_B:Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    .line 40
    new-instance v0, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    const-string v1, "NETWORK_TYPE_LTE"

    const/16 v2, 0xd

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;->NETWORK_TYPE_LTE:Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    .line 41
    new-instance v0, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    const-string v1, "NETWORK_TYPE_EHRPD"

    const/16 v2, 0xe

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;->NETWORK_TYPE_EHRPD:Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    .line 42
    new-instance v0, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    const-string v1, "NETWORK_TYPE_HSPAP"

    const/16 v2, 0xf

    const/16 v3, 0xf

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;->NETWORK_TYPE_HSPAP:Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    .line 43
    new-instance v0, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    const-string v1, "NETWORK_TYPE_GSM"

    const/16 v2, 0x10

    const/16 v3, 0x10

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;->NETWORK_TYPE_GSM:Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    .line 44
    new-instance v0, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    const-string v1, "NETWORK_TYPE_TD_SCDMA"

    const/16 v2, 0x11

    const/16 v3, 0x11

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;->NETWORK_TYPE_TD_SCDMA:Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    .line 45
    new-instance v0, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    const-string v1, "NETWORK_TYPE_IWLAN"

    const/16 v2, 0x12

    sget v3, Lcom/sec/ims/util/TelephonyManagerExt;->NETWORK_TYPE_IWLAN:I

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;->NETWORK_TYPE_IWLAN:Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    .line 46
    new-instance v0, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    const-string v1, "NETWORK_TYPE_DC"

    const/16 v2, 0x13

    sget v3, Lcom/sec/ims/util/TelephonyManagerExt;->NETWORK_TYPE_DC:I

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;->NETWORK_TYPE_DC:Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    .line 47
    new-instance v0, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    const-string v1, "NETWORK_TYPE_TDLTE"

    const/16 v2, 0x14

    sget v3, Lcom/sec/ims/util/TelephonyManagerExt;->NETWORK_TYPE_TDLTE:I

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;->NETWORK_TYPE_TDLTE:Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    .line 26
    const/16 v0, 0x15

    new-array v0, v0, [Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    sget-object v1, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;->NETWORK_TYPE_UNKNOWN:Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;->NETWORK_TYPE_GPRS:Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;->NETWORK_TYPE_EDGE:Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;->NETWORK_TYPE_UMTS:Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;->NETWORK_TYPE_CDMA:Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;->NETWORK_TYPE_EVDO_0:Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;->NETWORK_TYPE_EVDO_A:Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;->NETWORK_TYPE_1xRTT:Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;->NETWORK_TYPE_HSDPA:Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;->NETWORK_TYPE_HSUPA:Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;->NETWORK_TYPE_HSPA:Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;->NETWORK_TYPE_IDEN:Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;->NETWORK_TYPE_EVDO_B:Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;->NETWORK_TYPE_LTE:Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;->NETWORK_TYPE_EHRPD:Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;->NETWORK_TYPE_HSPAP:Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;->NETWORK_TYPE_GSM:Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;->NETWORK_TYPE_TD_SCDMA:Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;->NETWORK_TYPE_IWLAN:Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;->NETWORK_TYPE_DC:Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;->NETWORK_TYPE_TDLTE:Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;->$VALUES:[Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 52
    iput p3, p0, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;->mValue:I

    .line 53
    return-void
.end method

.method static synthetic access$000(I)Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;
    .locals 1
    .param p0, "x0"    # I

    .prologue
    .line 26
    invoke-static {p0}, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;->valueOf(I)Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    move-result-object v0

    return-object v0
.end method

.method private static valueOf(I)Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;
    .locals 6
    .param p0, "value"    # I

    .prologue
    .line 56
    sget-object v4, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;->NETWORK_TYPE_UNKNOWN:Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    .line 57
    .local v4, "result":Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;
    invoke-static {}, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;->values()[Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    move-result-object v0

    .local v0, "arr$":[Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 58
    .local v3, "networkTypeExt":Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;
    iget v5, v3, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;->mValue:I

    if-ne v5, p0, :cond_1

    .line 59
    move-object v4, v3

    .line 64
    .end local v3    # "networkTypeExt":Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;
    :cond_0
    return-object v4

    .line 57
    .restart local v3    # "networkTypeExt":Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 26
    const-class v0, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    return-object v0
.end method

.method public static values()[Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;->$VALUES:[Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    invoke-virtual {v0}, [Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;

    return-object v0
.end method


# virtual methods
.method public toInt()I
    .locals 1

    .prologue
    .line 68
    iget v0, p0, Lcom/sec/ims/util/TelephonyManagerExt$NetworkTypeExt;->mValue:I

    return v0
.end method
