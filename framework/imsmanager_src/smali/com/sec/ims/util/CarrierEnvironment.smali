.class public Lcom/sec/ims/util/CarrierEnvironment;
.super Ljava/lang/Object;
.source "CarrierEnvironment.java"


# static fields
.field private static CSC_SALES_CODE:Ljava/lang/String;

.field private static LOG_TAG:Ljava/lang/String;

.field private static mSalesCodeDefault:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-class v0, Lcom/sec/ims/util/CarrierEnvironment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/ims/util/CarrierEnvironment;->LOG_TAG:Ljava/lang/String;

    .line 17
    const-string v0, "ro.csc.sales_code"

    sput-object v0, Lcom/sec/ims/util/CarrierEnvironment;->CSC_SALES_CODE:Ljava/lang/String;

    .line 18
    const-string v0, ""

    sput-object v0, Lcom/sec/ims/util/CarrierEnvironment;->mSalesCodeDefault:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    return-void
.end method

.method public static getCurrentCarrier()Ljava/lang/String;
    .locals 2

    .prologue
    .line 32
    sget-object v0, Lcom/sec/ims/util/CarrierEnvironment;->mSalesCodeDefault:Ljava/lang/String;

    .line 35
    .local v0, "currCarrier":Ljava/lang/String;
    const-string v1, "TFN"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 36
    const-string v0, "VZW"

    .line 40
    .end local v0    # "currCarrier":Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method public static getCurrentCarrierFromCsc()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lcom/sec/ims/util/CarrierEnvironment;->mSalesCodeDefault:Ljava/lang/String;

    return-object v0
.end method
