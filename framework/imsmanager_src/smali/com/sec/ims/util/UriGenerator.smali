.class public abstract Lcom/sec/ims/util/UriGenerator;
.super Ljava/lang/Object;
.source "UriGenerator.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract extractOwnAreaCode(Ljava/lang/String;)V
.end method

.method public abstract getNetworkPreferredUri(Landroid/net/Uri;)Landroid/net/Uri;
.end method

.method public abstract getNetworkPreferredUri(Ljava/util/Set;)Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Landroid/net/Uri;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getNormalizedUri(Ljava/lang/String;)Landroid/net/Uri;
.end method

.method public abstract normalize(Landroid/net/Uri;)Landroid/net/Uri;
.end method
