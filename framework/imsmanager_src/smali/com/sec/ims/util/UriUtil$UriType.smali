.class public final enum Lcom/sec/ims/util/UriUtil$UriType;
.super Ljava/lang/Enum;
.source "UriUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/ims/util/UriUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "UriType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/ims/util/UriUtil$UriType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/ims/util/UriUtil$UriType;

.field public static final enum SIP_URI:Lcom/sec/ims/util/UriUtil$UriType;

.field public static final enum TEL_URI:Lcom/sec/ims/util/UriUtil$UriType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 20
    new-instance v0, Lcom/sec/ims/util/UriUtil$UriType;

    const-string v1, "TEL_URI"

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/util/UriUtil$UriType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/util/UriUtil$UriType;->TEL_URI:Lcom/sec/ims/util/UriUtil$UriType;

    new-instance v0, Lcom/sec/ims/util/UriUtil$UriType;

    const-string v1, "SIP_URI"

    invoke-direct {v0, v1, v3}, Lcom/sec/ims/util/UriUtil$UriType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/util/UriUtil$UriType;->SIP_URI:Lcom/sec/ims/util/UriUtil$UriType;

    .line 19
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/ims/util/UriUtil$UriType;

    sget-object v1, Lcom/sec/ims/util/UriUtil$UriType;->TEL_URI:Lcom/sec/ims/util/UriUtil$UriType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/ims/util/UriUtil$UriType;->SIP_URI:Lcom/sec/ims/util/UriUtil$UriType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/ims/util/UriUtil$UriType;->$VALUES:[Lcom/sec/ims/util/UriUtil$UriType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/ims/util/UriUtil$UriType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 19
    const-class v0, Lcom/sec/ims/util/UriUtil$UriType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/ims/util/UriUtil$UriType;

    return-object v0
.end method

.method public static values()[Lcom/sec/ims/util/UriUtil$UriType;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/sec/ims/util/UriUtil$UriType;->$VALUES:[Lcom/sec/ims/util/UriUtil$UriType;

    invoke-virtual {v0}, [Lcom/sec/ims/util/UriUtil$UriType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/ims/util/UriUtil$UriType;

    return-object v0
.end method
