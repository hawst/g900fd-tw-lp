.class public final enum Lcom/sec/ims/NetInterfaceType;
.super Ljava/lang/Enum;
.source "NetInterfaceType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/ims/NetInterfaceType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/ims/NetInterfaceType;

.field public static final enum MOBILE:Lcom/sec/ims/NetInterfaceType;

.field public static final enum MOBILE_EPDN:Lcom/sec/ims/NetInterfaceType;

.field public static final enum MOBILE_IMS:Lcom/sec/ims/NetInterfaceType;

.field public static final enum WIFI:Lcom/sec/ims/NetInterfaceType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 19
    new-instance v0, Lcom/sec/ims/NetInterfaceType;

    const-string v1, "MOBILE"

    invoke-direct {v0, v1, v2}, Lcom/sec/ims/NetInterfaceType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/NetInterfaceType;->MOBILE:Lcom/sec/ims/NetInterfaceType;

    .line 24
    new-instance v0, Lcom/sec/ims/NetInterfaceType;

    const-string v1, "MOBILE_IMS"

    invoke-direct {v0, v1, v3}, Lcom/sec/ims/NetInterfaceType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/NetInterfaceType;->MOBILE_IMS:Lcom/sec/ims/NetInterfaceType;

    .line 29
    new-instance v0, Lcom/sec/ims/NetInterfaceType;

    const-string v1, "WIFI"

    invoke-direct {v0, v1, v4}, Lcom/sec/ims/NetInterfaceType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/NetInterfaceType;->WIFI:Lcom/sec/ims/NetInterfaceType;

    .line 34
    new-instance v0, Lcom/sec/ims/NetInterfaceType;

    const-string v1, "MOBILE_EPDN"

    invoke-direct {v0, v1, v5}, Lcom/sec/ims/NetInterfaceType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/ims/NetInterfaceType;->MOBILE_EPDN:Lcom/sec/ims/NetInterfaceType;

    .line 15
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/ims/NetInterfaceType;

    sget-object v1, Lcom/sec/ims/NetInterfaceType;->MOBILE:Lcom/sec/ims/NetInterfaceType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/ims/NetInterfaceType;->MOBILE_IMS:Lcom/sec/ims/NetInterfaceType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/ims/NetInterfaceType;->WIFI:Lcom/sec/ims/NetInterfaceType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/ims/NetInterfaceType;->MOBILE_EPDN:Lcom/sec/ims/NetInterfaceType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/ims/NetInterfaceType;->$VALUES:[Lcom/sec/ims/NetInterfaceType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 15
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/ims/NetInterfaceType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 15
    const-class v0, Lcom/sec/ims/NetInterfaceType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/ims/NetInterfaceType;

    return-object v0
.end method

.method public static values()[Lcom/sec/ims/NetInterfaceType;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/sec/ims/NetInterfaceType;->$VALUES:[Lcom/sec/ims/NetInterfaceType;

    invoke-virtual {v0}, [Lcom/sec/ims/NetInterfaceType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/ims/NetInterfaceType;

    return-object v0
.end method
