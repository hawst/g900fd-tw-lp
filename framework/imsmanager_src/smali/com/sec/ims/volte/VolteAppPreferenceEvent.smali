.class public Lcom/sec/ims/volte/VolteAppPreferenceEvent;
.super Lcom/sec/ims/volte/VoLTEEvent;
.source "VolteAppPreferenceEvent.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/ims/volte/VolteAppPreferenceEvent;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mAppType:I

.field private final mIsVolteUnSupported:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    new-instance v0, Lcom/sec/ims/volte/VolteAppPreferenceEvent$1;

    invoke-direct {v0}, Lcom/sec/ims/volte/VolteAppPreferenceEvent$1;-><init>()V

    sput-object v0, Lcom/sec/ims/volte/VolteAppPreferenceEvent;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(II)V
    .locals 0
    .param p1, "appType"    # I
    .param p2, "isVolteUnSupported"    # I

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/sec/ims/volte/VoLTEEvent;-><init>()V

    .line 23
    iput p1, p0, Lcom/sec/ims/volte/VolteAppPreferenceEvent;->mAppType:I

    .line 24
    iput p2, p0, Lcom/sec/ims/volte/VolteAppPreferenceEvent;->mIsVolteUnSupported:I

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/sec/ims/volte/VoLTEEvent;-><init>()V

    .line 28
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/ims/volte/VolteAppPreferenceEvent;->mAppType:I

    .line 29
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/ims/volte/VolteAppPreferenceEvent;->mIsVolteUnSupported:I

    .line 30
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    return v0
.end method

.method public getAppType()I
    .locals 1

    .prologue
    .line 37
    iget v0, p0, Lcom/sec/ims/volte/VolteAppPreferenceEvent;->mAppType:I

    return v0
.end method

.method public getVolteUnSupported()I
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lcom/sec/ims/volte/VolteAppPreferenceEvent;->mIsVolteUnSupported:I

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 48
    iget v0, p0, Lcom/sec/ims/volte/VolteAppPreferenceEvent;->mAppType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 49
    iget v0, p0, Lcom/sec/ims/volte/VolteAppPreferenceEvent;->mIsVolteUnSupported:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 50
    return-void
.end method
