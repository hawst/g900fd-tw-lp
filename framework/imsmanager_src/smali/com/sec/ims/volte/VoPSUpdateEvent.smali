.class public Lcom/sec/ims/volte/VoPSUpdateEvent;
.super Lcom/sec/ims/EngineEvent;
.source "VoPSUpdateEvent.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/ims/volte/VoPSUpdateEvent;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final isVoPSEnabled:Z

.field final mCurrentVoPSSysPropValue:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 62
    new-instance v0, Lcom/sec/ims/volte/VoPSUpdateEvent$1;

    invoke-direct {v0}, Lcom/sec/ims/volte/VoPSUpdateEvent$1;-><init>()V

    sput-object v0, Lcom/sec/ims/volte/VoPSUpdateEvent;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v0, 0x1

    .line 74
    invoke-direct {p0}, Lcom/sec/ims/EngineEvent;-><init>()V

    .line 75
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/sec/ims/volte/VoPSUpdateEvent;->isVoPSEnabled:Z

    .line 76
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/ims/volte/VoPSUpdateEvent;->mCurrentVoPSSysPropValue:Ljava/lang/String;

    .line 77
    return-void

    .line 75
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/ims/volte/VoPSUpdateEvent$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/sec/ims/volte/VoPSUpdateEvent$1;

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/sec/ims/volte/VoPSUpdateEvent;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(ZLjava/lang/String;)V
    .locals 0
    .param p1, "isVoPSEnabled"    # Z
    .param p2, "currentVoPSValue"    # Ljava/lang/String;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/sec/ims/EngineEvent;-><init>()V

    .line 30
    iput-boolean p1, p0, Lcom/sec/ims/volte/VoPSUpdateEvent;->isVoPSEnabled:Z

    .line 31
    iput-object p2, p0, Lcom/sec/ims/volte/VoPSUpdateEvent;->mCurrentVoPSSysPropValue:Ljava/lang/String;

    .line 32
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x0

    return v0
.end method

.method public getVoPSSystemProperty()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/ims/volte/VoPSUpdateEvent;->mCurrentVoPSSysPropValue:Ljava/lang/String;

    return-object v0
.end method

.method public isVoPSEnabled()Z
    .locals 1

    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/sec/ims/volte/VoPSUpdateEvent;->isVoPSEnabled:Z

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 58
    iget-boolean v0, p0, Lcom/sec/ims/volte/VoPSUpdateEvent;->isVoPSEnabled:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 59
    iget-object v0, p0, Lcom/sec/ims/volte/VoPSUpdateEvent;->mCurrentVoPSSysPropValue:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 60
    return-void

    .line 58
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
