.class public abstract Lcom/sec/ims/volte/VoLTEEngineSessionEvent;
.super Ljava/lang/Object;
.source "VoLTEEngineSessionEvent.java"

# interfaces
.implements Landroid/os/Parcelable;


# instance fields
.field private final mAppType:I

.field private final mEventType:I

.field private final mSessionID:I

.field private final mSubEventType:I


# direct methods
.method public constructor <init>(IIII)V
    .locals 0
    .param p1, "appType"    # I
    .param p2, "evntType"    # I
    .param p3, "sessionID"    # I
    .param p4, "subEventType"    # I

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput p1, p0, Lcom/sec/ims/volte/VoLTEEngineSessionEvent;->mAppType:I

    .line 27
    iput p2, p0, Lcom/sec/ims/volte/VoLTEEngineSessionEvent;->mEventType:I

    .line 28
    iput p3, p0, Lcom/sec/ims/volte/VoLTEEngineSessionEvent;->mSessionID:I

    .line 29
    iput p4, p0, Lcom/sec/ims/volte/VoLTEEngineSessionEvent;->mSubEventType:I

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/ims/volte/VoLTEEngineSessionEvent;->mAppType:I

    .line 34
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/ims/volte/VoLTEEngineSessionEvent;->mEventType:I

    .line 35
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/ims/volte/VoLTEEngineSessionEvent;->mSessionID:I

    .line 36
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/ims/volte/VoLTEEngineSessionEvent;->mSubEventType:I

    .line 37
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x0

    return v0
.end method

.method public getAppType()I
    .locals 1

    .prologue
    .line 40
    iget v0, p0, Lcom/sec/ims/volte/VoLTEEngineSessionEvent;->mAppType:I

    return v0
.end method

.method public getEventType()I
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lcom/sec/ims/volte/VoLTEEngineSessionEvent;->mEventType:I

    return v0
.end method

.method public getSessionID()I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lcom/sec/ims/volte/VoLTEEngineSessionEvent;->mSessionID:I

    return v0
.end method

.method public getSubEventType()I
    .locals 1

    .prologue
    .line 52
    iget v0, p0, Lcom/sec/ims/volte/VoLTEEngineSessionEvent;->mSubEventType:I

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 63
    iget v0, p0, Lcom/sec/ims/volte/VoLTEEngineSessionEvent;->mAppType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 64
    iget v0, p0, Lcom/sec/ims/volte/VoLTEEngineSessionEvent;->mEventType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 65
    iget v0, p0, Lcom/sec/ims/volte/VoLTEEngineSessionEvent;->mSessionID:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 66
    iget v0, p0, Lcom/sec/ims/volte/VoLTEEngineSessionEvent;->mSubEventType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 67
    return-void
.end method
