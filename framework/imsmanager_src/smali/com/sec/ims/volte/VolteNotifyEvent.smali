.class public Lcom/sec/ims/volte/VolteNotifyEvent;
.super Lcom/sec/ims/volte/VoLTEEngineSessionEvent;
.source "VolteNotifyEvent.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/ims/volte/VolteNotifyEvent;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mMaxParticipantsSize:I

.field private final mParticipantList:[I

.field private final mURIList:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 83
    new-instance v0, Lcom/sec/ims/volte/VolteNotifyEvent$1;

    invoke-direct {v0}, Lcom/sec/ims/volte/VolteNotifyEvent$1;-><init>()V

    sput-object v0, Lcom/sec/ims/volte/VolteNotifyEvent;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(IIII[I[Ljava/lang/String;I)V
    .locals 0
    .param p1, "appType"    # I
    .param p2, "evntType"    # I
    .param p3, "sessionID"    # I
    .param p4, "subEventType"    # I
    .param p5, "participantList"    # [I
    .param p6, "uriList"    # [Ljava/lang/String;
    .param p7, "maxParticipantsSize"    # I

    .prologue
    .line 25
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/ims/volte/VoLTEEngineSessionEvent;-><init>(IIII)V

    .line 27
    iput-object p5, p0, Lcom/sec/ims/volte/VolteNotifyEvent;->mParticipantList:[I

    .line 28
    iput-object p6, p0, Lcom/sec/ims/volte/VolteNotifyEvent;->mURIList:[Ljava/lang/String;

    .line 29
    iput p7, p0, Lcom/sec/ims/volte/VolteNotifyEvent;->mMaxParticipantsSize:I

    .line 30
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/sec/ims/volte/VoLTEEngineSessionEvent;-><init>(Landroid/os/Parcel;)V

    .line 34
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 35
    .local v0, "mParticipantListLen":I
    if-lez v0, :cond_0

    .line 37
    new-array v1, v0, [I

    .line 38
    .local v1, "mParticipantListTemp":[I
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readIntArray([I)V

    .line 39
    iput-object v1, p0, Lcom/sec/ims/volte/VolteNotifyEvent;->mParticipantList:[I

    .line 45
    .end local v1    # "mParticipantListTemp":[I
    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readStringArray()[Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/ims/volte/VolteNotifyEvent;->mURIList:[Ljava/lang/String;

    .line 46
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    iput v2, p0, Lcom/sec/ims/volte/VolteNotifyEvent;->mMaxParticipantsSize:I

    .line 47
    return-void

    .line 43
    :cond_0
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/ims/volte/VolteNotifyEvent;->mParticipantList:[I

    goto :goto_0
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/ims/volte/VolteNotifyEvent$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/sec/ims/volte/VolteNotifyEvent$1;

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/sec/ims/volte/VolteNotifyEvent;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 63
    invoke-super {p0}, Lcom/sec/ims/volte/VoLTEEngineSessionEvent;->describeContents()I

    .line 64
    const/4 v0, 0x0

    return v0
.end method

.method public getMaxParticipantsSize()I
    .locals 1

    .prologue
    .line 58
    iget v0, p0, Lcom/sec/ims/volte/VolteNotifyEvent;->mMaxParticipantsSize:I

    return v0
.end method

.method public getParticipantList()[I
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/ims/volte/VolteNotifyEvent;->mParticipantList:[I

    return-object v0
.end method

.method public getURIList()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/ims/volte/VolteNotifyEvent;->mURIList:[Ljava/lang/String;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 69
    invoke-super {p0, p1, p2}, Lcom/sec/ims/volte/VoLTEEngineSessionEvent;->writeToParcel(Landroid/os/Parcel;I)V

    .line 70
    iget-object v0, p0, Lcom/sec/ims/volte/VolteNotifyEvent;->mParticipantList:[I

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lcom/sec/ims/volte/VolteNotifyEvent;->mParticipantList:[I

    array-length v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 73
    iget-object v0, p0, Lcom/sec/ims/volte/VolteNotifyEvent;->mParticipantList:[I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeIntArray([I)V

    .line 79
    :goto_0
    iget-object v0, p0, Lcom/sec/ims/volte/VolteNotifyEvent;->mURIList:[Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    .line 80
    iget v0, p0, Lcom/sec/ims/volte/VolteNotifyEvent;->mMaxParticipantsSize:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 81
    return-void

    .line 77
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0
.end method
