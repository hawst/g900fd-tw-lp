.class Lcom/google/android/maps/MapView$Repainter;
.super Ljava/lang/Object;
.source "MapView.java"

# interfaces
.implements Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/maps/MapView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "Repainter"
.end annotation


# instance fields
.field private mThread:Ljava/lang/Thread;

.field final synthetic this$0:Lcom/google/android/maps/MapView;


# direct methods
.method constructor <init>(Lcom/google/android/maps/MapView;)V
    .locals 0

    .prologue
    .line 414
    iput-object p1, p0, Lcom/google/android/maps/MapView$Repainter;->this$0:Lcom/google/android/maps/MapView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onComplete(Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;)V
    .locals 1
    .param p1, "dataRequest"    # Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;

    .prologue
    .line 444
    invoke-interface {p1}, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;->isImmediate()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 445
    invoke-virtual {p0}, Lcom/google/android/maps/MapView$Repainter;->repaint()V

    .line 447
    :cond_0
    return-void
.end method

.method public onNetworkError(IZLjava/lang/String;)V
    .locals 0
    .param p1, "errorCode"    # I
    .param p2, "networkEverWorked"    # Z
    .param p3, "debugMessage"    # Ljava/lang/String;

    .prologue
    .line 451
    return-void
.end method

.method repaint()V
    .locals 1

    .prologue
    .line 423
    iget-object v0, p0, Lcom/google/android/maps/MapView$Repainter;->mThread:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/MapView$Repainter;->mThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-nez v0, :cond_1

    .line 424
    :cond_0
    new-instance v0, Lcom/google/android/maps/MapView$Repainter$1;

    invoke-direct {v0, p0}, Lcom/google/android/maps/MapView$Repainter$1;-><init>(Lcom/google/android/maps/MapView$Repainter;)V

    iput-object v0, p0, Lcom/google/android/maps/MapView$Repainter;->mThread:Ljava/lang/Thread;

    .line 434
    iget-object v0, p0, Lcom/google/android/maps/MapView$Repainter;->mThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 436
    :cond_1
    return-void
.end method
