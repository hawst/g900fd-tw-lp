.class public final Lcom/google/android/maps/MapController;
.super Ljava/lang/Object;
.source "MapController.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/maps/MapController$1;,
        Lcom/google/android/maps/MapController$VertPanState;,
        Lcom/google/android/maps/MapController$HorizPanState;
    }
.end annotation


# static fields
.field private static final EMPTY_TRANSFORM:Landroid/view/animation/Transformation;


# instance fields
.field private mAnimationCompletedMessage:Landroid/os/Message;

.field private mAnimationCompletedRunnable:Ljava/lang/Runnable;

.field private mDeferredLatSpanE6:I

.field private mDeferredLonSpanE6:I

.field private final mDeltas:[I

.field private volatile mDirty:Z

.field private mHasBeenMeasured:Z

.field private mHorizPan:Lcom/google/android/maps/MapController$HorizPanState;

.field private final mMap:Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;

.field private mMapView:Lcom/google/android/maps/MapView;

.field private final mOrigin:[F

.field private mPanAnimation:Landroid/view/animation/Animation;

.field private mPanPoint:Landroid_maps_conflict_avoidance/com/google/map/MapPoint;

.field private mVertPan:Lcom/google/android/maps/MapController$VertPanState;

.field private mXPanSpeed:F

.field private mYPanSpeed:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 57
    new-instance v0, Landroid/view/animation/Transformation;

    invoke-direct {v0}, Landroid/view/animation/Transformation;-><init>()V

    sput-object v0, Lcom/google/android/maps/MapController;->EMPTY_TRANSFORM:Landroid/view/animation/Transformation;

    return-void
.end method

.method constructor <init>(Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;Lcom/google/android/maps/MapView;)V
    .locals 5
    .param p1, "map"    # Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;
    .param p2, "mapView"    # Lcom/google/android/maps/MapView;

    .prologue
    const/4 v4, 0x2

    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    new-array v0, v4, [F

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/google/android/maps/MapController;->mOrigin:[F

    .line 46
    new-array v0, v4, [I

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/google/android/maps/MapController;->mDeltas:[I

    .line 47
    sget-object v0, Lcom/google/android/maps/MapController$HorizPanState;->NONE:Lcom/google/android/maps/MapController$HorizPanState;

    iput-object v0, p0, Lcom/google/android/maps/MapController;->mHorizPan:Lcom/google/android/maps/MapController$HorizPanState;

    .line 48
    sget-object v0, Lcom/google/android/maps/MapController$VertPanState;->NONE:Lcom/google/android/maps/MapController$VertPanState;

    iput-object v0, p0, Lcom/google/android/maps/MapController;->mVertPan:Lcom/google/android/maps/MapController$VertPanState;

    .line 49
    iput v2, p0, Lcom/google/android/maps/MapController;->mXPanSpeed:F

    .line 50
    iput v2, p0, Lcom/google/android/maps/MapController;->mYPanSpeed:F

    .line 51
    iput-object v1, p0, Lcom/google/android/maps/MapController;->mPanAnimation:Landroid/view/animation/Animation;

    .line 52
    iput-object v1, p0, Lcom/google/android/maps/MapController;->mPanPoint:Landroid_maps_conflict_avoidance/com/google/map/MapPoint;

    .line 54
    iput-object v1, p0, Lcom/google/android/maps/MapController;->mMapView:Lcom/google/android/maps/MapView;

    .line 55
    iput-object v1, p0, Lcom/google/android/maps/MapController;->mAnimationCompletedMessage:Landroid/os/Message;

    .line 56
    iput-object v1, p0, Lcom/google/android/maps/MapController;->mAnimationCompletedRunnable:Ljava/lang/Runnable;

    .line 62
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/MapController;->mHasBeenMeasured:Z

    .line 63
    iput v3, p0, Lcom/google/android/maps/MapController;->mDeferredLatSpanE6:I

    .line 64
    iput v3, p0, Lcom/google/android/maps/MapController;->mDeferredLonSpanE6:I

    .line 76
    iput-object p1, p0, Lcom/google/android/maps/MapController;->mMap:Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;

    .line 77
    iput-object p2, p0, Lcom/google/android/maps/MapController;->mMapView:Lcom/google/android/maps/MapView;

    .line 78
    return-void

    .line 45
    nop

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data

    .line 46
    :array_1
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method private animateTo(Lcom/google/android/maps/GeoPoint;Ljava/lang/Runnable;Landroid/os/Message;)V
    .locals 12
    .param p1, "point"    # Lcom/google/android/maps/GeoPoint;
    .param p2, "runnable"    # Ljava/lang/Runnable;
    .param p3, "message"    # Landroid/os/Message;

    .prologue
    const v11, 0x49742400    # 1000000.0f

    const/4 v10, 0x0

    .line 230
    iput-object p2, p0, Lcom/google/android/maps/MapController;->mAnimationCompletedRunnable:Ljava/lang/Runnable;

    .line 231
    iput-object p3, p0, Lcom/google/android/maps/MapController;->mAnimationCompletedMessage:Landroid/os/Message;

    .line 232
    invoke-virtual {p1}, Lcom/google/android/maps/GeoPoint;->getMapPoint()Landroid_maps_conflict_avoidance/com/google/map/MapPoint;

    move-result-object v4

    .line 233
    .local v4, "mapPoint":Landroid_maps_conflict_avoidance/com/google/map/MapPoint;
    invoke-virtual {p0, v10}, Lcom/google/android/maps/MapController;->stopAnimation(Z)V

    .line 234
    iget-object v5, p0, Lcom/google/android/maps/MapController;->mMap:Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;

    invoke-virtual {v5, v4}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;->preLoad(Landroid_maps_conflict_avoidance/com/google/map/MapPoint;)V

    .line 235
    iput-object v4, p0, Lcom/google/android/maps/MapController;->mPanPoint:Landroid_maps_conflict_avoidance/com/google/map/MapPoint;

    .line 236
    iget-object v5, p0, Lcom/google/android/maps/MapController;->mMap:Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;

    invoke-virtual {v5}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;->getCenterPoint()Landroid_maps_conflict_avoidance/com/google/map/MapPoint;

    move-result-object v1

    .line 237
    .local v1, "center":Landroid_maps_conflict_avoidance/com/google/map/MapPoint;
    iget-object v5, p0, Lcom/google/android/maps/MapController;->mMap:Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;

    invoke-virtual {v5}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;->getZoom()Landroid_maps_conflict_avoidance/com/google/map/Zoom;

    move-result-object v5

    invoke-virtual {v4, v1, v5}, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->pixelDistanceSquared(Landroid_maps_conflict_avoidance/com/google/map/MapPoint;Landroid_maps_conflict_avoidance/com/google/map/Zoom;)J

    move-result-wide v6

    long-to-int v5, v6

    int-to-double v6, v5

    invoke-static {v6, v7}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    .line 239
    .local v2, "distance":D
    const-wide/high16 v6, 0x4069000000000000L    # 200.0

    const-wide/high16 v8, 0x4024000000000000L    # 10.0

    mul-double/2addr v8, v2

    add-double/2addr v6, v8

    const-wide/high16 v8, 0x4089000000000000L    # 800.0

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->min(DD)D

    move-result-wide v6

    double-to-int v0, v6

    .line 240
    .local v0, "animateMillis":I
    new-instance v5, Landroid/view/animation/TranslateAnimation;

    invoke-virtual {v1}, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->getLatitude()I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v6, v11

    invoke-virtual {v4}, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->getLatitude()I

    move-result v7

    int-to-float v7, v7

    div-float/2addr v7, v11

    invoke-virtual {v1}, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->getLongitude()I

    move-result v8

    int-to-float v8, v8

    div-float/2addr v8, v11

    invoke-virtual {v4}, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->getLongitude()I

    move-result v9

    int-to-float v9, v9

    div-float/2addr v9, v11

    invoke-direct {v5, v6, v7, v8, v9}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    iput-object v5, p0, Lcom/google/android/maps/MapController;->mPanAnimation:Landroid/view/animation/Animation;

    .line 243
    iget-object v5, p0, Lcom/google/android/maps/MapController;->mPanAnimation:Landroid/view/animation/Animation;

    int-to-long v6, v0

    invoke-virtual {v5, v6, v7}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 244
    iget-object v5, p0, Lcom/google/android/maps/MapController;->mPanAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v5}, Landroid/view/animation/Animation;->startNow()V

    .line 245
    iget-object v5, p0, Lcom/google/android/maps/MapController;->mPanAnimation:Landroid/view/animation/Animation;

    new-instance v6, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v6}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v5, v6}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 246
    iget-object v5, p0, Lcom/google/android/maps/MapController;->mPanAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v5, v10, v10, v10, v10}, Landroid/view/animation/Animation;->initialize(IIII)V

    .line 247
    invoke-virtual {p0}, Lcom/google/android/maps/MapController;->repaint()V

    .line 248
    return-void
.end method

.method private centerMapToInternal(Landroid_maps_conflict_avoidance/com/google/map/MapPoint;)V
    .locals 1
    .param p1, "mapPoint"    # Landroid_maps_conflict_avoidance/com/google/map/MapPoint;

    .prologue
    .line 352
    iget-object v0, p0, Lcom/google/android/maps/MapController;->mMap:Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;

    invoke-virtual {v0, p1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;->setCenterPoint(Landroid_maps_conflict_avoidance/com/google/map/MapPoint;)V

    .line 353
    invoke-virtual {p0}, Lcom/google/android/maps/MapController;->repaint()V

    .line 354
    return-void
.end method

.method private curve(FF)F
    .locals 2
    .param p1, "last"    # F
    .param p2, "max"    # F

    .prologue
    .line 196
    sub-float v0, p2, p1

    const/high16 v1, 0x41000000    # 8.0f

    div-float/2addr v0, v1

    add-float/2addr v0, p1

    return v0
.end method

.method private onKeyDown(I)Z
    .locals 2
    .param p1, "keyCode"    # I

    .prologue
    const/4 v0, 0x1

    .line 175
    packed-switch p1, :pswitch_data_0

    .line 189
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 177
    :pswitch_0
    sget-object v1, Lcom/google/android/maps/MapController$VertPanState;->UP:Lcom/google/android/maps/MapController$VertPanState;

    iput-object v1, p0, Lcom/google/android/maps/MapController;->mVertPan:Lcom/google/android/maps/MapController$VertPanState;

    goto :goto_0

    .line 180
    :pswitch_1
    sget-object v1, Lcom/google/android/maps/MapController$VertPanState;->DOWN:Lcom/google/android/maps/MapController$VertPanState;

    iput-object v1, p0, Lcom/google/android/maps/MapController;->mVertPan:Lcom/google/android/maps/MapController$VertPanState;

    goto :goto_0

    .line 183
    :pswitch_2
    sget-object v1, Lcom/google/android/maps/MapController$HorizPanState;->LEFT:Lcom/google/android/maps/MapController$HorizPanState;

    iput-object v1, p0, Lcom/google/android/maps/MapController;->mHorizPan:Lcom/google/android/maps/MapController$HorizPanState;

    goto :goto_0

    .line 186
    :pswitch_3
    sget-object v1, Lcom/google/android/maps/MapController$HorizPanState;->RIGHT:Lcom/google/android/maps/MapController$HorizPanState;

    iput-object v1, p0, Lcom/google/android/maps/MapController;->mHorizPan:Lcom/google/android/maps/MapController$HorizPanState;

    goto :goto_0

    .line 175
    :pswitch_data_0
    .packed-switch 0x13
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private onKeyUp(I)Z
    .locals 4
    .param p1, "keyCode"    # I

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 145
    packed-switch p1, :pswitch_data_0

    move v0, v1

    .line 171
    :goto_0
    return v0

    .line 147
    :pswitch_0
    iget-object v2, p0, Lcom/google/android/maps/MapController;->mVertPan:Lcom/google/android/maps/MapController$VertPanState;

    sget-object v3, Lcom/google/android/maps/MapController$VertPanState;->UP:Lcom/google/android/maps/MapController$VertPanState;

    if-ne v2, v3, :cond_0

    .line 148
    sget-object v1, Lcom/google/android/maps/MapController$VertPanState;->NONE:Lcom/google/android/maps/MapController$VertPanState;

    iput-object v1, p0, Lcom/google/android/maps/MapController;->mVertPan:Lcom/google/android/maps/MapController$VertPanState;

    goto :goto_0

    :cond_0
    move v0, v1

    .line 151
    goto :goto_0

    .line 153
    :pswitch_1
    iget-object v2, p0, Lcom/google/android/maps/MapController;->mVertPan:Lcom/google/android/maps/MapController$VertPanState;

    sget-object v3, Lcom/google/android/maps/MapController$VertPanState;->DOWN:Lcom/google/android/maps/MapController$VertPanState;

    if-ne v2, v3, :cond_1

    .line 154
    sget-object v1, Lcom/google/android/maps/MapController$VertPanState;->NONE:Lcom/google/android/maps/MapController$VertPanState;

    iput-object v1, p0, Lcom/google/android/maps/MapController;->mVertPan:Lcom/google/android/maps/MapController$VertPanState;

    goto :goto_0

    :cond_1
    move v0, v1

    .line 157
    goto :goto_0

    .line 159
    :pswitch_2
    iget-object v2, p0, Lcom/google/android/maps/MapController;->mHorizPan:Lcom/google/android/maps/MapController$HorizPanState;

    sget-object v3, Lcom/google/android/maps/MapController$HorizPanState;->LEFT:Lcom/google/android/maps/MapController$HorizPanState;

    if-ne v2, v3, :cond_2

    .line 160
    sget-object v1, Lcom/google/android/maps/MapController$HorizPanState;->NONE:Lcom/google/android/maps/MapController$HorizPanState;

    iput-object v1, p0, Lcom/google/android/maps/MapController;->mHorizPan:Lcom/google/android/maps/MapController$HorizPanState;

    goto :goto_0

    :cond_2
    move v0, v1

    .line 163
    goto :goto_0

    .line 165
    :pswitch_3
    iget-object v2, p0, Lcom/google/android/maps/MapController;->mHorizPan:Lcom/google/android/maps/MapController$HorizPanState;

    sget-object v3, Lcom/google/android/maps/MapController$HorizPanState;->RIGHT:Lcom/google/android/maps/MapController$HorizPanState;

    if-ne v2, v3, :cond_3

    .line 166
    sget-object v1, Lcom/google/android/maps/MapController$HorizPanState;->NONE:Lcom/google/android/maps/MapController$HorizPanState;

    iput-object v1, p0, Lcom/google/android/maps/MapController;->mHorizPan:Lcom/google/android/maps/MapController$HorizPanState;

    goto :goto_0

    :cond_3
    move v0, v1

    .line 169
    goto :goto_0

    .line 145
    nop

    :pswitch_data_0
    .packed-switch 0x13
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public animateTo(Lcom/google/android/maps/GeoPoint;)V
    .locals 1
    .param p1, "point"    # Lcom/google/android/maps/GeoPoint;

    .prologue
    const/4 v0, 0x0

    .line 203
    invoke-direct {p0, p1, v0, v0}, Lcom/google/android/maps/MapController;->animateTo(Lcom/google/android/maps/GeoPoint;Ljava/lang/Runnable;Landroid/os/Message;)V

    .line 204
    return-void
.end method

.method public animateTo(Lcom/google/android/maps/GeoPoint;Landroid/os/Message;)V
    .locals 1
    .param p1, "point"    # Lcom/google/android/maps/GeoPoint;
    .param p2, "message"    # Landroid/os/Message;

    .prologue
    .line 213
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lcom/google/android/maps/MapController;->animateTo(Lcom/google/android/maps/GeoPoint;Ljava/lang/Runnable;Landroid/os/Message;)V

    .line 214
    return-void
.end method

.method public animateTo(Lcom/google/android/maps/GeoPoint;Ljava/lang/Runnable;)V
    .locals 1
    .param p1, "point"    # Lcom/google/android/maps/GeoPoint;
    .param p2, "runnable"    # Ljava/lang/Runnable;

    .prologue
    .line 225
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/maps/MapController;->animateTo(Lcom/google/android/maps/GeoPoint;Ljava/lang/Runnable;Landroid/os/Message;)V

    .line 226
    return-void
.end method

.method clean()V
    .locals 1

    .prologue
    .line 498
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/maps/MapController;->mDirty:Z

    .line 499
    return-void
.end method

.method getDeltas()[I
    .locals 5

    .prologue
    const/high16 v4, 0x41000000    # 8.0f

    const/4 v3, 0x0

    const/high16 v2, -0x3f000000    # -8.0f

    .line 96
    sget-object v0, Lcom/google/android/maps/MapController$1;->$SwitchMap$com$google$android$maps$MapController$HorizPanState:[I

    iget-object v1, p0, Lcom/google/android/maps/MapController;->mHorizPan:Lcom/google/android/maps/MapController$HorizPanState;

    invoke-virtual {v1}, Lcom/google/android/maps/MapController$HorizPanState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 106
    :goto_0
    sget-object v0, Lcom/google/android/maps/MapController$1;->$SwitchMap$com$google$android$maps$MapController$VertPanState:[I

    iget-object v1, p0, Lcom/google/android/maps/MapController;->mVertPan:Lcom/google/android/maps/MapController$VertPanState;

    invoke-virtual {v1}, Lcom/google/android/maps/MapController$VertPanState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    .line 116
    :goto_1
    iget-object v0, p0, Lcom/google/android/maps/MapController;->mDeltas:[I

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/maps/MapController;->mXPanSpeed:F

    float-to-int v2, v2

    aput v2, v0, v1

    .line 117
    iget-object v0, p0, Lcom/google/android/maps/MapController;->mDeltas:[I

    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/maps/MapController;->mYPanSpeed:F

    float-to-int v2, v2

    aput v2, v0, v1

    .line 118
    iget-object v0, p0, Lcom/google/android/maps/MapController;->mDeltas:[I

    return-object v0

    .line 98
    :pswitch_0
    iget v0, p0, Lcom/google/android/maps/MapController;->mXPanSpeed:F

    invoke-direct {p0, v0, v2}, Lcom/google/android/maps/MapController;->curve(FF)F

    move-result v0

    iput v0, p0, Lcom/google/android/maps/MapController;->mXPanSpeed:F

    goto :goto_0

    .line 101
    :pswitch_1
    iget v0, p0, Lcom/google/android/maps/MapController;->mXPanSpeed:F

    invoke-direct {p0, v0, v4}, Lcom/google/android/maps/MapController;->curve(FF)F

    move-result v0

    iput v0, p0, Lcom/google/android/maps/MapController;->mXPanSpeed:F

    goto :goto_0

    .line 104
    :pswitch_2
    iput v3, p0, Lcom/google/android/maps/MapController;->mXPanSpeed:F

    goto :goto_0

    .line 108
    :pswitch_3
    iget v0, p0, Lcom/google/android/maps/MapController;->mYPanSpeed:F

    invoke-direct {p0, v0, v2}, Lcom/google/android/maps/MapController;->curve(FF)F

    move-result v0

    iput v0, p0, Lcom/google/android/maps/MapController;->mYPanSpeed:F

    goto :goto_1

    .line 111
    :pswitch_4
    iget v0, p0, Lcom/google/android/maps/MapController;->mYPanSpeed:F

    invoke-direct {p0, v0, v4}, Lcom/google/android/maps/MapController;->curve(FF)F

    move-result v0

    iput v0, p0, Lcom/google/android/maps/MapController;->mYPanSpeed:F

    goto :goto_1

    .line 114
    :pswitch_5
    iput v3, p0, Lcom/google/android/maps/MapController;->mYPanSpeed:F

    goto :goto_1

    .line 96
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 106
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method isDirty()Z
    .locals 1

    .prologue
    .line 494
    iget-boolean v0, p0, Lcom/google/android/maps/MapController;->mDirty:Z

    return v0
.end method

.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 126
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 140
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown key action: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 128
    :pswitch_0
    invoke-direct {p0, p2}, Lcom/google/android/maps/MapController;->onKeyDown(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 129
    invoke-virtual {p0}, Lcom/google/android/maps/MapController;->repaint()V

    .line 138
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 132
    goto :goto_0

    .line 134
    :pswitch_1
    invoke-direct {p0, p2}, Lcom/google/android/maps/MapController;->onKeyUp(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 135
    invoke-virtual {p0}, Lcom/google/android/maps/MapController;->repaint()V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 138
    goto :goto_0

    .line 126
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method onMeasure()V
    .locals 2

    .prologue
    .line 485
    iget-boolean v0, p0, Lcom/google/android/maps/MapController;->mHasBeenMeasured:Z

    if-nez v0, :cond_0

    .line 486
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/MapController;->mHasBeenMeasured:Z

    .line 487
    iget v0, p0, Lcom/google/android/maps/MapController;->mDeferredLatSpanE6:I

    if-ltz v0, :cond_0

    .line 488
    iget v0, p0, Lcom/google/android/maps/MapController;->mDeferredLatSpanE6:I

    iget v1, p0, Lcom/google/android/maps/MapController;->mDeferredLonSpanE6:I

    invoke-virtual {p0, v0, v1}, Lcom/google/android/maps/MapController;->zoomToSpan(II)V

    .line 491
    :cond_0
    return-void
.end method

.method repaint()V
    .locals 1

    .prologue
    .line 337
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/MapController;->mDirty:Z

    .line 338
    iget-object v0, p0, Lcom/google/android/maps/MapController;->mMapView:Lcom/google/android/maps/MapView;

    invoke-virtual {v0}, Lcom/google/android/maps/MapView;->postInvalidate()V

    .line 339
    return-void
.end method

.method public scrollBy(II)V
    .locals 3
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 303
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/maps/MapController;->stopAnimation(Z)V

    .line 304
    iget-object v1, p0, Lcom/google/android/maps/MapController;->mMap:Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;

    invoke-virtual {v1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;->getCenterPoint()Landroid_maps_conflict_avoidance/com/google/map/MapPoint;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/maps/MapController;->mMap:Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;

    invoke-virtual {v2}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;->getZoom()Landroid_maps_conflict_avoidance/com/google/map/Zoom;

    move-result-object v2

    invoke-virtual {v1, p1, p2, v2}, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->pixelOffset(IILandroid_maps_conflict_avoidance/com/google/map/Zoom;)Landroid_maps_conflict_avoidance/com/google/map/MapPoint;

    move-result-object v0

    .line 306
    .local v0, "newCenter":Landroid_maps_conflict_avoidance/com/google/map/MapPoint;
    invoke-direct {p0, v0}, Lcom/google/android/maps/MapController;->centerMapToInternal(Landroid_maps_conflict_avoidance/com/google/map/MapPoint;)V

    .line 307
    return-void
.end method

.method scrollByTrackball(II)V
    .locals 6
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 322
    iget-object v2, p0, Lcom/google/android/maps/MapController;->mPanAnimation:Landroid/view/animation/Animation;

    if-eqz v2, :cond_0

    .line 323
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v2

    iget-object v4, p0, Lcom/google/android/maps/MapController;->mPanAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v4}, Landroid/view/animation/Animation;->getStartTime()J

    move-result-wide v4

    sub-long v0, v2, v4

    .line 325
    .local v0, "delta":J
    const-wide/16 v2, 0xfa

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    .line 330
    .end local v0    # "delta":J
    :goto_0
    return-void

    .line 329
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/google/android/maps/MapController;->scrollBy(II)V

    goto :goto_0
.end method

.method public setCenter(Lcom/google/android/maps/GeoPoint;)V
    .locals 1
    .param p1, "point"    # Lcom/google/android/maps/GeoPoint;

    .prologue
    .line 345
    invoke-virtual {p1}, Lcom/google/android/maps/GeoPoint;->getMapPoint()Landroid_maps_conflict_avoidance/com/google/map/MapPoint;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/maps/MapController;->centerMapToInternal(Landroid_maps_conflict_avoidance/com/google/map/MapPoint;)V

    .line 346
    return-void
.end method

.method public setZoom(I)I
    .locals 2
    .param p1, "zoomLevel"    # I

    .prologue
    .line 394
    const/16 v0, 0x16

    const/4 v1, 0x1

    invoke-static {v1, p1}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result p1

    .line 395
    invoke-static {p1}, Landroid_maps_conflict_avoidance/com/google/map/Zoom;->getZoom(I)Landroid_maps_conflict_avoidance/com/google/map/Zoom;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/maps/MapController;->zoomTo(Landroid_maps_conflict_avoidance/com/google/map/Zoom;)V

    .line 396
    return p1
.end method

.method stepAnimation(J)Z
    .locals 11
    .param p1, "drawTime"    # J

    .prologue
    const/4 v7, 0x0

    const-wide v8, 0x412e848000000000L    # 1000000.0

    const/4 v6, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 262
    invoke-virtual {p0}, Lcom/google/android/maps/MapController;->getDeltas()[I

    move-result-object v1

    .line 263
    .local v1, "panDeltas":[I
    aget v5, v1, v4

    if-nez v5, :cond_0

    aget v5, v1, v3

    if-eqz v5, :cond_1

    .line 264
    :cond_0
    aget v4, v1, v4

    aget v5, v1, v3

    invoke-virtual {p0, v4, v5}, Lcom/google/android/maps/MapController;->scrollBy(II)V

    .line 293
    :goto_0
    return v3

    .line 268
    :cond_1
    iget-object v5, p0, Lcom/google/android/maps/MapController;->mPanAnimation:Landroid/view/animation/Animation;

    if-eqz v5, :cond_5

    .line 269
    sget-object v2, Lcom/google/android/maps/MapController;->EMPTY_TRANSFORM:Landroid/view/animation/Transformation;

    .line 270
    .local v2, "xform":Landroid/view/animation/Transformation;
    invoke-virtual {v2}, Landroid/view/animation/Transformation;->clear()V

    .line 271
    iget-object v5, p0, Lcom/google/android/maps/MapController;->mPanAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v5, p1, p2, v2}, Landroid/view/animation/Animation;->getTransformation(JLandroid/view/animation/Transformation;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 272
    iget-object v5, p0, Lcom/google/android/maps/MapController;->mOrigin:[F

    aput v7, v5, v4

    .line 273
    iget-object v5, p0, Lcom/google/android/maps/MapController;->mOrigin:[F

    aput v7, v5, v3

    .line 274
    invoke-virtual {v2}, Landroid/view/animation/Transformation;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/maps/MapController;->mOrigin:[F

    invoke-virtual {v5, v6}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 275
    new-instance v0, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;

    iget-object v5, p0, Lcom/google/android/maps/MapController;->mOrigin:[F

    aget v4, v5, v4

    float-to-double v4, v4

    mul-double/2addr v4, v8

    double-to-int v4, v4

    iget-object v5, p0, Lcom/google/android/maps/MapController;->mOrigin:[F

    aget v5, v5, v3

    float-to-double v6, v5

    mul-double/2addr v6, v8

    double-to-int v5, v6

    invoke-direct {v0, v4, v5}, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;-><init>(II)V

    .line 277
    .local v0, "newCenter":Landroid_maps_conflict_avoidance/com/google/map/MapPoint;
    invoke-direct {p0, v0}, Lcom/google/android/maps/MapController;->centerMapToInternal(Landroid_maps_conflict_avoidance/com/google/map/MapPoint;)V

    goto :goto_0

    .line 280
    .end local v0    # "newCenter":Landroid_maps_conflict_avoidance/com/google/map/MapPoint;
    :cond_2
    iget-object v3, p0, Lcom/google/android/maps/MapController;->mPanPoint:Landroid_maps_conflict_avoidance/com/google/map/MapPoint;

    invoke-direct {p0, v3}, Lcom/google/android/maps/MapController;->centerMapToInternal(Landroid_maps_conflict_avoidance/com/google/map/MapPoint;)V

    .line 281
    iput-object v6, p0, Lcom/google/android/maps/MapController;->mPanPoint:Landroid_maps_conflict_avoidance/com/google/map/MapPoint;

    .line 282
    iput-object v6, p0, Lcom/google/android/maps/MapController;->mPanAnimation:Landroid/view/animation/Animation;

    .line 283
    iget-object v3, p0, Lcom/google/android/maps/MapController;->mAnimationCompletedMessage:Landroid/os/Message;

    if-eqz v3, :cond_3

    .line 284
    iget-object v3, p0, Lcom/google/android/maps/MapController;->mAnimationCompletedMessage:Landroid/os/Message;

    invoke-virtual {v3}, Landroid/os/Message;->sendToTarget()V

    .line 285
    iput-object v6, p0, Lcom/google/android/maps/MapController;->mAnimationCompletedMessage:Landroid/os/Message;

    .line 287
    :cond_3
    iget-object v3, p0, Lcom/google/android/maps/MapController;->mAnimationCompletedRunnable:Ljava/lang/Runnable;

    if-eqz v3, :cond_4

    .line 288
    iget-object v3, p0, Lcom/google/android/maps/MapController;->mMapView:Lcom/google/android/maps/MapView;

    iget-object v5, p0, Lcom/google/android/maps/MapController;->mAnimationCompletedRunnable:Ljava/lang/Runnable;

    invoke-virtual {v3, v5}, Lcom/google/android/maps/MapView;->post(Ljava/lang/Runnable;)Z

    .line 289
    iput-object v6, p0, Lcom/google/android/maps/MapController;->mAnimationCompletedRunnable:Ljava/lang/Runnable;

    :cond_4
    move v3, v4

    .line 291
    goto :goto_0

    .end local v2    # "xform":Landroid/view/animation/Transformation;
    :cond_5
    move v3, v4

    .line 293
    goto :goto_0
.end method

.method public stopAnimation(Z)V
    .locals 3
    .param p1, "jumpToFinish"    # Z

    .prologue
    const/4 v2, 0x0

    .line 365
    iget-object v0, p0, Lcom/google/android/maps/MapController;->mPanAnimation:Landroid/view/animation/Animation;

    if-eqz v0, :cond_1

    .line 366
    if-eqz p1, :cond_0

    .line 367
    iget-object v1, p0, Lcom/google/android/maps/MapController;->mMap:Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;

    monitor-enter v1

    .line 368
    :try_start_0
    iget-object v0, p0, Lcom/google/android/maps/MapController;->mPanPoint:Landroid_maps_conflict_avoidance/com/google/map/MapPoint;

    invoke-direct {p0, v0}, Lcom/google/android/maps/MapController;->centerMapToInternal(Landroid_maps_conflict_avoidance/com/google/map/MapPoint;)V

    .line 369
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 371
    :cond_0
    iput-object v2, p0, Lcom/google/android/maps/MapController;->mPanAnimation:Landroid/view/animation/Animation;

    .line 372
    iput-object v2, p0, Lcom/google/android/maps/MapController;->mPanPoint:Landroid_maps_conflict_avoidance/com/google/map/MapPoint;

    .line 374
    :cond_1
    iput-object v2, p0, Lcom/google/android/maps/MapController;->mAnimationCompletedMessage:Landroid/os/Message;

    .line 375
    return-void

    .line 369
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public stopPanning()V
    .locals 1

    .prologue
    .line 86
    sget-object v0, Lcom/google/android/maps/MapController$HorizPanState;->NONE:Lcom/google/android/maps/MapController$HorizPanState;

    iput-object v0, p0, Lcom/google/android/maps/MapController;->mHorizPan:Lcom/google/android/maps/MapController$HorizPanState;

    .line 87
    sget-object v0, Lcom/google/android/maps/MapController$VertPanState;->NONE:Lcom/google/android/maps/MapController$VertPanState;

    iput-object v0, p0, Lcom/google/android/maps/MapController;->mVertPan:Lcom/google/android/maps/MapController$VertPanState;

    .line 88
    return-void
.end method

.method public zoomIn()Z
    .locals 2

    .prologue
    .line 427
    iget-object v0, p0, Lcom/google/android/maps/MapController;->mMapView:Lcom/google/android/maps/MapView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/maps/MapView;->doZoom(Z)Z

    move-result v0

    return v0
.end method

.method public zoomInFixing(II)Z
    .locals 2
    .param p1, "xPixel"    # I
    .param p2, "yPixel"    # I

    .prologue
    .line 458
    iget-object v0, p0, Lcom/google/android/maps/MapController;->mMapView:Lcom/google/android/maps/MapView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1, p2}, Lcom/google/android/maps/MapView;->doZoom(ZII)Z

    move-result v0

    return v0
.end method

.method public zoomOut()Z
    .locals 2

    .prologue
    .line 439
    iget-object v0, p0, Lcom/google/android/maps/MapController;->mMapView:Lcom/google/android/maps/MapView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/maps/MapView;->doZoom(Z)Z

    move-result v0

    return v0
.end method

.method public zoomOutFixing(II)Z
    .locals 2
    .param p1, "xPixel"    # I
    .param p2, "yPixel"    # I

    .prologue
    .line 477
    iget-object v0, p0, Lcom/google/android/maps/MapController;->mMapView:Lcom/google/android/maps/MapView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1, p2}, Lcom/google/android/maps/MapView;->doZoom(ZII)Z

    move-result v0

    return v0
.end method

.method zoomTo(Landroid_maps_conflict_avoidance/com/google/map/Zoom;)V
    .locals 1
    .param p1, "zoom"    # Landroid_maps_conflict_avoidance/com/google/map/Zoom;

    .prologue
    .line 378
    iget-object v0, p0, Lcom/google/android/maps/MapController;->mMap:Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;

    invoke-virtual {v0, p1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;->setZoom(Landroid_maps_conflict_avoidance/com/google/map/Zoom;)V

    .line 379
    invoke-virtual {p0}, Lcom/google/android/maps/MapController;->repaint()V

    .line 380
    return-void
.end method

.method public zoomToSpan(II)V
    .locals 1
    .param p1, "latSpanE6"    # I
    .param p2, "lonSpanE6"    # I

    .prologue
    .line 409
    iget-boolean v0, p0, Lcom/google/android/maps/MapController;->mHasBeenMeasured:Z

    if-eqz v0, :cond_0

    .line 410
    iget-object v0, p0, Lcom/google/android/maps/MapController;->mMap:Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;

    invoke-virtual {v0, p1, p2}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;->zoomToSpan(II)V

    .line 411
    invoke-virtual {p0}, Lcom/google/android/maps/MapController;->repaint()V

    .line 416
    :goto_0
    return-void

    .line 413
    :cond_0
    iput p1, p0, Lcom/google/android/maps/MapController;->mDeferredLatSpanE6:I

    .line 414
    iput p2, p0, Lcom/google/android/maps/MapController;->mDeferredLonSpanE6:I

    goto :goto_0
.end method
