.class Lcom/google/android/maps/ZoomHelper;
.super Ljava/lang/Object;
.source "ZoomHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/maps/ZoomHelper$1;,
        Lcom/google/android/maps/ZoomHelper$Snapshot;
    }
.end annotation


# instance fields
.field private final mAnimations:Landroid/view/animation/AnimationSet;

.field private final mBitmapPaint:Landroid/graphics/Paint;

.field private mCommitTime:J

.field private final mController:Lcom/google/android/maps/MapController;

.field private mFading:Z

.field private final mLastDrawnScale:Landroid/view/animation/Transformation;

.field private mManualZoomActive:Z

.field private final mMapView:Lcom/google/android/maps/MapView;

.field private mPCAtBeginningOfManualZoom:Lcom/google/android/maps/PixelConverter;

.field private mSnapshot:Lcom/google/android/maps/ZoomHelper$Snapshot;

.field private mSnapshotOffsetX:F

.field private mSnapshotOffsetY:F

.field private mSnapshotScale:F

.field private final mTempMatrix:Landroid/graphics/Matrix;

.field protected final mTempPoint:Landroid/graphics/Point;


# direct methods
.method constructor <init>(Lcom/google/android/maps/MapView;Lcom/google/android/maps/MapController;)V
    .locals 2
    .param p1, "mapView"    # Lcom/google/android/maps/MapView;
    .param p2, "controller"    # Lcom/google/android/maps/MapController;

    .prologue
    const/4 v1, 0x0

    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    new-instance v0, Landroid/view/animation/Transformation;

    invoke-direct {v0}, Landroid/view/animation/Transformation;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/ZoomHelper;->mLastDrawnScale:Landroid/view/animation/Transformation;

    .line 63
    new-instance v0, Landroid/view/animation/AnimationSet;

    invoke-direct {v0, v1}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/maps/ZoomHelper;->mAnimations:Landroid/view/animation/AnimationSet;

    .line 66
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/ZoomHelper;->mBitmapPaint:Landroid/graphics/Paint;

    .line 69
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/ZoomHelper;->mTempMatrix:Landroid/graphics/Matrix;

    .line 74
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/ZoomHelper;->mSnapshot:Lcom/google/android/maps/ZoomHelper$Snapshot;

    .line 80
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/maps/ZoomHelper;->mSnapshotScale:F

    .line 86
    iput-boolean v1, p0, Lcom/google/android/maps/ZoomHelper;->mFading:Z

    .line 90
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/google/android/maps/ZoomHelper;->mCommitTime:J

    .line 93
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/ZoomHelper;->mTempPoint:Landroid/graphics/Point;

    .line 103
    iput-object p1, p0, Lcom/google/android/maps/ZoomHelper;->mMapView:Lcom/google/android/maps/MapView;

    .line 104
    iput-object p2, p0, Lcom/google/android/maps/ZoomHelper;->mController:Lcom/google/android/maps/MapController;

    .line 107
    iget-object v0, p0, Lcom/google/android/maps/ZoomHelper;->mBitmapPaint:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 110
    new-instance v1, Lcom/google/android/maps/PixelConverter;

    invoke-virtual {p1}, Lcom/google/android/maps/MapView;->getProjection()Lcom/google/android/maps/Projection;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/PixelConverter;

    check-cast v0, Lcom/google/android/maps/PixelConverter;

    invoke-direct {v1, v0}, Lcom/google/android/maps/PixelConverter;-><init>(Lcom/google/android/maps/PixelConverter;)V

    iput-object v1, p0, Lcom/google/android/maps/ZoomHelper;->mPCAtBeginningOfManualZoom:Lcom/google/android/maps/PixelConverter;

    .line 111
    return-void
.end method

.method private addFade()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 442
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 443
    .local v0, "fade":Landroid/view/animation/AlphaAnimation;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V

    .line 444
    const-wide/16 v2, 0x96

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 445
    invoke-virtual {v0, v4, v4, v4, v4}, Landroid/view/animation/AlphaAnimation;->initialize(IIII)V

    .line 446
    invoke-virtual {v0}, Landroid/view/animation/AlphaAnimation;->startNow()V

    .line 447
    iget-object v1, p0, Lcom/google/android/maps/ZoomHelper;->mAnimations:Landroid/view/animation/AnimationSet;

    invoke-virtual {v1, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 448
    return-void
.end method

.method private addScale(J)V
    .locals 9
    .param p1, "time"    # J

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 417
    iget-object v3, p0, Lcom/google/android/maps/ZoomHelper;->mLastDrawnScale:Landroid/view/animation/Transformation;

    invoke-virtual {v3}, Landroid/view/animation/Transformation;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v3

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v3, v4}, Landroid/graphics/Matrix;->mapRadius(F)F

    move-result v1

    .line 421
    .local v1, "fromFactor":F
    iget-object v3, p0, Lcom/google/android/maps/ZoomHelper;->mSnapshot:Lcom/google/android/maps/ZoomHelper$Snapshot;

    iget-object v3, v3, Lcom/google/android/maps/ZoomHelper$Snapshot;->zoom:Landroid_maps_conflict_avoidance/com/google/map/Zoom;

    iget-object v4, p0, Lcom/google/android/maps/ZoomHelper;->mMapView:Lcom/google/android/maps/MapView;

    invoke-virtual {v4}, Lcom/google/android/maps/MapView;->getZoom()Landroid_maps_conflict_avoidance/com/google/map/Zoom;

    move-result-object v4

    invoke-direct {p0, v3, v4}, Lcom/google/android/maps/ZoomHelper;->getScale(Landroid_maps_conflict_avoidance/com/google/map/Zoom;Landroid_maps_conflict_avoidance/com/google/map/Zoom;)F

    move-result v2

    .line 423
    .local v2, "toFactor":F
    new-instance v0, Landroid/view/animation/ScaleAnimation;

    iget-object v3, p0, Lcom/google/android/maps/ZoomHelper;->mSnapshot:Lcom/google/android/maps/ZoomHelper$Snapshot;

    iget-object v3, v3, Lcom/google/android/maps/ZoomHelper$Snapshot;->fixedPointScreenCoords:[F

    aget v5, v3, v7

    iget-object v3, p0, Lcom/google/android/maps/ZoomHelper;->mSnapshot:Lcom/google/android/maps/ZoomHelper$Snapshot;

    iget-object v3, v3, Lcom/google/android/maps/ZoomHelper$Snapshot;->fixedPointScreenCoords:[F

    aget v6, v3, v8

    move v3, v1

    move v4, v2

    invoke-direct/range {v0 .. v6}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFFF)V

    .line 427
    .local v0, "scale":Landroid/view/animation/ScaleAnimation;
    invoke-virtual {v0, v8}, Landroid/view/animation/ScaleAnimation;->setFillAfter(Z)V

    .line 428
    invoke-virtual {v0, p1, p2}, Landroid/view/animation/ScaleAnimation;->setDuration(J)V

    .line 429
    invoke-virtual {v0, v7, v7, v7, v7}, Landroid/view/animation/ScaleAnimation;->initialize(IIII)V

    .line 430
    invoke-virtual {v0}, Landroid/view/animation/ScaleAnimation;->startNow()V

    .line 431
    new-instance v3, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v3}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v0, v3}, Landroid/view/animation/ScaleAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 433
    iput-boolean v7, p0, Lcom/google/android/maps/ZoomHelper;->mFading:Z

    .line 434
    iget-object v3, p0, Lcom/google/android/maps/ZoomHelper;->mAnimations:Landroid/view/animation/AnimationSet;

    invoke-virtual {v3}, Landroid/view/animation/AnimationSet;->getAnimations()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 435
    iget-object v3, p0, Lcom/google/android/maps/ZoomHelper;->mAnimations:Landroid/view/animation/AnimationSet;

    invoke-virtual {v3, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 436
    return-void
.end method

.method private static calculateRoundedZoom(FI)I
    .locals 8
    .param p0, "scale"    # F
    .param p1, "startingZoomLevel"    # I

    .prologue
    const/4 v3, 0x1

    const/high16 v5, 0x3f800000    # 1.0f

    .line 327
    cmpg-float v4, p0, v5

    if-gez v4, :cond_1

    move v2, v3

    .line 328
    .local v2, "zoomOut":Z
    :goto_0
    if-eqz v2, :cond_0

    div-float p0, v5, p0

    .line 329
    :cond_0
    float-to-int v1, p0

    .line 330
    .local v1, "intZoom":I
    const/4 v0, 0x0

    .line 331
    .local v0, "i":I
    :goto_1
    if-le v1, v3, :cond_2

    .line 332
    shr-int/lit8 v1, v1, 0x1

    .line 333
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 327
    .end local v0    # "i":I
    .end local v1    # "intZoom":I
    .end local v2    # "zoomOut":Z
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 339
    .restart local v0    # "i":I
    .restart local v1    # "intZoom":I
    .restart local v2    # "zoomOut":Z
    :cond_2
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    float-to-double v6, p0

    sub-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(D)D

    move-result-wide v4

    const-wide/high16 v6, 0x3fd0000000000000L    # 0.25

    cmpl-double v3, v4, v6

    if-lez v3, :cond_3

    .line 340
    add-int/lit8 v0, v0, 0x1

    .line 342
    :cond_3
    if-eqz v2, :cond_4

    neg-int v0, v0

    .end local v0    # "i":I
    :cond_4
    add-int v3, p1, v0

    return v3
.end method

.method private createSnapshot()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 464
    new-instance v1, Lcom/google/android/maps/ZoomHelper$Snapshot;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/maps/ZoomHelper$Snapshot;-><init>(Lcom/google/android/maps/ZoomHelper$1;)V

    .line 465
    .local v1, "snapshot":Lcom/google/android/maps/ZoomHelper$Snapshot;
    iget-object v2, p0, Lcom/google/android/maps/ZoomHelper;->mMapView:Lcom/google/android/maps/MapView;

    invoke-virtual {v2}, Lcom/google/android/maps/MapView;->getWidth()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/maps/ZoomHelper;->mMapView:Lcom/google/android/maps/MapView;

    invoke-virtual {v3}, Lcom/google/android/maps/MapView;->getHeight()I

    move-result v3

    sget-object v4, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/maps/ZoomHelper$Snapshot;->bitmap:Landroid/graphics/Bitmap;

    .line 468
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v2, v1, Lcom/google/android/maps/ZoomHelper$Snapshot;->bitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 469
    .local v0, "canvas":Landroid/graphics/Canvas;
    invoke-virtual {v0, v5}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 470
    iget-object v2, p0, Lcom/google/android/maps/ZoomHelper;->mMapView:Lcom/google/android/maps/MapView;

    invoke-virtual {v2, v0, v5}, Lcom/google/android/maps/MapView;->drawMap(Landroid/graphics/Canvas;Z)Z

    .line 471
    iget-object v2, p0, Lcom/google/android/maps/ZoomHelper;->mMapView:Lcom/google/android/maps/MapView;

    invoke-virtual {v2}, Lcom/google/android/maps/MapView;->getZoom()Landroid_maps_conflict_avoidance/com/google/map/Zoom;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/maps/ZoomHelper$Snapshot;->zoom:Landroid_maps_conflict_avoidance/com/google/map/Zoom;

    .line 472
    iget-object v2, p0, Lcom/google/android/maps/ZoomHelper;->mLastDrawnScale:Landroid/view/animation/Transformation;

    invoke-virtual {v2}, Landroid/view/animation/Transformation;->clear()V

    .line 473
    iput-object v1, p0, Lcom/google/android/maps/ZoomHelper;->mSnapshot:Lcom/google/android/maps/ZoomHelper$Snapshot;

    .line 474
    return-void
.end method

.method private getScale(Landroid_maps_conflict_avoidance/com/google/map/Zoom;Landroid_maps_conflict_avoidance/com/google/map/Zoom;)F
    .locals 2
    .param p1, "numerator"    # Landroid_maps_conflict_avoidance/com/google/map/Zoom;
    .param p2, "denominator"    # Landroid_maps_conflict_avoidance/com/google/map/Zoom;

    .prologue
    .line 405
    invoke-virtual {p2, p1}, Landroid_maps_conflict_avoidance/com/google/map/Zoom;->isMoreZoomedIn(Landroid_maps_conflict_avoidance/com/google/map/Zoom;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, p2}, Landroid_maps_conflict_avoidance/com/google/map/Zoom;->getZoomRatio(Landroid_maps_conflict_avoidance/com/google/map/Zoom;)I

    move-result v0

    int-to-float v0, v0

    :goto_0
    return v0

    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p2, p1}, Landroid_maps_conflict_avoidance/com/google/map/Zoom;->getZoomRatio(Landroid_maps_conflict_avoidance/com/google/map/Zoom;)I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    goto :goto_0
.end method

.method private stepAnimation(JLcom/google/android/maps/PixelConverter;)V
    .locals 7
    .param p1, "when"    # J
    .param p3, "converter"    # Lcom/google/android/maps/PixelConverter;

    .prologue
    .line 481
    iget-object v0, p0, Lcom/google/android/maps/ZoomHelper;->mAnimations:Landroid/view/animation/AnimationSet;

    iget-object v1, p0, Lcom/google/android/maps/ZoomHelper;->mLastDrawnScale:Landroid/view/animation/Transformation;

    invoke-virtual {v0, p1, p2, v1}, Landroid/view/animation/AnimationSet;->getTransformation(JLandroid/view/animation/Transformation;)Z

    .line 483
    iget-object v0, p0, Lcom/google/android/maps/ZoomHelper;->mLastDrawnScale:Landroid/view/animation/Transformation;

    invoke-virtual {v0}, Landroid/view/animation/Transformation;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/maps/ZoomHelper;->mMapView:Lcom/google/android/maps/MapView;

    invoke-virtual {v0}, Lcom/google/android/maps/MapView;->getZoom()Landroid_maps_conflict_avoidance/com/google/map/Zoom;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/maps/ZoomHelper;->mSnapshot:Lcom/google/android/maps/ZoomHelper$Snapshot;

    iget-object v2, v2, Lcom/google/android/maps/ZoomHelper$Snapshot;->zoom:Landroid_maps_conflict_avoidance/com/google/map/Zoom;

    invoke-direct {p0, v0, v2}, Lcom/google/android/maps/ZoomHelper;->getScale(Landroid_maps_conflict_avoidance/com/google/map/Zoom;Landroid_maps_conflict_avoidance/com/google/map/Zoom;)F

    move-result v2

    iget-object v0, p0, Lcom/google/android/maps/ZoomHelper;->mSnapshot:Lcom/google/android/maps/ZoomHelper$Snapshot;

    iget-object v3, v0, Lcom/google/android/maps/ZoomHelper$Snapshot;->fixedPoint:Lcom/google/android/maps/GeoPoint;

    iget-object v0, p0, Lcom/google/android/maps/ZoomHelper;->mSnapshot:Lcom/google/android/maps/ZoomHelper$Snapshot;

    iget-object v0, v0, Lcom/google/android/maps/ZoomHelper$Snapshot;->fixedPointCoords:[F

    const/4 v4, 0x0

    aget v4, v0, v4

    iget-object v0, p0, Lcom/google/android/maps/ZoomHelper;->mSnapshot:Lcom/google/android/maps/ZoomHelper$Snapshot;

    iget-object v0, v0, Lcom/google/android/maps/ZoomHelper$Snapshot;->fixedPointCoords:[F

    const/4 v5, 0x1

    aget v5, v0, v5

    move-object v0, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/maps/PixelConverter;->setMatrix(Landroid/graphics/Matrix;FLcom/google/android/maps/GeoPoint;FF)V

    .line 488
    return-void
.end method

.method private updateSnapshotFixedPoint(FF)V
    .locals 5
    .param p1, "xOffset"    # F
    .param p2, "yOffset"    # F

    .prologue
    .line 285
    iget-object v2, p0, Lcom/google/android/maps/ZoomHelper;->mMapView:Lcom/google/android/maps/MapView;

    invoke-virtual {v2}, Lcom/google/android/maps/MapView;->getProjection()Lcom/google/android/maps/Projection;

    move-result-object v1

    check-cast v1, Lcom/google/android/maps/PixelConverter;

    .line 287
    .local v1, "pc":Lcom/google/android/maps/PixelConverter;
    iget-object v0, p0, Lcom/google/android/maps/ZoomHelper;->mTempMatrix:Landroid/graphics/Matrix;

    .line 288
    .local v0, "inverse":Landroid/graphics/Matrix;
    iget-object v2, p0, Lcom/google/android/maps/ZoomHelper;->mLastDrawnScale:Landroid/view/animation/Transformation;

    invoke-virtual {v2}, Landroid/view/animation/Transformation;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 289
    const-string v2, "ZoomHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Singular matrix "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/maps/ZoomHelper;->mLastDrawnScale:Landroid/view/animation/Transformation;

    invoke-virtual {v4}, Landroid/view/animation/Transformation;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 292
    :cond_0
    invoke-direct {p0, v1, v0, p1, p2}, Lcom/google/android/maps/ZoomHelper;->updateSnapshotFixedPoint(Lcom/google/android/maps/PixelConverter;Landroid/graphics/Matrix;FF)V

    .line 293
    return-void
.end method

.method private updateSnapshotFixedPoint(Lcom/google/android/maps/PixelConverter;Landroid/graphics/Matrix;FF)V
    .locals 5
    .param p1, "pc"    # Lcom/google/android/maps/PixelConverter;
    .param p2, "inverse"    # Landroid/graphics/Matrix;
    .param p3, "xOffset"    # F
    .param p4, "yOffset"    # F

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 302
    iget-object v0, p0, Lcom/google/android/maps/ZoomHelper;->mSnapshot:Lcom/google/android/maps/ZoomHelper$Snapshot;

    invoke-static {p3}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-static {p4}, Ljava/lang/Math;->round(F)I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lcom/google/android/maps/PixelConverter;->fromPixels(II)Lcom/google/android/maps/GeoPoint;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/maps/ZoomHelper$Snapshot;->fixedPoint:Lcom/google/android/maps/GeoPoint;

    .line 308
    iget-object v0, p0, Lcom/google/android/maps/ZoomHelper;->mSnapshot:Lcom/google/android/maps/ZoomHelper$Snapshot;

    iget-object v0, v0, Lcom/google/android/maps/ZoomHelper$Snapshot;->fixedPointCoords:[F

    aput p3, v0, v3

    .line 309
    iget-object v0, p0, Lcom/google/android/maps/ZoomHelper;->mSnapshot:Lcom/google/android/maps/ZoomHelper$Snapshot;

    iget-object v0, v0, Lcom/google/android/maps/ZoomHelper$Snapshot;->fixedPointCoords:[F

    aput p4, v0, v4

    .line 310
    iget-object v0, p0, Lcom/google/android/maps/ZoomHelper;->mSnapshot:Lcom/google/android/maps/ZoomHelper$Snapshot;

    iget-object v0, v0, Lcom/google/android/maps/ZoomHelper$Snapshot;->fixedPointScreenCoords:[F

    aput p3, v0, v3

    .line 311
    iget-object v0, p0, Lcom/google/android/maps/ZoomHelper;->mSnapshot:Lcom/google/android/maps/ZoomHelper$Snapshot;

    iget-object v0, v0, Lcom/google/android/maps/ZoomHelper$Snapshot;->fixedPointScreenCoords:[F

    aput p4, v0, v4

    .line 312
    iget-object v0, p0, Lcom/google/android/maps/ZoomHelper;->mSnapshot:Lcom/google/android/maps/ZoomHelper$Snapshot;

    iget-object v0, v0, Lcom/google/android/maps/ZoomHelper$Snapshot;->fixedPointCoords:[F

    invoke-virtual {p2, v0}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 313
    return-void
.end method


# virtual methods
.method beginZoom(FF)V
    .locals 3
    .param p1, "xOffset"    # F
    .param p2, "yOffset"    # F

    .prologue
    const/4 v2, 0x0

    .line 199
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/maps/ZoomHelper;->mManualZoomActive:Z

    .line 200
    invoke-direct {p0}, Lcom/google/android/maps/ZoomHelper;->createSnapshot()V

    .line 201
    iput v2, p0, Lcom/google/android/maps/ZoomHelper;->mSnapshotOffsetX:F

    .line 202
    iput v2, p0, Lcom/google/android/maps/ZoomHelper;->mSnapshotOffsetY:F

    .line 203
    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, p0, Lcom/google/android/maps/ZoomHelper;->mSnapshotScale:F

    .line 204
    invoke-direct {p0, p1, p2}, Lcom/google/android/maps/ZoomHelper;->updateSnapshotFixedPoint(FF)V

    .line 206
    iget-object v1, p0, Lcom/google/android/maps/ZoomHelper;->mMapView:Lcom/google/android/maps/MapView;

    invoke-virtual {v1}, Lcom/google/android/maps/MapView;->getProjection()Lcom/google/android/maps/Projection;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/PixelConverter;

    .line 207
    .local v0, "pc":Lcom/google/android/maps/PixelConverter;
    iget-object v1, p0, Lcom/google/android/maps/ZoomHelper;->mPCAtBeginningOfManualZoom:Lcom/google/android/maps/PixelConverter;

    invoke-virtual {v1, v0}, Lcom/google/android/maps/PixelConverter;->setMatricesFrom(Lcom/google/android/maps/PixelConverter;)V

    .line 208
    return-void
.end method

.method doZoom(Landroid_maps_conflict_avoidance/com/google/map/Zoom;ZII)Z
    .locals 6
    .param p1, "newZoom"    # Landroid_maps_conflict_avoidance/com/google/map/Zoom;
    .param p2, "delay"    # Z
    .param p3, "xOffset"    # I
    .param p4, "yOffset"    # I

    .prologue
    .line 156
    iget-object v2, p0, Lcom/google/android/maps/ZoomHelper;->mSnapshot:Lcom/google/android/maps/ZoomHelper$Snapshot;

    if-nez v2, :cond_0

    .line 157
    invoke-direct {p0}, Lcom/google/android/maps/ZoomHelper;->createSnapshot()V

    .line 160
    :cond_0
    iget-object v2, p0, Lcom/google/android/maps/ZoomHelper;->mMapView:Lcom/google/android/maps/MapView;

    invoke-virtual {v2}, Lcom/google/android/maps/MapView;->getProjection()Lcom/google/android/maps/Projection;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/PixelConverter;

    .line 162
    .local v0, "pc":Lcom/google/android/maps/PixelConverter;
    int-to-float v2, p3

    int-to-float v3, p4

    invoke-direct {p0, v2, v3}, Lcom/google/android/maps/ZoomHelper;->updateSnapshotFixedPoint(FF)V

    .line 166
    iget-object v2, p0, Lcom/google/android/maps/ZoomHelper;->mController:Lcom/google/android/maps/MapController;

    invoke-virtual {v2, p1}, Lcom/google/android/maps/MapController;->zoomTo(Landroid_maps_conflict_avoidance/com/google/map/Zoom;)V

    .line 169
    iget-object v2, p0, Lcom/google/android/maps/ZoomHelper;->mMapView:Lcom/google/android/maps/MapView;

    invoke-virtual {v2}, Lcom/google/android/maps/MapView;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    if-ne p3, v2, :cond_1

    iget-object v2, p0, Lcom/google/android/maps/ZoomHelper;->mMapView:Lcom/google/android/maps/MapView;

    invoke-virtual {v2}, Lcom/google/android/maps/MapView;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    if-eq p4, v2, :cond_2

    .line 171
    :cond_1
    iget-object v2, p0, Lcom/google/android/maps/ZoomHelper;->mSnapshot:Lcom/google/android/maps/ZoomHelper$Snapshot;

    iget-object v2, v2, Lcom/google/android/maps/ZoomHelper$Snapshot;->fixedPoint:Lcom/google/android/maps/GeoPoint;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v4}, Lcom/google/android/maps/PixelConverter;->toPixels(Lcom/google/android/maps/GeoPoint;Landroid/graphics/Point;Z)Landroid/graphics/Point;

    move-result-object v1

    .line 172
    .local v1, "realLocationOfFixedPoint":Landroid/graphics/Point;
    iget-object v2, p0, Lcom/google/android/maps/ZoomHelper;->mController:Lcom/google/android/maps/MapController;

    iget v3, v1, Landroid/graphics/Point;->x:I

    sub-int/2addr v3, p3

    iget v4, v1, Landroid/graphics/Point;->y:I

    sub-int/2addr v4, p4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/maps/MapController;->scrollBy(II)V

    .line 176
    .end local v1    # "realLocationOfFixedPoint":Landroid/graphics/Point;
    :cond_2
    const-wide/16 v2, 0x12c

    invoke-direct {p0, v2, v3}, Lcom/google/android/maps/ZoomHelper;->addScale(J)V

    .line 181
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v2

    invoke-direct {p0, v2, v3, v0}, Lcom/google/android/maps/ZoomHelper;->stepAnimation(JLcom/google/android/maps/PixelConverter;)V

    .line 184
    if-eqz p2, :cond_3

    .line 185
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x258

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/android/maps/ZoomHelper;->mCommitTime:J

    .line 190
    :goto_0
    const/4 v2, 0x1

    return v2

    .line 187
    :cond_3
    iget-object v2, p0, Lcom/google/android/maps/ZoomHelper;->mMapView:Lcom/google/android/maps/MapView;

    invoke-virtual {v2}, Lcom/google/android/maps/MapView;->preLoad()V

    goto :goto_0
.end method

.method doZoom(ZZII)Z
    .locals 4
    .param p1, "zoomIn"    # Z
    .param p2, "delay"    # Z
    .param p3, "xOffset"    # I
    .param p4, "yOffset"    # I

    .prologue
    .line 136
    iget-object v2, p0, Lcom/google/android/maps/ZoomHelper;->mMapView:Lcom/google/android/maps/MapView;

    invoke-virtual {v2}, Lcom/google/android/maps/MapView;->getZoom()Landroid_maps_conflict_avoidance/com/google/map/Zoom;

    move-result-object v0

    .line 137
    .local v0, "currentZoom":Landroid_maps_conflict_avoidance/com/google/map/Zoom;
    if-eqz p1, :cond_1

    invoke-virtual {v0}, Landroid_maps_conflict_avoidance/com/google/map/Zoom;->getNextHigherZoom()Landroid_maps_conflict_avoidance/com/google/map/Zoom;

    move-result-object v1

    .line 141
    .local v1, "newZoom":Landroid_maps_conflict_avoidance/com/google/map/Zoom;
    :goto_0
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid_maps_conflict_avoidance/com/google/map/Zoom;->getZoomLevel()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/maps/ZoomHelper;->mMapView:Lcom/google/android/maps/MapView;

    invoke-virtual {v3}, Lcom/google/android/maps/MapView;->getMaxZoomLevel()I

    move-result v3

    if-le v2, v3, :cond_2

    .line 143
    :cond_0
    const/4 v2, 0x0

    .line 146
    :goto_1
    return v2

    .line 137
    .end local v1    # "newZoom":Landroid_maps_conflict_avoidance/com/google/map/Zoom;
    :cond_1
    invoke-virtual {v0}, Landroid_maps_conflict_avoidance/com/google/map/Zoom;->getNextLowerZoom()Landroid_maps_conflict_avoidance/com/google/map/Zoom;

    move-result-object v1

    goto :goto_0

    .line 146
    .restart local v1    # "newZoom":Landroid_maps_conflict_avoidance/com/google/map/Zoom;
    :cond_2
    invoke-virtual {p0, v1, p2, p3, p4}, Lcom/google/android/maps/ZoomHelper;->doZoom(Landroid_maps_conflict_avoidance/com/google/map/Zoom;ZII)Z

    move-result v2

    goto :goto_1
.end method

.method endZoom()V
    .locals 15

    .prologue
    .line 241
    iget-object v12, p0, Lcom/google/android/maps/ZoomHelper;->mMapView:Lcom/google/android/maps/MapView;

    invoke-virtual {v12}, Lcom/google/android/maps/MapView;->getProjection()Lcom/google/android/maps/Projection;

    move-result-object v7

    check-cast v7, Lcom/google/android/maps/PixelConverter;

    .line 245
    .local v7, "pc":Lcom/google/android/maps/PixelConverter;
    iget-object v12, p0, Lcom/google/android/maps/ZoomHelper;->mLastDrawnScale:Landroid/view/animation/Transformation;

    invoke-virtual {v12}, Landroid/view/animation/Transformation;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v12

    const/high16 v13, 0x3f800000    # 1.0f

    invoke-virtual {v12, v13}, Landroid/graphics/Matrix;->mapRadius(F)F

    move-result v9

    .line 246
    .local v9, "scaleFactorBegin":F
    iget-object v12, p0, Lcom/google/android/maps/ZoomHelper;->mMapView:Lcom/google/android/maps/MapView;

    invoke-virtual {v12}, Lcom/google/android/maps/MapView;->getZoom()Landroid_maps_conflict_avoidance/com/google/map/Zoom;

    move-result-object v6

    .line 247
    .local v6, "oldZoom":Landroid_maps_conflict_avoidance/com/google/map/Zoom;
    invoke-virtual {v6}, Landroid_maps_conflict_avoidance/com/google/map/Zoom;->getZoomLevel()I

    move-result v12

    invoke-static {v9, v12}, Lcom/google/android/maps/ZoomHelper;->calculateRoundedZoom(FI)I

    move-result v4

    .line 248
    .local v4, "newLevel":I
    invoke-static {v4}, Landroid_maps_conflict_avoidance/com/google/map/Zoom;->getZoom(I)Landroid_maps_conflict_avoidance/com/google/map/Zoom;

    move-result-object v5

    .line 250
    .local v5, "newZoom":Landroid_maps_conflict_avoidance/com/google/map/Zoom;
    iget-object v12, p0, Lcom/google/android/maps/ZoomHelper;->mController:Lcom/google/android/maps/MapController;

    invoke-virtual {v12, v5}, Lcom/google/android/maps/MapController;->zoomTo(Landroid_maps_conflict_avoidance/com/google/map/Zoom;)V

    .line 253
    iget-object v12, p0, Lcom/google/android/maps/ZoomHelper;->mSnapshot:Lcom/google/android/maps/ZoomHelper$Snapshot;

    iget-object v12, v12, Lcom/google/android/maps/ZoomHelper$Snapshot;->fixedPointScreenCoords:[F

    const/4 v13, 0x0

    aget v2, v12, v13

    .line 254
    .local v2, "focusX":F
    iget-object v12, p0, Lcom/google/android/maps/ZoomHelper;->mSnapshot:Lcom/google/android/maps/ZoomHelper$Snapshot;

    iget-object v12, v12, Lcom/google/android/maps/ZoomHelper$Snapshot;->fixedPointScreenCoords:[F

    const/4 v13, 0x1

    aget v3, v12, v13

    .line 255
    .local v3, "focusY":F
    iget-object v12, p0, Lcom/google/android/maps/ZoomHelper;->mMapView:Lcom/google/android/maps/MapView;

    invoke-virtual {v12}, Lcom/google/android/maps/MapView;->getWidth()I

    move-result v12

    div-int/lit8 v12, v12, 0x2

    int-to-float v0, v12

    .line 256
    .local v0, "centerX":F
    iget-object v12, p0, Lcom/google/android/maps/ZoomHelper;->mMapView:Lcom/google/android/maps/MapView;

    invoke-virtual {v12}, Lcom/google/android/maps/MapView;->getHeight()I

    move-result v12

    div-int/lit8 v12, v12, 0x2

    int-to-float v1, v12

    .line 257
    .local v1, "centerY":F
    float-to-int v10, v2

    .line 258
    .local v10, "xOffset":I
    float-to-int v11, v3

    .line 259
    .local v11, "yOffset":I
    int-to-float v12, v10

    cmpl-float v12, v12, v0

    if-nez v12, :cond_0

    int-to-float v12, v11

    cmpl-float v12, v12, v1

    if-eqz v12, :cond_1

    .line 260
    :cond_0
    iget-object v12, p0, Lcom/google/android/maps/ZoomHelper;->mSnapshot:Lcom/google/android/maps/ZoomHelper$Snapshot;

    iget-object v12, v12, Lcom/google/android/maps/ZoomHelper$Snapshot;->fixedPoint:Lcom/google/android/maps/GeoPoint;

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-virtual {v7, v12, v13, v14}, Lcom/google/android/maps/PixelConverter;->toPixels(Lcom/google/android/maps/GeoPoint;Landroid/graphics/Point;Z)Landroid/graphics/Point;

    move-result-object v8

    .line 261
    .local v8, "realLocationOfFixedPoint":Landroid/graphics/Point;
    iget-object v12, p0, Lcom/google/android/maps/ZoomHelper;->mController:Lcom/google/android/maps/MapController;

    iget v13, v8, Landroid/graphics/Point;->x:I

    sub-int/2addr v13, v10

    iget v14, v8, Landroid/graphics/Point;->y:I

    sub-int/2addr v14, v11

    invoke-virtual {v12, v13, v14}, Lcom/google/android/maps/MapController;->scrollBy(II)V

    .line 266
    .end local v8    # "realLocationOfFixedPoint":Landroid/graphics/Point;
    :cond_1
    const-wide/16 v12, 0xc8

    invoke-direct {p0, v12, v13}, Lcom/google/android/maps/ZoomHelper;->addScale(J)V

    .line 271
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v12

    invoke-direct {p0, v12, v13, v7}, Lcom/google/android/maps/ZoomHelper;->stepAnimation(JLcom/google/android/maps/PixelConverter;)V

    .line 274
    iget-object v12, p0, Lcom/google/android/maps/ZoomHelper;->mMapView:Lcom/google/android/maps/MapView;

    invoke-virtual {v12}, Lcom/google/android/maps/MapView;->preLoad()V

    .line 276
    const/4 v12, 0x0

    iput-boolean v12, p0, Lcom/google/android/maps/ZoomHelper;->mManualZoomActive:Z

    .line 277
    const/high16 v12, 0x3f800000    # 1.0f

    iput v12, p0, Lcom/google/android/maps/ZoomHelper;->mSnapshotScale:F

    .line 278
    return-void
.end method

.method getCurrentScale()F
    .locals 1

    .prologue
    .line 281
    iget v0, p0, Lcom/google/android/maps/ZoomHelper;->mSnapshotScale:F

    return v0
.end method

.method onDraw(Landroid/graphics/Canvas;Lcom/google/android/maps/MapView;J)Z
    .locals 7
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "mapView"    # Lcom/google/android/maps/MapView;
    .param p3, "when"    # J

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x1

    const/16 v4, 0xff

    const/4 v1, 0x0

    .line 351
    iget-object v3, p0, Lcom/google/android/maps/ZoomHelper;->mSnapshot:Lcom/google/android/maps/ZoomHelper$Snapshot;

    if-nez v3, :cond_0

    .line 398
    :goto_0
    return v1

    .line 352
    :cond_0
    iget-object v3, p0, Lcom/google/android/maps/ZoomHelper;->mMapView:Lcom/google/android/maps/MapView;

    invoke-virtual {v3}, Lcom/google/android/maps/MapView;->getProjection()Lcom/google/android/maps/Projection;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/PixelConverter;

    .line 353
    .local v0, "converter":Lcom/google/android/maps/PixelConverter;
    invoke-virtual {p0, p3, p4}, Lcom/google/android/maps/ZoomHelper;->shouldDrawMap(J)Z

    move-result v3

    if-nez v3, :cond_1

    .line 354
    invoke-virtual {p1, v4, v4, v4, v4}, Landroid/graphics/Canvas;->drawARGB(IIII)V

    .line 357
    :cond_1
    iget-wide v4, p0, Lcom/google/android/maps/ZoomHelper;->mCommitTime:J

    cmp-long v3, p3, v4

    if-lez v3, :cond_2

    .line 358
    iget-object v3, p0, Lcom/google/android/maps/ZoomHelper;->mMapView:Lcom/google/android/maps/MapView;

    invoke-virtual {v3}, Lcom/google/android/maps/MapView;->preLoad()V

    .line 359
    const-wide v4, 0x7fffffffffffffffL

    iput-wide v4, p0, Lcom/google/android/maps/ZoomHelper;->mCommitTime:J

    .line 362
    :cond_2
    iget-boolean v3, p0, Lcom/google/android/maps/ZoomHelper;->mManualZoomActive:Z

    if-nez v3, :cond_3

    .line 364
    invoke-direct {p0, p3, p4, v0}, Lcom/google/android/maps/ZoomHelper;->stepAnimation(JLcom/google/android/maps/PixelConverter;)V

    .line 368
    :cond_3
    iget-object v3, p0, Lcom/google/android/maps/ZoomHelper;->mBitmapPaint:Landroid/graphics/Paint;

    const/high16 v4, 0x437f0000    # 255.0f

    iget-object v5, p0, Lcom/google/android/maps/ZoomHelper;->mLastDrawnScale:Landroid/view/animation/Transformation;

    invoke-virtual {v5}, Landroid/view/animation/Transformation;->getAlpha()F

    move-result v5

    mul-float/2addr v4, v5

    float-to-int v4, v4

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 370
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 371
    iget-object v3, p0, Lcom/google/android/maps/ZoomHelper;->mLastDrawnScale:Landroid/view/animation/Transformation;

    invoke-virtual {v3}, Landroid/view/animation/Transformation;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    .line 372
    iget v3, p0, Lcom/google/android/maps/ZoomHelper;->mSnapshotOffsetX:F

    iget v4, p0, Lcom/google/android/maps/ZoomHelper;->mSnapshotOffsetY:F

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 373
    iget-object v3, p0, Lcom/google/android/maps/ZoomHelper;->mSnapshot:Lcom/google/android/maps/ZoomHelper$Snapshot;

    iget-object v3, v3, Lcom/google/android/maps/ZoomHelper$Snapshot;->bitmap:Landroid/graphics/Bitmap;

    iget-object v4, p0, Lcom/google/android/maps/ZoomHelper;->mBitmapPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v6, v6, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 374
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 376
    iget-boolean v3, p0, Lcom/google/android/maps/ZoomHelper;->mManualZoomActive:Z

    if-nez v3, :cond_6

    iget-object v3, p0, Lcom/google/android/maps/ZoomHelper;->mAnimations:Landroid/view/animation/AnimationSet;

    invoke-virtual {v3}, Landroid/view/animation/AnimationSet;->hasEnded()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 377
    iget-boolean v3, p0, Lcom/google/android/maps/ZoomHelper;->mFading:Z

    if-eqz v3, :cond_4

    .line 380
    iget-object v2, p0, Lcom/google/android/maps/ZoomHelper;->mAnimations:Landroid/view/animation/AnimationSet;

    invoke-virtual {v2}, Landroid/view/animation/AnimationSet;->getAnimations()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 381
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/maps/ZoomHelper;->mSnapshot:Lcom/google/android/maps/ZoomHelper$Snapshot;

    .line 382
    iput-boolean v1, p0, Lcom/google/android/maps/ZoomHelper;->mFading:Z

    .line 383
    invoke-virtual {v0}, Lcom/google/android/maps/PixelConverter;->resetMatrix()V

    goto :goto_0

    .line 385
    :cond_4
    iget-object v3, p0, Lcom/google/android/maps/ZoomHelper;->mMapView:Lcom/google/android/maps/MapView;

    invoke-virtual {v3}, Lcom/google/android/maps/MapView;->canCoverCenter()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 388
    iput-boolean v2, p0, Lcom/google/android/maps/ZoomHelper;->mFading:Z

    .line 389
    invoke-direct {p0}, Lcom/google/android/maps/ZoomHelper;->addFade()V

    move v1, v2

    .line 390
    goto/16 :goto_0

    .line 395
    :cond_5
    iget-object v2, p0, Lcom/google/android/maps/ZoomHelper;->mMapView:Lcom/google/android/maps/MapView;

    iget-object v2, v2, Lcom/google/android/maps/MapView;->mRepainter:Lcom/google/android/maps/MapView$Repainter;

    invoke-virtual {v2}, Lcom/google/android/maps/MapView$Repainter;->repaint()V

    goto/16 :goto_0

    :cond_6
    move v1, v2

    .line 398
    goto/16 :goto_0
.end method

.method scrollBy(II)V
    .locals 3
    .param p1, "dx"    # I
    .param p2, "dy"    # I

    .prologue
    .line 232
    iget v0, p0, Lcom/google/android/maps/ZoomHelper;->mSnapshotOffsetX:F

    int-to-float v1, p1

    iget v2, p0, Lcom/google/android/maps/ZoomHelper;->mSnapshotScale:F

    div-float/2addr v1, v2

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/maps/ZoomHelper;->mSnapshotOffsetX:F

    .line 233
    iget v0, p0, Lcom/google/android/maps/ZoomHelper;->mSnapshotOffsetY:F

    int-to-float v1, p2

    iget v2, p0, Lcom/google/android/maps/ZoomHelper;->mSnapshotScale:F

    div-float/2addr v1, v2

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/maps/ZoomHelper;->mSnapshotOffsetY:F

    .line 234
    return-void
.end method

.method shouldDrawMap(J)Z
    .locals 1
    .param p1, "when"    # J

    .prologue
    .line 456
    iget-boolean v0, p0, Lcom/google/android/maps/ZoomHelper;->mManualZoomActive:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/maps/ZoomHelper;->mSnapshot:Lcom/google/android/maps/ZoomHelper$Snapshot;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/maps/ZoomHelper;->mFading:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/ZoomHelper;->mAnimations:Landroid/view/animation/AnimationSet;

    invoke-virtual {v0}, Landroid/view/animation/AnimationSet;->hasEnded()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method updateZoom(FFF)V
    .locals 7
    .param p1, "scale"    # F
    .param p2, "xOffset"    # F
    .param p3, "yOffset"    # F

    .prologue
    .line 217
    iget-object v2, p0, Lcom/google/android/maps/ZoomHelper;->mPCAtBeginningOfManualZoom:Lcom/google/android/maps/PixelConverter;

    iget-object v3, p0, Lcom/google/android/maps/ZoomHelper;->mPCAtBeginningOfManualZoom:Lcom/google/android/maps/PixelConverter;

    invoke-virtual {v3}, Lcom/google/android/maps/PixelConverter;->getInverseMatrix()Landroid/graphics/Matrix;

    move-result-object v3

    invoke-direct {p0, v2, v3, p2, p3}, Lcom/google/android/maps/ZoomHelper;->updateSnapshotFixedPoint(Lcom/google/android/maps/PixelConverter;Landroid/graphics/Matrix;FF)V

    .line 219
    iget-object v2, p0, Lcom/google/android/maps/ZoomHelper;->mLastDrawnScale:Landroid/view/animation/Transformation;

    invoke-virtual {v2}, Landroid/view/animation/Transformation;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v1

    .line 221
    .local v1, "m":Landroid/graphics/Matrix;
    neg-float v2, p2

    neg-float v3, p3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 222
    invoke-virtual {v1, p1, p1}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 223
    invoke-virtual {v1, p2, p3}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 224
    iget v2, p0, Lcom/google/android/maps/ZoomHelper;->mSnapshotScale:F

    mul-float/2addr v2, p1

    iput v2, p0, Lcom/google/android/maps/ZoomHelper;->mSnapshotScale:F

    .line 226
    iget-object v2, p0, Lcom/google/android/maps/ZoomHelper;->mMapView:Lcom/google/android/maps/MapView;

    invoke-virtual {v2}, Lcom/google/android/maps/MapView;->getProjection()Lcom/google/android/maps/Projection;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/PixelConverter;

    .line 227
    .local v0, "pc":Lcom/google/android/maps/PixelConverter;
    iget-object v2, p0, Lcom/google/android/maps/ZoomHelper;->mMapView:Lcom/google/android/maps/MapView;

    invoke-virtual {v2}, Lcom/google/android/maps/MapView;->getZoom()Landroid_maps_conflict_avoidance/com/google/map/Zoom;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/maps/ZoomHelper;->mSnapshot:Lcom/google/android/maps/ZoomHelper$Snapshot;

    iget-object v3, v3, Lcom/google/android/maps/ZoomHelper$Snapshot;->zoom:Landroid_maps_conflict_avoidance/com/google/map/Zoom;

    invoke-direct {p0, v2, v3}, Lcom/google/android/maps/ZoomHelper;->getScale(Landroid_maps_conflict_avoidance/com/google/map/Zoom;Landroid_maps_conflict_avoidance/com/google/map/Zoom;)F

    move-result v2

    iget-object v3, p0, Lcom/google/android/maps/ZoomHelper;->mSnapshot:Lcom/google/android/maps/ZoomHelper$Snapshot;

    iget-object v3, v3, Lcom/google/android/maps/ZoomHelper$Snapshot;->fixedPoint:Lcom/google/android/maps/GeoPoint;

    iget-object v4, p0, Lcom/google/android/maps/ZoomHelper;->mSnapshot:Lcom/google/android/maps/ZoomHelper$Snapshot;

    iget-object v4, v4, Lcom/google/android/maps/ZoomHelper$Snapshot;->fixedPointCoords:[F

    const/4 v5, 0x0

    aget v4, v4, v5

    iget-object v5, p0, Lcom/google/android/maps/ZoomHelper;->mSnapshot:Lcom/google/android/maps/ZoomHelper$Snapshot;

    iget-object v5, v5, Lcom/google/android/maps/ZoomHelper$Snapshot;->fixedPointCoords:[F

    const/4 v6, 0x1

    aget v5, v5, v6

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/maps/PixelConverter;->setMatrix(Landroid/graphics/Matrix;FLcom/google/android/maps/GeoPoint;FF)V

    .line 229
    return-void
.end method
