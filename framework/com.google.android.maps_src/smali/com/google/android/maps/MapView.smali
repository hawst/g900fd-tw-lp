.class public Lcom/google/android/maps/MapView;
.super Landroid/view/ViewGroup;
.source "MapView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/maps/MapView$LayoutParams;,
        Lcom/google/android/maps/MapView$Repainter;,
        Lcom/google/android/maps/MapView$ReticleDrawMode;
    }
.end annotation


# static fields
.field private static final KEY_CENTER_LATITUDE:Ljava/lang/String;

.field private static final KEY_CENTER_LONGITUDE:Ljava/lang/String;

.field private static final KEY_ZOOM_DISPLAYED:Ljava/lang/String;

.field private static final KEY_ZOOM_LEVEL:Ljava/lang/String;

.field private static final ZOOM_CONTROLS_TIMEOUT:J


# instance fields
.field private mBuiltInZoomControlsEnabled:Z

.field private mController:Lcom/google/android/maps/MapController;

.field private mConverter:Lcom/google/android/maps/PixelConverter;

.field private final mDrawer:Landroid_maps_conflict_avoidance/com/google/common/graphics/android/AndroidGraphics;

.field private mFakeStreetViewEnabled:Z

.field private mGestureDetector:Lcom/google/android/maps/GestureDetector;

.field private final mGoogleLogo:Landroid/graphics/drawable/Drawable;

.field private final mGoogleLogoHeight:I

.field private final mGoogleLogoWidth:I

.field private mHandler:Landroid/os/Handler;

.field final mKey:Ljava/lang/String;

.field private mLastFlingX:I

.field private mLastFlingY:I

.field private mMap:Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;

.field private mOverlayBundle:Lcom/google/android/maps/OverlayBundle;

.field private mOverlayRenderer:Landroid_maps_conflict_avoidance/com/google/googlenav/ui/android/AndroidTileOverlayRenderer;

.field final mRepainter:Lcom/google/android/maps/MapView$Repainter;

.field private mReticle:Landroid/graphics/drawable/Drawable;

.field private mReticleDrawMode:Lcom/google/android/maps/MapView$ReticleDrawMode;

.field private mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

.field private final mScroller:Landroid/widget/Scroller;

.field private mTrackballGestureDetector:Lcom/google/android/maps/TrackballGestureDetector;

.field private mZoomButtonsController:Landroid/widget/ZoomButtonsController;

.field private mZoomControlRunnable:Ljava/lang/Runnable;

.field private mZoomControls:Landroid/widget/ZoomControls;

.field private mZoomHelper:Lcom/google/android/maps/ZoomHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 134
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/google/android/maps/MapView;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".zoomDisplayed"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/maps/MapView;->KEY_ZOOM_DISPLAYED:Ljava/lang/String;

    .line 136
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/google/android/maps/MapView;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".centerLatitude"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/maps/MapView;->KEY_CENTER_LATITUDE:Ljava/lang/String;

    .line 138
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/google/android/maps/MapView;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".centerLongitude"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/maps/MapView;->KEY_CENTER_LONGITUDE:Ljava/lang/String;

    .line 140
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/google/android/maps/MapView;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".zoomLevel"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/maps/MapView;->KEY_ZOOM_LEVEL:Ljava/lang/String;

    .line 143
    invoke-static {}, Landroid/view/ViewConfiguration;->getZoomControlsTimeout()J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/maps/MapView;->ZOOM_CONTROLS_TIMEOUT:J

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 247
    sget v0, Lcom/google/android/maps/InternalR$attr;->mapViewStyle:I

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/maps/MapView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 248
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 264
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/maps/MapView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILjava/lang/String;)V

    .line 265
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILjava/lang/String;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I
    .param p4, "apiKey"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 271
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 154
    new-instance v1, Landroid_maps_conflict_avoidance/com/google/common/graphics/android/AndroidGraphics;

    invoke-direct {v1, v2}, Landroid_maps_conflict_avoidance/com/google/common/graphics/android/AndroidGraphics;-><init>(Landroid/graphics/Canvas;)V

    iput-object v1, p0, Lcom/google/android/maps/MapView;->mDrawer:Landroid_maps_conflict_avoidance/com/google/common/graphics/android/AndroidGraphics;

    .line 163
    iput-object v2, p0, Lcom/google/android/maps/MapView;->mOverlayBundle:Lcom/google/android/maps/OverlayBundle;

    .line 181
    iput-object v2, p0, Lcom/google/android/maps/MapView;->mReticle:Landroid/graphics/drawable/Drawable;

    .line 221
    new-instance v1, Lcom/google/android/maps/MapView$Repainter;

    invoke-direct {v1, p0}, Lcom/google/android/maps/MapView$Repainter;-><init>(Lcom/google/android/maps/MapView;)V

    iput-object v1, p0, Lcom/google/android/maps/MapView;->mRepainter:Lcom/google/android/maps/MapView$Repainter;

    .line 273
    if-nez p4, :cond_0

    .line 274
    sget-object v1, Lcom/google/android/maps/InternalR$styleable;->MapView:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 276
    .local v0, "a":Landroid/content/res/TypedArray;
    sget v1, Lcom/google/android/maps/InternalR$styleable;->MapView_apiKey:I

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/maps/MapView;->mKey:Ljava/lang/String;

    .line 277
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 282
    .end local v0    # "a":Landroid/content/res/TypedArray;
    :goto_0
    iget-object v1, p0, Lcom/google/android/maps/MapView;->mKey:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 283
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "You need to specify an API Key for each MapView.  See the MapView documentation for details."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 279
    :cond_0
    iput-object p4, p0, Lcom/google/android/maps/MapView;->mKey:Ljava/lang/String;

    goto :goto_0

    .line 287
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/maps/MapView;->setWillNotDraw(Z)V

    .line 288
    instance-of v1, p1, Lcom/google/android/maps/MapActivity;

    if-eqz v1, :cond_2

    move-object v1, p1

    .line 289
    check-cast v1, Lcom/google/android/maps/MapActivity;

    invoke-virtual {v1, p0}, Lcom/google/android/maps/MapActivity;->setupMapView(Lcom/google/android/maps/MapView;)V

    .line 294
    new-instance v1, Landroid/widget/Scroller;

    invoke-direct {v1, p1}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/maps/MapView;->mScroller:Landroid/widget/Scroller;

    .line 295
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/maps/InternalR$drawable;->maps_google_logo:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/maps/MapView;->mGoogleLogo:Landroid/graphics/drawable/Drawable;

    .line 297
    iget-object v1, p0, Lcom/google/android/maps/MapView;->mGoogleLogo:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    iput v1, p0, Lcom/google/android/maps/MapView;->mGoogleLogoWidth:I

    .line 298
    iget-object v1, p0, Lcom/google/android/maps/MapView;->mGoogleLogo:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    iput v1, p0, Lcom/google/android/maps/MapView;->mGoogleLogoHeight:I

    .line 299
    return-void

    .line 291
    :cond_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "MapViews can only be created inside instances of MapActivity."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "apiKey"    # Ljava/lang/String;

    .prologue
    .line 235
    const/4 v0, 0x0

    sget v1, Lcom/google/android/maps/InternalR$attr;->mapViewStyle:I

    invoke-direct {p0, p1, v0, v1, p2}, Lcom/google/android/maps/MapView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILjava/lang/String;)V

    .line 236
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/maps/MapView;)Landroid/widget/Scroller;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/maps/MapView;

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mScroller:Landroid/widget/Scroller;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/maps/MapView;)Lcom/google/android/maps/ZoomHelper;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/maps/MapView;

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mZoomHelper:Lcom/google/android/maps/ZoomHelper;

    return-object v0
.end method

.method static synthetic access$1000()J
    .locals 2

    .prologue
    .line 115
    sget-wide v0, Lcom/google/android/maps/MapView;->ZOOM_CONTROLS_TIMEOUT:J

    return-wide v0
.end method

.method static synthetic access$1100(Lcom/google/android/maps/MapView;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/maps/MapView;

    .prologue
    .line 115
    invoke-direct {p0}, Lcom/google/android/maps/MapView;->updateZoomControls()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/maps/MapView;)Lcom/google/android/maps/MapController;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/maps/MapView;

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mController:Lcom/google/android/maps/MapController;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/maps/MapView;)Lcom/google/android/maps/PixelConverter;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/maps/MapView;

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mConverter:Lcom/google/android/maps/PixelConverter;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/maps/MapView;)Lcom/google/android/maps/OverlayBundle;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/maps/MapView;

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mOverlayBundle:Lcom/google/android/maps/OverlayBundle;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/maps/MapView;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/maps/MapView;

    .prologue
    .line 115
    iget v0, p0, Lcom/google/android/maps/MapView;->mLastFlingX:I

    return v0
.end method

.method static synthetic access$502(Lcom/google/android/maps/MapView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/maps/MapView;
    .param p1, "x1"    # I

    .prologue
    .line 115
    iput p1, p0, Lcom/google/android/maps/MapView;->mLastFlingX:I

    return p1
.end method

.method static synthetic access$602(Lcom/google/android/maps/MapView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/maps/MapView;
    .param p1, "x1"    # I

    .prologue
    .line 115
    iput p1, p0, Lcom/google/android/maps/MapView;->mLastFlingY:I

    return p1
.end method

.method static synthetic access$700(Lcom/google/android/maps/MapView;)Landroid/widget/ZoomControls;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/maps/MapView;

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mZoomControls:Landroid/widget/ZoomControls;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/maps/MapView;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/maps/MapView;

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mZoomControlRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/maps/MapView;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/maps/MapView;

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private canZoomIn()Z
    .locals 2

    .prologue
    .line 1555
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mMap:Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;

    invoke-virtual {v0}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;->getZoom()Landroid_maps_conflict_avoidance/com/google/map/Zoom;

    move-result-object v0

    invoke-virtual {v0}, Landroid_maps_conflict_avoidance/com/google/map/Zoom;->getZoomLevel()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/maps/MapView;->getMaxZoomLevel()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private canZoomOut()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1551
    iget-object v1, p0, Lcom/google/android/maps/MapView;->mMap:Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;

    invoke-virtual {v1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;->getZoom()Landroid_maps_conflict_avoidance/com/google/map/Zoom;

    move-result-object v1

    invoke-virtual {v1}, Landroid_maps_conflict_avoidance/com/google/map/Zoom;->getZoomLevel()I

    move-result v1

    if-le v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private createZoomButtonsController()Landroid/widget/ZoomButtonsController;
    .locals 4

    .prologue
    .line 1446
    new-instance v0, Landroid/widget/ZoomButtonsController;

    invoke-direct {v0, p0}, Landroid/widget/ZoomButtonsController;-><init>(Landroid/view/View;)V

    .line 1447
    .local v0, "controller":Landroid/widget/ZoomButtonsController;
    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v2, v3}, Landroid/widget/ZoomButtonsController;->setZoomSpeed(J)V

    .line 1448
    new-instance v1, Lcom/google/android/maps/MapView$6;

    invoke-direct {v1, p0, v0}, Lcom/google/android/maps/MapView$6;-><init>(Lcom/google/android/maps/MapView;Landroid/widget/ZoomButtonsController;)V

    invoke-virtual {v0, v1}, Landroid/widget/ZoomButtonsController;->setOnZoomListener(Landroid/widget/ZoomButtonsController$OnZoomListener;)V

    .line 1487
    return-object v0
.end method

.method private createZoomControls()Landroid/widget/ZoomControls;
    .locals 4

    .prologue
    .line 1418
    new-instance v0, Landroid/widget/ZoomControls;

    iget-object v1, p0, Lcom/google/android/maps/MapView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ZoomControls;-><init>(Landroid/content/Context;)V

    .line 1419
    .local v0, "zoomControls":Landroid/widget/ZoomControls;
    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v2, v3}, Landroid/widget/ZoomControls;->setZoomSpeed(J)V

    .line 1420
    new-instance v1, Lcom/google/android/maps/MapView$4;

    invoke-direct {v1, p0}, Lcom/google/android/maps/MapView$4;-><init>(Lcom/google/android/maps/MapView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ZoomControls;->setOnZoomInClickListener(Landroid/view/View$OnClickListener;)V

    .line 1425
    new-instance v1, Lcom/google/android/maps/MapView$5;

    invoke-direct {v1, p0}, Lcom/google/android/maps/MapView$5;-><init>(Lcom/google/android/maps/MapView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ZoomControls;->setOnZoomOutClickListener(Landroid/view/View$OnClickListener;)V

    .line 1430
    return-object v0
.end method

.method private isLocationDisplayed()Z
    .locals 1

    .prologue
    .line 1072
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mContext:Landroid/content/Context;

    instance-of v0, v0, Lcom/google/android/maps/MapActivity;

    if-eqz v0, :cond_0

    .line 1073
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/google/android/maps/MapActivity;

    invoke-virtual {v0}, Lcom/google/android/maps/MapActivity;->isLocationDisplayed()Z

    move-result v0

    .line 1076
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isRouteDisplayed()Z
    .locals 1

    .prologue
    .line 1080
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mContext:Landroid/content/Context;

    instance-of v0, v0, Lcom/google/android/maps/MapActivity;

    if-eqz v0, :cond_0

    .line 1081
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/google/android/maps/MapActivity;

    invoke-virtual {v0}, Lcom/google/android/maps/MapActivity;->isRouteDisplayed()Z

    move-result v0

    .line 1084
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateZoomControls()V
    .locals 2

    .prologue
    .line 1540
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mZoomControls:Landroid/widget/ZoomControls;

    if-eqz v0, :cond_0

    .line 1541
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mZoomControls:Landroid/widget/ZoomControls;

    invoke-direct {p0}, Lcom/google/android/maps/MapView;->canZoomIn()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ZoomControls;->setIsZoomInEnabled(Z)V

    .line 1542
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mZoomControls:Landroid/widget/ZoomControls;

    invoke-direct {p0}, Lcom/google/android/maps/MapView;->canZoomOut()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ZoomControls;->setIsZoomOutEnabled(Z)V

    .line 1544
    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mZoomButtonsController:Landroid/widget/ZoomButtonsController;

    if-eqz v0, :cond_1

    .line 1545
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mZoomButtonsController:Landroid/widget/ZoomButtonsController;

    invoke-direct {p0}, Lcom/google/android/maps/MapView;->canZoomIn()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ZoomButtonsController;->setZoomInEnabled(Z)V

    .line 1546
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mZoomButtonsController:Landroid/widget/ZoomButtonsController;

    invoke-direct {p0}, Lcom/google/android/maps/MapView;->canZoomOut()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ZoomButtonsController;->setZoomOutEnabled(Z)V

    .line 1548
    :cond_1
    return-void
.end method


# virtual methods
.method public canCoverCenter()Z
    .locals 3

    .prologue
    .line 1104
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mMap:Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;

    iget-object v1, p0, Lcom/google/android/maps/MapView;->mMap:Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;

    invoke-virtual {v1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;->getCenterPoint()Landroid_maps_conflict_avoidance/com/google/map/MapPoint;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;->canCover(Landroid_maps_conflict_avoidance/com/google/map/MapPoint;Z)Z

    move-result v0

    return v0
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1
    .param p1, "p"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 784
    instance-of v0, p1, Lcom/google/android/maps/MapView$LayoutParams;

    return v0
.end method

.method cleanupMapReferences(Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;)V
    .locals 1
    .param p1, "dispatcher"    # Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;

    .prologue
    .line 411
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mRepainter:Lcom/google/android/maps/MapView$Repainter;

    invoke-virtual {p1, v0}, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->removeDataRequestListener(Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestListener;)V

    .line 412
    return-void
.end method

.method public computeScroll()V
    .locals 4

    .prologue
    .line 485
    iget-object v2, p0, Lcom/google/android/maps/MapView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 486
    iget-object v2, p0, Lcom/google/android/maps/MapView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->getCurrX()I

    move-result v2

    iget v3, p0, Lcom/google/android/maps/MapView;->mLastFlingX:I

    sub-int v0, v2, v3

    .line 487
    .local v0, "x":I
    iget-object v2, p0, Lcom/google/android/maps/MapView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->getCurrY()I

    move-result v2

    iget v3, p0, Lcom/google/android/maps/MapView;->mLastFlingY:I

    sub-int v1, v2, v3

    .line 488
    .local v1, "y":I
    iget-object v2, p0, Lcom/google/android/maps/MapView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->getCurrX()I

    move-result v2

    iput v2, p0, Lcom/google/android/maps/MapView;->mLastFlingX:I

    .line 489
    iget-object v2, p0, Lcom/google/android/maps/MapView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->getCurrY()I

    move-result v2

    iput v2, p0, Lcom/google/android/maps/MapView;->mLastFlingY:I

    .line 490
    iget-object v2, p0, Lcom/google/android/maps/MapView;->mController:Lcom/google/android/maps/MapController;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/maps/MapController;->scrollBy(II)V

    .line 491
    invoke-virtual {p0}, Lcom/google/android/maps/MapView;->postInvalidate()V

    .line 495
    .end local v0    # "x":I
    .end local v1    # "y":I
    :goto_0
    return-void

    .line 493
    :cond_0
    invoke-super {p0}, Landroid/view/ViewGroup;->computeScroll()V

    goto :goto_0
.end method

.method public displayZoomControls(Z)V
    .locals 4
    .param p1, "takeFocus"    # Z

    .prologue
    .line 1052
    iget-boolean v0, p0, Lcom/google/android/maps/MapView;->mBuiltInZoomControlsEnabled:Z

    if-eqz v0, :cond_1

    .line 1053
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mZoomButtonsController:Landroid/widget/ZoomButtonsController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/MapView;->mZoomButtonsController:Landroid/widget/ZoomButtonsController;

    invoke-virtual {v0}, Landroid/widget/ZoomButtonsController;->isVisible()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1054
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mZoomButtonsController:Landroid/widget/ZoomButtonsController;

    invoke-virtual {v0, p1}, Landroid/widget/ZoomButtonsController;->setFocusable(Z)V

    .line 1055
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mZoomButtonsController:Landroid/widget/ZoomButtonsController;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ZoomButtonsController;->setVisible(Z)V

    .line 1069
    :cond_0
    :goto_0
    return-void

    .line 1058
    :cond_1
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mZoomControls:Landroid/widget/ZoomControls;

    if-eqz v0, :cond_0

    .line 1059
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mZoomControls:Landroid/widget/ZoomControls;

    invoke-virtual {v0}, Landroid/widget/ZoomControls;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_2

    .line 1060
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mZoomControls:Landroid/widget/ZoomControls;

    invoke-virtual {v0}, Landroid/widget/ZoomControls;->show()V

    .line 1062
    :cond_2
    if-eqz p1, :cond_3

    .line 1063
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mZoomControls:Landroid/widget/ZoomControls;

    invoke-virtual {v0}, Landroid/widget/ZoomControls;->requestFocus()Z

    .line 1065
    :cond_3
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/maps/MapView;->mZoomControlRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1066
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/maps/MapView;->mZoomControlRunnable:Ljava/lang/Runnable;

    sget-wide v2, Lcom/google/android/maps/MapView;->ZOOM_CONTROLS_TIMEOUT:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method doZoom(Z)Z
    .locals 2
    .param p1, "zoomIn"    # Z

    .prologue
    .line 1528
    invoke-virtual {p0}, Lcom/google/android/maps/MapView;->getMeasuredWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/maps/MapView;->getMeasuredHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/android/maps/MapView;->doZoom(ZII)Z

    move-result v0

    return v0
.end method

.method doZoom(ZII)Z
    .locals 3
    .param p1, "zoomIn"    # Z
    .param p2, "xOffset"    # I
    .param p3, "yOffset"    # I

    .prologue
    .line 1517
    const/4 v0, 0x0

    .line 1518
    .local v0, "success":Z
    if-eqz p1, :cond_1

    invoke-direct {p0}, Lcom/google/android/maps/MapView;->canZoomIn()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1519
    :goto_0
    iget-object v1, p0, Lcom/google/android/maps/MapView;->mZoomHelper:Lcom/google/android/maps/ZoomHelper;

    const/4 v2, 0x1

    invoke-virtual {v1, p1, v2, p2, p3}, Lcom/google/android/maps/ZoomHelper;->doZoom(ZZII)Z

    .line 1520
    const/4 v0, 0x1

    .line 1522
    :cond_0
    invoke-direct {p0}, Lcom/google/android/maps/MapView;->updateZoomControls()V

    .line 1523
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/maps/MapView;->displayZoomControls(Z)V

    .line 1524
    return v0

    .line 1518
    :cond_1
    invoke-direct {p0}, Lcom/google/android/maps/MapView;->canZoomOut()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0
.end method

.method drawMap(Landroid/graphics/Canvas;Z)Z
    .locals 8
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "fetchTiles"    # Z

    .prologue
    .line 1091
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mDrawer:Landroid_maps_conflict_avoidance/com/google/common/graphics/android/AndroidGraphics;

    invoke-virtual {v0, p1}, Landroid_maps_conflict_avoidance/com/google/common/graphics/android/AndroidGraphics;->setCanvas(Landroid/graphics/Canvas;)V

    .line 1092
    const/4 v7, 0x1

    .line 1093
    .local v7, "drawIncompleteTiles":Z
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mMap:Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;

    iget-object v1, p0, Lcom/google/android/maps/MapView;->mDrawer:Landroid_maps_conflict_avoidance/com/google/common/graphics/android/AndroidGraphics;

    invoke-direct {p0}, Lcom/google/android/maps/MapView;->isLocationDisplayed()Z

    move-result v3

    invoke-direct {p0}, Lcom/google/android/maps/MapView;->isRouteDisplayed()Z

    move-result v4

    const/4 v5, 0x1

    const/4 v6, 0x0

    move v2, p2

    invoke-virtual/range {v0 .. v6}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;->drawMap(Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleGraphics;ZZZZZ)Z

    move-result v0

    return v0
.end method

.method protected bridge synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 115
    invoke-virtual {p0}, Lcom/google/android/maps/MapView;->generateDefaultLayoutParams()Lcom/google/android/maps/MapView$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method protected generateDefaultLayoutParams()Lcom/google/android/maps/MapView$LayoutParams;
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v3, -0x2

    .line 708
    new-instance v0, Lcom/google/android/maps/MapView$LayoutParams;

    new-instance v1, Lcom/google/android/maps/GeoPoint;

    invoke-direct {v1, v2, v2}, Lcom/google/android/maps/GeoPoint;-><init>(II)V

    const/16 v2, 0x11

    invoke-direct {v0, v3, v3, v1, v2}, Lcom/google/android/maps/MapView$LayoutParams;-><init>(IILcom/google/android/maps/GeoPoint;I)V

    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 2
    .param p1, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 773
    new-instance v0, Lcom/google/android/maps/MapView$LayoutParams;

    invoke-virtual {p0}, Lcom/google/android/maps/MapView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/google/android/maps/MapView$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1
    .param p1, "p"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 788
    new-instance v0, Lcom/google/android/maps/MapView$LayoutParams;

    invoke-direct {v0, p1}, Lcom/google/android/maps/MapView$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method public getController()Lcom/google/android/maps/MapController;
    .locals 1

    .prologue
    .line 1237
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mController:Lcom/google/android/maps/MapController;

    return-object v0
.end method

.method public getLatitudeSpan()I
    .locals 1

    .prologue
    .line 1264
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mMap:Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;

    invoke-virtual {v0}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;->getLatitudeSpan()I

    move-result v0

    return v0
.end method

.method public getLongitudeSpan()I
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 1275
    iget-object v8, p0, Lcom/google/android/maps/MapView;->mMap:Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;

    invoke-virtual {v8}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;->getMapState()Landroid_maps_conflict_avoidance/com/google/map/MapState;

    move-result-object v3

    .line 1276
    .local v3, "mapState":Landroid_maps_conflict_avoidance/com/google/map/MapState;
    invoke-virtual {v3}, Landroid_maps_conflict_avoidance/com/google/map/MapState;->getCenterPoint()Landroid_maps_conflict_avoidance/com/google/map/MapPoint;

    move-result-object v4

    .line 1277
    .local v4, "point":Landroid_maps_conflict_avoidance/com/google/map/MapPoint;
    invoke-virtual {v3}, Landroid_maps_conflict_avoidance/com/google/map/MapState;->getZoom()Landroid_maps_conflict_avoidance/com/google/map/Zoom;

    move-result-object v7

    .line 1278
    .local v7, "zoom":Landroid_maps_conflict_avoidance/com/google/map/Zoom;
    iget-object v8, p0, Lcom/google/android/maps/MapView;->mMap:Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;

    invoke-virtual {v8}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;->getWidth()I

    move-result v8

    div-int/lit8 v1, v8, 0x2

    .line 1282
    .local v1, "halfWidth":I
    const/4 v6, 0x0

    .line 1283
    .local v6, "shift":I
    invoke-virtual {v7}, Landroid_maps_conflict_avoidance/com/google/map/Zoom;->getZoomLevel()I

    move-result v8

    const/4 v9, 0x1

    if-ne v8, v9, :cond_0

    .line 1284
    const/4 v6, 0x2

    .line 1285
    shr-int/lit8 v1, v1, 0x2

    .line 1287
    :cond_0
    neg-int v8, v1

    invoke-virtual {v4, v8, v10, v7}, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->pixelOffset(IILandroid_maps_conflict_avoidance/com/google/map/Zoom;)Landroid_maps_conflict_avoidance/com/google/map/MapPoint;

    move-result-object v2

    .line 1288
    .local v2, "left":Landroid_maps_conflict_avoidance/com/google/map/MapPoint;
    invoke-virtual {v4, v1, v10, v7}, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->pixelOffset(IILandroid_maps_conflict_avoidance/com/google/map/Zoom;)Landroid_maps_conflict_avoidance/com/google/map/MapPoint;

    move-result-object v5

    .line 1289
    .local v5, "right":Landroid_maps_conflict_avoidance/com/google/map/MapPoint;
    invoke-virtual {v5}, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->getLongitude()I

    move-result v8

    invoke-virtual {v2}, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->getLongitude()I

    move-result v9

    sub-int v0, v8, v9

    .line 1290
    .local v0, "diff":I
    if-gtz v0, :cond_1

    .line 1291
    const v8, 0x15752a00

    add-int/2addr v0, v8

    .line 1293
    :cond_1
    shl-int v8, v0, v6

    return v8
.end method

.method public getMapCenter()Lcom/google/android/maps/GeoPoint;
    .locals 2

    .prologue
    .line 1227
    new-instance v0, Lcom/google/android/maps/GeoPoint;

    iget-object v1, p0, Lcom/google/android/maps/MapView;->mMap:Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;

    invoke-virtual {v1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;->getCenterPoint()Landroid_maps_conflict_avoidance/com/google/map/MapPoint;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/maps/GeoPoint;-><init>(Landroid_maps_conflict_avoidance/com/google/map/MapPoint;)V

    return-object v0
.end method

.method public getMaxZoomLevel()I
    .locals 2

    .prologue
    .line 1324
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mMap:Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;

    iget-object v1, p0, Lcom/google/android/maps/MapView;->mMap:Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;

    invoke-virtual {v1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;->getCenterPoint()Landroid_maps_conflict_avoidance/com/google/map/MapPoint;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;->getMaxMapZoomForPoint(Landroid_maps_conflict_avoidance/com/google/map/MapPoint;)I

    move-result v0

    return v0
.end method

.method public final getOverlays()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/maps/Overlay;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1254
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mOverlayBundle:Lcom/google/android/maps/OverlayBundle;

    invoke-virtual {v0}, Lcom/google/android/maps/OverlayBundle;->getOverlays()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getProjection()Lcom/google/android/maps/Projection;
    .locals 1

    .prologue
    .line 1566
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mConverter:Lcom/google/android/maps/PixelConverter;

    return-object v0
.end method

.method getZoom()Landroid_maps_conflict_avoidance/com/google/map/Zoom;
    .locals 1

    .prologue
    .line 1116
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mMap:Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;

    invoke-virtual {v0}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;->getZoom()Landroid_maps_conflict_avoidance/com/google/map/Zoom;

    move-result-object v0

    return-object v0
.end method

.method public getZoomButtonsController()Landroid/widget/ZoomButtonsController;
    .locals 1

    .prologue
    .line 1442
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mZoomButtonsController:Landroid/widget/ZoomButtonsController;

    return-object v0
.end method

.method public getZoomControls()Landroid/view/View;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1395
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mZoomControls:Landroid/widget/ZoomControls;

    if-nez v0, :cond_0

    .line 1396
    invoke-direct {p0}, Lcom/google/android/maps/MapView;->createZoomControls()Landroid/widget/ZoomControls;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/MapView;->mZoomControls:Landroid/widget/ZoomControls;

    .line 1397
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mZoomControls:Landroid/widget/ZoomControls;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ZoomControls;->setVisibility(I)V

    .line 1398
    new-instance v0, Lcom/google/android/maps/MapView$3;

    invoke-direct {v0, p0}, Lcom/google/android/maps/MapView$3;-><init>(Lcom/google/android/maps/MapView;)V

    iput-object v0, p0, Lcom/google/android/maps/MapView;->mZoomControlRunnable:Ljava/lang/Runnable;

    .line 1414
    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mZoomControls:Landroid/widget/ZoomControls;

    return-object v0
.end method

.method public getZoomLevel()I
    .locals 1

    .prologue
    .line 1128
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mMap:Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;

    invoke-virtual {v0}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;->getZoom()Landroid_maps_conflict_avoidance/com/google/map/Zoom;

    move-result-object v0

    invoke-virtual {v0}, Landroid_maps_conflict_avoidance/com/google/map/Zoom;->getZoomLevel()I

    move-result v0

    return v0
.end method

.method public isOpaque()Z
    .locals 1

    .prologue
    .line 499
    const/4 v0, 0x1

    return v0
.end method

.method public isSatellite()Z
    .locals 1

    .prologue
    .line 1160
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mMap:Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;

    invoke-virtual {v0}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;->isSatellite()Z

    move-result v0

    return v0
.end method

.method public isStreetView()Z
    .locals 1

    .prologue
    .line 1217
    iget-boolean v0, p0, Lcom/google/android/maps/MapView;->mFakeStreetViewEnabled:Z

    return v0
.end method

.method public isTraffic()Z
    .locals 1

    .prologue
    .line 1187
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mOverlayRenderer:Landroid_maps_conflict_avoidance/com/google/googlenav/ui/android/AndroidTileOverlayRenderer;

    invoke-virtual {v0}, Landroid_maps_conflict_avoidance/com/google/googlenav/ui/android/AndroidTileOverlayRenderer;->isShowTraffic()Z

    move-result v0

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 475
    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    .line 476
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mZoomButtonsController:Landroid/widget/ZoomButtonsController;

    if-eqz v0, :cond_0

    .line 477
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mZoomButtonsController:Landroid/widget/ZoomButtonsController;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ZoomButtonsController;->setVisible(Z)V

    .line 479
    :cond_0
    return-void
.end method

.method protected final onDraw(Landroid/graphics/Canvas;)V
    .locals 10
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 509
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mController:Lcom/google/android/maps/MapController;

    invoke-virtual {v0}, Lcom/google/android/maps/MapController;->isDirty()Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, p0

    move v3, v2

    move v4, v2

    move v5, v2

    .line 511
    invoke-virtual/range {v0 .. v5}, Lcom/google/android/maps/MapView;->onLayout(ZIIII)V

    .line 514
    :cond_0
    const/4 v7, 0x1

    .line 515
    .local v7, "fetchTiles":Z
    const/4 v6, 0x0

    .line 517
    .local v6, "drawAgain":Z
    invoke-virtual {p0}, Lcom/google/android/maps/MapView;->getDrawingTime()J

    move-result-wide v8

    .line 521
    .local v8, "drawTime":J
    if-nez v6, :cond_5

    move v7, v1

    .line 523
    :goto_0
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mZoomHelper:Lcom/google/android/maps/ZoomHelper;

    invoke-virtual {v0, v8, v9}, Lcom/google/android/maps/ZoomHelper;->shouldDrawMap(J)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 524
    invoke-virtual {p0, p1, v7}, Lcom/google/android/maps/MapView;->drawMap(Landroid/graphics/Canvas;Z)Z

    move-result v0

    if-nez v0, :cond_6

    :goto_1
    or-int/2addr v6, v1

    .line 527
    :cond_1
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mReticleDrawMode:Lcom/google/android/maps/MapView$ReticleDrawMode;

    sget-object v1, Lcom/google/android/maps/MapView$ReticleDrawMode;->DRAW_RETICLE_UNDER:Lcom/google/android/maps/MapView$ReticleDrawMode;

    if-ne v0, v1, :cond_2

    invoke-virtual {p0}, Lcom/google/android/maps/MapView;->isInTouchMode()Z

    move-result v0

    if-nez v0, :cond_2

    .line 528
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mReticle:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 531
    :cond_2
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mZoomHelper:Lcom/google/android/maps/ZoomHelper;

    invoke-virtual {v0, p1, p0, v8, v9}, Lcom/google/android/maps/ZoomHelper;->onDraw(Landroid/graphics/Canvas;Lcom/google/android/maps/MapView;J)Z

    move-result v0

    or-int/2addr v6, v0

    .line 532
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mOverlayBundle:Lcom/google/android/maps/OverlayBundle;

    invoke-virtual {v0, p1, p0, v8, v9}, Lcom/google/android/maps/OverlayBundle;->draw(Landroid/graphics/Canvas;Lcom/google/android/maps/MapView;J)Z

    move-result v0

    or-int/2addr v6, v0

    .line 534
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mReticleDrawMode:Lcom/google/android/maps/MapView$ReticleDrawMode;

    sget-object v1, Lcom/google/android/maps/MapView$ReticleDrawMode;->DRAW_RETICLE_OVER:Lcom/google/android/maps/MapView$ReticleDrawMode;

    if-ne v0, v1, :cond_3

    invoke-virtual {p0}, Lcom/google/android/maps/MapView;->isInTouchMode()Z

    move-result v0

    if-nez v0, :cond_3

    .line 535
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mReticle:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 539
    :cond_3
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mGoogleLogo:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 541
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mController:Lcom/google/android/maps/MapController;

    invoke-virtual {v0, v8, v9}, Lcom/google/android/maps/MapController;->stepAnimation(J)Z

    move-result v0

    or-int/2addr v6, v0

    .line 543
    if-eqz v6, :cond_4

    .line 544
    invoke-virtual {p0}, Lcom/google/android/maps/MapView;->requestLayout()V

    .line 545
    invoke-virtual {p0}, Lcom/google/android/maps/MapView;->invalidate()V

    .line 547
    :cond_4
    return-void

    :cond_5
    move v7, v2

    .line 521
    goto :goto_0

    :cond_6
    move v1, v2

    .line 524
    goto :goto_1
.end method

.method public onFocusChanged(ZILandroid/graphics/Rect;)V
    .locals 1
    .param p1, "hasFocus"    # Z
    .param p2, "direction"    # I
    .param p3, "unused"    # Landroid/graphics/Rect;

    .prologue
    .line 614
    if-nez p1, :cond_0

    .line 615
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mController:Lcom/google/android/maps/MapController;

    invoke-virtual {v0}, Lcom/google/android/maps/MapController;->stopPanning()V

    .line 617
    :cond_0
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onWindowFocusChanged(Z)V

    .line 618
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v0, 0x1

    .line 625
    iget-object v1, p0, Lcom/google/android/maps/MapView;->mOverlayBundle:Lcom/google/android/maps/OverlayBundle;

    invoke-virtual {v1, p1, p2, p0}, Lcom/google/android/maps/OverlayBundle;->onKeyDown(ILandroid/view/KeyEvent;Lcom/google/android/maps/MapView;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 627
    :cond_0
    :goto_0
    return v0

    .line 626
    :cond_1
    iget-object v1, p0, Lcom/google/android/maps/MapView;->mController:Lcom/google/android/maps/MapController;

    invoke-virtual {v1, p0, p1, p2}, Lcom/google/android/maps/MapController;->onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 627
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v0, 0x1

    .line 635
    iget-object v1, p0, Lcom/google/android/maps/MapView;->mOverlayBundle:Lcom/google/android/maps/OverlayBundle;

    invoke-virtual {v1, p1, p2, p0}, Lcom/google/android/maps/OverlayBundle;->onKeyUp(ILandroid/view/KeyEvent;Lcom/google/android/maps/MapView;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 637
    :cond_0
    :goto_0
    return v0

    .line 636
    :cond_1
    iget-object v1, p0, Lcom/google/android/maps/MapView;->mController:Lcom/google/android/maps/MapController;

    invoke-virtual {v1, p0, p1, p2}, Lcom/google/android/maps/MapController;->onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 637
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 14
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 715
    invoke-direct {p0}, Lcom/google/android/maps/MapView;->updateZoomControls()V

    .line 717
    invoke-virtual {p0}, Lcom/google/android/maps/MapView;->getChildCount()I

    move-result v4

    .line 718
    .local v4, "count":I
    new-instance v8, Landroid/graphics/Point;

    invoke-direct {v8}, Landroid/graphics/Point;-><init>()V

    .line 720
    .local v8, "point":Landroid/graphics/Point;
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    if-ge v6, v4, :cond_2

    .line 721
    invoke-virtual {p0, v6}, Lcom/google/android/maps/MapView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 722
    .local v1, "child":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v12

    const/16 v13, 0x8

    if-eq v12, v13, :cond_0

    .line 724
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Lcom/google/android/maps/MapView$LayoutParams;

    .line 726
    .local v7, "lp":Lcom/google/android/maps/MapView$LayoutParams;
    iget v12, v7, Lcom/google/android/maps/MapView$LayoutParams;->mode:I

    if-nez v12, :cond_1

    .line 727
    iget-object v12, p0, Lcom/google/android/maps/MapView;->mConverter:Lcom/google/android/maps/PixelConverter;

    iget-object v13, v7, Lcom/google/android/maps/MapView$LayoutParams;->point:Lcom/google/android/maps/GeoPoint;

    invoke-virtual {v12, v13, v8}, Lcom/google/android/maps/PixelConverter;->toPixels(Lcom/google/android/maps/GeoPoint;Landroid/graphics/Point;)Landroid/graphics/Point;

    .line 728
    iget v12, v8, Landroid/graphics/Point;->x:I

    iget v13, v7, Lcom/google/android/maps/MapView$LayoutParams;->x:I

    add-int/2addr v12, v13

    iput v12, v8, Landroid/graphics/Point;->x:I

    .line 729
    iget v12, v8, Landroid/graphics/Point;->y:I

    iget v13, v7, Lcom/google/android/maps/MapView$LayoutParams;->y:I

    add-int/2addr v12, v13

    iput v12, v8, Landroid/graphics/Point;->y:I

    .line 735
    :goto_1
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v9

    .line 736
    .local v9, "width":I
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    .line 737
    .local v5, "height":I
    iget v10, v8, Landroid/graphics/Point;->x:I

    .line 738
    .local v10, "x":I
    iget v11, v8, Landroid/graphics/Point;->y:I

    .line 739
    .local v11, "y":I
    iget v0, v7, Lcom/google/android/maps/MapView$LayoutParams;->alignment:I

    .line 740
    .local v0, "alignment":I
    and-int/lit8 v12, v0, 0x7

    packed-switch v12, :pswitch_data_0

    .line 752
    :goto_2
    :pswitch_0
    and-int/lit8 v12, v0, 0x70

    sparse-switch v12, :sswitch_data_0

    .line 764
    :goto_3
    :sswitch_0
    iget v12, p0, Lcom/google/android/maps/MapView;->mPaddingLeft:I

    add-int v2, v12, v10

    .line 765
    .local v2, "childLeft":I
    iget v12, p0, Lcom/google/android/maps/MapView;->mPaddingTop:I

    add-int v3, v12, v11

    .line 766
    .local v3, "childTop":I
    add-int v12, v2, v9

    add-int v13, v3, v5

    invoke-virtual {v1, v2, v3, v12, v13}, Landroid/view/View;->layout(IIII)V

    .line 720
    .end local v0    # "alignment":I
    .end local v2    # "childLeft":I
    .end local v3    # "childTop":I
    .end local v5    # "height":I
    .end local v7    # "lp":Lcom/google/android/maps/MapView$LayoutParams;
    .end local v9    # "width":I
    .end local v10    # "x":I
    .end local v11    # "y":I
    :cond_0
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 731
    .restart local v7    # "lp":Lcom/google/android/maps/MapView$LayoutParams;
    :cond_1
    iget v12, v7, Lcom/google/android/maps/MapView$LayoutParams;->x:I

    iput v12, v8, Landroid/graphics/Point;->x:I

    .line 732
    iget v12, v7, Lcom/google/android/maps/MapView$LayoutParams;->y:I

    iput v12, v8, Landroid/graphics/Point;->y:I

    goto :goto_1

    .line 744
    .restart local v0    # "alignment":I
    .restart local v5    # "height":I
    .restart local v9    # "width":I
    .restart local v10    # "x":I
    .restart local v11    # "y":I
    :pswitch_1
    div-int/lit8 v12, v9, 0x2

    sub-int/2addr v10, v12

    .line 745
    goto :goto_2

    .line 749
    :pswitch_2
    add-int/lit8 v12, v9, -0x1

    sub-int/2addr v10, v12

    goto :goto_2

    .line 756
    :sswitch_1
    div-int/lit8 v12, v5, 0x2

    sub-int/2addr v11, v12

    .line 757
    goto :goto_3

    .line 761
    :sswitch_2
    add-int/lit8 v12, v5, -0x1

    sub-int/2addr v11, v12

    goto :goto_3

    .line 769
    .end local v0    # "alignment":I
    .end local v1    # "child":Landroid/view/View;
    .end local v5    # "height":I
    .end local v7    # "lp":Lcom/google/android/maps/MapView$LayoutParams;
    .end local v9    # "width":I
    .end local v10    # "x":I
    .end local v11    # "y":I
    :cond_2
    iget-object v12, p0, Lcom/google/android/maps/MapView;->mController:Lcom/google/android/maps/MapController;

    invoke-virtual {v12}, Lcom/google/android/maps/MapController;->clean()V

    .line 770
    return-void

    .line 740
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch

    .line 752
    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_1
        0x30 -> :sswitch_0
        0x50 -> :sswitch_2
    .end sparse-switch
.end method

.method protected final onMeasure(II)V
    .locals 9
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 550
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    .line 553
    invoke-virtual {p0, p1, p2}, Lcom/google/android/maps/MapView;->measureChildren(II)V

    .line 561
    invoke-virtual {p0}, Lcom/google/android/maps/MapView;->getMeasuredWidth()I

    move-result v2

    .line 562
    .local v2, "maxWidth":I
    invoke-virtual {p0}, Lcom/google/android/maps/MapView;->getMeasuredHeight()I

    move-result v1

    .line 563
    .local v1, "maxHeight":I
    if-eqz v2, :cond_0

    if-nez v1, :cond_2

    .line 564
    :cond_0
    iget-object v4, p0, Lcom/google/android/maps/MapView;->mContext:Landroid/content/Context;

    const-string v5, "window"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/WindowManager;

    .line 565
    .local v3, "wm":Landroid/view/WindowManager;
    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 566
    .local v0, "display":Landroid/view/Display;
    if-nez v2, :cond_1

    .line 567
    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v2

    .line 569
    :cond_1
    if-nez v1, :cond_2

    .line 570
    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v1

    .line 574
    .end local v0    # "display":Landroid/view/Display;
    .end local v3    # "wm":Landroid/view/WindowManager;
    :cond_2
    invoke-static {v2, p1}, Lcom/google/android/maps/MapView;->resolveSize(II)I

    move-result v4

    invoke-static {v1, p2}, Lcom/google/android/maps/MapView;->resolveSize(II)I

    move-result v5

    invoke-virtual {p0, v4, v5}, Lcom/google/android/maps/MapView;->setMeasuredDimension(II)V

    .line 582
    iget-object v4, p0, Lcom/google/android/maps/MapView;->mGoogleLogo:Landroid/graphics/drawable/Drawable;

    const/16 v5, 0xa

    invoke-virtual {p0}, Lcom/google/android/maps/MapView;->getMeasuredHeight()I

    move-result v6

    iget v7, p0, Lcom/google/android/maps/MapView;->mGoogleLogoHeight:I

    sub-int/2addr v6, v7

    add-int/lit8 v6, v6, -0xa

    iget v7, p0, Lcom/google/android/maps/MapView;->mGoogleLogoWidth:I

    add-int/lit8 v7, v7, 0xa

    invoke-virtual {p0}, Lcom/google/android/maps/MapView;->getMeasuredHeight()I

    move-result v8

    add-int/lit8 v8, v8, -0xa

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 592
    iget-object v4, p0, Lcom/google/android/maps/MapView;->mMap:Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;

    invoke-virtual {p0}, Lcom/google/android/maps/MapView;->getMeasuredWidth()I

    move-result v5

    invoke-virtual {p0}, Lcom/google/android/maps/MapView;->getMeasuredHeight()I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;->resize(II)V

    .line 593
    iget-object v4, p0, Lcom/google/android/maps/MapView;->mController:Lcom/google/android/maps/MapController;

    invoke-virtual {v4}, Lcom/google/android/maps/MapController;->onMeasure()V

    .line 594
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "state"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x0

    .line 1353
    if-nez p1, :cond_1

    .line 1372
    :cond_0
    :goto_0
    return-void

    .line 1356
    :cond_1
    iget-object v6, p0, Lcom/google/android/maps/MapView;->mController:Lcom/google/android/maps/MapController;

    if-eqz v6, :cond_3

    .line 1357
    const v0, 0x7fffffff

    .line 1358
    .local v0, "invalid":I
    sget-object v6, Lcom/google/android/maps/MapView;->KEY_CENTER_LATITUDE:Ljava/lang/String;

    invoke-virtual {p1, v6, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 1359
    .local v1, "latitude":I
    sget-object v6, Lcom/google/android/maps/MapView;->KEY_CENTER_LONGITUDE:Ljava/lang/String;

    invoke-virtual {p1, v6, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 1360
    .local v2, "longitude":I
    if-eq v1, v0, :cond_2

    if-eq v2, v0, :cond_2

    .line 1361
    iget-object v6, p0, Lcom/google/android/maps/MapView;->mController:Lcom/google/android/maps/MapController;

    new-instance v7, Lcom/google/android/maps/GeoPoint;

    invoke-direct {v7, v1, v2}, Lcom/google/android/maps/GeoPoint;-><init>(II)V

    invoke-virtual {v6, v7}, Lcom/google/android/maps/MapController;->setCenter(Lcom/google/android/maps/GeoPoint;)V

    .line 1363
    :cond_2
    sget-object v6, Lcom/google/android/maps/MapView;->KEY_ZOOM_LEVEL:Ljava/lang/String;

    invoke-virtual {p1, v6, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v4

    .line 1364
    .local v4, "zoomLevel":I
    if-eq v4, v0, :cond_3

    .line 1365
    iget-object v6, p0, Lcom/google/android/maps/MapView;->mController:Lcom/google/android/maps/MapController;

    invoke-virtual {v6, v4}, Lcom/google/android/maps/MapController;->setZoom(I)I

    .line 1368
    .end local v0    # "invalid":I
    .end local v1    # "latitude":I
    .end local v2    # "longitude":I
    .end local v4    # "zoomLevel":I
    :cond_3
    sget-object v6, Lcom/google/android/maps/MapView;->KEY_ZOOM_DISPLAYED:Ljava/lang/String;

    invoke-virtual {p1, v6, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v6

    if-eqz v6, :cond_4

    const/4 v3, 0x1

    .line 1369
    .local v3, "zoomDisplayed":Z
    :goto_1
    if-eqz v3, :cond_0

    .line 1370
    invoke-virtual {p0, v5}, Lcom/google/android/maps/MapView;->displayZoomControls(Z)V

    goto :goto_0

    .end local v3    # "zoomDisplayed":Z
    :cond_4
    move v3, v5

    .line 1368
    goto :goto_1
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "state"    # Landroid/os/Bundle;

    .prologue
    .line 1334
    sget-object v0, Lcom/google/android/maps/MapView;->KEY_CENTER_LATITUDE:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/maps/MapView;->mMap:Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;

    invoke-virtual {v1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;->getCenterPoint()Landroid_maps_conflict_avoidance/com/google/map/MapPoint;

    move-result-object v1

    invoke-virtual {v1}, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->getLatitude()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1335
    sget-object v0, Lcom/google/android/maps/MapView;->KEY_CENTER_LONGITUDE:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/maps/MapView;->mMap:Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;

    invoke-virtual {v1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;->getCenterPoint()Landroid_maps_conflict_avoidance/com/google/map/MapPoint;

    move-result-object v1

    invoke-virtual {v1}, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->getLongitude()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1336
    sget-object v0, Lcom/google/android/maps/MapView;->KEY_ZOOM_LEVEL:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/maps/MapView;->getZoomLevel()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1338
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mZoomButtonsController:Landroid/widget/ZoomButtonsController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/maps/MapView;->mZoomButtonsController:Landroid/widget/ZoomButtonsController;

    invoke-virtual {v0}, Landroid/widget/ZoomButtonsController;->isVisible()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mZoomControls:Landroid/widget/ZoomControls;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/maps/MapView;->mZoomControls:Landroid/widget/ZoomControls;

    invoke-virtual {v0}, Landroid/widget/ZoomControls;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    .line 1340
    :cond_1
    sget-object v0, Lcom/google/android/maps/MapView;->KEY_ZOOM_DISPLAYED:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1344
    :goto_0
    return-void

    .line 1342
    :cond_2
    sget-object v0, Lcom/google/android/maps/MapView;->KEY_ZOOM_DISPLAYED:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method protected onSizeChanged(IIII)V
    .locals 6
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 463
    iget-object v4, p0, Lcom/google/android/maps/MapView;->mMap:Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;

    invoke-virtual {v4, p1, p2}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;->resize(II)V

    .line 464
    iget-object v4, p0, Lcom/google/android/maps/MapView;->mReticle:Landroid/graphics/drawable/Drawable;

    if-eqz v4, :cond_0

    .line 465
    div-int/lit8 v4, p1, 0x2

    iget-object v5, p0, Lcom/google/android/maps/MapView;->mReticle:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    sub-int v1, v4, v5

    .line 466
    .local v1, "left":I
    div-int/lit8 v4, p2, 0x2

    iget-object v5, p0, Lcom/google/android/maps/MapView;->mReticle:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    sub-int v3, v4, v5

    .line 467
    .local v3, "top":I
    iget-object v4, p0, Lcom/google/android/maps/MapView;->mReticle:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    add-int v2, v1, v4

    .line 468
    .local v2, "right":I
    iget-object v4, p0, Lcom/google/android/maps/MapView;->mReticle:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    add-int v0, v3, v4

    .line 469
    .local v0, "bottom":I
    iget-object v4, p0, Lcom/google/android/maps/MapView;->mReticle:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4, v1, v3, v2, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 471
    .end local v0    # "bottom":I
    .end local v1    # "left":I
    .end local v2    # "right":I
    .end local v3    # "top":I
    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v0, 0x1

    .line 675
    invoke-virtual {p0}, Lcom/google/android/maps/MapView;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/maps/MapView;->isClickable()Z

    move-result v1

    if-nez v1, :cond_2

    .line 676
    :cond_0
    const/4 v0, 0x0

    .line 686
    :cond_1
    :goto_0
    return v0

    .line 680
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/maps/MapView;->postInvalidate()V

    .line 681
    iget-object v1, p0, Lcom/google/android/maps/MapView;->mOverlayBundle:Lcom/google/android/maps/OverlayBundle;

    invoke-virtual {v1, p1, p0}, Lcom/google/android/maps/OverlayBundle;->onTouchEvent(Landroid/view/MotionEvent;Lcom/google/android/maps/MapView;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 684
    iget-object v1, p0, Lcom/google/android/maps/MapView;->mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v1, p1}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 685
    iget-object v1, p0, Lcom/google/android/maps/MapView;->mGestureDetector:Lcom/google/android/maps/GestureDetector;

    invoke-virtual {v1, p1}, Lcom/google/android/maps/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    goto :goto_0
.end method

.method public onTrackballEvent(Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/high16 v4, 0x41200000    # 10.0f

    .line 647
    invoke-virtual {p0}, Lcom/google/android/maps/MapView;->postInvalidate()V

    .line 648
    iget-object v1, p0, Lcom/google/android/maps/MapView;->mOverlayBundle:Lcom/google/android/maps/OverlayBundle;

    invoke-virtual {v1, p1, p0}, Lcom/google/android/maps/OverlayBundle;->onTrackballEvent(Landroid/view/MotionEvent;Lcom/google/android/maps/MapView;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 649
    const/4 v1, 0x1

    .line 665
    :goto_0
    return v1

    .line 651
    :cond_0
    iget-object v1, p0, Lcom/google/android/maps/MapView;->mTrackballGestureDetector:Lcom/google/android/maps/TrackballGestureDetector;

    invoke-virtual {v1, p1}, Lcom/google/android/maps/TrackballGestureDetector;->analyze(Landroid/view/MotionEvent;)V

    .line 653
    iget-object v1, p0, Lcom/google/android/maps/MapView;->mTrackballGestureDetector:Lcom/google/android/maps/TrackballGestureDetector;

    invoke-virtual {v1}, Lcom/google/android/maps/TrackballGestureDetector;->isScroll()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 654
    const/high16 v0, 0x41200000    # 10.0f

    .line 656
    .local v0, "TRACKBALL_SCALE":F
    iget-object v1, p0, Lcom/google/android/maps/MapView;->mController:Lcom/google/android/maps/MapController;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    mul-float/2addr v2, v4

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    mul-float/2addr v3, v4

    float-to-int v3, v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/maps/MapController;->scrollByTrackball(II)V

    .line 665
    .end local v0    # "TRACKBALL_SCALE":F
    :cond_1
    :goto_1
    const/4 v1, 0x0

    goto :goto_0

    .line 658
    :cond_2
    iget-object v1, p0, Lcom/google/android/maps/MapView;->mTrackballGestureDetector:Lcom/google/android/maps/TrackballGestureDetector;

    invoke-virtual {v1}, Lcom/google/android/maps/TrackballGestureDetector;->isTap()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 659
    iget-object v1, p0, Lcom/google/android/maps/MapView;->mOverlayBundle:Lcom/google/android/maps/OverlayBundle;

    new-instance v2, Lcom/google/android/maps/GeoPoint;

    iget-object v3, p0, Lcom/google/android/maps/MapView;->mMap:Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;

    invoke-virtual {v3}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;->getCenterPoint()Landroid_maps_conflict_avoidance/com/google/map/MapPoint;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/maps/GeoPoint;-><init>(Landroid_maps_conflict_avoidance/com/google/map/MapPoint;)V

    invoke-virtual {v1, v2, p0}, Lcom/google/android/maps/OverlayBundle;->onTap(Lcom/google/android/maps/GeoPoint;Lcom/google/android/maps/MapView;)Z

    goto :goto_1
.end method

.method protected onVisibilityChanged(Landroid/view/View;I)V
    .locals 2
    .param p1, "changedView"    # Landroid/view/View;
    .param p2, "visibility"    # I

    .prologue
    .line 691
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onVisibilityChanged(Landroid/view/View;I)V

    .line 692
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mZoomButtonsController:Landroid/widget/ZoomButtonsController;

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    .line 693
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mZoomButtonsController:Landroid/widget/ZoomButtonsController;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ZoomButtonsController;->setVisible(Z)V

    .line 695
    :cond_0
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1
    .param p1, "hasFocus"    # Z

    .prologue
    .line 602
    if-nez p1, :cond_0

    .line 603
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mController:Lcom/google/android/maps/MapController;

    invoke-virtual {v0}, Lcom/google/android/maps/MapController;->stopPanning()V

    .line 605
    :cond_0
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onWindowFocusChanged(Z)V

    .line 606
    return-void
.end method

.method public preLoad()V
    .locals 2

    .prologue
    .line 1112
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mMap:Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;

    iget-object v1, p0, Lcom/google/android/maps/MapView;->mMap:Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;

    invoke-virtual {v1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;->getCenterPoint()Landroid_maps_conflict_avoidance/com/google/map/MapPoint;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;->preLoad(Landroid_maps_conflict_avoidance/com/google/map/MapPoint;)V

    .line 1113
    return-void
.end method

.method restoreMapReferences(Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;)V
    .locals 2
    .param p1, "dispatcher"    # Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;

    .prologue
    .line 403
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mRepainter:Lcom/google/android/maps/MapView$Repainter;

    invoke-virtual {p1, v0}, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->addDataRequestListener(Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestListener;)V

    .line 404
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mMap:Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;

    iget-object v1, p0, Lcom/google/android/maps/MapView;->mOverlayRenderer:Landroid_maps_conflict_avoidance/com/google/googlenav/ui/android/AndroidTileOverlayRenderer;

    invoke-virtual {v0, v1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;->setTileOverlayRenderer(Landroid_maps_conflict_avoidance/com/google/googlenav/map/TileOverlayRenderer;)V

    .line 405
    return-void
.end method

.method public setBuiltInZoomControls(Z)V
    .locals 1
    .param p1, "on"    # Z

    .prologue
    .line 1498
    iput-boolean p1, p0, Lcom/google/android/maps/MapView;->mBuiltInZoomControlsEnabled:Z

    .line 1499
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mZoomButtonsController:Landroid/widget/ZoomButtonsController;

    if-nez v0, :cond_0

    .line 1500
    invoke-direct {p0}, Lcom/google/android/maps/MapView;->createZoomButtonsController()Landroid/widget/ZoomButtonsController;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/MapView;->mZoomButtonsController:Landroid/widget/ZoomButtonsController;

    .line 1502
    :cond_0
    return-void
.end method

.method public setReticleDrawMode(Lcom/google/android/maps/MapView$ReticleDrawMode;)V
    .locals 2
    .param p1, "mode"    # Lcom/google/android/maps/MapView$ReticleDrawMode;

    .prologue
    .line 1310
    if-nez p1, :cond_0

    .line 1311
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "The ReticleDrawMode cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1313
    :cond_0
    iput-object p1, p0, Lcom/google/android/maps/MapView;->mReticleDrawMode:Lcom/google/android/maps/MapView$ReticleDrawMode;

    .line 1314
    return-void
.end method

.method public setSatellite(Z)V
    .locals 2
    .param p1, "on"    # Z

    .prologue
    .line 1142
    invoke-virtual {p0}, Lcom/google/android/maps/MapView;->isSatellite()Z

    move-result v0

    if-ne v0, p1, :cond_0

    .line 1152
    :goto_0
    return-void

    .line 1145
    :cond_0
    if-eqz p1, :cond_1

    .line 1146
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mMap:Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;->setMapMode(I)V

    .line 1150
    :goto_1
    invoke-direct {p0}, Lcom/google/android/maps/MapView;->updateZoomControls()V

    .line 1151
    invoke-virtual {p0}, Lcom/google/android/maps/MapView;->postInvalidate()V

    goto :goto_0

    .line 1148
    :cond_1
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mMap:Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;->setMapMode(I)V

    goto :goto_1
.end method

.method public setStreetView(Z)V
    .locals 1
    .param p1, "on"    # Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1204
    if-eqz p1, :cond_0

    .line 1205
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/maps/MapView;->setTraffic(Z)V

    .line 1207
    :cond_0
    iput-boolean p1, p0, Lcom/google/android/maps/MapView;->mFakeStreetViewEnabled:Z

    .line 1208
    return-void
.end method

.method public setTraffic(Z)V
    .locals 1
    .param p1, "on"    # Z

    .prologue
    .line 1174
    if-eqz p1, :cond_0

    .line 1175
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/maps/MapView;->setStreetView(Z)V

    .line 1177
    :cond_0
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mOverlayRenderer:Landroid_maps_conflict_avoidance/com/google/googlenav/ui/android/AndroidTileOverlayRenderer;

    invoke-virtual {v0, p1}, Landroid_maps_conflict_avoidance/com/google/googlenav/ui/android/AndroidTileOverlayRenderer;->setShowTraffic(Z)V

    .line 1178
    invoke-virtual {p0}, Lcom/google/android/maps/MapView;->postInvalidate()V

    .line 1179
    return-void
.end method

.method setup(Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;Landroid_maps_conflict_avoidance/com/google/googlenav/map/TrafficService;Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;)V
    .locals 3
    .param p1, "map"    # Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;
    .param p2, "traffic"    # Landroid_maps_conflict_avoidance/com/google/googlenav/map/TrafficService;
    .param p3, "dispatcher"    # Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;

    .prologue
    .line 310
    iput-object p1, p0, Lcom/google/android/maps/MapView;->mMap:Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;

    .line 311
    new-instance v0, Lcom/google/android/maps/PixelConverter;

    invoke-direct {v0, p1}, Lcom/google/android/maps/PixelConverter;-><init>(Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;)V

    iput-object v0, p0, Lcom/google/android/maps/MapView;->mConverter:Lcom/google/android/maps/PixelConverter;

    .line 313
    new-instance v0, Lcom/google/android/maps/OverlayBundle;

    invoke-direct {v0}, Lcom/google/android/maps/OverlayBundle;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/MapView;->mOverlayBundle:Lcom/google/android/maps/OverlayBundle;

    .line 314
    new-instance v0, Lcom/google/android/maps/MapController;

    iget-object v1, p0, Lcom/google/android/maps/MapView;->mMap:Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;

    invoke-direct {v0, v1, p0}, Lcom/google/android/maps/MapController;-><init>(Landroid_maps_conflict_avoidance/com/google/googlenav/map/Map;Lcom/google/android/maps/MapView;)V

    iput-object v0, p0, Lcom/google/android/maps/MapView;->mController:Lcom/google/android/maps/MapController;

    .line 317
    new-instance v0, Lcom/google/android/maps/ZoomHelper;

    iget-object v1, p0, Lcom/google/android/maps/MapView;->mController:Lcom/google/android/maps/MapController;

    invoke-direct {v0, p0, v1}, Lcom/google/android/maps/ZoomHelper;-><init>(Lcom/google/android/maps/MapView;Lcom/google/android/maps/MapController;)V

    iput-object v0, p0, Lcom/google/android/maps/MapView;->mZoomHelper:Lcom/google/android/maps/ZoomHelper;

    .line 319
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/maps/InternalR$drawable;->reticle:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/maps/MapView;->mReticle:Landroid/graphics/drawable/Drawable;

    .line 322
    sget-object v0, Lcom/google/android/maps/MapView$ReticleDrawMode;->DRAW_RETICLE_OVER:Lcom/google/android/maps/MapView$ReticleDrawMode;

    iput-object v0, p0, Lcom/google/android/maps/MapView;->mReticleDrawMode:Lcom/google/android/maps/MapView$ReticleDrawMode;

    .line 324
    new-instance v0, Landroid_maps_conflict_avoidance/com/google/googlenav/ui/android/AndroidTileOverlayRenderer;

    invoke-direct {v0}, Landroid_maps_conflict_avoidance/com/google/googlenav/ui/android/AndroidTileOverlayRenderer;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/MapView;->mOverlayRenderer:Landroid_maps_conflict_avoidance/com/google/googlenav/ui/android/AndroidTileOverlayRenderer;

    .line 325
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mOverlayRenderer:Landroid_maps_conflict_avoidance/com/google/googlenav/ui/android/AndroidTileOverlayRenderer;

    invoke-virtual {v0, p2}, Landroid_maps_conflict_avoidance/com/google/googlenav/ui/android/AndroidTileOverlayRenderer;->setTrafficService(Landroid_maps_conflict_avoidance/com/google/googlenav/map/TrafficService;)V

    .line 327
    invoke-virtual {p0, p3}, Lcom/google/android/maps/MapView;->restoreMapReferences(Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;)V

    .line 329
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/maps/MapView;->mHandler:Landroid/os/Handler;

    .line 330
    new-instance v0, Lcom/google/android/maps/GestureDetector;

    invoke-virtual {p0}, Lcom/google/android/maps/MapView;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/google/android/maps/MapView$1;

    invoke-direct {v2, p0}, Lcom/google/android/maps/MapView$1;-><init>(Lcom/google/android/maps/MapView;)V

    invoke-direct {v0, v1, v2}, Lcom/google/android/maps/GestureDetector;-><init>(Landroid/content/Context;Lcom/google/android/maps/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/google/android/maps/MapView;->mGestureDetector:Lcom/google/android/maps/GestureDetector;

    .line 378
    iget-object v0, p0, Lcom/google/android/maps/MapView;->mGestureDetector:Lcom/google/android/maps/GestureDetector;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/maps/GestureDetector;->setIsLongpressEnabled(Z)V

    .line 379
    new-instance v0, Landroid/view/ScaleGestureDetector;

    invoke-virtual {p0}, Lcom/google/android/maps/MapView;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/google/android/maps/MapView$2;

    invoke-direct {v2, p0}, Lcom/google/android/maps/MapView$2;-><init>(Lcom/google/android/maps/MapView;)V

    invoke-direct {v0, v1, v2}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object v0, p0, Lcom/google/android/maps/MapView;->mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

    .line 396
    new-instance v0, Lcom/google/android/maps/TrackballGestureDetector;

    iget-object v1, p0, Lcom/google/android/maps/MapView;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, v1}, Lcom/google/android/maps/TrackballGestureDetector;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/maps/MapView;->mTrackballGestureDetector:Lcom/google/android/maps/TrackballGestureDetector;

    .line 397
    return-void
.end method
