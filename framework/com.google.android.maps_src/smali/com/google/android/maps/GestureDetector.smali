.class Lcom/google/android/maps/GestureDetector;
.super Ljava/lang/Object;
.source "GestureDetector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/maps/GestureDetector$GestureHandler;,
        Lcom/google/android/maps/GestureDetector$SimpleOnGestureListener;,
        Lcom/google/android/maps/GestureDetector$OnDoubleTapListener;,
        Lcom/google/android/maps/GestureDetector$OnGestureListener;
    }
.end annotation


# static fields
.field private static final DOUBLE_TAP_TIMEOUT:I

.field private static final LONGPRESS_TIMEOUT:I

.field private static final TAP_TIMEOUT:I


# instance fields
.field private mAlwaysInBiggerTapRegion:Z

.field private mAlwaysInTapRegion:Z

.field private mCurrentDownEvent:Landroid/view/MotionEvent;

.field private mDoubleTapListener:Lcom/google/android/maps/GestureDetector$OnDoubleTapListener;

.field private mDoubleTapSlopSquare:I

.field private mDoubleTapTouchSlopSquare:I

.field private mDownFocusX:F

.field private mDownFocusY:F

.field private final mHandler:Landroid/os/Handler;

.field private mInLongPress:Z

.field private mIsDoubleTapping:Z

.field private mIsLongpressEnabled:Z

.field private mLastFocusX:F

.field private mLastFocusY:F

.field private final mListener:Lcom/google/android/maps/GestureDetector$OnGestureListener;

.field private mMaximumFlingVelocity:I

.field private mMinimumFlingVelocity:I

.field private mPreviousUpEvent:Landroid/view/MotionEvent;

.field private mStillDown:Z

.field private mTouchSlopSquare:I

.field private mVelocityTracker:Landroid/view/VelocityTracker;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 207
    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result v0

    sput v0, Lcom/google/android/maps/GestureDetector;->LONGPRESS_TIMEOUT:I

    .line 208
    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    move-result v0

    sput v0, Lcom/google/android/maps/GestureDetector;->TAP_TIMEOUT:I

    .line 209
    invoke-static {}, Landroid/view/ViewConfiguration;->getDoubleTapTimeout()I

    move-result v0

    sput v0, Lcom/google/android/maps/GestureDetector;->DOUBLE_TAP_TIMEOUT:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/maps/GestureDetector$OnGestureListener;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/google/android/maps/GestureDetector$OnGestureListener;

    .prologue
    .line 329
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/maps/GestureDetector;-><init>(Landroid/content/Context;Lcom/google/android/maps/GestureDetector$OnGestureListener;Landroid/os/Handler;)V

    .line 330
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/maps/GestureDetector$OnGestureListener;Landroid/os/Handler;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/google/android/maps/GestureDetector$OnGestureListener;
    .param p3, "handler"    # Landroid/os/Handler;

    .prologue
    .line 344
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 345
    if-eqz p3, :cond_1

    .line 346
    new-instance v0, Lcom/google/android/maps/GestureDetector$GestureHandler;

    invoke-direct {v0, p0, p3}, Lcom/google/android/maps/GestureDetector$GestureHandler;-><init>(Lcom/google/android/maps/GestureDetector;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/maps/GestureDetector;->mHandler:Landroid/os/Handler;

    .line 350
    :goto_0
    iput-object p2, p0, Lcom/google/android/maps/GestureDetector;->mListener:Lcom/google/android/maps/GestureDetector$OnGestureListener;

    .line 351
    instance-of v0, p2, Lcom/google/android/maps/GestureDetector$OnDoubleTapListener;

    if-eqz v0, :cond_0

    .line 352
    check-cast p2, Lcom/google/android/maps/GestureDetector$OnDoubleTapListener;

    .end local p2    # "listener":Lcom/google/android/maps/GestureDetector$OnGestureListener;
    invoke-virtual {p0, p2}, Lcom/google/android/maps/GestureDetector;->setOnDoubleTapListener(Lcom/google/android/maps/GestureDetector$OnDoubleTapListener;)V

    .line 354
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/maps/GestureDetector;->init(Landroid/content/Context;)V

    .line 355
    return-void

    .line 348
    .restart local p2    # "listener":Lcom/google/android/maps/GestureDetector$OnGestureListener;
    :cond_1
    new-instance v0, Lcom/google/android/maps/GestureDetector$GestureHandler;

    invoke-direct {v0, p0}, Lcom/google/android/maps/GestureDetector$GestureHandler;-><init>(Lcom/google/android/maps/GestureDetector;)V

    iput-object v0, p0, Lcom/google/android/maps/GestureDetector;->mHandler:Landroid/os/Handler;

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/google/android/maps/GestureDetector;)Landroid/view/MotionEvent;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/maps/GestureDetector;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/maps/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/maps/GestureDetector;)Lcom/google/android/maps/GestureDetector$OnGestureListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/maps/GestureDetector;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/maps/GestureDetector;->mListener:Lcom/google/android/maps/GestureDetector$OnGestureListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/maps/GestureDetector;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/maps/GestureDetector;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/google/android/maps/GestureDetector;->dispatchLongPress()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/maps/GestureDetector;)Lcom/google/android/maps/GestureDetector$OnDoubleTapListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/maps/GestureDetector;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/maps/GestureDetector;->mDoubleTapListener:Lcom/google/android/maps/GestureDetector$OnDoubleTapListener;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/maps/GestureDetector;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/maps/GestureDetector;

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/google/android/maps/GestureDetector;->mStillDown:Z

    return v0
.end method

.method private cancel()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 600
    iget-object v0, p0, Lcom/google/android/maps/GestureDetector;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 601
    iget-object v0, p0, Lcom/google/android/maps/GestureDetector;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 602
    iget-object v0, p0, Lcom/google/android/maps/GestureDetector;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 603
    iget-object v0, p0, Lcom/google/android/maps/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 604
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/maps/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 605
    iput-boolean v2, p0, Lcom/google/android/maps/GestureDetector;->mIsDoubleTapping:Z

    .line 606
    iput-boolean v2, p0, Lcom/google/android/maps/GestureDetector;->mStillDown:Z

    .line 607
    iput-boolean v2, p0, Lcom/google/android/maps/GestureDetector;->mAlwaysInTapRegion:Z

    .line 608
    iput-boolean v2, p0, Lcom/google/android/maps/GestureDetector;->mAlwaysInBiggerTapRegion:Z

    .line 609
    iget-boolean v0, p0, Lcom/google/android/maps/GestureDetector;->mInLongPress:Z

    if-eqz v0, :cond_0

    .line 610
    iput-boolean v2, p0, Lcom/google/android/maps/GestureDetector;->mInLongPress:Z

    .line 612
    :cond_0
    return-void
.end method

.method private cancelTaps()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 615
    iget-object v0, p0, Lcom/google/android/maps/GestureDetector;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 616
    iget-object v0, p0, Lcom/google/android/maps/GestureDetector;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 617
    iget-object v0, p0, Lcom/google/android/maps/GestureDetector;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 618
    iput-boolean v2, p0, Lcom/google/android/maps/GestureDetector;->mIsDoubleTapping:Z

    .line 619
    iput-boolean v2, p0, Lcom/google/android/maps/GestureDetector;->mAlwaysInTapRegion:Z

    .line 620
    iput-boolean v2, p0, Lcom/google/android/maps/GestureDetector;->mAlwaysInBiggerTapRegion:Z

    .line 621
    iget-boolean v0, p0, Lcom/google/android/maps/GestureDetector;->mInLongPress:Z

    if-eqz v0, :cond_0

    .line 622
    iput-boolean v2, p0, Lcom/google/android/maps/GestureDetector;->mInLongPress:Z

    .line 624
    :cond_0
    return-void
.end method

.method private dispatchLongPress()V
    .locals 2

    .prologue
    .line 642
    iget-object v0, p0, Lcom/google/android/maps/GestureDetector;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 643
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/maps/GestureDetector;->mInLongPress:Z

    .line 644
    iget-object v0, p0, Lcom/google/android/maps/GestureDetector;->mListener:Lcom/google/android/maps/GestureDetector$OnGestureListener;

    iget-object v1, p0, Lcom/google/android/maps/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    invoke-interface {v0, v1}, Lcom/google/android/maps/GestureDetector$OnGestureListener;->onLongPress(Landroid/view/MotionEvent;)V

    .line 645
    return-void
.end method

.method private init(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 375
    iget-object v4, p0, Lcom/google/android/maps/GestureDetector;->mListener:Lcom/google/android/maps/GestureDetector$OnGestureListener;

    if-nez v4, :cond_0

    .line 376
    new-instance v4, Ljava/lang/NullPointerException;

    const-string v5, "OnGestureListener must not be null"

    invoke-direct {v4, v5}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 378
    :cond_0
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/google/android/maps/GestureDetector;->mIsLongpressEnabled:Z

    .line 382
    if-nez p1, :cond_1

    .line 384
    invoke-static {}, Landroid/view/ViewConfiguration;->getTouchSlop()I

    move-result v3

    .line 385
    .local v3, "touchSlop":I
    move v2, v3

    .line 386
    .local v2, "doubleTapTouchSlop":I
    invoke-static {}, Landroid/view/ViewConfiguration;->getDoubleTapSlop()I

    move-result v1

    .line 388
    .local v1, "doubleTapSlop":I
    invoke-static {}, Landroid/view/ViewConfiguration;->getMinimumFlingVelocity()I

    move-result v4

    iput v4, p0, Lcom/google/android/maps/GestureDetector;->mMinimumFlingVelocity:I

    .line 389
    invoke-static {}, Landroid/view/ViewConfiguration;->getMaximumFlingVelocity()I

    move-result v4

    iput v4, p0, Lcom/google/android/maps/GestureDetector;->mMaximumFlingVelocity:I

    .line 398
    :goto_0
    mul-int v4, v3, v3

    iput v4, p0, Lcom/google/android/maps/GestureDetector;->mTouchSlopSquare:I

    .line 399
    mul-int v4, v2, v2

    iput v4, p0, Lcom/google/android/maps/GestureDetector;->mDoubleTapTouchSlopSquare:I

    .line 400
    mul-int v4, v1, v1

    iput v4, p0, Lcom/google/android/maps/GestureDetector;->mDoubleTapSlopSquare:I

    .line 401
    return-void

    .line 391
    .end local v1    # "doubleTapSlop":I
    .end local v2    # "doubleTapTouchSlop":I
    .end local v3    # "touchSlop":I
    :cond_1
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 392
    .local v0, "configuration":Landroid/view/ViewConfiguration;
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v3

    .line 393
    .restart local v3    # "touchSlop":I
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledDoubleTapTouchSlop()I

    move-result v2

    .line 394
    .restart local v2    # "doubleTapTouchSlop":I
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledDoubleTapSlop()I

    move-result v1

    .line 395
    .restart local v1    # "doubleTapSlop":I
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v4

    iput v4, p0, Lcom/google/android/maps/GestureDetector;->mMinimumFlingVelocity:I

    .line 396
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v4

    iput v4, p0, Lcom/google/android/maps/GestureDetector;->mMaximumFlingVelocity:I

    goto :goto_0
.end method

.method private isConsideredDoubleTap(Landroid/view/MotionEvent;Landroid/view/MotionEvent;Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1, "firstDown"    # Landroid/view/MotionEvent;
    .param p2, "firstUp"    # Landroid/view/MotionEvent;
    .param p3, "secondDown"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x0

    .line 628
    iget-boolean v3, p0, Lcom/google/android/maps/GestureDetector;->mAlwaysInBiggerTapRegion:Z

    if-nez v3, :cond_1

    .line 638
    :cond_0
    :goto_0
    return v2

    .line 632
    :cond_1
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v4

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v6

    sub-long/2addr v4, v6

    sget v3, Lcom/google/android/maps/GestureDetector;->DOUBLE_TAP_TIMEOUT:I

    int-to-long v6, v3

    cmp-long v3, v4, v6

    if-gtz v3, :cond_0

    .line 636
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p3}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    float-to-int v4, v4

    sub-int v0, v3, v4

    .line 637
    .local v0, "deltaX":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p3}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v4, v4

    sub-int v1, v3, v4

    .line 638
    .local v1, "deltaY":I
    mul-int v3, v0, v0

    mul-int v4, v1, v1

    add-int/2addr v3, v4

    iget v4, p0, Lcom/google/android/maps/GestureDetector;->mDoubleTapSlopSquare:I

    if-ge v3, v4, :cond_0

    const/4 v2, 0x1

    goto :goto_0
.end method


# virtual methods
.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 34
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 443
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v6

    .line 445
    .local v6, "action":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v27, v0

    if-nez v27, :cond_0

    .line 446
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v27

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/maps/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 448
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 450
    and-int/lit16 v0, v6, 0xff

    move/from16 v27, v0

    const/16 v28, 0x6

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_1

    const/16 v18, 0x1

    .line 452
    .local v18, "pointerUp":Z
    :goto_0
    if-eqz v18, :cond_2

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v21

    .line 455
    .local v21, "skipIndex":I
    :goto_1
    const/16 v22, 0x0

    .local v22, "sumX":F
    const/16 v23, 0x0

    .line 456
    .local v23, "sumY":F
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v7

    .line 457
    .local v7, "count":I
    const/16 v17, 0x0

    .local v17, "i":I
    :goto_2
    move/from16 v0, v17

    if-ge v0, v7, :cond_4

    .line 458
    move/from16 v0, v21

    move/from16 v1, v17

    if-ne v0, v1, :cond_3

    .line 457
    :goto_3
    add-int/lit8 v17, v17, 0x1

    goto :goto_2

    .line 450
    .end local v7    # "count":I
    .end local v17    # "i":I
    .end local v18    # "pointerUp":Z
    .end local v21    # "skipIndex":I
    .end local v22    # "sumX":F
    .end local v23    # "sumY":F
    :cond_1
    const/16 v18, 0x0

    goto :goto_0

    .line 452
    .restart local v18    # "pointerUp":Z
    :cond_2
    const/16 v21, -0x1

    goto :goto_1

    .line 459
    .restart local v7    # "count":I
    .restart local v17    # "i":I
    .restart local v21    # "skipIndex":I
    .restart local v22    # "sumX":F
    .restart local v23    # "sumY":F
    :cond_3
    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v27

    add-float v22, v22, v27

    .line 460
    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v27

    add-float v23, v23, v27

    goto :goto_3

    .line 462
    :cond_4
    if-eqz v18, :cond_6

    add-int/lit8 v12, v7, -0x1

    .line 463
    .local v12, "div":I
    :goto_4
    int-to-float v0, v12

    move/from16 v27, v0

    div-float v13, v22, v27

    .line 464
    .local v13, "focusX":F
    int-to-float v0, v12

    move/from16 v27, v0

    div-float v14, v23, v27

    .line 466
    .local v14, "focusY":F
    const/16 v16, 0x0

    .line 468
    .local v16, "handled":Z
    and-int/lit16 v0, v6, 0xff

    move/from16 v27, v0

    packed-switch v27, :pswitch_data_0

    .line 596
    :cond_5
    :goto_5
    :pswitch_0
    return v16

    .end local v12    # "div":I
    .end local v13    # "focusX":F
    .end local v14    # "focusY":F
    .end local v16    # "handled":Z
    :cond_6
    move v12, v7

    .line 462
    goto :goto_4

    .line 470
    .restart local v12    # "div":I
    .restart local v13    # "focusX":F
    .restart local v14    # "focusY":F
    .restart local v16    # "handled":Z
    :pswitch_1
    move-object/from16 v0, p0

    iput v13, v0, Lcom/google/android/maps/GestureDetector;->mLastFocusX:F

    move-object/from16 v0, p0

    iput v13, v0, Lcom/google/android/maps/GestureDetector;->mDownFocusX:F

    .line 471
    move-object/from16 v0, p0

    iput v14, v0, Lcom/google/android/maps/GestureDetector;->mLastFocusY:F

    move-object/from16 v0, p0

    iput v14, v0, Lcom/google/android/maps/GestureDetector;->mDownFocusY:F

    .line 473
    invoke-direct/range {p0 .. p0}, Lcom/google/android/maps/GestureDetector;->cancelTaps()V

    goto :goto_5

    .line 477
    :pswitch_2
    move-object/from16 v0, p0

    iput v13, v0, Lcom/google/android/maps/GestureDetector;->mLastFocusX:F

    move-object/from16 v0, p0

    iput v13, v0, Lcom/google/android/maps/GestureDetector;->mDownFocusX:F

    .line 478
    move-object/from16 v0, p0

    iput v14, v0, Lcom/google/android/maps/GestureDetector;->mLastFocusY:F

    move-object/from16 v0, p0

    iput v14, v0, Lcom/google/android/maps/GestureDetector;->mDownFocusY:F

    .line 479
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/view/VelocityTracker;->clear()V

    goto :goto_5

    .line 483
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mDoubleTapListener:Lcom/google/android/maps/GestureDetector$OnDoubleTapListener;

    move-object/from16 v27, v0

    if-eqz v27, :cond_8

    .line 484
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mHandler:Landroid/os/Handler;

    move-object/from16 v27, v0

    const/16 v28, 0x3

    invoke-virtual/range {v27 .. v28}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v15

    .line 485
    .local v15, "hadTapMessage":Z
    if-eqz v15, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mHandler:Landroid/os/Handler;

    move-object/from16 v27, v0

    const/16 v28, 0x3

    invoke-virtual/range {v27 .. v28}, Landroid/os/Handler;->removeMessages(I)V

    .line 486
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    move-object/from16 v27, v0

    if-eqz v27, :cond_b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mPreviousUpEvent:Landroid/view/MotionEvent;

    move-object/from16 v27, v0

    if-eqz v27, :cond_b

    if-eqz v15, :cond_b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mPreviousUpEvent:Landroid/view/MotionEvent;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    move-object/from16 v2, v28

    move-object/from16 v3, p1

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/maps/GestureDetector;->isConsideredDoubleTap(Landroid/view/MotionEvent;Landroid/view/MotionEvent;Landroid/view/MotionEvent;)Z

    move-result v27

    if-eqz v27, :cond_b

    .line 489
    const/16 v27, 0x1

    move/from16 v0, v27

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/maps/GestureDetector;->mIsDoubleTapping:Z

    .line 491
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mDoubleTapListener:Lcom/google/android/maps/GestureDetector$OnDoubleTapListener;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    move-object/from16 v28, v0

    invoke-interface/range {v27 .. v28}, Lcom/google/android/maps/GestureDetector$OnDoubleTapListener;->onDoubleTap(Landroid/view/MotionEvent;)Z

    move-result v27

    or-int v16, v16, v27

    .line 493
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mDoubleTapListener:Lcom/google/android/maps/GestureDetector$OnDoubleTapListener;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    move-object/from16 v1, p1

    invoke-interface {v0, v1}, Lcom/google/android/maps/GestureDetector$OnDoubleTapListener;->onDoubleTapEvent(Landroid/view/MotionEvent;)Z

    move-result v27

    or-int v16, v16, v27

    .line 500
    .end local v15    # "hadTapMessage":Z
    :cond_8
    :goto_6
    move-object/from16 v0, p0

    iput v13, v0, Lcom/google/android/maps/GestureDetector;->mLastFocusX:F

    move-object/from16 v0, p0

    iput v13, v0, Lcom/google/android/maps/GestureDetector;->mDownFocusX:F

    .line 501
    move-object/from16 v0, p0

    iput v14, v0, Lcom/google/android/maps/GestureDetector;->mLastFocusY:F

    move-object/from16 v0, p0

    iput v14, v0, Lcom/google/android/maps/GestureDetector;->mDownFocusY:F

    .line 502
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    move-object/from16 v27, v0

    if-eqz v27, :cond_9

    .line 503
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/view/MotionEvent;->recycle()V

    .line 505
    :cond_9
    invoke-static/range {p1 .. p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v27

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/maps/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    .line 506
    const/16 v27, 0x1

    move/from16 v0, v27

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/maps/GestureDetector;->mAlwaysInTapRegion:Z

    .line 507
    const/16 v27, 0x1

    move/from16 v0, v27

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/maps/GestureDetector;->mAlwaysInBiggerTapRegion:Z

    .line 508
    const/16 v27, 0x1

    move/from16 v0, v27

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/maps/GestureDetector;->mStillDown:Z

    .line 509
    const/16 v27, 0x0

    move/from16 v0, v27

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/maps/GestureDetector;->mInLongPress:Z

    .line 511
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/maps/GestureDetector;->mIsLongpressEnabled:Z

    move/from16 v27, v0

    if-eqz v27, :cond_a

    .line 512
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mHandler:Landroid/os/Handler;

    move-object/from16 v27, v0

    const/16 v28, 0x2

    invoke-virtual/range {v27 .. v28}, Landroid/os/Handler;->removeMessages(I)V

    .line 513
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mHandler:Landroid/os/Handler;

    move-object/from16 v27, v0

    const/16 v28, 0x2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    move-object/from16 v29, v0

    invoke-virtual/range {v29 .. v29}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v30

    sget v29, Lcom/google/android/maps/GestureDetector;->TAP_TIMEOUT:I

    move/from16 v0, v29

    int-to-long v0, v0

    move-wide/from16 v32, v0

    add-long v30, v30, v32

    sget v29, Lcom/google/android/maps/GestureDetector;->LONGPRESS_TIMEOUT:I

    move/from16 v0, v29

    int-to-long v0, v0

    move-wide/from16 v32, v0

    add-long v30, v30, v32

    move-object/from16 v0, v27

    move/from16 v1, v28

    move-wide/from16 v2, v30

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageAtTime(IJ)Z

    .line 516
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mHandler:Landroid/os/Handler;

    move-object/from16 v27, v0

    const/16 v28, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    move-object/from16 v29, v0

    invoke-virtual/range {v29 .. v29}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v30

    sget v29, Lcom/google/android/maps/GestureDetector;->TAP_TIMEOUT:I

    move/from16 v0, v29

    int-to-long v0, v0

    move-wide/from16 v32, v0

    add-long v30, v30, v32

    move-object/from16 v0, v27

    move/from16 v1, v28

    move-wide/from16 v2, v30

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageAtTime(IJ)Z

    .line 517
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mListener:Lcom/google/android/maps/GestureDetector$OnGestureListener;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    move-object/from16 v1, p1

    invoke-interface {v0, v1}, Lcom/google/android/maps/GestureDetector$OnGestureListener;->onDown(Landroid/view/MotionEvent;)Z

    move-result v27

    or-int v16, v16, v27

    .line 518
    goto/16 :goto_5

    .line 496
    .restart local v15    # "hadTapMessage":Z
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mHandler:Landroid/os/Handler;

    move-object/from16 v27, v0

    const/16 v28, 0x3

    sget v29, Lcom/google/android/maps/GestureDetector;->DOUBLE_TAP_TIMEOUT:I

    move/from16 v0, v29

    int-to-long v0, v0

    move-wide/from16 v30, v0

    move-object/from16 v0, v27

    move/from16 v1, v28

    move-wide/from16 v2, v30

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_6

    .line 521
    .end local v15    # "hadTapMessage":Z
    :pswitch_4
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/maps/GestureDetector;->mInLongPress:Z

    move/from16 v27, v0

    if-nez v27, :cond_5

    .line 524
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/maps/GestureDetector;->mLastFocusX:F

    move/from16 v27, v0

    sub-float v19, v27, v13

    .line 525
    .local v19, "scrollX":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/maps/GestureDetector;->mLastFocusY:F

    move/from16 v27, v0

    sub-float v20, v27, v14

    .line 526
    .local v20, "scrollY":F
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/maps/GestureDetector;->mIsDoubleTapping:Z

    move/from16 v27, v0

    if-eqz v27, :cond_c

    .line 528
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mDoubleTapListener:Lcom/google/android/maps/GestureDetector$OnDoubleTapListener;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    move-object/from16 v1, p1

    invoke-interface {v0, v1}, Lcom/google/android/maps/GestureDetector$OnDoubleTapListener;->onDoubleTapEvent(Landroid/view/MotionEvent;)Z

    move-result v27

    or-int v16, v16, v27

    goto/16 :goto_5

    .line 529
    :cond_c
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/maps/GestureDetector;->mAlwaysInTapRegion:Z

    move/from16 v27, v0

    if-eqz v27, :cond_e

    .line 530
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/maps/GestureDetector;->mDownFocusX:F

    move/from16 v27, v0

    sub-float v27, v13, v27

    move/from16 v0, v27

    float-to-int v9, v0

    .line 531
    .local v9, "deltaX":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/maps/GestureDetector;->mDownFocusY:F

    move/from16 v27, v0

    sub-float v27, v14, v27

    move/from16 v0, v27

    float-to-int v10, v0

    .line 532
    .local v10, "deltaY":I
    mul-int v27, v9, v9

    mul-int v28, v10, v10

    add-int v11, v27, v28

    .line 533
    .local v11, "distance":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/maps/GestureDetector;->mTouchSlopSquare:I

    move/from16 v27, v0

    move/from16 v0, v27

    if-le v11, v0, :cond_d

    .line 534
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mListener:Lcom/google/android/maps/GestureDetector$OnGestureListener;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    move-object/from16 v28, v0

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    move-object/from16 v2, p1

    move/from16 v3, v19

    move/from16 v4, v20

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/android/maps/GestureDetector$OnGestureListener;->onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v16

    .line 535
    move-object/from16 v0, p0

    iput v13, v0, Lcom/google/android/maps/GestureDetector;->mLastFocusX:F

    .line 536
    move-object/from16 v0, p0

    iput v14, v0, Lcom/google/android/maps/GestureDetector;->mLastFocusY:F

    .line 537
    const/16 v27, 0x0

    move/from16 v0, v27

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/maps/GestureDetector;->mAlwaysInTapRegion:Z

    .line 538
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mHandler:Landroid/os/Handler;

    move-object/from16 v27, v0

    const/16 v28, 0x3

    invoke-virtual/range {v27 .. v28}, Landroid/os/Handler;->removeMessages(I)V

    .line 539
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mHandler:Landroid/os/Handler;

    move-object/from16 v27, v0

    const/16 v28, 0x1

    invoke-virtual/range {v27 .. v28}, Landroid/os/Handler;->removeMessages(I)V

    .line 540
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mHandler:Landroid/os/Handler;

    move-object/from16 v27, v0

    const/16 v28, 0x2

    invoke-virtual/range {v27 .. v28}, Landroid/os/Handler;->removeMessages(I)V

    .line 542
    :cond_d
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/maps/GestureDetector;->mDoubleTapTouchSlopSquare:I

    move/from16 v27, v0

    move/from16 v0, v27

    if-le v11, v0, :cond_5

    .line 543
    const/16 v27, 0x0

    move/from16 v0, v27

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/maps/GestureDetector;->mAlwaysInBiggerTapRegion:Z

    goto/16 :goto_5

    .line 545
    .end local v9    # "deltaX":I
    .end local v10    # "deltaY":I
    .end local v11    # "distance":I
    :cond_e
    invoke-static/range {v19 .. v19}, Ljava/lang/Math;->abs(F)F

    move-result v27

    const/high16 v28, 0x3f800000    # 1.0f

    cmpl-float v27, v27, v28

    if-gez v27, :cond_f

    invoke-static/range {v20 .. v20}, Ljava/lang/Math;->abs(F)F

    move-result v27

    const/high16 v28, 0x3f800000    # 1.0f

    cmpl-float v27, v27, v28

    if-ltz v27, :cond_5

    .line 546
    :cond_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mListener:Lcom/google/android/maps/GestureDetector$OnGestureListener;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    move-object/from16 v28, v0

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    move-object/from16 v2, p1

    move/from16 v3, v19

    move/from16 v4, v20

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/android/maps/GestureDetector$OnGestureListener;->onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v16

    .line 547
    move-object/from16 v0, p0

    iput v13, v0, Lcom/google/android/maps/GestureDetector;->mLastFocusX:F

    .line 548
    move-object/from16 v0, p0

    iput v14, v0, Lcom/google/android/maps/GestureDetector;->mLastFocusY:F

    goto/16 :goto_5

    .line 553
    .end local v19    # "scrollX":F
    .end local v20    # "scrollY":F
    :pswitch_5
    const/16 v27, 0x0

    move/from16 v0, v27

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/maps/GestureDetector;->mStillDown:Z

    .line 554
    invoke-static/range {p1 .. p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v8

    .line 555
    .local v8, "currentUpEvent":Landroid/view/MotionEvent;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/maps/GestureDetector;->mIsDoubleTapping:Z

    move/from16 v27, v0

    if-eqz v27, :cond_13

    .line 557
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mDoubleTapListener:Lcom/google/android/maps/GestureDetector$OnDoubleTapListener;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    move-object/from16 v1, p1

    invoke-interface {v0, v1}, Lcom/google/android/maps/GestureDetector$OnDoubleTapListener;->onDoubleTapEvent(Landroid/view/MotionEvent;)Z

    move-result v27

    or-int v16, v16, v27

    .line 575
    :cond_10
    :goto_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mPreviousUpEvent:Landroid/view/MotionEvent;

    move-object/from16 v27, v0

    if-eqz v27, :cond_11

    .line 576
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mPreviousUpEvent:Landroid/view/MotionEvent;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/view/MotionEvent;->recycle()V

    .line 579
    :cond_11
    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/google/android/maps/GestureDetector;->mPreviousUpEvent:Landroid/view/MotionEvent;

    .line 580
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v27, v0

    if-eqz v27, :cond_12

    .line 583
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/view/VelocityTracker;->recycle()V

    .line 584
    const/16 v27, 0x0

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/maps/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 586
    :cond_12
    const/16 v27, 0x0

    move/from16 v0, v27

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/maps/GestureDetector;->mIsDoubleTapping:Z

    .line 587
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mHandler:Landroid/os/Handler;

    move-object/from16 v27, v0

    const/16 v28, 0x1

    invoke-virtual/range {v27 .. v28}, Landroid/os/Handler;->removeMessages(I)V

    .line 588
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mHandler:Landroid/os/Handler;

    move-object/from16 v27, v0

    const/16 v28, 0x2

    invoke-virtual/range {v27 .. v28}, Landroid/os/Handler;->removeMessages(I)V

    goto/16 :goto_5

    .line 558
    :cond_13
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/maps/GestureDetector;->mInLongPress:Z

    move/from16 v27, v0

    if-eqz v27, :cond_14

    .line 559
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mHandler:Landroid/os/Handler;

    move-object/from16 v27, v0

    const/16 v28, 0x3

    invoke-virtual/range {v27 .. v28}, Landroid/os/Handler;->removeMessages(I)V

    .line 560
    const/16 v27, 0x0

    move/from16 v0, v27

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/maps/GestureDetector;->mInLongPress:Z

    goto :goto_7

    .line 561
    :cond_14
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/maps/GestureDetector;->mAlwaysInTapRegion:Z

    move/from16 v27, v0

    if-eqz v27, :cond_15

    .line 562
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mListener:Lcom/google/android/maps/GestureDetector$OnGestureListener;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    move-object/from16 v1, p1

    invoke-interface {v0, v1}, Lcom/google/android/maps/GestureDetector$OnGestureListener;->onSingleTapUp(Landroid/view/MotionEvent;)Z

    move-result v16

    goto :goto_7

    .line 565
    :cond_15
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v24, v0

    .line 566
    .local v24, "velocityTracker":Landroid/view/VelocityTracker;
    const/16 v27, 0x3e8

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/maps/GestureDetector;->mMaximumFlingVelocity:I

    move/from16 v28, v0

    move/from16 v0, v28

    int-to-float v0, v0

    move/from16 v28, v0

    move-object/from16 v0, v24

    move/from16 v1, v27

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 567
    invoke-virtual/range {v24 .. v24}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v26

    .line 568
    .local v26, "velocityY":F
    invoke-virtual/range {v24 .. v24}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v25

    .line 570
    .local v25, "velocityX":F
    invoke-static/range {v26 .. v26}, Ljava/lang/Math;->abs(F)F

    move-result v27

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/maps/GestureDetector;->mMinimumFlingVelocity:I

    move/from16 v28, v0

    move/from16 v0, v28

    int-to-float v0, v0

    move/from16 v28, v0

    cmpl-float v27, v27, v28

    if-gtz v27, :cond_16

    invoke-static/range {v25 .. v25}, Ljava/lang/Math;->abs(F)F

    move-result v27

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/maps/GestureDetector;->mMinimumFlingVelocity:I

    move/from16 v28, v0

    move/from16 v0, v28

    int-to-float v0, v0

    move/from16 v28, v0

    cmpl-float v27, v27, v28

    if-lez v27, :cond_10

    .line 572
    :cond_16
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mListener:Lcom/google/android/maps/GestureDetector$OnGestureListener;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/maps/GestureDetector;->mCurrentDownEvent:Landroid/view/MotionEvent;

    move-object/from16 v28, v0

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    move-object/from16 v2, p1

    move/from16 v3, v25

    move/from16 v4, v26

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/android/maps/GestureDetector$OnGestureListener;->onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v16

    goto/16 :goto_7

    .line 592
    .end local v8    # "currentUpEvent":Landroid/view/MotionEvent;
    .end local v24    # "velocityTracker":Landroid/view/VelocityTracker;
    .end local v25    # "velocityX":F
    .end local v26    # "velocityY":F
    :pswitch_6
    invoke-direct/range {p0 .. p0}, Lcom/google/android/maps/GestureDetector;->cancel()V

    goto/16 :goto_5

    .line 468
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_5
        :pswitch_4
        :pswitch_6
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setIsLongpressEnabled(Z)V
    .locals 0
    .param p1, "isLongpressEnabled"    # Z

    .prologue
    .line 424
    iput-boolean p1, p0, Lcom/google/android/maps/GestureDetector;->mIsLongpressEnabled:Z

    .line 425
    return-void
.end method

.method public setOnDoubleTapListener(Lcom/google/android/maps/GestureDetector$OnDoubleTapListener;)V
    .locals 0
    .param p1, "onDoubleTapListener"    # Lcom/google/android/maps/GestureDetector$OnDoubleTapListener;

    .prologue
    .line 411
    iput-object p1, p0, Lcom/google/android/maps/GestureDetector;->mDoubleTapListener:Lcom/google/android/maps/GestureDetector$OnDoubleTapListener;

    .line 412
    return-void
.end method
