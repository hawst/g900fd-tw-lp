.class public Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;
.super Ljava/lang/Object;
.source "DataRequestDispatcher.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher$1;,
        Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher$DataRequestEventUploader;,
        Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher$DispatcherServer;,
        Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher$CookieDataRequest;
    }
.end annotation


# static fields
.field public static final MAX_WORKER_THREAD_COUNT:I

.field private static volatile instance:Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;

.field private static requestId:I


# instance fields
.field protected volatile active:Z

.field protected bytesReceived:I

.field protected bytesSent:I

.field private final clock:Landroid_maps_conflict_avoidance/com/google/common/Clock;

.field protected connectionFactory:Landroid_maps_conflict_avoidance/com/google/common/io/HttpConnectionFactory;

.field protected cookie:J

.field protected final debug:Z

.field protected final defaultServer:Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher$DispatcherServer;

.field protected final distributionChannel:Ljava/lang/String;

.field private errorRetryTime:J

.field private firstConnectionErrorTime:J

.field protected globalSpecialUrlArguments:Ljava/lang/String;

.field private volatile lastActiveTime:J

.field private lastException:Ljava/lang/Throwable;

.field private lastExceptionTime:J

.field private volatile lastSuccessTime:J

.field private final listeners:Ljava/util/Vector;

.field private maxNetworkErrorRetryTimeout:J

.field protected volatile mockLostDataConnection:Z

.field private volatile networkErrorMode:Z

.field private volatile networkSpeedBytesPerSecond:I

.field protected final platformID:Ljava/lang/String;

.field protected final properties:Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;

.field protected volatile serverAddress:Ljava/lang/String;

.field protected final softwareVersion:Ljava/lang/String;

.field private volatile suspendCount:I

.field protected thirdPartyServers:Ljava/util/Vector;

.field private final threadDispatchLock:Ljava/lang/Object;

.field protected warmUpManager:Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/ConnectionWarmUpManager;

.field private volatile workerForegroundThreadCount:I

.field private volatile workerSubmissionThreadCount:I

.field private volatile workerThreadCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 287
    const/4 v0, 0x0

    sput v0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->requestId:I

    .line 316
    const/4 v0, 0x4

    sput v0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->MAX_WORKER_THREAD_COUNT:I

    .line 320
    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 6
    .param p1, "serverAddress"    # Ljava/lang/String;
    .param p2, "platformID"    # Ljava/lang/String;
    .param p3, "softwareVersion"    # Ljava/lang/String;
    .param p4, "distributionChannel"    # Ljava/lang/String;
    .param p5, "debug"    # Z

    .prologue
    const-wide/high16 v4, -0x8000000000000000L

    const/4 v3, 0x0

    .line 374
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 115
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->thirdPartyServers:Ljava/util/Vector;

    .line 139
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->listeners:Ljava/util/Vector;

    .line 187
    const-wide/32 v0, 0x493e0

    iput-wide v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->maxNetworkErrorRetryTimeout:J

    .line 197
    iput-boolean v3, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->active:Z

    .line 211
    iput-wide v4, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->lastActiveTime:J

    .line 217
    iput-wide v4, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->lastSuccessTime:J

    .line 232
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->errorRetryTime:J

    .line 239
    iput-wide v4, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->firstConnectionErrorTime:J

    .line 242
    iput v3, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->workerThreadCount:I

    .line 248
    iput v3, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->workerForegroundThreadCount:I

    .line 255
    iput v3, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->workerSubmissionThreadCount:I

    .line 258
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->threadDispatchLock:Ljava/lang/Object;

    .line 272
    const/4 v0, -0x1

    iput v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->networkSpeedBytesPerSecond:I

    .line 375
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 376
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 378
    :cond_1
    iput-object p1, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->serverAddress:Ljava/lang/String;

    .line 379
    iput-object p3, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->softwareVersion:Ljava/lang/String;

    .line 380
    iput-object p2, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->platformID:Ljava/lang/String;

    .line 381
    iput-object p4, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->distributionChannel:Ljava/lang/String;

    .line 382
    iput-boolean p5, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->debug:Z

    .line 383
    invoke-static {}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getInstance()Landroid_maps_conflict_avoidance/com/google/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getConnectionFactory()Landroid_maps_conflict_avoidance/com/google/common/io/HttpConnectionFactory;

    move-result-object v0

    iput-object v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->connectionFactory:Landroid_maps_conflict_avoidance/com/google/common/io/HttpConnectionFactory;

    .line 384
    invoke-static {}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getInstance()Landroid_maps_conflict_avoidance/com/google/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getClock()Landroid_maps_conflict_avoidance/com/google/common/Clock;

    move-result-object v0

    iput-object v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->clock:Landroid_maps_conflict_avoidance/com/google/common/Clock;

    .line 385
    new-instance v0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/ConnectionWarmUpManager;

    iget-object v1, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->clock:Landroid_maps_conflict_avoidance/com/google/common/Clock;

    invoke-direct {v0, p0, v1}, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/ConnectionWarmUpManager;-><init>(Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;Landroid_maps_conflict_avoidance/com/google/common/Clock;)V

    iput-object v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->warmUpManager:Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/ConnectionWarmUpManager;

    .line 386
    iput v3, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->bytesSent:I

    .line 387
    iput v3, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->bytesReceived:I

    .line 388
    new-instance v0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher$DispatcherServer;

    iget-object v1, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->serverAddress:Ljava/lang/String;

    new-instance v2, Ljava/util/Vector;

    invoke-direct {v2}, Ljava/util/Vector;-><init>()V

    invoke-direct {v0, p0, v1, v2, v3}, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher$DispatcherServer;-><init>(Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;Ljava/lang/String;Ljava/util/Vector;B)V

    iput-object v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->defaultServer:Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher$DispatcherServer;

    .line 390
    invoke-virtual {p0}, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->loadOrRequestCookie()J

    move-result-wide v0

    iput-wide v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->cookie:J

    .line 392
    new-instance v0, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;

    sget-object v1, Landroid_maps_conflict_avoidance/com/google/googlenav/proto/GmmMessageTypes;->CLIENT_PROPERTIES_REQUEST_PROTO:Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBufType;

    invoke-direct {v0, v1}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;-><init>(Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBufType;)V

    iput-object v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->properties:Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;

    .line 395
    return-void
.end method

.method static synthetic access$1002(Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;J)J
    .locals 1
    .param p0, "x0"    # Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;
    .param p1, "x1"    # J

    .prologue
    .line 53
    iput-wide p1, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->lastSuccessTime:J

    return-wide p1
.end method

.method static synthetic access$1100(Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;ILjava/lang/Throwable;)V
    .locals 0
    .param p0, "x0"    # Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;
    .param p1, "x1"    # I
    .param p2, "x2"    # Ljava/lang/Throwable;

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->handleError(ILjava/lang/Throwable;)V

    return-void
.end method

.method static synthetic access$200(Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;

    .prologue
    .line 53
    iget-object v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->threadDispatchLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$308(Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;)I
    .locals 2
    .param p0, "x0"    # Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;

    .prologue
    .line 53
    iget v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->workerThreadCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->workerThreadCount:I

    return v0
.end method

.method static synthetic access$310(Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;)I
    .locals 2
    .param p0, "x0"    # Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;

    .prologue
    .line 53
    iget v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->workerThreadCount:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->workerThreadCount:I

    return v0
.end method

.method static synthetic access$408(Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;)I
    .locals 2
    .param p0, "x0"    # Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;

    .prologue
    .line 53
    iget v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->workerForegroundThreadCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->workerForegroundThreadCount:I

    return v0
.end method

.method static synthetic access$410(Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;)I
    .locals 2
    .param p0, "x0"    # Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;

    .prologue
    .line 53
    iget v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->workerForegroundThreadCount:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->workerForegroundThreadCount:I

    return v0
.end method

.method static synthetic access$508(Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;)I
    .locals 2
    .param p0, "x0"    # Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;

    .prologue
    .line 53
    iget v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->workerSubmissionThreadCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->workerSubmissionThreadCount:I

    return v0
.end method

.method static synthetic access$510(Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;)I
    .locals 2
    .param p0, "x0"    # Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;

    .prologue
    .line 53
    iget v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->workerSubmissionThreadCount:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->workerSubmissionThreadCount:I

    return v0
.end method

.method static synthetic access$602(Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;J)J
    .locals 1
    .param p0, "x0"    # Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;
    .param p1, "x1"    # J

    .prologue
    .line 53
    iput-wide p1, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->lastActiveTime:J

    return-wide p1
.end method

.method static synthetic access$700(Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;)Landroid_maps_conflict_avoidance/com/google/common/Clock;
    .locals 1
    .param p0, "x0"    # Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;

    .prologue
    .line 53
    iget-object v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->clock:Landroid_maps_conflict_avoidance/com/google/common/Clock;

    return-object v0
.end method

.method static synthetic access$800(Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;)J
    .locals 2
    .param p0, "x0"    # Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;

    .prologue
    .line 53
    iget-wide v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->errorRetryTime:J

    return-wide v0
.end method

.method static synthetic access$900(Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;)V
    .locals 0
    .param p0, "x0"    # Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;

    .prologue
    .line 53
    invoke-direct {p0}, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->clearNetworkError()V

    return-void
.end method

.method private addClientPropertiesRequest(Ljava/util/Vector;Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher$DispatcherServer;)V
    .locals 4
    .param p1, "requests"    # Ljava/util/Vector;
    .param p2, "dispatcherServer"    # Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher$DispatcherServer;

    .prologue
    const/4 v3, 0x0

    .line 1175
    const/16 v2, 0x3e

    invoke-virtual {p2, v2}, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher$DispatcherServer;->canHandle(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1176
    new-instance v0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/ClientPropertiesRequest;

    iget-object v2, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->properties:Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;

    invoke-direct {v0, v2}, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/ClientPropertiesRequest;-><init>(Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;)V

    .line 1178
    .local v0, "clientProperties":Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/ClientPropertiesRequest;
    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result v2

    if-lez v2, :cond_2

    .line 1179
    invoke-virtual {p1, v3}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;

    .line 1180
    .local v1, "request":Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;
    instance-of v2, v1, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/ClientPropertiesRequest;

    if-eqz v2, :cond_1

    .line 1182
    invoke-virtual {p1, v0, v3}, Ljava/util/Vector;->setElementAt(Ljava/lang/Object;I)V

    .line 1190
    .end local v0    # "clientProperties":Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/ClientPropertiesRequest;
    .end local v1    # "request":Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;
    :cond_0
    :goto_0
    return-void

    .line 1184
    .restart local v0    # "clientProperties":Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/ClientPropertiesRequest;
    .restart local v1    # "request":Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;
    :cond_1
    invoke-virtual {p1, v0, v3}, Ljava/util/Vector;->insertElementAt(Ljava/lang/Object;I)V

    goto :goto_0

    .line 1187
    .end local v1    # "request":Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;
    :cond_2
    invoke-virtual {p1, v0, v3}, Ljava/util/Vector;->insertElementAt(Ljava/lang/Object;I)V

    goto :goto_0
.end method

.method private declared-synchronized clearNetworkError()V
    .locals 2

    .prologue
    .line 787
    monitor-enter p0

    const-wide/high16 v0, -0x8000000000000000L

    :try_start_0
    iput-wide v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->firstConnectionErrorTime:J

    .line 788
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->networkErrorMode:Z

    .line 789
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->errorRetryTime:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 790
    monitor-exit p0

    return-void

    .line 787
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected static containsForegroundRequest(Ljava/util/Vector;)Z
    .locals 3
    .param p0, "requests"    # Ljava/util/Vector;

    .prologue
    .line 1211
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/util/Vector;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 1212
    invoke-virtual {p0, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;

    .line 1213
    .local v1, "request":Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;
    invoke-interface {v1}, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;->isForeground()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1214
    const/4 v2, 0x1

    .line 1218
    .end local v1    # "request":Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;
    :goto_1
    return v2

    .line 1211
    .restart local v1    # "request":Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1218
    .end local v1    # "request":Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method protected static containsSubmissionRequest(Ljava/util/Vector;)Z
    .locals 3
    .param p0, "requests"    # Ljava/util/Vector;

    .prologue
    .line 1223
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/util/Vector;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 1224
    invoke-virtual {p0, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;

    .line 1225
    .local v1, "request":Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;
    invoke-interface {v1}, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;->isSubmission()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1226
    const/4 v2, 0x1

    .line 1229
    .end local v1    # "request":Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;
    :goto_1
    return v2

    .line 1223
    .restart local v1    # "request":Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1229
    .end local v1    # "request":Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public static declared-synchronized createInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;
    .locals 7
    .param p0, "serverAddress"    # Ljava/lang/String;
    .param p1, "platformID"    # Ljava/lang/String;
    .param p2, "softwareVersion"    # Ljava/lang/String;
    .param p3, "distributionChannel"    # Ljava/lang/String;
    .param p4, "debug"    # Z

    .prologue
    .line 337
    const-class v6, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;

    monitor-enter v6

    :try_start_0
    sget-object v0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->instance:Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;

    if-eqz v0, :cond_0

    .line 338
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Attempting to create multiple DataRequestDispatchers"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 337
    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0

    .line 341
    :cond_0
    :try_start_1
    new-instance v0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    sput-object v0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->instance:Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;

    .line 343
    new-instance v0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher$DataRequestEventUploader;

    sget-object v1, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->instance:Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher$DataRequestEventUploader;-><init>(Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher$1;)V

    invoke-static {v0}, Landroid_maps_conflict_avoidance/com/google/common/Log;->setLogSaver(Landroid_maps_conflict_avoidance/com/google/common/Log$LogSaver;)V

    .line 344
    sget-object v0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->instance:Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v6

    return-object v0
.end method

.method public static getInstance()Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;
    .locals 1

    .prologue
    .line 348
    sget-object v0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->instance:Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;

    return-object v0
.end method

.method private handleError(ILjava/lang/Throwable;)V
    .locals 6
    .param p1, "code"    # I
    .param p2, "t"    # Ljava/lang/Throwable;

    .prologue
    const-wide/16 v4, 0x7d0

    .line 801
    const/4 v0, 0x0

    .line 802
    .local v0, "call":Z
    monitor-enter p0

    .line 803
    :try_start_0
    iput-object p2, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->lastException:Ljava/lang/Throwable;

    .line 804
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->lastExceptionTime:J

    .line 807
    if-eqz p2, :cond_0

    invoke-static {}, Landroid_maps_conflict_avoidance/com/google/googlenav/GmmSettings;->isDebugBuild()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 808
    invoke-virtual {p2}, Ljava/lang/Throwable;->printStackTrace()V

    .line 811
    :cond_0
    iget-object v1, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->connectionFactory:Landroid_maps_conflict_avoidance/com/google/common/io/HttpConnectionFactory;

    invoke-interface {v1}, Landroid_maps_conflict_avoidance/com/google/common/io/HttpConnectionFactory;->notifyFailure()V

    .line 813
    iget-boolean v1, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->networkErrorMode:Z

    if-nez v1, :cond_5

    .line 814
    const-wide/16 v2, 0xc8

    iput-wide v2, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->errorRetryTime:J

    .line 816
    iget-wide v2, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->firstConnectionErrorTime:J

    const-wide/high16 v4, -0x8000000000000000L

    cmp-long v1, v2, v4

    if-nez v1, :cond_4

    .line 817
    iget-object v1, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->clock:Landroid_maps_conflict_avoidance/com/google/common/Clock;

    invoke-interface {v1}, Landroid_maps_conflict_avoidance/com/google/common/Clock;->relativeTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->firstConnectionErrorTime:J

    .line 837
    :cond_1
    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 839
    if-eqz v0, :cond_3

    .line 842
    const/4 v1, 0x3

    if-ne p1, v1, :cond_2

    iget-object v1, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->connectionFactory:Landroid_maps_conflict_avoidance/com/google/common/io/HttpConnectionFactory;

    invoke-interface {v1}, Landroid_maps_conflict_avoidance/com/google/common/io/HttpConnectionFactory;->usingMDS()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->connectionFactory:Landroid_maps_conflict_avoidance/com/google/common/io/HttpConnectionFactory;

    invoke-interface {v1}, Landroid_maps_conflict_avoidance/com/google/common/io/HttpConnectionFactory;->getNetworkWorked()Z

    move-result v1

    if-nez v1, :cond_2

    .line 845
    const/4 p1, 0x4

    .line 847
    :cond_2
    invoke-virtual {p0, p1}, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->maybeNotifyNetworkError(I)V

    .line 849
    :cond_3
    return-void

    .line 818
    :cond_4
    :try_start_1
    iget-wide v2, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->firstConnectionErrorTime:J

    const-wide/16 v4, 0x3a98

    add-long/2addr v2, v4

    iget-object v1, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->clock:Landroid_maps_conflict_avoidance/com/google/common/Clock;

    invoke-interface {v1}, Landroid_maps_conflict_avoidance/com/google/common/Clock;->relativeTimeMillis()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-gez v1, :cond_1

    .line 822
    const/4 v0, 0x1

    goto :goto_0

    .line 825
    :cond_5
    iget-wide v2, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->errorRetryTime:J

    cmp-long v1, v2, v4

    if-gez v1, :cond_6

    .line 826
    const-wide/16 v2, 0x7d0

    iput-wide v2, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->errorRetryTime:J

    .line 833
    :goto_1
    iget-wide v2, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->errorRetryTime:J

    iget-wide v4, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->maxNetworkErrorRetryTimeout:J

    cmp-long v1, v2, v4

    if-lez v1, :cond_1

    .line 834
    iget-wide v2, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->maxNetworkErrorRetryTimeout:J

    iput-wide v2, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->errorRetryTime:J

    goto :goto_0

    .line 837
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 829
    :cond_6
    :try_start_2
    iget-wide v2, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->errorRetryTime:J

    const-wide/16 v4, 0x5

    mul-long/2addr v2, v4

    const-wide/16 v4, 0x4

    div-long/2addr v2, v4

    iput-wide v2, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->errorRetryTime:J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method static saveCookie(J)V
    .locals 6
    .param p0, "cookie"    # J

    .prologue
    .line 1238
    :try_start_0
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 1239
    .local v0, "baos":Ljava/io/ByteArrayOutputStream;
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 1240
    .local v1, "dos":Ljava/io/DataOutput;
    invoke-interface {v1, p0, p1}, Ljava/io/DataOutput;->writeLong(J)V

    .line 1241
    invoke-static {}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getInstance()Landroid_maps_conflict_avoidance/com/google/common/Config;

    move-result-object v4

    invoke-virtual {v4}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getPersistentStore()Landroid_maps_conflict_avoidance/com/google/common/io/PersistentStore;

    move-result-object v3

    .line 1242
    .local v3, "store":Landroid_maps_conflict_avoidance/com/google/common/io/PersistentStore;
    const-string v4, "SessionID"

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v5

    invoke-interface {v3, v4, v5}, Landroid_maps_conflict_avoidance/com/google/common/io/PersistentStore;->setPreference(Ljava/lang/String;[B)Z

    .line 1246
    invoke-interface {v3}, Landroid_maps_conflict_avoidance/com/google/common/io/PersistentStore;->savePreferences()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1251
    return-void

    .line 1247
    .end local v0    # "baos":Ljava/io/ByteArrayOutputStream;
    .end local v1    # "dos":Ljava/io/DataOutput;
    .end local v3    # "store":Landroid_maps_conflict_avoidance/com/google/common/io/PersistentStore;
    :catch_0
    move-exception v2

    .line 1249
    .local v2, "e":Ljava/io/IOException;
    new-instance v4, Ljava/lang/RuntimeException;

    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4
.end method


# virtual methods
.method public addDataRequest(Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;)V
    .locals 5
    .param p1, "dataRequest"    # Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;

    .prologue
    .line 612
    iget-boolean v2, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->mockLostDataConnection:Z

    if-eqz v2, :cond_0

    .line 613
    const/4 v2, 0x5

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {p0, v2, v3, v4}, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->notifyNetworkError(IZLjava/lang/String;)V

    .line 617
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->thirdPartyServers:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 618
    iget-object v2, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->thirdPartyServers:Ljava/util/Vector;

    invoke-virtual {v2, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher$DispatcherServer;

    .line 619
    .local v1, "tps":Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher$DispatcherServer;
    invoke-interface {p1}, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;->getRequestType()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher$DispatcherServer;->canHandle(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 620
    invoke-virtual {v1, p1}, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher$DispatcherServer;->addDataRequest(Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;)V

    .line 627
    .end local v1    # "tps":Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher$DispatcherServer;
    :goto_1
    return-void

    .line 617
    .restart local v1    # "tps":Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher$DispatcherServer;
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 626
    .end local v1    # "tps":Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher$DispatcherServer;
    :cond_2
    iget-object v2, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->defaultServer:Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher$DispatcherServer;

    invoke-virtual {v2, p1}, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher$DispatcherServer;->addDataRequest(Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;)V

    goto :goto_1
.end method

.method public declared-synchronized addDataRequestListener(Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestListener;)V
    .locals 1
    .param p1, "listenerData"    # Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestListener;

    .prologue
    .line 513
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->listeners:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 514
    iget-object v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->listeners:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 516
    :cond_0
    monitor-exit p0

    return-void

    .line 513
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final addSimpleRequest(I[BZZ)V
    .locals 1
    .param p1, "requestType"    # I
    .param p2, "data"    # [B
    .param p3, "immediate"    # Z
    .param p4, "foreground"    # Z

    .prologue
    .line 1204
    new-instance v0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/SimpleDataRequest;

    invoke-direct {v0, p1, p2, p3, p4}, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/SimpleDataRequest;-><init>(I[BZZ)V

    invoke-virtual {p0, v0}, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->addDataRequest(Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;)V

    .line 1206
    return-void
.end method

.method public declared-synchronized canDispatchNow()Z
    .locals 2

    .prologue
    .line 670
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->active:Z

    if-eqz v0, :cond_1

    iget v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->workerThreadCount:I

    sget v1, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->MAX_WORKER_THREAD_COUNT:I

    if-ge v0, v1, :cond_1

    iget-object v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->connectionFactory:Landroid_maps_conflict_avoidance/com/google/common/io/HttpConnectionFactory;

    invoke-interface {v0}, Landroid_maps_conflict_avoidance/com/google/common/io/HttpConnectionFactory;->getNetworkWorkedThisSession()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->workerThreadCount:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public generateRequest(Ljava/util/Vector;Ljava/io/OutputStream;Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher$DispatcherServer;)V
    .locals 6
    .param p1, "requests"    # Ljava/util/Vector;
    .param p2, "outputStream"    # Ljava/io/OutputStream;
    .param p3, "dispatcherServer"    # Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher$DispatcherServer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v5, 0x17

    .line 1139
    new-instance v2, Ljava/io/DataOutputStream;

    invoke-direct {v2, p2}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 1141
    .local v2, "out":Ljava/io/DataOutputStream;
    invoke-direct {p0, p1, p3}, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->addClientPropertiesRequest(Ljava/util/Vector;Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher$DispatcherServer;)V

    .line 1143
    iget-byte v3, p3, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher$DispatcherServer;->headerFlag:B

    if-nez v3, :cond_1

    .line 1144
    invoke-virtual {v2, v5}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 1145
    iget-wide v4, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->cookie:J

    invoke-virtual {v2, v4, v5}, Ljava/io/DataOutputStream;->writeLong(J)V

    .line 1146
    invoke-static {}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getLocale()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 1147
    iget-object v3, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->platformID:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 1148
    iget-object v3, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->softwareVersion:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 1149
    iget-object v3, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->distributionChannel:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 1158
    :cond_0
    :goto_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result v3

    if-ge v1, v3, :cond_2

    .line 1159
    invoke-virtual {p1, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;

    .line 1161
    .local v0, "dataRequest":Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;
    invoke-interface {v0}, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;->getRequestType()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 1162
    invoke-interface {v0, v2}, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;->writeRequestData(Ljava/io/DataOutput;)V

    .line 1158
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1150
    .end local v0    # "dataRequest":Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;
    .end local v1    # "i":I
    :cond_1
    iget-byte v3, p3, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher$DispatcherServer;->headerFlag:B

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 1151
    invoke-virtual {v2, v5}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 1152
    iget-wide v4, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->cookie:J

    invoke-virtual {v2, v4, v5}, Ljava/io/DataOutputStream;->writeLong(J)V

    .line 1153
    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 1154
    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 1155
    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    .line 1156
    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/io/DataOutputStream;->writeUTF(Ljava/lang/String;)V

    goto :goto_0

    .line 1164
    .restart local v1    # "i":I
    :cond_2
    invoke-virtual {v2}, Ljava/io/DataOutputStream;->flush()V

    .line 1165
    return-void
.end method

.method public declared-synchronized isSuspended()Z
    .locals 1

    .prologue
    .line 448
    monitor-enter p0

    :try_start_0
    iget v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->suspendCount:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected loadOrRequestCookie()J
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 493
    const-string v2, "SessionID"

    invoke-static {v2}, Landroid_maps_conflict_avoidance/com/google/common/StaticUtil;->readPreferenceAsDataInput(Ljava/lang/String;)Ljava/io/DataInput;

    move-result-object v0

    .line 494
    .local v0, "dis":Ljava/io/DataInput;
    if-eqz v0, :cond_0

    .line 497
    :try_start_0
    invoke-interface {v0}, Ljava/io/DataInput;->readLong()J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 506
    :goto_0
    return-wide v2

    .line 498
    :catch_0
    move-exception v1

    .line 501
    .local v1, "e":Ljava/io/IOException;
    invoke-static {}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getInstance()Landroid_maps_conflict_avoidance/com/google/common/Config;

    move-result-object v2

    invoke-virtual {v2}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getPersistentStore()Landroid_maps_conflict_avoidance/com/google/common/io/PersistentStore;

    move-result-object v2

    const-string v3, "SessionID"

    invoke-interface {v2, v3, v4}, Landroid_maps_conflict_avoidance/com/google/common/io/PersistentStore;->setPreference(Ljava/lang/String;[B)Z

    .line 505
    .end local v1    # "e":Ljava/io/IOException;
    :cond_0
    new-instance v2, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher$CookieDataRequest;

    invoke-direct {v2, p0, v4}, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher$CookieDataRequest;-><init>(Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher$1;)V

    invoke-virtual {p0, v2}, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->addDataRequest(Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;)V

    .line 506
    const-wide/16 v2, 0x0

    goto :goto_0
.end method

.method protected final maybeNotifyNetworkError(I)V
    .locals 4
    .param p1, "errorCode"    # I

    .prologue
    .line 581
    const/4 v1, 0x0

    .line 582
    .local v1, "notifyListeners":Z
    monitor-enter p0

    .line 583
    :try_start_0
    iget-boolean v2, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->networkErrorMode:Z

    if-nez v2, :cond_0

    .line 584
    const-string v2, "DRD: in Error Mode"

    invoke-static {v2}, Landroid_maps_conflict_avoidance/com/google/common/Log;->logToScreen(Ljava/lang/String;)V

    .line 585
    const/4 v2, 0x1

    iput-boolean v2, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->networkErrorMode:Z

    .line 586
    const-wide/high16 v2, -0x8000000000000000L

    iput-wide v2, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->firstConnectionErrorTime:J

    .line 587
    const/4 v1, 0x1

    .line 589
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 591
    iget-object v2, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->connectionFactory:Landroid_maps_conflict_avoidance/com/google/common/io/HttpConnectionFactory;

    invoke-interface {v2}, Landroid_maps_conflict_avoidance/com/google/common/io/HttpConnectionFactory;->getNetworkWorked()Z

    move-result v0

    .line 593
    .local v0, "networkEverWorked":Z
    if-eqz v1, :cond_1

    .line 594
    const/4 v2, 0x0

    invoke-virtual {p0, p1, v0, v2}, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->notifyNetworkError(IZLjava/lang/String;)V

    .line 596
    :cond_1
    return-void

    .line 589
    .end local v0    # "networkEverWorked":Z
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method protected notifyComplete(Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;)V
    .locals 3
    .param p1, "dataRequest"    # Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;

    .prologue
    .line 541
    invoke-virtual {p0}, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->snapshotListeners()[Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestListener;

    move-result-object v1

    .line 545
    .local v1, "listenersArray":[Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestListener;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    .line 546
    aget-object v2, v1, v0

    invoke-interface {v2, p1}, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestListener;->onComplete(Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;)V

    .line 545
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 548
    :cond_0
    return-void
.end method

.method protected notifyNetworkError(IZLjava/lang/String;)V
    .locals 3
    .param p1, "errorCode"    # I
    .param p2, "networkEverWorked"    # Z
    .param p3, "debugMessage"    # Ljava/lang/String;

    .prologue
    .line 555
    invoke-virtual {p0}, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->snapshotListeners()[Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestListener;

    move-result-object v1

    .line 558
    .local v1, "listenersArray":[Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestListener;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    .line 559
    aget-object v2, v1, v0

    invoke-interface {v2, p1, p2, p3}, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestListener;->onNetworkError(IZLjava/lang/String;)V

    .line 558
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 561
    :cond_0
    return-void
.end method

.method protected processDataRequest(Ljava/io/DataInput;Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher$DispatcherServer;)V
    .locals 4
    .param p1, "is"    # Ljava/io/DataInput;
    .param p2, "dataRequest"    # Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;
    .param p3, "dispatcherServer"    # Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher$DispatcherServer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1108
    invoke-interface {p1}, Ljava/io/DataInput;->readUnsignedByte()I

    move-result v0

    .line 1109
    .local v0, "requestType":I
    invoke-interface {p2}, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;->getRequestType()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 1113
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "RT: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " != "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p2}, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;->getRequestType()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1117
    :cond_0
    invoke-interface {p2, p1}, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;->readResponseData(Ljava/io/DataInput;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1122
    if-eq p2, p0, :cond_1

    invoke-interface {p2}, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;->isCancelled()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1123
    invoke-virtual {p0, p2}, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->notifyComplete(Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;)V

    .line 1129
    :cond_1
    :goto_0
    return-void

    .line 1127
    :cond_2
    iget-object v1, p3, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher$DispatcherServer;->serverRequests:Ljava/util/Vector;

    const/4 v2, 0x0

    invoke-virtual {v1, p2, v2}, Ljava/util/Vector;->insertElementAt(Ljava/lang/Object;I)V

    goto :goto_0
.end method

.method public declared-synchronized removeDataRequestListener(Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestListener;)V
    .locals 1
    .param p1, "listenerData"    # Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestListener;

    .prologue
    .line 523
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->listeners:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->removeElement(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 524
    monitor-exit p0

    return-void

    .line 523
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public resetConnectionFactory()V
    .locals 1

    .prologue
    .line 1434
    invoke-static {}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getInstance()Landroid_maps_conflict_avoidance/com/google/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getConnectionFactory()Landroid_maps_conflict_avoidance/com/google/common/io/HttpConnectionFactory;

    move-result-object v0

    iput-object v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->connectionFactory:Landroid_maps_conflict_avoidance/com/google/common/io/HttpConnectionFactory;

    .line 1435
    return-void
.end method

.method protected serviceRequests(Ljava/util/Vector;Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher$DispatcherServer;)V
    .locals 36
    .param p1, "requests"    # Ljava/util/Vector;
    .param p2, "dispatcherServer"    # Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher$DispatcherServer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/SecurityException;
        }
    .end annotation

    .prologue
    .line 870
    const/16 v17, 0x0

    .line 871
    .local v17, "hc":Landroid_maps_conflict_avoidance/com/google/common/io/GoogleHttpConnection;
    const/16 v24, 0x0

    .line 872
    .local v24, "os":Ljava/io/DataOutputStream;
    const/16 v25, 0x0

    .line 873
    .local v25, "osSkipClose":Z
    const/16 v22, 0x0

    .line 875
    .local v22, "is":Ljava/io/DataInputStream;
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->warmUpManager:Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/ConnectionWarmUpManager;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/ConnectionWarmUpManager;->onStartServiceRequests(Ljava/lang/Object;)V

    .line 884
    new-instance v11, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v11}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 885
    .local v11, "baos":Ljava/io/ByteArrayOutputStream;
    invoke-virtual/range {p0 .. p1}, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->urlArguments(Ljava/util/Vector;)Ljava/lang/String;

    move-result-object v10

    .line 886
    .local v10, "addToUrl":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-virtual {v0, v1, v11, v2}, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->generateRequest(Ljava/util/Vector;Ljava/io/OutputStream;Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher$DispatcherServer;)V

    .line 889
    new-instance v15, Ljava/lang/StringBuffer;

    const-string v4, "DRD"

    invoke-direct {v15, v4}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 890
    .local v15, "drdDebug":Ljava/lang/StringBuffer;
    const-string v4, "("

    invoke-virtual {v15, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    sget v5, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->requestId:I

    add-int/lit8 v8, v5, 0x1

    sput v8, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->requestId:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, "): "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 891
    const/16 v20, 0x0

    .local v20, "i":I
    :goto_0
    invoke-virtual/range {p1 .. p1}, Ljava/util/Vector;->size()I

    move-result v4

    move/from16 v0, v20

    if-ge v0, v4, :cond_1

    .line 892
    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;

    .line 893
    .local v13, "dataRequest":Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;
    invoke-interface {v13}, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;->getRequestType()I

    move-result v4

    invoke-virtual {v15, v4}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 894
    invoke-virtual/range {p1 .. p1}, Ljava/util/Vector;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    move/from16 v0, v20

    if-eq v0, v4, :cond_0

    .line 895
    const-string v4, "|"

    invoke-virtual {v15, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 891
    :cond_0
    add-int/lit8 v20, v20, 0x1

    goto :goto_0

    .line 899
    .end local v13    # "dataRequest":Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;
    :cond_1
    invoke-virtual {v11}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v31

    .line 900
    .local v31, "sendData":[B
    const/4 v11, 0x0

    .line 908
    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->clock:Landroid_maps_conflict_avoidance/com/google/common/Clock;

    invoke-interface {v4}, Landroid_maps_conflict_avoidance/com/google/common/Clock;->relativeTimeMillis()J

    move-result-wide v6

    .line 909
    .local v6, "startTime":J
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->connectionFactory:Landroid_maps_conflict_avoidance/com/google/common/io/HttpConnectionFactory;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p2

    iget-object v8, v0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher$DispatcherServer;->serverAddress:Ljava/lang/String;

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v8, 0x1

    invoke-interface {v4, v5, v8}, Landroid_maps_conflict_avoidance/com/google/common/io/HttpConnectionFactory;->createConnection(Ljava/lang/String;Z)Landroid_maps_conflict_avoidance/com/google/common/io/GoogleHttpConnection;

    move-result-object v17

    .line 913
    const-string v4, "Content-Type"

    const-string v5, "application/binary"

    move-object/from16 v0, v17

    invoke-interface {v0, v4, v5}, Landroid_maps_conflict_avoidance/com/google/common/io/GoogleHttpConnection;->setConnectionProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 914
    const-string v4, "Content-Length"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v31

    array-length v8, v0

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v17

    invoke-interface {v0, v4, v5}, Landroid_maps_conflict_avoidance/com/google/common/io/GoogleHttpConnection;->setConnectionProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 917
    invoke-interface/range {v17 .. v17}, Landroid_maps_conflict_avoidance/com/google/common/io/GoogleHttpConnection;->openDataOutputStream()Ljava/io/DataOutputStream;

    move-result-object v24

    .line 919
    move-object/from16 v0, v24

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->write([B)V

    .line 920
    move-object/from16 v0, p0

    iget v4, v0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->bytesSent:I

    move-object/from16 v0, v31

    array-length v5, v0

    add-int/2addr v4, v5

    move-object/from16 v0, p0

    iput v4, v0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->bytesSent:I

    .line 922
    invoke-interface/range {v17 .. v17}, Landroid_maps_conflict_avoidance/com/google/common/io/GoogleHttpConnection;->openDataInputStream()Ljava/io/DataInputStream;

    move-result-object v22

    .line 923
    invoke-interface/range {v17 .. v17}, Landroid_maps_conflict_avoidance/com/google/common/io/GoogleHttpConnection;->getResponseCode()I

    move-result v29

    .line 924
    .local v29, "responseCode":I
    invoke-interface/range {v17 .. v17}, Landroid_maps_conflict_avoidance/com/google/common/io/GoogleHttpConnection;->getContentType()Ljava/lang/String;

    move-result-object v12

    .line 925
    .local v12, "contentType":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->clock:Landroid_maps_conflict_avoidance/com/google/common/Clock;

    invoke-interface {v4}, Landroid_maps_conflict_avoidance/com/google/common/Clock;->relativeTimeMillis()J

    move-result-wide v4

    sub-long v18, v4, v6

    .line 927
    .local v18, "firstByteTime":J
    const-string v4, ", "

    invoke-virtual {v15, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 928
    const-wide/16 v4, 0x3e8

    cmp-long v4, v18, v4

    if-gez v4, :cond_6

    .line 929
    const-string v4, "<1s"

    invoke-virtual {v15, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 934
    :goto_1
    const/16 v4, 0x1f5

    move/from16 v0, v29

    if-ne v0, v4, :cond_b

    .line 938
    const/4 v4, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->maybeNotifyNetworkError(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1062
    invoke-virtual {v15}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid_maps_conflict_avoidance/com/google/common/Log;->logToScreen(Ljava/lang/String;)V

    .line 1064
    if-eqz v22, :cond_2

    .line 1065
    :try_start_1
    invoke-virtual/range {v22 .. v22}, Ljava/io/DataInputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5

    .line 1071
    :cond_2
    :goto_2
    if-eqz v24, :cond_3

    if-nez v25, :cond_3

    .line 1072
    :try_start_2
    invoke-virtual/range {v24 .. v24}, Ljava/io/DataOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_6

    .line 1078
    :cond_3
    :goto_3
    if-eqz v17, :cond_4

    .line 1079
    :try_start_3
    invoke-interface/range {v17 .. v17}, Landroid_maps_conflict_avoidance/com/google/common/io/GoogleHttpConnection;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_7

    .line 1087
    :cond_4
    :goto_4
    const/16 v20, 0x0

    :goto_5
    invoke-virtual/range {p1 .. p1}, Ljava/util/Vector;->size()I

    move-result v4

    move/from16 v0, v20

    if-ge v0, v4, :cond_22

    .line 1088
    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;

    .line 1090
    .restart local v13    # "dataRequest":Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;
    invoke-interface {v13}, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;->retryOnFailure()Z

    move-result v4

    if-nez v4, :cond_5

    .line 1091
    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Ljava/util/Vector;->removeElement(Ljava/lang/Object;)Z

    .line 1087
    :cond_5
    add-int/lit8 v20, v20, 0x1

    goto :goto_5

    .line 931
    .end local v13    # "dataRequest":Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;
    :cond_6
    const-wide/16 v4, 0x3e8

    :try_start_4
    div-long v4, v18, v4

    invoke-virtual {v15, v4, v5}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, "s"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 1062
    .end local v6    # "startTime":J
    .end local v12    # "contentType":Ljava/lang/String;
    .end local v18    # "firstByteTime":J
    .end local v29    # "responseCode":I
    :catchall_0
    move-exception v4

    invoke-virtual {v15}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid_maps_conflict_avoidance/com/google/common/Log;->logToScreen(Ljava/lang/String;)V

    .line 1064
    if-eqz v22, :cond_7

    .line 1065
    :try_start_5
    invoke-virtual/range {v22 .. v22}, Ljava/io/DataInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 1071
    :cond_7
    :goto_6
    if-eqz v24, :cond_8

    if-nez v25, :cond_8

    .line 1072
    :try_start_6
    invoke-virtual/range {v24 .. v24}, Ljava/io/DataOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    .line 1078
    :cond_8
    :goto_7
    if-eqz v17, :cond_9

    .line 1079
    :try_start_7
    invoke-interface/range {v17 .. v17}, Landroid_maps_conflict_avoidance/com/google/common/io/GoogleHttpConnection;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    .line 1087
    :cond_9
    :goto_8
    const/16 v20, 0x0

    :goto_9
    invoke-virtual/range {p1 .. p1}, Ljava/util/Vector;->size()I

    move-result v5

    move/from16 v0, v20

    if-ge v0, v5, :cond_21

    .line 1088
    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;

    .line 1090
    .restart local v13    # "dataRequest":Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;
    invoke-interface {v13}, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;->retryOnFailure()Z

    move-result v5

    if-nez v5, :cond_a

    .line 1091
    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Ljava/util/Vector;->removeElement(Ljava/lang/Object;)Z

    .line 1087
    :cond_a
    add-int/lit8 v20, v20, 0x1

    goto :goto_9

    .line 942
    .end local v13    # "dataRequest":Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;
    .restart local v6    # "startTime":J
    .restart local v12    # "contentType":Ljava/lang/String;
    .restart local v18    # "firstByteTime":J
    .restart local v29    # "responseCode":I
    :cond_b
    const/16 v4, 0xc8

    move/from16 v0, v29

    if-eq v0, v4, :cond_f

    .line 943
    :try_start_8
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Bad Response Code "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v29

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v15}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid_maps_conflict_avoidance/com/google/common/Log;->logToScreen(Ljava/lang/String;)V

    .line 946
    const/16 v4, 0x1f4

    move/from16 v0, v29

    if-ne v0, v4, :cond_e

    .line 951
    new-instance v14, Ljava/lang/StringBuffer;

    const-string v4, "Server 500 for request types: "

    invoke-direct {v14, v4}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 952
    .local v14, "debugMessage":Ljava/lang/StringBuffer;
    const/16 v21, 0x0

    .local v21, "index":I
    :goto_a
    invoke-virtual/range {p1 .. p1}, Ljava/util/Vector;->size()I

    move-result v4

    move/from16 v0, v21

    if-ge v0, v4, :cond_d

    .line 953
    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;

    .line 954
    .restart local v13    # "dataRequest":Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;
    invoke-interface {v13}, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;->onServerFailure()V

    .line 955
    invoke-interface {v13}, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;->getRequestType()I

    move-result v4

    invoke-virtual {v14, v4}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 956
    invoke-virtual/range {p1 .. p1}, Ljava/util/Vector;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    move/from16 v0, v21

    if-eq v0, v4, :cond_c

    .line 957
    const/16 v4, 0x2c

    invoke-virtual {v14, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 952
    :cond_c
    add-int/lit8 v21, v21, 0x1

    goto :goto_a

    .line 961
    .end local v13    # "dataRequest":Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;
    :cond_d
    move-object/from16 v0, p0

    iget-boolean v4, v0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->debug:Z

    if-eqz v4, :cond_e

    .line 962
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->connectionFactory:Landroid_maps_conflict_avoidance/com/google/common/io/HttpConnectionFactory;

    invoke-interface {v4}, Landroid_maps_conflict_avoidance/com/google/common/io/HttpConnectionFactory;->getNetworkWorked()Z

    move-result v23

    .line 963
    .local v23, "networkEverWorked":Z
    const/4 v4, 0x7

    invoke-virtual {v14}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-virtual {v0, v4, v1, v5}, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->notifyNetworkError(IZLjava/lang/String;)V

    .line 969
    .end local v14    # "debugMessage":Ljava/lang/StringBuffer;
    .end local v21    # "index":I
    .end local v23    # "networkEverWorked":Z
    :cond_e
    new-instance v4, Ljava/io/IOException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Bad HTTP response code: "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v29

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 971
    :cond_f
    const-string v4, "application/binary"

    invoke-virtual {v4, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_10

    .line 972
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Bad HTTP content type: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v15}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid_maps_conflict_avoidance/com/google/common/Log;->logToScreen(Ljava/lang/String;)V

    .line 977
    new-instance v4, Ljava/io/IOException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Bad HTTP content type: "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 979
    :cond_10
    invoke-interface/range {v17 .. v17}, Landroid_maps_conflict_avoidance/com/google/common/io/GoogleHttpConnection;->getLength()J

    move-result-wide v4

    long-to-int v0, v4

    move/from16 v30, v0

    .line 980
    .local v30, "responseLength":I
    move-object/from16 v0, p0

    iget v4, v0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->bytesReceived:I

    add-int v4, v4, v30

    move-object/from16 v0, p0

    iput v4, v0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->bytesReceived:I

    .line 982
    invoke-virtual/range {v22 .. v22}, Ljava/io/DataInputStream;->readUnsignedShort()I

    move-result v32

    .line 983
    .local v32, "serverProtocolVersion":I
    const/16 v4, 0x17

    move/from16 v0, v32

    if-eq v0, v4, :cond_15

    .line 985
    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->maybeNotifyNetworkError(I)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 1062
    invoke-virtual {v15}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid_maps_conflict_avoidance/com/google/common/Log;->logToScreen(Ljava/lang/String;)V

    .line 1064
    if-eqz v22, :cond_11

    .line 1065
    :try_start_9
    invoke-virtual/range {v22 .. v22}, Ljava/io/DataInputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_8

    .line 1071
    :cond_11
    :goto_b
    if-eqz v24, :cond_12

    if-nez v25, :cond_12

    .line 1072
    :try_start_a
    invoke-virtual/range {v24 .. v24}, Ljava/io/DataOutputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_9

    .line 1078
    :cond_12
    :goto_c
    if-eqz v17, :cond_13

    .line 1079
    :try_start_b
    invoke-interface/range {v17 .. v17}, Landroid_maps_conflict_avoidance/com/google/common/io/GoogleHttpConnection;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_a

    .line 1087
    :cond_13
    :goto_d
    const/16 v20, 0x0

    :goto_e
    invoke-virtual/range {p1 .. p1}, Ljava/util/Vector;->size()I

    move-result v4

    move/from16 v0, v20

    if-ge v0, v4, :cond_22

    .line 1088
    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;

    .line 1090
    .restart local v13    # "dataRequest":Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;
    invoke-interface {v13}, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;->retryOnFailure()Z

    move-result v4

    if-nez v4, :cond_14

    .line 1091
    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Ljava/util/Vector;->removeElement(Ljava/lang/Object;)Z

    .line 1087
    :cond_14
    add-int/lit8 v20, v20, 0x1

    goto :goto_e

    .line 991
    .end local v13    # "dataRequest":Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;
    :cond_15
    const/16 v21, 0x0

    .restart local v21    # "index":I
    :goto_f
    :try_start_c
    invoke-virtual/range {p1 .. p1}, Ljava/util/Vector;->size()I

    move-result v4

    move/from16 v0, v21

    if-ge v0, v4, :cond_1a

    .line 992
    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    .line 994
    .restart local v13    # "dataRequest":Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;
    :try_start_d
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, p2

    invoke-virtual {v0, v1, v13, v2}, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->processDataRequest(Ljava/io/DataInput;Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher$DispatcherServer;)V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_d .. :try_end_d} :catch_1
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    .line 991
    add-int/lit8 v21, v21, 0x1

    goto :goto_f

    .line 995
    :catch_0
    move-exception v16

    .line 996
    .local v16, "e":Ljava/io/IOException;
    :try_start_e
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "IOException: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v13}, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;->getRequestType()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid_maps_conflict_avoidance/com/google/common/Log;->logToScreen(Ljava/lang/String;)V

    .line 997
    move-object/from16 v0, p0

    iget-boolean v4, v0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->debug:Z

    if-eqz v4, :cond_16

    .line 998
    sget-object v4, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "IOException processing: "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v13}, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;->getRequestType()I

    move-result v8

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 999
    invoke-virtual/range {v16 .. v16}, Ljava/io/IOException;->printStackTrace()V

    .line 1002
    :cond_16
    move-object/from16 v0, v16

    instance-of v4, v0, Ljava/io/EOFException;

    if-eqz v4, :cond_17

    .line 1006
    invoke-interface {v13}, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;->onServerFailure()V

    .line 1008
    move-object/from16 v0, p0

    iget-boolean v4, v0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->debug:Z

    if-eqz v4, :cond_17

    .line 1009
    invoke-interface {v13}, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;->getRequestType()I

    move-result v28

    .line 1010
    .local v28, "requestType":I
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "No server support for data request: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v28

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 1011
    .local v14, "debugMessage":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->connectionFactory:Landroid_maps_conflict_avoidance/com/google/common/io/HttpConnectionFactory;

    invoke-interface {v4}, Landroid_maps_conflict_avoidance/com/google/common/io/HttpConnectionFactory;->getNetworkWorked()Z

    move-result v23

    .line 1012
    .restart local v23    # "networkEverWorked":Z
    const/4 v4, 0x7

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-virtual {v0, v4, v1, v14}, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->notifyNetworkError(IZLjava/lang/String;)V

    .line 1018
    .end local v14    # "debugMessage":Ljava/lang/String;
    .end local v23    # "networkEverWorked":Z
    .end local v28    # "requestType":I
    :cond_17
    const/16 v20, 0x0

    :goto_10
    move/from16 v0, v20

    move/from16 v1, v21

    if-ge v0, v1, :cond_18

    .line 1019
    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Ljava/util/Vector;->removeElementAt(I)V

    .line 1018
    add-int/lit8 v20, v20, 0x1

    goto :goto_10

    .line 1021
    :cond_18
    throw v16

    .line 1022
    .end local v16    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v16

    .line 1023
    .local v16, "e":Ljava/lang/RuntimeException;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "RuntimeException: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v13}, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;->getRequestType()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid_maps_conflict_avoidance/com/google/common/Log;->logToScreen(Ljava/lang/String;)V

    .line 1024
    move-object/from16 v0, p0

    iget-boolean v4, v0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->debug:Z

    if-eqz v4, :cond_19

    .line 1025
    sget-object v4, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "RuntimeException processing: "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v13}, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;->getRequestType()I

    move-result v8

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1026
    invoke-virtual/range {v16 .. v16}, Ljava/lang/RuntimeException;->printStackTrace()V

    .line 1028
    :cond_19
    throw v16

    .line 1031
    .end local v13    # "dataRequest":Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;
    .end local v16    # "e":Ljava/lang/RuntimeException;
    :cond_1a
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->clock:Landroid_maps_conflict_avoidance/com/google/common/Clock;

    invoke-interface {v4}, Landroid_maps_conflict_avoidance/com/google/common/Clock;->relativeTimeMillis()J

    move-result-wide v26

    .line 1032
    .local v26, "readEndTime":J
    sub-long v4, v26, v6

    long-to-int v9, v4

    .line 1034
    .local v9, "elapsedTime":I
    const/16 v4, 0x16

    const-string v5, "fb"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v33, ""

    move-object/from16 v0, v33

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-wide/from16 v0, v18

    invoke-virtual {v8, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v4, v5, v8}, Landroid_maps_conflict_avoidance/com/google/common/Log;->addEvent(SLjava/lang/String;Ljava/lang/String;)Z

    .line 1036
    const/16 v4, 0x16

    const-string v5, "lb"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v33, ""

    move-object/from16 v0, v33

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v4, v5, v8}, Landroid_maps_conflict_avoidance/com/google/common/Log;->addEvent(SLjava/lang/String;Ljava/lang/String;)Z

    .line 1039
    move-object/from16 v0, p0

    iget-object v4, v0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->warmUpManager:Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/ConnectionWarmUpManager;

    move-wide/from16 v0, v18

    long-to-int v8, v0

    move-object/from16 v5, p1

    invoke-virtual/range {v4 .. v9}, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/ConnectionWarmUpManager;->onFinishServiceRequests(Ljava/lang/Object;JII)V

    .line 1043
    const/16 v4, 0x2000

    move/from16 v0, v30

    if-lt v0, v4, :cond_1b

    int-to-long v4, v9

    const-wide/32 v34, 0xea60

    cmp-long v4, v4, v34

    if-gtz v4, :cond_1b

    .line 1045
    move/from16 v0, v30

    mul-int/lit16 v4, v0, 0x3e8

    div-int/2addr v4, v9

    move-object/from16 v0, p0

    iput v4, v0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->networkSpeedBytesPerSecond:I

    .line 1051
    :cond_1b
    const-string v4, ", "

    invoke-virtual {v15, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1052
    const/16 v4, 0x3e8

    move/from16 v0, v30

    if-ge v0, v4, :cond_20

    .line 1053
    const-string v4, "<1kb"

    invoke-virtual {v15, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1058
    :goto_11
    invoke-virtual/range {p1 .. p1}, Ljava/util/Vector;->removeAllElements()V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    .line 1062
    invoke-virtual {v15}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid_maps_conflict_avoidance/com/google/common/Log;->logToScreen(Ljava/lang/String;)V

    .line 1064
    if-eqz v22, :cond_1c

    .line 1065
    :try_start_f
    invoke-virtual/range {v22 .. v22}, Ljava/io/DataInputStream;->close()V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_b

    .line 1071
    :cond_1c
    :goto_12
    if-eqz v24, :cond_1d

    if-nez v25, :cond_1d

    .line 1072
    :try_start_10
    invoke-virtual/range {v24 .. v24}, Ljava/io/DataOutputStream;->close()V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_c

    .line 1078
    :cond_1d
    :goto_13
    if-eqz v17, :cond_1e

    .line 1079
    :try_start_11
    invoke-interface/range {v17 .. v17}, Landroid_maps_conflict_avoidance/com/google/common/io/GoogleHttpConnection;->close()V
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_d

    .line 1087
    :cond_1e
    :goto_14
    const/16 v20, 0x0

    :goto_15
    invoke-virtual/range {p1 .. p1}, Ljava/util/Vector;->size()I

    move-result v4

    move/from16 v0, v20

    if-ge v0, v4, :cond_22

    .line 1088
    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;

    .line 1090
    .restart local v13    # "dataRequest":Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;
    invoke-interface {v13}, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;->retryOnFailure()Z

    move-result v4

    if-nez v4, :cond_1f

    .line 1091
    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Ljava/util/Vector;->removeElement(Ljava/lang/Object;)Z

    .line 1087
    :cond_1f
    add-int/lit8 v20, v20, 0x1

    goto :goto_15

    .line 1055
    .end local v13    # "dataRequest":Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;
    :cond_20
    :try_start_12
    move/from16 v0, v30

    div-int/lit16 v4, v0, 0x3e8

    invoke-virtual {v15, v4}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, "kb"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_0

    goto :goto_11

    .line 1062
    .end local v6    # "startTime":J
    .end local v9    # "elapsedTime":I
    .end local v12    # "contentType":Ljava/lang/String;
    .end local v18    # "firstByteTime":J
    .end local v21    # "index":I
    .end local v26    # "readEndTime":J
    .end local v29    # "responseCode":I
    .end local v30    # "responseLength":I
    .end local v32    # "serverProtocolVersion":I
    :cond_21
    throw v4

    .line 1096
    .restart local v6    # "startTime":J
    .restart local v12    # "contentType":Ljava/lang/String;
    .restart local v18    # "firstByteTime":J
    .restart local v29    # "responseCode":I
    :cond_22
    return-void

    .line 1067
    .end local v6    # "startTime":J
    .end local v12    # "contentType":Ljava/lang/String;
    .end local v18    # "firstByteTime":J
    .end local v29    # "responseCode":I
    :catch_2
    move-exception v5

    goto/16 :goto_6

    .line 1074
    :catch_3
    move-exception v5

    goto/16 :goto_7

    .line 1082
    :catch_4
    move-exception v5

    goto/16 :goto_8

    .line 1067
    .restart local v6    # "startTime":J
    .restart local v12    # "contentType":Ljava/lang/String;
    .restart local v18    # "firstByteTime":J
    .restart local v29    # "responseCode":I
    :catch_5
    move-exception v4

    goto/16 :goto_2

    .line 1074
    :catch_6
    move-exception v4

    goto/16 :goto_3

    .line 1082
    :catch_7
    move-exception v4

    goto/16 :goto_4

    .line 1067
    .restart local v30    # "responseLength":I
    .restart local v32    # "serverProtocolVersion":I
    :catch_8
    move-exception v4

    goto/16 :goto_b

    .line 1074
    :catch_9
    move-exception v4

    goto/16 :goto_c

    .line 1082
    :catch_a
    move-exception v4

    goto/16 :goto_d

    .line 1067
    .restart local v9    # "elapsedTime":I
    .restart local v21    # "index":I
    .restart local v26    # "readEndTime":J
    :catch_b
    move-exception v4

    goto :goto_12

    .line 1074
    :catch_c
    move-exception v4

    goto :goto_13

    .line 1082
    :catch_d
    move-exception v4

    goto :goto_14
.end method

.method public setAndroidLoggingId2(Ljava/lang/String;)V
    .locals 2
    .param p1, "androidLoggingId2"    # Ljava/lang/String;

    .prologue
    .line 1337
    iget-object v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->properties:Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;

    const/16 v1, 0x13

    invoke-virtual {v0, v1, p1}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    .line 1339
    return-void
.end method

.method public setAndroidMapKey(Ljava/lang/String;)V
    .locals 2
    .param p1, "mapKey"    # Ljava/lang/String;

    .prologue
    .line 1328
    iget-object v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->properties:Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;

    const/16 v1, 0x11

    invoke-virtual {v0, v1, p1}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    .line 1329
    return-void
.end method

.method public setAndroidSignature(Ljava/lang/String;)V
    .locals 2
    .param p1, "signature"    # Ljava/lang/String;

    .prologue
    .line 1332
    iget-object v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->properties:Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;

    const/16 v1, 0x12

    invoke-virtual {v0, v1, p1}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    .line 1334
    return-void
.end method

.method public setApplicationName(Ljava/lang/String;)V
    .locals 2
    .param p1, "applicationName"    # Ljava/lang/String;

    .prologue
    .line 1342
    iget-object v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->properties:Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;

    const/4 v1, 0x5

    invoke-virtual {v0, v1, p1}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->setString(ILjava/lang/String;)V

    .line 1344
    return-void
.end method

.method protected declared-synchronized snapshotListeners()[Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestListener;
    .locals 2

    .prologue
    .line 531
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->listeners:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    new-array v0, v1, [Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestListener;

    .line 532
    .local v0, "listenersArray":[Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestListener;
    iget-object v1, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->listeners:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->copyInto([Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 533
    monitor-exit p0

    return-object v0

    .line 531
    .end local v0    # "listenersArray":[Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestListener;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public start()V
    .locals 2

    .prologue
    .line 759
    const/4 v1, 0x1

    iput-boolean v1, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->active:Z

    .line 760
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->thirdPartyServers:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 761
    iget-object v1, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->thirdPartyServers:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher$DispatcherServer;

    invoke-virtual {v1}, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher$DispatcherServer;->start()V

    .line 760
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 763
    :cond_0
    iget-object v1, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->defaultServer:Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher$DispatcherServer;

    invoke-virtual {v1}, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher$DispatcherServer;->start()V

    .line 764
    return-void
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 755
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->active:Z

    .line 756
    return-void
.end method

.method protected urlArguments(Ljava/util/Vector;)Ljava/lang/String;
    .locals 7
    .param p1, "requests"    # Ljava/util/Vector;

    .prologue
    .line 1265
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 1266
    .local v1, "addToUrl":Ljava/lang/StringBuffer;
    const-string v5, "?"

    .line 1267
    .local v5, "separator":Ljava/lang/String;
    iget-object v6, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->globalSpecialUrlArguments:Ljava/lang/String;

    invoke-static {v6}, Landroid_maps_conflict_avoidance/com/google/common/util/text/TextUtil;->isEmpty(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 1268
    invoke-virtual {v1, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1269
    iget-object v6, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->globalSpecialUrlArguments:Ljava/lang/String;

    invoke-virtual {v1, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1270
    const-string v5, "&"

    .line 1272
    :cond_0
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/util/Vector;->size()I

    move-result v6

    if-ge v2, v6, :cond_2

    .line 1273
    invoke-virtual {p1, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;

    .line 1274
    .local v4, "request":Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;
    instance-of v6, v4, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/NeedsSpecialUrl;

    if-eqz v6, :cond_1

    .line 1275
    check-cast v4, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/NeedsSpecialUrl;

    .end local v4    # "request":Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;
    invoke-interface {v4}, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/NeedsSpecialUrl;->getParams()Ljava/lang/String;

    move-result-object v3

    .line 1276
    .local v3, "param":Ljava/lang/String;
    invoke-static {v3}, Landroid_maps_conflict_avoidance/com/google/common/util/text/TextUtil;->isEmpty(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 1277
    invoke-virtual {v1, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1278
    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1279
    const-string v5, "&"

    .line 1272
    .end local v3    # "param":Ljava/lang/String;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1283
    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1284
    .local v0, "addString":Ljava/lang/String;
    invoke-static {v0}, Landroid_maps_conflict_avoidance/com/google/common/util/text/TextUtil;->isEmpty(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 1287
    :cond_3
    return-object v0
.end method
