.class public Landroid_maps_conflict_avoidance/com/google/googlenav/AddressUtil;
.super Ljava/lang/Object;
.source "AddressUtil.java"


# direct methods
.method public static getAddressLine(IILandroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;)Ljava/lang/String;
    .locals 1
    .param p0, "addressLineIndex"    # I
    .param p1, "number"    # I
    .param p2, "proto"    # Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;

    .prologue
    .line 26
    if-nez p2, :cond_0

    .line 27
    const-string v0, ""

    .line 29
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p2, p0}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    if-le v0, p1, :cond_1

    invoke-virtual {p2, p0, p1}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->getString(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const-string v0, ""

    goto :goto_0
.end method
