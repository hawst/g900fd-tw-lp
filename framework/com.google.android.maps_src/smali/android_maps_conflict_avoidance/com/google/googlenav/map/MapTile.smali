.class public Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;
.super Ljava/lang/Object;
.source "MapTile.java"


# static fields
.field private static final CJPG_HEADER:[B

.field protected static final LAYER_DATA_HEADER:[B

.field private static loadingImage:Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;

.field private static notAvailableImage:Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;

.field private static notLoadingImage:Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;

.field private static textSize:I

.field private static final unicolorTiles:Ljava/util/Hashtable;


# instance fields
.field private baseMapImage:Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;

.field private completePaintCount:I

.field private data:[B

.field private firstPaintTime:J

.field private hasScaledImage:Z

.field private imageVersion:I

.field private isBaseMapImageRecyclable:Z

.field private isMapImageRecyclable:Z

.field private isPreCached:Z

.field private final isTemp:Z

.field private lastAccessTime:J

.field private lastPaintTime:J

.field private layerTile:Landroid_maps_conflict_avoidance/com/google/googlenav/map/LayerTile;

.field private final location:Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;

.field private mapImage:Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;

.field private requested:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    sput-object v0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->unicolorTiles:Ljava/util/Hashtable;

    .line 69
    const/4 v0, 0x1

    sput v0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->textSize:I

    .line 72
    const/4 v0, 0x4

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->CJPG_HEADER:[B

    .line 76
    const/4 v0, 0x5

    new-array v0, v0, [B

    fill-array-data v0, :array_1

    sput-object v0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->LAYER_DATA_HEADER:[B

    return-void

    .line 72
    nop

    :array_0
    .array-data 1
        0x43t
        0x4at
        0x50t
        0x47t
    .end array-data

    .line 76
    :array_1
    .array-data 1
        0x4ct
        0x54t
        0x49t
        0x50t
        0xat
    .end array-data
.end method

.method public constructor <init>(Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;)V
    .locals 1
    .param p1, "location"    # Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;
    .param p2, "tempImage"    # Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;

    .prologue
    .line 223
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;-><init>(Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;Z)V

    .line 224
    return-void
.end method

.method public constructor <init>(Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;Z)V
    .locals 4
    .param p1, "location"    # Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;
    .param p2, "tempImage"    # Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;
    .param p3, "isTemp"    # Z

    .prologue
    const/4 v1, 0x0

    .line 239
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 181
    iput-boolean v1, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->requested:Z

    .line 189
    iput-boolean v1, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->isMapImageRecyclable:Z

    .line 190
    iput-boolean v1, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->isBaseMapImageRecyclable:Z

    .line 240
    invoke-static {p2}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->verifyTileDimensions(Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;)V

    .line 241
    iput-object p1, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->location:Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;

    .line 242
    iput-boolean p3, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->isTemp:Z

    .line 243
    invoke-direct {p0, p2, v1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->setMapImage(Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;Z)V

    .line 244
    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->hasScaledImage:Z

    .line 245
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->lastAccessTime:J

    .line 246
    iput v1, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->imageVersion:I

    .line 247
    return-void

    :cond_0
    move v0, v1

    .line 244
    goto :goto_0
.end method

.method public constructor <init>(Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;[B)V
    .locals 3
    .param p1, "location"    # Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;
    .param p2, "imageData"    # [B

    .prologue
    const/4 v2, 0x0

    .line 255
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 181
    iput-boolean v2, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->requested:Z

    .line 189
    iput-boolean v2, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->isMapImageRecyclable:Z

    .line 190
    iput-boolean v2, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->isBaseMapImageRecyclable:Z

    .line 256
    iput-object p1, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->location:Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;

    .line 257
    iput-boolean v2, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->isTemp:Z

    .line 258
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->lastAccessTime:J

    .line 259
    iput-boolean v2, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->hasScaledImage:Z

    .line 260
    invoke-virtual {p0, p2}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->setData([B)V

    .line 261
    return-void
.end method

.method private static createTempImages()V
    .locals 4

    .prologue
    const/16 v3, 0x100

    .line 713
    invoke-static {}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getInstance()Landroid_maps_conflict_avoidance/com/google/common/Config;

    move-result-object v2

    invoke-virtual {v2}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getImageFactory()Landroid_maps_conflict_avoidance/com/google/common/graphics/ImageFactory;

    move-result-object v1

    .line 720
    .local v1, "factory":Landroid_maps_conflict_avoidance/com/google/common/graphics/ImageFactory;
    :try_start_0
    const-string v2, "/loading_tile_android.png"

    invoke-interface {v1, v2}, Landroid_maps_conflict_avoidance/com/google/common/graphics/ImageFactory;->createUnscaledImage(Ljava/lang/String;)Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;

    move-result-object v2

    sput-object v2, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->loadingImage:Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;

    .line 721
    sget-object v2, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->loadingImage:Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;

    sput-object v2, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->notLoadingImage:Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 734
    :goto_0
    return-void

    .line 728
    :catch_0
    move-exception v0

    .line 731
    .local v0, "e":Ljava/io/IOException;
    const/4 v2, 0x0

    invoke-interface {v1, v3, v3, v2}, Landroid_maps_conflict_avoidance/com/google/common/graphics/ImageFactory;->createImage(IIZ)Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;

    move-result-object v2

    sput-object v2, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->notLoadingImage:Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;

    sput-object v2, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->loadingImage:Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;

    .line 732
    const-string v2, "MAP"

    invoke-static {v2, v0}, Landroid_maps_conflict_avoidance/com/google/common/Log;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private static equalBytes([BI[B)Z
    .locals 4
    .param p0, "src"    # [B
    .param p1, "srcOffset"    # I
    .param p2, "reference"    # [B

    .prologue
    const/4 v1, 0x0

    .line 741
    array-length v2, p0

    array-length v3, p2

    add-int/2addr v3, p1

    if-ge v2, v3, :cond_1

    .line 749
    :cond_0
    :goto_0
    return v1

    .line 744
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v2, p2

    if-ge v0, v2, :cond_2

    .line 745
    add-int v2, p1, v0

    aget-byte v2, p0, v2

    aget-byte v3, p2, v0

    if-ne v2, v3, :cond_0

    .line 744
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 749
    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private extractLayerTileAndImageData()[B
    .locals 14

    .prologue
    const/4 v13, 0x0

    .line 402
    iget-object v3, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->data:[B

    .line 406
    .local v3, "imageData":[B
    iget-object v11, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->data:[B

    sget-object v12, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->LAYER_DATA_HEADER:[B

    invoke-static {v11, v13, v12}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->equalBytes([BI[B)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 408
    :try_start_0
    sget-object v11, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->LAYER_DATA_HEADER:[B

    array-length v7, v11

    .line 410
    .local v7, "offset":I
    const/4 v11, 0x4

    new-array v9, v11, [B

    .line 411
    .local v9, "sizeBytes":[B
    iget-object v11, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->data:[B

    const/4 v12, 0x0

    array-length v13, v9

    invoke-static {v11, v7, v9, v12, v13}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 413
    invoke-static {v9}, Landroid_maps_conflict_avoidance/com/google/common/util/ConversionUtil;->byteArrayToInt([B)I

    move-result v8

    .line 414
    .local v8, "size":I
    invoke-static {v8}, Ljava/lang/Math;->abs(I)I

    move-result v6

    .line 416
    .local v6, "layerDataSize":I
    add-int/lit8 v7, v7, 0x4

    .line 417
    new-instance v4, Ljava/io/ByteArrayInputStream;

    iget-object v11, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->data:[B

    invoke-direct {v4, v11, v7, v6}, Ljava/io/ByteArrayInputStream;-><init>([BII)V

    .line 419
    .local v4, "is":Ljava/io/InputStream;
    add-int/2addr v7, v6

    .line 420
    iget-object v11, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->data:[B

    array-length v11, v11

    sub-int/2addr v11, v7

    new-array v3, v11, [B

    .line 421
    iget-object v11, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->data:[B

    const/4 v12, 0x0

    array-length v13, v3

    invoke-static {v11, v7, v3, v12, v13}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 423
    if-gez v8, :cond_0

    .line 424
    invoke-static {v4}, Landroid_maps_conflict_avoidance/com/google/common/io/Gunzipper;->gunzip(Ljava/io/InputStream;)Ljava/io/InputStream;

    move-result-object v4

    .line 428
    :cond_0
    new-instance v10, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;

    sget-object v11, Landroid_maps_conflict_avoidance/com/google/googlenav/proto/GmmMessageTypes;->LAYER_TILE_INFO_PROTO:Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBufType;

    invoke-direct {v10, v11}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;-><init>(Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBufType;)V

    .line 429
    .local v10, "tileInfo":Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;
    invoke-virtual {v10, v4}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->parse(Ljava/io/InputStream;)Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;

    .line 432
    const/4 v11, 0x3

    invoke-virtual {v10, v11}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v1

    .line 433
    .local v1, "areasNum":I
    new-array v0, v1, [Landroid_maps_conflict_avoidance/com/google/googlenav/layer/ClickableArea;

    .line 434
    .local v0, "areas":[Landroid_maps_conflict_avoidance/com/google/googlenav/layer/ClickableArea;
    const/4 v5, 0x0

    .local v5, "j":I
    :goto_0
    if-ge v5, v1, :cond_1

    .line 435
    new-instance v11, Landroid_maps_conflict_avoidance/com/google/googlenav/layer/ClickableArea;

    const/4 v12, 0x3

    invoke-virtual {v10, v12, v5}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->getProtoBuf(II)Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;

    move-result-object v12

    invoke-direct {v11, v12}, Landroid_maps_conflict_avoidance/com/google/googlenav/layer/ClickableArea;-><init>(Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;)V

    aput-object v11, v0, v5

    .line 434
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 439
    :cond_1
    new-instance v11, Landroid_maps_conflict_avoidance/com/google/googlenav/map/LayerTile;

    iget-object v12, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->location:Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;

    invoke-direct {v11, v12}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/LayerTile;-><init>(Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;)V

    iput-object v11, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->layerTile:Landroid_maps_conflict_avoidance/com/google/googlenav/map/LayerTile;

    .line 440
    iget-object v11, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->layerTile:Landroid_maps_conflict_avoidance/com/google/googlenav/map/LayerTile;

    invoke-virtual {v11, v0}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/LayerTile;->setLayerTileData([Landroid_maps_conflict_avoidance/com/google/googlenav/layer/ClickableArea;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 446
    .end local v0    # "areas":[Landroid_maps_conflict_avoidance/com/google/googlenav/layer/ClickableArea;
    .end local v1    # "areasNum":I
    .end local v4    # "is":Ljava/io/InputStream;
    .end local v5    # "j":I
    .end local v6    # "layerDataSize":I
    .end local v7    # "offset":I
    .end local v8    # "size":I
    .end local v9    # "sizeBytes":[B
    .end local v10    # "tileInfo":Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;
    :cond_2
    :goto_1
    return-object v3

    .line 441
    :catch_0
    move-exception v2

    .line 442
    .local v2, "e":Ljava/io/IOException;
    const/4 v11, 0x0

    iput-object v11, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->layerTile:Landroid_maps_conflict_avoidance/com/google/googlenav/map/LayerTile;

    .line 443
    const-string v11, "IOException reading layer data"

    invoke-static {v11, v2}, Landroid_maps_conflict_avoidance/com/google/common/Log;->logQuietThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method private static getImageFromCjpg([B)Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;
    .locals 4
    .param p0, "data"    # [B

    .prologue
    .line 810
    :try_start_0
    invoke-static {p0}, Landroid_maps_conflict_avoidance/com/google/image/compression/jpeg/JpegUtil;->uncompactJpeg([B)[B

    move-result-object p0

    .line 811
    invoke-static {}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getInstance()Landroid_maps_conflict_avoidance/com/google/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getImageFactory()Landroid_maps_conflict_avoidance/com/google/common/graphics/ImageFactory;

    move-result-object v1

    const/4 v2, 0x0

    array-length v3, p0

    invoke-interface {v1, p0, v2, v3}, Landroid_maps_conflict_avoidance/com/google/common/graphics/ImageFactory;->createImage([BII)Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 815
    :goto_0
    return-object v1

    .line 813
    :catch_0
    move-exception v0

    .line 814
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v1, "MAP"

    invoke-static {v1, v0}, Landroid_maps_conflict_avoidance/com/google/common/Log;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 815
    invoke-static {}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->getNotAvailableImage()Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;

    move-result-object v1

    goto :goto_0
.end method

.method private static getImageFromUnicolor([B)Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;
    .locals 14
    .param p0, "unicolorData"    # [B

    .prologue
    const/16 v13, 0x100

    const/4 v12, 0x0

    .line 777
    array-length v10, p0

    const/4 v11, 0x3

    if-ge v10, v11, :cond_1

    .line 779
    invoke-static {}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->getNotAvailableImage()Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;

    move-result-object v6

    .line 799
    :cond_0
    :goto_0
    return-object v6

    .line 781
    :cond_1
    aget-byte v10, p0, v12

    and-int/lit16 v8, v10, 0xff

    .line 782
    .local v8, "red":I
    const/4 v10, 0x1

    aget-byte v10, p0, v10

    and-int/lit16 v5, v10, 0xff

    .line 783
    .local v5, "green":I
    const/4 v10, 0x2

    aget-byte v10, p0, v10

    and-int/lit16 v0, v10, 0xff

    .line 784
    .local v0, "blue":I
    shl-int/lit8 v10, v8, 0x10

    shl-int/lit8 v11, v5, 0x8

    or-int/2addr v10, v11

    or-int v1, v10, v0

    .line 785
    .local v1, "color":I
    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, v1}, Ljava/lang/Integer;-><init>(I)V

    .line 786
    .local v2, "colorKey":Ljava/lang/Integer;
    sget-object v10, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->unicolorTiles:Ljava/util/Hashtable;

    invoke-virtual {v10, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/ref/WeakReference;

    .line 787
    .local v9, "ref":Ljava/lang/ref/WeakReference;
    if-eqz v9, :cond_2

    .line 788
    invoke-virtual {v9}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;

    .line 789
    .local v6, "image":Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;
    if-nez v6, :cond_0

    .line 793
    .end local v6    # "image":Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;
    :cond_2
    invoke-static {}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getInstance()Landroid_maps_conflict_avoidance/com/google/common/Config;

    move-result-object v10

    invoke-virtual {v10}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getImageFactory()Landroid_maps_conflict_avoidance/com/google/common/graphics/ImageFactory;

    move-result-object v3

    .line 794
    .local v3, "factory":Landroid_maps_conflict_avoidance/com/google/common/graphics/ImageFactory;
    invoke-interface {v3, v13, v13, v12}, Landroid_maps_conflict_avoidance/com/google/common/graphics/ImageFactory;->createImage(IIZ)Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;

    move-result-object v7

    .line 795
    .local v7, "img":Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;
    invoke-interface {v7}, Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;->getGraphics()Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleGraphics;

    move-result-object v4

    .line 796
    .local v4, "gcs":Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleGraphics;
    invoke-interface {v4, v1}, Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleGraphics;->setColor(I)V

    .line 797
    invoke-interface {v4, v12, v12, v13, v13}, Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleGraphics;->fillRect(IIII)V

    .line 798
    sget-object v10, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->unicolorTiles:Ljava/util/Hashtable;

    new-instance v11, Ljava/lang/ref/WeakReference;

    invoke-direct {v11, v7}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v10, v2, v11}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v6, v7

    .line 799
    goto :goto_0
.end method

.method private static getNotAvailableImage()Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;
    .locals 4

    .prologue
    const/16 v3, 0x100

    .line 757
    invoke-static {}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getInstance()Landroid_maps_conflict_avoidance/com/google/common/Config;

    move-result-object v2

    invoke-virtual {v2}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getImageFactory()Landroid_maps_conflict_avoidance/com/google/common/graphics/ImageFactory;

    move-result-object v1

    .line 758
    .local v1, "factory":Landroid_maps_conflict_avoidance/com/google/common/graphics/ImageFactory;
    sget-object v2, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->notAvailableImage:Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;

    if-nez v2, :cond_0

    .line 761
    :try_start_0
    const-string v2, "/no_tile_256.png"

    invoke-interface {v1, v2}, Landroid_maps_conflict_avoidance/com/google/common/graphics/ImageFactory;->createUnscaledImage(Ljava/lang/String;)Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;

    move-result-object v2

    sput-object v2, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->notAvailableImage:Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 770
    :cond_0
    :goto_0
    sget-object v2, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->notAvailableImage:Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;

    return-object v2

    .line 763
    :catch_0
    move-exception v0

    .line 765
    .local v0, "e":Ljava/io/IOException;
    invoke-interface {v1, v3, v3}, Landroid_maps_conflict_avoidance/com/google/common/graphics/ImageFactory;->createImage(II)Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;

    move-result-object v2

    sput-object v2, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->notAvailableImage:Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;

    .line 766
    const-string v2, "MAP"

    invoke-static {v2, v0}, Landroid_maps_conflict_avoidance/com/google/common/Log;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private getTempImage()Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;
    .locals 1

    .prologue
    .line 621
    sget-object v0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->notLoadingImage:Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;

    if-eqz v0, :cond_0

    sget-object v0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->loadingImage:Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;

    if-nez v0, :cond_1

    .line 622
    :cond_0
    invoke-static {}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->createTempImages()V

    .line 625
    :cond_1
    iget-boolean v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->isTemp:Z

    if-eqz v0, :cond_2

    sget-object v0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->notLoadingImage:Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;

    :goto_0
    return-object v0

    :cond_2
    sget-object v0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->loadingImage:Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;

    goto :goto_0
.end method

.method public static getTextSize()I
    .locals 1

    .prologue
    .line 851
    sget v0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->textSize:I

    return v0
.end method

.method public static read(Ljava/io/DataInput;)Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;
    .locals 4
    .param p0, "dis"    # Ljava/io/DataInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 651
    invoke-static {p0}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;->read(Ljava/io/DataInput;)Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;

    move-result-object v2

    .line 652
    .local v2, "location":Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;
    invoke-interface {p0}, Ljava/io/DataInput;->readUnsignedShort()I

    move-result v1

    .line 653
    .local v1, "length":I
    new-array v0, v1, [B

    .line 654
    .local v0, "data":[B
    invoke-interface {p0, v0}, Ljava/io/DataInput;->readFully([B)V

    .line 656
    new-instance v3, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;

    invoke-direct {v3, v2, v0}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;-><init>(Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;[B)V

    return-object v3
.end method

.method private setBaseMapImage(Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;Z)V
    .locals 1
    .param p1, "image"    # Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;
    .param p2, "isRecyclable"    # Z

    .prologue
    .line 502
    iget-object v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->baseMapImage:Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->isBaseMapImageRecyclable:Z

    if-eqz v0, :cond_0

    .line 503
    iget-object v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->baseMapImage:Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;

    invoke-interface {v0}, Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;->recycle()V

    .line 505
    :cond_0
    iput-object p1, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->baseMapImage:Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;

    .line 506
    iput-boolean p2, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->isBaseMapImageRecyclable:Z

    .line 507
    return-void
.end method

.method private setMapImage(Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;Z)V
    .locals 1
    .param p1, "image"    # Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;
    .param p2, "isRecyclable"    # Z

    .prologue
    .line 489
    iget-object v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->mapImage:Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->isMapImageRecyclable:Z

    if-eqz v0, :cond_0

    .line 490
    iget-object v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->mapImage:Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;

    invoke-interface {v0}, Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;->recycle()V

    .line 492
    :cond_0
    iput-object p1, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->mapImage:Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;

    .line 493
    iput-boolean p2, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->isMapImageRecyclable:Z

    .line 494
    return-void
.end method

.method public static setTextSize(I)V
    .locals 0
    .param p0, "desiredTextSize"    # I

    .prologue
    .line 843
    sput p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->textSize:I

    .line 844
    return-void
.end method

.method private static verifyTileDimensions(Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;)V
    .locals 3
    .param p0, "image"    # Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;

    .prologue
    const/16 v1, 0x100

    .line 515
    if-eqz p0, :cond_1

    invoke-interface {p0}, Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;->getHeight()I

    move-result v0

    if-ne v0, v1, :cond_0

    invoke-interface {p0}, Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;->getWidth()I

    move-result v0

    if-eq v0, v1, :cond_1

    .line 517
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "wrong image size: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p0}, Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;->getWidth()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p0}, Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;->getHeight()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 520
    :cond_1
    return-void
.end method


# virtual methods
.method public declared-synchronized compact()V
    .locals 2

    .prologue
    .line 283
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->isComplete()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->getRequested()Z

    move-result v0

    if-nez v0, :cond_3

    .line 284
    :cond_0
    iget-object v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->mapImage:Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;

    if-eqz v0, :cond_1

    .line 285
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->setMapImage(Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;Z)V

    .line 287
    :cond_1
    iget-object v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->baseMapImage:Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;

    if-eqz v0, :cond_2

    .line 288
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->setBaseMapImage(Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;Z)V

    .line 290
    :cond_2
    const/4 v0, 0x0

    iput v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->imageVersion:I

    .line 291
    const/4 v0, 0x0

    iput-object v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->layerTile:Landroid_maps_conflict_avoidance/com/google/googlenav/map/LayerTile;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 293
    :cond_3
    monitor-exit p0

    return-void

    .line 283
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized createImage()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 365
    monitor-enter p0

    :try_start_0
    iget-object v3, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->mapImage:Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;

    if-eqz v3, :cond_0

    iget-boolean v3, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->hasScaledImage:Z

    if-eqz v3, :cond_1

    .line 367
    :cond_0
    invoke-direct {p0}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->extractLayerTileAndImageData()[B

    move-result-object v1

    .line 375
    .local v1, "imageData":[B
    array-length v3, v1

    if-nez v3, :cond_2

    .line 376
    invoke-static {}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->getNotAvailableImage()Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;

    move-result-object v0

    .line 377
    .local v0, "createdImage":Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;
    const/4 v2, 0x0

    .line 389
    .local v2, "isRecyclable":Z
    :goto_0
    const/4 v3, 0x0

    invoke-virtual {p0, v0, v3, v2}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->setImage(Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;IZ)V

    .line 391
    const/4 v3, 0x0

    iput-boolean v3, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->hasScaledImage:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 393
    .end local v0    # "createdImage":Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;
    .end local v1    # "imageData":[B
    .end local v2    # "isRecyclable":Z
    :cond_1
    monitor-exit p0

    return-void

    .line 378
    .restart local v1    # "imageData":[B
    :cond_2
    :try_start_1
    array-length v3, v1

    const/4 v4, 0x3

    if-ne v3, v4, :cond_3

    .line 379
    invoke-static {v1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->getImageFromUnicolor([B)Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;

    move-result-object v0

    .line 380
    .restart local v0    # "createdImage":Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;
    const/4 v2, 0x0

    .restart local v2    # "isRecyclable":Z
    goto :goto_0

    .line 381
    .end local v0    # "createdImage":Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;
    .end local v2    # "isRecyclable":Z
    :cond_3
    const/4 v3, 0x0

    sget-object v4, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->CJPG_HEADER:[B

    invoke-static {v1, v3, v4}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->equalBytes([BI[B)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 382
    invoke-static {v1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->getImageFromCjpg([B)Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;

    move-result-object v0

    .line 383
    .restart local v0    # "createdImage":Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;
    sget-object v3, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->notAvailableImage:Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;

    if-eq v0, v3, :cond_4

    const/4 v2, 0x1

    .restart local v2    # "isRecyclable":Z
    :cond_4
    goto :goto_0

    .line 385
    .end local v0    # "createdImage":Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;
    .end local v2    # "isRecyclable":Z
    :cond_5
    invoke-static {}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getInstance()Landroid_maps_conflict_avoidance/com/google/common/Config;

    move-result-object v3

    invoke-virtual {v3}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getImageFactory()Landroid_maps_conflict_avoidance/com/google/common/graphics/ImageFactory;

    move-result-object v3

    const/4 v4, 0x0

    array-length v5, v1

    invoke-interface {v3, v1, v4, v5}, Landroid_maps_conflict_avoidance/com/google/common/graphics/ImageFactory;->createImage([BII)Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 387
    .restart local v0    # "createdImage":Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;
    const/4 v2, 0x1

    .restart local v2    # "isRecyclable":Z
    goto :goto_0

    .line 365
    .end local v0    # "createdImage":Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;
    .end local v1    # "imageData":[B
    .end local v2    # "isRecyclable":Z
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 661
    if-ne p0, p1, :cond_1

    .line 669
    :cond_0
    :goto_0
    return v1

    .line 664
    :cond_1
    instance-of v3, p1, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;

    if-nez v3, :cond_2

    move v1, v2

    .line 665
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 668
    check-cast v0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;

    .line 669
    .local v0, "imageTile":Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;
    iget-object v3, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->location:Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;

    if-nez v3, :cond_3

    iget-object v3, v0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->location:Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;

    if-eqz v3, :cond_0

    move v1, v2

    goto :goto_0

    :cond_3
    iget-object v1, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->location:Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;

    iget-object v2, v0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->location:Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;

    invoke-virtual {v1, v2}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public getCompletePaintCount()I
    .locals 1

    .prologue
    .line 352
    iget v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->completePaintCount:I

    return v0
.end method

.method public getDataSize()I
    .locals 1

    .prologue
    .line 357
    iget-object v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->data:[B

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->data:[B

    array-length v0, v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getFirstPaintTime()J
    .locals 2

    .prologue
    .line 344
    iget-wide v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->firstPaintTime:J

    return-wide v0
.end method

.method public getImage()Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;
    .locals 2

    .prologue
    .line 558
    const-wide/high16 v0, -0x8000000000000000L

    invoke-virtual {p0, v0, v1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->getImage(J)Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;

    move-result-object v0

    return-object v0
.end method

.method public getImage(J)Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;
    .locals 7
    .param p1, "accessTime"    # J

    .prologue
    .line 574
    const/4 v1, 0x0

    .line 576
    .local v1, "handleOutOfMemory":Z
    monitor-enter p0

    .line 577
    const-wide/high16 v4, -0x8000000000000000L

    cmp-long v3, p1, v4

    if-nez v3, :cond_3

    .line 578
    :try_start_0
    invoke-static {}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getInstance()Landroid_maps_conflict_avoidance/com/google/common/Config;

    move-result-object v3

    invoke-virtual {v3}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getClock()Landroid_maps_conflict_avoidance/com/google/common/Clock;

    move-result-object v3

    invoke-interface {v3}, Landroid_maps_conflict_avoidance/com/google/common/Clock;->currentTimeMillis()J

    move-result-wide v4

    iput-wide v4, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->lastAccessTime:J

    .line 585
    :goto_0
    iget-object v3, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->mapImage:Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;

    if-eqz v3, :cond_0

    iget-boolean v3, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->hasScaledImage:Z

    if-eqz v3, :cond_1

    :cond_0
    invoke-virtual {p0}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->isComplete()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-eqz v3, :cond_1

    .line 588
    :try_start_1
    invoke-virtual {p0}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->createImage()V
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 596
    :cond_1
    :goto_1
    :try_start_2
    iget-object v3, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->mapImage:Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;

    if-nez v3, :cond_4

    .line 598
    invoke-direct {p0}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->getTempImage()Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;

    move-result-object v2

    .line 602
    .local v2, "returnValue":Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;
    :goto_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 603
    if-eqz v1, :cond_2

    .line 605
    invoke-static {}, Landroid_maps_conflict_avoidance/com/google/common/StaticUtil;->handleOutOfMemory()V

    .line 607
    :cond_2
    return-object v2

    .line 580
    .end local v2    # "returnValue":Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;
    :cond_3
    :try_start_3
    iput-wide p1, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->lastAccessTime:J

    goto :goto_0

    .line 602
    :catchall_0
    move-exception v3

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v3

    .line 589
    :catch_0
    move-exception v0

    .line 592
    .local v0, "e":Ljava/lang/OutOfMemoryError;
    const/4 v1, 0x1

    goto :goto_1

    .line 600
    .end local v0    # "e":Ljava/lang/OutOfMemoryError;
    :cond_4
    :try_start_4
    iget-object v2, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->mapImage:Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .restart local v2    # "returnValue":Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;
    goto :goto_2
.end method

.method public getImageVersion()I
    .locals 1

    .prologue
    .line 541
    iget v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->imageVersion:I

    return v0
.end method

.method public getIsPreCached()Z
    .locals 1

    .prologue
    .line 825
    iget-boolean v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->isPreCached:Z

    return v0
.end method

.method public getLastAccessTime()J
    .locals 2

    .prologue
    .line 307
    iget-wide v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->lastAccessTime:J

    return-wide v0
.end method

.method public getLocation()Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;
    .locals 1

    .prologue
    .line 275
    iget-object v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->location:Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;

    return-object v0
.end method

.method public getRequested()Z
    .locals 1

    .prologue
    .line 833
    iget-boolean v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->requested:Z

    return v0
.end method

.method public hasImage()Z
    .locals 1

    .prologue
    .line 303
    iget-object v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->mapImage:Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasRenderedImage()Z
    .locals 2

    .prologue
    .line 707
    iget v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->imageVersion:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->baseMapImage:Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;

    iget-object v1, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->mapImage:Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasScaledImage()Z
    .locals 1

    .prologue
    .line 549
    iget-boolean v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->hasScaledImage:Z

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 675
    iget-object v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->location:Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->location:Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;

    invoke-virtual {v0}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;->hashCode()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isComplete()Z
    .locals 1

    .prologue
    .line 297
    iget-object v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->data:[B

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public removeScaledImage()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 264
    iget-boolean v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->hasScaledImage:Z

    if-eqz v0, :cond_0

    .line 265
    iput-boolean v1, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->hasScaledImage:Z

    .line 266
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->setMapImage(Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;Z)V

    .line 268
    :cond_0
    return-void
.end method

.method public restoreBaseImage()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 532
    iget-object v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->baseMapImage:Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;

    invoke-direct {p0, v0, v1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->setMapImage(Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;Z)V

    .line 533
    iput v1, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->imageVersion:I

    .line 534
    return-void
.end method

.method public declared-synchronized setData([B)V
    .locals 2
    .param p1, "data"    # [B

    .prologue
    .line 688
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->isComplete()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 689
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Tile already complete"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 688
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 691
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    iput v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->imageVersion:I

    .line 692
    iput-object p1, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->data:[B

    .line 696
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->setBaseMapImage(Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;Z)V

    .line 697
    iget-boolean v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->hasScaledImage:Z

    if-nez v0, :cond_1

    .line 698
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->setMapImage(Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 700
    :cond_1
    monitor-exit p0

    return-void
.end method

.method public setImage(Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;IZ)V
    .locals 0
    .param p1, "image"    # Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;
    .param p2, "imageVersion"    # I
    .param p3, "isRecyclable"    # Z

    .prologue
    .line 461
    invoke-static {p1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->verifyTileDimensions(Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;)V

    .line 462
    invoke-direct {p0, p1, p3}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->setMapImage(Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;Z)V

    .line 463
    invoke-virtual {p0, p2}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->setImageVersion(I)V

    .line 464
    return-void
.end method

.method public setImageVersion(I)V
    .locals 2
    .param p1, "imageVersion"    # I

    .prologue
    .line 472
    if-nez p1, :cond_0

    .line 473
    iget-object v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->mapImage:Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;

    iget-boolean v1, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->isMapImageRecyclable:Z

    invoke-direct {p0, v0, v1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->setBaseMapImage(Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;Z)V

    .line 478
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->isMapImageRecyclable:Z

    .line 480
    :cond_0
    iput p1, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->imageVersion:I

    .line 481
    return-void
.end method

.method public final setLastAccessTime(J)V
    .locals 1
    .param p1, "lastAccessTime"    # J

    .prologue
    .line 311
    iput-wide p1, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->lastAccessTime:J

    .line 312
    return-void
.end method

.method public setPaint(JJ)V
    .locals 7
    .param p1, "mapPaintTime"    # J
    .param p3, "lastMapPaintTime"    # J

    .prologue
    const-wide/16 v4, 0x0

    .line 327
    iget v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->completePaintCount:I

    if-nez v0, :cond_0

    iget-wide v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->lastPaintTime:J

    cmp-long v0, v0, p3

    if-eqz v0, :cond_0

    iget-wide v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->lastPaintTime:J

    sub-long v0, p1, v0

    const-wide/16 v2, 0x7d0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 329
    iput-wide v4, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->firstPaintTime:J

    .line 331
    :cond_0
    iget-wide v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->firstPaintTime:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_1

    .line 332
    iput-wide p1, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->firstPaintTime:J

    .line 334
    :cond_1
    iput-wide p1, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->lastPaintTime:J

    .line 337
    invoke-virtual {p0}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->hasImage()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->hasScaledImage:Z

    if-nez v0, :cond_2

    iget v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->completePaintCount:I

    const v1, 0x7fffffff

    if-ge v0, v1, :cond_2

    .line 339
    iget v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->completePaintCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->completePaintCount:I

    .line 341
    :cond_2
    return-void
.end method

.method public setRequested(Z)V
    .locals 0
    .param p1, "requested"    # Z

    .prologue
    .line 829
    iput-boolean p1, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->requested:Z

    .line 830
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 679
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->location:Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;

    invoke-virtual {v1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->data:[B

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "B"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->data:[B

    array-length v2, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, "B?"

    goto :goto_0
.end method

.method public write(Ljava/io/DataOutput;)V
    .locals 1
    .param p1, "dos"    # Ljava/io/DataOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 636
    iget-object v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->location:Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;

    invoke-virtual {v0, p1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;->write(Ljava/io/DataOutput;)V

    .line 637
    iget-object v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->data:[B

    array-length v0, v0

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeShort(I)V

    .line 638
    iget-object v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->data:[B

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->write([B)V

    .line 639
    return-void
.end method
