.class public Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;
.super Ljava/lang/Object;
.source "MapService.java"

# interfaces
.implements Landroid_maps_conflict_avoidance/com/google/common/OutOfMemoryHandler;
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService$MapTileRequest;
    }
.end annotation


# instance fields
.field private final autoConfigCache:Z

.field private currentRequest:Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService$MapTileRequest;

.field volatile exitWorkThread:Z

.field final flashService:Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTileStorage;

.field private final indefiniteThreadLockObject:Ljava/lang/Object;

.field private lastMapMoveTime:J

.field private final layerImageTiles:Ljava/util/Vector;

.field private final layerServices:Ljava/util/Vector;

.field final mapCache:Ljava/util/Hashtable;

.field private volatile mapCacheLocked:Z

.field private maxCacheDataSize:I

.field private observer:Landroid_maps_conflict_avoidance/com/google/googlenav/map/LayerService$TileUpdateObserver;

.field private outOfMemoryTime:J

.field private final repaintListeners:Ljava/util/Vector;

.field private requestType:I

.field private requestsOutstanding:I

.field private targetCacheDataSize:I

.field private final tempScaledImages:Ljava/util/Hashtable;

.field private final timedThreadLockObject:Ljava/lang/Object;


# direct methods
.method constructor <init>(IIIILjava/lang/String;)V
    .locals 5
    .param p1, "maxCacheDataSize"    # I
    .param p2, "targetCacheDataSize"    # I
    .param p3, "maxFlashSize"    # I
    .param p4, "maxRecordStores"    # I
    .param p5, "tileRecordStoreName"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 242
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->layerServices:Ljava/util/Vector;

    .line 84
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->layerImageTiles:Ljava/util/Vector;

    .line 112
    const/4 v0, 0x0

    iput-object v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->currentRequest:Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService$MapTileRequest;

    .line 115
    iput v2, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->requestsOutstanding:I

    .line 151
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->indefiniteThreadLockObject:Ljava/lang/Object;

    .line 156
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->timedThreadLockObject:Ljava/lang/Object;

    .line 162
    iput-boolean v4, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->exitWorkThread:Z

    .line 165
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->repaintListeners:Ljava/util/Vector;

    .line 182
    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->outOfMemoryTime:J

    .line 243
    const/16 v0, 0x1a

    iput v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->requestType:I

    .line 244
    if-ne p1, v3, :cond_0

    .line 245
    iput-boolean v4, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->autoConfigCache:Z

    .line 246
    const/16 v0, 0x61a8

    iput v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->maxCacheDataSize:I

    .line 247
    invoke-direct {p0}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->setAutoTargetCacheSize()V

    .line 259
    :goto_0
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->tempScaledImages:Ljava/util/Hashtable;

    .line 260
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->mapCache:Ljava/util/Hashtable;

    .line 261
    iput-boolean v2, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->mapCacheLocked:Z

    .line 265
    if-lez p3, :cond_2

    .line 266
    new-instance v0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapFlashService;

    invoke-direct {v0, p0, p5, p3, p4}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapFlashService;-><init>(Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;Ljava/lang/String;II)V

    iput-object v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->flashService:Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTileStorage;

    .line 272
    :goto_1
    invoke-static {}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->getRelativeTime()J

    move-result-wide v0

    iput-wide v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->lastMapMoveTime:J

    .line 274
    new-instance v0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService$1;

    invoke-direct {v0, p0}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService$1;-><init>(Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;)V

    invoke-static {v0}, Landroid_maps_conflict_avoidance/com/google/googlenav/StartupHelper;->addPostStartupBgCallback(Ljava/lang/Runnable;)V

    .line 280
    invoke-static {p0}, Landroid_maps_conflict_avoidance/com/google/common/StaticUtil;->registerOutOfMemoryHandler(Landroid_maps_conflict_avoidance/com/google/common/OutOfMemoryHandler;)V

    .line 281
    return-void

    .line 249
    :cond_0
    iput-boolean v2, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->autoConfigCache:Z

    .line 250
    iput p1, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->maxCacheDataSize:I

    .line 252
    if-ne p2, v3, :cond_1

    .line 253
    invoke-direct {p0}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->setAutoTargetCacheSize()V

    goto :goto_0

    .line 255
    :cond_1
    iput p2, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->targetCacheDataSize:I

    goto :goto_0

    .line 269
    :cond_2
    new-instance v0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/NullMapTileStorage;

    invoke-direct {v0}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/NullMapTileStorage;-><init>()V

    iput-object v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->flashService:Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTileStorage;

    goto :goto_1
.end method

.method static synthetic access$000(Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;)V
    .locals 0
    .param p0, "x0"    # Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;

    .prologue
    .line 35
    invoke-direct {p0}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->startWorkThread()V

    return-void
.end method

.method static synthetic access$100(Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;)I
    .locals 1
    .param p0, "x0"    # Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;

    .prologue
    .line 35
    iget v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->requestType:I

    return v0
.end method

.method static synthetic access$208(Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;)I
    .locals 2
    .param p0, "x0"    # Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;

    .prologue
    .line 35
    iget v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->requestsOutstanding:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->requestsOutstanding:I

    return v0
.end method

.method static synthetic access$210(Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;)I
    .locals 2
    .param p0, "x0"    # Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;

    .prologue
    .line 35
    iget v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->requestsOutstanding:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->requestsOutstanding:I

    return v0
.end method

.method static synthetic access$300(Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;)Ljava/util/Hashtable;
    .locals 1
    .param p0, "x0"    # Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;

    .prologue
    .line 35
    iget-object v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->tempScaledImages:Ljava/util/Hashtable;

    return-object v0
.end method

.method static synthetic access$400(Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;)Ljava/util/Vector;
    .locals 1
    .param p0, "x0"    # Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;

    .prologue
    .line 35
    iget-object v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->repaintListeners:Ljava/util/Vector;

    return-object v0
.end method

.method private addMapEntry(Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;)V
    .locals 2
    .param p1, "mapTile"    # Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;

    .prologue
    .line 805
    invoke-virtual {p1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->getLocation()Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;

    move-result-object v0

    .line 806
    .local v0, "tile":Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;
    iget-object v1, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->mapCache:Ljava/util/Hashtable;

    invoke-virtual {v1, v0, p1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 807
    return-void
.end method

.method private clearScaledImages()V
    .locals 4

    .prologue
    .line 625
    iget-object v3, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->mapCache:Ljava/util/Hashtable;

    monitor-enter v3

    .line 626
    const/4 v2, 0x1

    :try_start_0
    iput-boolean v2, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->mapCacheLocked:Z

    .line 627
    iget-object v2, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->tempScaledImages:Ljava/util/Hashtable;

    invoke-virtual {v2}, Ljava/util/Hashtable;->clear()V

    .line 629
    iget-object v2, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->mapCache:Ljava/util/Hashtable;

    invoke-virtual {v2}, Ljava/util/Hashtable;->elements()Ljava/util/Enumeration;

    move-result-object v0

    .line 630
    .local v0, "enumeration":Ljava/util/Enumeration;
    :goto_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 631
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;

    .line 632
    .local v1, "mapTile":Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;
    invoke-virtual {v1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->removeScaledImage()V

    goto :goto_0

    .line 635
    .end local v0    # "enumeration":Ljava/util/Enumeration;
    .end local v1    # "mapTile":Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 634
    .restart local v0    # "enumeration":Ljava/util/Enumeration;
    :cond_0
    const/4 v2, 0x0

    :try_start_1
    iput-boolean v2, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->mapCacheLocked:Z

    .line 635
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 636
    return-void
.end method

.method private createScaledImage(Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;)Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;
    .locals 12
    .param p1, "tile"    # Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;

    .prologue
    .line 574
    invoke-static {}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->getRelativeTime()J

    move-result-wide v0

    .line 575
    .local v0, "currentTime":J
    iget-wide v8, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->outOfMemoryTime:J

    const-wide/16 v10, 0x2710

    add-long/2addr v8, v10

    cmp-long v7, v0, v8

    if-gez v7, :cond_1

    .line 576
    const/4 v6, 0x0

    .line 621
    :cond_0
    :goto_0
    return-object v6

    .line 578
    :cond_1
    const/4 v6, 0x0

    .line 580
    .local v6, "tempImage":Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;
    :try_start_0
    invoke-virtual {p1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;->getZoomParent()Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;

    move-result-object v3

    .line 581
    .local v3, "parent":Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;
    if-eqz v3, :cond_0

    .line 582
    invoke-virtual {v3}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;->getZoom()Landroid_maps_conflict_avoidance/com/google/map/Zoom;

    move-result-object v7

    invoke-virtual {p1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;->getZoom()Landroid_maps_conflict_avoidance/com/google/map/Zoom;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid_maps_conflict_avoidance/com/google/map/Zoom;->getZoomRatio(Landroid_maps_conflict_avoidance/com/google/map/Zoom;)I

    move-result v5

    .line 583
    .local v5, "ratio":I
    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {p0, v3, v7, v8, v9}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->getTile(Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;IZZ)Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;

    move-result-object v4

    .line 584
    .local v4, "parentMapTile":Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;
    const/4 v7, 0x2

    if-ne v5, v7, :cond_0

    invoke-virtual {v4}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->hasImage()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 587
    invoke-virtual {v4}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->getImage()Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;

    move-result-object v7

    invoke-direct {p0, p1, v3, v7}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->createScaledImage(Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;)Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    goto :goto_0

    .line 603
    .end local v3    # "parent":Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;
    .end local v4    # "parentMapTile":Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;
    .end local v5    # "ratio":I
    :catch_0
    move-exception v2

    .line 616
    .local v2, "e":Ljava/lang/OutOfMemoryError;
    invoke-direct {p0}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->clearScaledImages()V

    .line 617
    iput-wide v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->outOfMemoryTime:J

    .line 618
    const-string v7, "Map Service image scaling"

    invoke-static {v7, v2}, Landroid_maps_conflict_avoidance/com/google/common/Log;->logQuietThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private createScaledImage(Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;)Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;
    .locals 9
    .param p1, "tile"    # Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;
    .param p2, "parentTile"    # Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;
    .param p3, "parentImage"    # Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;

    .prologue
    const/16 v5, 0x100

    const/4 v0, 0x0

    const/16 v3, 0x80

    .line 741
    const/16 v7, 0x80

    .line 742
    .local v7, "halfTile":I
    invoke-virtual {p1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;->getXIndex()I

    move-result v4

    invoke-virtual {p2}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;->getXIndex()I

    move-result v6

    mul-int/lit8 v6, v6, 0x2

    if-ne v4, v6, :cond_0

    move v1, v0

    .line 743
    .local v1, "xOffset":I
    :goto_0
    invoke-virtual {p1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;->getYIndex()I

    move-result v4

    invoke-virtual {p2}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;->getYIndex()I

    move-result v6

    mul-int/lit8 v6, v6, 0x2

    if-ne v4, v6, :cond_1

    move v2, v0

    .local v2, "yOffset":I
    :goto_1
    move-object v0, p3

    move v4, v3

    move v6, v5

    .line 744
    invoke-interface/range {v0 .. v6}, Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;->createScaledImage(IIIIII)Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;

    move-result-object v8

    .line 746
    .local v8, "scaledImage":Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;
    return-object v8

    .end local v1    # "xOffset":I
    .end local v2    # "yOffset":I
    .end local v8    # "scaledImage":Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;
    :cond_0
    move v1, v3

    .line 742
    goto :goto_0

    .restart local v1    # "xOffset":I
    :cond_1
    move v2, v3

    .line 743
    goto :goto_1
.end method

.method private doCompact(Z)V
    .locals 14
    .param p1, "emergency"    # Z

    .prologue
    .line 774
    if-eqz p1, :cond_1

    const-wide/16 v8, 0x7d0

    .line 775
    .local v8, "maxAge":J
    :goto_0
    iget-object v11, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->mapCache:Ljava/util/Hashtable;

    monitor-enter v11

    .line 776
    const/4 v10, 0x1

    :try_start_0
    invoke-virtual {p0, v10}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->setMapCacheLocked(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 778
    :try_start_1
    invoke-static {}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getInstance()Landroid_maps_conflict_avoidance/com/google/common/Config;

    move-result-object v10

    invoke-virtual {v10}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getClock()Landroid_maps_conflict_avoidance/com/google/common/Clock;

    move-result-object v10

    invoke-interface {v10}, Landroid_maps_conflict_avoidance/com/google/common/Clock;->currentTimeMillis()J

    move-result-wide v0

    .line 779
    .local v0, "currentTime":J
    iget-object v10, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->mapCache:Ljava/util/Hashtable;

    invoke-virtual {v10}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v5

    .local v5, "keys":Ljava/util/Enumeration;
    :cond_0
    invoke-interface {v5}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v10

    if-eqz v10, :cond_2

    .line 780
    invoke-interface {v5}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;

    .line 781
    .local v7, "tile":Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;
    iget-object v10, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->mapCache:Ljava/util/Hashtable;

    invoke-virtual {v10, v7}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;

    .line 782
    .local v6, "mapTile":Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;
    invoke-virtual {v6}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->getLastAccessTime()J

    move-result-wide v2

    .line 784
    .local v2, "date":J
    add-long v12, v2, v8

    cmp-long v10, v12, v0

    if-gez v10, :cond_0

    .line 785
    invoke-virtual {v6}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->compact()V

    .line 788
    iget-object v10, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->layerServices:Ljava/util/Vector;

    invoke-virtual {v10}, Ljava/util/Vector;->size()I

    move-result v10

    add-int/lit8 v4, v10, -0x1

    .local v4, "i":I
    :goto_1
    if-ltz v4, :cond_0

    .line 789
    iget-object v10, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->layerServices:Ljava/util/Vector;

    invoke-virtual {v10, v4}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid_maps_conflict_avoidance/com/google/googlenav/map/LayerService;

    const/16 v12, 0x8

    invoke-static {v12, v7}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;->getTile(BLandroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;)Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;

    move-result-object v12

    invoke-virtual {v10, v12}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/LayerService;->doCompact(Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 788
    add-int/lit8 v4, v4, -0x1

    goto :goto_1

    .line 774
    .end local v0    # "currentTime":J
    .end local v2    # "date":J
    .end local v4    # "i":I
    .end local v5    # "keys":Ljava/util/Enumeration;
    .end local v6    # "mapTile":Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;
    .end local v7    # "tile":Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;
    .end local v8    # "maxAge":J
    :cond_1
    const-wide/16 v8, 0xfa0

    goto :goto_0

    .line 795
    .restart local v8    # "maxAge":J
    :catchall_0
    move-exception v10

    const/4 v12, 0x0

    :try_start_2
    invoke-virtual {p0, v12}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->setMapCacheLocked(Z)V

    throw v10

    .line 797
    :catchall_1
    move-exception v10

    monitor-exit v11
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v10

    .line 795
    .restart local v0    # "currentTime":J
    .restart local v5    # "keys":Ljava/util/Enumeration;
    :cond_2
    const/4 v10, 0x0

    :try_start_3
    invoke-virtual {p0, v10}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->setMapCacheLocked(Z)V

    .line 797
    monitor-exit v11
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 798
    return-void
.end method

.method private getOrCreateScaledImage(Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;)Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;
    .locals 2
    .param p1, "tile"    # Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;

    .prologue
    .line 557
    iget-object v1, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->tempScaledImages:Ljava/util/Hashtable;

    invoke-virtual {v1, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;

    .line 558
    .local v0, "image":Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;
    if-nez v0, :cond_0

    .line 559
    invoke-direct {p0, p1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->createScaledImage(Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;)Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;

    move-result-object v0

    .line 560
    if-eqz v0, :cond_0

    .line 561
    iget-object v1, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->tempScaledImages:Ljava/util/Hashtable;

    invoke-virtual {v1, p1, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 565
    :cond_0
    return-object v0
.end method

.method private static getRelativeTime()J
    .locals 2

    .prologue
    .line 1314
    invoke-static {}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getInstance()Landroid_maps_conflict_avoidance/com/google/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getClock()Landroid_maps_conflict_avoidance/com/google/common/Clock;

    move-result-object v0

    invoke-interface {v0}, Landroid_maps_conflict_avoidance/com/google/common/Clock;->relativeTimeMillis()J

    move-result-wide v0

    return-wide v0
.end method

.method private getScaledImageFromCache(Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;)Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;
    .locals 1
    .param p1, "tile"    # Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;

    .prologue
    .line 546
    iget-object v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->tempScaledImages:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;

    return-object v0
.end method

.method static getScore(Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;JJ)J
    .locals 3
    .param p0, "tile"    # Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;
    .param p1, "currentTime"    # J
    .param p3, "lastAccessTime"    # J

    .prologue
    .line 976
    sub-long v0, p1, p3

    return-wide v0
.end method

.method private getTempImage(Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;I)Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;
    .locals 1
    .param p1, "tile"    # Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;
    .param p2, "scaleMode"    # I

    .prologue
    .line 523
    packed-switch p2, :pswitch_data_0

    .line 532
    invoke-direct {p0, p1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->getOrCreateScaledImage(Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;)Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;

    move-result-object v0

    .line 536
    .local v0, "tempImage":Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;
    :goto_0
    return-object v0

    .line 525
    .end local v0    # "tempImage":Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;
    :pswitch_0
    const/4 v0, 0x0

    .line 526
    .restart local v0    # "tempImage":Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;
    goto :goto_0

    .line 528
    .end local v0    # "tempImage":Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;
    :pswitch_1
    invoke-direct {p0, p1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->getScaledImageFromCache(Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;)Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;

    move-result-object v0

    .line 529
    .restart local v0    # "tempImage":Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;
    goto :goto_0

    .line 523
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private partition([J[Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;III)I
    .locals 10
    .param p1, "scoreList"    # [J
    .param p2, "list"    # [Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;
    .param p3, "left"    # I
    .param p4, "right"    # I
    .param p5, "pivotIndex"    # I

    .prologue
    .line 1034
    aget-wide v2, p1, p5

    .line 1035
    .local v2, "pivotValue":J
    invoke-direct {p0, p1, p2, p5, p4}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->swap([J[Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;II)V

    .line 1036
    move v1, p3

    .line 1037
    .local v1, "store":I
    move v0, p3

    .local v0, "i":I
    move v4, v1

    .end local v1    # "store":I
    .local v4, "store":I
    :goto_0
    if-ge v0, p4, :cond_0

    .line 1038
    aget-wide v6, p1, v0

    cmp-long v5, v6, v2

    if-ltz v5, :cond_2

    .line 1039
    add-int/lit8 v1, v4, 0x1

    .end local v4    # "store":I
    .restart local v1    # "store":I
    invoke-direct {p0, p1, p2, v0, v4}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->swap([J[Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;II)V

    .line 1037
    :goto_1
    add-int/lit8 v0, v0, 0x1

    move v4, v1

    .end local v1    # "store":I
    .restart local v4    # "store":I
    goto :goto_0

    .line 1043
    :cond_0
    aget-wide v6, p1, p4

    aget-wide v8, p1, v4

    cmp-long v5, v6, v8

    if-lez v5, :cond_1

    .line 1044
    invoke-direct {p0, p1, p2, p4, v4}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->swap([J[Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;II)V

    .line 1047
    .end local v4    # "store":I
    :goto_2
    return v4

    .restart local v4    # "store":I
    :cond_1
    move v4, p4

    goto :goto_2

    :cond_2
    move v1, v4

    .end local v4    # "store":I
    .restart local v1    # "store":I
    goto :goto_1
.end method

.method private qsort([J[Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;II)V
    .locals 7
    .param p1, "scoreList"    # [J
    .param p2, "list"    # [Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;
    .param p3, "left"    # I
    .param p4, "right"    # I

    .prologue
    .line 1057
    if-le p4, p3, :cond_0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p3

    .line 1059
    invoke-direct/range {v0 .. v5}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->partition([J[Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;III)I

    move-result v6

    .line 1060
    .local v6, "newPivot":I
    add-int/lit8 v0, v6, -0x1

    invoke-direct {p0, p1, p2, p3, v0}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->qsort([J[Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;II)V

    .line 1061
    add-int/lit8 v0, v6, 0x1

    invoke-direct {p0, p1, p2, v0, p4}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->qsort([J[Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;II)V

    .line 1063
    .end local v6    # "newPivot":I
    :cond_0
    return-void
.end method

.method private queueTileRequest(Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;I)V
    .locals 2
    .param p1, "mapTile"    # Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;
    .param p2, "priority"    # I

    .prologue
    .line 751
    iget-object v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->currentRequest:Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService$MapTileRequest;

    if-nez v0, :cond_0

    .line 754
    new-instance v0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService$MapTileRequest;

    invoke-virtual {p1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->getLocation()Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;

    move-result-object v1

    invoke-virtual {v1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;->getFlags()B

    move-result v1

    invoke-direct {v0, p0, v1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService$MapTileRequest;-><init>(Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;B)V

    iput-object v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->currentRequest:Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService$MapTileRequest;

    .line 756
    :cond_0
    iget-object v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->currentRequest:Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService$MapTileRequest;

    invoke-virtual {v0, p1, p2}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService$MapTileRequest;->requestTile(Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;I)V

    .line 757
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->setRequested(Z)V

    .line 758
    return-void
.end method

.method private setAutoTargetCacheSize()V
    .locals 1

    .prologue
    .line 302
    iget v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->maxCacheDataSize:I

    mul-int/lit8 v0, v0, 0x4

    div-int/lit8 v0, v0, 0x5

    iput v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->targetCacheDataSize:I

    .line 303
    return-void
.end method

.method private sort([J[Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;)V
    .locals 2
    .param p1, "scoreList"    # [J
    .param p2, "list"    # [Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;

    .prologue
    .line 1066
    const/4 v0, 0x0

    array-length v1, p2

    add-int/lit8 v1, v1, -0x1

    invoke-direct {p0, p1, p2, v0, v1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->qsort([J[Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;II)V

    .line 1067
    return-void
.end method

.method private startWorkThread()V
    .locals 2

    .prologue
    .line 1336
    iget-boolean v1, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->exitWorkThread:Z

    if-eqz v1, :cond_0

    .line 1337
    const/4 v1, 0x0

    iput-boolean v1, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->exitWorkThread:Z

    .line 1338
    new-instance v0, Ljava/lang/Thread;

    const-string v1, "MapService"

    invoke-direct {v0, p0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 1340
    .local v0, "bgThread":Ljava/lang/Thread;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setPriority(I)V

    .line 1342
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 1344
    .end local v0    # "bgThread":Ljava/lang/Thread;
    :cond_0
    return-void
.end method

.method private stopWorkThread()V
    .locals 2

    .prologue
    .line 1321
    iget-boolean v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->exitWorkThread:Z

    if-nez v0, :cond_0

    .line 1322
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->exitWorkThread:Z

    .line 1323
    iget-object v1, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->timedThreadLockObject:Ljava/lang/Object;

    monitor-enter v1

    .line 1324
    :try_start_0
    iget-object v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->timedThreadLockObject:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 1325
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1326
    iget-object v1, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->indefiniteThreadLockObject:Ljava/lang/Object;

    monitor-enter v1

    .line 1327
    :try_start_1
    iget-object v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->indefiniteThreadLockObject:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 1328
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1330
    :cond_0
    return-void

    .line 1325
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 1328
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private swap([J[Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;II)V
    .locals 6
    .param p1, "scoreList"    # [J
    .param p2, "list"    # [Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;
    .param p3, "indexA"    # I
    .param p4, "indexB"    # I

    .prologue
    .line 1013
    aget-object v2, p2, p4

    .line 1014
    .local v2, "tempTile":Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;
    aget-object v3, p2, p3

    aput-object v3, p2, p4

    .line 1015
    aput-object v2, p2, p3

    .line 1017
    aget-wide v0, p1, p4

    .line 1018
    .local v0, "tempScore":J
    aget-wide v4, p1, p3

    aput-wide v4, p1, p4

    .line 1019
    aput-wide v0, p1, p3

    .line 1020
    return-void
.end method

.method private trimCache(I)V
    .locals 7
    .param p1, "cacheSize"    # I

    .prologue
    .line 852
    iget-object v5, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->mapCache:Ljava/util/Hashtable;

    monitor-enter v5

    .line 854
    const/4 v4, 0x1

    :try_start_0
    iput-boolean v4, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->mapCacheLocked:Z

    .line 857
    invoke-virtual {p0}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->getSortedCacheList()[Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;

    move-result-object v3

    .line 859
    .local v3, "sortedList":[Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v4, v3

    if-ge v0, v4, :cond_2

    iget v4, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->targetCacheDataSize:I

    if-le p1, v4, :cond_2

    .line 860
    aget-object v2, v3, v0

    .line 862
    .local v2, "minKey":Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;
    iget-object v4, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->mapCache:Ljava/util/Hashtable;

    invoke-virtual {v4, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;

    .line 865
    .local v1, "mapTile":Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;
    invoke-virtual {v1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->isComplete()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->getRequested()Z

    move-result v4

    if-nez v4, :cond_1

    .line 866
    :cond_0
    iget-object v4, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->mapCache:Ljava/util/Hashtable;

    invoke-virtual {v4, v2}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 868
    invoke-virtual {v1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->getDataSize()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    sub-int/2addr p1, v4

    .line 859
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 879
    .end local v0    # "i":I
    .end local v1    # "mapTile":Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;
    .end local v2    # "minKey":Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;
    .end local v3    # "sortedList":[Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;
    :catchall_0
    move-exception v4

    const/4 v6, 0x0

    :try_start_1
    iput-boolean v6, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->mapCacheLocked:Z

    throw v4

    .line 881
    :catchall_1
    move-exception v4

    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v4

    .line 879
    .restart local v0    # "i":I
    .restart local v3    # "sortedList":[Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;
    :cond_2
    const/4 v4, 0x0

    :try_start_2
    iput-boolean v4, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->mapCacheLocked:Z

    .line 881
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 882
    return-void
.end method


# virtual methods
.method checkTrimCache()V
    .locals 10

    .prologue
    .line 814
    invoke-virtual {p0}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->getCacheSize()I

    move-result v0

    .line 815
    .local v0, "cacheSize":I
    iget v5, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->maxCacheDataSize:I

    if-le v0, v5, :cond_0

    .line 816
    iget-boolean v5, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->autoConfigCache:Z

    if-eqz v5, :cond_1

    .line 818
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 821
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v6

    int-to-long v8, v0

    add-long v2, v6, v8

    .line 823
    .local v2, "memAvail":J
    const-wide/32 v6, 0x9c40

    sub-long v6, v2, v6

    long-to-int v5, v6

    div-int/lit8 v1, v5, 0x2

    .line 828
    .local v1, "size":I
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Runtime;->totalMemory()J

    move-result-wide v6

    long-to-int v4, v6

    .line 831
    .local v4, "totalMemory":I
    div-int/lit8 v5, v4, 0x3

    invoke-static {v1, v5}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 833
    const/16 v5, 0x61a8

    invoke-static {v5, v1}, Ljava/lang/Math;->max(II)I

    move-result v5

    iput v5, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->maxCacheDataSize:I

    .line 835
    invoke-direct {p0}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->setAutoTargetCacheSize()V

    .line 838
    iget v5, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->maxCacheDataSize:I

    if-ge v0, v5, :cond_1

    .line 844
    .end local v1    # "size":I
    .end local v2    # "memAvail":J
    .end local v4    # "totalMemory":I
    :cond_0
    :goto_0
    return-void

    .line 842
    :cond_1
    invoke-direct {p0, v0}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->trimCache(I)V

    goto :goto_0
.end method

.method close(Z)V
    .locals 3
    .param p1, "saveState"    # Z

    .prologue
    .line 325
    invoke-static {p0}, Landroid_maps_conflict_avoidance/com/google/common/StaticUtil;->removeOutOfMemoryHandler(Landroid_maps_conflict_avoidance/com/google/common/OutOfMemoryHandler;)V

    .line 326
    invoke-direct {p0}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->stopWorkThread()V

    .line 327
    iget-object v2, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->flashService:Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTileStorage;

    invoke-interface {v2, p1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTileStorage;->close(Z)V

    .line 330
    iget-object v2, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->layerServices:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    add-int/lit8 v0, v2, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_0

    .line 331
    iget-object v2, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->layerServices:Ljava/util/Vector;

    invoke-virtual {v2, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid_maps_conflict_avoidance/com/google/googlenav/map/LayerService;

    .line 332
    .local v1, "layerService":Landroid_maps_conflict_avoidance/com/google/googlenav/map/LayerService;
    invoke-virtual {v1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/LayerService;->close()V

    .line 333
    invoke-static {v1}, Landroid_maps_conflict_avoidance/com/google/common/StaticUtil;->removeOutOfMemoryHandler(Landroid_maps_conflict_avoidance/com/google/common/OutOfMemoryHandler;)V

    .line 330
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 335
    .end local v1    # "layerService":Landroid_maps_conflict_avoidance/com/google/googlenav/map/LayerService;
    :cond_0
    iget-object v2, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->layerServices:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->removeAllElements()V

    .line 336
    return-void
.end method

.method getCacheSize()I
    .locals 5

    .prologue
    .line 887
    const/4 v0, 0x0

    .line 888
    .local v0, "cacheSize":I
    iget-object v4, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->mapCache:Ljava/util/Hashtable;

    monitor-enter v4

    .line 889
    :try_start_0
    iget-object v3, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->mapCache:Ljava/util/Hashtable;

    invoke-virtual {v3}, Ljava/util/Hashtable;->elements()Ljava/util/Enumeration;

    move-result-object v1

    .local v1, "entries":Ljava/util/Enumeration;
    :goto_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 890
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;

    .line 891
    .local v2, "tile":Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;
    invoke-virtual {v2}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->getDataSize()I

    move-result v3

    add-int/2addr v0, v3

    .line 892
    goto :goto_0

    .line 893
    .end local v2    # "tile":Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;
    :cond_0
    monitor-exit v4

    .line 894
    return v0

    .line 893
    .end local v1    # "entries":Ljava/util/Enumeration;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method public getLayerTiles(Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;Z)Ljava/util/Vector;
    .locals 6
    .param p1, "tile"    # Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;
    .param p2, "fetch"    # Z

    .prologue
    .line 1109
    iget-object v4, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->layerImageTiles:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->removeAllElements()V

    .line 1110
    iget-object v4, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->layerServices:Ljava/util/Vector;

    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v4

    add-int/lit8 v0, v4, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_1

    .line 1111
    iget-object v4, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->layerServices:Ljava/util/Vector;

    invoke-virtual {v4, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid_maps_conflict_avoidance/com/google/googlenav/map/LayerService;

    .line 1112
    .local v1, "layerService":Landroid_maps_conflict_avoidance/com/google/googlenav/map/LayerService;
    invoke-virtual {v1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/LayerService;->needFetchLayerTiles()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1113
    const/16 v4, 0x8

    invoke-static {v4, p1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;->getTile(BLandroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;)Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;

    move-result-object v3

    .line 1114
    .local v3, "location":Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;
    invoke-virtual {v1, v3, p2}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/LayerService;->getTile(Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;Z)Landroid_maps_conflict_avoidance/com/google/googlenav/map/LayerTile;

    move-result-object v2

    .line 1118
    .local v2, "layerTile":Landroid_maps_conflict_avoidance/com/google/googlenav/map/LayerTile;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/LayerTile;->hasImage()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1119
    iget-object v4, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->layerImageTiles:Ljava/util/Vector;

    invoke-virtual {v2}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/LayerTile;->getImage()Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 1110
    .end local v2    # "layerTile":Landroid_maps_conflict_avoidance/com/google/googlenav/map/LayerTile;
    .end local v3    # "location":Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 1123
    .end local v1    # "layerService":Landroid_maps_conflict_avoidance/com/google/googlenav/map/LayerService;
    :cond_1
    iget-object v4, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->layerImageTiles:Ljava/util/Vector;

    return-object v4
.end method

.method getMapCache()Ljava/util/Hashtable;
    .locals 1

    .prologue
    .line 293
    iget-object v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->mapCache:Ljava/util/Hashtable;

    return-object v0
.end method

.method getRenderedImageCount()I
    .locals 3

    .prologue
    .line 955
    const/4 v1, 0x0

    .line 956
    .local v1, "renderedImageCount":I
    iget-object v2, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->mapCache:Ljava/util/Hashtable;

    invoke-virtual {v2}, Ljava/util/Hashtable;->elements()Ljava/util/Enumeration;

    move-result-object v0

    .local v0, "entries":Ljava/util/Enumeration;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 957
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;

    invoke-virtual {v2}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->hasRenderedImage()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 958
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 961
    :cond_1
    return v1
.end method

.method getSortedCacheList()[Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;
    .locals 10

    .prologue
    .line 991
    invoke-static {}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getInstance()Landroid_maps_conflict_avoidance/com/google/common/Config;

    move-result-object v6

    invoke-virtual {v6}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getClock()Landroid_maps_conflict_avoidance/com/google/common/Clock;

    move-result-object v6

    invoke-interface {v6}, Landroid_maps_conflict_avoidance/com/google/common/Clock;->currentTimeMillis()J

    move-result-wide v4

    .line 992
    .local v4, "startTime":J
    iget-object v6, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->mapCache:Ljava/util/Hashtable;

    invoke-virtual {v6}, Ljava/util/Hashtable;->size()I

    move-result v6

    new-array v2, v6, [Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;

    .line 993
    .local v2, "list":[Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;
    iget-object v6, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->mapCache:Ljava/util/Hashtable;

    invoke-virtual {v6}, Ljava/util/Hashtable;->size()I

    move-result v6

    new-array v3, v6, [J

    .line 994
    .local v3, "scoreList":[J
    const/4 v1, 0x0

    .line 996
    .local v1, "index":I
    iget-object v6, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->mapCache:Ljava/util/Hashtable;

    invoke-virtual {v6}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v0

    .line 997
    .local v0, "enumeration":Ljava/util/Enumeration;
    :goto_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 998
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;

    aput-object v6, v2, v1

    .line 999
    aget-object v6, v2, v1

    aget-object v7, v2, v1

    invoke-virtual {p0, v7}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->getTileDate(Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;)J

    move-result-wide v8

    invoke-static {v6, v4, v5, v8, v9}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->getScore(Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;JJ)J

    move-result-wide v6

    aput-wide v6, v3, v1

    .line 1001
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1004
    :cond_0
    invoke-direct {p0, v3, v2}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->sort([J[Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;)V

    .line 1006
    return-object v2
.end method

.method getTile(Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;IZIJ)Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;
    .locals 9
    .param p1, "tile"    # Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;
    .param p2, "priority"    # I
    .param p3, "loadTile"    # Z
    .param p4, "scaleMode"    # I
    .param p5, "accessTime"    # J

    .prologue
    const/4 v6, 0x1

    .line 419
    iget-object v3, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->mapCache:Ljava/util/Hashtable;

    invoke-virtual {v3, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;

    .line 421
    .local v0, "mapTile":Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;
    const-wide/high16 v4, -0x8000000000000000L

    cmp-long v3, p5, v4

    if-nez v3, :cond_0

    .line 422
    invoke-static {}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getInstance()Landroid_maps_conflict_avoidance/com/google/common/Config;

    move-result-object v3

    invoke-virtual {v3}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getClock()Landroid_maps_conflict_avoidance/com/google/common/Clock;

    move-result-object v3

    invoke-interface {v3}, Landroid_maps_conflict_avoidance/com/google/common/Clock;->currentTimeMillis()J

    move-result-wide p5

    .line 425
    :cond_0
    if-nez v0, :cond_6

    .line 427
    iget-boolean v3, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->mapCacheLocked:Z

    if-nez v3, :cond_5

    .line 428
    iget-object v4, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->mapCache:Ljava/util/Hashtable;

    monitor-enter v4

    .line 429
    const/4 v3, 0x1

    :try_start_0
    invoke-virtual {p0, v3}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->setMapCacheLocked(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 433
    :try_start_1
    iget-object v3, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->flashService:Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTileStorage;

    invoke-interface {v3, p1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTileStorage;->getMapTile(Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;)Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;

    move-result-object v0

    .line 435
    if-nez v0, :cond_3

    .line 440
    invoke-direct {p0, p1, p4}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->getTempImage(Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;I)Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;

    move-result-object v2

    .line 444
    .local v2, "tempImage":Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;
    if-eqz p3, :cond_2

    invoke-static {}, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->getInstance()Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;

    move-result-object v3

    invoke-virtual {v3}, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->canDispatchNow()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 447
    new-instance v1, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;

    invoke-direct {v1, p1, v2}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;-><init>(Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 449
    .end local v0    # "mapTile":Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;
    .local v1, "mapTile":Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;
    :try_start_2
    invoke-direct {p0, v1, p2}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->queueTileRequest(Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;I)V

    .line 450
    invoke-direct {p0, v1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->addMapEntry(Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;)V

    .line 452
    invoke-static {}, Landroid_maps_conflict_avoidance/com/google/googlenav/Stats;->getInstance()Landroid_maps_conflict_avoidance/com/google/googlenav/Stats;

    move-result-object v3

    invoke-virtual {v3}, Landroid_maps_conflict_avoidance/com/google/googlenav/Stats;->flashCacheMiss()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-object v0, v1

    .line 481
    .end local v1    # "mapTile":Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;
    .end local v2    # "tempImage":Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;
    .restart local v0    # "mapTile":Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;
    :goto_0
    const/4 v3, 0x0

    :try_start_3
    invoke-virtual {p0, v3}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->setMapCacheLocked(Z)V

    .line 483
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 501
    :cond_1
    :goto_1
    invoke-virtual {v0, p5, p6}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->setLastAccessTime(J)V

    .line 502
    return-object v0

    .line 455
    .restart local v2    # "tempImage":Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;
    :cond_2
    :try_start_4
    new-instance v1, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;

    const/4 v3, 0x1

    invoke-direct {v1, p1, v2, v3}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;-><init>(Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;Z)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 462
    .end local v0    # "mapTile":Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;
    .restart local v1    # "mapTile":Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;
    :try_start_5
    invoke-direct {p0, v1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->addMapEntry(Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    move-object v0, v1

    .end local v1    # "mapTile":Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;
    .restart local v0    # "mapTile":Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;
    goto :goto_0

    .line 469
    .end local v2    # "tempImage":Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;
    :cond_3
    if-nez p3, :cond_4

    .line 470
    const-wide/16 v6, 0x4e20

    sub-long/2addr p5, v6

    .line 473
    :cond_4
    :try_start_6
    invoke-direct {p0, v0}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->addMapEntry(Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;)V

    .line 478
    invoke-static {}, Landroid_maps_conflict_avoidance/com/google/googlenav/Stats;->getInstance()Landroid_maps_conflict_avoidance/com/google/googlenav/Stats;

    move-result-object v3

    invoke-virtual {v3}, Landroid_maps_conflict_avoidance/com/google/googlenav/Stats;->flashCacheHit()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_0

    .line 481
    :catchall_0
    move-exception v3

    :goto_2
    const/4 v5, 0x0

    :try_start_7
    invoke-virtual {p0, v5}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->setMapCacheLocked(Z)V

    throw v3

    .line 483
    :catchall_1
    move-exception v3

    monitor-exit v4
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    throw v3

    .line 488
    :cond_5
    new-instance v0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;

    .end local v0    # "mapTile":Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;
    const/4 v3, 0x0

    check-cast v3, Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;

    invoke-direct {v0, p1, v3, v6}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;-><init>(Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;Z)V

    .restart local v0    # "mapTile":Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;
    goto :goto_1

    .line 490
    :cond_6
    invoke-virtual {v0}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->isComplete()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v0}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->getRequested()Z

    move-result v3

    if-nez v3, :cond_1

    if-eqz p3, :cond_1

    invoke-static {}, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->getInstance()Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;

    move-result-object v3

    invoke-virtual {v3}, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->canDispatchNow()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 495
    invoke-direct {p0, v0, p2}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->queueTileRequest(Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;I)V

    .line 497
    invoke-static {}, Landroid_maps_conflict_avoidance/com/google/googlenav/Stats;->getInstance()Landroid_maps_conflict_avoidance/com/google/googlenav/Stats;

    move-result-object v3

    invoke-virtual {v3}, Landroid_maps_conflict_avoidance/com/google/googlenav/Stats;->flashCacheMiss()V

    goto :goto_1

    .line 481
    .end local v0    # "mapTile":Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;
    .restart local v1    # "mapTile":Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;
    .restart local v2    # "tempImage":Landroid_maps_conflict_avoidance/com/google/common/graphics/GoogleImage;
    :catchall_2
    move-exception v3

    move-object v0, v1

    .end local v1    # "mapTile":Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;
    .restart local v0    # "mapTile":Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;
    goto :goto_2
.end method

.method public getTile(Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;IZZ)Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;
    .locals 8
    .param p1, "tile"    # Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;
    .param p2, "priority"    # I
    .param p3, "loadTile"    # Z
    .param p4, "scaleOk"    # Z

    .prologue
    .line 361
    const-wide/high16 v6, -0x8000000000000000L

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v1 .. v7}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->getTile(Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;IZZJ)Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;

    move-result-object v0

    return-object v0
.end method

.method getTile(Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;IZZJ)Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;
    .locals 9
    .param p1, "tile"    # Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;
    .param p2, "priority"    # I
    .param p3, "loadTile"    # Z
    .param p4, "scaleOk"    # Z
    .param p5, "accessTime"    # J

    .prologue
    .line 390
    if-eqz p4, :cond_0

    const/4 v5, 0x2

    :goto_0
    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move-wide v6, p5

    invoke-virtual/range {v1 .. v7}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->getTile(Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;IZIJ)Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v5, 0x0

    goto :goto_0
.end method

.method getTileDate(Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;)J
    .locals 2
    .param p1, "tile"    # Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;

    .prologue
    .line 982
    iget-object v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->mapCache:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;

    invoke-virtual {v0}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->getLastAccessTime()J

    move-result-wide v0

    return-wide v0
.end method

.method public handleOutOfMemory(Z)V
    .locals 4
    .param p1, "warning"    # Z

    .prologue
    .line 1226
    invoke-static {}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashRecord;->clearDataCache()V

    .line 1227
    invoke-direct {p0}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->clearScaledImages()V

    .line 1229
    iget-object v2, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->mapCache:Ljava/util/Hashtable;

    monitor-enter v2

    .line 1230
    const/16 v0, 0x1f40

    .line 1231
    .local v0, "CACHE_SHRINK_SIZE":I
    const/4 v1, 0x1

    :try_start_0
    invoke-direct {p0, v1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->doCompact(Z)V

    .line 1233
    iget-boolean v1, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->autoConfigCache:Z

    if-eqz v1, :cond_0

    .line 1234
    const/16 v1, 0x61a8

    iput v1, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->maxCacheDataSize:I

    .line 1235
    invoke-direct {p0}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->setAutoTargetCacheSize()V

    .line 1242
    :goto_0
    invoke-virtual {p0}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->checkTrimCache()V

    .line 1243
    monitor-exit v2

    .line 1244
    return-void

    .line 1237
    :cond_0
    iget v1, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->maxCacheDataSize:I

    add-int/lit16 v1, v1, -0x1f40

    const/16 v3, 0x61a8

    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->maxCacheDataSize:I

    .line 1239
    invoke-direct {p0}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->setAutoTargetCacheSize()V

    goto :goto_0

    .line 1243
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method mapChanged()V
    .locals 2

    .prologue
    .line 1218
    invoke-static {}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->getRelativeTime()J

    move-result-wide v0

    iput-wide v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->lastMapMoveTime:J

    .line 1219
    iget-object v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->flashService:Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTileStorage;

    invoke-interface {v0}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTileStorage;->mapChanged()V

    .line 1220
    iget-object v1, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->indefiniteThreadLockObject:Ljava/lang/Object;

    monitor-enter v1

    .line 1221
    :try_start_0
    iget-object v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->indefiniteThreadLockObject:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 1222
    monitor-exit v1

    .line 1223
    return-void

    .line 1222
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public notifyLayerTilesDirty()V
    .locals 2

    .prologue
    .line 1128
    iget-object v1, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->observer:Landroid_maps_conflict_avoidance/com/google/googlenav/map/LayerService$TileUpdateObserver;

    if-eqz v1, :cond_0

    .line 1129
    iget-object v1, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->observer:Landroid_maps_conflict_avoidance/com/google/googlenav/map/LayerService$TileUpdateObserver;

    invoke-interface {v1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/LayerService$TileUpdateObserver;->setLayerTilesDirty()V

    .line 1131
    :cond_0
    iget-object v1, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->layerServices:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    add-int/lit8 v0, v1, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_1

    .line 1132
    iget-object v1, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->layerServices:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid_maps_conflict_avoidance/com/google/googlenav/map/LayerService;

    invoke-virtual {v1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/LayerService;->notifyLayerTilesDirty()V

    .line 1131
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 1134
    :cond_1
    return-void
.end method

.method pause()V
    .locals 0

    .prologue
    .line 1350
    invoke-direct {p0}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->stopWorkThread()V

    .line 1351
    return-void
.end method

.method requestLayerTiles()V
    .locals 3

    .prologue
    .line 1088
    iget-object v2, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->layerServices:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    add-int/lit8 v0, v2, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_1

    .line 1089
    iget-object v2, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->layerServices:Ljava/util/Vector;

    invoke-virtual {v2, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid_maps_conflict_avoidance/com/google/googlenav/map/LayerService;

    .line 1090
    .local v1, "layerService":Landroid_maps_conflict_avoidance/com/google/googlenav/map/LayerService;
    invoke-virtual {v1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/LayerService;->needFetchLayerTiles()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1091
    invoke-virtual {v1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/LayerService;->requestTiles()V

    .line 1088
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 1094
    .end local v1    # "layerService":Landroid_maps_conflict_avoidance/com/google/googlenav/map/LayerService;
    :cond_1
    return-void
.end method

.method requestTiles()Z
    .locals 2

    .prologue
    .line 1073
    iget-object v1, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->currentRequest:Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService$MapTileRequest;

    if-eqz v1, :cond_0

    .line 1074
    iget-object v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->currentRequest:Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService$MapTileRequest;

    .line 1077
    .local v0, "tempRequest":Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService$MapTileRequest;
    const/4 v1, 0x0

    iput-object v1, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->currentRequest:Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService$MapTileRequest;

    .line 1078
    invoke-static {}, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->getInstance()Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequestDispatcher;->addDataRequest(Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/DataRequest;)V

    .line 1079
    const/4 v1, 0x1

    .line 1081
    .end local v0    # "tempRequest":Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService$MapTileRequest;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public restoreBaseImagesIfNeeded()I
    .locals 8

    .prologue
    .line 909
    const/4 v3, 0x0

    .line 910
    .local v3, "renderedImageCount":I
    iget-object v6, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->mapCache:Ljava/util/Hashtable;

    monitor-enter v6

    .line 912
    const/4 v5, 0x1

    :try_start_0
    invoke-virtual {p0, v5}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->setMapCacheLocked(Z)V

    .line 913
    invoke-virtual {p0}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->getRenderedImageCount()I

    move-result v3

    .line 914
    const/16 v5, 0x30

    if-le v3, v5, :cond_1

    .line 919
    invoke-virtual {p0}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->getSortedCacheList()[Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;

    move-result-object v4

    .line 922
    .local v4, "sortedList":[Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;
    const/4 v0, 0x0

    .line 923
    .local v0, "i":I
    :goto_0
    array-length v5, v4

    if-ge v0, v5, :cond_1

    const/16 v5, 0x18

    if-le v3, v5, :cond_1

    .line 924
    aget-object v2, v4, v0

    .line 925
    .local v2, "minKey":Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;
    iget-object v5, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->mapCache:Ljava/util/Hashtable;

    invoke-virtual {v5, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;

    .line 932
    .local v1, "mapTile":Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;
    invoke-virtual {v1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->hasRenderedImage()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 933
    invoke-virtual {v1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->restoreBaseImage()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 934
    add-int/lit8 v3, v3, -0x1

    .line 923
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 944
    .end local v0    # "i":I
    .end local v1    # "mapTile":Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;
    .end local v2    # "minKey":Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;
    .end local v4    # "sortedList":[Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;
    :catchall_0
    move-exception v5

    const/4 v7, 0x0

    :try_start_1
    invoke-virtual {p0, v7}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->setMapCacheLocked(Z)V

    throw v5

    .line 946
    :catchall_1
    move-exception v5

    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v5

    .line 944
    :cond_1
    const/4 v5, 0x0

    :try_start_2
    invoke-virtual {p0, v5}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->setMapCacheLocked(Z)V

    .line 946
    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 947
    return v3
.end method

.method resume()V
    .locals 0

    .prologue
    .line 1357
    invoke-direct {p0}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->startWorkThread()V

    .line 1358
    return-void
.end method

.method public run()V
    .locals 14

    .prologue
    .line 1247
    invoke-static {}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->getRelativeTime()J

    move-result-wide v10

    const-wide/16 v12, 0x835

    add-long v6, v10, v12

    .line 1248
    .local v6, "nextTrimTime":J
    invoke-static {}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->getRelativeTime()J

    move-result-wide v10

    const-wide/16 v12, 0xc29

    add-long v4, v10, v12

    .line 1250
    .local v4, "nextCompactTime":J
    :cond_0
    :goto_0
    iget-boolean v3, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->exitWorkThread:Z

    if-nez v3, :cond_5

    .line 1253
    :try_start_0
    iget-object v12, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->timedThreadLockObject:Ljava/lang/Object;

    monitor-enter v12
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1

    .line 1260
    :try_start_1
    invoke-static {}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->getRelativeTime()J

    move-result-wide v0

    .line 1261
    .local v0, "currentTime":J
    cmp-long v3, v6, v4

    if-gez v3, :cond_4

    move-wide v10, v6

    :goto_1
    sub-long v8, v10, v0

    .line 1263
    .local v8, "nextWakeup":J
    const-wide/16 v10, 0x0

    cmp-long v3, v8, v10

    if-lez v3, :cond_1

    .line 1264
    iget-object v3, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->timedThreadLockObject:Ljava/lang/Object;

    invoke-virtual {v3, v8, v9}, Ljava/lang/Object;->wait(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1269
    .end local v0    # "currentTime":J
    .end local v8    # "nextWakeup":J
    :cond_1
    :goto_2
    :try_start_2
    monitor-exit v12
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1271
    :try_start_3
    iget-boolean v3, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->exitWorkThread:Z

    if-nez v3, :cond_0

    .line 1272
    invoke-static {}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->getRelativeTime()J

    move-result-wide v0

    .line 1274
    .restart local v0    # "currentTime":J
    cmp-long v3, v6, v0

    if-gez v3, :cond_2

    .line 1275
    invoke-virtual {p0}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->checkTrimCache()V

    .line 1276
    const-wide/16 v10, 0x835

    add-long v6, v0, v10

    .line 1280
    :cond_2
    cmp-long v3, v4, v0

    if-gez v3, :cond_3

    .line 1281
    const/4 v3, 0x0

    invoke-direct {p0, v3}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->doCompact(Z)V

    .line 1282
    const-wide/16 v10, 0xc29

    add-long v4, v0, v10

    .line 1290
    :cond_3
    iget-object v3, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->flashService:Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTileStorage;

    invoke-interface {v3}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTileStorage;->writeCache()Z

    move-result v3

    if-nez v3, :cond_0

    iget-wide v10, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->lastMapMoveTime:J

    const-wide/16 v12, 0xfa0

    add-long/2addr v10, v12

    cmp-long v3, v10, v0

    if-gez v3, :cond_0

    .line 1293
    iget-object v10, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->indefiniteThreadLockObject:Ljava/lang/Object;

    monitor-enter v10
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_1

    .line 1295
    :try_start_4
    iget-object v3, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->indefiniteThreadLockObject:Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/lang/Object;->wait()V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1299
    :goto_3
    :try_start_5
    monitor-exit v10

    goto :goto_0

    :catchall_0
    move-exception v3

    monitor-exit v10
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    throw v3
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_6 .. :try_end_6} :catch_1

    .line 1302
    .end local v0    # "currentTime":J
    :catch_0
    move-exception v2

    .line 1303
    .local v2, "e":Ljava/lang/Exception;
    const-string v3, "MapService BG"

    invoke-static {v3, v2}, Landroid_maps_conflict_avoidance/com/google/common/Log;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .end local v2    # "e":Ljava/lang/Exception;
    .restart local v0    # "currentTime":J
    :cond_4
    move-wide v10, v4

    .line 1261
    goto :goto_1

    .line 1269
    .end local v0    # "currentTime":J
    :catchall_1
    move-exception v3

    :try_start_7
    monitor-exit v12
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :try_start_8
    throw v3
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_8 .. :try_end_8} :catch_1

    .line 1304
    :catch_1
    move-exception v2

    .line 1306
    .local v2, "e":Ljava/lang/OutOfMemoryError;
    invoke-static {}, Landroid_maps_conflict_avoidance/com/google/common/StaticUtil;->handleOutOfMemory()V

    goto :goto_0

    .line 1311
    .end local v2    # "e":Ljava/lang/OutOfMemoryError;
    :cond_5
    return-void

    .line 1296
    .restart local v0    # "currentTime":J
    :catch_2
    move-exception v3

    goto :goto_3

    .line 1266
    .end local v0    # "currentTime":J
    :catch_3
    move-exception v3

    goto :goto_2
.end method

.method setMapCacheLocked(Z)V
    .locals 0
    .param p1, "mapCacheLocked"    # Z

    .prologue
    .line 225
    iput-boolean p1, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->mapCacheLocked:Z

    .line 226
    return-void
.end method

.method setTileEditionAndTextSize(II)V
    .locals 8
    .param p1, "tileEdition"    # I
    .param p2, "textSize"    # I

    .prologue
    .line 1190
    iget-object v5, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->flashService:Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTileStorage;

    invoke-interface {v5, p1, p2}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTileStorage;->setTileEditionAndTextSize(II)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1191
    iget-object v6, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->mapCache:Ljava/util/Hashtable;

    monitor-enter v6

    .line 1195
    :try_start_0
    iget-object v5, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->mapCache:Ljava/util/Hashtable;

    invoke-virtual {v5}, Ljava/util/Hashtable;->keys()Ljava/util/Enumeration;

    move-result-object v2

    .line 1196
    .local v2, "mapTiles":Ljava/util/Enumeration;
    new-instance v4, Ljava/util/Vector;

    invoke-direct {v4}, Ljava/util/Vector;-><init>()V

    .line 1197
    .local v4, "toRemove":Ljava/util/Vector;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1198
    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;

    .line 1199
    .local v3, "tile":Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;
    iget-object v5, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->mapCache:Ljava/util/Hashtable;

    invoke-virtual {v5, v3}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;

    .line 1200
    .local v1, "mapTile":Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;
    invoke-virtual {v1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->isComplete()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1201
    invoke-virtual {v4, v3}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_0

    .line 1209
    .end local v1    # "mapTile":Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;
    .end local v2    # "mapTiles":Ljava/util/Enumeration;
    .end local v3    # "tile":Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;
    .end local v4    # "toRemove":Ljava/util/Vector;
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5

    .line 1206
    .restart local v2    # "mapTiles":Ljava/util/Enumeration;
    .restart local v4    # "toRemove":Ljava/util/Vector;
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    :try_start_1
    invoke-virtual {v4}, Ljava/util/Vector;->size()I

    move-result v5

    if-ge v0, v5, :cond_2

    .line 1207
    iget-object v5, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->mapCache:Ljava/util/Hashtable;

    invoke-virtual {v4, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1206
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1209
    :cond_2
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1211
    .end local v0    # "i":I
    .end local v2    # "mapTiles":Ljava/util/Enumeration;
    .end local v4    # "toRemove":Ljava/util/Vector;
    :cond_3
    return-void
.end method
