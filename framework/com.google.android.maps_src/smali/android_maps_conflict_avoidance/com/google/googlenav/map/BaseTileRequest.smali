.class public abstract Landroid_maps_conflict_avoidance/com/google/googlenav/map/BaseTileRequest;
.super Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/BaseDataRequest;
.source "BaseTileRequest.java"


# instance fields
.field protected final createTime:J

.field private final requestType:I

.field private stopwatchStatsTile:Landroid_maps_conflict_avoidance/com/google/common/util/StopwatchStats;

.field private textSize:I

.field private writeLatency:I


# direct methods
.method protected constructor <init>(IB)V
    .locals 4
    .param p1, "requestType"    # I
    .param p2, "flags"    # B

    .prologue
    .line 49
    invoke-direct {p0}, Landroid_maps_conflict_avoidance/com/google/googlenav/datarequest/BaseDataRequest;-><init>()V

    .line 34
    invoke-static {}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getInstance()Landroid_maps_conflict_avoidance/com/google/common/Config;

    move-result-object v1

    invoke-virtual {v1}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getClock()Landroid_maps_conflict_avoidance/com/google/common/Clock;

    move-result-object v1

    invoke-interface {v1}, Landroid_maps_conflict_avoidance/com/google/common/Clock;->relativeTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/BaseTileRequest;->createTime:J

    .line 50
    iput p1, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/BaseTileRequest;->requestType:I

    .line 53
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "tile-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x1

    shl-int/2addr v2, p2

    invoke-static {v2}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/BaseTileRequest;->formatTileTypesForLog(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 54
    .local v0, "stopwatchName":Ljava/lang/String;
    new-instance v1, Landroid_maps_conflict_avoidance/com/google/common/util/StopwatchStats;

    const-string v2, "t"

    const/16 v3, 0x16

    invoke-direct {v1, v0, v2, v3}, Landroid_maps_conflict_avoidance/com/google/common/util/StopwatchStats;-><init>(Ljava/lang/String;Ljava/lang/String;S)V

    iput-object v1, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/BaseTileRequest;->stopwatchStatsTile:Landroid_maps_conflict_avoidance/com/google/common/util/StopwatchStats;

    .line 56
    iget-object v1, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/BaseTileRequest;->stopwatchStatsTile:Landroid_maps_conflict_avoidance/com/google/common/util/StopwatchStats;

    invoke-virtual {v1}, Landroid_maps_conflict_avoidance/com/google/common/util/StopwatchStats;->start()V

    .line 57
    return-void
.end method

.method private static formatTileTypesForLog(I)Ljava/lang/String;
    .locals 2
    .param p0, "tileTypes"    # I

    .prologue
    .line 205
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    .line 206
    .local v0, "result":Ljava/util/Vector;
    and-int/lit8 v1, p0, 0x4

    if-eqz v1, :cond_0

    .line 207
    const-string v1, "m"

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 209
    :cond_0
    and-int/lit8 v1, p0, 0x8

    if-eqz v1, :cond_1

    .line 210
    const-string v1, "s"

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 213
    :cond_1
    and-int/lit8 v1, p0, 0x40

    if-eqz v1, :cond_2

    .line 214
    const-string v1, "h"

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 217
    :cond_2
    and-int/lit16 v1, p0, 0x80

    if-eqz v1, :cond_3

    .line 218
    const-string v1, "n"

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 221
    :cond_3
    and-int/lit8 v1, p0, 0x10

    if-eqz v1, :cond_4

    .line 222
    const-string v1, "t"

    invoke-virtual {v0, v1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 224
    :cond_4
    const-string v1, ","

    invoke-static {v0, v1}, Landroid_maps_conflict_avoidance/com/google/common/util/text/TextUtil;->join(Ljava/util/Vector;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private readImageData(Ljava/io/DataInput;)[B
    .locals 2
    .param p1, "dis"    # Ljava/io/DataInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 190
    invoke-interface {p1}, Ljava/io/DataInput;->readUnsignedShort()I

    move-result v1

    .line 191
    .local v1, "tileSize":I
    new-array v0, v1, [B

    .line 194
    .local v0, "imageBytes":[B
    invoke-interface {p1, v0}, Ljava/io/DataInput;->readFully([B)V

    .line 195
    return-object v0
.end method


# virtual methods
.method public getRequestType()I
    .locals 1

    .prologue
    .line 60
    iget v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/BaseTileRequest;->requestType:I

    return v0
.end method

.method protected abstract handleEndOfResponse(I)V
.end method

.method protected abstract processDownloadedTile(ILandroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;[B)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public readResponseData(Ljava/io/DataInput;)Z
    .locals 19
    .param p1, "dis"    # Ljava/io/DataInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 98
    const/16 v16, 0x0

    .line 100
    .local v16, "tileIndex":I
    :try_start_0
    invoke-static {}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getInstance()Landroid_maps_conflict_avoidance/com/google/common/Config;

    move-result-object v2

    invoke-virtual {v2}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getClock()Landroid_maps_conflict_avoidance/com/google/common/Clock;

    move-result-object v2

    invoke-interface {v2}, Landroid_maps_conflict_avoidance/com/google/common/Clock;->relativeTimeMillis()J

    move-result-wide v10

    .line 101
    .local v10, "now":J
    move-object/from16 v0, p0

    iget-wide v2, v0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/BaseTileRequest;->createTime:J

    sub-long v2, v10, v2

    long-to-int v4, v2

    .line 102
    .local v4, "firstByteLatency":I
    invoke-interface/range {p1 .. p1}, Ljava/io/DataInput;->readUnsignedShort()I

    move-result v15

    .line 103
    .local v15, "tileEdition":I
    move-object/from16 v0, p0

    iget v2, v0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/BaseTileRequest;->textSize:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v15, v2}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/BaseTileRequest;->setTileEditionAndTextSize(II)V

    .line 105
    const/4 v6, 0x0

    .line 106
    .local v6, "tileCount":I
    const/4 v7, 0x0

    .line 107
    .local v7, "totalSize":I
    const/16 v17, 0x0

    .line 108
    .local v17, "tileTypes":I
    move-object/from16 v0, p0

    iget v2, v0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/BaseTileRequest;->requestType:I

    const/16 v3, 0x1a

    if-ne v2, v3, :cond_1

    .line 112
    invoke-interface/range {p1 .. p1}, Ljava/io/DataInput;->readUnsignedByte()I

    move-result v14

    .line 113
    .local v14, "responseCode":I
    if-eqz v14, :cond_0

    .line 115
    new-instance v2, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "Server returned: "

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 155
    .end local v4    # "firstByteLatency":I
    .end local v6    # "tileCount":I
    .end local v7    # "totalSize":I
    .end local v10    # "now":J
    .end local v14    # "responseCode":I
    .end local v15    # "tileEdition":I
    .end local v17    # "tileTypes":I
    :catchall_0
    move-exception v2

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/BaseTileRequest;->handleEndOfResponse(I)V

    throw v2

    .line 117
    .restart local v4    # "firstByteLatency":I
    .restart local v6    # "tileCount":I
    .restart local v7    # "totalSize":I
    .restart local v10    # "now":J
    .restart local v14    # "responseCode":I
    .restart local v15    # "tileEdition":I
    .restart local v17    # "tileTypes":I
    :cond_0
    :try_start_1
    invoke-interface/range {p1 .. p1}, Ljava/io/DataInput;->readUnsignedShort()I

    move-result v6

    .line 121
    .end local v14    # "responseCode":I
    :cond_1
    const/16 v16, 0x0

    :goto_0
    move/from16 v0, v16

    if-ge v0, v6, :cond_2

    .line 123
    invoke-static/range {p1 .. p1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;->read(Ljava/io/DataInput;)Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;

    move-result-object v9

    .line 124
    .local v9, "location":Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;
    invoke-direct/range {p0 .. p1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/BaseTileRequest;->readImageData(Ljava/io/DataInput;)[B

    move-result-object v8

    .line 125
    .local v8, "imageBytes":[B
    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1, v9, v8}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/BaseTileRequest;->processDownloadedTile(ILandroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;[B)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 147
    .end local v8    # "imageBytes":[B
    .end local v9    # "location":Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;
    :cond_2
    invoke-static {}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getInstance()Landroid_maps_conflict_avoidance/com/google/common/Config;

    move-result-object v2

    invoke-virtual {v2}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getClock()Landroid_maps_conflict_avoidance/com/google/common/Clock;

    move-result-object v2

    invoke-interface {v2}, Landroid_maps_conflict_avoidance/com/google/common/Clock;->relativeTimeMillis()J

    move-result-wide v12

    .line 148
    .local v12, "now2":J
    move-object/from16 v0, p0

    iget-wide v2, v0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/BaseTileRequest;->createTime:J

    sub-long v2, v12, v2

    long-to-int v5, v2

    .line 150
    .local v5, "lastByteLatency":I
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/BaseTileRequest;->stopwatchStatsTile:Landroid_maps_conflict_avoidance/com/google/common/util/StopwatchStats;

    invoke-virtual {v2}, Landroid_maps_conflict_avoidance/com/google/common/util/StopwatchStats;->stop()V

    .line 152
    invoke-static/range {v17 .. v17}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/BaseTileRequest;->formatTileTypesForLog(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget v3, v0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/BaseTileRequest;->writeLatency:I

    invoke-static/range {v2 .. v7}, Landroid_maps_conflict_avoidance/com/google/googlenav/GmmLogger;->logTimingTileLatency(Ljava/lang/String;IIIII)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 155
    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/BaseTileRequest;->handleEndOfResponse(I)V

    .line 158
    const/4 v2, 0x1

    return v2

    .line 134
    .end local v5    # "lastByteLatency":I
    .end local v12    # "now2":J
    .restart local v8    # "imageBytes":[B
    .restart local v9    # "location":Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;
    :cond_3
    :try_start_2
    array-length v2, v8

    add-int/2addr v7, v2

    .line 142
    const/4 v2, 0x1

    invoke-virtual {v9}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;->getFlags()B
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v3

    shl-int/2addr v2, v3

    or-int v17, v17, v2

    .line 121
    add-int/lit8 v16, v16, 0x1

    goto :goto_0
.end method

.method protected abstract setTileEditionAndTextSize(II)V
.end method

.method protected writeRequestForTiles([Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;Ljava/io/DataOutput;)V
    .locals 8
    .param p1, "tileList"    # [Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;
    .param p2, "dos"    # Ljava/io/DataOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 65
    iget v6, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/BaseTileRequest;->requestType:I

    const/16 v7, 0x1a

    if-ne v6, v7, :cond_1

    .line 71
    array-length v6, p1

    invoke-interface {p2, v6}, Ljava/io/DataOutput;->writeShort(I)V

    .line 72
    invoke-static {}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->getTextSize()I

    move-result v6

    iput v6, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/BaseTileRequest;->textSize:I

    .line 73
    iget v6, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/BaseTileRequest;->textSize:I

    invoke-interface {p2, v6}, Ljava/io/DataOutput;->writeShort(I)V

    .line 74
    const/16 v6, 0x100

    invoke-interface {p2, v6}, Ljava/io/DataOutput;->writeShort(I)V

    .line 76
    const-wide/16 v0, 0xa2f

    .line 83
    .local v0, "format":J
    sget-object v6, Landroid_maps_conflict_avoidance/com/google/googlenav/labs/LocalLanguageTileLab;->INSTANCE:Landroid_maps_conflict_avoidance/com/google/googlenav/labs/LocalLanguageTileLab;

    invoke-virtual {v6}, Landroid_maps_conflict_avoidance/com/google/googlenav/labs/LocalLanguageTileLab;->isActive()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 84
    const-wide/16 v6, 0x2000

    or-long/2addr v0, v6

    .line 86
    :cond_0
    invoke-interface {p2, v0, v1}, Ljava/io/DataOutput;->writeLong(J)V

    .line 89
    .end local v0    # "format":J
    :cond_1
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v6, p1

    if-ge v2, v6, :cond_2

    .line 90
    aget-object v3, p1, v2

    .line 91
    .local v3, "tile":Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;
    invoke-virtual {v3, p2}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;->write(Ljava/io/DataOutput;)V

    .line 89
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 93
    .end local v3    # "tile":Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;
    :cond_2
    invoke-static {}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getInstance()Landroid_maps_conflict_avoidance/com/google/common/Config;

    move-result-object v6

    invoke-virtual {v6}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getClock()Landroid_maps_conflict_avoidance/com/google/common/Clock;

    move-result-object v6

    invoke-interface {v6}, Landroid_maps_conflict_avoidance/com/google/common/Clock;->relativeTimeMillis()J

    move-result-wide v4

    .line 94
    .local v4, "now":J
    iget-wide v6, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/BaseTileRequest;->createTime:J

    sub-long v6, v4, v6

    long-to-int v6, v6

    iput v6, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/BaseTileRequest;->writeLatency:I

    .line 95
    return-void
.end method
