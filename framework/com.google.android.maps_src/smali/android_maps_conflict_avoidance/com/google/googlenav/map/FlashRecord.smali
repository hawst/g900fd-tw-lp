.class Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashRecord;
.super Ljava/lang/Object;
.source "FlashRecord.java"


# static fields
.field private static final lastCacheDataLock:Ljava/lang/Object;

.field private static lastCachedData:[B

.field private static lastFlashRecord:Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashRecord;


# instance fields
.field private final flashEntries:Ljava/util/Vector;

.field private recordId:I

.field private unverified:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 75
    sput-object v0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashRecord;->lastCachedData:[B

    .line 80
    sput-object v0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashRecord;->lastFlashRecord:Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashRecord;

    .line 86
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashRecord;->lastCacheDataLock:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashRecord;->unverified:Z

    .line 90
    const/4 v0, -0x1

    iput v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashRecord;->recordId:I

    .line 91
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashRecord;->flashEntries:Ljava/util/Vector;

    .line 92
    return-void
.end method

.method static clearDataCache()V
    .locals 2

    .prologue
    .line 503
    sget-object v1, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashRecord;->lastCacheDataLock:Ljava/lang/Object;

    monitor-enter v1

    .line 504
    const/4 v0, 0x0

    :try_start_0
    sput-object v0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashRecord;->lastCachedData:[B

    .line 505
    const/4 v0, 0x0

    sput-object v0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashRecord;->lastFlashRecord:Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashRecord;

    .line 506
    monitor-exit v1

    .line 507
    return-void

    .line 506
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private loadTileFromDataEntry([BLandroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;)Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;
    .locals 9
    .param p1, "recordBlock"    # [B
    .param p2, "desiredTile"    # Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;

    .prologue
    const/4 v7, 0x0

    .line 323
    const/4 v6, 0x0

    .line 325
    .local v6, "result":Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;
    if-eqz p1, :cond_0

    array-length v8, p1

    if-nez v8, :cond_1

    .line 363
    :cond_0
    :goto_0
    return-object v7

    .line 331
    :cond_1
    :try_start_0
    invoke-static {p1}, Landroid_maps_conflict_avoidance/com/google/common/io/IoUtil;->createDataInputFromBytes([B)Ljava/io/DataInput;

    move-result-object v1

    .line 332
    .local v1, "dis":Ljava/io/DataInput;
    invoke-interface {v1}, Ljava/io/DataInput;->readUnsignedByte()I

    move-result v3

    .line 334
    .local v3, "entries":I
    invoke-virtual {p0}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashRecord;->numEntries()I

    move-result v8

    if-ne v3, v8, :cond_0

    .line 339
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_1
    if-ge v5, v3, :cond_3

    .line 340
    invoke-static {v1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->read(Ljava/io/DataInput;)Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;

    move-result-object v0

    .line 342
    .local v0, "currentTile":Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;
    iget-boolean v8, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashRecord;->unverified:Z

    if-eqz v8, :cond_2

    .line 343
    new-instance v4, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashEntry;

    invoke-direct {v4, v0}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashEntry;-><init>(Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;)V

    .line 344
    .local v4, "flashEntry":Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashEntry;
    iget-object v8, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashRecord;->flashEntries:Ljava/util/Vector;

    invoke-virtual {v8, v5}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v4, v8}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashEntry;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 350
    .end local v4    # "flashEntry":Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashEntry;
    :cond_2
    invoke-virtual {v0}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->getLocation()Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;

    move-result-object v8

    invoke-virtual {v8, p2}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 351
    move-object v6, v0

    .line 352
    iget-boolean v8, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashRecord;->unverified:Z

    if-nez v8, :cond_4

    .line 358
    .end local v0    # "currentTile":Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;
    :cond_3
    const/4 v7, 0x0

    iput-boolean v7, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashRecord;->unverified:Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v1    # "dis":Ljava/io/DataInput;
    .end local v3    # "entries":I
    .end local v5    # "i":I
    :goto_2
    move-object v7, v6

    .line 363
    goto :goto_0

    .line 339
    .restart local v0    # "currentTile":Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;
    .restart local v1    # "dis":Ljava/io/DataInput;
    .restart local v3    # "entries":I
    .restart local v5    # "i":I
    :cond_4
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 359
    .end local v0    # "currentTile":Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;
    .end local v1    # "dis":Ljava/io/DataInput;
    .end local v3    # "entries":I
    .end local v5    # "i":I
    :catch_0
    move-exception v2

    .line 360
    .local v2, "e":Ljava/io/IOException;
    const-string v7, "FLASH"

    invoke-static {v7, v2}, Landroid_maps_conflict_avoidance/com/google/common/Log;->logQuietThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2
.end method

.method public static readFromCatalog(Ljava/io/DataInput;)Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashRecord;
    .locals 7
    .param p0, "is"    # Ljava/io/DataInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 267
    invoke-interface {p0}, Ljava/io/DataInput;->readUnsignedByte()I

    move-result v0

    .line 268
    .local v0, "entries":I
    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v4

    .line 269
    .local v4, "recordIndex":I
    new-instance v3, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashRecord;

    invoke-direct {v3}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashRecord;-><init>()V

    .line 271
    .local v3, "flashRecord":Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashRecord;
    const/4 v1, 0x0

    .local v1, "entry":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 272
    invoke-static {p0}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashEntry;->readFromCatalog(Ljava/io/DataInput;)Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashEntry;

    move-result-object v2

    .line 273
    .local v2, "flashEntry":Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashEntry;
    invoke-virtual {v3, v2}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashRecord;->addEntry(Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashEntry;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 274
    new-instance v5, Ljava/io/IOException;

    const-string v6, "FlashRecord full"

    invoke-direct {v5, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 271
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 277
    .end local v2    # "flashEntry":Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashEntry;
    :cond_1
    const/4 v5, 0x1

    iput-boolean v5, v3, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashRecord;->unverified:Z

    .line 278
    iput v4, v3, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashRecord;->recordId:I

    .line 280
    return-object v3
.end method


# virtual methods
.method public addEntry(Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashEntry;)Z
    .locals 2
    .param p1, "entry"    # Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashEntry;

    .prologue
    .line 102
    invoke-virtual {p0}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashRecord;->numEntries()I

    move-result v0

    const/16 v1, 0xff

    if-ge v0, v1, :cond_0

    iget v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashRecord;->recordId:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    .line 104
    :cond_0
    const/4 v0, 0x0

    .line 109
    :goto_0
    return v0

    .line 107
    :cond_1
    iget-object v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashRecord;->flashEntries:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 108
    invoke-virtual {p1, p0}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashEntry;->setFlashRecord(Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashRecord;)V

    .line 109
    const/4 v0, 0x1

    goto :goto_0
.end method

.method createDataEntry(Ljava/util/Hashtable;)[B
    .locals 7
    .param p1, "tileMap"    # Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 378
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    const v6, 0x11940

    invoke-direct {v0, v6}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 380
    .local v0, "baos":Ljava/io/ByteArrayOutputStream;
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 381
    .local v1, "dos":Ljava/io/DataOutputStream;
    invoke-virtual {p0}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashRecord;->numEntries()I

    move-result v5

    .line 383
    .local v5, "numEntries":I
    invoke-virtual {v1, v5}, Ljava/io/DataOutputStream;->writeByte(I)V

    .line 384
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v5, :cond_1

    .line 385
    invoke-virtual {p0, v3}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashRecord;->getEntry(I)Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashEntry;

    move-result-object v2

    .line 386
    .local v2, "entry":Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashEntry;
    invoke-virtual {v2}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashEntry;->getTile()Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;

    move-result-object v6

    invoke-virtual {p1, v6}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;

    .line 387
    .local v4, "mapTile":Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;
    if-nez v4, :cond_0

    .line 388
    const/4 v6, 0x0

    .line 393
    .end local v2    # "entry":Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashEntry;
    .end local v4    # "mapTile":Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;
    :goto_1
    return-object v6

    .line 390
    .restart local v2    # "entry":Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashEntry;
    .restart local v4    # "mapTile":Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;
    :cond_0
    invoke-virtual {v4, v1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;->write(Ljava/io/DataOutput;)V

    .line 384
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 393
    .end local v2    # "entry":Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashEntry;
    .end local v4    # "mapTile":Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;
    :cond_1
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v6

    goto :goto_1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 9
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 226
    if-ne p0, p1, :cond_1

    .line 252
    :cond_0
    :goto_0
    return v5

    .line 229
    :cond_1
    instance-of v7, p1, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashRecord;

    if-nez v7, :cond_2

    move v5, v6

    .line 230
    goto :goto_0

    :cond_2
    move-object v1, p1

    .line 233
    check-cast v1, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashRecord;

    .line 235
    .local v1, "flashRecord":Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashRecord;
    iget v7, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashRecord;->recordId:I

    iget v8, v1, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashRecord;->recordId:I

    if-eq v7, v8, :cond_3

    move v5, v6

    .line 236
    goto :goto_0

    .line 239
    :cond_3
    invoke-virtual {p0}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashRecord;->numEntries()I

    move-result v3

    .line 240
    .local v3, "numEntries":I
    invoke-virtual {v1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashRecord;->numEntries()I

    move-result v7

    if-eq v3, v7, :cond_4

    move v5, v6

    .line 241
    goto :goto_0

    .line 244
    :cond_4
    iget-object v4, v1, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashRecord;->flashEntries:Ljava/util/Vector;

    .line 245
    .local v4, "otherEntries":Ljava/util/Vector;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v3, :cond_0

    .line 246
    iget-object v7, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashRecord;->flashEntries:Ljava/util/Vector;

    invoke-virtual {v7, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashEntry;

    .line 247
    .local v0, "flashEntry":Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashEntry;
    invoke-virtual {v4, v2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v0, v7}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashEntry;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_5

    move v5, v6

    .line 248
    goto :goto_0

    .line 245
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public getDataSize()I
    .locals 4

    .prologue
    .line 147
    iget-object v3, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashRecord;->flashEntries:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v1

    .line 148
    .local v1, "numEntries":I
    const/4 v2, 0x1

    .line 150
    .local v2, "size":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 151
    invoke-virtual {p0, v0}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashRecord;->getEntry(I)Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashEntry;

    move-result-object v3

    invoke-virtual {v3}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashEntry;->getByteSize()I

    move-result v3

    add-int/2addr v2, v3

    .line 150
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 154
    :cond_0
    return v2
.end method

.method public getEntry(I)Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashEntry;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 131
    iget-object v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashRecord;->flashEntries:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashEntry;

    return-object v0
.end method

.method public getEntry(Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;)Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashEntry;
    .locals 3
    .param p1, "location"    # Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;

    .prologue
    .line 117
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashRecord;->flashEntries:Ljava/util/Vector;

    invoke-virtual {v2}, Ljava/util/Vector;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 118
    invoke-virtual {p0, v1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashRecord;->getEntry(I)Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashEntry;

    move-result-object v0

    .line 119
    .local v0, "entry":Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashEntry;
    invoke-virtual {v0}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashEntry;->getTile()Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 124
    .end local v0    # "entry":Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashEntry;
    :goto_1
    return-object v0

    .line 117
    .restart local v0    # "entry":Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashEntry;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 124
    .end local v0    # "entry":Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashEntry;
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public getRecordId()I
    .locals 1

    .prologue
    .line 163
    iget v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashRecord;->recordId:I

    return v0
.end method

.method public getScore(J)J
    .locals 9
    .param p1, "currentTime"    # J

    .prologue
    .line 190
    iget-object v3, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashRecord;->flashEntries:Ljava/util/Vector;

    invoke-virtual {v3}, Ljava/util/Vector;->size()I

    move-result v2

    .line 191
    .local v2, "numEntries":I
    const-wide/16 v4, 0x0

    .line 193
    .local v4, "scores":J
    if-lez v2, :cond_1

    .line 194
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 195
    invoke-virtual {p0, v1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashRecord;->getEntry(I)Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashEntry;

    move-result-object v0

    .line 196
    .local v0, "entry":Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashEntry;
    invoke-virtual {v0}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashEntry;->getTile()Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;

    move-result-object v3

    invoke-virtual {v0}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashEntry;->getLastAccessTime()J

    move-result-wide v6

    invoke-static {v3, p1, p2, v6, v7}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapService;->getScore(Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;JJ)J

    move-result-wide v6

    add-long/2addr v4, v6

    .line 194
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 204
    .end local v0    # "entry":Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashEntry;
    :cond_0
    int-to-long v6, v2

    div-long v6, v4, v6

    .line 207
    .end local v1    # "i":I
    :goto_1
    return-wide v6

    :cond_1
    const-wide v6, 0x7fffffffffffffffL

    goto :goto_1
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 257
    iget v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashRecord;->recordId:I

    return v0
.end method

.method isSaved()Z
    .locals 2

    .prologue
    .line 170
    iget v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashRecord;->recordId:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public loadTile(Ljava/lang/String;Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;)Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;
    .locals 4
    .param p1, "blockName"    # Ljava/lang/String;
    .param p2, "desiredTile"    # Landroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;

    .prologue
    const/4 v1, 0x0

    .line 414
    iget v2, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashRecord;->recordId:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_1

    .line 445
    :cond_0
    :goto_0
    return-object v1

    .line 418
    :cond_1
    const/4 v0, 0x0

    .line 422
    .local v0, "recordBlock":[B
    sget-object v2, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashRecord;->lastCacheDataLock:Ljava/lang/Object;

    monitor-enter v2

    .line 423
    :try_start_0
    sget-object v3, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashRecord;->lastFlashRecord:Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashRecord;

    invoke-virtual {p0, v3}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashRecord;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 424
    sget-object v0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashRecord;->lastCachedData:[B

    .line 426
    :cond_2
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 428
    if-nez v0, :cond_3

    .line 429
    invoke-static {}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getInstance()Landroid_maps_conflict_avoidance/com/google/common/Config;

    move-result-object v2

    invoke-virtual {v2}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getPersistentStore()Landroid_maps_conflict_avoidance/com/google/common/io/PersistentStore;

    move-result-object v2

    invoke-interface {v2, p1}, Landroid_maps_conflict_avoidance/com/google/common/io/PersistentStore;->readBlock(Ljava/lang/String;)[B

    move-result-object v0

    .line 432
    if-eqz v0, :cond_3

    array-length v2, v0

    if-eqz v2, :cond_3

    .line 433
    sget-object v2, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashRecord;->lastCacheDataLock:Ljava/lang/Object;

    monitor-enter v2

    .line 434
    :try_start_1
    sput-object v0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashRecord;->lastCachedData:[B

    .line 435
    sput-object p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashRecord;->lastFlashRecord:Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashRecord;

    .line 436
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 440
    :cond_3
    if-eqz v0, :cond_0

    .line 445
    invoke-direct {p0, v0, p2}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashRecord;->loadTileFromDataEntry([BLandroid_maps_conflict_avoidance/com/google/googlenav/map/Tile;)Landroid_maps_conflict_avoidance/com/google/googlenav/map/MapTile;

    move-result-object v1

    goto :goto_0

    .line 426
    :catchall_0
    move-exception v1

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 436
    :catchall_1
    move-exception v1

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v1
.end method

.method public numEntries()I
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashRecord;->flashEntries:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    return v0
.end method

.method setUnsaved()V
    .locals 1

    .prologue
    .line 179
    const/4 v0, -0x1

    iput v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashRecord;->recordId:I

    .line 180
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 221
    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method writeRecord(Ljava/lang/String;I[B)V
    .locals 2
    .param p1, "blockName"    # Ljava/lang/String;
    .param p2, "recordId"    # I
    .param p3, "recordData"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid_maps_conflict_avoidance/com/google/common/io/PersistentStore$PersistentStoreException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 463
    iget v0, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashRecord;->recordId:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 464
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "already saved"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 468
    :cond_0
    invoke-static {}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getInstance()Landroid_maps_conflict_avoidance/com/google/common/Config;

    move-result-object v0

    invoke-virtual {v0}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getPersistentStore()Landroid_maps_conflict_avoidance/com/google/common/io/PersistentStore;

    move-result-object v0

    invoke-interface {v0, p3, p1}, Landroid_maps_conflict_avoidance/com/google/common/io/PersistentStore;->writeBlockX([BLjava/lang/String;)I

    .line 471
    iput p2, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashRecord;->recordId:I

    .line 472
    return-void
.end method

.method public writeToCatalog(Ljava/io/DataOutput;)V
    .locals 5
    .param p1, "dos"    # Ljava/io/DataOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 293
    iget v3, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashRecord;->recordId:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_0

    .line 294
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "Can\'t write unsaved FlashRecord"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 297
    :cond_0
    invoke-virtual {p0}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashRecord;->numEntries()I

    move-result v2

    .line 299
    .local v2, "numEntries":I
    invoke-interface {p1, v2}, Ljava/io/DataOutput;->writeByte(I)V

    .line 300
    iget v3, p0, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashRecord;->recordId:I

    invoke-interface {p1, v3}, Ljava/io/DataOutput;->writeInt(I)V

    .line 301
    const/4 v1, 0x0

    .local v1, "index":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 302
    invoke-virtual {p0, v1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashRecord;->getEntry(I)Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashEntry;

    move-result-object v0

    .line 303
    .local v0, "entry":Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashEntry;
    invoke-virtual {v0, p1}, Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashEntry;->writeToCatalog(Ljava/io/DataOutput;)V

    .line 301
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 305
    .end local v0    # "entry":Landroid_maps_conflict_avoidance/com/google/googlenav/map/FlashEntry;
    :cond_1
    return-void
.end method
