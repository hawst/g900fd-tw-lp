.class public Landroid_maps_conflict_avoidance/com/google/common/io/IoUtil;
.super Ljava/lang/Object;
.source "IoUtil.java"


# static fields
.field private static final HEX_DIGITS:[C


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const/16 v0, 0x10

    new-array v0, v0, [C

    fill-array-data v0, :array_0

    sput-object v0, Landroid_maps_conflict_avoidance/com/google/common/io/IoUtil;->HEX_DIGITS:[C

    return-void

    :array_0
    .array-data 2
        0x30s
        0x31s
        0x32s
        0x33s
        0x34s
        0x35s
        0x36s
        0x37s
        0x38s
        0x39s
        0x41s
        0x42s
        0x43s
        0x44s
        0x45s
        0x46s
    .end array-data
.end method

.method public static createDataInputFromBytes([B)Ljava/io/DataInput;
    .locals 1
    .param p0, "bytes"    # [B

    .prologue
    .line 941
    new-instance v0, Landroid_maps_conflict_avoidance/com/google/common/io/ByteArrayDataInput;

    invoke-direct {v0, p0}, Landroid_maps_conflict_avoidance/com/google/common/io/ByteArrayDataInput;-><init>([B)V

    return-object v0
.end method

.method public static decodeUtf8([BIIZ)Ljava/lang/String;
    .locals 12
    .param p0, "data"    # [B
    .param p1, "start"    # I
    .param p2, "end"    # I
    .param p3, "tolerant"    # Z

    .prologue
    .line 545
    new-instance v9, Ljava/lang/StringBuffer;

    sub-int v10, p2, p1

    invoke-direct {v9, v10}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 546
    .local v9, "sb":Ljava/lang/StringBuffer;
    move v7, p1

    .local v7, "pos":I
    move v8, v7

    .line 548
    .end local v7    # "pos":I
    .local v8, "pos":I
    :goto_0
    if-ge v8, p2, :cond_c

    .line 549
    add-int/lit8 v7, v8, 0x1

    .end local v8    # "pos":I
    .restart local v7    # "pos":I
    aget-byte v10, p0, v8

    and-int/lit16 v0, v10, 0xff

    .line 550
    .local v0, "b":I
    const/16 v10, 0x7f

    if-gt v0, v10, :cond_0

    .line 551
    int-to-char v10, v0

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    :goto_1
    move v8, v7

    .line 598
    .end local v7    # "pos":I
    .restart local v8    # "pos":I
    goto :goto_0

    .line 552
    .end local v8    # "pos":I
    .restart local v7    # "pos":I
    :cond_0
    const/16 v10, 0xf5

    if-lt v0, v10, :cond_2

    .line 553
    if-nez p3, :cond_1

    .line 554
    new-instance v10, Ljava/lang/IllegalArgumentException;

    const-string v11, "Invalid UTF8"

    invoke-direct {v10, v11}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 556
    :cond_1
    int-to-char v10, v0

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 558
    :cond_2
    const/16 v1, 0xe0

    .line 559
    .local v1, "border":I
    const/4 v3, 0x1

    .line 560
    .local v3, "count":I
    const/16 v6, 0x80

    .line 561
    .local v6, "minCode":I
    const/16 v5, 0x1f

    .line 562
    .local v5, "mask":I
    :goto_2
    if-lt v0, v1, :cond_4

    .line 563
    shr-int/lit8 v10, v1, 0x1

    or-int/lit16 v1, v10, 0x80

    .line 564
    const/4 v10, 0x1

    if-ne v3, v10, :cond_3

    const/4 v10, 0x4

    :goto_3
    shl-int/2addr v6, v10

    .line 565
    add-int/lit8 v3, v3, 0x1

    .line 566
    shr-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 564
    :cond_3
    const/4 v10, 0x5

    goto :goto_3

    .line 568
    :cond_4
    and-int v2, v0, v5

    .line 570
    .local v2, "code":I
    const/4 v4, 0x0

    .local v4, "i":I
    move v8, v7

    .end local v7    # "pos":I
    .restart local v8    # "pos":I
    :goto_4
    if-ge v4, v3, :cond_7

    .line 571
    shl-int/lit8 v2, v2, 0x6

    .line 572
    if-lt v8, p2, :cond_5

    .line 573
    if-nez p3, :cond_d

    .line 574
    new-instance v10, Ljava/lang/IllegalArgumentException;

    const-string v11, "Invalid UTF8"

    invoke-direct {v10, v11}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 578
    :cond_5
    if-nez p3, :cond_6

    aget-byte v10, p0, v8

    and-int/lit16 v10, v10, 0xc0

    const/16 v11, 0x80

    if-eq v10, v11, :cond_6

    .line 579
    new-instance v10, Ljava/lang/IllegalArgumentException;

    const-string v11, "Invalid UTF8"

    invoke-direct {v10, v11}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 581
    :cond_6
    add-int/lit8 v7, v8, 0x1

    .end local v8    # "pos":I
    .restart local v7    # "pos":I
    aget-byte v10, p0, v8

    and-int/lit8 v10, v10, 0x3f

    or-int/2addr v2, v10

    .line 570
    :goto_5
    add-int/lit8 v4, v4, 0x1

    move v8, v7

    .end local v7    # "pos":I
    .restart local v8    # "pos":I
    goto :goto_4

    .line 586
    :cond_7
    if-nez p3, :cond_8

    if-lt v2, v6, :cond_9

    :cond_8
    const v10, 0xd800

    if-lt v2, v10, :cond_a

    const v10, 0xdfff

    if-gt v2, v10, :cond_a

    .line 587
    :cond_9
    new-instance v10, Ljava/lang/IllegalArgumentException;

    const-string v11, "Invalid UTF8"

    invoke-direct {v10, v11}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 590
    :cond_a
    const v10, 0xffff

    if-gt v2, v10, :cond_b

    .line 591
    int-to-char v10, v2

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move v7, v8

    .end local v8    # "pos":I
    .restart local v7    # "pos":I
    goto :goto_1

    .line 593
    .end local v7    # "pos":I
    .restart local v8    # "pos":I
    :cond_b
    const/high16 v10, 0x10000

    sub-int/2addr v2, v10

    .line 594
    const v10, 0xd800

    shr-int/lit8 v11, v2, 0xa

    or-int/2addr v10, v11

    int-to-char v10, v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 595
    const v10, 0xdc00

    and-int/lit16 v11, v2, 0x3ff

    or-int/2addr v10, v11

    int-to-char v10, v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move v7, v8

    .end local v8    # "pos":I
    .restart local v7    # "pos":I
    goto/16 :goto_1

    .line 599
    .end local v0    # "b":I
    .end local v1    # "border":I
    .end local v2    # "code":I
    .end local v3    # "count":I
    .end local v4    # "i":I
    .end local v5    # "mask":I
    .end local v6    # "minCode":I
    .end local v7    # "pos":I
    .restart local v8    # "pos":I
    :cond_c
    invoke-virtual {v9}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    return-object v10

    .restart local v0    # "b":I
    .restart local v1    # "border":I
    .restart local v2    # "code":I
    .restart local v3    # "count":I
    .restart local v4    # "i":I
    .restart local v5    # "mask":I
    .restart local v6    # "minCode":I
    :cond_d
    move v7, v8

    .end local v8    # "pos":I
    .restart local v7    # "pos":I
    goto :goto_5
.end method

.method public static encodeUtf8(Ljava/lang/String;[BI)I
    .locals 9
    .param p0, "s"    # Ljava/lang/String;
    .param p1, "buf"    # [B
    .param p2, "pos"    # I

    .prologue
    const v8, 0xd800

    const v7, 0xfc00

    .line 472
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    .line 473
    .local v4, "len":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v4, :cond_9

    .line 474
    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 477
    .local v0, "code":I
    if-lt v0, v8, :cond_0

    const v5, 0xdfff

    if-gt v0, v5, :cond_0

    add-int/lit8 v5, v3, 0x1

    if-ge v5, v4, :cond_0

    .line 478
    add-int/lit8 v5, v3, 0x1

    invoke-virtual {p0, v5}, Ljava/lang/String;->charAt(I)C

    move-result v2

    .line 483
    .local v2, "codeLo":I
    and-int v5, v2, v7

    and-int v6, v0, v7

    xor-int/2addr v5, v6

    const/16 v6, 0x400

    if-ne v5, v6, :cond_0

    .line 485
    add-int/lit8 v3, v3, 0x1

    .line 488
    and-int v5, v2, v7

    if-ne v5, v8, :cond_2

    .line 489
    move v1, v2

    .line 490
    .local v1, "codeHi":I
    move v2, v0

    .line 494
    :goto_1
    and-int/lit16 v5, v1, 0x3ff

    shl-int/lit8 v5, v5, 0xa

    and-int/lit16 v6, v2, 0x3ff

    or-int/2addr v5, v6

    const/high16 v6, 0x10000

    add-int v0, v5, v6

    .line 497
    .end local v1    # "codeHi":I
    .end local v2    # "codeLo":I
    :cond_0
    const/16 v5, 0x7f

    if-gt v0, v5, :cond_3

    .line 498
    if-eqz p1, :cond_1

    .line 499
    int-to-byte v5, v0

    aput-byte v5, p1, p2

    .line 501
    :cond_1
    add-int/lit8 p2, p2, 0x1

    .line 473
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 492
    .restart local v2    # "codeLo":I
    :cond_2
    move v1, v0

    .restart local v1    # "codeHi":I
    goto :goto_1

    .line 502
    .end local v1    # "codeHi":I
    .end local v2    # "codeLo":I
    :cond_3
    const/16 v5, 0x7ff

    if-gt v0, v5, :cond_5

    .line 504
    if-eqz p1, :cond_4

    .line 505
    shr-int/lit8 v5, v0, 0x6

    or-int/lit16 v5, v5, 0xc0

    int-to-byte v5, v5

    aput-byte v5, p1, p2

    .line 506
    add-int/lit8 v5, p2, 0x1

    and-int/lit8 v6, v0, 0x3f

    or-int/lit16 v6, v6, 0x80

    int-to-byte v6, v6

    aput-byte v6, p1, v5

    .line 508
    :cond_4
    add-int/lit8 p2, p2, 0x2

    goto :goto_2

    .line 509
    :cond_5
    const v5, 0xffff

    if-gt v0, v5, :cond_7

    .line 511
    if-eqz p1, :cond_6

    .line 512
    shr-int/lit8 v5, v0, 0xc

    or-int/lit16 v5, v5, 0xe0

    int-to-byte v5, v5

    aput-byte v5, p1, p2

    .line 513
    add-int/lit8 v5, p2, 0x1

    shr-int/lit8 v6, v0, 0x6

    and-int/lit8 v6, v6, 0x3f

    or-int/lit16 v6, v6, 0x80

    int-to-byte v6, v6

    aput-byte v6, p1, v5

    .line 514
    add-int/lit8 v5, p2, 0x2

    and-int/lit8 v6, v0, 0x3f

    or-int/lit16 v6, v6, 0x80

    int-to-byte v6, v6

    aput-byte v6, p1, v5

    .line 516
    :cond_6
    add-int/lit8 p2, p2, 0x3

    goto :goto_2

    .line 518
    :cond_7
    if-eqz p1, :cond_8

    .line 519
    shr-int/lit8 v5, v0, 0x12

    or-int/lit16 v5, v5, 0xf0

    int-to-byte v5, v5

    aput-byte v5, p1, p2

    .line 520
    add-int/lit8 v5, p2, 0x1

    shr-int/lit8 v6, v0, 0xc

    and-int/lit8 v6, v6, 0x3f

    or-int/lit16 v6, v6, 0x80

    int-to-byte v6, v6

    aput-byte v6, p1, v5

    .line 521
    add-int/lit8 v5, p2, 0x2

    shr-int/lit8 v6, v0, 0x6

    and-int/lit8 v6, v6, 0x3f

    or-int/lit16 v6, v6, 0x80

    int-to-byte v6, v6

    aput-byte v6, p1, v5

    .line 522
    add-int/lit8 v5, p2, 0x3

    and-int/lit8 v6, v0, 0x3f

    or-int/lit16 v6, v6, 0x80

    int-to-byte v6, v6

    aput-byte v6, p1, v5

    .line 524
    :cond_8
    add-int/lit8 p2, p2, 0x4

    goto :goto_2

    .line 528
    .end local v0    # "code":I
    :cond_9
    return p2
.end method

.method public static encodeUtf8(Ljava/lang/String;)[B
    .locals 4
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 456
    const/4 v2, 0x0

    invoke-static {p0, v2, v3}, Landroid_maps_conflict_avoidance/com/google/common/io/IoUtil;->encodeUtf8(Ljava/lang/String;[BI)I

    move-result v0

    .line 457
    .local v0, "len":I
    new-array v1, v0, [B

    .line 458
    .local v1, "result":[B
    invoke-static {p0, v1, v3}, Landroid_maps_conflict_avoidance/com/google/common/io/IoUtil;->encodeUtf8(Ljava/lang/String;[BI)I

    .line 459
    return-object v1
.end method

.method public static inflate([BIII)[B
    .locals 10
    .param p0, "deflatedData"    # [B
    .param p1, "offset"    # I
    .param p2, "length"    # I
    .param p3, "inflatedDataSize"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 958
    add-int/lit8 v6, p2, 0x1

    new-array v0, v6, [B

    .line 959
    .local v0, "compressedDataWithDummyByte":[B
    invoke-static {p0, p1, v0, v9, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 960
    invoke-static {}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getInstance()Landroid_maps_conflict_avoidance/com/google/common/Config;

    move-result-object v6

    new-instance v7, Ljava/io/ByteArrayInputStream;

    add-int/lit8 v8, p2, 0x1

    invoke-direct {v7, v0, v9, v8}, Ljava/io/ByteArrayInputStream;-><init>([BII)V

    invoke-virtual {v6, v7}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getInflaterInputStream(Ljava/io/InputStream;)Ljava/io/InputStream;

    move-result-object v3

    .line 964
    .local v3, "is":Ljava/io/InputStream;
    new-array v2, p3, [B

    .line 966
    .local v2, "decompressedData":[B
    move v5, p3

    .line 967
    .local v5, "numBytesRemaining":I
    const/4 v4, 0x0

    .line 971
    .local v4, "numBytesRead":I
    :goto_0
    if-lez v5, :cond_0

    :try_start_0
    invoke-virtual {v3, v2, v4, v5}, Ljava/io/InputStream;->read([BII)I

    move-result v1

    .local v1, "currentReadCount":I
    const/4 v6, -0x1

    if-eq v1, v6, :cond_0

    .line 972
    sub-int/2addr v5, v1

    .line 973
    add-int/2addr v4, v1

    goto :goto_0

    .line 975
    .end local v1    # "currentReadCount":I
    :cond_0
    if-eqz v5, :cond_1

    .line 976
    new-instance v6, Ljava/io/IOException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Failed to read ["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "] bytes, but only read ["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 980
    :catchall_0
    move-exception v6

    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    throw v6

    :cond_1
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    .line 982
    return-object v2
.end method
