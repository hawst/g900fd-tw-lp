.class public Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;
.super Ljava/lang/Object;
.source "ProtoBuf.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf$1;,
        Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf$SimpleCounter;
    }
.end annotation


# static fields
.field public static final EMPTY_BYTE_ARRAY:[B

.field public static final FALSE:Ljava/lang/Boolean;

.field private static final NULL_COUNTER:Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf$SimpleCounter;

.field public static final TRUE:Ljava/lang/Boolean;


# instance fields
.field private cachedSize:I

.field private msgType:Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBufType;

.field private final values:Landroid_maps_conflict_avoidance/com/google/common/util/IntMap;

.field private wireTypes:Landroid_maps_conflict_avoidance/com/google/common/util/IntMap;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 52
    new-instance v0, Ljava/lang/Boolean;

    invoke-direct {v0, v2}, Ljava/lang/Boolean;-><init>(Z)V

    sput-object v0, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->FALSE:Ljava/lang/Boolean;

    .line 53
    new-instance v0, Ljava/lang/Boolean;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/lang/Boolean;-><init>(Z)V

    sput-object v0, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->TRUE:Ljava/lang/Boolean;

    .line 54
    new-array v0, v2, [B

    sput-object v0, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->EMPTY_BYTE_ARRAY:[B

    .line 381
    new-instance v0, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf$SimpleCounter;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf$SimpleCounter;-><init>(Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf$1;)V

    sput-object v0, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->NULL_COUNTER:Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf$SimpleCounter;

    return-void
.end method

.method public constructor <init>(Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBufType;)V
    .locals 1
    .param p1, "type"    # Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBufType;

    .prologue
    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    const/high16 v0, -0x80000000

    iput v0, p0, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->cachedSize:I

    .line 96
    iput-object p1, p0, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->msgType:Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBufType;

    .line 97
    new-instance v0, Landroid_maps_conflict_avoidance/com/google/common/util/IntMap;

    invoke-direct {v0}, Landroid_maps_conflict_avoidance/com/google/common/util/IntMap;-><init>()V

    iput-object v0, p0, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->values:Landroid_maps_conflict_avoidance/com/google/common/util/IntMap;

    .line 98
    return-void
.end method

.method private addObject(ILjava/lang/Object;)V
    .locals 2
    .param p1, "tag"    # I
    .param p2, "o"    # Ljava/lang/Object;

    .prologue
    .line 1249
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, p2, v1}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->insertObject(IILjava/lang/Object;Z)V

    .line 1250
    return-void
.end method

.method private assertTypeMatch(ILjava/lang/Object;)V
    .locals 0
    .param p1, "tag"    # I
    .param p2, "object"    # Ljava/lang/Object;

    .prologue
    .line 1009
    return-void
.end method

.method private static checkTag(I)V
    .locals 0
    .param p0, "tag"    # I

    .prologue
    .line 1101
    return-void
.end method

.method private static convert(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 6
    .param p0, "obj"    # Ljava/lang/Object;
    .param p1, "tagType"    # I

    .prologue
    .line 1257
    packed-switch p1, :pswitch_data_0

    .line 1318
    :pswitch_0
    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "Unsupp.Type"

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1262
    :pswitch_1
    instance-of v3, p0, Ljava/lang/Boolean;

    if-eqz v3, :cond_1

    .line 1315
    .end local p0    # "obj":Ljava/lang/Object;
    :cond_0
    :goto_0
    :pswitch_2
    return-object p0

    .line 1265
    .restart local p0    # "obj":Ljava/lang/Object;
    :cond_1
    check-cast p0, Ljava/lang/Long;

    .end local p0    # "obj":Ljava/lang/Object;
    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    long-to-int v3, v4

    packed-switch v3, :pswitch_data_1

    .line 1271
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Type mismatch"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1267
    :pswitch_3
    sget-object p0, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->FALSE:Ljava/lang/Boolean;

    goto :goto_0

    .line 1269
    :pswitch_4
    sget-object p0, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->TRUE:Ljava/lang/Boolean;

    goto :goto_0

    .line 1281
    .restart local p0    # "obj":Ljava/lang/Object;
    :pswitch_5
    instance-of v3, p0, Ljava/lang/Boolean;

    if-eqz v3, :cond_0

    .line 1282
    check-cast p0, Ljava/lang/Boolean;

    .end local p0    # "obj":Ljava/lang/Object;
    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_2

    const-wide/16 v4, 0x1

    :goto_1
    invoke-static {v4, v5}, Landroid_maps_conflict_avoidance/com/google/common/util/Primitives;->toLong(J)Ljava/lang/Long;

    move-result-object p0

    goto :goto_0

    :cond_2
    const-wide/16 v4, 0x0

    goto :goto_1

    .line 1287
    .restart local p0    # "obj":Ljava/lang/Object;
    :pswitch_6
    instance-of v3, p0, Ljava/lang/String;

    if-eqz v3, :cond_3

    .line 1288
    check-cast p0, Ljava/lang/String;

    .end local p0    # "obj":Ljava/lang/Object;
    invoke-static {p0}, Landroid_maps_conflict_avoidance/com/google/common/io/IoUtil;->encodeUtf8(Ljava/lang/String;)[B

    move-result-object p0

    goto :goto_0

    .line 1289
    .restart local p0    # "obj":Ljava/lang/Object;
    :cond_3
    instance-of v3, p0, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;

    if-eqz v3, :cond_0

    .line 1290
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 1292
    .local v0, "buf":Ljava/io/ByteArrayOutputStream;
    :try_start_0
    check-cast p0, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;

    .end local p0    # "obj":Ljava/lang/Object;
    invoke-virtual {p0, v0}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->outputTo(Ljava/io/OutputStream;)V

    .line 1293
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p0

    goto :goto_0

    .line 1294
    :catch_0
    move-exception v2

    .line 1295
    .local v2, "e":Ljava/io/IOException;
    new-instance v3, Ljava/lang/RuntimeException;

    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1301
    .end local v0    # "buf":Ljava/io/ByteArrayOutputStream;
    .end local v2    # "e":Ljava/io/IOException;
    .restart local p0    # "obj":Ljava/lang/Object;
    :pswitch_7
    instance-of v3, p0, [B

    if-eqz v3, :cond_0

    .line 1302
    check-cast p0, [B

    .end local p0    # "obj":Ljava/lang/Object;
    move-object v1, p0

    check-cast v1, [B

    .line 1303
    .local v1, "data":[B
    const/4 v3, 0x0

    array-length v4, v1

    const/4 v5, 0x1

    invoke-static {v1, v3, v4, v5}, Landroid_maps_conflict_avoidance/com/google/common/io/IoUtil;->decodeUtf8([BIIZ)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 1308
    .end local v1    # "data":[B
    .restart local p0    # "obj":Ljava/lang/Object;
    :pswitch_8
    instance-of v3, p0, [B

    if-eqz v3, :cond_0

    .line 1310
    :try_start_1
    new-instance v3, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;

    const/4 v4, 0x0

    invoke-direct {v3, v4}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;-><init>(Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBufType;)V

    check-cast p0, [B

    .end local p0    # "obj":Ljava/lang/Object;
    check-cast p0, [B

    invoke-virtual {v3, p0}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->parse([B)Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object p0

    goto :goto_0

    .line 1311
    :catch_1
    move-exception v2

    .line 1312
    .restart local v2    # "e":Ljava/io/IOException;
    new-instance v3, Ljava/lang/RuntimeException;

    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1257
    :pswitch_data_0
    .packed-switch 0x10
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_1
        :pswitch_6
        :pswitch_8
        :pswitch_8
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch

    .line 1265
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private static getCount(Ljava/lang/Object;)I
    .locals 1
    .param p0, "o"    # Ljava/lang/Object;

    .prologue
    .line 533
    if-nez p0, :cond_0

    const/4 v0, 0x0

    .end local p0    # "o":Ljava/lang/Object;
    :goto_0
    return v0

    .restart local p0    # "o":Ljava/lang/Object;
    :cond_0
    instance-of v0, p0, Ljava/util/Vector;

    if-eqz v0, :cond_1

    check-cast p0, Ljava/util/Vector;

    .end local p0    # "o":Ljava/lang/Object;
    invoke-virtual {p0}, Ljava/util/Vector;->size()I

    move-result v0

    goto :goto_0

    .restart local p0    # "o":Ljava/lang/Object;
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private getDefault(I)Ljava/lang/Object;
    .locals 1
    .param p1, "tag"    # I

    .prologue
    .line 1084
    invoke-virtual {p0, p1}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->getType(I)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 1090
    iget-object v0, p0, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->msgType:Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBufType;

    invoke-virtual {v0, p1}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBufType;->getData(I)Ljava/lang/Object;

    move-result-object v0

    :goto_0
    return-object v0

    .line 1088
    :sswitch_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1084
    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_0
        0x1a -> :sswitch_0
        0x1b -> :sswitch_0
    .end sparse-switch
.end method

.method private getObject(II)Ljava/lang/Object;
    .locals 3
    .param p1, "tag"    # I
    .param p2, "desiredType"    # I

    .prologue
    .line 1110
    invoke-static {p1}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->checkTag(I)V

    .line 1111
    iget-object v2, p0, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->values:Landroid_maps_conflict_avoidance/com/google/common/util/IntMap;

    invoke-virtual {v2, p1}, Landroid_maps_conflict_avoidance/com/google/common/util/IntMap;->get(I)Ljava/lang/Object;

    move-result-object v1

    .line 1112
    .local v1, "o":Ljava/lang/Object;
    invoke-static {v1}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->getCount(Ljava/lang/Object;)I

    move-result v0

    .line 1114
    .local v0, "count":I
    if-nez v0, :cond_0

    .line 1115
    invoke-direct {p0, p1}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->getDefault(I)Ljava/lang/Object;

    move-result-object v2

    .line 1121
    :goto_0
    return-object v2

    .line 1118
    :cond_0
    const/4 v2, 0x1

    if-le v0, v2, :cond_1

    .line 1119
    new-instance v2, Ljava/lang/IllegalArgumentException;

    invoke-direct {v2}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v2

    .line 1121
    :cond_1
    const/4 v2, 0x0

    invoke-direct {p0, p1, v2, p2, v1}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->getObjectWithoutArgChecking(IIILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    goto :goto_0
.end method

.method private getObject(III)Ljava/lang/Object;
    .locals 3
    .param p1, "tag"    # I
    .param p2, "index"    # I
    .param p3, "desiredType"    # I

    .prologue
    .line 1130
    invoke-static {p1}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->checkTag(I)V

    .line 1131
    iget-object v2, p0, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->values:Landroid_maps_conflict_avoidance/com/google/common/util/IntMap;

    invoke-virtual {v2, p1}, Landroid_maps_conflict_avoidance/com/google/common/util/IntMap;->get(I)Ljava/lang/Object;

    move-result-object v1

    .line 1132
    .local v1, "o":Ljava/lang/Object;
    invoke-static {v1}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->getCount(Ljava/lang/Object;)I

    move-result v0

    .line 1134
    .local v0, "count":I
    if-lt p2, v0, :cond_0

    .line 1135
    new-instance v2, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v2}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v2

    .line 1137
    :cond_0
    invoke-direct {p0, p1, p2, p3, v1}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->getObjectWithoutArgChecking(IIILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    return-object v2
.end method

.method private getObjectWithoutArgChecking(IIILjava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p1, "tag"    # I
    .param p2, "index"    # I
    .param p3, "desiredType"    # I
    .param p4, "o"    # Ljava/lang/Object;

    .prologue
    .line 1145
    const/4 v1, 0x0

    .line 1146
    .local v1, "v":Ljava/util/Vector;
    instance-of v2, p4, Ljava/util/Vector;

    if-eqz v2, :cond_0

    move-object v1, p4

    .line 1147
    check-cast v1, Ljava/util/Vector;

    .line 1148
    invoke-virtual {v1, p2}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object p4

    .line 1151
    :cond_0
    invoke-static {p4, p3}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->convert(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    .line 1153
    .local v0, "o2":Ljava/lang/Object;
    if-eq v0, p4, :cond_1

    if-eqz p4, :cond_1

    .line 1154
    if-nez v1, :cond_2

    .line 1155
    invoke-direct {p0, p1, v0}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->setObject(ILjava/lang/Object;)V

    .line 1160
    :cond_1
    :goto_0
    return-object v0

    .line 1157
    :cond_2
    invoke-virtual {v1, v0, p2}, Ljava/util/Vector;->setElementAt(Ljava/lang/Object;I)V

    goto :goto_0
.end method

.method private static getVarIntSize(J)I
    .locals 4
    .param p0, "i"    # J

    .prologue
    .line 666
    const-wide/16 v2, 0x0

    cmp-long v1, p0, v2

    if-gez v1, :cond_1

    .line 667
    const/16 v0, 0xa

    .line 674
    :cond_0
    return v0

    .line 669
    :cond_1
    const/4 v0, 0x1

    .line 670
    .local v0, "size":I
    :goto_0
    const-wide/16 v2, 0x80

    cmp-long v1, p0, v2

    if-ltz v1, :cond_0

    .line 671
    add-int/lit8 v0, v0, 0x1

    .line 672
    const/4 v1, 0x7

    shr-long/2addr p0, v1

    goto :goto_0
.end method

.method private final getWireType(I)I
    .locals 5
    .param p1, "tag"    # I

    .prologue
    const/16 v4, 0x2f

    .line 1170
    invoke-virtual {p0, p1}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->getType(I)I

    move-result v0

    .line 1172
    .local v0, "tagType":I
    packed-switch v0, :pswitch_data_0

    .line 1207
    :pswitch_0
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unsupp.Type:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->msgType:Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBufType;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1189
    :pswitch_1
    const/4 v0, 0x0

    .line 1205
    .end local v0    # "tagType":I
    :goto_0
    :pswitch_2
    return v0

    .line 1195
    .restart local v0    # "tagType":I
    :pswitch_3
    const/4 v0, 0x2

    goto :goto_0

    .line 1199
    :pswitch_4
    const/4 v0, 0x1

    goto :goto_0

    .line 1203
    :pswitch_5
    const/4 v0, 0x5

    goto :goto_0

    .line 1205
    :pswitch_6
    const/4 v0, 0x3

    goto :goto_0

    .line 1172
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_4
        :pswitch_5
        :pswitch_1
        :pswitch_3
        :pswitch_6
        :pswitch_3
        :pswitch_3
        :pswitch_1
        :pswitch_1
        :pswitch_5
        :pswitch_4
        :pswitch_1
        :pswitch_1
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method private insertObject(IILjava/lang/Object;Z)V
    .locals 3
    .param p1, "tag"    # I
    .param p2, "index"    # I
    .param p3, "o"    # Ljava/lang/Object;
    .param p4, "appendToEnd"    # Z

    .prologue
    .line 1225
    invoke-static {p1}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->checkTag(I)V

    .line 1226
    iget-object v2, p0, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->values:Landroid_maps_conflict_avoidance/com/google/common/util/IntMap;

    invoke-virtual {v2, p1}, Landroid_maps_conflict_avoidance/com/google/common/util/IntMap;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 1227
    .local v0, "current":Ljava/lang/Object;
    const/4 v1, 0x0

    .line 1228
    .local v1, "v":Ljava/util/Vector;
    instance-of v2, v0, Ljava/util/Vector;

    if-eqz v2, :cond_0

    move-object v1, v0

    .line 1229
    check-cast v1, Ljava/util/Vector;

    .line 1231
    :cond_0
    if-eqz v0, :cond_1

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v2

    if-nez v2, :cond_2

    .line 1232
    :cond_1
    invoke-direct {p0, p1, p3}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->setObject(ILjava/lang/Object;)V

    .line 1246
    :goto_0
    return-void

    .line 1234
    :cond_2
    invoke-direct {p0, p1, p3}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->assertTypeMatch(ILjava/lang/Object;)V

    .line 1235
    if-nez v1, :cond_3

    .line 1236
    new-instance v1, Ljava/util/Vector;

    .end local v1    # "v":Ljava/util/Vector;
    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    .line 1237
    .restart local v1    # "v":Ljava/util/Vector;
    invoke-virtual {v1, v0}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    .line 1238
    iget-object v2, p0, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->values:Landroid_maps_conflict_avoidance/com/google/common/util/IntMap;

    invoke-virtual {v2, p1, v1}, Landroid_maps_conflict_avoidance/com/google/common/util/IntMap;->put(ILjava/lang/Object;)V

    .line 1240
    :cond_3
    if-eqz p4, :cond_4

    .line 1241
    invoke-virtual {v1, p3}, Ljava/util/Vector;->addElement(Ljava/lang/Object;)V

    goto :goto_0

    .line 1243
    :cond_4
    invoke-virtual {v1, p3, p2}, Ljava/util/Vector;->insertElementAt(Ljava/lang/Object;I)V

    goto :goto_0
.end method

.method private isZigZagEncodedType(I)Z
    .locals 2
    .param p1, "tag"    # I

    .prologue
    .line 849
    invoke-virtual {p0, p1}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->getType(I)I

    move-result v0

    .line 850
    .local v0, "declaredType":I
    const/16 v1, 0x21

    if-eq v0, v1, :cond_0

    const/16 v1, 0x22

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private outputField(ILandroid_maps_conflict_avoidance/com/google/common/io/MarkedOutputStream;)I
    .locals 22
    .param p1, "tag"    # I
    .param p2, "os"    # Landroid_maps_conflict_avoidance/com/google/common/io/MarkedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 763
    invoke-virtual/range {p0 .. p1}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v12

    .line 764
    .local v12, "size":I
    invoke-direct/range {p0 .. p1}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->getWireType(I)I

    move-result v15

    .line 765
    .local v15, "wireType":I
    shl-int/lit8 v19, p1, 0x3

    or-int v18, v19, v15

    .line 768
    .local v18, "wireTypeTag":I
    const/4 v14, 0x0

    .line 770
    .local v14, "totalSize":I
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    if-ge v9, v12, :cond_6

    .line 771
    move/from16 v0, v18

    int-to-long v0, v0

    move-wide/from16 v20, v0

    move-object/from16 v0, p2

    move-wide/from16 v1, v20

    invoke-static {v0, v1, v2}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->writeVarInt(Ljava/io/OutputStream;J)I

    move-result v19

    add-int v14, v14, v19

    .line 772
    const/4 v4, 0x0

    .line 773
    .local v4, "added":Z
    invoke-virtual/range {p2 .. p2}, Landroid_maps_conflict_avoidance/com/google/common/io/MarkedOutputStream;->availableContent()I

    move-result v7

    .line 774
    .local v7, "contentStart":I
    packed-switch v15, :pswitch_data_0

    .line 827
    :pswitch_0
    new-instance v19, Ljava/lang/IllegalArgumentException;

    invoke-direct/range {v19 .. v19}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v19

    .line 777
    :pswitch_1
    const/16 v19, 0x13

    move-object/from16 v0, p0

    move/from16 v1, p1

    move/from16 v2, v19

    invoke-direct {v0, v1, v9, v2}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->getObject(III)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Long;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    .line 779
    .local v16, "v":J
    const/16 v19, 0x5

    move/from16 v0, v19

    if-ne v15, v0, :cond_0

    const/4 v6, 0x4

    .line 780
    .local v6, "cnt":I
    :goto_1
    const/4 v5, 0x0

    .local v5, "b":I
    :goto_2
    if-ge v5, v6, :cond_2

    .line 781
    const-wide/16 v20, 0xff

    and-long v20, v20, v16

    move-wide/from16 v0, v20

    long-to-int v0, v0

    move/from16 v19, v0

    move-object/from16 v0, p2

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid_maps_conflict_avoidance/com/google/common/io/MarkedOutputStream;->write(I)V

    .line 782
    const/16 v19, 0x8

    shr-long v16, v16, v19

    .line 780
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 779
    .end local v5    # "b":I
    .end local v6    # "cnt":I
    :cond_0
    const/16 v6, 0x8

    goto :goto_1

    .line 787
    .end local v16    # "v":J
    :pswitch_2
    const/16 v19, 0x13

    move-object/from16 v0, p0

    move/from16 v1, p1

    move/from16 v2, v19

    invoke-direct {v0, v1, v9, v2}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->getObject(III)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/Long;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    .line 788
    .restart local v16    # "v":J
    invoke-direct/range {p0 .. p1}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->isZigZagEncodedType(I)Z

    move-result v19

    if-eqz v19, :cond_1

    .line 789
    invoke-static/range {v16 .. v17}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->zigZagEncode(J)J

    move-result-wide v16

    .line 791
    :cond_1
    move-object/from16 v0, p2

    move-wide/from16 v1, v16

    invoke-static {v0, v1, v2}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->writeVarInt(Ljava/io/OutputStream;J)I

    .line 829
    .end local v16    # "v":J
    :cond_2
    :goto_3
    if-nez v4, :cond_3

    .line 830
    invoke-virtual/range {p2 .. p2}, Landroid_maps_conflict_avoidance/com/google/common/io/MarkedOutputStream;->availableContent()I

    move-result v19

    sub-int v19, v19, v7

    add-int v14, v14, v19

    .line 770
    :cond_3
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_0

    .line 795
    :pswitch_3
    invoke-virtual/range {p0 .. p1}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->getType(I)I

    move-result v19

    const/16 v20, 0x1b

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_4

    const/16 v19, 0x10

    :goto_4
    move-object/from16 v0, p0

    move/from16 v1, p1

    move/from16 v2, v19

    invoke-direct {v0, v1, v9, v2}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->getObject(III)Ljava/lang/Object;

    move-result-object v10

    .line 800
    .local v10, "o":Ljava/lang/Object;
    instance-of v0, v10, [B

    move/from16 v19, v0

    if-eqz v19, :cond_5

    .line 801
    check-cast v10, [B

    .end local v10    # "o":Ljava/lang/Object;
    move-object v8, v10

    check-cast v8, [B

    .line 802
    .local v8, "data":[B
    array-length v0, v8

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-long v0, v0

    move-wide/from16 v20, v0

    move-object/from16 v0, p2

    move-wide/from16 v1, v20

    invoke-static {v0, v1, v2}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->writeVarInt(Ljava/io/OutputStream;J)I

    .line 803
    move-object/from16 v0, p2

    invoke-virtual {v0, v8}, Landroid_maps_conflict_avoidance/com/google/common/io/MarkedOutputStream;->write([B)V

    goto :goto_3

    .line 795
    .end local v8    # "data":[B
    :cond_4
    const/16 v19, 0x19

    goto :goto_4

    .line 805
    .restart local v10    # "o":Ljava/lang/Object;
    :cond_5
    invoke-virtual/range {p2 .. p2}, Landroid_maps_conflict_avoidance/com/google/common/io/MarkedOutputStream;->availableContent()I

    move-result v19

    move-object/from16 v0, p2

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid_maps_conflict_avoidance/com/google/common/io/MarkedOutputStream;->addMarker(I)V

    .line 807
    invoke-virtual/range {p2 .. p2}, Landroid_maps_conflict_avoidance/com/google/common/io/MarkedOutputStream;->numMarkers()I

    move-result v13

    .line 808
    .local v13, "tmpPos":I
    const/16 v19, -0x1

    move-object/from16 v0, p2

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid_maps_conflict_avoidance/com/google/common/io/MarkedOutputStream;->addMarker(I)V

    .line 809
    check-cast v10, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;

    .end local v10    # "o":Ljava/lang/Object;
    move-object/from16 v0, p2

    invoke-direct {v10, v0}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->outputToInternal(Landroid_maps_conflict_avoidance/com/google/common/io/MarkedOutputStream;)I

    move-result v11

    .line 811
    .local v11, "protoSize":I
    move-object/from16 v0, p2

    invoke-virtual {v0, v13, v11}, Landroid_maps_conflict_avoidance/com/google/common/io/MarkedOutputStream;->setMarker(II)V

    .line 813
    int-to-long v0, v11

    move-wide/from16 v20, v0

    invoke-static/range {v20 .. v21}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->getVarIntSize(J)I

    move-result v19

    add-int v19, v19, v11

    add-int v14, v14, v19

    .line 814
    const/4 v4, 0x1

    .line 816
    goto :goto_3

    .line 820
    .end local v11    # "protoSize":I
    .end local v13    # "tmpPos":I
    :pswitch_4
    const/16 v19, 0x1a

    move-object/from16 v0, p0

    move/from16 v1, p1

    move/from16 v2, v19

    invoke-direct {v0, v1, v9, v2}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->getObject(III)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;

    move-object/from16 v0, v19

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->outputToInternal(Landroid_maps_conflict_avoidance/com/google/common/io/MarkedOutputStream;)I

    move-result v19

    add-int v14, v14, v19

    .line 822
    shl-int/lit8 v19, p1, 0x3

    or-int/lit8 v19, v19, 0x4

    move/from16 v0, v19

    int-to-long v0, v0

    move-wide/from16 v20, v0

    move-object/from16 v0, p2

    move-wide/from16 v1, v20

    invoke-static {v0, v1, v2}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->writeVarInt(Ljava/io/OutputStream;J)I

    move-result v19

    add-int v14, v14, v19

    .line 823
    const/4 v4, 0x1

    .line 824
    goto/16 :goto_3

    .line 833
    .end local v4    # "added":Z
    .end local v7    # "contentStart":I
    :cond_6
    return v14

    .line 774
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private outputTo(Ljava/io/OutputStream;Z)V
    .locals 8
    .param p1, "os"    # Ljava/io/OutputStream;
    .param p2, "addSize"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 717
    new-instance v2, Landroid_maps_conflict_avoidance/com/google/common/io/MarkedOutputStream;

    invoke-direct {v2}, Landroid_maps_conflict_avoidance/com/google/common/io/MarkedOutputStream;-><init>()V

    .line 718
    .local v2, "mos":Landroid_maps_conflict_avoidance/com/google/common/io/MarkedOutputStream;
    invoke-direct {p0, v2}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->outputToInternal(Landroid_maps_conflict_avoidance/com/google/common/io/MarkedOutputStream;)I

    move-result v5

    .line 720
    .local v5, "size":I
    if-eqz p2, :cond_0

    move-object v6, p1

    .line 722
    check-cast v6, Ljava/io/DataOutput;

    invoke-interface {v6, v5}, Ljava/io/DataOutput;->writeInt(I)V

    .line 725
    :cond_0
    const/4 v4, 0x0

    .line 726
    .local v4, "previous":I
    const/4 v1, 0x0

    .local v1, "i":I
    invoke-virtual {v2}, Landroid_maps_conflict_avoidance/com/google/common/io/MarkedOutputStream;->numMarkers()I

    move-result v3

    .local v3, "n":I
    :goto_0
    if-ge v1, v3, :cond_1

    .line 727
    invoke-virtual {v2, v1}, Landroid_maps_conflict_avoidance/com/google/common/io/MarkedOutputStream;->getMarker(I)I

    move-result v0

    .line 728
    .local v0, "current":I
    sub-int v6, v0, v4

    invoke-virtual {v2, p1, v4, v6}, Landroid_maps_conflict_avoidance/com/google/common/io/MarkedOutputStream;->writeContentsTo(Ljava/io/OutputStream;II)V

    .line 729
    add-int/lit8 v6, v1, 0x1

    invoke-virtual {v2, v6}, Landroid_maps_conflict_avoidance/com/google/common/io/MarkedOutputStream;->getMarker(I)I

    move-result v6

    int-to-long v6, v6

    invoke-static {p1, v6, v7}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->writeVarInt(Ljava/io/OutputStream;J)I

    .line 730
    move v4, v0

    .line 726
    add-int/lit8 v1, v1, 0x2

    goto :goto_0

    .line 732
    .end local v0    # "current":I
    :cond_1
    invoke-virtual {v2}, Landroid_maps_conflict_avoidance/com/google/common/io/MarkedOutputStream;->availableContent()I

    move-result v6

    if-ge v4, v6, :cond_2

    .line 733
    invoke-virtual {v2}, Landroid_maps_conflict_avoidance/com/google/common/io/MarkedOutputStream;->availableContent()I

    move-result v6

    sub-int/2addr v6, v4

    invoke-virtual {v2, p1, v4, v6}, Landroid_maps_conflict_avoidance/com/google/common/io/MarkedOutputStream;->writeContentsTo(Ljava/io/OutputStream;II)V

    .line 735
    :cond_2
    return-void
.end method

.method private outputToInternal(Landroid_maps_conflict_avoidance/com/google/common/io/MarkedOutputStream;)I
    .locals 4
    .param p1, "os"    # Landroid_maps_conflict_avoidance/com/google/common/io/MarkedOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 744
    iget-object v3, p0, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->values:Landroid_maps_conflict_avoidance/com/google/common/util/IntMap;

    invoke-virtual {v3}, Landroid_maps_conflict_avoidance/com/google/common/util/IntMap;->keys()Landroid_maps_conflict_avoidance/com/google/common/util/IntMap$KeyIterator;

    move-result-object v0

    .line 745
    .local v0, "itr":Landroid_maps_conflict_avoidance/com/google/common/util/IntMap$KeyIterator;
    const/4 v2, 0x0

    .line 746
    .local v2, "totalSize":I
    :goto_0
    invoke-virtual {v0}, Landroid_maps_conflict_avoidance/com/google/common/util/IntMap$KeyIterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 747
    invoke-virtual {v0}, Landroid_maps_conflict_avoidance/com/google/common/util/IntMap$KeyIterator;->next()I

    move-result v1

    .line 748
    .local v1, "tag":I
    invoke-direct {p0, v1, p1}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->outputField(ILandroid_maps_conflict_avoidance/com/google/common/io/MarkedOutputStream;)I

    move-result v3

    add-int/2addr v2, v3

    .line 749
    goto :goto_0

    .line 750
    .end local v1    # "tag":I
    :cond_0
    return v2
.end method

.method private parseInternal(Ljava/io/InputStream;IZLandroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf$SimpleCounter;)I
    .locals 25
    .param p1, "is"    # Ljava/io/InputStream;
    .param p2, "available"    # I
    .param p3, "clear"    # Z
    .param p4, "counter"    # Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf$SimpleCounter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 415
    if-eqz p3, :cond_0

    .line 416
    invoke-virtual/range {p0 .. p0}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->clear()V

    .line 418
    :cond_0
    :goto_0
    if-lez p2, :cond_1

    .line 419
    const/16 v22, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v22

    move-object/from16 v2, p4

    invoke-static {v0, v1, v2}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->readVarInt(Ljava/io/InputStream;ZLandroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf$SimpleCounter;)J

    move-result-wide v14

    .line 421
    .local v14, "tagAndType":J
    const-wide/16 v22, -0x1

    cmp-long v22, v14, v22

    if-nez v22, :cond_2

    .line 509
    .end local v14    # "tagAndType":J
    :cond_1
    if-gez p2, :cond_d

    .line 510
    new-instance v22, Ljava/io/IOException;

    invoke-direct/range {v22 .. v22}, Ljava/io/IOException;-><init>()V

    throw v22

    .line 424
    .restart local v14    # "tagAndType":J
    :cond_2
    move-object/from16 v0, p4

    iget v0, v0, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf$SimpleCounter;->count:I

    move/from16 v22, v0

    sub-int p2, p2, v22

    .line 425
    long-to-int v0, v14

    move/from16 v22, v0

    and-int/lit8 v21, v22, 0x7

    .line 426
    .local v21, "wireType":I
    const/16 v22, 0x4

    move/from16 v0, v21

    move/from16 v1, v22

    if-eq v0, v1, :cond_1

    .line 429
    const/16 v22, 0x3

    ushr-long v22, v14, v22

    move-wide/from16 v0, v22

    long-to-int v13, v0

    .line 431
    .local v13, "tag":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->getType(I)I

    move-result v16

    .line 432
    .local v16, "tagType":I
    const/16 v22, 0x10

    move/from16 v0, v16

    move/from16 v1, v22

    if-ne v0, v1, :cond_4

    .line 433
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->wireTypes:Landroid_maps_conflict_avoidance/com/google/common/util/IntMap;

    move-object/from16 v22, v0

    if-nez v22, :cond_3

    .line 434
    new-instance v22, Landroid_maps_conflict_avoidance/com/google/common/util/IntMap;

    invoke-direct/range {v22 .. v22}, Landroid_maps_conflict_avoidance/com/google/common/util/IntMap;-><init>()V

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->wireTypes:Landroid_maps_conflict_avoidance/com/google/common/util/IntMap;

    .line 436
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->wireTypes:Landroid_maps_conflict_avoidance/com/google/common/util/IntMap;

    move-object/from16 v22, v0

    invoke-static/range {v21 .. v21}, Landroid_maps_conflict_avoidance/com/google/common/util/Primitives;->toInteger(I)Ljava/lang/Integer;

    move-result-object v23

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-virtual {v0, v13, v1}, Landroid_maps_conflict_avoidance/com/google/common/util/IntMap;->put(ILjava/lang/Object;)V

    .line 437
    move/from16 v16, v21

    .line 442
    :cond_4
    packed-switch v21, :pswitch_data_0

    .line 503
    :pswitch_0
    new-instance v22, Ljava/io/IOException;

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Unknown wire type "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, ", reading garbage data?"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-direct/range {v22 .. v23}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v22

    .line 444
    :pswitch_1
    const/16 v22, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v22

    move-object/from16 v2, p4

    invoke-static {v0, v1, v2}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->readVarInt(Ljava/io/InputStream;ZLandroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf$SimpleCounter;)J

    move-result-wide v18

    .line 445
    .local v18, "v":J
    move-object/from16 v0, p4

    iget v0, v0, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf$SimpleCounter;->count:I

    move/from16 v22, v0

    sub-int p2, p2, v22

    .line 446
    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->isZigZagEncodedType(I)Z

    move-result v22

    if-eqz v22, :cond_5

    .line 447
    invoke-static/range {v18 .. v19}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->zigZagDecode(J)J

    move-result-wide v18

    .line 449
    :cond_5
    invoke-static/range {v18 .. v19}, Landroid_maps_conflict_avoidance/com/google/common/util/Primitives;->toLong(J)Ljava/lang/Long;

    move-result-object v20

    .line 506
    .end local v18    # "v":J
    :goto_1
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v13, v1}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->addObject(ILjava/lang/Object;)V

    goto/16 :goto_0

    .line 455
    :pswitch_2
    const-wide/16 v18, 0x0

    .line 456
    .restart local v18    # "v":J
    const/4 v12, 0x0

    .line 457
    .local v12, "shift":I
    const/16 v22, 0x5

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_6

    const/4 v4, 0x4

    .line 458
    .local v4, "count":I
    :goto_2
    sub-int p2, p2, v4

    move v5, v4

    .line 460
    .end local v4    # "count":I
    .local v5, "count":I
    :goto_3
    add-int/lit8 v4, v5, -0x1

    .end local v5    # "count":I
    .restart local v4    # "count":I
    if-lez v5, :cond_7

    .line 461
    invoke-virtual/range {p1 .. p1}, Ljava/io/InputStream;->read()I

    move-result v22

    move/from16 v0, v22

    int-to-long v8, v0

    .line 462
    .local v8, "l":J
    shl-long v22, v8, v12

    or-long v18, v18, v22

    .line 463
    add-int/lit8 v12, v12, 0x8

    move v5, v4

    .line 464
    .end local v4    # "count":I
    .restart local v5    # "count":I
    goto :goto_3

    .line 457
    .end local v5    # "count":I
    .end local v8    # "l":J
    :cond_6
    const/16 v4, 0x8

    goto :goto_2

    .line 466
    .restart local v4    # "count":I
    :cond_7
    invoke-static/range {v18 .. v19}, Landroid_maps_conflict_avoidance/com/google/common/util/Primitives;->toLong(J)Ljava/lang/Long;

    move-result-object v20

    .line 467
    .local v20, "value":Ljava/lang/Long;
    goto :goto_1

    .line 470
    .end local v4    # "count":I
    .end local v12    # "shift":I
    .end local v18    # "v":J
    .end local v20    # "value":Ljava/lang/Long;
    :pswitch_3
    const/16 v22, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v22

    move-object/from16 v2, p4

    invoke-static {v0, v1, v2}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->readVarInt(Ljava/io/InputStream;ZLandroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf$SimpleCounter;)J

    move-result-wide v22

    move-wide/from16 v0, v22

    long-to-int v0, v0

    move/from16 v17, v0

    .line 471
    .local v17, "total":I
    move-object/from16 v0, p4

    iget v0, v0, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf$SimpleCounter;->count:I

    move/from16 v22, v0

    sub-int p2, p2, v22

    .line 472
    sub-int p2, p2, v17

    .line 474
    const/16 v22, 0x1b

    move/from16 v0, v16

    move/from16 v1, v22

    if-ne v0, v1, :cond_8

    .line 475
    new-instance v10, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;

    move-object/from16 v0, p0

    iget-object v0, v0, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->msgType:Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBufType;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v13}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBufType;->getData(I)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBufType;

    move-object/from16 v0, v22

    invoke-direct {v10, v0}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;-><init>(Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBufType;)V

    .line 477
    .local v10, "msg":Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;
    const/16 v22, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v22

    move-object/from16 v3, p4

    invoke-direct {v10, v0, v1, v2, v3}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->parseInternal(Ljava/io/InputStream;IZLandroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf$SimpleCounter;)I

    .line 478
    move-object/from16 v20, v10

    .line 479
    .local v20, "value":Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;
    goto :goto_1

    .line 480
    .end local v10    # "msg":Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;
    .end local v20    # "value":Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;
    :cond_8
    if-nez v17, :cond_9

    sget-object v6, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->EMPTY_BYTE_ARRAY:[B

    .line 481
    .local v6, "data":[B
    :goto_4
    const/4 v11, 0x0

    .line 482
    .local v11, "pos":I
    :goto_5
    move/from16 v0, v17

    if-ge v11, v0, :cond_b

    .line 483
    sub-int v22, v17, v11

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v6, v11, v1}, Ljava/io/InputStream;->read([BII)I

    move-result v4

    .line 484
    .restart local v4    # "count":I
    if-gtz v4, :cond_a

    .line 485
    new-instance v22, Ljava/io/IOException;

    const-string v23, "Unexp.EOF"

    invoke-direct/range {v22 .. v23}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v22

    .line 480
    .end local v4    # "count":I
    .end local v6    # "data":[B
    .end local v11    # "pos":I
    :cond_9
    move/from16 v0, v17

    new-array v6, v0, [B

    goto :goto_4

    .line 487
    .restart local v4    # "count":I
    .restart local v6    # "data":[B
    .restart local v11    # "pos":I
    :cond_a
    add-int/2addr v11, v4

    goto :goto_5

    .line 489
    .end local v4    # "count":I
    :cond_b
    move-object/from16 v20, v6

    .line 491
    .local v20, "value":[B
    goto/16 :goto_1

    .line 494
    .end local v6    # "data":[B
    .end local v11    # "pos":I
    .end local v17    # "total":I
    .end local v20    # "value":[B
    :pswitch_4
    new-instance v7, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;

    move-object/from16 v0, p0

    iget-object v0, v0, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->msgType:Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBufType;

    move-object/from16 v22, v0

    if-nez v22, :cond_c

    const/16 v22, 0x0

    :goto_6
    move-object/from16 v0, v22

    invoke-direct {v7, v0}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;-><init>(Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBufType;)V

    .line 498
    .local v7, "group":Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;
    const/16 v22, 0x0

    move-object/from16 v0, p1

    move/from16 v1, p2

    move/from16 v2, v22

    move-object/from16 v3, p4

    invoke-direct {v7, v0, v1, v2, v3}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->parseInternal(Ljava/io/InputStream;IZLandroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf$SimpleCounter;)I

    move-result p2

    .line 499
    move-object/from16 v20, v7

    .line 500
    .local v20, "value":Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;
    goto/16 :goto_1

    .line 494
    .end local v7    # "group":Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;
    .end local v20    # "value":Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->msgType:Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBufType;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v13}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBufType;->getData(I)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBufType;

    goto :goto_6

    .line 513
    .end local v13    # "tag":I
    .end local v14    # "tagAndType":J
    .end local v16    # "tagType":I
    .end local v21    # "wireType":I
    :cond_d
    return p2

    .line 442
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private static readVarInt(Ljava/io/InputStream;ZLandroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf$SimpleCounter;)J
    .locals 8
    .param p0, "is"    # Ljava/io/InputStream;
    .param p1, "permitEOF"    # Z
    .param p2, "counter"    # Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf$SimpleCounter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1355
    const-wide/16 v2, 0x0

    .line 1356
    .local v2, "result":J
    const/4 v4, 0x0

    .line 1358
    .local v4, "shift":I
    const/4 v5, 0x0

    iput v5, p2, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf$SimpleCounter;->count:I

    .line 1362
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v5, 0xa

    if-ge v0, v5, :cond_2

    .line 1363
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v1

    .line 1365
    .local v1, "in":I
    const/4 v5, -0x1

    if-ne v1, v5, :cond_1

    .line 1366
    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 1367
    const-wide/16 v6, -0x1

    .line 1381
    .end local v1    # "in":I
    :goto_1
    return-wide v6

    .line 1369
    .restart local v1    # "in":I
    :cond_0
    new-instance v5, Ljava/io/IOException;

    const-string v6, "EOF"

    invoke-direct {v5, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 1372
    :cond_1
    and-int/lit8 v5, v1, 0x7f

    int-to-long v6, v5

    shl-long/2addr v6, v4

    or-long/2addr v2, v6

    .line 1374
    and-int/lit16 v5, v1, 0x80

    if-nez v5, :cond_3

    .line 1380
    .end local v1    # "in":I
    :cond_2
    add-int/lit8 v5, v0, 0x1

    iput v5, p2, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf$SimpleCounter;->count:I

    move-wide v6, v2

    .line 1381
    goto :goto_1

    .line 1378
    .restart local v1    # "in":I
    :cond_3
    add-int/lit8 v4, v4, 0x7

    .line 1362
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private setObject(ILjava/lang/Object;)V
    .locals 1
    .param p1, "tag"    # I
    .param p2, "o"    # Ljava/lang/Object;

    .prologue
    .line 1389
    if-gez p1, :cond_0

    .line 1390
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0

    .line 1392
    :cond_0
    if-eqz p2, :cond_1

    .line 1393
    invoke-direct {p0, p1, p2}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->assertTypeMatch(ILjava/lang/Object;)V

    .line 1395
    :cond_1
    iget-object v0, p0, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->values:Landroid_maps_conflict_avoidance/com/google/common/util/IntMap;

    invoke-virtual {v0, p1, p2}, Landroid_maps_conflict_avoidance/com/google/common/util/IntMap;->put(ILjava/lang/Object;)V

    .line 1396
    return-void
.end method

.method static writeVarInt(Ljava/io/OutputStream;J)I
    .locals 5
    .param p0, "os"    # Ljava/io/OutputStream;
    .param p1, "value"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1405
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v2, 0xa

    if-ge v0, v2, :cond_0

    .line 1407
    const-wide/16 v2, 0x7f

    and-long/2addr v2, p1

    long-to-int v1, v2

    .line 1409
    .local v1, "toWrite":I
    const/4 v2, 0x7

    ushr-long/2addr p1, v2

    .line 1411
    const-wide/16 v2, 0x0

    cmp-long v2, p1, v2

    if-nez v2, :cond_1

    .line 1412
    invoke-virtual {p0, v1}, Ljava/io/OutputStream;->write(I)V

    .line 1413
    add-int/lit8 v0, v0, 0x1

    .line 1418
    .end local v0    # "i":I
    .end local v1    # "toWrite":I
    :cond_0
    return v0

    .line 1415
    .restart local v0    # "i":I
    .restart local v1    # "toWrite":I
    :cond_1
    or-int/lit16 v2, v1, 0x80

    invoke-virtual {p0, v2}, Ljava/io/OutputStream;->write(I)V

    .line 1405
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private static zigZagDecode(J)J
    .locals 4
    .param p0, "v"    # J

    .prologue
    .line 866
    const/4 v0, 0x1

    ushr-long v0, p0, v0

    const-wide/16 v2, 0x1

    and-long/2addr v2, p0

    neg-long v2, v2

    xor-long p0, v0, v2

    .line 867
    return-wide p0
.end method

.method private static zigZagEncode(J)J
    .locals 4
    .param p0, "v"    # J

    .prologue
    .line 858
    const/4 v0, 0x1

    shl-long v0, p0, v0

    const/16 v2, 0x3f

    ushr-long v2, p0, v2

    neg-long v2, v2

    xor-long p0, v0, v2

    .line 859
    return-wide p0
.end method


# virtual methods
.method public addProtoBuf(ILandroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;)V
    .locals 0
    .param p1, "tag"    # I
    .param p2, "value"    # Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;

    .prologue
    .line 163
    invoke-direct {p0, p1, p2}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->addObject(ILjava/lang/Object;)V

    .line 164
    return-void
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->values:Landroid_maps_conflict_avoidance/com/google/common/util/IntMap;

    invoke-virtual {v0}, Landroid_maps_conflict_avoidance/com/google/common/util/IntMap;->clear()V

    .line 105
    const/4 v0, 0x0

    iput-object v0, p0, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->wireTypes:Landroid_maps_conflict_avoidance/com/google/common/util/IntMap;

    .line 106
    return-void
.end method

.method public getBool(I)Z
    .locals 1
    .param p1, "tag"    # I

    .prologue
    .line 177
    const/16 v0, 0x18

    invoke-direct {p0, p1, v0}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->getObject(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public getBytes(I)[B
    .locals 1
    .param p1, "tag"    # I

    .prologue
    .line 193
    const/16 v0, 0x19

    invoke-direct {p0, p1, v0}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->getObject(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    check-cast v0, [B

    return-object v0
.end method

.method public getCount(I)I
    .locals 1
    .param p1, "tag"    # I

    .prologue
    .line 548
    iget-object v0, p0, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->values:Landroid_maps_conflict_avoidance/com/google/common/util/IntMap;

    invoke-virtual {v0, p1}, Landroid_maps_conflict_avoidance/com/google/common/util/IntMap;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->getCount(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public getInt(I)I
    .locals 2
    .param p1, "tag"    # I

    .prologue
    .line 207
    const/16 v0, 0x15

    invoke-direct {p0, p1, v0}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->getObject(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public getLong(I)J
    .locals 2
    .param p1, "tag"    # I

    .prologue
    .line 222
    const/16 v0, 0x13

    invoke-direct {p0, p1, v0}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->getObject(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public getProtoBuf(I)Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;
    .locals 1
    .param p1, "tag"    # I

    .prologue
    .line 272
    const/16 v0, 0x1a

    invoke-direct {p0, p1, v0}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->getObject(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method public getProtoBuf(II)Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;
    .locals 1
    .param p1, "tag"    # I
    .param p2, "index"    # I

    .prologue
    .line 280
    const/16 v0, 0x1a

    invoke-direct {p0, p1, p2, v0}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->getObject(III)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;

    return-object v0
.end method

.method public getString(I)Ljava/lang/String;
    .locals 1
    .param p1, "tag"    # I

    .prologue
    .line 288
    const/16 v0, 0x1c

    invoke-direct {p0, p1, v0}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->getObject(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getString(II)Ljava/lang/String;
    .locals 1
    .param p1, "tag"    # I
    .param p2, "index"    # I

    .prologue
    .line 296
    const/16 v0, 0x1c

    invoke-direct {p0, p1, p2, v0}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->getObject(III)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getType(I)I
    .locals 6
    .param p1, "tag"    # I

    .prologue
    const/4 v4, 0x0

    const/16 v5, 0x10

    .line 559
    const/16 v1, 0x10

    .line 560
    .local v1, "tagType":I
    iget-object v3, p0, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->msgType:Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBufType;

    if-eqz v3, :cond_0

    .line 561
    iget-object v3, p0, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->msgType:Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBufType;

    invoke-virtual {v3, p1}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBufType;->getType(I)I

    move-result v1

    .line 564
    :cond_0
    if-ne v1, v5, :cond_1

    .line 565
    iget-object v3, p0, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->wireTypes:Landroid_maps_conflict_avoidance/com/google/common/util/IntMap;

    if-eqz v3, :cond_4

    iget-object v3, p0, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->wireTypes:Landroid_maps_conflict_avoidance/com/google/common/util/IntMap;

    invoke-virtual {v3, p1}, Landroid_maps_conflict_avoidance/com/google/common/util/IntMap;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    move-object v2, v3

    .line 566
    .local v2, "tagTypeObj":Ljava/lang/Integer;
    :goto_0
    if-eqz v2, :cond_1

    .line 567
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 571
    .end local v2    # "tagTypeObj":Ljava/lang/Integer;
    :cond_1
    if-ne v1, v5, :cond_3

    invoke-virtual {p0, p1}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v3

    if-lez v3, :cond_3

    .line 572
    invoke-direct {p0, p1, v4, v5}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->getObject(III)Ljava/lang/Object;

    move-result-object v0

    .line 574
    .local v0, "o":Ljava/lang/Object;
    instance-of v3, v0, Ljava/lang/Long;

    if-nez v3, :cond_2

    instance-of v3, v0, Ljava/lang/Boolean;

    if-eqz v3, :cond_5

    :cond_2
    move v1, v4

    .line 578
    .end local v0    # "o":Ljava/lang/Object;
    :cond_3
    :goto_1
    return v1

    .line 565
    :cond_4
    const/4 v2, 0x0

    goto :goto_0

    .line 574
    .restart local v0    # "o":Ljava/lang/Object;
    :cond_5
    const/4 v1, 0x2

    goto :goto_1
.end method

.method public has(I)Z
    .locals 1
    .param p1, "tag"    # I

    .prologue
    .line 338
    invoke-virtual {p0, p1}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->getCount(I)I

    move-result v0

    if-gtz v0, :cond_0

    invoke-direct {p0, p1}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->getDefault(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public outputTo(Ljava/io/OutputStream;)V
    .locals 1
    .param p1, "os"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 696
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->outputTo(Ljava/io/OutputStream;Z)V

    .line 697
    return-void
.end method

.method public outputWithSizeTo(Ljava/io/OutputStream;)V
    .locals 1
    .param p1, "os"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 686
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->outputTo(Ljava/io/OutputStream;Z)V

    .line 687
    return-void
.end method

.method public parse(Ljava/io/InputStream;I)I
    .locals 3
    .param p1, "is"    # Ljava/io/InputStream;
    .param p2, "available"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 396
    const/4 v0, 0x1

    new-instance v1, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf$SimpleCounter;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf$SimpleCounter;-><init>(Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf$1;)V

    invoke-direct {p0, p1, p2, v0, v1}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->parseInternal(Ljava/io/InputStream;IZLandroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf$SimpleCounter;)I

    move-result v0

    return v0
.end method

.method public parse(Ljava/io/InputStream;)Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;
    .locals 1
    .param p1, "is"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 366
    const v0, 0x7fffffff

    invoke-virtual {p0, p1, v0}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->parse(Ljava/io/InputStream;I)I

    .line 367
    return-object p0
.end method

.method public parse([B)Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;
    .locals 2
    .param p1, "data"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 351
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    array-length v1, p1

    invoke-virtual {p0, v0, v1}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->parse(Ljava/io/InputStream;I)I

    .line 352
    return-object p0
.end method

.method public setInt(II)V
    .locals 2
    .param p1, "tag"    # I
    .param p2, "value"    # I

    .prologue
    .line 906
    int-to-long v0, p2

    invoke-virtual {p0, p1, v0, v1}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->setLong(IJ)V

    .line 907
    return-void
.end method

.method public setLong(IJ)V
    .locals 2
    .param p1, "tag"    # I
    .param p2, "value"    # J

    .prologue
    .line 913
    invoke-static {p2, p3}, Landroid_maps_conflict_avoidance/com/google/common/util/Primitives;->toLong(J)Ljava/lang/Long;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->setObject(ILjava/lang/Object;)V

    .line 914
    return-void
.end method

.method public setString(ILjava/lang/String;)V
    .locals 0
    .param p1, "tag"    # I
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 943
    invoke-direct {p0, p1, p2}, Landroid_maps_conflict_avoidance/com/google/common/io/protocol/ProtoBuf;->setObject(ILjava/lang/Object;)V

    .line 944
    return-void
.end method
