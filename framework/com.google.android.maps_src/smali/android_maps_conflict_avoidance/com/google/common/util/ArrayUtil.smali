.class public Landroid_maps_conflict_avoidance/com/google/common/util/ArrayUtil;
.super Ljava/lang/Object;
.source "ArrayUtil.java"


# direct methods
.method public static copyIntoVector(Ljava/util/Vector;ILjava/util/Vector;)V
    .locals 3
    .param p0, "src"    # Ljava/util/Vector;
    .param p1, "srcIndex"    # I
    .param p2, "dest"    # Ljava/util/Vector;

    .prologue
    .line 384
    monitor-enter p2

    .line 385
    move v0, p1

    .local v0, "i":I
    :goto_0
    :try_start_0
    invoke-virtual {p0}, Ljava/util/Vector;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 386
    invoke-virtual {p0, v0}, Ljava/util/Vector;->elementAt(I)Ljava/lang/Object;

    move-result-object v1

    .line 388
    .local v1, "request":Ljava/lang/Object;
    sub-int v2, v0, p1

    invoke-virtual {p2, v1, v2}, Ljava/util/Vector;->insertElementAt(Ljava/lang/Object;I)V

    .line 385
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 390
    .end local v1    # "request":Ljava/lang/Object;
    :cond_0
    monitor-exit p2

    .line 391
    return-void

    .line 390
    :catchall_0
    move-exception v2

    monitor-exit p2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method
