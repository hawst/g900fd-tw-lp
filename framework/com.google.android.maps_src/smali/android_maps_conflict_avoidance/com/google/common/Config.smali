.class public abstract Landroid_maps_conflict_avoidance/com/google/common/Config;
.super Ljava/lang/Object;
.source "Config.java"


# static fields
.field private static ADS_CLIENT:Ljava/lang/String;

.field protected static ALT_ARROWS_ENABLED:Z

.field protected static ALT_DOWN:I

.field protected static ALT_LEFT:I

.field private static ALT_NUMBER_KEYS:[I

.field protected static ALT_RIGHT:I

.field protected static ALT_UP:I

.field private static CARRIER:Ljava/lang/String;

.field private static DISTRIBUTION_CHANNEL:Ljava/lang/String;

.field public static KEY_BACK:I

.field public static KEY_CLEAR:I

.field public static KEY_MENU:I

.field public static KEY_OK:I

.field public static KEY_POUND:I

.field public static KEY_SIDE_DOWN:I

.field public static KEY_SIDE_SELECT:I

.field public static KEY_SIDE_UP:I

.field public static KEY_SOFT_LEFT:I

.field public static KEY_SOFT_MIDDLE:I

.field public static KEY_SOFT_RIGHT:I

.field public static KEY_STAR:I

.field public static KEY_TALK:I

.field public static KEY_VOICE_SEARCH:I

.field public static QWERTY_KEYBOARD:Z

.field public static REVERSE_SOFTKEYS:Z

.field public static SOFTKEYS_ON_SIDE_IN_LANDSCAPE:Z

.field public static SOFTKEY_HEIGHT:I

.field public static USE_NATIVE_COMMANDS:Z

.field public static USE_NATIVE_MENUS:Z

.field private static instance:Landroid_maps_conflict_avoidance/com/google/common/Config;


# instance fields
.field private final applicationStartTime:J

.field private final clock:Landroid_maps_conflict_avoidance/com/google/common/Clock;

.field private countryCode:Ljava/lang/String;

.field private i18n:Landroid_maps_conflict_avoidance/com/google/common/I18n;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/16 v1, -0x270f

    .line 50
    sput v1, Landroid_maps_conflict_avoidance/com/google/common/Config;->KEY_BACK:I

    .line 59
    sput v1, Landroid_maps_conflict_avoidance/com/google/common/Config;->KEY_SOFT_LEFT:I

    .line 65
    sput v1, Landroid_maps_conflict_avoidance/com/google/common/Config;->KEY_SOFT_MIDDLE:I

    .line 72
    sput v1, Landroid_maps_conflict_avoidance/com/google/common/Config;->KEY_SOFT_RIGHT:I

    .line 73
    const/16 v0, 0x2a

    sput v0, Landroid_maps_conflict_avoidance/com/google/common/Config;->KEY_STAR:I

    .line 74
    const/16 v0, 0x23

    sput v0, Landroid_maps_conflict_avoidance/com/google/common/Config;->KEY_POUND:I

    .line 75
    sput v1, Landroid_maps_conflict_avoidance/com/google/common/Config;->KEY_MENU:I

    .line 76
    sput v1, Landroid_maps_conflict_avoidance/com/google/common/Config;->KEY_OK:I

    .line 77
    sput v1, Landroid_maps_conflict_avoidance/com/google/common/Config;->KEY_TALK:I

    .line 78
    sput v1, Landroid_maps_conflict_avoidance/com/google/common/Config;->KEY_VOICE_SEARCH:I

    .line 87
    sput v1, Landroid_maps_conflict_avoidance/com/google/common/Config;->KEY_CLEAR:I

    .line 95
    sput v1, Landroid_maps_conflict_avoidance/com/google/common/Config;->KEY_SIDE_UP:I

    .line 96
    sput v1, Landroid_maps_conflict_avoidance/com/google/common/Config;->KEY_SIDE_DOWN:I

    .line 97
    sput v1, Landroid_maps_conflict_avoidance/com/google/common/Config;->KEY_SIDE_SELECT:I

    .line 100
    sput v1, Landroid_maps_conflict_avoidance/com/google/common/Config;->ALT_UP:I

    .line 101
    sput v1, Landroid_maps_conflict_avoidance/com/google/common/Config;->ALT_DOWN:I

    .line 102
    sput v1, Landroid_maps_conflict_avoidance/com/google/common/Config;->ALT_LEFT:I

    .line 103
    sput v1, Landroid_maps_conflict_avoidance/com/google/common/Config;->ALT_RIGHT:I

    .line 109
    const/4 v0, 0x0

    sput-boolean v0, Landroid_maps_conflict_avoidance/com/google/common/Config;->ALT_ARROWS_ENABLED:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 221
    const-string v0, ""

    iput-object v0, p0, Landroid_maps_conflict_avoidance/com/google/common/Config;->countryCode:Ljava/lang/String;

    .line 249
    new-instance v0, Landroid_maps_conflict_avoidance/com/google/common/GenericClock;

    invoke-direct {v0}, Landroid_maps_conflict_avoidance/com/google/common/GenericClock;-><init>()V

    iput-object v0, p0, Landroid_maps_conflict_avoidance/com/google/common/Config;->clock:Landroid_maps_conflict_avoidance/com/google/common/Clock;

    .line 256
    iget-object v0, p0, Landroid_maps_conflict_avoidance/com/google/common/Config;->clock:Landroid_maps_conflict_avoidance/com/google/common/Clock;

    invoke-interface {v0}, Landroid_maps_conflict_avoidance/com/google/common/Clock;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Landroid_maps_conflict_avoidance/com/google/common/Config;->applicationStartTime:J

    return-void
.end method

.method public static getInstance()Landroid_maps_conflict_avoidance/com/google/common/Config;
    .locals 1

    .prologue
    .line 236
    sget-object v0, Landroid_maps_conflict_avoidance/com/google/common/Config;->instance:Landroid_maps_conflict_avoidance/com/google/common/Config;

    return-object v0
.end method

.method public static declared-synchronized getLocale()Ljava/lang/String;
    .locals 2

    .prologue
    .line 638
    const-class v1, Landroid_maps_conflict_avoidance/com/google/common/Config;

    monitor-enter v1

    :try_start_0
    sget-object v0, Landroid_maps_conflict_avoidance/com/google/common/Config;->instance:Landroid_maps_conflict_avoidance/com/google/common/Config;

    iget-object v0, v0, Landroid_maps_conflict_avoidance/com/google/common/Config;->i18n:Landroid_maps_conflict_avoidance/com/google/common/I18n;

    invoke-virtual {v0}, Landroid_maps_conflict_avoidance/com/google/common/I18n;->getUiLocale()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static isChinaVersion()Z
    .locals 1

    .prologue
    .line 733
    const/4 v0, 0x0

    return v0
.end method

.method private static parseAltArrowKeys(Ljava/lang/String;)V
    .locals 3
    .param p0, "altArrowKeys"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    .line 412
    const/4 v1, 0x4

    invoke-static {v1, p0}, Landroid_maps_conflict_avoidance/com/google/common/Config;->parseAltKeys(ILjava/lang/String;)[I

    move-result-object v0

    .line 413
    .local v0, "altKeys":[I
    if-eqz v0, :cond_0

    .line 414
    sput-boolean v2, Landroid_maps_conflict_avoidance/com/google/common/Config;->ALT_ARROWS_ENABLED:Z

    .line 415
    const/4 v1, 0x0

    aget v1, v0, v1

    sput v1, Landroid_maps_conflict_avoidance/com/google/common/Config;->ALT_UP:I

    .line 416
    aget v1, v0, v2

    sput v1, Landroid_maps_conflict_avoidance/com/google/common/Config;->ALT_LEFT:I

    .line 417
    const/4 v1, 0x2

    aget v1, v0, v1

    sput v1, Landroid_maps_conflict_avoidance/com/google/common/Config;->ALT_DOWN:I

    .line 418
    const/4 v1, 0x3

    aget v1, v0, v1

    sput v1, Landroid_maps_conflict_avoidance/com/google/common/Config;->ALT_RIGHT:I

    .line 420
    :cond_0
    return-void
.end method

.method private static parseAltKeys(ILjava/lang/String;)[I
    .locals 9
    .param p0, "numberOfKeys"    # I
    .param p1, "altKeys"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    .line 438
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v8

    if-nez v8, :cond_2

    :cond_0
    move-object v4, v7

    .line 470
    :cond_1
    :goto_0
    return-object v4

    .line 443
    :cond_2
    :try_start_0
    new-array v4, p0, [I

    .line 444
    .local v4, "keyCodes":[I
    const/4 v5, 0x0

    .line 445
    .local v5, "lastComma":I
    const/4 v1, 0x0

    .line 446
    .local v1, "index":I
    const/4 v6, 0x0

    .local v6, "nextComma":I
    move v2, v1

    .line 447
    .end local v1    # "index":I
    .local v2, "index":I
    :goto_1
    const-string v8, ","

    invoke-virtual {p1, v8, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v6

    const/4 v8, -0x1

    if-eq v6, v8, :cond_3

    .line 448
    invoke-virtual {p1, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 449
    .local v3, "key":Ljava/lang/String;
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "index":I
    .restart local v1    # "index":I
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    aput v8, v4, v2

    .line 450
    add-int/lit8 v5, v6, 0x1

    move v2, v1

    .line 451
    .end local v1    # "index":I
    .restart local v2    # "index":I
    goto :goto_1

    .line 454
    .end local v3    # "key":Ljava/lang/String;
    :cond_3
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "index":I
    .restart local v1    # "index":I
    invoke-virtual {p1, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    aput v8, v4, v2
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_1

    .line 457
    if-eq v1, p0, :cond_1

    move-object v4, v7

    .line 460
    goto :goto_0

    .line 464
    .end local v1    # "index":I
    .end local v4    # "keyCodes":[I
    .end local v5    # "lastComma":I
    .end local v6    # "nextComma":I
    :catch_0
    move-exception v0

    .line 465
    .local v0, "e":Ljava/lang/NumberFormatException;
    const-string v8, "CONFIG"

    invoke-static {v8, v0}, Landroid_maps_conflict_avoidance/com/google/common/Log;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    .end local v0    # "e":Ljava/lang/NumberFormatException;
    :goto_2
    move-object v4, v7

    .line 470
    goto :goto_0

    .line 466
    :catch_1
    move-exception v0

    .line 467
    .local v0, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    const-string v8, "CONFIG"

    invoke-static {v8, v0}, Landroid_maps_conflict_avoidance/com/google/common/Log;->logThrowable(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2
.end method

.method private static parseAltNumberKeys(Ljava/lang/String;)V
    .locals 1
    .param p0, "altNumberKeys"    # Ljava/lang/String;

    .prologue
    .line 427
    const/16 v0, 0xc

    invoke-static {v0, p0}, Landroid_maps_conflict_avoidance/com/google/common/Config;->parseAltKeys(ILjava/lang/String;)[I

    move-result-object v0

    sput-object v0, Landroid_maps_conflict_avoidance/com/google/common/Config;->ALT_NUMBER_KEYS:[I

    .line 428
    return-void
.end method

.method public static setConfig(Landroid_maps_conflict_avoidance/com/google/common/Config;)V
    .locals 0
    .param p0, "config"    # Landroid_maps_conflict_avoidance/com/google/common/Config;

    .prologue
    .line 228
    sput-object p0, Landroid_maps_conflict_avoidance/com/google/common/Config;->instance:Landroid_maps_conflict_avoidance/com/google/common/Config;

    .line 229
    return-void
.end method


# virtual methods
.method protected getAdsClientInternal()Ljava/lang/String;
    .locals 1

    .prologue
    .line 380
    const-string v0, "AdsClient"

    invoke-virtual {p0, v0}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getAppProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public abstract getAppProperty(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public getBooleanProperty(Ljava/lang/String;Z)Z
    .locals 2
    .param p1, "property"    # Ljava/lang/String;
    .param p2, "defaultValue"    # Z

    .prologue
    .line 839
    invoke-virtual {p0, p1}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getAppProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 840
    .local v0, "value":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 841
    const-string v1, "true"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 842
    const/4 p2, 0x1

    .line 854
    .end local p2    # "defaultValue":Z
    :cond_0
    :goto_0
    return p2

    .line 843
    .restart local p2    # "defaultValue":Z
    :cond_1
    const-string v1, "false"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 844
    const/4 p2, 0x0

    goto :goto_0
.end method

.method public getClock()Landroid_maps_conflict_avoidance/com/google/common/Clock;
    .locals 1

    .prologue
    .line 995
    iget-object v0, p0, Landroid_maps_conflict_avoidance/com/google/common/Config;->clock:Landroid_maps_conflict_avoidance/com/google/common/Clock;

    return-object v0
.end method

.method public abstract getConnectionFactory()Landroid_maps_conflict_avoidance/com/google/common/io/HttpConnectionFactory;
.end method

.method protected getDistributionChannelInternal()Ljava/lang/String;
    .locals 1

    .prologue
    .line 371
    const-string v0, "DistributionChannel"

    invoke-virtual {p0, v0}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getAppProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getI18n()Landroid_maps_conflict_avoidance/com/google/common/I18n;
    .locals 1

    .prologue
    .line 646
    iget-object v0, p0, Landroid_maps_conflict_avoidance/com/google/common/Config;->i18n:Landroid_maps_conflict_avoidance/com/google/common/I18n;

    return-object v0
.end method

.method public abstract getImageFactory()Landroid_maps_conflict_avoidance/com/google/common/graphics/ImageFactory;
.end method

.method public abstract getInflaterInputStream(Ljava/io/InputStream;)Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public getIntProperty(Ljava/lang/String;I)I
    .locals 2
    .param p1, "property"    # Ljava/lang/String;
    .param p2, "defaultValue"    # I

    .prologue
    .line 819
    invoke-virtual {p0, p1}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getAppProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 820
    .local v0, "value":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 822
    :try_start_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result p2

    .line 831
    .end local p2    # "defaultValue":I
    :cond_0
    :goto_0
    return p2

    .line 823
    .restart local p2    # "defaultValue":I
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public abstract getPersistentStore()Landroid_maps_conflict_avoidance/com/google/common/io/PersistentStore;
.end method

.method public abstract getPixelsPerInch()I
.end method

.method protected init()V
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 264
    iget-object v2, p0, Landroid_maps_conflict_avoidance/com/google/common/Config;->i18n:Landroid_maps_conflict_avoidance/com/google/common/I18n;

    if-nez v2, :cond_0

    .line 268
    const-string v2, "DownloadLocale"

    invoke-virtual {p0, v2}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getAppProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid_maps_conflict_avoidance/com/google/common/I18n;->init(Ljava/lang/String;)Landroid_maps_conflict_avoidance/com/google/common/I18n;

    move-result-object v2

    iput-object v2, p0, Landroid_maps_conflict_avoidance/com/google/common/Config;->i18n:Landroid_maps_conflict_avoidance/com/google/common/I18n;

    .line 271
    :cond_0
    const-string v2, "microedition.platform"

    invoke-static {v2}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 272
    .local v0, "platform":Ljava/lang/String;
    if-nez v0, :cond_6

    const-string v0, ""

    .line 273
    :goto_0
    invoke-virtual {p0}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getDistributionChannelInternal()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Landroid_maps_conflict_avoidance/com/google/common/Config;->DISTRIBUTION_CHANNEL:Ljava/lang/String;

    .line 274
    sget-object v2, Landroid_maps_conflict_avoidance/com/google/common/Config;->DISTRIBUTION_CHANNEL:Ljava/lang/String;

    if-nez v2, :cond_1

    .line 275
    const-string v2, "unknown"

    sput-object v2, Landroid_maps_conflict_avoidance/com/google/common/Config;->DISTRIBUTION_CHANNEL:Ljava/lang/String;

    .line 277
    :cond_1
    invoke-virtual {p0}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getAdsClientInternal()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Landroid_maps_conflict_avoidance/com/google/common/Config;->ADS_CLIENT:Ljava/lang/String;

    .line 278
    sget-object v2, Landroid_maps_conflict_avoidance/com/google/common/Config;->ADS_CLIENT:Ljava/lang/String;

    if-nez v2, :cond_2

    .line 279
    const-string v2, "unknown"

    sput-object v2, Landroid_maps_conflict_avoidance/com/google/common/Config;->ADS_CLIENT:Ljava/lang/String;

    .line 281
    :cond_2
    const-string v2, "Carrier"

    invoke-virtual {p0, v2}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getAppProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Landroid_maps_conflict_avoidance/com/google/common/Config;->CARRIER:Ljava/lang/String;

    .line 282
    sget-object v2, Landroid_maps_conflict_avoidance/com/google/common/Config;->CARRIER:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 283
    const-string v2, "unknown"

    sput-object v2, Landroid_maps_conflict_avoidance/com/google/common/Config;->CARRIER:Ljava/lang/String;

    .line 285
    :cond_3
    const-string v2, "BackKey"

    sget v5, Landroid_maps_conflict_avoidance/com/google/common/Config;->KEY_BACK:I

    invoke-virtual {p0, v2, v5}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getIntProperty(Ljava/lang/String;I)I

    move-result v2

    sput v2, Landroid_maps_conflict_avoidance/com/google/common/Config;->KEY_BACK:I

    .line 286
    const-string v2, "LeftSoftKey"

    sget v5, Landroid_maps_conflict_avoidance/com/google/common/Config;->KEY_SOFT_LEFT:I

    invoke-virtual {p0, v2, v5}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getIntProperty(Ljava/lang/String;I)I

    move-result v2

    sput v2, Landroid_maps_conflict_avoidance/com/google/common/Config;->KEY_SOFT_LEFT:I

    .line 287
    const-string v2, "MiddleSoftKey"

    sget v5, Landroid_maps_conflict_avoidance/com/google/common/Config;->KEY_SOFT_MIDDLE:I

    invoke-virtual {p0, v2, v5}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getIntProperty(Ljava/lang/String;I)I

    move-result v2

    sput v2, Landroid_maps_conflict_avoidance/com/google/common/Config;->KEY_SOFT_MIDDLE:I

    .line 288
    const-string v2, "RightSoftKey"

    sget v5, Landroid_maps_conflict_avoidance/com/google/common/Config;->KEY_SOFT_RIGHT:I

    invoke-virtual {p0, v2, v5}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getIntProperty(Ljava/lang/String;I)I

    move-result v2

    sput v2, Landroid_maps_conflict_avoidance/com/google/common/Config;->KEY_SOFT_RIGHT:I

    .line 289
    const-string v2, "ReverseSoftkeys"

    const-string v5, "nokia"

    invoke-virtual {v0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    invoke-virtual {p0, v2, v5}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getBooleanProperty(Ljava/lang/String;Z)Z

    move-result v2

    sput-boolean v2, Landroid_maps_conflict_avoidance/com/google/common/Config;->REVERSE_SOFTKEYS:Z

    .line 291
    const-string v2, "SoftkeysOnSideInLandscape"

    invoke-virtual {p0, v2, v4}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getBooleanProperty(Ljava/lang/String;Z)Z

    move-result v2

    sput-boolean v2, Landroid_maps_conflict_avoidance/com/google/common/Config;->SOFTKEYS_ON_SIDE_IN_LANDSCAPE:Z

    .line 293
    const-string v2, "SideUpKey"

    sget v5, Landroid_maps_conflict_avoidance/com/google/common/Config;->KEY_SIDE_UP:I

    invoke-virtual {p0, v2, v5}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getIntProperty(Ljava/lang/String;I)I

    move-result v2

    sput v2, Landroid_maps_conflict_avoidance/com/google/common/Config;->KEY_SIDE_UP:I

    .line 294
    const-string v2, "SideDownKey"

    sget v5, Landroid_maps_conflict_avoidance/com/google/common/Config;->KEY_SIDE_DOWN:I

    invoke-virtual {p0, v2, v5}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getIntProperty(Ljava/lang/String;I)I

    move-result v2

    sput v2, Landroid_maps_conflict_avoidance/com/google/common/Config;->KEY_SIDE_DOWN:I

    .line 295
    const-string v2, "SideSelectKey"

    sget v5, Landroid_maps_conflict_avoidance/com/google/common/Config;->KEY_SIDE_SELECT:I

    invoke-virtual {p0, v2, v5}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getIntProperty(Ljava/lang/String;I)I

    move-result v2

    sput v2, Landroid_maps_conflict_avoidance/com/google/common/Config;->KEY_SIDE_SELECT:I

    .line 296
    const-string v2, "QwertyKeyboard"

    invoke-virtual {p0, v2, v4}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getBooleanProperty(Ljava/lang/String;Z)Z

    move-result v2

    sput-boolean v2, Landroid_maps_conflict_avoidance/com/google/common/Config;->QWERTY_KEYBOARD:Z

    .line 300
    sget-boolean v2, Landroid_maps_conflict_avoidance/com/google/common/Config;->REVERSE_SOFTKEYS:Z

    if-eqz v2, :cond_4

    .line 301
    sget v1, Landroid_maps_conflict_avoidance/com/google/common/Config;->KEY_SOFT_LEFT:I

    .line 302
    .local v1, "temp":I
    sget v2, Landroid_maps_conflict_avoidance/com/google/common/Config;->KEY_SOFT_RIGHT:I

    sput v2, Landroid_maps_conflict_avoidance/com/google/common/Config;->KEY_SOFT_LEFT:I

    .line 303
    sput v1, Landroid_maps_conflict_avoidance/com/google/common/Config;->KEY_SOFT_RIGHT:I

    .line 306
    .end local v1    # "temp":I
    :cond_4
    const-string v2, "MenuKey"

    sget v5, Landroid_maps_conflict_avoidance/com/google/common/Config;->KEY_MENU:I

    invoke-virtual {p0, v2, v5}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getIntProperty(Ljava/lang/String;I)I

    move-result v2

    sput v2, Landroid_maps_conflict_avoidance/com/google/common/Config;->KEY_MENU:I

    .line 307
    const-string v2, "SelectKey"

    sget v5, Landroid_maps_conflict_avoidance/com/google/common/Config;->KEY_OK:I

    invoke-virtual {p0, v2, v5}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getIntProperty(Ljava/lang/String;I)I

    move-result v2

    sput v2, Landroid_maps_conflict_avoidance/com/google/common/Config;->KEY_OK:I

    .line 308
    const-string v2, "TalkKey"

    sget v5, Landroid_maps_conflict_avoidance/com/google/common/Config;->KEY_TALK:I

    invoke-virtual {p0, v2, v5}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getIntProperty(Ljava/lang/String;I)I

    move-result v2

    sput v2, Landroid_maps_conflict_avoidance/com/google/common/Config;->KEY_TALK:I

    .line 312
    const-string v2, "VoiceSearchKey"

    sget v5, Landroid_maps_conflict_avoidance/com/google/common/Config;->KEY_VOICE_SEARCH:I

    invoke-virtual {p0, v2, v5}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getIntProperty(Ljava/lang/String;I)I

    move-result v2

    sput v2, Landroid_maps_conflict_avoidance/com/google/common/Config;->KEY_VOICE_SEARCH:I

    .line 313
    const-string v2, "ClearKey"

    sget v5, Landroid_maps_conflict_avoidance/com/google/common/Config;->KEY_CLEAR:I

    invoke-virtual {p0, v2, v5}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getIntProperty(Ljava/lang/String;I)I

    move-result v2

    sput v2, Landroid_maps_conflict_avoidance/com/google/common/Config;->KEY_CLEAR:I

    .line 317
    const-string v5, "UseNativeCommands"

    const-string v2, "nokia"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_7

    const-string v2, "sony"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_7

    move v2, v3

    :goto_1
    invoke-virtual {p0, v5, v2}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getBooleanProperty(Ljava/lang/String;Z)Z

    move-result v2

    sput-boolean v2, Landroid_maps_conflict_avoidance/com/google/common/Config;->USE_NATIVE_COMMANDS:Z

    .line 319
    const-string v2, "UseNativeMenus"

    invoke-virtual {p0, v2, v4}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getBooleanProperty(Ljava/lang/String;Z)Z

    move-result v2

    sput-boolean v2, Landroid_maps_conflict_avoidance/com/google/common/Config;->USE_NATIVE_MENUS:Z

    .line 320
    const-string v2, "SoftkeyHeight"

    invoke-virtual {p0, v2, v4}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getIntProperty(Ljava/lang/String;I)I

    move-result v2

    sput v2, Landroid_maps_conflict_avoidance/com/google/common/Config;->SOFTKEY_HEIGHT:I

    .line 322
    const-string v2, "AltNumberKeys"

    invoke-virtual {p0, v2}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getAppProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid_maps_conflict_avoidance/com/google/common/Config;->parseAltNumberKeys(Ljava/lang/String;)V

    .line 323
    const-string v2, "AltArrowKeys"

    invoke-virtual {p0, v2}, Landroid_maps_conflict_avoidance/com/google/common/Config;->getAppProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid_maps_conflict_avoidance/com/google/common/Config;->parseAltArrowKeys(Ljava/lang/String;)V

    .line 325
    sget-boolean v2, Landroid_maps_conflict_avoidance/com/google/common/Config;->USE_NATIVE_MENUS:Z

    if-eqz v2, :cond_5

    sget-boolean v2, Landroid_maps_conflict_avoidance/com/google/common/Config;->USE_NATIVE_COMMANDS:Z

    if-nez v2, :cond_5

    .line 331
    sput-boolean v3, Landroid_maps_conflict_avoidance/com/google/common/Config;->USE_NATIVE_COMMANDS:Z

    .line 333
    :cond_5
    invoke-virtual {p0}, Landroid_maps_conflict_avoidance/com/google/common/Config;->setupGzipper()V

    .line 334
    return-void

    .line 272
    :cond_6
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_7
    move v2, v4

    .line 317
    goto :goto_1
.end method

.method protected abstract setupGzipper()V
.end method
