.class public Landroid_maps_conflict_avoidance/com/google/image/compression/jpeg/JpegUtil;
.super Ljava/lang/Object;
.source "JpegUtil.java"


# static fields
.field private static final JPEG_QUANT_TABLES:[[B

.field private static final imageIoScaleFactor:[I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/16 v3, 0x40

    .line 31
    const/4 v0, 0x2

    new-array v0, v0, [[B

    const/4 v1, 0x0

    new-array v2, v3, [B

    fill-array-data v2, :array_0

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-array v2, v3, [B

    fill-array-data v2, :array_1

    aput-object v2, v0, v1

    sput-object v0, Landroid_maps_conflict_avoidance/com/google/image/compression/jpeg/JpegUtil;->JPEG_QUANT_TABLES:[[B

    .line 81
    const/16 v0, 0x65

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    sput-object v0, Landroid_maps_conflict_avoidance/com/google/image/compression/jpeg/JpegUtil;->imageIoScaleFactor:[I

    return-void

    .line 31
    nop

    :array_0
    .array-data 1
        0x10t
        0xbt
        0xct
        0xet
        0xct
        0xat
        0x10t
        0xet
        0xdt
        0xet
        0x12t
        0x11t
        0x10t
        0x13t
        0x18t
        0x28t
        0x1at
        0x18t
        0x16t
        0x16t
        0x18t
        0x31t
        0x23t
        0x25t
        0x1dt
        0x28t
        0x3at
        0x33t
        0x3dt
        0x3ct
        0x39t
        0x33t
        0x38t
        0x37t
        0x40t
        0x48t
        0x5ct
        0x4et
        0x40t
        0x44t
        0x57t
        0x45t
        0x37t
        0x38t
        0x50t
        0x6dt
        0x51t
        0x57t
        0x5ft
        0x62t
        0x67t
        0x68t
        0x67t
        0x3et
        0x4dt
        0x71t
        0x79t
        0x70t
        0x64t
        0x78t
        0x5ct
        0x65t
        0x67t
        0x63t
    .end array-data

    :array_1
    .array-data 1
        0x11t
        0x12t
        0x12t
        0x18t
        0x15t
        0x18t
        0x2ft
        0x1at
        0x1at
        0x2ft
        0x63t
        0x42t
        0x38t
        0x42t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
        0x63t
    .end array-data

    .line 81
    :array_2
    .array-data 4
        -0x1
        0x64000001
        0x32000001
        0x21555581
        0x19000001
        0x14000001
        0x10aaaac1
        0xe492491
        0xc800001
        0xb1c71c1
        0xa000001
        0x91745d1
        0x8555561
        0x7b13b19
        0x7249249
        0x6aaaaa9
        0x6400001
        0x5e1e1e1
        0x58e38e1
        0x5435e51
        0x5000001
        0x4c30c31
        0x48ba2e9
        0x4590b21
        0x42aaab1
        0x4000001
        0x3d89d8d
        0x3b425ed
        0x3924925
        0x372c239
        0x3555555
        0x339ce75
        0x3200001
        0x307c1f1
        0x2f0f0f1
        0x2db6db9
        0x2c71c71
        0x2b3e455
        0x2a1af29
        0x2906909
        0x2800001
        0x27063e9
        0x2618619
        0x253594d
        0x245d175
        0x238e391
        0x22c8591
        0x220ae4d
        0x2155559
        0x20a72f1
        0x2000001
        0x1f5c291
        0x1eb8521
        0x1e147b1
        0x1d70a3d
        0x1cccccd
        0x1c28f5d
        0x1b851ed
        0x1ae147d
        0x1a3d70d
        0x1999999
        0x18f5c29
        0x1851eb9
        0x17ae149
        0x170a3d9
        0x1666669
        0x15c28f5
        0x151eb85
        0x147ae15
        0x13d70a5
        0x1333335
        0x128f5c5
        0x11eb851
        0x1147ae1
        0x10a3d71
        0x1000001
        0xf5c291
        0xeb8521
        0xe147b1
        0xd70a3d
        0xcccccd
        0xc28f5d
        0xb851ed
        0xae147d
        0xa3d70d
        0x999999
        0x8f5c29
        0x851eb9
        0x7ae149
        0x70a3d9
        0x666669
        0x5c28f5
        0x51eb85
        0x47ae15
        0x3d70a5
        0x333335
        0x28f5c5
        0x1eb851
        0x147ae1
        0xa3d71
        0x1
    .end array-data
.end method

.method public static declared-synchronized getQuantTable(III)[B
    .locals 8
    .param p0, "quantType"    # I
    .param p1, "quality"    # I
    .param p2, "qualityAlgorithm"    # I

    .prologue
    const/16 v7, 0x40

    .line 188
    const-class v5, Landroid_maps_conflict_avoidance/com/google/image/compression/jpeg/JpegUtil;

    monitor-enter v5

    mul-int/lit16 v4, p0, 0x9a

    mul-int/lit8 v6, p2, 0x4d

    add-int/2addr v4, v6

    add-int/lit8 v6, p1, -0x18

    add-int v0, v4, v6

    .line 189
    .local v0, "index":I
    const/16 v4, 0x40

    :try_start_0
    new-array v2, v4, [B

    .line 190
    .local v2, "qtable":[B
    sget-object v4, Landroid_maps_conflict_avoidance/com/google/image/compression/jpeg/JpegUtil;->JPEG_QUANT_TABLES:[[B

    aget-object v3, v4, p0

    .line 191
    .local v3, "rawTable":[B
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_0
    if-ge v1, v7, :cond_0

    .line 192
    aget-byte v4, v3, v1

    and-int/lit16 v4, v4, 0xff

    invoke-static {v4, p1, p2}, Landroid_maps_conflict_avoidance/com/google/image/compression/jpeg/JpegUtil;->getScaledQuantizationFactor(III)B

    move-result v4

    aput-byte v4, v2, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 191
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 195
    :cond_0
    monitor-exit v5

    return-object v2

    .line 188
    .end local v1    # "j":I
    .end local v2    # "qtable":[B
    .end local v3    # "rawTable":[B
    :catchall_0
    move-exception v4

    monitor-exit v5

    throw v4
.end method

.method public static getScaledQuantizationFactor(III)B
    .locals 6
    .param p0, "q"    # I
    .param p1, "quality"    # I
    .param p2, "qualityAlgorithm"    # I

    .prologue
    const/16 v3, 0x1388

    .line 139
    packed-switch p2, :pswitch_data_0

    .line 165
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "qualityAlgorithm"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 146
    :pswitch_0
    const/16 v2, 0x63

    if-ne p0, v2, :cond_1

    const/16 v2, 0x24

    if-ne p1, v2, :cond_1

    .line 147
    const/16 v1, 0x8a

    .line 169
    .local v1, "val":I
    :goto_0
    const/4 v2, 0x1

    if-ge v1, v2, :cond_3

    .line 170
    const/4 v1, 0x1

    .line 174
    :cond_0
    :goto_1
    int-to-byte v2, v1

    return v2

    .line 149
    .end local v1    # "val":I
    :cond_1
    int-to-long v2, p0

    sget-object v4, Landroid_maps_conflict_avoidance/com/google/image/compression/jpeg/JpegUtil;->imageIoScaleFactor:[I

    aget v4, v4, p1

    int-to-long v4, v4

    mul-long/2addr v2, v4

    const-wide/32 v4, 0x1000000

    div-long/2addr v2, v4

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    const-wide/16 v4, 0x2

    div-long/2addr v2, v4

    long-to-int v1, v2

    .line 152
    .restart local v1    # "val":I
    goto :goto_0

    .line 156
    .end local v1    # "val":I
    :pswitch_1
    const/16 v2, 0x32

    if-ge p1, v2, :cond_2

    .line 157
    div-int v2, v3, p1

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 161
    .local v0, "iscale":I
    :goto_2
    mul-int v2, p0, v0

    add-int/lit8 v2, v2, 0x32

    div-int/lit8 v1, v2, 0x64

    .line 162
    .restart local v1    # "val":I
    goto :goto_0

    .line 159
    .end local v0    # "iscale":I
    .end local v1    # "val":I
    :cond_2
    mul-int/lit8 v2, p1, 0x2

    rsub-int v2, v2, 0xc8

    const/4 v3, 0x0

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v0

    .restart local v0    # "iscale":I
    goto :goto_2

    .line 171
    .end local v0    # "iscale":I
    .restart local v1    # "val":I
    :cond_3
    const/16 v2, 0xff

    if-le v1, v2, :cond_0

    .line 172
    const/16 v1, 0xff

    goto :goto_1

    .line 139
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static prependStandardHeader([BII[BILandroid_maps_conflict_avoidance/com/google/image/compression/jpeg/JpegHeaderParams;)V
    .locals 8
    .param p0, "src"    # [B
    .param p1, "soff"    # I
    .param p2, "len"    # I
    .param p3, "dst"    # [B
    .param p4, "doff"    # I
    .param p5, "params"    # Landroid_maps_conflict_avoidance/com/google/image/compression/jpeg/JpegHeaderParams;

    .prologue
    .line 218
    invoke-virtual {p5}, Landroid_maps_conflict_avoidance/com/google/image/compression/jpeg/JpegHeaderParams;->getVariant()I

    move-result v2

    .line 219
    .local v2, "variant":I
    invoke-virtual {p5}, Landroid_maps_conflict_avoidance/com/google/image/compression/jpeg/JpegHeaderParams;->getWidth()I

    move-result v3

    .line 220
    .local v3, "width":I
    invoke-virtual {p5}, Landroid_maps_conflict_avoidance/com/google/image/compression/jpeg/JpegHeaderParams;->getHeight()I

    move-result v4

    .line 221
    .local v4, "height":I
    invoke-virtual {p5}, Landroid_maps_conflict_avoidance/com/google/image/compression/jpeg/JpegHeaderParams;->getQuality()I

    move-result v5

    .line 222
    .local v5, "quality":I
    invoke-virtual {p5}, Landroid_maps_conflict_avoidance/com/google/image/compression/jpeg/JpegHeaderParams;->getQualityAlgorithm()I

    move-result v6

    .line 223
    .local v6, "qualityAlgorithm":I
    if-eqz v2, :cond_0

    .line 224
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "variant"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 226
    :cond_0
    invoke-static {v2}, Landroid_maps_conflict_avoidance/com/google/image/compression/jpeg/GenerateJpegHeader;->getHeaderLength(I)I

    move-result v7

    .line 230
    .local v7, "hlen":I
    add-int v0, p4, v7

    invoke-static {p0, p1, p3, v0, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object v0, p3

    move v1, p4

    .line 232
    invoke-static/range {v0 .. v6}, Landroid_maps_conflict_avoidance/com/google/image/compression/jpeg/GenerateJpegHeader;->generate([BIIIIII)I

    .line 234
    return-void
.end method

.method public static uncompactJpeg([B)[B
    .locals 2
    .param p0, "compactJpegData"    # [B

    .prologue
    .line 382
    const/4 v0, 0x0

    array-length v1, p0

    invoke-static {p0, v0, v1}, Landroid_maps_conflict_avoidance/com/google/image/compression/jpeg/JpegUtil;->uncompactJpeg([BII)[B

    move-result-object v0

    return-object v0
.end method

.method public static uncompactJpeg([BII)[B
    .locals 19
    .param p0, "compactJpegData"    # [B
    .param p1, "off"    # I
    .param p2, "len"    # I

    .prologue
    .line 333
    aget-byte v11, p0, p1

    const/4 v12, -0x1

    if-ne v11, v12, :cond_0

    add-int/lit8 v11, p1, 0x1

    aget-byte v11, p0, v11

    const/16 v12, -0x28

    if-ne v11, v12, :cond_0

    .line 336
    move/from16 v0, p2

    new-array v0, v0, [B

    move-object/from16 v17, v0

    .line 337
    .local v17, "data":[B
    const/4 v11, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p1

    move-object/from16 v2, v17

    move/from16 v3, p2

    invoke-static {v0, v1, v2, v11, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 366
    .end local v17    # "data":[B
    :goto_0
    return-object v17

    .line 341
    :cond_0
    aget-byte v11, p0, p1

    const/16 v12, 0x43

    if-ne v11, v12, :cond_1

    add-int/lit8 v11, p1, 0x1

    aget-byte v11, p0, v11

    const/16 v12, 0x4a

    if-ne v11, v12, :cond_1

    add-int/lit8 v11, p1, 0x2

    aget-byte v11, p0, v11

    const/16 v12, 0x50

    if-ne v11, v12, :cond_1

    add-int/lit8 v11, p1, 0x3

    aget-byte v11, p0, v11

    const/16 v12, 0x47

    if-eq v11, v12, :cond_2

    .line 343
    :cond_1
    new-instance v11, Ljava/lang/IllegalArgumentException;

    const-string v12, "Input is not in compact JPEG format"

    invoke-direct {v11, v12}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v11

    .line 346
    :cond_2
    add-int/lit8 v11, p1, 0x4

    aget-byte v11, p0, v11

    and-int/lit16 v5, v11, 0xff

    .line 347
    .local v5, "variant":I
    add-int/lit8 v11, p1, 0x5

    aget-byte v11, p0, v11

    and-int/lit16 v11, v11, 0xff

    shl-int/lit8 v11, v11, 0x8

    add-int/lit8 v12, p1, 0x6

    aget-byte v12, p0, v12

    and-int/lit16 v12, v12, 0xff

    or-int v6, v11, v12

    .line 349
    .local v6, "width":I
    add-int/lit8 v11, p1, 0x7

    aget-byte v11, p0, v11

    and-int/lit16 v11, v11, 0xff

    shl-int/lit8 v11, v11, 0x8

    add-int/lit8 v12, p1, 0x8

    aget-byte v12, p0, v12

    and-int/lit16 v12, v12, 0xff

    or-int v7, v11, v12

    .line 351
    .local v7, "height":I
    add-int/lit8 v11, p1, 0x9

    aget-byte v11, p0, v11

    and-int/lit16 v8, v11, 0xff

    .line 352
    .local v8, "quality":I
    add-int/lit8 v11, p1, 0xa

    aget-byte v11, p0, v11

    and-int/lit16 v9, v11, 0xff

    .line 356
    .local v9, "qualityAlgorithm":I
    :try_start_0
    invoke-static {v5}, Landroid_maps_conflict_avoidance/com/google/image/compression/jpeg/GenerateJpegHeader;->getHeaderLength(I)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v10

    .line 361
    .local v10, "hlen":I
    add-int v11, v10, p2

    add-int/lit8 v11, v11, -0xb

    new-array v14, v11, [B

    .line 362
    .local v14, "jpegData":[B
    new-instance v4, Landroid_maps_conflict_avoidance/com/google/image/compression/jpeg/JpegHeaderParams;

    invoke-direct/range {v4 .. v10}, Landroid_maps_conflict_avoidance/com/google/image/compression/jpeg/JpegHeaderParams;-><init>(IIIIII)V

    .line 364
    .local v4, "params":Landroid_maps_conflict_avoidance/com/google/image/compression/jpeg/JpegHeaderParams;
    add-int/lit8 v12, p1, 0xb

    add-int/lit8 v13, p2, -0xb

    const/4 v15, 0x0

    move-object/from16 v11, p0

    move-object/from16 v16, v4

    invoke-static/range {v11 .. v16}, Landroid_maps_conflict_avoidance/com/google/image/compression/jpeg/JpegUtil;->prependStandardHeader([BII[BILandroid_maps_conflict_avoidance/com/google/image/compression/jpeg/JpegHeaderParams;)V

    move-object/from16 v17, v14

    .line 366
    goto :goto_0

    .line 357
    .end local v4    # "params":Landroid_maps_conflict_avoidance/com/google/image/compression/jpeg/JpegHeaderParams;
    .end local v10    # "hlen":I
    .end local v14    # "jpegData":[B
    :catch_0
    move-exception v18

    .line 358
    .local v18, "e":Ljava/lang/IllegalArgumentException;
    new-instance v11, Ljava/lang/IllegalArgumentException;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Unknown variant "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v11
.end method
