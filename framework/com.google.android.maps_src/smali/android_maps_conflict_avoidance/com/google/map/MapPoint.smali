.class public Landroid_maps_conflict_avoidance/com/google/map/MapPoint;
.super Ljava/lang/Object;
.source "MapPoint.java"

# interfaces
.implements Landroid_maps_conflict_avoidance/com/google/map/Geometry;


# static fields
.field private static final PIXEL_COORDS_ZOOM:Landroid_maps_conflict_avoidance/com/google/map/Zoom;

.field static mercatorValues:[I

.field static reverseMercatorValues:[I


# instance fields
.field private final latitudeE6:I

.field private final longitudeE6:I

.field private final pixelCoordX:I

.field private final pixelCoordY:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 61
    sput-object v0, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->mercatorValues:[I

    .line 62
    sput-object v0, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->reverseMercatorValues:[I

    .line 71
    const/16 v0, 0x16

    invoke-static {v0}, Landroid_maps_conflict_avoidance/com/google/map/Zoom;->getZoom(I)Landroid_maps_conflict_avoidance/com/google/map/Zoom;

    move-result-object v0

    sput-object v0, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->PIXEL_COORDS_ZOOM:Landroid_maps_conflict_avoidance/com/google/map/Zoom;

    .line 72
    return-void
.end method

.method public constructor <init>(II)V
    .locals 1
    .param p1, "latitude"    # I
    .param p2, "longitude"    # I

    .prologue
    .line 125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 127
    invoke-static {p2}, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->adjustLongitude(I)I

    move-result p2

    .line 128
    const v0, 0x4c4b400

    if-le p1, v0, :cond_0

    .line 129
    const p1, 0x4c4b400

    .line 132
    :cond_0
    const v0, -0x4c4b400

    if-ge p1, v0, :cond_1

    .line 133
    const p1, -0x4c4b400

    .line 136
    :cond_1
    iput p1, p0, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->latitudeE6:I

    .line 137
    iput p2, p0, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->longitudeE6:I

    .line 138
    sget-object v0, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->PIXEL_COORDS_ZOOM:Landroid_maps_conflict_avoidance/com/google/map/Zoom;

    invoke-static {p2, v0}, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->getXPixelFromLon(ILandroid_maps_conflict_avoidance/com/google/map/Zoom;)I

    move-result v0

    iput v0, p0, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->pixelCoordX:I

    .line 139
    sget-object v0, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->PIXEL_COORDS_ZOOM:Landroid_maps_conflict_avoidance/com/google/map/Zoom;

    invoke-static {p1, v0}, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->getYPixelFromLat(ILandroid_maps_conflict_avoidance/com/google/map/Zoom;)I

    move-result v0

    iput v0, p0, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->pixelCoordY:I

    .line 140
    return-void
.end method

.method public constructor <init>(III)V
    .locals 3
    .param p1, "pixelX"    # I
    .param p2, "pixelY"    # I
    .param p3, "zoomLevel"    # I

    .prologue
    const/16 v2, 0x16

    .line 150
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 151
    invoke-static {p3}, Landroid_maps_conflict_avoidance/com/google/map/Zoom;->getZoom(I)Landroid_maps_conflict_avoidance/com/google/map/Zoom;

    move-result-object v0

    .line 152
    .local v0, "zoom":Landroid_maps_conflict_avoidance/com/google/map/Zoom;
    invoke-virtual {v0}, Landroid_maps_conflict_avoidance/com/google/map/Zoom;->getEquatorPixels()I

    move-result v1

    invoke-static {p1, v1}, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->adjustPixelX(II)I

    move-result p1

    .line 153
    invoke-virtual {v0}, Landroid_maps_conflict_avoidance/com/google/map/Zoom;->getEquatorPixels()I

    move-result v1

    invoke-static {p2, v1}, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->adjustPixelY(II)I

    move-result p2

    .line 154
    invoke-virtual {v0, p1, v2}, Landroid_maps_conflict_avoidance/com/google/map/Zoom;->changePixelsToTargetZoomlevel(II)I

    move-result v1

    iput v1, p0, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->pixelCoordX:I

    .line 156
    invoke-virtual {v0, p2, v2}, Landroid_maps_conflict_avoidance/com/google/map/Zoom;->changePixelsToTargetZoomlevel(II)I

    move-result v1

    iput v1, p0, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->pixelCoordY:I

    .line 158
    iget v1, p0, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->pixelCoordY:I

    sget-object v2, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->PIXEL_COORDS_ZOOM:Landroid_maps_conflict_avoidance/com/google/map/Zoom;

    invoke-static {v1, v2}, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->getLatitudeFromY(ILandroid_maps_conflict_avoidance/com/google/map/Zoom;)I

    move-result v1

    iput v1, p0, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->latitudeE6:I

    .line 159
    iget v1, p0, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->pixelCoordX:I

    sget-object v2, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->PIXEL_COORDS_ZOOM:Landroid_maps_conflict_avoidance/com/google/map/Zoom;

    invoke-static {v1, v2}, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->getLongitudeFromX(ILandroid_maps_conflict_avoidance/com/google/map/Zoom;)I

    move-result v1

    iput v1, p0, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->longitudeE6:I

    .line 160
    return-void
.end method

.method public constructor <init>(IIIII)V
    .locals 3
    .param p1, "pixelX"    # I
    .param p2, "pixelY"    # I
    .param p3, "zoomLevel"    # I
    .param p4, "latitude"    # I
    .param p5, "longitude"    # I

    .prologue
    const/16 v2, 0x16

    .line 173
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 174
    invoke-static {p5}, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->adjustLongitude(I)I

    move-result p5

    .line 175
    iput p4, p0, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->latitudeE6:I

    .line 176
    iput p5, p0, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->longitudeE6:I

    .line 177
    invoke-static {p3}, Landroid_maps_conflict_avoidance/com/google/map/Zoom;->getZoom(I)Landroid_maps_conflict_avoidance/com/google/map/Zoom;

    move-result-object v0

    .line 178
    .local v0, "zoom":Landroid_maps_conflict_avoidance/com/google/map/Zoom;
    invoke-virtual {v0}, Landroid_maps_conflict_avoidance/com/google/map/Zoom;->getEquatorPixels()I

    move-result v1

    invoke-static {p1, v1}, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->adjustPixelX(II)I

    move-result p1

    .line 179
    invoke-virtual {v0}, Landroid_maps_conflict_avoidance/com/google/map/Zoom;->getEquatorPixels()I

    move-result v1

    invoke-static {p2, v1}, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->adjustPixelY(II)I

    move-result p2

    .line 180
    invoke-virtual {v0, p1, v2}, Landroid_maps_conflict_avoidance/com/google/map/Zoom;->changePixelsToTargetZoomlevel(II)I

    move-result v1

    iput v1, p0, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->pixelCoordX:I

    .line 182
    invoke-virtual {v0, p2, v2}, Landroid_maps_conflict_avoidance/com/google/map/Zoom;->changePixelsToTargetZoomlevel(II)I

    move-result v1

    iput v1, p0, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->pixelCoordY:I

    .line 184
    return-void
.end method

.method private static adjustLongitude(I)I
    .locals 2
    .param p0, "longitude"    # I

    .prologue
    const v1, 0x15752a00

    .line 80
    :goto_0
    const v0, -0xaba9500

    if-ge p0, v0, :cond_0

    .line 81
    add-int/2addr p0, v1

    goto :goto_0

    .line 83
    :cond_0
    :goto_1
    const v0, 0xaba9500

    if-le p0, v0, :cond_1

    .line 84
    sub-int/2addr p0, v1

    goto :goto_1

    .line 86
    :cond_1
    return p0
.end method

.method private static adjustPixelX(II)I
    .locals 0
    .param p0, "pixelX"    # I
    .param p1, "equatorPixels"    # I

    .prologue
    .line 95
    :goto_0
    if-lt p0, p1, :cond_0

    .line 96
    sub-int/2addr p0, p1

    goto :goto_0

    .line 98
    :cond_0
    :goto_1
    if-gez p0, :cond_1

    .line 99
    add-int/2addr p0, p1

    goto :goto_1

    .line 101
    :cond_1
    return p0
.end method

.method private static adjustPixelY(II)I
    .locals 0
    .param p0, "pixelY"    # I
    .param p1, "equatorPixels"    # I

    .prologue
    .line 111
    if-gez p0, :cond_1

    .line 112
    const/4 p0, 0x0

    .line 116
    .end local p0    # "pixelY":I
    :cond_0
    :goto_0
    return p0

    .line 113
    .restart local p0    # "pixelY":I
    :cond_1
    if-lt p0, p1, :cond_0

    .line 114
    add-int/lit8 p0, p1, -0x1

    goto :goto_0
.end method

.method private static getLatitudeFromY(ILandroid_maps_conflict_avoidance/com/google/map/Zoom;)I
    .locals 18
    .param p0, "y"    # I
    .param p1, "zoom"    # Landroid_maps_conflict_avoidance/com/google/map/Zoom;

    .prologue
    .line 417
    invoke-static {}, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->getReverseMercatorValues()[I

    move-result-object v6

    .line 419
    .local v6, "data":[I
    invoke-virtual/range {p1 .. p1}, Landroid_maps_conflict_avoidance/com/google/map/Zoom;->getEquatorPixels()I

    move-result v11

    div-int/lit8 v11, v11, 0x2

    sub-int v11, v11, p0

    move-object/from16 v0, p1

    invoke-static {v11, v0}, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->pixelsToMercator(ILandroid_maps_conflict_avoidance/com/google/map/Zoom;)I

    move-result v9

    .line 420
    .local v9, "mercator":I
    invoke-static {v9}, Ljava/lang/Math;->abs(I)I

    move-result v11

    const v12, 0x4c4b40

    div-int/2addr v11, v12

    add-int/lit8 v7, v11, 0x1

    .line 423
    .local v7, "indexFloor":I
    array-length v11, v6

    add-int/lit8 v11, v11, -0x2

    if-lt v7, v11, :cond_1

    .line 424
    if-lez v9, :cond_0

    const v11, 0x4c4b400

    .line 450
    :goto_0
    return v11

    .line 424
    :cond_0
    const v11, -0x4c4b400

    goto :goto_0

    .line 427
    :cond_1
    invoke-static {v9}, Ljava/lang/Math;->abs(I)I

    move-result v11

    const v12, 0x4c4b40

    rem-int v10, v11, v12

    .line 428
    .local v10, "yDiff":I
    add-int/lit8 v11, v7, -0x1

    aget v11, v6, v11

    mul-int/lit8 v11, v11, -0x1

    aget v12, v6, v7

    mul-int/lit8 v12, v12, 0x3

    add-int/2addr v11, v12

    add-int/lit8 v12, v7, 0x1

    aget v12, v6, v12

    mul-int/lit8 v12, v12, 0x3

    sub-int/2addr v11, v12

    add-int/lit8 v12, v7, 0x2

    aget v12, v6, v12

    add-int/2addr v11, v12

    div-int/lit8 v2, v11, 0x6

    .line 432
    .local v2, "cubicA":I
    add-int/lit8 v11, v7, -0x1

    aget v11, v6, v11

    mul-int/lit8 v11, v11, 0x3

    aget v12, v6, v7

    mul-int/lit8 v12, v12, 0x6

    sub-int/2addr v11, v12

    add-int/lit8 v12, v7, 0x1

    aget v12, v6, v12

    mul-int/lit8 v12, v12, 0x3

    add-int/2addr v11, v12

    div-int/lit8 v3, v11, 0x6

    .line 435
    .local v3, "cubicB":I
    add-int/lit8 v11, v7, -0x1

    aget v11, v6, v11

    mul-int/lit8 v11, v11, -0x2

    aget v12, v6, v7

    mul-int/lit8 v12, v12, 0x3

    sub-int/2addr v11, v12

    add-int/lit8 v12, v7, 0x1

    aget v12, v6, v12

    mul-int/lit8 v12, v12, 0x6

    add-int/2addr v11, v12

    add-int/lit8 v12, v7, 0x2

    aget v12, v6, v12

    sub-int/2addr v11, v12

    div-int/lit8 v4, v11, 0x6

    .line 439
    .local v4, "cubicC":I
    aget v5, v6, v7

    .line 440
    .local v5, "cubicD":I
    int-to-long v12, v2

    int-to-long v14, v10

    mul-long/2addr v12, v14

    int-to-long v14, v10

    mul-long/2addr v12, v14

    const-wide/32 v14, 0x4c4b40

    div-long/2addr v12, v14

    int-to-long v14, v10

    mul-long/2addr v12, v14

    const-wide/32 v14, 0x4c4b40

    div-long/2addr v12, v14

    const-wide/32 v14, 0x4c4b40

    div-long/2addr v12, v14

    int-to-long v14, v3

    int-to-long v0, v10

    move-wide/from16 v16, v0

    mul-long v14, v14, v16

    int-to-long v0, v10

    move-wide/from16 v16, v0

    mul-long v14, v14, v16

    const-wide/32 v16, 0x4c4b40

    div-long v14, v14, v16

    const-wide/32 v16, 0x4c4b40

    div-long v14, v14, v16

    add-long/2addr v12, v14

    int-to-long v14, v4

    int-to-long v0, v10

    move-wide/from16 v16, v0

    mul-long v14, v14, v16

    const-wide/32 v16, 0x4c4b40

    div-long v14, v14, v16

    add-long/2addr v12, v14

    int-to-long v14, v5

    add-long/2addr v12, v14

    long-to-int v8, v12

    .line 446
    .local v8, "latitude":I
    const v11, 0x4c4b400

    if-le v8, v11, :cond_2

    .line 447
    const v8, 0x4c4b400

    .line 449
    :cond_2
    if-gez v9, :cond_3

    neg-int v8, v8

    :cond_3
    move v11, v8

    .line 450
    goto/16 :goto_0
.end method

.method private static getLongitudeFromX(ILandroid_maps_conflict_avoidance/com/google/map/Zoom;)I
    .locals 4
    .param p0, "x"    # I
    .param p1, "zoom"    # Landroid_maps_conflict_avoidance/com/google/map/Zoom;

    .prologue
    .line 485
    invoke-virtual {p1}, Landroid_maps_conflict_avoidance/com/google/map/Zoom;->getEquatorPixels()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    sub-int v0, p0, v0

    int-to-long v0, v0

    const-wide/32 v2, 0x15752a00

    mul-long/2addr v0, v2

    invoke-virtual {p1}, Landroid_maps_conflict_avoidance/com/google/map/Zoom;->getEquatorPixels()I

    move-result v2

    int-to-long v2, v2

    div-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method public static getMapPointFromXY(III)Landroid_maps_conflict_avoidance/com/google/map/MapPoint;
    .locals 1
    .param p0, "pixelX"    # I
    .param p1, "pixelY"    # I
    .param p2, "zoomLevel"    # I

    .prologue
    .line 815
    new-instance v0, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;

    invoke-direct {v0, p0, p1, p2}, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;-><init>(III)V

    return-object v0
.end method

.method private static getMercatorIndex(I)I
    .locals 2
    .param p0, "latitude"    # I

    .prologue
    .line 639
    invoke-static {p0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    const v1, 0xf4240

    div-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method private static getMercatorValues()[I
    .locals 4

    .prologue
    .line 223
    sget-object v2, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->mercatorValues:[I

    if-nez v2, :cond_0

    .line 224
    const/16 v2, 0x54

    new-array v2, v2, [I

    sput-object v2, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->mercatorValues:[I

    .line 227
    const/16 v2, 0xfd

    :try_start_0
    new-array v1, v2, [B

    fill-array-data v1, :array_0

    .line 246
    .local v1, "mercbytes":[B
    invoke-static {v1}, Landroid_maps_conflict_avoidance/com/google/common/io/IoUtil;->createDataInputFromBytes([B)Ljava/io/DataInput;

    move-result-object v2

    sget-object v3, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->mercatorValues:[I

    invoke-static {v2, v3}, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->readMercatorValues(Ljava/io/DataInput;[I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 252
    :cond_0
    sget-object v2, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->mercatorValues:[I

    return-object v2

    .line 247
    :catch_0
    move-exception v0

    .line 248
    .local v0, "e":Ljava/io/IOException;
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Can\'t read mercator.dat"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 227
    :array_0
    .array-data 1
        -0x1t
        -0x10t
        -0x43t
        -0x73t
        0xft
        0x42t
        0x73t
        0xft
        0x42t
        0x73t
        0xft
        0x43t
        -0x5dt
        0xft
        0x46t
        0x6t
        0xft
        0x49t
        -0x67t
        0xft
        0x4et
        0x61t
        0xft
        0x54t
        0x5et
        0xft
        0x5bt
        -0x6dt
        0xft
        0x64t
        0x2t
        0xft
        0x6dt
        -0x50t
        0xft
        0x78t
        -0x61t
        0xft
        -0x7ct
        -0x2ct
        0xft
        -0x6et
        0x54t
        0xft
        -0x5ft
        0x26t
        0xft
        -0x4ft
        0x4et
        0xft
        -0x3et
        -0x2dt
        0xft
        -0x2bt
        -0x43t
        0xft
        -0x16t
        0x15t
        0xft
        -0x1t
        -0x1ft
        0x10t
        0x17t
        0x2dt
        0x10t
        0x30t
        0x1t
        0x10t
        0x4at
        0x6bt
        0x10t
        0x66t
        0x74t
        0x10t
        -0x7ct
        0x2bt
        0x10t
        -0x5dt
        -0x64t
        0x10t
        -0x3ct
        -0x29t
        0x10t
        -0x19t
        -0x13t
        0x11t
        0xct
        -0x12t
        0x11t
        0x33t
        -0x14t
        0x11t
        0x5ct
        -0x4t
        0x11t
        -0x78t
        0x34t
        0x11t
        -0x4bt
        -0x57t
        0x11t
        -0x1bt
        0x76t
        0x12t
        0x17t
        -0x4ct
        0x12t
        0x4ct
        -0x7ft
        0x12t
        -0x7dt
        -0x3t
        0x12t
        -0x42t
        0x46t
        0x12t
        -0x5t
        -0x7ct
        0x13t
        0x3bt
        -0x25t
        0x13t
        0x7ft
        0x77t
        0x13t
        -0x3at
        -0x7at
        0x14t
        0x11t
        0x38t
        0x14t
        0x5ft
        -0x3ct
        0x14t
        -0x4et
        0x64t
        0x15t
        0x9t
        0x57t
        0x15t
        0x64t
        -0x1bt
        0x15t
        -0x3bt
        0x56t
        0x16t
        0x2at
        -0x1t
        0x16t
        -0x6at
        0x3at
        0x17t
        0x7t
        0x6dt
        0x17t
        0x7ft
        0x2t
        0x17t
        -0x3t
        0x75t
        0x18t
        -0x7dt
        0x48t
        0x19t
        0x11t
        0x14t
        0x19t
        -0x59t
        0x78t
        0x1at
        0x47t
        0x2et
        0x1at
        -0xft
        0x3t
        0x1bt
        -0x5bt
        -0x27t
        0x1ct
        0x66t
        -0x4dt
        0x1dt
        0x34t
        -0x4dt
        0x1et
        0x11t
        0x1ft
        0x1et
        -0x3t
        0x6ft
        0x1ft
        -0x5t
        0x4at
        0x21t
        0xct
        -0x69t
        0x22t
        0x33t
        -0x78t
        0x23t
        0x72t
        -0x5bt
        0x24t
        -0x34t
        -0x1et
        0x26t
        0x45t
        -0x4ct
        0x27t
        -0x1ft
        0x2at
        0x29t
        -0x5ct
        0x19t
        0x2bt
        -0x6ct
        0x46t
        0x2dt
        -0x48t
        -0x5bt
        0x30t
        0x19t
        -0x54t
        0x32t
        -0x3ft
        -0x3ft
        0x35t
        -0x43t
        -0x2ft
        0x39t
        0x1et
        0x1ct
        0x3ct
        -0x9t
        0x69t
        0x41t
        0x64t
        -0x60t
        0x46t
        -0x77t
        0x52t
        0x4ct
        -0x6bt
        0x73t
        0x53t
        -0x35t
        0x4ft
        0x5ct
        -0x77t
        0x34t
        0x67t
        0x5at
        0xct
    .end array-data
.end method

.method private static getReverseMercatorValues()[I
    .locals 4

    .prologue
    .line 351
    sget-object v2, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->reverseMercatorValues:[I

    if-nez v2, :cond_0

    .line 352
    const/16 v2, 0x8d

    new-array v2, v2, [I

    sput-object v2, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->reverseMercatorValues:[I

    .line 355
    const/16 v2, 0x1a8

    :try_start_0
    new-array v1, v2, [B

    fill-array-data v1, :array_0

    .line 392
    .local v1, "latBytes":[B
    invoke-static {v1}, Landroid_maps_conflict_avoidance/com/google/common/io/IoUtil;->createDataInputFromBytes([B)Ljava/io/DataInput;

    move-result-object v2

    sget-object v3, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->reverseMercatorValues:[I

    invoke-static {v2, v3}, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->readMercatorValues(Ljava/io/DataInput;[I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 398
    :cond_0
    sget-object v2, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->reverseMercatorValues:[I

    return-object v2

    .line 393
    :catch_0
    move-exception v0

    .line 394
    .local v0, "e":Ljava/io/IOException;
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "rmercator.dat is incorrect"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 355
    :array_0
    .array-data 1
        -0x1t
        -0x1ct
        -0x77t
        -0x18t
        0x1bt
        0x76t
        0x18t
        0x1bt
        0x76t
        0x18t
        0x1bt
        0x6ft
        0x2at
        0x1bt
        0x61t
        0x55t
        0x1bt
        0x4ct
        -0x52t
        0x1bt
        0x31t
        0x4bt
        0x1bt
        0xft
        0x4ft
        0x1at
        -0x1at
        -0x1at
        0x1at
        -0x48t
        0x3dt
        0x1at
        -0x7dt
        -0x72t
        0x1at
        0x49t
        0x16t
        0x1at
        0x9t
        0x19t
        0x19t
        -0x3dt
        -0x23t
        0x19t
        0x79t
        -0x51t
        0x19t
        0x2at
        -0x22t
        0x18t
        -0x29t
        -0x46t
        0x18t
        -0x80t
        -0x67t
        0x18t
        0x25t
        -0x33t
        0x17t
        -0x39t
        -0x52t
        0x17t
        0x66t
        -0x73t
        0x17t
        0x2t
        -0x40t
        0x16t
        -0x64t
        -0x65t
        0x16t
        0x34t
        0x6dt
        0x15t
        -0x36t
        -0x7at
        0x15t
        0x5ft
        0x30t
        0x14t
        -0xet
        -0x48t
        0x14t
        -0x7bt
        0x61t
        0x14t
        0x17t
        0x6ft
        0x13t
        -0x57t
        0x21t
        0x13t
        0x3at
        -0x4ct
        0x12t
        -0x34t
        0x5et
        0x12t
        0x5et
        0x55t
        0x11t
        -0x10t
        -0x38t
        0x11t
        -0x7dt
        -0x1at
        0x11t
        0x17t
        -0x2at
        0x10t
        -0x54t
        -0x40t
        0x10t
        0x42t
        -0x3dt
        0xft
        -0x26t
        0x2t
        0xft
        0x72t
        -0x6at
        0xft
        0xct
        -0x67t
        0xet
        -0x58t
        0x20t
        0xet
        0x45t
        0x40t
        0xdt
        -0x1ct
        0x7t
        0xdt
        -0x7ct
        -0x7at
        0xdt
        0x26t
        -0x3at
        0xct
        -0x36t
        -0x2dt
        0xct
        0x70t
        -0x4bt
        0xct
        0x18t
        0x71t
        0xbt
        -0x3et
        0xat
        0xbt
        0x6dt
        -0x79t
        0xbt
        0x1at
        -0x1bt
        0xat
        -0x36t
        0x28t
        0xat
        0x7bt
        0x4dt
        0xat
        0x2et
        0x52t
        0x9t
        -0x1dt
        0x34t
        0x9t
        -0x67t
        -0x10t
        0x9t
        0x52t
        -0x7et
        0x9t
        0xct
        -0x1dt
        0x8t
        -0x37t
        0xet
        0x8t
        -0x7at
        -0x2t
        0x8t
        0x46t
        -0x56t
        0x8t
        0x8t
        0xdt
        0x7t
        -0x35t
        0x1et
        0x7t
        -0x71t
        -0x29t
        0x7t
        0x56t
        0x2dt
        0x7t
        0x1et
        0x1bt
        0x6t
        -0x19t
        -0x68t
        0x6t
        -0x4et
        -0x66t
        0x6t
        0x7ft
        0x1at
        0x6t
        0x4dt
        0xft
        0x6t
        0x1ct
        0x72t
        0x5t
        -0x13t
        0x38t
        0x5t
        -0x41t
        0x59t
        0x5t
        -0x6et
        -0x32t
        0x5t
        0x67t
        -0x72t
        0x5t
        0x3dt
        -0x6ft
        0x5t
        0x14t
        -0x33t
        0x4t
        -0x13t
        0x3bt
        0x4t
        -0x3at
        -0x2bt
        0x4t
        -0x5ft
        -0x71t
        0x4t
        0x7dt
        0x66t
        0x4t
        0x5at
        0x4et
        0x4t
        0x38t
        0x41t
        0x4t
        0x17t
        0x3at
        0x3t
        -0x9t
        0x2ft
        0x3t
        -0x28t
        0x1at
        0x3t
        -0x47t
        -0xdt
        0x3t
        -0x64t
        -0x4bt
        0x3t
        -0x80t
        0x58t
        0x3t
        0x64t
        -0x29t
        0x3t
        0x4at
        0x2bt
        0x3t
        0x30t
        0x4ct
        0x3t
        0x17t
        0x37t
        0x2t
        -0x2t
        -0x1ct
        0x2t
        -0x19t
        0x4ft
        0x2t
        -0x30t
        0x72t
        0x2t
        -0x46t
        0x46t
        0x2t
        -0x5ct
        -0x38t
        0x2t
        -0x71t
        -0xft
        0x2t
        0x7bt
        -0x43t
        0x2t
        0x68t
        0x28t
        0x2t
        0x55t
        0x2bt
        0x2t
        0x42t
        -0x3dt
        0x2t
        0x30t
        -0x14t
        0x2t
        0x1ft
        -0x60t
        0x2t
        0xet
        -0x23t
        0x1t
        -0x2t
        -0x64t
        0x1t
        -0x12t
        -0x24t
        0x1t
        -0x21t
        -0x6at
        0x1t
        -0x30t
        -0x35t
        0x1t
        -0x3et
        0x72t
        0x1t
        -0x4ct
        -0x76t
        0x1t
        -0x59t
        0x11t
        0x1t
        -0x66t
        0x1t
        0x1t
        -0x73t
        0x58t
        0x1t
        -0x7ft
        0x12t
        0x1t
        0x75t
        0x2ft
        0x1t
        0x69t
        -0x59t
        0x1t
        0x5et
        0x7ct
        0x1t
        0x53t
        -0x58t
        0x1t
        0x49t
        0x2at
        0x1t
        0x3et
        -0x1t
        0x1t
        0x35t
        0x23t
        0x1t
        0x2bt
        -0x69t
        0x1t
        0x22t
        0x54t
        0x1t
        0x19t
        0x5bt
        0x1t
        0x10t
        -0x56t
        0x1t
        0x8t
        0x3ct
        0x1t
        0x0t
        0x11t
        0x0t
        -0x8t
        0x28t
        0x0t
        -0x10t
        0x7ct
        0x0t
        -0x17t
        0xdt
        0x0t
        -0x1ft
        -0x28t
        0x0t
        -0x26t
        -0x22t
        0x0t
        -0x2ct
        0x19t
        0x0t
        -0x33t
        -0x75t
        0x0t
        -0x39t
        0x30t
        0x0t
        -0x3ft
        0x8t
        0x0t
        -0x45t
        0x10t
        0x0t
        -0x4bt
        0x47t
    .end array-data
.end method

.method public static getXPixelFromLon(ILandroid_maps_conflict_avoidance/com/google/map/Zoom;)I
    .locals 6
    .param p0, "longitude"    # I
    .param p1, "zoom"    # Landroid_maps_conflict_avoidance/com/google/map/Zoom;

    .prologue
    .line 212
    invoke-virtual {p1}, Landroid_maps_conflict_avoidance/com/google/map/Zoom;->getEquatorPixels()I

    move-result v0

    int-to-long v0, v0

    const-wide/16 v2, 0x2

    div-long/2addr v0, v2

    int-to-long v2, p0

    invoke-virtual {p1}, Landroid_maps_conflict_avoidance/com/google/map/Zoom;->getEquatorPixels()I

    move-result v4

    int-to-long v4, v4

    mul-long/2addr v2, v4

    const-wide/32 v4, 0x15752a00

    div-long/2addr v2, v4

    add-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method public static getYPixelFromLat(ILandroid_maps_conflict_avoidance/com/google/map/Zoom;)I
    .locals 18
    .param p0, "latitude"    # I
    .param p1, "zoom"    # Landroid_maps_conflict_avoidance/com/google/map/Zoom;

    .prologue
    .line 288
    invoke-static/range {p0 .. p0}, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->getMercatorIndex(I)I

    move-result v6

    .line 290
    .local v6, "indexFloor":I
    invoke-static/range {p0 .. p0}, Ljava/lang/Math;->abs(I)I

    move-result v11

    const v12, 0xf4240

    rem-int v7, v11, v12

    .line 292
    .local v7, "latDiff":I
    invoke-static {}, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->getMercatorValues()[I

    move-result-object v10

    .line 294
    .local v10, "values":[I
    add-int/lit8 v11, v6, -0x1

    aget v11, v10, v11

    mul-int/lit8 v11, v11, -0x1

    aget v12, v10, v6

    mul-int/lit8 v12, v12, 0x3

    add-int/2addr v11, v12

    add-int/lit8 v12, v6, 0x1

    aget v12, v10, v12

    mul-int/lit8 v12, v12, 0x3

    sub-int/2addr v11, v12

    add-int/lit8 v12, v6, 0x2

    aget v12, v10, v12

    add-int/2addr v11, v12

    div-int/lit8 v2, v11, 0x6

    .line 299
    .local v2, "cubicA":I
    add-int/lit8 v11, v6, -0x1

    aget v11, v10, v11

    mul-int/lit8 v11, v11, 0x3

    aget v12, v10, v6

    mul-int/lit8 v12, v12, 0x6

    sub-int/2addr v11, v12

    add-int/lit8 v12, v6, 0x1

    aget v12, v10, v12

    mul-int/lit8 v12, v12, 0x3

    add-int/2addr v11, v12

    div-int/lit8 v3, v11, 0x6

    .line 303
    .local v3, "cubicB":I
    add-int/lit8 v11, v6, -0x1

    aget v11, v10, v11

    mul-int/lit8 v11, v11, -0x2

    aget v12, v10, v6

    mul-int/lit8 v12, v12, 0x3

    sub-int/2addr v11, v12

    add-int/lit8 v12, v6, 0x1

    aget v12, v10, v12

    mul-int/lit8 v12, v12, 0x6

    add-int/2addr v11, v12

    add-int/lit8 v12, v6, 0x2

    aget v12, v10, v12

    sub-int/2addr v11, v12

    div-int/lit8 v4, v11, 0x6

    .line 308
    .local v4, "cubicC":I
    aget v5, v10, v6

    .line 310
    .local v5, "cubicD":I
    int-to-long v12, v2

    int-to-long v14, v7

    mul-long/2addr v12, v14

    int-to-long v14, v7

    mul-long/2addr v12, v14

    const-wide/32 v14, 0xf4240

    div-long/2addr v12, v14

    int-to-long v14, v7

    mul-long/2addr v12, v14

    const-wide/32 v14, 0xf4240

    div-long/2addr v12, v14

    const-wide/32 v14, 0xf4240

    div-long/2addr v12, v14

    int-to-long v14, v3

    int-to-long v0, v7

    move-wide/from16 v16, v0

    mul-long v14, v14, v16

    int-to-long v0, v7

    move-wide/from16 v16, v0

    mul-long v14, v14, v16

    const-wide/32 v16, 0xf4240

    div-long v14, v14, v16

    const-wide/32 v16, 0xf4240

    div-long v14, v14, v16

    add-long/2addr v12, v14

    int-to-long v14, v4

    int-to-long v0, v7

    move-wide/from16 v16, v0

    mul-long v14, v14, v16

    const-wide/32 v16, 0xf4240

    div-long v14, v14, v16

    add-long/2addr v12, v14

    int-to-long v14, v5

    add-long/2addr v12, v14

    long-to-int v8, v12

    .line 317
    .local v8, "mercatorY":I
    if-gez p0, :cond_0

    .line 318
    neg-int v8, v8

    .line 327
    :cond_0
    invoke-virtual/range {p1 .. p1}, Landroid_maps_conflict_avoidance/com/google/map/Zoom;->getEquatorPixels()I

    move-result v11

    int-to-long v12, v11

    const-wide/16 v14, 0x2

    div-long/2addr v12, v14

    move-object/from16 v0, p1

    invoke-static {v8, v0}, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->mercatorToPixelsTimesTen(ILandroid_maps_conflict_avoidance/com/google/map/Zoom;)I

    move-result v11

    div-int/lit8 v11, v11, 0xa

    int-to-long v14, v11

    sub-long/2addr v12, v14

    long-to-int v9, v12

    .line 330
    .local v9, "pixelY":I
    return v9
.end method

.method public static latLonToString(II)Ljava/lang/String;
    .locals 2
    .param p0, "latitude"    # I
    .param p1, "longitude"    # I

    .prologue
    .line 504
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p0}, Landroid_maps_conflict_avoidance/com/google/common/util/text/TextUtil;->e6ToString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x2c

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Landroid_maps_conflict_avoidance/com/google/common/util/text/TextUtil;->e6ToString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static mercatorToPixelsTimesTen(ILandroid_maps_conflict_avoidance/com/google/map/Zoom;)I
    .locals 4
    .param p0, "mercatorValue"    # I
    .param p1, "zoom"    # Landroid_maps_conflict_avoidance/com/google/map/Zoom;

    .prologue
    .line 633
    int-to-long v0, p0

    invoke-virtual {p1}, Landroid_maps_conflict_avoidance/com/google/map/Zoom;->getEquatorPixels()I

    move-result v2

    int-to-long v2, v2

    mul-long/2addr v0, v2

    const-wide/16 v2, 0xa

    mul-long/2addr v0, v2

    const-wide/32 v2, 0x15752a00

    div-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method private static pixelsToMercator(ILandroid_maps_conflict_avoidance/com/google/map/Zoom;)I
    .locals 4
    .param p0, "pixelValue"    # I
    .param p1, "zoom"    # Landroid_maps_conflict_avoidance/com/google/map/Zoom;

    .prologue
    .line 646
    int-to-long v0, p0

    const-wide/32 v2, 0x3b9aca00

    mul-long/2addr v0, v2

    invoke-virtual {p1}, Landroid_maps_conflict_avoidance/com/google/map/Zoom;->getEquatorPixels()I

    move-result v2

    int-to-long v2, v2

    div-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method private static readMercatorValues(Ljava/io/DataInput;[I)V
    .locals 4
    .param p0, "dis"    # Ljava/io/DataInput;
    .param p1, "values"    # [I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 459
    const/4 v1, 0x0

    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v2

    aput v2, p1, v1

    .line 463
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_0

    .line 464
    add-int/lit8 v1, v0, -0x1

    aget v1, p1, v1

    invoke-interface {p0}, Ljava/io/DataInput;->readUnsignedByte()I

    move-result v2

    shl-int/lit8 v2, v2, 0x10

    invoke-interface {p0}, Ljava/io/DataInput;->readUnsignedByte()I

    move-result v3

    shl-int/lit8 v3, v3, 0x8

    or-int/2addr v2, v3

    invoke-interface {p0}, Ljava/io/DataInput;->readUnsignedByte()I

    move-result v3

    or-int/2addr v2, v3

    add-int/2addr v1, v2

    aput v1, p1, v0

    .line 463
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 469
    :cond_0
    return-void
.end method

.method public static readPoint(Ljava/io/DataInput;)Landroid_maps_conflict_avoidance/com/google/map/MapPoint;
    .locals 3
    .param p0, "is"    # Ljava/io/DataInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 802
    new-instance v0, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;

    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v1

    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;-><init>(II)V

    return-object v0
.end method

.method public static writePoint(Landroid_maps_conflict_avoidance/com/google/map/MapPoint;Ljava/io/DataOutput;)V
    .locals 1
    .param p0, "point"    # Landroid_maps_conflict_avoidance/com/google/map/MapPoint;
    .param p1, "out"    # Ljava/io/DataOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 771
    if-eqz p0, :cond_0

    .line 772
    invoke-direct {p0, p1}, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->writePoint(Ljava/io/DataOutput;)V

    .line 777
    :goto_0
    return-void

    .line 774
    :cond_0
    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeInt(I)V

    .line 775
    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeInt(I)V

    goto :goto_0
.end method

.method private writePoint(Ljava/io/DataOutput;)V
    .locals 1
    .param p1, "out"    # Ljava/io/DataOutput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 820
    iget v0, p0, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->latitudeE6:I

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeInt(I)V

    .line 821
    iget v0, p0, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->longitudeE6:I

    invoke-interface {p1, v0}, Ljava/io/DataOutput;->writeInt(I)V

    .line 822
    return-void
.end method


# virtual methods
.method public distanceSquared(Landroid_maps_conflict_avoidance/com/google/map/MapPoint;)J
    .locals 8
    .param p1, "point"    # Landroid_maps_conflict_avoidance/com/google/map/MapPoint;

    .prologue
    const-wide/16 v6, 0x64

    .line 666
    iget v0, p0, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->latitudeE6:I

    iget v1, p1, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->latitudeE6:I

    sub-int/2addr v0, v1

    int-to-long v0, v0

    iget v2, p0, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->latitudeE6:I

    iget v3, p1, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->latitudeE6:I

    sub-int/2addr v2, v3

    int-to-long v2, v2

    mul-long/2addr v0, v2

    div-long/2addr v0, v6

    iget v2, p0, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->longitudeE6:I

    iget v3, p1, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->longitudeE6:I

    sub-int/2addr v2, v3

    int-to-long v2, v2

    iget v4, p0, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->longitudeE6:I

    iget v5, p1, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->longitudeE6:I

    sub-int/2addr v4, v5

    int-to-long v4, v4

    mul-long/2addr v2, v4

    div-long/2addr v2, v6

    add-long/2addr v0, v2

    return-wide v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 825
    if-ne p0, p1, :cond_1

    move v1, v2

    .line 835
    :cond_0
    :goto_0
    return v1

    .line 828
    :cond_1
    instance-of v3, p1, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;

    if-eqz v3, :cond_0

    move-object v0, p1

    .line 832
    check-cast v0, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;

    .line 835
    .local v0, "mapPoint":Landroid_maps_conflict_avoidance/com/google/map/MapPoint;
    iget v3, p0, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->latitudeE6:I

    iget v4, v0, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->latitudeE6:I

    if-ne v3, v4, :cond_2

    iget v3, p0, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->longitudeE6:I

    iget v4, v0, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->longitudeE6:I

    if-eq v3, v4, :cond_3

    :cond_2
    iget v3, p0, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->pixelCoordX:I

    iget v4, v0, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->pixelCoordX:I

    if-ne v3, v4, :cond_0

    iget v3, p0, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->pixelCoordY:I

    iget v4, v0, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->pixelCoordY:I

    if-ne v3, v4, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public getDefiningPoint()Landroid_maps_conflict_avoidance/com/google/map/MapPoint;
    .locals 0

    .prologue
    .line 791
    return-object p0
.end method

.method public getLatitude()I
    .locals 1

    .prologue
    .line 403
    iget v0, p0, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->latitudeE6:I

    return v0
.end method

.method public getLongitude()I
    .locals 1

    .prologue
    .line 473
    iget v0, p0, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->longitudeE6:I

    return v0
.end method

.method public getXPixel(Landroid_maps_conflict_avoidance/com/google/map/Zoom;)I
    .locals 3
    .param p1, "zoom"    # Landroid_maps_conflict_avoidance/com/google/map/Zoom;

    .prologue
    .line 200
    sget-object v0, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->PIXEL_COORDS_ZOOM:Landroid_maps_conflict_avoidance/com/google/map/Zoom;

    iget v1, p0, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->pixelCoordX:I

    invoke-virtual {p1}, Landroid_maps_conflict_avoidance/com/google/map/Zoom;->getZoomLevel()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid_maps_conflict_avoidance/com/google/map/Zoom;->changePixelsToTargetZoomlevel(II)I

    move-result v0

    return v0
.end method

.method public getYPixel(Landroid_maps_conflict_avoidance/com/google/map/Zoom;)I
    .locals 3
    .param p1, "zoom"    # Landroid_maps_conflict_avoidance/com/google/map/Zoom;

    .prologue
    .line 340
    sget-object v0, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->PIXEL_COORDS_ZOOM:Landroid_maps_conflict_avoidance/com/google/map/Zoom;

    iget v1, p0, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->pixelCoordY:I

    invoke-virtual {p1}, Landroid_maps_conflict_avoidance/com/google/map/Zoom;->getZoomLevel()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid_maps_conflict_avoidance/com/google/map/Zoom;->changePixelsToTargetZoomlevel(II)I

    move-result v0

    return v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 842
    iget v0, p0, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->latitudeE6:I

    .line 843
    .local v0, "result":I
    mul-int/lit8 v1, v0, 0x1d

    iget v2, p0, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->longitudeE6:I

    add-int v0, v1, v2

    .line 844
    return v0
.end method

.method public pixelDistanceSquared(Landroid_maps_conflict_avoidance/com/google/map/MapPoint;Landroid_maps_conflict_avoidance/com/google/map/Zoom;)J
    .locals 8
    .param p1, "point"    # Landroid_maps_conflict_avoidance/com/google/map/MapPoint;
    .param p2, "zoom"    # Landroid_maps_conflict_avoidance/com/google/map/Zoom;

    .prologue
    .line 758
    invoke-virtual {p1, p2}, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->getXPixel(Landroid_maps_conflict_avoidance/com/google/map/Zoom;)I

    move-result v2

    invoke-virtual {p0, p2}, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->getXPixel(Landroid_maps_conflict_avoidance/com/google/map/Zoom;)I

    move-result v3

    sub-int v0, v2, v3

    .line 759
    .local v0, "xDiff":I
    invoke-virtual {p1, p2}, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->getYPixel(Landroid_maps_conflict_avoidance/com/google/map/Zoom;)I

    move-result v2

    invoke-virtual {p0, p2}, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->getYPixel(Landroid_maps_conflict_avoidance/com/google/map/Zoom;)I

    move-result v3

    sub-int v1, v2, v3

    .line 760
    .local v1, "yDiff":I
    int-to-long v2, v0

    int-to-long v4, v0

    mul-long/2addr v2, v4

    int-to-long v4, v1

    int-to-long v6, v1

    mul-long/2addr v4, v6

    add-long/2addr v2, v4

    return-wide v2
.end method

.method public pixelOffset(IILandroid_maps_conflict_avoidance/com/google/map/Zoom;)Landroid_maps_conflict_avoidance/com/google/map/MapPoint;
    .locals 10
    .param p1, "xPixel"    # I
    .param p2, "yPixel"    # I
    .param p3, "zoom"    # Landroid_maps_conflict_avoidance/com/google/map/Zoom;

    .prologue
    const/16 v3, 0x16

    .line 593
    invoke-virtual {p3, p1, v3}, Landroid_maps_conflict_avoidance/com/google/map/Zoom;->changePixelsToTargetZoomlevel(II)I

    move-result v0

    iget v6, p0, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->pixelCoordX:I

    add-int v1, v0, v6

    .line 595
    .local v1, "newX":I
    invoke-virtual {p3, p2, v3}, Landroid_maps_conflict_avoidance/com/google/map/Zoom;->changePixelsToTargetZoomlevel(II)I

    move-result v0

    iget v6, p0, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->pixelCoordY:I

    add-int v2, v0, v6

    .line 599
    .local v2, "newY":I
    iget v4, p0, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->latitudeE6:I

    .line 600
    .local v4, "newLatitude":I
    if-eqz p2, :cond_0

    .line 601
    sget-object v0, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->PIXEL_COORDS_ZOOM:Landroid_maps_conflict_avoidance/com/google/map/Zoom;

    invoke-static {v2, v0}, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->getLatitudeFromY(ILandroid_maps_conflict_avoidance/com/google/map/Zoom;)I

    move-result v4

    .line 603
    :cond_0
    iget v5, p0, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->longitudeE6:I

    .line 604
    .local v5, "newLongitude":I
    if-eqz p1, :cond_1

    .line 605
    int-to-long v6, p1

    const-wide/32 v8, 0x15752a00

    mul-long/2addr v6, v8

    invoke-virtual {p3}, Landroid_maps_conflict_avoidance/com/google/map/Zoom;->getEquatorPixels()I

    move-result v0

    int-to-long v8, v0

    div-long/2addr v6, v8

    long-to-int v0, v6

    add-int/2addr v5, v0

    .line 608
    :cond_1
    new-instance v0, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;

    invoke-direct/range {v0 .. v5}, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;-><init>(IIIII)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 491
    iget v0, p0, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->latitudeE6:I

    iget v1, p0, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->longitudeE6:I

    invoke-static {v0, v1}, Landroid_maps_conflict_avoidance/com/google/map/MapPoint;->latLonToString(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
