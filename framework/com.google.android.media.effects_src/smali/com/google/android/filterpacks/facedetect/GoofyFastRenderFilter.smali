.class public Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;
.super Landroid/filterfw/core/Filter;
.source "GoofyFastRenderFilter.java"


# static fields
.field private static final BIG_EYES:I = 0x1

.field private static final BIG_MOUTH:I = 0x2

.field private static final BIG_NOSE:I = 0x4

.field private static final NUM_EFFECTS:I = 0x6

.field private static final SMALL_EYES:I = 0x5

.field private static final SMALL_MOUTH:I = 0x3

.field private static final SQUEEZE:I = 0x0

.field private static final TAG:Ljava/lang/String; = "GoofyFastRenderFilter"


# instance fields
.field private mAnimateCurrent:F

.field private mAnimationStartTimeStamp:J

.field private mAspect:[F

.field private mBigEyesProgram:Landroid/filterfw/core/ShaderProgram;

.field private mCurrentEffect:I
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "currentEffect"
    .end annotation
.end field

.field private mCurrentTimeStamp:J

.field private mDistortionAmount:F
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "distortionAmount"
    .end annotation
.end field

.field private final mDistortionVertexShader:Ljava/lang/String;

.field private final mDistortionVertexShader2:Ljava/lang/String;

.field private mEnableAnimation:Z
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "enableAnimation"
    .end annotation
.end field

.field private mIdentityProgram:Landroid/filterfw/core/ShaderProgram;

.field private final mIdentityShader:Ljava/lang/String;

.field private mMeshDistortionFrame:Landroid/filterfw/core/VertexFrame;

.field private mPureIdentityProgram:Landroid/filterfw/core/ShaderProgram;

.field private mSmoothness:F
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "smoothness"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 150
    invoke-direct {p0, p1}, Landroid/filterfw/core/Filter;-><init>(Ljava/lang/String;)V

    .line 57
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mCurrentEffect:I

    .line 63
    iput v1, p0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mDistortionAmount:F

    .line 70
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mEnableAnimation:Z

    .line 78
    const v0, 0x3e99999a    # 0.3f

    iput v0, p0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mSmoothness:F

    .line 92
    iput v1, p0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mAnimateCurrent:F

    .line 95
    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mAspect:[F

    .line 97
    const-string v0, "precision mediump float;\nuniform sampler2D tex_sampler_0;\nvarying vec2 v_texcoord;\nvoid main() {\n  gl_FragColor = texture2D(tex_sampler_0, v_texcoord);\n}\n"

    iput-object v0, p0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mIdentityShader:Ljava/lang/String;

    .line 105
    const-string v0, "uniform vec2 center;\nuniform vec2 weight;\nuniform mat2 rotate;\nuniform float amount;\nattribute vec4 positions;\nattribute vec2 texcoords;\nvarying vec2 v_texcoord;\nvoid main() {\n  vec2 mesh_point = (rotate * positions.xy) * weight *2.0 +\n                    2.0 * (center - vec2(0.5, 0.5));\n  gl_Position = positions;\n  gl_Position.x = mesh_point.x;\n  gl_Position.y = mesh_point.y;\n  vec2 p = (1.0 + texcoords * amount) * positions.xy;\n  v_texcoord = (rotate * p) * weight  + center;\n}\n"

    iput-object v0, p0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mDistortionVertexShader:Ljava/lang/String;

    .line 123
    const-string v0, "uniform vec2 center;\nuniform mat2 rotate;\nuniform vec2 weight;\nuniform float amount;\nattribute vec4 positions;\nattribute vec2 texcoords;\nvarying vec2 v_texcoord;\nvoid main() {\n  vec2 mesh_point = (rotate * (positions.xy * vec2(3.0, 2.0))) * weight +\n                    2.0 * (center - vec2(0.5, 0.5));\n  gl_Position = positions;\n  gl_Position.x = mesh_point.x;\n  gl_Position.y = mesh_point.y;\n  float x = (1.0 + amount * texcoords.x) * positions.x + amount * texcoords.y;\n  float y = positions.y * (1.0 + texcoords.x * amount);\n  vec2 p = vec2(x,y);\n  v_texcoord = (rotate * (p * vec2(3.0,2.0))) * weight * 0.5 + center;\n}\n"

    iput-object v0, p0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mDistortionVertexShader2:Ljava/lang/String;

    .line 151
    return-void

    .line 95
    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private createMesh(Landroid/filterfw/core/FilterContext;)V
    .locals 32
    .param p1, "context"    # Landroid/filterfw/core/FilterContext;

    .prologue
    .line 282
    move-object/from16 v0, p0

    iget v12, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mDistortionAmount:F

    .line 283
    .local v12, "amount":F
    const/high16 v3, 0x42c80000    # 100.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mSmoothness:F

    mul-float/2addr v3, v4

    float-to-int v0, v3

    move/from16 v20, v0

    .line 284
    .local v20, "nrows":I
    const/high16 v3, 0x42c80000    # 100.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mSmoothness:F

    mul-float/2addr v3, v4

    float-to-int v0, v3

    move/from16 v19, v0

    .line 286
    .local v19, "ncols":I
    const/4 v11, 0x4

    .line 287
    .local v11, "FLOAT_SIZE":I
    const/16 v18, 0x6

    .line 288
    .local v18, "kNumVerticesPerGrid":I
    const/16 v17, 0x4

    .line 290
    .local v17, "kNumValuesPerVertex":I
    mul-int v3, v20, v19

    mul-int/lit8 v3, v3, 0x6

    mul-int/lit8 v21, v3, 0x4

    .line 291
    .local v21, "num_floats":I
    move/from16 v0, v21

    new-array v0, v0, [F

    move-object/from16 v23, v0

    .line 292
    .local v23, "positions":[F
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_0
    move/from16 v0, v20

    if-ge v14, v0, :cond_2

    .line 293
    const/4 v15, 0x0

    .local v15, "j":I
    :goto_1
    move/from16 v0, v19

    if-ge v15, v0, :cond_1

    .line 294
    mul-int v3, v14, v19

    add-int/2addr v3, v15

    mul-int/lit8 v3, v3, 0x6

    mul-int/lit8 v22, v3, 0x4

    .line 296
    .local v22, "p":I
    int-to-float v3, v15

    move/from16 v0, v19

    int-to-float v4, v0

    div-float v27, v3, v4

    .line 297
    .local v27, "x0":F
    int-to-float v3, v14

    move/from16 v0, v20

    int-to-float v4, v0

    div-float v30, v3, v4

    .line 298
    .local v30, "y0":F
    add-int/lit8 v3, v15, 0x1

    int-to-float v3, v3

    move/from16 v0, v19

    int-to-float v4, v0

    div-float v28, v3, v4

    .line 299
    .local v28, "x1":F
    add-int/lit8 v3, v14, 0x1

    int-to-float v3, v3

    move/from16 v0, v20

    int-to-float v4, v0

    div-float v31, v3, v4

    .line 300
    .local v31, "y1":F
    const/16 v16, 0x0

    .local v16, "k":I
    :goto_2
    const/4 v3, 0x6

    move/from16 v0, v16

    if-ge v0, v3, :cond_0

    .line 301
    const/16 v26, 0x0

    .line 302
    .local v26, "x":F
    const/16 v29, 0x0

    .line 303
    .local v29, "y":F
    packed-switch v16, :pswitch_data_0

    .line 323
    :goto_3
    const/high16 v3, 0x3f000000    # 0.5f

    sub-float v3, v26, v3

    const/high16 v4, 0x40000000    # 2.0f

    mul-float v26, v3, v4

    .line 324
    const/high16 v3, 0x3f000000    # 0.5f

    sub-float v3, v29, v3

    const/high16 v4, 0x40000000    # 2.0f

    mul-float v29, v3, v4

    .line 325
    mul-int/lit8 v3, v16, 0x4

    add-int v3, v3, v22

    aput v26, v23, v3

    .line 326
    mul-int/lit8 v3, v16, 0x4

    add-int v3, v3, v22

    add-int/lit8 v3, v3, 0x1

    aput v29, v23, v3

    .line 327
    move-object/from16 v0, p0

    move/from16 v1, v26

    move/from16 v2, v29

    invoke-direct {v0, v1, v2, v12}, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->getTexturePosition(FFF)[F

    move-result-object v24

    .line 328
    .local v24, "texture_pos":[F
    mul-int/lit8 v3, v16, 0x4

    add-int v3, v3, v22

    add-int/lit8 v3, v3, 0x2

    const/4 v4, 0x0

    aget v4, v24, v4

    aput v4, v23, v3

    .line 329
    mul-int/lit8 v3, v16, 0x4

    add-int v3, v3, v22

    add-int/lit8 v3, v3, 0x3

    const/4 v4, 0x1

    aget v4, v24, v4

    aput v4, v23, v3

    .line 300
    add-int/lit8 v16, v16, 0x1

    goto :goto_2

    .line 305
    .end local v24    # "texture_pos":[F
    :pswitch_0
    move/from16 v26, v27

    .line 306
    move/from16 v29, v30

    .line 307
    goto :goto_3

    .line 310
    :pswitch_1
    move/from16 v26, v27

    .line 311
    move/from16 v29, v31

    .line 312
    goto :goto_3

    .line 315
    :pswitch_2
    move/from16 v26, v28

    .line 316
    move/from16 v29, v30

    .line 317
    goto :goto_3

    .line 319
    :pswitch_3
    move/from16 v26, v28

    .line 320
    move/from16 v29, v31

    goto :goto_3

    .line 293
    .end local v26    # "x":F
    .end local v29    # "y":F
    :cond_0
    add-int/lit8 v15, v15, 0x1

    goto/16 :goto_1

    .line 292
    .end local v16    # "k":I
    .end local v22    # "p":I
    .end local v27    # "x0":F
    .end local v28    # "x1":F
    .end local v30    # "y0":F
    .end local v31    # "y1":F
    :cond_1
    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_0

    .line 334
    .end local v15    # "j":I
    :cond_2
    const/4 v3, 0x4

    move/from16 v0, v21

    invoke-static {v0, v3}, Landroid/filterfw/format/PrimitiveFormat;->createFloatFormat(II)Landroid/filterfw/core/MutableFrameFormat;

    move-result-object v25

    .line 337
    .local v25, "vertexFormat":Landroid/filterfw/core/MutableFrameFormat;
    invoke-virtual/range {p1 .. p1}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    move-result-object v13

    .line 338
    .local v13, "frameManager":Landroid/filterfw/core/FrameManager;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mMeshDistortionFrame:Landroid/filterfw/core/VertexFrame;

    if-eqz v3, :cond_3

    .line 339
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mMeshDistortionFrame:Landroid/filterfw/core/VertexFrame;

    invoke-virtual {v3}, Landroid/filterfw/core/VertexFrame;->release()Landroid/filterfw/core/Frame;

    .line 340
    :cond_3
    move-object/from16 v0, v25

    invoke-virtual {v13, v0}, Landroid/filterfw/core/FrameManager;->newFrame(Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/Frame;

    move-result-object v3

    check-cast v3, Landroid/filterfw/core/VertexFrame;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mMeshDistortionFrame:Landroid/filterfw/core/VertexFrame;

    .line 341
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mMeshDistortionFrame:Landroid/filterfw/core/VertexFrame;

    move-object/from16 v0, v23

    invoke-virtual {v3, v0}, Landroid/filterfw/core/VertexFrame;->setFloats([F)V

    .line 343
    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mCurrentEffect:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_4

    .line 344
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mBigEyesProgram:Landroid/filterfw/core/ShaderProgram;

    const-string v4, "positions"

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mMeshDistortionFrame:Landroid/filterfw/core/VertexFrame;

    const/16 v6, 0x1406

    const/4 v7, 0x2

    const/16 v8, 0x10

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual/range {v3 .. v10}, Landroid/filterfw/core/ShaderProgram;->setAttributeValues(Ljava/lang/String;Landroid/filterfw/core/VertexFrame;IIIIZ)V

    .line 351
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mBigEyesProgram:Landroid/filterfw/core/ShaderProgram;

    const-string v4, "texcoords"

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mMeshDistortionFrame:Landroid/filterfw/core/VertexFrame;

    const/16 v6, 0x1406

    const/4 v7, 0x2

    const/16 v8, 0x10

    const/16 v9, 0x8

    const/4 v10, 0x0

    invoke-virtual/range {v3 .. v10}, Landroid/filterfw/core/ShaderProgram;->setAttributeValues(Ljava/lang/String;Landroid/filterfw/core/VertexFrame;IIIIZ)V

    .line 358
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mBigEyesProgram:Landroid/filterfw/core/ShaderProgram;

    mul-int v4, v20, v19

    mul-int/lit8 v4, v4, 0x6

    invoke-virtual {v3, v4}, Landroid/filterfw/core/ShaderProgram;->setVertexCount(I)V

    .line 359
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mBigEyesProgram:Landroid/filterfw/core/ShaderProgram;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/filterfw/core/ShaderProgram;->setDrawMode(I)V

    .line 378
    :goto_4
    return-void

    .line 361
    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mIdentityProgram:Landroid/filterfw/core/ShaderProgram;

    const-string v4, "positions"

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mMeshDistortionFrame:Landroid/filterfw/core/VertexFrame;

    const/16 v6, 0x1406

    const/4 v7, 0x2

    const/16 v8, 0x10

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual/range {v3 .. v10}, Landroid/filterfw/core/ShaderProgram;->setAttributeValues(Ljava/lang/String;Landroid/filterfw/core/VertexFrame;IIIIZ)V

    .line 368
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mIdentityProgram:Landroid/filterfw/core/ShaderProgram;

    const-string v4, "texcoords"

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mMeshDistortionFrame:Landroid/filterfw/core/VertexFrame;

    const/16 v6, 0x1406

    const/4 v7, 0x2

    const/16 v8, 0x10

    const/16 v9, 0x8

    const/4 v10, 0x0

    invoke-virtual/range {v3 .. v10}, Landroid/filterfw/core/ShaderProgram;->setAttributeValues(Ljava/lang/String;Landroid/filterfw/core/VertexFrame;IIIIZ)V

    .line 375
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mIdentityProgram:Landroid/filterfw/core/ShaderProgram;

    mul-int v4, v20, v19

    mul-int/lit8 v4, v4, 0x6

    invoke-virtual {v3, v4}, Landroid/filterfw/core/ShaderProgram;->setVertexCount(I)V

    .line 376
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mIdentityProgram:Landroid/filterfw/core/ShaderProgram;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/filterfw/core/ShaderProgram;->setDrawMode(I)V

    goto :goto_4

    .line 303
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method private createProgram(Landroid/filterfw/core/FilterContext;)V
    .locals 3
    .param p1, "context"    # Landroid/filterfw/core/FilterContext;

    .prologue
    .line 174
    new-instance v0, Landroid/filterfw/core/ShaderProgram;

    const-string v1, "uniform vec2 center;\nuniform vec2 weight;\nuniform mat2 rotate;\nuniform float amount;\nattribute vec4 positions;\nattribute vec2 texcoords;\nvarying vec2 v_texcoord;\nvoid main() {\n  vec2 mesh_point = (rotate * positions.xy) * weight *2.0 +\n                    2.0 * (center - vec2(0.5, 0.5));\n  gl_Position = positions;\n  gl_Position.x = mesh_point.x;\n  gl_Position.y = mesh_point.y;\n  vec2 p = (1.0 + texcoords * amount) * positions.xy;\n  v_texcoord = (rotate * p) * weight  + center;\n}\n"

    const-string v2, "precision mediump float;\nuniform sampler2D tex_sampler_0;\nvarying vec2 v_texcoord;\nvoid main() {\n  gl_FragColor = texture2D(tex_sampler_0, v_texcoord);\n}\n"

    invoke-direct {v0, p1, v1, v2}, Landroid/filterfw/core/ShaderProgram;-><init>(Landroid/filterfw/core/FilterContext;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mIdentityProgram:Landroid/filterfw/core/ShaderProgram;

    .line 176
    new-instance v0, Landroid/filterfw/core/ShaderProgram;

    const-string v1, "uniform vec2 center;\nuniform mat2 rotate;\nuniform vec2 weight;\nuniform float amount;\nattribute vec4 positions;\nattribute vec2 texcoords;\nvarying vec2 v_texcoord;\nvoid main() {\n  vec2 mesh_point = (rotate * (positions.xy * vec2(3.0, 2.0))) * weight +\n                    2.0 * (center - vec2(0.5, 0.5));\n  gl_Position = positions;\n  gl_Position.x = mesh_point.x;\n  gl_Position.y = mesh_point.y;\n  float x = (1.0 + amount * texcoords.x) * positions.x + amount * texcoords.y;\n  float y = positions.y * (1.0 + texcoords.x * amount);\n  vec2 p = vec2(x,y);\n  v_texcoord = (rotate * (p * vec2(3.0,2.0))) * weight * 0.5 + center;\n}\n"

    const-string v2, "precision mediump float;\nuniform sampler2D tex_sampler_0;\nvarying vec2 v_texcoord;\nvoid main() {\n  gl_FragColor = texture2D(tex_sampler_0, v_texcoord);\n}\n"

    invoke-direct {v0, p1, v1, v2}, Landroid/filterfw/core/ShaderProgram;-><init>(Landroid/filterfw/core/FilterContext;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mBigEyesProgram:Landroid/filterfw/core/ShaderProgram;

    .line 177
    return-void
.end method

.method private getDistortionScale(FF)F
    .locals 12
    .param p1, "x"    # F
    .param p2, "amount"    # F

    .prologue
    const v5, 0x3e4ccccd    # 0.2f

    const/high16 v11, 0x3f800000    # 1.0f

    const/high16 v10, 0x40000000    # 2.0f

    .line 188
    move v3, p1

    .line 189
    .local v3, "value":F
    move v0, p1

    .line 190
    .local v0, "dist":F
    iget v4, p0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mCurrentEffect:I

    packed-switch v4, :pswitch_data_0

    .line 220
    :goto_0
    return v3

    .line 192
    :pswitch_0
    const v2, 0x3e99999a    # 0.3f

    .line 194
    .local v2, "sigma":F
    add-float v4, v0, v5

    neg-float v4, v4

    add-float/2addr v5, v0

    mul-float/2addr v4, v5

    const/high16 v5, 0x40400000    # 3.0f

    mul-float/2addr v4, v5

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->exp(D)D

    move-result-wide v4

    double-to-float v4, v4

    const v5, 0x3f63126f    # 0.887f

    div-float/2addr v4, v5

    add-float/2addr v4, v11

    div-float v4, v10, v4

    sub-float v4, v10, v4

    mul-float v3, p2, v4

    .line 195
    goto :goto_0

    .line 199
    .end local v2    # "sigma":F
    :pswitch_1
    const v2, 0x3e99999a    # 0.3f

    .line 202
    .restart local v2    # "sigma":F
    const v4, 0x3f4ccccd    # 0.8f

    mul-float/2addr v4, p2

    neg-float v5, v0

    mul-float/2addr v5, v0

    mul-float v6, v2, v2

    div-float/2addr v5, v6

    float-to-double v6, v5

    invoke-static {v6, v7}, Ljava/lang/Math;->exp(D)D

    move-result-wide v6

    double-to-float v5, v6

    sub-float v5, v10, v5

    const/high16 v6, 0x3e800000    # 0.25f

    add-float/2addr v6, v0

    float-to-double v6, v6

    const-wide/high16 v8, 0x4010000000000000L    # 4.0

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v6

    neg-double v6, v6

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    mul-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->exp(D)D

    move-result-wide v6

    double-to-float v6, v6

    add-float/2addr v6, v11

    div-float v6, v10, v6

    sub-float/2addr v5, v6

    mul-float v3, v4, v5

    .line 204
    goto :goto_0

    .line 209
    .end local v2    # "sigma":F
    :pswitch_2
    const v2, 0x3f333333    # 0.7f

    .line 214
    .restart local v2    # "sigma":F
    const-wide/high16 v4, 0x4090000000000000L    # 1024.0

    invoke-static {v4, v5}, Ljava/lang/Math;->log(D)D

    move-result-wide v4

    float-to-double v6, v2

    mul-double/2addr v4, v6

    float-to-double v6, v2

    mul-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    double-to-float v1, v4

    .line 215
    .local v1, "scale":F
    cmpl-float v4, v1, v11

    if-lez v4, :cond_0

    mul-float/2addr v0, v1

    .line 216
    :cond_0
    const v4, -0x40d9999a    # -0.65f

    mul-float/2addr v4, p2

    neg-float v5, v0

    mul-float/2addr v5, v0

    mul-float v6, v2, v2

    div-float/2addr v5, v6

    float-to-double v6, v5

    invoke-static {v6, v7}, Ljava/lang/Math;->exp(D)D

    move-result-wide v6

    double-to-float v5, v6

    mul-float v3, v4, v5

    .line 217
    goto :goto_0

    .line 190
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private getTexturePosition(FFF)[F
    .locals 20
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "amount"    # F

    .prologue
    .line 241
    const/16 v16, 0x2

    move/from16 v0, v16

    new-array v15, v0, [F

    fill-array-data v15, :array_0

    .line 242
    .local v15, "texture_pos":[F
    const/4 v6, 0x0

    .line 243
    .local v6, "dist2":F
    const/high16 v11, 0x3f800000    # 1.0f

    .line 244
    .local v11, "scale":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mCurrentEffect:I

    move/from16 v16, v0

    const/16 v17, 0x1

    move/from16 v0, v16

    move/from16 v1, v17

    if-eq v0, v1, :cond_2

    .line 245
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->getEffectAspectRatio()[F

    move-result-object v7

    .line 246
    .local v7, "e_aspect":[F
    const/16 v16, 0x0

    aget v16, v7, v16

    const/16 v17, 0x1

    aget v17, v7, v17

    cmpg-float v16, v16, v17

    if-gez v16, :cond_1

    const/16 v16, 0x1

    aget v2, v7, v16

    .line 248
    .local v2, "as_ratio":F
    :goto_0
    mul-float v16, p1, p1

    const/16 v17, 0x0

    aget v17, v7, v17

    div-float v16, v16, v17

    const/16 v17, 0x0

    aget v17, v7, v17

    div-float v16, v16, v17

    mul-float v17, p2, p2

    const/16 v18, 0x1

    aget v18, v7, v18

    div-float v17, v17, v18

    const/16 v18, 0x1

    aget v18, v7, v18

    div-float v17, v17, v18

    add-float v16, v16, v17

    move/from16 v0, v16

    float-to-double v0, v0

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v16

    move-wide/from16 v0, v16

    double-to-float v5, v0

    .line 250
    .local v5, "dist":F
    const/high16 v16, 0x3f800000    # 1.0f

    cmpg-float v16, v5, v16

    if-gtz v16, :cond_0

    .line 251
    const/high16 v16, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v5, v1}, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->getDistortionScale(FF)F

    move-result v11

    .line 252
    const/16 v16, 0x0

    aput v11, v15, v16

    .line 253
    const/16 v16, 0x1

    aput v11, v15, v16

    .line 278
    .end local v2    # "as_ratio":F
    .end local v7    # "e_aspect":[F
    :cond_0
    :goto_1
    return-object v15

    .line 246
    .end local v5    # "dist":F
    .restart local v7    # "e_aspect":[F
    :cond_1
    const/16 v16, 0x0

    aget v2, v7, v16

    goto :goto_0

    .line 260
    .end local v7    # "e_aspect":[F
    :cond_2
    const/high16 v8, 0x3f800000    # 1.0f

    .line 261
    .local v8, "kBigEyeScale":F
    const/high16 v16, 0x40000000    # 2.0f

    mul-float v16, v16, v8

    const/high16 v17, 0x3f800000    # 1.0f

    add-float v14, v16, v17

    .line 262
    .local v14, "size":F
    div-float v9, v8, v14

    .line 263
    .local v9, "left":F
    const/high16 v16, 0x3f800000    # 1.0f

    sub-float v10, v16, v9

    .line 264
    .local v10, "right":F
    const/high16 v16, 0x3f000000    # 0.5f

    sub-float v16, v9, v16

    const/high16 v17, 0x40000000    # 2.0f

    mul-float v9, v16, v17

    .line 265
    const/high16 v16, 0x3f000000    # 0.5f

    sub-float v16, v10, v16

    const/high16 v17, 0x40000000    # 2.0f

    mul-float v10, v16, v17

    .line 266
    sub-float v3, p1, v9

    .line 267
    .local v3, "dL":F
    const/high16 v16, 0x40000000    # 2.0f

    div-float v16, v14, v16

    mul-float v17, v3, v3

    mul-float v18, p2, p2

    add-float v17, v17, v18

    move/from16 v0, v17

    float-to-double v0, v0

    move-wide/from16 v18, v0

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v18

    move-wide/from16 v0, v18

    double-to-float v0, v0

    move/from16 v17, v0

    mul-float v5, v16, v17

    .line 268
    .restart local v5    # "dist":F
    sub-float v4, p1, v10

    .line 269
    .local v4, "dR":F
    const/high16 v16, 0x40000000    # 2.0f

    div-float v16, v14, v16

    mul-float v17, v4, v4

    mul-float v18, p2, p2

    add-float v17, v17, v18

    move/from16 v0, v17

    float-to-double v0, v0

    move-wide/from16 v18, v0

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v18

    move-wide/from16 v0, v18

    double-to-float v0, v0

    move/from16 v17, v0

    mul-float v6, v16, v17

    .line 270
    const/high16 v16, 0x3f800000    # 1.0f

    cmpg-float v16, v5, v16

    if-ltz v16, :cond_3

    const/high16 v16, 0x3f800000    # 1.0f

    cmpg-float v16, v6, v16

    if-gez v16, :cond_0

    .line 271
    :cond_3
    const/high16 v16, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v5, v1}, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->getDistortionScale(FF)F

    move-result v12

    .line 272
    .local v12, "scale1":F
    const/high16 v16, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v0, v6, v1}, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->getDistortionScale(FF)F

    move-result v13

    .line 273
    .local v13, "scale2":F
    const/16 v16, 0x0

    mul-float v17, v12, v12

    mul-float v18, v13, v13

    add-float v17, v17, v18

    add-float v18, v12, v13

    div-float v17, v17, v18

    aput v17, v15, v16

    .line 275
    const/16 v16, 0x1

    sub-float v17, v12, v13

    move/from16 v0, v17

    neg-float v0, v0

    move/from16 v17, v0

    mul-float v17, v17, v9

    aput v17, v15, v16

    goto/16 :goto_1

    .line 241
    nop

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method


# virtual methods
.method public fieldPortValueUpdated(Ljava/lang/String;Landroid/filterfw/core/FilterContext;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/filterfw/core/FilterContext;

    .prologue
    .line 181
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mAnimateCurrent:F

    .line 182
    iget-object v0, p0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mIdentityProgram:Landroid/filterfw/core/ShaderProgram;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mBigEyesProgram:Landroid/filterfw/core/ShaderProgram;

    if-eqz v0, :cond_0

    .line 183
    invoke-direct {p0, p2}, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->createMesh(Landroid/filterfw/core/FilterContext;)V

    .line 185
    :cond_0
    return-void
.end method

.method getEffectAspectRatio()[F
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 223
    iget v0, p0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mCurrentEffect:I

    packed-switch v0, :pswitch_data_0

    .line 237
    new-array v0, v1, [F

    fill-array-data v0, :array_0

    :goto_0
    return-object v0

    .line 225
    :pswitch_0
    new-array v0, v1, [F

    fill-array-data v0, :array_1

    goto :goto_0

    .line 227
    :pswitch_1
    new-array v0, v1, [F

    fill-array-data v0, :array_2

    goto :goto_0

    .line 229
    :pswitch_2
    new-array v0, v1, [F

    fill-array-data v0, :array_3

    goto :goto_0

    .line 231
    :pswitch_3
    new-array v0, v1, [F

    fill-array-data v0, :array_4

    goto :goto_0

    .line 233
    :pswitch_4
    new-array v0, v1, [F

    fill-array-data v0, :array_5

    goto :goto_0

    .line 235
    :pswitch_5
    new-array v0, v1, [F

    fill-array-data v0, :array_6

    goto :goto_0

    .line 223
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_5
        :pswitch_2
        :pswitch_4
    .end packed-switch

    .line 237
    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data

    .line 225
    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data

    .line 227
    :array_2
    .array-data 4
        0x3f800000    # 1.0f
        0x3f19999a    # 0.6f
    .end array-data

    .line 229
    :array_3
    .array-data 4
        0x3f800000    # 1.0f
        0x3f4ccccd    # 0.8f
    .end array-data

    .line 231
    :array_4
    .array-data 4
        0x3f800000    # 1.0f
        0x3f333333    # 0.7f
    .end array-data

    .line 233
    :array_5
    .array-data 4
        0x3f800000    # 1.0f
        0x3ecccccd    # 0.4f
    .end array-data

    .line 235
    :array_6
    .array-data 4
        0x3f800000    # 1.0f
        0x3f19999a    # 0.6f
    .end array-data
.end method

.method public getOutputFormat(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/FrameFormat;
    .locals 0
    .param p1, "portName"    # Ljava/lang/String;
    .param p2, "inputFormat"    # Landroid/filterfw/core/FrameFormat;

    .prologue
    .line 170
    return-object p2
.end method

.method public process(Landroid/filterfw/core/FilterContext;)V
    .locals 50
    .param p1, "context"    # Landroid/filterfw/core/FilterContext;

    .prologue
    .line 383
    const-string v45, "image"

    move-object/from16 v0, p0

    move-object/from16 v1, v45

    invoke-virtual {v0, v1}, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->pullInput(Ljava/lang/String;)Landroid/filterfw/core/Frame;

    move-result-object v23

    .line 384
    .local v23, "input":Landroid/filterfw/core/Frame;
    invoke-virtual/range {v23 .. v23}, Landroid/filterfw/core/Frame;->getTimestamp()J

    move-result-wide v46

    move-wide/from16 v0, v46

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mCurrentTimeStamp:J

    .line 386
    invoke-virtual/range {v23 .. v23}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    move-result-object v24

    .line 388
    .local v24, "inputFormat":Landroid/filterfw/core/FrameFormat;
    invoke-virtual/range {v24 .. v24}, Landroid/filterfw/core/FrameFormat;->getWidth()I

    move-result v44

    .line 389
    .local v44, "width":I
    invoke-virtual/range {v24 .. v24}, Landroid/filterfw/core/FrameFormat;->getHeight()I

    move-result v21

    .line 390
    .local v21, "height":I
    move/from16 v0, v44

    move/from16 v1, v21

    if-le v0, v1, :cond_4

    .line 391
    const/16 v45, 0x2

    move/from16 v0, v45

    new-array v10, v0, [F

    const/16 v45, 0x0

    const/high16 v46, 0x3f800000    # 1.0f

    aput v46, v10, v45

    const/16 v45, 0x1

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v46, v0

    move/from16 v0, v44

    int-to-float v0, v0

    move/from16 v47, v0

    div-float v46, v46, v47

    aput v46, v10, v45

    .line 395
    .local v10, "aspectRatio":[F
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mBigEyesProgram:Landroid/filterfw/core/ShaderProgram;

    move-object/from16 v45, v0

    if-nez v45, :cond_5

    .line 396
    invoke-direct/range {p0 .. p1}, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->createProgram(Landroid/filterfw/core/FilterContext;)V

    .line 397
    move-object/from16 v0, p0

    iput-object v10, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mAspect:[F

    .line 398
    invoke-direct/range {p0 .. p1}, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->createMesh(Landroid/filterfw/core/FilterContext;)V

    .line 405
    :cond_0
    :goto_1
    const-string v45, "faces"

    move-object/from16 v0, p0

    move-object/from16 v1, v45

    invoke-virtual {v0, v1}, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->pullInput(Ljava/lang/String;)Landroid/filterfw/core/Frame;

    move-result-object v20

    .line 406
    .local v20, "facesFrame":Landroid/filterfw/core/Frame;
    invoke-virtual/range {v20 .. v20}, Landroid/filterfw/core/Frame;->getObjectValue()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/google/android/filterpacks/facedetect/FaceMeta;

    .line 408
    .local v19, "faces":Lcom/google/android/filterpacks/facedetect/FaceMeta;
    const/16 v45, 0x2

    move/from16 v0, v45

    new-array v14, v0, [F

    .line 409
    .local v14, "center":[F
    const/16 v45, 0x2

    move/from16 v0, v45

    new-array v0, v0, [F

    move-object/from16 v43, v0

    .line 411
    .local v43, "weight":[F
    invoke-virtual/range {v19 .. v19}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->count()I

    move-result v18

    .line 412
    .local v18, "face_count":I
    if-lez v18, :cond_d

    .line 413
    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mDistortionAmount:F

    .line 414
    .local v6, "amount":F
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mEnableAnimation:Z

    move/from16 v45, v0

    if-eqz v45, :cond_1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mAnimateCurrent:F

    move/from16 v45, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mDistortionAmount:F

    move/from16 v46, v0

    cmpg-float v45, v45, v46

    if-gez v45, :cond_1

    .line 415
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mCurrentTimeStamp:J

    move-wide/from16 v46, v0

    const-wide/16 v48, 0x0

    cmp-long v45, v46, v48

    if-lez v45, :cond_8

    .line 416
    const/high16 v25, 0x44fa0000    # 2000.0f

    .line 418
    .local v25, "kAnimationDuration":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mAnimateCurrent:F

    move/from16 v45, v0

    const/16 v46, 0x0

    cmpl-float v45, v45, v46

    if-nez v45, :cond_7

    .line 419
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mCurrentTimeStamp:J

    move-wide/from16 v46, v0

    move-wide/from16 v0, v46

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mAnimationStartTimeStamp:J

    .line 420
    const v45, 0x3a83126f    # 0.001f

    move/from16 v0, v45

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mAnimateCurrent:F

    .line 428
    .end local v25    # "kAnimationDuration":F
    :goto_2
    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mAnimateCurrent:F

    .line 429
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mDistortionAmount:F

    move/from16 v45, v0

    cmpl-float v45, v6, v45

    if-lez v45, :cond_1

    .line 430
    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mDistortionAmount:F

    .line 432
    :cond_1
    invoke-virtual/range {p1 .. p1}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    move-result-object v45

    invoke-virtual/range {v23 .. v23}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    move-result-object v46

    invoke-virtual/range {v45 .. v46}, Landroid/filterfw/core/FrameManager;->newFrame(Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/Frame;

    move-result-object v12

    .line 433
    .local v12, "buf1":Landroid/filterfw/core/Frame;
    move-object/from16 v0, v23

    invoke-virtual {v12, v0}, Landroid/filterfw/core/Frame;->setDataFromFrame(Landroid/filterfw/core/Frame;)V

    .line 434
    const/4 v13, 0x0

    .line 435
    .local v13, "buf2":Landroid/filterfw/core/Frame;
    const/16 v45, 0x1

    move/from16 v0, v18

    move/from16 v1, v45

    if-le v0, v1, :cond_2

    .line 436
    invoke-virtual/range {p1 .. p1}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    move-result-object v45

    invoke-virtual/range {v23 .. v23}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    move-result-object v46

    invoke-virtual/range {v45 .. v46}, Landroid/filterfw/core/FrameManager;->newFrame(Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/Frame;

    move-result-object v13

    .line 438
    :cond_2
    move-object/from16 v37, v23

    .local v37, "source":Landroid/filterfw/core/Frame;
    move-object/from16 v30, v12

    .line 439
    .local v30, "output":Landroid/filterfw/core/Frame;
    const/16 v22, 0x0

    .local v22, "i":I
    :goto_3
    move/from16 v0, v22

    move/from16 v1, v18

    if-ge v0, v1, :cond_b

    .line 441
    const/16 v45, 0x1

    move/from16 v0, v18

    move/from16 v1, v45

    if-le v0, v1, :cond_3

    if-lez v22, :cond_3

    .line 442
    rem-int/lit8 v45, v22, 0x2

    const/16 v46, 0x1

    move/from16 v0, v45

    move/from16 v1, v46

    if-ne v0, v1, :cond_9

    .line 443
    move-object/from16 v37, v12

    .line 444
    move-object/from16 v30, v13

    .line 445
    move-object/from16 v0, v37

    invoke-virtual {v13, v0}, Landroid/filterfw/core/Frame;->setDataFromFrame(Landroid/filterfw/core/Frame;)V

    .line 453
    :cond_3
    :goto_4
    const/16 v45, 0x2

    move/from16 v0, v45

    new-array v0, v0, [F

    move-object/from16 v27, v0

    const/16 v45, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getLeftEyeX(I)F

    move-result v46

    aput v46, v27, v45

    const/16 v45, 0x1

    move-object/from16 v0, v19

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getLeftEyeY(I)F

    move-result v46

    aput v46, v27, v45

    .line 454
    .local v27, "leftEye":[F
    const/16 v45, 0x2

    move/from16 v0, v45

    new-array v0, v0, [F

    move-object/from16 v34, v0

    const/16 v45, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getRightEyeX(I)F

    move-result v46

    aput v46, v34, v45

    const/16 v45, 0x1

    move-object/from16 v0, v19

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getRightEyeY(I)F

    move-result v46

    aput v46, v34, v45

    .line 455
    .local v34, "rightEye":[F
    const/16 v45, 0x2

    move/from16 v0, v45

    new-array v0, v0, [F

    move-object/from16 v29, v0

    const/16 v45, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getMouthX(I)F

    move-result v46

    aput v46, v29, v45

    const/16 v45, 0x1

    move-object/from16 v0, v19

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getMouthY(I)F

    move-result v46

    aput v46, v29, v45

    .line 456
    .local v29, "mouth":[F
    const/16 v45, 0x1

    aget v45, v34, v45

    const/16 v46, 0x1

    aget v46, v27, v46

    sub-float v45, v45, v46

    const/16 v46, 0x1

    aget v46, v10, v46

    mul-float v45, v45, v46

    move/from16 v0, v45

    float-to-double v0, v0

    move-wide/from16 v46, v0

    const/16 v45, 0x0

    aget v45, v34, v45

    const/16 v48, 0x0

    aget v48, v27, v48

    sub-float v45, v45, v48

    const/16 v48, 0x0

    aget v48, v10, v48

    mul-float v45, v45, v48

    move/from16 v0, v45

    float-to-double v0, v0

    move-wide/from16 v48, v0

    invoke-static/range {v46 .. v49}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v46

    move-wide/from16 v0, v46

    double-to-float v8, v0

    .line 458
    .local v8, "angleEyes":F
    const/16 v45, 0x0

    aget v45, v34, v45

    const/16 v46, 0x0

    aget v46, v27, v46

    add-float v45, v45, v46

    const/high16 v46, 0x40000000    # 2.0f

    div-float v45, v45, v46

    const/16 v46, 0x0

    aget v46, v29, v46

    sub-float v41, v45, v46

    .line 459
    .local v41, "v_axis_x":F
    const/16 v45, 0x1

    aget v45, v34, v45

    const/16 v46, 0x1

    aget v46, v27, v46

    add-float v45, v45, v46

    const/high16 v46, 0x40000000    # 2.0f

    div-float v45, v45, v46

    const/16 v46, 0x1

    aget v46, v29, v46

    sub-float v42, v45, v46

    .line 460
    .local v42, "v_axis_y":F
    move/from16 v0, v42

    float-to-double v0, v0

    move-wide/from16 v46, v0

    move/from16 v0, v41

    float-to-double v0, v0

    move-wide/from16 v48, v0

    invoke-static/range {v46 .. v49}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v46

    move-wide/from16 v0, v46

    double-to-float v9, v0

    .line 461
    .local v9, "angleFace":F
    float-to-double v0, v9

    move-wide/from16 v46, v0

    const-wide v48, 0x3ff921fb54442d18L    # 1.5707963267948966

    sub-double v46, v46, v48

    move-wide/from16 v0, v46

    double-to-float v9, v0

    .line 463
    const/16 v45, 0x0

    aget v45, v27, v45

    const/16 v46, 0x0

    aget v46, v34, v46

    sub-float v45, v45, v46

    const/16 v46, 0x0

    aget v46, v10, v46

    mul-float v15, v45, v46

    .line 464
    .local v15, "dx":F
    const/16 v45, 0x1

    aget v45, v27, v45

    const/16 v46, 0x1

    aget v46, v34, v46

    sub-float v45, v45, v46

    const/16 v46, 0x1

    aget v46, v10, v46

    mul-float v16, v45, v46

    .line 465
    .local v16, "dy":F
    mul-float v45, v15, v15

    mul-float v46, v16, v16

    add-float v45, v45, v46

    move/from16 v0, v45

    float-to-double v0, v0

    move-wide/from16 v46, v0

    invoke-static/range {v46 .. v47}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v46

    move-wide/from16 v0, v46

    double-to-float v0, v0

    move/from16 v28, v0

    .line 467
    .local v28, "length":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mCurrentEffect:I

    move/from16 v45, v0

    const/16 v46, 0x1

    move/from16 v0, v45

    move/from16 v1, v46

    if-ne v0, v1, :cond_a

    .line 468
    const/16 v45, 0x1

    aget v45, v27, v45

    sub-float v45, v45, v28

    const/16 v46, 0x1

    aget v46, v34, v46

    sub-float v46, v46, v28

    invoke-static/range {v45 .. v46}, Ljava/lang/Math;->min(FF)F

    move-result v40

    .line 469
    .local v40, "top":F
    const/16 v45, 0x0

    aget v45, v27, v45

    sub-float v45, v45, v28

    const/16 v46, 0x0

    aget v46, v34, v46

    sub-float v46, v46, v28

    invoke-static/range {v45 .. v46}, Ljava/lang/Math;->min(FF)F

    move-result v26

    .line 471
    .local v26, "left":F
    const/16 v45, 0x1

    aget v45, v27, v45

    add-float v45, v45, v28

    const/16 v46, 0x1

    aget v46, v34, v46

    add-float v46, v46, v28

    invoke-static/range {v45 .. v46}, Ljava/lang/Math;->max(FF)F

    move-result v11

    .line 472
    .local v11, "bottom":F
    const/16 v45, 0x0

    aget v45, v27, v45

    add-float v45, v45, v28

    const/16 v46, 0x0

    aget v46, v34, v46

    add-float v46, v46, v28

    invoke-static/range {v45 .. v46}, Ljava/lang/Math;->max(FF)F

    move-result v33

    .line 474
    .local v33, "right":F
    new-instance v31, Landroid/filterfw/geometry/Rectangle;

    sub-float v45, v33, v26

    sub-float v46, v11, v40

    move-object/from16 v0, v31

    move/from16 v1, v26

    move/from16 v2, v40

    move/from16 v3, v45

    move/from16 v4, v46

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/filterfw/geometry/Rectangle;-><init>(FFFF)V

    .line 475
    .local v31, "rect":Landroid/filterfw/geometry/Rectangle;
    const/16 v45, 0x0

    const/16 v46, 0x0

    move-object/from16 v0, v31

    move/from16 v1, v45

    move/from16 v2, v46

    invoke-virtual {v0, v1, v2}, Landroid/filterfw/geometry/Rectangle;->translated(FF)Landroid/filterfw/geometry/Quad;

    move-result-object v32

    .line 477
    .local v32, "region":Landroid/filterfw/geometry/Quad;
    const/16 v45, 0x0

    const/16 v46, 0x0

    aget v46, v27, v46

    const/16 v47, 0x0

    aget v47, v34, v47

    add-float v46, v46, v47

    const/high16 v47, 0x40000000    # 2.0f

    div-float v46, v46, v47

    aput v46, v14, v45

    .line 478
    const/16 v45, 0x1

    const/16 v46, 0x1

    aget v46, v27, v46

    const/16 v47, 0x1

    aget v47, v34, v47

    add-float v46, v46, v47

    const/high16 v47, 0x40000000    # 2.0f

    div-float v46, v46, v47

    aput v46, v14, v45

    .line 479
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mBigEyesProgram:Landroid/filterfw/core/ShaderProgram;

    move-object/from16 v45, v0

    const-string v46, "center"

    move-object/from16 v0, v45

    move-object/from16 v1, v46

    invoke-virtual {v0, v1, v14}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 480
    const/16 v45, 0x4

    move/from16 v0, v45

    new-array v0, v0, [F

    move-object/from16 v35, v0

    const/16 v45, 0x0

    float-to-double v0, v8

    move-wide/from16 v46, v0

    invoke-static/range {v46 .. v47}, Ljava/lang/Math;->cos(D)D

    move-result-wide v46

    move-wide/from16 v0, v46

    double-to-float v0, v0

    move/from16 v46, v0

    aput v46, v35, v45

    const/16 v45, 0x1

    float-to-double v0, v8

    move-wide/from16 v46, v0

    invoke-static/range {v46 .. v47}, Ljava/lang/Math;->sin(D)D

    move-result-wide v46

    move-wide/from16 v0, v46

    double-to-float v0, v0

    move/from16 v46, v0

    aput v46, v35, v45

    const/16 v45, 0x2

    float-to-double v0, v8

    move-wide/from16 v46, v0

    invoke-static/range {v46 .. v47}, Ljava/lang/Math;->sin(D)D

    move-result-wide v46

    move-wide/from16 v0, v46

    neg-double v0, v0

    move-wide/from16 v46, v0

    move-wide/from16 v0, v46

    double-to-float v0, v0

    move/from16 v46, v0

    aput v46, v35, v45

    const/16 v45, 0x3

    float-to-double v0, v8

    move-wide/from16 v46, v0

    invoke-static/range {v46 .. v47}, Ljava/lang/Math;->cos(D)D

    move-result-wide v46

    move-wide/from16 v0, v46

    double-to-float v0, v0

    move/from16 v46, v0

    aput v46, v35, v45

    .line 482
    .local v35, "rotate":[F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mBigEyesProgram:Landroid/filterfw/core/ShaderProgram;

    move-object/from16 v45, v0

    const-string v46, "rotate"

    move-object/from16 v0, v45

    move-object/from16 v1, v46

    move-object/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 483
    const/16 v45, 0x2

    move/from16 v0, v45

    new-array v0, v0, [F

    move-object/from16 v36, v0

    const/16 v45, 0x0

    const/16 v46, 0x0

    aget v46, v10, v46

    div-float v46, v28, v46

    aput v46, v36, v45

    const/16 v45, 0x1

    const/16 v46, 0x1

    aget v46, v10, v46

    div-float v46, v28, v46

    aput v46, v36, v45

    .line 484
    .local v36, "scales":[F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mBigEyesProgram:Landroid/filterfw/core/ShaderProgram;

    move-object/from16 v45, v0

    const-string v46, "weight"

    move-object/from16 v0, v45

    move-object/from16 v1, v46

    move-object/from16 v2, v36

    invoke-virtual {v0, v1, v2}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 485
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mBigEyesProgram:Landroid/filterfw/core/ShaderProgram;

    move-object/from16 v45, v0

    const-string v46, "amount"

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v47

    invoke-virtual/range {v45 .. v47}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 487
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mBigEyesProgram:Landroid/filterfw/core/ShaderProgram;

    move-object/from16 v45, v0

    move-object/from16 v0, v45

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/filterfw/core/ShaderProgram;->setSourceRegion(Landroid/filterfw/geometry/Quad;)V

    .line 488
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mBigEyesProgram:Landroid/filterfw/core/ShaderProgram;

    move-object/from16 v45, v0

    move-object/from16 v0, v45

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/filterfw/core/ShaderProgram;->setTargetRegion(Landroid/filterfw/geometry/Quad;)V

    .line 491
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mBigEyesProgram:Landroid/filterfw/core/ShaderProgram;

    move-object/from16 v45, v0

    move-object/from16 v0, v45

    move-object/from16 v1, v37

    move-object/from16 v2, v30

    invoke-virtual {v0, v1, v2}, Landroid/filterfw/core/ShaderProgram;->process(Landroid/filterfw/core/Frame;Landroid/filterfw/core/Frame;)V

    .line 439
    .end local v11    # "bottom":F
    .end local v26    # "left":F
    .end local v33    # "right":F
    .end local v36    # "scales":[F
    .end local v40    # "top":F
    :goto_5
    add-int/lit8 v22, v22, 0x1

    goto/16 :goto_3

    .line 393
    .end local v6    # "amount":F
    .end local v8    # "angleEyes":F
    .end local v9    # "angleFace":F
    .end local v10    # "aspectRatio":[F
    .end local v12    # "buf1":Landroid/filterfw/core/Frame;
    .end local v13    # "buf2":Landroid/filterfw/core/Frame;
    .end local v14    # "center":[F
    .end local v15    # "dx":F
    .end local v16    # "dy":F
    .end local v18    # "face_count":I
    .end local v19    # "faces":Lcom/google/android/filterpacks/facedetect/FaceMeta;
    .end local v20    # "facesFrame":Landroid/filterfw/core/Frame;
    .end local v22    # "i":I
    .end local v27    # "leftEye":[F
    .end local v28    # "length":F
    .end local v29    # "mouth":[F
    .end local v30    # "output":Landroid/filterfw/core/Frame;
    .end local v31    # "rect":Landroid/filterfw/geometry/Rectangle;
    .end local v32    # "region":Landroid/filterfw/geometry/Quad;
    .end local v34    # "rightEye":[F
    .end local v35    # "rotate":[F
    .end local v37    # "source":Landroid/filterfw/core/Frame;
    .end local v41    # "v_axis_x":F
    .end local v42    # "v_axis_y":F
    .end local v43    # "weight":[F
    :cond_4
    const/16 v45, 0x2

    move/from16 v0, v45

    new-array v10, v0, [F

    const/16 v45, 0x0

    move/from16 v0, v44

    int-to-float v0, v0

    move/from16 v46, v0

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v47, v0

    div-float v46, v46, v47

    aput v46, v10, v45

    const/16 v45, 0x1

    const/high16 v46, 0x3f800000    # 1.0f

    aput v46, v10, v45

    .restart local v10    # "aspectRatio":[F
    goto/16 :goto_0

    .line 400
    :cond_5
    const/16 v45, 0x0

    aget v45, v10, v45

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mAspect:[F

    move-object/from16 v46, v0

    const/16 v47, 0x0

    aget v46, v46, v47

    cmpl-float v45, v45, v46

    if-nez v45, :cond_6

    const/16 v45, 0x1

    aget v45, v10, v45

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mAspect:[F

    move-object/from16 v46, v0

    const/16 v47, 0x1

    aget v46, v46, v47

    cmpl-float v45, v45, v46

    if-eqz v45, :cond_0

    .line 401
    :cond_6
    move-object/from16 v0, p0

    iput-object v10, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mAspect:[F

    .line 402
    invoke-direct/range {p0 .. p1}, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->createMesh(Landroid/filterfw/core/FilterContext;)V

    goto/16 :goto_1

    .line 422
    .restart local v6    # "amount":F
    .restart local v14    # "center":[F
    .restart local v18    # "face_count":I
    .restart local v19    # "faces":Lcom/google/android/filterpacks/facedetect/FaceMeta;
    .restart local v20    # "facesFrame":Landroid/filterfw/core/Frame;
    .restart local v25    # "kAnimationDuration":F
    .restart local v43    # "weight":[F
    :cond_7
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mCurrentTimeStamp:J

    move-wide/from16 v46, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mAnimationStartTimeStamp:J

    move-wide/from16 v48, v0

    sub-long v38, v46, v48

    .line 423
    .local v38, "t":J
    const-wide/32 v46, 0xf4240

    div-long v38, v38, v46

    .line 424
    move-wide/from16 v0, v38

    long-to-float v0, v0

    move/from16 v45, v0

    const/high16 v46, 0x44fa0000    # 2000.0f

    div-float v45, v45, v46

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mDistortionAmount:F

    move/from16 v46, v0

    mul-float v45, v45, v46

    move/from16 v0, v45

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mAnimateCurrent:F

    goto/16 :goto_2

    .line 427
    .end local v25    # "kAnimationDuration":F
    .end local v38    # "t":J
    :cond_8
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mAnimateCurrent:F

    move/from16 v45, v0

    const v46, 0x3cf5c28f    # 0.03f

    add-float v45, v45, v46

    move/from16 v0, v45

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mAnimateCurrent:F

    goto/16 :goto_2

    .line 447
    .restart local v12    # "buf1":Landroid/filterfw/core/Frame;
    .restart local v13    # "buf2":Landroid/filterfw/core/Frame;
    .restart local v22    # "i":I
    .restart local v30    # "output":Landroid/filterfw/core/Frame;
    .restart local v37    # "source":Landroid/filterfw/core/Frame;
    :cond_9
    move-object/from16 v37, v13

    .line 448
    move-object/from16 v30, v12

    .line 449
    move-object/from16 v0, v37

    invoke-virtual {v12, v0}, Landroid/filterfw/core/Frame;->setDataFromFrame(Landroid/filterfw/core/Frame;)V

    goto/16 :goto_4

    .line 494
    .restart local v8    # "angleEyes":F
    .restart local v9    # "angleFace":F
    .restart local v15    # "dx":F
    .restart local v16    # "dy":F
    .restart local v27    # "leftEye":[F
    .restart local v28    # "length":F
    .restart local v29    # "mouth":[F
    .restart local v34    # "rightEye":[F
    .restart local v41    # "v_axis_x":F
    .restart local v42    # "v_axis_y":F
    :cond_a
    move v7, v9

    .line 495
    .local v7, "angle":F
    const/high16 v17, 0x3f800000    # 1.0f

    .line 496
    .local v17, "effectSize":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mCurrentEffect:I

    move/from16 v45, v0

    packed-switch v45, :pswitch_data_0

    .line 538
    :pswitch_0
    new-instance v45, Ljava/lang/RuntimeException;

    new-instance v46, Ljava/lang/StringBuilder;

    invoke-direct/range {v46 .. v46}, Ljava/lang/StringBuilder;-><init>()V

    const-string v47, "Undefined effect: "

    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v46

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mCurrentEffect:I

    move/from16 v47, v0

    invoke-virtual/range {v46 .. v47}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v46

    invoke-virtual/range {v46 .. v46}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v46

    invoke-direct/range {v45 .. v46}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v45

    .line 499
    :pswitch_1
    const/16 v45, 0x0

    const/high16 v46, 0x3f000000    # 0.5f

    const/16 v47, 0x0

    aget v47, v27, v47

    mul-float v46, v46, v47

    const/high16 v47, 0x3f000000    # 0.5f

    const/16 v48, 0x0

    aget v48, v34, v48

    mul-float v47, v47, v48

    add-float v46, v46, v47

    const v47, 0x38d1b717    # 1.0E-4f

    const/16 v48, 0x0

    aget v48, v29, v48

    mul-float v47, v47, v48

    add-float v46, v46, v47

    aput v46, v14, v45

    .line 500
    const/16 v45, 0x1

    const/high16 v46, 0x3f000000    # 0.5f

    const/16 v47, 0x1

    aget v47, v27, v47

    mul-float v46, v46, v47

    const/high16 v47, 0x3f000000    # 0.5f

    const/16 v48, 0x1

    aget v48, v34, v48

    mul-float v47, v47, v48

    add-float v46, v46, v47

    const v47, 0x38d1b717    # 1.0E-4f

    const/16 v48, 0x1

    aget v48, v29, v48

    mul-float v47, v47, v48

    add-float v46, v46, v47

    aput v46, v14, v45

    .line 501
    const/high16 v17, 0x3f800000    # 1.0f

    .line 503
    move v7, v8

    .line 541
    :goto_6
    const/16 v45, 0x0

    mul-float v46, v17, v28

    const/16 v47, 0x0

    aget v47, v10, v47

    div-float v46, v46, v47

    aput v46, v43, v45

    .line 542
    const/16 v45, 0x1

    mul-float v46, v17, v28

    const/16 v47, 0x1

    aget v47, v10, v47

    div-float v46, v46, v47

    aput v46, v43, v45

    .line 543
    new-instance v31, Landroid/filterfw/geometry/Rectangle;

    const/high16 v45, -0x40800000    # -1.0f

    const/16 v46, 0x0

    aget v46, v43, v46

    mul-float v45, v45, v46

    const/high16 v46, -0x40800000    # -1.0f

    const/16 v47, 0x1

    aget v47, v43, v47

    mul-float v46, v46, v47

    const/high16 v47, 0x40000000    # 2.0f

    const/16 v48, 0x0

    aget v48, v43, v48

    mul-float v47, v47, v48

    const/high16 v48, 0x40000000    # 2.0f

    const/16 v49, 0x1

    aget v49, v43, v49

    mul-float v48, v48, v49

    move-object/from16 v0, v31

    move/from16 v1, v45

    move/from16 v2, v46

    move/from16 v3, v47

    move/from16 v4, v48

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/filterfw/geometry/Rectangle;-><init>(FFFF)V

    .line 546
    .restart local v31    # "rect":Landroid/filterfw/geometry/Rectangle;
    const/16 v45, 0x0

    aget v45, v14, v45

    const/16 v46, 0x1

    aget v46, v14, v46

    move-object/from16 v0, v31

    move/from16 v1, v45

    move/from16 v2, v46

    invoke-virtual {v0, v1, v2}, Landroid/filterfw/geometry/Rectangle;->translated(FF)Landroid/filterfw/geometry/Quad;

    move-result-object v32

    .line 549
    .restart local v32    # "region":Landroid/filterfw/geometry/Quad;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mIdentityProgram:Landroid/filterfw/core/ShaderProgram;

    move-object/from16 v45, v0

    const-string v46, "center"

    move-object/from16 v0, v45

    move-object/from16 v1, v46

    invoke-virtual {v0, v1, v14}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 550
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mIdentityProgram:Landroid/filterfw/core/ShaderProgram;

    move-object/from16 v45, v0

    const-string v46, "weight"

    move-object/from16 v0, v45

    move-object/from16 v1, v46

    move-object/from16 v2, v43

    invoke-virtual {v0, v1, v2}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 551
    const/16 v45, 0x4

    move/from16 v0, v45

    new-array v0, v0, [F

    move-object/from16 v35, v0

    const/16 v45, 0x0

    float-to-double v0, v7

    move-wide/from16 v46, v0

    invoke-static/range {v46 .. v47}, Ljava/lang/Math;->cos(D)D

    move-result-wide v46

    move-wide/from16 v0, v46

    double-to-float v0, v0

    move/from16 v46, v0

    aput v46, v35, v45

    const/16 v45, 0x1

    float-to-double v0, v7

    move-wide/from16 v46, v0

    invoke-static/range {v46 .. v47}, Ljava/lang/Math;->sin(D)D

    move-result-wide v46

    move-wide/from16 v0, v46

    double-to-float v0, v0

    move/from16 v46, v0

    aput v46, v35, v45

    const/16 v45, 0x2

    float-to-double v0, v7

    move-wide/from16 v46, v0

    invoke-static/range {v46 .. v47}, Ljava/lang/Math;->sin(D)D

    move-result-wide v46

    move-wide/from16 v0, v46

    neg-double v0, v0

    move-wide/from16 v46, v0

    move-wide/from16 v0, v46

    double-to-float v0, v0

    move/from16 v46, v0

    aput v46, v35, v45

    const/16 v45, 0x3

    float-to-double v0, v7

    move-wide/from16 v46, v0

    invoke-static/range {v46 .. v47}, Ljava/lang/Math;->cos(D)D

    move-result-wide v46

    move-wide/from16 v0, v46

    double-to-float v0, v0

    move/from16 v46, v0

    aput v46, v35, v45

    .line 553
    .restart local v35    # "rotate":[F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mIdentityProgram:Landroid/filterfw/core/ShaderProgram;

    move-object/from16 v45, v0

    const-string v46, "rotate"

    move-object/from16 v0, v45

    move-object/from16 v1, v46

    move-object/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 554
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mIdentityProgram:Landroid/filterfw/core/ShaderProgram;

    move-object/from16 v45, v0

    const-string v46, "amount"

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v47

    invoke-virtual/range {v45 .. v47}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 556
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mIdentityProgram:Landroid/filterfw/core/ShaderProgram;

    move-object/from16 v45, v0

    move-object/from16 v0, v45

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/filterfw/core/ShaderProgram;->setSourceRegion(Landroid/filterfw/geometry/Quad;)V

    .line 557
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mIdentityProgram:Landroid/filterfw/core/ShaderProgram;

    move-object/from16 v45, v0

    move-object/from16 v0, v45

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/filterfw/core/ShaderProgram;->setTargetRegion(Landroid/filterfw/geometry/Quad;)V

    .line 560
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->mIdentityProgram:Landroid/filterfw/core/ShaderProgram;

    move-object/from16 v45, v0

    move-object/from16 v0, v45

    move-object/from16 v1, v37

    move-object/from16 v2, v30

    invoke-virtual {v0, v1, v2}, Landroid/filterfw/core/ShaderProgram;->process(Landroid/filterfw/core/Frame;Landroid/filterfw/core/Frame;)V

    goto/16 :goto_5

    .line 509
    .end local v31    # "rect":Landroid/filterfw/geometry/Rectangle;
    .end local v32    # "region":Landroid/filterfw/geometry/Quad;
    .end local v35    # "rotate":[F
    :pswitch_2
    const/16 v45, 0x0

    const/16 v46, 0x0

    aget v46, v29, v46

    const v47, 0x3d75c28f    # 0.06f

    mul-float v47, v47, v41

    sub-float v46, v46, v47

    aput v46, v14, v45

    .line 510
    const/16 v45, 0x1

    const/16 v46, 0x1

    aget v46, v29, v46

    const v47, 0x3d75c28f    # 0.06f

    mul-float v47, v47, v42

    sub-float v46, v46, v47

    aput v46, v14, v45

    .line 511
    const v17, 0x3f333333    # 0.7f

    .line 512
    goto/16 :goto_6

    .line 517
    :pswitch_3
    const/16 v45, 0x0

    const/16 v46, 0x0

    aget v46, v29, v46

    const v47, 0x3d75c28f    # 0.06f

    mul-float v47, v47, v41

    sub-float v46, v46, v47

    aput v46, v14, v45

    .line 518
    const/16 v45, 0x1

    const/16 v46, 0x1

    aget v46, v29, v46

    const v47, 0x3d75c28f    # 0.06f

    mul-float v47, v47, v42

    sub-float v46, v46, v47

    aput v46, v14, v45

    .line 519
    const/high16 v17, 0x40200000    # 2.5f

    .line 520
    goto/16 :goto_6

    .line 525
    :pswitch_4
    const/16 v45, 0x0

    const/high16 v46, 0x3e800000    # 0.25f

    const/16 v47, 0x0

    aget v47, v27, v47

    mul-float v46, v46, v47

    const/high16 v47, 0x3e800000    # 0.25f

    const/16 v48, 0x0

    aget v48, v34, v48

    mul-float v47, v47, v48

    add-float v46, v46, v47

    const/high16 v47, 0x3f000000    # 0.5f

    const/16 v48, 0x0

    aget v48, v29, v48

    mul-float v47, v47, v48

    add-float v46, v46, v47

    aput v46, v14, v45

    .line 526
    const/16 v45, 0x1

    const/high16 v46, 0x3e800000    # 0.25f

    const/16 v47, 0x1

    aget v47, v27, v47

    mul-float v46, v46, v47

    const/high16 v47, 0x3e800000    # 0.25f

    const/16 v48, 0x1

    aget v48, v34, v48

    mul-float v47, v47, v48

    add-float v46, v46, v47

    const/high16 v47, 0x3f000000    # 0.5f

    const/16 v48, 0x1

    aget v48, v29, v48

    mul-float v47, v47, v48

    add-float v46, v46, v47

    aput v46, v14, v45

    .line 527
    const v17, 0x3f8ccccd    # 1.1f

    .line 528
    goto/16 :goto_6

    .line 533
    :pswitch_5
    const/16 v45, 0x0

    const/high16 v46, 0x3e800000    # 0.25f

    const/16 v47, 0x0

    aget v47, v27, v47

    mul-float v46, v46, v47

    const/high16 v47, 0x3e800000    # 0.25f

    const/16 v48, 0x0

    aget v48, v34, v48

    mul-float v47, v47, v48

    add-float v46, v46, v47

    const/high16 v47, 0x3f000000    # 0.5f

    const/16 v48, 0x0

    aget v48, v29, v48

    mul-float v47, v47, v48

    add-float v46, v46, v47

    aput v46, v14, v45

    .line 534
    const/16 v45, 0x1

    const/high16 v46, 0x3e800000    # 0.25f

    const/16 v47, 0x1

    aget v47, v27, v47

    mul-float v46, v46, v47

    const/high16 v47, 0x3e800000    # 0.25f

    const/16 v48, 0x1

    aget v48, v34, v48

    mul-float v47, v47, v48

    add-float v46, v46, v47

    const/high16 v47, 0x3f000000    # 0.5f

    const/16 v48, 0x1

    aget v48, v29, v48

    mul-float v47, v47, v48

    add-float v46, v46, v47

    aput v46, v14, v45

    .line 535
    const/high16 v17, 0x40000000    # 2.0f

    .line 536
    goto/16 :goto_6

    .line 563
    .end local v7    # "angle":F
    .end local v8    # "angleEyes":F
    .end local v9    # "angleFace":F
    .end local v15    # "dx":F
    .end local v16    # "dy":F
    .end local v17    # "effectSize":F
    .end local v27    # "leftEye":[F
    .end local v28    # "length":F
    .end local v29    # "mouth":[F
    .end local v34    # "rightEye":[F
    .end local v41    # "v_axis_x":F
    .end local v42    # "v_axis_y":F
    :cond_b
    const-string v45, "outimage"

    move-object/from16 v0, p0

    move-object/from16 v1, v45

    move-object/from16 v2, v30

    invoke-virtual {v0, v1, v2}, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->pushOutput(Ljava/lang/String;Landroid/filterfw/core/Frame;)V

    .line 564
    invoke-virtual {v12}, Landroid/filterfw/core/Frame;->release()Landroid/filterfw/core/Frame;

    .line 565
    if-eqz v13, :cond_c

    .line 566
    invoke-virtual {v13}, Landroid/filterfw/core/Frame;->release()Landroid/filterfw/core/Frame;

    .line 570
    .end local v6    # "amount":F
    .end local v12    # "buf1":Landroid/filterfw/core/Frame;
    .end local v13    # "buf2":Landroid/filterfw/core/Frame;
    .end local v22    # "i":I
    .end local v30    # "output":Landroid/filterfw/core/Frame;
    .end local v37    # "source":Landroid/filterfw/core/Frame;
    :cond_c
    :goto_7
    return-void

    .line 568
    :cond_d
    const-string v45, "outimage"

    move-object/from16 v0, p0

    move-object/from16 v1, v45

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->pushOutput(Ljava/lang/String;Landroid/filterfw/core/Frame;)V

    goto :goto_7

    .line 496
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_5
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_1
    .end packed-switch
.end method

.method public setupPorts()V
    .locals 4

    .prologue
    const/4 v2, 0x3

    .line 157
    invoke-static {v2, v2}, Landroid/filterfw/format/ImageFormat;->create(II)Landroid/filterfw/core/MutableFrameFormat;

    move-result-object v1

    .line 159
    .local v1, "imageFormat":Landroid/filterfw/core/FrameFormat;
    const-class v2, Lcom/google/android/filterpacks/facedetect/FaceMeta;

    const/4 v3, 0x2

    invoke-static {v2, v3}, Landroid/filterfw/format/ObjectFormat;->fromClass(Ljava/lang/Class;I)Landroid/filterfw/core/MutableFrameFormat;

    move-result-object v0

    .line 163
    .local v0, "facesFormat":Landroid/filterfw/core/FrameFormat;
    const-string v2, "image"

    invoke-virtual {p0, v2, v1}, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->addMaskedInputPort(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)V

    .line 164
    const-string v2, "faces"

    invoke-virtual {p0, v2, v0}, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->addMaskedInputPort(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)V

    .line 165
    const-string v2, "outimage"

    const-string v3, "image"

    invoke-virtual {p0, v2, v3}, Lcom/google/android/filterpacks/facedetect/GoofyFastRenderFilter;->addOutputBasedOnInput(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    return-void
.end method
