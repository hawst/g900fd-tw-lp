.class public Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;
.super Landroid/filterfw/core/Filter;
.source "GoofyRenderFilter.java"


# static fields
.field private static final BIG_EYES:I = 0x1

.field private static final BIG_MOUTH:I = 0x2

.field private static final BIG_NOSE:I = 0x4

.field private static final NUM_EFFECTS:I = 0x6

.field private static final SMALL_EYES:I = 0x5

.field private static final SMALL_MOUTH:I = 0x3

.field private static final SQUEEZE:I = 0x0

.field private static final TAG:Ljava/lang/String; = "GoofyRenderFilter"


# instance fields
.field private mBigEyesProgram:Landroid/filterfw/core/ShaderProgram;

.field private final mBigEyesShader:Ljava/lang/String;

.field private mCurrentEffect:I
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "currentEffect"
    .end annotation
.end field

.field private mDistortionAmount:F
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        hasDefault = true
        name = "distortionAmount"
    .end annotation
.end field

.field private mGoofyProgram:Landroid/filterfw/core/ShaderProgram;

.field private final mGoofyShader:Ljava/lang/String;

.field private mShrinkFunc:Z

.field private mTableFrame:Landroid/filterfw/core/Frame;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 148
    invoke-direct {p0, p1}, Landroid/filterfw/core/Filter;-><init>(Ljava/lang/String;)V

    .line 54
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mCurrentEffect:I

    .line 60
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mDistortionAmount:F

    .line 75
    const-string v0, "precision mediump float;\nuniform sampler2D tex_sampler_0;\nuniform sampler2D tex_sampler_1;\nuniform vec2 center;\nuniform vec2 weight;\nuniform float dist_offset;\nuniform float dist_mult;\nuniform bool use_shrink;\nvarying vec2 v_texcoord;\nvoid main() {\n  vec2 point = v_texcoord - center;\n  vec2 spoint;\n  spoint = weight * point;\n  float dist = length(spoint) * dist_mult + dist_offset;\n  vec4 scale_byte = texture2D(tex_sampler_1, vec2(dist, 0.5));\n  float scale = scale_byte.g + scale_byte.r * 0.00390625;\n  if (use_shrink) {\n    scale = 1.0 + scale;\n  } else {\n    scale = 1.0 - scale;\n  }\n  if (dist >= 1.0) { \n     scale = 1.0;\n  } \n  gl_FragColor = texture2D(tex_sampler_0, center + scale * point);\n}\n"

    iput-object v0, p0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mGoofyShader:Ljava/lang/String;

    .line 103
    const-string v0, "precision mediump float;\nuniform sampler2D tex_sampler_0;\nuniform sampler2D tex_sampler_1;\nuniform vec2 left_eye;\nuniform vec2 right_eye;\nuniform vec2 scale;\nuniform float dist_offset;\nuniform float dist_mult;\nvarying vec2 v_texcoord;\nvoid main() {\n  vec2 left_eye_offset = (v_texcoord - left_eye); \n  float left_eye_dist = length(left_eye_offset * scale); \n  vec2 right_eye_offset = (v_texcoord - right_eye); \n  float right_eye_dist = length(right_eye_offset * scale); \n  float dist;\n  vec2 offset;\n  vec2 center;\n  if (left_eye_dist < 1.0 || right_eye_dist < 1.0){\n    float dist_left = left_eye_dist * dist_mult + dist_offset;\n    vec4 value_byte = texture2D(tex_sampler_1, vec2(dist_left, 0.5));\n    float value_left = (value_byte.g + value_byte.r * 0.00390625);\n    vec4 color_left = texture2D(tex_sampler_0,\n            left_eye + (1.0 - value_left) * left_eye_offset);\n    float dist_right = right_eye_dist * dist_mult + dist_offset;\n    value_byte = texture2D(tex_sampler_1, vec2(dist_right, 0.5));\n    float value_right = (value_byte.g + value_byte.r * 0.00390625);\n    vec4 color_right = texture2D(tex_sampler_0,\n            right_eye + (1.0 - value_right) * right_eye_offset);\n    float alpha = value_left / (value_left + value_right);\n    gl_FragColor = mix(color_right, color_left, alpha);\n  } else {\n    gl_FragColor = texture2D(tex_sampler_0, v_texcoord);\n  }\n}\n"

    iput-object v0, p0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mBigEyesShader:Ljava/lang/String;

    .line 143
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mShrinkFunc:Z

    .line 149
    return-void
.end method

.method private createLookupTable(Landroid/filterfw/core/FilterContext;)V
    .locals 20
    .param p1, "context"    # Landroid/filterfw/core/FilterContext;

    .prologue
    .line 187
    const/16 v10, 0x7d0

    .line 188
    .local v10, "tableSize":I
    const v6, 0x477fff00    # 65535.0f

    .line 189
    .local v6, "precision":F
    const v5, 0x40490fda

    .line 191
    .local v5, "pi":F
    const/16 v12, 0x7d0

    new-array v2, v12, [I

    .line 193
    .local v2, "array":[I
    const/high16 v7, 0x3f800000    # 1.0f

    .line 194
    .local v7, "scale":F
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_0
    const/16 v12, 0x7d0

    if-ge v4, v12, :cond_4

    .line 195
    int-to-float v12, v4

    const/high16 v13, 0x44fa0000    # 2000.0f

    div-float v3, v12, v13

    .line 196
    .local v3, "dist":F
    const/4 v11, 0x0

    .line 197
    .local v11, "value":F
    move-object/from16 v0, p0

    iget v12, v0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mCurrentEffect:I

    packed-switch v12, :pswitch_data_0

    .line 232
    :goto_1
    const v12, 0x477fff00    # 65535.0f

    mul-float/2addr v12, v11

    float-to-int v12, v12

    aput v12, v2, v4

    .line 194
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 201
    :pswitch_0
    const v8, 0x3e99999a    # 0.3f

    .line 206
    .local v8, "sigma":F
    if-nez v4, :cond_0

    const-wide/high16 v12, 0x4090000000000000L    # 1024.0

    invoke-static {v12, v13}, Ljava/lang/Math;->log(D)D

    move-result-wide v12

    float-to-double v14, v8

    mul-double/2addr v12, v14

    float-to-double v14, v8

    mul-double/2addr v12, v14

    invoke-static {v12, v13}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v12

    double-to-float v7, v12

    .line 207
    :cond_0
    const/high16 v12, 0x3f800000    # 1.0f

    cmpl-float v12, v7, v12

    if-lez v12, :cond_1

    mul-float/2addr v3, v7

    .line 210
    :cond_1
    move-object/from16 v0, p0

    iget v12, v0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mDistortionAmount:F

    const v13, 0x3f4ccccd    # 0.8f

    mul-float/2addr v12, v13

    const/high16 v13, 0x40000000    # 2.0f

    neg-float v14, v3

    mul-float/2addr v14, v3

    mul-float v15, v8, v8

    div-float/2addr v14, v15

    float-to-double v14, v14

    invoke-static {v14, v15}, Ljava/lang/Math;->exp(D)D

    move-result-wide v14

    double-to-float v14, v14

    sub-float/2addr v13, v14

    const/high16 v14, 0x40000000    # 2.0f

    const/high16 v15, 0x3f800000    # 1.0f

    const/high16 v16, 0x3e800000    # 0.25f

    add-float v16, v16, v3

    move/from16 v0, v16

    float-to-double v0, v0

    move-wide/from16 v16, v0

    const-wide/high16 v18, 0x4010000000000000L    # 4.0

    invoke-static/range {v16 .. v19}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v16

    move-wide/from16 v0, v16

    neg-double v0, v0

    move-wide/from16 v16, v0

    const-wide/high16 v18, 0x4000000000000000L    # 2.0

    mul-double v16, v16, v18

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->exp(D)D

    move-result-wide v16

    move-wide/from16 v0, v16

    double-to-float v0, v0

    move/from16 v16, v0

    add-float v15, v15, v16

    div-float/2addr v14, v15

    sub-float/2addr v13, v14

    mul-float v11, v12, v13

    .line 213
    const/4 v12, 0x1

    move-object/from16 v0, p0

    iput-boolean v12, v0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mShrinkFunc:Z

    goto :goto_1

    .line 219
    .end local v8    # "sigma":F
    :pswitch_1
    const v8, 0x3f333333    # 0.7f

    .line 224
    .restart local v8    # "sigma":F
    if-nez v4, :cond_2

    const-wide/high16 v12, 0x4090000000000000L    # 1024.0

    invoke-static {v12, v13}, Ljava/lang/Math;->log(D)D

    move-result-wide v12

    float-to-double v14, v8

    mul-double/2addr v12, v14

    float-to-double v14, v8

    mul-double/2addr v12, v14

    invoke-static {v12, v13}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v12

    double-to-float v7, v12

    .line 225
    :cond_2
    const/high16 v12, 0x3f800000    # 1.0f

    cmpl-float v12, v7, v12

    if-lez v12, :cond_3

    mul-float/2addr v3, v7

    .line 226
    :cond_3
    const v12, 0x3f266666    # 0.65f

    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mDistortionAmount:F

    mul-float/2addr v12, v13

    neg-float v13, v3

    mul-float/2addr v13, v3

    mul-float v14, v8, v8

    div-float/2addr v13, v14

    float-to-double v14, v13

    invoke-static {v14, v15}, Ljava/lang/Math;->exp(D)D

    move-result-wide v14

    double-to-float v13, v14

    mul-float v11, v12, v13

    .line 228
    const/4 v12, 0x0

    move-object/from16 v0, p0

    iput-boolean v12, v0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mShrinkFunc:Z

    goto/16 :goto_1

    .line 235
    .end local v3    # "dist":F
    .end local v8    # "sigma":F
    .end local v11    # "value":F
    :cond_4
    const/16 v12, 0x7d0

    const/4 v13, 0x1

    const/4 v14, 0x3

    const/4 v15, 0x3

    invoke-static {v12, v13, v14, v15}, Landroid/filterfw/format/ImageFormat;->create(IIII)Landroid/filterfw/core/MutableFrameFormat;

    move-result-object v9

    .line 238
    .local v9, "tableFormat":Landroid/filterfw/core/FrameFormat;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mTableFrame:Landroid/filterfw/core/Frame;

    if-eqz v12, :cond_5

    .line 239
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mTableFrame:Landroid/filterfw/core/Frame;

    invoke-virtual {v12}, Landroid/filterfw/core/Frame;->release()Landroid/filterfw/core/Frame;

    .line 241
    :cond_5
    invoke-virtual/range {p1 .. p1}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    move-result-object v12

    invoke-virtual {v12, v9}, Landroid/filterfw/core/FrameManager;->newFrame(Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/Frame;

    move-result-object v12

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mTableFrame:Landroid/filterfw/core/Frame;

    .line 242
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mTableFrame:Landroid/filterfw/core/Frame;

    invoke-virtual {v12, v2}, Landroid/filterfw/core/Frame;->setInts([I)V

    .line 244
    move-object/from16 v0, p0

    iget v12, v0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mCurrentEffect:I

    const/4 v13, 0x1

    if-ne v12, v13, :cond_6

    .line 245
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mBigEyesProgram:Landroid/filterfw/core/ShaderProgram;

    const-string v13, "dist_offset"

    const v14, 0x3a03126f    # 5.0E-4f

    invoke-static {v14}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 246
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mBigEyesProgram:Landroid/filterfw/core/ShaderProgram;

    const-string v13, "dist_mult"

    const v14, 0x3f7fdf3b    # 0.9995f

    invoke-static {v14}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 252
    :goto_2
    return-void

    .line 248
    :cond_6
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mGoofyProgram:Landroid/filterfw/core/ShaderProgram;

    const-string v13, "dist_offset"

    const v14, 0x3a03126f    # 5.0E-4f

    invoke-static {v14}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 249
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mGoofyProgram:Landroid/filterfw/core/ShaderProgram;

    const-string v13, "dist_mult"

    const v14, 0x3f7fdf3b    # 0.9995f

    invoke-static {v14}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 250
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mGoofyProgram:Landroid/filterfw/core/ShaderProgram;

    const-string v13, "use_shrink"

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mShrinkFunc:Z

    invoke-static {v14}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v14

    invoke-virtual {v12, v13, v14}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_2

    .line 197
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public fieldPortValueUpdated(Ljava/lang/String;Landroid/filterfw/core/FilterContext;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/filterfw/core/FilterContext;

    .prologue
    .line 181
    iget-object v0, p0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mGoofyProgram:Landroid/filterfw/core/ShaderProgram;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mBigEyesProgram:Landroid/filterfw/core/ShaderProgram;

    if-eqz v0, :cond_0

    .line 182
    invoke-direct {p0, p2}, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->createLookupTable(Landroid/filterfw/core/FilterContext;)V

    .line 184
    :cond_0
    return-void
.end method

.method public getOutputFormat(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/FrameFormat;
    .locals 0
    .param p1, "portName"    # Ljava/lang/String;
    .param p2, "inputFormat"    # Landroid/filterfw/core/FrameFormat;

    .prologue
    .line 167
    return-object p2
.end method

.method protected prepare(Landroid/filterfw/core/FilterContext;)V
    .locals 2
    .param p1, "context"    # Landroid/filterfw/core/FilterContext;

    .prologue
    .line 172
    new-instance v0, Landroid/filterfw/core/ShaderProgram;

    const-string v1, "precision mediump float;\nuniform sampler2D tex_sampler_0;\nuniform sampler2D tex_sampler_1;\nuniform vec2 center;\nuniform vec2 weight;\nuniform float dist_offset;\nuniform float dist_mult;\nuniform bool use_shrink;\nvarying vec2 v_texcoord;\nvoid main() {\n  vec2 point = v_texcoord - center;\n  vec2 spoint;\n  spoint = weight * point;\n  float dist = length(spoint) * dist_mult + dist_offset;\n  vec4 scale_byte = texture2D(tex_sampler_1, vec2(dist, 0.5));\n  float scale = scale_byte.g + scale_byte.r * 0.00390625;\n  if (use_shrink) {\n    scale = 1.0 + scale;\n  } else {\n    scale = 1.0 - scale;\n  }\n  if (dist >= 1.0) { \n     scale = 1.0;\n  } \n  gl_FragColor = texture2D(tex_sampler_0, center + scale * point);\n}\n"

    invoke-direct {v0, p1, v1}, Landroid/filterfw/core/ShaderProgram;-><init>(Landroid/filterfw/core/FilterContext;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mGoofyProgram:Landroid/filterfw/core/ShaderProgram;

    .line 174
    new-instance v0, Landroid/filterfw/core/ShaderProgram;

    const-string v1, "precision mediump float;\nuniform sampler2D tex_sampler_0;\nuniform sampler2D tex_sampler_1;\nuniform vec2 left_eye;\nuniform vec2 right_eye;\nuniform vec2 scale;\nuniform float dist_offset;\nuniform float dist_mult;\nvarying vec2 v_texcoord;\nvoid main() {\n  vec2 left_eye_offset = (v_texcoord - left_eye); \n  float left_eye_dist = length(left_eye_offset * scale); \n  vec2 right_eye_offset = (v_texcoord - right_eye); \n  float right_eye_dist = length(right_eye_offset * scale); \n  float dist;\n  vec2 offset;\n  vec2 center;\n  if (left_eye_dist < 1.0 || right_eye_dist < 1.0){\n    float dist_left = left_eye_dist * dist_mult + dist_offset;\n    vec4 value_byte = texture2D(tex_sampler_1, vec2(dist_left, 0.5));\n    float value_left = (value_byte.g + value_byte.r * 0.00390625);\n    vec4 color_left = texture2D(tex_sampler_0,\n            left_eye + (1.0 - value_left) * left_eye_offset);\n    float dist_right = right_eye_dist * dist_mult + dist_offset;\n    value_byte = texture2D(tex_sampler_1, vec2(dist_right, 0.5));\n    float value_right = (value_byte.g + value_byte.r * 0.00390625);\n    vec4 color_right = texture2D(tex_sampler_0,\n            right_eye + (1.0 - value_right) * right_eye_offset);\n    float alpha = value_left / (value_left + value_right);\n    gl_FragColor = mix(color_right, color_left, alpha);\n  } else {\n    gl_FragColor = texture2D(tex_sampler_0, v_texcoord);\n  }\n}\n"

    invoke-direct {v0, p1, v1}, Landroid/filterfw/core/ShaderProgram;-><init>(Landroid/filterfw/core/FilterContext;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mBigEyesProgram:Landroid/filterfw/core/ShaderProgram;

    .line 176
    invoke-direct {p0, p1}, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->createLookupTable(Landroid/filterfw/core/FilterContext;)V

    .line 177
    return-void
.end method

.method public process(Landroid/filterfw/core/FilterContext;)V
    .locals 42
    .param p1, "context"    # Landroid/filterfw/core/FilterContext;

    .prologue
    .line 258
    const-string v37, "image"

    move-object/from16 v0, p0

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->pullInput(Ljava/lang/String;)Landroid/filterfw/core/Frame;

    move-result-object v20

    .line 259
    .local v20, "input":Landroid/filterfw/core/Frame;
    invoke-virtual/range {v20 .. v20}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    move-result-object v21

    .line 261
    .local v21, "inputFormat":Landroid/filterfw/core/FrameFormat;
    invoke-virtual/range {v21 .. v21}, Landroid/filterfw/core/FrameFormat;->getWidth()I

    move-result v36

    .line 262
    .local v36, "width":I
    invoke-virtual/range {v21 .. v21}, Landroid/filterfw/core/FrameFormat;->getHeight()I

    move-result v18

    .line 263
    .local v18, "height":I
    move/from16 v0, v36

    move/from16 v1, v18

    if-le v0, v1, :cond_2

    .line 264
    const/16 v37, 0x2

    move/from16 v0, v37

    new-array v6, v0, [F

    const/16 v37, 0x0

    const/high16 v38, 0x3f800000    # 1.0f

    aput v38, v6, v37

    const/16 v37, 0x1

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v38, v0

    move/from16 v0, v36

    int-to-float v0, v0

    move/from16 v39, v0

    div-float v38, v38, v39

    aput v38, v6, v37

    .line 268
    .local v6, "aspectRatio":[F
    :goto_0
    const-string v37, "faces"

    move-object/from16 v0, p0

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->pullInput(Ljava/lang/String;)Landroid/filterfw/core/Frame;

    move-result-object v17

    .line 269
    .local v17, "facesFrame":Landroid/filterfw/core/Frame;
    invoke-virtual/range {v17 .. v17}, Landroid/filterfw/core/Frame;->getObjectValue()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/google/android/filterpacks/facedetect/FaceMeta;

    .line 271
    .local v16, "faces":Lcom/google/android/filterpacks/facedetect/FaceMeta;
    const/16 v37, 0x2

    move/from16 v0, v37

    new-array v11, v0, [F

    .line 272
    .local v11, "center":[F
    const/16 v37, 0x2

    move/from16 v0, v37

    new-array v0, v0, [F

    move-object/from16 v35, v0

    .line 274
    .local v35, "weight":[F
    invoke-virtual/range {v16 .. v16}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->count()I

    move-result v14

    .line 275
    .local v14, "face_count":I
    if-lez v14, :cond_7

    .line 276
    invoke-virtual/range {p1 .. p1}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    move-result-object v37

    invoke-virtual/range {v20 .. v20}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    move-result-object v38

    invoke-virtual/range {v37 .. v38}, Landroid/filterfw/core/FrameManager;->newFrame(Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/Frame;

    move-result-object v9

    .line 277
    .local v9, "buf1":Landroid/filterfw/core/Frame;
    move-object/from16 v0, v20

    invoke-virtual {v9, v0}, Landroid/filterfw/core/Frame;->setDataFromFrame(Landroid/filterfw/core/Frame;)V

    .line 278
    const/4 v10, 0x0

    .line 279
    .local v10, "buf2":Landroid/filterfw/core/Frame;
    const/16 v37, 0x1

    move/from16 v0, v37

    if-le v14, v0, :cond_0

    .line 280
    invoke-virtual/range {p1 .. p1}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    move-result-object v37

    invoke-virtual/range {v20 .. v20}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    move-result-object v38

    invoke-virtual/range {v37 .. v38}, Landroid/filterfw/core/FrameManager;->newFrame(Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/Frame;

    move-result-object v10

    .line 282
    :cond_0
    move-object/from16 v33, v20

    .local v33, "source":Landroid/filterfw/core/Frame;
    move-object/from16 v27, v9

    .line 283
    .local v27, "output":Landroid/filterfw/core/Frame;
    const/16 v19, 0x0

    .local v19, "i":I
    :goto_1
    move/from16 v0, v19

    if-ge v0, v14, :cond_5

    .line 285
    const/16 v37, 0x1

    move/from16 v0, v37

    if-le v14, v0, :cond_1

    if-lez v19, :cond_1

    .line 286
    rem-int/lit8 v37, v19, 0x2

    const/16 v38, 0x1

    move/from16 v0, v37

    move/from16 v1, v38

    if-ne v0, v1, :cond_3

    .line 287
    move-object/from16 v33, v9

    .line 288
    move-object/from16 v27, v10

    .line 289
    move-object/from16 v0, v33

    invoke-virtual {v10, v0}, Landroid/filterfw/core/Frame;->setDataFromFrame(Landroid/filterfw/core/Frame;)V

    .line 297
    :cond_1
    :goto_2
    const/16 v37, 0x2

    move/from16 v0, v37

    new-array v0, v0, [F

    move-object/from16 v24, v0

    const/16 v37, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getLeftEyeX(I)F

    move-result v38

    aput v38, v24, v37

    const/16 v37, 0x1

    move-object/from16 v0, v16

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getLeftEyeY(I)F

    move-result v38

    aput v38, v24, v37

    .line 298
    .local v24, "leftEye":[F
    const/16 v37, 0x2

    move/from16 v0, v37

    new-array v0, v0, [F

    move-object/from16 v31, v0

    const/16 v37, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getRightEyeX(I)F

    move-result v38

    aput v38, v31, v37

    const/16 v37, 0x1

    move-object/from16 v0, v16

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getRightEyeY(I)F

    move-result v38

    aput v38, v31, v37

    .line 299
    .local v31, "rightEye":[F
    const/16 v37, 0x2

    move/from16 v0, v37

    new-array v0, v0, [F

    move-object/from16 v26, v0

    const/16 v37, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getMouthX(I)F

    move-result v38

    aput v38, v26, v37

    const/16 v37, 0x1

    move-object/from16 v0, v16

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getMouthY(I)F

    move-result v38

    aput v38, v26, v37

    .line 300
    .local v26, "mouth":[F
    const/16 v37, 0x0

    aget v37, v24, v37

    const/16 v38, 0x0

    aget v38, v31, v38

    sub-float v37, v37, v38

    const/16 v38, 0x0

    aget v38, v6, v38

    mul-float v12, v37, v38

    .line 301
    .local v12, "dx":F
    const/16 v37, 0x1

    aget v37, v24, v37

    const/16 v38, 0x1

    aget v38, v31, v38

    sub-float v37, v37, v38

    const/16 v38, 0x1

    aget v38, v6, v38

    mul-float v13, v37, v38

    .line 302
    .local v13, "dy":F
    mul-float v37, v12, v12

    mul-float v38, v13, v13

    add-float v37, v37, v38

    move/from16 v0, v37

    float-to-double v0, v0

    move-wide/from16 v38, v0

    invoke-static/range {v38 .. v39}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v38

    move-wide/from16 v0, v38

    double-to-float v7, v0

    .line 304
    .local v7, "baseline":F
    const/high16 v15, 0x3f800000    # 1.0f

    .line 306
    .local v15, "face_size":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mCurrentEffect:I

    move/from16 v37, v0

    const/16 v38, 0x1

    move/from16 v0, v37

    move/from16 v1, v38

    if-ne v0, v1, :cond_4

    .line 307
    move v15, v7

    .line 309
    move/from16 v25, v15

    .line 311
    .local v25, "length":F
    const/16 v37, 0x1

    aget v37, v24, v37

    sub-float v37, v37, v25

    const/16 v38, 0x1

    aget v38, v31, v38

    sub-float v38, v38, v25

    invoke-static/range {v37 .. v38}, Ljava/lang/Math;->min(FF)F

    move-result v34

    .line 312
    .local v34, "top":F
    const/16 v37, 0x0

    aget v37, v24, v37

    sub-float v37, v37, v25

    const/16 v38, 0x0

    aget v38, v31, v38

    sub-float v38, v38, v25

    invoke-static/range {v37 .. v38}, Ljava/lang/Math;->min(FF)F

    move-result v23

    .line 314
    .local v23, "left":F
    const/16 v37, 0x1

    aget v37, v24, v37

    add-float v37, v37, v25

    const/16 v38, 0x1

    aget v38, v31, v38

    add-float v38, v38, v25

    invoke-static/range {v37 .. v38}, Ljava/lang/Math;->max(FF)F

    move-result v8

    .line 315
    .local v8, "bottom":F
    const/16 v37, 0x0

    aget v37, v24, v37

    add-float v37, v37, v25

    const/16 v38, 0x0

    aget v38, v31, v38

    add-float v38, v38, v25

    invoke-static/range {v37 .. v38}, Ljava/lang/Math;->max(FF)F

    move-result v30

    .line 317
    .local v30, "right":F
    new-instance v28, Landroid/filterfw/geometry/Rectangle;

    sub-float v37, v30, v23

    sub-float v38, v8, v34

    move-object/from16 v0, v28

    move/from16 v1, v23

    move/from16 v2, v34

    move/from16 v3, v37

    move/from16 v4, v38

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/filterfw/geometry/Rectangle;-><init>(FFFF)V

    .line 318
    .local v28, "rect":Landroid/filterfw/geometry/Rectangle;
    const/16 v37, 0x0

    const/16 v38, 0x0

    move-object/from16 v0, v28

    move/from16 v1, v37

    move/from16 v2, v38

    invoke-virtual {v0, v1, v2}, Landroid/filterfw/geometry/Rectangle;->translated(FF)Landroid/filterfw/geometry/Quad;

    move-result-object v29

    .line 320
    .local v29, "region":Landroid/filterfw/geometry/Quad;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mBigEyesProgram:Landroid/filterfw/core/ShaderProgram;

    move-object/from16 v37, v0

    const-string v38, "left_eye"

    move-object/from16 v0, v37

    move-object/from16 v1, v38

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 321
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mBigEyesProgram:Landroid/filterfw/core/ShaderProgram;

    move-object/from16 v37, v0

    const-string v38, "right_eye"

    move-object/from16 v0, v37

    move-object/from16 v1, v38

    move-object/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 322
    const/16 v37, 0x2

    move/from16 v0, v37

    new-array v0, v0, [F

    move-object/from16 v32, v0

    const/16 v37, 0x0

    const/16 v38, 0x0

    aget v38, v6, v38

    div-float v38, v38, v15

    aput v38, v32, v37

    const/16 v37, 0x1

    const/16 v38, 0x1

    aget v38, v6, v38

    div-float v38, v38, v15

    aput v38, v32, v37

    .line 323
    .local v32, "scales":[F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mBigEyesProgram:Landroid/filterfw/core/ShaderProgram;

    move-object/from16 v37, v0

    const-string v38, "scale"

    move-object/from16 v0, v37

    move-object/from16 v1, v38

    move-object/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 325
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mBigEyesProgram:Landroid/filterfw/core/ShaderProgram;

    move-object/from16 v37, v0

    move-object/from16 v0, v37

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Landroid/filterfw/core/ShaderProgram;->setSourceRegion(Landroid/filterfw/geometry/Quad;)V

    .line 326
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mBigEyesProgram:Landroid/filterfw/core/ShaderProgram;

    move-object/from16 v37, v0

    move-object/from16 v0, v37

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Landroid/filterfw/core/ShaderProgram;->setTargetRegion(Landroid/filterfw/geometry/Quad;)V

    .line 329
    const/16 v37, 0x2

    move/from16 v0, v37

    new-array v0, v0, [Landroid/filterfw/core/Frame;

    move-object/from16 v22, v0

    const/16 v37, 0x0

    aput-object v33, v22, v37

    const/16 v37, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mTableFrame:Landroid/filterfw/core/Frame;

    move-object/from16 v38, v0

    aput-object v38, v22, v37

    .line 330
    .local v22, "inputs":[Landroid/filterfw/core/Frame;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mBigEyesProgram:Landroid/filterfw/core/ShaderProgram;

    move-object/from16 v37, v0

    move-object/from16 v0, v37

    move-object/from16 v1, v22

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Landroid/filterfw/core/ShaderProgram;->process([Landroid/filterfw/core/Frame;Landroid/filterfw/core/Frame;)V

    .line 283
    .end local v8    # "bottom":F
    .end local v23    # "left":F
    .end local v25    # "length":F
    .end local v30    # "right":F
    .end local v32    # "scales":[F
    .end local v34    # "top":F
    :goto_3
    add-int/lit8 v19, v19, 0x1

    goto/16 :goto_1

    .line 266
    .end local v6    # "aspectRatio":[F
    .end local v7    # "baseline":F
    .end local v9    # "buf1":Landroid/filterfw/core/Frame;
    .end local v10    # "buf2":Landroid/filterfw/core/Frame;
    .end local v11    # "center":[F
    .end local v12    # "dx":F
    .end local v13    # "dy":F
    .end local v14    # "face_count":I
    .end local v15    # "face_size":F
    .end local v16    # "faces":Lcom/google/android/filterpacks/facedetect/FaceMeta;
    .end local v17    # "facesFrame":Landroid/filterfw/core/Frame;
    .end local v19    # "i":I
    .end local v22    # "inputs":[Landroid/filterfw/core/Frame;
    .end local v24    # "leftEye":[F
    .end local v26    # "mouth":[F
    .end local v27    # "output":Landroid/filterfw/core/Frame;
    .end local v28    # "rect":Landroid/filterfw/geometry/Rectangle;
    .end local v29    # "region":Landroid/filterfw/geometry/Quad;
    .end local v31    # "rightEye":[F
    .end local v33    # "source":Landroid/filterfw/core/Frame;
    .end local v35    # "weight":[F
    :cond_2
    const/16 v37, 0x2

    move/from16 v0, v37

    new-array v6, v0, [F

    const/16 v37, 0x0

    move/from16 v0, v36

    int-to-float v0, v0

    move/from16 v38, v0

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v39, v0

    div-float v38, v38, v39

    aput v38, v6, v37

    const/16 v37, 0x1

    const/high16 v38, 0x3f800000    # 1.0f

    aput v38, v6, v37

    .restart local v6    # "aspectRatio":[F
    goto/16 :goto_0

    .line 291
    .restart local v9    # "buf1":Landroid/filterfw/core/Frame;
    .restart local v10    # "buf2":Landroid/filterfw/core/Frame;
    .restart local v11    # "center":[F
    .restart local v14    # "face_count":I
    .restart local v16    # "faces":Lcom/google/android/filterpacks/facedetect/FaceMeta;
    .restart local v17    # "facesFrame":Landroid/filterfw/core/Frame;
    .restart local v19    # "i":I
    .restart local v27    # "output":Landroid/filterfw/core/Frame;
    .restart local v33    # "source":Landroid/filterfw/core/Frame;
    .restart local v35    # "weight":[F
    :cond_3
    move-object/from16 v33, v10

    .line 292
    move-object/from16 v27, v9

    .line 293
    move-object/from16 v0, v33

    invoke-virtual {v9, v0}, Landroid/filterfw/core/Frame;->setDataFromFrame(Landroid/filterfw/core/Frame;)V

    goto/16 :goto_2

    .line 333
    .restart local v7    # "baseline":F
    .restart local v12    # "dx":F
    .restart local v13    # "dy":F
    .restart local v15    # "face_size":F
    .restart local v24    # "leftEye":[F
    .restart local v26    # "mouth":[F
    .restart local v31    # "rightEye":[F
    :cond_4
    const/16 v37, 0x0

    const/high16 v38, 0x3f800000    # 1.0f

    aput v38, v35, v37

    .line 334
    const/16 v37, 0x1

    const/high16 v38, 0x3f800000    # 1.0f

    aput v38, v35, v37

    .line 336
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mCurrentEffect:I

    move/from16 v37, v0

    packed-switch v37, :pswitch_data_0

    .line 371
    :pswitch_0
    new-instance v37, Ljava/lang/RuntimeException;

    new-instance v38, Ljava/lang/StringBuilder;

    invoke-direct/range {v38 .. v38}, Ljava/lang/StringBuilder;-><init>()V

    const-string v39, "Undefined effect: "

    invoke-virtual/range {v38 .. v39}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v38

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mCurrentEffect:I

    move/from16 v39, v0

    invoke-virtual/range {v38 .. v39}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v38

    invoke-virtual/range {v38 .. v38}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v38

    invoke-direct/range {v37 .. v38}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v37

    .line 338
    :pswitch_1
    const v37, 0x3f4ccccd    # 0.8f

    mul-float v15, v37, v7

    .line 340
    const/16 v37, 0x0

    const/high16 v38, 0x3f000000    # 0.5f

    const/16 v39, 0x0

    aget v39, v24, v39

    mul-float v38, v38, v39

    const/high16 v39, 0x3f000000    # 0.5f

    const/16 v40, 0x0

    aget v40, v31, v40

    mul-float v39, v39, v40

    add-float v38, v38, v39

    const v39, 0x38d1b717    # 1.0E-4f

    const/16 v40, 0x0

    aget v40, v26, v40

    mul-float v39, v39, v40

    add-float v38, v38, v39

    aput v38, v11, v37

    .line 341
    const/16 v37, 0x1

    const/high16 v38, 0x3f000000    # 0.5f

    const/16 v39, 0x1

    aget v39, v24, v39

    mul-float v38, v38, v39

    const/high16 v39, 0x3f000000    # 0.5f

    const/16 v40, 0x1

    aget v40, v31, v40

    mul-float v39, v39, v40

    add-float v38, v38, v39

    const v39, 0x38d1b717    # 1.0E-4f

    const/16 v40, 0x1

    aget v40, v26, v40

    mul-float v39, v39, v40

    add-float v38, v38, v39

    aput v38, v11, v37

    .line 343
    const/16 v37, 0x0

    const-wide v38, 0x3ff3333333333333L    # 1.2

    invoke-static/range {v38 .. v39}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v38

    move-wide/from16 v0, v38

    double-to-float v0, v0

    move/from16 v38, v0

    aput v38, v35, v37

    .line 344
    const/16 v37, 0x1

    const-wide v38, 0x3ff3333333333333L    # 1.2

    invoke-static/range {v38 .. v39}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v38

    move-wide/from16 v0, v38

    double-to-float v0, v0

    move/from16 v38, v0

    aput v38, v35, v37

    .line 374
    :goto_4
    const/16 v37, 0x0

    aget v38, v35, v37

    div-float v38, v38, v15

    aput v38, v35, v37

    .line 375
    const/16 v37, 0x1

    aget v38, v35, v37

    div-float v38, v38, v15

    aput v38, v35, v37

    .line 376
    const/16 v37, 0x0

    aget v38, v35, v37

    const/16 v39, 0x0

    aget v39, v6, v39

    mul-float v38, v38, v39

    aput v38, v35, v37

    .line 377
    const/16 v37, 0x1

    aget v38, v35, v37

    const/16 v39, 0x1

    aget v39, v6, v39

    mul-float v38, v38, v39

    aput v38, v35, v37

    .line 379
    new-instance v28, Landroid/filterfw/geometry/Rectangle;

    const/high16 v37, -0x40800000    # -1.0f

    const/16 v38, 0x0

    aget v38, v35, v38

    div-float v37, v37, v38

    const/high16 v38, -0x40800000    # -1.0f

    const/16 v39, 0x1

    aget v39, v35, v39

    div-float v38, v38, v39

    const/high16 v39, 0x40000000    # 2.0f

    const/16 v40, 0x0

    aget v40, v35, v40

    div-float v39, v39, v40

    const/high16 v40, 0x40000000    # 2.0f

    const/16 v41, 0x1

    aget v41, v35, v41

    div-float v40, v40, v41

    move-object/from16 v0, v28

    move/from16 v1, v37

    move/from16 v2, v38

    move/from16 v3, v39

    move/from16 v4, v40

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/filterfw/geometry/Rectangle;-><init>(FFFF)V

    .line 382
    .restart local v28    # "rect":Landroid/filterfw/geometry/Rectangle;
    const/16 v37, 0x0

    aget v37, v11, v37

    const/16 v38, 0x1

    aget v38, v11, v38

    move-object/from16 v0, v28

    move/from16 v1, v37

    move/from16 v2, v38

    invoke-virtual {v0, v1, v2}, Landroid/filterfw/geometry/Rectangle;->translated(FF)Landroid/filterfw/geometry/Quad;

    move-result-object v29

    .line 385
    .restart local v29    # "region":Landroid/filterfw/geometry/Quad;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mGoofyProgram:Landroid/filterfw/core/ShaderProgram;

    move-object/from16 v37, v0

    const-string v38, "center"

    move-object/from16 v0, v37

    move-object/from16 v1, v38

    invoke-virtual {v0, v1, v11}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 386
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mGoofyProgram:Landroid/filterfw/core/ShaderProgram;

    move-object/from16 v37, v0

    const-string v38, "weight"

    move-object/from16 v0, v37

    move-object/from16 v1, v38

    move-object/from16 v2, v35

    invoke-virtual {v0, v1, v2}, Landroid/filterfw/core/ShaderProgram;->setHostValue(Ljava/lang/String;Ljava/lang/Object;)V

    .line 388
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mGoofyProgram:Landroid/filterfw/core/ShaderProgram;

    move-object/from16 v37, v0

    move-object/from16 v0, v37

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Landroid/filterfw/core/ShaderProgram;->setSourceRegion(Landroid/filterfw/geometry/Quad;)V

    .line 389
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mGoofyProgram:Landroid/filterfw/core/ShaderProgram;

    move-object/from16 v37, v0

    move-object/from16 v0, v37

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Landroid/filterfw/core/ShaderProgram;->setTargetRegion(Landroid/filterfw/geometry/Quad;)V

    .line 392
    const/16 v37, 0x2

    move/from16 v0, v37

    new-array v0, v0, [Landroid/filterfw/core/Frame;

    move-object/from16 v22, v0

    const/16 v37, 0x0

    aput-object v33, v22, v37

    const/16 v37, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mTableFrame:Landroid/filterfw/core/Frame;

    move-object/from16 v38, v0

    aput-object v38, v22, v37

    .line 393
    .restart local v22    # "inputs":[Landroid/filterfw/core/Frame;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->mGoofyProgram:Landroid/filterfw/core/ShaderProgram;

    move-object/from16 v37, v0

    move-object/from16 v0, v37

    move-object/from16 v1, v22

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Landroid/filterfw/core/ShaderProgram;->process([Landroid/filterfw/core/Frame;Landroid/filterfw/core/Frame;)V

    goto/16 :goto_3

    .line 348
    .end local v22    # "inputs":[Landroid/filterfw/core/Frame;
    .end local v28    # "rect":Landroid/filterfw/geometry/Rectangle;
    .end local v29    # "region":Landroid/filterfw/geometry/Quad;
    :pswitch_2
    const/high16 v37, 0x3f000000    # 0.5f

    mul-float v15, v37, v7

    .line 349
    const/16 v37, 0x0

    const/16 v38, 0x0

    aget v38, v26, v38

    aput v38, v11, v37

    .line 350
    const/16 v37, 0x1

    const/16 v38, 0x1

    aget v38, v26, v38

    aput v38, v11, v37

    goto/16 :goto_4

    .line 354
    :pswitch_3
    const/high16 v37, 0x40200000    # 2.5f

    mul-float v15, v37, v7

    .line 355
    const/16 v37, 0x0

    const/16 v38, 0x0

    aget v38, v26, v38

    aput v38, v11, v37

    .line 356
    const/16 v37, 0x1

    const/16 v38, 0x1

    aget v38, v26, v38

    aput v38, v11, v37

    goto/16 :goto_4

    .line 360
    :pswitch_4
    const v37, 0x3f8ccccd    # 1.1f

    mul-float v15, v37, v7

    .line 361
    const/16 v37, 0x0

    const/high16 v38, 0x3e800000    # 0.25f

    const/16 v39, 0x0

    aget v39, v24, v39

    mul-float v38, v38, v39

    const/high16 v39, 0x3e800000    # 0.25f

    const/16 v40, 0x0

    aget v40, v31, v40

    mul-float v39, v39, v40

    add-float v38, v38, v39

    const/high16 v39, 0x3f000000    # 0.5f

    const/16 v40, 0x0

    aget v40, v26, v40

    mul-float v39, v39, v40

    add-float v38, v38, v39

    aput v38, v11, v37

    .line 362
    const/16 v37, 0x1

    const/high16 v38, 0x3e800000    # 0.25f

    const/16 v39, 0x1

    aget v39, v24, v39

    mul-float v38, v38, v39

    const/high16 v39, 0x3e800000    # 0.25f

    const/16 v40, 0x1

    aget v40, v31, v40

    mul-float v39, v39, v40

    add-float v38, v38, v39

    const/high16 v39, 0x3f000000    # 0.5f

    const/16 v40, 0x1

    aget v40, v26, v40

    mul-float v39, v39, v40

    add-float v38, v38, v39

    aput v38, v11, v37

    goto/16 :goto_4

    .line 366
    :pswitch_5
    const v37, 0x3f4ccccd    # 0.8f

    mul-float v15, v37, v7

    .line 367
    const/16 v37, 0x0

    const/high16 v38, 0x3e800000    # 0.25f

    const/16 v39, 0x0

    aget v39, v24, v39

    mul-float v38, v38, v39

    const/high16 v39, 0x3e800000    # 0.25f

    const/16 v40, 0x0

    aget v40, v31, v40

    mul-float v39, v39, v40

    add-float v38, v38, v39

    const/high16 v39, 0x3f000000    # 0.5f

    const/16 v40, 0x0

    aget v40, v26, v40

    mul-float v39, v39, v40

    add-float v38, v38, v39

    aput v38, v11, v37

    .line 368
    const/16 v37, 0x1

    const/high16 v38, 0x3e800000    # 0.25f

    const/16 v39, 0x1

    aget v39, v24, v39

    mul-float v38, v38, v39

    const/high16 v39, 0x3e800000    # 0.25f

    const/16 v40, 0x1

    aget v40, v31, v40

    mul-float v39, v39, v40

    add-float v38, v38, v39

    const/high16 v39, 0x3f000000    # 0.5f

    const/16 v40, 0x1

    aget v40, v26, v40

    mul-float v39, v39, v40

    add-float v38, v38, v39

    aput v38, v11, v37

    goto/16 :goto_4

    .line 396
    .end local v7    # "baseline":F
    .end local v12    # "dx":F
    .end local v13    # "dy":F
    .end local v15    # "face_size":F
    .end local v24    # "leftEye":[F
    .end local v26    # "mouth":[F
    .end local v31    # "rightEye":[F
    :cond_5
    const-string v37, "outimage"

    move-object/from16 v0, p0

    move-object/from16 v1, v37

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->pushOutput(Ljava/lang/String;Landroid/filterfw/core/Frame;)V

    .line 397
    invoke-virtual {v9}, Landroid/filterfw/core/Frame;->release()Landroid/filterfw/core/Frame;

    .line 398
    if-eqz v10, :cond_6

    .line 399
    invoke-virtual {v10}, Landroid/filterfw/core/Frame;->release()Landroid/filterfw/core/Frame;

    .line 403
    .end local v9    # "buf1":Landroid/filterfw/core/Frame;
    .end local v10    # "buf2":Landroid/filterfw/core/Frame;
    .end local v19    # "i":I
    .end local v27    # "output":Landroid/filterfw/core/Frame;
    .end local v33    # "source":Landroid/filterfw/core/Frame;
    :cond_6
    :goto_5
    return-void

    .line 401
    :cond_7
    const-string v37, "outimage"

    move-object/from16 v0, p0

    move-object/from16 v1, v37

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->pushOutput(Ljava/lang/String;Landroid/filterfw/core/Frame;)V

    goto :goto_5

    .line 336
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_5
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_1
    .end packed-switch
.end method

.method public setupPorts()V
    .locals 4

    .prologue
    const/4 v2, 0x3

    .line 154
    invoke-static {v2, v2}, Landroid/filterfw/format/ImageFormat;->create(II)Landroid/filterfw/core/MutableFrameFormat;

    move-result-object v1

    .line 156
    .local v1, "imageFormat":Landroid/filterfw/core/FrameFormat;
    const-class v2, Lcom/google/android/filterpacks/facedetect/FaceMeta;

    const/4 v3, 0x2

    invoke-static {v2, v3}, Landroid/filterfw/format/ObjectFormat;->fromClass(Ljava/lang/Class;I)Landroid/filterfw/core/MutableFrameFormat;

    move-result-object v0

    .line 160
    .local v0, "facesFormat":Landroid/filterfw/core/FrameFormat;
    const-string v2, "image"

    invoke-virtual {p0, v2, v1}, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->addMaskedInputPort(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)V

    .line 161
    const-string v2, "faces"

    invoke-virtual {p0, v2, v0}, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->addMaskedInputPort(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)V

    .line 162
    const-string v2, "outimage"

    const-string v3, "image"

    invoke-virtual {p0, v2, v3}, Lcom/google/android/filterpacks/facedetect/GoofyRenderFilter;->addOutputBasedOnInput(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    return-void
.end method
