.class public Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter;
.super Landroid/filterfw/core/Filter;
.source "FaceMetaSmoothFilter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;
    }
.end annotation


# instance fields
.field private mLastPositions:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;",
            ">;"
        }
    .end annotation
.end field

.field final mMaximumGap:J

.field final mMaximumPredictionGap:J

.field final mSmoothingRate:F


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 4
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    const-wide/16 v2, 0x12c

    .line 140
    invoke-direct {p0, p1}, Landroid/filterfw/core/Filter;-><init>(Ljava/lang/String;)V

    .line 40
    const/high16 v0, 0x3f000000    # 0.5f

    iput v0, p0, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter;->mSmoothingRate:F

    .line 41
    iput-wide v2, p0, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter;->mMaximumGap:J

    .line 42
    iput-wide v2, p0, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter;->mMaximumPredictionGap:J

    .line 64
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter;->mLastPositions:Ljava/util/HashMap;

    .line 141
    return-void
.end method

.method private getCurrentPositions(Lcom/google/android/filterpacks/facedetect/FaceMeta;)Ljava/util/Vector;
    .locals 20
    .param p1, "face"    # Lcom/google/android/filterpacks/facedetect/FaceMeta;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/filterpacks/facedetect/FaceMeta;",
            ")",
            "Ljava/util/Vector",
            "<",
            "Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;",
            ">;"
        }
    .end annotation

    .prologue
    .line 66
    new-instance v11, Ljava/util/Vector;

    invoke-direct {v11}, Ljava/util/Vector;-><init>()V

    .line 67
    .local v11, "retFaces":Ljava/util/Vector;, "Ljava/util/Vector<Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;>;"
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->count()I

    move-result v8

    .line 68
    .local v8, "num_face":I
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v12

    .line 69
    .local v12, "t":J
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    if-ge v6, v8, :cond_1

    .line 70
    new-instance v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;

    move-object/from16 v0, p0

    invoke-direct {v9, v0}, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;-><init>(Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter;)V

    .line 71
    .local v9, "p":Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;
    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getLeftEyeX(I)F

    move-result v15

    iput v15, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->left_eye_x:F

    .line 72
    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getLeftEyeY(I)F

    move-result v15

    iput v15, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->left_eye_y:F

    .line 73
    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getRightEyeX(I)F

    move-result v15

    iput v15, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->right_eye_x:F

    .line 74
    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getRightEyeY(I)F

    move-result v15

    iput v15, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->right_eye_y:F

    .line 75
    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getMouthX(I)F

    move-result v15

    iput v15, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->mouth_x:F

    .line 76
    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getMouthY(I)F

    move-result v15

    iput v15, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->mouth_y:F

    .line 77
    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getFaceX0(I)F

    move-result v15

    iput v15, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->face_x0:F

    .line 78
    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getFaceY0(I)F

    move-result v15

    iput v15, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->face_y0:F

    .line 79
    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getFaceX1(I)F

    move-result v15

    iput v15, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->face_x1:F

    .line 80
    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getFaceY1(I)F

    move-result v15

    iput v15, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->face_y1:F

    .line 81
    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getUpperLipX(I)F

    move-result v15

    iput v15, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->upper_lip_x:F

    .line 82
    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getUpperLipY(I)F

    move-result v15

    iput v15, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->upper_lip_y:F

    .line 83
    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getLowerLipX(I)F

    move-result v15

    iput v15, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->lower_lip_x:F

    .line 84
    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getLowerLipY(I)F

    move-result v15

    iput v15, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->lower_lip_y:F

    .line 85
    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getId(I)I

    move-result v15

    iput v15, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->id:I

    .line 86
    iput-wide v12, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->last_seen:J

    .line 87
    const/4 v15, 0x0

    iput v15, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->speed_x:F

    .line 88
    const/4 v15, 0x0

    iput v15, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->speed_y:F

    .line 89
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter;->mLastPositions:Ljava/util/HashMap;

    iget v0, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->id:I

    move/from16 v16, v0

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_0

    .line 90
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter;->mLastPositions:Ljava/util/HashMap;

    iget v0, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->id:I

    move/from16 v16, v0

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;

    .line 91
    .local v14, "v":Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;
    iget v15, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->left_eye_x:F

    iget v0, v14, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->left_eye_x:F

    move/from16 v16, v0

    sub-float v15, v15, v16

    iget v0, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->right_eye_x:F

    move/from16 v16, v0

    iget v0, v14, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->right_eye_x:F

    move/from16 v17, v0

    sub-float v16, v16, v17

    add-float v15, v15, v16

    const/high16 v16, 0x40000000    # 2.0f

    div-float v3, v15, v16

    .line 93
    .local v3, "dx":F
    iget v15, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->left_eye_y:F

    iget v0, v14, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->left_eye_y:F

    move/from16 v16, v0

    sub-float v15, v15, v16

    iget v0, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->right_eye_y:F

    move/from16 v16, v0

    iget v0, v14, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->right_eye_y:F

    move/from16 v17, v0

    sub-float v16, v16, v17

    add-float v15, v15, v16

    const/high16 v16, 0x40000000    # 2.0f

    div-float v4, v15, v16

    .line 95
    .local v4, "dy":F
    iget-wide v0, v14, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->last_seen:J

    move-wide/from16 v16, v0

    sub-long v16, v12, v16

    move-wide/from16 v0, v16

    long-to-float v15, v0

    const/high16 v16, 0x447a0000    # 1000.0f

    div-float v2, v15, v16

    .line 96
    .local v2, "dt":F
    iget v15, v14, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->speed_x:F

    div-float v16, v3, v2

    iget v0, v14, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->speed_x:F

    move/from16 v17, v0

    sub-float v16, v16, v17

    const/high16 v17, 0x3f000000    # 0.5f

    mul-float v16, v16, v17

    add-float v15, v15, v16

    iput v15, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->speed_x:F

    .line 97
    iget v15, v14, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->speed_y:F

    div-float v16, v4, v2

    iget v0, v14, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->speed_y:F

    move/from16 v17, v0

    sub-float v16, v16, v17

    const/high16 v17, 0x3f000000    # 0.5f

    mul-float v16, v16, v17

    add-float v15, v15, v16

    iput v15, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->speed_y:F

    .line 98
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter;->mLastPositions:Ljava/util/HashMap;

    iget v0, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->id:I

    move/from16 v16, v0

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v15, v0, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    .end local v2    # "dt":F
    .end local v3    # "dx":F
    .end local v4    # "dy":F
    .end local v14    # "v":Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;
    :goto_1
    invoke-virtual {v11, v9}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 69
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_0

    .line 101
    :cond_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter;->mLastPositions:Ljava/util/HashMap;

    iget v0, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->id:I

    move/from16 v16, v0

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v15, v0, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 105
    .end local v9    # "p":Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;
    :cond_1
    new-instance v10, Ljava/util/Vector;

    invoke-direct {v10}, Ljava/util/Vector;-><init>()V

    .line 106
    .local v10, "removeList":Ljava/util/Vector;, "Ljava/util/Vector<Ljava/lang/Integer;>;"
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter;->mLastPositions:Ljava/util/HashMap;

    invoke-virtual {v15}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v15

    invoke-interface {v15}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;

    .line 108
    .local v5, "f":Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;
    iget-wide v0, v5, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->last_seen:J

    move-wide/from16 v16, v0

    const-wide/16 v18, 0x12c

    sub-long v18, v12, v18

    cmp-long v15, v16, v18

    if-gez v15, :cond_3

    iget v15, v5, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->id:I

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    invoke-virtual {v10, v15}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 111
    :cond_3
    iget-wide v0, v5, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->last_seen:J

    move-wide/from16 v16, v0

    cmp-long v15, v16, v12

    if-eqz v15, :cond_2

    iget-wide v0, v5, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->last_seen:J

    move-wide/from16 v16, v0

    const-wide/16 v18, 0x12c

    sub-long v18, v12, v18

    cmp-long v15, v16, v18

    if-lez v15, :cond_2

    .line 112
    invoke-virtual {v5}, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->clone()Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;

    move-result-object v9

    .line 113
    .restart local v9    # "p":Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;
    iget v15, v5, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->speed_x:F

    const/high16 v16, 0x447a0000    # 1000.0f

    div-float v15, v15, v16

    iget-wide v0, v5, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->last_seen:J

    move-wide/from16 v16, v0

    sub-long v16, v12, v16

    move-wide/from16 v0, v16

    long-to-float v0, v0

    move/from16 v16, v0

    mul-float v3, v15, v16

    .line 114
    .restart local v3    # "dx":F
    iget v15, v5, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->speed_x:F

    const/high16 v16, 0x447a0000    # 1000.0f

    div-float v15, v15, v16

    iget-wide v0, v5, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->last_seen:J

    move-wide/from16 v16, v0

    sub-long v16, v12, v16

    move-wide/from16 v0, v16

    long-to-float v0, v0

    move/from16 v16, v0

    mul-float v4, v15, v16

    .line 115
    .restart local v4    # "dy":F
    iget v15, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->left_eye_x:F

    add-float/2addr v15, v3

    iput v15, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->left_eye_x:F

    .line 116
    iget v15, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->left_eye_y:F

    add-float/2addr v15, v4

    iput v15, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->left_eye_y:F

    .line 117
    iget v15, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->right_eye_x:F

    add-float/2addr v15, v3

    iput v15, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->right_eye_x:F

    .line 118
    iget v15, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->right_eye_y:F

    add-float/2addr v15, v4

    iput v15, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->right_eye_y:F

    .line 119
    iget v15, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->mouth_x:F

    add-float/2addr v15, v3

    iput v15, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->mouth_x:F

    .line 120
    iget v15, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->mouth_y:F

    add-float/2addr v15, v4

    iput v15, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->mouth_y:F

    .line 121
    iget v15, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->face_x0:F

    add-float/2addr v15, v3

    iput v15, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->face_x0:F

    .line 122
    iget v15, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->face_y0:F

    add-float/2addr v15, v4

    iput v15, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->face_y0:F

    .line 123
    iget v15, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->face_x1:F

    add-float/2addr v15, v3

    iput v15, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->face_x1:F

    .line 124
    iget v15, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->face_y1:F

    add-float/2addr v15, v4

    iput v15, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->face_y1:F

    .line 125
    iget v15, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->upper_lip_x:F

    add-float/2addr v15, v3

    iput v15, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->upper_lip_x:F

    .line 126
    iget v15, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->upper_lip_y:F

    add-float/2addr v15, v4

    iput v15, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->upper_lip_y:F

    .line 127
    iget v15, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->lower_lip_x:F

    add-float/2addr v15, v3

    iput v15, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->lower_lip_x:F

    .line 128
    iget v15, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->lower_lip_y:F

    add-float/2addr v15, v4

    iput v15, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->lower_lip_y:F

    .line 129
    iget v15, v5, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->id:I

    iput v15, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->id:I

    .line 130
    invoke-virtual {v11, v9}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 134
    .end local v3    # "dx":F
    .end local v4    # "dy":F
    .end local v5    # "f":Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;
    .end local v9    # "p":Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;
    :cond_4
    invoke-virtual {v10}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .end local v6    # "i":I
    :goto_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_5

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    .line 135
    .local v6, "i":Ljava/lang/Integer;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter;->mLastPositions:Ljava/util/HashMap;

    invoke-virtual {v15, v6}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    .line 137
    .end local v6    # "i":Ljava/lang/Integer;
    :cond_5
    return-object v11
.end method


# virtual methods
.method public process(Landroid/filterfw/core/FilterContext;)V
    .locals 12
    .param p1, "context"    # Landroid/filterfw/core/FilterContext;

    .prologue
    .line 155
    invoke-virtual {p1}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    move-result-object v2

    .line 158
    .local v2, "frameManager":Landroid/filterfw/core/FrameManager;
    const-string v9, "faces"

    invoke-virtual {p0, v9}, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter;->pullInput(Ljava/lang/String;)Landroid/filterfw/core/Frame;

    move-result-object v1

    .line 161
    .local v1, "facesFrame":Landroid/filterfw/core/Frame;
    invoke-virtual {v1}, Landroid/filterfw/core/Frame;->getObjectValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/filterpacks/facedetect/FaceMeta;

    .line 162
    .local v0, "face":Lcom/google/android/filterpacks/facedetect/FaceMeta;
    invoke-direct {p0, v0}, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter;->getCurrentPositions(Lcom/google/android/filterpacks/facedetect/FaceMeta;)Ljava/util/Vector;

    move-result-object v8

    .line 163
    .local v8, "vnewfaces":Ljava/util/Vector;, "Ljava/util/Vector<Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;>;"
    invoke-virtual {v8}, Ljava/util/Vector;->size()I

    move-result v9

    new-array v4, v9, [Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;

    .line 164
    .local v4, "newfaces":[Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;
    invoke-virtual {v8, v4}, Ljava/util/Vector;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 165
    const-class v9, Lcom/google/android/filterpacks/facedetect/FaceMeta;

    array-length v10, v4

    const/4 v11, 0x2

    invoke-static {v9, v10, v11}, Landroid/filterfw/format/ObjectFormat;->fromClass(Ljava/lang/Class;II)Landroid/filterfw/core/MutableFrameFormat;

    move-result-object v7

    .line 168
    .local v7, "outputFormat":Landroid/filterfw/core/FrameFormat;
    invoke-virtual {v2, v7}, Landroid/filterfw/core/FrameManager;->newFrame(Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/Frame;

    move-result-object v6

    .line 169
    .local v6, "output":Landroid/filterfw/core/Frame;
    invoke-virtual {v6}, Landroid/filterfw/core/Frame;->getObjectValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/filterpacks/facedetect/FaceMeta;

    .line 170
    .local v5, "outfaces":Lcom/google/android/filterpacks/facedetect/FaceMeta;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-virtual {v5}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->count()I

    move-result v9

    if-ge v3, v9, :cond_0

    .line 171
    aget-object v9, v4, v3

    iget v9, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->id:I

    invoke-virtual {v5, v3, v9}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->setId(II)V

    .line 172
    aget-object v9, v4, v3

    iget v9, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->face_x0:F

    invoke-virtual {v5, v3, v9}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->setFaceX0(IF)V

    .line 173
    aget-object v9, v4, v3

    iget v9, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->face_y0:F

    invoke-virtual {v5, v3, v9}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->setFaceY0(IF)V

    .line 174
    aget-object v9, v4, v3

    iget v9, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->face_x1:F

    invoke-virtual {v5, v3, v9}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->setFaceX1(IF)V

    .line 175
    aget-object v9, v4, v3

    iget v9, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->face_y1:F

    invoke-virtual {v5, v3, v9}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->setFaceY1(IF)V

    .line 176
    aget-object v9, v4, v3

    iget v9, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->left_eye_x:F

    invoke-virtual {v5, v3, v9}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->setLeftEyeX(IF)V

    .line 177
    aget-object v9, v4, v3

    iget v9, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->left_eye_y:F

    invoke-virtual {v5, v3, v9}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->setLeftEyeY(IF)V

    .line 178
    aget-object v9, v4, v3

    iget v9, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->right_eye_x:F

    invoke-virtual {v5, v3, v9}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->setRightEyeX(IF)V

    .line 179
    aget-object v9, v4, v3

    iget v9, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->right_eye_y:F

    invoke-virtual {v5, v3, v9}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->setRightEyeY(IF)V

    .line 180
    aget-object v9, v4, v3

    iget v9, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->mouth_x:F

    invoke-virtual {v5, v3, v9}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->setMouthX(IF)V

    .line 181
    aget-object v9, v4, v3

    iget v9, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->mouth_y:F

    invoke-virtual {v5, v3, v9}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->setMouthY(IF)V

    .line 182
    aget-object v9, v4, v3

    iget v9, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->upper_lip_x:F

    invoke-virtual {v5, v3, v9}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->setUpperLipX(IF)V

    .line 183
    aget-object v9, v4, v3

    iget v9, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->upper_lip_y:F

    invoke-virtual {v5, v3, v9}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->setUpperLipY(IF)V

    .line 184
    aget-object v9, v4, v3

    iget v9, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->lower_lip_x:F

    invoke-virtual {v5, v3, v9}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->setLowerLipX(IF)V

    .line 185
    aget-object v9, v4, v3

    iget v9, v9, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter$FacePos;->lower_lip_y:F

    invoke-virtual {v5, v3, v9}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->setLowerLipY(IF)V

    .line 170
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 188
    :cond_0
    const-string v9, "faces"

    invoke-virtual {p0, v9, v6}, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter;->pushOutput(Ljava/lang/String;Landroid/filterfw/core/Frame;)V

    .line 191
    invoke-virtual {v6}, Landroid/filterfw/core/Frame;->release()Landroid/filterfw/core/Frame;

    .line 192
    return-void
.end method

.method public setupPorts()V
    .locals 3

    .prologue
    .line 146
    const-class v1, Lcom/google/android/filterpacks/facedetect/FaceMeta;

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/filterfw/format/ObjectFormat;->fromClass(Ljava/lang/Class;I)Landroid/filterfw/core/MutableFrameFormat;

    move-result-object v0

    .line 149
    .local v0, "facesFormat":Landroid/filterfw/core/FrameFormat;
    const-string v1, "faces"

    invoke-virtual {p0, v1, v0}, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter;->addMaskedInputPort(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)V

    .line 150
    const-string v1, "faces"

    invoke-virtual {p0, v1, v0}, Lcom/google/android/filterpacks/facedetect/FaceMetaSmoothFilter;->addOutputPort(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)V

    .line 151
    return-void
.end method
