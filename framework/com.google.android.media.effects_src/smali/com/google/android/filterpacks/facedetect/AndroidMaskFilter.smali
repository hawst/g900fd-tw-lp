.class public Lcom/google/android/filterpacks/facedetect/AndroidMaskFilter;
.super Landroid/filterfw/core/Filter;
.source "AndroidMaskFilter.java"


# instance fields
.field private mAndroidCenterX:F
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        name = "EyeCenterX"
    .end annotation
.end field

.field private mAndroidCenterY:F
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        name = "EyeCenterY"
    .end annotation
.end field

.field private mAndroidEyeDist:F
    .annotation runtime Landroid/filterfw/core/GenerateFieldPort;
        name = "EyeDistance"
    .end annotation
.end field

.field private mOverlayProgram:Landroid/filterfw/core/ShaderProgram;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 53
    invoke-direct {p0, p1}, Landroid/filterfw/core/Filter;-><init>(Ljava/lang/String;)V

    .line 54
    return-void
.end method

.method private rotate2D([F[FF)[F
    .locals 8
    .param p1, "x"    # [F
    .param p2, "center"    # [F
    .param p3, "angle"    # F

    .prologue
    const/4 v4, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 84
    new-array v1, v4, [F

    aget v2, p1, v6

    aget v3, p2, v6

    sub-float/2addr v2, v3

    aput v2, v1, v6

    aget v2, p1, v7

    aget v3, p2, v7

    sub-float/2addr v2, v3

    aput v2, v1, v7

    .line 85
    .local v1, "xx":[F
    new-array v0, v4, [F

    aget v2, v1, v6

    float-to-double v4, p3

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    double-to-float v3, v4

    mul-float/2addr v2, v3

    aget v3, v1, v7

    float-to-double v4, p3

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    double-to-float v4, v4

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    aput v2, v0, v6

    aget v2, v1, v6

    float-to-double v4, p3

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    double-to-float v3, v4

    mul-float/2addr v2, v3

    aget v3, v1, v7

    float-to-double v4, p3

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    double-to-float v4, v4

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    aput v2, v0, v7

    .line 87
    .local v0, "ret":[F
    return-object v0
.end method


# virtual methods
.method public getOutputFormat(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/FrameFormat;
    .locals 0
    .param p1, "portName"    # Ljava/lang/String;
    .param p2, "inputFormat"    # Landroid/filterfw/core/FrameFormat;

    .prologue
    .line 73
    return-object p2
.end method

.method public prepare(Landroid/filterfw/core/FilterContext;)V
    .locals 3
    .param p1, "context"    # Landroid/filterfw/core/FilterContext;

    .prologue
    .line 78
    invoke-static {p1}, Landroid/filterfw/core/ShaderProgram;->createIdentity(Landroid/filterfw/core/FilterContext;)Landroid/filterfw/core/ShaderProgram;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/filterpacks/facedetect/AndroidMaskFilter;->mOverlayProgram:Landroid/filterfw/core/ShaderProgram;

    .line 79
    iget-object v0, p0, Lcom/google/android/filterpacks/facedetect/AndroidMaskFilter;->mOverlayProgram:Landroid/filterfw/core/ShaderProgram;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/filterfw/core/ShaderProgram;->setBlendEnabled(Z)V

    .line 80
    iget-object v0, p0, Lcom/google/android/filterpacks/facedetect/AndroidMaskFilter;->mOverlayProgram:Landroid/filterfw/core/ShaderProgram;

    const/16 v1, 0x302

    const/16 v2, 0x303

    invoke-virtual {v0, v1, v2}, Landroid/filterfw/core/ShaderProgram;->setBlendFunc(II)V

    .line 81
    return-void
.end method

.method public process(Landroid/filterfw/core/FilterContext;)V
    .locals 40
    .param p1, "context"    # Landroid/filterfw/core/FilterContext;

    .prologue
    .line 92
    invoke-virtual/range {p1 .. p1}, Landroid/filterfw/core/FilterContext;->getFrameManager()Landroid/filterfw/core/FrameManager;

    move-result-object v18

    .line 95
    .local v18, "frameManager":Landroid/filterfw/core/FrameManager;
    const-string v35, "image"

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Lcom/google/android/filterpacks/facedetect/AndroidMaskFilter;->pullInput(Ljava/lang/String;)Landroid/filterfw/core/Frame;

    move-result-object v20

    .line 96
    .local v20, "imageFrame":Landroid/filterfw/core/Frame;
    const-string v35, "faces"

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Lcom/google/android/filterpacks/facedetect/AndroidMaskFilter;->pullInput(Ljava/lang/String;)Landroid/filterfw/core/Frame;

    move-result-object v15

    .line 97
    .local v15, "facesFrame":Landroid/filterfw/core/Frame;
    const-string v35, "android"

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Lcom/google/android/filterpacks/facedetect/AndroidMaskFilter;->pullInput(Ljava/lang/String;)Landroid/filterfw/core/Frame;

    move-result-object v7

    .line 100
    .local v7, "androidFrame":Landroid/filterfw/core/Frame;
    invoke-virtual/range {v20 .. v20}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    move-result-object v24

    .line 101
    .local v24, "outputFormat":Landroid/filterfw/core/FrameFormat;
    invoke-virtual {v7}, Landroid/filterfw/core/Frame;->getFormat()Landroid/filterfw/core/FrameFormat;

    move-result-object v6

    .line 103
    .local v6, "androidFormat":Landroid/filterfw/core/FrameFormat;
    move-object/from16 v0, v18

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/filterfw/core/FrameManager;->newFrame(Landroid/filterfw/core/FrameFormat;)Landroid/filterfw/core/Frame;

    move-result-object v23

    .line 104
    .local v23, "output":Landroid/filterfw/core/Frame;
    move-object/from16 v0, v23

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/filterfw/core/Frame;->setDataFromFrame(Landroid/filterfw/core/Frame;)V

    .line 106
    invoke-virtual {v15}, Landroid/filterfw/core/Frame;->getObjectValue()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/google/android/filterpacks/facedetect/FaceMeta;

    .line 108
    .local v14, "face":Lcom/google/android/filterpacks/facedetect/FaceMeta;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/filterpacks/facedetect/AndroidMaskFilter;->mOverlayProgram:Landroid/filterfw/core/ShaderProgram;

    move-object/from16 v35, v0

    const/16 v36, 0x0

    const/16 v37, 0x0

    const/high16 v38, 0x3f800000    # 1.0f

    const/high16 v39, 0x3f800000    # 1.0f

    invoke-virtual/range {v35 .. v39}, Landroid/filterfw/core/ShaderProgram;->setSourceRect(FFFF)V

    .line 109
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/filterpacks/facedetect/AndroidMaskFilter;->mOverlayProgram:Landroid/filterfw/core/ShaderProgram;

    move-object/from16 v35, v0

    const/16 v36, 0x0

    const/16 v37, 0x0

    const/high16 v38, 0x3f800000    # 1.0f

    const/high16 v39, 0x3f800000    # 1.0f

    invoke-virtual/range {v35 .. v39}, Landroid/filterfw/core/ShaderProgram;->setTargetRect(FFFF)V

    .line 111
    invoke-virtual {v14}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->count()I

    move-result v22

    .line 112
    .local v22, "num_face":I
    const/16 v19, 0x0

    .local v19, "i":I
    :goto_0
    move/from16 v0, v19

    move/from16 v1, v22

    if-ge v0, v1, :cond_0

    .line 113
    new-instance v16, Landroid/filterfw/geometry/Point;

    move/from16 v0, v19

    invoke-virtual {v14, v0}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getFaceX0(I)F

    move-result v35

    move/from16 v0, v19

    invoke-virtual {v14, v0}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getFaceY0(I)F

    move-result v36

    move-object/from16 v0, v16

    move/from16 v1, v35

    move/from16 v2, v36

    invoke-direct {v0, v1, v2}, Landroid/filterfw/geometry/Point;-><init>(FF)V

    .line 114
    .local v16, "fr0":Landroid/filterfw/geometry/Point;
    new-instance v17, Landroid/filterfw/geometry/Point;

    move/from16 v0, v19

    invoke-virtual {v14, v0}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getFaceX1(I)F

    move-result v35

    move/from16 v0, v19

    invoke-virtual {v14, v0}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getFaceY1(I)F

    move-result v36

    move-object/from16 v0, v17

    move/from16 v1, v35

    move/from16 v2, v36

    invoke-direct {v0, v1, v2}, Landroid/filterfw/geometry/Point;-><init>(FF)V

    .line 115
    .local v17, "fr1":Landroid/filterfw/geometry/Point;
    new-instance v21, Landroid/filterfw/geometry/Point;

    move/from16 v0, v19

    invoke-virtual {v14, v0}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getLeftEyeX(I)F

    move-result v35

    move/from16 v0, v19

    invoke-virtual {v14, v0}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getLeftEyeY(I)F

    move-result v36

    move-object/from16 v0, v21

    move/from16 v1, v35

    move/from16 v2, v36

    invoke-direct {v0, v1, v2}, Landroid/filterfw/geometry/Point;-><init>(FF)V

    .line 116
    .local v21, "le":Landroid/filterfw/geometry/Point;
    new-instance v29, Landroid/filterfw/geometry/Point;

    move/from16 v0, v19

    invoke-virtual {v14, v0}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getRightEyeX(I)F

    move-result v35

    move/from16 v0, v19

    invoke-virtual {v14, v0}, Lcom/google/android/filterpacks/facedetect/FaceMeta;->getRightEyeY(I)F

    move-result v36

    move-object/from16 v0, v29

    move/from16 v1, v35

    move/from16 v2, v36

    invoke-direct {v0, v1, v2}, Landroid/filterfw/geometry/Point;-><init>(FF)V

    .line 117
    .local v29, "re":Landroid/filterfw/geometry/Point;
    move-object/from16 v0, v21

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Landroid/filterfw/geometry/Point;->plus(Landroid/filterfw/geometry/Point;)Landroid/filterfw/geometry/Point;

    move-result-object v35

    const/high16 v36, 0x3f000000    # 0.5f

    invoke-virtual/range {v35 .. v36}, Landroid/filterfw/geometry/Point;->times(F)Landroid/filterfw/geometry/Point;

    move-result-object v11

    .line 118
    .local v11, "center":Landroid/filterfw/geometry/Point;
    move-object/from16 v0, v21

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Landroid/filterfw/geometry/Point;->minus(Landroid/filterfw/geometry/Point;)Landroid/filterfw/geometry/Point;

    move-result-object v35

    invoke-virtual/range {v35 .. v35}, Landroid/filterfw/geometry/Point;->length()F

    move-result v13

    .line 119
    .local v13, "dist":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/filterpacks/facedetect/AndroidMaskFilter;->mAndroidEyeDist:F

    move/from16 v35, v0

    div-float v31, v13, v35

    .line 120
    .local v31, "s":F
    invoke-virtual {v6}, Landroid/filterfw/core/FrameFormat;->getWidth()I

    move-result v35

    move/from16 v0, v35

    int-to-float v0, v0

    move/from16 v35, v0

    invoke-virtual {v6}, Landroid/filterfw/core/FrameFormat;->getHeight()I

    move-result v36

    move/from16 v0, v36

    int-to-float v0, v0

    move/from16 v36, v0

    div-float v32, v35, v36

    .line 121
    .local v32, "s1":F
    invoke-virtual/range {v24 .. v24}, Landroid/filterfw/core/FrameFormat;->getWidth()I

    move-result v35

    move/from16 v0, v35

    int-to-float v0, v0

    move/from16 v35, v0

    invoke-virtual/range {v24 .. v24}, Landroid/filterfw/core/FrameFormat;->getHeight()I

    move-result v36

    move/from16 v0, v36

    int-to-float v0, v0

    move/from16 v36, v0

    div-float v33, v35, v36

    .line 122
    .local v33, "s2":F
    div-float v34, v31, v32

    .line 123
    .local v34, "sy":F
    new-instance v9, Landroid/filterfw/geometry/Point;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/filterpacks/facedetect/AndroidMaskFilter;->mAndroidCenterX:F

    move/from16 v35, v0

    mul-float v35, v35, v31

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/filterpacks/facedetect/AndroidMaskFilter;->mAndroidCenterY:F

    move/from16 v36, v0

    mul-float v36, v36, v34

    move/from16 v0, v35

    move/from16 v1, v36

    invoke-direct {v9, v0, v1}, Landroid/filterfw/geometry/Point;-><init>(FF)V

    .line 124
    .local v9, "c":Landroid/filterfw/geometry/Point;
    new-instance v10, Landroid/filterfw/geometry/Point;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/filterpacks/facedetect/AndroidMaskFilter;->mAndroidCenterX:F

    move/from16 v35, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/filterpacks/facedetect/AndroidMaskFilter;->mAndroidCenterY:F

    move/from16 v36, v0

    move/from16 v0, v35

    move/from16 v1, v36

    invoke-direct {v10, v0, v1}, Landroid/filterfw/geometry/Point;-><init>(FF)V

    .line 125
    .local v10, "c0":Landroid/filterfw/geometry/Point;
    move-object/from16 v0, v29

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/filterfw/geometry/Point;->minus(Landroid/filterfw/geometry/Point;)Landroid/filterfw/geometry/Point;

    move-result-object v12

    .line 126
    .local v12, "d":Landroid/filterfw/geometry/Point;
    iget v0, v12, Landroid/filterfw/geometry/Point;->y:F

    move/from16 v35, v0

    move/from16 v0, v35

    float-to-double v0, v0

    move-wide/from16 v36, v0

    iget v0, v12, Landroid/filterfw/geometry/Point;->x:F

    move/from16 v35, v0

    move/from16 v0, v35

    float-to-double v0, v0

    move-wide/from16 v38, v0

    invoke-static/range {v36 .. v39}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v36

    move-wide/from16 v0, v36

    double-to-float v8, v0

    .line 127
    .local v8, "angle":F
    new-instance v35, Landroid/filterfw/geometry/Point;

    const/16 v36, 0x0

    const/16 v37, 0x0

    invoke-direct/range {v35 .. v37}, Landroid/filterfw/geometry/Point;-><init>(FF)V

    move-object/from16 v0, v35

    invoke-virtual {v0, v9}, Landroid/filterfw/geometry/Point;->minus(Landroid/filterfw/geometry/Point;)Landroid/filterfw/geometry/Point;

    move-result-object v35

    move-object/from16 v0, v35

    invoke-virtual {v0, v8}, Landroid/filterfw/geometry/Point;->rotated(F)Landroid/filterfw/geometry/Point;

    move-result-object v35

    const/high16 v36, 0x3f800000    # 1.0f

    move-object/from16 v0, v35

    move/from16 v1, v36

    move/from16 v2, v33

    invoke-virtual {v0, v1, v2}, Landroid/filterfw/geometry/Point;->mult(FF)Landroid/filterfw/geometry/Point;

    move-result-object v25

    .line 128
    .local v25, "p0":Landroid/filterfw/geometry/Point;
    new-instance v35, Landroid/filterfw/geometry/Point;

    const/16 v36, 0x0

    move-object/from16 v0, v35

    move/from16 v1, v36

    move/from16 v2, v34

    invoke-direct {v0, v1, v2}, Landroid/filterfw/geometry/Point;-><init>(FF)V

    move-object/from16 v0, v35

    invoke-virtual {v0, v9}, Landroid/filterfw/geometry/Point;->minus(Landroid/filterfw/geometry/Point;)Landroid/filterfw/geometry/Point;

    move-result-object v35

    move-object/from16 v0, v35

    invoke-virtual {v0, v8}, Landroid/filterfw/geometry/Point;->rotated(F)Landroid/filterfw/geometry/Point;

    move-result-object v35

    const/high16 v36, 0x3f800000    # 1.0f

    move-object/from16 v0, v35

    move/from16 v1, v36

    move/from16 v2, v33

    invoke-virtual {v0, v1, v2}, Landroid/filterfw/geometry/Point;->mult(FF)Landroid/filterfw/geometry/Point;

    move-result-object v26

    .line 129
    .local v26, "p1":Landroid/filterfw/geometry/Point;
    new-instance v35, Landroid/filterfw/geometry/Point;

    move-object/from16 v0, v35

    move/from16 v1, v31

    move/from16 v2, v34

    invoke-direct {v0, v1, v2}, Landroid/filterfw/geometry/Point;-><init>(FF)V

    move-object/from16 v0, v35

    invoke-virtual {v0, v9}, Landroid/filterfw/geometry/Point;->minus(Landroid/filterfw/geometry/Point;)Landroid/filterfw/geometry/Point;

    move-result-object v35

    move-object/from16 v0, v35

    invoke-virtual {v0, v8}, Landroid/filterfw/geometry/Point;->rotated(F)Landroid/filterfw/geometry/Point;

    move-result-object v35

    const/high16 v36, 0x3f800000    # 1.0f

    move-object/from16 v0, v35

    move/from16 v1, v36

    move/from16 v2, v33

    invoke-virtual {v0, v1, v2}, Landroid/filterfw/geometry/Point;->mult(FF)Landroid/filterfw/geometry/Point;

    move-result-object v27

    .line 130
    .local v27, "p2":Landroid/filterfw/geometry/Point;
    new-instance v35, Landroid/filterfw/geometry/Point;

    const/16 v36, 0x0

    move-object/from16 v0, v35

    move/from16 v1, v31

    move/from16 v2, v36

    invoke-direct {v0, v1, v2}, Landroid/filterfw/geometry/Point;-><init>(FF)V

    move-object/from16 v0, v35

    invoke-virtual {v0, v9}, Landroid/filterfw/geometry/Point;->minus(Landroid/filterfw/geometry/Point;)Landroid/filterfw/geometry/Point;

    move-result-object v35

    move-object/from16 v0, v35

    invoke-virtual {v0, v8}, Landroid/filterfw/geometry/Point;->rotated(F)Landroid/filterfw/geometry/Point;

    move-result-object v35

    const/high16 v36, 0x3f800000    # 1.0f

    move-object/from16 v0, v35

    move/from16 v1, v36

    move/from16 v2, v33

    invoke-virtual {v0, v1, v2}, Landroid/filterfw/geometry/Point;->mult(FF)Landroid/filterfw/geometry/Point;

    move-result-object v28

    .line 131
    .local v28, "p3":Landroid/filterfw/geometry/Point;
    new-instance v30, Landroid/filterfw/geometry/Quad;

    move-object/from16 v0, v30

    move-object/from16 v1, v25

    move-object/from16 v2, v28

    move-object/from16 v3, v26

    move-object/from16 v4, v27

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/filterfw/geometry/Quad;-><init>(Landroid/filterfw/geometry/Point;Landroid/filterfw/geometry/Point;Landroid/filterfw/geometry/Point;Landroid/filterfw/geometry/Point;)V

    .line 132
    .local v30, "region":Landroid/filterfw/geometry/Quad;
    move-object/from16 v0, v30

    invoke-virtual {v0, v11}, Landroid/filterfw/geometry/Quad;->translated(Landroid/filterfw/geometry/Point;)Landroid/filterfw/geometry/Quad;

    move-result-object v30

    .line 133
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/filterpacks/facedetect/AndroidMaskFilter;->mOverlayProgram:Landroid/filterfw/core/ShaderProgram;

    move-object/from16 v35, v0

    move-object/from16 v0, v35

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/filterfw/core/ShaderProgram;->setTargetRegion(Landroid/filterfw/geometry/Quad;)V

    .line 134
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/filterpacks/facedetect/AndroidMaskFilter;->mOverlayProgram:Landroid/filterfw/core/ShaderProgram;

    move-object/from16 v35, v0

    move-object/from16 v0, v35

    move-object/from16 v1, v23

    invoke-virtual {v0, v7, v1}, Landroid/filterfw/core/ShaderProgram;->process(Landroid/filterfw/core/Frame;Landroid/filterfw/core/Frame;)V

    .line 112
    add-int/lit8 v19, v19, 0x1

    goto/16 :goto_0

    .line 138
    .end local v8    # "angle":F
    .end local v9    # "c":Landroid/filterfw/geometry/Point;
    .end local v10    # "c0":Landroid/filterfw/geometry/Point;
    .end local v11    # "center":Landroid/filterfw/geometry/Point;
    .end local v12    # "d":Landroid/filterfw/geometry/Point;
    .end local v13    # "dist":F
    .end local v16    # "fr0":Landroid/filterfw/geometry/Point;
    .end local v17    # "fr1":Landroid/filterfw/geometry/Point;
    .end local v21    # "le":Landroid/filterfw/geometry/Point;
    .end local v25    # "p0":Landroid/filterfw/geometry/Point;
    .end local v26    # "p1":Landroid/filterfw/geometry/Point;
    .end local v27    # "p2":Landroid/filterfw/geometry/Point;
    .end local v28    # "p3":Landroid/filterfw/geometry/Point;
    .end local v29    # "re":Landroid/filterfw/geometry/Point;
    .end local v30    # "region":Landroid/filterfw/geometry/Quad;
    .end local v31    # "s":F
    .end local v32    # "s1":F
    .end local v33    # "s2":F
    .end local v34    # "sy":F
    :cond_0
    const-string v35, "image"

    move-object/from16 v0, p0

    move-object/from16 v1, v35

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Lcom/google/android/filterpacks/facedetect/AndroidMaskFilter;->pushOutput(Ljava/lang/String;Landroid/filterfw/core/Frame;)V

    .line 141
    invoke-virtual/range {v23 .. v23}, Landroid/filterfw/core/Frame;->release()Landroid/filterfw/core/Frame;

    .line 142
    return-void
.end method

.method public setupPorts()V
    .locals 4

    .prologue
    const/4 v2, 0x3

    .line 59
    invoke-static {v2, v2}, Landroid/filterfw/format/ImageFormat;->create(II)Landroid/filterfw/core/MutableFrameFormat;

    move-result-object v1

    .line 61
    .local v1, "imageFormat":Landroid/filterfw/core/FrameFormat;
    const-class v2, Lcom/google/android/filterpacks/facedetect/FaceMeta;

    const/4 v3, 0x2

    invoke-static {v2, v3}, Landroid/filterfw/format/ObjectFormat;->fromClass(Ljava/lang/Class;I)Landroid/filterfw/core/MutableFrameFormat;

    move-result-object v0

    .line 65
    .local v0, "facesFormat":Landroid/filterfw/core/FrameFormat;
    const-string v2, "image"

    invoke-virtual {p0, v2, v1}, Lcom/google/android/filterpacks/facedetect/AndroidMaskFilter;->addMaskedInputPort(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)V

    .line 66
    const-string v2, "faces"

    invoke-virtual {p0, v2, v0}, Lcom/google/android/filterpacks/facedetect/AndroidMaskFilter;->addMaskedInputPort(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)V

    .line 67
    const-string v2, "android"

    invoke-virtual {p0, v2, v1}, Lcom/google/android/filterpacks/facedetect/AndroidMaskFilter;->addMaskedInputPort(Ljava/lang/String;Landroid/filterfw/core/FrameFormat;)V

    .line 68
    const-string v2, "image"

    const-string v3, "image"

    invoke-virtual {p0, v2, v3}, Lcom/google/android/filterpacks/facedetect/AndroidMaskFilter;->addOutputBasedOnInput(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    return-void
.end method
