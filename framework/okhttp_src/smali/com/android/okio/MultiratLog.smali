.class public Lcom/android/okio/MultiratLog;
.super Ljava/lang/Object;
.source "MultiratLog.java"


# static fields
.field public static final MRATLOG:Z = false

.field static final SLOG_DEBUG:I = 0x1

.field static final SLOG_ERROR:I = 0x4

.field static final SLOG_INFO:I = 0x2

.field static final SLOG_LEVEL:I = 0x1

.field static final SLOG_WARN:I = 0x3

.field private static final mStaticLogger:Lcom/android/okio/MultiratLog;


# instance fields
.field private final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 21
    new-instance v0, Lcom/android/okio/MultiratLog;

    const-string v1, "Static"

    invoke-direct {v0, v1}, Lcom/android/okio/MultiratLog;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/android/okio/MultiratLog;->mStaticLogger:Lcom/android/okio/MultiratLog;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "tag"    # Ljava/lang/String;

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "(HTTPLog)-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-SM:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/okio/MultiratLog;->TAG:Ljava/lang/String;

    .line 25
    return-void
.end method

.method public static debug(Ljava/lang/String;)V
    .locals 1
    .param p0, "args"    # Ljava/lang/String;

    .prologue
    .line 30
    sget-object v0, Lcom/android/okio/MultiratLog;->mStaticLogger:Lcom/android/okio/MultiratLog;

    invoke-virtual {v0, p0}, Lcom/android/okio/MultiratLog;->d(Ljava/lang/String;)V

    .line 31
    return-void
.end method

.method public static debug(Ljava/lang/Throwable;)V
    .locals 1
    .param p0, "e"    # Ljava/lang/Throwable;

    .prologue
    .line 34
    sget-object v0, Lcom/android/okio/MultiratLog;->mStaticLogger:Lcom/android/okio/MultiratLog;

    invoke-virtual {v0, p0}, Lcom/android/okio/MultiratLog;->d(Ljava/lang/Throwable;)V

    .line 35
    return-void
.end method

.method private getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;
    .locals 4
    .param p1, "tr"    # Ljava/lang/Throwable;

    .prologue
    .line 111
    if-nez p1, :cond_0

    .line 112
    const-string v3, ""

    .line 126
    :goto_0
    return-object v3

    .line 116
    :cond_0
    move-object v2, p1

    .line 117
    .local v2, "t":Ljava/lang/Throwable;
    :goto_1
    if-eqz v2, :cond_2

    .line 118
    instance-of v3, v2, Ljava/net/UnknownHostException;

    if-eqz v3, :cond_1

    .line 119
    const-string v3, ""

    goto :goto_0

    .line 121
    :cond_1
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    goto :goto_1

    .line 123
    :cond_2
    new-instance v1, Ljava/io/StringWriter;

    invoke-direct {v1}, Ljava/io/StringWriter;-><init>()V

    .line 124
    .local v1, "sw":Ljava/io/StringWriter;
    new-instance v0, Ljava/io/PrintWriter;

    invoke-direct {v0, v1}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    .line 125
    .local v0, "pw":Ljava/io/PrintWriter;
    invoke-virtual {p1, v0}, Ljava/lang/Throwable;->printStackTrace(Ljava/io/PrintWriter;)V

    .line 126
    invoke-virtual {v1}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method public static info(Ljava/lang/String;)V
    .locals 1
    .param p0, "args"    # Ljava/lang/String;

    .prologue
    .line 38
    sget-object v0, Lcom/android/okio/MultiratLog;->mStaticLogger:Lcom/android/okio/MultiratLog;

    invoke-virtual {v0, p0}, Lcom/android/okio/MultiratLog;->i(Ljava/lang/String;)V

    .line 39
    return-void
.end method

.method public static info(Ljava/lang/Throwable;)V
    .locals 1
    .param p0, "e"    # Ljava/lang/Throwable;

    .prologue
    .line 42
    sget-object v0, Lcom/android/okio/MultiratLog;->mStaticLogger:Lcom/android/okio/MultiratLog;

    invoke-virtual {v0, p0}, Lcom/android/okio/MultiratLog;->i(Ljava/lang/Throwable;)V

    .line 43
    return-void
.end method

.method private logThrowable(Ljava/lang/Throwable;)V
    .locals 4
    .param p1, "e"    # Ljava/lang/Throwable;

    .prologue
    .line 95
    :try_start_0
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 96
    .local v0, "sBuf":Ljava/lang/StringBuffer;
    iget-object v2, p0, Lcom/android/okio/MultiratLog;->TAG:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, "(This is just Trace Log)"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-direct {p0, p1}, Lcom/android/okio/MultiratLog;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 97
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    .line 98
    .local v1, "ws":Ljava/lang/String;
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v2, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 101
    .end local v0    # "sBuf":Ljava/lang/StringBuffer;
    .end local v1    # "ws":Ljava/lang/String;
    :goto_0
    return-void

    .line 100
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public static warn(Ljava/lang/String;)V
    .locals 1
    .param p0, "args"    # Ljava/lang/String;

    .prologue
    .line 46
    sget-object v0, Lcom/android/okio/MultiratLog;->mStaticLogger:Lcom/android/okio/MultiratLog;

    invoke-virtual {v0, p0}, Lcom/android/okio/MultiratLog;->i(Ljava/lang/String;)V

    .line 47
    return-void
.end method

.method public static warn(Ljava/lang/Throwable;)V
    .locals 1
    .param p0, "e"    # Ljava/lang/Throwable;

    .prologue
    .line 50
    sget-object v0, Lcom/android/okio/MultiratLog;->mStaticLogger:Lcom/android/okio/MultiratLog;

    invoke-virtual {v0, p0}, Lcom/android/okio/MultiratLog;->i(Ljava/lang/Throwable;)V

    .line 51
    return-void
.end method


# virtual methods
.method public d(Ljava/lang/String;)V
    .locals 3
    .param p1, "args"    # Ljava/lang/String;

    .prologue
    .line 55
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/android/okio/MultiratLog;->TAG:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 56
    return-void
.end method

.method public d(Ljava/lang/Throwable;)V
    .locals 0
    .param p1, "e"    # Ljava/lang/Throwable;

    .prologue
    .line 75
    invoke-direct {p0, p1}, Lcom/android/okio/MultiratLog;->logThrowable(Ljava/lang/Throwable;)V

    .line 76
    return-void
.end method

.method public e(Ljava/lang/String;)V
    .locals 3
    .param p1, "args"    # Ljava/lang/String;

    .prologue
    .line 70
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/android/okio/MultiratLog;->TAG:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 71
    return-void
.end method

.method public e(Ljava/lang/Throwable;)V
    .locals 0
    .param p1, "e"    # Ljava/lang/Throwable;

    .prologue
    .line 90
    invoke-direct {p0, p1}, Lcom/android/okio/MultiratLog;->logThrowable(Ljava/lang/Throwable;)V

    .line 91
    return-void
.end method

.method public i(Ljava/lang/String;)V
    .locals 3
    .param p1, "args"    # Ljava/lang/String;

    .prologue
    .line 60
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/android/okio/MultiratLog;->TAG:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 61
    return-void
.end method

.method public i(Ljava/lang/Throwable;)V
    .locals 0
    .param p1, "e"    # Ljava/lang/Throwable;

    .prologue
    .line 80
    invoke-direct {p0, p1}, Lcom/android/okio/MultiratLog;->logThrowable(Ljava/lang/Throwable;)V

    .line 81
    return-void
.end method

.method public w(Ljava/lang/String;)V
    .locals 3
    .param p1, "args"    # Ljava/lang/String;

    .prologue
    .line 65
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/android/okio/MultiratLog;->TAG:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 66
    return-void
.end method

.method public w(Ljava/lang/Throwable;)V
    .locals 0
    .param p1, "e"    # Ljava/lang/Throwable;

    .prologue
    .line 85
    invoke-direct {p0, p1}, Lcom/android/okio/MultiratLog;->logThrowable(Ljava/lang/Throwable;)V

    .line 86
    return-void
.end method
