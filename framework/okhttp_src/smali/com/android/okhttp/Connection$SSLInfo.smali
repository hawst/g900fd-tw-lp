.class Lcom/android/okhttp/Connection$SSLInfo;
.super Ljava/lang/Object;
.source "Connection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/okhttp/Connection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "SSLInfo"
.end annotation


# instance fields
.field protected final isSSL:Z

.field protected final route:Lcom/android/okhttp/Route;

.field protected final tunnel:Lcom/android/okhttp/TunnelRequest;


# direct methods
.method protected constructor <init>(ZLcom/android/okhttp/Route;Lcom/android/okhttp/TunnelRequest;)V
    .locals 0
    .param p1, "isS"    # Z
    .param p2, "r"    # Lcom/android/okhttp/Route;
    .param p3, "t"    # Lcom/android/okhttp/TunnelRequest;

    .prologue
    .line 132
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 133
    iput-boolean p1, p0, Lcom/android/okhttp/Connection$SSLInfo;->isSSL:Z

    .line 134
    iput-object p2, p0, Lcom/android/okhttp/Connection$SSLInfo;->route:Lcom/android/okhttp/Route;

    .line 135
    iput-object p3, p0, Lcom/android/okhttp/Connection$SSLInfo;->tunnel:Lcom/android/okhttp/TunnelRequest;

    .line 136
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 140
    :try_start_0
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 141
    .local v1, "sb":Ljava/lang/StringBuffer;
    iget-boolean v2, p0, Lcom/android/okhttp/Connection$SSLInfo;->isSSL:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 142
    iget-object v2, p0, Lcom/android/okhttp/Connection$SSLInfo;->route:Lcom/android/okhttp/Route;

    if-nez v2, :cond_0

    const-string v2, "null"

    :goto_0
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 143
    iget-object v2, p0, Lcom/android/okhttp/Connection$SSLInfo;->tunnel:Lcom/android/okhttp/TunnelRequest;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    .line 144
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    .line 147
    .end local v1    # "sb":Ljava/lang/StringBuffer;
    :goto_1
    return-object v2

    .line 142
    .restart local v1    # "sb":Ljava/lang/StringBuffer;
    :cond_0
    iget-object v2, p0, Lcom/android/okhttp/Connection$SSLInfo;->route:Lcom/android/okhttp/Route;

    invoke-virtual {v2}, Lcom/android/okhttp/Route;->getSocketAddress()Ljava/net/InetSocketAddress;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    .line 146
    .end local v1    # "sb":Ljava/lang/StringBuffer;
    :catch_0
    move-exception v0

    .line 147
    .local v0, "e":Ljava/lang/Throwable;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "@"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method
