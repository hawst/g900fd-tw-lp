.class public final Lcom/android/okhttp/Connection;
.super Ljava/lang/Object;
.source "Connection.java"

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/okhttp/Connection$SSLInfo;
    }
.end annotation


# static fields
.field private static final INF_NUMBER:I = 0x2

.field protected static final SOCK_CONN_TIME_OUT:I = 0x2710


# instance fields
.field private bTagUidSet:Z

.field private connTimeout:I

.field private connected:Z

.field private destPorts:[I

.field private handshake:Lcom/android/okhttp/Handshake;

.field private httpConnection:Lcom/android/okhttp/internal/http/HttpConnection;

.field private httpMinorVersion:I

.field private idleStartTimeNs:J

.field private isSSL:Z

.field private logger:Lcom/android/okhttp/internal/http/MultiratLog;

.field private mDestIPver:I

.field private mProxy:[Ljava/net/Proxy;

.field private mTag:I

.field private mUid:I

.field private mainTimeout:I

.field private multiSockDestAddr:[Ljava/net/InetAddress;

.field private multiSocket:[Ljava/net/Socket;

.field private multiSocketStream:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

.field private owner:Ljava/lang/Object;

.field private final pool:Lcom/android/okhttp/ConnectionPool;

.field private recycleCount:I

.field private final route:Lcom/android/okhttp/Route;

.field private socket:Ljava/net/Socket;

.field private spdyConnection:Lcom/android/okhttp/internal/spdy/SpdyConnection;

.field protected ssl_oriReq:Lcom/android/okhttp/Connection$SSLInfo;

.field protected ssl_redirect:Lcom/android/okhttp/Connection$SSLInfo;

.field public startExtremSocketCreation:J

.field public startSampleSocketCreation:J

.field public startSocketCreation:J


# direct methods
.method public constructor <init>(Lcom/android/okhttp/ConnectionPool;Lcom/android/okhttp/Route;)V
    .locals 4
    .param p1, "pool"    # Lcom/android/okhttp/ConnectionPool;
    .param p2, "route"    # Lcom/android/okhttp/Route;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x2

    const/4 v1, 0x0

    .line 1103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    iput-boolean v1, p0, Lcom/android/okhttp/Connection;->connected:Z

    .line 88
    const/4 v0, 0x1

    iput v0, p0, Lcom/android/okhttp/Connection;->httpMinorVersion:I

    .line 102
    iput-boolean v1, p0, Lcom/android/okhttp/Connection;->isSSL:Z

    .line 103
    iput-object v3, p0, Lcom/android/okhttp/Connection;->multiSocketStream:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    .line 105
    new-array v0, v2, [Ljava/net/InetAddress;

    iput-object v0, p0, Lcom/android/okhttp/Connection;->multiSockDestAddr:[Ljava/net/InetAddress;

    .line 106
    iput v1, p0, Lcom/android/okhttp/Connection;->mainTimeout:I

    .line 111
    new-array v0, v2, [Ljava/net/Socket;

    iput-object v0, p0, Lcom/android/okhttp/Connection;->multiSocket:[Ljava/net/Socket;

    .line 112
    new-array v0, v2, [Ljava/net/Proxy;

    iput-object v0, p0, Lcom/android/okhttp/Connection;->mProxy:[Ljava/net/Proxy;

    .line 113
    new-array v0, v2, [I

    iput-object v0, p0, Lcom/android/okhttp/Connection;->destPorts:[I

    .line 115
    iput-boolean v1, p0, Lcom/android/okhttp/Connection;->bTagUidSet:Z

    .line 116
    iput v1, p0, Lcom/android/okhttp/Connection;->mTag:I

    .line 117
    iput v1, p0, Lcom/android/okhttp/Connection;->mUid:I

    .line 118
    iput v1, p0, Lcom/android/okhttp/Connection;->mDestIPver:I

    .line 120
    iput-object v3, p0, Lcom/android/okhttp/Connection;->ssl_oriReq:Lcom/android/okhttp/Connection$SSLInfo;

    .line 122
    iput-object v3, p0, Lcom/android/okhttp/Connection;->ssl_redirect:Lcom/android/okhttp/Connection$SSLInfo;

    .line 124
    new-instance v0, Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ConnThread-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/okhttp/internal/http/MultiratLog;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    .line 1104
    iput-object p1, p0, Lcom/android/okhttp/Connection;->pool:Lcom/android/okhttp/ConnectionPool;

    .line 1105
    iput-object p2, p0, Lcom/android/okhttp/Connection;->route:Lcom/android/okhttp/Route;

    .line 1106
    return-void
.end method

.method private connect(I[Ljava/net/InetAddress;ILjava/net/InetAddress;Ljava/net/Proxy;)Ljava/net/Socket;
    .locals 7
    .param p1, "connectTimeout"    # I
    .param p2, "addresses"    # [Ljava/net/InetAddress;
    .param p3, "destPort"    # I
    .param p4, "sourceAddr"    # Ljava/net/InetAddress;
    .param p5, "proxy"    # Ljava/net/Proxy;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 513
    const/4 v3, 0x0

    .line 514
    .local v3, "socketCandidate":Ljava/net/Socket;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v4, p2

    if-ge v2, v4, :cond_4

    .line 515
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "http socket try to connect to "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget-object v6, p2, v2

    invoke-virtual {v6}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ":"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " from "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " with Proxy "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 517
    :cond_0
    :try_start_0
    iget-boolean v4, p0, Lcom/android/okhttp/Connection;->bTagUidSet:Z

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/android/okhttp/Connection;->multiSocketStream:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    if-eqz v4, :cond_1

    .line 518
    iget v4, p0, Lcom/android/okhttp/Connection;->mTag:I

    invoke-static {v4}, Lcom/android/okhttp/internal/http/SBServiceAPI;->setThreadStatsTag(I)V

    .line 519
    iget v4, p0, Lcom/android/okhttp/Connection;->mUid:I

    invoke-static {v4}, Lcom/android/okhttp/internal/http/SBServiceAPI;->setThreadStatsUid(I)V

    .line 521
    :cond_1
    if-eqz p5, :cond_5

    invoke-virtual {p5}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v4

    sget-object v5, Ljava/net/Proxy$Type;->HTTP:Ljava/net/Proxy$Type;

    if-eq v4, v5, :cond_5

    new-instance v4, Ljava/net/Socket;

    invoke-direct {v4, p5}, Ljava/net/Socket;-><init>(Ljava/net/Proxy;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-object v3, v4

    .line 524
    :goto_1
    if-eqz p4, :cond_8

    .line 526
    :try_start_1
    invoke-virtual {v3}, Ljava/net/Socket;->isBound()Z

    move-result v4

    if-nez v4, :cond_2

    .line 527
    new-instance v4, Ljava/net/InetSocketAddress;

    const/4 v5, 0x0

    invoke-direct {v4, p4, v5}, Ljava/net/InetSocketAddress;-><init>(Ljava/net/InetAddress;I)V

    invoke-virtual {v3, v4}, Ljava/net/Socket;->bind(Ljava/net/SocketAddress;)V

    .line 528
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "socket bind to "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 537
    :cond_2
    :goto_2
    if-eqz p5, :cond_3

    :try_start_2
    invoke-virtual {p5}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v4

    sget-object v5, Ljava/net/Proxy$Type;->DIRECT:Ljava/net/Proxy$Type;

    if-ne v4, v5, :cond_9

    .line 538
    :cond_3
    new-instance v4, Ljava/net/InetSocketAddress;

    aget-object v5, p2, v2

    invoke-direct {v4, v5, p3}, Ljava/net/InetSocketAddress;-><init>(Ljava/net/InetAddress;I)V

    invoke-virtual {v3, v4, p1}, Ljava/net/Socket;->connect(Ljava/net/SocketAddress;I)V

    .line 541
    :goto_3
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "http socket connected to "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " from "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Ljava/net/Socket;->getLocalAddress()Ljava/net/InetAddress;

    move-result-object v6

    invoke-virtual {v6}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ":"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Ljava/net/Socket;->getLocalPort()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 556
    :cond_4
    return-object v3

    .line 521
    :cond_5
    new-instance v4, Ljava/net/Socket;

    invoke-direct {v4}, Ljava/net/Socket;-><init>()V

    move-object v3, v4

    goto/16 :goto_1

    .line 530
    :catch_0
    move-exception v1

    .line 531
    .local v1, "ex":Ljava/lang/Throwable;
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    invoke-virtual {v4, v1}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_2

    .line 544
    .end local v1    # "ex":Ljava/lang/Throwable;
    :catch_1
    move-exception v0

    .line 545
    .local v0, "e":Ljava/io/IOException;
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_6

    iget-object v4, p0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "failed to connect to "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget-object v6, p2, v2

    invoke-virtual {v6}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " from "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " with Proxy "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 546
    :cond_6
    array-length v4, p2

    add-int/lit8 v4, v4, -0x1

    if-ne v2, v4, :cond_a

    .line 548
    if-eqz v3, :cond_7

    .line 549
    :try_start_3
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiratUtil;->closeQuietly(Ljava/net/Socket;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_2

    .line 552
    :cond_7
    :goto_4
    throw v0

    .line 535
    .end local v0    # "e":Ljava/io/IOException;
    :cond_8
    :try_start_4
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    const-string v5, "main socket not bind "

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 540
    :cond_9
    invoke-virtual {p5}, Ljava/net/Proxy;->address()Ljava/net/SocketAddress;

    move-result-object v4

    invoke-virtual {v3, v4, p1}, Ljava/net/Socket;->connect(Ljava/net/SocketAddress;I)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto/16 :goto_3

    .line 514
    .restart local v0    # "e":Ljava/io/IOException;
    :cond_a
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    .line 551
    :catch_2
    move-exception v4

    goto :goto_4
.end method

.method private extremeConditionConnectInternal(ILcom/android/okhttp/Request;JJJLcom/android/okhttp/internal/http/MultiSocketInputStream;ZZ)Ljava/net/Socket;
    .locals 45
    .param p1, "netType"    # I
    .param p2, "oriReqHeaders"    # Lcom/android/okhttp/Request;
    .param p3, "start"    # J
    .param p5, "end"    # J
    .param p7, "fullSize"    # J
    .param p9, "multiSock"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;
    .param p10, "bUpdateTime"    # Z
    .param p11, "bDonotBindIfNull"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 676
    const/4 v11, 0x0

    .line 678
    .local v11, "socketCandidate":Ljava/net/Socket;
    invoke-virtual/range {p0 .. p1}, Lcom/android/okhttp/Connection;->getLocalAddr(I)Ljava/net/InetAddress;

    move-result-object v8

    .line 679
    .local v8, "localAddr":Ljava/net/InetAddress;
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "connecting socket for Main or Extrem for netType "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " and get IP "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ", bDonotBindIfNull="

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p11

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 680
    :cond_0
    if-nez v8, :cond_1

    .line 681
    new-instance v4, Ljava/io/IOException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Cannot find local Address for netType "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 683
    :cond_1
    if-nez v8, :cond_b

    const-string v41, "[DefaultLocalInf]"

    .line 684
    .local v41, "socketName":Ljava/lang/String;
    :goto_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->multiSocketStream:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-object v4, v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->sRedirectedRequestHeaders:[Lcom/android/okhttp/Request;

    aget-object v37, v4, p1

    .line 685
    .local v37, "requestHeaders":Lcom/android/okhttp/Request;
    if-nez v37, :cond_2

    .line 686
    move-object/from16 v37, p2

    .line 688
    :cond_2
    invoke-virtual/range {v37 .. v37}, Lcom/android/okhttp/Request;->getHeaders()Lcom/android/okhttp/Headers;

    move-result-object v4

    const-string v5, "Host"

    invoke-virtual {v4, v5}, Lcom/android/okhttp/Headers;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    .line 689
    .local v24, "host":Ljava/lang/String;
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Opening Socket for sending 2nd request by socket :"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v41

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "config :"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v24

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 691
    :cond_3
    const/16 v32, 0x1

    .line 694
    .local v32, "notOkResponse":Z
    new-instance v35, Ljava/lang/StringBuffer;

    const-string v4, "bytes="

    move-object/from16 v0, v35

    invoke-direct {v0, v4}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 696
    .local v35, "range":Ljava/lang/StringBuffer;
    move-object/from16 v0, v35

    move-wide/from16 v1, p3

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, "-"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    move-wide/from16 v0, p5

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    .line 699
    invoke-virtual/range {v37 .. v37}, Lcom/android/okhttp/Request;->uri()Ljava/net/URI;

    move-result-object v44

    .line 701
    .local v44, "uri":Ljava/net/URI;
    invoke-virtual/range {v44 .. v44}, Ljava/net/URI;->toURL()Ljava/net/URL;

    move-result-object v31

    .line 703
    .local v31, "newUrl":Ljava/net/URL;
    invoke-virtual/range {v44 .. v44}, Ljava/net/URI;->toURL()Ljava/net/URL;

    move-result-object v34

    .line 705
    .local v34, "previousUrl":Ljava/net/URL;
    invoke-virtual/range {v37 .. v37}, Lcom/android/okhttp/Request;->getHeaders()Lcom/android/okhttp/Headers;

    move-result-object v4

    const-string v5, "Host"

    invoke-virtual {v4, v5}, Lcom/android/okhttp/Headers;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    .line 706
    .local v28, "location":Ljava/lang/String;
    const/16 v40, 0x0

    .line 708
    .local v40, "rsp":Lcom/android/okhttp/Response;
    const/16 v36, 0x0

    .line 709
    .local v36, "redirectionCount":I
    invoke-virtual/range {v37 .. v37}, Lcom/android/okhttp/Request;->newBuilder()Lcom/android/okhttp/Request$Builder;

    move-result-object v4

    const-string v5, "Range"

    invoke-virtual/range {v35 .. v35}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v5, v7}, Lcom/android/okhttp/Request$Builder;->header(Ljava/lang/String;Ljava/lang/String;)Lcom/android/okhttp/Request$Builder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/okhttp/Request$Builder;->build()Lcom/android/okhttp/Request;

    move-result-object v29

    .line 710
    .local v29, "newHeader":Lcom/android/okhttp/Request;
    :goto_1
    if-eqz v32, :cond_a

    .line 711
    const/16 v30, 0x0

    .line 713
    .local v30, "newUri":Ljava/net/URI;
    :try_start_0
    invoke-static {}, Lcom/android/okhttp/internal/Platform;->get()Lcom/android/okhttp/internal/Platform;

    move-result-object v4

    move-object/from16 v0, v31

    invoke-virtual {v4, v0}, Lcom/android/okhttp/internal/Platform;->toUriLenient(Ljava/net/URL;)Ljava/net/URI;

    move-result-object v30

    .line 714
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "connecting to url "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {v31 .. v31}, Ljava/net/URL;->toExternalForm()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " with protocol "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {v31 .. v31}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 719
    :cond_4
    invoke-virtual/range {v29 .. v29}, Lcom/android/okhttp/Request;->newBuilder()Lcom/android/okhttp/Request$Builder;

    move-result-object v4

    move-object/from16 v0, v31

    invoke-virtual {v4, v0}, Lcom/android/okhttp/Request$Builder;->url(Ljava/net/URL;)Lcom/android/okhttp/Request$Builder;

    move-result-object v4

    const-string v5, "Host"

    invoke-virtual/range {v31 .. v31}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v5, v7}, Lcom/android/okhttp/Request$Builder;->header(Ljava/lang/String;Ljava/lang/String;)Lcom/android/okhttp/Request$Builder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/okhttp/Request$Builder;->build()Lcom/android/okhttp/Request;

    move-result-object v29

    .line 720
    invoke-virtual/range {v29 .. v29}, Lcom/android/okhttp/Request;->headers()Lcom/android/okhttp/Headers;

    move-result-object v22

    .line 721
    .local v22, "h":Lcom/android/okhttp/Headers;
    invoke-virtual/range {v31 .. v31}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v0, v1, v4}, Lcom/android/okhttp/Connection;->getProxy(ILjava/lang/String;)Ljava/net/Proxy;

    move-result-object v9

    .line 722
    .local v9, "curProxy":Ljava/net/Proxy;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->mProxy:[Ljava/net/Proxy;

    aget-object v4, v4, p1

    move-object/from16 v0, p0

    invoke-direct {v0, v9, v4}, Lcom/android/okhttp/Connection;->isSameProxy(Ljava/net/Proxy;Ljava/net/Proxy;)Z

    move-result v4

    if-nez v4, :cond_c

    const/16 v16, 0x1

    .line 723
    .local v16, "bProxyChanged":Z
    :goto_2
    if-eqz v16, :cond_6

    .line 724
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_5

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "proxy updated from "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/Connection;->mProxy:[Ljava/net/Proxy;

    aget-object v7, v7, p1

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " to "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " for netType "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 725
    :cond_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->mProxy:[Ljava/net/Proxy;

    aput-object v9, v4, p1

    .line 726
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->multiSockDestAddr:[Ljava/net/InetAddress;

    const/4 v5, 0x0

    aput-object v5, v4, p1

    .line 728
    :cond_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->destPorts:[I

    invoke-static/range {v30 .. v30}, Lcom/android/okhttp/internal/Util;->getEffectivePort(Ljava/net/URI;)I

    move-result v5

    aput v5, v4, p1

    .line 729
    invoke-virtual {v9}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lcom/android/okhttp/Connection;->getHttpMinorVersion()I

    move-result v5

    move-object/from16 v0, v29

    invoke-static {v0, v4, v5}, Lcom/android/okhttp/internal/http/MultiratUtil;->getRequestLine(Lcom/android/okhttp/Request;Ljava/net/Proxy$Type;I)Ljava/lang/String;

    move-result-object v38

    .line 730
    .local v38, "requestLine":Ljava/lang/String;
    move-object/from16 v0, v22

    move-object/from16 v1, v38

    invoke-static {v0, v1}, Lcom/android/okhttp/internal/http/MultiratUtil;->getRequestHeadersBytes(Lcom/android/okhttp/Headers;Ljava/lang/String;)[B

    move-result-object v23

    .line 732
    .local v23, "headBytes":[B
    :try_start_1
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_7

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v5, Ljava/lang/String;

    move-object/from16 v0, v23

    invoke-direct {v5, v0}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_7

    .line 736
    :cond_7
    :goto_3
    :try_start_2
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_8

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "connecting socketName:"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v41

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ", oriSSL:"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/Connection;->ssl_oriReq:Lcom/android/okhttp/Connection$SSLInfo;

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ", redSSL:"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/Connection;->ssl_redirect:Lcom/android/okhttp/Connection$SSLInfo;

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 737
    :cond_8
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->multiSockDestAddr:[Ljava/net/InetAddress;

    aget-object v4, v4, p1

    if-eqz v4, :cond_13

    .line 739
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->multiSockDestAddr:[Ljava/net/InetAddress;

    aget-object v4, v4, p1

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/android/okhttp/Connection;->getIPVersion(Ljava/net/InetAddress;)I

    move-result v4

    move/from16 v0, p1

    invoke-static {v0, v4}, Lcom/android/okhttp/internal/http/SBServiceAPI;->getSBInterface(II)Ljava/net/InetAddress;

    move-result-object v8

    .line 741
    if-eqz p10, :cond_9

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/android/okhttp/Connection;->startExtremSocketCreation:J
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    .line 744
    :cond_9
    :try_start_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->multiSockDestAddr:[Ljava/net/InetAddress;

    aget-object v4, v4, p1

    move-object/from16 v0, p0

    invoke-direct {v0, v8, v4}, Lcom/android/okhttp/Connection;->matchIPver(Ljava/net/InetAddress;Ljava/net/InetAddress;)Z

    move-result v4

    if-nez v4, :cond_d

    .line 745
    invoke-static {v11}, Lcom/android/okhttp/internal/Util;->closeQuietly(Ljava/net/Socket;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    .line 746
    const/4 v11, 0x0

    .line 747
    const/16 v32, 0x0

    .line 901
    .end local v9    # "curProxy":Ljava/net/Proxy;
    .end local v16    # "bProxyChanged":Z
    .end local v22    # "h":Lcom/android/okhttp/Headers;
    .end local v23    # "headBytes":[B
    .end local v30    # "newUri":Ljava/net/URI;
    .end local v38    # "requestLine":Ljava/lang/String;
    :cond_a
    :goto_4
    return-object v11

    .line 683
    .end local v24    # "host":Ljava/lang/String;
    .end local v28    # "location":Ljava/lang/String;
    .end local v29    # "newHeader":Lcom/android/okhttp/Request;
    .end local v31    # "newUrl":Ljava/net/URL;
    .end local v32    # "notOkResponse":Z
    .end local v34    # "previousUrl":Ljava/net/URL;
    .end local v35    # "range":Ljava/lang/StringBuffer;
    .end local v36    # "redirectionCount":I
    .end local v37    # "requestHeaders":Lcom/android/okhttp/Request;
    .end local v40    # "rsp":Lcom/android/okhttp/Response;
    .end local v41    # "socketName":Ljava/lang/String;
    .end local v44    # "uri":Ljava/net/URI;
    :cond_b
    invoke-virtual {v8}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v41

    goto/16 :goto_0

    .line 716
    .restart local v24    # "host":Ljava/lang/String;
    .restart local v28    # "location":Ljava/lang/String;
    .restart local v29    # "newHeader":Lcom/android/okhttp/Request;
    .restart local v30    # "newUri":Ljava/net/URI;
    .restart local v31    # "newUrl":Ljava/net/URL;
    .restart local v32    # "notOkResponse":Z
    .restart local v34    # "previousUrl":Ljava/net/URL;
    .restart local v35    # "range":Ljava/lang/StringBuffer;
    .restart local v36    # "redirectionCount":I
    .restart local v37    # "requestHeaders":Lcom/android/okhttp/Request;
    .restart local v40    # "rsp":Lcom/android/okhttp/Response;
    .restart local v41    # "socketName":Ljava/lang/String;
    .restart local v44    # "uri":Ljava/net/URI;
    :catch_0
    move-exception v18

    .line 717
    .local v18, "ex":Ljava/lang/Exception;
    new-instance v4, Ljava/io/IOException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Cannot parse URL: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v31

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 722
    .end local v18    # "ex":Ljava/lang/Exception;
    .restart local v9    # "curProxy":Ljava/net/Proxy;
    .restart local v22    # "h":Lcom/android/okhttp/Headers;
    :cond_c
    const/16 v16, 0x0

    goto/16 :goto_2

    .line 751
    .restart local v16    # "bProxyChanged":Z
    .restart local v23    # "headBytes":[B
    .restart local v38    # "requestLine":Ljava/lang/String;
    :catch_1
    move-exception v19

    .line 752
    .local v19, "exx":Ljava/lang/Throwable;
    :try_start_4
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_d

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V

    .line 754
    .end local v19    # "exx":Ljava/lang/Throwable;
    :cond_d
    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/okhttp/Connection;->connTimeout:I

    const/4 v4, 0x1

    new-array v6, v4, [Ljava/net/InetAddress;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/Connection;->multiSockDestAddr:[Ljava/net/InetAddress;

    aget-object v7, v7, p1

    aput-object v7, v6, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->destPorts:[I

    aget v7, v4, p1

    move-object/from16 v4, p0

    invoke-direct/range {v4 .. v9}, Lcom/android/okhttp/Connection;->connect(I[Ljava/net/InetAddress;ILjava/net/InetAddress;Ljava/net/Proxy;)Ljava/net/Socket;
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    move-result-object v11

    .line 790
    :goto_5
    :try_start_5
    const-string v4, "https"

    invoke-virtual/range {v30 .. v30}, Ljava/net/URI;->getScheme()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_e

    .line 791
    invoke-virtual/range {v30 .. v30}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->destPorts:[I

    aget v14, v4, p1

    invoke-virtual/range {p2 .. p2}, Lcom/android/okhttp/Request;->uri()Ljava/net/URI;

    move-result-object v4

    invoke-virtual {v4}, Ljava/net/URI;->toURL()Ljava/net/URL;

    move-result-object v4

    move-object/from16 v0, v31

    invoke-virtual {v0, v4}, Ljava/net/URL;->equals(Ljava/lang/Object;)Z

    move-result v15

    move-object/from16 v10, p0

    move-object v12, v9

    invoke-direct/range {v10 .. v15}, Lcom/android/okhttp/Connection;->upgradeToTlsForSecChunk(Ljava/net/Socket;Ljava/net/Proxy;Ljava/lang/String;IZ)Ljava/net/Socket;

    move-result-object v11

    .line 792
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_e

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "upgrade to Tls Socket "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    .line 803
    :cond_e
    :try_start_6
    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/okhttp/Connection;->mainTimeout:I
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    if-lez v4, :cond_f

    .line 805
    :try_start_7
    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/okhttp/Connection;->mainTimeout:I

    invoke-virtual {v11, v4}, Ljava/net/Socket;->setSoTimeout(I)V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_6
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    .line 809
    :cond_f
    :goto_6
    :try_start_8
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_10

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "inside try after extrem connecting with:"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v41

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ", socket addr is "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " for net type "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ", startExtremSocketCreation is "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/android/okhttp/Connection;->startExtremSocketCreation:J

    invoke-virtual {v5, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3

    .line 820
    :cond_10
    invoke-virtual {v11}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v26

    .line 821
    .local v26, "in":Ljava/io/InputStream;
    invoke-virtual {v11}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v33

    .line 823
    .local v33, "out":Ljava/io/OutputStream;
    move-object/from16 v0, v33

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V

    .line 825
    invoke-static/range {v26 .. v26}, Lcom/android/okhttp/internal/http/MultiratUtil;->readResponseHeaders(Ljava/io/InputStream;)Lcom/android/okhttp/Response$Builder;

    move-result-object v4

    move-object/from16 v0, v29

    invoke-virtual {v4, v0}, Lcom/android/okhttp/Response$Builder;->request(Lcom/android/okhttp/Request;)Lcom/android/okhttp/Response$Builder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/okhttp/Response$Builder;->build()Lcom/android/okhttp/Response;

    move-result-object v40

    .line 827
    :try_start_9
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_11

    .line 828
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "get response "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {v40 .. v40}, Lcom/android/okhttp/Response;->statusLine()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 829
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    invoke-virtual/range {v40 .. v40}, Lcom/android/okhttp/Response;->headers()Lcom/android/okhttp/Headers;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/okhttp/Headers;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->d(Ljava/lang/String;)V
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_5

    .line 833
    :cond_11
    :goto_7
    invoke-virtual/range {v40 .. v40}, Lcom/android/okhttp/Response;->code()I

    move-result v39

    .line 834
    .local v39, "responseCode":I
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_12

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "HttpConnection startOffset:"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, p3

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " endOffset:"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, p5

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " response code: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v39

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 835
    :cond_12
    sparse-switch v39, :sswitch_data_0

    .line 893
    invoke-virtual/range {v26 .. v26}, Ljava/io/InputStream;->close()V

    .line 894
    invoke-virtual/range {v33 .. v33}, Ljava/io/OutputStream;->close()V

    .line 895
    invoke-virtual {v11}, Ljava/net/Socket;->close()V

    .line 896
    const/4 v11, 0x0

    .line 897
    const/16 v32, 0x0

    goto/16 :goto_1

    .line 757
    .end local v26    # "in":Ljava/io/InputStream;
    .end local v33    # "out":Ljava/io/OutputStream;
    .end local v39    # "responseCode":I
    :cond_13
    :try_start_a
    invoke-virtual/range {v30 .. v30}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v0, v1, v4}, Lcom/android/okhttp/Connection;->getAllByName(ILjava/lang/String;)[Ljava/net/InetAddress;

    move-result-object v6

    .line 760
    .local v6, "addresses":[Ljava/net/InetAddress;
    if-eqz v6, :cond_14

    array-length v4, v6

    if-lez v4, :cond_14

    .line 761
    const/4 v4, 0x0

    aget-object v4, v6, v4

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/android/okhttp/Connection;->getIPVersion(Ljava/net/InetAddress;)I

    move-result v4

    move/from16 v0, p1

    invoke-static {v0, v4}, Lcom/android/okhttp/internal/http/SBServiceAPI;->getSBInterface(II)Ljava/net/InetAddress;

    move-result-object v8

    .line 764
    :cond_14
    new-instance v27, Ljava/util/ArrayList;

    invoke-direct/range {v27 .. v27}, Ljava/util/ArrayList;-><init>()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_3

    .line 767
    .local v27, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/net/InetAddress;>;"
    const/16 v25, 0x0

    .local v25, "i":I
    :goto_8
    :try_start_b
    array-length v4, v6

    move/from16 v0, v25

    if-ge v0, v4, :cond_16

    .line 768
    aget-object v4, v6, v25

    move-object/from16 v0, p0

    invoke-direct {v0, v8, v4}, Lcom/android/okhttp/Connection;->matchIPver(Ljava/net/InetAddress;Ljava/net/InetAddress;)Z

    move-result v4

    if-eqz v4, :cond_15

    .line 769
    aget-object v4, v6, v25

    move-object/from16 v0, v27

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 767
    :cond_15
    add-int/lit8 v25, v25, 0x1

    goto :goto_8

    .line 772
    :cond_16
    invoke-virtual/range {v27 .. v27}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_17

    .line 773
    invoke-static {v11}, Lcom/android/okhttp/internal/Util;->closeQuietly(Ljava/net/Socket;)V

    .line 774
    const/4 v11, 0x0

    .line 775
    const/16 v32, 0x0

    goto/16 :goto_4

    .line 779
    :cond_17
    const/4 v4, 0x0

    new-array v4, v4, [Ljava/net/InetAddress;

    move-object/from16 v0, v27

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, [Ljava/net/InetAddress;

    move-object v6, v0

    .line 781
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_18

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "server ip number after filtering with IP version is "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    array-length v7, v6

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V
    :try_end_b
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_2
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_3

    .line 786
    :cond_18
    :goto_9
    if-eqz p10, :cond_19

    :try_start_c
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/android/okhttp/Connection;->startExtremSocketCreation:J

    .line 787
    :cond_19
    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/okhttp/Connection;->connTimeout:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->destPorts:[I

    aget v7, v4, p1

    move-object/from16 v4, p0

    invoke-direct/range {v4 .. v9}, Lcom/android/okhttp/Connection;->connect(I[Ljava/net/InetAddress;ILjava/net/InetAddress;Ljava/net/Proxy;)Ljava/net/Socket;

    move-result-object v11

    goto/16 :goto_5

    .line 783
    :catch_2
    move-exception v19

    .line 784
    .restart local v19    # "exx":Ljava/lang/Throwable;
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_18

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_3

    goto :goto_9

    .line 813
    .end local v6    # "addresses":[Ljava/net/InetAddress;
    .end local v19    # "exx":Ljava/lang/Throwable;
    .end local v25    # "i":I
    .end local v27    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/net/InetAddress;>;"
    :catch_3
    move-exception v17

    .line 814
    .local v17, "e":Ljava/io/IOException;
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_1a

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "connecting socketName insside exception:"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v41

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 815
    :cond_1a
    throw v17

    .line 795
    .end local v17    # "e":Ljava/io/IOException;
    :catch_4
    move-exception v18

    .line 796
    .local v18, "ex":Ljava/io/IOException;
    :try_start_d
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_1b

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V

    .line 797
    :cond_1b
    invoke-static {v11}, Lcom/android/okhttp/internal/Util;->closeQuietly(Ljava/net/Socket;)V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_3

    .line 798
    const/4 v11, 0x0

    .line 799
    const/16 v32, 0x0

    goto/16 :goto_4

    .line 840
    .end local v18    # "ex":Ljava/io/IOException;
    .restart local v26    # "in":Ljava/io/InputStream;
    .restart local v33    # "out":Ljava/io/OutputStream;
    .restart local v39    # "responseCode":I
    :sswitch_0
    add-int/lit8 v36, v36, 0x1

    sget v4, Lcom/android/okhttp/internal/http/HttpEngine;->MAX_REDIRECTS:I

    move/from16 v0, v36

    if-le v0, v4, :cond_1c

    .line 841
    new-instance v4, Ljava/io/IOException;

    const-string v5, "Too many redirects"

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 843
    :cond_1c
    invoke-virtual/range {v40 .. v40}, Lcom/android/okhttp/Response;->headers()Lcom/android/okhttp/Headers;

    move-result-object v4

    const-string v5, "Location"

    invoke-virtual {v4, v5}, Lcom/android/okhttp/Headers;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v28

    .line 844
    if-nez v28, :cond_1d

    .line 845
    invoke-virtual/range {v26 .. v26}, Ljava/io/InputStream;->close()V

    .line 846
    invoke-virtual/range {v33 .. v33}, Ljava/io/OutputStream;->close()V

    .line 847
    invoke-virtual {v11}, Ljava/net/Socket;->close()V

    .line 848
    const/4 v11, 0x0

    .line 849
    const/16 v32, 0x0

    .line 850
    goto/16 :goto_1

    .line 852
    :cond_1d
    move-object/from16 v34, v31

    .line 853
    new-instance v31, Ljava/net/URL;

    .end local v31    # "newUrl":Ljava/net/URL;
    move-object/from16 v0, v31

    move-object/from16 v1, v34

    move-object/from16 v2, v28

    invoke-direct {v0, v1, v2}, Ljava/net/URL;-><init>(Ljava/net/URL;Ljava/lang/String;)V

    .line 854
    .restart local v31    # "newUrl":Ljava/net/URL;
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_1e

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "redirected to "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {v31 .. v31}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ", prevPro="

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {v34 .. v34}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ", newPro="

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {v31 .. v31}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 855
    :cond_1e
    const-string v4, "https"

    invoke-virtual/range {v31 .. v31}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_21

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->ssl_oriReq:Lcom/android/okhttp/Connection$SSLInfo;

    iget-boolean v4, v4, Lcom/android/okhttp/Connection$SSLInfo;->isSSL:Z

    if-nez v4, :cond_21

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->ssl_redirect:Lcom/android/okhttp/Connection$SSLInfo;

    if-eqz v4, :cond_1f

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->ssl_redirect:Lcom/android/okhttp/Connection$SSLInfo;

    iget-boolean v4, v4, Lcom/android/okhttp/Connection$SSLInfo;->isSSL:Z

    if-nez v4, :cond_21

    .line 856
    :cond_1f
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_20

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    const-string v5, "redirected to a diffrent schema, not support"

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 857
    :cond_20
    invoke-virtual/range {v26 .. v26}, Ljava/io/InputStream;->close()V

    .line 858
    invoke-virtual/range {v33 .. v33}, Ljava/io/OutputStream;->close()V

    .line 859
    invoke-virtual {v11}, Ljava/net/Socket;->close()V

    .line 860
    const/4 v11, 0x0

    .line 861
    const/16 v32, 0x0

    .line 862
    goto/16 :goto_1

    .line 865
    :cond_21
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->multiSockDestAddr:[Ljava/net/InetAddress;

    const/4 v5, 0x0

    aput-object v5, v4, p1

    .line 866
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->multiSocketStream:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-object v4, v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->sRedirectedRequestHeaders:[Lcom/android/okhttp/Request;

    const/4 v5, 0x0

    aput-object v5, v4, p1

    .line 867
    invoke-virtual/range {v26 .. v26}, Ljava/io/InputStream;->close()V

    .line 868
    invoke-virtual/range {v33 .. v33}, Ljava/io/OutputStream;->close()V

    .line 869
    invoke-virtual {v11}, Ljava/net/Socket;->close()V

    goto/16 :goto_1

    .line 873
    :sswitch_1
    monitor-enter p0

    .line 875
    :try_start_e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->multiSocketStream:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-object v4, v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->sRedirectedRequestHeaders:[Lcom/android/okhttp/Request;

    aget-object v4, v4, p1

    if-nez v4, :cond_22

    .line 876
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->multiSocketStream:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-object v4, v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->sRedirectedRequestHeaders:[Lcom/android/okhttp/Request;

    aput-object v29, v4, p1

    .line 877
    :cond_22
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->multiSockDestAddr:[Ljava/net/InetAddress;

    aget-object v4, v4, p1

    if-nez v4, :cond_23

    .line 878
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->multiSockDestAddr:[Ljava/net/InetAddress;

    invoke-virtual {v11}, Ljava/net/Socket;->getInetAddress()Ljava/net/InetAddress;

    move-result-object v5

    aput-object v5, v4, p1

    .line 879
    :cond_23
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_24

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "set socket dest IP after extrem connecting with:"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v41

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ", socket addr is "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/Connection;->multiSockDestAddr:[Ljava/net/InetAddress;

    aget-object v7, v7, p1

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " for net type "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 881
    :cond_24
    invoke-static/range {v40 .. v40}, Lcom/android/okhttp/internal/http/OkHeaders;->contentLength(Lcom/android/okhttp/Response;)J

    move-result-wide v42

    .line 882
    .local v42, "sampleRequestSize":J
    invoke-virtual/range {v40 .. v40}, Lcom/android/okhttp/Response;->headers()Lcom/android/okhttp/Headers;

    move-result-object v4

    const-wide/16 v12, -0x1

    invoke-static {v4, v12, v13}, Lcom/android/okhttp/internal/http/MultiratUtil;->getFullContentLength(Lcom/android/okhttp/Headers;J)J

    move-result-wide v20

    .line 883
    .local v20, "fullContentLength":J
    sub-long v4, p5, p3

    const-wide/16 v12, 0x1

    add-long/2addr v4, v12

    cmp-long v4, v42, v4

    if-nez v4, :cond_25

    cmp-long v4, v20, p7

    if-eqz v4, :cond_26

    .line 884
    :cond_25
    invoke-virtual/range {v26 .. v26}, Ljava/io/InputStream;->close()V

    .line 885
    invoke-virtual/range {v33 .. v33}, Ljava/io/OutputStream;->close()V

    .line 886
    invoke-virtual {v11}, Ljava/net/Socket;->close()V

    .line 887
    const/4 v11, 0x0

    .line 889
    :cond_26
    monitor-exit p0

    .line 890
    const/16 v32, 0x0

    .line 891
    goto/16 :goto_1

    .line 889
    .end local v20    # "fullContentLength":J
    .end local v42    # "sampleRequestSize":J
    :catchall_0
    move-exception v4

    monitor-exit p0
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    throw v4

    .line 832
    .end local v39    # "responseCode":I
    :catch_5
    move-exception v4

    goto/16 :goto_7

    .line 807
    .end local v26    # "in":Ljava/io/InputStream;
    .end local v33    # "out":Ljava/io/OutputStream;
    :catch_6
    move-exception v4

    goto/16 :goto_6

    .line 734
    :catch_7
    move-exception v4

    goto/16 :goto_3

    .line 835
    :sswitch_data_0
    .sparse-switch
        0xc8 -> :sswitch_1
        0xce -> :sswitch_1
        0x12c -> :sswitch_0
        0x12d -> :sswitch_0
        0x12e -> :sswitch_0
        0x12f -> :sswitch_0
    .end sparse-switch
.end method

.method private getAllByName(ILjava/lang/String;)[Ljava/net/InetAddress;
    .locals 10
    .param p1, "netType"    # I
    .param p2, "host"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/UnknownHostException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 480
    new-array v2, v3, [Ljava/net/InetAddress;

    .line 482
    .local v2, "ret":[Ljava/net/InetAddress;
    const/4 v6, 0x1

    if-ne p1, v6, :cond_3

    .line 483
    :try_start_0
    invoke-direct {p0, p1}, Lcom/android/okhttp/Connection;->getIPStyle(I)I

    move-result v1

    .line 484
    .local v1, "ipStyle":I
    sget-boolean v6, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "start to get IP for host with new style("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ") "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " at time "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 485
    :cond_0
    const-wide/16 v4, 0x0

    .line 486
    .local v4, "threadID":J
    iget-object v6, p0, Lcom/android/okhttp/Connection;->multiSocketStream:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    if-eqz v6, :cond_1

    .line 487
    iget-object v6, p0, Lcom/android/okhttp/Connection;->multiSocketStream:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    invoke-virtual {v6}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->getMainThreadID()J

    move-result-wide v4

    .line 488
    :cond_1
    iget-boolean v6, p0, Lcom/android/okhttp/Connection;->isSSL:Z

    invoke-static {v4, v5, p2, v1, v6}, Lcom/android/okhttp/internal/http/SBServiceAPI;->getAddrsByHost(JLjava/lang/String;IZ)[Ljava/net/InetAddress;
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 499
    .end local v1    # "ipStyle":I
    .end local v4    # "threadID":J
    :goto_0
    sget-boolean v6, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "finish to get IP for host "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " at time "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", result number "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    if-nez v2, :cond_6

    :goto_1
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v3}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 500
    :cond_2
    return-object v2

    .line 491
    :cond_3
    :try_start_1
    sget-boolean v6, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v6, :cond_4

    iget-object v6, p0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "start to get IP for host with default "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " at time "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 492
    :cond_4
    invoke-static {p2}, Ljava/net/InetAddress;->getAllByName(Ljava/lang/String;)[Ljava/net/InetAddress;
    :try_end_1
    .catch Ljava/net/UnknownHostException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v2

    goto :goto_0

    .line 495
    :catch_0
    move-exception v0

    .line 496
    .local v0, "ex":Ljava/net/UnknownHostException;
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    invoke-virtual {v3, v0}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V

    .line 497
    :cond_5
    throw v0

    .line 499
    .end local v0    # "ex":Ljava/net/UnknownHostException;
    :cond_6
    array-length v3, v2

    goto :goto_1
.end method

.method private getIPStyle(I)I
    .locals 5
    .param p1, "netType"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 231
    if-nez p1, :cond_1

    .line 240
    :cond_0
    :goto_0
    return v2

    .line 232
    :cond_1
    invoke-virtual {p0, v2}, Lcom/android/okhttp/Connection;->getLocalAddr(I)Ljava/net/InetAddress;

    move-result-object v0

    .line 233
    .local v0, "addr0":Ljava/net/InetAddress;
    invoke-virtual {p0, v3}, Lcom/android/okhttp/Connection;->getLocalAddr(I)Ljava/net/InetAddress;

    move-result-object v1

    .line 234
    .local v1, "addr1":Ljava/net/InetAddress;
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 235
    instance-of v4, v0, Ljava/net/Inet4Address;

    if-eqz v4, :cond_2

    instance-of v4, v1, Ljava/net/Inet4Address;

    if-nez v4, :cond_2

    move v2, v3

    .line 236
    goto :goto_0

    .line 237
    :cond_2
    instance-of v4, v1, Ljava/net/Inet4Address;

    if-eqz v4, :cond_0

    instance-of v4, v0, Ljava/net/Inet4Address;

    if-nez v4, :cond_0

    move v2, v3

    .line 238
    goto :goto_0
.end method

.method private getIPVersion(Ljava/net/InetAddress;)I
    .locals 4
    .param p1, "addr"    # Ljava/net/InetAddress;

    .prologue
    .line 1498
    const/4 v0, 0x0

    .line 1500
    .local v0, "ipVersion":I
    instance-of v1, p1, Ljava/net/Inet6Address;

    if-eqz v1, :cond_1

    .line 1501
    const/4 v0, 0x1

    .line 1506
    :goto_0
    sget-boolean v1, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getIPVersion = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1508
    :cond_0
    return v0

    .line 1503
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getProxy(ILjava/lang/String;)Ljava/net/Proxy;
    .locals 7
    .param p1, "netType"    # I
    .param p2, "host"    # Ljava/lang/String;

    .prologue
    .line 244
    const/4 v2, 0x0

    .line 246
    .local v2, "ret":Ljava/net/Proxy;
    :try_start_0
    invoke-static {p1}, Lcom/android/okhttp/internal/http/SBServiceAPI;->getHttpProxy(I)[Ljava/lang/String;

    move-result-object v3

    .line 247
    .local v3, "sProxyInfo":[Ljava/lang/String;
    array-length v4, v3

    if-nez v4, :cond_0

    .line 248
    sget-object v2, Ljava/net/Proxy;->NO_PROXY:Ljava/net/Proxy;

    .line 266
    .end local v3    # "sProxyInfo":[Ljava/lang/String;
    :goto_0
    return-object v2

    .line 251
    .restart local v3    # "sProxyInfo":[Ljava/lang/String;
    :cond_0
    new-instance v1, Ljava/net/InetSocketAddress;

    const/4 v4, 0x0

    aget-object v4, v3, v4

    const/4 v5, 0x1

    aget-object v5, v3, v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-direct {v1, v4, v5}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    .line 252
    .local v1, "inetSocketAddress":Ljava/net/InetSocketAddress;
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "get Proxy "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " for network "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 253
    :cond_1
    const/4 v4, 0x2

    aget-object v4, v3, v4

    invoke-direct {p0, v4, p2}, Lcom/android/okhttp/Connection;->isExcluded(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 254
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "host "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " is in the exclusion list "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x2

    aget-object v6, v3, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 255
    :cond_2
    sget-object v2, Ljava/net/Proxy;->NO_PROXY:Ljava/net/Proxy;

    goto :goto_0

    .line 258
    :cond_3
    new-instance v2, Ljava/net/Proxy;

    .end local v2    # "ret":Ljava/net/Proxy;
    sget-object v4, Ljava/net/Proxy$Type;->HTTP:Ljava/net/Proxy$Type;

    invoke-direct {v2, v4, v1}, Ljava/net/Proxy;-><init>(Ljava/net/Proxy$Type;Ljava/net/SocketAddress;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .restart local v2    # "ret":Ljava/net/Proxy;
    goto :goto_0

    .line 262
    .end local v1    # "inetSocketAddress":Ljava/net/InetSocketAddress;
    .end local v2    # "ret":Ljava/net/Proxy;
    .end local v3    # "sProxyInfo":[Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 263
    .local v0, "e":Ljava/lang/Throwable;
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    invoke-virtual {v4, v0}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V

    .line 264
    :cond_4
    sget-object v2, Ljava/net/Proxy;->NO_PROXY:Ljava/net/Proxy;

    .restart local v2    # "ret":Ljava/net/Proxy;
    goto :goto_0
.end method

.method private isExcluded(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 9
    .param p1, "exclusionList"    # Ljava/lang/String;
    .param p2, "urlDomain"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 270
    const/4 v1, 0x0

    .line 271
    .local v1, "mParsedExclusionList":[Ljava/lang/String;
    if-nez p1, :cond_3

    .line 272
    new-array v1, v5, [Ljava/lang/String;

    .line 285
    :cond_0
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_1

    if-eqz v1, :cond_1

    array-length v6, v1

    if-nez v6, :cond_5

    :cond_1
    move v4, v5

    .line 294
    :cond_2
    :goto_0
    return v4

    .line 275
    :cond_3
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    const-string v7, ","

    invoke-virtual {v6, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 276
    .local v3, "splitExclusionList":[Ljava/lang/String;
    array-length v6, v3

    mul-int/lit8 v6, v6, 0x2

    new-array v1, v6, [Ljava/lang/String;

    .line 277
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v6, v3

    if-ge v0, v6, :cond_0

    .line 278
    aget-object v6, v3, v0

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 279
    .local v2, "s":Ljava/lang/String;
    const-string v6, "."

    invoke-virtual {v2, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-virtual {v2, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 280
    :cond_4
    mul-int/lit8 v6, v0, 0x2

    aput-object v2, v1, v6

    .line 281
    mul-int/lit8 v6, v0, 0x2

    add-int/lit8 v6, v6, 0x1

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v1, v6

    .line 277
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 289
    .end local v0    # "i":I
    .end local v2    # "s":Ljava/lang/String;
    .end local v3    # "splitExclusionList":[Ljava/lang/String;
    :cond_5
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_2
    array-length v6, v1

    if-ge v0, v6, :cond_6

    .line 290
    aget-object v6, v1, v0

    invoke-virtual {p2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    add-int/lit8 v6, v0, 0x1

    aget-object v6, v1, v6

    invoke-virtual {p2, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 289
    add-int/lit8 v0, v0, 0x2

    goto :goto_2

    :cond_6
    move v4, v5

    .line 294
    goto :goto_0
.end method

.method private isSameProxy(Ljava/net/Proxy;Ljava/net/Proxy;)Z
    .locals 5
    .param p1, "p1"    # Ljava/net/Proxy;
    .param p2, "p2"    # Ljava/net/Proxy;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 299
    if-nez p1, :cond_1

    if-nez p2, :cond_1

    .line 314
    :cond_0
    :goto_0
    return v1

    .line 301
    :cond_1
    if-eqz p1, :cond_4

    if-eqz p2, :cond_4

    .line 302
    :try_start_0
    invoke-virtual {p1}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v3

    sget-object v4, Ljava/net/Proxy$Type;->DIRECT:Ljava/net/Proxy$Type;

    if-ne v3, v4, :cond_2

    invoke-virtual {p2}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v3

    sget-object v4, Ljava/net/Proxy$Type;->DIRECT:Ljava/net/Proxy$Type;

    if-eq v3, v4, :cond_0

    .line 304
    :cond_2
    invoke-virtual {p1}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v1

    sget-object v3, Ljava/net/Proxy$Type;->DIRECT:Ljava/net/Proxy$Type;

    if-eq v1, v3, :cond_3

    invoke-virtual {p2}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v1

    sget-object v3, Ljava/net/Proxy$Type;->DIRECT:Ljava/net/Proxy$Type;

    if-eq v1, v3, :cond_3

    .line 305
    invoke-virtual {p1, p2}, Ljava/net/Proxy;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    :cond_3
    move v1, v2

    .line 308
    goto :goto_0

    :cond_4
    move v1, v2

    .line 311
    goto :goto_0

    .line 313
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/lang/Throwable;
    move v1, v2

    .line 314
    goto :goto_0
.end method

.method private makeTunnel(Lcom/android/okhttp/TunnelRequest;Lcom/android/okhttp/Route;Ljava/net/Socket;)V
    .locals 8
    .param p1, "tunnelRequest"    # Lcom/android/okhttp/TunnelRequest;
    .param p2, "route"    # Lcom/android/okhttp/Route;
    .param p3, "socket"    # Ljava/net/Socket;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1464
    new-instance v3, Lcom/android/okhttp/internal/http/HttpConnection;

    iget-object v4, p0, Lcom/android/okhttp/Connection;->pool:Lcom/android/okhttp/ConnectionPool;

    invoke-direct {v3, v4, p0, p3}, Lcom/android/okhttp/internal/http/HttpConnection;-><init>(Lcom/android/okhttp/ConnectionPool;Lcom/android/okhttp/Connection;Ljava/net/Socket;)V

    .line 1465
    .local v3, "tunnelConnection":Lcom/android/okhttp/internal/http/HttpConnection;
    invoke-virtual {p1}, Lcom/android/okhttp/TunnelRequest;->getRequest()Lcom/android/okhttp/Request;

    move-result-object v0

    .line 1466
    .local v0, "request":Lcom/android/okhttp/Request;
    invoke-virtual {p1}, Lcom/android/okhttp/TunnelRequest;->requestLine()Ljava/lang/String;

    move-result-object v1

    .line 1468
    .local v1, "requestLine":Ljava/lang/String;
    :cond_0
    invoke-virtual {v0}, Lcom/android/okhttp/Request;->headers()Lcom/android/okhttp/Headers;

    move-result-object v4

    invoke-virtual {v3, v4, v1}, Lcom/android/okhttp/internal/http/HttpConnection;->writeRequest(Lcom/android/okhttp/Headers;Ljava/lang/String;)V

    .line 1469
    invoke-virtual {v3}, Lcom/android/okhttp/internal/http/HttpConnection;->flush()V

    .line 1470
    invoke-virtual {v3}, Lcom/android/okhttp/internal/http/HttpConnection;->readResponse()Lcom/android/okhttp/Response$Builder;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/android/okhttp/Response$Builder;->request(Lcom/android/okhttp/Request;)Lcom/android/okhttp/Response$Builder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/okhttp/Response$Builder;->build()Lcom/android/okhttp/Response;

    move-result-object v2

    .line 1471
    .local v2, "response":Lcom/android/okhttp/Response;
    invoke-virtual {v3}, Lcom/android/okhttp/internal/http/HttpConnection;->emptyResponseBody()V

    .line 1473
    invoke-virtual {v2}, Lcom/android/okhttp/Response;->code()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    .line 1489
    new-instance v4, Ljava/io/IOException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unexpected response code for CONNECT: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Lcom/android/okhttp/Response;->code()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 1477
    :sswitch_0
    invoke-virtual {v3}, Lcom/android/okhttp/internal/http/HttpConnection;->bufferSize()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-lez v4, :cond_1

    .line 1478
    new-instance v4, Ljava/io/IOException;

    const-string v5, "TLS tunnel buffered too many bytes!"

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 1483
    :sswitch_1
    iget-object v4, p2, Lcom/android/okhttp/Route;->address:Lcom/android/okhttp/Address;

    iget-object v4, v4, Lcom/android/okhttp/Address;->authenticator:Lcom/android/okhttp/OkAuthenticator;

    iget-object v5, p2, Lcom/android/okhttp/Route;->proxy:Ljava/net/Proxy;

    invoke-static {v4, v2, v5}, Lcom/android/okhttp/internal/http/HttpAuthenticator;->processAuthHeader(Lcom/android/okhttp/OkAuthenticator;Lcom/android/okhttp/Response;Ljava/net/Proxy;)Lcom/android/okhttp/Request;

    move-result-object v0

    .line 1485
    if-nez v0, :cond_0

    .line 1486
    new-instance v4, Ljava/io/IOException;

    const-string v5, "Failed to authenticate with proxy"

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 1480
    :cond_1
    return-void

    .line 1473
    nop

    :sswitch_data_0
    .sparse-switch
        0xc8 -> :sswitch_0
        0x197 -> :sswitch_1
    .end sparse-switch
.end method

.method private matchIPver(Ljava/net/InetAddress;Ljava/net/InetAddress;)Z
    .locals 3
    .param p1, "addr1"    # Ljava/net/InetAddress;
    .param p2, "addr2"    # Ljava/net/InetAddress;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 653
    if-nez p1, :cond_1

    .line 657
    :cond_0
    :goto_0
    return v0

    .line 654
    :cond_1
    if-nez p2, :cond_2

    move v0, v1

    goto :goto_0

    .line 655
    :cond_2
    instance-of v2, p1, Ljava/net/Inet4Address;

    if-eqz v2, :cond_3

    instance-of v2, p2, Ljava/net/Inet4Address;

    if-nez v2, :cond_0

    .line 656
    :cond_3
    instance-of v2, p1, Ljava/net/Inet6Address;

    if-eqz v2, :cond_4

    instance-of v2, p2, Ljava/net/Inet6Address;

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    .line 657
    goto :goto_0
.end method

.method private upgradeToTls(Lcom/android/okhttp/TunnelRequest;)V
    .locals 12
    .param p1, "tunnelRequest"    # Lcom/android/okhttp/TunnelRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v11, 0x1

    .line 1210
    invoke-static {}, Lcom/android/okhttp/internal/Platform;->get()Lcom/android/okhttp/internal/Platform;

    move-result-object v2

    .line 1213
    .local v2, "platform":Lcom/android/okhttp/internal/Platform;
    sget-boolean v7, Lcom/android/okhttp/internal/http/HttpEngine;->SMARTBONDING_ENABLED:Z

    if-eqz v7, :cond_0

    .line 1214
    iput-boolean v11, p0, Lcom/android/okhttp/Connection;->isSSL:Z

    .line 1219
    :cond_0
    invoke-virtual {p0}, Lcom/android/okhttp/Connection;->requiresTunnel()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1221
    iget-object v7, p0, Lcom/android/okhttp/Connection;->route:Lcom/android/okhttp/Route;

    iget-object v8, p0, Lcom/android/okhttp/Connection;->socket:Ljava/net/Socket;

    invoke-direct {p0, p1, v7, v8}, Lcom/android/okhttp/Connection;->makeTunnel(Lcom/android/okhttp/TunnelRequest;Lcom/android/okhttp/Route;Ljava/net/Socket;)V

    .line 1226
    :cond_1
    iget-object v7, p0, Lcom/android/okhttp/Connection;->route:Lcom/android/okhttp/Route;

    iget-object v7, v7, Lcom/android/okhttp/Route;->address:Lcom/android/okhttp/Address;

    iget-object v7, v7, Lcom/android/okhttp/Address;->sslSocketFactory:Ljavax/net/ssl/SSLSocketFactory;

    iget-object v8, p0, Lcom/android/okhttp/Connection;->socket:Ljava/net/Socket;

    iget-object v9, p0, Lcom/android/okhttp/Connection;->route:Lcom/android/okhttp/Route;

    iget-object v9, v9, Lcom/android/okhttp/Route;->address:Lcom/android/okhttp/Address;

    iget-object v9, v9, Lcom/android/okhttp/Address;->uriHost:Ljava/lang/String;

    iget-object v10, p0, Lcom/android/okhttp/Connection;->route:Lcom/android/okhttp/Route;

    iget-object v10, v10, Lcom/android/okhttp/Route;->address:Lcom/android/okhttp/Address;

    iget v10, v10, Lcom/android/okhttp/Address;->uriPort:I

    invoke-virtual {v7, v8, v9, v10, v11}, Ljavax/net/ssl/SSLSocketFactory;->createSocket(Ljava/net/Socket;Ljava/lang/String;IZ)Ljava/net/Socket;

    move-result-object v7

    iput-object v7, p0, Lcom/android/okhttp/Connection;->socket:Ljava/net/Socket;

    .line 1228
    iget-object v5, p0, Lcom/android/okhttp/Connection;->socket:Ljava/net/Socket;

    check-cast v5, Ljavax/net/ssl/SSLSocket;

    .line 1229
    .local v5, "sslSocket":Ljavax/net/ssl/SSLSocket;
    iget-object v7, p0, Lcom/android/okhttp/Connection;->route:Lcom/android/okhttp/Route;

    iget-boolean v7, v7, Lcom/android/okhttp/Route;->modernTls:Z

    if-eqz v7, :cond_3

    .line 1230
    iget-object v7, p0, Lcom/android/okhttp/Connection;->route:Lcom/android/okhttp/Route;

    iget-object v7, v7, Lcom/android/okhttp/Route;->address:Lcom/android/okhttp/Address;

    iget-object v7, v7, Lcom/android/okhttp/Address;->uriHost:Ljava/lang/String;

    invoke-virtual {v2, v5, v7}, Lcom/android/okhttp/internal/Platform;->enableTlsExtensions(Ljavax/net/ssl/SSLSocket;Ljava/lang/String;)V

    .line 1235
    :goto_0
    const/4 v6, 0x0

    .line 1236
    .local v6, "useNpn":Z
    iget-object v7, p0, Lcom/android/okhttp/Connection;->route:Lcom/android/okhttp/Route;

    iget-boolean v7, v7, Lcom/android/okhttp/Route;->modernTls:Z

    if-eqz v7, :cond_2

    .line 1237
    iget-object v7, p0, Lcom/android/okhttp/Connection;->route:Lcom/android/okhttp/Route;

    iget-object v7, v7, Lcom/android/okhttp/Route;->address:Lcom/android/okhttp/Address;

    iget-object v7, v7, Lcom/android/okhttp/Address;->protocols:Ljava/util/List;

    sget-object v8, Lcom/android/okhttp/Protocol;->HTTP_2:Lcom/android/okhttp/Protocol;

    invoke-interface {v7, v8}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    .line 1238
    .local v0, "http2":Z
    iget-object v7, p0, Lcom/android/okhttp/Connection;->route:Lcom/android/okhttp/Route;

    iget-object v7, v7, Lcom/android/okhttp/Route;->address:Lcom/android/okhttp/Address;

    iget-object v7, v7, Lcom/android/okhttp/Address;->protocols:Ljava/util/List;

    sget-object v8, Lcom/android/okhttp/Protocol;->SPDY_3:Lcom/android/okhttp/Protocol;

    invoke-interface {v7, v8}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    .line 1239
    .local v4, "spdy3":Z
    if-eqz v0, :cond_4

    if-eqz v4, :cond_4

    .line 1240
    sget-object v7, Lcom/android/okhttp/Protocol;->HTTP2_SPDY3_AND_HTTP:Ljava/util/List;

    invoke-virtual {v2, v5, v7}, Lcom/android/okhttp/internal/Platform;->setNpnProtocols(Ljavax/net/ssl/SSLSocket;Ljava/util/List;)V

    .line 1241
    const/4 v6, 0x1

    .line 1252
    .end local v0    # "http2":Z
    .end local v4    # "spdy3":Z
    :cond_2
    :goto_1
    invoke-virtual {v5}, Ljavax/net/ssl/SSLSocket;->startHandshake()V

    .line 1255
    iget-object v7, p0, Lcom/android/okhttp/Connection;->route:Lcom/android/okhttp/Route;

    iget-object v7, v7, Lcom/android/okhttp/Route;->address:Lcom/android/okhttp/Address;

    iget-object v7, v7, Lcom/android/okhttp/Address;->hostnameVerifier:Ljavax/net/ssl/HostnameVerifier;

    iget-object v8, p0, Lcom/android/okhttp/Connection;->route:Lcom/android/okhttp/Route;

    iget-object v8, v8, Lcom/android/okhttp/Route;->address:Lcom/android/okhttp/Address;

    iget-object v8, v8, Lcom/android/okhttp/Address;->uriHost:Ljava/lang/String;

    invoke-virtual {v5}, Ljavax/net/ssl/SSLSocket;->getSession()Ljavax/net/ssl/SSLSession;

    move-result-object v9

    invoke-interface {v7, v8, v9}, Ljavax/net/ssl/HostnameVerifier;->verify(Ljava/lang/String;Ljavax/net/ssl/SSLSession;)Z

    move-result v7

    if-nez v7, :cond_6

    .line 1256
    new-instance v7, Ljava/io/IOException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Hostname \'"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/android/okhttp/Connection;->route:Lcom/android/okhttp/Route;

    iget-object v9, v9, Lcom/android/okhttp/Route;->address:Lcom/android/okhttp/Address;

    iget-object v9, v9, Lcom/android/okhttp/Address;->uriHost:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\' was not verified"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 1232
    .end local v6    # "useNpn":Z
    :cond_3
    invoke-virtual {v2, v5}, Lcom/android/okhttp/internal/Platform;->supportTlsIntolerantServer(Ljavax/net/ssl/SSLSocket;)V

    goto :goto_0

    .line 1242
    .restart local v0    # "http2":Z
    .restart local v4    # "spdy3":Z
    .restart local v6    # "useNpn":Z
    :cond_4
    if-eqz v0, :cond_5

    .line 1243
    sget-object v7, Lcom/android/okhttp/Protocol;->HTTP2_AND_HTTP_11:Ljava/util/List;

    invoke-virtual {v2, v5, v7}, Lcom/android/okhttp/internal/Platform;->setNpnProtocols(Ljavax/net/ssl/SSLSocket;Ljava/util/List;)V

    .line 1244
    const/4 v6, 0x1

    goto :goto_1

    .line 1245
    :cond_5
    if-eqz v4, :cond_2

    .line 1246
    sget-object v7, Lcom/android/okhttp/Protocol;->SPDY3_AND_HTTP11:Ljava/util/List;

    invoke-virtual {v2, v5, v7}, Lcom/android/okhttp/internal/Platform;->setNpnProtocols(Ljavax/net/ssl/SSLSocket;Ljava/util/List;)V

    .line 1247
    const/4 v6, 0x1

    goto :goto_1

    .line 1259
    .end local v0    # "http2":Z
    .end local v4    # "spdy3":Z
    :cond_6
    invoke-virtual {v5}, Ljavax/net/ssl/SSLSocket;->getSession()Ljavax/net/ssl/SSLSession;

    move-result-object v7

    invoke-static {v7}, Lcom/android/okhttp/Handshake;->get(Ljavax/net/ssl/SSLSession;)Lcom/android/okhttp/Handshake;

    move-result-object v7

    iput-object v7, p0, Lcom/android/okhttp/Connection;->handshake:Lcom/android/okhttp/Handshake;

    .line 1262
    sget-object v3, Lcom/android/okhttp/Protocol;->HTTP_11:Lcom/android/okhttp/Protocol;

    .line 1263
    .local v3, "selectedProtocol":Lcom/android/okhttp/Protocol;
    if-eqz v6, :cond_7

    invoke-virtual {v2, v5}, Lcom/android/okhttp/internal/Platform;->getNpnSelectedProtocol(Ljavax/net/ssl/SSLSocket;)Lcom/android/okio/ByteString;

    move-result-object v1

    .local v1, "maybeProtocol":Lcom/android/okio/ByteString;
    if-eqz v1, :cond_7

    .line 1264
    invoke-static {v1}, Lcom/android/okhttp/Protocol;->find(Lcom/android/okio/ByteString;)Lcom/android/okhttp/Protocol;

    move-result-object v3

    .line 1267
    .end local v1    # "maybeProtocol":Lcom/android/okio/ByteString;
    :cond_7
    iget-boolean v7, v3, Lcom/android/okhttp/Protocol;->spdyVariant:Z

    if-eqz v7, :cond_8

    .line 1268
    const/4 v7, 0x0

    invoke-virtual {v5, v7}, Ljavax/net/ssl/SSLSocket;->setSoTimeout(I)V

    .line 1269
    new-instance v7, Lcom/android/okhttp/internal/spdy/SpdyConnection$Builder;

    iget-object v8, p0, Lcom/android/okhttp/Connection;->route:Lcom/android/okhttp/Route;

    iget-object v8, v8, Lcom/android/okhttp/Route;->address:Lcom/android/okhttp/Address;

    invoke-virtual {v8}, Lcom/android/okhttp/Address;->getUriHost()Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, Lcom/android/okhttp/Connection;->socket:Ljava/net/Socket;

    invoke-direct {v7, v8, v11, v9}, Lcom/android/okhttp/internal/spdy/SpdyConnection$Builder;-><init>(Ljava/lang/String;ZLjava/net/Socket;)V

    invoke-virtual {v7, v3}, Lcom/android/okhttp/internal/spdy/SpdyConnection$Builder;->protocol(Lcom/android/okhttp/Protocol;)Lcom/android/okhttp/internal/spdy/SpdyConnection$Builder;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/okhttp/internal/spdy/SpdyConnection$Builder;->build()Lcom/android/okhttp/internal/spdy/SpdyConnection;

    move-result-object v7

    iput-object v7, p0, Lcom/android/okhttp/Connection;->spdyConnection:Lcom/android/okhttp/internal/spdy/SpdyConnection;

    .line 1271
    iget-object v7, p0, Lcom/android/okhttp/Connection;->spdyConnection:Lcom/android/okhttp/internal/spdy/SpdyConnection;

    invoke-virtual {v7}, Lcom/android/okhttp/internal/spdy/SpdyConnection;->sendConnectionHeader()V

    .line 1275
    :goto_2
    return-void

    .line 1273
    :cond_8
    new-instance v7, Lcom/android/okhttp/internal/http/HttpConnection;

    iget-object v8, p0, Lcom/android/okhttp/Connection;->pool:Lcom/android/okhttp/ConnectionPool;

    iget-object v9, p0, Lcom/android/okhttp/Connection;->socket:Ljava/net/Socket;

    invoke-direct {v7, v8, p0, v9}, Lcom/android/okhttp/internal/http/HttpConnection;-><init>(Lcom/android/okhttp/ConnectionPool;Lcom/android/okhttp/Connection;Ljava/net/Socket;)V

    iput-object v7, p0, Lcom/android/okhttp/Connection;->httpConnection:Lcom/android/okhttp/internal/http/HttpConnection;

    goto :goto_2
.end method

.method private upgradeToTlsForSecChunk(Ljava/net/Socket;Ljava/net/Proxy;Ljava/lang/String;IZ)Ljava/net/Socket;
    .locals 8
    .param p1, "sock"    # Ljava/net/Socket;
    .param p2, "proxy"    # Ljava/net/Proxy;
    .param p3, "host"    # Ljava/lang/String;
    .param p4, "port"    # I
    .param p5, "bGetOriRequest"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 426
    sget-boolean v5, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "upgradeToTlsForSecChunk("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 427
    :cond_0
    iget-object v3, p0, Lcom/android/okhttp/Connection;->ssl_oriReq:Lcom/android/okhttp/Connection$SSLInfo;

    .line 428
    .local v3, "sslInfo":Lcom/android/okhttp/Connection$SSLInfo;
    if-nez p5, :cond_1

    iget-object v5, p0, Lcom/android/okhttp/Connection;->ssl_redirect:Lcom/android/okhttp/Connection$SSLInfo;

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/android/okhttp/Connection;->ssl_redirect:Lcom/android/okhttp/Connection$SSLInfo;

    iget-boolean v5, v5, Lcom/android/okhttp/Connection$SSLInfo;->isSSL:Z

    if-eqz v5, :cond_1

    .line 429
    iget-object v3, p0, Lcom/android/okhttp/Connection;->ssl_redirect:Lcom/android/okhttp/Connection$SSLInfo;

    .line 431
    :cond_1
    sget-boolean v5, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "usingSSL: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 432
    :cond_2
    iget-boolean v5, v3, Lcom/android/okhttp/Connection$SSLInfo;->isSSL:Z

    if-nez v5, :cond_4

    move-object v4, p1

    .line 470
    :cond_3
    return-object v4

    .line 434
    :cond_4
    invoke-static {}, Lcom/android/okhttp/internal/Platform;->get()Lcom/android/okhttp/internal/Platform;

    move-result-object v1

    .line 437
    .local v1, "platform":Lcom/android/okhttp/internal/Platform;
    iget-object v5, v3, Lcom/android/okhttp/Connection$SSLInfo;->route:Lcom/android/okhttp/Route;

    iget-object v5, v5, Lcom/android/okhttp/Route;->address:Lcom/android/okhttp/Address;

    iget-object v5, v5, Lcom/android/okhttp/Address;->sslSocketFactory:Ljavax/net/ssl/SSLSocketFactory;

    if-eqz v5, :cond_5

    invoke-virtual {p2}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v5

    sget-object v6, Ljava/net/Proxy$Type;->HTTP:Ljava/net/Proxy$Type;

    if-ne v5, v6, :cond_5

    iget-object v5, v3, Lcom/android/okhttp/Connection$SSLInfo;->tunnel:Lcom/android/okhttp/TunnelRequest;

    if-eqz v5, :cond_5

    .line 438
    iget-object v5, v3, Lcom/android/okhttp/Connection$SSLInfo;->tunnel:Lcom/android/okhttp/TunnelRequest;

    iget-object v6, v3, Lcom/android/okhttp/Connection$SSLInfo;->route:Lcom/android/okhttp/Route;

    invoke-direct {p0, v5, v6, p1}, Lcom/android/okhttp/Connection;->makeTunnel(Lcom/android/okhttp/TunnelRequest;Lcom/android/okhttp/Route;Ljava/net/Socket;)V

    .line 442
    :cond_5
    iget-object v5, v3, Lcom/android/okhttp/Connection$SSLInfo;->route:Lcom/android/okhttp/Route;

    iget-object v5, v5, Lcom/android/okhttp/Route;->address:Lcom/android/okhttp/Address;

    iget-object v5, v5, Lcom/android/okhttp/Address;->sslSocketFactory:Ljavax/net/ssl/SSLSocketFactory;

    const/4 v6, 0x1

    invoke-virtual {v5, p1, p3, p4, v6}, Ljavax/net/ssl/SSLSocketFactory;->createSocket(Ljava/net/Socket;Ljava/lang/String;IZ)Ljava/net/Socket;

    move-result-object p1

    move-object v4, p1

    .line 443
    check-cast v4, Ljavax/net/ssl/SSLSocket;

    .line 444
    .local v4, "sslSocket":Ljavax/net/ssl/SSLSocket;
    iget-object v5, v3, Lcom/android/okhttp/Connection$SSLInfo;->route:Lcom/android/okhttp/Route;

    iget-boolean v5, v5, Lcom/android/okhttp/Route;->modernTls:Z

    if-eqz v5, :cond_7

    .line 445
    invoke-virtual {v1, v4, p3}, Lcom/android/okhttp/internal/Platform;->enableTlsExtensions(Ljavax/net/ssl/SSLSocket;Ljava/lang/String;)V

    .line 450
    :goto_0
    iget-object v5, v3, Lcom/android/okhttp/Connection$SSLInfo;->route:Lcom/android/okhttp/Route;

    iget-boolean v5, v5, Lcom/android/okhttp/Route;->modernTls:Z

    if-eqz v5, :cond_6

    .line 451
    iget-object v5, v3, Lcom/android/okhttp/Connection$SSLInfo;->route:Lcom/android/okhttp/Route;

    iget-object v5, v5, Lcom/android/okhttp/Route;->address:Lcom/android/okhttp/Address;

    iget-object v5, v5, Lcom/android/okhttp/Address;->protocols:Ljava/util/List;

    sget-object v6, Lcom/android/okhttp/Protocol;->HTTP_2:Lcom/android/okhttp/Protocol;

    invoke-interface {v5, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    .line 452
    .local v0, "http2":Z
    iget-object v5, v3, Lcom/android/okhttp/Connection$SSLInfo;->route:Lcom/android/okhttp/Route;

    iget-object v5, v5, Lcom/android/okhttp/Route;->address:Lcom/android/okhttp/Address;

    iget-object v5, v5, Lcom/android/okhttp/Address;->protocols:Ljava/util/List;

    sget-object v6, Lcom/android/okhttp/Protocol;->SPDY_3:Lcom/android/okhttp/Protocol;

    invoke-interface {v5, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    .line 453
    .local v2, "spdy3":Z
    if-eqz v0, :cond_8

    if-eqz v2, :cond_8

    .line 454
    sget-object v5, Lcom/android/okhttp/Protocol;->HTTP2_SPDY3_AND_HTTP:Ljava/util/List;

    invoke-virtual {v1, v4, v5}, Lcom/android/okhttp/internal/Platform;->setNpnProtocols(Ljavax/net/ssl/SSLSocket;Ljava/util/List;)V

    .line 463
    .end local v0    # "http2":Z
    .end local v2    # "spdy3":Z
    :cond_6
    :goto_1
    invoke-virtual {v4}, Ljavax/net/ssl/SSLSocket;->startHandshake()V

    .line 466
    iget-object v5, v3, Lcom/android/okhttp/Connection$SSLInfo;->route:Lcom/android/okhttp/Route;

    iget-object v5, v5, Lcom/android/okhttp/Route;->address:Lcom/android/okhttp/Address;

    iget-object v5, v5, Lcom/android/okhttp/Address;->hostnameVerifier:Ljavax/net/ssl/HostnameVerifier;

    invoke-virtual {v4}, Ljavax/net/ssl/SSLSocket;->getSession()Ljavax/net/ssl/SSLSession;

    move-result-object v6

    invoke-interface {v5, p3, v6}, Ljavax/net/ssl/HostnameVerifier;->verify(Ljava/lang/String;Ljavax/net/ssl/SSLSession;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 467
    new-instance v5, Ljava/io/IOException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Hostname \'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\' was not verified"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 447
    :cond_7
    invoke-virtual {v1, v4}, Lcom/android/okhttp/internal/Platform;->supportTlsIntolerantServer(Ljavax/net/ssl/SSLSocket;)V

    goto :goto_0

    .line 455
    .restart local v0    # "http2":Z
    .restart local v2    # "spdy3":Z
    :cond_8
    if-eqz v0, :cond_9

    .line 456
    sget-object v5, Lcom/android/okhttp/Protocol;->HTTP2_AND_HTTP_11:Ljava/util/List;

    invoke-virtual {v1, v4, v5}, Lcom/android/okhttp/internal/Platform;->setNpnProtocols(Ljavax/net/ssl/SSLSocket;Ljava/util/List;)V

    goto :goto_1

    .line 457
    :cond_9
    if-eqz v2, :cond_6

    .line 458
    sget-object v5, Lcom/android/okhttp/Protocol;->SPDY3_AND_HTTP11:Ljava/util/List;

    invoke-virtual {v1, v4, v5}, Lcom/android/okhttp/internal/Platform;->setNpnProtocols(Ljavax/net/ssl/SSLSocket;Ljava/util/List;)V

    goto :goto_1
.end method


# virtual methods
.method public bBothInfAvail()I
    .locals 9

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 1084
    invoke-virtual {p0, v5}, Lcom/android/okhttp/Connection;->getLocalAddr(I)Ljava/net/InetAddress;

    move-result-object v0

    .line 1085
    .local v0, "addr0":Ljava/net/InetAddress;
    invoke-virtual {p0, v4}, Lcom/android/okhttp/Connection;->getLocalAddr(I)Ljava/net/InetAddress;

    move-result-object v1

    .line 1086
    .local v1, "addr1":Ljava/net/InetAddress;
    if-eqz v0, :cond_2

    move v2, v4

    .line 1087
    .local v2, "ret0":Z
    :goto_0
    if-eqz v1, :cond_3

    move v3, v4

    .line 1088
    .local v3, "ret1":Z
    :goto_1
    sget-boolean v6, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v6, :cond_0

    .line 1089
    if-eqz v2, :cond_4

    if-eqz v3, :cond_4

    .line 1090
    iget-object v6, p0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "bBothInfAvail "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " and "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1096
    :cond_0
    :goto_2
    if-eqz v2, :cond_5

    if-eqz v3, :cond_5

    const/4 v4, 0x3

    .line 1099
    :cond_1
    :goto_3
    return v4

    .end local v2    # "ret0":Z
    .end local v3    # "ret1":Z
    :cond_2
    move v2, v5

    .line 1086
    goto :goto_0

    .restart local v2    # "ret0":Z
    :cond_3
    move v3, v5

    .line 1087
    goto :goto_1

    .line 1092
    .restart local v3    # "ret1":Z
    :cond_4
    if-nez v2, :cond_0

    if-nez v3, :cond_0

    .line 1093
    iget-object v6, p0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    const-string v7, "bBothInfAvail false and false"

    invoke-virtual {v6, v7}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    goto :goto_2

    .line 1097
    :cond_5
    if-nez v2, :cond_6

    if-eqz v3, :cond_6

    const/4 v4, 0x2

    goto :goto_3

    .line 1098
    :cond_6
    if-eqz v2, :cond_7

    if-eqz v3, :cond_1

    :cond_7
    move v4, v5

    .line 1099
    goto :goto_3
.end method

.method public clearOwner()Z
    .locals 2

    .prologue
    .line 1134
    iget-object v1, p0, Lcom/android/okhttp/Connection;->pool:Lcom/android/okhttp/ConnectionPool;

    monitor-enter v1

    .line 1135
    :try_start_0
    iget-object v0, p0, Lcom/android/okhttp/Connection;->owner:Ljava/lang/Object;

    if-nez v0, :cond_0

    .line 1137
    const/4 v0, 0x0

    monitor-exit v1

    .line 1141
    :goto_0
    return v0

    .line 1140
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/okhttp/Connection;->owner:Ljava/lang/Object;

    .line 1141
    const/4 v0, 0x1

    monitor-exit v1

    goto :goto_0

    .line 1142
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public close()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1284
    sget-boolean v1, Lcom/android/okhttp/internal/http/HttpEngine;->SMARTBONDING_ENABLED:Z

    if-eqz v1, :cond_4

    .line 1285
    sget-boolean v1, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "close connection :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " while multiSocketStream is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/okhttp/Connection;->multiSocketStream:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1286
    :cond_0
    iget-object v1, p0, Lcom/android/okhttp/Connection;->multiSocketStream:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    if-eqz v1, :cond_2

    .line 1287
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x2

    if-ge v0, v1, :cond_1

    .line 1288
    iget-object v1, p0, Lcom/android/okhttp/Connection;->multiSocket:[Ljava/net/Socket;

    aget-object v1, v1, v0

    invoke-static {v1}, Lcom/android/okhttp/internal/http/MultiratUtil;->closeQuietly(Ljava/net/Socket;)V

    .line 1287
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1290
    :cond_1
    iget-object v1, p0, Lcom/android/okhttp/Connection;->multiSocketStream:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    invoke-static {v1}, Lcom/android/okhttp/internal/http/MultiratUtil;->closeQuietly(Ljava/io/Closeable;)V

    .line 1292
    .end local v0    # "i":I
    :cond_2
    iget-object v1, p0, Lcom/android/okhttp/Connection;->socket:Ljava/net/Socket;

    invoke-static {v1}, Lcom/android/okhttp/internal/Util;->closeQuietly(Ljava/net/Socket;)V

    .line 1297
    :cond_3
    :goto_1
    return-void

    .line 1296
    :cond_4
    iget-object v1, p0, Lcom/android/okhttp/Connection;->socket:Ljava/net/Socket;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/okhttp/Connection;->socket:Ljava/net/Socket;

    invoke-virtual {v1}, Ljava/net/Socket;->close()V

    goto :goto_1
.end method

.method public closeIfOwnedBy(Ljava/lang/Object;)V
    .locals 2
    .param p1, "owner"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1150
    invoke-virtual {p0}, Lcom/android/okhttp/Connection;->isSpdy()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 1151
    :cond_0
    iget-object v1, p0, Lcom/android/okhttp/Connection;->pool:Lcom/android/okhttp/ConnectionPool;

    monitor-enter v1

    .line 1152
    :try_start_0
    iget-object v0, p0, Lcom/android/okhttp/Connection;->owner:Ljava/lang/Object;

    if-eq v0, p1, :cond_1

    .line 1153
    monitor-exit v1

    .line 1161
    :goto_0
    return-void

    .line 1156
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/okhttp/Connection;->owner:Ljava/lang/Object;

    .line 1157
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1160
    iget-object v0, p0, Lcom/android/okhttp/Connection;->socket:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->close()V

    goto :goto_0

    .line 1157
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public connect(IILcom/android/okhttp/TunnelRequest;)V
    .locals 4
    .param p1, "connectTimeout"    # I
    .param p2, "readTimeout"    # I
    .param p3, "tunnelRequest"    # Lcom/android/okhttp/TunnelRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 1165
    iget-boolean v0, p0, Lcom/android/okhttp/Connection;->connected:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "already connected"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1168
    :cond_0
    sget-boolean v0, Lcom/android/okhttp/internal/http/HttpEngine;->SMARTBONDING_ENABLED:Z

    if-eqz v0, :cond_1

    .line 1169
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/okhttp/Connection;->startSocketCreation:J

    .line 1170
    sget-boolean v0, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "http socket try to connect "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/okhttp/Connection;->route:Lcom/android/okhttp/Route;

    iget-object v2, v2, Lcom/android/okhttp/Route;->inetSocketAddress:Ljava/net/InetSocketAddress;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with time out "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", proxy is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/okhttp/Connection;->route:Lcom/android/okhttp/Route;

    iget-object v2, v2, Lcom/android/okhttp/Route;->proxy:Ljava/net/Proxy;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1173
    :cond_1
    iget-object v0, p0, Lcom/android/okhttp/Connection;->route:Lcom/android/okhttp/Route;

    iget-object v0, v0, Lcom/android/okhttp/Route;->proxy:Ljava/net/Proxy;

    invoke-virtual {v0}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v0

    sget-object v1, Ljava/net/Proxy$Type;->DIRECT:Ljava/net/Proxy$Type;

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/android/okhttp/Connection;->route:Lcom/android/okhttp/Route;

    iget-object v0, v0, Lcom/android/okhttp/Route;->proxy:Ljava/net/Proxy;

    invoke-virtual {v0}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v0

    sget-object v1, Ljava/net/Proxy$Type;->HTTP:Ljava/net/Proxy$Type;

    if-ne v0, v1, :cond_5

    .line 1174
    :cond_2
    iget-object v0, p0, Lcom/android/okhttp/Connection;->route:Lcom/android/okhttp/Route;

    iget-object v0, v0, Lcom/android/okhttp/Route;->address:Lcom/android/okhttp/Address;

    iget-object v0, v0, Lcom/android/okhttp/Address;->socketFactory:Ljavax/net/SocketFactory;

    invoke-virtual {v0}, Ljavax/net/SocketFactory;->createSocket()Ljava/net/Socket;

    move-result-object v0

    iput-object v0, p0, Lcom/android/okhttp/Connection;->socket:Ljava/net/Socket;

    .line 1179
    :goto_0
    iget-object v0, p0, Lcom/android/okhttp/Connection;->socket:Ljava/net/Socket;

    invoke-virtual {v0, p2}, Ljava/net/Socket;->setSoTimeout(I)V

    .line 1180
    invoke-static {}, Lcom/android/okhttp/internal/Platform;->get()Lcom/android/okhttp/internal/Platform;

    move-result-object v0

    iget-object v1, p0, Lcom/android/okhttp/Connection;->socket:Ljava/net/Socket;

    iget-object v2, p0, Lcom/android/okhttp/Connection;->route:Lcom/android/okhttp/Route;

    iget-object v2, v2, Lcom/android/okhttp/Route;->inetSocketAddress:Ljava/net/InetSocketAddress;

    invoke-virtual {v0, v1, v2, p1}, Lcom/android/okhttp/internal/Platform;->connectSocket(Ljava/net/Socket;Ljava/net/InetSocketAddress;I)V

    .line 1182
    iget-object v0, p0, Lcom/android/okhttp/Connection;->route:Lcom/android/okhttp/Route;

    iget-object v0, v0, Lcom/android/okhttp/Route;->address:Lcom/android/okhttp/Address;

    iget-object v0, v0, Lcom/android/okhttp/Address;->sslSocketFactory:Ljavax/net/ssl/SSLSocketFactory;

    if-eqz v0, :cond_6

    .line 1183
    invoke-direct {p0, p3}, Lcom/android/okhttp/Connection;->upgradeToTls(Lcom/android/okhttp/TunnelRequest;)V

    .line 1187
    :goto_1
    iput-boolean v3, p0, Lcom/android/okhttp/Connection;->connected:Z

    .line 1189
    sget-boolean v0, Lcom/android/okhttp/internal/http/HttpEngine;->SMARTBONDING_ENABLED:Z

    if-eqz v0, :cond_4

    .line 1190
    sget-boolean v0, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "http socket connected to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/okhttp/Connection;->route:Lcom/android/okhttp/Route;

    iget-object v2, v2, Lcom/android/okhttp/Route;->inetSocketAddress:Ljava/net/InetSocketAddress;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " from "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/okhttp/Connection;->socket:Ljava/net/Socket;

    invoke-virtual {v2}, Ljava/net/Socket;->getLocalAddress()Ljava/net/InetAddress;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/okhttp/Connection;->socket:Ljava/net/Socket;

    invoke-virtual {v2}, Ljava/net/Socket;->getLocalPort()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with reading time out "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1192
    :cond_3
    iget-object v0, p0, Lcom/android/okhttp/Connection;->socket:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->getLocalAddress()Ljava/net/InetAddress;

    move-result-object v0

    instance-of v0, v0, Ljava/net/Inet6Address;

    if-eqz v0, :cond_7

    .line 1193
    iput v3, p0, Lcom/android/okhttp/Connection;->mDestIPver:I

    .line 1198
    :goto_2
    iput p2, p0, Lcom/android/okhttp/Connection;->mainTimeout:I

    .line 1199
    const/16 v0, 0x2710

    iput v0, p0, Lcom/android/okhttp/Connection;->connTimeout:I

    .line 1200
    new-instance v0, Lcom/android/okhttp/Connection$SSLInfo;

    iget-boolean v1, p0, Lcom/android/okhttp/Connection;->isSSL:Z

    iget-object v2, p0, Lcom/android/okhttp/Connection;->route:Lcom/android/okhttp/Route;

    invoke-direct {v0, v1, v2, p3}, Lcom/android/okhttp/Connection$SSLInfo;-><init>(ZLcom/android/okhttp/Route;Lcom/android/okhttp/TunnelRequest;)V

    iput-object v0, p0, Lcom/android/okhttp/Connection;->ssl_oriReq:Lcom/android/okhttp/Connection$SSLInfo;

    .line 1203
    :cond_4
    return-void

    .line 1176
    :cond_5
    new-instance v0, Ljava/net/Socket;

    iget-object v1, p0, Lcom/android/okhttp/Connection;->route:Lcom/android/okhttp/Route;

    iget-object v1, v1, Lcom/android/okhttp/Route;->proxy:Ljava/net/Proxy;

    invoke-direct {v0, v1}, Ljava/net/Socket;-><init>(Ljava/net/Proxy;)V

    iput-object v0, p0, Lcom/android/okhttp/Connection;->socket:Ljava/net/Socket;

    goto/16 :goto_0

    .line 1185
    :cond_6
    new-instance v0, Lcom/android/okhttp/internal/http/HttpConnection;

    iget-object v1, p0, Lcom/android/okhttp/Connection;->pool:Lcom/android/okhttp/ConnectionPool;

    iget-object v2, p0, Lcom/android/okhttp/Connection;->socket:Ljava/net/Socket;

    invoke-direct {v0, v1, p0, v2}, Lcom/android/okhttp/internal/http/HttpConnection;-><init>(Lcom/android/okhttp/ConnectionPool;Lcom/android/okhttp/Connection;Ljava/net/Socket;)V

    iput-object v0, p0, Lcom/android/okhttp/Connection;->httpConnection:Lcom/android/okhttp/internal/http/HttpConnection;

    goto/16 :goto_1

    .line 1196
    :cond_7
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/okhttp/Connection;->mDestIPver:I

    goto :goto_2
.end method

.method public connect(IILcom/android/okhttp/TunnelRequest;ILcom/android/okhttp/Connection;)V
    .locals 7
    .param p1, "connectTimeout"    # I
    .param p2, "readTimeout"    # I
    .param p3, "tunnelRequest"    # Lcom/android/okhttp/TunnelRequest;
    .param p4, "netType"    # I
    .param p5, "oriCon"    # Lcom/android/okhttp/Connection;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 364
    iget-boolean v2, p0, Lcom/android/okhttp/Connection;->connected:Z

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "already connected"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 366
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/okhttp/Connection;->startSocketCreation:J

    .line 367
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "http socket try to connect "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/okhttp/Connection;->route:Lcom/android/okhttp/Route;

    iget-object v4, v4, Lcom/android/okhttp/Route;->inetSocketAddress:Ljava/net/InetSocketAddress;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " with time out "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", proxy is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/okhttp/Connection;->route:Lcom/android/okhttp/Route;

    iget-object v4, v4, Lcom/android/okhttp/Route;->proxy:Ljava/net/Proxy;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 368
    :cond_1
    iget-object v2, p0, Lcom/android/okhttp/Connection;->route:Lcom/android/okhttp/Route;

    iget-object v2, v2, Lcom/android/okhttp/Route;->proxy:Ljava/net/Proxy;

    invoke-virtual {v2}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v2

    sget-object v3, Ljava/net/Proxy$Type;->DIRECT:Ljava/net/Proxy$Type;

    if-eq v2, v3, :cond_2

    iget-object v2, p0, Lcom/android/okhttp/Connection;->route:Lcom/android/okhttp/Route;

    iget-object v2, v2, Lcom/android/okhttp/Route;->proxy:Ljava/net/Proxy;

    invoke-virtual {v2}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v2

    sget-object v3, Ljava/net/Proxy$Type;->HTTP:Ljava/net/Proxy$Type;

    if-ne v2, v3, :cond_5

    .line 369
    :cond_2
    iget-object v2, p0, Lcom/android/okhttp/Connection;->route:Lcom/android/okhttp/Route;

    iget-object v2, v2, Lcom/android/okhttp/Route;->address:Lcom/android/okhttp/Address;

    iget-object v2, v2, Lcom/android/okhttp/Address;->socketFactory:Ljavax/net/SocketFactory;

    invoke-virtual {v2}, Ljavax/net/SocketFactory;->createSocket()Ljava/net/Socket;

    move-result-object v2

    iput-object v2, p0, Lcom/android/okhttp/Connection;->socket:Ljava/net/Socket;

    .line 375
    :goto_0
    :try_start_0
    iget-object v2, p0, Lcom/android/okhttp/Connection;->route:Lcom/android/okhttp/Route;

    iget-object v2, v2, Lcom/android/okhttp/Route;->inetSocketAddress:Ljava/net/InetSocketAddress;

    invoke-virtual {v2}, Ljava/net/InetSocketAddress;->getAddress()Ljava/net/InetAddress;

    move-result-object v2

    instance-of v2, v2, Ljava/net/Inet6Address;

    if-eqz v2, :cond_6

    .line 376
    const/4 v2, 0x1

    iput v2, p0, Lcom/android/okhttp/Connection;->mDestIPver:I

    .line 381
    :goto_1
    invoke-virtual {p0, p4}, Lcom/android/okhttp/Connection;->getLocalAddr(I)Ljava/net/InetAddress;

    move-result-object v1

    .line 382
    .local v1, "localAddr":Ljava/net/InetAddress;
    if-eqz v1, :cond_3

    iget-object v2, p0, Lcom/android/okhttp/Connection;->socket:Ljava/net/Socket;

    invoke-virtual {v2}, Ljava/net/Socket;->isBound()Z

    move-result v2

    if-nez v2, :cond_3

    .line 383
    iget-object v2, p0, Lcom/android/okhttp/Connection;->socket:Ljava/net/Socket;

    new-instance v3, Ljava/net/InetSocketAddress;

    const/4 v4, 0x0

    invoke-direct {v3, v1, v4}, Ljava/net/InetSocketAddress;-><init>(Ljava/net/InetAddress;I)V

    invoke-virtual {v2, v3}, Ljava/net/Socket;->bind(Ljava/net/SocketAddress;)V

    .line 384
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "main socket bind to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 390
    .end local v1    # "localAddr":Ljava/net/InetAddress;
    :cond_3
    :goto_2
    iget-object v2, p0, Lcom/android/okhttp/Connection;->socket:Ljava/net/Socket;

    invoke-virtual {v2, p2}, Ljava/net/Socket;->setSoTimeout(I)V

    .line 391
    invoke-static {}, Lcom/android/okhttp/internal/Platform;->get()Lcom/android/okhttp/internal/Platform;

    move-result-object v2

    iget-object v3, p0, Lcom/android/okhttp/Connection;->socket:Ljava/net/Socket;

    iget-object v4, p0, Lcom/android/okhttp/Connection;->route:Lcom/android/okhttp/Route;

    iget-object v4, v4, Lcom/android/okhttp/Route;->inetSocketAddress:Ljava/net/InetSocketAddress;

    invoke-virtual {v2, v3, v4, p1}, Lcom/android/okhttp/internal/Platform;->connectSocket(Ljava/net/Socket;Ljava/net/InetSocketAddress;I)V

    .line 393
    iget-object v2, p0, Lcom/android/okhttp/Connection;->route:Lcom/android/okhttp/Route;

    iget-object v2, v2, Lcom/android/okhttp/Route;->address:Lcom/android/okhttp/Address;

    iget-object v2, v2, Lcom/android/okhttp/Address;->sslSocketFactory:Ljavax/net/ssl/SSLSocketFactory;

    if-eqz v2, :cond_7

    .line 394
    invoke-direct {p0, p3}, Lcom/android/okhttp/Connection;->upgradeToTls(Lcom/android/okhttp/TunnelRequest;)V

    .line 398
    :goto_3
    iput-boolean v6, p0, Lcom/android/okhttp/Connection;->connected:Z

    .line 399
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "http socket connected to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/okhttp/Connection;->route:Lcom/android/okhttp/Route;

    iget-object v4, v4, Lcom/android/okhttp/Route;->inetSocketAddress:Ljava/net/InetSocketAddress;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " from "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/okhttp/Connection;->socket:Ljava/net/Socket;

    invoke-virtual {v4}, Ljava/net/Socket;->getLocalAddress()Ljava/net/InetAddress;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/okhttp/Connection;->socket:Ljava/net/Socket;

    invoke-virtual {v4}, Ljava/net/Socket;->getLocalPort()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " with reading time out "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 401
    :cond_4
    iget-object v2, p0, Lcom/android/okhttp/Connection;->socket:Ljava/net/Socket;

    invoke-virtual {v2}, Ljava/net/Socket;->getLocalAddress()Ljava/net/InetAddress;

    move-result-object v2

    instance-of v2, v2, Ljava/net/Inet6Address;

    if-eqz v2, :cond_8

    .line 402
    iput v6, p0, Lcom/android/okhttp/Connection;->mDestIPver:I

    .line 407
    :goto_4
    iput p2, p0, Lcom/android/okhttp/Connection;->mainTimeout:I

    .line 408
    const/16 v2, 0x2710

    iput v2, p0, Lcom/android/okhttp/Connection;->connTimeout:I

    .line 409
    if-nez p5, :cond_9

    .line 410
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/android/okhttp/Connection;->ssl_oriReq:Lcom/android/okhttp/Connection$SSLInfo;

    .line 413
    :goto_5
    iget-object v2, p0, Lcom/android/okhttp/Connection;->ssl_oriReq:Lcom/android/okhttp/Connection$SSLInfo;

    if-nez v2, :cond_a

    .line 414
    new-instance v2, Lcom/android/okhttp/Connection$SSLInfo;

    iget-boolean v3, p0, Lcom/android/okhttp/Connection;->isSSL:Z

    iget-object v4, p0, Lcom/android/okhttp/Connection;->route:Lcom/android/okhttp/Route;

    invoke-direct {v2, v3, v4, p3}, Lcom/android/okhttp/Connection$SSLInfo;-><init>(ZLcom/android/okhttp/Route;Lcom/android/okhttp/TunnelRequest;)V

    iput-object v2, p0, Lcom/android/okhttp/Connection;->ssl_oriReq:Lcom/android/okhttp/Connection$SSLInfo;

    .line 419
    :goto_6
    return-void

    .line 371
    :cond_5
    new-instance v2, Ljava/net/Socket;

    iget-object v3, p0, Lcom/android/okhttp/Connection;->route:Lcom/android/okhttp/Route;

    iget-object v3, v3, Lcom/android/okhttp/Route;->proxy:Ljava/net/Proxy;

    invoke-direct {v2, v3}, Ljava/net/Socket;-><init>(Ljava/net/Proxy;)V

    iput-object v2, p0, Lcom/android/okhttp/Connection;->socket:Ljava/net/Socket;

    goto/16 :goto_0

    .line 378
    :cond_6
    const/4 v2, 0x0

    :try_start_1
    iput v2, p0, Lcom/android/okhttp/Connection;->mDestIPver:I
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1

    .line 386
    :catch_0
    move-exception v0

    .line 387
    .local v0, "ex":Ljava/lang/Throwable;
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    invoke-virtual {v2, v0}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V

    goto/16 :goto_2

    .line 396
    .end local v0    # "ex":Ljava/lang/Throwable;
    :cond_7
    new-instance v2, Lcom/android/okhttp/internal/http/HttpConnection;

    iget-object v3, p0, Lcom/android/okhttp/Connection;->pool:Lcom/android/okhttp/ConnectionPool;

    iget-object v4, p0, Lcom/android/okhttp/Connection;->socket:Ljava/net/Socket;

    invoke-direct {v2, v3, p0, v4}, Lcom/android/okhttp/internal/http/HttpConnection;-><init>(Lcom/android/okhttp/ConnectionPool;Lcom/android/okhttp/Connection;Ljava/net/Socket;)V

    iput-object v2, p0, Lcom/android/okhttp/Connection;->httpConnection:Lcom/android/okhttp/internal/http/HttpConnection;

    goto/16 :goto_3

    .line 405
    :cond_8
    iput v5, p0, Lcom/android/okhttp/Connection;->mDestIPver:I

    goto :goto_4

    .line 412
    :cond_9
    iget-object v2, p5, Lcom/android/okhttp/Connection;->ssl_oriReq:Lcom/android/okhttp/Connection$SSLInfo;

    iput-object v2, p0, Lcom/android/okhttp/Connection;->ssl_oriReq:Lcom/android/okhttp/Connection$SSLInfo;

    goto :goto_5

    .line 417
    :cond_a
    new-instance v2, Lcom/android/okhttp/Connection$SSLInfo;

    iget-boolean v3, p0, Lcom/android/okhttp/Connection;->isSSL:Z

    iget-object v4, p0, Lcom/android/okhttp/Connection;->route:Lcom/android/okhttp/Route;

    invoke-direct {v2, v3, v4, p3}, Lcom/android/okhttp/Connection$SSLInfo;-><init>(ZLcom/android/okhttp/Route;Lcom/android/okhttp/TunnelRequest;)V

    iput-object v2, p0, Lcom/android/okhttp/Connection;->ssl_redirect:Lcom/android/okhttp/Connection$SSLInfo;

    goto :goto_6
.end method

.method public connectSocket(ILjava/net/URI;)Ljava/net/Socket;
    .locals 13
    .param p1, "iNetType"    # I
    .param p2, "uri"    # Ljava/net/URI;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    const/4 v11, 0x0

    .line 566
    sget-boolean v1, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "connecting socket for netType "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " and URI "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 567
    :cond_0
    iget-object v1, p0, Lcom/android/okhttp/Connection;->multiSocket:[Ljava/net/Socket;

    aget-object v1, v1, p1

    invoke-static {v1}, Lcom/android/okhttp/internal/http/MultiratUtil;->closeQuietly(Ljava/net/Socket;)V

    .line 568
    iget-object v1, p0, Lcom/android/okhttp/Connection;->multiSocket:[Ljava/net/Socket;

    aput-object v0, v1, p1

    .line 569
    invoke-virtual {p0, p1}, Lcom/android/okhttp/Connection;->getLocalAddr(I)Ljava/net/InetAddress;

    move-result-object v4

    .line 570
    .local v4, "localAddr":Ljava/net/InetAddress;
    sget-boolean v1, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "connecting socket for netType "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " get address "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 571
    :cond_1
    if-nez v4, :cond_4

    .line 572
    sget-boolean v1, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "fail to get local ip for interface "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 573
    :cond_2
    add-int/lit8 v1, p1, 0x1

    rem-int/lit8 v1, v1, 0x2

    invoke-virtual {p0, v1}, Lcom/android/okhttp/Connection;->getLocalAddr(I)Ljava/net/InetAddress;

    move-result-object v4

    .line 574
    if-nez v4, :cond_4

    .line 575
    sget-boolean v1, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "also fail to get local ip for interface "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    add-int/lit8 v6, p1, 0x1

    rem-int/lit8 v6, v6, 0x2

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 606
    :cond_3
    :goto_0
    return-object v0

    .line 580
    :cond_4
    invoke-virtual {p2}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1, v1}, Lcom/android/okhttp/Connection;->getProxy(ILjava/lang/String;)Ljava/net/Proxy;

    move-result-object v5

    .line 581
    .local v5, "curProxy":Ljava/net/Proxy;
    iget-object v1, p0, Lcom/android/okhttp/Connection;->mProxy:[Ljava/net/Proxy;

    aget-object v1, v1, p1

    invoke-direct {p0, v5, v1}, Lcom/android/okhttp/Connection;->isSameProxy(Ljava/net/Proxy;Ljava/net/Proxy;)Z

    move-result v1

    if-nez v1, :cond_9

    move v12, v3

    .line 582
    .local v12, "bProxyChanged":Z
    :goto_1
    if-eqz v12, :cond_6

    .line 583
    sget-boolean v1, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "proxy updated from "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/android/okhttp/Connection;->mProxy:[Ljava/net/Proxy;

    aget-object v7, v7, p1

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " to "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " for netType "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 584
    :cond_5
    iget-object v1, p0, Lcom/android/okhttp/Connection;->mProxy:[Ljava/net/Proxy;

    aput-object v5, v1, p1

    .line 585
    iget-object v1, p0, Lcom/android/okhttp/Connection;->multiSockDestAddr:[Ljava/net/InetAddress;

    aput-object v0, v1, p1

    .line 588
    :cond_6
    iget-object v0, p0, Lcom/android/okhttp/Connection;->destPorts:[I

    invoke-static {p2}, Lcom/android/okhttp/internal/Util;->getEffectivePort(Ljava/net/URI;)I

    move-result v1

    aput v1, v0, p1

    .line 589
    iget-object v0, p0, Lcom/android/okhttp/Connection;->multiSockDestAddr:[Ljava/net/InetAddress;

    aget-object v0, v0, p1

    if-eqz v0, :cond_a

    .line 590
    iget-object v6, p0, Lcom/android/okhttp/Connection;->multiSocket:[Ljava/net/Socket;

    iget v1, p0, Lcom/android/okhttp/Connection;->connTimeout:I

    new-array v2, v3, [Ljava/net/InetAddress;

    iget-object v0, p0, Lcom/android/okhttp/Connection;->multiSockDestAddr:[Ljava/net/InetAddress;

    aget-object v0, v0, p1

    aput-object v0, v2, v11

    iget-object v0, p0, Lcom/android/okhttp/Connection;->destPorts:[I

    aget v3, v0, p1

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/okhttp/Connection;->connect(I[Ljava/net/InetAddress;ILjava/net/InetAddress;Ljava/net/Proxy;)Ljava/net/Socket;

    move-result-object v0

    aput-object v0, v6, p1

    .line 596
    :goto_2
    const-string v0, "https"

    invoke-virtual {p2}, Ljava/net/URI;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 597
    iget-object v0, p0, Lcom/android/okhttp/Connection;->multiSocket:[Ljava/net/Socket;

    iget-object v1, p0, Lcom/android/okhttp/Connection;->multiSocket:[Ljava/net/Socket;

    aget-object v7, v1, p1

    invoke-virtual {p2}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v9

    iget-object v1, p0, Lcom/android/okhttp/Connection;->destPorts:[I

    aget v10, v1, p1

    move-object v6, p0

    move-object v8, v5

    invoke-direct/range {v6 .. v11}, Lcom/android/okhttp/Connection;->upgradeToTlsForSecChunk(Ljava/net/Socket;Ljava/net/Proxy;Ljava/lang/String;IZ)Ljava/net/Socket;

    move-result-object v1

    aput-object v1, v0, p1

    .line 598
    sget-boolean v0, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "upgrade to Tls Socket "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/android/okhttp/Connection;->multiSocket:[Ljava/net/Socket;

    aget-object v3, v3, p1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 600
    :cond_7
    iget v0, p0, Lcom/android/okhttp/Connection;->mainTimeout:I

    if-lez v0, :cond_8

    .line 602
    :try_start_0
    iget-object v0, p0, Lcom/android/okhttp/Connection;->multiSocket:[Ljava/net/Socket;

    aget-object v0, v0, p1

    iget v1, p0, Lcom/android/okhttp/Connection;->mainTimeout:I

    invoke-virtual {v0, v1}, Ljava/net/Socket;->setSoTimeout(I)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 606
    :cond_8
    :goto_3
    iget-object v0, p0, Lcom/android/okhttp/Connection;->multiSocket:[Ljava/net/Socket;

    aget-object v0, v0, p1

    goto/16 :goto_0

    .end local v12    # "bProxyChanged":Z
    :cond_9
    move v12, v11

    .line 581
    goto/16 :goto_1

    .line 593
    .restart local v12    # "bProxyChanged":Z
    :cond_a
    invoke-virtual {p2}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/android/okhttp/Connection;->getAllByName(ILjava/lang/String;)[Ljava/net/InetAddress;

    move-result-object v2

    .line 594
    .local v2, "addresses":[Ljava/net/InetAddress;
    iget-object v6, p0, Lcom/android/okhttp/Connection;->multiSocket:[Ljava/net/Socket;

    iget v1, p0, Lcom/android/okhttp/Connection;->connTimeout:I

    iget-object v0, p0, Lcom/android/okhttp/Connection;->destPorts:[I

    aget v3, v0, p1

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/okhttp/Connection;->connect(I[Ljava/net/InetAddress;ILjava/net/InetAddress;Ljava/net/Proxy;)Ljava/net/Socket;

    move-result-object v0

    aput-object v0, v6, p1

    goto :goto_2

    .line 604
    .end local v2    # "addresses":[Ljava/net/InetAddress;
    :catch_0
    move-exception v0

    goto :goto_3
.end method

.method public extremeConditionConnect(ILcom/android/okhttp/Request;JJJLcom/android/okhttp/internal/http/MultiSocketInputStream;ZZ)Ljava/net/Socket;
    .locals 17
    .param p1, "netType"    # I
    .param p2, "requestHeaders"    # Lcom/android/okhttp/Request;
    .param p3, "start"    # J
    .param p5, "end"    # J
    .param p7, "fullSize"    # J
    .param p9, "multiSock"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;
    .param p10, "bTryAnother"    # Z
    .param p11, "bUpdateTime"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 626
    const/4 v15, 0x0

    .line 628
    .local v15, "sock":Ljava/net/Socket;
    :try_start_0
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "connecting socket for Main or Extrem :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    :cond_0
    move-object/from16 v3, p0

    move/from16 v4, p1

    move-object/from16 v5, p2

    move-wide/from16 v6, p3

    move-wide/from16 v8, p5

    move-wide/from16 v10, p7

    move-object/from16 v12, p9

    move/from16 v13, p11

    move/from16 v14, p10

    .line 629
    invoke-direct/range {v3 .. v14}, Lcom/android/okhttp/Connection;->extremeConditionConnectInternal(ILcom/android/okhttp/Request;JJJLcom/android/okhttp/internal/http/MultiSocketInputStream;ZZ)Ljava/net/Socket;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v15

    .line 643
    :goto_0
    return-object v15

    .line 631
    :catch_0
    move-exception v2

    .line 632
    .local v2, "e":Ljava/io/IOException;
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "failed to connecting socket for Main or Extrem :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 633
    :cond_1
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    invoke-virtual {v3, v2}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V

    .line 634
    :cond_2
    if-eqz p10, :cond_4

    .line 635
    add-int/lit8 v3, p1, 0x1

    rem-int/lit8 v4, v3, 0x2

    .line 636
    .local v4, "otherInf":I
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "connecting socket for Main or Extrem on the other inf:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    :cond_3
    move-object/from16 v3, p0

    move-object/from16 v5, p2

    move-wide/from16 v6, p3

    move-wide/from16 v8, p5

    move-wide/from16 v10, p7

    move-object/from16 v12, p9

    move/from16 v13, p11

    move/from16 v14, p10

    .line 637
    invoke-direct/range {v3 .. v14}, Lcom/android/okhttp/Connection;->extremeConditionConnectInternal(ILcom/android/okhttp/Request;JJJLcom/android/okhttp/internal/http/MultiSocketInputStream;ZZ)Ljava/net/Socket;

    move-result-object v15

    .line 638
    goto :goto_0

    .line 640
    .end local v4    # "otherInf":I
    :cond_4
    throw v2
.end method

.method public getHandshake()Lcom/android/okhttp/Handshake;
    .locals 1

    .prologue
    .line 1388
    iget-object v0, p0, Lcom/android/okhttp/Connection;->handshake:Lcom/android/okhttp/Handshake;

    return-object v0
.end method

.method public getHttpMinorVersion()I
    .locals 1

    .prologue
    .line 1412
    iget v0, p0, Lcom/android/okhttp/Connection;->httpMinorVersion:I

    return v0
.end method

.method public getIdleStartTimeNs()J
    .locals 2

    .prologue
    .line 1384
    iget-object v0, p0, Lcom/android/okhttp/Connection;->spdyConnection:Lcom/android/okhttp/internal/spdy/SpdyConnection;

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/android/okhttp/Connection;->idleStartTimeNs:J

    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/android/okhttp/Connection;->spdyConnection:Lcom/android/okhttp/internal/spdy/SpdyConnection;

    invoke-virtual {v0}, Lcom/android/okhttp/internal/spdy/SpdyConnection;->getIdleStartTimeNs()J

    move-result-wide v0

    goto :goto_0
.end method

.method public getLocalAddr(I)Ljava/net/InetAddress;
    .locals 5
    .param p1, "netType"    # I

    .prologue
    const/4 v0, 0x0

    .line 348
    const/4 v2, 0x2

    if-lt p1, v2, :cond_1

    .line 349
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "illegal netType: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 358
    :cond_0
    :goto_0
    return-object v0

    .line 353
    :cond_1
    :try_start_0
    iget v2, p0, Lcom/android/okhttp/Connection;->mDestIPver:I

    invoke-static {p1, v2}, Lcom/android/okhttp/internal/http/SBServiceAPI;->getSBInterface(II)Ljava/net/InetAddress;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 354
    .local v0, "addr":Ljava/net/InetAddress;
    goto :goto_0

    .line 356
    .end local v0    # "addr":Ljava/net/InetAddress;
    :catch_0
    move-exception v1

    .line 357
    .local v1, "e":Ljava/lang/Throwable;
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    invoke-virtual {v2, v1}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public getOwner()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1109
    iget-object v1, p0, Lcom/android/okhttp/Connection;->pool:Lcom/android/okhttp/ConnectionPool;

    monitor-enter v1

    .line 1110
    :try_start_0
    iget-object v0, p0, Lcom/android/okhttp/Connection;->owner:Ljava/lang/Object;

    monitor-exit v1

    return-object v0

    .line 1111
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getProxy(I)Ljava/net/Proxy;
    .locals 1
    .param p1, "netType"    # I

    .prologue
    .line 220
    iget-object v0, p0, Lcom/android/okhttp/Connection;->mProxy:[Ljava/net/Proxy;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getRoute()Lcom/android/okhttp/Route;
    .locals 1

    .prologue
    .line 1301
    iget-object v0, p0, Lcom/android/okhttp/Connection;->route:Lcom/android/okhttp/Route;

    return-object v0
.end method

.method public getSocket()Ljava/net/Socket;
    .locals 1

    .prologue
    .line 1324
    iget-object v0, p0, Lcom/android/okhttp/Connection;->socket:Ljava/net/Socket;

    return-object v0
.end method

.method public incrementRecycleCount()V
    .locals 1

    .prologue
    .line 1445
    iget v0, p0, Lcom/android/okhttp/Connection;->recycleCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/okhttp/Connection;->recycleCount:I

    .line 1446
    return-void
.end method

.method public isAlive()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1330
    sget-boolean v1, Lcom/android/okhttp/internal/http/HttpEngine;->SMARTBONDING_ENABLED:Z

    if-eqz v1, :cond_1

    .line 1331
    iget-object v1, p0, Lcom/android/okhttp/Connection;->multiSocketStream:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    if-eqz v1, :cond_1

    .line 1336
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/android/okhttp/Connection;->socket:Ljava/net/Socket;

    invoke-virtual {v1}, Ljava/net/Socket;->isClosed()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/android/okhttp/Connection;->socket:Ljava/net/Socket;

    invoke-virtual {v1}, Ljava/net/Socket;->isInputShutdown()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/android/okhttp/Connection;->socket:Ljava/net/Socket;

    invoke-virtual {v1}, Ljava/net/Socket;->isOutputShutdown()Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isConnected()Z
    .locals 1

    .prologue
    .line 1279
    iget-boolean v0, p0, Lcom/android/okhttp/Connection;->connected:Z

    return v0
.end method

.method public isExpired(J)Z
    .locals 5
    .param p1, "keepAliveDurationNs"    # J

    .prologue
    .line 1376
    invoke-virtual {p0}, Lcom/android/okhttp/Connection;->getIdleStartTimeNs()J

    move-result-wide v0

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    sub-long/2addr v2, p1

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isIdle()Z
    .locals 1

    .prologue
    .line 1368
    iget-object v0, p0, Lcom/android/okhttp/Connection;->spdyConnection:Lcom/android/okhttp/internal/spdy/SpdyConnection;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/okhttp/Connection;->spdyConnection:Lcom/android/okhttp/internal/spdy/SpdyConnection;

    invoke-virtual {v0}, Lcom/android/okhttp/internal/spdy/SpdyConnection;->isIdle()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isMultiSocket()Z
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/android/okhttp/Connection;->multiSocketStream:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isReadable()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1346
    sget-boolean v3, Lcom/android/okhttp/internal/http/HttpEngine;->SMARTBONDING_ENABLED:Z

    if-eqz v3, :cond_2

    .line 1347
    iget-object v3, p0, Lcom/android/okhttp/Connection;->multiSocketStream:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    if-eqz v3, :cond_2

    .line 1349
    :try_start_0
    iget-object v3, p0, Lcom/android/okhttp/Connection;->multiSocketStream:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    invoke-virtual {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->available()I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-lez v3, :cond_1

    .line 1358
    :cond_0
    :goto_0
    return v1

    :cond_1
    move v1, v2

    .line 1349
    goto :goto_0

    .line 1351
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/lang/Throwable;
    move v1, v2

    .line 1352
    goto :goto_0

    .line 1357
    .end local v0    # "e":Ljava/lang/Throwable;
    :cond_2
    iget-object v2, p0, Lcom/android/okhttp/Connection;->httpConnection:Lcom/android/okhttp/internal/http/HttpConnection;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/android/okhttp/Connection;->httpConnection:Lcom/android/okhttp/internal/http/HttpConnection;

    invoke-virtual {v1}, Lcom/android/okhttp/internal/http/HttpConnection;->isReadable()Z

    move-result v1

    goto :goto_0
.end method

.method public isSSLSocket()Z
    .locals 1

    .prologue
    .line 161
    iget-boolean v0, p0, Lcom/android/okhttp/Connection;->isSSL:Z

    return v0
.end method

.method public isSpdy()Z
    .locals 1

    .prologue
    .line 1403
    iget-object v0, p0, Lcom/android/okhttp/Connection;->spdyConnection:Lcom/android/okhttp/internal/spdy/SpdyConnection;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isValidSocketForMultiRAT()Z
    .locals 8

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 169
    invoke-virtual {p0}, Lcom/android/okhttp/Connection;->getSocket()Ljava/net/Socket;

    move-result-object v3

    .line 170
    .local v3, "sock":Ljava/net/Socket;
    invoke-static {}, Lcom/android/okhttp/internal/http/SBServiceAPI;->isShipBuild()Z

    move-result v0

    .line 171
    .local v0, "SHIP_BUILD":Z
    invoke-static {}, Lcom/android/okhttp/internal/http/SBServiceAPI;->isSKTBuild()Z

    move-result v1

    .line 172
    .local v1, "SKT_BUILD":Z
    iget-boolean v6, p0, Lcom/android/okhttp/Connection;->isSSL:Z

    if-eqz v6, :cond_0

    .line 173
    :cond_0
    if-nez v3, :cond_2

    .line 183
    :cond_1
    :goto_0
    return v4

    .line 174
    :cond_2
    invoke-virtual {v3}, Ljava/net/Socket;->isClosed()Z

    move-result v6

    if-nez v6, :cond_1

    invoke-virtual {v3}, Ljava/net/Socket;->isInputShutdown()Z

    move-result v6

    if-nez v6, :cond_1

    invoke-virtual {v3}, Ljava/net/Socket;->isOutputShutdown()Z

    move-result v6

    if-nez v6, :cond_1

    .line 175
    iget-object v6, p0, Lcom/android/okhttp/Connection;->route:Lcom/android/okhttp/Route;

    invoke-virtual {v6}, Lcom/android/okhttp/Route;->getProxy()Ljava/net/Proxy;

    move-result-object v6

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/android/okhttp/Connection;->route:Lcom/android/okhttp/Route;

    invoke-virtual {v6}, Lcom/android/okhttp/Route;->getProxy()Ljava/net/Proxy;

    move-result-object v6

    invoke-virtual {v6}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v6

    sget-object v7, Ljava/net/Proxy$Type;->DIRECT:Ljava/net/Proxy$Type;

    if-eq v6, v7, :cond_3

    move v4, v5

    goto :goto_0

    .line 176
    :cond_3
    invoke-virtual {v3}, Ljava/net/Socket;->getInetAddress()Ljava/net/InetAddress;

    move-result-object v2

    .line 177
    .local v2, "addr":Ljava/net/InetAddress;
    if-eqz v2, :cond_1

    .line 178
    if-nez v0, :cond_4

    if-eqz v1, :cond_5

    .line 179
    :cond_4
    invoke-virtual {v2}, Ljava/net/InetAddress;->isAnyLocalAddress()Z

    move-result v6

    if-nez v6, :cond_1

    invoke-virtual {v2}, Ljava/net/InetAddress;->isLinkLocalAddress()Z

    move-result v6

    if-nez v6, :cond_1

    invoke-virtual {v2}, Ljava/net/InetAddress;->isLoopbackAddress()Z

    move-result v6

    if-nez v6, :cond_1

    invoke-virtual {v2}, Ljava/net/InetAddress;->isSiteLocalAddress()Z

    move-result v6

    if-nez v6, :cond_1

    invoke-virtual {v2}, Ljava/net/InetAddress;->isMulticastAddress()Z

    move-result v6

    if-nez v6, :cond_1

    :cond_5
    move v4, v5

    .line 183
    goto :goto_0
.end method

.method public newTransport(Lcom/android/okhttp/internal/http/HttpEngine;)Ljava/lang/Object;
    .locals 2
    .param p1, "httpEngine"    # Lcom/android/okhttp/internal/http/HttpEngine;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1393
    iget-object v0, p0, Lcom/android/okhttp/Connection;->spdyConnection:Lcom/android/okhttp/internal/spdy/SpdyConnection;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/android/okhttp/internal/http/SpdyTransport;

    iget-object v1, p0, Lcom/android/okhttp/Connection;->spdyConnection:Lcom/android/okhttp/internal/spdy/SpdyConnection;

    invoke-direct {v0, p1, v1}, Lcom/android/okhttp/internal/http/SpdyTransport;-><init>(Lcom/android/okhttp/internal/http/HttpEngine;Lcom/android/okhttp/internal/spdy/SpdyConnection;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/android/okhttp/internal/http/HttpTransport;

    iget-object v1, p0, Lcom/android/okhttp/Connection;->httpConnection:Lcom/android/okhttp/internal/http/HttpConnection;

    invoke-direct {v0, p1, v1}, Lcom/android/okhttp/internal/http/HttpTransport;-><init>(Lcom/android/okhttp/internal/http/HttpEngine;Lcom/android/okhttp/internal/http/HttpConnection;)V

    goto :goto_0
.end method

.method public recycleCount()I
    .locals 1

    .prologue
    .line 1453
    iget v0, p0, Lcom/android/okhttp/Connection;->recycleCount:I

    return v0
.end method

.method public requiresTunnel()Z
    .locals 2

    .prologue
    .line 1425
    iget-object v0, p0, Lcom/android/okhttp/Connection;->route:Lcom/android/okhttp/Route;

    iget-object v0, v0, Lcom/android/okhttp/Route;->address:Lcom/android/okhttp/Address;

    iget-object v0, v0, Lcom/android/okhttp/Address;->sslSocketFactory:Ljavax/net/ssl/SSLSocketFactory;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/okhttp/Connection;->route:Lcom/android/okhttp/Route;

    iget-object v0, v0, Lcom/android/okhttp/Route;->proxy:Ljava/net/Proxy;

    invoke-virtual {v0}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v0

    sget-object v1, Ljava/net/Proxy$Type;->HTTP:Ljava/net/Proxy$Type;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public resetIdleStartTime()V
    .locals 2

    .prologue
    .line 1362
    iget-object v0, p0, Lcom/android/okhttp/Connection;->spdyConnection:Lcom/android/okhttp/internal/spdy/SpdyConnection;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "spdyConnection != null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1363
    :cond_0
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/okhttp/Connection;->idleStartTimeNs:J

    .line 1364
    return-void
.end method

.method public sampleRequestConnect(ILcom/android/okhttp/Request;JJ)Lcom/android/okhttp/Response;
    .locals 39
    .param p1, "netType"    # I
    .param p2, "oriReqHeaders"    # Lcom/android/okhttp/Request;
    .param p3, "start"    # J
    .param p5, "end"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/net/URISyntaxException;
        }
    .end annotation

    .prologue
    .line 915
    const/4 v11, 0x0

    .line 917
    .local v11, "socketCandidate":Ljava/net/Socket;
    invoke-virtual/range {p0 .. p1}, Lcom/android/okhttp/Connection;->getLocalAddr(I)Ljava/net/InetAddress;

    move-result-object v8

    .line 918
    .local v8, "localAddr":Ljava/net/InetAddress;
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "connecting socket for sample request for netType "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " and get IP "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 919
    :cond_0
    if-nez v8, :cond_1

    .line 920
    new-instance v4, Ljava/io/IOException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Cannot find local Address for netType "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 922
    :cond_1
    invoke-virtual {v8}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v36

    .line 923
    .local v36, "socketName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->multiSocketStream:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-object v4, v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->sRedirectedRequestHeaders:[Lcom/android/okhttp/Request;

    aget-object v32, v4, p1

    .line 924
    .local v32, "requestHeaders":Lcom/android/okhttp/Request;
    if-nez v32, :cond_2

    .line 925
    move-object/from16 v32, p2

    .line 927
    :cond_2
    invoke-virtual/range {v32 .. v32}, Lcom/android/okhttp/Request;->getHeaders()Lcom/android/okhttp/Headers;

    move-result-object v4

    const-string v5, "Host"

    invoke-virtual {v4, v5}, Lcom/android/okhttp/Headers;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    .line 928
    .local v21, "host":Ljava/lang/String;
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Opening Socket for sending sample request by socket :"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v36

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "config :"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 931
    :cond_3
    const/16 v27, 0x1

    .line 934
    .local v27, "notOkResponse":Z
    new-instance v30, Ljava/lang/StringBuffer;

    const-string v4, "bytes="

    move-object/from16 v0, v30

    invoke-direct {v0, v4}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 936
    .local v30, "range":Ljava/lang/StringBuffer;
    move-object/from16 v0, v30

    move-wide/from16 v1, p3

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, "-"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    move-wide/from16 v0, p5

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    .line 939
    invoke-virtual/range {v32 .. v32}, Lcom/android/okhttp/Request;->uri()Ljava/net/URI;

    move-result-object v37

    .line 941
    .local v37, "uri":Ljava/net/URI;
    invoke-virtual/range {v37 .. v37}, Ljava/net/URI;->toURL()Ljava/net/URL;

    move-result-object v26

    .line 943
    .local v26, "newUrl":Ljava/net/URL;
    invoke-virtual/range {v37 .. v37}, Ljava/net/URI;->toURL()Ljava/net/URL;

    move-result-object v29

    .line 945
    .local v29, "previousUrl":Ljava/net/URL;
    invoke-virtual/range {v32 .. v32}, Lcom/android/okhttp/Request;->getHeaders()Lcom/android/okhttp/Headers;

    move-result-object v4

    const-string v5, "Host"

    invoke-virtual {v4, v5}, Lcom/android/okhttp/Headers;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    .line 946
    .local v23, "location":Ljava/lang/String;
    const/16 v35, 0x0

    .line 948
    .local v35, "rsp":Lcom/android/okhttp/Response;
    const/16 v31, 0x0

    .line 949
    .local v31, "redirectionCount":I
    invoke-virtual/range {v32 .. v32}, Lcom/android/okhttp/Request;->newBuilder()Lcom/android/okhttp/Request$Builder;

    move-result-object v4

    const-string v5, "Range"

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v5, v7}, Lcom/android/okhttp/Request$Builder;->header(Ljava/lang/String;Ljava/lang/String;)Lcom/android/okhttp/Request$Builder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/okhttp/Request$Builder;->build()Lcom/android/okhttp/Request;

    move-result-object v24

    .line 950
    .local v24, "newHeader":Lcom/android/okhttp/Request;
    :goto_0
    if-eqz v27, :cond_17

    .line 951
    invoke-static {}, Lcom/android/okhttp/internal/Platform;->get()Lcom/android/okhttp/internal/Platform;

    move-result-object v4

    move-object/from16 v0, v26

    invoke-virtual {v4, v0}, Lcom/android/okhttp/internal/Platform;->toUriLenient(Ljava/net/URL;)Ljava/net/URI;

    move-result-object v25

    .line 952
    .local v25, "newUri":Ljava/net/URI;
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "connecting to url "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {v26 .. v26}, Ljava/net/URL;->toExternalForm()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " with protocol "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {v26 .. v26}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 953
    :cond_4
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_5

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "header in sampleRequestConnect before update host: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {v32 .. v32}, Lcom/android/okhttp/Request;->getHeaders()Lcom/android/okhttp/Headers;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/okhttp/Headers;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 954
    :cond_5
    invoke-virtual/range {v24 .. v24}, Lcom/android/okhttp/Request;->newBuilder()Lcom/android/okhttp/Request$Builder;

    move-result-object v4

    move-object/from16 v0, v26

    invoke-virtual {v4, v0}, Lcom/android/okhttp/Request$Builder;->url(Ljava/net/URL;)Lcom/android/okhttp/Request$Builder;

    move-result-object v4

    const-string v5, "Host"

    invoke-virtual/range {v26 .. v26}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v5, v7}, Lcom/android/okhttp/Request$Builder;->header(Ljava/lang/String;Ljava/lang/String;)Lcom/android/okhttp/Request$Builder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/okhttp/Request$Builder;->build()Lcom/android/okhttp/Request;

    move-result-object v24

    .line 955
    invoke-virtual/range {v24 .. v24}, Lcom/android/okhttp/Request;->headers()Lcom/android/okhttp/Headers;

    move-result-object v19

    .line 956
    .local v19, "h":Lcom/android/okhttp/Headers;
    invoke-virtual/range {v26 .. v26}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v0, v1, v4}, Lcom/android/okhttp/Connection;->getProxy(ILjava/lang/String;)Ljava/net/Proxy;

    move-result-object v9

    .line 957
    .local v9, "curProxy":Ljava/net/Proxy;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->mProxy:[Ljava/net/Proxy;

    aget-object v4, v4, p1

    move-object/from16 v0, p0

    invoke-direct {v0, v9, v4}, Lcom/android/okhttp/Connection;->isSameProxy(Ljava/net/Proxy;Ljava/net/Proxy;)Z

    move-result v4

    if-nez v4, :cond_14

    const/16 v16, 0x1

    .line 958
    .local v16, "bProxyChanged":Z
    :goto_1
    if-eqz v16, :cond_7

    .line 959
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_6

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "proxy updated from "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/Connection;->mProxy:[Ljava/net/Proxy;

    aget-object v7, v7, p1

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " to "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " for netType "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 960
    :cond_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->mProxy:[Ljava/net/Proxy;

    aput-object v9, v4, p1

    .line 961
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->multiSockDestAddr:[Ljava/net/InetAddress;

    const/4 v5, 0x0

    aput-object v5, v4, p1

    .line 963
    :cond_7
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->destPorts:[I

    invoke-static/range {v25 .. v25}, Lcom/android/okhttp/internal/Util;->getEffectivePort(Ljava/net/URI;)I

    move-result v5

    aput v5, v4, p1

    .line 964
    invoke-virtual {v9}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lcom/android/okhttp/Connection;->getHttpMinorVersion()I

    move-result v5

    move-object/from16 v0, v32

    invoke-static {v0, v4, v5}, Lcom/android/okhttp/internal/http/MultiratUtil;->getRequestLine(Lcom/android/okhttp/Request;Ljava/net/Proxy$Type;I)Ljava/lang/String;

    move-result-object v33

    .line 965
    .local v33, "requestLine":Ljava/lang/String;
    move-object/from16 v0, v19

    move-object/from16 v1, v33

    invoke-static {v0, v1}, Lcom/android/okhttp/internal/http/MultiratUtil;->getRequestHeadersBytes(Lcom/android/okhttp/Headers;Ljava/lang/String;)[B

    move-result-object v20

    .line 966
    .local v20, "headBytes":[B
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_8

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "writeRequestHeaders for sampleRequestConnect: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    new-instance v7, Ljava/lang/String;

    sget-object v10, Lcom/android/okhttp/internal/Util;->UTF_8:Ljava/nio/charset/Charset;

    move-object/from16 v0, v20

    invoke-direct {v7, v0, v10}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 968
    :cond_8
    :try_start_0
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_9

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "connecting socketName :"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v36

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 969
    :cond_9
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->multiSockDestAddr:[Ljava/net/InetAddress;

    aget-object v4, v4, p1

    if-eqz v4, :cond_15

    .line 970
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/android/okhttp/Connection;->startSampleSocketCreation:J

    .line 971
    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/okhttp/Connection;->connTimeout:I

    const/4 v4, 0x1

    new-array v6, v4, [Ljava/net/InetAddress;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/Connection;->multiSockDestAddr:[Ljava/net/InetAddress;

    aget-object v7, v7, p1

    aput-object v7, v6, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->destPorts:[I

    aget v7, v4, p1

    move-object/from16 v4, p0

    invoke-direct/range {v4 .. v9}, Lcom/android/okhttp/Connection;->connect(I[Ljava/net/InetAddress;ILjava/net/InetAddress;Ljava/net/Proxy;)Ljava/net/Socket;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v11

    .line 979
    :goto_2
    :try_start_1
    const-string v4, "https"

    invoke-virtual/range {v25 .. v25}, Ljava/net/URI;->getScheme()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 980
    invoke-virtual/range {v25 .. v25}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->destPorts:[I

    aget v14, v4, p1

    invoke-virtual/range {p2 .. p2}, Lcom/android/okhttp/Request;->uri()Ljava/net/URI;

    move-result-object v4

    invoke-virtual {v4}, Ljava/net/URI;->toURL()Ljava/net/URL;

    move-result-object v4

    move-object/from16 v0, v26

    invoke-virtual {v0, v4}, Ljava/net/URL;->equals(Ljava/lang/Object;)Z

    move-result v15

    move-object/from16 v10, p0

    move-object v12, v9

    invoke-direct/range {v10 .. v15}, Lcom/android/okhttp/Connection;->upgradeToTlsForSecChunk(Ljava/net/Socket;Ljava/net/Proxy;Ljava/lang/String;IZ)Ljava/net/Socket;

    move-result-object v11

    .line 981
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_a

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "upgrade to Tls Socket "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 992
    :cond_a
    :try_start_2
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_b

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "inside try after sample connecting with:"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v36

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ", socket addr is "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " for net type "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 1000
    :cond_b
    :try_start_3
    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/okhttp/Connection;->mainTimeout:I

    if-lez v4, :cond_d

    .line 1001
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_c

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "set socket read time out from "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v11}, Ljava/net/Socket;->getSoTimeout()I

    move-result v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " to "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget v7, v0, Lcom/android/okhttp/Connection;->mainTimeout:I

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1004
    :cond_c
    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/okhttp/Connection;->mainTimeout:I

    invoke-virtual {v11, v4}, Ljava/net/Socket;->setSoTimeout(I)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_2

    .line 1011
    :cond_d
    :goto_3
    invoke-virtual {v11}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v22

    .line 1012
    .local v22, "in":Ljava/io/InputStream;
    invoke-virtual {v11}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v28

    .line 1014
    .local v28, "out":Ljava/io/OutputStream;
    move-object/from16 v0, v28

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V

    .line 1016
    invoke-static/range {v22 .. v22}, Lcom/android/okhttp/internal/http/MultiratUtil;->readResponseHeaders(Ljava/io/InputStream;)Lcom/android/okhttp/Response$Builder;

    move-result-object v4

    move-object/from16 v0, v24

    invoke-virtual {v4, v0}, Lcom/android/okhttp/Response$Builder;->request(Lcom/android/okhttp/Request;)Lcom/android/okhttp/Response$Builder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/okhttp/Response$Builder;->build()Lcom/android/okhttp/Response;

    move-result-object v35

    .line 1018
    :try_start_4
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_e

    .line 1019
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "get response "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {v35 .. v35}, Lcom/android/okhttp/Response;->statusLine()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1020
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    invoke-virtual/range {v35 .. v35}, Lcom/android/okhttp/Response;->headers()Lcom/android/okhttp/Headers;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/okhttp/Headers;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->d(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_3

    .line 1024
    :cond_e
    :goto_4
    invoke-virtual/range {v35 .. v35}, Lcom/android/okhttp/Response;->code()I

    move-result v34

    .line 1025
    .local v34, "responseCode":I
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_f

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "HttpConnection startOffset:"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, p3

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " endOffset:"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, p5

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " response code: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v34

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1026
    :cond_f
    packed-switch v34, :pswitch_data_0

    .line 1051
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->multiSocketStream:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-object v4, v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->sRedirectedRequestHeaders:[Lcom/android/okhttp/Request;

    aget-object v4, v4, p1

    if-nez v4, :cond_10

    .line 1052
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->multiSocketStream:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-object v4, v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->sRedirectedRequestHeaders:[Lcom/android/okhttp/Request;

    aput-object v24, v4, p1

    .line 1053
    :cond_10
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->multiSockDestAddr:[Ljava/net/InetAddress;

    aget-object v4, v4, p1

    if-nez v4, :cond_11

    .line 1054
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->multiSockDestAddr:[Ljava/net/InetAddress;

    invoke-virtual {v11}, Ljava/net/Socket;->getInetAddress()Ljava/net/InetAddress;

    move-result-object v5

    aput-object v5, v4, p1

    .line 1055
    :cond_11
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_12

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "set socket dest IP for local IP :"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v36

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ", socket addr is "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/Connection;->multiSockDestAddr:[Ljava/net/InetAddress;

    aget-object v7, v7, p1

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " for net type "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1057
    :cond_12
    const/16 v27, 0x0

    .line 1060
    :cond_13
    :goto_5
    invoke-virtual/range {v22 .. v22}, Ljava/io/InputStream;->close()V

    .line 1061
    invoke-virtual/range {v28 .. v28}, Ljava/io/OutputStream;->close()V

    .line 1062
    invoke-virtual {v11}, Ljava/net/Socket;->close()V

    goto/16 :goto_0

    .line 957
    .end local v16    # "bProxyChanged":Z
    .end local v20    # "headBytes":[B
    .end local v22    # "in":Ljava/io/InputStream;
    .end local v28    # "out":Ljava/io/OutputStream;
    .end local v33    # "requestLine":Ljava/lang/String;
    .end local v34    # "responseCode":I
    :cond_14
    const/16 v16, 0x0

    goto/16 :goto_1

    .line 974
    .restart local v16    # "bProxyChanged":Z
    .restart local v20    # "headBytes":[B
    .restart local v33    # "requestLine":Ljava/lang/String;
    :cond_15
    :try_start_5
    invoke-virtual/range {v25 .. v25}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v0, v1, v4}, Lcom/android/okhttp/Connection;->getAllByName(ILjava/lang/String;)[Ljava/net/InetAddress;

    move-result-object v6

    .line 975
    .local v6, "addresses":[Ljava/net/InetAddress;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/android/okhttp/Connection;->startSampleSocketCreation:J

    .line 976
    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/okhttp/Connection;->connTimeout:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->destPorts:[I

    aget v7, v4, p1

    move-object/from16 v4, p0

    invoke-direct/range {v4 .. v9}, Lcom/android/okhttp/Connection;->connect(I[Ljava/net/InetAddress;ILjava/net/InetAddress;Ljava/net/Proxy;)Ljava/net/Socket;

    move-result-object v11

    goto/16 :goto_2

    .line 984
    .end local v6    # "addresses":[Ljava/net/InetAddress;
    :catch_0
    move-exception v18

    .line 985
    .local v18, "ex":Ljava/io/IOException;
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_16

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V

    .line 986
    :cond_16
    invoke-static {v11}, Lcom/android/okhttp/internal/Util;->closeQuietly(Ljava/net/Socket;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    .line 987
    const/4 v11, 0x0

    .line 988
    const/16 v27, 0x0

    .line 1064
    .end local v9    # "curProxy":Ljava/net/Proxy;
    .end local v16    # "bProxyChanged":Z
    .end local v18    # "ex":Ljava/io/IOException;
    .end local v19    # "h":Lcom/android/okhttp/Headers;
    .end local v20    # "headBytes":[B
    .end local v25    # "newUri":Ljava/net/URI;
    .end local v33    # "requestLine":Ljava/lang/String;
    :cond_17
    return-object v35

    .line 994
    .restart local v9    # "curProxy":Ljava/net/Proxy;
    .restart local v16    # "bProxyChanged":Z
    .restart local v19    # "h":Lcom/android/okhttp/Headers;
    .restart local v20    # "headBytes":[B
    .restart local v25    # "newUri":Ljava/net/URI;
    .restart local v33    # "requestLine":Ljava/lang/String;
    :catch_1
    move-exception v17

    .line 995
    .local v17, "e":Ljava/io/IOException;
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_18

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "connecting socketName insside exception:"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v36

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 996
    :cond_18
    throw v17

    .line 1006
    .end local v17    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v17

    .line 1007
    .local v17, "e":Ljava/lang/Throwable;
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_d

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V

    goto/16 :goto_3

    .line 1031
    .end local v17    # "e":Ljava/lang/Throwable;
    .restart local v22    # "in":Ljava/io/InputStream;
    .restart local v28    # "out":Ljava/io/OutputStream;
    .restart local v34    # "responseCode":I
    :pswitch_0
    add-int/lit8 v31, v31, 0x1

    sget v4, Lcom/android/okhttp/internal/http/HttpEngine;->MAX_REDIRECTS:I

    move/from16 v0, v31

    if-le v0, v4, :cond_19

    .line 1032
    new-instance v4, Ljava/io/IOException;

    const-string v5, "Too many redirects"

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 1034
    :cond_19
    invoke-virtual/range {v35 .. v35}, Lcom/android/okhttp/Response;->headers()Lcom/android/okhttp/Headers;

    move-result-object v4

    const-string v5, "Location"

    invoke-virtual {v4, v5}, Lcom/android/okhttp/Headers;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    .line 1035
    if-nez v23, :cond_1a

    .line 1036
    const/16 v27, 0x0

    .line 1037
    goto/16 :goto_5

    .line 1039
    :cond_1a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->multiSockDestAddr:[Ljava/net/InetAddress;

    const/4 v5, 0x0

    aput-object v5, v4, p1

    .line 1040
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->multiSocketStream:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-object v4, v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->sRedirectedRequestHeaders:[Lcom/android/okhttp/Request;

    const/4 v5, 0x0

    aput-object v5, v4, p1

    .line 1041
    move-object/from16 v29, v26

    .line 1042
    new-instance v26, Ljava/net/URL;

    .end local v26    # "newUrl":Ljava/net/URL;
    move-object/from16 v0, v26

    move-object/from16 v1, v29

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v2}, Ljava/net/URL;-><init>(Ljava/net/URL;Ljava/lang/String;)V

    .line 1043
    .restart local v26    # "newUrl":Ljava/net/URL;
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_1b

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "redirected to "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {v26 .. v26}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ", prevPro="

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {v29 .. v29}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ", newPro="

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {v26 .. v26}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1044
    :cond_1b
    invoke-virtual/range {v29 .. v29}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {v26 .. v26}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_13

    .line 1045
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_1c

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    const-string v5, "redirected to a diffrent schema, not support"

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1046
    :cond_1c
    const/16 v27, 0x0

    .line 1047
    goto/16 :goto_5

    .line 1023
    .end local v34    # "responseCode":I
    :catch_3
    move-exception v4

    goto/16 :goto_4

    .line 1026
    :pswitch_data_0
    .packed-switch 0x12c
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public setHttpMinorVersion(I)V
    .locals 0
    .param p1, "httpMinorVersion"    # I

    .prologue
    .line 1416
    iput p1, p0, Lcom/android/okhttp/Connection;->httpMinorVersion:I

    .line 1417
    return-void
.end method

.method public setLogger(Lcom/android/okhttp/internal/http/MultiratLog;)V
    .locals 0
    .param p1, "log"    # Lcom/android/okhttp/internal/http/MultiratLog;

    .prologue
    .line 153
    iput-object p1, p0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    .line 154
    return-void
.end method

.method public setMainSocketDestIP(I)V
    .locals 2
    .param p1, "mainID"    # I

    .prologue
    .line 328
    iget-object v0, p0, Lcom/android/okhttp/Connection;->multiSockDestAddr:[Ljava/net/InetAddress;

    iget-object v1, p0, Lcom/android/okhttp/Connection;->socket:Ljava/net/Socket;

    invoke-virtual {v1}, Ljava/net/Socket;->getInetAddress()Ljava/net/InetAddress;

    move-result-object v1

    aput-object v1, v0, p1

    .line 329
    return-void
.end method

.method public setMultiSocketInputStream(Lcom/android/okhttp/internal/http/MultiSocketInputStream;Ljava/net/URI;)V
    .locals 7
    .param p1, "is"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;
    .param p2, "uri"    # Ljava/net/URI;

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 191
    sget-boolean v1, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setMultiSocketInputStream "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 192
    :cond_0
    iput-object p1, p0, Lcom/android/okhttp/Connection;->multiSocketStream:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    .line 193
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x2

    if-ge v0, v1, :cond_1

    .line 194
    iget-object v1, p0, Lcom/android/okhttp/Connection;->multiSockDestAddr:[Ljava/net/InetAddress;

    aput-object v6, v1, v0

    .line 193
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 196
    :cond_1
    iget-object v1, p0, Lcom/android/okhttp/Connection;->socket:Ljava/net/Socket;

    invoke-virtual {v1}, Ljava/net/Socket;->getLocalAddress()Ljava/net/InetAddress;

    move-result-object v1

    invoke-virtual {p0, v4}, Lcom/android/okhttp/Connection;->getLocalAddr(I)Ljava/net/InetAddress;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/net/InetAddress;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 197
    iget-object v1, p0, Lcom/android/okhttp/Connection;->mProxy:[Ljava/net/Proxy;

    iget-object v2, p0, Lcom/android/okhttp/Connection;->route:Lcom/android/okhttp/Route;

    invoke-virtual {v2}, Lcom/android/okhttp/Route;->getProxy()Ljava/net/Proxy;

    move-result-object v2

    aput-object v2, v1, v4

    .line 198
    iget-object v1, p0, Lcom/android/okhttp/Connection;->mProxy:[Ljava/net/Proxy;

    sget-object v2, Ljava/net/Proxy;->NO_PROXY:Ljava/net/Proxy;

    aput-object v2, v1, v5

    .line 199
    iget-object v1, p0, Lcom/android/okhttp/Connection;->destPorts:[I

    iget-object v2, p0, Lcom/android/okhttp/Connection;->socket:Ljava/net/Socket;

    invoke-virtual {v2}, Ljava/net/Socket;->getPort()I

    move-result v2

    aput v2, v1, v4

    .line 200
    iget-object v1, p0, Lcom/android/okhttp/Connection;->destPorts:[I

    invoke-static {p2}, Lcom/android/okhttp/internal/Util;->getEffectivePort(Ljava/net/URI;)I

    move-result v2

    aput v2, v1, v5

    .line 201
    iget-object v1, p0, Lcom/android/okhttp/Connection;->multiSockDestAddr:[Ljava/net/InetAddress;

    iget-object v2, p0, Lcom/android/okhttp/Connection;->socket:Ljava/net/Socket;

    invoke-virtual {v2}, Ljava/net/Socket;->getInetAddress()Ljava/net/InetAddress;

    move-result-object v2

    aput-object v2, v1, v4

    .line 202
    iget-object v1, p0, Lcom/android/okhttp/Connection;->multiSockDestAddr:[Ljava/net/InetAddress;

    aput-object v6, v1, v5

    .line 212
    :goto_1
    return-void

    .line 205
    :cond_2
    iget-object v1, p0, Lcom/android/okhttp/Connection;->mProxy:[Ljava/net/Proxy;

    iget-object v2, p0, Lcom/android/okhttp/Connection;->route:Lcom/android/okhttp/Route;

    invoke-virtual {v2}, Lcom/android/okhttp/Route;->getProxy()Ljava/net/Proxy;

    move-result-object v2

    aput-object v2, v1, v5

    .line 206
    iget-object v1, p0, Lcom/android/okhttp/Connection;->mProxy:[Ljava/net/Proxy;

    sget-object v2, Ljava/net/Proxy;->NO_PROXY:Ljava/net/Proxy;

    aput-object v2, v1, v4

    .line 207
    iget-object v1, p0, Lcom/android/okhttp/Connection;->destPorts:[I

    iget-object v2, p0, Lcom/android/okhttp/Connection;->socket:Ljava/net/Socket;

    invoke-virtual {v2}, Ljava/net/Socket;->getPort()I

    move-result v2

    aput v2, v1, v5

    .line 208
    iget-object v1, p0, Lcom/android/okhttp/Connection;->destPorts:[I

    invoke-static {p2}, Lcom/android/okhttp/internal/Util;->getEffectivePort(Ljava/net/URI;)I

    move-result v2

    aput v2, v1, v4

    .line 209
    iget-object v1, p0, Lcom/android/okhttp/Connection;->multiSockDestAddr:[Ljava/net/InetAddress;

    iget-object v2, p0, Lcom/android/okhttp/Connection;->socket:Ljava/net/Socket;

    invoke-virtual {v2}, Ljava/net/Socket;->getInetAddress()Ljava/net/InetAddress;

    move-result-object v2

    aput-object v2, v1, v5

    .line 210
    iget-object v1, p0, Lcom/android/okhttp/Connection;->multiSockDestAddr:[Ljava/net/InetAddress;

    aput-object v6, v1, v4

    goto :goto_1
.end method

.method public setOwner(Ljava/lang/Object;)V
    .locals 3
    .param p1, "owner"    # Ljava/lang/Object;

    .prologue
    .line 1116
    sget-boolean v0, Lcom/android/okhttp/internal/http/HttpEngine;->SMARTBONDING_ENABLED:Z

    if-eqz v0, :cond_0

    .line 1117
    sget-boolean v0, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setOwner for connection: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1120
    :cond_0
    invoke-virtual {p0}, Lcom/android/okhttp/Connection;->isSpdy()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1125
    :goto_0
    return-void

    .line 1121
    :cond_1
    iget-object v1, p0, Lcom/android/okhttp/Connection;->pool:Lcom/android/okhttp/ConnectionPool;

    monitor-enter v1

    .line 1122
    :try_start_0
    iget-object v0, p0, Lcom/android/okhttp/Connection;->owner:Ljava/lang/Object;

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "Connection already has an owner!"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1124
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1123
    :cond_2
    :try_start_1
    iput-object p1, p0, Lcom/android/okhttp/Connection;->owner:Ljava/lang/Object;

    .line 1124
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public setSoTimeout(I)V
    .locals 3
    .param p1, "readTimeout"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/SocketException;
        }
    .end annotation

    .prologue
    .line 337
    sget-boolean v0, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "HttpConnection setSoTimeout "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 338
    :cond_0
    iput p1, p0, Lcom/android/okhttp/Connection;->mainTimeout:I

    .line 339
    iget-object v0, p0, Lcom/android/okhttp/Connection;->socket:Ljava/net/Socket;

    invoke-virtual {v0, p1}, Ljava/net/Socket;->setSoTimeout(I)V

    .line 340
    return-void
.end method

.method public setSocket(Ljava/net/Socket;)V
    .locals 3
    .param p1, "s"    # Ljava/net/Socket;

    .prologue
    .line 1307
    sget-boolean v0, Lcom/android/okhttp/internal/http/HttpEngine;->SMARTBONDING_ENABLED:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/okhttp/Connection;->multiSocketStream:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    if-eqz v0, :cond_1

    .line 1308
    sget-boolean v0, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Main socket is changed to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1309
    :cond_0
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/net/Socket;->isClosed()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p1}, Ljava/net/Socket;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Ljava/net/Socket;->isInputShutdown()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p1}, Ljava/net/Socket;->isOutputShutdown()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1310
    iput-object p1, p0, Lcom/android/okhttp/Connection;->socket:Ljava/net/Socket;

    .line 1316
    :cond_1
    :goto_0
    return-void

    .line 1313
    :cond_2
    sget-boolean v0, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Main socket is not switched since the new socket is not active, keep the socket "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/okhttp/Connection;->socket:Ljava/net/Socket;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setTagUid(II)V
    .locals 3
    .param p1, "tag"    # I
    .param p2, "uid"    # I

    .prologue
    .line 224
    iput p1, p0, Lcom/android/okhttp/Connection;->mTag:I

    .line 225
    iput p2, p0, Lcom/android/okhttp/Connection;->mUid:I

    .line 226
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/okhttp/Connection;->bTagUidSet:Z

    .line 227
    sget-boolean v0, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/okhttp/Connection;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "set socket tag "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", uid "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 228
    :cond_0
    return-void
.end method

.method public socketAlive(Ljava/net/Socket;)Z
    .locals 1
    .param p1, "s"    # Ljava/net/Socket;

    .prologue
    .line 1073
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/net/Socket;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ljava/net/Socket;->isBound()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ljava/net/Socket;->isInputShutdown()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Ljava/net/Socket;->isOutputShutdown()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Ljava/net/Socket;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1074
    const/4 v0, 0x1

    .line 1076
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public updateReadTimeout(I)V
    .locals 2
    .param p1, "newTimeout"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1429
    iget-boolean v0, p0, Lcom/android/okhttp/Connection;->connected:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "updateReadTimeout - not connected"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1431
    :cond_0
    sget-boolean v0, Lcom/android/okhttp/internal/http/HttpEngine;->SMARTBONDING_ENABLED:Z

    if-eqz v0, :cond_2

    .line 1432
    iget-object v0, p0, Lcom/android/okhttp/Connection;->multiSocketStream:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    if-eqz v0, :cond_1

    .line 1433
    iput p1, p0, Lcom/android/okhttp/Connection;->mainTimeout:I

    .line 1442
    :goto_0
    return-void

    .line 1436
    :cond_1
    iget-object v0, p0, Lcom/android/okhttp/Connection;->socket:Ljava/net/Socket;

    invoke-virtual {v0, p1}, Ljava/net/Socket;->setSoTimeout(I)V

    goto :goto_0

    .line 1441
    :cond_2
    iget-object v0, p0, Lcom/android/okhttp/Connection;->socket:Ljava/net/Socket;

    invoke-virtual {v0, p1}, Ljava/net/Socket;->setSoTimeout(I)V

    goto :goto_0
.end method
