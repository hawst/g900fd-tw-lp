.class public final Lcom/android/okhttp/internal/Platform;
.super Ljava/lang/Object;
.source "Platform.java"


# static fields
.field private static final PLATFORM:Lcom/android/okhttp/internal/Platform;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    new-instance v0, Lcom/android/okhttp/internal/Platform;

    invoke-direct {v0}, Lcom/android/okhttp/internal/Platform;-><init>()V

    sput-object v0, Lcom/android/okhttp/internal/Platform;->PLATFORM:Lcom/android/okhttp/internal/Platform;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static concatLengthPrefixed(Ljava/util/List;)[B
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/okhttp/Protocol;",
            ">;)[B"
        }
    .end annotation

    .prologue
    .line 153
    .local p0, "protocols":Ljava/util/List;, "Ljava/util/List<Lcom/android/okhttp/Protocol;>;"
    const/4 v6, 0x0

    .line 154
    .local v6, "size":I
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/okhttp/Protocol;

    .line 155
    .local v4, "protocol":Lcom/android/okhttp/Protocol;
    iget-object v7, v4, Lcom/android/okhttp/Protocol;->name:Lcom/android/okio/ByteString;

    invoke-virtual {v7}, Lcom/android/okio/ByteString;->size()I

    move-result v7

    add-int/lit8 v7, v7, 0x1

    add-int/2addr v6, v7

    .line 156
    goto :goto_0

    .line 157
    .end local v4    # "protocol":Lcom/android/okhttp/Protocol;
    :cond_0
    new-array v5, v6, [B

    .line 158
    .local v5, "result":[B
    const/4 v2, 0x0

    .line 159
    .local v2, "pos":I
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/okhttp/Protocol;

    .line 160
    .restart local v4    # "protocol":Lcom/android/okhttp/Protocol;
    iget-object v7, v4, Lcom/android/okhttp/Protocol;->name:Lcom/android/okio/ByteString;

    invoke-virtual {v7}, Lcom/android/okio/ByteString;->size()I

    move-result v1

    .line 161
    .local v1, "nameSize":I
    add-int/lit8 v3, v2, 0x1

    .end local v2    # "pos":I
    .local v3, "pos":I
    int-to-byte v7, v1

    aput-byte v7, v5, v2

    .line 163
    iget-object v7, v4, Lcom/android/okhttp/Protocol;->name:Lcom/android/okio/ByteString;

    invoke-virtual {v7}, Lcom/android/okio/ByteString;->toByteArray()[B

    move-result-object v7

    const/4 v8, 0x0

    invoke-static {v7, v8, v5, v3, v1}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 164
    add-int v2, v3, v1

    .line 165
    .end local v3    # "pos":I
    .restart local v2    # "pos":I
    goto :goto_1

    .line 166
    .end local v1    # "nameSize":I
    .end local v4    # "protocol":Lcom/android/okhttp/Protocol;
    :cond_1
    return-object v5
.end method

.method public static get()Lcom/android/okhttp/internal/Platform;
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lcom/android/okhttp/internal/Platform;->PLATFORM:Lcom/android/okhttp/internal/Platform;

    return-object v0
.end method


# virtual methods
.method public connectSocket(Ljava/net/Socket;Ljava/net/InetSocketAddress;I)V
    .locals 0
    .param p1, "socket"    # Ljava/net/Socket;
    .param p2, "address"    # Ljava/net/InetSocketAddress;
    .param p3, "connectTimeout"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 139
    invoke-virtual {p1, p2, p3}, Ljava/net/Socket;->connect(Ljava/net/SocketAddress;I)V

    .line 140
    return-void
.end method

.method public enableTlsExtensions(Ljavax/net/ssl/SSLSocket;Ljava/lang/String;)V
    .locals 2
    .param p1, "socket"    # Ljavax/net/ssl/SSLSocket;
    .param p2, "uriHost"    # Ljava/lang/String;

    .prologue
    .line 64
    instance-of v1, p1, Lcom/android/org/conscrypt/OpenSSLSocketImpl;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 65
    check-cast v0, Lcom/android/org/conscrypt/OpenSSLSocketImpl;

    .line 66
    .local v0, "openSSLSocket":Lcom/android/org/conscrypt/OpenSSLSocketImpl;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/org/conscrypt/OpenSSLSocketImpl;->setUseSessionTickets(Z)V

    .line 67
    invoke-virtual {v0, p2}, Lcom/android/org/conscrypt/OpenSSLSocketImpl;->setHostname(Ljava/lang/String;)V

    .line 69
    .end local v0    # "openSSLSocket":Lcom/android/org/conscrypt/OpenSSLSocketImpl;
    :cond_0
    return-void
.end method

.method public getNpnSelectedProtocol(Ljavax/net/ssl/SSLSocket;)Lcom/android/okio/ByteString;
    .locals 5
    .param p1, "socket"    # Ljavax/net/ssl/SSLSocket;

    .prologue
    const/4 v3, 0x0

    .line 100
    instance-of v4, p1, Lcom/android/org/conscrypt/OpenSSLSocketImpl;

    if-nez v4, :cond_1

    .line 111
    :cond_0
    :goto_0
    return-object v3

    :cond_1
    move-object v2, p1

    .line 104
    check-cast v2, Lcom/android/org/conscrypt/OpenSSLSocketImpl;

    .line 106
    .local v2, "socketImpl":Lcom/android/org/conscrypt/OpenSSLSocketImpl;
    invoke-virtual {v2}, Lcom/android/org/conscrypt/OpenSSLSocketImpl;->getAlpnSelectedProtocol()[B

    move-result-object v0

    .line 107
    .local v0, "alpnResult":[B
    if-eqz v0, :cond_2

    .line 108
    invoke-static {v0}, Lcom/android/okio/ByteString;->of([B)Lcom/android/okio/ByteString;

    move-result-object v3

    goto :goto_0

    .line 110
    :cond_2
    invoke-virtual {v2}, Lcom/android/org/conscrypt/OpenSSLSocketImpl;->getNpnSelectedProtocol()[B

    move-result-object v1

    .line 111
    .local v1, "npnResult":[B
    if-eqz v1, :cond_0

    invoke-static {v1}, Lcom/android/okio/ByteString;->of([B)Lcom/android/okio/ByteString;

    move-result-object v3

    goto :goto_0
.end method

.method public getPrefix()Ljava/lang/String;
    .locals 1

    .prologue
    .line 144
    const-string v0, "X-Android"

    return-object v0
.end method

.method public logW(Ljava/lang/String;)V
    .locals 0
    .param p1, "warning"    # Ljava/lang/String;

    .prologue
    .line 48
    invoke-static {p1}, Ljava/lang/System;->logW(Ljava/lang/String;)V

    .line 49
    return-void
.end method

.method public newDeflaterOutputStream(Ljava/io/OutputStream;Ljava/util/zip/Deflater;Z)Ljava/io/OutputStream;
    .locals 1
    .param p1, "out"    # Ljava/io/OutputStream;
    .param p2, "deflater"    # Ljava/util/zip/Deflater;
    .param p3, "syncFlush"    # Z

    .prologue
    .line 134
    new-instance v0, Ljava/util/zip/DeflaterOutputStream;

    invoke-direct {v0, p1, p2, p3}, Ljava/util/zip/DeflaterOutputStream;-><init>(Ljava/io/OutputStream;Ljava/util/zip/Deflater;Z)V

    return-object v0
.end method

.method public setNpnProtocols(Ljavax/net/ssl/SSLSocket;Ljava/util/List;)V
    .locals 3
    .param p1, "socket"    # Ljavax/net/ssl/SSLSocket;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/net/ssl/SSLSocket;",
            "Ljava/util/List",
            "<",
            "Lcom/android/okhttp/Protocol;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 119
    .local p2, "npnProtocols":Ljava/util/List;, "Ljava/util/List<Lcom/android/okhttp/Protocol;>;"
    instance-of v2, p1, Lcom/android/org/conscrypt/OpenSSLSocketImpl;

    if-eqz v2, :cond_0

    move-object v1, p1

    .line 120
    check-cast v1, Lcom/android/org/conscrypt/OpenSSLSocketImpl;

    .line 121
    .local v1, "socketImpl":Lcom/android/org/conscrypt/OpenSSLSocketImpl;
    invoke-static {p2}, Lcom/android/okhttp/internal/Platform;->concatLengthPrefixed(Ljava/util/List;)[B

    move-result-object v0

    .line 122
    .local v0, "protocols":[B
    invoke-virtual {v1, v0}, Lcom/android/org/conscrypt/OpenSSLSocketImpl;->setAlpnProtocols([B)V

    .line 123
    invoke-virtual {v1, v0}, Lcom/android/org/conscrypt/OpenSSLSocketImpl;->setNpnProtocols([B)V

    .line 125
    .end local v0    # "protocols":[B
    .end local v1    # "socketImpl":Lcom/android/org/conscrypt/OpenSSLSocketImpl;
    :cond_0
    return-void
.end method

.method public supportTlsIntolerantServer(Ljavax/net/ssl/SSLSocket;)V
    .locals 10
    .param p1, "socket"    # Ljavax/net/ssl/SSLSocket;

    .prologue
    const/4 v9, 0x0

    .line 74
    const-string v1, "TLS_FALLBACK_SCSV"

    .line 75
    .local v1, "fallbackScsv":Ljava/lang/String;
    const/4 v4, 0x0

    .line 76
    .local v4, "socketSupportsFallbackScsv":Z
    invoke-virtual {p1}, Ljavax/net/ssl/SSLSocket;->getSupportedCipherSuites()[Ljava/lang/String;

    move-result-object v6

    .line 77
    .local v6, "supportedCipherSuites":[Ljava/lang/String;
    array-length v7, v6

    add-int/lit8 v2, v7, -0x1

    .local v2, "i":I
    :goto_0
    if-ltz v2, :cond_0

    .line 78
    aget-object v5, v6, v2

    .line 79
    .local v5, "supportedCipherSuite":Ljava/lang/String;
    const-string v7, "TLS_FALLBACK_SCSV"

    invoke-virtual {v7, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 80
    const/4 v4, 0x1

    .line 84
    .end local v5    # "supportedCipherSuite":Ljava/lang/String;
    :cond_0
    if-eqz v4, :cond_1

    .line 86
    invoke-virtual {p1}, Ljavax/net/ssl/SSLSocket;->getEnabledCipherSuites()[Ljava/lang/String;

    move-result-object v0

    .line 87
    .local v0, "enabledCipherSuites":[Ljava/lang/String;
    array-length v7, v0

    add-int/lit8 v7, v7, 0x1

    new-array v3, v7, [Ljava/lang/String;

    .line 88
    .local v3, "newEnabledCipherSuites":[Ljava/lang/String;
    array-length v7, v0

    invoke-static {v0, v9, v3, v9, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 90
    array-length v7, v3

    add-int/lit8 v7, v7, -0x1

    const-string v8, "TLS_FALLBACK_SCSV"

    aput-object v8, v3, v7

    .line 91
    invoke-virtual {p1, v3}, Ljavax/net/ssl/SSLSocket;->setEnabledCipherSuites([Ljava/lang/String;)V

    .line 93
    .end local v0    # "enabledCipherSuites":[Ljava/lang/String;
    .end local v3    # "newEnabledCipherSuites":[Ljava/lang/String;
    :cond_1
    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const-string v8, "SSLv3"

    aput-object v8, v7, v9

    invoke-virtual {p1, v7}, Ljavax/net/ssl/SSLSocket;->setEnabledProtocols([Ljava/lang/String;)V

    .line 94
    return-void

    .line 77
    .restart local v5    # "supportedCipherSuite":Ljava/lang/String;
    :cond_2
    add-int/lit8 v2, v2, -0x1

    goto :goto_0
.end method

.method public tagSocket(Ljava/net/Socket;)V
    .locals 1
    .param p1, "socket"    # Ljava/net/Socket;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/SocketException;
        }
    .end annotation

    .prologue
    .line 52
    invoke-static {}, Ldalvik/system/SocketTagger;->get()Ldalvik/system/SocketTagger;

    move-result-object v0

    invoke-virtual {v0, p1}, Ldalvik/system/SocketTagger;->tag(Ljava/net/Socket;)V

    .line 53
    return-void
.end method

.method public toUriLenient(Ljava/net/URL;)Ljava/net/URI;
    .locals 1
    .param p1, "url"    # Ljava/net/URL;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/URISyntaxException;
        }
    .end annotation

    .prologue
    .line 60
    invoke-virtual {p1}, Ljava/net/URL;->toURILenient()Ljava/net/URI;

    move-result-object v0

    return-object v0
.end method

.method public untagSocket(Ljava/net/Socket;)V
    .locals 1
    .param p1, "socket"    # Ljava/net/Socket;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/SocketException;
        }
    .end annotation

    .prologue
    .line 56
    invoke-static {}, Ldalvik/system/SocketTagger;->get()Ldalvik/system/SocketTagger;

    move-result-object v0

    invoke-virtual {v0, p1}, Ldalvik/system/SocketTagger;->untag(Ljava/net/Socket;)V

    .line 57
    return-void
.end method
