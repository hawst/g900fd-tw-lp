.class public final Lcom/android/okhttp/internal/http/MultiSocketInputStream;
.super Lcom/android/okhttp/internal/http/AbstractHttpInputStream;
.source "MultiSocketInputStream.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;,
        Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;,
        Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;,
        Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;,
        Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;
    }
.end annotation


# static fields
.field private static final BLOCKED:B = -0x1t

.field protected static BUF_Read_Speed:D = 0.0

.field protected static B_Ratio_BUF_LTE:D = 0.0

.field private static final CAL_REAL_TIME_SPEED:Z = true

.field private static final CLEARED:B = 0x4t

.field protected static final DETAIL_LOG:Z = false

.field private static final ENABLE_EXTREME_CONDITION_THREAD:Z = true

.field private static final ENABLE_TWO_CHUNK_AFTER_SLOW:Z = true

.field private static final EXIT_CANCEL:I = 0x1

.field private static final EXIT_CONNECTION_FAIL:I = 0x3

.field private static final EXIT_NOTSUPPORT:I = 0x2

.field private static final EXIT_SUCCEED:I = 0x0

.field protected static EXPECTSIZE:I = 0x0

.field private static final FULLREAD:B = 0x3t

.field protected static HANDOVER_TIME_RATIO:I = 0x0

.field protected static final HANDOVER_WAIT_INTERVAL:I = 0x1f4

.field private static final IDLE_THREAD_WAIT_INTERVAL:I = 0x3e8

.field protected static INIT_BUFFERLEN:I = 0x0

.field private static final LOADBALANCE:Z = true

.field private static final MAXBLOCK:I = 0x6

.field protected static final MAXTIMEFORSPEED:J = 0x1eL

.field protected static MAX_BUFFERLEN:I = 0x0

.field private static final MAX_DATA_BUFFER_REST_SIZE:J = 0x10000000L

.field private static final MAX_DATA_BUFFER_SIZE:J = 0x7fffffffffffffffL

.field private static final MAX_DATA_BUFFER_SIZE_MEMORY:J = 0x2000000L

.field protected static MAX_EXCEPTION_COUNT:I = 0x0

.field private static final MAX_MULTISOCK_THREAD_NUM:I = 0x4

.field protected static final MINSIZEFORSPEED:I

.field protected static final MINTIMEFORSPEED:J = 0x5L

.field protected static MIN_BYTES_TO_CALC_INIT_SPEED:I = 0x0

.field private static final MIN_DATA_FOR_HANDOVER_EXTREME:J = 0x400000L

.field protected static MIN_DIFF_TIME_FOR_HANDOVER:I = 0x0

.field protected static MIN_MILS_TO_CALC_SPEED:I = 0x0

.field private static MIN_RATIO_FOR_ONLY_ONE_INF_0V1:I = 0x0

.field private static MIN_RATIO_FOR_ONLY_ONE_INF_1V0:I = 0x0

.field private static final MIN_SIZE_FOR_MULTISOCKET:I = 0x200000

.field private static final MIN_SIZE_FOR_MULTISOCKET_CON:I = 0x500000

.field private static final MIN_TIME_TO_SUBMIT:J = 0x7d0L

.field private static final NOT_READ:B = 0x0t

.field private static final OCCUPIED:B = 0x1t

.field private static final OFFSET_TIME_TO_CHECK_STOP_SLOW:I = 0xbb8

.field private static final READING:B = 0x2t

.field private static final RR_FAILED:I = 0x1

.field private static final RR_NOT_INITIALED:I = 0x0

.field private static final RR_STOPPED:I = -0x1

.field private static final RR_SUCCESS1:I = 0x3

.field private static final RR_SUCCESS2:I = 0x4

.field private static final SAVE_BUF_TO_FILE:Z = true

.field private static final SB_EXIT_SPRATIO_LTE:I = -0x2

.field private static final SB_EXIT_SPRATIO_WIFI:I = -0x1

.field private static final SB_SHOWABNORMAL_LTE:I = -0x4

.field private static final SB_SHOWABNORMAL_STARTREADBUFFER:I = -0x5

.field private static final SB_SHOWABNORMAL_WIFI:I = -0x3

.field private static final SLOW_SOCKET_START:J = 0x7d0L

.field protected static SPEED_RATIO_FOR_EXTREME_HO:I = 0x0

.field protected static SPEED_RATIO_MAKE_STOPPED:I = 0x0

.field private static final STOP_SEC_INF_WHILE_SLOW:Z = true

.field private static final THRESHOLD_OF_SLOW_TH:I = 0x28f5

.field protected static final WAIT_FOR_FIRST_RANGEREQUEST:I = 0x1388

.field protected static final WAIT_FOR_FORCE_HANDOVER:I = 0x3e8

.field protected static WAIT_FOR_INTERFACE_TIME_OUT:I = 0x0

.field protected static final WAIT_FOR_RANGEREQUEST_TIME_OUT:I = 0x3e8

.field protected static WAIT_TIME_TILL_READ_FROM_COMMON_BUFFER:I = 0x0

.field private static WIFI2SESSION:Z = false

.field private static bConsiderWiFiOnly:Z = false

.field private static bPropInitiated:Z = false

.field private static final iContinueFullMaxBlock:I = 0x2

.field private static iCurrentMultiSocketNum:I = 0x0

.field protected static final sBufFileName_pre:Ljava/lang/String; = ".sbBuf_"

.field protected static final sBufFilePath_pre:Ljava/lang/String; = "/data/data/sbcache"

.field private static threadID:I


# instance fields
.field private BlockSize:J

.field private final MAX_JUMP_STEP:I

.field protected final MIN_BYTES_TO_CALC_SPEED:I

.field private final MIN_TIME_TO_CHECK_STOP_SLOW:I

.field private final MIN_TIME_TO_CHECK_STOP_SLOW_FOR_LOW_TH:I

.field private SEND_NO_TRAFFIC_WARN:Z

.field private TESTSPEEDRATIO:I

.field private TEST_GET_TRAFFIC:Z

.field private bAppIsReadingNow:Z

.field protected bAppReading:Z

.field private bAppStartRead:Z

.field protected bConClose:Z

.field protected bDoNotUseMultiSockSinceWiFiOnly:Z

.field private bExtremThreadExisted:Z

.field private bExtremThreadStarted:Z

.field protected bFinished:Z

.field private bHasIOException:Z

.field protected final bISHTTPS:Z

.field private bMultiSocketStarted:Z

.field private bRRSuccessInMainSocket:I

.field private bRangeRequestSuccess:I

.field private bReadFromBuffer:Z

.field private bSBUsed:Z

.field private bSourceBufferCleared:Z

.field private bStartReadBufferSBStopSent:Z

.field private bStopSecDecisionMade:Z

.field private bWaitPrinted:Z

.field private blockStatus:[B

.field private bufReadSpeed:J

.field private bufSource:Lcom/android/okio/BufferedSource;

.field private bytesForMultiSocket:J

.field private bytesRemaining:J

.field private childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

.field private connection:Lcom/android/okhttp/Connection;

.field protected connectionTime:[J

.field private cr:Ljava/net/CacheRequest;

.field protected disconnCount:[I

.field private failReasonInExtreme:I

.field private finishedThreadNum:I

.field private final iMaxBlockNumber:I

.field private inBuffer:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;",
            ">;"
        }
    .end annotation
.end field

.field protected lockAppReading:Ljava/lang/Object;

.field private logger:Lcom/android/okhttp/internal/http/MultiratLog;

.field private mBlockManager:Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;

.field protected final mCacheType:I

.field private mDataDownloaded:[J

.field protected mDiff_Http_File_Pos:J

.field protected mDownloadedFile:Ljava/io/File;

.field protected mDownloadedFileName:Ljava/lang/String;

.field private mExtDBuf:Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

.field private mExtThread:Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;

.field private mFasterInterface:I

.field private final mFullConSize:J

.field private final mHeader:Lcom/android/okhttp/Headers;

.field protected mInitPosition:J

.field private mLastReadTime:J

.field private mMainSocketAfterHandover:I

.field private mMainSocketConnectTime:J

.field private mMainSocketInterfaceID:I

.field private mMainThreadID:J

.field private mOffset:J

.field protected final mOriMainSocketID:I

.field private final mOriOffset:J

.field protected mOriginalRequestHeader:Lcom/android/okhttp/Request;

.field private mOtherSocketConnectTime:J

.field protected mRAdownloadedFile:Ljava/io/RandomAccessFile;

.field private mSecChunkEnd:J

.field private mSecChunkStart:J

.field private mSpeedForEachInterface:[J

.field private mSpeedRatio:D

.field private mStartDataOffsetForSpeedCalc:[J

.field private mStartReadTime:[J

.field private mStartTimeOffsetForSpeedCalc:[J

.field private mTimeForDownload:[J

.field private mainInput:Ljava/io/InputStream;

.field protected parentDir:Ljava/io/File;

.field private readBlockNumber:Ljava/lang/Integer;

.field private requestHandlers:[Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;

.field private restBlockSize:J

.field private restBuffer:Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

.field public sRedirectedRequestHeaders:[Lcom/android/okhttp/Request;

.field private sbUsedLocker:Ljava/lang/Object;

.field private final socketNumber:I

.field protected tmpFileLocker:Ljava/lang/Object;

.field private final totalLengthToBeRead:J

.field private final uri:Ljava/net/URI;

.field private waitTime:J


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/16 v4, 0x3e8

    const/4 v3, 0x1

    const/4 v2, 0x5

    const/4 v1, 0x0

    .line 262
    const/high16 v0, 0x80000

    sput v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->EXPECTSIZE:I

    .line 293
    const v0, 0x8000

    sput v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->INIT_BUFFERLEN:I

    .line 298
    const/high16 v0, 0x100000

    sput v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->MAX_BUFFERLEN:I

    .line 302
    const/high16 v0, 0x40000

    sput v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->MIN_BYTES_TO_CALC_INIT_SPEED:I

    .line 310
    const/16 v0, 0x7d0

    sput v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->MIN_MILS_TO_CALC_SPEED:I

    .line 314
    sget v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->INIT_BUFFERLEN:I

    sput v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->MINSIZEFORSPEED:I

    .line 333
    const/16 v0, 0x7530

    sput v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->WAIT_FOR_INTERFACE_TIME_OUT:I

    .line 393
    sput v1, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->threadID:I

    .line 413
    sput v1, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->iCurrentMultiSocketNum:I

    .line 421
    sput v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->WAIT_TIME_TILL_READ_FROM_COMMON_BUFFER:I

    .line 425
    sput v2, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->SPEED_RATIO_MAKE_STOPPED:I

    .line 429
    const/4 v0, 0x2

    sput v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->SPEED_RATIO_FOR_EXTREME_HO:I

    .line 433
    sput v3, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->MAX_EXCEPTION_COUNT:I

    .line 446
    sput v2, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->HANDOVER_TIME_RATIO:I

    .line 450
    sput v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->MIN_DIFF_TIME_FOR_HANDOVER:I

    .line 574
    sput v2, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->MIN_RATIO_FOR_ONLY_ONE_INF_0V1:I

    .line 575
    sput v2, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->MIN_RATIO_FOR_ONLY_ONE_INF_1V0:I

    .line 577
    sput-boolean v1, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->WIFI2SESSION:Z

    .line 593
    sput-boolean v3, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bConsiderWiFiOnly:Z

    .line 594
    sput-boolean v1, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bPropInitiated:Z

    .line 610
    const-wide/high16 v0, 0x400c000000000000L    # 3.5

    sput-wide v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->B_Ratio_BUF_LTE:D

    .line 611
    const-wide/high16 v0, 0x403e000000000000L    # 30.0

    sput-wide v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->BUF_Read_Speed:D

    return-void
.end method

.method public constructor <init>(Lcom/android/okio/BufferedSource;Ljava/io/InputStream;Ljava/net/CacheRequest;Lcom/android/okhttp/internal/http/HttpEngine;JJLcom/android/okhttp/Request;JZJJLjava/net/Socket;Lcom/android/okhttp/Request;ILcom/android/okhttp/internal/http/MultiratLog;)V
    .locals 11
    .param p1, "source"    # Lcom/android/okio/BufferedSource;
    .param p2, "is"    # Ljava/io/InputStream;
    .param p3, "cacheRequest"    # Ljava/net/CacheRequest;
    .param p4, "httpEngine"    # Lcom/android/okhttp/internal/http/HttpEngine;
    .param p5, "length"    # J
    .param p7, "blockSize"    # J
    .param p9, "reqHeader"    # Lcom/android/okhttp/Request;
    .param p10, "offset"    # J
    .param p12, "conClose"    # Z
    .param p13, "fullS"    # J
    .param p15, "mainResponseTime"    # J
    .param p17, "mainSocket"    # Ljava/net/Socket;
    .param p18, "originalRequestHeader"    # Lcom/android/okhttp/Request;
    .param p19, "cacheType"    # I
    .param p20, "log"    # Lcom/android/okhttp/internal/http/MultiratLog;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 650
    invoke-direct {p0, p2, p4, p3}, Lcom/android/okhttp/internal/http/AbstractHttpInputStream;-><init>(Ljava/io/InputStream;Lcom/android/okhttp/internal/http/HttpEngine;Ljava/net/CacheRequest;)V

    .line 91
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->parentDir:Ljava/io/File;

    .line 215
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bMultiSocketStarted:Z

    .line 219
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bReadFromBuffer:Z

    .line 224
    const/4 v3, 0x0

    iput v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bRRSuccessInMainSocket:I

    .line 229
    const/4 v3, 0x0

    iput v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bRangeRequestSuccess:I

    .line 258
    const-wide/16 v6, 0x0

    iput-wide v6, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesForMultiSocket:J

    .line 468
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mExtDBuf:Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

    .line 472
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mExtThread:Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;

    .line 490
    const-wide/16 v6, 0x0

    iput-wide v6, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mSpeedRatio:D

    .line 498
    const/4 v3, -0x1

    iput v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainSocketAfterHandover:I

    .line 533
    const/4 v3, 0x2

    new-array v3, v3, [Lcom/android/okhttp/Request;

    iput-object v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->sRedirectedRequestHeaders:[Lcom/android/okhttp/Request;

    .line 544
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bSBUsed:Z

    .line 545
    new-instance v3, Ljava/lang/Object;

    invoke-direct {v3}, Ljava/lang/Object;-><init>()V

    iput-object v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->sbUsedLocker:Ljava/lang/Object;

    .line 547
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    .line 548
    const-wide/16 v6, 0x0

    iput-wide v6, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mLastReadTime:J

    .line 563
    const/4 v3, 0x3

    iput v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->failReasonInExtreme:I

    .line 564
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bStartReadBufferSBStopSent:Z

    .line 565
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bHasIOException:Z

    .line 582
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bStopSecDecisionMade:Z

    .line 584
    new-instance v3, Ljava/lang/Object;

    invoke-direct {v3}, Ljava/lang/Object;-><init>()V

    iput-object v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->tmpFileLocker:Ljava/lang/Object;

    .line 588
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mDownloadedFileName:Ljava/lang/String;

    .line 589
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mDownloadedFile:Ljava/io/File;

    .line 590
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mRAdownloadedFile:Ljava/io/RandomAccessFile;

    .line 595
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bDoNotUseMultiSockSinceWiFiOnly:Z

    .line 596
    const-wide/16 v6, -0x1

    iput-wide v6, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mSecChunkStart:J

    .line 597
    const-wide/16 v6, -0x1

    iput-wide v6, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mSecChunkEnd:J

    .line 598
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->TEST_GET_TRAFFIC:Z

    .line 599
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->SEND_NO_TRAFFIC_WARN:Z

    .line 601
    const/4 v3, 0x2

    new-array v3, v3, [I

    fill-array-data v3, :array_0

    iput-object v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->disconnCount:[I

    .line 602
    const/4 v3, -0x1

    iput v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->TESTSPEEDRATIO:I

    .line 603
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bAppIsReadingNow:Z

    .line 604
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bAppReading:Z

    .line 605
    new-instance v3, Ljava/lang/Object;

    invoke-direct {v3}, Ljava/lang/Object;-><init>()V

    iput-object v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->lockAppReading:Ljava/lang/Object;

    .line 607
    const/4 v3, 0x2

    new-array v3, v3, [J

    fill-array-data v3, :array_1

    iput-object v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->connectionTime:[J

    .line 609
    const-wide/16 v6, 0x0

    iput-wide v6, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bufReadSpeed:J

    .line 651
    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    .line 652
    iput-object p1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bufSource:Lcom/android/okio/BufferedSource;

    .line 653
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bSourceBufferCleared:Z

    .line 654
    invoke-virtual {p4}, Lcom/android/okhttp/internal/http/HttpEngine;->getConnection()Lcom/android/okhttp/Connection;

    move-result-object v3

    iput-object v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->connection:Lcom/android/okhttp/Connection;

    .line 655
    iput-object p2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mainInput:Ljava/io/InputStream;

    .line 656
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "new input stream mainInput = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mainInput:Ljava/io/InputStream;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 657
    :cond_0
    iput-object p3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->cr:Ljava/net/CacheRequest;

    .line 658
    move-wide/from16 v0, p5

    iput-wide v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesRemaining:J

    .line 659
    iget-wide v6, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesRemaining:J

    const-wide/16 v8, 0x0

    cmp-long v3, v6, v8

    if-nez v3, :cond_1

    .line 660
    const/4 v3, 0x1

    invoke-virtual {p0, v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->endOfInput(Z)V

    .line 662
    :cond_1
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    iput-object v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->inBuffer:Ljava/util/HashMap;

    .line 663
    iget-object v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->inBuffer:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->clear()V

    .line 664
    new-instance v3, Ljava/lang/Integer;

    const/4 v6, 0x0

    invoke-direct {v3, v6}, Ljava/lang/Integer;-><init>(I)V

    iput-object v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->readBlockNumber:Ljava/lang/Integer;

    .line 665
    move-wide/from16 v0, p5

    iput-wide v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->totalLengthToBeRead:J

    .line 666
    const-wide/16 v6, 0x0

    iput-wide v6, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->waitTime:J

    .line 667
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bWaitPrinted:Z

    .line 668
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->restBuffer:Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

    .line 669
    move-wide/from16 v0, p7

    iput-wide v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->BlockSize:J

    .line 670
    move-wide/from16 v0, p7

    iput-wide v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->restBlockSize:J

    .line 671
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bFinished:Z

    .line 672
    const/4 v3, 0x2

    iput v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->socketNumber:I

    .line 673
    move-wide/from16 v0, p10

    iput-wide v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mOffset:J

    .line 674
    move-wide/from16 v0, p10

    iput-wide v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mOriOffset:J

    .line 675
    invoke-virtual/range {p9 .. p9}, Lcom/android/okhttp/Request;->uri()Ljava/net/URI;

    move-result-object v3

    iput-object v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->uri:Ljava/net/URI;

    .line 676
    move/from16 v0, p12

    iput-boolean v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bConClose:Z

    .line 677
    iget v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->socketNumber:I

    new-array v3, v3, [Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;

    iput-object v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->requestHandlers:[Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;

    .line 678
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bAppStartRead:Z

    .line 679
    invoke-virtual/range {p9 .. p9}, Lcom/android/okhttp/Request;->getHeaders()Lcom/android/okhttp/Headers;

    move-result-object v3

    iput-object v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mHeader:Lcom/android/okhttp/Headers;

    .line 680
    move-wide/from16 v0, p13

    iput-wide v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mFullConSize:J

    .line 682
    const-wide/16 v6, -0x1

    iput-wide v6, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainThreadID:J

    .line 683
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v4

    .line 684
    .local v4, "maxMem":J
    const-wide/16 v6, 0x2

    div-long v6, v4, v6

    iget-wide v8, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->BlockSize:J

    div-long/2addr v6, v8

    long-to-int v3, v6

    const/4 v6, 0x1

    invoke-static {v3, v6}, Ljava/lang/Math;->max(II)I

    move-result v3

    const/4 v6, 0x6

    invoke-static {v3, v6}, Ljava/lang/Math;->min(II)I

    move-result v3

    iput v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->iMaxBlockNumber:I

    .line 685
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "max memory size: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", maxBlockNumber: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->iMaxBlockNumber:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 686
    :cond_2
    new-instance v3, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;

    invoke-direct {v3, p0}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;-><init>(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)V

    iput-object v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mBlockManager:Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;

    .line 687
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bMultiSocketStarted:Z

    .line 688
    const-wide/16 v6, 0x0

    iput-wide v6, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesForMultiSocket:J

    .line 689
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bReadFromBuffer:Z

    .line 690
    iget-wide v6, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->BlockSize:J

    const-wide/16 v8, 0x4

    div-long/2addr v6, v8

    long-to-int v3, v6

    const/high16 v6, 0x300000

    invoke-static {v3, v6}, Ljava/lang/Math;->min(II)I

    move-result v3

    iput v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->MIN_BYTES_TO_CALC_SPEED:I

    .line 691
    const/4 v3, 0x2

    iget v6, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->iMaxBlockNumber:I

    div-int/lit8 v6, v6, 0x2

    add-int/lit8 v6, v6, 0x1

    invoke-static {v3, v6}, Ljava/lang/Math;->max(II)I

    move-result v3

    iput v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->MAX_JUMP_STEP:I

    .line 692
    move-wide/from16 v0, p15

    iput-wide v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainSocketConnectTime:J

    .line 695
    invoke-virtual/range {p17 .. p17}, Ljava/net/Socket;->getLocalAddress()Ljava/net/InetAddress;

    move-result-object v3

    iget-object v6, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->connection:Lcom/android/okhttp/Connection;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lcom/android/okhttp/Connection;->getLocalAddr(I)Ljava/net/InetAddress;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/net/InetAddress;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 696
    const/4 v3, 0x0

    iput v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainSocketInterfaceID:I

    .line 700
    :goto_0
    iget v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainSocketInterfaceID:I

    iput v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mOriMainSocketID:I

    .line 701
    iget-object v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->connection:Lcom/android/okhttp/Connection;

    iget v6, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mOriMainSocketID:I

    invoke-virtual {v3, v6}, Lcom/android/okhttp/Connection;->setMainSocketDestIP(I)V

    .line 703
    const/4 v3, 0x2

    new-array v3, v3, [J

    iput-object v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mDataDownloaded:[J

    .line 704
    const/4 v3, 0x2

    new-array v3, v3, [J

    iput-object v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mStartDataOffsetForSpeedCalc:[J

    .line 705
    const/4 v3, 0x2

    new-array v3, v3, [J

    iput-object v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mTimeForDownload:[J

    .line 706
    const/4 v3, 0x2

    new-array v3, v3, [J

    iput-object v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mStartTimeOffsetForSpeedCalc:[J

    .line 707
    const/4 v3, 0x2

    new-array v3, v3, [J

    iput-object v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mStartReadTime:[J

    .line 708
    const/4 v3, 0x2

    new-array v3, v3, [J

    iput-object v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mSpeedForEachInterface:[J

    .line 710
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    const/4 v3, 0x2

    if-ge v2, v3, :cond_4

    .line 711
    iget-object v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mDataDownloaded:[J

    const-wide/16 v6, 0x0

    aput-wide v6, v3, v2

    .line 712
    iget-object v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mStartDataOffsetForSpeedCalc:[J

    const-wide/16 v6, 0x0

    aput-wide v6, v3, v2

    .line 713
    iget-object v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mTimeForDownload:[J

    const-wide/16 v6, 0x0

    aput-wide v6, v3, v2

    .line 714
    iget-object v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mStartTimeOffsetForSpeedCalc:[J

    const-wide/16 v6, 0x0

    aput-wide v6, v3, v2

    .line 715
    iget-object v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mSpeedForEachInterface:[J

    const-wide/16 v6, 0x0

    aput-wide v6, v3, v2

    .line 716
    iget-object v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mStartReadTime:[J

    const-wide/16 v6, 0x0

    aput-wide v6, v3, v2

    .line 710
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 698
    .end local v2    # "i":I
    :cond_3
    const/4 v3, 0x1

    iput v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainSocketInterfaceID:I

    goto :goto_0

    .line 718
    .restart local v2    # "i":I
    :cond_4
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bExtremThreadStarted:Z

    .line 719
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bExtremThreadExisted:Z

    .line 720
    invoke-virtual/range {p18 .. p18}, Lcom/android/okhttp/Request;->newBuilder()Lcom/android/okhttp/Request$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/okhttp/Request$Builder;->build()Lcom/android/okhttp/Request;

    move-result-object v3

    iput-object v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mOriginalRequestHeader:Lcom/android/okhttp/Request;

    .line 721
    iget-object v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->sRedirectedRequestHeaders:[Lcom/android/okhttp/Request;

    iget v6, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainSocketInterfaceID:I

    invoke-virtual/range {p9 .. p9}, Lcom/android/okhttp/Request;->newBuilder()Lcom/android/okhttp/Request$Builder;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/okhttp/Request$Builder;->build()Lcom/android/okhttp/Request;

    move-result-object v7

    aput-object v7, v3, v6

    .line 722
    iget-object v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->sRedirectedRequestHeaders:[Lcom/android/okhttp/Request;

    iget v6, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainSocketInterfaceID:I

    add-int/lit8 v6, v6, 0x1

    rem-int/lit8 v6, v6, 0x2

    const/4 v7, 0x0

    aput-object v7, v3, v6

    .line 724
    iget-object v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->connection:Lcom/android/okhttp/Connection;

    invoke-virtual {v3}, Lcom/android/okhttp/Connection;->isSSLSocket()Z

    move-result v3

    iput-boolean v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bISHTTPS:Z

    .line 725
    iget-object v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->connectionTime:[J

    iget v6, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainSocketInterfaceID:I

    iget-wide v8, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainSocketConnectTime:J

    aput-wide v8, v3, v6

    .line 727
    iget-boolean v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bISHTTPS:Z

    if-eqz v3, :cond_a

    .line 728
    const/16 v3, 0x4650

    iput v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->MIN_TIME_TO_CHECK_STOP_SLOW:I

    .line 729
    const/16 v3, 0x5208

    iput v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->MIN_TIME_TO_CHECK_STOP_SLOW_FOR_LOW_TH:I

    .line 736
    :goto_2
    move/from16 v0, p19

    iput v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mCacheType:I

    .line 739
    iget-object v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mHeader:Lcom/android/okhttp/Headers;

    invoke-direct {p0, v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->checkLocalFile(Lcom/android/okhttp/Headers;)V

    .line 740
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "write to local file + "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mDownloadedFile:Ljava/io/File;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " from start pos "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-wide v8, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mInitPosition:J

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 741
    :cond_5
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_8

    .line 742
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bPropInitiated:Z

    if-nez v3, :cond_7

    .line 744
    iget-object v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    const-string v6, "Start to init prop"

    invoke-virtual {v3, v6}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 746
    const-string v3, "0v1"

    invoke-direct {p0, v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->getRatioThreshold(Ljava/lang/String;)I

    move-result v3

    sput v3, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->MIN_RATIO_FOR_ONLY_ONE_INF_0V1:I

    .line 747
    const-string v3, "1v0"

    invoke-direct {p0, v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->getRatioThreshold(Ljava/lang/String;)I

    move-result v3

    sput v3, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->MIN_RATIO_FOR_ONLY_ONE_INF_1V0:I

    .line 748
    const-string v3, "persist.sys.sb.gettraffic"

    iget-boolean v6, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->TEST_GET_TRAFFIC:Z

    invoke-direct {p0, v3, v6}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->getBooleanProp(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->TEST_GET_TRAFFIC:Z

    .line 749
    iget-boolean v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->TEST_GET_TRAFFIC:Z

    if-eqz v3, :cond_6

    .line 750
    const-string v3, "persist.sys.sb.warning.show"

    iget-boolean v6, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->SEND_NO_TRAFFIC_WARN:Z

    invoke-direct {p0, v3, v6}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->getBooleanProp(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->SEND_NO_TRAFFIC_WARN:Z

    .line 752
    :cond_6
    const-string v3, "persist.sys.sb.testinitratio"

    iget v6, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->TESTSPEEDRATIO:I

    invoke-direct {p0, v3, v6}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->getIntProp(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->TESTSPEEDRATIO:I

    .line 757
    :cond_7
    iget-object v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "CacheType is (0-File, 1-RAM, 2-None) :  "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mCacheType:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 758
    iget-object v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "MIN_RATIO_FOR_ONLY_ONE_INF_0V1 is : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget v7, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->MIN_RATIO_FOR_ONLY_ONE_INF_0V1:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 759
    iget-object v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "MIN_RATIO_FOR_ONLY_ONE_INF_1V0 is : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget v7, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->MIN_RATIO_FOR_ONLY_ONE_INF_1V0:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 760
    iget-object v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "WIFI2SESSION is : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-boolean v7, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->WIFI2SESSION:Z

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 761
    iget-object v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "bConsiderWiFiOnly is : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-boolean v7, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bConsiderWiFiOnly:Z

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 762
    iget-object v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "MAX_BUFFERLEN is : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget v7, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->MAX_BUFFERLEN:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 763
    iget-object v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "TEST_GET_TRAFFIC is : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-boolean v7, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->TEST_GET_TRAFFIC:Z

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 764
    iget-object v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "SEND_NO_TRAFFIC_WARN is : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-boolean v7, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->SEND_NO_TRAFFIC_WARN:Z

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 765
    iget-object v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "TESTSPEEDRATIO is : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->TESTSPEEDRATIO:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 766
    iget-object v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "bISHTTPS is : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-boolean v7, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bISHTTPS:Z

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 767
    iget-object v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Main Socket connection time is "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->connectionTime:[J

    iget v8, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainSocketInterfaceID:I

    aget-wide v8, v7, v8

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 769
    :cond_8
    iget-object v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mHeader:Lcom/android/okhttp/Headers;

    invoke-direct {p0, v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->checkWiFiOnly(Lcom/android/okhttp/Headers;)Z

    move-result v3

    iput-boolean v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bDoNotUseMultiSockSinceWiFiOnly:Z

    .line 770
    iget-boolean v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bDoNotUseMultiSockSinceWiFiOnly:Z

    if-eqz v3, :cond_9

    .line 771
    iget-object v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "bDoNotUseMultiSockSinceWiFiOnly is : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-boolean v7, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bDoNotUseMultiSockSinceWiFiOnly:Z

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 772
    :cond_9
    return-void

    .line 732
    :cond_a
    const/16 v3, 0x1770

    iput v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->MIN_TIME_TO_CHECK_STOP_SLOW:I

    .line 733
    const/16 v3, 0x1b58

    iput v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->MIN_TIME_TO_CHECK_STOP_SLOW_FOR_LOW_TH:I

    goto/16 :goto_2

    .line 601
    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data

    .line 607
    :array_1
    .array-data 8
        0x0
        0x0
    .end array-data
.end method

.method static synthetic access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;
    .locals 1
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J
    .locals 2
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    .prologue
    .line 73
    iget-wide v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainThreadID:J

    return-wide v0
.end method

.method static synthetic access$1000()I
    .locals 1

    .prologue
    .line 73
    sget v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->iCurrentMultiSocketNum:I

    return v0
.end method

.method static synthetic access$1008()I
    .locals 2

    .prologue
    .line 73
    sget v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->iCurrentMultiSocketNum:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->iCurrentMultiSocketNum:I

    return v0
.end method

.method static synthetic access$1010()I
    .locals 2

    .prologue
    .line 73
    sget v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->iCurrentMultiSocketNum:I

    add-int/lit8 v1, v0, -0x1

    sput v1, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->iCurrentMultiSocketNum:I

    return v0
.end method

.method static synthetic access$102(Lcom/android/okhttp/internal/http/MultiSocketInputStream;J)J
    .locals 1
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;
    .param p1, "x1"    # J

    .prologue
    .line 73
    iput-wide p1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainThreadID:J

    return-wide p1
.end method

.method static synthetic access$1100(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I
    .locals 1
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    .prologue
    .line 73
    iget v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->TESTSPEEDRATIO:I

    return v0
.end method

.method static synthetic access$1200(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)D
    .locals 2
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    .prologue
    .line 73
    iget-wide v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mSpeedRatio:D

    return-wide v0
.end method

.method static synthetic access$1202(Lcom/android/okhttp/internal/http/MultiSocketInputStream;D)D
    .locals 1
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;
    .param p1, "x1"    # D

    .prologue
    .line 73
    iput-wide p1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mSpeedRatio:D

    return-wide p1
.end method

.method static synthetic access$1300(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I
    .locals 1
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    .prologue
    .line 73
    iget v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainSocketInterfaceID:I

    return v0
.end method

.method static synthetic access$1400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J
    .locals 2
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    .prologue
    .line 73
    iget-wide v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mOffset:J

    return-wide v0
.end method

.method static synthetic access$1402(Lcom/android/okhttp/internal/http/MultiSocketInputStream;J)J
    .locals 1
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;
    .param p1, "x1"    # J

    .prologue
    .line 73
    iput-wide p1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mOffset:J

    return-wide p1
.end method

.method static synthetic access$1500(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J
    .locals 2
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    .prologue
    .line 73
    iget-wide v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesForMultiSocket:J

    return-wide v0
.end method

.method static synthetic access$1502(Lcom/android/okhttp/internal/http/MultiSocketInputStream;J)J
    .locals 1
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;
    .param p1, "x1"    # J

    .prologue
    .line 73
    iput-wide p1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesForMultiSocket:J

    return-wide p1
.end method

.method static synthetic access$1600(Lcom/android/okhttp/internal/http/MultiSocketInputStream;I)I
    .locals 1
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;
    .param p1, "x1"    # I

    .prologue
    .line 73
    invoke-direct {p0, p1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->getOtherSocketID(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$1700(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;
    .locals 1
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mExtDBuf:Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

    return-object v0
.end method

.method static synthetic access$1702(Lcom/android/okhttp/internal/http/MultiSocketInputStream;Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;)Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;
    .locals 0
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;
    .param p1, "x1"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mExtDBuf:Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

    return-object p1
.end method

.method static synthetic access$1800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->inBuffer:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B
    .locals 1
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B

    return-object v0
.end method

.method static synthetic access$1902(Lcom/android/okhttp/internal/http/MultiSocketInputStream;[B)[B
    .locals 0
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;
    .param p1, "x1"    # [B

    .prologue
    .line 73
    iput-object p1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B

    return-object p1
.end method

.method static synthetic access$200(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Ljava/net/URI;
    .locals 1
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->uri:Ljava/net/URI;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;
    .locals 1
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mExtThread:Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;

    return-object v0
.end method

.method static synthetic access$2002(Lcom/android/okhttp/internal/http/MultiSocketInputStream;Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;)Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;
    .locals 0
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;
    .param p1, "x1"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mExtThread:Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;

    return-object p1
.end method

.method static synthetic access$2108()I
    .locals 2

    .prologue
    .line 73
    sget v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->threadID:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->threadID:I

    return v0
.end method

.method static synthetic access$2200(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    .prologue
    .line 73
    iget-boolean v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bExtremThreadStarted:Z

    return v0
.end method

.method static synthetic access$2202(Lcom/android/okhttp/internal/http/MultiSocketInputStream;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;
    .param p1, "x1"    # Z

    .prologue
    .line 73
    iput-boolean p1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bExtremThreadStarted:Z

    return p1
.end method

.method static synthetic access$2300(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    .prologue
    .line 73
    iget-boolean v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bExtremThreadExisted:Z

    return v0
.end method

.method static synthetic access$2302(Lcom/android/okhttp/internal/http/MultiSocketInputStream;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;
    .param p1, "x1"    # Z

    .prologue
    .line 73
    iput-boolean p1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bExtremThreadExisted:Z

    return p1
.end method

.method static synthetic access$2400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I
    .locals 1
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    .prologue
    .line 73
    iget v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bRangeRequestSuccess:I

    return v0
.end method

.method static synthetic access$2402(Lcom/android/okhttp/internal/http/MultiSocketInputStream;I)I
    .locals 0
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;
    .param p1, "x1"    # I

    .prologue
    .line 73
    iput p1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bRangeRequestSuccess:I

    return p1
.end method

.method static synthetic access$2500(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J
    .locals 2
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    .prologue
    .line 73
    iget-wide v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mFullConSize:J

    return-wide v0
.end method

.method static synthetic access$2600(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J
    .locals 2
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    .prologue
    .line 73
    iget-wide v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mOtherSocketConnectTime:J

    return-wide v0
.end method

.method static synthetic access$2602(Lcom/android/okhttp/internal/http/MultiSocketInputStream;J)J
    .locals 1
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;
    .param p1, "x1"    # J

    .prologue
    .line 73
    iput-wide p1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mOtherSocketConnectTime:J

    return-wide p1
.end method

.method static synthetic access$2700(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J
    .locals 2
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    .prologue
    .line 73
    iget-wide v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainSocketConnectTime:J

    return-wide v0
.end method

.method static synthetic access$2802(Lcom/android/okhttp/internal/http/MultiSocketInputStream;I)I
    .locals 0
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;
    .param p1, "x1"    # I

    .prologue
    .line 73
    iput p1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainSocketAfterHandover:I

    return p1
.end method

.method static synthetic access$2900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J
    .locals 2
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    .prologue
    .line 73
    iget-wide v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->BlockSize:J

    return-wide v0
.end method

.method static synthetic access$2902(Lcom/android/okhttp/internal/http/MultiSocketInputStream;J)J
    .locals 1
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;
    .param p1, "x1"    # J

    .prologue
    .line 73
    iput-wide p1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->BlockSize:J

    return-wide p1
.end method

.method static synthetic access$300(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J
    .locals 2
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    .prologue
    .line 73
    iget-wide v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->totalLengthToBeRead:J

    return-wide v0
.end method

.method static synthetic access$3000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I
    .locals 1
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    .prologue
    .line 73
    iget v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mFasterInterface:I

    return v0
.end method

.method static synthetic access$3002(Lcom/android/okhttp/internal/http/MultiSocketInputStream;I)I
    .locals 0
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;
    .param p1, "x1"    # I

    .prologue
    .line 73
    iput p1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mFasterInterface:I

    return p1
.end method

.method static synthetic access$3100(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    .prologue
    .line 73
    iget-boolean v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bMultiSocketStarted:Z

    return v0
.end method

.method static synthetic access$3102(Lcom/android/okhttp/internal/http/MultiSocketInputStream;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;
    .param p1, "x1"    # Z

    .prologue
    .line 73
    iput-boolean p1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bMultiSocketStarted:Z

    return p1
.end method

.method static synthetic access$3200(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I
    .locals 1
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    .prologue
    .line 73
    iget v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->finishedThreadNum:I

    return v0
.end method

.method static synthetic access$3202(Lcom/android/okhttp/internal/http/MultiSocketInputStream;I)I
    .locals 0
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;
    .param p1, "x1"    # I

    .prologue
    .line 73
    iput p1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->finishedThreadNum:I

    return p1
.end method

.method static synthetic access$3208(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I
    .locals 2
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    .prologue
    .line 73
    iget v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->finishedThreadNum:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->finishedThreadNum:I

    return v0
.end method

.method static synthetic access$3300(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I
    .locals 1
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    .prologue
    .line 73
    iget v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->socketNumber:I

    return v0
.end method

.method static synthetic access$3400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;
    .locals 1
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->requestHandlers:[Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;

    return-object v0
.end method

.method static synthetic access$3500(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    .prologue
    .line 73
    iget-boolean v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->TEST_GET_TRAFFIC:Z

    return v0
.end method

.method static synthetic access$3600(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I
    .locals 1
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    .prologue
    .line 73
    iget v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->MIN_TIME_TO_CHECK_STOP_SLOW:I

    return v0
.end method

.method static synthetic access$3700(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I
    .locals 1
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    .prologue
    .line 73
    iget v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->failReasonInExtreme:I

    return v0
.end method

.method static synthetic access$3702(Lcom/android/okhttp/internal/http/MultiSocketInputStream;I)I
    .locals 0
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;
    .param p1, "x1"    # I

    .prologue
    .line 73
    iput p1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->failReasonInExtreme:I

    return p1
.end method

.method static synthetic access$3800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[J
    .locals 1
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mDataDownloaded:[J

    return-object v0
.end method

.method static synthetic access$3900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/TwoChunkInputStream;
    .locals 1
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    return-object v0
.end method

.method static synthetic access$3902(Lcom/android/okhttp/internal/http/MultiSocketInputStream;Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/TwoChunkInputStream;
    .locals 0
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;
    .param p1, "x1"    # Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    return-object p1
.end method

.method static synthetic access$400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J
    .locals 2
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    .prologue
    .line 73
    iget-wide v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mOriOffset:J

    return-wide v0
.end method

.method static synthetic access$4000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[J
    .locals 1
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mTimeForDownload:[J

    return-object v0
.end method

.method static synthetic access$4100(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    .prologue
    .line 73
    iget-boolean v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->SEND_NO_TRAFFIC_WARN:Z

    return v0
.end method

.method static synthetic access$4200(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bTwoInfDownloading()Z

    move-result v0

    return v0
.end method

.method static synthetic access$4300(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    .prologue
    .line 73
    iget-boolean v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bStopSecDecisionMade:Z

    return v0
.end method

.method static synthetic access$4302(Lcom/android/okhttp/internal/http/MultiSocketInputStream;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;
    .param p1, "x1"    # Z

    .prologue
    .line 73
    iput-boolean p1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bStopSecDecisionMade:Z

    return p1
.end method

.method static synthetic access$4400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    .prologue
    .line 73
    iget-boolean v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bReadFromBuffer:Z

    return v0
.end method

.method static synthetic access$4402(Lcom/android/okhttp/internal/http/MultiSocketInputStream;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;
    .param p1, "x1"    # Z

    .prologue
    .line 73
    iput-boolean p1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bReadFromBuffer:Z

    return p1
.end method

.method static synthetic access$4500(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[J
    .locals 1
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mStartReadTime:[J

    return-object v0
.end method

.method static synthetic access$4600(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[J
    .locals 1
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mStartDataOffsetForSpeedCalc:[J

    return-object v0
.end method

.method static synthetic access$4700(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[J
    .locals 1
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mStartTimeOffsetForSpeedCalc:[J

    return-object v0
.end method

.method static synthetic access$4800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I
    .locals 1
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    .prologue
    .line 73
    iget v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->MIN_TIME_TO_CHECK_STOP_SLOW_FOR_LOW_TH:I

    return v0
.end method

.method static synthetic access$4900()I
    .locals 1

    .prologue
    .line 73
    sget v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->MIN_RATIO_FOR_ONLY_ONE_INF_0V1:I

    return v0
.end method

.method static synthetic access$500(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->sbUsedLocker:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$5000()I
    .locals 1

    .prologue
    .line 73
    sget v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->MIN_RATIO_FOR_ONLY_ONE_INF_1V0:I

    return v0
.end method

.method static synthetic access$5100()Z
    .locals 1

    .prologue
    .line 73
    sget-boolean v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->WIFI2SESSION:Z

    return v0
.end method

.method static synthetic access$5400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->closeMainSocket()V

    return-void
.end method

.method static synthetic access$5500(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J
    .locals 2
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    .prologue
    .line 73
    iget-wide v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mLastReadTime:J

    return-wide v0
.end method

.method static synthetic access$5600(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    .prologue
    .line 73
    iget-boolean v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bAppIsReadingNow:Z

    return v0
.end method

.method static synthetic access$5700(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    .prologue
    .line 73
    iget-boolean v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bHasIOException:Z

    return v0
.end method

.method static synthetic access$5800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I
    .locals 1
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    .prologue
    .line 73
    iget v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->iMaxBlockNumber:I

    return v0
.end method

.method static synthetic access$5900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Ljava/lang/Integer;
    .locals 1
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->readBlockNumber:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$600(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    .prologue
    .line 73
    iget-boolean v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bSBUsed:Z

    return v0
.end method

.method static synthetic access$602(Lcom/android/okhttp/internal/http/MultiSocketInputStream;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;
    .param p1, "x1"    # Z

    .prologue
    .line 73
    iput-boolean p1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bSBUsed:Z

    return p1
.end method

.method static synthetic access$6100(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J
    .locals 2
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    .prologue
    .line 73
    iget-wide v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bufReadSpeed:J

    return-wide v0
.end method

.method static synthetic access$6102(Lcom/android/okhttp/internal/http/MultiSocketInputStream;J)J
    .locals 1
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;
    .param p1, "x1"    # J

    .prologue
    .line 73
    iput-wide p1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bufReadSpeed:J

    return-wide p1
.end method

.method static synthetic access$6300(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I
    .locals 1
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    .prologue
    .line 73
    iget v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->MAX_JUMP_STEP:I

    return v0
.end method

.method static synthetic access$6400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Ljava/net/CacheRequest;
    .locals 1
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->cr:Ljava/net/CacheRequest;

    return-object v0
.end method

.method static synthetic access$6500(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J
    .locals 2
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    .prologue
    .line 73
    iget-wide v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mSecChunkStart:J

    return-wide v0
.end method

.method static synthetic access$6502(Lcom/android/okhttp/internal/http/MultiSocketInputStream;J)J
    .locals 1
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;
    .param p1, "x1"    # J

    .prologue
    .line 73
    iput-wide p1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mSecChunkStart:J

    return-wide p1
.end method

.method static synthetic access$6602(Lcom/android/okhttp/internal/http/MultiSocketInputStream;J)J
    .locals 1
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;
    .param p1, "x1"    # J

    .prologue
    .line 73
    iput-wide p1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mSecChunkEnd:J

    return-wide p1
.end method

.method static synthetic access$6614(Lcom/android/okhttp/internal/http/MultiSocketInputStream;J)J
    .locals 3
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;
    .param p1, "x1"    # J

    .prologue
    .line 73
    iget-wide v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mSecChunkEnd:J

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mSecChunkEnd:J

    return-wide v0
.end method

.method static synthetic access$6700(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[J
    .locals 1
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mSpeedForEachInterface:[J

    return-object v0
.end method

.method static synthetic access$700(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J
    .locals 2
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    .prologue
    .line 73
    iget-wide v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesRemaining:J

    return-wide v0
.end method

.method static synthetic access$800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;
    .locals 1
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mBlockManager:Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;

    return-object v0
.end method

.method static synthetic access$900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/Connection;
    .locals 1
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->connection:Lcom/android/okhttp/Connection;

    return-object v0
.end method

.method private bTwoInfDownloading()Z
    .locals 1

    .prologue
    .line 2024
    invoke-virtual {p0}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->isMultiRATworking()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 2026
    :goto_0
    return v0

    .line 2025
    :cond_0
    iget-object v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bExtremThreadExisted:Z

    goto :goto_0

    .line 2026
    :cond_1
    iget-object v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    invoke-virtual {v0}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->bTwoInfDownloading()Z

    move-result v0

    goto :goto_0
.end method

.method private checkLocalFile(Lcom/android/okhttp/Headers;)V
    .locals 13
    .param p1, "header"    # Lcom/android/okhttp/Headers;

    .prologue
    const/4 v12, 0x0

    const-wide/16 v10, -0x1

    .line 785
    :try_start_0
    const-string v7, "NETWORKBOOSTER_LOCAL_FILE_NAME"

    invoke-virtual {p1, v7}, Lcom/android/okhttp/Headers;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mDownloadedFileName:Ljava/lang/String;

    .line 786
    iget-object v7, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mDownloadedFileName:Ljava/lang/String;

    if-eqz v7, :cond_5

    .line 787
    new-instance v7, Ljava/io/File;

    iget-object v8, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mDownloadedFileName:Ljava/lang/String;

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v7, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mDownloadedFile:Ljava/io/File;

    .line 788
    iget-object v7, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mDownloadedFile:Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_5

    iget-object v7, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mDownloadedFile:Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->isFile()Z

    move-result v7

    if-eqz v7, :cond_5

    .line 789
    new-instance v7, Ljava/io/RandomAccessFile;

    iget-object v8, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mDownloadedFileName:Ljava/lang/String;

    const-string v9, "rw"

    invoke-direct {v7, v8, v9}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v7, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mRAdownloadedFile:Ljava/io/RandomAccessFile;

    .line 790
    sget-boolean v7, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "[fileIO][create] "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mDownloadedFile:Ljava/io/File;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 791
    :cond_0
    const-wide/16 v4, -0x1

    .line 792
    .local v4, "fileLen":J
    const-wide/16 v8, -0x1

    iput-wide v8, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mInitPosition:J

    .line 793
    const-string v7, "NETWORKBOOSTER_LOCAL_FILE_RANGE"

    invoke-virtual {p1, v7}, Lcom/android/okhttp/Headers;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 794
    .local v0, "cr":Ljava/lang/String;
    if-eqz v0, :cond_2

    .line 795
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v3

    .line 796
    .local v3, "index":I
    if-lez v3, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    if-eq v3, v7, :cond_1

    .line 797
    add-int/lit8 v7, v3, 0x1

    invoke-virtual {v0, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    .line 798
    .local v6, "subS":Ljava/lang/String;
    if-eqz v6, :cond_1

    invoke-virtual {v6}, Ljava/lang/String;->length()I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_2

    move-result v7

    if-lez v7, :cond_1

    .line 800
    :try_start_1
    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    move-result-wide v4

    .line 808
    .end local v6    # "subS":Ljava/lang/String;
    :cond_1
    :goto_0
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    const-string v8, "-"

    invoke-virtual {v7, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 809
    if-lez v3, :cond_2

    .line 810
    const/4 v7, 0x0

    invoke-virtual {v0, v7, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 811
    .restart local v6    # "subS":Ljava/lang/String;
    if-eqz v6, :cond_2

    invoke-virtual {v6}, Ljava/lang/String;->length()I
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_2

    move-result v7

    if-lez v7, :cond_2

    .line 813
    :try_start_3
    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    iput-wide v8, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mInitPosition:J
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1

    .line 823
    .end local v3    # "index":I
    .end local v6    # "subS":Ljava/lang/String;
    :cond_2
    :goto_1
    cmp-long v7, v4, v10

    if-nez v7, :cond_3

    :try_start_4
    iget-wide v4, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->totalLengthToBeRead:J

    .line 824
    :cond_3
    iget-wide v8, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mInitPosition:J

    cmp-long v7, v8, v10

    if-nez v7, :cond_4

    const-wide/16 v8, 0x0

    iput-wide v8, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mInitPosition:J

    .line 825
    :cond_4
    iget-object v7, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mRAdownloadedFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v7, v4, v5}, Ljava/io/RandomAccessFile;->setLength(J)V

    .line 826
    iget-wide v8, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mOriOffset:J

    iget-wide v10, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mInitPosition:J

    sub-long/2addr v8, v10

    iput-wide v8, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mDiff_Http_File_Pos:J

    .line 827
    sget-boolean v7, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v7, :cond_5

    iget-object v7, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "[fileIO][setlen] "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", current file position "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mRAdownloadedFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v9}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 836
    .end local v0    # "cr":Ljava/lang/String;
    .end local v4    # "fileLen":J
    :cond_5
    :goto_2
    return-void

    .line 802
    .restart local v0    # "cr":Ljava/lang/String;
    .restart local v3    # "index":I
    .restart local v4    # "fileLen":J
    .restart local v6    # "subS":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 803
    .local v2, "ex":Ljava/lang/Throwable;
    sget-boolean v7, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v7, :cond_6

    iget-object v7, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    invoke-virtual {v7, v2}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V

    .line 804
    :cond_6
    const-wide/16 v4, -0x1

    goto :goto_0

    .line 815
    .end local v2    # "ex":Ljava/lang/Throwable;
    :catch_1
    move-exception v2

    .line 816
    .restart local v2    # "ex":Ljava/lang/Throwable;
    sget-boolean v7, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v7, :cond_7

    iget-object v7, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    invoke-virtual {v7, v2}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V

    .line 817
    :cond_7
    const-wide/16 v8, -0x1

    iput-wide v8, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mInitPosition:J
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_1

    .line 831
    .end local v0    # "cr":Ljava/lang/String;
    .end local v2    # "ex":Ljava/lang/Throwable;
    .end local v3    # "index":I
    .end local v4    # "fileLen":J
    .end local v6    # "subS":Ljava/lang/String;
    :catch_2
    move-exception v1

    .line 832
    .local v1, "e":Ljava/lang/Throwable;
    sget-boolean v7, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v7, :cond_8

    iget-object v7, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    invoke-virtual {v7, v1}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V

    .line 833
    :cond_8
    iput-object v12, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mDownloadedFile:Ljava/io/File;

    .line 834
    iput-object v12, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mRAdownloadedFile:Ljava/io/RandomAccessFile;

    goto :goto_2
.end method

.method private checkLocalFileFromFirstRead([B)V
    .locals 9
    .param p1, "bFileName"    # [B

    .prologue
    const/4 v8, 0x0

    .line 840
    if-eqz p1, :cond_0

    :try_start_0
    array-length v1, p1

    if-nez v1, :cond_1

    .line 860
    :cond_0
    :goto_0
    return-void

    .line 841
    :cond_1
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, p1}, Ljava/lang/String;-><init>([B)V

    iput-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mDownloadedFileName:Ljava/lang/String;

    .line 842
    sget-boolean v1, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[fileIO][create-fname] "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mDownloadedFileName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 843
    :cond_2
    new-instance v1, Ljava/io/File;

    iget-object v4, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mDownloadedFileName:Ljava/lang/String;

    invoke-direct {v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mDownloadedFile:Ljava/io/File;

    .line 844
    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mDownloadedFile:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mDownloadedFile:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->isFile()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 845
    new-instance v1, Ljava/io/RandomAccessFile;

    iget-object v4, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mDownloadedFileName:Ljava/lang/String;

    const-string v5, "rw"

    invoke-direct {v1, v4, v5}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mRAdownloadedFile:Ljava/io/RandomAccessFile;

    .line 846
    sget-boolean v1, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[fileIO][create] "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mDownloadedFile:Ljava/io/File;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 847
    :cond_3
    iget-wide v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mFullConSize:J

    .line 848
    .local v2, "fileLen":J
    iget-wide v4, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mOriOffset:J

    iput-wide v4, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mInitPosition:J

    .line 849
    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mRAdownloadedFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v1, v2, v3}, Ljava/io/RandomAccessFile;->setLength(J)V

    .line 850
    iget-wide v4, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mOriOffset:J

    iget-wide v6, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mInitPosition:J

    sub-long/2addr v4, v6

    iput-wide v4, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mDiff_Http_File_Pos:J

    .line 851
    sget-boolean v1, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[fileIO][setlen] "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", current file position "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mRAdownloadedFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v5}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", init position="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v6, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mInitPosition:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 852
    :cond_4
    sget-boolean v1, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[fileIO][create] created file FD is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mRAdownloadedFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v5}, Ljava/io/RandomAccessFile;->getFD()Ljava/io/FileDescriptor;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 855
    .end local v2    # "fileLen":J
    :catch_0
    move-exception v0

    .line 856
    .local v0, "e":Ljava/lang/Throwable;
    sget-boolean v1, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    invoke-virtual {v1, v0}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V

    .line 857
    :cond_5
    iput-object v8, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mDownloadedFile:Ljava/io/File;

    .line 858
    iput-object v8, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mRAdownloadedFile:Ljava/io/RandomAccessFile;

    goto/16 :goto_0
.end method

.method private checkWiFiOnly(Lcom/android/okhttp/Headers;)Z
    .locals 5
    .param p1, "header"    # Lcom/android/okhttp/Headers;

    .prologue
    const/4 v1, 0x0

    .line 775
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bConsiderWiFiOnly:Z

    if-nez v2, :cond_1

    .line 780
    :cond_0
    :goto_0
    return v1

    .line 776
    :cond_1
    const-string v2, "NETWORKBOOSTER_CANNOT_USE_WIFIORMOBILE"

    invoke-virtual {p1, v2}, Lcom/android/okhttp/Headers;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 777
    .local v0, "cr":Ljava/lang/String;
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "NETWORKBOOSTER_CANNOT_USE_WIFIORMOBILE="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 778
    :cond_2
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    .line 779
    const-string v2, "TRUE"

    invoke-virtual {v0, v2}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method private closeMainSocket()V
    .locals 3

    .prologue
    .line 2018
    sget-boolean v0, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "try to close maininput and socket, input is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mainInput:Ljava/io/InputStream;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", socket is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->connection:Lcom/android/okhttp/Connection;

    invoke-virtual {v2}, Lcom/android/okhttp/Connection;->getSocket()Ljava/net/Socket;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 2019
    :cond_0
    iget-object v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mainInput:Ljava/io/InputStream;

    invoke-static {v0}, Lcom/android/okhttp/internal/http/MultiratUtil;->closeQuietly(Ljava/io/Closeable;)V

    .line 2020
    iget-object v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->connection:Lcom/android/okhttp/Connection;

    invoke-virtual {v0}, Lcom/android/okhttp/Connection;->getSocket()Ljava/net/Socket;

    move-result-object v0

    invoke-static {v0}, Lcom/android/okhttp/internal/http/MultiratUtil;->closeQuietly(Ljava/net/Socket;)V

    .line 2021
    return-void
.end method

.method private getBooleanProp(Ljava/lang/String;Z)Z
    .locals 5
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "defaultValue"    # Z

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 911
    :try_start_0
    invoke-direct {p0, p1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->getProp(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 912
    .local v1, "value":Ljava/lang/String;
    if-eqz v1, :cond_0

    if-nez p2, :cond_1

    move v4, v2

    :goto_0
    invoke-static {v4}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    if-nez v4, :cond_0

    .line 913
    if-nez p2, :cond_2

    :goto_1
    move p2, v2

    .line 920
    .end local v1    # "value":Ljava/lang/String;
    .end local p2    # "defaultValue":Z
    :cond_0
    :goto_2
    return p2

    .restart local v1    # "value":Ljava/lang/String;
    .restart local p2    # "defaultValue":Z
    :cond_1
    move v4, v3

    .line 912
    goto :goto_0

    :cond_2
    move v2, v3

    .line 913
    goto :goto_1

    .line 919
    .end local v1    # "value":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 920
    .local v0, "e":Ljava/lang/Throwable;
    goto :goto_2
.end method

.method private getBufferSize()I
    .locals 6

    .prologue
    const/high16 v3, 0x100000

    .line 864
    :try_start_0
    const-string v4, "com.samsung.DB.bufsize"

    invoke-direct {p0, v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->getProp(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 865
    .local v2, "value":Ljava/lang/String;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_0

    .line 866
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 867
    .local v1, "size":I
    const/high16 v4, 0xa00000

    const/16 v5, 0x4000

    invoke-static {v5, v1}, Ljava/lang/Math;->max(II)I

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 875
    .end local v1    # "size":I
    .end local v2    # "value":Ljava/lang/String;
    :goto_0
    return v1

    .restart local v2    # "value":Ljava/lang/String;
    :cond_0
    move v1, v3

    .line 871
    goto :goto_0

    .line 874
    .end local v2    # "value":Ljava/lang/String;
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/lang/Throwable;
    move v1, v3

    .line 875
    goto :goto_0
.end method

.method private getCacheType()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 958
    :try_start_0
    const-string v3, "com.samsung.DB.cachetype"

    invoke-direct {p0, v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->getProp(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 959
    .local v1, "value":Ljava/lang/String;
    if-eqz v1, :cond_1

    const-string v3, "RAM"

    invoke-virtual {v1, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_1

    .line 960
    const/4 v2, 0x1

    .line 970
    .end local v1    # "value":Ljava/lang/String;
    :cond_0
    :goto_0
    return v2

    .line 962
    .restart local v1    # "value":Ljava/lang/String;
    :cond_1
    if-eqz v1, :cond_0

    const-string v3, "MAGIC"

    invoke-virtual {v1, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-nez v3, :cond_0

    .line 963
    const/4 v2, 0x2

    goto :goto_0

    .line 969
    .end local v1    # "value":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 970
    .local v0, "e":Ljava/lang/Throwable;
    goto :goto_0
.end method

.method private getIntProp(Ljava/lang/String;I)I
    .locals 3
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "defaultValue"    # I

    .prologue
    .line 926
    :try_start_0
    invoke-direct {p0, p1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->getProp(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 927
    .local v1, "value":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 928
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result p2

    .line 935
    .end local v1    # "value":Ljava/lang/String;
    .end local p2    # "defaultValue":I
    :cond_0
    :goto_0
    return p2

    .line 934
    .restart local p2    # "defaultValue":I
    :catch_0
    move-exception v0

    .line 935
    .local v0, "e":Ljava/lang/Throwable;
    goto :goto_0
.end method

.method private getOtherSocketID(I)I
    .locals 2
    .param p1, "sockID"    # I

    .prologue
    .line 4555
    add-int/lit8 v0, p1, 0x1

    iget v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->socketNumber:I

    rem-int/2addr v0, v1

    return v0
.end method

.method private getProp(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 975
    const/4 v4, 0x0

    .line 977
    .local v4, "value":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getprop "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v3

    .line 978
    .local v3, "process":Ljava/lang/Process;
    new-instance v2, Ljava/io/InputStreamReader;

    invoke-virtual {v3}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;

    move-result-object v5

    invoke-direct {v2, v5}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    .line 979
    .local v2, "ir":Ljava/io/InputStreamReader;
    new-instance v1, Ljava/io/BufferedReader;

    invoke-direct {v1, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 980
    .local v1, "input":Ljava/io/BufferedReader;
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    .line 981
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 986
    .end local v1    # "input":Ljava/io/BufferedReader;
    .end local v2    # "ir":Ljava/io/InputStreamReader;
    .end local v3    # "process":Ljava/lang/Process;
    :cond_0
    :goto_0
    sget-boolean v5, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getProp("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ") from exec is "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 987
    :cond_1
    return-object v4

    .line 983
    :catch_0
    move-exception v0

    .line 984
    .local v0, "ex":Ljava/lang/Throwable;
    sget-boolean v5, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    invoke-virtual {v5, v0}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private getRatioThreshold(Ljava/lang/String;)I
    .locals 6
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x5

    .line 941
    :try_start_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "persist.sys.sb.speedratio"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->getProp(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 942
    .local v2, "value":Ljava/lang/String;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_0

    .line 943
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 944
    .local v1, "ratio":I
    const v4, 0x186a0

    const/16 v5, -0x2710

    invoke-static {v5, v1}, Ljava/lang/Math;->max(II)I

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 952
    .end local v1    # "ratio":I
    .end local v2    # "value":Ljava/lang/String;
    :goto_0
    return v1

    .restart local v2    # "value":Ljava/lang/String;
    :cond_0
    move v1, v3

    .line 948
    goto :goto_0

    .line 951
    .end local v2    # "value":Ljava/lang/String;
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/lang/Throwable;
    move v1, v3

    .line 952
    goto :goto_0
.end method

.method private getWhetherUseTwoSession()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 896
    :try_start_0
    const-string v3, "com.samsung.NB.wifi2session"

    invoke-direct {p0, v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->getProp(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 897
    .local v1, "value":Ljava/lang/String;
    if-eqz v1, :cond_0

    const-string v3, "true"

    invoke-virtual {v1, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-nez v3, :cond_0

    .line 898
    const/4 v2, 0x1

    .line 905
    .end local v1    # "value":Ljava/lang/String;
    :cond_0
    :goto_0
    return v2

    .line 904
    :catch_0
    move-exception v0

    .line 905
    .local v0, "e":Ljava/lang/Throwable;
    goto :goto_0
.end method

.method private getWhetherUseWiFiOnly()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 881
    :try_start_0
    const-string v3, "com.samsung.NB.wifionly"

    invoke-direct {p0, v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->getProp(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 882
    .local v1, "value":Ljava/lang/String;
    if-eqz v1, :cond_0

    const-string v3, "false"

    invoke-virtual {v1, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-nez v3, :cond_0

    .line 883
    const/4 v2, 0x0

    .line 890
    .end local v1    # "value":Ljava/lang/String;
    :cond_0
    :goto_0
    return v2

    .line 889
    :catch_0
    move-exception v0

    .line 890
    .local v0, "e":Ljava/lang/Throwable;
    goto :goto_0
.end method

.method private readFromMainSocket([BII)I
    .locals 29
    .param p1, "buffer"    # [B
    .param p2, "offset"    # I
    .param p3, "count"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2090
    :try_start_0
    sget-boolean v5, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v5, :cond_0

    .line 2091
    :cond_0
    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->createTwoChunkInputInMain(Z)V

    .line 2092
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mStartReadTime:[J

    move-object/from16 v0, p0

    iget v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainSocketInterfaceID:I

    aget-wide v8, v5, v7

    const-wide/16 v10, 0x0

    cmp-long v5, v8, v10

    if-nez v5, :cond_1

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mStartReadTime:[J

    move-object/from16 v0, p0

    iget v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainSocketInterfaceID:I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    aput-wide v8, v5, v7

    .line 2093
    :cond_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mainInput:Ljava/io/InputStream;

    move-object/from16 v0, p1

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-virtual {v5, v0, v1, v2}, Ljava/io/InputStream;->read([BII)I

    move-result v20

    .line 2094
    .local v20, "read":I
    if-gez v20, :cond_6

    .line 2095
    new-instance v5, Ljava/io/IOException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "read return exception value "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, v20

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2103
    .end local v20    # "read":I
    :catch_0
    move-exception v17

    .line 2104
    .local v17, "e":Ljava/io/IOException;
    sget-boolean v5, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v5, :cond_2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "IOException in reading while bytesRemaining="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesRemaining:J

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " totallen="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->totalLengthToBeRead:J

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " start offset="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mOriOffset:J

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 2105
    :cond_2
    sget-boolean v5, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v5, :cond_3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V

    .line 2106
    :cond_3
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->disconnCount:[I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainSocketInterfaceID:I

    aget v8, v5, v7

    add-int/lit8 v8, v8, 0x1

    aput v8, v5, v7

    .line 2107
    sget-boolean v5, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v5, :cond_4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "SocketTimeoutException Count: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->disconnCount:[I

    const/4 v9, 0x0

    aget v8, v8, v9

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->disconnCount:[I

    const/4 v9, 0x1

    aget v8, v8, v9

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 2108
    :cond_4
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bDoNotUseMultiSockSinceWiFiOnly:Z

    if-eqz v5, :cond_8

    .line 2109
    sget-boolean v5, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v5, :cond_5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    const-string v7, "throw exception since bDoNotUseMultiSockSinceWiFiOnly is true"

    invoke-virtual {v5, v7}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 2110
    :cond_5
    throw v17

    .line 2097
    .end local v17    # "e":Ljava/io/IOException;
    .restart local v20    # "read":I
    :cond_6
    :try_start_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    if-nez v5, :cond_7

    .line 2098
    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainSocketInterfaceID:I

    move/from16 v0, v20

    int-to-long v8, v0

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v8, v9}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->incByte(IJ)V

    .line 2099
    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainSocketInterfaceID:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->setTime(I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 2202
    .end local v20    # "read":I
    :cond_7
    :goto_0
    return v20

    .line 2112
    .restart local v17    # "e":Ljava/io/IOException;
    :cond_8
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bSBUsed:Z

    if-nez v5, :cond_a

    .line 2113
    sget-boolean v5, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v5, :cond_9

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    const-string v7, "throw exception since bSBUsed is false"

    invoke-virtual {v5, v7}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 2114
    :cond_9
    throw v17

    .line 2116
    :cond_a
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->tmpFileLocker:Ljava/lang/Object;

    monitor-enter v7

    .line 2117
    :try_start_2
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    if-eqz v5, :cond_d

    .line 2118
    sget-boolean v5, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v5, :cond_b

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    const-string v8, "childIS is created"

    invoke-virtual {v5, v8}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 2119
    :cond_b
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mainInput:Ljava/io/InputStream;

    move-object/from16 v0, p1

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-virtual {v5, v0, v1, v2}, Ljava/io/InputStream;->read([BII)I

    move-result v20

    .line 2120
    .restart local v20    # "read":I
    sget-boolean v5, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v5, :cond_c

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "childIS is created and read "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move/from16 v0, v20

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 2121
    :cond_c
    monitor-exit v7

    goto :goto_0

    .line 2123
    .end local v20    # "read":I
    :catchall_0
    move-exception v5

    monitor-exit v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v5

    :cond_d
    :try_start_3
    monitor-exit v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2124
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v24

    .line 2126
    .local v24, "startWaitTime":J
    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainSocketAfterHandover:I

    const/4 v7, -0x1

    if-ne v5, v7, :cond_11

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainSocketInterfaceID:I

    add-int/lit8 v5, v5, 0x1

    rem-int/lit8 v6, v5, 0x2

    .line 2127
    .local v6, "otherInterfaceID":I
    :goto_1
    const/4 v5, -0x1

    move-object/from16 v0, p0

    iput v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainSocketAfterHandover:I

    .line 2128
    const/16 v23, 0x0

    .line 2129
    .local v23, "triedNum":I
    :goto_2
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bFinished:Z

    if-nez v5, :cond_29

    .line 2130
    const/4 v4, 0x0

    .line 2132
    .local v4, "bForceThrow":Z
    :try_start_4
    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bRRSuccessInMainSocket:I

    const/4 v7, 0x1

    if-ne v5, v7, :cond_12

    .line 2133
    const/4 v4, 0x1

    .line 2134
    sget-boolean v5, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v5, :cond_e

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    const-string v7, "RR_FAILED in main, throw exception to app"

    invoke-virtual {v5, v7}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 2135
    :cond_e
    throw v17
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    .line 2205
    :catch_1
    move-exception v18

    .line 2206
    .local v18, "ex":Ljava/io/IOException;
    sget-boolean v5, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v5, :cond_f

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Exception in mainInput Handover "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v18

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 2207
    :cond_f
    sget-boolean v5, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v5, :cond_10

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V

    .line 2208
    :cond_10
    if-eqz v4, :cond_25

    .line 2209
    throw v18

    .line 2126
    .end local v4    # "bForceThrow":Z
    .end local v6    # "otherInterfaceID":I
    .end local v18    # "ex":Ljava/io/IOException;
    .end local v23    # "triedNum":I
    :cond_11
    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainSocketAfterHandover:I

    goto :goto_1

    .line 2137
    .restart local v4    # "bForceThrow":Z
    .restart local v6    # "otherInterfaceID":I
    .restart local v23    # "triedNum":I
    :cond_12
    const/4 v5, 0x3

    move/from16 v0, v23

    if-le v0, v5, :cond_16

    .line 2138
    :try_start_5
    sget-boolean v5, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v5, :cond_13

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    const-string v7, "tried twice, check NB status"

    invoke-virtual {v5, v7}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 2139
    :cond_13
    invoke-virtual/range {p0 .. p0}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->isMultiRATworking()Z

    move-result v5

    if-nez v5, :cond_15

    .line 2140
    sget-boolean v5, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v5, :cond_14

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    const-string v7, "tried twice, and NB Status is false"

    invoke-virtual {v5, v7}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 2141
    :cond_14
    const/4 v4, 0x1

    .line 2142
    throw v17

    .line 2145
    :cond_15
    const/16 v23, 0x0

    goto :goto_2

    .line 2149
    :cond_16
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->tmpFileLocker:Ljava/lang/Object;

    move-object/from16 v28, v0

    monitor-enter v28
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    .line 2150
    :try_start_6
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    if-nez v5, :cond_22

    .line 2151
    const/16 v22, 0x0

    .line 2152
    .local v22, "s":Ljava/net/Socket;
    sget-boolean v5, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v5, :cond_17

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "try to close maininput input stream in main exception, input is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mainInput:Ljava/io/InputStream;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 2153
    :cond_17
    invoke-direct/range {p0 .. p0}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->closeMainSocket()V

    .line 2154
    sget-boolean v5, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v5, :cond_18

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "This is MainThead, try to switch to "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 2155
    :cond_18
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->connection:Lcom/android/okhttp/Connection;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mOriginalRequestHeader:Lcom/android/okhttp/Request;

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mOriOffset:J

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->totalLengthToBeRead:J

    add-long/2addr v8, v10

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesRemaining:J

    sub-long/2addr v8, v10

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mOriOffset:J

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->totalLengthToBeRead:J

    add-long/2addr v10, v12

    const-wide/16 v12, 0x1

    sub-long/2addr v10, v12

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mFullConSize:J

    const/4 v15, 0x1

    const/16 v16, 0x0

    move-object/from16 v14, p0

    invoke-virtual/range {v5 .. v16}, Lcom/android/okhttp/Connection;->extremeConditionConnect(ILcom/android/okhttp/Request;JJJLcom/android/okhttp/internal/http/MultiSocketInputStream;ZZ)Ljava/net/Socket;

    move-result-object v22

    .line 2158
    if-nez v22, :cond_19

    .line 2160
    const/4 v5, 0x1

    move-object/from16 v0, p0

    iput v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bRRSuccessInMainSocket:I

    .line 2161
    new-instance v5, Ljava/io/IOException;

    const-string v7, "Cannot Connect to Server"

    invoke-direct {v5, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 2193
    .end local v22    # "s":Ljava/net/Socket;
    :catchall_1
    move-exception v5

    monitor-exit v28
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :try_start_7
    throw v5
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1

    .line 2163
    .restart local v22    # "s":Ljava/net/Socket;
    :cond_19
    const/4 v5, 0x4

    :try_start_8
    move-object/from16 v0, p0

    iput v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bRRSuccessInMainSocket:I

    .line 2164
    sget-boolean v5, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v5, :cond_1a

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Main socket reconnected, now from "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mOriOffset:J

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->totalLengthToBeRead:J

    add-long/2addr v8, v10

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesRemaining:J

    sub-long/2addr v8, v10

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "-"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mOriOffset:J

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->totalLengthToBeRead:J

    add-long/2addr v8, v10

    const-wide/16 v10, 0x1

    sub-long/2addr v8, v10

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 2167
    :cond_1a
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainSocketInterfaceID:I

    move/from16 v19, v0

    .line 2168
    .local v19, "preMainSocketID":I
    invoke-virtual/range {v22 .. v22}, Ljava/net/Socket;->getLocalAddress()Ljava/net/InetAddress;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->connection:Lcom/android/okhttp/Connection;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/android/okhttp/Connection;->getLocalAddr(I)Ljava/net/InetAddress;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/net/InetAddress;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1f

    .line 2169
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainSocketInterfaceID:I

    .line 2174
    :goto_3
    sget-boolean v5, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v5, :cond_1b

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "This is MainThead, actually switch to "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p0

    iget v8, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainSocketInterfaceID:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " from "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, v19

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 2175
    :cond_1b
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bExtremThreadStarted:Z

    if-eqz v5, :cond_1d

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bExtremThreadExisted:Z

    if-eqz v5, :cond_1d

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainSocketInterfaceID:I

    move/from16 v0, v19

    if-eq v5, v0, :cond_1d

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mExtThread:Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;

    if-eqz v5, :cond_1d

    .line 2176
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bStopSecDecisionMade:Z

    if-nez v5, :cond_20

    .line 2177
    sget-boolean v5, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v5, :cond_1c

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    const-string v7, "Need to switch ExtremThread Socket ID"

    invoke-virtual {v5, v7}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 2178
    :cond_1c
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mExtThread:Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;

    move/from16 v0, v19

    invoke-virtual {v5, v0}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->switchSocket(I)Z

    .line 2186
    :cond_1d
    :goto_4
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->connection:Lcom/android/okhttp/Connection;

    move-object/from16 v0, v22

    invoke-virtual {v5, v0}, Lcom/android/okhttp/Connection;->setSocket(Ljava/net/Socket;)V

    .line 2187
    invoke-virtual/range {v22 .. v22}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mainInput:Ljava/io/InputStream;

    .line 2188
    sget-boolean v5, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v5, :cond_1e

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "new input stream in main exception mainInput = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mainInput:Ljava/io/InputStream;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 2193
    .end local v19    # "preMainSocketID":I
    .end local v22    # "s":Ljava/net/Socket;
    :cond_1e
    :goto_5
    monitor-exit v28
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 2194
    :try_start_9
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mainInput:Ljava/io/InputStream;

    move-object/from16 v0, p1

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-virtual {v5, v0, v1, v2}, Ljava/io/InputStream;->read([BII)I

    move-result v21

    .line 2195
    .local v21, "readLen":I
    if-gez v21, :cond_23

    .line 2196
    new-instance v5, Ljava/io/IOException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "read return exception value "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, v21

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_1

    .line 2172
    .end local v21    # "readLen":I
    .restart local v19    # "preMainSocketID":I
    .restart local v22    # "s":Ljava/net/Socket;
    :cond_1f
    const/4 v5, 0x1

    :try_start_a
    move-object/from16 v0, p0

    iput v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainSocketInterfaceID:I

    goto/16 :goto_3

    .line 2181
    :cond_20
    sget-boolean v5, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v5, :cond_21

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    const-string v7, "Do not Need to switch ExtremThread Socket ID"

    invoke-virtual {v5, v7}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 2182
    :cond_21
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mExtThread:Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mExtThread:Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;

    invoke-virtual {v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->getSocketID()I

    move-result v7

    invoke-virtual {v5, v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->startTryBoth(I)V

    goto/16 :goto_4

    .line 2191
    .end local v19    # "preMainSocketID":I
    .end local v22    # "s":Ljava/net/Socket;
    :cond_22
    sget-boolean v5, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v5, :cond_1e

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    const-string v7, "childIS is created before this "

    invoke-virtual {v5, v7}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    goto :goto_5

    .line 2198
    .restart local v21    # "readLen":I
    :cond_23
    :try_start_b
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bExtremThreadStarted:Z

    if-eqz v5, :cond_24

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    if-nez v5, :cond_24

    .line 2199
    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainSocketInterfaceID:I

    move/from16 v0, v21

    int-to-long v8, v0

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v8, v9}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->incByte(IJ)V

    .line 2200
    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainSocketInterfaceID:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->setTime(I)V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_1

    :cond_24
    move/from16 v20, v21

    .line 2202
    goto/16 :goto_0

    .line 2211
    .end local v21    # "readLen":I
    .restart local v18    # "ex":Ljava/io/IOException;
    :cond_25
    add-int/lit8 v23, v23, 0x1

    .line 2212
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sub-long v26, v8, v24

    .line 2213
    .local v26, "waitedTime":J
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mTimeForDownload:[J

    move-object/from16 v0, p0

    iget v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainSocketInterfaceID:I

    aget-wide v8, v5, v7

    const-wide/16 v10, 0x0

    cmp-long v5, v8, v10

    if-lez v5, :cond_26

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    if-nez v5, :cond_26

    .line 2214
    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainSocketInterfaceID:I

    move-object/from16 v0, p0

    move-wide/from16 v1, v26

    invoke-virtual {v0, v5, v1, v2}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->decTime(IJ)V

    .line 2215
    :cond_26
    sget v5, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->WAIT_FOR_INTERFACE_TIME_OUT:I

    if-lez v5, :cond_28

    sget v5, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->WAIT_FOR_INTERFACE_TIME_OUT:I

    int-to-long v8, v5

    cmp-long v5, v26, v8

    if-lez v5, :cond_28

    .line 2216
    sget-boolean v5, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v5, :cond_27

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "waited time "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-wide/from16 v0, v26

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " time out "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget v8, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->WAIT_FOR_INTERFACE_TIME_OUT:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 2217
    :cond_27
    throw v18

    .line 2219
    :cond_28
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->tmpFileLocker:Ljava/lang/Object;

    monitor-enter v7

    .line 2221
    :try_start_c
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->tmpFileLocker:Ljava/lang/Object;

    const-wide/16 v8, 0x1f4

    invoke-virtual {v5, v8, v9}, Ljava/lang/Object;->wait(J)V
    :try_end_c
    .catch Ljava/lang/Throwable; {:try_start_c .. :try_end_c} :catch_2
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    .line 2224
    :goto_6
    :try_start_d
    monitor-exit v7

    goto/16 :goto_2

    :catchall_2
    move-exception v5

    monitor-exit v7
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_2

    throw v5

    .line 2227
    .end local v4    # "bForceThrow":Z
    .end local v18    # "ex":Ljava/io/IOException;
    .end local v26    # "waitedTime":J
    :cond_29
    throw v17

    .line 2223
    .restart local v4    # "bForceThrow":Z
    .restart local v18    # "ex":Ljava/io/IOException;
    .restart local v26    # "waitedTime":J
    :catch_2
    move-exception v5

    goto :goto_6
.end method

.method private readFromSourceBuffer([BII)I
    .locals 4
    .param p1, "buffer"    # [B
    .param p2, "offset"    # I
    .param p3, "count"    # I

    .prologue
    .line 2069
    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bufSource:Lcom/android/okio/BufferedSource;

    invoke-interface {v1, p1, p2, p3}, Lcom/android/okio/BufferedSource;->readFromBuffer([BII)I

    move-result v0

    .line 2070
    .local v0, "len":I
    sget-boolean v1, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "read from source buffer, and get byte number "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 2071
    :cond_0
    if-gtz v0, :cond_1

    .line 2072
    const/4 v0, 0x0

    .line 2075
    .end local v0    # "len":I
    :cond_1
    return v0
.end method


# virtual methods
.method public available()I
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v6, 0x0

    const/4 v14, 0x3

    const/4 v5, 0x0

    .line 2238
    :try_start_0
    invoke-virtual {p0}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->checkNotClosed()V

    .line 2239
    const/4 v4, 0x0

    .line 2240
    .local v4, "ret":I
    iget-boolean v8, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bReadFromBuffer:Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    if-nez v8, :cond_1

    .line 2242
    :try_start_1
    iget-object v8, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mainInput:Ljava/io/InputStream;

    if-eqz v8, :cond_0

    .line 2243
    iget-object v8, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mainInput:Ljava/io/InputStream;

    invoke-virtual {v8}, Ljava/io/InputStream;->available()I
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    move-result v4

    .line 2265
    :cond_0
    :goto_0
    :try_start_2
    iget-wide v8, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesRemaining:J

    cmp-long v8, v8, v6

    if-nez v8, :cond_8

    :goto_1
    long-to-int v4, v6

    .line 2272
    .end local v4    # "ret":I
    :goto_2
    return v4

    .line 2246
    .restart local v4    # "ret":I
    :catch_0
    move-exception v1

    .line 2247
    .local v1, "e":Ljava/lang/Throwable;
    const/4 v4, 0x0

    .line 2248
    goto :goto_0

    .line 2251
    .end local v1    # "e":Ljava/lang/Throwable;
    :cond_1
    iget-object v8, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->inBuffer:Ljava/util/HashMap;

    monitor-enter v8
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    .line 2252
    :try_start_3
    iget-object v9, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->readBlockNumber:Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    if-lez v9, :cond_4

    iget-object v9, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B

    if-eqz v9, :cond_4

    .line 2253
    const/4 v9, 0x0

    iget-object v10, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->readBlockNumber:Ljava/lang/Integer;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v10

    add-int/lit8 v10, v10, -0x1

    invoke-static {v9, v10}, Ljava/lang/Math;->max(II)I

    move-result v3

    .local v3, "i":I
    :goto_3
    iget-object v9, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B

    array-length v9, v9

    if-ge v3, v9, :cond_4

    .line 2254
    iget-object v9, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B

    aget-byte v9, v9, v3

    if-eq v9, v14, :cond_2

    iget-object v9, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B

    aget-byte v9, v9, v3

    const/4 v10, 0x2

    if-eq v9, v10, :cond_2

    iget-object v9, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B

    aget-byte v9, v9, v3

    const/4 v10, -0x1

    if-ne v9, v10, :cond_3

    .line 2255
    :cond_2
    iget-object v9, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->inBuffer:Ljava/util/HashMap;

    new-instance v10, Ljava/lang/Integer;

    invoke-direct {v10, v3}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v9, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

    .line 2256
    .local v0, "buf":Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;
    int-to-long v10, v4

    invoke-virtual {v0}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->getRestLength()J

    move-result-wide v12

    add-long/2addr v10, v12

    long-to-int v4, v10

    .line 2258
    .end local v0    # "buf":Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;
    :cond_3
    iget-object v9, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B

    aget-byte v9, v9, v3

    if-eq v9, v14, :cond_7

    .line 2263
    .end local v3    # "i":I
    :cond_4
    monitor-exit v8

    goto :goto_0

    :catchall_0
    move-exception v6

    monitor-exit v8
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v6
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1

    .line 2269
    .end local v4    # "ret":I
    :catch_1
    move-exception v2

    .line 2270
    .local v2, "ex":Ljava/lang/Throwable;
    sget-boolean v6, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v6, :cond_5

    iget-object v6, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    const-string v7, "Exception in MultiSocketInputStream:available"

    invoke-virtual {v6, v7}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 2271
    :cond_5
    sget-boolean v6, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v6, :cond_6

    iget-object v6, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    invoke-virtual {v6, v2}, Lcom/android/okhttp/internal/http/MultiratLog;->w(Ljava/lang/Throwable;)V

    :cond_6
    move v4, v5

    .line 2272
    goto :goto_2

    .line 2253
    .end local v2    # "ex":Ljava/lang/Throwable;
    .restart local v3    # "i":I
    .restart local v4    # "ret":I
    :cond_7
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 2265
    .end local v3    # "i":I
    :cond_8
    int-to-long v6, v4

    :try_start_5
    iget-wide v8, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesRemaining:J

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->min(JJ)J
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_1

    move-result-wide v6

    goto :goto_1
.end method

.method protected clearBufferDir()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1006
    :try_start_0
    iget-object v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->parentDir:Ljava/io/File;

    if-nez v2, :cond_1

    .line 1007
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "it is a NULL directory "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->parentDir:Ljava/io/File;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1027
    :cond_0
    :goto_0
    return-void

    .line 1009
    :cond_1
    iget-object v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->parentDir:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-nez v2, :cond_4

    .line 1010
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "it is not a directory "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->parentDir:Ljava/io/File;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1011
    :cond_2
    iget-object v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->parentDir:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 1012
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->parentDir:Ljava/io/File;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1023
    :catch_0
    move-exception v0

    .line 1024
    .local v0, "e":Ljava/lang/Throwable;
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    invoke-virtual {v2, v0}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V

    .line 1025
    :cond_3
    iput-object v5, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->parentDir:Ljava/io/File;

    goto :goto_0

    .line 1015
    .end local v0    # "e":Ljava/lang/Throwable;
    :cond_4
    :try_start_1
    iget-object v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->parentDir:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v1

    .line 1016
    .local v1, "files":[Ljava/lang/String;
    if-eqz v1, :cond_5

    array-length v2, v1

    if-nez v2, :cond_0

    .line 1017
    :cond_5
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "this directory is empty, can be removed: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->parentDir:Ljava/io/File;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1018
    :cond_6
    iget-object v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->parentDir:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 1019
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->parentDir:Ljava/io/File;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public close()V
    .locals 22
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2280
    sget-boolean v13, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v13, :cond_0

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    const-string v14, "MultiSocketInputStream:close"

    invoke-virtual {v13, v14}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 2281
    :cond_0
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->closed:Z

    if-eqz v13, :cond_2

    .line 2373
    :cond_1
    :goto_0
    return-void

    .line 2284
    :cond_2
    const/4 v13, 0x1

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->closed:Z

    .line 2285
    const/4 v13, 0x1

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bFinished:Z

    .line 2286
    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesRemaining:J

    const-wide/16 v16, 0x0

    cmp-long v13, v14, v16

    if-eqz v13, :cond_3

    .line 2287
    invoke-virtual/range {p0 .. p0}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->unexpectedEndOfInput()V

    .line 2290
    :cond_3
    :try_start_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->inBuffer:Ljava/util/HashMap;

    monitor-enter v14
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 2291
    :try_start_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->inBuffer:Ljava/util/HashMap;

    invoke-virtual {v13}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v13

    invoke-interface {v13}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, "i$":Ljava/util/Iterator;
    :cond_4
    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_5

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/Integer;

    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v9

    .line 2292
    .local v9, "i":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->inBuffer:Ljava/util/HashMap;

    new-instance v15, Ljava/lang/Integer;

    invoke-direct {v15, v9}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v13, v15}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

    .line 2293
    .local v6, "buf":Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;
    if-eqz v6, :cond_4

    .line 2294
    invoke-virtual {v6}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->clearBuffer()V

    goto :goto_1

    .line 2298
    .end local v6    # "buf":Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;
    .end local v9    # "i":I
    .end local v10    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v13

    monitor-exit v14
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v13
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    .line 2372
    :catch_0
    move-exception v13

    goto :goto_0

    .line 2297
    .restart local v10    # "i$":Ljava/util/Iterator;
    :cond_5
    :try_start_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->inBuffer:Ljava/util/HashMap;

    invoke-virtual {v13}, Ljava/util/HashMap;->clear()V

    .line 2298
    monitor-exit v14
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2300
    :try_start_4
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->sbUsedLocker:Ljava/lang/Object;

    monitor-enter v14
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0

    .line 2301
    :try_start_5
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bSBUsed:Z

    if-eqz v13, :cond_6

    .line 2302
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesRemaining:J

    move-wide/from16 v16, v0

    const-wide/16 v18, 0x0

    cmp-long v13, v16, v18

    if-nez v13, :cond_7

    .line 2303
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainThreadID:J

    move-wide/from16 v16, v0

    const/4 v13, 0x0

    move-wide/from16 v0, v16

    invoke-static {v0, v1, v13}, Lcom/android/okhttp/internal/http/SBServiceAPI;->stopSBUsageWithReason(JI)I

    .line 2312
    :goto_2
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bSBUsed:Z

    .line 2314
    :cond_6
    monitor-exit v14
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 2316
    :try_start_6
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bExtremThreadStarted:Z

    if-eqz v13, :cond_e

    .line 2317
    move-object/from16 v0, p0

    iget v13, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->socketNumber:I

    new-array v11, v13, [J

    .line 2318
    .local v11, "lens":[J
    move-object/from16 v0, p0

    iget v13, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->socketNumber:I

    new-array v12, v13, [J

    .line 2319
    .local v12, "times":[J
    const/4 v9, 0x0

    .restart local v9    # "i":I
    :goto_3
    move-object/from16 v0, p0

    iget v13, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->socketNumber:I

    if-ge v9, v13, :cond_c

    .line 2320
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mDataDownloaded:[J

    aget-wide v16, v13, v9

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    if-nez v13, :cond_a

    const-wide/16 v14, 0x0

    :goto_4
    add-long v14, v14, v16

    aput-wide v14, v11, v9

    .line 2321
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mTimeForDownload:[J

    aget-wide v16, v13, v9

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    if-nez v13, :cond_b

    const-wide/16 v14, 0x0

    :goto_5
    add-long v14, v14, v16

    aput-wide v14, v12, v9
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_0

    .line 2319
    add-int/lit8 v9, v9, 0x1

    goto :goto_3

    .line 2304
    .end local v9    # "i":I
    .end local v11    # "lens":[J
    .end local v12    # "times":[J
    :cond_7
    :try_start_7
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bHasIOException:Z

    if-nez v13, :cond_8

    .line 2305
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainThreadID:J

    move-wide/from16 v16, v0

    const/4 v13, 0x1

    move-wide/from16 v0, v16

    invoke-static {v0, v1, v13}, Lcom/android/okhttp/internal/http/SBServiceAPI;->stopSBUsageWithReason(JI)I

    goto :goto_2

    .line 2314
    :catchall_1
    move-exception v13

    monitor-exit v14
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :try_start_8
    throw v13
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_0

    .line 2307
    :cond_8
    :try_start_9
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    if-eqz v13, :cond_9

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    invoke-virtual {v13}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->isServerReject()Z

    move-result v13

    if-eqz v13, :cond_9

    .line 2308
    const/4 v13, 0x2

    move-object/from16 v0, p0

    iput v13, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->failReasonInExtreme:I

    .line 2310
    :cond_9
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainThreadID:J

    move-wide/from16 v16, v0

    move-object/from16 v0, p0

    iget v13, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->failReasonInExtreme:I

    move-wide/from16 v0, v16

    invoke-static {v0, v1, v13}, Lcom/android/okhttp/internal/http/SBServiceAPI;->stopSBUsageWithReason(JI)I
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    goto :goto_2

    .line 2320
    .restart local v9    # "i":I
    .restart local v11    # "lens":[J
    .restart local v12    # "times":[J
    :cond_a
    :try_start_a
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    invoke-virtual {v13, v9}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->getDownloadLen(I)J

    move-result-wide v14

    goto :goto_4

    .line 2321
    :cond_b
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    invoke-virtual {v13, v9}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->getDownloadTime(I)J

    move-result-wide v14

    goto :goto_5

    .line 2323
    :cond_c
    const/4 v13, 0x0

    aget-wide v14, v12, v13

    const-wide/16 v16, 0x7d0

    cmp-long v13, v14, v16

    if-lez v13, :cond_f

    const/4 v13, 0x1

    aget-wide v14, v12, v13

    const-wide/16 v16, 0x7d0

    cmp-long v13, v14, v16

    if-lez v13, :cond_f

    const/4 v13, 0x0

    aget-wide v14, v11, v13

    const-wide/16 v16, 0x0

    cmp-long v13, v14, v16

    if-lez v13, :cond_f

    const/4 v13, 0x1

    aget-wide v14, v11, v13

    const-wide/16 v16, 0x0

    cmp-long v13, v14, v16

    if-lez v13, :cond_f

    .line 2324
    invoke-static {v11, v12}, Lcom/android/okhttp/internal/http/SBServiceAPI;->submitMultiSocketData([J[J)V

    .line 2325
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mRAdownloadedFile:Ljava/io/RandomAccessFile;

    if-nez v13, :cond_d

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bufReadSpeed:J

    const-wide/16 v16, 0x0

    cmp-long v13, v14, v16

    if-lez v13, :cond_d

    .line 2326
    const-wide/high16 v14, 0x4000000000000000L    # 2.0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bufReadSpeed:J

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    long-to-double v0, v0

    move-wide/from16 v16, v0

    const/4 v13, 0x1

    aget-wide v18, v11, v13

    const/4 v13, 0x1

    aget-wide v20, v12, v13

    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    move-wide/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->getSpeed(JJ)J

    move-result-wide v18

    move-wide/from16 v0, v18

    long-to-double v0, v0

    move-wide/from16 v18, v0

    div-double v16, v16, v18

    invoke-static/range {v14 .. v17}, Ljava/lang/Math;->max(DD)D

    move-result-wide v14

    sput-wide v14, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->B_Ratio_BUF_LTE:D

    .line 2327
    const-wide/high16 v14, 0x4034000000000000L    # 20.0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bufReadSpeed:J

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    long-to-double v0, v0

    move-wide/from16 v16, v0

    const-wide v18, 0x408f400000000000L    # 1000.0

    mul-double v16, v16, v18

    const-wide/high16 v18, 0x4020000000000000L    # 8.0

    div-double v16, v16, v18

    const-wide/high16 v18, 0x4090000000000000L    # 1024.0

    div-double v16, v16, v18

    const-wide/high16 v18, 0x4090000000000000L    # 1024.0

    div-double v16, v16, v18

    invoke-static/range {v14 .. v17}, Ljava/lang/Math;->max(DD)D

    move-result-wide v14

    sput-wide v14, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->BUF_Read_Speed:D

    .line 2329
    :cond_d
    sget-boolean v13, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v13, :cond_e

    .line 2330
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "submit speed for extreme case - socket[0]: len="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const/4 v15, 0x0

    aget-wide v16, v11, v15

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ", time="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const/4 v15, 0x0

    aget-wide v16, v12, v15

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ", speed="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const/4 v15, 0x0

    aget-wide v16, v11, v15

    const-wide/16 v18, 0x8

    mul-long v16, v16, v18

    const-wide/16 v18, 0x3e8

    mul-long v16, v16, v18

    const-wide/16 v18, 0x400

    div-long v16, v16, v18

    const/4 v15, 0x0

    aget-wide v18, v12, v15

    div-long v16, v16, v18

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "Kbps"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 2331
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "submit speed for extreme case - socket[1]: len="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const/4 v15, 0x1

    aget-wide v16, v11, v15

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ", time="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const/4 v15, 0x1

    aget-wide v16, v12, v15

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ", speed="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const/4 v15, 0x1

    aget-wide v16, v11, v15

    const-wide/16 v18, 0x8

    mul-long v16, v16, v18

    const-wide/16 v18, 0x3e8

    mul-long v16, v16, v18

    const-wide/16 v18, 0x400

    div-long v16, v16, v18

    const/4 v15, 0x1

    aget-wide v18, v12, v15

    div-long v16, v16, v18

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "Kbps"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 2332
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "update B_Ratio_BUF_LTE to be "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    sget-wide v16, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->B_Ratio_BUF_LTE:D

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " with buf read speed = "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    sget-wide v16, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->BUF_Read_Speed:D

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "MB/s"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V
    :try_end_a
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_0

    .line 2344
    .end local v9    # "i":I
    .end local v11    # "lens":[J
    .end local v12    # "times":[J
    :cond_e
    :goto_6
    :try_start_b
    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainThreadID:J

    const-wide/16 v16, 0x0

    cmp-long v13, v14, v16

    if-ltz v13, :cond_13

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->requestHandlers:[Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;

    const/4 v14, 0x0

    aget-object v13, v13, v14

    if-nez v13, :cond_13

    .line 2345
    move-object/from16 v0, p0

    iget v13, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->socketNumber:I

    mul-int/lit8 v13, v13, 0x2

    new-array v7, v13, [J

    .line 2346
    .local v7, "data":[J
    const/4 v9, 0x0

    .restart local v9    # "i":I
    :goto_7
    move-object/from16 v0, p0

    iget v13, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->socketNumber:I

    if-ge v9, v13, :cond_12

    .line 2347
    mul-int/lit8 v13, v9, 0x2

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mDataDownloaded:[J

    aget-wide v16, v14, v9

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    if-nez v14, :cond_10

    const-wide/16 v14, 0x0

    :goto_8
    add-long v14, v14, v16

    aput-wide v14, v7, v13

    .line 2348
    mul-int/lit8 v13, v9, 0x2

    add-int/lit8 v13, v13, 0x1

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mTimeForDownload:[J

    aget-wide v16, v14, v9

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    if-nez v14, :cond_11

    const-wide/16 v14, 0x0

    :goto_9
    add-long v14, v14, v16

    aput-wide v14, v7, v13
    :try_end_b
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_2

    .line 2346
    add-int/lit8 v9, v9, 0x1

    goto :goto_7

    .line 2336
    .end local v7    # "data":[J
    .restart local v11    # "lens":[J
    .restart local v12    # "times":[J
    :cond_f
    :try_start_c
    sget-boolean v13, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v13, :cond_e

    .line 2337
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "not to submit speed for extreme case - socket[0]: len="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const/4 v15, 0x0

    aget-wide v16, v11, v15

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ", time="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const/4 v15, 0x0

    aget-wide v16, v12, v15

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 2338
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "not to submit speed for extreme case - socket[1]: len="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const/4 v15, 0x1

    aget-wide v16, v11, v15

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ", time="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const/4 v15, 0x1

    aget-wide v16, v12, v15

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V
    :try_end_c
    .catch Ljava/lang/Throwable; {:try_start_c .. :try_end_c} :catch_0

    goto/16 :goto_6

    .line 2347
    .end local v11    # "lens":[J
    .end local v12    # "times":[J
    .restart local v7    # "data":[J
    :cond_10
    :try_start_d
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    invoke-virtual {v14, v9}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->getDownloadLen(I)J

    move-result-wide v14

    goto/16 :goto_8

    .line 2348
    :cond_11
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    invoke-virtual {v14, v9}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->getDownloadTime(I)J

    move-result-wide v14

    goto :goto_9

    .line 2350
    :cond_12
    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainThreadID:J

    invoke-static {v14, v15, v7}, Lcom/android/okhttp/internal/http/SBServiceAPI;->reportSBUsage(J[J)V
    :try_end_d
    .catch Ljava/lang/Throwable; {:try_start_d .. :try_end_d} :catch_2

    .line 2356
    .end local v7    # "data":[J
    .end local v9    # "i":I
    :cond_13
    :goto_a
    :try_start_e
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mRAdownloadedFile:Ljava/io/RandomAccessFile;

    if-eqz v13, :cond_14

    .line 2357
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mRAdownloadedFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v13}, Ljava/io/RandomAccessFile;->close()V
    :try_end_e
    .catch Ljava/lang/Throwable; {:try_start_e .. :try_end_e} :catch_1

    .line 2364
    :cond_14
    :goto_b
    :try_start_f
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    if-eqz v13, :cond_15

    .line 2365
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    invoke-virtual {v13}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->close()V

    .line 2368
    :cond_15
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mExtThread:Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;

    if-eqz v13, :cond_1

    .line 2369
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mExtThread:Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;

    invoke-virtual {v13}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->closeSocketAndStreams()V

    goto/16 :goto_0

    .line 2360
    :catch_1
    move-exception v8

    .line 2361
    .local v8, "e":Ljava/lang/Throwable;
    sget-boolean v13, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v13, :cond_14

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    invoke-virtual {v13, v8}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V
    :try_end_f
    .catch Ljava/lang/Throwable; {:try_start_f .. :try_end_f} :catch_0

    goto :goto_b

    .line 2353
    .end local v8    # "e":Ljava/lang/Throwable;
    :catch_2
    move-exception v13

    goto :goto_a
.end method

.method protected createBufferDir()V
    .locals 1

    .prologue
    .line 995
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->parentDir:Ljava/io/File;

    .line 996
    return-void
.end method

.method protected createTwoChunkInputInMain(Z)V
    .locals 29
    .param p1, "bReversed"    # Z

    .prologue
    .line 1976
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bFinished:Z

    if-eqz v3, :cond_1

    .line 1977
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    const-string v4, "createTwoChunkInputInMain: session is finished"

    invoke-virtual {v3, v4}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 2015
    :cond_0
    :goto_0
    return-void

    .line 1980
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->tmpFileLocker:Ljava/lang/Object;

    move-object/from16 v28, v0

    monitor-enter v28

    .line 1981
    :try_start_0
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bExtremThreadStarted:Z

    if-eqz v3, :cond_5

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bExtremThreadExisted:Z

    if-nez v3, :cond_5

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    if-nez v3, :cond_5

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bRangeRequestSuccess:I

    const/4 v4, 0x4

    if-ne v3, v4, :cond_5

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bStopSecDecisionMade:Z

    if-nez v3, :cond_5

    .line 1983
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Chunk1 is finished while mRemainBytes="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesRemaining:J

    invoke-virtual {v4, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1985
    :cond_2
    :try_start_1
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesRemaining:J

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesForMultiSocket:J

    const-wide/32 v12, 0x100000

    add-long/2addr v10, v12

    cmp-long v3, v4, v10

    if-lez v3, :cond_6

    .line 1986
    move-object/from16 v0, p0

    iget v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainSocketInterfaceID:I

    .line 1987
    .local v7, "sock0":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->getSpeed(I)J

    move-result-wide v14

    .line 1988
    .local v14, "speed0":J
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainSocketInterfaceID:I

    add-int/lit8 v3, v3, 0x1

    rem-int/lit8 v8, v3, 0x2

    .line 1989
    .local v8, "sock1":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->getSpeed(I)J

    move-result-wide v16

    .line 1990
    .local v16, "speed1":J
    new-instance v3, Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mainInput:Ljava/io/InputStream;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->cr:Ljava/net/CacheRequest;

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mOriOffset:J

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->totalLengthToBeRead:J

    add-long/2addr v10, v12

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesRemaining:J

    sub-long v9, v10, v12

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mOriOffset:J

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->totalLengthToBeRead:J

    move-wide/from16 v18, v0

    add-long v12, v12, v18

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesForMultiSocket:J

    move-wide/from16 v18, v0

    sub-long v12, v12, v18

    const-wide/16 v18, 0x1

    sub-long v11, v12, v18

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mOriginalRequestHeader:Lcom/android/okhttp/Request;

    const/16 v18, 0x0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mFullConSize:J

    move-wide/from16 v20, v0

    sget v22, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->WAIT_FOR_INTERFACE_TIME_OUT:I

    const-wide/16 v24, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->connection:Lcom/android/okhttp/Connection;

    move-object/from16 v27, v0

    move-object/from16 v19, p0

    move/from16 v23, p1

    invoke-direct/range {v3 .. v27}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;-><init>(Ljava/io/InputStream;Lcom/android/okhttp/internal/http/HttpEngine;Ljava/net/CacheRequest;IIJJLcom/android/okhttp/Request;JJILcom/android/okhttp/internal/http/MultiSocketInputStream;JIZJLcom/android/okhttp/internal/http/MultiratLog;Lcom/android/okhttp/Connection;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    .line 1993
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mainInput:Ljava/io/InputStream;

    .line 1994
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "new twochunk input stream mainInput = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mainInput:Ljava/io/InputStream;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1995
    :cond_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    if-eqz v3, :cond_4

    .line 1996
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    invoke-virtual {v3}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->isSingleThreadRun()Z

    move-result v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->startSingleThreadRunning(Z)V

    .line 1997
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->connection:Lcom/android/okhttp/Connection;

    invoke-virtual {v4}, Lcom/android/okhttp/Connection;->getSocket()Ljava/net/Socket;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->startRun(Ljava/net/Socket;)V

    .line 2000
    :cond_4
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_5

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "resource check: finish reading chunk "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mOriOffset:J

    invoke-virtual {v4, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "-"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mOriOffset:J

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->totalLengthToBeRead:J

    add-long/2addr v10, v12

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesRemaining:J

    sub-long/2addr v10, v12

    const-wide/16 v12, 0x1

    sub-long/2addr v10, v12

    invoke-virtual {v4, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " with length "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->totalLengthToBeRead:J

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesRemaining:J

    sub-long/2addr v10, v12

    invoke-virtual {v4, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " from main socket, "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " now start to read from child input "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2014
    .end local v7    # "sock0":I
    .end local v8    # "sock1":I
    .end local v14    # "speed0":J
    .end local v16    # "speed1":J
    :cond_5
    :goto_1
    :try_start_2
    monitor-exit v28

    goto/16 :goto_0

    :catchall_0
    move-exception v3

    monitor-exit v28
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3

    .line 2005
    :cond_6
    const/4 v3, 0x1

    :try_start_3
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->startSingleThreadRunning(Z)V

    .line 2006
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_5

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "not to create sec thread since byteRemaining is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesRemaining:J

    invoke-virtual {v4, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " and bytesForMultiSocket is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesForMultiSocket:J

    invoke-virtual {v4, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 2010
    :catch_0
    move-exception v2

    .line 2011
    .local v2, "e":Ljava/lang/Throwable;
    :try_start_4
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_5

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    invoke-virtual {v3, v2}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1
.end method

.method protected decTime(IJ)V
    .locals 0
    .param p1, "sid"    # I
    .param p2, "time"    # J

    .prologue
    .line 2392
    return-void
.end method

.method public getMainThreadID()J
    .locals 2

    .prologue
    .line 991
    iget-wide v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainThreadID:J

    return-wide v0
.end method

.method protected getSecChunkRange()[J
    .locals 10

    .prologue
    const/4 v4, 0x2

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 2030
    new-array v1, v4, [J

    iget-wide v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mSecChunkStart:J

    aput-wide v2, v1, v9

    iget-wide v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mSecChunkEnd:J

    aput-wide v2, v1, v8

    .line 2031
    .local v1, "ret":[J
    const/4 v0, 0x0

    .line 2032
    .local v0, "childSec":[J
    iget-object v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    if-eqz v2, :cond_0

    .line 2033
    iget-object v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    invoke-virtual {v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->getSecChunkRange()[J

    move-result-object v0

    .line 2035
    :cond_0
    if-eqz v0, :cond_1

    array-length v2, v0

    if-ne v2, v4, :cond_1

    .line 2036
    aget-wide v2, v0, v8

    aget-wide v4, v1, v9

    cmp-long v2, v2, v4

    if-gez v2, :cond_2

    aget-wide v2, v0, v8

    aget-wide v4, v0, v9

    sub-long/2addr v2, v4

    aget-wide v4, v1, v8

    aget-wide v6, v1, v9

    sub-long/2addr v4, v6

    cmp-long v2, v2, v4

    if-lez v2, :cond_2

    .line 2037
    aget-wide v2, v0, v9

    aput-wide v2, v1, v9

    .line 2038
    aget-wide v2, v0, v8

    aput-wide v2, v1, v8

    .line 2051
    :cond_1
    :goto_0
    return-object v1

    .line 2040
    :cond_2
    aget-wide v2, v0, v8

    aget-wide v4, v1, v9

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    aget-wide v2, v0, v9

    aget-wide v4, v1, v9

    cmp-long v2, v2, v4

    if-gez v2, :cond_3

    .line 2041
    aget-wide v2, v0, v9

    aput-wide v2, v1, v9

    goto :goto_0

    .line 2043
    :cond_3
    aget-wide v2, v0, v9

    aget-wide v4, v1, v8

    cmp-long v2, v2, v4

    if-nez v2, :cond_4

    aget-wide v2, v0, v8

    aget-wide v4, v1, v8

    cmp-long v2, v2, v4

    if-lez v2, :cond_4

    .line 2044
    aget-wide v2, v0, v8

    aput-wide v2, v1, v8

    goto :goto_0

    .line 2046
    :cond_4
    aget-wide v2, v0, v9

    aget-wide v4, v1, v8

    cmp-long v2, v2, v4

    if-lez v2, :cond_1

    iget-wide v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mOriOffset:J

    iget-wide v4, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->totalLengthToBeRead:J

    add-long/2addr v2, v4

    iget-wide v4, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesRemaining:J

    sub-long/2addr v2, v4

    aget-wide v4, v1, v8

    cmp-long v2, v2, v4

    if-lez v2, :cond_1

    .line 2047
    aget-wide v2, v0, v9

    aput-wide v2, v1, v9

    .line 2048
    aget-wide v2, v0, v8

    aput-wide v2, v1, v8

    goto :goto_0
.end method

.method protected getSpeed(I)J
    .locals 12
    .param p1, "sid"    # I

    .prologue
    const-wide/16 v10, 0x8

    const-wide/16 v6, 0x0

    .line 2403
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_1

    .line 2404
    iget-object v4, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mDataDownloaded:[J

    aget-wide v4, v4, p1

    const-wide/32 v8, 0x100000

    cmp-long v4, v4, v8

    if-ltz v4, :cond_0

    iget-object v4, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mTimeForDownload:[J

    aget-wide v4, v4, p1

    const-wide/16 v8, 0x3e8

    cmp-long v4, v4, v8

    if-gez v4, :cond_1

    .line 2405
    :cond_0
    iget-object v4, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "main speed calculation with short time for socket "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, ", byte="

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v8, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mDataDownloaded:[J

    aget-wide v8, v8, p1

    invoke-virtual {v5, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, ", time="

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v8, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mTimeForDownload:[J

    aget-wide v8, v8, p1

    invoke-virtual {v5, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->w(Ljava/lang/String;)V

    .line 2408
    :cond_1
    iget-object v4, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mDataDownloaded:[J

    aget-wide v8, v4, p1

    iget-object v4, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    if-nez v4, :cond_2

    move-wide v4, v6

    :goto_0
    add-long v0, v8, v4

    .line 2409
    .local v0, "len":J
    iget-object v4, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mTimeForDownload:[J

    aget-wide v8, v4, p1

    iget-object v4, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    if-nez v4, :cond_3

    move-wide v4, v6

    :goto_1
    add-long v2, v8, v4

    .line 2411
    .local v2, "time":J
    cmp-long v4, v2, v6

    if-gtz v4, :cond_4

    .line 2412
    mul-long v4, v0, v10

    .line 2415
    :goto_2
    return-wide v4

    .line 2408
    .end local v0    # "len":J
    .end local v2    # "time":J
    :cond_2
    iget-object v4, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    invoke-virtual {v4, p1}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->getDownloadLen(I)J

    move-result-wide v4

    goto :goto_0

    .line 2409
    .restart local v0    # "len":J
    :cond_3
    iget-object v4, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    invoke-virtual {v4, p1}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->getDownloadTime(I)J

    move-result-wide v4

    goto :goto_1

    .line 2415
    .restart local v2    # "time":J
    :cond_4
    mul-long v4, v0, v10

    div-long/2addr v4, v2

    goto :goto_2
.end method

.method protected getSpeed(JJ)J
    .locals 3
    .param p1, "len"    # J
    .param p3, "time"    # J

    .prologue
    const-wide/16 v0, 0x0

    .line 2429
    cmp-long v2, p3, v0

    if-gtz v2, :cond_0

    .line 2433
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x8

    mul-long/2addr v0, p1

    div-long/2addr v0, p3

    goto :goto_0
.end method

.method protected getSpeedWithOffset(I)J
    .locals 10
    .param p1, "sid"    # I

    .prologue
    const-wide/16 v4, 0x0

    .line 2419
    iget-object v6, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mDataDownloaded:[J

    aget-wide v6, v6, p1

    iget-object v8, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mStartDataOffsetForSpeedCalc:[J

    aget-wide v8, v8, p1

    sub-long v0, v6, v8

    .line 2420
    .local v0, "data":J
    iget-object v6, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mTimeForDownload:[J

    aget-wide v6, v6, p1

    iget-object v8, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mStartTimeOffsetForSpeedCalc:[J

    aget-wide v8, v8, p1

    sub-long v2, v6, v8

    .line 2421
    .local v2, "time":J
    cmp-long v6, v2, v4

    if-lez v6, :cond_0

    cmp-long v6, v0, v4

    if-gtz v6, :cond_1

    .line 2425
    :cond_0
    :goto_0
    return-wide v4

    :cond_1
    const-wide/16 v4, 0x8

    mul-long/2addr v4, v0

    div-long/2addr v4, v2

    goto :goto_0
.end method

.method protected incByte(IJ)V
    .locals 4
    .param p1, "sid"    # I
    .param p2, "bytes"    # J

    .prologue
    .line 2395
    sget-boolean v0, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v0, :cond_1

    .line 2396
    const-wide/32 v0, 0x100000

    cmp-long v0, p2, v0

    if-gtz v0, :cond_0

    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-gtz v0, :cond_1

    .line 2397
    :cond_0
    iget-object v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "speed calc >> set data for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " from "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mDataDownloaded:[J

    aget-wide v2, v2, p1

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with a inc "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/okhttp/internal/http/MultiratLog;->w(Ljava/lang/String;)V

    .line 2399
    :cond_1
    iget-object v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mDataDownloaded:[J

    aget-wide v2, v0, p1

    add-long/2addr v2, p2

    aput-wide v2, v0, p1

    .line 2400
    return-void
.end method

.method protected isMultiRATworking()Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 999
    iget-wide v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainThreadID:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-gez v2, :cond_0

    .line 1000
    :goto_0
    return v1

    :cond_0
    iget-wide v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainThreadID:J

    invoke-static {v2, v3}, Lcom/android/okhttp/internal/http/SBServiceAPI;->getSBUsageStatus(J)I

    move-result v2

    if-ne v2, v0, :cond_1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public read([BII)I
    .locals 34
    .param p1, "buffer"    # [B
    .param p2, "offset"    # I
    .param p3, "count"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1630
    const/16 v28, 0x1

    :try_start_0
    move/from16 v0, v28

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bAppIsReadingNow:Z

    .line 1631
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v28

    move-wide/from16 v0, v28

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mLastReadTime:J

    .line 1632
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bAppReading:Z

    move/from16 v28, v0

    if-nez v28, :cond_0

    .line 1633
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->lockAppReading:Ljava/lang/Object;

    move-object/from16 v29, v0

    monitor-enter v29
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1634
    const/16 v28, 0x1

    :try_start_1
    move/from16 v0, v28

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bAppReading:Z

    .line 1635
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->lockAppReading:Ljava/lang/Object;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Object;->notifyAll()V

    .line 1636
    monitor-exit v29
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1638
    :cond_0
    const/16 v28, -0x64

    move/from16 v0, p2

    move/from16 v1, v28

    if-ne v0, v1, :cond_4

    .line 1639
    :try_start_2
    sget-boolean v28, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v28, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    move-object/from16 v28, v0

    const-string v29, "get fname setting command"

    invoke-virtual/range {v28 .. v29}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1640
    :cond_1
    invoke-direct/range {p0 .. p1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->checkLocalFileFromFirstRead([B)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1641
    const/16 v16, -0x65

    .line 1970
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v28

    move-wide/from16 v0, v28

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mLastReadTime:J

    .line 1971
    const/16 v28, 0x0

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bAppIsReadingNow:Z

    :goto_0
    return v16

    .line 1636
    :catchall_0
    move-exception v28

    :try_start_3
    monitor-exit v29
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v28
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1963
    :catch_0
    move-exception v10

    .line 1964
    .local v10, "ex":Ljava/io/IOException;
    :try_start_5
    sget-boolean v28, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v28, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    move-object/from16 v28, v0

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "Exception in read "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual {v10}, Ljava/io/IOException;->getCause()Ljava/lang/Throwable;

    move-result-object v30

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1965
    :cond_2
    sget-boolean v28, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v28, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    invoke-virtual {v0, v10}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V

    .line 1966
    :cond_3
    const/16 v28, 0x1

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bHasIOException:Z

    .line 1967
    throw v10
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 1970
    .end local v10    # "ex":Ljava/io/IOException;
    :catchall_1
    move-exception v28

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v30

    move-wide/from16 v0, v30

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mLastReadTime:J

    .line 1971
    const/16 v29, 0x0

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bAppIsReadingNow:Z

    throw v28

    .line 1643
    :cond_4
    const/16 v28, -0x66

    move/from16 v0, p2

    move/from16 v1, v28

    if-ne v0, v1, :cond_7

    .line 1644
    :try_start_6
    sget-boolean v28, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v28, :cond_5

    .line 1645
    :cond_5
    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v28, v0

    const/16 v29, 0x10

    move/from16 v0, v28

    move/from16 v1, v29

    if-lt v0, v1, :cond_6

    .line 1651
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mSecChunkStart:J

    move-wide/from16 v28, v0

    const/16 v30, 0x0

    const/16 v31, 0x8

    move-wide/from16 v0, v28

    move-object/from16 v2, p1

    move/from16 v3, v30

    move/from16 v4, v31

    invoke-static {v0, v1, v2, v3, v4}, Lcom/android/okhttp/internal/http/SBServiceAPI;->convertLongToByte(J[BII)V

    .line 1652
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mSecChunkEnd:J

    move-wide/from16 v28, v0

    const/16 v30, 0x8

    const/16 v31, 0x10

    move-wide/from16 v0, v28

    move-object/from16 v2, p1

    move/from16 v3, v30

    move/from16 v4, v31

    invoke-static {v0, v1, v2, v3, v4}, Lcom/android/okhttp/internal/http/SBServiceAPI;->convertLongToByte(J[BII)V

    .line 1655
    sget-boolean v28, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    if-eqz v28, :cond_6

    .line 1665
    :cond_6
    const/16 v16, -0x67

    .line 1970
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v28

    move-wide/from16 v0, v28

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mLastReadTime:J

    .line 1971
    const/16 v28, 0x0

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bAppIsReadingNow:Z

    goto/16 :goto_0

    .line 1668
    :cond_7
    :try_start_7
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesRemaining:J

    move-wide/from16 v28, v0

    const-wide/16 v30, 0x0

    cmp-long v28, v28, v30

    if-gtz v28, :cond_a

    .line 1669
    sget-boolean v28, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v28, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    move-object/from16 v28, v0

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "finish to read size, no byte remained, return -1, remain is "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesRemaining:J

    move-wide/from16 v30, v0

    invoke-virtual/range {v29 .. v31}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1670
    :cond_8
    sget-boolean v28, Lcom/android/okhttp/internal/http/HttpEngine;->HTTPTIMER:Z

    if-eqz v28, :cond_9

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bWaitPrinted:Z

    move/from16 v28, v0

    if-nez v28, :cond_9

    .line 1671
    new-instance v22, Ljava/lang/StringBuffer;

    const-string v28, "\tHTTPTIMER"

    move-object/from16 v0, v22

    move-object/from16 v1, v28

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 1672
    .local v22, "timerStr":Ljava/lang/StringBuffer;
    const-string v28, "_MWAIT\t\t"

    move-object/from16 v0, v22

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v28

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->waitTime:J

    move-wide/from16 v30, v0

    move-object/from16 v0, v28

    move-wide/from16 v1, v30

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    .line 1673
    sget-object v28, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1674
    const/16 v28, 0x1

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bWaitPrinted:Z

    .line 1676
    .end local v22    # "timerStr":Ljava/lang/StringBuffer;
    :cond_9
    const/16 v28, 0x1

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bFinished:Z
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 1677
    const/16 v16, -0x1

    .line 1970
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v28

    move-wide/from16 v0, v28

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mLastReadTime:J

    .line 1971
    const/16 v28, 0x0

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bAppIsReadingNow:Z

    goto/16 :goto_0

    .line 1680
    :cond_a
    :try_start_8
    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v28, v0

    move/from16 v0, v28

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-static {v0, v1, v2}, Lcom/android/okhttp/internal/http/MultiratUtil;->checkOffsetAndCount(III)V

    .line 1681
    invoke-virtual/range {p0 .. p0}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->checkNotClosed()V

    .line 1683
    const/4 v15, 0x0

    .line 1684
    .local v15, "read":I
    const/16 v16, -0x64

    .line 1687
    .local v16, "readLenForReturn":I
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bAppStartRead:Z

    move/from16 v28, v0

    if-nez v28, :cond_c

    .line 1688
    sget-boolean v28, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v28, :cond_b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    move-object/from16 v28, v0

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "first read, buffer size="

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v30, v0

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string v30, ", offset="

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string v30, ", count="

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1689
    :cond_b
    const/16 v28, 0x1

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bAppStartRead:Z

    .line 1693
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bDoNotUseMultiSockSinceWiFiOnly:Z

    move/from16 v28, v0

    if-nez v28, :cond_c

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesRemaining:J

    move-wide/from16 v28, v0

    const-wide/32 v30, 0x200000

    cmp-long v28, v28, v30

    if-lez v28, :cond_c

    .line 1694
    new-instance v14, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mHeader:Lcom/android/okhttp/Headers;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-direct {v14, v0, v1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;-><init>(Lcom/android/okhttp/internal/http/MultiSocketInputStream;Lcom/android/okhttp/Headers;)V

    .line 1695
    .local v14, "infw":Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;
    new-instance v17, Ljava/lang/Thread;

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, ":InfWatchdog_"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    sget v29, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->threadID:I

    add-int/lit8 v30, v29, 0x1

    sput v30, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->threadID:I

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v17

    move-object/from16 v1, v28

    invoke-direct {v0, v14, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 1696
    .local v17, "tInfw":Ljava/lang/Thread;
    invoke-virtual/range {v17 .. v17}, Ljava/lang/Thread;->start()V

    .line 1700
    .end local v14    # "infw":Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;
    .end local v17    # "tInfw":Ljava/lang/Thread;
    :cond_c
    move/from16 v23, p3

    .line 1701
    .local v23, "toReadCount":I
    move/from16 v0, v23

    int-to-long v0, v0

    move-wide/from16 v28, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesRemaining:J

    move-wide/from16 v30, v0

    cmp-long v28, v28, v30

    if-lez v28, :cond_e

    .line 1702
    sget-boolean v28, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v28, :cond_d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    move-object/from16 v28, v0

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "toReadCount "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string v30, " > bytesRemaining "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesRemaining:J

    move-wide/from16 v30, v0

    invoke-virtual/range {v29 .. v31}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1703
    :cond_d
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesRemaining:J

    move-wide/from16 v28, v0

    move-wide/from16 v0, v28

    long-to-int v0, v0

    move/from16 v23, v0

    .line 1706
    :cond_e
    sget-boolean v28, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v28, :cond_f

    .line 1709
    :cond_f
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bSourceBufferCleared:Z

    move/from16 v28, v0

    if-nez v28, :cond_10

    .line 1710
    move/from16 v0, v23

    int-to-long v0, v0

    move-wide/from16 v28, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesRemaining:J

    move-wide/from16 v30, v0

    invoke-static/range {v28 .. v31}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v28

    move-wide/from16 v0, v28

    long-to-int v0, v0

    move/from16 v28, v0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, v28

    invoke-direct {v0, v1, v2, v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->readFromSourceBuffer([BII)I

    move-result v15

    .line 1711
    if-nez v15, :cond_10

    .line 1712
    const/16 v28, 0x1

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bSourceBufferCleared:Z

    .line 1716
    :cond_10
    if-nez v15, :cond_11

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bReadFromBuffer:Z

    move/from16 v28, v0

    if-nez v28, :cond_11

    .line 1718
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bExtremThreadStarted:Z

    move/from16 v28, v0

    if-nez v28, :cond_17

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bMultiSocketStarted:Z

    move/from16 v28, v0

    if-nez v28, :cond_17

    .line 1719
    move/from16 v0, v23

    int-to-long v0, v0

    move-wide/from16 v28, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesRemaining:J

    move-wide/from16 v30, v0

    invoke-static/range {v28 .. v31}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v28

    move-wide/from16 v0, v28

    long-to-int v11, v0

    .line 1720
    .local v11, "expReadLen":I
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    invoke-direct {v0, v1, v2, v11}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->readFromMainSocket([BII)I

    move-result v15

    .line 1787
    .end local v11    # "expReadLen":I
    :goto_1
    if-lez v15, :cond_11

    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v28, v0

    move/from16 v0, v28

    if-le v15, v0, :cond_11

    .line 1788
    move/from16 v16, v15

    .line 1789
    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v28, v0

    sub-int v15, v15, v28

    .line 1790
    sget-boolean v28, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v28, :cond_11

    .line 1794
    :cond_11
    if-nez v15, :cond_15

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bReadFromBuffer:Z

    move/from16 v28, v0

    if-eqz v28, :cond_15

    .line 1796
    :cond_12
    :goto_2
    if-gtz v15, :cond_15

    move/from16 v0, v23

    if-ge v15, v0, :cond_15

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bFinished:Z

    move/from16 v28, v0

    if-nez v28, :cond_15

    .line 1797
    const-wide/16 v20, 0x0

    .line 1798
    .local v20, "t1":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->restBuffer:Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

    move-object/from16 v28, v0

    if-nez v28, :cond_2f

    .line 1799
    sget-boolean v28, Lcom/android/okhttp/internal/http/HttpEngine;->HTTPTIMER:Z

    if-eqz v28, :cond_13

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v20

    .line 1800
    :cond_13
    sget-boolean v28, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v28, :cond_14

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    move-object/from16 v28, v0

    const-string v29, "no buffered size"

    invoke-virtual/range {v28 .. v29}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1802
    :cond_14
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->inBuffer:Ljava/util/HashMap;

    move-object/from16 v29, v0

    monitor-enter v29
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_0
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 1804
    :try_start_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->inBuffer:Ljava/util/HashMap;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->readBlockNumber:Ljava/lang/Integer;

    move-object/from16 v30, v0

    move-object/from16 v0, v28

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v28

    if-nez v28, :cond_26

    if-lez v15, :cond_26

    .line 1805
    monitor-exit v29
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_4

    .line 1946
    .end local v20    # "t1":J
    :cond_15
    :goto_3
    const/16 v28, -0x1

    move/from16 v0, v28

    if-ne v15, v0, :cond_46

    .line 1947
    :try_start_a
    invoke-virtual/range {p0 .. p0}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->unexpectedEndOfInput()V

    .line 1948
    sget-boolean v28, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v28, :cond_16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    move-object/from16 v28, v0

    const-string v29, "unexpected end of stream"

    invoke-virtual/range {v28 .. v29}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1949
    :cond_16
    new-instance v28, Ljava/io/IOException;

    const-string v29, "unexpected end of stream"

    invoke-direct/range {v28 .. v29}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v28

    .line 1724
    :cond_17
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesRemaining:J

    move-wide/from16 v28, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesForMultiSocket:J

    move-wide/from16 v30, v0

    cmp-long v28, v28, v30

    if-nez v28, :cond_23

    .line 1725
    sget-boolean v28, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v28, :cond_18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    move-object/from16 v28, v0

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "resource check: finish reading chunk "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mOriOffset:J

    move-wide/from16 v30, v0

    invoke-virtual/range {v29 .. v31}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string v30, "-"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mOriOffset:J

    move-wide/from16 v30, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->totalLengthToBeRead:J

    move-wide/from16 v32, v0

    add-long v30, v30, v32

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesForMultiSocket:J

    move-wide/from16 v32, v0

    sub-long v30, v30, v32

    const-wide/16 v32, 0x1

    sub-long v30, v30, v32

    invoke-virtual/range {v29 .. v31}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string v30, " with length "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->totalLengthToBeRead:J

    move-wide/from16 v30, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesForMultiSocket:J

    move-wide/from16 v32, v0

    sub-long v30, v30, v32

    invoke-virtual/range {v29 .. v31}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string v30, " from main socket, "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string v30, " now bytesRemaining==bytesForMultiSocket=="

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesRemaining:J

    move-wide/from16 v30, v0

    invoke-virtual/range {v29 .. v31}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string v30, ", mOffset="

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mOffset:J

    move-wide/from16 v30, v0

    invoke-virtual/range {v29 .. v31}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1728
    :cond_18
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mCacheType:I

    move/from16 v28, v0

    const/16 v29, 0x1

    move/from16 v0, v28

    move/from16 v1, v29

    if-ne v0, v1, :cond_1b

    .line 1730
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v28

    const-wide/16 v30, 0x1388

    add-long v12, v28, v30

    .line 1731
    .local v12, "finalTime":J
    :goto_4
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bRangeRequestSuccess:I

    move/from16 v28, v0

    if-eqz v28, :cond_19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bRangeRequestSuccess:I

    move/from16 v28, v0

    const/16 v29, 0x3

    move/from16 v0, v28

    move/from16 v1, v29

    if-ne v0, v1, :cond_1b

    .line 1732
    :cond_19
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mBlockManager:Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;

    move-object/from16 v29, v0

    monitor-enter v29
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_0
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 1734
    :try_start_b
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v30

    cmp-long v28, v30, v12

    if-lez v28, :cond_1f

    .line 1735
    sget-boolean v28, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v28, :cond_1a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    move-object/from16 v28, v0

    const-string v30, "time out to wait for first range request"

    move-object/from16 v0, v28

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1736
    :cond_1a
    const/16 v28, 0x1

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bRangeRequestSuccess:I
    :try_end_b
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_4
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    .line 1737
    :try_start_c
    monitor-exit v29
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_3

    .line 1747
    .end local v12    # "finalTime":J
    :cond_1b
    :try_start_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->sbUsedLocker:Ljava/lang/Object;

    move-object/from16 v29, v0

    monitor-enter v29
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_0
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    .line 1748
    :try_start_e
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bRangeRequestSuccess:I

    move/from16 v28, v0

    const/16 v30, 0x4

    move/from16 v0, v28

    move/from16 v1, v30

    if-ne v0, v1, :cond_20

    .line 1749
    const/16 v28, 0x1

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bReadFromBuffer:Z

    .line 1750
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bExtremThreadStarted:Z

    move/from16 v28, v0

    if-eqz v28, :cond_1c

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bExtremThreadExisted:Z

    move/from16 v28, v0

    if-eqz v28, :cond_1c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mExtThread:Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;

    move-object/from16 v28, v0

    if-eqz v28, :cond_1c

    .line 1751
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mExtThread:Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainSocketInterfaceID:I

    move/from16 v30, v0

    move-object/from16 v0, v28

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->startTryBoth(I)V

    .line 1754
    :cond_1c
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bReadFromBuffer:Z

    move/from16 v28, v0

    if-eqz v28, :cond_1e

    .line 1755
    sget-boolean v28, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v28, :cond_1d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    move-object/from16 v28, v0

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "try to close maininput input stream in main, input is "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mainInput:Ljava/io/InputStream;

    move-object/from16 v31, v0

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v28

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1756
    :cond_1d
    invoke-direct/range {p0 .. p0}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->closeMainSocket()V

    .line 1757
    sget-boolean v28, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v28, :cond_1e

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    move-object/from16 v28, v0

    const-string v30, "end to close main stream"

    move-object/from16 v0, v28

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1773
    :cond_1e
    :goto_5
    monitor-exit v29

    goto/16 :goto_1

    :catchall_2
    move-exception v28

    monitor-exit v29
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_2

    :try_start_f
    throw v28
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_0
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    .line 1739
    .restart local v12    # "finalTime":J
    :cond_1f
    :try_start_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mBlockManager:Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;

    move-object/from16 v28, v0

    const-wide/16 v30, 0x3e8

    move-object/from16 v0, v28

    move-wide/from16 v1, v30

    invoke-virtual {v0, v1, v2}, Ljava/lang/Object;->wait(J)V
    :try_end_10
    .catch Ljava/lang/Throwable; {:try_start_10 .. :try_end_10} :catch_4
    .catchall {:try_start_10 .. :try_end_10} :catchall_3

    .line 1742
    :goto_6
    :try_start_11
    monitor-exit v29

    goto/16 :goto_4

    :catchall_3
    move-exception v28

    monitor-exit v29
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_3

    :try_start_12
    throw v28
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_0
    .catchall {:try_start_12 .. :try_end_12} :catchall_1

    .line 1762
    .end local v12    # "finalTime":J
    :cond_20
    const/16 v28, 0x0

    :try_start_13
    move/from16 v0, v28

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bReadFromBuffer:Z

    .line 1763
    const/16 v28, 0x0

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bMultiSocketStarted:Z

    .line 1764
    const/16 v28, 0x0

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bExtremThreadStarted:Z

    .line 1765
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mExtThread:Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;

    move-object/from16 v28, v0

    if-eqz v28, :cond_21

    .line 1766
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mExtThread:Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;

    move-object/from16 v28, v0

    const/16 v30, 0x1

    move-object/from16 v0, v28

    move/from16 v1, v30

    # setter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->bDeprecated:Z
    invoke-static {v0, v1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->access$5202(Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;Z)Z

    .line 1767
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mExtThread:Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->closeSocketAndStreams()V

    .line 1769
    :cond_21
    sget-boolean v28, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v28, :cond_22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    move-object/from16 v28, v0

    const-string v30, "server does not support range request, keep on read from main inputstream"

    move-object/from16 v0, v28

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1770
    :cond_22
    move/from16 v0, v23

    int-to-long v0, v0

    move-wide/from16 v30, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesRemaining:J

    move-wide/from16 v32, v0

    invoke-static/range {v30 .. v33}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v30

    move-wide/from16 v0, v30

    long-to-int v11, v0

    .line 1771
    .restart local v11    # "expReadLen":I
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    invoke-direct {v0, v1, v2, v11}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->readFromMainSocket([BII)I
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_2

    move-result v15

    goto/16 :goto_5

    .line 1775
    .end local v11    # "expReadLen":I
    :cond_23
    :try_start_14
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesRemaining:J

    move-wide/from16 v28, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesForMultiSocket:J

    move-wide/from16 v30, v0

    cmp-long v28, v28, v30

    if-lez v28, :cond_24

    .line 1776
    move/from16 v0, v23

    int-to-long v0, v0

    move-wide/from16 v28, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesRemaining:J

    move-wide/from16 v30, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesForMultiSocket:J

    move-wide/from16 v32, v0

    sub-long v30, v30, v32

    invoke-static/range {v28 .. v31}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v28

    move-wide/from16 v0, v28

    long-to-int v11, v0

    .line 1777
    .restart local v11    # "expReadLen":I
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    invoke-direct {v0, v1, v2, v11}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->readFromMainSocket([BII)I

    move-result v15

    .line 1778
    goto/16 :goto_1

    .line 1781
    .end local v11    # "expReadLen":I
    :cond_24
    sget-boolean v28, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v28, :cond_25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    move-object/from16 v28, v0

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "Exception: bytesRemaining<bytesForMultiSocket("

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesRemaining:J

    move-wide/from16 v30, v0

    invoke-virtual/range {v29 .. v31}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string v30, "<"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesForMultiSocket:J

    move-wide/from16 v30, v0

    invoke-virtual/range {v29 .. v31}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string v30, "), mOffset="

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mOffset:J

    move-wide/from16 v30, v0

    invoke-virtual/range {v29 .. v31}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Lcom/android/okhttp/internal/http/MultiratLog;->w(Ljava/lang/String;)V

    .line 1782
    :cond_25
    const/16 v28, 0x0

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bMultiSocketStarted:Z

    .line 1783
    const/16 v28, 0x0

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bExtremThreadStarted:Z

    .line 1784
    const/16 v28, 0x0

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bReadFromBuffer:Z
    :try_end_14
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_14} :catch_0
    .catchall {:try_start_14 .. :try_end_14} :catchall_1

    goto/16 :goto_1

    .line 1809
    .restart local v20    # "t1":J
    :cond_26
    :try_start_15
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    .line 1810
    .local v18, "startWaitTime":J
    const/4 v7, 0x0

    .line 1811
    .local v7, "bForceHO":Z
    :goto_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->inBuffer:Ljava/util/HashMap;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->readBlockNumber:Ljava/lang/Integer;

    move-object/from16 v30, v0

    move-object/from16 v0, v28

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v28

    if-nez v28, :cond_2d

    .line 1812
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v30

    sub-long v26, v30, v18

    .line 1813
    .local v26, "waitedTime":J
    sget v28, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->WAIT_FOR_INTERFACE_TIME_OUT:I

    if-lez v28, :cond_28

    sget v28, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->WAIT_FOR_INTERFACE_TIME_OUT:I

    move/from16 v0, v28

    int-to-long v0, v0

    move-wide/from16 v30, v0

    cmp-long v28, v26, v30

    if-lez v28, :cond_28

    .line 1814
    sget-boolean v28, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v28, :cond_27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    move-object/from16 v28, v0

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "waited time "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, v30

    move-wide/from16 v1, v26

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, " time out "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    sget v31, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->WAIT_FOR_INTERFACE_TIME_OUT:I

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v28

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1815
    :cond_27
    new-instance v28, Ljava/io/IOException;

    const-string v30, "Timeout to wait either interface connected"

    move-object/from16 v0, v28

    move-object/from16 v1, v30

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v28

    .line 1842
    .end local v7    # "bForceHO":Z
    .end local v18    # "startWaitTime":J
    .end local v26    # "waitedTime":J
    :catchall_4
    move-exception v28

    monitor-exit v29
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_4

    :try_start_16
    throw v28
    :try_end_16
    .catch Ljava/io/IOException; {:try_start_16 .. :try_end_16} :catch_0
    .catchall {:try_start_16 .. :try_end_16} :catchall_1

    .line 1817
    .restart local v7    # "bForceHO":Z
    .restart local v18    # "startWaitTime":J
    .restart local v26    # "waitedTime":J
    :cond_28
    if-nez v7, :cond_2b

    const-wide/16 v30, 0x3e8

    cmp-long v28, v26, v30

    if-lez v28, :cond_2b

    :try_start_17
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bMultiSocketStarted:Z

    move/from16 v28, v0

    const/16 v30, 0x1

    move/from16 v0, v28

    move/from16 v1, v30

    if-ne v0, v1, :cond_2b

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bRangeRequestSuccess:I

    move/from16 v28, v0

    const/16 v30, 0x4

    move/from16 v0, v28

    move/from16 v1, v30

    if-ne v0, v1, :cond_2b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->readBlockNumber:Ljava/lang/Integer;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v28

    if-ltz v28, :cond_2b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->readBlockNumber:Ljava/lang/Integer;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v28

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    array-length v0, v0

    move/from16 v30, v0

    move/from16 v0, v28

    move/from16 v1, v30

    if-ge v0, v1, :cond_2b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->readBlockNumber:Ljava/lang/Integer;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Ljava/lang/Integer;->intValue()I

    move-result v30

    aget-byte v28, v28, v30

    if-gtz v28, :cond_2b

    .line 1821
    sget-boolean v28, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v28, :cond_29

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    move-object/from16 v28, v0

    const-string v30, "force handover"

    move-object/from16 v0, v28

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1822
    :cond_29
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->connection:Lcom/android/okhttp/Connection;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Lcom/android/okhttp/Connection;->bBothInfAvail()I

    move-result v28

    const/16 v30, 0x1

    move/from16 v0, v28

    move/from16 v1, v30

    if-ne v0, v1, :cond_2c

    .line 1823
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->requestHandlers:[Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;

    move-object/from16 v28, v0

    const/16 v30, 0x0

    aget-object v28, v28, v30

    if-eqz v28, :cond_2a

    .line 1824
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->requestHandlers:[Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;

    move-object/from16 v28, v0

    const/16 v30, 0x0

    aget-object v28, v28, v30

    invoke-virtual/range {v28 .. v28}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->closeHTTP()V
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_4

    .line 1830
    :cond_2a
    :goto_8
    const/4 v7, 0x1

    .line 1833
    :cond_2b
    :try_start_18
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->inBuffer:Ljava/util/HashMap;

    move-object/from16 v28, v0

    const-wide/16 v30, 0x1f4

    move-object/from16 v0, v28

    move-wide/from16 v1, v30

    invoke-virtual {v0, v1, v2}, Ljava/lang/Object;->wait(J)V
    :try_end_18
    .catch Ljava/lang/Throwable; {:try_start_18 .. :try_end_18} :catch_1
    .catchall {:try_start_18 .. :try_end_18} :catchall_4

    goto/16 :goto_7

    .line 1835
    :catch_1
    move-exception v28

    goto/16 :goto_7

    .line 1826
    :cond_2c
    :try_start_19
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->connection:Lcom/android/okhttp/Connection;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Lcom/android/okhttp/Connection;->bBothInfAvail()I

    move-result v28

    const/16 v30, 0x2

    move/from16 v0, v28

    move/from16 v1, v30

    if-ne v0, v1, :cond_2a

    .line 1827
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->requestHandlers:[Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;

    move-object/from16 v28, v0

    const/16 v30, 0x1

    aget-object v28, v28, v30

    if-eqz v28, :cond_2a

    .line 1828
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->requestHandlers:[Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;

    move-object/from16 v28, v0

    const/16 v30, 0x1

    aget-object v28, v28, v30

    invoke-virtual/range {v28 .. v28}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->closeHTTP()V

    goto :goto_8

    .line 1837
    .end local v26    # "waitedTime":J
    :cond_2d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->inBuffer:Ljava/util/HashMap;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->readBlockNumber:Ljava/lang/Integer;

    move-object/from16 v30, v0

    move-object/from16 v0, v28

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v28

    check-cast v28, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

    move-object/from16 v0, v28

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->restBuffer:Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

    .line 1838
    sget-boolean v28, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v28, :cond_2e

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    move-object/from16 v28, v0

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "get new buffer list["

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->readBlockNumber:Ljava/lang/Integer;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Ljava/lang/Integer;->intValue()I

    move-result v31

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, "]"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v28

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1839
    :cond_2e
    new-instance v28, Ljava/lang/Integer;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->readBlockNumber:Ljava/lang/Integer;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Ljava/lang/Integer;->intValue()I

    move-result v30

    add-int/lit8 v30, v30, 0x1

    move-object/from16 v0, v28

    move/from16 v1, v30

    invoke-direct {v0, v1}, Ljava/lang/Integer;-><init>(I)V

    move-object/from16 v0, v28

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->readBlockNumber:Ljava/lang/Integer;

    .line 1840
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->restBuffer:Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->getLength()J

    move-result-wide v30

    move-wide/from16 v0, v30

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->restBlockSize:J

    .line 1842
    monitor-exit v29
    :try_end_19
    .catchall {:try_start_19 .. :try_end_19} :catchall_4

    .line 1845
    .end local v7    # "bForceHO":Z
    .end local v18    # "startWaitTime":J
    :cond_2f
    :try_start_1a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->restBuffer:Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

    move-object/from16 v29, v0

    monitor-enter v29
    :try_end_1a
    .catch Ljava/io/IOException; {:try_start_1a .. :try_end_1a} :catch_0
    .catchall {:try_start_1a .. :try_end_1a} :catchall_1

    .line 1847
    :try_start_1b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->restBuffer:Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->isEmpty()Z

    move-result v28

    if-eqz v28, :cond_30

    if-lez v15, :cond_30

    .line 1848
    monitor-exit v29

    goto/16 :goto_3

    .line 1925
    :catchall_5
    move-exception v28

    monitor-exit v29
    :try_end_1b
    .catchall {:try_start_1b .. :try_end_1b} :catchall_5

    :try_start_1c
    throw v28
    :try_end_1c
    .catch Ljava/io/IOException; {:try_start_1c .. :try_end_1c} :catch_0
    .catchall {:try_start_1c .. :try_end_1c} :catchall_1

    .line 1851
    :cond_30
    :try_start_1d
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    .line 1852
    .restart local v18    # "startWaitTime":J
    const/4 v7, 0x0

    .line 1853
    .restart local v7    # "bForceHO":Z
    :goto_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->restBuffer:Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->isEmpty()Z

    move-result v28

    if-eqz v28, :cond_3e

    .line 1854
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v30

    sub-long v26, v30, v18

    .line 1855
    .restart local v26    # "waitedTime":J
    sget v28, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->WAIT_FOR_INTERFACE_TIME_OUT:I

    if-lez v28, :cond_32

    sget v28, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->WAIT_FOR_INTERFACE_TIME_OUT:I

    move/from16 v0, v28

    int-to-long v0, v0

    move-wide/from16 v30, v0

    cmp-long v28, v26, v30

    if-lez v28, :cond_32

    .line 1856
    sget-boolean v28, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v28, :cond_31

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    move-object/from16 v28, v0

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "waited time "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, v30

    move-wide/from16 v1, v26

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, " time out "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    sget v31, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->WAIT_FOR_INTERFACE_TIME_OUT:I

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v28

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1857
    :cond_31
    new-instance v28, Ljava/io/IOException;

    const-string v30, "Timeout to wait either interface connected"

    move-object/from16 v0, v28

    move-object/from16 v1, v30

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v28

    .line 1859
    :cond_32
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bExtremThreadStarted:Z

    move/from16 v28, v0

    if-eqz v28, :cond_3a

    .line 1860
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bRangeRequestSuccess:I

    move/from16 v28, v0

    const/16 v30, 0x1

    move/from16 v0, v28

    move/from16 v1, v30

    if-ne v0, v1, :cond_34

    .line 1861
    sget-boolean v28, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v28, :cond_33

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    move-object/from16 v28, v0

    const-string v30, "Range Request failed in extremthread or secchunktread"

    move-object/from16 v0, v28

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1862
    :cond_33
    new-instance v28, Ljava/io/IOException;

    const-string v30, "Disconnected from Server"

    move-object/from16 v0, v28

    move-object/from16 v1, v30

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v28

    .line 1864
    :cond_34
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    move-object/from16 v28, v0

    if-nez v28, :cond_37

    const-wide/16 v30, 0xfa0

    cmp-long v28, v26, v30

    if-lez v28, :cond_37

    .line 1865
    sget-boolean v28, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v28, :cond_35

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    move-object/from16 v28, v0

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "waited time extreme case "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, v30

    move-wide/from16 v1, v26

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, " time out "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    const/16 v31, 0xfa0

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v28

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1866
    :cond_35
    invoke-virtual/range {p0 .. p0}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->isMultiRATworking()Z

    move-result v28

    if-nez v28, :cond_39

    .line 1867
    sget-boolean v28, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v28, :cond_36

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    move-object/from16 v28, v0

    const-string v30, "NB disabled, throw exception"

    move-object/from16 v0, v28

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1868
    :cond_36
    const/16 v28, 0x1

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bFinished:Z

    .line 1869
    new-instance v28, Ljava/io/IOException;

    const-string v30, "read socket time out"

    move-object/from16 v0, v28

    move-object/from16 v1, v30

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v28

    .line 1872
    :cond_37
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    move-object/from16 v28, v0

    if-nez v28, :cond_39

    const-wide/16 v30, 0x7d0

    cmp-long v28, v26, v30

    if-lez v28, :cond_39

    .line 1873
    sget-boolean v28, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v28, :cond_38

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    move-object/from16 v28, v0

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "waited time extreme case "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, v30

    move-wide/from16 v1, v26

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, " time out "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    const/16 v31, 0x7d0

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v28

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1874
    :cond_38
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mExtThread:Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;

    move-object/from16 v28, v0

    const/16 v30, 0x1

    move-object/from16 v0, v28

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->createTwoChunkInput(Z)V
    :try_end_1d
    .catchall {:try_start_1d .. :try_end_1d} :catchall_5

    .line 1894
    :cond_39
    :goto_a
    :try_start_1e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->restBuffer:Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

    move-object/from16 v28, v0

    const-wide/16 v30, 0x1f4

    move-object/from16 v0, v28

    move-wide/from16 v1, v30

    invoke-virtual {v0, v1, v2}, Ljava/lang/Object;->wait(J)V
    :try_end_1e
    .catch Ljava/lang/Throwable; {:try_start_1e .. :try_end_1e} :catch_2
    .catchall {:try_start_1e .. :try_end_1e} :catchall_5

    goto/16 :goto_9

    .line 1896
    :catch_2
    move-exception v28

    goto/16 :goto_9

    .line 1877
    :cond_3a
    if-nez v7, :cond_39

    const-wide/16 v30, 0x3e8

    cmp-long v28, v26, v30

    if-lez v28, :cond_39

    :try_start_1f
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bMultiSocketStarted:Z

    move/from16 v28, v0

    const/16 v30, 0x1

    move/from16 v0, v28

    move/from16 v1, v30

    if-ne v0, v1, :cond_39

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bRangeRequestSuccess:I

    move/from16 v28, v0

    const/16 v30, 0x4

    move/from16 v0, v28

    move/from16 v1, v30

    if-ne v0, v1, :cond_39

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->readBlockNumber:Ljava/lang/Integer;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v28

    add-int/lit8 v28, v28, -0x1

    if-ltz v28, :cond_39

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->readBlockNumber:Ljava/lang/Integer;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Integer;->intValue()I

    move-result v28

    add-int/lit8 v28, v28, -0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    array-length v0, v0

    move/from16 v30, v0

    move/from16 v0, v28

    move/from16 v1, v30

    if-ge v0, v1, :cond_39

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->readBlockNumber:Ljava/lang/Integer;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Ljava/lang/Integer;->intValue()I

    move-result v30

    add-int/lit8 v30, v30, -0x1

    aget-byte v28, v28, v30

    if-gtz v28, :cond_39

    .line 1881
    sget-boolean v28, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v28, :cond_3b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    move-object/from16 v28, v0

    const-string v30, "force handover"

    move-object/from16 v0, v28

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1882
    :cond_3b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->connection:Lcom/android/okhttp/Connection;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Lcom/android/okhttp/Connection;->bBothInfAvail()I

    move-result v6

    .line 1883
    .local v6, "bBothInfAvail":I
    const/16 v28, 0x1

    move/from16 v0, v28

    if-ne v6, v0, :cond_3d

    .line 1884
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->requestHandlers:[Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;

    move-object/from16 v28, v0

    const/16 v30, 0x0

    aget-object v28, v28, v30

    if-eqz v28, :cond_3c

    .line 1885
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->requestHandlers:[Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;

    move-object/from16 v28, v0

    const/16 v30, 0x0

    aget-object v28, v28, v30

    invoke-virtual/range {v28 .. v28}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->closeHTTP()V

    .line 1891
    :cond_3c
    :goto_b
    const/4 v7, 0x1

    goto/16 :goto_a

    .line 1887
    :cond_3d
    const/16 v28, 0x2

    move/from16 v0, v28

    if-ne v6, v0, :cond_3c

    .line 1888
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->requestHandlers:[Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;

    move-object/from16 v28, v0

    const/16 v30, 0x1

    aget-object v28, v28, v30

    if-eqz v28, :cond_3c

    .line 1889
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->requestHandlers:[Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;

    move-object/from16 v28, v0

    const/16 v30, 0x1

    aget-object v28, v28, v30

    invoke-virtual/range {v28 .. v28}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->closeHTTP()V

    goto :goto_b

    .line 1898
    .end local v6    # "bBothInfAvail":I
    .end local v26    # "waitedTime":J
    :cond_3e
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bReadFromBuffer:Z

    move/from16 v28, v0

    if-nez v28, :cond_40

    .line 1899
    sget-boolean v28, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v28, :cond_3f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    move-object/from16 v28, v0

    const-string v30, "ExtremeCase Timeout, break to read from main socket"

    move-object/from16 v0, v28

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1900
    :cond_3f
    monitor-exit v29
    :try_end_1f
    .catchall {:try_start_1f .. :try_end_1f} :catchall_5

    goto/16 :goto_3

    .line 1902
    :cond_40
    const/4 v8, 0x0

    .line 1905
    .local v8, "blockLen":I
    :try_start_20
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->restBuffer:Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

    move-object/from16 v28, v0

    add-int v30, p2, v15

    sub-int v31, v23, v15

    move-object/from16 v0, v28

    move-object/from16 v1, p1

    move/from16 v2, v30

    move/from16 v3, v31

    invoke-virtual {v0, v1, v2, v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->read([BII)I

    move-result v8

    .line 1906
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->restBuffer:Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Object;->notifyAll()V
    :try_end_20
    .catch Ljava/lang/Throwable; {:try_start_20 .. :try_end_20} :catch_3
    .catchall {:try_start_20 .. :try_end_20} :catchall_5

    .line 1911
    :cond_41
    :goto_c
    const/16 v28, -0x1

    move/from16 v0, v28

    if-ne v8, v0, :cond_43

    .line 1912
    :try_start_21
    sget-boolean v28, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v28, :cond_42

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    move-object/from16 v28, v0

    const-string v30, "unexpected end from restBuffer.read"

    move-object/from16 v0, v28

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1913
    :cond_42
    const/4 v15, -0x1

    .line 1914
    monitor-exit v29

    goto/16 :goto_3

    .line 1908
    :catch_3
    move-exception v9

    .line 1909
    .local v9, "e":Ljava/lang/Throwable;
    sget-boolean v28, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v28, :cond_41

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    invoke-virtual {v0, v9}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V

    goto :goto_c

    .line 1916
    .end local v9    # "e":Ljava/lang/Throwable;
    :cond_43
    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v28, v0

    move/from16 v0, v28

    if-le v8, v0, :cond_44

    .line 1917
    move/from16 v16, v8

    .line 1918
    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v28, v0

    sub-int v8, v8, v28

    .line 1919
    sget-boolean v28, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v28, :cond_44

    .line 1922
    :cond_44
    add-int/2addr v15, v8

    .line 1923
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->restBlockSize:J

    move-wide/from16 v30, v0

    int-to-long v0, v8

    move-wide/from16 v32, v0

    sub-long v30, v30, v32

    move-wide/from16 v0, v30

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->restBlockSize:J

    .line 1925
    monitor-exit v29
    :try_end_21
    .catchall {:try_start_21 .. :try_end_21} :catchall_5

    .line 1928
    :try_start_22
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->restBlockSize:J

    move-wide/from16 v28, v0

    const-wide/16 v30, 0x0

    cmp-long v28, v28, v30

    if-nez v28, :cond_45

    .line 1929
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->restBuffer:Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

    move-object/from16 v29, v0

    monitor-enter v29
    :try_end_22
    .catch Ljava/io/IOException; {:try_start_22 .. :try_end_22} :catch_0
    .catchall {:try_start_22 .. :try_end_22} :catchall_1

    .line 1930
    const/16 v28, 0x0

    :try_start_23
    move-object/from16 v0, v28

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->restBuffer:Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

    .line 1931
    monitor-exit v29
    :try_end_23
    .catchall {:try_start_23 .. :try_end_23} :catchall_6

    .line 1932
    :try_start_24
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->inBuffer:Ljava/util/HashMap;

    move-object/from16 v29, v0

    monitor-enter v29
    :try_end_24
    .catch Ljava/io/IOException; {:try_start_24 .. :try_end_24} :catch_0
    .catchall {:try_start_24 .. :try_end_24} :catchall_1

    .line 1933
    :try_start_25
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->inBuffer:Ljava/util/HashMap;

    move-object/from16 v28, v0

    new-instance v30, Ljava/lang/Integer;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->readBlockNumber:Ljava/lang/Integer;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Ljava/lang/Integer;->intValue()I

    move-result v31

    add-int/lit8 v31, v31, -0x1

    invoke-direct/range {v30 .. v31}, Ljava/lang/Integer;-><init>(I)V

    move-object/from16 v0, v28

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1934
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->inBuffer:Ljava/util/HashMap;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Ljava/lang/Object;->notifyAll()V

    .line 1935
    monitor-exit v29
    :try_end_25
    .catchall {:try_start_25 .. :try_end_25} :catchall_7

    .line 1938
    :cond_45
    :try_start_26
    sget-boolean v28, Lcom/android/okhttp/internal/http/HttpEngine;->HTTPTIMER:Z

    if-eqz v28, :cond_12

    .line 1939
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v28

    sub-long v24, v28, v20

    .line 1940
    .local v24, "tmp":J
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->waitTime:J

    move-wide/from16 v28, v0

    add-long v28, v28, v24

    move-wide/from16 v0, v28

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->waitTime:J

    .line 1941
    sget-boolean v28, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v28, :cond_12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    move-object/from16 v28, v0

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "time used for wait buffer to read:"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V
    :try_end_26
    .catch Ljava/io/IOException; {:try_start_26 .. :try_end_26} :catch_0
    .catchall {:try_start_26 .. :try_end_26} :catchall_1

    goto/16 :goto_2

    .line 1931
    .end local v24    # "tmp":J
    :catchall_6
    move-exception v28

    :try_start_27
    monitor-exit v29
    :try_end_27
    .catchall {:try_start_27 .. :try_end_27} :catchall_6

    :try_start_28
    throw v28
    :try_end_28
    .catch Ljava/io/IOException; {:try_start_28 .. :try_end_28} :catch_0
    .catchall {:try_start_28 .. :try_end_28} :catchall_1

    .line 1935
    :catchall_7
    move-exception v28

    :try_start_29
    monitor-exit v29
    :try_end_29
    .catchall {:try_start_29 .. :try_end_29} :catchall_7

    :try_start_2a
    throw v28

    .line 1951
    .end local v7    # "bForceHO":Z
    .end local v8    # "blockLen":I
    .end local v18    # "startWaitTime":J
    .end local v20    # "t1":J
    :cond_46
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesRemaining:J

    move-wide/from16 v28, v0

    int-to-long v0, v15

    move-wide/from16 v30, v0

    sub-long v28, v28, v30

    move-wide/from16 v0, v28

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesRemaining:J

    .line 1953
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesRemaining:J

    move-wide/from16 v28, v0

    const-wide/16 v30, 0x0

    cmp-long v28, v28, v30

    if-gtz v28, :cond_48

    .line 1954
    const/16 v28, 0x1

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bFinished:Z

    .line 1955
    sget-boolean v28, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v28, :cond_47

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    move-object/from16 v28, v0

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "no byte remained, start to end input, remain is "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesRemaining:J

    move-wide/from16 v30, v0

    invoke-virtual/range {v29 .. v31}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1956
    :cond_47
    const/16 v28, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->endOfInput(Z)V

    .line 1957
    sget-boolean v28, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v28, :cond_48

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    move-object/from16 v28, v0

    const-string v29, "end to end input"

    invoke-virtual/range {v28 .. v29}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1959
    :cond_48
    sget-boolean v28, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z
    :try_end_2a
    .catch Ljava/io/IOException; {:try_start_2a .. :try_end_2a} :catch_0
    .catchall {:try_start_2a .. :try_end_2a} :catchall_1

    if-eqz v28, :cond_49

    .line 1960
    :cond_49
    if-ltz v16, :cond_4a

    .line 1970
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v28

    move-wide/from16 v0, v28

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mLastReadTime:J

    .line 1971
    const/16 v28, 0x0

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bAppIsReadingNow:Z

    goto/16 :goto_0

    .line 1970
    :cond_4a
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v28

    move-wide/from16 v0, v28

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mLastReadTime:J

    .line 1971
    const/16 v28, 0x0

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bAppIsReadingNow:Z

    move/from16 v16, v15

    goto/16 :goto_0

    .line 1741
    .restart local v12    # "finalTime":J
    :catch_4
    move-exception v28

    goto/16 :goto_6
.end method

.method protected setTime(I)V
    .locals 8
    .param p1, "sid"    # I

    .prologue
    .line 2376
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_1

    .line 2377
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v4, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mStartReadTime:[J

    aget-wide v4, v4, p1

    sub-long/2addr v2, v4

    iget-object v4, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mTimeForDownload:[J

    aget-wide v4, v4, p1

    sub-long v0, v2, v4

    .line 2378
    .local v0, "timeDiff":J
    const-wide/16 v2, 0x1388

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gez v2, :cond_1

    .line 2379
    :cond_0
    iget-object v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v3, Ljava/lang/Throwable;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "speed calc >> set time for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " from "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mTimeForDownload:[J

    aget-wide v6, v5, p1

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " with a inc "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Lcom/android/okhttp/internal/http/MultiratLog;->w(Ljava/lang/Throwable;)V

    .line 2382
    .end local v0    # "timeDiff":J
    :cond_1
    iget-object v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mTimeForDownload:[J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-object v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mStartReadTime:[J

    aget-wide v6, v3, p1

    sub-long/2addr v4, v6

    aput-wide v4, v2, p1

    .line 2383
    return-void
.end method

.method protected startSingleThreadRunning(Z)V
    .locals 3
    .param p1, "isSingleThread"    # Z

    .prologue
    .line 2055
    if-eqz p1, :cond_0

    iget-boolean v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bSBUsed:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mRAdownloadedFile:Ljava/io/RandomAccessFile;

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bStartReadBufferSBStopSent:Z

    if-nez v0, :cond_0

    .line 2056
    iget-wide v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainThreadID:J

    const/4 v2, -0x5

    invoke-static {v0, v1, v2}, Lcom/android/okhttp/internal/http/SBServiceAPI;->stopSBUsageWithReason(JI)I

    .line 2057
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bStartReadBufferSBStopSent:Z

    .line 2059
    :cond_0
    return-void
.end method
