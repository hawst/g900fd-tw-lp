.class public final Lcom/android/okhttp/internal/http/TwoChunkInputStream;
.super Lcom/android/okhttp/internal/http/AbstractHttpInputStream;
.source "TwoChunkInputStream.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;,
        Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;
    }
.end annotation


# static fields
.field private static final CACHE_READING_SPEED:I = 0x1e

.field private static final MAXDEPTH:I = 0xa

.field private static final MAX_TIMEFOR_REUSE:J = 0x1388L

.field private static final MIN_CACHED_DATA_TO_CONSIDER:J = 0xa00000L

.field private static final MIN_DIV_SIZE:I = 0x100000


# instance fields
.field private final MAX_TIMEFORALL_BY0:J

.field private final MAX_TIMEFORALL_BY1:J

.field private final MAX_TIMEFORTAIL_BY0:J

.field private bFailedInSecChunk:Z

.field private bHasSetMainSocket:Z

.field private bSecThreadExisted:Z

.field private bSecThreadStarted:Z

.field private bServerReject:Z

.field private bSwitchOnFirstRead:Z

.field private childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

.field private connection:Lcom/android/okhttp/Connection;

.field private cr:Ljava/net/CacheRequest;

.field private logger:Lcom/android/okhttp/internal/http/MultiratLog;

.field private mBytesForTail:J

.field protected mChildRAdownloadedFile:Ljava/io/RandomAccessFile;

.field private mDBuf:Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;

.field private mDataDownloaded:[J

.field private final mDepth:I

.field private final mEnd:J

.field private final mFullConSize:J

.field private mKeepSecondChunk:I

.field private final mLength:J

.field private mOriReqHeader:Lcom/android/okhttp/Request;

.field private mPrevSpeed:[J

.field private mRemainBytes:J

.field private mSecChunkEnd:J

.field private mSecChunkStart:J

.field private mSecThread:Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;

.field private mSocket0:I

.field private mSocket1:I

.field private final mStart:J

.field private mStartReadTime:[J

.field private mTimeForDownload:[J

.field private final mTimeOut:I

.field private mainInput:Ljava/io/InputStream;

.field private mainSocket:Ljava/net/Socket;

.field private final parentIS:Lcom/android/okhttp/internal/http/MultiSocketInputStream;


# direct methods
.method constructor <init>(Ljava/io/InputStream;Lcom/android/okhttp/internal/http/HttpEngine;Ljava/net/CacheRequest;IIJJLcom/android/okhttp/Request;JJILcom/android/okhttp/internal/http/MultiSocketInputStream;JIZJLcom/android/okhttp/internal/http/MultiratLog;Lcom/android/okhttp/Connection;)V
    .locals 35
    .param p1, "in"    # Ljava/io/InputStream;
    .param p2, "httpEngine"    # Lcom/android/okhttp/internal/http/HttpEngine;
    .param p3, "cacheRequest"    # Ljava/net/CacheRequest;
    .param p4, "sockID0"    # I
    .param p5, "sockID1"    # I
    .param p6, "start"    # J
    .param p8, "end"    # J
    .param p10, "oriReq"    # Lcom/android/okhttp/Request;
    .param p11, "speed0"    # J
    .param p13, "speed1"    # J
    .param p15, "depth"    # I
    .param p16, "multiInput"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;
    .param p17, "fullSize"    # J
    .param p19, "timeout"    # I
    .param p20, "bswitch"    # Z
    .param p21, "cachedData"    # J
    .param p23, "log"    # Lcom/android/okhttp/internal/http/MultiratLog;
    .param p24, "con"    # Lcom/android/okhttp/Connection;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 161
    invoke-direct/range {p0 .. p3}, Lcom/android/okhttp/internal/http/AbstractHttpInputStream;-><init>(Ljava/io/InputStream;Lcom/android/okhttp/internal/http/HttpEngine;Ljava/net/CacheRequest;)V

    .line 123
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mKeepSecondChunk:I

    .line 135
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->bFailedInSecChunk:Z

    .line 136
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->bServerReject:Z

    .line 137
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mChildRAdownloadedFile:Ljava/io/RandomAccessFile;

    .line 138
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mainSocket:Ljava/net/Socket;

    .line 139
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->bHasSetMainSocket:Z

    .line 140
    const-wide/16 v4, -0x1

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mSecChunkStart:J

    .line 141
    const-wide/16 v4, -0x1

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mSecChunkEnd:J

    .line 162
    move-object/from16 v0, p24

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->connection:Lcom/android/okhttp/Connection;

    .line 163
    move-object/from16 v0, p23

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    .line 164
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mainInput:Ljava/io/InputStream;

    .line 165
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "new input stream in twochunk creator sec mainInput = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mainInput:Ljava/io/InputStream;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 166
    :cond_0
    move-object/from16 v0, p3

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->cr:Ljava/net/CacheRequest;

    .line 167
    move-object/from16 v0, p10

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mOriReqHeader:Lcom/android/okhttp/Request;

    .line 168
    move-wide/from16 v0, p6

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mStart:J

    .line 169
    move-wide/from16 v0, p8

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mEnd:J

    .line 170
    sub-long v4, p8, p6

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mLength:J

    .line 171
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mLength:J

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mRemainBytes:J

    .line 172
    move/from16 v0, p4

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mSocket0:I

    .line 173
    move/from16 v0, p5

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mSocket1:I

    .line 174
    move/from16 v0, p15

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mDepth:I

    .line 175
    move-object/from16 v0, p16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->parentIS:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    .line 176
    move-wide/from16 v0, p17

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mFullConSize:J

    .line 177
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    .line 178
    move/from16 v0, p19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mTimeOut:I

    .line 180
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->parentIS:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-boolean v4, v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bISHTTPS:Z

    if-eqz v4, :cond_1

    .line 181
    const-wide/16 v4, 0xfa0

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->MAX_TIMEFORTAIL_BY0:J

    .line 182
    const-wide/16 v4, 0x2710

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->MAX_TIMEFORALL_BY0:J

    .line 183
    const-wide/16 v4, 0x1f40

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->MAX_TIMEFORALL_BY1:J

    .line 191
    :goto_0
    const/4 v4, 0x2

    new-array v4, v4, [J

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mDataDownloaded:[J

    .line 192
    const/4 v4, 0x2

    new-array v4, v4, [J

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mTimeForDownload:[J

    .line 193
    const/4 v4, 0x2

    new-array v4, v4, [J

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mStartReadTime:[J

    .line 194
    const/4 v4, 0x2

    new-array v4, v4, [J

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mPrevSpeed:[J

    .line 195
    const/16 v19, 0x0

    .local v19, "i":I
    :goto_1
    const/4 v4, 0x2

    move/from16 v0, v19

    if-ge v0, v4, :cond_2

    .line 196
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mDataDownloaded:[J

    const-wide/16 v6, 0x0

    aput-wide v6, v4, v19

    .line 197
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mTimeForDownload:[J

    const-wide/16 v6, 0x0

    aput-wide v6, v4, v19

    .line 198
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mStartReadTime:[J

    const-wide/16 v6, 0x0

    aput-wide v6, v4, v19

    .line 195
    add-int/lit8 v19, v19, 0x1

    goto :goto_1

    .line 186
    .end local v19    # "i":I
    :cond_1
    const-wide/16 v4, 0x7d0

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->MAX_TIMEFORTAIL_BY0:J

    .line 187
    const-wide/16 v4, 0x1388

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->MAX_TIMEFORALL_BY0:J

    .line 188
    const-wide/16 v4, 0xfa0

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->MAX_TIMEFORALL_BY1:J

    goto :goto_0

    .line 200
    .restart local v19    # "i":I
    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mPrevSpeed:[J

    aput-wide p11, v4, p4

    .line 201
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mPrevSpeed:[J

    aput-wide p13, v4, p5

    .line 202
    move/from16 v0, p20

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->bSwitchOnFirstRead:Z

    .line 204
    const-wide/high16 v24, 0x4059000000000000L    # 100.0

    .line 205
    .local v24, "ratio":D
    const-wide/16 v4, 0x0

    cmp-long v4, p13, v4

    if-eqz v4, :cond_3

    .line 206
    const-wide/high16 v6, 0x4059000000000000L    # 100.0

    const-wide v10, 0x3f847ae147ae147bL    # 0.01

    move-wide/from16 v0, p11

    long-to-double v14, v0

    move-wide/from16 v0, p13

    long-to-double v0, v0

    move-wide/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->parentIS:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-boolean v4, v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bISHTTPS:Z

    if-eqz v4, :cond_a

    const/4 v4, 0x2

    :goto_2
    int-to-double v4, v4

    div-double v4, v16, v4

    div-double v4, v14, v4

    invoke-static {v10, v11, v4, v5}, Ljava/lang/Math;->max(DD)D

    move-result-wide v4

    invoke-static {v6, v7, v4, v5}, Ljava/lang/Math;->min(DD)D

    move-result-wide v24

    .line 208
    :cond_3
    move-wide/from16 v20, p21

    .line 209
    .local v20, "cd":J
    const-wide/32 v4, 0xa00000

    cmp-long v4, v20, v4

    if-gtz v4, :cond_4

    const-wide/16 v20, 0x0

    .line 210
    :cond_4
    const-wide/16 v4, 0x400

    div-long v4, v20, v4

    const-wide/16 v6, 0x400

    div-long/2addr v4, v6

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    long-to-double v4, v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->parentIS:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-boolean v6, v6, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bISHTTPS:Z

    move-object/from16 v0, p0

    move-wide/from16 v1, p17

    invoke-direct {v0, v1, v2, v6}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->getCacheReadSpeed(JZ)D

    move-result-wide v6

    div-double/2addr v4, v6

    double-to-long v0, v4

    move-wide/from16 v30, v0

    .line 211
    .local v30, "timeForCachedData":J
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mLength:J

    const-wide/16 v6, 0x8

    div-long v6, p11, v6

    mul-long v6, v6, v30

    add-long/2addr v4, v6

    long-to-double v6, v4

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    add-double v10, v24, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->parentIS:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-object v4, v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mRAdownloadedFile:Ljava/io/RandomAccessFile;

    if-nez v4, :cond_b

    sget-wide v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->B_Ratio_BUF_LTE:D

    div-double v4, v24, v4

    :goto_3
    sub-double v4, v10, v4

    div-double v4, v6, v4

    double-to-long v8, v4

    .line 212
    .local v8, "lenTail":J
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mLength:J

    cmp-long v4, v8, v4

    if-lez v4, :cond_5

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mLength:J

    .line 213
    :cond_5
    const-wide/16 v4, 0x0

    cmp-long v4, p11, v4

    if-nez v4, :cond_c

    const-wide v32, 0x7fffffffffffffffL

    .line 214
    .local v32, "timeForTailBy0":J
    :goto_4
    const-wide/16 v4, 0x0

    cmp-long v4, p11, v4

    if-nez v4, :cond_d

    const-wide v26, 0x7fffffffffffffffL

    .line 215
    .local v26, "timeForAllBy0":J
    :goto_5
    const-wide/16 v4, 0x0

    cmp-long v4, p13, v4

    if-nez v4, :cond_e

    const-wide v28, 0x7fffffffffffffffL

    .line 217
    .local v28, "timeForAllBy1":J
    :goto_6
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_6

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "start twochunkInputStream["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p4

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] : range="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, p6

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "-"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, p8

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", speed="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, p11

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ":"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, p13

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v24

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "), depth="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p15

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", tail="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", timeForTailBy0="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v32

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", timeForAllBy0="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v26

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", timeForAllBy1="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v28

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", cachedData="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, p21

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", timeForCachedData="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v30

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", SocketTimeoutException Count: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->parentIS:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-object v6, v6, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->disconnCount:[I

    aget v6, v6, p4

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->parentIS:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-object v6, v6, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->disconnCount:[I

    aget v6, v6, p5

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 223
    :cond_6
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mLength:J

    const-wide/32 v6, 0x100000

    cmp-long v4, v4, v6

    if-gtz v4, :cond_f

    .line 224
    const-wide/16 v4, 0x0

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mBytesForTail:J

    .line 225
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mSecThread:Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;

    .line 226
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mDBuf:Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;

    .line 227
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->bSecThreadStarted:Z

    .line 228
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->bSecThreadExisted:Z

    .line 229
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mKeepSecondChunk:I

    .line 230
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_7

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "download by one interface for length is small, bSwitchOnFirstRead = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->bSwitchOnFirstRead:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 292
    :cond_7
    :goto_7
    if-eqz p20, :cond_8

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mKeepSecondChunk:I

    .line 293
    :cond_8
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_9

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "calculated mBytesForTail is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mBytesForTail:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", chunk0: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, p6

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "-"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mBytesForTail:J

    sub-long v6, p8, v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", chunk1: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mBytesForTail:J

    sub-long v6, p8, v6

    const-wide/16 v10, 0x1

    add-long/2addr v6, v10

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "-"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, p8

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", mKeepSecondChunk is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mKeepSecondChunk:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 297
    :cond_9
    return-void

    .line 206
    .end local v8    # "lenTail":J
    .end local v20    # "cd":J
    .end local v26    # "timeForAllBy0":J
    .end local v28    # "timeForAllBy1":J
    .end local v30    # "timeForCachedData":J
    .end local v32    # "timeForTailBy0":J
    :cond_a
    const/4 v4, 0x1

    goto/16 :goto_2

    .line 211
    .restart local v20    # "cd":J
    .restart local v30    # "timeForCachedData":J
    :cond_b
    const-wide/16 v4, 0x0

    goto/16 :goto_3

    .line 213
    .restart local v8    # "lenTail":J
    :cond_c
    const-wide/16 v4, 0x8

    mul-long/2addr v4, v8

    div-long v32, v4, p11

    goto/16 :goto_4

    .line 214
    .restart local v32    # "timeForTailBy0":J
    :cond_d
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mLength:J

    const-wide/16 v6, 0x8

    mul-long/2addr v4, v6

    div-long v26, v4, p11

    goto/16 :goto_5

    .line 215
    .restart local v26    # "timeForAllBy0":J
    :cond_e
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mLength:J

    const-wide/16 v6, 0x8

    mul-long/2addr v4, v6

    div-long v4, v4, p13

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->parentIS:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-object v6, v6, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->connectionTime:[J

    aget-wide v6, v6, p5

    add-long v28, v4, v6

    goto/16 :goto_6

    .line 232
    .restart local v28    # "timeForAllBy1":J
    :cond_f
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->MAX_TIMEFORALL_BY0:J

    cmp-long v4, v26, v4

    if-ltz v4, :cond_10

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->MAX_TIMEFORALL_BY1:J

    cmp-long v4, v28, v4

    if-gez v4, :cond_12

    .line 233
    :cond_10
    const-wide/16 v4, 0x0

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mBytesForTail:J

    .line 234
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mSecThread:Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;

    .line 235
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mDBuf:Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;

    .line 236
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->bSecThreadStarted:Z

    .line 237
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->bSecThreadExisted:Z

    .line 238
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mKeepSecondChunk:I

    .line 239
    sub-long v4, v26, v28

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->MAX_TIMEFORALL_BY0:J

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->MAX_TIMEFORALL_BY1:J

    sub-long/2addr v6, v10

    const-wide/16 v10, 0x2

    mul-long/2addr v6, v10

    cmp-long v4, v4, v6

    if-lez v4, :cond_11

    .line 240
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->bSwitchOnFirstRead:Z

    .line 241
    const/4 v4, -0x1

    move-object/from16 v0, p0

    iput v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mKeepSecondChunk:I

    .line 243
    :cond_11
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_7

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "download by one interface time is small, bSwitchOnFirstRead = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->bSwitchOnFirstRead:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    goto/16 :goto_7

    .line 245
    :cond_12
    const-wide/16 v4, 0x2

    cmp-long v4, p13, v4

    if-gtz v4, :cond_14

    .line 246
    const-wide/16 v22, 0x0

    .line 247
    .end local v8    # "lenTail":J
    .local v22, "lenTail":J
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_13

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    const-string v5, "speed1 is 0, set tail be 0"

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 248
    :cond_13
    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mBytesForTail:J

    .line 249
    new-instance v5, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;

    const-wide/16 v6, 0x1

    sub-long v8, p8, v6

    const/4 v12, 0x0

    move-object/from16 v6, p0

    move/from16 v7, p5

    move-wide/from16 v10, p8

    invoke-direct/range {v5 .. v12}, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;-><init>(Lcom/android/okhttp/internal/http/TwoChunkInputStream;IJJLcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mSecThread:Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;

    .line 250
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mKeepSecondChunk:I

    move-wide/from16 v8, v22

    .end local v22    # "lenTail":J
    .restart local v8    # "lenTail":J
    goto/16 :goto_7

    .line 252
    :cond_14
    const-wide/16 v4, 0x2

    cmp-long v4, p11, v4

    if-gtz v4, :cond_16

    .line 253
    const-wide/16 v22, 0x0

    .line 254
    .end local v8    # "lenTail":J
    .restart local v22    # "lenTail":J
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_15

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    const-string v5, "speed0 is 0, switch chunk and set tail be 0"

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 255
    :cond_15
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->bSwitchOnFirstRead:Z

    .line 256
    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mBytesForTail:J

    .line 257
    new-instance v5, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;

    const-wide/16 v6, 0x1

    sub-long v8, p8, v6

    const/4 v12, 0x0

    move-object/from16 v6, p0

    move/from16 v7, p4

    move-wide/from16 v10, p8

    invoke-direct/range {v5 .. v12}, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;-><init>(Lcom/android/okhttp/internal/http/TwoChunkInputStream;IJJLcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mSecThread:Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;

    .line 258
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mKeepSecondChunk:I

    move-wide/from16 v8, v22

    .end local v22    # "lenTail":J
    .restart local v8    # "lenTail":J
    goto/16 :goto_7

    .line 260
    :cond_16
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->MAX_TIMEFORTAIL_BY0:J

    cmp-long v4, v32, v4

    if-ltz v4, :cond_1e

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mDepth:I

    const/16 v5, 0xa

    if-gt v4, v5, :cond_1e

    .line 261
    move/from16 v13, p5

    .line 262
    .local v13, "secChunkSock":I
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    cmpg-double v4, v24, v4

    if-gez v4, :cond_17

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->parentIS:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-object v4, v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->disconnCount:[I

    aget v4, v4, p4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->parentIS:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-object v5, v5, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->disconnCount:[I

    aget v5, v5, p5

    if-gt v4, v5, :cond_19

    :cond_17
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->parentIS:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-object v4, v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->disconnCount:[I

    aget v4, v4, p4

    const/4 v5, 0x2

    if-lt v4, v5, :cond_18

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->parentIS:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-object v4, v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->disconnCount:[I

    aget v4, v4, p5

    if-eqz v4, :cond_19

    :cond_18
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->parentIS:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-object v4, v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->disconnCount:[I

    aget v4, v4, p4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->parentIS:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-object v5, v5, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->disconnCount:[I

    aget v5, v5, p5

    mul-int/lit8 v5, v5, 0x2

    if-le v4, v5, :cond_1b

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->parentIS:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-object v4, v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->disconnCount:[I

    aget v4, v4, p5

    if-lez v4, :cond_1b

    .line 265
    :cond_19
    const-wide/high16 v4, 0x4024000000000000L    # 10.0

    const-wide v6, 0x3fb999999999999aL    # 0.1

    move-wide/from16 v0, p13

    long-to-double v10, v0

    move-wide/from16 v0, p11

    long-to-double v14, v0

    div-double/2addr v10, v14

    invoke-static {v6, v7, v10, v11}, Ljava/lang/Math;->max(DD)D

    move-result-wide v6

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->min(DD)D

    move-result-wide v24

    .line 267
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mLength:J

    long-to-double v6, v4

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    add-double v10, v24, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->parentIS:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-object v4, v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mRAdownloadedFile:Ljava/io/RandomAccessFile;

    if-nez v4, :cond_1d

    sget-wide v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->B_Ratio_BUF_LTE:D

    div-double v4, v24, v4

    :goto_8
    sub-double v4, v10, v4

    div-double v4, v6, v4

    double-to-long v8, v4

    .line 268
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_1a

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    const-string v5, "ratio too small and there is disconnection experience in sockID0, switch chunk"

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 269
    :cond_1a
    move/from16 v13, p4

    .line 270
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->bSwitchOnFirstRead:Z

    .line 272
    :cond_1b
    move-object/from16 v0, p0

    iput-wide v8, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mBytesForTail:J

    .line 273
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->parentIS:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-object v4, v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mRAdownloadedFile:Ljava/io/RandomAccessFile;

    if-eqz v4, :cond_1c

    .line 274
    new-instance v4, Ljava/io/RandomAccessFile;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->parentIS:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-object v5, v5, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mDownloadedFileName:Ljava/lang/String;

    const-string v6, "rw"

    invoke-direct {v4, v5, v6}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mChildRAdownloadedFile:Ljava/io/RandomAccessFile;

    .line 275
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_1c

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[fileIO][create] "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->parentIS:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-object v6, v6, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mDownloadedFileName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", current file position "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mChildRAdownloadedFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v6}, Ljava/io/RandomAccessFile;->getFilePointer()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 277
    :cond_1c
    new-instance v4, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;

    sub-long v6, p8, v8

    const-wide/16 v10, 0x1

    add-long/2addr v6, v10

    move-object/from16 v5, p0

    invoke-direct/range {v4 .. v9}, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;-><init>(Lcom/android/okhttp/internal/http/TwoChunkInputStream;JJ)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mDBuf:Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;

    .line 278
    new-instance v11, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;

    sub-long v4, p8, v8

    const-wide/16 v6, 0x1

    add-long v14, v4, v6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mDBuf:Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;

    move-object/from16 v18, v0

    move-object/from16 v12, p0

    move-wide/from16 v16, p8

    invoke-direct/range {v11 .. v18}, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;-><init>(Lcom/android/okhttp/internal/http/TwoChunkInputStream;IJJLcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;)V

    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mSecThread:Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;

    .line 279
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mKeepSecondChunk:I

    .line 280
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_7

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "divide chunk with a tail length "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", bSwitchOnFirstRead = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->bSwitchOnFirstRead:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    goto/16 :goto_7

    .line 267
    :cond_1d
    const-wide/16 v4, 0x0

    goto/16 :goto_8

    .line 283
    .end local v13    # "secChunkSock":I
    :cond_1e
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_1f

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "tail chunk is small, not to start another chunk for length "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 284
    :cond_1f
    const-wide/16 v8, 0x0

    .line 285
    move-object/from16 v0, p0

    iput-wide v8, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mBytesForTail:J

    .line 286
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mSecThread:Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;

    .line 287
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mDBuf:Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;

    .line 288
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->bSecThreadStarted:Z

    .line 289
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->bSecThreadExisted:Z

    .line 290
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mKeepSecondChunk:I

    goto/16 :goto_7
.end method

.method static synthetic access$000(Lcom/android/okhttp/internal/http/TwoChunkInputStream;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/TwoChunkInputStream;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->log(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;
    .locals 1
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)I
    .locals 1
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    .prologue
    .line 23
    iget v0, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mDepth:I

    return v0
.end method

.method static synthetic access$1100(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)J
    .locals 2
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    .prologue
    .line 23
    iget-wide v0, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mFullConSize:J

    return-wide v0
.end method

.method static synthetic access$1200(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)I
    .locals 1
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    .prologue
    .line 23
    iget v0, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mTimeOut:I

    return v0
.end method

.method static synthetic access$1300(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/Connection;
    .locals 1
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->connection:Lcom/android/okhttp/Connection;

    return-object v0
.end method

.method static synthetic access$1402(Lcom/android/okhttp/internal/http/TwoChunkInputStream;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/TwoChunkInputStream;
    .param p1, "x1"    # Z

    .prologue
    .line 23
    iput-boolean p1, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->bFailedInSecChunk:Z

    return p1
.end method

.method static synthetic access$1500(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)I
    .locals 1
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    .prologue
    .line 23
    iget v0, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mSocket0:I

    return v0
.end method

.method static synthetic access$1600(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;
    .locals 1
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mDBuf:Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;

    return-object v0
.end method

.method static synthetic access$1602(Lcom/android/okhttp/internal/http/TwoChunkInputStream;Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;)Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;
    .locals 0
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/TwoChunkInputStream;
    .param p1, "x1"    # Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;

    .prologue
    .line 23
    iput-object p1, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mDBuf:Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;

    return-object p1
.end method

.method static synthetic access$1700(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)[J
    .locals 1
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mStartReadTime:[J

    return-object v0
.end method

.method static synthetic access$1800(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)J
    .locals 2
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    .prologue
    .line 23
    iget-wide v0, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mSecChunkStart:J

    return-wide v0
.end method

.method static synthetic access$1802(Lcom/android/okhttp/internal/http/TwoChunkInputStream;J)J
    .locals 1
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/TwoChunkInputStream;
    .param p1, "x1"    # J

    .prologue
    .line 23
    iput-wide p1, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mSecChunkStart:J

    return-wide p1
.end method

.method static synthetic access$1902(Lcom/android/okhttp/internal/http/TwoChunkInputStream;J)J
    .locals 1
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/TwoChunkInputStream;
    .param p1, "x1"    # J

    .prologue
    .line 23
    iput-wide p1, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mSecChunkEnd:J

    return-wide p1
.end method

.method static synthetic access$1914(Lcom/android/okhttp/internal/http/TwoChunkInputStream;J)J
    .locals 3
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/TwoChunkInputStream;
    .param p1, "x1"    # J

    .prologue
    .line 23
    iget-wide v0, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mSecChunkEnd:J

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mSecChunkEnd:J

    return-wide v0
.end method

.method static synthetic access$200(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream;
    .locals 1
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->parentIS:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)[J
    .locals 1
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mTimeForDownload:[J

    return-object v0
.end method

.method static synthetic access$2102(Lcom/android/okhttp/internal/http/TwoChunkInputStream;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/TwoChunkInputStream;
    .param p1, "x1"    # Z

    .prologue
    .line 23
    iput-boolean p1, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->bSecThreadExisted:Z

    return p1
.end method

.method static synthetic access$2200(Lcom/android/okhttp/internal/http/TwoChunkInputStream;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/TwoChunkInputStream;
    .param p1, "x1"    # Z

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->createTwoChunkInTwoChunk(Z)V

    return-void
.end method

.method static synthetic access$300(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)J
    .locals 2
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    .prologue
    .line 23
    iget-wide v0, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mRemainBytes:J

    return-wide v0
.end method

.method static synthetic access$400(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)J
    .locals 2
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    .prologue
    .line 23
    iget-wide v0, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mBytesForTail:J

    return-wide v0
.end method

.method static synthetic access$402(Lcom/android/okhttp/internal/http/TwoChunkInputStream;J)J
    .locals 1
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/TwoChunkInputStream;
    .param p1, "x1"    # J

    .prologue
    .line 23
    iput-wide p1, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mBytesForTail:J

    return-wide p1
.end method

.method static synthetic access$500(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/TwoChunkInputStream;
    .locals 1
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    return-object v0
.end method

.method static synthetic access$502(Lcom/android/okhttp/internal/http/TwoChunkInputStream;Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/TwoChunkInputStream;
    .locals 0
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/TwoChunkInputStream;
    .param p1, "x1"    # Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    .prologue
    .line 23
    iput-object p1, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    return-object p1
.end method

.method static synthetic access$600(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)J
    .locals 2
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    .prologue
    .line 23
    iget-wide v0, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mLength:J

    return-wide v0
.end method

.method static synthetic access$700(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)J
    .locals 2
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    .prologue
    .line 23
    iget-wide v0, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mStart:J

    return-wide v0
.end method

.method static synthetic access$800(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Ljava/net/CacheRequest;
    .locals 1
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->cr:Ljava/net/CacheRequest;

    return-object v0
.end method

.method static synthetic access$900(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/Request;
    .locals 1
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mOriReqHeader:Lcom/android/okhttp/Request;

    return-object v0
.end method

.method private closeMainSocket()V
    .locals 3

    .prologue
    .line 362
    sget-boolean v0, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "try to close maininput and socket, input is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mainInput:Ljava/io/InputStream;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", socket is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->connection:Lcom/android/okhttp/Connection;

    invoke-virtual {v2}, Lcom/android/okhttp/Connection;->getSocket()Ljava/net/Socket;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 363
    :cond_0
    iget-object v0, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mainInput:Ljava/io/InputStream;

    invoke-static {v0}, Lcom/android/okhttp/internal/http/MultiratUtil;->closeQuietly(Ljava/io/Closeable;)V

    .line 364
    iget-object v0, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->connection:Lcom/android/okhttp/Connection;

    invoke-virtual {v0}, Lcom/android/okhttp/Connection;->getSocket()Ljava/net/Socket;

    move-result-object v0

    invoke-static {v0}, Lcom/android/okhttp/internal/http/MultiratUtil;->closeQuietly(Ljava/net/Socket;)V

    .line 365
    return-void
.end method

.method private createTwoChunkInTwoChunk(Z)V
    .locals 28
    .param p1, "bReversed"    # Z

    .prologue
    .line 567
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->parentIS:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-boolean v2, v2, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bFinished:Z

    if-eqz v2, :cond_1

    .line 568
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    const-string v3, "createTwoChunkInTwoChunk: session is finished"

    invoke-virtual {v2, v3}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 602
    :cond_0
    :goto_0
    return-void

    .line 571
    :cond_1
    monitor-enter p0

    .line 572
    :try_start_0
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->bSecThreadStarted:Z

    if-eqz v2, :cond_5

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->bSecThreadExisted:Z

    if-nez v2, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    if-nez v2, :cond_5

    .line 573
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Chunk1 is finished while mRemainBytes="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mRemainBytes:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " totallen="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mLength:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " start offset="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mStart:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 575
    :cond_2
    :try_start_1
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mRemainBytes:J

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mBytesForTail:J

    const-wide/32 v6, 0x100000

    add-long/2addr v4, v6

    cmp-long v2, v2, v4

    if-lez v2, :cond_6

    .line 576
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->parentIS:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mSocket0:I

    invoke-virtual {v2, v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->getSpeed(I)J

    move-result-wide v14

    .line 577
    .local v14, "speed0":J
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mSocket0:I

    add-int/lit8 v2, v2, 0x1

    rem-int/lit8 v2, v2, 0x2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mSocket1:I

    .line 578
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->parentIS:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mSocket1:I

    invoke-virtual {v2, v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->getSpeed(I)J

    move-result-wide v16

    .line 579
    .local v16, "speed1":J
    new-instance v3, Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mainInput:Ljava/io/InputStream;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->cr:Ljava/net/CacheRequest;

    move-object/from16 v0, p0

    iget v7, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mSocket0:I

    move-object/from16 v0, p0

    iget v8, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mSocket1:I

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mStart:J

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mLength:J

    add-long/2addr v10, v12

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mRemainBytes:J

    sub-long v9, v10, v12

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mStart:J

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mLength:J

    move-wide/from16 v18, v0

    add-long v12, v12, v18

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mBytesForTail:J

    move-wide/from16 v18, v0

    sub-long v12, v12, v18

    const-wide/16 v18, 0x1

    sub-long v11, v12, v18

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mOriReqHeader:Lcom/android/okhttp/Request;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mDepth:I

    add-int/lit8 v18, v2, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->parentIS:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mFullConSize:J

    move-wide/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mTimeOut:I

    move/from16 v22, v0

    const-wide/16 v24, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->connection:Lcom/android/okhttp/Connection;

    move-object/from16 v27, v0

    move/from16 v23, p1

    invoke-direct/range {v3 .. v27}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;-><init>(Ljava/io/InputStream;Lcom/android/okhttp/internal/http/HttpEngine;Ljava/net/CacheRequest;IIJJLcom/android/okhttp/Request;JJILcom/android/okhttp/internal/http/MultiSocketInputStream;JIZJLcom/android/okhttp/internal/http/MultiratLog;Lcom/android/okhttp/Connection;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    .line 582
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mainInput:Ljava/io/InputStream;

    .line 583
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "new input stream in create twochunk sec mainInput = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mainInput:Ljava/io/InputStream;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 584
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    if-eqz v2, :cond_4

    .line 585
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->parentIS:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    invoke-virtual {v3}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->isSingleThreadRun()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->startSingleThreadRunning(Z)V

    .line 586
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->connection:Lcom/android/okhttp/Connection;

    invoke-virtual {v3}, Lcom/android/okhttp/Connection;->getSocket()Ljava/net/Socket;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->startRun(Ljava/net/Socket;)V

    .line 589
    :cond_4
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "resource check: finish reading chunk "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mStart:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "-"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mStart:J

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mLength:J

    add-long/2addr v4, v6

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mRemainBytes:J

    sub-long/2addr v4, v6

    const-wide/16 v6, 0x1

    sub-long/2addr v4, v6

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " with length "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mLength:J

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mRemainBytes:J

    sub-long/2addr v4, v6

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " from child socket, "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " now start to read from child child input "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 601
    .end local v14    # "speed0":J
    .end local v16    # "speed1":J
    :cond_5
    :goto_1
    :try_start_2
    monitor-exit p0

    goto/16 :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    .line 594
    :cond_6
    :try_start_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->parentIS:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->startSingleThreadRunning(Z)V

    .line 595
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "not to create sec thread since mRemainBytes is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mRemainBytes:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " and mBytesForTail is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mBytesForTail:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 599
    :catch_0
    move-exception v2

    goto :goto_1
.end method

.method private getCacheReadSpeed(JZ)D
    .locals 2
    .param p1, "fileSize"    # J
    .param p3, "bIsHTTPS"    # Z

    .prologue
    .line 1549
    sget-wide v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->BUF_Read_Speed:D

    return-wide v0
.end method

.method private log(Ljava/lang/String;)V
    .locals 3
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 358
    iget-object v0, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "(Depth_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mDepth:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 359
    return-void
.end method

.method private readForChunk0([BII)I
    .locals 30
    .param p1, "buffer"    # [B
    .param p2, "offset"    # I
    .param p3, "count"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 614
    const/16 v19, 0x0

    .line 616
    .local v19, "isMyThrown":Z
    :try_start_0
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->bSwitchOnFirstRead:Z

    if-eqz v5, :cond_6

    .line 617
    sget-boolean v5, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v5, :cond_0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Need to switch socket for the first read from "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget v7, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mSocket0:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " to "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget v7, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mSocket1:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 618
    :cond_0
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->bSwitchOnFirstRead:Z

    .line 619
    const/16 v19, 0x1

    .line 620
    new-instance v5, Ljava/io/IOException;

    const-string v6, "Please switch socket"

    invoke-direct {v5, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 634
    :catch_0
    move-exception v17

    .line 635
    .local v17, "e":Ljava/io/IOException;
    sget-boolean v5, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v5, :cond_1

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "IOException in TwoChunk reading while mRemainBytes="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mRemainBytes:J

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " totallen="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mLength:J

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " start offset="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mStart:J

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 636
    :cond_1
    sget-boolean v5, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v5, :cond_2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V

    .line 637
    :cond_2
    if-nez v19, :cond_3

    .line 638
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->parentIS:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-object v5, v5, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->disconnCount:[I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mSocket0:I

    aget v7, v5, v6

    add-int/lit8 v7, v7, 0x1

    aput v7, v5, v6

    .line 639
    :cond_3
    sget-boolean v5, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v5, :cond_4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SocketTimeoutException Count: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->parentIS:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-object v6, v6, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->disconnCount:[I

    const/4 v7, 0x0

    aget v6, v6, v7

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->parentIS:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-object v6, v6, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->disconnCount:[I

    const/4 v7, 0x1

    aget v6, v6, v7

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->log(Ljava/lang/String;)V

    .line 640
    :cond_4
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->parentIS:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-boolean v5, v5, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bDoNotUseMultiSockSinceWiFiOnly:Z

    if-eqz v5, :cond_a

    .line 641
    sget-boolean v5, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v5, :cond_5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    const-string v6, "throw exception since bDoNotUseMultiSockSinceWiFiOnly is true"

    invoke-virtual {v5, v6}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 642
    :cond_5
    throw v17

    .line 622
    .end local v17    # "e":Ljava/io/IOException;
    :cond_6
    :try_start_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mStartReadTime:[J

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mSocket0:I

    aget-wide v6, v5, v6

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-nez v5, :cond_7

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mStartReadTime:[J

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mSocket0:I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    aput-wide v8, v5, v6

    .line 623
    :cond_7
    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->createTwoChunkInTwoChunk(Z)V

    .line 624
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mainInput:Ljava/io/InputStream;

    move-object/from16 v0, p1

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-virtual {v5, v0, v1, v2}, Ljava/io/InputStream;->read([BII)I

    move-result v21

    .line 625
    .local v21, "read":I
    if-gez v21, :cond_8

    .line 626
    new-instance v5, Ljava/io/IOException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "read return exception value "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v21

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 628
    :cond_8
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    if-nez v5, :cond_9

    .line 629
    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mSocket0:I

    move/from16 v0, v21

    int-to-long v6, v0

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6, v7}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->incByte(IJ)V

    .line 630
    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mSocket0:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->setTime(I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 715
    .end local v21    # "read":I
    :cond_9
    :goto_0
    return v21

    .line 644
    .restart local v17    # "e":Ljava/io/IOException;
    :cond_a
    monitor-enter p0

    .line 645
    :try_start_2
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    if-eqz v5, :cond_d

    .line 646
    sget-boolean v5, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v5, :cond_b

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    const-string v6, "childIS is created"

    invoke-virtual {v5, v6}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 647
    :cond_b
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mainInput:Ljava/io/InputStream;

    move-object/from16 v0, p1

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-virtual {v5, v0, v1, v2}, Ljava/io/InputStream;->read([BII)I

    move-result v21

    .line 648
    .restart local v21    # "read":I
    sget-boolean v5, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v5, :cond_c

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "childIS is created and read "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v21

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 649
    :cond_c
    monitor-exit p0

    goto :goto_0

    .line 651
    .end local v21    # "read":I
    :catchall_0
    move-exception v5

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v5

    :cond_d
    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 652
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v24

    .line 653
    .local v24, "startWaitTime":J
    const/4 v4, 0x0

    .line 655
    .local v4, "bForceThrow":Z
    const/16 v26, 0x0

    .line 656
    .local v26, "triedNum":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->parentIS:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-boolean v5, v5, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bFinished:Z

    if-nez v5, :cond_24

    .line 658
    const/4 v5, 0x3

    move/from16 v0, v26

    if-le v0, v5, :cond_13

    .line 659
    :try_start_4
    sget-boolean v5, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v5, :cond_e

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    const-string v6, "tried twice, check NB status"

    invoke-virtual {v5, v6}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 660
    :cond_e
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->parentIS:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    invoke-virtual {v5}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->isMultiRATworking()Z

    move-result v5

    if-nez v5, :cond_12

    .line 661
    sget-boolean v5, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v5, :cond_f

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    const-string v6, "tried twice, and NB Status is false"

    invoke-virtual {v5, v6}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 662
    :cond_f
    const/4 v4, 0x1

    .line 663
    throw v17
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    .line 717
    :catch_1
    move-exception v18

    .line 718
    .local v18, "ex":Ljava/io/IOException;
    sget-boolean v5, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v5, :cond_10

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Exception in TwoChunk mainInput Handover while force throw is "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 719
    :cond_10
    sget-boolean v5, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v5, :cond_11

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V

    .line 720
    :cond_11
    if-eqz v4, :cond_20

    throw v18

    .line 666
    .end local v18    # "ex":Ljava/io/IOException;
    :cond_12
    const/16 v26, 0x0

    .line 669
    :cond_13
    :try_start_5
    monitor-enter p0
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    .line 670
    :try_start_6
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    if-nez v5, :cond_1d

    .line 671
    const/16 v23, 0x0

    .line 672
    .local v23, "s":Ljava/net/Socket;
    sget-boolean v5, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v5, :cond_14

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "try to close main input stream  in exception in depth "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget v7, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mDepth:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", input is "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mainInput:Ljava/io/InputStream;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 673
    :cond_14
    invoke-direct/range {p0 .. p0}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->closeMainSocket()V

    .line 674
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->connection:Lcom/android/okhttp/Connection;

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mSocket1:I

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mOriReqHeader:Lcom/android/okhttp/Request;

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mStart:J

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mLength:J

    add-long/2addr v8, v10

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mRemainBytes:J

    sub-long/2addr v8, v10

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mEnd:J

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mFullConSize:J

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->parentIS:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    const/4 v15, 0x1

    const/16 v16, 0x0

    invoke-virtual/range {v5 .. v16}, Lcom/android/okhttp/Connection;->extremeConditionConnect(ILcom/android/okhttp/Request;JJJLcom/android/okhttp/internal/http/MultiSocketInputStream;ZZ)Ljava/net/Socket;

    move-result-object v23

    .line 676
    if-nez v23, :cond_15

    .line 677
    const/4 v4, 0x1

    .line 678
    const/4 v5, 0x1

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->bServerReject:Z

    .line 679
    new-instance v5, Ljava/io/IOException;

    const-string v6, "Cannot Connect to Server"

    invoke-direct {v5, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 705
    .end local v23    # "s":Ljava/net/Socket;
    :catchall_1
    move-exception v5

    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :try_start_7
    throw v5
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1

    .line 681
    .restart local v23    # "s":Ljava/net/Socket;
    :cond_15
    :try_start_8
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mSocket0:I

    move/from16 v20, v0

    .line 682
    .local v20, "preMainSocketID":I
    invoke-virtual/range {v23 .. v23}, Ljava/net/Socket;->getLocalAddress()Ljava/net/InetAddress;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->connection:Lcom/android/okhttp/Connection;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lcom/android/okhttp/Connection;->getLocalAddr(I)Ljava/net/InetAddress;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/net/InetAddress;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1c

    .line 683
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput v5, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mSocket0:I

    .line 684
    const/4 v5, 0x1

    move-object/from16 v0, p0

    iput v5, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mSocket1:I

    .line 690
    :goto_2
    sget-boolean v5, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v5, :cond_16

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "This is TwoChunk MainThead, actually switch to "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget v7, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mSocket0:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " from "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v20

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 691
    :cond_16
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->bSecThreadStarted:Z

    if-eqz v5, :cond_18

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->bSecThreadExisted:Z

    if-eqz v5, :cond_18

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mSocket0:I

    move/from16 v0, v20

    if-eq v5, v0, :cond_18

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mSecThread:Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;

    if-eqz v5, :cond_18

    .line 692
    sget-boolean v5, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v5, :cond_17

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    const-string v6, "Need to switch TwoChunk SecondThread Socket ID"

    invoke-virtual {v5, v6}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 693
    :cond_17
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mSecThread:Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;

    move/from16 v0, v20

    invoke-virtual {v5, v0}, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->switchSocket(I)V

    .line 695
    :cond_18
    sget-boolean v5, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v5, :cond_19

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Twochunk Main socket reconnected, now from "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mStart:J

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mLength:J

    add-long/2addr v8, v10

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mRemainBytes:J

    sub-long/2addr v8, v10

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "-"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mEnd:J

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 698
    :cond_19
    invoke-virtual/range {v23 .. v23}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mainInput:Ljava/io/InputStream;

    .line 699
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->connection:Lcom/android/okhttp/Connection;

    move-object/from16 v0, v23

    invoke-virtual {v5, v0}, Lcom/android/okhttp/Connection;->setSocket(Ljava/net/Socket;)V

    .line 700
    sget-boolean v5, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v5, :cond_1a

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "new input stream in twochunk main exception sec mainInput = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mainInput:Ljava/io/InputStream;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 705
    .end local v20    # "preMainSocketID":I
    .end local v23    # "s":Ljava/net/Socket;
    :cond_1a
    :goto_3
    monitor-exit p0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 706
    :try_start_9
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mStartReadTime:[J

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mSocket0:I

    aget-wide v6, v5, v6

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-nez v5, :cond_1b

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mStartReadTime:[J

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mSocket0:I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    aput-wide v8, v5, v6

    .line 707
    :cond_1b
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mainInput:Ljava/io/InputStream;

    move-object/from16 v0, p1

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-virtual {v5, v0, v1, v2}, Ljava/io/InputStream;->read([BII)I

    move-result v22

    .line 708
    .local v22, "readLen":I
    if-gez v22, :cond_1e

    .line 709
    new-instance v5, Ljava/io/IOException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "read return exception value "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v22

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_1

    .line 687
    .end local v22    # "readLen":I
    .restart local v20    # "preMainSocketID":I
    .restart local v23    # "s":Ljava/net/Socket;
    :cond_1c
    const/4 v5, 0x1

    :try_start_a
    move-object/from16 v0, p0

    iput v5, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mSocket0:I

    .line 688
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput v5, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mSocket1:I

    goto/16 :goto_2

    .line 703
    .end local v20    # "preMainSocketID":I
    .end local v23    # "s":Ljava/net/Socket;
    :cond_1d
    sget-boolean v5, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v5, :cond_1a

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    const-string v6, "childIS is created before this "

    invoke-virtual {v5, v6}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    goto :goto_3

    .line 711
    .restart local v22    # "readLen":I
    :cond_1e
    :try_start_b
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    if-nez v5, :cond_1f

    .line 712
    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mSocket0:I

    move/from16 v0, v22

    int-to-long v6, v0

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6, v7}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->incByte(IJ)V

    .line 713
    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mSocket0:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->setTime(I)V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_1

    :cond_1f
    move/from16 v21, v22

    .line 715
    goto/16 :goto_0

    .line 721
    .end local v22    # "readLen":I
    .restart local v18    # "ex":Ljava/io/IOException;
    :cond_20
    add-int/lit8 v26, v26, 0x1

    .line 722
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v28, v6, v24

    .line 723
    .local v28, "waitedTime":J
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mTimeForDownload:[J

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mSocket0:I

    aget-wide v6, v5, v6

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-lez v5, :cond_21

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    if-nez v5, :cond_21

    .line 724
    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mSocket0:I

    move-object/from16 v0, p0

    move-wide/from16 v1, v28

    invoke-virtual {v0, v5, v1, v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->decTime(IJ)V

    .line 725
    :cond_21
    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mTimeOut:I

    if-lez v5, :cond_23

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mTimeOut:I

    int-to-long v6, v5

    cmp-long v5, v28, v6

    if-lez v5, :cond_23

    .line 726
    sget-boolean v5, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v5, :cond_22

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "TwoChunk waited time "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, v28

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " time out "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget v7, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mTimeOut:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 727
    :cond_22
    throw v18

    .line 729
    :cond_23
    monitor-enter p0

    .line 731
    const-wide/16 v6, 0x1f4

    :try_start_c
    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v7}, Ljava/lang/Object;->wait(J)V
    :try_end_c
    .catch Ljava/lang/Throwable; {:try_start_c .. :try_end_c} :catch_2
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    .line 734
    :goto_4
    :try_start_d
    monitor-exit p0

    goto/16 :goto_1

    :catchall_2
    move-exception v5

    monitor-exit p0
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_2

    throw v5

    .line 737
    .end local v18    # "ex":Ljava/io/IOException;
    .end local v28    # "waitedTime":J
    :cond_24
    throw v17

    .line 733
    .restart local v18    # "ex":Ljava/io/IOException;
    .restart local v28    # "waitedTime":J
    :catch_2
    move-exception v5

    goto :goto_4
.end method


# virtual methods
.method public available()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 481
    iget-wide v0, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mRemainBytes:J

    iget-wide v2, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mBytesForTail:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 482
    iget-object v0, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mainInput:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->available()I

    move-result v0

    .line 486
    :goto_0
    return v0

    .line 485
    :cond_0
    iget-object v1, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mDBuf:Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;

    monitor-enter v1

    .line 486
    :try_start_0
    iget-object v0, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mDBuf:Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;

    invoke-virtual {v0}, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->getRestLength()J

    move-result-wide v2

    long-to-int v0, v2

    monitor-exit v1

    goto :goto_0

    .line 487
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected bTwoInfDownloading()Z
    .locals 1

    .prologue
    .line 320
    iget-object v0, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->bSecThreadExisted:Z

    .line 321
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    invoke-virtual {v0}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->bTwoInfDownloading()Z

    move-result v0

    goto :goto_0
.end method

.method public close()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 505
    sget-boolean v1, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "TwoChunkInputStream:close "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mDepth:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 506
    :cond_0
    iget-boolean v1, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->closed:Z

    if-eqz v1, :cond_2

    .line 529
    :cond_1
    :goto_0
    return-void

    .line 509
    :cond_2
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->closed:Z

    .line 511
    :try_start_0
    iget-object v1, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mDBuf:Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;

    if-eqz v1, :cond_3

    .line 512
    iget-object v2, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mDBuf:Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;

    monitor-enter v2
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 513
    :try_start_1
    iget-object v1, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mDBuf:Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;

    invoke-virtual {v1}, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->clearBuffer()V

    .line 514
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 516
    :cond_3
    :try_start_2
    iget-object v1, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    if-eqz v1, :cond_4

    .line 517
    iget-object v1, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    invoke-virtual {v1}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->close()V

    .line 519
    :cond_4
    iget-object v1, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mSecThread:Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;

    if-eqz v1, :cond_5

    .line 520
    iget-object v1, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mSecThread:Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;

    invoke-virtual {v1}, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->closeSocketAndStreams()V

    .line 522
    :cond_5
    iget-object v1, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mChildRAdownloadedFile:Ljava/io/RandomAccessFile;

    if-eqz v1, :cond_1

    .line 523
    iget-object v1, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mChildRAdownloadedFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 526
    :catch_0
    move-exception v0

    .line 527
    .local v0, "e":Ljava/lang/Throwable;
    sget-boolean v1, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    invoke-virtual {v1, v0}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 514
    .end local v0    # "e":Ljava/lang/Throwable;
    :catchall_0
    move-exception v1

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v1
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0
.end method

.method protected decTime(IJ)V
    .locals 0
    .param p1, "sid"    # I
    .param p2, "time"    # J

    .prologue
    .line 556
    return-void
.end method

.method protected getDownloadLen(I)J
    .locals 4
    .param p1, "sid"    # I

    .prologue
    .line 532
    iget-object v0, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mDataDownloaded:[J

    aget-wide v2, v0, p1

    iget-object v0, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    add-long/2addr v0, v2

    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    invoke-virtual {v0, p1}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->getDownloadLen(I)J

    move-result-wide v0

    goto :goto_0
.end method

.method protected getDownloadTime(I)J
    .locals 4
    .param p1, "sid"    # I

    .prologue
    .line 536
    iget-object v0, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mTimeForDownload:[J

    aget-wide v2, v0, p1

    iget-object v0, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    add-long/2addr v0, v2

    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    invoke-virtual {v0, p1}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->getDownloadTime(I)J

    move-result-wide v0

    goto :goto_0
.end method

.method public getNeedToExitSecThread()I
    .locals 1

    .prologue
    .line 354
    iget v0, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mKeepSecondChunk:I

    return v0
.end method

.method protected getSecChunkRange()[J
    .locals 10

    .prologue
    const/4 v4, 0x2

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 325
    new-array v1, v4, [J

    iget-wide v2, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mSecChunkStart:J

    aput-wide v2, v1, v9

    iget-wide v2, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mSecChunkEnd:J

    aput-wide v2, v1, v8

    .line 326
    .local v1, "ret":[J
    const/4 v0, 0x0

    .line 327
    .local v0, "childSec":[J
    iget-object v2, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    if-eqz v2, :cond_0

    .line 328
    iget-object v2, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    invoke-virtual {v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->getSecChunkRange()[J

    move-result-object v0

    .line 330
    :cond_0
    if-eqz v0, :cond_1

    array-length v2, v0

    if-ne v2, v4, :cond_1

    .line 331
    aget-wide v2, v0, v8

    aget-wide v4, v1, v9

    cmp-long v2, v2, v4

    if-gez v2, :cond_2

    aget-wide v2, v0, v8

    aget-wide v4, v0, v9

    sub-long/2addr v2, v4

    aget-wide v4, v1, v8

    aget-wide v6, v1, v9

    sub-long/2addr v4, v6

    cmp-long v2, v2, v4

    if-lez v2, :cond_2

    .line 332
    aget-wide v2, v0, v9

    aput-wide v2, v1, v9

    .line 333
    aget-wide v2, v0, v8

    aput-wide v2, v1, v8

    .line 346
    :cond_1
    :goto_0
    return-object v1

    .line 335
    :cond_2
    aget-wide v2, v0, v8

    aget-wide v4, v1, v9

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    aget-wide v2, v0, v9

    aget-wide v4, v1, v9

    cmp-long v2, v2, v4

    if-gez v2, :cond_3

    .line 336
    aget-wide v2, v0, v9

    aput-wide v2, v1, v9

    goto :goto_0

    .line 338
    :cond_3
    aget-wide v2, v0, v9

    aget-wide v4, v1, v8

    cmp-long v2, v2, v4

    if-nez v2, :cond_4

    aget-wide v2, v0, v8

    aget-wide v4, v1, v8

    cmp-long v2, v2, v4

    if-lez v2, :cond_4

    .line 339
    aget-wide v2, v0, v8

    aput-wide v2, v1, v8

    goto :goto_0

    .line 341
    :cond_4
    aget-wide v2, v0, v9

    aget-wide v4, v1, v8

    cmp-long v2, v2, v4

    if-lez v2, :cond_1

    iget-wide v2, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mStart:J

    iget-wide v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mLength:J

    add-long/2addr v2, v4

    iget-wide v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mRemainBytes:J

    sub-long/2addr v2, v4

    aget-wide v4, v1, v8

    cmp-long v2, v2, v4

    if-lez v2, :cond_1

    .line 342
    aget-wide v2, v0, v9

    aput-wide v2, v1, v9

    .line 343
    aget-wide v2, v0, v8

    aput-wide v2, v1, v8

    goto :goto_0
.end method

.method protected incByte(IJ)V
    .locals 6
    .param p1, "sid"    # I
    .param p2, "bytes"    # J

    .prologue
    .line 559
    sget-boolean v0, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v0, :cond_1

    .line 560
    const-wide/32 v0, 0x100000

    cmp-long v0, p2, v0

    if-gtz v0, :cond_0

    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-gtz v0, :cond_1

    .line 561
    :cond_0
    iget-object v0, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v1, Ljava/lang/Throwable;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "speed calc >> set data for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " from "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mDataDownloaded:[J

    aget-wide v4, v3, p1

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " with a inc "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/android/okhttp/internal/http/MultiratLog;->w(Ljava/lang/Throwable;)V

    .line 563
    :cond_1
    iget-object v0, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mDataDownloaded:[J

    aget-wide v2, v0, p1

    add-long/2addr v2, p2

    aput-wide v2, v0, p1

    .line 564
    return-void
.end method

.method protected isClosed()Z
    .locals 1

    .prologue
    .line 498
    iget-boolean v0, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->closed:Z

    return v0
.end method

.method protected isServerReject()Z
    .locals 1

    .prologue
    .line 492
    iget-boolean v0, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->bServerReject:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 494
    :goto_0
    return v0

    .line 493
    :cond_0
    iget-object v0, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 494
    :cond_1
    iget-object v0, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    invoke-virtual {v0}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->isServerReject()Z

    move-result v0

    goto :goto_0
.end method

.method public isSingleThreadRun()Z
    .locals 1

    .prologue
    .line 300
    iget-object v0, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mSecThread:Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public read([BII)I
    .locals 12
    .param p1, "buffer"    # [B
    .param p2, "offset"    # I
    .param p3, "count"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    const/4 v4, 0x0

    const-wide/16 v10, 0x0

    .line 371
    invoke-virtual {p0}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->checkNotClosed()V

    .line 372
    array-length v5, p1

    invoke-static {v5, p2, p3}, Lcom/android/okhttp/internal/http/MultiratUtil;->checkOffsetAndCount(III)V

    .line 373
    const/4 v0, 0x0

    .line 375
    .local v0, "readLen":I
    if-nez p3, :cond_1

    .line 376
    sget-boolean v5, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "read for 0 : buffer.length="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    array-length v7, p1

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", offset="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", count="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 477
    :cond_0
    :goto_0
    return v4

    .line 380
    :cond_1
    iget-wide v6, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mRemainBytes:J

    cmp-long v5, v6, v10

    if-nez v5, :cond_2

    .line 381
    sget-boolean v5, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    const-string v6, "finish to read size, no byte remained, return 0"

    invoke-virtual {v5, v6}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    goto :goto_0

    .line 384
    :cond_2
    iget-wide v6, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mRemainBytes:J

    cmp-long v5, v6, v10

    if-gez v5, :cond_3

    .line 385
    sget-boolean v5, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "finish to read size, remained bytes is negative: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-wide v8, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mRemainBytes:J

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    goto :goto_0

    .line 389
    :cond_3
    move v1, p3

    .line 390
    .local v1, "toReadCount":I
    int-to-long v4, v1

    iget-wide v6, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mRemainBytes:J

    cmp-long v4, v4, v6

    if-lez v4, :cond_5

    .line 391
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "toReadCount "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " > mReamainBytes "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mRemainBytes:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 392
    :cond_4
    iget-wide v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mRemainBytes:J

    long-to-int v1, v4

    .line 395
    :cond_5
    iget-boolean v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->bHasSetMainSocket:Z

    if-nez v4, :cond_7

    .line 396
    iget-object v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mainSocket:Ljava/net/Socket;

    if-eqz v4, :cond_6

    .line 397
    iget-object v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->connection:Lcom/android/okhttp/Connection;

    iget-object v5, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mainSocket:Ljava/net/Socket;

    invoke-virtual {v4, v5}, Lcom/android/okhttp/Connection;->setSocket(Ljava/net/Socket;)V

    .line 399
    :cond_6
    iput-boolean v8, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->bHasSetMainSocket:Z

    .line 402
    :cond_7
    iget-wide v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mBytesForTail:J

    cmp-long v4, v4, v10

    if-gtz v4, :cond_a

    .line 403
    invoke-direct {p0, p1, p2, v1}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->readForChunk0([BII)I

    move-result v0

    .line 459
    :cond_8
    :goto_1
    const/4 v4, -0x1

    if-ne v0, v4, :cond_17

    .line 460
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_9

    iget-object v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "unexpected end of stream, but return 0, mRemainBytes="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mRemainBytes:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", mBytesForTail="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mBytesForTail:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    :cond_9
    :goto_2
    move v4, v0

    .line 477
    goto/16 :goto_0

    .line 406
    :cond_a
    iget-wide v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mRemainBytes:J

    iget-wide v6, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mBytesForTail:J

    cmp-long v4, v4, v6

    if-lez v4, :cond_b

    .line 407
    int-to-long v4, v1

    iget-wide v6, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mRemainBytes:J

    iget-wide v8, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mBytesForTail:J

    sub-long/2addr v6, v8

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    long-to-int v4, v4

    invoke-direct {p0, p1, p2, v4}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->readForChunk0([BII)I

    move-result v0

    goto :goto_1

    .line 410
    :cond_b
    iget-wide v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mRemainBytes:J

    iget-wide v6, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mBytesForTail:J

    cmp-long v4, v4, v6

    if-nez v4, :cond_e

    .line 411
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_c

    iget-object v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "resource check: finish reading child chunk "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mStart:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "-"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mStart:J

    iget-wide v8, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mLength:J

    add-long/2addr v6, v8

    iget-wide v8, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mRemainBytes:J

    sub-long/2addr v6, v8

    const-wide/16 v8, 0x1

    sub-long/2addr v6, v8

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " with length "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mLength:J

    iget-wide v8, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mRemainBytes:J

    sub-long/2addr v6, v8

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " from child socket, "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " now mRemainBytes==mBytesForTail=="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mRemainBytes:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 414
    :cond_c
    iget-boolean v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->bFailedInSecChunk:Z

    if-eqz v4, :cond_10

    .line 415
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_d

    iget-object v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    const-string v5, "bFailedInSecChunk is true, not to use second chunk, and never re-divide"

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 416
    :cond_d
    iput-wide v10, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mBytesForTail:J

    .line 417
    invoke-direct {p0, p1, p2, v1}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->readForChunk0([BII)I

    move-result v0

    .line 429
    :cond_e
    :goto_3
    iget-wide v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mBytesForTail:J

    cmp-long v4, v4, v10

    if-lez v4, :cond_8

    .line 430
    iget-object v5, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mDBuf:Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;

    monitor-enter v5

    .line 431
    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 432
    .local v2, "startTime":J
    :goto_4
    iget-object v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->parentIS:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-boolean v4, v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bFinished:Z

    if-nez v4, :cond_16

    .line 433
    iget-object v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mDBuf:Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;

    invoke-virtual {v4, p1, p2, v1}, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->read([BII)I

    move-result v0

    .line 434
    if-nez v0, :cond_16

    .line 435
    iget-boolean v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->bFailedInSecChunk:Z

    if-eqz v4, :cond_13

    .line 436
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_f

    iget-object v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    const-string v6, "exception in reading secChunk"

    invoke-virtual {v4, v6}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 437
    :cond_f
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->bServerReject:Z

    .line 438
    new-instance v4, Ljava/io/IOException;

    const-string v6, "exception in reading secChunk"

    invoke-direct {v4, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 455
    .end local v2    # "startTime":J
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .line 420
    :cond_10
    monitor-enter p0

    .line 421
    :try_start_1
    iget-boolean v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->bSecThreadStarted:Z

    if-eqz v4, :cond_11

    iget-boolean v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->bSecThreadExisted:Z

    if-eqz v4, :cond_11

    iget-object v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mSecThread:Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;

    if-eqz v4, :cond_11

    .line 422
    iget-object v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mSecThread:Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;

    iget v5, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mSocket0:I

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->startTryBoth(I)V

    .line 423
    :cond_11
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 424
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_12

    iget-object v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "try to close main input stream in depth "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mDepth:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", input is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mainInput:Ljava/io/InputStream;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 425
    :cond_12
    invoke-direct {p0}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->closeMainSocket()V

    .line 426
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_e

    iget-object v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "end to close main stream in depth "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mDepth:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 423
    :catchall_1
    move-exception v4

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v4

    .line 440
    .restart local v2    # "startTime":J
    :cond_13
    :try_start_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long/2addr v6, v2

    const-wide/16 v8, 0x7d0

    cmp-long v4, v6, v8

    if-ltz v4, :cond_15

    .line 441
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_14

    iget-object v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "wait for long time without read data, sec thread = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mSecThread:Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 442
    :cond_14
    iget-object v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mSecThread:Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;

    if-eqz v4, :cond_15

    .line 443
    iget-object v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mSecThread:Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;

    const/4 v6, 0x1

    invoke-virtual {v4, v6}, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->createTwoChunkInSec(Z)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 447
    :cond_15
    :try_start_4
    iget-object v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mDBuf:Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;

    const-wide/16 v6, 0x1f4

    invoke-virtual {v4, v6, v7}, Ljava/lang/Object;->wait(J)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_4

    .line 449
    :catch_0
    move-exception v4

    goto/16 :goto_4

    .line 455
    :cond_16
    :try_start_5
    monitor-exit v5
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_1

    .line 463
    .end local v2    # "startTime":J
    :cond_17
    array-length v4, p1

    if-le v0, v4, :cond_19

    .line 464
    iget-wide v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mRemainBytes:J

    array-length v6, p1

    sub-int v6, v0, v6

    int-to-long v6, v6

    sub-long/2addr v4, v6

    iput-wide v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mRemainBytes:J

    .line 465
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_18

    .line 471
    :cond_18
    :goto_5
    iget-wide v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mRemainBytes:J

    cmp-long v4, v4, v10

    if-gtz v4, :cond_9

    .line 472
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_9

    iget-object v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "no byte remained, start to end input "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mRemainBytes:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 468
    :cond_19
    iget-wide v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mRemainBytes:J

    int-to-long v6, v0

    sub-long/2addr v4, v6

    iput-wide v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mRemainBytes:J

    goto :goto_5
.end method

.method protected setTime(I)V
    .locals 8
    .param p1, "sid"    # I

    .prologue
    .line 540
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_1

    .line 541
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mStartReadTime:[J

    aget-wide v4, v4, p1

    sub-long/2addr v2, v4

    iget-object v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mTimeForDownload:[J

    aget-wide v4, v4, p1

    sub-long v0, v2, v4

    .line 542
    .local v0, "timeDiff":J
    const-wide/16 v2, 0x1388

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gez v2, :cond_1

    .line 543
    :cond_0
    iget-object v2, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v3, Ljava/lang/Throwable;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "speed calc >> set time for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " from "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mTimeForDownload:[J

    aget-wide v6, v5, p1

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " with a inc "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Lcom/android/okhttp/internal/http/MultiratLog;->w(Ljava/lang/Throwable;)V

    .line 546
    .end local v0    # "timeDiff":J
    :cond_1
    iget-object v2, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mTimeForDownload:[J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-object v3, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mStartReadTime:[J

    aget-wide v6, v3, p1

    sub-long/2addr v4, v6

    aput-wide v4, v2, p1

    .line 547
    return-void
.end method

.method public startRun(Ljava/net/Socket;)V
    .locals 4
    .param p1, "ms"    # Ljava/net/Socket;

    .prologue
    .line 304
    monitor-enter p0

    .line 305
    :try_start_0
    iget-object v1, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mSecThread:Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;

    if-eqz v1, :cond_2

    .line 306
    sget-boolean v1, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    const-string v2, "start run sec chunk"

    invoke-virtual {v1, v2}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 307
    :cond_0
    new-instance v0, Ljava/lang/Thread;

    iget-object v1, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mSecThread:Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":SecChunk_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mDepth:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 308
    .local v0, "t":Ljava/lang/Thread;
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->bSecThreadExisted:Z

    .line 309
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->bSecThreadStarted:Z

    .line 310
    iput-object p1, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mainSocket:Ljava/net/Socket;

    .line 311
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 316
    .end local v0    # "t":Ljava/lang/Thread;
    :cond_1
    :goto_0
    monitor-exit p0

    .line 317
    return-void

    .line 314
    :cond_2
    sget-boolean v1, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    const-string v2, "sec chunk is not created"

    invoke-virtual {v1, v2}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    goto :goto_0

    .line 316
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
