.class public Lcom/android/okhttp/internal/http/HttpEngine;
.super Ljava/lang/Object;
.source "HttpEngine.java"


# static fields
.field protected static HTTPTIMER:Z = false

.field protected static final INF_MOBILE:I = 0x1

.field protected static final INF_NUMBER:I = 0x2

.field protected static final INF_WIFI:I = 0x0

.field public static MAX_REDIRECTS:I = 0x0

.field private static SB_INITIAGED:Z = false

.field public static SMARTBONDING_ENABLED:Z = false

.field protected static final SOCK_TIME_OUT:I = 0x1388

.field public static final SUPPORT_HTTPS:Z = true

.field private static mainInterfaceID:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static originalRequestUri:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/android/okhttp/Request;",
            ">;"
        }
    .end annotation
.end field

.field private static originalSSLList:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/android/okhttp/Connection;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final bufferRequestBody:Z

.field private bufferedRequestBody:Lcom/android/okio/BufferedSink;

.field private cacheResponse:Lcom/android/okhttp/Response;

.field final client:Lcom/android/okhttp/OkHttpClient;

.field private connection:Lcom/android/okhttp/Connection;

.field protected logger:Lcom/android/okhttp/internal/http/MultiratLog;

.field protected mOriRequestHeader:Lcom/android/okhttp/Request;

.field private mThreadID:J

.field protected mainResponseTime:J

.field private networkRequest:Lcom/android/okhttp/Request;

.field private networkResponse:Lcom/android/okhttp/Response;

.field private final priorResponse:Lcom/android/okhttp/Response;

.field private requestBodyOut:Lcom/android/okio/Sink;

.field private responseBody:Lcom/android/okio/Source;

.field private responseBodyBytes:Ljava/io/InputStream;

.field private responseSource:Lcom/android/okhttp/ResponseSource;

.field private responseTransferSource:Lcom/android/okio/Source;

.field private route:Lcom/android/okhttp/Route;

.field private routeSelector:Lcom/android/okhttp/internal/http/RouteSelector;

.field sentRequestMillis:J

.field private storeRequest:Ljava/net/CacheRequest;

.field private transparentGzip:Z

.field private transport:Lcom/android/okhttp/internal/http/Transport;

.field private final userRequest:Lcom/android/okhttp/Request;

.field private userResponse:Lcom/android/okhttp/Response;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 160
    sput-boolean v0, Lcom/android/okhttp/internal/http/HttpEngine;->HTTPTIMER:Z

    .line 162
    sput-boolean v0, Lcom/android/okhttp/internal/http/HttpEngine;->SMARTBONDING_ENABLED:Z

    .line 163
    sput-boolean v0, Lcom/android/okhttp/internal/http/HttpEngine;->SB_INITIAGED:Z

    .line 167
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/okhttp/internal/http/HttpEngine;->originalRequestUri:Ljava/util/HashMap;

    .line 168
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/okhttp/internal/http/HttpEngine;->mainInterfaceID:Ljava/util/HashMap;

    .line 169
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/okhttp/internal/http/HttpEngine;->originalSSLList:Ljava/util/HashMap;

    .line 170
    const/4 v0, 0x5

    sput v0, Lcom/android/okhttp/internal/http/HttpEngine;->MAX_REDIRECTS:I

    return-void
.end method

.method public constructor <init>(Lcom/android/okhttp/OkHttpClient;Lcom/android/okhttp/Request;ZLcom/android/okhttp/Connection;Lcom/android/okhttp/internal/http/RouteSelector;Lcom/android/okhttp/internal/http/RetryableSink;Lcom/android/okhttp/Response;)V
    .locals 8
    .param p1, "client"    # Lcom/android/okhttp/OkHttpClient;
    .param p2, "request"    # Lcom/android/okhttp/Request;
    .param p3, "bufferRequestBody"    # Z
    .param p4, "connection"    # Lcom/android/okhttp/Connection;
    .param p5, "routeSelector"    # Lcom/android/okhttp/internal/http/RouteSelector;
    .param p6, "requestBodyOut"    # Lcom/android/okhttp/internal/http/RetryableSink;
    .param p7, "priorResponse"    # Lcom/android/okhttp/Response;

    .prologue
    const/4 v4, 0x0

    .line 190
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/android/okhttp/internal/http/HttpEngine;->sentRequestMillis:J

    .line 165
    iput-object v4, p0, Lcom/android/okhttp/internal/http/HttpEngine;->mOriRequestHeader:Lcom/android/okhttp/Request;

    .line 191
    iput-object p1, p0, Lcom/android/okhttp/internal/http/HttpEngine;->client:Lcom/android/okhttp/OkHttpClient;

    .line 192
    iput-object p2, p0, Lcom/android/okhttp/internal/http/HttpEngine;->userRequest:Lcom/android/okhttp/Request;

    .line 193
    iput-boolean p3, p0, Lcom/android/okhttp/internal/http/HttpEngine;->bufferRequestBody:Z

    .line 194
    iput-object p4, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    .line 195
    iput-object p5, p0, Lcom/android/okhttp/internal/http/HttpEngine;->routeSelector:Lcom/android/okhttp/internal/http/RouteSelector;

    .line 196
    iput-object p6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->requestBodyOut:Lcom/android/okio/Sink;

    .line 197
    iput-object p7, p0, Lcom/android/okhttp/internal/http/HttpEngine;->priorResponse:Lcom/android/okhttp/Response;

    .line 199
    if-eqz p4, :cond_7

    .line 200
    invoke-virtual {p4, p0}, Lcom/android/okhttp/Connection;->setOwner(Ljava/lang/Object;)V

    .line 201
    invoke-virtual {p4}, Lcom/android/okhttp/Connection;->getRoute()Lcom/android/okhttp/Route;

    move-result-object v2

    iput-object v2, p0, Lcom/android/okhttp/internal/http/HttpEngine;->route:Lcom/android/okhttp/Route;

    .line 207
    :goto_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getId()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/android/okhttp/internal/http/HttpEngine;->mThreadID:J

    .line 208
    new-instance v2, Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Thread-"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p0, Lcom/android/okhttp/internal/http/HttpEngine;->mThreadID:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "-"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/android/okhttp/internal/http/MultiratLog;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/android/okhttp/internal/http/HttpEngine;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    .line 209
    sget-boolean v2, Lcom/android/okhttp/internal/http/HttpEngine;->SB_INITIAGED:Z

    if-nez v2, :cond_1

    .line 211
    :try_start_0
    invoke-static {}, Lcom/android/okhttp/internal/http/SBServiceAPI;->isSBSettingEnabled()Z

    move-result v2

    sput-boolean v2, Lcom/android/okhttp/internal/http/HttpEngine;->SMARTBONDING_ENABLED:Z

    .line 212
    invoke-static {}, Lcom/android/okhttp/internal/http/SBServiceAPI;->isShipBuild()Z

    move-result v0

    .line 213
    .local v0, "SHIP_BUILD":Z
    const/4 v2, 0x0

    sput-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->HTTPFILELOG:Z

    .line 214
    const/4 v2, 0x0

    sput-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    .line 215
    sget-boolean v2, Lcom/android/okhttp/internal/http/HttpEngine;->SMARTBONDING_ENABLED:Z

    if-eqz v2, :cond_0

    .line 216
    if-eqz v0, :cond_8

    .line 217
    const/4 v2, 0x0

    sput-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->HTTPFILELOG:Z

    .line 218
    invoke-static {}, Lcom/android/okhttp/internal/http/SBServiceAPI;->getHttpLogEnabled()Z

    move-result v2

    sput-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    .line 225
    :cond_0
    :goto_1
    iget-object v2, p0, Lcom/android/okhttp/internal/http/HttpEngine;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SmartBonding Enabling is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-boolean v4, Lcom/android/okhttp/internal/http/HttpEngine;->SMARTBONDING_ENABLED:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", SHIP_BUILD is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", log to file is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->HTTPFILELOG:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", DBG is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 226
    const/4 v2, 0x1

    sput-boolean v2, Lcom/android/okhttp/internal/http/HttpEngine;->SB_INITIAGED:Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 233
    .end local v0    # "SHIP_BUILD":Z
    :cond_1
    :goto_2
    sget-boolean v2, Lcom/android/okhttp/internal/http/HttpEngine;->SMARTBONDING_ENABLED:Z

    if-eqz v2, :cond_6

    .line 235
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_2

    .line 236
    iget-object v2, p0, Lcom/android/okhttp/internal/http/HttpEngine;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Request header host for threadID:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p0, Lcom/android/okhttp/internal/http/HttpEngine;->mThreadID:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/okhttp/internal/http/HttpEngine;->userRequest:Lcom/android/okhttp/Request;

    invoke-virtual {v4}, Lcom/android/okhttp/Request;->urlString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 238
    :cond_2
    sget-object v3, Lcom/android/okhttp/internal/http/HttpEngine;->originalRequestUri:Ljava/util/HashMap;

    monitor-enter v3

    .line 239
    :try_start_1
    sget-object v2, Lcom/android/okhttp/internal/http/HttpEngine;->originalRequestUri:Ljava/util/HashMap;

    iget-wide v4, p0, Lcom/android/okhttp/internal/http/HttpEngine;->mThreadID:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_4

    .line 240
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_3

    .line 241
    iget-object v2, p0, Lcom/android/okhttp/internal/http/HttpEngine;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "add originalRequestHeader: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->mThreadID:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " - "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/okhttp/internal/http/HttpEngine;->userRequest:Lcom/android/okhttp/Request;

    invoke-virtual {v5}, Lcom/android/okhttp/Request;->urlString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 243
    :cond_3
    sget-object v2, Lcom/android/okhttp/internal/http/HttpEngine;->originalRequestUri:Ljava/util/HashMap;

    iget-wide v4, p0, Lcom/android/okhttp/internal/http/HttpEngine;->mThreadID:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    iget-object v5, p0, Lcom/android/okhttp/internal/http/HttpEngine;->userRequest:Lcom/android/okhttp/Request;

    invoke-virtual {v2, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 245
    :cond_4
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/android/okhttp/internal/http/HttpEngine;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "now we have ori request number "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/android/okhttp/internal/http/HttpEngine;->originalRequestUri:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " in "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/android/okhttp/internal/http/HttpEngine;->originalRequestUri:Ljava/util/HashMap;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " of object "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/android/okhttp/internal/http/HttpEngine;->originalRequestUri:Ljava/util/HashMap;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 246
    :cond_5
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 249
    :cond_6
    return-void

    .line 203
    :cond_7
    iput-object v4, p0, Lcom/android/okhttp/internal/http/HttpEngine;->route:Lcom/android/okhttp/Route;

    goto/16 :goto_0

    .line 221
    .restart local v0    # "SHIP_BUILD":Z
    :cond_8
    :try_start_2
    invoke-static {}, Lcom/android/okhttp/internal/http/SBServiceAPI;->getHttpLogEnabled()Z

    move-result v2

    sput-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->HTTPFILELOG:Z

    .line 222
    const/4 v2, 0x1

    sput-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_1

    .line 228
    .end local v0    # "SHIP_BUILD":Z
    :catch_0
    move-exception v1

    .line 229
    .local v1, "e":Ljava/lang/Throwable;
    iget-object v2, p0, Lcom/android/okhttp/internal/http/HttpEngine;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    invoke-virtual {v2, v1}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V

    goto/16 :goto_2

    .line 246
    .end local v1    # "e":Ljava/lang/Throwable;
    :catchall_0
    move-exception v2

    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v2
.end method

.method private static combine(Lcom/android/okhttp/Headers;Lcom/android/okhttp/Headers;)Lcom/android/okhttp/Headers;
    .locals 5
    .param p0, "cachedHeaders"    # Lcom/android/okhttp/Headers;
    .param p1, "networkHeaders"    # Lcom/android/okhttp/Headers;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 892
    new-instance v2, Lcom/android/okhttp/Headers$Builder;

    invoke-direct {v2}, Lcom/android/okhttp/Headers$Builder;-><init>()V

    .line 894
    .local v2, "result":Lcom/android/okhttp/Headers$Builder;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/android/okhttp/Headers;->size()I

    move-result v4

    if-ge v1, v4, :cond_3

    .line 895
    invoke-virtual {p0, v1}, Lcom/android/okhttp/Headers;->name(I)Ljava/lang/String;

    move-result-object v0

    .line 896
    .local v0, "fieldName":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/android/okhttp/Headers;->value(I)Ljava/lang/String;

    move-result-object v3

    .line 897
    .local v3, "value":Ljava/lang/String;
    const-string v4, "Warning"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "1"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 894
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 900
    :cond_1
    invoke-static {v0}, Lcom/android/okhttp/internal/http/HttpEngine;->isEndToEnd(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {p1, v0}, Lcom/android/okhttp/Headers;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_0

    .line 901
    :cond_2
    invoke-virtual {v2, v0, v3}, Lcom/android/okhttp/Headers$Builder;->add(Ljava/lang/String;Ljava/lang/String;)Lcom/android/okhttp/Headers$Builder;

    goto :goto_1

    .line 905
    .end local v0    # "fieldName":Ljava/lang/String;
    .end local v3    # "value":Ljava/lang/String;
    :cond_3
    const/4 v1, 0x0

    :goto_2
    invoke-virtual {p1}, Lcom/android/okhttp/Headers;->size()I

    move-result v4

    if-ge v1, v4, :cond_5

    .line 906
    invoke-virtual {p1, v1}, Lcom/android/okhttp/Headers;->name(I)Ljava/lang/String;

    move-result-object v0

    .line 907
    .restart local v0    # "fieldName":Ljava/lang/String;
    invoke-static {v0}, Lcom/android/okhttp/internal/http/HttpEngine;->isEndToEnd(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 908
    invoke-virtual {p1, v1}, Lcom/android/okhttp/Headers;->value(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v0, v4}, Lcom/android/okhttp/Headers$Builder;->add(Ljava/lang/String;Ljava/lang/String;)Lcom/android/okhttp/Headers$Builder;

    .line 905
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 912
    .end local v0    # "fieldName":Ljava/lang/String;
    :cond_5
    invoke-virtual {v2}, Lcom/android/okhttp/Headers$Builder;->build()Lcom/android/okhttp/Headers;

    move-result-object v4

    return-object v4
.end method

.method private connect(Lcom/android/okhttp/Request;)V
    .locals 15
    .param p1, "request"    # Lcom/android/okhttp/Request;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 343
    sget-boolean v4, Lcom/android/okhttp/internal/http/HttpEngine;->SMARTBONDING_ENABLED:Z

    if-eqz v4, :cond_0

    .line 344
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/okhttp/internal/http/HttpEngine;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "connect(), current connection is "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v8, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, " for engine "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 347
    :cond_0
    iget-object v4, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    if-eqz v4, :cond_1

    new-instance v4, Ljava/lang/IllegalStateException;

    invoke-direct {v4}, Ljava/lang/IllegalStateException;-><init>()V

    throw v4

    .line 349
    :cond_1
    iget-object v4, p0, Lcom/android/okhttp/internal/http/HttpEngine;->routeSelector:Lcom/android/okhttp/internal/http/RouteSelector;

    if-nez v4, :cond_5

    .line 350
    invoke-virtual/range {p1 .. p1}, Lcom/android/okhttp/Request;->url()Ljava/net/URL;

    move-result-object v4

    invoke-virtual {v4}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v3

    .line 351
    .local v3, "uriHost":Ljava/lang/String;
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_3

    .line 352
    :cond_2
    new-instance v4, Ljava/net/UnknownHostException;

    invoke-virtual/range {p1 .. p1}, Lcom/android/okhttp/Request;->url()Ljava/net/URL;

    move-result-object v5

    invoke-virtual {v5}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/net/UnknownHostException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 354
    :cond_3
    const/4 v6, 0x0

    .line 355
    .local v6, "sslSocketFactory":Ljavax/net/ssl/SSLSocketFactory;
    const/4 v7, 0x0

    .line 356
    .local v7, "hostnameVerifier":Ljavax/net/ssl/HostnameVerifier;
    invoke-virtual/range {p1 .. p1}, Lcom/android/okhttp/Request;->isHttps()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 357
    iget-object v4, p0, Lcom/android/okhttp/internal/http/HttpEngine;->client:Lcom/android/okhttp/OkHttpClient;

    invoke-virtual {v4}, Lcom/android/okhttp/OkHttpClient;->getSslSocketFactory()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v6

    .line 358
    iget-object v4, p0, Lcom/android/okhttp/internal/http/HttpEngine;->client:Lcom/android/okhttp/OkHttpClient;

    invoke-virtual {v4}, Lcom/android/okhttp/OkHttpClient;->getHostnameVerifier()Ljavax/net/ssl/HostnameVerifier;

    move-result-object v7

    .line 360
    :cond_4
    new-instance v2, Lcom/android/okhttp/Address;

    invoke-virtual/range {p1 .. p1}, Lcom/android/okhttp/Request;->url()Ljava/net/URL;

    move-result-object v4

    invoke-static {v4}, Lcom/android/okhttp/internal/Util;->getEffectivePort(Ljava/net/URL;)I

    move-result v4

    iget-object v5, p0, Lcom/android/okhttp/internal/http/HttpEngine;->client:Lcom/android/okhttp/OkHttpClient;

    invoke-virtual {v5}, Lcom/android/okhttp/OkHttpClient;->getSocketFactory()Ljavax/net/SocketFactory;

    move-result-object v5

    iget-object v8, p0, Lcom/android/okhttp/internal/http/HttpEngine;->client:Lcom/android/okhttp/OkHttpClient;

    invoke-virtual {v8}, Lcom/android/okhttp/OkHttpClient;->getAuthenticator()Lcom/android/okhttp/OkAuthenticator;

    move-result-object v8

    iget-object v9, p0, Lcom/android/okhttp/internal/http/HttpEngine;->client:Lcom/android/okhttp/OkHttpClient;

    invoke-virtual {v9}, Lcom/android/okhttp/OkHttpClient;->getProxy()Ljava/net/Proxy;

    move-result-object v9

    iget-object v10, p0, Lcom/android/okhttp/internal/http/HttpEngine;->client:Lcom/android/okhttp/OkHttpClient;

    invoke-virtual {v10}, Lcom/android/okhttp/OkHttpClient;->getProtocols()Ljava/util/List;

    move-result-object v10

    invoke-direct/range {v2 .. v10}, Lcom/android/okhttp/Address;-><init>(Ljava/lang/String;ILjavax/net/SocketFactory;Ljavax/net/ssl/SSLSocketFactory;Ljavax/net/ssl/HostnameVerifier;Lcom/android/okhttp/OkAuthenticator;Ljava/net/Proxy;Ljava/util/List;)V

    .line 363
    .local v2, "address":Lcom/android/okhttp/Address;
    new-instance v8, Lcom/android/okhttp/internal/http/RouteSelector;

    invoke-virtual/range {p1 .. p1}, Lcom/android/okhttp/Request;->uri()Ljava/net/URI;

    move-result-object v10

    iget-object v4, p0, Lcom/android/okhttp/internal/http/HttpEngine;->client:Lcom/android/okhttp/OkHttpClient;

    invoke-virtual {v4}, Lcom/android/okhttp/OkHttpClient;->getProxySelector()Ljava/net/ProxySelector;

    move-result-object v11

    iget-object v4, p0, Lcom/android/okhttp/internal/http/HttpEngine;->client:Lcom/android/okhttp/OkHttpClient;

    invoke-virtual {v4}, Lcom/android/okhttp/OkHttpClient;->getConnectionPool()Lcom/android/okhttp/ConnectionPool;

    move-result-object v12

    iget-object v4, p0, Lcom/android/okhttp/internal/http/HttpEngine;->client:Lcom/android/okhttp/OkHttpClient;

    invoke-virtual {v4}, Lcom/android/okhttp/OkHttpClient;->getHostResolver()Lcom/android/okhttp/HostResolver;

    move-result-object v13

    iget-object v4, p0, Lcom/android/okhttp/internal/http/HttpEngine;->client:Lcom/android/okhttp/OkHttpClient;

    invoke-virtual {v4}, Lcom/android/okhttp/OkHttpClient;->getRoutesDatabase()Lcom/android/okhttp/RouteDatabase;

    move-result-object v14

    move-object v9, v2

    invoke-direct/range {v8 .. v14}, Lcom/android/okhttp/internal/http/RouteSelector;-><init>(Lcom/android/okhttp/Address;Ljava/net/URI;Ljava/net/ProxySelector;Lcom/android/okhttp/ConnectionPool;Lcom/android/okhttp/HostResolver;Lcom/android/okhttp/RouteDatabase;)V

    iput-object v8, p0, Lcom/android/okhttp/internal/http/HttpEngine;->routeSelector:Lcom/android/okhttp/internal/http/RouteSelector;

    .line 367
    .end local v2    # "address":Lcom/android/okhttp/Address;
    .end local v3    # "uriHost":Ljava/lang/String;
    .end local v6    # "sslSocketFactory":Ljavax/net/ssl/SSLSocketFactory;
    .end local v7    # "hostnameVerifier":Ljavax/net/ssl/HostnameVerifier;
    :cond_5
    iget-object v4, p0, Lcom/android/okhttp/internal/http/HttpEngine;->routeSelector:Lcom/android/okhttp/internal/http/RouteSelector;

    invoke-virtual/range {p1 .. p1}, Lcom/android/okhttp/Request;->method()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/RouteSelector;->next(Ljava/lang/String;)Lcom/android/okhttp/Connection;

    move-result-object v4

    iput-object v4, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    .line 369
    iget-object v4, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    iget-object v5, p0, Lcom/android/okhttp/internal/http/HttpEngine;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    invoke-virtual {v4, v5}, Lcom/android/okhttp/Connection;->setLogger(Lcom/android/okhttp/internal/http/MultiratLog;)V

    .line 371
    iget-object v4, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    invoke-virtual {v4, p0}, Lcom/android/okhttp/Connection;->setOwner(Ljava/lang/Object;)V

    .line 373
    iget-object v4, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    invoke-virtual {v4}, Lcom/android/okhttp/Connection;->isConnected()Z

    move-result v4

    if-nez v4, :cond_d

    .line 375
    sget-boolean v4, Lcom/android/okhttp/internal/http/HttpEngine;->SMARTBONDING_ENABLED:Z

    if-eqz v4, :cond_c

    .line 376
    const/4 v12, -0x1

    .line 377
    .local v12, "netType":I
    const/4 v13, 0x0

    .line 378
    .local v13, "oriCon":Lcom/android/okhttp/Connection;
    sget-object v5, Lcom/android/okhttp/internal/http/HttpEngine;->mainInterfaceID:Ljava/util/HashMap;

    monitor-enter v5

    .line 379
    :try_start_0
    sget-object v4, Lcom/android/okhttp/internal/http/HttpEngine;->mainInterfaceID:Ljava/util/HashMap;

    iget-wide v8, p0, Lcom/android/okhttp/internal/http/HttpEngine;->mThreadID:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_6

    .line 380
    sget-object v4, Lcom/android/okhttp/internal/http/HttpEngine;->mainInterfaceID:Ljava/util/HashMap;

    iget-wide v8, p0, Lcom/android/okhttp/internal/http/HttpEngine;->mThreadID:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v12

    .line 381
    sget-object v4, Lcom/android/okhttp/internal/http/HttpEngine;->originalSSLList:Ljava/util/HashMap;

    iget-wide v8, p0, Lcom/android/okhttp/internal/http/HttpEngine;->mThreadID:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Lcom/android/okhttp/Connection;

    move-object v13, v0

    .line 383
    :cond_6
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_7

    iget-object v4, p0, Lcom/android/okhttp/internal/http/HttpEngine;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "openSocketConnection() netType = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", now mainInfID map size is "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Lcom/android/okhttp/internal/http/HttpEngine;->mainInterfaceID:Ljava/util/HashMap;

    invoke-virtual {v9}, Ljava/util/HashMap;->size()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 384
    :cond_7
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 385
    const/4 v4, -0x1

    if-ne v12, v4, :cond_b

    .line 386
    iget-object v4, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    iget-object v5, p0, Lcom/android/okhttp/internal/http/HttpEngine;->client:Lcom/android/okhttp/OkHttpClient;

    invoke-virtual {v5}, Lcom/android/okhttp/OkHttpClient;->getConnectTimeout()I

    move-result v5

    iget-object v8, p0, Lcom/android/okhttp/internal/http/HttpEngine;->client:Lcom/android/okhttp/OkHttpClient;

    invoke-virtual {v8}, Lcom/android/okhttp/OkHttpClient;->getReadTimeout()I

    move-result v8

    invoke-direct {p0}, Lcom/android/okhttp/internal/http/HttpEngine;->getTunnelConfig()Lcom/android/okhttp/TunnelRequest;

    move-result-object v9

    invoke-virtual {v4, v5, v8, v9}, Lcom/android/okhttp/Connection;->connect(IILcom/android/okhttp/TunnelRequest;)V

    .line 395
    .end local v12    # "netType":I
    .end local v13    # "oriCon":Lcom/android/okhttp/Connection;
    :goto_0
    iget-object v4, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    invoke-virtual {v4}, Lcom/android/okhttp/Connection;->isSpdy()Z

    move-result v4

    if-eqz v4, :cond_8

    iget-object v4, p0, Lcom/android/okhttp/internal/http/HttpEngine;->client:Lcom/android/okhttp/OkHttpClient;

    invoke-virtual {v4}, Lcom/android/okhttp/OkHttpClient;->getConnectionPool()Lcom/android/okhttp/ConnectionPool;

    move-result-object v4

    iget-object v5, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    invoke-virtual {v4, v5}, Lcom/android/okhttp/ConnectionPool;->share(Lcom/android/okhttp/Connection;)V

    .line 397
    :cond_8
    iget-object v4, p0, Lcom/android/okhttp/internal/http/HttpEngine;->client:Lcom/android/okhttp/OkHttpClient;

    invoke-virtual {v4}, Lcom/android/okhttp/OkHttpClient;->getRoutesDatabase()Lcom/android/okhttp/RouteDatabase;

    move-result-object v4

    if-eqz v4, :cond_9

    iget-object v4, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    invoke-virtual {v4}, Lcom/android/okhttp/Connection;->getRoute()Lcom/android/okhttp/Route;

    move-result-object v4

    if-eqz v4, :cond_9

    .line 399
    iget-object v4, p0, Lcom/android/okhttp/internal/http/HttpEngine;->client:Lcom/android/okhttp/OkHttpClient;

    invoke-virtual {v4}, Lcom/android/okhttp/OkHttpClient;->getRoutesDatabase()Lcom/android/okhttp/RouteDatabase;

    move-result-object v4

    iget-object v5, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    invoke-virtual {v5}, Lcom/android/okhttp/Connection;->getRoute()Lcom/android/okhttp/Route;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/RouteDatabase;->connected(Lcom/android/okhttp/Route;)V

    .line 405
    :cond_9
    :goto_1
    sget-boolean v4, Lcom/android/okhttp/internal/http/HttpEngine;->SMARTBONDING_ENABLED:Z

    if-eqz v4, :cond_a

    .line 406
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_a

    iget-object v4, p0, Lcom/android/okhttp/internal/http/HttpEngine;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "connect() done, current connection is "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v8, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 410
    :cond_a
    iget-object v4, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    invoke-virtual {v4}, Lcom/android/okhttp/Connection;->getRoute()Lcom/android/okhttp/Route;

    move-result-object v4

    iput-object v4, p0, Lcom/android/okhttp/internal/http/HttpEngine;->route:Lcom/android/okhttp/Route;

    .line 411
    return-void

    .line 384
    .restart local v12    # "netType":I
    .restart local v13    # "oriCon":Lcom/android/okhttp/Connection;
    :catchall_0
    move-exception v4

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4

    .line 388
    :cond_b
    iget-object v8, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    iget-object v4, p0, Lcom/android/okhttp/internal/http/HttpEngine;->client:Lcom/android/okhttp/OkHttpClient;

    invoke-virtual {v4}, Lcom/android/okhttp/OkHttpClient;->getConnectTimeout()I

    move-result v9

    iget-object v4, p0, Lcom/android/okhttp/internal/http/HttpEngine;->client:Lcom/android/okhttp/OkHttpClient;

    invoke-virtual {v4}, Lcom/android/okhttp/OkHttpClient;->getReadTimeout()I

    move-result v10

    invoke-direct {p0}, Lcom/android/okhttp/internal/http/HttpEngine;->getTunnelConfig()Lcom/android/okhttp/TunnelRequest;

    move-result-object v11

    invoke-virtual/range {v8 .. v13}, Lcom/android/okhttp/Connection;->connect(IILcom/android/okhttp/TunnelRequest;ILcom/android/okhttp/Connection;)V

    goto :goto_0

    .line 392
    .end local v12    # "netType":I
    .end local v13    # "oriCon":Lcom/android/okhttp/Connection;
    :cond_c
    iget-object v4, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    iget-object v5, p0, Lcom/android/okhttp/internal/http/HttpEngine;->client:Lcom/android/okhttp/OkHttpClient;

    invoke-virtual {v5}, Lcom/android/okhttp/OkHttpClient;->getConnectTimeout()I

    move-result v5

    iget-object v8, p0, Lcom/android/okhttp/internal/http/HttpEngine;->client:Lcom/android/okhttp/OkHttpClient;

    invoke-virtual {v8}, Lcom/android/okhttp/OkHttpClient;->getReadTimeout()I

    move-result v8

    invoke-direct {p0}, Lcom/android/okhttp/internal/http/HttpEngine;->getTunnelConfig()Lcom/android/okhttp/TunnelRequest;

    move-result-object v9

    invoke-virtual {v4, v5, v8, v9}, Lcom/android/okhttp/Connection;->connect(IILcom/android/okhttp/TunnelRequest;)V

    goto/16 :goto_0

    .line 400
    :cond_d
    iget-object v4, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    invoke-virtual {v4}, Lcom/android/okhttp/Connection;->isSpdy()Z

    move-result v4

    if-nez v4, :cond_9

    .line 401
    iget-object v4, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    iget-object v5, p0, Lcom/android/okhttp/internal/http/HttpEngine;->client:Lcom/android/okhttp/OkHttpClient;

    invoke-virtual {v5}, Lcom/android/okhttp/OkHttpClient;->getReadTimeout()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/Connection;->updateReadTimeout(I)V

    goto :goto_1
.end method

.method public static getDefaultUserAgent()Ljava/lang/String;
    .locals 3

    .prologue
    .line 712
    const-string v1, "http.agent"

    invoke-static {v1}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 713
    .local v0, "agent":Ljava/lang/String;
    if-eqz v0, :cond_0

    .end local v0    # "agent":Ljava/lang/String;
    :goto_0
    return-object v0

    .restart local v0    # "agent":Ljava/lang/String;
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Java"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "java.version"

    invoke-static {v2}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private getTunnelConfig()Lcom/android/okhttp/TunnelRequest;
    .locals 6

    .prologue
    .line 931
    iget-object v2, p0, Lcom/android/okhttp/internal/http/HttpEngine;->userRequest:Lcom/android/okhttp/Request;

    invoke-virtual {v2}, Lcom/android/okhttp/Request;->isHttps()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x0

    .line 937
    :goto_0
    return-object v2

    .line 933
    :cond_0
    iget-object v2, p0, Lcom/android/okhttp/internal/http/HttpEngine;->userRequest:Lcom/android/okhttp/Request;

    invoke-virtual {v2}, Lcom/android/okhttp/Request;->getUserAgent()Ljava/lang/String;

    move-result-object v1

    .line 934
    .local v1, "userAgent":Ljava/lang/String;
    if-nez v1, :cond_1

    invoke-static {}, Lcom/android/okhttp/internal/http/HttpEngine;->getDefaultUserAgent()Ljava/lang/String;

    move-result-object v1

    .line 936
    :cond_1
    iget-object v2, p0, Lcom/android/okhttp/internal/http/HttpEngine;->userRequest:Lcom/android/okhttp/Request;

    invoke-virtual {v2}, Lcom/android/okhttp/Request;->url()Ljava/net/URL;

    move-result-object v0

    .line 937
    .local v0, "url":Ljava/net/URL;
    new-instance v2, Lcom/android/okhttp/TunnelRequest;

    invoke-virtual {v0}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0}, Lcom/android/okhttp/internal/Util;->getEffectivePort(Ljava/net/URL;)I

    move-result v4

    iget-object v5, p0, Lcom/android/okhttp/internal/http/HttpEngine;->userRequest:Lcom/android/okhttp/Request;

    invoke-virtual {v5}, Lcom/android/okhttp/Request;->getProxyAuthorization()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v3, v4, v1, v5}, Lcom/android/okhttp/TunnelRequest;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static hostHeader(Ljava/net/URL;)Ljava/lang/String;
    .locals 2
    .param p0, "url"    # Ljava/net/URL;

    .prologue
    .line 717
    invoke-static {p0}, Lcom/android/okhttp/internal/Util;->getEffectivePort(Ljava/net/URL;)I

    move-result v0

    invoke-virtual {p0}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/okhttp/internal/Util;->getDefaultPort(Ljava/lang/String;)I

    move-result v1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/net/URL;->getPort()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private initContentStream(Lcom/android/okio/Source;)V
    .locals 3
    .param p1, "transferSource"    # Lcom/android/okio/Source;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 619
    iput-object p1, p0, Lcom/android/okhttp/internal/http/HttpEngine;->responseTransferSource:Lcom/android/okio/Source;

    .line 620
    iget-boolean v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->transparentGzip:Z

    if-eqz v0, :cond_1

    const-string v0, "gzip"

    iget-object v1, p0, Lcom/android/okhttp/internal/http/HttpEngine;->userResponse:Lcom/android/okhttp/Response;

    const-string v2, "Content-Encoding"

    invoke-virtual {v1, v2}, Lcom/android/okhttp/Response;->header(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 621
    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->userResponse:Lcom/android/okhttp/Response;

    invoke-virtual {v0}, Lcom/android/okhttp/Response;->newBuilder()Lcom/android/okhttp/Response$Builder;

    move-result-object v0

    const-string v1, "Content-Encoding"

    invoke-virtual {v0, v1}, Lcom/android/okhttp/Response$Builder;->removeHeader(Ljava/lang/String;)Lcom/android/okhttp/Response$Builder;

    move-result-object v0

    const-string v1, "Content-Length"

    invoke-virtual {v0, v1}, Lcom/android/okhttp/Response$Builder;->removeHeader(Ljava/lang/String;)Lcom/android/okhttp/Response$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/okhttp/Response$Builder;->build()Lcom/android/okhttp/Response;

    move-result-object v0

    iput-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->userResponse:Lcom/android/okhttp/Response;

    .line 625
    new-instance v0, Lcom/android/okio/GzipSource;

    invoke-direct {v0, p1}, Lcom/android/okio/GzipSource;-><init>(Lcom/android/okio/Source;)V

    iput-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->responseBody:Lcom/android/okio/Source;

    .line 630
    :goto_0
    sget-boolean v0, Lcom/android/okhttp/internal/http/HttpEngine;->SMARTBONDING_ENABLED:Z

    if-eqz v0, :cond_0

    .line 631
    sget-boolean v0, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "initContentStream(), responseBody="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/okhttp/internal/http/HttpEngine;->responseBody:Lcom/android/okio/Source;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 634
    :cond_0
    return-void

    .line 627
    :cond_1
    iput-object p1, p0, Lcom/android/okhttp/internal/http/HttpEngine;->responseBody:Lcom/android/okio/Source;

    goto :goto_0
.end method

.method private static isEndToEnd(Ljava/lang/String;)Z
    .locals 1
    .param p0, "fieldName"    # Ljava/lang/String;

    .prologue
    .line 920
    const-string v0, "Connection"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Keep-Alive"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Proxy-Authenticate"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Proxy-Authorization"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "TE"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Trailers"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Transfer-Encoding"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Upgrade"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isRecoverable(Ljava/io/IOException;)Z
    .locals 5
    .param p1, "e"    # Ljava/io/IOException;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 510
    instance-of v4, p1, Ljavax/net/ssl/SSLHandshakeException;

    if-eqz v4, :cond_0

    invoke-virtual {p1}, Ljava/io/IOException;->getCause()Ljava/lang/Throwable;

    move-result-object v4

    instance-of v4, v4, Ljava/security/cert/CertificateException;

    if-eqz v4, :cond_0

    move v1, v2

    .line 512
    .local v1, "sslFailure":Z
    :goto_0
    instance-of v0, p1, Ljava/net/ProtocolException;

    .line 513
    .local v0, "protocolFailure":Z
    if-nez v1, :cond_1

    if-nez v0, :cond_1

    :goto_1
    return v2

    .end local v0    # "protocolFailure":Z
    .end local v1    # "sslFailure":Z
    :cond_0
    move v1, v3

    .line 510
    goto :goto_0

    .restart local v0    # "protocolFailure":Z
    .restart local v1    # "sslFailure":Z
    :cond_1
    move v2, v3

    .line 513
    goto :goto_1
.end method

.method private maybeCache()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 525
    iget-object v1, p0, Lcom/android/okhttp/internal/http/HttpEngine;->client:Lcom/android/okhttp/OkHttpClient;

    invoke-virtual {v1}, Lcom/android/okhttp/OkHttpClient;->getOkResponseCache()Lcom/android/okhttp/OkResponseCache;

    move-result-object v0

    .line 526
    .local v0, "responseCache":Lcom/android/okhttp/OkResponseCache;
    if-nez v0, :cond_0

    .line 536
    :goto_0
    return-void

    .line 529
    :cond_0
    iget-object v1, p0, Lcom/android/okhttp/internal/http/HttpEngine;->userResponse:Lcom/android/okhttp/Response;

    iget-object v2, p0, Lcom/android/okhttp/internal/http/HttpEngine;->networkRequest:Lcom/android/okhttp/Request;

    invoke-static {v1, v2}, Lcom/android/okhttp/internal/http/CacheStrategy;->isCacheable(Lcom/android/okhttp/Response;Lcom/android/okhttp/Request;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 530
    iget-object v1, p0, Lcom/android/okhttp/internal/http/HttpEngine;->networkRequest:Lcom/android/okhttp/Request;

    invoke-interface {v0, v1}, Lcom/android/okhttp/OkResponseCache;->maybeRemove(Lcom/android/okhttp/Request;)Z

    goto :goto_0

    .line 535
    :cond_1
    iget-object v1, p0, Lcom/android/okhttp/internal/http/HttpEngine;->userResponse:Lcom/android/okhttp/Response;

    invoke-static {v1}, Lcom/android/okhttp/internal/http/HttpEngine;->stripBody(Lcom/android/okhttp/Response;)Lcom/android/okhttp/Response;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/android/okhttp/OkResponseCache;->put(Lcom/android/okhttp/Response;)Ljava/net/CacheRequest;

    move-result-object v1

    iput-object v1, p0, Lcom/android/okhttp/internal/http/HttpEngine;->storeRequest:Ljava/net/CacheRequest;

    goto :goto_0
.end method

.method private networkRequest(Lcom/android/okhttp/Request;)Lcom/android/okhttp/Request;
    .locals 6
    .param p1, "request"    # Lcom/android/okhttp/Request;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 671
    invoke-virtual {p1}, Lcom/android/okhttp/Request;->newBuilder()Lcom/android/okhttp/Request$Builder;

    move-result-object v3

    .line 673
    .local v3, "result":Lcom/android/okhttp/Request$Builder;
    invoke-virtual {p1}, Lcom/android/okhttp/Request;->getUserAgent()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_0

    .line 674
    invoke-static {}, Lcom/android/okhttp/internal/http/HttpEngine;->getDefaultUserAgent()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/android/okhttp/Request$Builder;->setUserAgent(Ljava/lang/String;)Lcom/android/okhttp/Request$Builder;

    .line 677
    :cond_0
    const-string v4, "Host"

    invoke-virtual {p1, v4}, Lcom/android/okhttp/Request;->header(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_1

    .line 678
    const-string v4, "Host"

    invoke-virtual {p1}, Lcom/android/okhttp/Request;->url()Ljava/net/URL;

    move-result-object v5

    invoke-static {v5}, Lcom/android/okhttp/internal/http/HttpEngine;->hostHeader(Ljava/net/URL;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/android/okhttp/Request$Builder;->header(Ljava/lang/String;Ljava/lang/String;)Lcom/android/okhttp/Request$Builder;

    .line 681
    :cond_1
    iget-object v4, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    invoke-virtual {v4}, Lcom/android/okhttp/Connection;->getHttpMinorVersion()I

    move-result v4

    if-eqz v4, :cond_3

    :cond_2
    const-string v4, "Connection"

    invoke-virtual {p1, v4}, Lcom/android/okhttp/Request;->header(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_3

    .line 683
    const-string v4, "Connection"

    const-string v5, "Keep-Alive"

    invoke-virtual {v3, v4, v5}, Lcom/android/okhttp/Request$Builder;->header(Ljava/lang/String;Ljava/lang/String;)Lcom/android/okhttp/Request$Builder;

    .line 686
    :cond_3
    const-string v4, "Accept-Encoding"

    invoke-virtual {p1, v4}, Lcom/android/okhttp/Request;->header(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_4

    .line 687
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/android/okhttp/internal/http/HttpEngine;->transparentGzip:Z

    .line 688
    const-string v4, "Accept-Encoding"

    const-string v5, "gzip"

    invoke-virtual {v3, v4, v5}, Lcom/android/okhttp/Request$Builder;->header(Ljava/lang/String;Ljava/lang/String;)Lcom/android/okhttp/Request$Builder;

    .line 691
    :cond_4
    invoke-virtual {p0}, Lcom/android/okhttp/internal/http/HttpEngine;->hasRequestBody()Z

    move-result v4

    if-eqz v4, :cond_5

    const-string v4, "Content-Type"

    invoke-virtual {p1, v4}, Lcom/android/okhttp/Request;->header(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_5

    .line 692
    const-string v4, "Content-Type"

    const-string v5, "application/x-www-form-urlencoded"

    invoke-virtual {v3, v4, v5}, Lcom/android/okhttp/Request$Builder;->header(Ljava/lang/String;Ljava/lang/String;)Lcom/android/okhttp/Request$Builder;

    .line 695
    :cond_5
    iget-object v4, p0, Lcom/android/okhttp/internal/http/HttpEngine;->client:Lcom/android/okhttp/OkHttpClient;

    invoke-virtual {v4}, Lcom/android/okhttp/OkHttpClient;->getCookieHandler()Ljava/net/CookieHandler;

    move-result-object v0

    .line 696
    .local v0, "cookieHandler":Ljava/net/CookieHandler;
    if-eqz v0, :cond_6

    .line 700
    invoke-virtual {v3}, Lcom/android/okhttp/Request$Builder;->build()Lcom/android/okhttp/Request;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/okhttp/Request;->headers()Lcom/android/okhttp/Headers;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v4, v5}, Lcom/android/okhttp/internal/http/OkHeaders;->toMultimap(Lcom/android/okhttp/Headers;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v2

    .line 702
    .local v2, "headers":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    invoke-virtual {p1}, Lcom/android/okhttp/Request;->uri()Ljava/net/URI;

    move-result-object v4

    invoke-virtual {v0, v4, v2}, Ljava/net/CookieHandler;->get(Ljava/net/URI;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    .line 705
    .local v1, "cookies":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    invoke-static {v3, v1}, Lcom/android/okhttp/internal/http/OkHeaders;->addCookies(Lcom/android/okhttp/Request$Builder;Ljava/util/Map;)V

    .line 708
    .end local v1    # "cookies":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    .end local v2    # "headers":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    :cond_6
    invoke-virtual {v3}, Lcom/android/okhttp/Request$Builder;->build()Lcom/android/okhttp/Request;

    move-result-object v4

    return-object v4
.end method

.method private static stripBody(Lcom/android/okhttp/Response;)Lcom/android/okhttp/Response;
    .locals 2
    .param p0, "response"    # Lcom/android/okhttp/Response;

    .prologue
    .line 335
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/android/okhttp/Response;->body()Lcom/android/okhttp/Response$Body;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/okhttp/Response;->newBuilder()Lcom/android/okhttp/Response$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/okhttp/Response$Builder;->body(Lcom/android/okhttp/Response$Body;)Lcom/android/okhttp/Response$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/okhttp/Response$Builder;->build()Lcom/android/okhttp/Response;

    move-result-object p0

    .end local p0    # "response":Lcom/android/okhttp/Response;
    :cond_0
    return-object p0
.end method


# virtual methods
.method public final close()Lcom/android/okhttp/Connection;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 561
    sget-boolean v2, Lcom/android/okhttp/internal/http/HttpEngine;->SMARTBONDING_ENABLED:Z

    if-eqz v2, :cond_0

    .line 562
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/okhttp/internal/http/HttpEngine;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v3, Ljava/lang/Throwable;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "close(), "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/okhttp/internal/http/HttpEngine;->responseBodyBytes:Ljava/io/InputStream;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V

    .line 565
    :cond_0
    iget-object v2, p0, Lcom/android/okhttp/internal/http/HttpEngine;->bufferedRequestBody:Lcom/android/okio/BufferedSink;

    if-eqz v2, :cond_2

    .line 567
    iget-object v2, p0, Lcom/android/okhttp/internal/http/HttpEngine;->bufferedRequestBody:Lcom/android/okio/BufferedSink;

    invoke-static {v2}, Lcom/android/okhttp/internal/Util;->closeQuietly(Ljava/io/Closeable;)V

    .line 573
    :cond_1
    :goto_0
    iget-object v2, p0, Lcom/android/okhttp/internal/http/HttpEngine;->responseBody:Lcom/android/okio/Source;

    if-nez v2, :cond_3

    .line 574
    iget-object v2, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    invoke-static {v2}, Lcom/android/okhttp/internal/Util;->closeQuietly(Ljava/io/Closeable;)V

    .line 575
    iput-object v1, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    move-object v0, v1

    .line 599
    :goto_1
    return-object v0

    .line 568
    :cond_2
    iget-object v2, p0, Lcom/android/okhttp/internal/http/HttpEngine;->requestBodyOut:Lcom/android/okio/Sink;

    if-eqz v2, :cond_1

    .line 569
    iget-object v2, p0, Lcom/android/okhttp/internal/http/HttpEngine;->requestBodyOut:Lcom/android/okio/Sink;

    invoke-static {v2}, Lcom/android/okhttp/internal/Util;->closeQuietly(Ljava/io/Closeable;)V

    goto :goto_0

    .line 580
    :cond_3
    iget-object v2, p0, Lcom/android/okhttp/internal/http/HttpEngine;->responseBody:Lcom/android/okio/Source;

    invoke-static {v2}, Lcom/android/okhttp/internal/Util;->closeQuietly(Ljava/io/Closeable;)V

    .line 583
    iget-object v2, p0, Lcom/android/okhttp/internal/http/HttpEngine;->responseBodyBytes:Ljava/io/InputStream;

    invoke-static {v2}, Lcom/android/okhttp/internal/Util;->closeQuietly(Ljava/io/Closeable;)V

    .line 586
    iget-object v2, p0, Lcom/android/okhttp/internal/http/HttpEngine;->transport:Lcom/android/okhttp/internal/http/Transport;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/android/okhttp/internal/http/HttpEngine;->transport:Lcom/android/okhttp/internal/http/Transport;

    invoke-interface {v2}, Lcom/android/okhttp/internal/http/Transport;->canReuseConnection()Z

    move-result v2

    if-nez v2, :cond_4

    .line 587
    iget-object v2, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    invoke-static {v2}, Lcom/android/okhttp/internal/Util;->closeQuietly(Ljava/io/Closeable;)V

    .line 588
    iput-object v1, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    move-object v0, v1

    .line 589
    goto :goto_1

    .line 593
    :cond_4
    iget-object v2, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    invoke-virtual {v2}, Lcom/android/okhttp/Connection;->clearOwner()Z

    move-result v2

    if-nez v2, :cond_5

    .line 594
    iput-object v1, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    .line 597
    :cond_5
    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    .line 598
    .local v0, "result":Lcom/android/okhttp/Connection;
    iput-object v1, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    goto :goto_1
.end method

.method public final getBufferedRequestBody()Lcom/android/okio/BufferedSink;
    .locals 3

    .prologue
    .line 433
    iget-object v1, p0, Lcom/android/okhttp/internal/http/HttpEngine;->bufferedRequestBody:Lcom/android/okio/BufferedSink;

    .line 434
    .local v1, "result":Lcom/android/okio/BufferedSink;
    if-eqz v1, :cond_0

    .line 436
    .end local v1    # "result":Lcom/android/okio/BufferedSink;
    :goto_0
    return-object v1

    .line 435
    .restart local v1    # "result":Lcom/android/okio/BufferedSink;
    :cond_0
    invoke-virtual {p0}, Lcom/android/okhttp/internal/http/HttpEngine;->getRequestBody()Lcom/android/okio/Sink;

    move-result-object v0

    .line 436
    .local v0, "requestBody":Lcom/android/okio/Sink;
    if-eqz v0, :cond_1

    invoke-static {v0}, Lcom/android/okio/Okio;->buffer(Lcom/android/okio/Sink;)Lcom/android/okio/BufferedSink;

    move-result-object v2

    iput-object v2, p0, Lcom/android/okhttp/internal/http/HttpEngine;->bufferedRequestBody:Lcom/android/okio/BufferedSink;

    :goto_1
    move-object v1, v2

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public final getConnection()Lcom/android/okhttp/Connection;
    .locals 1

    .prologue
    .line 479
    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    return-object v0
.end method

.method public final getRequest()Lcom/android/okhttp/Request;
    .locals 1

    .prologue
    .line 446
    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->userRequest:Lcom/android/okhttp/Request;

    return-object v0
.end method

.method public final getRequestBody()Lcom/android/okio/Sink;
    .locals 1

    .prologue
    .line 428
    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->responseSource:Lcom/android/okhttp/ResponseSource;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 429
    :cond_0
    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->requestBodyOut:Lcom/android/okio/Sink;

    return-object v0
.end method

.method public final getResponse()Lcom/android/okhttp/Response;
    .locals 1

    .prologue
    .line 452
    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->userResponse:Lcom/android/okhttp/Response;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 453
    :cond_0
    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->userResponse:Lcom/android/okhttp/Response;

    return-object v0
.end method

.method public final getResponseBody()Lcom/android/okio/Source;
    .locals 1

    .prologue
    .line 457
    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->userResponse:Lcom/android/okhttp/Response;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 458
    :cond_0
    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->responseBody:Lcom/android/okio/Source;

    return-object v0
.end method

.method public final getResponseBodyBytes()Ljava/io/InputStream;
    .locals 5

    .prologue
    .line 463
    sget-boolean v2, Lcom/android/okhttp/internal/http/HttpEngine;->SMARTBONDING_ENABLED:Z

    if-eqz v2, :cond_2

    .line 464
    invoke-virtual {p0}, Lcom/android/okhttp/internal/http/HttpEngine;->getResponseBody()Lcom/android/okio/Source;

    move-result-object v1

    .line 465
    .local v1, "source":Lcom/android/okio/Source;
    instance-of v2, v1, Lcom/android/okhttp/internal/http/MultiratSource;

    if-eqz v2, :cond_2

    .line 466
    check-cast v1, Lcom/android/okhttp/internal/http/MultiratSource;

    .end local v1    # "source":Lcom/android/okio/Source;
    iget-object v2, v1, Lcom/android/okhttp/internal/http/MultiratSource;->mInput:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iput-object v2, p0, Lcom/android/okhttp/internal/http/HttpEngine;->responseBodyBytes:Ljava/io/InputStream;

    .line 467
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/okhttp/internal/http/HttpEngine;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getResponseBodyBytes(), responseBodyBytes="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/okhttp/internal/http/HttpEngine;->responseBodyBytes:Ljava/io/InputStream;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 468
    :cond_0
    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->responseBodyBytes:Ljava/io/InputStream;

    .line 473
    :cond_1
    :goto_0
    return-object v0

    .line 472
    :cond_2
    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->responseBodyBytes:Ljava/io/InputStream;

    .line 473
    .local v0, "result":Ljava/io/InputStream;
    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/android/okhttp/internal/http/HttpEngine;->getResponseBody()Lcom/android/okio/Source;

    move-result-object v2

    invoke-static {v2}, Lcom/android/okio/Okio;->buffer(Lcom/android/okio/Source;)Lcom/android/okio/BufferedSource;

    move-result-object v2

    invoke-interface {v2}, Lcom/android/okio/BufferedSource;->inputStream()Ljava/io/InputStream;

    move-result-object v0

    .end local v0    # "result":Ljava/io/InputStream;
    iput-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->responseBodyBytes:Ljava/io/InputStream;

    goto :goto_0
.end method

.method public getRoute()Lcom/android/okhttp/Route;
    .locals 1

    .prologue
    .line 521
    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->route:Lcom/android/okhttp/Route;

    return-object v0
.end method

.method hasRequestBody()Z
    .locals 1

    .prologue
    .line 423
    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->userRequest:Lcom/android/okhttp/Request;

    invoke-virtual {v0}, Lcom/android/okhttp/Request;->method()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/okhttp/internal/http/HttpMethod;->hasRequestBody(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final hasResponse()Z
    .locals 1

    .prologue
    .line 442
    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->userResponse:Lcom/android/okhttp/Response;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hasResponseBody()Z
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 642
    iget-object v3, p0, Lcom/android/okhttp/internal/http/HttpEngine;->userRequest:Lcom/android/okhttp/Request;

    invoke-virtual {v3}, Lcom/android/okhttp/Request;->method()Ljava/lang/String;

    move-result-object v3

    const-string v4, "HEAD"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 661
    :cond_0
    :goto_0
    return v1

    .line 646
    :cond_1
    iget-object v3, p0, Lcom/android/okhttp/internal/http/HttpEngine;->userResponse:Lcom/android/okhttp/Response;

    invoke-virtual {v3}, Lcom/android/okhttp/Response;->code()I

    move-result v0

    .line 647
    .local v0, "responseCode":I
    const/16 v3, 0x64

    if-lt v0, v3, :cond_2

    const/16 v3, 0xc8

    if-lt v0, v3, :cond_3

    :cond_2
    const/16 v3, 0xcc

    if-eq v0, v3, :cond_3

    const/16 v3, 0x130

    if-eq v0, v3, :cond_3

    move v1, v2

    .line 650
    goto :goto_0

    .line 656
    :cond_3
    iget-object v3, p0, Lcom/android/okhttp/internal/http/HttpEngine;->networkResponse:Lcom/android/okhttp/Response;

    invoke-static {v3}, Lcom/android/okhttp/internal/http/OkHeaders;->contentLength(Lcom/android/okhttp/Response;)J

    move-result-wide v4

    const-wide/16 v6, -0x1

    cmp-long v3, v4, v6

    if-nez v3, :cond_4

    const-string v3, "chunked"

    iget-object v4, p0, Lcom/android/okhttp/internal/http/HttpEngine;->networkResponse:Lcom/android/okhttp/Response;

    const-string v5, "Transfer-Encoding"

    invoke-virtual {v4, v5}, Lcom/android/okhttp/Response;->header(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    :cond_4
    move v1, v2

    .line 658
    goto :goto_0
.end method

.method public final readResponse()V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 727
    iget-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->userResponse:Lcom/android/okhttp/Response;

    if-eqz v6, :cond_1

    .line 729
    sget-object v7, Lcom/android/okhttp/internal/http/HttpEngine;->originalRequestUri:Ljava/util/HashMap;

    monitor-enter v7

    .line 730
    :try_start_0
    sget-object v6, Lcom/android/okhttp/internal/http/HttpEngine;->originalRequestUri:Ljava/util/HashMap;

    iget-wide v8, p0, Lcom/android/okhttp/internal/http/HttpEngine;->mThreadID:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 731
    monitor-exit v7

    .line 885
    :cond_0
    :goto_0
    return-void

    .line 731
    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v6

    .line 735
    :cond_1
    iget-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->networkRequest:Lcom/android/okhttp/Request;

    if-nez v6, :cond_2

    iget-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->cacheResponse:Lcom/android/okhttp/Response;

    if-nez v6, :cond_2

    .line 737
    sget-object v7, Lcom/android/okhttp/internal/http/HttpEngine;->originalRequestUri:Ljava/util/HashMap;

    monitor-enter v7

    .line 738
    :try_start_1
    sget-object v6, Lcom/android/okhttp/internal/http/HttpEngine;->originalRequestUri:Ljava/util/HashMap;

    iget-wide v8, p0, Lcom/android/okhttp/internal/http/HttpEngine;->mThreadID:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 739
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 741
    new-instance v6, Ljava/lang/IllegalStateException;

    const-string v7, "call sendRequest() first!"

    invoke-direct {v6, v7}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 739
    :catchall_1
    move-exception v6

    :try_start_2
    monitor-exit v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v6

    .line 743
    :cond_2
    iget-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->networkRequest:Lcom/android/okhttp/Request;

    if-nez v6, :cond_3

    .line 745
    sget-object v7, Lcom/android/okhttp/internal/http/HttpEngine;->originalRequestUri:Ljava/util/HashMap;

    monitor-enter v7

    .line 746
    :try_start_3
    sget-object v6, Lcom/android/okhttp/internal/http/HttpEngine;->originalRequestUri:Ljava/util/HashMap;

    iget-wide v8, p0, Lcom/android/okhttp/internal/http/HttpEngine;->mThreadID:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 747
    monitor-exit v7

    goto :goto_0

    :catchall_2
    move-exception v6

    monitor-exit v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    throw v6

    .line 753
    :cond_3
    iget-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->bufferedRequestBody:Lcom/android/okio/BufferedSink;

    if-eqz v6, :cond_4

    iget-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->bufferedRequestBody:Lcom/android/okio/BufferedSink;

    invoke-interface {v6}, Lcom/android/okio/BufferedSink;->buffer()Lcom/android/okio/OkBuffer;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/okio/OkBuffer;->size()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-lez v6, :cond_4

    .line 754
    iget-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->bufferedRequestBody:Lcom/android/okio/BufferedSink;

    invoke-interface {v6}, Lcom/android/okio/BufferedSink;->flush()V

    .line 757
    :cond_4
    iget-wide v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->sentRequestMillis:J

    const-wide/16 v8, -0x1

    cmp-long v6, v6, v8

    if-nez v6, :cond_6

    .line 758
    iget-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->networkRequest:Lcom/android/okhttp/Request;

    invoke-static {v6}, Lcom/android/okhttp/internal/http/OkHeaders;->contentLength(Lcom/android/okhttp/Request;)J

    move-result-wide v6

    const-wide/16 v8, -0x1

    cmp-long v6, v6, v8

    if-nez v6, :cond_5

    iget-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->requestBodyOut:Lcom/android/okio/Sink;

    instance-of v6, v6, Lcom/android/okhttp/internal/http/RetryableSink;

    if-eqz v6, :cond_5

    .line 761
    iget-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->requestBodyOut:Lcom/android/okio/Sink;

    check-cast v6, Lcom/android/okhttp/internal/http/RetryableSink;

    invoke-virtual {v6}, Lcom/android/okhttp/internal/http/RetryableSink;->contentLength()J

    move-result-wide v0

    .line 762
    .local v0, "contentLength":J
    iget-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->networkRequest:Lcom/android/okhttp/Request;

    invoke-virtual {v6}, Lcom/android/okhttp/Request;->newBuilder()Lcom/android/okhttp/Request$Builder;

    move-result-object v6

    const-string v7, "Content-Length"

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lcom/android/okhttp/Request$Builder;->header(Ljava/lang/String;Ljava/lang/String;)Lcom/android/okhttp/Request$Builder;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/okhttp/Request$Builder;->build()Lcom/android/okhttp/Request;

    move-result-object v6

    iput-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->networkRequest:Lcom/android/okhttp/Request;

    .line 766
    .end local v0    # "contentLength":J
    :cond_5
    iget-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->transport:Lcom/android/okhttp/internal/http/Transport;

    iget-object v7, p0, Lcom/android/okhttp/internal/http/HttpEngine;->networkRequest:Lcom/android/okhttp/Request;

    invoke-interface {v6, v7}, Lcom/android/okhttp/internal/http/Transport;->writeRequestHeaders(Lcom/android/okhttp/Request;)V

    .line 769
    :cond_6
    iget-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->requestBodyOut:Lcom/android/okio/Sink;

    if-eqz v6, :cond_7

    .line 770
    iget-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->bufferedRequestBody:Lcom/android/okio/BufferedSink;

    if-eqz v6, :cond_8

    .line 772
    iget-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->bufferedRequestBody:Lcom/android/okio/BufferedSink;

    invoke-interface {v6}, Lcom/android/okio/BufferedSink;->close()V

    .line 776
    :goto_1
    iget-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->requestBodyOut:Lcom/android/okio/Sink;

    instance-of v6, v6, Lcom/android/okhttp/internal/http/RetryableSink;

    if-eqz v6, :cond_7

    .line 777
    iget-object v7, p0, Lcom/android/okhttp/internal/http/HttpEngine;->transport:Lcom/android/okhttp/internal/http/Transport;

    iget-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->requestBodyOut:Lcom/android/okio/Sink;

    check-cast v6, Lcom/android/okhttp/internal/http/RetryableSink;

    invoke-interface {v7, v6}, Lcom/android/okhttp/internal/http/Transport;->writeRequestBody(Lcom/android/okhttp/internal/http/RetryableSink;)V

    .line 781
    :cond_7
    iget-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->transport:Lcom/android/okhttp/internal/http/Transport;

    invoke-interface {v6}, Lcom/android/okhttp/internal/http/Transport;->flushRequest()V

    .line 783
    iget-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->transport:Lcom/android/okhttp/internal/http/Transport;

    invoke-interface {v6}, Lcom/android/okhttp/internal/http/Transport;->readResponseHeaders()Lcom/android/okhttp/Response$Builder;

    move-result-object v6

    iget-object v7, p0, Lcom/android/okhttp/internal/http/HttpEngine;->networkRequest:Lcom/android/okhttp/Request;

    invoke-virtual {v6, v7}, Lcom/android/okhttp/Response$Builder;->request(Lcom/android/okhttp/Request;)Lcom/android/okhttp/Response$Builder;

    move-result-object v6

    iget-object v7, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    invoke-virtual {v7}, Lcom/android/okhttp/Connection;->getHandshake()Lcom/android/okhttp/Handshake;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/android/okhttp/Response$Builder;->handshake(Lcom/android/okhttp/Handshake;)Lcom/android/okhttp/Response$Builder;

    move-result-object v6

    sget-object v7, Lcom/android/okhttp/internal/http/OkHeaders;->SENT_MILLIS:Ljava/lang/String;

    iget-wide v8, p0, Lcom/android/okhttp/internal/http/HttpEngine;->sentRequestMillis:J

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lcom/android/okhttp/Response$Builder;->header(Ljava/lang/String;Ljava/lang/String;)Lcom/android/okhttp/Response$Builder;

    move-result-object v6

    sget-object v7, Lcom/android/okhttp/internal/http/OkHeaders;->RECEIVED_MILLIS:Ljava/lang/String;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lcom/android/okhttp/Response$Builder;->header(Ljava/lang/String;Ljava/lang/String;)Lcom/android/okhttp/Response$Builder;

    move-result-object v6

    iget-object v7, p0, Lcom/android/okhttp/internal/http/HttpEngine;->responseSource:Lcom/android/okhttp/ResponseSource;

    invoke-virtual {v6, v7}, Lcom/android/okhttp/Response$Builder;->setResponseSource(Lcom/android/okhttp/ResponseSource;)Lcom/android/okhttp/Response$Builder;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/okhttp/Response$Builder;->build()Lcom/android/okhttp/Response;

    move-result-object v6

    iput-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->networkResponse:Lcom/android/okhttp/Response;

    .line 790
    iget-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    iget-object v7, p0, Lcom/android/okhttp/internal/http/HttpEngine;->networkResponse:Lcom/android/okhttp/Response;

    invoke-virtual {v7}, Lcom/android/okhttp/Response;->httpMinorVersion()I

    move-result v7

    invoke-virtual {v6, v7}, Lcom/android/okhttp/Connection;->setHttpMinorVersion(I)V

    .line 791
    iget-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->networkResponse:Lcom/android/okhttp/Response;

    invoke-virtual {v6}, Lcom/android/okhttp/Response;->headers()Lcom/android/okhttp/Headers;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/android/okhttp/internal/http/HttpEngine;->receiveHeaders(Lcom/android/okhttp/Headers;)V

    .line 793
    iget-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->responseSource:Lcom/android/okhttp/ResponseSource;

    sget-object v7, Lcom/android/okhttp/ResponseSource;->CONDITIONAL_CACHE:Lcom/android/okhttp/ResponseSource;

    if-ne v6, v7, :cond_a

    .line 794
    iget-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->cacheResponse:Lcom/android/okhttp/Response;

    iget-object v7, p0, Lcom/android/okhttp/internal/http/HttpEngine;->networkResponse:Lcom/android/okhttp/Response;

    invoke-virtual {v6, v7}, Lcom/android/okhttp/Response;->validate(Lcom/android/okhttp/Response;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 795
    iget-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->cacheResponse:Lcom/android/okhttp/Response;

    invoke-virtual {v6}, Lcom/android/okhttp/Response;->newBuilder()Lcom/android/okhttp/Response$Builder;

    move-result-object v6

    iget-object v7, p0, Lcom/android/okhttp/internal/http/HttpEngine;->userRequest:Lcom/android/okhttp/Request;

    invoke-virtual {v6, v7}, Lcom/android/okhttp/Response$Builder;->request(Lcom/android/okhttp/Request;)Lcom/android/okhttp/Response$Builder;

    move-result-object v6

    iget-object v7, p0, Lcom/android/okhttp/internal/http/HttpEngine;->priorResponse:Lcom/android/okhttp/Response;

    invoke-static {v7}, Lcom/android/okhttp/internal/http/HttpEngine;->stripBody(Lcom/android/okhttp/Response;)Lcom/android/okhttp/Response;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/android/okhttp/Response$Builder;->priorResponse(Lcom/android/okhttp/Response;)Lcom/android/okhttp/Response$Builder;

    move-result-object v6

    iget-object v7, p0, Lcom/android/okhttp/internal/http/HttpEngine;->cacheResponse:Lcom/android/okhttp/Response;

    invoke-virtual {v7}, Lcom/android/okhttp/Response;->headers()Lcom/android/okhttp/Headers;

    move-result-object v7

    iget-object v8, p0, Lcom/android/okhttp/internal/http/HttpEngine;->networkResponse:Lcom/android/okhttp/Response;

    invoke-virtual {v8}, Lcom/android/okhttp/Response;->headers()Lcom/android/okhttp/Headers;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/android/okhttp/internal/http/HttpEngine;->combine(Lcom/android/okhttp/Headers;Lcom/android/okhttp/Headers;)Lcom/android/okhttp/Headers;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/android/okhttp/Response$Builder;->headers(Lcom/android/okhttp/Headers;)Lcom/android/okhttp/Response$Builder;

    move-result-object v6

    iget-object v7, p0, Lcom/android/okhttp/internal/http/HttpEngine;->cacheResponse:Lcom/android/okhttp/Response;

    invoke-static {v7}, Lcom/android/okhttp/internal/http/HttpEngine;->stripBody(Lcom/android/okhttp/Response;)Lcom/android/okhttp/Response;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/android/okhttp/Response$Builder;->cacheResponse(Lcom/android/okhttp/Response;)Lcom/android/okhttp/Response$Builder;

    move-result-object v6

    iget-object v7, p0, Lcom/android/okhttp/internal/http/HttpEngine;->networkResponse:Lcom/android/okhttp/Response;

    invoke-static {v7}, Lcom/android/okhttp/internal/http/HttpEngine;->stripBody(Lcom/android/okhttp/Response;)Lcom/android/okhttp/Response;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/android/okhttp/Response$Builder;->networkResponse(Lcom/android/okhttp/Response;)Lcom/android/okhttp/Response$Builder;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/okhttp/Response$Builder;->build()Lcom/android/okhttp/Response;

    move-result-object v6

    iput-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->userResponse:Lcom/android/okhttp/Response;

    .line 802
    iget-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->transport:Lcom/android/okhttp/internal/http/Transport;

    invoke-interface {v6}, Lcom/android/okhttp/internal/http/Transport;->emptyTransferStream()V

    .line 803
    invoke-virtual {p0}, Lcom/android/okhttp/internal/http/HttpEngine;->releaseConnection()V

    .line 807
    iget-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->client:Lcom/android/okhttp/OkHttpClient;

    invoke-virtual {v6}, Lcom/android/okhttp/OkHttpClient;->getOkResponseCache()Lcom/android/okhttp/OkResponseCache;

    move-result-object v4

    .line 808
    .local v4, "responseCache":Lcom/android/okhttp/OkResponseCache;
    invoke-interface {v4}, Lcom/android/okhttp/OkResponseCache;->trackConditionalCacheHit()V

    .line 809
    iget-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->cacheResponse:Lcom/android/okhttp/Response;

    iget-object v7, p0, Lcom/android/okhttp/internal/http/HttpEngine;->userResponse:Lcom/android/okhttp/Response;

    invoke-static {v7}, Lcom/android/okhttp/internal/http/HttpEngine;->stripBody(Lcom/android/okhttp/Response;)Lcom/android/okhttp/Response;

    move-result-object v7

    invoke-interface {v4, v6, v7}, Lcom/android/okhttp/OkResponseCache;->update(Lcom/android/okhttp/Response;Lcom/android/okhttp/Response;)V

    .line 810
    iget-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->cacheResponse:Lcom/android/okhttp/Response;

    invoke-virtual {v6}, Lcom/android/okhttp/Response;->body()Lcom/android/okhttp/Response$Body;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 811
    iget-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->cacheResponse:Lcom/android/okhttp/Response;

    invoke-virtual {v6}, Lcom/android/okhttp/Response;->body()Lcom/android/okhttp/Response$Body;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/okhttp/Response$Body;->source()Lcom/android/okio/Source;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/android/okhttp/internal/http/HttpEngine;->initContentStream(Lcom/android/okio/Source;)V

    goto/16 :goto_0

    .line 774
    .end local v4    # "responseCache":Lcom/android/okhttp/OkResponseCache;
    :cond_8
    iget-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->requestBodyOut:Lcom/android/okio/Sink;

    invoke-interface {v6}, Lcom/android/okio/Sink;->close()V

    goto/16 :goto_1

    .line 816
    :cond_9
    iget-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->cacheResponse:Lcom/android/okhttp/Response;

    invoke-virtual {v6}, Lcom/android/okhttp/Response;->body()Lcom/android/okhttp/Response$Body;

    move-result-object v6

    invoke-static {v6}, Lcom/android/okhttp/internal/Util;->closeQuietly(Ljava/io/Closeable;)V

    .line 820
    :cond_a
    iget-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->networkResponse:Lcom/android/okhttp/Response;

    invoke-virtual {v6}, Lcom/android/okhttp/Response;->newBuilder()Lcom/android/okhttp/Response$Builder;

    move-result-object v6

    iget-object v7, p0, Lcom/android/okhttp/internal/http/HttpEngine;->userRequest:Lcom/android/okhttp/Request;

    invoke-virtual {v6, v7}, Lcom/android/okhttp/Response$Builder;->request(Lcom/android/okhttp/Request;)Lcom/android/okhttp/Response$Builder;

    move-result-object v6

    iget-object v7, p0, Lcom/android/okhttp/internal/http/HttpEngine;->priorResponse:Lcom/android/okhttp/Response;

    invoke-static {v7}, Lcom/android/okhttp/internal/http/HttpEngine;->stripBody(Lcom/android/okhttp/Response;)Lcom/android/okhttp/Response;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/android/okhttp/Response$Builder;->priorResponse(Lcom/android/okhttp/Response;)Lcom/android/okhttp/Response$Builder;

    move-result-object v6

    iget-object v7, p0, Lcom/android/okhttp/internal/http/HttpEngine;->cacheResponse:Lcom/android/okhttp/Response;

    invoke-static {v7}, Lcom/android/okhttp/internal/http/HttpEngine;->stripBody(Lcom/android/okhttp/Response;)Lcom/android/okhttp/Response;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/android/okhttp/Response$Builder;->cacheResponse(Lcom/android/okhttp/Response;)Lcom/android/okhttp/Response$Builder;

    move-result-object v6

    iget-object v7, p0, Lcom/android/okhttp/internal/http/HttpEngine;->networkResponse:Lcom/android/okhttp/Response;

    invoke-static {v7}, Lcom/android/okhttp/internal/http/HttpEngine;->stripBody(Lcom/android/okhttp/Response;)Lcom/android/okhttp/Response;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/android/okhttp/Response$Builder;->networkResponse(Lcom/android/okhttp/Response;)Lcom/android/okhttp/Response$Builder;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/okhttp/Response$Builder;->build()Lcom/android/okhttp/Response;

    move-result-object v6

    iput-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->userResponse:Lcom/android/okhttp/Response;

    .line 828
    sget-boolean v6, Lcom/android/okhttp/internal/http/HttpEngine;->SMARTBONDING_ENABLED:Z

    if-eqz v6, :cond_13

    .line 830
    :try_start_4
    sget-boolean v6, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v6, :cond_b

    .line 831
    iget-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "get response "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/okhttp/internal/http/HttpEngine;->userResponse:Lcom/android/okhttp/Response;

    invoke-virtual {v8}, Lcom/android/okhttp/Response;->statusLine()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 832
    iget-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    iget-object v7, p0, Lcom/android/okhttp/internal/http/HttpEngine;->userResponse:Lcom/android/okhttp/Response;

    invoke-virtual {v7}, Lcom/android/okhttp/Response;->headers()Lcom/android/okhttp/Headers;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/okhttp/Headers;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/android/okhttp/internal/http/MultiratLog;->d(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1

    .line 837
    :cond_b
    :goto_2
    iget-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    if-eqz v6, :cond_c

    .line 838
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    iget-object v8, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    iget-wide v8, v8, Lcom/android/okhttp/Connection;->startSocketCreation:J

    sub-long/2addr v6, v8

    iput-wide v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->mainResponseTime:J

    .line 839
    :cond_c
    iget-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->userResponse:Lcom/android/okhttp/Response;

    invoke-virtual {v6}, Lcom/android/okhttp/Response;->code()I

    move-result v5

    .line 840
    .local v5, "responseCode":I
    const/4 v3, -0x1

    .line 842
    .local v3, "netType":I
    :try_start_5
    sget-boolean v6, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v6, :cond_d

    iget-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "now we have ori request number "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/android/okhttp/internal/http/HttpEngine;->originalRequestUri:Ljava/util/HashMap;

    invoke-virtual {v8}, Ljava/util/HashMap;->size()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " in "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/android/okhttp/internal/http/HttpEngine;->originalRequestUri:Ljava/util/HashMap;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " of object "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/android/okhttp/internal/http/HttpEngine;->originalRequestUri:Ljava/util/HashMap;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 843
    :cond_d
    const/16 v6, 0x12c

    if-eq v5, v6, :cond_f

    const/16 v6, 0x12d

    if-eq v5, v6, :cond_f

    const/16 v6, 0x12e

    if-eq v5, v6, :cond_f

    const/16 v6, 0x12f

    if-eq v5, v6, :cond_f

    .line 845
    sget-object v7, Lcom/android/okhttp/internal/http/HttpEngine;->originalRequestUri:Ljava/util/HashMap;

    monitor-enter v7
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_0

    .line 846
    :try_start_6
    sget-object v6, Lcom/android/okhttp/internal/http/HttpEngine;->originalRequestUri:Ljava/util/HashMap;

    iget-wide v8, p0, Lcom/android/okhttp/internal/http/HttpEngine;->mThreadID:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/okhttp/Request;

    iput-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->mOriRequestHeader:Lcom/android/okhttp/Request;

    .line 847
    monitor-exit v7
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    .line 848
    :try_start_7
    iget-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->mOriRequestHeader:Lcom/android/okhttp/Request;

    if-nez v6, :cond_e

    .line 849
    invoke-virtual {p0}, Lcom/android/okhttp/internal/http/HttpEngine;->getRequest()Lcom/android/okhttp/Request;

    move-result-object v6

    iput-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->mOriRequestHeader:Lcom/android/okhttp/Request;

    .line 851
    :cond_e
    sget-boolean v6, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v6, :cond_f

    iget-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getTransferStream() : oriRequestHeader="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/okhttp/internal/http/HttpEngine;->mOriRequestHeader:Lcom/android/okhttp/Request;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 854
    :cond_f
    const/16 v6, 0x12c

    if-eq v5, v6, :cond_10

    const/16 v6, 0x12d

    if-eq v5, v6, :cond_10

    const/16 v6, 0x12e

    if-eq v5, v6, :cond_10

    const/16 v6, 0x12f

    if-ne v5, v6, :cond_13

    .line 856
    :cond_10
    iget-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    invoke-virtual {v6}, Lcom/android/okhttp/Connection;->getSocket()Ljava/net/Socket;

    move-result-object v6

    invoke-virtual {v6}, Ljava/net/Socket;->getLocalAddress()Ljava/net/InetAddress;

    move-result-object v6

    iget-object v7, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/android/okhttp/Connection;->getLocalAddr(I)Ljava/net/InetAddress;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/net/InetAddress;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_14

    .line 857
    const/4 v3, 0x0

    .line 861
    :goto_3
    sget-object v7, Lcom/android/okhttp/internal/http/HttpEngine;->mainInterfaceID:Ljava/util/HashMap;

    monitor-enter v7
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_0

    .line 862
    :try_start_8
    sget-object v6, Lcom/android/okhttp/internal/http/HttpEngine;->mainInterfaceID:Ljava/util/HashMap;

    iget-wide v8, p0, Lcom/android/okhttp/internal/http/HttpEngine;->mThreadID:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    if-nez v6, :cond_11

    .line 863
    sget-object v6, Lcom/android/okhttp/internal/http/HttpEngine;->mainInterfaceID:Ljava/util/HashMap;

    iget-wide v8, p0, Lcom/android/okhttp/internal/http/HttpEngine;->mThreadID:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v6, v8, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 864
    sget-object v6, Lcom/android/okhttp/internal/http/HttpEngine;->originalSSLList:Ljava/util/HashMap;

    iget-wide v8, p0, Lcom/android/okhttp/internal/http/HttpEngine;->mThreadID:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    iget-object v9, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    invoke-virtual {v6, v8, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 866
    :cond_11
    sget-boolean v6, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v6, :cond_12

    iget-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "getTransferStream() Thread id: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-wide v10, p0, Lcom/android/okhttp/internal/http/HttpEngine;->mThreadID:J

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " netType:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", now mainInfID map size is "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Lcom/android/okhttp/internal/http/HttpEngine;->mainInterfaceID:Ljava/util/HashMap;

    invoke-virtual {v9}, Ljava/util/HashMap;->size()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 867
    :cond_12
    monitor-exit v7
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_4

    .line 876
    .end local v3    # "netType":I
    .end local v5    # "responseCode":I
    :cond_13
    :goto_4
    invoke-virtual {p0}, Lcom/android/okhttp/internal/http/HttpEngine;->hasResponseBody()Z

    move-result v6

    if-nez v6, :cond_15

    .line 878
    iget-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->transport:Lcom/android/okhttp/internal/http/Transport;

    iget-object v7, p0, Lcom/android/okhttp/internal/http/HttpEngine;->storeRequest:Ljava/net/CacheRequest;

    invoke-interface {v6, v7}, Lcom/android/okhttp/internal/http/Transport;->getTransferStream(Ljava/net/CacheRequest;)Lcom/android/okio/Source;

    move-result-object v6

    iput-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->responseTransferSource:Lcom/android/okio/Source;

    .line 879
    iget-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->responseTransferSource:Lcom/android/okio/Source;

    iput-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->responseBody:Lcom/android/okio/Source;

    goto/16 :goto_0

    .line 847
    .restart local v3    # "netType":I
    .restart local v5    # "responseCode":I
    :catchall_3
    move-exception v6

    :try_start_9
    monitor-exit v7
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    :try_start_a
    throw v6
    :try_end_a
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_0

    .line 870
    :catch_0
    move-exception v2

    .line 871
    .local v2, "ex":Ljava/lang/Throwable;
    sget-boolean v6, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v6, :cond_13

    iget-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    invoke-virtual {v6, v2}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V

    goto :goto_4

    .line 859
    .end local v2    # "ex":Ljava/lang/Throwable;
    :cond_14
    const/4 v3, 0x1

    goto/16 :goto_3

    .line 867
    :catchall_4
    move-exception v6

    :try_start_b
    monitor-exit v7
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_4

    :try_start_c
    throw v6
    :try_end_c
    .catch Ljava/lang/Throwable; {:try_start_c .. :try_end_c} :catch_0

    .line 883
    .end local v3    # "netType":I
    .end local v5    # "responseCode":I
    :cond_15
    invoke-direct {p0}, Lcom/android/okhttp/internal/http/HttpEngine;->maybeCache()V

    .line 884
    iget-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->transport:Lcom/android/okhttp/internal/http/Transport;

    iget-object v7, p0, Lcom/android/okhttp/internal/http/HttpEngine;->storeRequest:Ljava/net/CacheRequest;

    invoke-interface {v6, v7}, Lcom/android/okhttp/internal/http/Transport;->getTransferStream(Ljava/net/CacheRequest;)Lcom/android/okio/Source;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/android/okhttp/internal/http/HttpEngine;->initContentStream(Lcom/android/okio/Source;)V

    goto/16 :goto_0

    .line 835
    :catch_1
    move-exception v6

    goto/16 :goto_2
.end method

.method public receiveHeaders(Lcom/android/okhttp/Headers;)V
    .locals 3
    .param p1, "headers"    # Lcom/android/okhttp/Headers;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 942
    iget-object v1, p0, Lcom/android/okhttp/internal/http/HttpEngine;->client:Lcom/android/okhttp/OkHttpClient;

    invoke-virtual {v1}, Lcom/android/okhttp/OkHttpClient;->getCookieHandler()Ljava/net/CookieHandler;

    move-result-object v0

    .line 943
    .local v0, "cookieHandler":Ljava/net/CookieHandler;
    if-eqz v0, :cond_0

    .line 944
    iget-object v1, p0, Lcom/android/okhttp/internal/http/HttpEngine;->userRequest:Lcom/android/okhttp/Request;

    invoke-virtual {v1}, Lcom/android/okhttp/Request;->uri()Ljava/net/URI;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {p1, v2}, Lcom/android/okhttp/internal/http/OkHeaders;->toMultimap(Lcom/android/okhttp/Headers;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/net/CookieHandler;->put(Ljava/net/URI;Ljava/util/Map;)V

    .line 946
    :cond_0
    return-void
.end method

.method public recover(Ljava/io/IOException;)Lcom/android/okhttp/internal/http/HttpEngine;
    .locals 9
    .param p1, "e"    # Ljava/io/IOException;

    .prologue
    .line 488
    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->routeSelector:Lcom/android/okhttp/internal/http/RouteSelector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    if-eqz v0, :cond_0

    .line 489
    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->routeSelector:Lcom/android/okhttp/internal/http/RouteSelector;

    iget-object v1, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    invoke-virtual {v0, v1, p1}, Lcom/android/okhttp/internal/http/RouteSelector;->connectFailed(Lcom/android/okhttp/Connection;Ljava/io/IOException;)V

    .line 492
    :cond_0
    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->requestBodyOut:Lcom/android/okio/Sink;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->requestBodyOut:Lcom/android/okio/Sink;

    instance-of v0, v0, Lcom/android/okhttp/internal/http/RetryableSink;

    if-eqz v0, :cond_5

    :cond_1
    const/4 v8, 0x1

    .line 493
    .local v8, "canRetryRequestBody":Z
    :goto_0
    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->routeSelector:Lcom/android/okhttp/internal/http/RouteSelector;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    if-eqz v0, :cond_4

    :cond_2
    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->routeSelector:Lcom/android/okhttp/internal/http/RouteSelector;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->routeSelector:Lcom/android/okhttp/internal/http/RouteSelector;

    invoke-virtual {v0}, Lcom/android/okhttp/internal/http/RouteSelector;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    invoke-direct {p0, p1}, Lcom/android/okhttp/internal/http/HttpEngine;->isRecoverable(Ljava/io/IOException;)Z

    move-result v0

    if-eqz v0, :cond_4

    if-nez v8, :cond_6

    .line 497
    :cond_4
    const/4 v0, 0x0

    .line 503
    :goto_1
    return-object v0

    .line 492
    .end local v8    # "canRetryRequestBody":Z
    :cond_5
    const/4 v8, 0x0

    goto :goto_0

    .line 500
    .restart local v8    # "canRetryRequestBody":Z
    :cond_6
    invoke-virtual {p0}, Lcom/android/okhttp/internal/http/HttpEngine;->close()Lcom/android/okhttp/Connection;

    move-result-object v4

    .line 503
    .local v4, "connection":Lcom/android/okhttp/Connection;
    new-instance v0, Lcom/android/okhttp/internal/http/HttpEngine;

    iget-object v1, p0, Lcom/android/okhttp/internal/http/HttpEngine;->client:Lcom/android/okhttp/OkHttpClient;

    iget-object v2, p0, Lcom/android/okhttp/internal/http/HttpEngine;->userRequest:Lcom/android/okhttp/Request;

    iget-boolean v3, p0, Lcom/android/okhttp/internal/http/HttpEngine;->bufferRequestBody:Z

    iget-object v5, p0, Lcom/android/okhttp/internal/http/HttpEngine;->routeSelector:Lcom/android/okhttp/internal/http/RouteSelector;

    iget-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->requestBodyOut:Lcom/android/okio/Sink;

    check-cast v6, Lcom/android/okhttp/internal/http/RetryableSink;

    iget-object v7, p0, Lcom/android/okhttp/internal/http/HttpEngine;->priorResponse:Lcom/android/okhttp/Response;

    invoke-direct/range {v0 .. v7}, Lcom/android/okhttp/internal/http/HttpEngine;-><init>(Lcom/android/okhttp/OkHttpClient;Lcom/android/okhttp/Request;ZLcom/android/okhttp/Connection;Lcom/android/okhttp/internal/http/RouteSelector;Lcom/android/okhttp/internal/http/RetryableSink;Lcom/android/okhttp/Response;)V

    goto :goto_1
.end method

.method public final releaseConnection()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 545
    sget-boolean v0, Lcom/android/okhttp/internal/http/HttpEngine;->SMARTBONDING_ENABLED:Z

    if-eqz v0, :cond_0

    .line 546
    sget-boolean v0, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v1, Ljava/lang/Throwable;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "releaseConnection(), "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/okhttp/internal/http/HttpEngine;->transport:Lcom/android/okhttp/internal/http/Transport;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V

    .line 549
    :cond_0
    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->transport:Lcom/android/okhttp/internal/http/Transport;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    if-eqz v0, :cond_1

    .line 550
    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->transport:Lcom/android/okhttp/internal/http/Transport;

    invoke-interface {v0}, Lcom/android/okhttp/internal/http/Transport;->releaseConnectionOnIdle()V

    .line 552
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    .line 553
    return-void
.end method

.method public final sendRequest()V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 257
    iget-object v7, p0, Lcom/android/okhttp/internal/http/HttpEngine;->responseSource:Lcom/android/okhttp/ResponseSource;

    if-eqz v7, :cond_1

    .line 332
    :cond_0
    :goto_0
    return-void

    .line 258
    :cond_1
    iget-object v7, p0, Lcom/android/okhttp/internal/http/HttpEngine;->transport:Lcom/android/okhttp/internal/http/Transport;

    if-eqz v7, :cond_2

    new-instance v6, Ljava/lang/IllegalStateException;

    invoke-direct {v6}, Ljava/lang/IllegalStateException;-><init>()V

    throw v6

    .line 263
    :cond_2
    invoke-static {}, Lcom/android/okhttp/internal/http/SBServiceAPI;->isSBSettingEnabled()Z

    move-result v7

    sput-boolean v7, Lcom/android/okhttp/internal/http/HttpEngine;->SMARTBONDING_ENABLED:Z

    .line 266
    iget-object v7, p0, Lcom/android/okhttp/internal/http/HttpEngine;->userRequest:Lcom/android/okhttp/Request;

    invoke-direct {p0, v7}, Lcom/android/okhttp/internal/http/HttpEngine;->networkRequest(Lcom/android/okhttp/Request;)Lcom/android/okhttp/Request;

    move-result-object v4

    .line 268
    .local v4, "request":Lcom/android/okhttp/Request;
    iget-object v7, p0, Lcom/android/okhttp/internal/http/HttpEngine;->client:Lcom/android/okhttp/OkHttpClient;

    invoke-virtual {v7}, Lcom/android/okhttp/OkHttpClient;->getOkResponseCache()Lcom/android/okhttp/OkResponseCache;

    move-result-object v5

    .line 269
    .local v5, "responseCache":Lcom/android/okhttp/OkResponseCache;
    if-eqz v5, :cond_8

    invoke-interface {v5, v4}, Lcom/android/okhttp/OkResponseCache;->get(Lcom/android/okhttp/Request;)Lcom/android/okhttp/Response;

    move-result-object v0

    .line 272
    .local v0, "cacheCandidate":Lcom/android/okhttp/Response;
    :goto_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 273
    .local v2, "now":J
    new-instance v7, Lcom/android/okhttp/internal/http/CacheStrategy$Factory;

    invoke-direct {v7, v2, v3, v4, v0}, Lcom/android/okhttp/internal/http/CacheStrategy$Factory;-><init>(JLcom/android/okhttp/Request;Lcom/android/okhttp/Response;)V

    invoke-virtual {v7}, Lcom/android/okhttp/internal/http/CacheStrategy$Factory;->get()Lcom/android/okhttp/internal/http/CacheStrategy;

    move-result-object v1

    .line 274
    .local v1, "cacheStrategy":Lcom/android/okhttp/internal/http/CacheStrategy;
    iget-object v7, v1, Lcom/android/okhttp/internal/http/CacheStrategy;->source:Lcom/android/okhttp/ResponseSource;

    iput-object v7, p0, Lcom/android/okhttp/internal/http/HttpEngine;->responseSource:Lcom/android/okhttp/ResponseSource;

    .line 275
    iget-object v7, v1, Lcom/android/okhttp/internal/http/CacheStrategy;->networkRequest:Lcom/android/okhttp/Request;

    iput-object v7, p0, Lcom/android/okhttp/internal/http/HttpEngine;->networkRequest:Lcom/android/okhttp/Request;

    .line 276
    iget-object v7, v1, Lcom/android/okhttp/internal/http/CacheStrategy;->cacheResponse:Lcom/android/okhttp/Response;

    iput-object v7, p0, Lcom/android/okhttp/internal/http/HttpEngine;->cacheResponse:Lcom/android/okhttp/Response;

    .line 278
    if-eqz v5, :cond_3

    .line 279
    iget-object v7, p0, Lcom/android/okhttp/internal/http/HttpEngine;->responseSource:Lcom/android/okhttp/ResponseSource;

    invoke-interface {v5, v7}, Lcom/android/okhttp/OkResponseCache;->trackResponse(Lcom/android/okhttp/ResponseSource;)V

    .line 282
    :cond_3
    if-eqz v0, :cond_5

    iget-object v7, p0, Lcom/android/okhttp/internal/http/HttpEngine;->responseSource:Lcom/android/okhttp/ResponseSource;

    sget-object v8, Lcom/android/okhttp/ResponseSource;->NONE:Lcom/android/okhttp/ResponseSource;

    if-eq v7, v8, :cond_4

    iget-object v7, p0, Lcom/android/okhttp/internal/http/HttpEngine;->cacheResponse:Lcom/android/okhttp/Response;

    if-nez v7, :cond_5

    .line 284
    :cond_4
    invoke-virtual {v0}, Lcom/android/okhttp/Response;->body()Lcom/android/okhttp/Response$Body;

    move-result-object v7

    invoke-static {v7}, Lcom/android/okhttp/internal/Util;->closeQuietly(Ljava/io/Closeable;)V

    .line 287
    :cond_5
    iget-object v7, p0, Lcom/android/okhttp/internal/http/HttpEngine;->networkRequest:Lcom/android/okhttp/Request;

    if-eqz v7, :cond_b

    .line 289
    iget-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    if-nez v6, :cond_7

    .line 291
    sget-boolean v6, Lcom/android/okhttp/internal/http/HttpEngine;->SMARTBONDING_ENABLED:Z

    if-eqz v6, :cond_6

    .line 292
    sget-boolean v6, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v6, :cond_6

    iget-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    const-string v7, "new connection"

    invoke-virtual {v6, v7}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 295
    :cond_6
    iget-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->networkRequest:Lcom/android/okhttp/Request;

    invoke-direct {p0, v6}, Lcom/android/okhttp/internal/http/HttpEngine;->connect(Lcom/android/okhttp/Request;)V

    .line 299
    :cond_7
    iget-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    invoke-virtual {v6}, Lcom/android/okhttp/Connection;->getOwner()Ljava/lang/Object;

    move-result-object v6

    if-eq v6, p0, :cond_9

    iget-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    invoke-virtual {v6}, Lcom/android/okhttp/Connection;->isSpdy()Z

    move-result v6

    if-nez v6, :cond_9

    new-instance v6, Ljava/lang/AssertionError;

    invoke-direct {v6}, Ljava/lang/AssertionError;-><init>()V

    throw v6

    .end local v0    # "cacheCandidate":Lcom/android/okhttp/Response;
    .end local v1    # "cacheStrategy":Lcom/android/okhttp/internal/http/CacheStrategy;
    .end local v2    # "now":J
    :cond_8
    move-object v0, v6

    .line 269
    goto :goto_1

    .line 301
    .restart local v0    # "cacheCandidate":Lcom/android/okhttp/Response;
    .restart local v1    # "cacheStrategy":Lcom/android/okhttp/internal/http/CacheStrategy;
    .restart local v2    # "now":J
    :cond_9
    iget-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    invoke-virtual {v6, p0}, Lcom/android/okhttp/Connection;->newTransport(Lcom/android/okhttp/internal/http/HttpEngine;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/okhttp/internal/http/Transport;

    iput-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->transport:Lcom/android/okhttp/internal/http/Transport;

    .line 304
    sget-boolean v6, Lcom/android/okhttp/internal/http/HttpEngine;->SMARTBONDING_ENABLED:Z

    if-eqz v6, :cond_a

    .line 305
    sget-boolean v6, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v6, :cond_a

    iget-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "transport is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/okhttp/internal/http/HttpEngine;->transport:Lcom/android/okhttp/internal/http/Transport;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 311
    :cond_a
    invoke-virtual {p0}, Lcom/android/okhttp/internal/http/HttpEngine;->hasRequestBody()Z

    move-result v6

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->requestBodyOut:Lcom/android/okio/Sink;

    if-nez v6, :cond_0

    .line 312
    iget-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->transport:Lcom/android/okhttp/internal/http/Transport;

    invoke-interface {v6, v4}, Lcom/android/okhttp/internal/http/Transport;->createRequestBody(Lcom/android/okhttp/Request;)Lcom/android/okio/Sink;

    move-result-object v6

    iput-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->requestBodyOut:Lcom/android/okio/Sink;

    goto/16 :goto_0

    .line 317
    :cond_b
    iget-object v7, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    if-eqz v7, :cond_c

    .line 318
    iget-object v7, p0, Lcom/android/okhttp/internal/http/HttpEngine;->client:Lcom/android/okhttp/OkHttpClient;

    invoke-virtual {v7}, Lcom/android/okhttp/OkHttpClient;->getConnectionPool()Lcom/android/okhttp/ConnectionPool;

    move-result-object v7

    iget-object v8, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    invoke-virtual {v7, v8}, Lcom/android/okhttp/ConnectionPool;->recycle(Lcom/android/okhttp/Connection;)V

    .line 319
    iput-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->connection:Lcom/android/okhttp/Connection;

    .line 323
    :cond_c
    iget-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->cacheResponse:Lcom/android/okhttp/Response;

    invoke-virtual {v6}, Lcom/android/okhttp/Response;->newBuilder()Lcom/android/okhttp/Response$Builder;

    move-result-object v6

    iget-object v7, p0, Lcom/android/okhttp/internal/http/HttpEngine;->userRequest:Lcom/android/okhttp/Request;

    invoke-virtual {v6, v7}, Lcom/android/okhttp/Response$Builder;->request(Lcom/android/okhttp/Request;)Lcom/android/okhttp/Response$Builder;

    move-result-object v6

    iget-object v7, p0, Lcom/android/okhttp/internal/http/HttpEngine;->priorResponse:Lcom/android/okhttp/Response;

    invoke-static {v7}, Lcom/android/okhttp/internal/http/HttpEngine;->stripBody(Lcom/android/okhttp/Response;)Lcom/android/okhttp/Response;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/android/okhttp/Response$Builder;->priorResponse(Lcom/android/okhttp/Response;)Lcom/android/okhttp/Response$Builder;

    move-result-object v6

    iget-object v7, p0, Lcom/android/okhttp/internal/http/HttpEngine;->cacheResponse:Lcom/android/okhttp/Response;

    invoke-static {v7}, Lcom/android/okhttp/internal/http/HttpEngine;->stripBody(Lcom/android/okhttp/Response;)Lcom/android/okhttp/Response;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/android/okhttp/Response$Builder;->cacheResponse(Lcom/android/okhttp/Response;)Lcom/android/okhttp/Response$Builder;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/okhttp/Response$Builder;->build()Lcom/android/okhttp/Response;

    move-result-object v6

    iput-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->userResponse:Lcom/android/okhttp/Response;

    .line 328
    iget-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->userResponse:Lcom/android/okhttp/Response;

    invoke-virtual {v6}, Lcom/android/okhttp/Response;->body()Lcom/android/okhttp/Response$Body;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 329
    iget-object v6, p0, Lcom/android/okhttp/internal/http/HttpEngine;->userResponse:Lcom/android/okhttp/Response;

    invoke-virtual {v6}, Lcom/android/okhttp/Response;->body()Lcom/android/okhttp/Response$Body;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/okhttp/Response$Body;->source()Lcom/android/okio/Source;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/android/okhttp/internal/http/HttpEngine;->initContentStream(Lcom/android/okio/Source;)V

    goto/16 :goto_0
.end method

.method public writingRequestHeaders()V
    .locals 4

    .prologue
    .line 418
    iget-wide v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->sentRequestMillis:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 419
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/okhttp/internal/http/HttpEngine;->sentRequestMillis:J

    .line 420
    return-void
.end method
