.class final Lcom/android/okhttp/internal/http/SBServiceAPI$1;
.super Ljava/lang/Object;
.source "SBServiceAPI.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/okhttp/internal/http/SBServiceAPI;->getAddrsByHost(JLjava/lang/String;IZ)[Ljava/net/InetAddress;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$host:Ljava/lang/String;

.field final synthetic val$retAddrs:Ljava/util/LinkedList;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/util/LinkedList;)V
    .locals 0

    .prologue
    .line 179
    iput-object p1, p0, Lcom/android/okhttp/internal/http/SBServiceAPI$1;->val$host:Ljava/lang/String;

    iput-object p2, p0, Lcom/android/okhttp/internal/http/SBServiceAPI$1;->val$retAddrs:Ljava/util/LinkedList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 182
    :try_start_0
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_0

    const-string v3, "SBServiceAPI: start run2: get from Default"

    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiratLog;->info(Ljava/lang/String;)V

    .line 183
    :cond_0
    iget-object v3, p0, Lcom/android/okhttp/internal/http/SBServiceAPI$1;->val$host:Ljava/lang/String;

    invoke-static {v3}, Ljava/net/InetAddress;->getAllByName(Ljava/lang/String;)[Ljava/net/InetAddress;

    move-result-object v0

    .line 184
    .local v0, "addrs2":[Ljava/net/InetAddress;
    if-eqz v0, :cond_1

    array-length v3, v0

    if-lez v3, :cond_1

    .line 185
    iget-object v4, p0, Lcom/android/okhttp/internal/http/SBServiceAPI$1;->val$retAddrs:Ljava/util/LinkedList;

    monitor-enter v4
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 186
    :try_start_1
    iget-object v3, p0, Lcom/android/okhttp/internal/http/SBServiceAPI$1;->val$retAddrs:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/util/LinkedList;->size()I

    move-result v3

    if-lez v3, :cond_2

    monitor-exit v4

    .line 198
    .end local v0    # "addrs2":[Ljava/net/InetAddress;
    :cond_1
    :goto_0
    return-void

    .line 187
    .restart local v0    # "addrs2":[Ljava/net/InetAddress;
    :cond_2
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    array-length v3, v0

    if-ge v2, v3, :cond_4

    .line 188
    iget-object v3, p0, Lcom/android/okhttp/internal/http/SBServiceAPI$1;->val$retAddrs:Ljava/util/LinkedList;

    aget-object v5, v0, v2

    invoke-virtual {v3, v5}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 189
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SBServiceAPI: InetAddress.getAllByName "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v5, v0, v2

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiratLog;->info(Ljava/lang/String;)V

    .line 187
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 191
    :cond_4
    iget-object v3, p0, Lcom/android/okhttp/internal/http/SBServiceAPI$1;->val$retAddrs:Ljava/util/LinkedList;

    invoke-virtual {v3}, Ljava/lang/Object;->notifyAll()V

    .line 192
    monitor-exit v4

    goto :goto_0

    .end local v2    # "i":I
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v3
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    .line 195
    .end local v0    # "addrs2":[Ljava/net/InetAddress;
    :catch_0
    move-exception v1

    .line 196
    .local v1, "e":Ljava/lang/Throwable;
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_1

    invoke-static {v1}, Lcom/android/okhttp/internal/http/MultiratLog;->info(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
