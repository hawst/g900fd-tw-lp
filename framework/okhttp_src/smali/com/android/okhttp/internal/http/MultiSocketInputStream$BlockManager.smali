.class Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;
.super Ljava/lang/Object;
.source "MultiSocketInputStream.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/okhttp/internal/http/MultiSocketInputStream;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "BlockManager"
.end annotation


# static fields
.field public static final BLOCKINFOSIZE:I = 0x4

.field public static final MIN_BLOCKSIZE_TO_HANDOVER:I = 0x20000


# instance fields
.field protected headerTime:[J

.field private httpRequestID:[I

.field private minNotReadBlockID:I

.field protected reconnTime:[J

.field private socketSpeed:[J

.field final synthetic this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

.field protected toBeReadLen:[J


# direct methods
.method public constructor <init>(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 4006
    iput-object p1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4007
    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->socketNumber:I
    invoke-static {p1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3300(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v1

    new-array v1, v1, [I

    iput-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->httpRequestID:[I

    .line 4008
    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->socketNumber:I
    invoke-static {p1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3300(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v1

    new-array v1, v1, [J

    iput-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->socketSpeed:[J

    .line 4009
    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->socketNumber:I
    invoke-static {p1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3300(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v1

    new-array v1, v1, [J

    iput-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->reconnTime:[J

    .line 4010
    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->socketNumber:I
    invoke-static {p1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3300(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v1

    new-array v1, v1, [J

    iput-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->headerTime:[J

    .line 4011
    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->socketNumber:I
    invoke-static {p1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3300(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v1

    new-array v1, v1, [J

    iput-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->toBeReadLen:[J

    .line 4012
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->socketNumber:I
    invoke-static {p1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3300(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 4013
    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->httpRequestID:[I

    const/4 v2, -0x1

    aput v2, v1, v0

    .line 4014
    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->socketSpeed:[J

    aput-wide v4, v1, v0

    .line 4015
    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->reconnTime:[J

    aput-wide v4, v1, v0

    .line 4016
    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->headerTime:[J

    aput-wide v4, v1, v0

    .line 4017
    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->toBeReadLen:[J

    aput-wide v4, v1, v0

    .line 4012
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 4019
    :cond_0
    const/4 v1, 0x0

    iput v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->minNotReadBlockID:I

    .line 4020
    return-void
.end method

.method static synthetic access$6000(Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;)[J
    .locals 1
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;

    .prologue
    .line 3969
    iget-object v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->socketSpeed:[J

    return-object v0
.end method

.method private getBlockForWithID(II)[J
    .locals 5
    .param p1, "sockID"    # I
    .param p2, "blockid"    # I

    .prologue
    const/4 v4, 0x0

    .line 4303
    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static {v1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B

    move-result-object v1

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {p2, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 4304
    .local v0, "tmpID":I
    :cond_0
    if-ltz v0, :cond_1

    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static {v1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B

    move-result-object v1

    array-length v1, v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static {v1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B

    move-result-object v1

    aget-byte v1, v1, v0

    if-lez v1, :cond_1

    .line 4305
    add-int/lit8 v0, v0, 0x1

    .line 4307
    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static {v1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B

    move-result-object v1

    array-length v1, v1

    if-ne v0, v1, :cond_0

    .line 4308
    sget-boolean v1, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "id reach the maximum "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B

    move-result-object v3

    array-length v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 4312
    :cond_1
    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static {v1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B

    move-result-object v1

    array-length v1, v1

    if-ne v0, v1, :cond_4

    .line 4313
    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static {v1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B

    move-result-object v1

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {p2, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 4314
    :cond_2
    if-ltz v0, :cond_4

    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static {v1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B

    move-result-object v1

    array-length v1, v1

    if-ge v0, v1, :cond_4

    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static {v1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B

    move-result-object v1

    aget-byte v1, v1, v0

    if-lez v1, :cond_4

    .line 4315
    add-int/lit8 v0, v0, -0x1

    .line 4316
    iget v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->minNotReadBlockID:I

    if-ne v0, v1, :cond_2

    .line 4317
    sget-boolean v1, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "id reach the minimum "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->minNotReadBlockID:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 4318
    :cond_3
    new-array v1, v4, [J

    .line 4327
    :goto_0
    return-object v1

    .line 4322
    :cond_4
    if-ltz v0, :cond_5

    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static {v1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B

    move-result-object v1

    array-length v1, v1

    if-lt v0, v1, :cond_7

    .line 4323
    :cond_5
    sget-boolean v1, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "get an illegal tmpID "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 4324
    :cond_6
    new-array v1, v4, [J

    goto :goto_0

    .line 4326
    :cond_7
    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->httpRequestID:[I

    aput v0, v1, p1

    .line 4327
    invoke-direct {p0, v0, p1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->getNewBuf(II)[J

    move-result-object v1

    goto :goto_0
.end method

.method private getContinuousChunk(I)[J
    .locals 20
    .param p1, "sockID"    # I

    .prologue
    .line 4393
    const/4 v11, 0x0

    .line 4394
    .local v11, "notReadContinuousBlock":I
    const-wide/16 v4, -0x1

    .line 4395
    .local v4, "startOffset":J
    const-wide/16 v14, -0x1

    .line 4396
    .local v14, "endOffset":J
    const/4 v6, 0x0

    .line 4397
    .local v6, "blockId":I
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_0

    .line 4398
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v3

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "mOffset = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mOffset:J
    invoke-static {v8}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 4399
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v3

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Inside getContinuousChunk(), mReadBlockNumber="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->readBlockNumber:Ljava/lang/Integer;
    invoke-static {v8}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$5900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 4401
    :cond_0
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->readBlockNumber:Ljava/lang/Integer;
    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$5900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-static {v3, v7}, Ljava/lang/Math;->max(II)I

    move-result v16

    .local v16, "i":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B

    move-result-object v3

    array-length v3, v3

    move/from16 v0, v16

    if-ge v0, v3, :cond_d

    .line 4403
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v3

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "mBlockStatus["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, v16

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "] = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static {v8}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B

    move-result-object v8

    aget-byte v8, v8, v16

    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->statusToStr(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 4404
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B

    move-result-object v3

    aget-byte v3, v3, v16

    packed-switch v3, :pswitch_data_0

    .line 4401
    :cond_2
    :goto_1
    :pswitch_0
    add-int/lit8 v16, v16, 0x1

    goto :goto_0

    .line 4407
    :pswitch_1
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v3

    const-string v7, "Inside NOT_READ BLOCK"

    invoke-virtual {v3, v7}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 4410
    :cond_3
    add-int/lit8 v11, v11, 0x1

    .line 4412
    const-wide/16 v8, -0x1

    cmp-long v3, v4, v8

    if-nez v3, :cond_4

    .line 4413
    move/from16 v0, v16

    int-to-long v8, v0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->BlockSize:J
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v18

    mul-long v8, v8, v18

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mOffset:J
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v18

    add-long v4, v8, v18

    .line 4414
    move/from16 v6, v16

    .line 4418
    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B

    move-result-object v3

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    move/from16 v0, v16

    if-ne v0, v3, :cond_5

    .line 4419
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->totalLengthToBeRead:J
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$300(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v8

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mOriOffset:J
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v18

    add-long v8, v8, v18

    const-wide/16 v18, 0x1

    sub-long v14, v8, v18

    goto :goto_1

    .line 4421
    :cond_5
    add-int v3, v6, v11

    int-to-long v8, v3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->BlockSize:J
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v18

    mul-long v8, v8, v18

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mOffset:J
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v18

    add-long v8, v8, v18

    const-wide/16 v18, 0x1

    sub-long v14, v8, v18

    .line 4424
    goto :goto_1

    .line 4426
    :pswitch_2
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_6

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v3

    const-string v7, "Inside BLOCKED BLOCK"

    invoke-virtual {v3, v7}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 4427
    :cond_6
    const/4 v3, 0x1

    if-lt v11, v3, :cond_7

    .line 4428
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B

    move-result-object v3

    array-length v0, v3

    move/from16 v16, v0

    goto/16 :goto_1

    .line 4429
    :cond_7
    if-nez v11, :cond_2

    .line 4430
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->inBuffer:Ljava/util/HashMap;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Ljava/util/HashMap;

    move-result-object v3

    new-instance v7, Ljava/lang/Integer;

    move/from16 v0, v16

    invoke-direct {v7, v0}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

    .line 4431
    .local v2, "buf":Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;
    invoke-virtual {v2}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->getOffset()J

    move-result-wide v4

    .line 4432
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_8

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v3

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Inside BLOCKED BLOCK: startOffset: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 4433
    :cond_8
    move/from16 v6, v16

    .line 4434
    add-int/lit8 v11, v11, 0x1

    .line 4435
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B

    move-result-object v3

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    move/from16 v0, v16

    if-ne v0, v3, :cond_9

    .line 4436
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->totalLengthToBeRead:J
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$300(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v8

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mOriOffset:J
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v18

    add-long v8, v8, v18

    const-wide/16 v18, 0x1

    sub-long v14, v8, v18

    goto/16 :goto_1

    .line 4438
    :cond_9
    add-int v3, v6, v11

    int-to-long v8, v3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->BlockSize:J
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v18

    mul-long v8, v8, v18

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mOffset:J
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v18

    add-long v8, v8, v18

    const-wide/16 v18, 0x1

    sub-long v14, v8, v18

    goto/16 :goto_1

    .line 4444
    .end local v2    # "buf":Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;
    :pswitch_3
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_a

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v3

    const-string v7, "Inside FULLREAD BLOCK"

    invoke-virtual {v3, v7}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 4445
    :cond_a
    const/4 v3, 0x1

    if-lt v11, v3, :cond_2

    .line 4446
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B

    move-result-object v3

    array-length v0, v3

    move/from16 v16, v0

    .line 4447
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_b

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v3

    const-string v7, "Inside FULLREAD BLOCK: exiting this loop as notReadContinuousBlock >= 1"

    invoke-virtual {v3, v7}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 4448
    :cond_b
    add-int v3, v6, v11

    int-to-long v8, v3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->BlockSize:J
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v18

    mul-long v8, v8, v18

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mOffset:J
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v18

    add-long v8, v8, v18

    const-wide/16 v18, 0x1

    sub-long v14, v8, v18

    goto/16 :goto_1

    .line 4453
    :pswitch_4
    const/4 v3, 0x1

    if-lt v11, v3, :cond_2

    .line 4454
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B

    move-result-object v3

    array-length v0, v3

    move/from16 v16, v0

    .line 4455
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_c

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v3

    const-string v7, "Inside READING Block: exiting this loop as notReadContinuousBlock >= 1"

    invoke-virtual {v3, v7}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 4456
    :cond_c
    add-int v3, v6, v11

    int-to-long v8, v3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->BlockSize:J
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v18

    mul-long v8, v8, v18

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mOffset:J
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v18

    add-long v8, v8, v18

    const-wide/16 v18, 0x1

    sub-long v14, v8, v18

    goto/16 :goto_1

    .line 4465
    :cond_d
    const/4 v3, 0x1

    if-lt v11, v3, :cond_13

    .line 4467
    const/4 v3, 0x4

    new-array v12, v3, [J

    .line 4468
    .local v12, "block":[J
    const/4 v3, 0x0

    aput-wide v4, v12, v3

    .line 4469
    const/4 v3, 0x1

    aput-wide v14, v12, v3

    .line 4470
    const/4 v3, 0x2

    sub-long v8, v14, v4

    const-wide/16 v18, 0x1

    add-long v8, v8, v18

    aput-wide v8, v12, v3

    .line 4471
    const/4 v3, 0x3

    int-to-long v8, v6

    aput-wide v8, v12, v3

    .line 4474
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B

    move-result-object v3

    aget-byte v3, v3, v6

    const/4 v7, -0x1

    if-eq v3, v7, :cond_12

    .line 4475
    const/4 v13, 0x0

    .line 4476
    .local v13, "endIndex":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B

    move-result-object v3

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    if-ne v6, v3, :cond_e

    .line 4477
    const/4 v3, 0x2

    aget-wide v8, v12, v3

    long-to-int v13, v8

    .line 4483
    :goto_2
    new-instance v2, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    int-to-long v7, v13

    new-instance v9, Ljava/util/LinkedList;

    invoke-direct {v9}, Ljava/util/LinkedList;-><init>()V

    move/from16 v10, p1

    invoke-direct/range {v2 .. v11}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;-><init>(Lcom/android/okhttp/internal/http/MultiSocketInputStream;JIJLjava/util/LinkedList;II)V

    .line 4485
    .restart local v2    # "buf":Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->inBuffer:Ljava/util/HashMap;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Ljava/util/HashMap;

    move-result-object v3

    new-instance v7, Ljava/lang/Integer;

    invoke-direct {v7, v6}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3, v7, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 4487
    monitor-enter v2

    .line 4488
    const/16 v17, 0x1

    .local v17, "j":I
    :goto_3
    move/from16 v0, v17

    if-ge v0, v11, :cond_10

    .line 4489
    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B

    move-result-object v3

    add-int v7, v6, v17

    const/4 v8, 0x1

    aput-byte v8, v3, v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4488
    add-int/lit8 v17, v17, 0x1

    goto :goto_3

    .line 4479
    .end local v2    # "buf":Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;
    .end local v17    # "j":I
    :cond_e
    const/4 v3, 0x2

    aget-wide v8, v12, v3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->BlockSize:J
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v18

    cmp-long v3, v8, v18

    if-lez v3, :cond_f

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->BlockSize:J
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v8

    :goto_4
    long-to-int v13, v8

    goto :goto_2

    :cond_f
    const/4 v3, 0x2

    aget-wide v8, v12, v3

    goto :goto_4

    .line 4491
    .restart local v2    # "buf":Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;
    .restart local v17    # "j":I
    :cond_10
    :try_start_1
    monitor-exit v2

    .line 4501
    .end local v2    # "buf":Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;
    .end local v12    # "block":[J
    .end local v13    # "endIndex":I
    .end local v17    # "j":I
    :cond_11
    :goto_5
    return-object v12

    .line 4491
    .restart local v2    # "buf":Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;
    .restart local v12    # "block":[J
    .restart local v13    # "endIndex":I
    .restart local v17    # "j":I
    :catchall_0
    move-exception v3

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3

    .line 4492
    .end local v2    # "buf":Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;
    .end local v13    # "endIndex":I
    .end local v17    # "j":I
    :cond_12
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B

    move-result-object v3

    aget-byte v3, v3, v6

    const/4 v7, -0x1

    if-ne v3, v7, :cond_11

    .line 4493
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->inBuffer:Ljava/util/HashMap;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Ljava/util/HashMap;

    move-result-object v3

    new-instance v7, Ljava/lang/Integer;

    invoke-direct {v7, v6}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

    .line 4494
    .restart local v2    # "buf":Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;
    monitor-enter v2

    .line 4495
    :try_start_2
    invoke-virtual {v2, v11}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->setTotalChunks(I)V

    .line 4496
    move/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->changeSockID(I)V

    .line 4497
    monitor-exit v2

    goto :goto_5

    :catchall_1
    move-exception v3

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v3

    .line 4501
    .end local v2    # "buf":Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;
    .end local v12    # "block":[J
    :cond_13
    const/4 v3, 0x0

    new-array v12, v3, [J

    goto :goto_5

    .line 4404
    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method private getMinNotReadBlock(I)[J
    .locals 5
    .param p1, "sockID"    # I

    .prologue
    const/4 v4, 0x0

    .line 4278
    iget v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->minNotReadBlockID:I

    iget-object v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static {v2}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B

    move-result-object v2

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 4279
    .local v0, "tmpID":I
    :cond_0
    if-ltz v0, :cond_2

    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static {v1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B

    move-result-object v1

    array-length v1, v1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static {v1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B

    move-result-object v1

    aget-byte v1, v1, v0

    if-lez v1, :cond_2

    .line 4280
    add-int/lit8 v0, v0, 0x1

    .line 4282
    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static {v1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B

    move-result-object v1

    array-length v1, v1

    if-ne v0, v1, :cond_0

    .line 4283
    sget-boolean v1, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "id reach the maximum "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B

    move-result-object v3

    array-length v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 4284
    :cond_1
    new-array v1, v4, [J

    .line 4293
    :goto_0
    return-object v1

    .line 4287
    :cond_2
    if-ltz v0, :cond_3

    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static {v1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B

    move-result-object v1

    array-length v1, v1

    if-lt v0, v1, :cond_5

    .line 4288
    :cond_3
    sget-boolean v1, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "get an illegal tmpID "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 4289
    :cond_4
    new-array v1, v4, [J

    goto :goto_0

    .line 4291
    :cond_5
    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->minNotReadBlockID:I

    .line 4292
    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->httpRequestID:[I

    aput v0, v1, p1

    .line 4293
    invoke-direct {p0, v0, p1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->getNewBuf(II)[J

    move-result-object v1

    goto :goto_0
.end method

.method private getMinNotReadBlockID()I
    .locals 4

    .prologue
    .line 4262
    iget v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->minNotReadBlockID:I

    iget-object v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static {v2}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B

    move-result-object v2

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 4263
    .local v0, "tmpID":I
    :cond_0
    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static {v1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B

    move-result-object v1

    aget-byte v1, v1, v0

    if-lez v1, :cond_2

    .line 4264
    add-int/lit8 v0, v0, 0x1

    .line 4265
    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static {v1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B

    move-result-object v1

    array-length v1, v1

    if-ne v0, v1, :cond_0

    .line 4266
    sget-boolean v1, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "in getMinNotReadBlockID, id reach the maximum "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B

    move-result-object v3

    array-length v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 4267
    :cond_1
    const/4 v1, -0x1

    .line 4270
    :goto_0
    return v1

    :cond_2
    move v1, v0

    goto :goto_0
.end method

.method private getNewBuf(II)[J
    .locals 17
    .param p1, "blockID"    # I
    .param p2, "sockID"    # I

    .prologue
    .line 4337
    const/4 v3, 0x4

    new-array v13, v3, [J

    .line 4338
    .local v13, "range":[J
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->inBuffer:Ljava/util/HashMap;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Ljava/util/HashMap;

    move-result-object v16

    monitor-enter v16

    .line 4340
    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B

    move-result-object v3

    aget-byte v3, v3, p1

    const/4 v4, -0x1

    if-ne v3, v4, :cond_1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->inBuffer:Ljava/util/HashMap;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Ljava/util/HashMap;

    move-result-object v3

    new-instance v4, Ljava/lang/Integer;

    move/from16 v0, p1

    invoke-direct {v4, v0}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 4341
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "block "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "is blocked and now it is occupied again"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 4342
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->inBuffer:Ljava/util/HashMap;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Ljava/util/HashMap;

    move-result-object v3

    new-instance v4, Ljava/lang/Integer;

    move/from16 v0, p1

    invoke-direct {v4, v0}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

    .line 4343
    .local v12, "buf":Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;
    const/4 v3, 0x0

    invoke-virtual {v12}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->getOffset()J

    move-result-wide v4

    aput-wide v4, v13, v3

    .line 4344
    const/4 v3, 0x1

    invoke-virtual {v12}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->getOffset()J

    move-result-wide v4

    invoke-virtual {v12}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->getToBeReadLength()J

    move-result-wide v6

    add-long/2addr v4, v6

    const-wide/16 v6, 0x1

    sub-long/2addr v4, v6

    aput-wide v4, v13, v3

    .line 4345
    const/4 v3, 0x2

    invoke-virtual {v12}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->getToBeReadLength()J

    move-result-wide v4

    aput-wide v4, v13, v3

    .line 4346
    const/4 v3, 0x3

    move/from16 v0, p1

    int-to-long v4, v0

    aput-wide v4, v13, v3

    .line 4347
    move/from16 v0, p2

    invoke-virtual {v12, v0}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->changeSockID(I)V

    .line 4371
    .end local v12    # "buf":Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;
    :goto_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B

    move-result-object v3

    const/4 v4, 0x1

    aput-byte v4, v3, p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4373
    :try_start_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->inBuffer:Ljava/util/HashMap;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Ljava/util/HashMap;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->notifyAll()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 4376
    :goto_1
    :try_start_2
    monitor-exit v16

    .line 4377
    .end local v13    # "range":[J
    :goto_2
    return-object v13

    .line 4351
    .restart local v13    # "range":[J
    :cond_1
    move/from16 v0, p1

    int-to-long v4, v0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->BlockSize:J
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v6

    mul-long/2addr v4, v6

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mOffset:J
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v6

    add-long v14, v4, v6

    .line 4352
    .local v14, "start":J
    const/4 v3, 0x0

    aput-wide v14, v13, v3

    .line 4354
    const/4 v3, 0x0

    aget-wide v4, v13, v3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mOriOffset:J
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v6

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->totalLengthToBeRead:J
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$300(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v8

    add-long/2addr v6, v8

    cmp-long v3, v4, v6

    if-ltz v3, :cond_3

    .line 4355
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "start("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x0

    aget-wide v6, v13, v5

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") >= totalLength("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mOriOffset:J
    invoke-static {v5}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v6

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->totalLengthToBeRead:J
    invoke-static {v5}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$300(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v8

    add-long/2addr v6, v8

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "), break"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 4356
    :cond_2
    const/4 v3, 0x0

    new-array v13, v3, [J

    .end local v13    # "range":[J
    monitor-exit v16

    goto :goto_2

    .line 4376
    .end local v14    # "start":J
    :catchall_0
    move-exception v3

    monitor-exit v16
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3

    .line 4358
    .restart local v13    # "range":[J
    .restart local v14    # "start":J
    :cond_3
    :try_start_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B

    move-result-object v3

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    move/from16 v0, p1

    if-ne v0, v3, :cond_5

    .line 4359
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mOriOffset:J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->totalLengthToBeRead:J
    invoke-static {v6}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$300(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v6

    add-long/2addr v4, v6

    const-wide/16 v6, 0x1

    sub-long/2addr v4, v6

    aput-wide v4, v13, v3

    .line 4360
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_4

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "set for tail chunk "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x1

    aget-wide v6, v13, v5

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 4365
    :cond_4
    :goto_3
    const/4 v3, 0x2

    const/4 v4, 0x1

    aget-wide v4, v13, v4

    const/4 v6, 0x0

    aget-wide v6, v13, v6

    sub-long/2addr v4, v6

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    aput-wide v4, v13, v3

    .line 4366
    const/4 v3, 0x3

    move/from16 v0, p1

    int-to-long v4, v0

    aput-wide v4, v13, v3

    .line 4368
    new-instance v2, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    const/4 v4, 0x0

    aget-wide v4, v13, v4

    const/4 v6, 0x2

    aget-wide v6, v13, v6

    long-to-int v6, v6

    int-to-long v7, v6

    new-instance v9, Ljava/util/LinkedList;

    invoke-direct {v9}, Ljava/util/LinkedList;-><init>()V

    const/4 v11, 0x1

    move/from16 v6, p1

    move/from16 v10, p2

    invoke-direct/range {v2 .. v11}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;-><init>(Lcom/android/okhttp/internal/http/MultiSocketInputStream;JIJLjava/util/LinkedList;II)V

    .line 4369
    .local v2, "dbuf":Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->inBuffer:Ljava/util/HashMap;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Ljava/util/HashMap;

    move-result-object v3

    new-instance v4, Ljava/lang/Integer;

    move/from16 v0, p1

    invoke-direct {v4, v0}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3, v4, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 4363
    .end local v2    # "dbuf":Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;
    :cond_5
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->BlockSize:J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v4

    add-long/2addr v4, v14

    const-wide/16 v6, 0x1

    sub-long/2addr v4, v6

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mOriOffset:J
    invoke-static {v6}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v6

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->totalLengthToBeRead:J
    invoke-static {v8}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$300(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v8

    add-long/2addr v6, v8

    const-wide/16 v8, 0x1

    sub-long/2addr v6, v8

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    aput-wide v4, v13, v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_3

    .line 4375
    .end local v14    # "start":J
    :catch_0
    move-exception v3

    goto/16 :goto_1
.end method

.method private statusToStr(I)Ljava/lang/String;
    .locals 1
    .param p1, "status"    # I

    .prologue
    .line 4536
    packed-switch p1, :pswitch_data_0

    .line 4543
    const-string v0, "UNKNOWN"

    :goto_0
    return-object v0

    .line 4537
    :pswitch_0
    const-string v0, "BLOCKED"

    goto :goto_0

    .line 4538
    :pswitch_1
    const-string v0, "NOT_READ"

    goto :goto_0

    .line 4539
    :pswitch_2
    const-string v0, "OCCUPIED"

    goto :goto_0

    .line 4540
    :pswitch_3
    const-string v0, "READING"

    goto :goto_0

    .line 4541
    :pswitch_4
    const-string v0, "FULLREAD"

    goto :goto_0

    .line 4542
    :pswitch_5
    const-string v0, "CLEARED"

    goto :goto_0

    .line 4536
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method


# virtual methods
.method public block(II)V
    .locals 2
    .param p1, "blockID"    # I
    .param p2, "socketID"    # I

    .prologue
    const/4 v1, -0x1

    .line 4062
    iget-object v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static {v0}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B

    move-result-object v0

    aput-byte v1, v0, p1

    .line 4063
    iget v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->minNotReadBlockID:I

    invoke-static {v0, p1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->minNotReadBlockID:I

    .line 4064
    iget-object v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->httpRequestID:[I

    aput v1, v0, p2

    .line 4065
    return-void
.end method

.method public blockStatusToStr()Ljava/lang/String;
    .locals 6

    .prologue
    .line 4509
    new-instance v3, Ljava/lang/StringBuffer;

    const-string v4, "BlockStatus: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 4510
    .local v3, "str":Ljava/lang/StringBuffer;
    iget-object v4, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B

    move-result-object v4

    array-length v4, v4

    add-int/lit8 v2, v4, -0x1

    .line 4511
    .local v2, "min":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v4, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B

    move-result-object v4

    array-length v4, v4

    if-ge v0, v4, :cond_0

    .line 4512
    iget-object v4, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B

    move-result-object v4

    aget-byte v4, v4, v0

    const/4 v5, 0x4

    if-ge v4, v5, :cond_2

    .line 4513
    add-int/lit8 v4, v0, -0x2

    const/4 v5, 0x0

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 4517
    :cond_0
    add-int/lit8 v4, v2, 0x2

    iget-object v5, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static {v5}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B

    move-result-object v5

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 4518
    .local v1, "max":I
    iget-object v4, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B

    move-result-object v4

    array-length v4, v4

    add-int/lit8 v0, v4, -0x1

    :goto_1
    if-le v0, v2, :cond_1

    .line 4519
    iget-object v4, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B

    move-result-object v4

    aget-byte v4, v4, v0

    if-eqz v4, :cond_3

    .line 4520
    add-int/lit8 v4, v0, 0x1

    iget-object v5, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static {v5}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B

    move-result-object v5

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 4525
    :cond_1
    move v0, v2

    :goto_2
    if-gt v0, v1, :cond_4

    .line 4526
    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, "-"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    iget-object v5, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static {v5}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B

    move-result-object v5

    aget-byte v5, v5, v0

    invoke-direct {p0, v5}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->statusToStr(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 4525
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 4511
    .end local v1    # "max":I
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 4518
    .restart local v1    # "max":I
    :cond_3
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 4528
    :cond_4
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method public getMaxFetchingBlock()I
    .locals 3

    .prologue
    .line 4056
    iget-object v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->httpRequestID:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->httpRequestID:[I

    const/4 v2, 0x1

    aget v1, v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method public getNextHTTPBlock(ILcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;)[J
    .locals 32
    .param p1, "sockID"    # I
    .param p2, "thisBuf"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

    .prologue
    .line 4073
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    move/from16 v1, p1

    # invokes: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->getOtherSocketID(I)I
    invoke-static {v0, v1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1600(Lcom/android/okhttp/internal/http/MultiSocketInputStream;I)I

    move-result v17

    .line 4074
    .local v17, "otherSockID":I
    sget-boolean v28, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v28, :cond_0

    .line 4075
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v28, v0

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static/range {v28 .. v28}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v28

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "getNextHTTPBlock("

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string v30, ","

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {p0 .. p0}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 4076
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v28, v0

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static/range {v28 .. v28}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v28

    invoke-virtual/range {p0 .. p0}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->blockStatusToStr()Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Lcom/android/okhttp/internal/http/MultiratLog;->d(Ljava/lang/String;)V

    .line 4081
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v28, v0

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->requestHandlers:[Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;
    invoke-static/range {v28 .. v28}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;

    move-result-object v28

    aget-object v28, v28, p1

    move-object/from16 v0, v28

    iget v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->rrStatus:I

    move/from16 v28, v0

    const/16 v29, -0x1

    move/from16 v0, v28

    move/from16 v1, v29

    if-ne v0, v1, :cond_1

    .line 4083
    const/16 v28, 0x1

    move/from16 v0, v28

    new-array v0, v0, [J

    move-object/from16 v24, v0

    const/16 v28, 0x0

    const-wide/16 v30, -0x1

    aput-wide v30, v24, v28

    .line 4251
    :goto_0
    return-object v24

    .line 4085
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v28, v0

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->requestHandlers:[Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;
    invoke-static/range {v28 .. v28}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;

    move-result-object v28

    aget-object v28, v28, v17

    move-object/from16 v0, v28

    iget v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->rrStatus:I

    move/from16 v28, v0

    const/16 v29, -0x1

    move/from16 v0, v28

    move/from16 v1, v29

    if-ne v0, v1, :cond_2

    .line 4088
    invoke-direct/range {p0 .. p1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->getContinuousChunk(I)[J

    move-result-object v24

    goto :goto_0

    .line 4092
    :cond_2
    if-nez p2, :cond_4

    .line 4093
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v28, v0

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mFasterInterface:I
    invoke-static/range {v28 .. v28}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v28

    move/from16 v0, p1

    move/from16 v1, v28

    if-ne v0, v1, :cond_3

    const/4 v6, 0x0

    .line 4094
    .local v6, "candidateChunk":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v28, v0

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static/range {v28 .. v28}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B

    move-result-object v28

    move-object/from16 v0, v28

    array-length v0, v0

    move/from16 v28, v0

    move/from16 v0, v28

    if-ge v6, v0, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v28, v0

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static/range {v28 .. v28}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B

    move-result-object v28

    aget-byte v28, v28, v6

    if-nez v28, :cond_4

    .line 4095
    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v0, v1, v6}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->getBlockForWithID(II)[J

    move-result-object v24

    goto :goto_0

    .line 4093
    .end local v6    # "candidateChunk":I
    :cond_3
    const/4 v6, 0x1

    goto :goto_1

    .line 4103
    :cond_4
    const-wide/16 v8, 0x0

    .line 4104
    .local v8, "fRatio":D
    const/16 v22, 0x1

    .line 4107
    .local v22, "ratio":I
    if-eqz p2, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->httpRequestID:[I

    move-object/from16 v28, v0

    aget v28, v28, v17

    if-gez v28, :cond_7

    .line 4108
    :cond_5
    sget-boolean v28, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v28, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v28, v0

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static/range {v28 .. v28}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v28

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "first time getNextHTTPBlock for socket "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string v30, " or the other socket is not active"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 4109
    :cond_6
    invoke-direct/range {p0 .. p1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->getMinNotReadBlock(I)[J

    move-result-object v24

    goto/16 :goto_0

    .line 4112
    :cond_7
    const-wide/16 v26, 0x0

    .line 4113
    .local v26, "thisSpeed":D
    const-wide/16 v20, 0x0

    .line 4114
    .local v20, "otherSpeed":D
    const-wide/16 v14, 0x0

    .line 4115
    .local v14, "lTS":J
    const-wide/16 v12, 0x0

    .line 4116
    .local v12, "lOS":J
    sget-boolean v28, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v28, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v28, v0

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static/range {v28 .. v28}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v28

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "thisBuf.getSockID="

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {p2 .. p2}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->getSockID()I

    move-result v30

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string v30, ", current sockID="

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 4118
    :cond_8
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->socketSpeed:[J

    move-object/from16 v28, v0

    aget-wide v14, v28, p1

    .line 4119
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->socketSpeed:[J

    move-object/from16 v28, v0

    aget-wide v12, v28, v17

    .line 4120
    sget-boolean v28, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v28, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v28, v0

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static/range {v28 .. v28}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v28

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "calculate speed "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    invoke-virtual {v0, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string v30, ":"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    invoke-virtual {v0, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 4122
    :cond_9
    const-wide/16 v28, 0x0

    cmp-long v28, v14, v28

    if-nez v28, :cond_a

    .line 4123
    invoke-direct/range {p0 .. p1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->getMinNotReadBlock(I)[J
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v24

    goto/16 :goto_0

    .line 4125
    :cond_a
    long-to-double v0, v14

    move-wide/from16 v26, v0

    .line 4126
    long-to-double v0, v12

    move-wide/from16 v20, v0

    .line 4134
    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->MIN_RATIO_FOR_ONLY_ONE_INF_0V1:I
    invoke-static {}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$4900()I

    move-result v28

    if-ltz v28, :cond_e

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->MIN_RATIO_FOR_ONLY_ONE_INF_0V1:I
    invoke-static {}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$4900()I

    move-result v28

    move/from16 v0, v28

    int-to-double v0, v0

    move-wide/from16 v28, v0

    mul-double v28, v28, v20

    cmpl-double v28, v26, v28

    if-gtz v28, :cond_b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v28, v0

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->requestHandlers:[Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;
    invoke-static/range {v28 .. v28}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;

    move-result-object v28

    aget-object v28, v28, v17

    move-object/from16 v0, v28

    iget v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->rrExceptionCount:I

    move/from16 v28, v0

    sget v29, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->MAX_EXCEPTION_COUNT:I

    move/from16 v0, v28

    move/from16 v1, v29

    if-le v0, v1, :cond_e

    .line 4136
    :cond_b
    sget-boolean v28, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v28, :cond_c

    .line 4137
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v28, v0

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static/range {v28 .. v28}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v28

    const-string v29, "The speed of other socket is slow so closing and setting the thread status as RR_STOPPED"

    invoke-virtual/range {v28 .. v29}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 4141
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v28, v0

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->requestHandlers:[Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;
    invoke-static/range {v28 .. v28}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;

    move-result-object v28

    aget-object v28, v28, v17

    invoke-virtual/range {v28 .. v28}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->closeHTTP()V

    .line 4145
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v28, v0

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->requestHandlers:[Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;
    invoke-static/range {v28 .. v28}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;

    move-result-object v28

    aget-object v28, v28, v17

    const/16 v29, -0x1

    move/from16 v0, v29

    move-object/from16 v1, v28

    iput v0, v1, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->rrStatus:I

    .line 4146
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v28, v0

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mBlockManager:Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;
    invoke-static/range {v28 .. v28}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;

    move-result-object v29

    monitor-enter v29

    .line 4148
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v28, v0

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mBlockManager:Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;
    invoke-static/range {v28 .. v28}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;

    move-result-object v28

    const-wide/16 v30, 0x3e8

    move-object/from16 v0, v28

    move-wide/from16 v1, v30

    invoke-virtual {v0, v1, v2}, Ljava/lang/Object;->wait(J)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 4151
    :goto_2
    :try_start_2
    monitor-exit v29
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 4152
    invoke-direct/range {p0 .. p1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->getContinuousChunk(I)[J

    move-result-object v24

    goto/16 :goto_0

    .line 4128
    :catch_0
    move-exception v7

    .line 4129
    .local v7, "e":Ljava/lang/Throwable;
    sget-boolean v28, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v28, :cond_d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v28, v0

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static/range {v28 .. v28}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v0, v7}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V

    .line 4130
    :cond_d
    invoke-direct/range {p0 .. p1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->getMinNotReadBlock(I)[J

    move-result-object v24

    goto/16 :goto_0

    .line 4151
    .end local v7    # "e":Ljava/lang/Throwable;
    :catchall_0
    move-exception v28

    :try_start_3
    monitor-exit v29
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v28

    .line 4154
    :cond_e
    invoke-direct/range {p0 .. p0}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->getMinNotReadBlockID()I

    move-result v25

    .line 4155
    .local v25, "toBeDoID":I
    if-gez v25, :cond_10

    .line 4156
    sget-boolean v28, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v28, :cond_f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v28, v0

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static/range {v28 .. v28}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v28

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "all block has been read or is reading "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 4158
    :cond_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->httpRequestID:[I

    move-object/from16 v28, v0

    aget v28, v28, v17

    if-ltz v28, :cond_13

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->httpRequestID:[I

    move-object/from16 v28, v0

    aget v28, v28, v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v29, v0

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static/range {v29 .. v29}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B

    move-result-object v29

    move-object/from16 v0, v29

    array-length v0, v0

    move/from16 v29, v0

    move/from16 v0, v28

    move/from16 v1, v29

    if-ge v0, v1, :cond_13

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v28, v0

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static/range {v28 .. v28}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B

    move-result-object v28

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->httpRequestID:[I

    move-object/from16 v29, v0

    aget v29, v29, v17

    aget-byte v28, v28, v29

    const/16 v29, 0x2

    move/from16 v0, v28

    move/from16 v1, v29

    if-gt v0, v1, :cond_13

    cmpl-double v28, v26, v20

    if-lez v28, :cond_13

    invoke-virtual/range {p2 .. p2}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->getSockID()I

    move-result v28

    move/from16 v0, v28

    move/from16 v1, p1

    if-ne v0, v1, :cond_13

    .line 4160
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->httpRequestID:[I

    move-object/from16 v28, v0

    aget v25, v28, v17

    .line 4166
    :cond_10
    sget-boolean v28, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v28, :cond_11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v28, v0

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static/range {v28 .. v28}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v28

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "thisSpeed="

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    move-wide/from16 v1, v26

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string v30, ", otherSpeed="

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string v30, ", toBeDoID="

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string v30, ", httpRequestID[otherSockID]="

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->httpRequestID:[I

    move-object/from16 v30, v0

    aget v30, v30, v17

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 4168
    :cond_11
    cmpl-double v28, v26, v20

    if-ltz v28, :cond_21

    invoke-virtual/range {p2 .. p2}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->getSockID()I

    move-result v28

    move/from16 v0, v28

    move/from16 v1, p1

    if-ne v0, v1, :cond_21

    .line 4170
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->httpRequestID:[I

    move-object/from16 v28, v0

    aget v28, v28, v17

    move/from16 v0, v25

    move/from16 v1, v28

    if-ge v0, v1, :cond_14

    .line 4171
    sget-boolean v28, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v28, :cond_12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v28, v0

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static/range {v28 .. v28}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v28

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "get a smaller block in getNextHTTPBlock for socket "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string v30, ", toBeDoID="

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string v30, ", httpRequestID[otherSockID]="

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->httpRequestID:[I

    move-object/from16 v30, v0

    aget v30, v30, v17

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 4172
    :cond_12
    invoke-direct/range {p0 .. p1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->getMinNotReadBlock(I)[J

    move-result-object v24

    goto/16 :goto_0

    .line 4163
    :cond_13
    const/16 v28, 0x0

    move/from16 v0, v28

    new-array v0, v0, [J

    move-object/from16 v24, v0

    goto/16 :goto_0

    .line 4176
    :cond_14
    const/16 v16, 0x0

    .line 4177
    .local v16, "otherBuf":Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v28, v0

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->inBuffer:Ljava/util/HashMap;
    invoke-static/range {v28 .. v28}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Ljava/util/HashMap;

    move-result-object v29

    monitor-enter v29

    .line 4178
    :try_start_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v28, v0

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->inBuffer:Ljava/util/HashMap;
    invoke-static/range {v28 .. v28}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Ljava/util/HashMap;

    move-result-object v28

    new-instance v30, Ljava/lang/Integer;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->httpRequestID:[I

    move-object/from16 v31, v0

    aget v31, v31, v17

    invoke-direct/range {v30 .. v31}, Ljava/lang/Integer;-><init>(I)V

    move-object/from16 v0, v28

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v28

    move-object/from16 v0, v28

    check-cast v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

    move-object/from16 v16, v0

    .line 4179
    monitor-exit v29
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 4181
    if-nez v16, :cond_16

    .line 4182
    sget-boolean v28, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v28, :cond_15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v28, v0

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static/range {v28 .. v28}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v28

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "otherBuf is null in getNextHTTPBlock for socket "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 4183
    :cond_15
    invoke-direct/range {p0 .. p1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->getMinNotReadBlock(I)[J

    move-result-object v24

    goto/16 :goto_0

    .line 4179
    :catchall_1
    move-exception v28

    :try_start_5
    monitor-exit v29
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v28

    .line 4186
    :cond_16
    const-wide v28, 0x3f50624dd2f1a9fcL    # 0.001

    cmpg-double v28, v20, v28

    if-gez v28, :cond_1a

    .line 4187
    const-wide v8, 0x408f400000000000L    # 1000.0

    .line 4190
    :goto_3
    const-wide/16 v18, 0x0

    .line 4191
    .local v18, "otherRest":J
    const/4 v5, 0x0

    .line 4192
    .local v5, "bSwitch":Z
    const/4 v4, 0x0

    .line 4194
    .local v4, "bForce":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->toBeReadLen:[J

    move-object/from16 v28, v0

    aget-wide v18, v28, v17

    .line 4195
    const-wide/16 v28, 0x0

    cmp-long v28, v12, v28

    if-nez v28, :cond_1c

    .line 4196
    const-wide/16 v28, 0x0

    cmp-long v28, v18, v28

    if-lez v28, :cond_1b

    const/4 v5, 0x1

    .line 4197
    :goto_4
    const/4 v4, 0x1

    .line 4208
    :goto_5
    sget-boolean v28, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v28, :cond_17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v28, v0

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static/range {v28 .. v28}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v28

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "prepare for switch socket "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string v30, ", "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    move-wide/from16 v1, v18

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string v30, ", "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string v30, ", "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->headerTime:[J

    move-object/from16 v30, v0

    aget-wide v30, v30, p1

    invoke-virtual/range {v29 .. v31}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 4209
    :cond_17
    if-eqz v5, :cond_1f

    .line 4210
    sget-boolean v28, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v28, :cond_18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v28, v0

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static/range {v28 .. v28}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v28

    const-string v29, "ratio or rest len is big, switch socket"

    invoke-virtual/range {v28 .. v29}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 4211
    :cond_18
    move-object/from16 v0, v16

    move/from16 v1, p1

    invoke-virtual {v0, v1, v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->switchSocket(IZ)[J

    move-result-object v23

    .line 4212
    .local v23, "ret":[J
    if-eqz v23, :cond_19

    move-object/from16 v0, v23

    array-length v0, v0

    move/from16 v28, v0

    if-nez v28, :cond_1e

    .line 4213
    :cond_19
    invoke-direct/range {p0 .. p1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->getMinNotReadBlock(I)[J

    move-result-object v24

    goto/16 :goto_0

    .line 4189
    .end local v4    # "bForce":Z
    .end local v5    # "bSwitch":Z
    .end local v18    # "otherRest":J
    .end local v23    # "ret":[J
    :cond_1a
    div-double v8, v26, v20

    goto/16 :goto_3

    .line 4196
    .restart local v4    # "bForce":Z
    .restart local v5    # "bSwitch":Z
    .restart local v18    # "otherRest":J
    :cond_1b
    const/4 v5, 0x0

    goto :goto_4

    .line 4200
    :cond_1c
    const-wide/high16 v28, 0x4000000000000000L    # 2.0

    cmpl-double v28, v8, v28

    if-lez v28, :cond_1d

    const-wide/16 v28, 0x8

    mul-long v28, v28, v18

    div-long v28, v28, v14

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->headerTime:[J

    move-object/from16 v30, v0

    aget-wide v30, v30, p1

    add-long v28, v28, v30

    const-wide/16 v30, 0x8

    mul-long v30, v30, v18

    div-long v30, v30, v12

    cmp-long v28, v28, v30

    if-gez v28, :cond_1d

    const/4 v5, 0x1

    .line 4201
    :goto_6
    const/4 v4, 0x0

    goto/16 :goto_5

    .line 4200
    :cond_1d
    const/4 v5, 0x0

    goto :goto_6

    .line 4216
    .restart local v23    # "ret":[J
    :cond_1e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->httpRequestID:[I

    move-object/from16 v28, v0

    const/16 v29, 0x3

    aget-wide v30, v23, v29

    move-wide/from16 v0, v30

    long-to-int v0, v0

    move/from16 v29, v0

    aput v29, v28, p1

    move-object/from16 v24, v23

    .line 4217
    goto/16 :goto_0

    .line 4221
    .end local v23    # "ret":[J
    :cond_1f
    sget-boolean v28, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v28, :cond_20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v28, v0

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static/range {v28 .. v28}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v28

    const-string v29, "ratio or rest len is not too big, get next block"

    invoke-virtual/range {v28 .. v29}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 4222
    :cond_20
    invoke-direct/range {p0 .. p1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->getMinNotReadBlock(I)[J

    move-result-object v24

    goto/16 :goto_0

    .line 4228
    .end local v4    # "bForce":Z
    .end local v5    # "bSwitch":Z
    .end local v16    # "otherBuf":Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;
    .end local v18    # "otherRest":J
    :cond_21
    div-double v8, v20, v26

    .line 4229
    sget-boolean v28, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v28, :cond_22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v28, v0

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static/range {v28 .. v28}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v28

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "this socket slower than other speed, fRatio="

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string v30, ", other socket is downloading "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->httpRequestID:[I

    move-object/from16 v30, v0

    aget v30, v30, v17

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string v30, ", toBeDoID is "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 4231
    :cond_22
    const-wide/high16 v28, 0x3ff8000000000000L    # 1.5

    cmpg-double v28, v8, v28

    if-gez v28, :cond_24

    .line 4232
    sget-boolean v28, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v28, :cond_23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v28, v0

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static/range {v28 .. v28}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v28

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "this socket is not too slow, download toBeDoID, ratio="

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 4233
    :cond_23
    invoke-direct/range {p0 .. p1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->getMinNotReadBlock(I)[J

    move-result-object v24

    goto/16 :goto_0

    .line 4235
    :cond_24
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->httpRequestID:[I

    move-object/from16 v28, v0

    aget v28, v28, v17

    move/from16 v0, v25

    move/from16 v1, v28

    if-ge v0, v1, :cond_26

    .line 4236
    sget-boolean v28, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v28, :cond_25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v28, v0

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static/range {v28 .. v28}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v28

    const-string v29, "this socket is slow, but there is blank portion, still download smallest portion"

    invoke-virtual/range {v28 .. v29}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 4237
    :cond_25
    invoke-direct/range {p0 .. p1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->getMinNotReadBlock(I)[J

    move-result-object v24

    goto/16 :goto_0

    .line 4242
    :cond_26
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v28, v0

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->inBuffer:Ljava/util/HashMap;
    invoke-static/range {v28 .. v28}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Ljava/util/HashMap;

    move-result-object v29

    monitor-enter v29

    .line 4243
    :try_start_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v28, v0

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->inBuffer:Ljava/util/HashMap;
    invoke-static/range {v28 .. v28}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Ljava/util/HashMap;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/util/HashMap;->size()I

    move-result v28

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v30, v0

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->iMaxBlockNumber:I
    invoke-static/range {v30 .. v30}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$5800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v30

    add-int/lit8 v30, v30, -0x1

    move/from16 v0, v28

    move/from16 v1, v30

    if-lt v0, v1, :cond_27

    .line 4244
    invoke-direct/range {p0 .. p1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->getMinNotReadBlock(I)[J

    move-result-object v24

    monitor-exit v29

    goto/16 :goto_0

    .line 4246
    :catchall_2
    move-exception v28

    monitor-exit v29
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    throw v28

    :cond_27
    :try_start_7
    monitor-exit v29
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 4248
    invoke-static {v8, v9}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v28

    move-wide/from16 v0, v28

    double-to-int v0, v0

    move/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v29, v0

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->MAX_JUMP_STEP:I
    invoke-static/range {v29 .. v29}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$6300(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v29

    invoke-static/range {v28 .. v29}, Ljava/lang/Math;->min(II)I

    move-result v10

    .line 4249
    .local v10, "iR":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->httpRequestID:[I

    move-object/from16 v28, v0

    aget v28, v28, v17

    add-int v11, v10, v28

    .line 4250
    .local v11, "nextid":I
    sget-boolean v28, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v28, :cond_28

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v28, v0

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static/range {v28 .. v28}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v28

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "this socket is slow, download with step, ratio="

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string v30, ", otherRequestID="

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->httpRequestID:[I

    move-object/from16 v30, v0

    aget v30, v30, v17

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string v30, ", nextid="

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 4251
    :cond_28
    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v0, v1, v11}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->getBlockForWithID(II)[J

    move-result-object v24

    goto/16 :goto_0

    .line 4149
    .end local v10    # "iR":I
    .end local v11    # "nextid":I
    .end local v25    # "toBeDoID":I
    :catch_1
    move-exception v28

    goto/16 :goto_2
.end method

.method public getSpeed(I)J
    .locals 2
    .param p1, "sid"    # I

    .prologue
    .line 4049
    iget-object v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->socketSpeed:[J

    aget-wide v0, v0, p1

    return-wide v0
.end method

.method public setSpeed(IJ)V
    .locals 2
    .param p1, "sid"    # I
    .param p2, "speed"    # J

    .prologue
    .line 4040
    iget-object v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->socketSpeed:[J

    aput-wide p2, v0, p1

    .line 4041
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 4025
    new-instance v1, Ljava/lang/StringBuffer;

    const-string v2, "BlockManager:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 4026
    .local v1, "sb":Ljava/lang/StringBuffer;
    const-string v2, "httpRequestID{"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 4027
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->socketNumber:I
    invoke-static {v2}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3300(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 4028
    iget-object v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->httpRequestID:[I

    aget v2, v2, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 4027
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 4030
    :cond_0
    const-string v2, "}; "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 4031
    const-string v2, "minNotReadBlockID{"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    iget v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->minNotReadBlockID:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, "}"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 4032
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method
