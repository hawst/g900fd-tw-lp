.class public Lcom/android/okhttp/internal/http/MultiratLog;
.super Ljava/lang/Object;
.source "MultiratLog.java"


# static fields
.field public static HTTPFILELOG:Z = false

.field public static MRATLOG:Z = false

.field static final SLOG_DEBUG:I = 0x1

.field static final SLOG_ERROR:I = 0x4

.field static final SLOG_INFO:I = 0x2

.field static final SLOG_LEVEL:I = 0x1

.field static final SLOG_WARN:I = 0x3

.field private static final mStaticLogger:Lcom/android/okhttp/internal/http/MultiratLog;


# instance fields
.field private final TAG:Ljava/lang/String;

.field private fDateFormat:Ljava/text/SimpleDateFormat;

.field private fHttpFileLog:Ljava/io/BufferedWriter;

.field private logFileLocker:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 19
    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    .line 21
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/okhttp/internal/http/MultiratLog;->HTTPFILELOG:Z

    .line 34
    new-instance v0, Lcom/android/okhttp/internal/http/MultiratLog;

    const-string v1, "Static"

    invoke-direct {v0, v1}, Lcom/android/okhttp/internal/http/MultiratLog;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/android/okhttp/internal/http/MultiratLog;->mStaticLogger:Lcom/android/okhttp/internal/http/MultiratLog;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "tag"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object v0, p0, Lcom/android/okhttp/internal/http/MultiratLog;->fHttpFileLog:Ljava/io/BufferedWriter;

    .line 24
    iput-object v0, p0, Lcom/android/okhttp/internal/http/MultiratLog;->fDateFormat:Ljava/text/SimpleDateFormat;

    .line 26
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/okhttp/internal/http/MultiratLog;->logFileLocker:Ljava/lang/Object;

    .line 37
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "(HTTPLog)-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/okhttp/internal/http/MultiratLog;->TAG:Ljava/lang/String;

    .line 38
    return-void
.end method

.method public static debug(Ljava/lang/String;)V
    .locals 1
    .param p0, "args"    # Ljava/lang/String;

    .prologue
    .line 43
    sget-object v0, Lcom/android/okhttp/internal/http/MultiratLog;->mStaticLogger:Lcom/android/okhttp/internal/http/MultiratLog;

    invoke-virtual {v0, p0}, Lcom/android/okhttp/internal/http/MultiratLog;->d(Ljava/lang/String;)V

    .line 44
    return-void
.end method

.method public static debug(Ljava/lang/Throwable;)V
    .locals 1
    .param p0, "e"    # Ljava/lang/Throwable;

    .prologue
    .line 47
    sget-object v0, Lcom/android/okhttp/internal/http/MultiratLog;->mStaticLogger:Lcom/android/okhttp/internal/http/MultiratLog;

    invoke-virtual {v0, p0}, Lcom/android/okhttp/internal/http/MultiratLog;->d(Ljava/lang/Throwable;)V

    .line 48
    return-void
.end method

.method private static getLogFileName()Ljava/lang/String;
    .locals 6

    .prologue
    .line 148
    new-instance v2, Ljava/lang/StringBuffer;

    const-string v4, "/sdcard/SB_OKHTTP_"

    invoke-direct {v2, v4}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 149
    .local v2, "fname":Ljava/lang/StringBuffer;
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Thread;->getId()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 150
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 151
    .local v0, "date":Ljava/util/Date;
    new-instance v3, Ljava/text/SimpleDateFormat;

    const-string v4, "yyyy_MM_dd_HH_mm_ss_SSS"

    invoke-direct {v3, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 152
    .local v3, "sdf":Ljava/text/SimpleDateFormat;
    invoke-virtual {v3, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    .line 153
    .local v1, "dateString":Ljava/lang/String;
    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, ".log"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method private getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;
    .locals 4
    .param p1, "tr"    # Ljava/lang/Throwable;

    .prologue
    .line 129
    if-nez p1, :cond_0

    .line 130
    const-string v3, ""

    .line 144
    :goto_0
    return-object v3

    .line 134
    :cond_0
    move-object v2, p1

    .line 135
    .local v2, "t":Ljava/lang/Throwable;
    :goto_1
    if-eqz v2, :cond_2

    .line 136
    instance-of v3, v2, Ljava/net/UnknownHostException;

    if-eqz v3, :cond_1

    .line 137
    const-string v3, ""

    goto :goto_0

    .line 139
    :cond_1
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    goto :goto_1

    .line 141
    :cond_2
    new-instance v1, Ljava/io/StringWriter;

    invoke-direct {v1}, Ljava/io/StringWriter;-><init>()V

    .line 142
    .local v1, "sw":Ljava/io/StringWriter;
    new-instance v0, Ljava/io/PrintWriter;

    invoke-direct {v0, v1}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    .line 143
    .local v0, "pw":Ljava/io/PrintWriter;
    invoke-virtual {p1, v0}, Ljava/lang/Throwable;->printStackTrace(Ljava/io/PrintWriter;)V

    .line 144
    invoke-virtual {v1}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method public static info(Ljava/lang/String;)V
    .locals 1
    .param p0, "args"    # Ljava/lang/String;

    .prologue
    .line 51
    sget-object v0, Lcom/android/okhttp/internal/http/MultiratLog;->mStaticLogger:Lcom/android/okhttp/internal/http/MultiratLog;

    invoke-virtual {v0, p0}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 52
    return-void
.end method

.method public static info(Ljava/lang/Throwable;)V
    .locals 1
    .param p0, "e"    # Ljava/lang/Throwable;

    .prologue
    .line 55
    sget-object v0, Lcom/android/okhttp/internal/http/MultiratLog;->mStaticLogger:Lcom/android/okhttp/internal/http/MultiratLog;

    invoke-virtual {v0, p0}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V

    .line 56
    return-void
.end method

.method private logThrowable(Ljava/lang/Throwable;)V
    .locals 4
    .param p1, "e"    # Ljava/lang/Throwable;

    .prologue
    .line 108
    :try_start_0
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 109
    .local v0, "sBuf":Ljava/lang/StringBuffer;
    iget-object v2, p0, Lcom/android/okhttp/internal/http/MultiratLog;->TAG:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, "(This is just Trace Log)"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-direct {p0, p1}, Lcom/android/okhttp/internal/http/MultiratLog;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 110
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    .line 111
    .local v1, "ws":Ljava/lang/String;
    invoke-direct {p0, v1}, Lcom/android/okhttp/internal/http/MultiratLog;->printLog(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 114
    .end local v0    # "sBuf":Ljava/lang/StringBuffer;
    .end local v1    # "ws":Ljava/lang/String;
    :goto_0
    return-void

    .line 113
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method private logToFile(Ljava/lang/String;)V
    .locals 8
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 157
    iget-object v4, p0, Lcom/android/okhttp/internal/http/MultiratLog;->logFileLocker:Ljava/lang/Object;

    monitor-enter v4

    .line 158
    :try_start_0
    iget-object v3, p0, Lcom/android/okhttp/internal/http/MultiratLog;->fHttpFileLog:Ljava/io/BufferedWriter;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v3, :cond_0

    .line 160
    :try_start_1
    invoke-static {}, Lcom/android/okhttp/internal/http/MultiratLog;->getLogFileName()Ljava/lang/String;

    move-result-object v1

    .line 161
    .local v1, "fname":Ljava/lang/String;
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/android/okhttp/internal/http/MultiratLog;->TAG:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "create log file "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 162
    new-instance v3, Ljava/io/BufferedWriter;

    new-instance v5, Ljava/io/FileWriter;

    const/4 v6, 0x1

    invoke-direct {v5, v1, v6}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;Z)V

    invoke-direct {v3, v5}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    iput-object v3, p0, Lcom/android/okhttp/internal/http/MultiratLog;->fHttpFileLog:Ljava/io/BufferedWriter;

    .line 163
    new-instance v3, Ljava/text/SimpleDateFormat;

    const-string v5, "yyyy-MM-dd HH:mm:ss.SSS"

    invoke-direct {v3, v5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    iput-object v3, p0, Lcom/android/okhttp/internal/http/MultiratLog;->fDateFormat:Ljava/text/SimpleDateFormat;

    .line 164
    iget-object v3, p0, Lcom/android/okhttp/internal/http/MultiratLog;->fHttpFileLog:Ljava/io/BufferedWriter;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " start write http log for "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Thread;->getThreadGroup()Ljava/lang/ThreadGroup;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 165
    iget-object v3, p0, Lcom/android/okhttp/internal/http/MultiratLog;->fHttpFileLog:Ljava/io/BufferedWriter;

    invoke-virtual {v3}, Ljava/io/BufferedWriter;->newLine()V

    .line 166
    iget-object v3, p0, Lcom/android/okhttp/internal/http/MultiratLog;->fHttpFileLog:Ljava/io/BufferedWriter;

    invoke-virtual {v3}, Ljava/io/BufferedWriter;->flush()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 173
    .end local v1    # "fname":Ljava/lang/String;
    :cond_0
    :goto_0
    :try_start_2
    iget-object v3, p0, Lcom/android/okhttp/internal/http/MultiratLog;->fHttpFileLog:Ljava/io/BufferedWriter;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v3, :cond_1

    .line 175
    :try_start_3
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 176
    .local v2, "sBuf":Ljava/lang/StringBuffer;
    iget-object v3, p0, Lcom/android/okhttp/internal/http/MultiratLog;->fDateFormat:Ljava/text/SimpleDateFormat;

    new-instance v5, Ljava/util/Date;

    invoke-direct {v5}, Ljava/util/Date;-><init>()V

    invoke-virtual {v3, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v5, "\t"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 177
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->getId()J

    move-result-wide v6

    invoke-virtual {v2, v6, v7}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v5, "\t"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 178
    invoke-virtual {v2, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 179
    iget-object v3, p0, Lcom/android/okhttp/internal/http/MultiratLog;->fHttpFileLog:Ljava/io/BufferedWriter;

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 180
    iget-object v3, p0, Lcom/android/okhttp/internal/http/MultiratLog;->fHttpFileLog:Ljava/io/BufferedWriter;

    invoke-virtual {v3}, Ljava/io/BufferedWriter;->newLine()V

    .line 181
    iget-object v3, p0, Lcom/android/okhttp/internal/http/MultiratLog;->fHttpFileLog:Ljava/io/BufferedWriter;

    invoke-virtual {v3}, Ljava/io/BufferedWriter;->flush()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 188
    .end local v2    # "sBuf":Ljava/lang/StringBuffer;
    :cond_1
    :goto_1
    :try_start_4
    monitor-exit v4

    .line 189
    return-void

    .line 168
    :catch_0
    move-exception v0

    .line 169
    .local v0, "e":Ljava/lang/Throwable;
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/android/okhttp/internal/http/MultiratLog;->TAG:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-direct {p0, v0}, Lcom/android/okhttp/internal/http/MultiratLog;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 170
    const/4 v3, 0x0

    sput-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->HTTPFILELOG:Z

    goto :goto_0

    .line 188
    .end local v0    # "e":Ljava/lang/Throwable;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v3

    .line 183
    :catch_1
    move-exception v0

    .line 184
    .restart local v0    # "e":Ljava/lang/Throwable;
    :try_start_5
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/android/okhttp/internal/http/MultiratLog;->TAG:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-direct {p0, v0}, Lcom/android/okhttp/internal/http/MultiratLog;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 185
    const/4 v3, 0x0

    sput-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->HTTPFILELOG:Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1
.end method

.method private printLog(Ljava/lang/String;)V
    .locals 1
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 117
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v0, p1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 118
    sget-boolean v0, Lcom/android/okhttp/internal/http/MultiratLog;->HTTPFILELOG:Z

    if-eqz v0, :cond_0

    .line 119
    invoke-direct {p0, p1}, Lcom/android/okhttp/internal/http/MultiratLog;->logToFile(Ljava/lang/String;)V

    .line 121
    :cond_0
    return-void
.end method

.method public static warn(Ljava/lang/String;)V
    .locals 1
    .param p0, "args"    # Ljava/lang/String;

    .prologue
    .line 59
    sget-object v0, Lcom/android/okhttp/internal/http/MultiratLog;->mStaticLogger:Lcom/android/okhttp/internal/http/MultiratLog;

    invoke-virtual {v0, p0}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 60
    return-void
.end method

.method public static warn(Ljava/lang/Throwable;)V
    .locals 1
    .param p0, "e"    # Ljava/lang/Throwable;

    .prologue
    .line 63
    sget-object v0, Lcom/android/okhttp/internal/http/MultiratLog;->mStaticLogger:Lcom/android/okhttp/internal/http/MultiratLog;

    invoke-virtual {v0, p0}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V

    .line 64
    return-void
.end method


# virtual methods
.method public d(Ljava/lang/String;)V
    .locals 2
    .param p1, "args"    # Ljava/lang/String;

    .prologue
    .line 68
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiratLog;->TAG:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/okhttp/internal/http/MultiratLog;->printLog(Ljava/lang/String;)V

    .line 69
    return-void
.end method

.method public d(Ljava/lang/Throwable;)V
    .locals 0
    .param p1, "e"    # Ljava/lang/Throwable;

    .prologue
    .line 88
    invoke-direct {p0, p1}, Lcom/android/okhttp/internal/http/MultiratLog;->logThrowable(Ljava/lang/Throwable;)V

    .line 89
    return-void
.end method

.method public e(Ljava/lang/String;)V
    .locals 2
    .param p1, "args"    # Ljava/lang/String;

    .prologue
    .line 83
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiratLog;->TAG:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/okhttp/internal/http/MultiratLog;->printLog(Ljava/lang/String;)V

    .line 84
    return-void
.end method

.method public e(Ljava/lang/Throwable;)V
    .locals 0
    .param p1, "e"    # Ljava/lang/Throwable;

    .prologue
    .line 103
    invoke-direct {p0, p1}, Lcom/android/okhttp/internal/http/MultiratLog;->logThrowable(Ljava/lang/Throwable;)V

    .line 104
    return-void
.end method

.method public i(Ljava/lang/String;)V
    .locals 2
    .param p1, "args"    # Ljava/lang/String;

    .prologue
    .line 73
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiratLog;->TAG:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/okhttp/internal/http/MultiratLog;->printLog(Ljava/lang/String;)V

    .line 74
    return-void
.end method

.method public i(Ljava/lang/Throwable;)V
    .locals 0
    .param p1, "e"    # Ljava/lang/Throwable;

    .prologue
    .line 93
    invoke-direct {p0, p1}, Lcom/android/okhttp/internal/http/MultiratLog;->logThrowable(Ljava/lang/Throwable;)V

    .line 94
    return-void
.end method

.method public w(Ljava/lang/String;)V
    .locals 2
    .param p1, "args"    # Ljava/lang/String;

    .prologue
    .line 78
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiratLog;->TAG:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/okhttp/internal/http/MultiratLog;->printLog(Ljava/lang/String;)V

    .line 79
    return-void
.end method

.method public w(Ljava/lang/Throwable;)V
    .locals 0
    .param p1, "e"    # Ljava/lang/Throwable;

    .prologue
    .line 98
    invoke-direct {p0, p1}, Lcom/android/okhttp/internal/http/MultiratLog;->logThrowable(Ljava/lang/Throwable;)V

    .line 99
    return-void
.end method
