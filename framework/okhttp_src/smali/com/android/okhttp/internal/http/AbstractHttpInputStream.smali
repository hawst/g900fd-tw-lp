.class abstract Lcom/android/okhttp/internal/http/AbstractHttpInputStream;
.super Ljava/io/InputStream;
.source "AbstractHttpInputStream.java"


# instance fields
.field private final cacheBody:Ljava/io/OutputStream;

.field private final cacheRequest:Ljava/net/CacheRequest;

.field protected closed:Z

.field protected final httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

.field protected final in:Ljava/io/InputStream;


# direct methods
.method constructor <init>(Ljava/io/InputStream;Lcom/android/okhttp/internal/http/HttpEngine;Ljava/net/CacheRequest;)V
    .locals 1
    .param p1, "in"    # Ljava/io/InputStream;
    .param p2, "httpEngine"    # Lcom/android/okhttp/internal/http/HttpEngine;
    .param p3, "cacheRequest"    # Ljava/net/CacheRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/android/okhttp/internal/http/AbstractHttpInputStream;->in:Ljava/io/InputStream;

    .line 44
    iput-object p2, p0, Lcom/android/okhttp/internal/http/AbstractHttpInputStream;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    .line 46
    if-eqz p3, :cond_1

    invoke-virtual {p3}, Ljava/net/CacheRequest;->getBody()Ljava/io/OutputStream;

    move-result-object v0

    .line 49
    .local v0, "cacheBody":Ljava/io/OutputStream;
    :goto_0
    if-nez v0, :cond_0

    .line 50
    const/4 p3, 0x0

    .line 53
    :cond_0
    iput-object v0, p0, Lcom/android/okhttp/internal/http/AbstractHttpInputStream;->cacheBody:Ljava/io/OutputStream;

    .line 54
    iput-object p3, p0, Lcom/android/okhttp/internal/http/AbstractHttpInputStream;->cacheRequest:Ljava/net/CacheRequest;

    .line 55
    return-void

    .line 46
    .end local v0    # "cacheBody":Ljava/io/OutputStream;
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected final cacheWrite([BII)V
    .locals 1
    .param p1, "buffer"    # [B
    .param p2, "offset"    # I
    .param p3, "count"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 74
    iget-object v0, p0, Lcom/android/okhttp/internal/http/AbstractHttpInputStream;->cacheBody:Ljava/io/OutputStream;

    if-eqz v0, :cond_0

    .line 75
    iget-object v0, p0, Lcom/android/okhttp/internal/http/AbstractHttpInputStream;->cacheBody:Ljava/io/OutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V

    .line 77
    :cond_0
    return-void
.end method

.method protected final checkNotClosed()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 68
    iget-boolean v0, p0, Lcom/android/okhttp/internal/http/AbstractHttpInputStream;->closed:Z

    if-eqz v0, :cond_0

    .line 69
    new-instance v0, Ljava/io/IOException;

    const-string v1, "stream closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 71
    :cond_0
    return-void
.end method

.method protected final endOfInput(Z)V
    .locals 1
    .param p1, "streamCancelled"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 84
    iget-object v0, p0, Lcom/android/okhttp/internal/http/AbstractHttpInputStream;->cacheRequest:Ljava/net/CacheRequest;

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/android/okhttp/internal/http/AbstractHttpInputStream;->cacheBody:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    .line 87
    :cond_0
    iget-object v0, p0, Lcom/android/okhttp/internal/http/AbstractHttpInputStream;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    invoke-virtual {v0}, Lcom/android/okhttp/internal/http/HttpEngine;->close()Lcom/android/okhttp/Connection;

    .line 88
    return-void
.end method

.method public final read()I
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v2, -0x1

    .line 62
    new-array v0, v5, [B

    .line 63
    .local v0, "buffer":[B
    iget-object v3, p0, Lcom/android/okhttp/internal/http/AbstractHttpInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v3, v0, v4, v5}, Ljava/io/InputStream;->read([BII)I

    move-result v1

    .line 64
    .local v1, "result":I
    if-eq v1, v2, :cond_0

    aget-byte v2, v0, v4

    and-int/lit16 v2, v2, 0xff

    :cond_0
    return v2
.end method

.method protected final unexpectedEndOfInput()V
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/android/okhttp/internal/http/AbstractHttpInputStream;->cacheRequest:Ljava/net/CacheRequest;

    if-eqz v0, :cond_0

    .line 104
    iget-object v0, p0, Lcom/android/okhttp/internal/http/AbstractHttpInputStream;->cacheRequest:Ljava/net/CacheRequest;

    invoke-virtual {v0}, Ljava/net/CacheRequest;->abort()V

    .line 106
    :cond_0
    iget-object v0, p0, Lcom/android/okhttp/internal/http/AbstractHttpInputStream;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    invoke-virtual {v0}, Lcom/android/okhttp/internal/http/HttpEngine;->close()Lcom/android/okhttp/Connection;

    .line 107
    return-void
.end method
