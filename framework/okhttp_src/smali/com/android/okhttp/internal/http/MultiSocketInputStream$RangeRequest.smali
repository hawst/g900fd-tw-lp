.class Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;
.super Ljava/lang/Object;
.source "MultiSocketInputStream.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/okhttp/internal/http/MultiSocketInputStream;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "RangeRequest"
.end annotation


# instance fields
.field private bHandover:Z

.field public bIOExceptionDuringContinueChunk:Z

.field private connInfID:I

.field private dbuf:Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

.field private headersToSend:Lcom/android/okhttp/Headers;

.field private in:Ljava/io/InputStream;

.field private final mbReconnect:Z

.field private out:Ljava/io/OutputStream;

.field public rrExceptionCount:I

.field public rrStatus:I

.field private sockID:I

.field private socket:Ljava/net/Socket;

.field private t0:J

.field private t1:J

.field private t2:J

.field final synthetic this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

.field private final threadName:Ljava/lang/String;

.field private timer:[J

.field public totalElapsedTime:J

.field public totalReadSize:J


# direct methods
.method public constructor <init>(Lcom/android/okhttp/internal/http/MultiSocketInputStream;Lcom/android/okhttp/Headers;I)V
    .locals 5
    .param p2, "h"    # Lcom/android/okhttp/Headers;
    .param p3, "id"    # I

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    .line 2522
    iput-object p1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2473
    iput-object v4, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->dbuf:Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

    .line 2482
    iput-boolean v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->bHandover:Z

    .line 2507
    iput v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->rrStatus:I

    .line 2511
    iput v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->rrExceptionCount:I

    .line 2515
    iput-boolean v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->bIOExceptionDuringContinueChunk:Z

    .line 2523
    iput-object v4, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->socket:Ljava/net/Socket;

    .line 2524
    invoke-virtual {p2}, Lcom/android/okhttp/Headers;->newBuilder()Lcom/android/okhttp/Headers$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/okhttp/Headers$Builder;->build()Lcom/android/okhttp/Headers;

    move-result-object v1

    iput-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->headersToSend:Lcom/android/okhttp/Headers;

    .line 2526
    iput p3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->sockID:I

    .line 2527
    iput p3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->connInfID:I

    .line 2528
    iget-boolean v1, p1, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bConClose:Z

    iput-boolean v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->mbReconnect:Z

    .line 2529
    iget v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->sockID:I

    if-nez v1, :cond_0

    const-string v1, "WIFI_Socket_Thread"

    iput-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->threadName:Ljava/lang/String;

    .line 2531
    :goto_0
    iput-wide v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->totalReadSize:J

    .line 2532
    iput-wide v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->totalElapsedTime:J

    .line 2533
    iput-wide v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->t0:J

    .line 2534
    iput-wide v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->t1:J

    .line 2535
    iput-wide v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->t2:J

    .line 2536
    sget-boolean v1, Lcom/android/okhttp/internal/http/HttpEngine;->HTTPTIMER:Z

    if-eqz v1, :cond_1

    .line 2537
    const/16 v1, 0x14

    new-array v1, v1, [J

    iput-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->timer:[J

    .line 2538
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->timer:[J

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 2539
    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->timer:[J

    aput-wide v2, v1, v0

    .line 2538
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2530
    .end local v0    # "i":I
    :cond_0
    const-string v1, "Mobile_Socket_Thread"

    iput-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->threadName:Ljava/lang/String;

    goto :goto_0

    .line 2542
    :cond_1
    return-void
.end method

.method private reconnect(Ljava/net/URI;)Z
    .locals 10
    .param p1, "uri"    # Ljava/net/URI;

    .prologue
    const/4 v4, 0x0

    .line 3276
    sget-boolean v5, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v5}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v5

    const-string v6, "try to connect again"

    invoke-virtual {v5, v6}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 3277
    :cond_0
    iput-boolean v4, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->bHandover:Z

    .line 3278
    const/4 v1, 0x0

    .line 3280
    .local v1, "ret":Z
    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->t1:J

    .line 3281
    invoke-virtual {p0}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->closeHTTP()V

    .line 3282
    iget-object v5, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->connection:Lcom/android/okhttp/Connection;
    invoke-static {v5}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/Connection;

    move-result-object v5

    iget v6, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->connInfID:I

    invoke-virtual {v5, v6, p1}, Lcom/android/okhttp/Connection;->connectSocket(ILjava/net/URI;)Ljava/net/Socket;

    move-result-object v5

    iput-object v5, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->socket:Ljava/net/Socket;

    .line 3283
    iget-object v5, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->socket:Ljava/net/Socket;

    if-nez v5, :cond_3

    .line 3284
    sget-boolean v5, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v5}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v5

    const-string v6, "Socket failed to reconnect()"

    invoke-virtual {v5, v6}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 3285
    :cond_1
    const/4 v1, 0x0

    .line 3295
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    iget-wide v8, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->t1:J

    sub-long v2, v6, v8

    .line 3296
    .local v2, "tmp":J
    iget-object v5, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mBlockManager:Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;
    invoke-static {v5}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;

    move-result-object v5

    iget-object v5, v5, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->reconnTime:[J

    iget v6, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->sockID:I

    aput-wide v2, v5, v6

    .line 3297
    sget-boolean v5, Lcom/android/okhttp/internal/http/HttpEngine;->HTTPTIMER:Z

    if-eqz v5, :cond_2

    .line 3298
    iget-object v5, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->timer:[J

    const/4 v6, 0x4

    aget-wide v8, v5, v6

    add-long/2addr v8, v2

    aput-wide v8, v5, v6

    .line 3299
    sget-boolean v5, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v5}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "time used for reconnect:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    :cond_2
    move v4, v1

    .line 3308
    .end local v2    # "tmp":J
    :goto_1
    return v4

    .line 3288
    :cond_3
    iget-object v5, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->socket:Ljava/net/Socket;

    invoke-virtual {v5}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v5

    iput-object v5, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->in:Ljava/io/InputStream;

    .line 3289
    iget-object v5, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->socket:Ljava/net/Socket;

    invoke-virtual {v5}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v5

    iput-object v5, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->out:Ljava/io/OutputStream;

    .line 3290
    sget-boolean v5, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v5}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Socket connected again:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->socket:Ljava/net/Socket;

    invoke-virtual {v7}, Ljava/net/Socket;->getLocalAddress()Ljava/net/InetAddress;

    move-result-object v7

    invoke-virtual {v7}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ":"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->socket:Ljava/net/Socket;

    invoke-virtual {v7}, Ljava/net/Socket;->getLocalPort()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 3291
    :cond_4
    const/4 v1, 0x1

    .line 3293
    iget-object v5, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->socket:Ljava/net/Socket;

    const/16 v6, 0x1388

    invoke-virtual {v5, v6}, Ljava/net/Socket;->setSoTimeout(I)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 3303
    :catch_0
    move-exception v0

    .line 3304
    .local v0, "ex":Ljava/lang/Throwable;
    sget-boolean v5, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v5, :cond_5

    iget-object v5, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v5}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v5

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 3305
    :cond_5
    sget-boolean v5, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v5, :cond_6

    iget-object v5, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v5}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v5

    invoke-virtual {v5, v0}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V

    .line 3306
    :cond_6
    invoke-virtual {p0}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->closeHTTP()V

    .line 3307
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->socket:Ljava/net/Socket;

    goto :goto_1
.end method

.method private submitData()V
    .locals 10

    .prologue
    .line 3222
    iget-object v4, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->socketNumber:I
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3300(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v4

    new-array v2, v4, [J

    .line 3223
    .local v2, "lens":[J
    iget-object v4, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->socketNumber:I
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3300(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v4

    new-array v3, v4, [J

    .line 3224
    .local v3, "times":[J
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v4, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->socketNumber:I
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3300(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v4

    if-ge v1, v4, :cond_0

    .line 3225
    iget-object v4, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->requestHandlers:[Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;

    move-result-object v4

    aget-object v4, v4, v1

    iget-wide v4, v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->totalReadSize:J

    aput-wide v4, v2, v1

    .line 3226
    iget-object v4, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->requestHandlers:[Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;

    move-result-object v4

    aget-object v4, v4, v1

    iget-wide v4, v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->totalElapsedTime:J

    aput-wide v4, v3, v1

    .line 3224
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 3228
    :cond_0
    const/4 v4, 0x0

    aget-wide v4, v3, v4

    const-wide/16 v6, 0x7d0

    cmp-long v4, v4, v6

    if-lez v4, :cond_3

    const/4 v4, 0x1

    aget-wide v4, v3, v4

    const-wide/16 v6, 0x7d0

    cmp-long v4, v4, v6

    if-lez v4, :cond_3

    const/4 v4, 0x0

    aget-wide v4, v2, v4

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-lez v4, :cond_3

    const/4 v4, 0x1

    aget-wide v4, v2, v4

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-lez v4, :cond_3

    .line 3229
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_1

    .line 3230
    iget-object v4, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "submit speed for extreme case - socket[0]: len="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x0

    aget-wide v6, v2, v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", time="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x0

    aget-wide v6, v3, v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", speed="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x0

    aget-wide v6, v2, v6

    const-wide/16 v8, 0x8

    mul-long/2addr v6, v8

    const-wide/16 v8, 0x3e8

    mul-long/2addr v6, v8

    const-wide/16 v8, 0x400

    div-long/2addr v6, v8

    const/4 v8, 0x0

    aget-wide v8, v3, v8

    div-long/2addr v6, v8

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "Kbps"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 3231
    iget-object v4, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "submit speed for extreme case - socket[1]: len="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x1

    aget-wide v6, v2, v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", time="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x1

    aget-wide v6, v3, v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", speed="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x1

    aget-wide v6, v2, v6

    const-wide/16 v8, 0x8

    mul-long/2addr v6, v8

    const-wide/16 v8, 0x3e8

    mul-long/2addr v6, v8

    const-wide/16 v8, 0x400

    div-long/2addr v6, v8

    const/4 v8, 0x1

    aget-wide v8, v3, v8

    div-long/2addr v6, v8

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "Kbps"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 3233
    :cond_1
    invoke-static {v2, v3}, Lcom/android/okhttp/internal/http/SBServiceAPI;->submitMultiSocketData([J[J)V

    .line 3241
    :cond_2
    :goto_1
    iget-object v4, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->socketNumber:I
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3300(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v4

    mul-int/lit8 v4, v4, 0x2

    new-array v0, v4, [J

    .line 3242
    .local v0, "data":[J
    const/4 v1, 0x0

    :goto_2
    iget-object v4, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->socketNumber:I
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3300(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v4

    if-ge v1, v4, :cond_4

    .line 3243
    mul-int/lit8 v4, v1, 0x2

    aget-wide v6, v2, v1

    aput-wide v6, v0, v4

    .line 3244
    mul-int/lit8 v4, v1, 0x2

    add-int/lit8 v4, v4, 0x1

    aget-wide v6, v3, v1

    aput-wide v6, v0, v4

    .line 3242
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 3236
    .end local v0    # "data":[J
    :cond_3
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_2

    .line 3237
    iget-object v4, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "not to submit speed for extreme case - socket[0]: len="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x0

    aget-wide v6, v2, v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", time="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x0

    aget-wide v6, v3, v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 3238
    iget-object v4, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "not to submit speed for extreme case - socket[1]: len="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x1

    aget-wide v6, v2, v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", time="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x1

    aget-wide v6, v3, v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    goto :goto_1

    .line 3246
    .restart local v0    # "data":[J
    :cond_4
    iget-object v4, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainThreadID:J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$100(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v4

    invoke-static {v4, v5, v0}, Lcom/android/okhttp/internal/http/SBServiceAPI;->reportSBUsage(J[J)V

    .line 3247
    return-void
.end method


# virtual methods
.method public closeHTTP()V
    .locals 4

    .prologue
    .line 3260
    :try_start_0
    sget-boolean v1, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v1

    const-string v2, "try to close current HTTP session"

    invoke-virtual {v1, v2}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 3261
    :cond_0
    sget-boolean v1, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "try to close input stream in rr thread, input is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->in:Ljava/io/InputStream;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 3262
    :cond_1
    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->in:Ljava/io/InputStream;

    invoke-static {v1}, Lcom/android/okhttp/internal/http/MultiratUtil;->closeQuietly(Ljava/io/Closeable;)V

    .line 3263
    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->out:Ljava/io/OutputStream;

    invoke-static {v1}, Lcom/android/okhttp/internal/http/MultiratUtil;->closeQuietly(Ljava/io/Closeable;)V

    .line 3264
    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->socket:Ljava/net/Socket;

    invoke-static {v1}, Lcom/android/okhttp/internal/http/MultiratUtil;->closeQuietly(Ljava/net/Socket;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 3270
    :cond_2
    :goto_0
    return-void

    .line 3266
    :catch_0
    move-exception v0

    .line 3267
    .local v0, "ex":Ljava/lang/Throwable;
    sget-boolean v1, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 3268
    :cond_3
    sget-boolean v1, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public handOver()V
    .locals 1

    .prologue
    .line 3252
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->bHandover:Z

    .line 3253
    invoke-virtual {p0}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->closeHTTP()V

    .line 3254
    return-void
.end method

.method public run()V
    .locals 92

    .prologue
    .line 2546
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->threadName:Ljava/lang/String;

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    # operator++ for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->threadID:I
    invoke-static {}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2108()I

    move-result v9

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 2547
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->socket:Ljava/net/Socket;

    if-nez v4, :cond_11

    .line 2548
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    const-string v5, "download thread start for with Idle"

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 2561
    :cond_0
    :goto_0
    # operator++ for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->iCurrentMultiSocketNum:I
    invoke-static {}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1008()I

    .line 2562
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "now active multi socket instance is "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->iCurrentMultiSocketNum:I
    invoke-static {}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1000()I

    move-result v9

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 2565
    :cond_1
    const/4 v14, 0x1

    .line 2566
    .local v14, "bFirstBlockRsp":Z
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->t0:J

    .line 2567
    sget v54, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->INIT_BUFFERLEN:I

    .line 2570
    .local v54, "iRealBlockSize":I
    const-wide/16 v48, 0x0

    .line 2572
    .local v48, "finalIdleWaitTime":J
    :cond_2
    :goto_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-boolean v4, v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bFinished:Z

    if-nez v4, :cond_f

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bRangeRequestSuccess:I
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v4

    const/4 v5, 0x1

    if-eq v4, v5, :cond_f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->rrStatus:I

    const/4 v5, -0x1

    if-eq v4, v5, :cond_f

    .line 2573
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->inBuffer:Ljava/util/HashMap;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Ljava/util/HashMap;

    move-result-object v5

    monitor-enter v5

    .line 2574
    :try_start_0
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "check buffer size "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->inBuffer:Ljava/util/HashMap;
    invoke-static {v10}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Ljava/util/HashMap;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/HashMap;->size()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " compare with max number "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->iMaxBlockNumber:I
    invoke-static {v10}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$5800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 2575
    :cond_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    sget v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->WAIT_FOR_INTERFACE_TIME_OUT:I

    int-to-long v12, v4

    add-long v46, v10, v12

    .line 2576
    .local v46, "expectWaitBufferTime":J
    :cond_4
    :goto_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-boolean v4, v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bFinished:Z

    if-nez v4, :cond_5

    .line 2577
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B

    move-result-object v4

    if-nez v4, :cond_13

    .line 2578
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_5

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    const-string v9, "buffer status not initiated, not full"

    invoke-virtual {v4, v9}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 2625
    :cond_5
    :goto_3
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2629
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->sockID:I

    # invokes: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->getOtherSocketID(I)I
    invoke-static {v4, v5}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1600(Lcom/android/okhttp/internal/http/MultiSocketInputStream;I)I

    move-result v61

    .line 2632
    .local v61, "otherSocketId":I
    :cond_6
    :goto_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-boolean v4, v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bFinished:Z

    if-nez v4, :cond_9

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->requestHandlers:[Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;

    move-result-object v4

    aget-object v4, v4, v61

    iget v4, v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->rrStatus:I

    const/4 v5, -0x1

    if-eq v4, v5, :cond_9

    .line 2636
    const/16 v29, 0x0

    .line 2637
    .local v29, "chunkNotRead":I
    const/16 v20, 0x0

    .line 2641
    .local v20, "blockedState":I
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->readBlockNumber:Ljava/lang/Integer;
    invoke-static {v5}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$5900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v53

    .line 2642
    .local v53, "i":I
    :goto_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B

    move-result-object v4

    array-length v4, v4

    move/from16 v0, v53

    if-ge v0, v4, :cond_8

    .line 2643
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B

    move-result-object v4

    aget-byte v4, v4, v53

    if-ltz v4, :cond_1f

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B

    move-result-object v4

    aget-byte v4, v4, v53

    const/4 v5, 0x3

    if-ge v4, v5, :cond_1f

    .line 2644
    add-int/lit8 v29, v29, 0x1

    .line 2649
    :cond_7
    const/4 v4, 0x1

    move/from16 v0, v29

    if-le v0, v4, :cond_20

    .line 2655
    :cond_8
    :goto_6
    const/4 v4, 0x1

    move/from16 v0, v29

    if-gt v0, v4, :cond_9

    if-lez v20, :cond_21

    .line 2678
    .end local v20    # "blockedState":I
    .end local v29    # "chunkNotRead":I
    .end local v53    # "i":I
    :cond_9
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->connection:Lcom/android/okhttp/Connection;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/Connection;

    move-result-object v5

    monitor-enter v5

    .line 2679
    :try_start_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-object v4, v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->sRedirectedRequestHeaders:[Lcom/android/okhttp/Request;

    move-object/from16 v0, p0

    iget v9, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->connInfID:I

    aget-object v70, v4, v9

    .line 2680
    .local v70, "requestHeaders":Lcom/android/okhttp/Request;
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 2681
    invoke-virtual/range {v70 .. v70}, Lcom/android/okhttp/Request;->getHeaders()Lcom/android/okhttp/Headers;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->headersToSend:Lcom/android/okhttp/Headers;

    .line 2682
    const/16 v69, 0x0

    .line 2684
    .local v69, "redirectedUri":Ljava/net/URI;
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_a

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "start to check socket connectivity ReCon="

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-boolean v9, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->mbReconnect:Z

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 2685
    :cond_a
    :goto_7
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-boolean v4, v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bFinished:Z

    if-nez v4, :cond_e

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->connection:Lcom/android/okhttp/Connection;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/Connection;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->socket:Ljava/net/Socket;

    invoke-virtual {v4, v5}, Lcom/android/okhttp/Connection;->socketAlive(Ljava/net/Socket;)Z

    move-result v4

    if-eqz v4, :cond_b

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->mbReconnect:Z

    if-eqz v4, :cond_e

    .line 2686
    :cond_b
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->connection:Lcom/android/okhttp/Connection;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/Connection;

    move-result-object v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->connInfID:I

    invoke-virtual {v4, v5}, Lcom/android/okhttp/Connection;->getLocalAddr(I)Ljava/net/InetAddress;

    move-result-object v59

    .line 2690
    .local v59, "localAddress":Ljava/net/InetAddress;
    if-nez v59, :cond_d

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->requestHandlers:[Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;

    move-result-object v4

    aget-object v4, v4, v61

    iget v4, v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->rrStatus:I

    const/4 v5, -0x1

    if-ne v4, v5, :cond_d

    .line 2691
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_c

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "The interface for socket id "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget v9, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->sockID:I

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, " is null and "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, "other thread is RR_STOPPED"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 2692
    :cond_c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->connection:Lcom/android/okhttp/Connection;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/Connection;

    move-result-object v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->connInfID:I

    add-int/lit8 v5, v5, 0x1

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->socketNumber:I
    invoke-static {v9}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3300(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v9

    rem-int/2addr v5, v9

    invoke-virtual {v4, v5}, Lcom/android/okhttp/Connection;->getLocalAddr(I)Ljava/net/InetAddress;

    move-result-object v59

    .line 2693
    if-eqz v59, :cond_d

    .line 2694
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->bIOExceptionDuringContinueChunk:Z

    .line 2695
    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->connInfID:I

    add-int/lit8 v4, v4, 0x1

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->socketNumber:I
    invoke-static {v5}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3300(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v5

    rem-int/2addr v4, v5

    move-object/from16 v0, p0

    iput v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->connInfID:I

    .line 2696
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->rrExceptionCount:I

    .line 2697
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_d

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Other socket interface ip is not null and so changing the interface id to "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget v9, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->connInfID:I

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, " for thread id "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget v9, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->sockID:I

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 2702
    :cond_d
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->connection:Lcom/android/okhttp/Connection;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/Connection;

    move-result-object v5

    monitor-enter v5

    .line 2703
    :try_start_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-object v4, v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->sRedirectedRequestHeaders:[Lcom/android/okhttp/Request;

    move-object/from16 v0, p0

    iget v9, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->connInfID:I

    aget-object v70, v4, v9

    .line 2704
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    .line 2705
    invoke-virtual/range {v70 .. v70}, Lcom/android/okhttp/Request;->getHeaders()Lcom/android/okhttp/Headers;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->headersToSend:Lcom/android/okhttp/Headers;

    .line 2707
    :try_start_3
    invoke-virtual/range {v70 .. v70}, Lcom/android/okhttp/Request;->uri()Ljava/net/URI;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    move-result-object v69

    .line 2721
    move-object/from16 v0, p0

    move-object/from16 v1, v69

    invoke-direct {v0, v1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->reconnect(Ljava/net/URI;)Z

    move-result v4

    if-eqz v4, :cond_27

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->socket:Ljava/net/Socket;

    if-eqz v4, :cond_27

    .line 2722
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_e

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "download thread bind new socket "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->socket:Ljava/net/Socket;

    invoke-virtual {v9}, Ljava/net/Socket;->getInetAddress()Ljava/net/InetAddress;

    move-result-object v9

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, ":"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->socket:Ljava/net/Socket;

    invoke-virtual {v9}, Ljava/net/Socket;->getPort()I

    move-result v9

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 2749
    .end local v59    # "localAddress":Ljava/net/InetAddress;
    :cond_e
    :goto_8
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bRangeRequestSuccess:I
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_2c

    .line 2750
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_f

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    const-string v5, "bRangeRequestSuccess == RR_FAILED, break1"

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 3182
    .end local v46    # "expectWaitBufferTime":J
    .end local v61    # "otherSocketId":I
    .end local v69    # "redirectedUri":Ljava/net/URI;
    .end local v70    # "requestHeaders":Lcom/android/okhttp/Request;
    :cond_f
    :goto_9
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->t0:J

    sub-long/2addr v4, v10

    sub-long v4, v4, v48

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->totalElapsedTime:J

    .line 3183
    sget-boolean v4, Lcom/android/okhttp/internal/http/HttpEngine;->HTTPTIMER:Z

    if-eqz v4, :cond_10

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->timer:[J

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->totalElapsedTime:J

    aput-wide v10, v4, v5

    .line 3184
    :cond_10
    sget-boolean v4, Lcom/android/okhttp/internal/http/HttpEngine;->HTTPTIMER:Z

    if-eqz v4, :cond_82

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->totalReadSize:J

    const-wide/16 v10, 0x64

    cmp-long v4, v4, v10

    if-lez v4, :cond_82

    .line 3185
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->timer:[J

    const/4 v5, 0x2

    aget-wide v10, v4, v5

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->timer:[J

    const/4 v12, 0x3

    aget-wide v12, v9, v12

    sub-long/2addr v10, v12

    aput-wide v10, v4, v5

    .line 3186
    new-instance v78, Ljava/lang/StringBuffer;

    const-string v4, "\tHTTPTIMER"

    move-object/from16 v0, v78

    invoke-direct {v0, v4}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 3187
    .local v78, "timerStr":Ljava/lang/StringBuffer;
    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->sockID:I

    move-object/from16 v0, v78

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, "\t\t"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->totalReadSize:J

    invoke-virtual {v4, v10, v11}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    .line 3188
    const/16 v53, 0x0

    .restart local v53    # "i":I
    :goto_a
    const/4 v4, 0x5

    move/from16 v0, v53

    if-ge v0, v4, :cond_81

    .line 3189
    const-string v4, "\t"

    move-object/from16 v0, v78

    invoke-virtual {v0, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->timer:[J

    aget-wide v10, v5, v53

    invoke-virtual {v4, v10, v11}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    .line 3188
    add-int/lit8 v53, v53, 0x1

    goto :goto_a

    .line 2551
    .end local v14    # "bFirstBlockRsp":Z
    .end local v48    # "finalIdleWaitTime":J
    .end local v53    # "i":I
    .end local v54    # "iRealBlockSize":I
    .end local v78    # "timerStr":Ljava/lang/StringBuffer;
    :cond_11
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_12

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "download thread start for "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->socket:Ljava/net/Socket;

    invoke-virtual {v9}, Ljava/net/Socket;->getInetAddress()Ljava/net/InetAddress;

    move-result-object v9

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 2553
    :cond_12
    :try_start_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->socket:Ljava/net/Socket;

    invoke-virtual {v4}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->in:Ljava/io/InputStream;

    .line 2554
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->socket:Ljava/net/Socket;

    invoke-virtual {v4}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->out:Ljava/io/OutputStream;
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    goto/16 :goto_0

    .line 2556
    :catch_0
    move-exception v44

    .line 2557
    .local v44, "e":Ljava/io/IOException;
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    move-object/from16 v0, v44

    invoke-virtual {v4, v0}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 2582
    .end local v44    # "e":Ljava/io/IOException;
    .restart local v14    # "bFirstBlockRsp":Z
    .restart local v46    # "expectWaitBufferTime":J
    .restart local v48    # "finalIdleWaitTime":J
    .restart local v54    # "iRealBlockSize":I
    :cond_13
    :try_start_5
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_14

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mBlockManager:Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;
    invoke-static {v9}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->blockStatusToStr()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 2583
    :cond_14
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->inBuffer:Ljava/util/HashMap;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Ljava/util/HashMap;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/HashMap;->size()I

    move-result v4

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->iMaxBlockNumber:I
    invoke-static {v9}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$5800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v9

    if-gt v4, v9, :cond_1b

    .line 2584
    const/16 v60, 0x0

    .line 2585
    .local v60, "number":I
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->readBlockNumber:Ljava/lang/Integer;
    invoke-static {v9}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$5900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    invoke-static {v4, v9}, Ljava/lang/Math;->max(II)I

    move-result v53

    .restart local v53    # "i":I
    :goto_b
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B

    move-result-object v4

    array-length v4, v4

    move/from16 v0, v53

    if-ge v0, v4, :cond_17

    .line 2586
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B

    move-result-object v4

    aget-byte v4, v4, v53

    const/4 v9, 0x3

    if-ne v4, v9, :cond_16

    .line 2587
    add-int/lit8 v60, v60, 0x1

    .line 2585
    :cond_15
    add-int/lit8 v53, v53, 0x1

    goto :goto_b

    .line 2589
    :cond_16
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B

    move-result-object v4

    aget-byte v4, v4, v53

    const/4 v9, 0x2

    if-gt v4, v9, :cond_15

    .line 2593
    :cond_17
    const/4 v4, 0x2

    move/from16 v0, v60

    if-ge v0, v4, :cond_18

    .line 2594
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_5

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "BlockStatus: full read block "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move/from16 v0, v60

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " < "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const/4 v10, 0x2

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 2625
    .end local v46    # "expectWaitBufferTime":J
    .end local v53    # "i":I
    .end local v60    # "number":I
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    throw v4

    .line 2598
    .restart local v46    # "expectWaitBufferTime":J
    .restart local v53    # "i":I
    .restart local v60    # "number":I
    :cond_18
    :try_start_6
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_19

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    const-string v9, "BlockStatus: there are continue number of portions fulled 2"

    invoke-virtual {v4, v9}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 2617
    .end local v60    # "number":I
    :cond_19
    :try_start_7
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->inBuffer:Ljava/util/HashMap;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Ljava/util/HashMap;

    move-result-object v4

    const-wide/16 v10, 0x3e8

    invoke-virtual {v4, v10, v11}, Ljava/lang/Object;->wait(J)V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_e
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 2620
    :goto_c
    :try_start_8
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    cmp-long v4, v10, v46

    if-lez v4, :cond_4

    .line 2621
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_1a

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    const-string v9, " RangeRequest wait for buffer free time out"

    invoke-virtual {v4, v9}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 2622
    :cond_1a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    const/4 v9, 0x1

    iput-boolean v9, v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bFinished:Z

    goto/16 :goto_2

    .line 2602
    .end local v53    # "i":I
    :cond_1b
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_1c

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "BlockStatus: block size "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->inBuffer:Ljava/util/HashMap;
    invoke-static {v10}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Ljava/util/HashMap;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/HashMap;->size()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " > Max Size "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->iMaxBlockNumber:I
    invoke-static {v10}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$5800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 2604
    :cond_1c
    const/4 v15, 0x0

    .line 2605
    .local v15, "bOtherBlocked":Z
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->readBlockNumber:Ljava/lang/Integer;
    invoke-static {v9}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$5900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    invoke-static {v4, v9}, Ljava/lang/Math;->max(II)I

    move-result v53

    .restart local v53    # "i":I
    :goto_d
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->readBlockNumber:Ljava/lang/Integer;
    invoke-static {v9}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$5900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    invoke-static {v4, v9}, Ljava/lang/Math;->max(II)I

    move-result v4

    move/from16 v0, v53

    if-gt v0, v4, :cond_1d

    .line 2606
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B

    move-result-object v4

    aget-byte v4, v4, v53

    const/4 v9, -0x1

    if-ne v4, v9, :cond_1e

    .line 2607
    const/4 v15, 0x1

    .line 2608
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_1d

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "BlockStatus: a blocked chunk: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move/from16 v0, v53

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 2612
    :cond_1d
    if-eqz v15, :cond_19

    goto/16 :goto_3

    .line 2605
    :cond_1e
    add-int/lit8 v53, v53, 0x1

    goto :goto_d

    .line 2645
    .end local v15    # "bOtherBlocked":Z
    .restart local v20    # "blockedState":I
    .restart local v29    # "chunkNotRead":I
    .restart local v61    # "otherSocketId":I
    :cond_1f
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B

    move-result-object v4

    aget-byte v4, v4, v53

    const/4 v5, -0x1

    if-ne v4, v5, :cond_7

    .line 2646
    add-int/lit8 v20, v20, 0x1

    .line 2647
    goto/16 :goto_6

    .line 2642
    :cond_20
    add-int/lit8 v53, v53, 0x1

    goto/16 :goto_5

    .line 2657
    :cond_21
    const/4 v4, 0x1

    move/from16 v0, v29

    if-ne v0, v4, :cond_22

    .line 2660
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mBlockManager:Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;

    move-result-object v4

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->socketSpeed:[J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->access$6000(Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;)[J

    move-result-object v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->sockID:I

    aget-wide v4, v4, v5

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mBlockManager:Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;
    invoke-static {v9}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;

    move-result-object v9

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->socketSpeed:[J
    invoke-static {v9}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->access$6000(Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;)[J

    move-result-object v9

    aget-wide v10, v9, v61

    const-wide/16 v12, 0x2

    mul-long/2addr v10, v12

    cmp-long v4, v4, v10

    if-gtz v4, :cond_9

    .line 2666
    :cond_22
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_23

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "wait for seconds: "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget v9, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->WAIT_TIME_TILL_READ_FROM_COMMON_BUFFER:I

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 2669
    :cond_23
    :try_start_9
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->inBuffer:Ljava/util/HashMap;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Ljava/util/HashMap;

    move-result-object v5

    monitor-enter v5
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_1

    .line 2670
    :try_start_a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->inBuffer:Ljava/util/HashMap;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Ljava/util/HashMap;

    move-result-object v4

    sget v9, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->WAIT_TIME_TILL_READ_FROM_COMMON_BUFFER:I

    int-to-long v10, v9

    invoke-virtual {v4, v10, v11}, Ljava/lang/Object;->wait(J)V

    .line 2671
    monitor-exit v5

    goto/16 :goto_4

    :catchall_1
    move-exception v4

    monitor-exit v5
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    :try_start_b
    throw v4
    :try_end_b
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_1

    .line 2672
    :catch_1
    move-exception v44

    .local v44, "e":Ljava/lang/Throwable;
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_6

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    move-object/from16 v0, v44

    invoke-virtual {v4, v0}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V

    goto/16 :goto_4

    .line 2680
    .end local v20    # "blockedState":I
    .end local v29    # "chunkNotRead":I
    .end local v44    # "e":Ljava/lang/Throwable;
    .end local v53    # "i":I
    :catchall_2
    move-exception v4

    :try_start_c
    monitor-exit v5
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    throw v4

    .line 2704
    .restart local v59    # "localAddress":Ljava/net/InetAddress;
    .restart local v69    # "redirectedUri":Ljava/net/URI;
    .restart local v70    # "requestHeaders":Lcom/android/okhttp/Request;
    :catchall_3
    move-exception v4

    :try_start_d
    monitor-exit v5
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_3

    throw v4

    .line 2709
    :catch_2
    move-exception v44

    .line 2710
    .local v44, "e":Ljava/io/IOException;
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_24

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    move-object/from16 v0, v44

    invoke-virtual {v4, v0}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V

    .line 2711
    :cond_24
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mBlockManager:Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;

    move-result-object v5

    monitor-enter v5

    .line 2712
    :try_start_e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bRangeRequestSuccess:I
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v4

    if-eqz v4, :cond_25

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bRangeRequestSuccess:I
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v4

    const/4 v9, 0x3

    if-ne v4, v9, :cond_26

    .line 2713
    :cond_25
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    const/4 v9, 0x1

    # setter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bRangeRequestSuccess:I
    invoke-static {v4, v9}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2402(Lcom/android/okhttp/internal/http/MultiSocketInputStream;I)I

    .line 2714
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mBlockManager:Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V

    .line 2716
    :cond_26
    monitor-exit v5
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_4

    .line 2717
    invoke-virtual/range {p0 .. p0}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->closeHTTP()V

    goto/16 :goto_8

    .line 2716
    :catchall_4
    move-exception v4

    :try_start_f
    monitor-exit v5
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_4

    throw v4

    .line 2727
    .end local v44    # "e":Ljava/io/IOException;
    :cond_27
    if-eqz v14, :cond_2b

    .line 2728
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_28

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "move to RR_FAILED from "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bRangeRequestSuccess:I
    invoke-static {v9}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v9

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, " in first connect()"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 2729
    :cond_28
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mBlockManager:Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;

    move-result-object v5

    monitor-enter v5

    .line 2730
    :try_start_10
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bRangeRequestSuccess:I
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v4

    if-eqz v4, :cond_29

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bRangeRequestSuccess:I
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v4

    const/4 v9, 0x3

    if-ne v4, v9, :cond_2a

    .line 2731
    :cond_29
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    const/4 v9, 0x1

    # setter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bRangeRequestSuccess:I
    invoke-static {v4, v9}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2402(Lcom/android/okhttp/internal/http/MultiSocketInputStream;I)I

    .line 2732
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mBlockManager:Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V

    .line 2734
    :cond_2a
    monitor-exit v5
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_5

    .line 2735
    invoke-virtual/range {p0 .. p0}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->closeHTTP()V

    goto/16 :goto_8

    .line 2734
    :catchall_5
    move-exception v4

    :try_start_11
    monitor-exit v5
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_5

    throw v4

    .line 2739
    :cond_2b
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    monitor-enter v5

    .line 2741
    :try_start_12
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    const-wide/16 v10, 0x3e8

    invoke-virtual {v4, v10, v11}, Ljava/lang/Object;->wait(J)V
    :try_end_12
    .catch Ljava/lang/Throwable; {:try_start_12 .. :try_end_12} :catch_d
    .catchall {:try_start_12 .. :try_end_12} :catchall_6

    .line 2744
    :goto_e
    :try_start_13
    monitor-exit v5

    goto/16 :goto_7

    :catchall_6
    move-exception v4

    monitor-exit v5
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_6

    throw v4

    .line 2754
    .end local v59    # "localAddress":Ljava/net/InetAddress;
    :cond_2c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-boolean v4, v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bFinished:Z

    if-eqz v4, :cond_2d

    .line 2755
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_f

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    const-string v5, "bFinished = true, break1"

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    goto/16 :goto_9

    .line 2759
    :cond_2d
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_2e

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    const-string v5, "start to get another range request"

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 2761
    :cond_2e
    sget-boolean v4, Lcom/android/okhttp/internal/http/HttpEngine;->HTTPTIMER:Z

    if-eqz v4, :cond_2f

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->t1:J

    .line 2762
    :cond_2f
    const-wide/16 v26, 0x0

    .line 2763
    .local v26, "bytesToRead":J
    const-wide/16 v88, 0x0

    .line 2765
    .local v88, "totallen":J
    const/16 v17, 0x0

    .line 2767
    .local v17, "blockInfo":[J
    :try_start_14
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mBlockManager:Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;

    move-result-object v5

    monitor-enter v5
    :try_end_14
    .catch Ljava/lang/Throwable; {:try_start_14 .. :try_end_14} :catch_3

    .line 2768
    :try_start_15
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mBlockManager:Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;

    move-result-object v4

    move-object/from16 v0, p0

    iget v9, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->sockID:I

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->dbuf:Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

    invoke-virtual {v4, v9, v10}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->getNextHTTPBlock(ILcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;)[J

    move-result-object v17

    .line 2769
    monitor-exit v5
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_8

    .line 2774
    :cond_30
    :goto_f
    if-eqz v17, :cond_31

    move-object/from16 v0, v17

    array-length v4, v0

    const/4 v5, 0x4

    if-ge v4, v5, :cond_42

    .line 2775
    :cond_31
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_32

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "getNextHTTPBlock() results 0, resource check: finish reading totalLength("

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->totalLengthToBeRead:J
    invoke-static {v9}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$300(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v10

    invoke-virtual {v5, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, "), break"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 2776
    :cond_32
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v76

    .line 2777
    .local v76, "startWaitFinishTime":J
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_33

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "start to wait for other thread exit: "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v76

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 2778
    :cond_33
    :goto_10
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-boolean v4, v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bFinished:Z

    if-nez v4, :cond_37

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bRangeRequestSuccess:I
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v4

    const/4 v5, 0x1

    if-eq v4, v5, :cond_37

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->rrStatus:I

    const/4 v5, -0x1

    if-eq v4, v5, :cond_37

    if-eqz v17, :cond_34

    move-object/from16 v0, v17

    array-length v4, v0

    const/4 v5, 0x4

    if-ge v4, v5, :cond_37

    .line 2779
    :cond_34
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mBlockManager:Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;

    move-result-object v5

    monitor-enter v5

    .line 2781
    :try_start_16
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mBlockManager:Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;

    move-result-object v4

    const-wide/16 v10, 0x3e8

    invoke-virtual {v4, v10, v11}, Ljava/lang/Object;->wait(J)V
    :try_end_16
    .catch Ljava/lang/Throwable; {:try_start_16 .. :try_end_16} :catch_4
    .catchall {:try_start_16 .. :try_end_16} :catchall_7

    .line 2787
    :cond_35
    :goto_11
    :try_start_17
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mBlockManager:Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;

    move-result-object v4

    move-object/from16 v0, p0

    iget v9, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->sockID:I

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->dbuf:Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

    invoke-virtual {v4, v9, v10}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->getNextHTTPBlock(ILcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;)[J
    :try_end_17
    .catch Ljava/lang/Throwable; {:try_start_17 .. :try_end_17} :catch_5
    .catchall {:try_start_17 .. :try_end_17} :catchall_7

    move-result-object v17

    .line 2792
    :cond_36
    :goto_12
    :try_start_18
    monitor-exit v5

    goto :goto_10

    :catchall_7
    move-exception v4

    monitor-exit v5
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_7

    throw v4

    .line 2769
    .end local v76    # "startWaitFinishTime":J
    :catchall_8
    move-exception v4

    :try_start_19
    monitor-exit v5
    :try_end_19
    .catchall {:try_start_19 .. :try_end_19} :catchall_8

    :try_start_1a
    throw v4
    :try_end_1a
    .catch Ljava/lang/Throwable; {:try_start_1a .. :try_end_1a} :catch_3

    .line 2771
    :catch_3
    move-exception v44

    .line 2772
    .local v44, "e":Ljava/lang/Throwable;
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_30

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    move-object/from16 v0, v44

    invoke-virtual {v4, v0}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V

    goto/16 :goto_f

    .line 2783
    .end local v44    # "e":Ljava/lang/Throwable;
    .restart local v76    # "startWaitFinishTime":J
    :catch_4
    move-exception v45

    .line 2784
    .local v45, "ex":Ljava/lang/Throwable;
    :try_start_1b
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_35

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    move-object/from16 v0, v45

    invoke-virtual {v4, v0}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V

    goto :goto_11

    .line 2789
    .end local v45    # "ex":Ljava/lang/Throwable;
    :catch_5
    move-exception v45

    .line 2790
    .restart local v45    # "ex":Ljava/lang/Throwable;
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_36

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    move-object/from16 v0, v45

    invoke-virtual {v4, v0}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V
    :try_end_1b
    .catchall {:try_start_1b .. :try_end_1b} :catchall_7

    goto :goto_12

    .line 2794
    .end local v45    # "ex":Ljava/lang/Throwable;
    :cond_37
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v4, v4, v76

    add-long v48, v48, v4

    .line 2795
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_38

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "finish to wait for other thread exit: "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v48

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 2796
    :cond_38
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-boolean v4, v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bFinished:Z

    if-nez v4, :cond_39

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bRangeRequestSuccess:I
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_3a

    .line 2797
    :cond_39
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_f

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    const-string v5, "finish to wait for other thread exit for download finished"

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    goto/16 :goto_9

    .line 2800
    :cond_3a
    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->rrStatus:I

    const/4 v5, -0x1

    if-ne v4, v5, :cond_3b

    .line 2801
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_f

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    const-string v5, "this thread is stopped, and the other thread is downloading continue chunk"

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    goto/16 :goto_9

    .line 2804
    :cond_3b
    if-eqz v17, :cond_41

    move-object/from16 v0, v17

    array-length v4, v0

    const/4 v5, 0x4

    if-lt v4, v5, :cond_41

    .line 2805
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_3c

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "getNextHTTPBlock() results id after wait "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v9, 0x3

    aget-wide v10, v17, v9

    invoke-virtual {v5, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, ": "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v9, 0x0

    aget-wide v10, v17, v9

    invoke-virtual {v5, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, "-"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v9, 0x1

    aget-wide v10, v17, v9

    invoke-virtual {v5, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 2816
    .end local v76    # "startWaitFinishTime":J
    :cond_3c
    :goto_13
    const/16 v28, 0x0

    .line 2818
    .local v28, "chunkLengths":[J
    const/16 v25, 0x0

    .line 2820
    .local v25, "chunkIndex":I
    const/16 v86, 0x0

    .line 2823
    .local v86, "totalContinuousChunk":I
    new-instance v68, Ljava/lang/StringBuffer;

    const-string v4, "bytes="

    move-object/from16 v0, v68

    invoke-direct {v0, v4}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 2824
    .local v68, "range":Ljava/lang/StringBuffer;
    const/4 v4, 0x0

    aget-wide v4, v17, v4

    move-object/from16 v0, v68

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, "-"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const/4 v5, 0x1

    aget-wide v10, v17, v5

    invoke-virtual {v4, v10, v11}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    .line 2825
    const/4 v4, 0x2

    aget-wide v26, v17, v4

    .line 2826
    const/4 v4, 0x3

    aget-wide v4, v17, v4

    long-to-int v0, v4

    move/from16 v38, v0

    .line 2828
    .local v38, "currentBlockNumber":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mBlockManager:Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;

    move-result-object v4

    iget-object v4, v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->toBeReadLen:[J

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->sockID:I

    aput-wide v26, v4, v5

    .line 2831
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->headersToSend:Lcom/android/okhttp/Headers;

    invoke-virtual {v4}, Lcom/android/okhttp/Headers;->newBuilder()Lcom/android/okhttp/Headers$Builder;

    move-result-object v4

    const-string v5, "Range"

    invoke-virtual/range {v68 .. v68}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v5, v9}, Lcom/android/okhttp/Headers$Builder;->add(Ljava/lang/String;Ljava/lang/String;)Lcom/android/okhttp/Headers$Builder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/okhttp/Headers$Builder;->build()Lcom/android/okhttp/Headers;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->headersToSend:Lcom/android/okhttp/Headers;

    .line 2832
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_3d

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "RangeRequestHeader["

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v38

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, "]: "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->headersToSend:Lcom/android/okhttp/Headers;

    invoke-virtual {v9}, Lcom/android/okhttp/Headers;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 2833
    :cond_3d
    sget-boolean v4, Lcom/android/okhttp/internal/http/HttpEngine;->HTTPTIMER:Z

    if-eqz v4, :cond_3e

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->timer:[J

    const/4 v5, 0x1

    aget-wide v10, v4, v5

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->t1:J

    move-wide/from16 v90, v0

    sub-long v12, v12, v90

    add-long/2addr v10, v12

    aput-wide v10, v4, v5

    .line 2834
    :cond_3e
    const/16 v23, 0x0

    .line 2835
    .local v23, "buf_offset":I
    const/16 v24, 0x0

    .line 2837
    .local v24, "buf_ret":I
    :try_start_1c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->connection:Lcom/android/okhttp/Connection;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/Connection;

    move-result-object v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->sockID:I

    invoke-virtual {v4, v5}, Lcom/android/okhttp/Connection;->getProxy(I)Ljava/net/Proxy;

    move-result-object v4

    invoke-virtual {v4}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->connection:Lcom/android/okhttp/Connection;
    invoke-static {v5}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/Connection;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/okhttp/Connection;->getHttpMinorVersion()I

    move-result v5

    move-object/from16 v0, v70

    invoke-static {v0, v4, v5}, Lcom/android/okhttp/internal/http/MultiratUtil;->getRequestLine(Lcom/android/okhttp/Request;Ljava/net/Proxy$Type;I)Ljava/lang/String;

    move-result-object v71

    .line 2838
    .local v71, "requestLine":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->headersToSend:Lcom/android/okhttp/Headers;

    move-object/from16 v0, v71

    invoke-static {v4, v0}, Lcom/android/okhttp/internal/http/MultiratUtil;->getRequestHeadersBytes(Lcom/android/okhttp/Headers;Ljava/lang/String;)[B

    move-result-object v52

    .line 2840
    .local v52, "headBytes":[B
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->inBuffer:Ljava/util/HashMap;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Ljava/util/HashMap;

    move-result-object v5

    monitor-enter v5
    :try_end_1c
    .catch Ljava/lang/InterruptedException; {:try_start_1c .. :try_end_1c} :catch_6
    .catch Ljava/lang/Throwable; {:try_start_1c .. :try_end_1c} :catch_7

    .line 2841
    :try_start_1d
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->inBuffer:Ljava/util/HashMap;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Ljava/util/HashMap;

    move-result-object v4

    new-instance v9, Ljava/lang/Integer;

    move/from16 v0, v38

    invoke-direct {v9, v0}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v4, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->dbuf:Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

    .line 2842
    monitor-exit v5
    :try_end_1d
    .catchall {:try_start_1d .. :try_end_1d} :catchall_9

    .line 2846
    :try_start_1e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B

    move-result-object v4

    array-length v4, v4

    add-int/lit8 v4, v4, -0x1

    move/from16 v0, v38

    if-eq v0, v4, :cond_48

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->BlockSize:J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v4

    cmp-long v4, v26, v4

    if-lez v4, :cond_48

    .line 2847
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->dbuf:Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

    invoke-virtual {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->getTotalChunks()I

    move-result v86

    .line 2848
    move/from16 v0, v86

    new-array v0, v0, [J

    move-object/from16 v28, v0

    .line 2850
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->dbuf:Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

    invoke-virtual {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->getToBeReadLength()J

    move-result-wide v4

    aput-wide v4, v28, v25

    .line 2852
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_3f

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "chunkLengths[0]"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v9, 0x0

    aget-wide v10, v28, v9

    invoke-virtual {v5, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 2854
    :cond_3f
    const/16 v55, 0x1

    .local v55, "j":I
    :goto_14
    move-object/from16 v0, v28

    array-length v4, v0

    move/from16 v0, v55

    if-ge v0, v4, :cond_48

    .line 2856
    add-int v4, v38, v55

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static {v5}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B

    move-result-object v5

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    if-ne v4, v5, :cond_44

    .line 2859
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesForMultiSocket:J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1500(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v4

    add-int v9, v38, v55

    int-to-long v10, v9

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->BlockSize:J
    invoke-static {v9}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v12

    mul-long/2addr v10, v12

    sub-long/2addr v4, v10

    aput-wide v4, v28, v55

    .line 2864
    :goto_15
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_40

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "chunkLengths["

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v55

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, "]"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget-wide v10, v28, v55

    invoke-virtual {v5, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V
    :try_end_1e
    .catch Ljava/lang/InterruptedException; {:try_start_1e .. :try_end_1e} :catch_6
    .catch Ljava/lang/Throwable; {:try_start_1e .. :try_end_1e} :catch_7

    .line 2854
    :cond_40
    add-int/lit8 v55, v55, 0x1

    goto :goto_14

    .line 2808
    .end local v23    # "buf_offset":I
    .end local v24    # "buf_ret":I
    .end local v25    # "chunkIndex":I
    .end local v28    # "chunkLengths":[J
    .end local v38    # "currentBlockNumber":I
    .end local v52    # "headBytes":[B
    .end local v55    # "j":I
    .end local v68    # "range":Ljava/lang/StringBuffer;
    .end local v71    # "requestLine":Ljava/lang/String;
    .end local v86    # "totalContinuousChunk":I
    .restart local v76    # "startWaitFinishTime":J
    :cond_41
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_3c

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    const-string v5, "Exception Case, shall not come here"

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    goto/16 :goto_13

    .line 2812
    .end local v76    # "startWaitFinishTime":J
    :cond_42
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_3c

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "threadID "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget v9, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->sockID:I

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, " getNextHTTPBlock() on interface "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget v9, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->connInfID:I

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, " results id "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v9, 0x3

    aget-wide v10, v17, v9

    invoke-virtual {v5, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, ": "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v9, 0x0

    aget-wide v10, v17, v9

    invoke-virtual {v5, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, "-"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v9, 0x1

    aget-wide v10, v17, v9

    invoke-virtual {v5, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    goto/16 :goto_13

    .line 2842
    .restart local v23    # "buf_offset":I
    .restart local v24    # "buf_ret":I
    .restart local v25    # "chunkIndex":I
    .restart local v28    # "chunkLengths":[J
    .restart local v38    # "currentBlockNumber":I
    .restart local v52    # "headBytes":[B
    .restart local v68    # "range":Ljava/lang/StringBuffer;
    .restart local v71    # "requestLine":Ljava/lang/String;
    .restart local v86    # "totalContinuousChunk":I
    :catchall_9
    move-exception v4

    :try_start_1f
    monitor-exit v5
    :try_end_1f
    .catchall {:try_start_1f .. :try_end_1f} :catchall_9

    :try_start_20
    throw v4
    :try_end_20
    .catch Ljava/lang/InterruptedException; {:try_start_20 .. :try_end_20} :catch_6
    .catch Ljava/lang/Throwable; {:try_start_20 .. :try_end_20} :catch_7

    .line 3103
    .end local v52    # "headBytes":[B
    .end local v71    # "requestLine":Ljava/lang/String;
    :catch_6
    move-exception v45

    .line 3104
    .local v45, "ex":Ljava/lang/InterruptedException;
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_43

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    const-string v5, "in InterruptedException, handle handover"

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 3107
    :cond_43
    invoke-virtual/range {p0 .. p0}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->closeHTTP()V

    goto/16 :goto_1

    .line 2862
    .end local v45    # "ex":Ljava/lang/InterruptedException;
    .restart local v52    # "headBytes":[B
    .restart local v55    # "j":I
    .restart local v71    # "requestLine":Ljava/lang/String;
    :cond_44
    :try_start_21
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->BlockSize:J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v4

    aput-wide v4, v28, v55
    :try_end_21
    .catch Ljava/lang/InterruptedException; {:try_start_21 .. :try_end_21} :catch_6
    .catch Ljava/lang/Throwable; {:try_start_21 .. :try_end_21} :catch_7

    goto/16 :goto_15

    .line 3109
    .end local v52    # "headBytes":[B
    .end local v55    # "j":I
    .end local v71    # "requestLine":Ljava/lang/String;
    :catch_7
    move-exception v44

    .line 3110
    .restart local v44    # "e":Ljava/lang/Throwable;
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_45

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "reading thread blocked by some Exception: "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {v44 .. v44}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 3111
    :cond_45
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_46

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    move-object/from16 v0, v44

    invoke-virtual {v4, v0}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V

    .line 3114
    :cond_46
    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->rrExceptionCount:I

    add-int/lit8 v4, v4, 0x1

    move-object/from16 v0, p0

    iput v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->rrExceptionCount:I

    .line 3117
    if-eqz v14, :cond_7a

    .line 3118
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mBlockManager:Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;

    move-result-object v5

    monitor-enter v5

    .line 3119
    :try_start_22
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bRangeRequestSuccess:I
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v4

    const/4 v9, 0x3

    if-ne v4, v9, :cond_76

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->rrStatus:I

    const/4 v9, -0x1

    if-ne v4, v9, :cond_76

    .line 3120
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_47

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    const-string v9, "move to RR_SUCCESS2 in first block exception"

    invoke-virtual {v4, v9}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 3121
    :cond_47
    if-lez v86, :cond_75

    .line 3122
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mBlockManager:Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;

    move-result-object v4

    add-int v9, v38, v25

    move-object/from16 v0, p0

    iget v10, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->sockID:I

    invoke-virtual {v4, v9, v10}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->block(II)V

    .line 3126
    :goto_16
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    const/4 v9, 0x4

    # setter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bRangeRequestSuccess:I
    invoke-static {v4, v9}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2402(Lcom/android/okhttp/internal/http/MultiSocketInputStream;I)I

    .line 3127
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mBlockManager:Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V

    .line 3128
    invoke-virtual/range {p0 .. p0}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->closeHTTP()V

    .line 3129
    monitor-exit v5

    goto/16 :goto_9

    .line 3138
    :catchall_a
    move-exception v4

    monitor-exit v5
    :try_end_22
    .catchall {:try_start_22 .. :try_end_22} :catchall_a

    throw v4

    .line 2868
    .end local v44    # "e":Ljava/lang/Throwable;
    .restart local v52    # "headBytes":[B
    .restart local v71    # "requestLine":Ljava/lang/String;
    :cond_48
    :try_start_23
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->t1:J

    .line 2870
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->out:Ljava/io/OutputStream;

    move-object/from16 v0, v52

    invoke-virtual {v4, v0}, Ljava/io/OutputStream;->write([B)V

    .line 2871
    const/16 v56, 0x0

    .line 2873
    .local v56, "len":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->in:Ljava/io/InputStream;

    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiratUtil;->readResponseHeaders(Ljava/io/InputStream;)Lcom/android/okhttp/Response$Builder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/okhttp/Response$Builder;->build()Lcom/android/okhttp/Response;

    move-result-object v72

    .line 2874
    .local v72, "rsp":Lcom/android/okhttp/Response;
    invoke-virtual/range {v72 .. v72}, Lcom/android/okhttp/Response;->code()I

    move-result v73

    .line 2879
    .local v73, "rspCode":I
    invoke-virtual/range {v72 .. v72}, Lcom/android/okhttp/Response;->headers()Lcom/android/okhttp/Headers;

    move-result-object v4

    const-wide/16 v10, -0x1

    invoke-static {v4, v10, v11}, Lcom/android/okhttp/internal/http/MultiratUtil;->getFullContentLength(Lcom/android/okhttp/Headers;J)J

    move-result-wide v50

    .line 2880
    .local v50, "fullSize":J
    invoke-static/range {v72 .. v72}, Lcom/android/okhttp/internal/http/OkHeaders;->contentLength(Lcom/android/okhttp/Response;)J

    move-result-wide v32

    .line 2881
    .local v32, "conLen":J
    const/16 v4, 0xce

    move/from16 v0, v73

    if-eq v0, v4, :cond_49

    const/16 v4, 0xc8

    move/from16 v0, v73

    if-ne v0, v4, :cond_4a

    :cond_49
    cmp-long v4, v32, v26

    if-nez v4, :cond_4a

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mFullConSize:J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2500(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v4

    cmp-long v4, v50, v4

    if-eqz v4, :cond_50

    .line 2882
    :cond_4a
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_4b

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "response code is not 206 or 200 : "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v73

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, " or length is not expected: "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v32

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, " or full lengthis not expected: "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v50

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 2885
    :cond_4b
    if-eqz v14, :cond_4f

    .line 2886
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_4c

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "move to RR_FAILED from "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bRangeRequestSuccess:I
    invoke-static {v9}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v9

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 2887
    :cond_4c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mBlockManager:Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;

    move-result-object v5

    monitor-enter v5
    :try_end_23
    .catch Ljava/lang/InterruptedException; {:try_start_23 .. :try_end_23} :catch_6
    .catch Ljava/lang/Throwable; {:try_start_23 .. :try_end_23} :catch_7

    .line 2888
    :try_start_24
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bRangeRequestSuccess:I
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v4

    if-eqz v4, :cond_4d

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bRangeRequestSuccess:I
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v4

    const/4 v9, 0x3

    if-ne v4, v9, :cond_4e

    .line 2889
    :cond_4d
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    const/4 v9, 0x1

    # setter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bRangeRequestSuccess:I
    invoke-static {v4, v9}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2402(Lcom/android/okhttp/internal/http/MultiSocketInputStream;I)I

    .line 2890
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mBlockManager:Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V

    .line 2892
    :cond_4e
    monitor-exit v5
    :try_end_24
    .catchall {:try_start_24 .. :try_end_24} :catchall_b

    .line 2893
    :try_start_25
    invoke-virtual/range {p0 .. p0}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->closeHTTP()V
    :try_end_25
    .catch Ljava/lang/InterruptedException; {:try_start_25 .. :try_end_25} :catch_6
    .catch Ljava/lang/Throwable; {:try_start_25 .. :try_end_25} :catch_7

    goto/16 :goto_9

    .line 2892
    :catchall_b
    move-exception v4

    :try_start_26
    monitor-exit v5
    :try_end_26
    .catchall {:try_start_26 .. :try_end_26} :catchall_b

    :try_start_27
    throw v4

    .line 2897
    :cond_4f
    new-instance v4, Ljava/io/IOException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "response code is not 206 or 200 : "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v73

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 2900
    :cond_50
    if-eqz v14, :cond_54

    .line 2901
    const/4 v14, 0x0

    .line 2902
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_51

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "move to RR_SUCCESS from "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bRangeRequestSuccess:I
    invoke-static {v9}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v9

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 2903
    :cond_51
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mBlockManager:Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;

    move-result-object v5

    monitor-enter v5
    :try_end_27
    .catch Ljava/lang/InterruptedException; {:try_start_27 .. :try_end_27} :catch_6
    .catch Ljava/lang/Throwable; {:try_start_27 .. :try_end_27} :catch_7

    .line 2904
    :try_start_28
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bRangeRequestSuccess:I
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v4

    const/4 v9, 0x1

    if-ne v4, v9, :cond_52

    .line 2905
    invoke-virtual/range {p0 .. p0}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->closeHTTP()V

    .line 2906
    monitor-exit v5

    goto/16 :goto_9

    .line 2915
    :catchall_c
    move-exception v4

    monitor-exit v5
    :try_end_28
    .catchall {:try_start_28 .. :try_end_28} :catchall_c

    :try_start_29
    throw v4
    :try_end_29
    .catch Ljava/lang/InterruptedException; {:try_start_29 .. :try_end_29} :catch_6
    .catch Ljava/lang/Throwable; {:try_start_29 .. :try_end_29} :catch_7

    .line 2908
    :cond_52
    :try_start_2a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bRangeRequestSuccess:I
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v4

    if-nez v4, :cond_5b

    .line 2909
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    const/4 v9, 0x3

    # setter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bRangeRequestSuccess:I
    invoke-static {v4, v9}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2402(Lcom/android/okhttp/internal/http/MultiSocketInputStream;I)I

    .line 2915
    :cond_53
    :goto_17
    monitor-exit v5
    :try_end_2a
    .catchall {:try_start_2a .. :try_end_2a} :catchall_c

    .line 2918
    :cond_54
    :try_start_2b
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->t2:J

    .line 2919
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->t2:J

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->t1:J

    sub-long v82, v4, v10

    .line 2920
    .local v82, "tmpH":J
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mBlockManager:Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;

    move-result-object v4

    iget-object v4, v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->headerTime:[J

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->sockID:I

    aput-wide v82, v4, v5

    .line 2921
    sget-boolean v4, Lcom/android/okhttp/internal/http/HttpEngine;->HTTPTIMER:Z

    if-eqz v4, :cond_55

    .line 2922
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->timer:[J

    const/4 v5, 0x1

    aget-wide v10, v4, v5

    add-long v10, v10, v82

    aput-wide v10, v4, v5

    .line 2923
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_55

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "time used for read range rsp header:"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v82

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 2926
    :cond_55
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_56

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "start to read body for block["

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v38

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, "], bytes to read: "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v26

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 2928
    :cond_56
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->dbuf:Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

    monitor-enter v5
    :try_end_2b
    .catch Ljava/lang/InterruptedException; {:try_start_2b .. :try_end_2b} :catch_6
    .catch Ljava/lang/Throwable; {:try_start_2b .. :try_end_2b} :catch_7

    .line 2929
    :try_start_2c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->dbuf:Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

    invoke-virtual {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->startReceiveData()V

    .line 2930
    monitor-exit v5
    :try_end_2c
    .catchall {:try_start_2c .. :try_end_2c} :catchall_e

    .line 2931
    const/16 v57, 0x0

    .line 2932
    .local v57, "listId":I
    const/16 v39, 0x0

    .line 2935
    .local v39, "dataRead":I
    :try_start_2d
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v30

    .local v30, "chunkStartTime":J
    move/from16 v58, v57

    .line 2936
    .end local v57    # "listId":I
    .local v58, "listId":I
    :goto_18
    cmp-long v4, v88, v26

    if-gez v4, :cond_73

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-boolean v4, v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bFinished:Z

    if-nez v4, :cond_73

    .line 2938
    const/16 v21, 0x0

    .line 2941
    .local v21, "bsize":I
    if-nez v28, :cond_5c

    .line 2942
    move/from16 v0, v54

    int-to-long v4, v0

    sub-long v10, v26, v88

    invoke-static {v4, v5, v10, v11}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    long-to-int v0, v4

    move/from16 v21, v0

    .line 2946
    :goto_19
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_57

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "RangeRequest alloc memory for new block size: "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v21

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 2947
    :cond_57
    move/from16 v0, v21

    new-array v0, v0, [B

    move-object/from16 v22, v0

    .line 2948
    .local v22, "buf":[B
    sget v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->MAX_BUFFERLEN:I

    int-to-long v4, v4

    shl-int/lit8 v9, v54, 0x1

    int-to-long v10, v9

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->BlockSize:J
    invoke-static {v9}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v12

    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v10

    invoke-static {v4, v5, v10, v11}, Ljava/lang/Math;->min(JJ)J
    :try_end_2d
    .catch Ljava/lang/InterruptedException; {:try_start_2d .. :try_end_2d} :catch_6
    .catch Ljava/lang/Throwable; {:try_start_2d .. :try_end_2d} :catch_7

    move-result-wide v4

    long-to-int v0, v4

    move/from16 v54, v0

    .line 2949
    const/16 v23, 0x0

    .line 2950
    const/16 v24, 0x0

    .line 2953
    :try_start_2e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-object v5, v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->lockAppReading:Ljava/lang/Object;

    monitor-enter v5
    :try_end_2e
    .catch Ljava/lang/Throwable; {:try_start_2e .. :try_end_2e} :catch_9
    .catch Ljava/lang/InterruptedException; {:try_start_2e .. :try_end_2e} :catch_6

    .line 2954
    :cond_58
    :goto_1a
    :try_start_2f
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-boolean v4, v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bAppReading:Z

    if-nez v4, :cond_5d

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-boolean v4, v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bFinished:Z

    if-nez v4, :cond_5d

    .line 2955
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_59

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    const-string v9, "APP not reading, waiting"

    invoke-virtual {v4, v9}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V
    :try_end_2f
    .catchall {:try_start_2f .. :try_end_2f} :catchall_d

    .line 2957
    :cond_59
    :try_start_30
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-object v4, v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->lockAppReading:Ljava/lang/Object;

    const-wide/16 v10, 0x3e8

    invoke-virtual {v4, v10, v11}, Ljava/lang/Object;->wait(J)V
    :try_end_30
    .catch Ljava/lang/Throwable; {:try_start_30 .. :try_end_30} :catch_8
    .catchall {:try_start_30 .. :try_end_30} :catchall_d

    goto :goto_1a

    .line 2959
    :catch_8
    move-exception v44

    .line 2960
    .restart local v44    # "e":Ljava/lang/Throwable;
    :try_start_31
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_58

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    move-object/from16 v0, v44

    invoke-virtual {v4, v0}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V

    goto :goto_1a

    .line 2963
    .end local v44    # "e":Ljava/lang/Throwable;
    :catchall_d
    move-exception v4

    monitor-exit v5
    :try_end_31
    .catchall {:try_start_31 .. :try_end_31} :catchall_d

    :try_start_32
    throw v4
    :try_end_32
    .catch Ljava/lang/Throwable; {:try_start_32 .. :try_end_32} :catch_9
    .catch Ljava/lang/InterruptedException; {:try_start_32 .. :try_end_32} :catch_6

    .line 3004
    :catch_9
    move-exception v45

    .line 3005
    .local v45, "ex":Ljava/lang/Throwable;
    :try_start_33
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->bHandover:Z

    if-eqz v4, :cond_67

    .line 3006
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_5a

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "This block shall be read by another socket, this socket is slow: block["

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v38

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, "], socket["

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget v9, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->sockID:I

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, "]"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 3007
    :cond_5a
    move/from16 v0, v23

    int-to-long v4, v0

    sub-long v88, v88, v4

    .line 3008
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->totalReadSize:J

    move/from16 v0, v23

    int-to-long v10, v0

    sub-long/2addr v4, v10

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->totalReadSize:J

    .line 3009
    new-instance v4, Ljava/lang/InterruptedException;

    invoke-direct {v4}, Ljava/lang/InterruptedException;-><init>()V

    throw v4
    :try_end_33
    .catch Ljava/lang/InterruptedException; {:try_start_33 .. :try_end_33} :catch_6
    .catch Ljava/lang/Throwable; {:try_start_33 .. :try_end_33} :catch_7

    .line 2911
    .end local v21    # "bsize":I
    .end local v22    # "buf":[B
    .end local v30    # "chunkStartTime":J
    .end local v39    # "dataRead":I
    .end local v45    # "ex":Ljava/lang/Throwable;
    .end local v58    # "listId":I
    .end local v82    # "tmpH":J
    :cond_5b
    :try_start_34
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bRangeRequestSuccess:I
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v4

    const/4 v9, 0x3

    if-ne v4, v9, :cond_53

    .line 2912
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    const/4 v9, 0x4

    # setter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bRangeRequestSuccess:I
    invoke-static {v4, v9}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2402(Lcom/android/okhttp/internal/http/MultiSocketInputStream;I)I

    .line 2913
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mBlockManager:Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V
    :try_end_34
    .catchall {:try_start_34 .. :try_end_34} :catchall_c

    goto/16 :goto_17

    .line 2930
    .restart local v82    # "tmpH":J
    :catchall_e
    move-exception v4

    :try_start_35
    monitor-exit v5
    :try_end_35
    .catchall {:try_start_35 .. :try_end_35} :catchall_e

    :try_start_36
    throw v4

    .line 2944
    .restart local v21    # "bsize":I
    .restart local v30    # "chunkStartTime":J
    .restart local v39    # "dataRead":I
    .restart local v58    # "listId":I
    :cond_5c
    move/from16 v0, v54

    int-to-long v4, v0

    aget-wide v10, v28, v25

    move/from16 v0, v39

    int-to-long v12, v0

    sub-long/2addr v10, v12

    invoke-static {v4, v5, v10, v11}, Ljava/lang/Math;->min(JJ)J
    :try_end_36
    .catch Ljava/lang/InterruptedException; {:try_start_36 .. :try_end_36} :catch_6
    .catch Ljava/lang/Throwable; {:try_start_36 .. :try_end_36} :catch_7

    move-result-wide v4

    long-to-int v0, v4

    move/from16 v21, v0

    goto/16 :goto_19

    .line 2963
    .restart local v22    # "buf":[B
    :cond_5d
    :try_start_37
    monitor-exit v5
    :try_end_37
    .catchall {:try_start_37 .. :try_end_37} :catchall_d

    .line 2966
    move/from16 v0, v23

    int-to-long v0, v0

    move-wide/from16 v64, v0

    .line 2967
    .local v64, "prevOffset":J
    :try_start_38
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    .line 2968
    .local v18, "blockStartTime":J
    move-wide/from16 v66, v18

    .line 2969
    .local v66, "prevTime":J
    :cond_5e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->in:Ljava/io/InputStream;

    move-object/from16 v0, v22

    array-length v5, v0

    sub-int v5, v5, v23

    move-object/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v4, v0, v1, v5}, Ljava/io/InputStream;->read([BII)I

    move-result v56

    if-lez v56, :cond_64

    .line 2970
    move/from16 v0, v56

    int-to-long v4, v0

    add-long v88, v88, v4

    .line 2971
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->totalReadSize:J

    move/from16 v0, v56

    int-to-long v10, v0

    add-long/2addr v4, v10

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->totalReadSize:J

    .line 2972
    add-int v23, v23, v56

    .line 2973
    add-int v39, v39, v56

    .line 2976
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mBlockManager:Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;

    move-result-object v4

    iget-object v4, v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->toBeReadLen:[J

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->sockID:I

    aget-wide v10, v4, v5

    move/from16 v0, v56

    int-to-long v12, v0

    sub-long/2addr v10, v12

    aput-wide v10, v4, v5

    .line 2977
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v36

    .line 2978
    .local v36, "curTime":J
    move/from16 v0, v23

    int-to-long v0, v0

    move-wide/from16 v34, v0

    .line 2979
    .local v34, "curOffset":J
    sub-long v42, v36, v30

    .line 2980
    .local v42, "diffTime":J
    sub-long v62, v36, v66

    .line 2981
    .local v62, "offsetTime":J
    sub-long v40, v34, v64

    .line 2982
    .local v40, "diffOffset":J
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget v4, v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->MIN_BYTES_TO_CALC_SPEED:I

    int-to-long v4, v4

    cmp-long v4, v88, v4

    if-gez v4, :cond_60

    sget v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->MIN_BYTES_TO_CALC_INIT_SPEED:I

    int-to-long v4, v4

    cmp-long v4, v88, v4

    if-ltz v4, :cond_5f

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mBlockManager:Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;

    move-result-object v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->sockID:I

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->getSpeed(I)J

    move-result-wide v4

    const-wide/16 v10, 0x0

    cmp-long v4, v4, v10

    if-eqz v4, :cond_60

    :cond_5f
    sget v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->MIN_MILS_TO_CALC_SPEED:I

    int-to-long v4, v4

    cmp-long v4, v42, v4

    if-ltz v4, :cond_63

    .line 2986
    :cond_60
    sget v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->MINSIZEFORSPEED:I

    int-to-long v4, v4

    cmp-long v4, v40, v4

    if-ltz v4, :cond_61

    const-wide/16 v4, 0x5

    cmp-long v4, v62, v4

    if-gez v4, :cond_62

    :cond_61
    const-wide/16 v4, 0x1e

    cmp-long v4, v62, v4

    if-ltz v4, :cond_63

    .line 2987
    :cond_62
    const-wide/16 v4, 0x8

    mul-long v4, v4, v88

    div-long v84, v4, v42

    .line 2988
    .local v84, "tmpSpeed":J
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mBlockManager:Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;

    move-result-object v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->sockID:I

    move-wide/from16 v0, v84

    invoke-virtual {v4, v5, v0, v1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->setSpeed(IJ)V

    .line 2989
    move-wide/from16 v64, v34

    .line 2990
    move-wide/from16 v66, v36

    .line 2994
    .end local v84    # "tmpSpeed":J
    :cond_63
    sub-long v4, v36, v18

    const-wide/16 v10, 0x3e8

    cmp-long v4, v4, v10

    if-lez v4, :cond_5e

    .line 2995
    move/from16 v0, v23

    new-array v0, v0, [B

    move-object/from16 v79, v0

    .line 2996
    .local v79, "tmpBuf":[B
    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, v22

    move-object/from16 v1, v79

    move/from16 v2, v23

    invoke-static {v0, v4, v1, v5, v2}, Ljava/lang/System;->arraycopy([BI[BII)V
    :try_end_38
    .catch Ljava/lang/Throwable; {:try_start_38 .. :try_end_38} :catch_9
    .catch Ljava/lang/InterruptedException; {:try_start_38 .. :try_end_38} :catch_6

    .line 2997
    move-object/from16 v22, v79

    .line 3017
    .end local v34    # "curOffset":J
    .end local v36    # "curTime":J
    .end local v40    # "diffOffset":J
    .end local v42    # "diffTime":J
    .end local v62    # "offsetTime":J
    .end local v79    # "tmpBuf":[B
    :cond_64
    :try_start_39
    move-object/from16 v0, v22

    array-length v4, v0

    move/from16 v0, v23

    if-ne v0, v4, :cond_6f

    .line 3018
    const-wide/16 v80, 0x0

    .line 3019
    .local v80, "tmp":J
    sget-boolean v4, Lcom/android/okhttp/internal/http/HttpEngine;->HTTPTIMER:Z

    if-eqz v4, :cond_65

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v80

    .line 3020
    :cond_65
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->dbuf:Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

    monitor-enter v5
    :try_end_39
    .catch Ljava/lang/InterruptedException; {:try_start_39 .. :try_end_39} :catch_6
    .catch Ljava/lang/Throwable; {:try_start_39 .. :try_end_39} :catch_7

    .line 3021
    :try_start_3a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->dbuf:Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

    move-object/from16 v0, p0

    iget v9, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->sockID:I

    move-object/from16 v0, v22

    array-length v10, v0

    move-object/from16 v0, v22

    invoke-virtual {v4, v9, v0, v10}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->push(I[BI)J

    move-result-wide v10

    long-to-int v0, v10

    move/from16 v24, v0

    .line 3022
    if-gez v24, :cond_68

    .line 3023
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_66

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "This block is already read by another socket, this socket is slow: block["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move/from16 v0, v38

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "], socket["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-object/from16 v0, p0

    iget v10, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->sockID:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "]"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 3024
    :cond_66
    move/from16 v0, v23

    int-to-long v10, v0

    sub-long v88, v88, v10

    .line 3025
    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->totalReadSize:J

    move/from16 v0, v23

    int-to-long v12, v0

    sub-long/2addr v10, v12

    move-object/from16 v0, p0

    iput-wide v10, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->totalReadSize:J

    .line 3026
    new-instance v4, Ljava/lang/InterruptedException;

    invoke-direct {v4}, Ljava/lang/InterruptedException;-><init>()V

    throw v4

    .line 3032
    :catchall_f
    move-exception v4

    monitor-exit v5
    :try_end_3a
    .catchall {:try_start_3a .. :try_end_3a} :catchall_f

    :try_start_3b
    throw v4

    .line 3012
    .end local v18    # "blockStartTime":J
    .end local v64    # "prevOffset":J
    .end local v66    # "prevTime":J
    .end local v80    # "tmp":J
    .restart local v45    # "ex":Ljava/lang/Throwable;
    :cond_67
    throw v45
    :try_end_3b
    .catch Ljava/lang/InterruptedException; {:try_start_3b .. :try_end_3b} :catch_6
    .catch Ljava/lang/Throwable; {:try_start_3b .. :try_end_3b} :catch_7

    .line 3029
    .end local v45    # "ex":Ljava/lang/Throwable;
    .restart local v18    # "blockStartTime":J
    .restart local v64    # "prevOffset":J
    .restart local v66    # "prevTime":J
    .restart local v80    # "tmp":J
    :cond_68
    :try_start_3c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->dbuf:Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V
    :try_end_3c
    .catch Ljava/lang/Throwable; {:try_start_3c .. :try_end_3c} :catch_c
    .catchall {:try_start_3c .. :try_end_3c} :catchall_f

    .line 3032
    :goto_1b
    :try_start_3d
    monitor-exit v5
    :try_end_3d
    .catchall {:try_start_3d .. :try_end_3d} :catchall_f

    .line 3033
    :try_start_3e
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_8b

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "RangeRequest response buffer["

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    add-int v9, v38, v25

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, "]["

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    add-int/lit8 v57, v58, 0x1

    .end local v58    # "listId":I
    .restart local v57    # "listId":I
    move/from16 v0, v58

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, "] inserted: "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v22

    array-length v9, v0

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 3037
    :goto_1c
    if-eqz v28, :cond_6d

    .line 3039
    move/from16 v0, v39

    int-to-long v4, v0

    aget-wide v10, v28, v25

    cmp-long v4, v4, v10

    if-nez v4, :cond_69

    add-int/lit8 v4, v86, -0x1

    move/from16 v0, v25

    if-ge v0, v4, :cond_69

    .line 3040
    add-int/lit8 v25, v25, 0x1

    .line 3041
    add-int v8, v38, v25

    .line 3042
    .local v8, "blockNum":I
    int-to-long v4, v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->BlockSize:J
    invoke-static {v9}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v10

    mul-long/2addr v4, v10

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mOffset:J
    invoke-static {v9}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v10

    add-long v6, v4, v10

    .line 3044
    .local v6, "startIndex":J
    const/16 v39, 0x0

    .line 3045
    const/16 v57, 0x0

    .line 3049
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->inBuffer:Ljava/util/HashMap;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Ljava/util/HashMap;

    move-result-object v87

    monitor-enter v87
    :try_end_3e
    .catch Ljava/lang/InterruptedException; {:try_start_3e .. :try_end_3e} :catch_6
    .catch Ljava/lang/Throwable; {:try_start_3e .. :try_end_3e} :catch_7

    .line 3050
    :try_start_3f
    new-instance v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    aget-wide v9, v28, v25

    new-instance v11, Ljava/util/LinkedList;

    invoke-direct {v11}, Ljava/util/LinkedList;-><init>()V

    move-object/from16 v0, p0

    iget v12, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->sockID:I

    const/4 v13, 0x1

    invoke-direct/range {v4 .. v13}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;-><init>(Lcom/android/okhttp/internal/http/MultiSocketInputStream;JIJLjava/util/LinkedList;II)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->dbuf:Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

    .line 3053
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->inBuffer:Ljava/util/HashMap;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Ljava/util/HashMap;

    move-result-object v4

    new-instance v5, Ljava/lang/Integer;

    invoke-direct {v5, v8}, Ljava/lang/Integer;-><init>(I)V

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->dbuf:Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

    invoke-virtual {v4, v5, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3054
    monitor-exit v87
    :try_end_3f
    .catchall {:try_start_3f .. :try_end_3f} :catchall_10

    .line 3055
    :try_start_40
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->dbuf:Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

    monitor-enter v5
    :try_end_40
    .catch Ljava/lang/InterruptedException; {:try_start_40 .. :try_end_40} :catch_6
    .catch Ljava/lang/Throwable; {:try_start_40 .. :try_end_40} :catch_7

    .line 3056
    :try_start_41
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->dbuf:Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

    invoke-virtual {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->startReceiveData()V

    .line 3057
    monitor-exit v5
    :try_end_41
    .catchall {:try_start_41 .. :try_end_41} :catchall_11

    .line 3060
    .end local v6    # "startIndex":J
    .end local v8    # "blockNum":I
    :cond_69
    :try_start_42
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v74

    .line 3066
    .local v74, "startChunkTime":J
    :cond_6a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->inBuffer:Ljava/util/HashMap;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Ljava/util/HashMap;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/HashMap;->size()I

    move-result v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->iMaxBlockNumber:I
    invoke-static {v5}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$5800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v5

    if-lt v4, v5, :cond_6d

    .line 3067
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->inBuffer:Ljava/util/HashMap;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Ljava/util/HashMap;

    move-result-object v5

    monitor-enter v5
    :try_end_42
    .catch Ljava/lang/InterruptedException; {:try_start_42 .. :try_end_42} :catch_6
    .catch Ljava/lang/Throwable; {:try_start_42 .. :try_end_42} :catch_7

    .line 3069
    :try_start_43
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->inBuffer:Ljava/util/HashMap;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Ljava/util/HashMap;

    move-result-object v4

    sget v9, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->WAIT_TIME_TILL_READ_FROM_COMMON_BUFFER:I

    int-to-long v10, v9

    invoke-virtual {v4, v10, v11}, Ljava/lang/Object;->wait(J)V
    :try_end_43
    .catch Ljava/lang/Throwable; {:try_start_43 .. :try_end_43} :catch_a
    .catchall {:try_start_43 .. :try_end_43} :catchall_12

    .line 3073
    :cond_6b
    :goto_1d
    :try_start_44
    monitor-exit v5
    :try_end_44
    .catchall {:try_start_44 .. :try_end_44} :catchall_12

    .line 3078
    :try_start_45
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v4, v4, v74

    sget v9, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->WAIT_FOR_INTERFACE_TIME_OUT:I

    int-to-long v10, v9

    cmp-long v4, v4, v10

    if-lez v4, :cond_6a

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesRemaining:J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$700(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesForMultiSocket:J
    invoke-static {v9}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1500(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v10

    cmp-long v4, v4, v10

    if-gtz v4, :cond_6a

    .line 3080
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_6c

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    const-string v5, "RangeRequest wait time out"

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 3081
    :cond_6c
    new-instance v4, Ljava/io/IOException;

    const-string v5, "main thread is not reading!"

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_45
    .catch Ljava/lang/InterruptedException; {:try_start_45 .. :try_end_45} :catch_6
    .catch Ljava/lang/Throwable; {:try_start_45 .. :try_end_45} :catch_7

    .line 3054
    .end local v74    # "startChunkTime":J
    .restart local v6    # "startIndex":J
    .restart local v8    # "blockNum":I
    :catchall_10
    move-exception v4

    :try_start_46
    monitor-exit v87
    :try_end_46
    .catchall {:try_start_46 .. :try_end_46} :catchall_10

    :try_start_47
    throw v4
    :try_end_47
    .catch Ljava/lang/InterruptedException; {:try_start_47 .. :try_end_47} :catch_6
    .catch Ljava/lang/Throwable; {:try_start_47 .. :try_end_47} :catch_7

    .line 3057
    :catchall_11
    move-exception v4

    :try_start_48
    monitor-exit v5
    :try_end_48
    .catchall {:try_start_48 .. :try_end_48} :catchall_11

    :try_start_49
    throw v4
    :try_end_49
    .catch Ljava/lang/InterruptedException; {:try_start_49 .. :try_end_49} :catch_6
    .catch Ljava/lang/Throwable; {:try_start_49 .. :try_end_49} :catch_7

    .line 3070
    .end local v6    # "startIndex":J
    .end local v8    # "blockNum":I
    .restart local v74    # "startChunkTime":J
    :catch_a
    move-exception v44

    .line 3071
    .restart local v44    # "e":Ljava/lang/Throwable;
    :try_start_4a
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_6b

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    move-object/from16 v0, v44

    invoke-virtual {v4, v0}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V

    goto :goto_1d

    .line 3073
    .end local v44    # "e":Ljava/lang/Throwable;
    :catchall_12
    move-exception v4

    monitor-exit v5
    :try_end_4a
    .catchall {:try_start_4a .. :try_end_4a} :catchall_12

    :try_start_4b
    throw v4

    .line 3085
    .end local v74    # "startChunkTime":J
    :cond_6d
    sget-boolean v4, Lcom/android/okhttp/internal/http/HttpEngine;->HTTPTIMER:Z

    if-eqz v4, :cond_6e

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->timer:[J

    const/4 v5, 0x3

    aget-wide v10, v4, v5

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    sub-long v12, v12, v80

    add-long/2addr v10, v12

    aput-wide v10, v4, v5

    :cond_6e
    move/from16 v58, v57

    .line 3095
    .end local v57    # "listId":I
    .restart local v58    # "listId":I
    goto/16 :goto_18

    .line 3087
    .end local v80    # "tmp":J
    :cond_6f
    const/4 v4, -0x1

    move/from16 v0, v56

    if-ne v0, v4, :cond_71

    cmp-long v4, v88, v26

    if-gez v4, :cond_71

    .line 3088
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_70

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "RangeRequest read body Exception: totallen="

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v88

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, ", bytesToRead= "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v26

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 3089
    :cond_70
    new-instance v4, Ljava/io/IOException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "RangeRequest read body Exception: totallen="

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v88

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, ", bytesToRead= "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v26

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 3092
    :cond_71
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_72

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "RangeRequest read body Exception2: totallen="

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v88

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, ", bytesToRead= "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v26

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 3093
    :cond_72
    new-instance v4, Ljava/io/IOException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "RangeRequest read body Exception2: totallen="

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v88

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, ", bytesToRead= "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v26

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 3096
    .end local v18    # "blockStartTime":J
    .end local v21    # "bsize":I
    .end local v22    # "buf":[B
    .end local v64    # "prevOffset":J
    .end local v66    # "prevTime":J
    :cond_73
    sget-boolean v4, Lcom/android/okhttp/internal/http/HttpEngine;->HTTPTIMER:Z

    if-eqz v4, :cond_74

    .line 3097
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->t1:J

    .line 3098
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->timer:[J

    const/4 v5, 0x2

    aget-wide v10, v4, v5

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->t1:J

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->t2:J

    move-wide/from16 v90, v0

    sub-long v12, v12, v90

    add-long/2addr v10, v12

    aput-wide v10, v4, v5

    .line 3099
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_74

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "time used for read range rsp body:"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->t1:J

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->t2:J

    sub-long/2addr v10, v12

    invoke-virtual {v5, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 3101
    :cond_74
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "finish to read body, bytes read: "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v88

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V
    :try_end_4b
    .catch Ljava/lang/InterruptedException; {:try_start_4b .. :try_end_4b} :catch_6
    .catch Ljava/lang/Throwable; {:try_start_4b .. :try_end_4b} :catch_7

    goto/16 :goto_1

    .line 3124
    .end local v30    # "chunkStartTime":J
    .end local v32    # "conLen":J
    .end local v39    # "dataRead":I
    .end local v50    # "fullSize":J
    .end local v52    # "headBytes":[B
    .end local v56    # "len":I
    .end local v58    # "listId":I
    .end local v71    # "requestLine":Ljava/lang/String;
    .end local v72    # "rsp":Lcom/android/okhttp/Response;
    .end local v73    # "rspCode":I
    .end local v82    # "tmpH":J
    .restart local v44    # "e":Ljava/lang/Throwable;
    :cond_75
    :try_start_4c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mBlockManager:Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;

    move-result-object v4

    move-object/from16 v0, p0

    iget v9, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->sockID:I

    move/from16 v0, v38

    invoke-virtual {v4, v0, v9}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->block(II)V

    goto/16 :goto_16

    .line 3131
    :cond_76
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bRangeRequestSuccess:I
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v4

    if-eqz v4, :cond_77

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bRangeRequestSuccess:I
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v4

    const/4 v9, 0x3

    if-ne v4, v9, :cond_79

    .line 3132
    :cond_77
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_78

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    const-string v9, "move to RR_FAILED  in first block exception"

    invoke-virtual {v4, v9}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 3133
    :cond_78
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    const/4 v9, 0x1

    # setter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bRangeRequestSuccess:I
    invoke-static {v4, v9}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2402(Lcom/android/okhttp/internal/http/MultiSocketInputStream;I)I

    .line 3134
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mBlockManager:Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V

    .line 3135
    invoke-virtual/range {p0 .. p0}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->closeHTTP()V

    .line 3136
    monitor-exit v5

    goto/16 :goto_9

    .line 3138
    :cond_79
    monitor-exit v5
    :try_end_4c
    .catchall {:try_start_4c .. :try_end_4c} :catchall_a

    .line 3142
    :cond_7a
    move-object/from16 v0, v44

    instance-of v4, v0, Ljava/lang/OutOfMemoryError;

    if-eqz v4, :cond_7c

    .line 3143
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->inBuffer:Ljava/util/HashMap;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Ljava/util/HashMap;

    move-result-object v5

    monitor-enter v5

    .line 3144
    :try_start_4d
    invoke-virtual/range {p0 .. p0}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->closeHTTP()V

    .line 3145
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->requestHandlers:[Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;

    move-result-object v4

    move-object/from16 v0, p0

    iget v9, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->sockID:I

    add-int/lit8 v9, v9, 0x1

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->socketNumber:I
    invoke-static {v10}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3300(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v10

    rem-int/2addr v9, v10

    aget-object v4, v4, v9

    invoke-virtual {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->closeHTTP()V

    .line 3146
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->readBlockNumber:Ljava/lang/Integer;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$5900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v53

    .restart local v53    # "i":I
    :goto_1e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B

    move-result-object v4

    array-length v4, v4

    move/from16 v0, v53

    if-ge v0, v4, :cond_7b

    .line 3147
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->inBuffer:Ljava/util/HashMap;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Ljava/util/HashMap;

    move-result-object v4

    new-instance v9, Ljava/lang/Integer;

    move/from16 v0, v53

    invoke-direct {v9, v0}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v4, v9}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3146
    add-int/lit8 v53, v53, 0x1

    goto :goto_1e

    .line 3149
    :cond_7b
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 3150
    monitor-exit v5

    goto/16 :goto_1

    .end local v53    # "i":I
    :catchall_13
    move-exception v4

    monitor-exit v5
    :try_end_4d
    .catchall {:try_start_4d .. :try_end_4d} :catchall_13

    throw v4

    .line 3154
    :cond_7c
    if-gtz v24, :cond_7d

    .line 3155
    move/from16 v0, v23

    int-to-long v4, v0

    sub-long v88, v88, v4

    .line 3156
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->totalReadSize:J

    move/from16 v0, v23

    int-to-long v10, v0

    sub-long/2addr v4, v10

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->totalReadSize:J

    .line 3158
    :cond_7d
    cmp-long v4, v88, v26

    if-gez v4, :cond_80

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-boolean v4, v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bFinished:Z

    if-nez v4, :cond_80

    .line 3159
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mBlockManager:Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;

    move-result-object v5

    monitor-enter v5

    .line 3163
    if-lez v86, :cond_7e

    .line 3164
    :try_start_4e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mBlockManager:Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;

    move-result-object v4

    add-int v9, v38, v25

    move-object/from16 v0, p0

    iget v10, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->sockID:I

    invoke-virtual {v4, v9, v10}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->block(II)V

    .line 3168
    :goto_1f
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mBlockManager:Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V

    .line 3169
    monitor-exit v5
    :try_end_4e
    .catchall {:try_start_4e .. :try_end_4e} :catchall_14

    .line 3171
    if-lez v86, :cond_7f

    .line 3172
    add-int/lit8 v53, v25, 0x1

    .restart local v53    # "i":I
    :goto_20
    move/from16 v0, v53

    move/from16 v1, v86

    if-ge v0, v1, :cond_7f

    .line 3173
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B

    move-result-object v4

    add-int v5, v38, v53

    const/4 v9, 0x0

    aput-byte v9, v4, v5

    .line 3172
    add-int/lit8 v53, v53, 0x1

    goto :goto_20

    .line 3166
    .end local v53    # "i":I
    :cond_7e
    :try_start_4f
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mBlockManager:Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;

    move-result-object v4

    move-object/from16 v0, p0

    iget v9, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->sockID:I

    move/from16 v0, v38

    invoke-virtual {v4, v0, v9}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;->block(II)V

    goto :goto_1f

    .line 3169
    :catchall_14
    move-exception v4

    monitor-exit v5
    :try_end_4f
    .catchall {:try_start_4f .. :try_end_4f} :catchall_14

    throw v4

    .line 3176
    :cond_7f
    invoke-virtual/range {p0 .. p0}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->closeHTTP()V

    .line 3178
    :cond_80
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    const-string v5, "Checking the block is moved to blocked state"

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 3191
    .end local v17    # "blockInfo":[J
    .end local v23    # "buf_offset":I
    .end local v24    # "buf_ret":I
    .end local v25    # "chunkIndex":I
    .end local v26    # "bytesToRead":J
    .end local v28    # "chunkLengths":[J
    .end local v38    # "currentBlockNumber":I
    .end local v44    # "e":Ljava/lang/Throwable;
    .end local v46    # "expectWaitBufferTime":J
    .end local v61    # "otherSocketId":I
    .end local v68    # "range":Ljava/lang/StringBuffer;
    .end local v69    # "redirectedUri":Ljava/net/URI;
    .end local v70    # "requestHeaders":Lcom/android/okhttp/Request;
    .end local v86    # "totalContinuousChunk":I
    .end local v88    # "totallen":J
    .restart local v53    # "i":I
    .restart local v78    # "timerStr":Ljava/lang/StringBuffer;
    :cond_81
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual/range {v78 .. v78}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 3193
    .end local v53    # "i":I
    .end local v78    # "timerStr":Ljava/lang/StringBuffer;
    :cond_82
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_83

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    const-string v5, "download thread end"

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 3194
    :cond_83
    const/16 v16, 0x0

    .line 3195
    .local v16, "bSubmitHere":Z
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mBlockManager:Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;

    move-result-object v5

    monitor-enter v5

    .line 3196
    :try_start_50
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->finishedThreadNum:I
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3200(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v4

    if-nez v4, :cond_85

    const/16 v16, 0x1

    .line 3197
    :goto_21
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # operator++ for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->finishedThreadNum:I
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3208(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    .line 3198
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_84

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "total finished thread num = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->finishedThreadNum:I
    invoke-static {v10}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3200(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 3199
    :cond_84
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mBlockManager:Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V

    .line 3200
    :goto_22
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->finishedThreadNum:I
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3200(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v4

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->socketNumber:I
    invoke-static {v9}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3300(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I
    :try_end_50
    .catchall {:try_start_50 .. :try_end_50} :catchall_15

    move-result v9

    if-ge v4, v9, :cond_86

    .line 3202
    :try_start_51
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mBlockManager:Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;

    move-result-object v4

    const-wide/16 v10, 0x3e8

    invoke-virtual {v4, v10, v11}, Ljava/lang/Object;->wait(J)V
    :try_end_51
    .catch Ljava/lang/Throwable; {:try_start_51 .. :try_end_51} :catch_b
    .catchall {:try_start_51 .. :try_end_51} :catchall_15

    goto :goto_22

    .line 3204
    :catch_b
    move-exception v4

    goto :goto_22

    .line 3196
    :cond_85
    const/16 v16, 0x0

    goto :goto_21

    .line 3206
    :cond_86
    :try_start_52
    monitor-exit v5
    :try_end_52
    .catchall {:try_start_52 .. :try_end_52} :catchall_15

    .line 3207
    if-eqz v16, :cond_88

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->requestHandlers:[Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;

    move-result-object v4

    const/4 v5, 0x0

    aget-object v4, v4, v5

    iget-boolean v4, v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->bIOExceptionDuringContinueChunk:Z

    if-nez v4, :cond_88

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->requestHandlers:[Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;

    move-result-object v4

    const/4 v5, 0x1

    aget-object v4, v4, v5

    iget-boolean v4, v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->bIOExceptionDuringContinueChunk:Z

    if-nez v4, :cond_88

    .line 3208
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_87

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    const-string v5, "submit statistics data to ConnectivityService"

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 3209
    :cond_87
    invoke-direct/range {p0 .. p0}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->submitData()V

    .line 3212
    :cond_88
    # operator-- for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->iCurrentMultiSocketNum:I
    invoke-static {}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1010()I

    .line 3213
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_89

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "now active multi socket instance is "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->iCurrentMultiSocketNum:I
    invoke-static {}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1000()I

    move-result v9

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 3215
    :cond_89
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_8a

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    const-string v5, "download thread exit"

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 3216
    :cond_8a
    return-void

    .line 3206
    :catchall_15
    move-exception v4

    :try_start_53
    monitor-exit v5
    :try_end_53
    .catchall {:try_start_53 .. :try_end_53} :catchall_15

    throw v4

    .line 3031
    .end local v16    # "bSubmitHere":Z
    .restart local v17    # "blockInfo":[J
    .restart local v18    # "blockStartTime":J
    .restart local v21    # "bsize":I
    .restart local v22    # "buf":[B
    .restart local v23    # "buf_offset":I
    .restart local v24    # "buf_ret":I
    .restart local v25    # "chunkIndex":I
    .restart local v26    # "bytesToRead":J
    .restart local v28    # "chunkLengths":[J
    .restart local v30    # "chunkStartTime":J
    .restart local v32    # "conLen":J
    .restart local v38    # "currentBlockNumber":I
    .restart local v39    # "dataRead":I
    .restart local v46    # "expectWaitBufferTime":J
    .restart local v50    # "fullSize":J
    .restart local v52    # "headBytes":[B
    .restart local v56    # "len":I
    .restart local v58    # "listId":I
    .restart local v61    # "otherSocketId":I
    .restart local v64    # "prevOffset":J
    .restart local v66    # "prevTime":J
    .restart local v68    # "range":Ljava/lang/StringBuffer;
    .restart local v69    # "redirectedUri":Ljava/net/URI;
    .restart local v70    # "requestHeaders":Lcom/android/okhttp/Request;
    .restart local v71    # "requestLine":Ljava/lang/String;
    .restart local v72    # "rsp":Lcom/android/okhttp/Response;
    .restart local v73    # "rspCode":I
    .restart local v80    # "tmp":J
    .restart local v82    # "tmpH":J
    .restart local v86    # "totalContinuousChunk":I
    .restart local v88    # "totallen":J
    :catch_c
    move-exception v4

    goto/16 :goto_1b

    .line 2743
    .end local v17    # "blockInfo":[J
    .end local v18    # "blockStartTime":J
    .end local v21    # "bsize":I
    .end local v22    # "buf":[B
    .end local v23    # "buf_offset":I
    .end local v24    # "buf_ret":I
    .end local v25    # "chunkIndex":I
    .end local v26    # "bytesToRead":J
    .end local v28    # "chunkLengths":[J
    .end local v30    # "chunkStartTime":J
    .end local v32    # "conLen":J
    .end local v38    # "currentBlockNumber":I
    .end local v39    # "dataRead":I
    .end local v50    # "fullSize":J
    .end local v52    # "headBytes":[B
    .end local v56    # "len":I
    .end local v58    # "listId":I
    .end local v64    # "prevOffset":J
    .end local v66    # "prevTime":J
    .end local v68    # "range":Ljava/lang/StringBuffer;
    .end local v71    # "requestLine":Ljava/lang/String;
    .end local v72    # "rsp":Lcom/android/okhttp/Response;
    .end local v73    # "rspCode":I
    .end local v80    # "tmp":J
    .end local v82    # "tmpH":J
    .end local v86    # "totalContinuousChunk":I
    .end local v88    # "totallen":J
    .restart local v59    # "localAddress":Ljava/net/InetAddress;
    :catch_d
    move-exception v4

    goto/16 :goto_e

    .line 2619
    .end local v59    # "localAddress":Ljava/net/InetAddress;
    .end local v61    # "otherSocketId":I
    .end local v69    # "redirectedUri":Ljava/net/URI;
    .end local v70    # "requestHeaders":Lcom/android/okhttp/Request;
    .restart local v53    # "i":I
    :catch_e
    move-exception v4

    goto/16 :goto_c

    .end local v53    # "i":I
    .restart local v17    # "blockInfo":[J
    .restart local v18    # "blockStartTime":J
    .restart local v21    # "bsize":I
    .restart local v22    # "buf":[B
    .restart local v23    # "buf_offset":I
    .restart local v24    # "buf_ret":I
    .restart local v25    # "chunkIndex":I
    .restart local v26    # "bytesToRead":J
    .restart local v28    # "chunkLengths":[J
    .restart local v30    # "chunkStartTime":J
    .restart local v32    # "conLen":J
    .restart local v38    # "currentBlockNumber":I
    .restart local v39    # "dataRead":I
    .restart local v50    # "fullSize":J
    .restart local v52    # "headBytes":[B
    .restart local v56    # "len":I
    .restart local v58    # "listId":I
    .restart local v61    # "otherSocketId":I
    .restart local v64    # "prevOffset":J
    .restart local v66    # "prevTime":J
    .restart local v68    # "range":Ljava/lang/StringBuffer;
    .restart local v69    # "redirectedUri":Ljava/net/URI;
    .restart local v70    # "requestHeaders":Lcom/android/okhttp/Request;
    .restart local v71    # "requestLine":Ljava/lang/String;
    .restart local v72    # "rsp":Lcom/android/okhttp/Response;
    .restart local v73    # "rspCode":I
    .restart local v80    # "tmp":J
    .restart local v82    # "tmpH":J
    .restart local v86    # "totalContinuousChunk":I
    .restart local v88    # "totallen":J
    :cond_8b
    move/from16 v57, v58

    .end local v58    # "listId":I
    .restart local v57    # "listId":I
    goto/16 :goto_1c
.end method
