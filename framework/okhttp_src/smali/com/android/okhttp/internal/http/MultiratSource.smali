.class public Lcom/android/okhttp/internal/http/MultiratSource;
.super Ljava/lang/Object;
.source "MultiratSource.java"

# interfaces
.implements Lcom/android/okio/Source;


# static fields
.field private static final MAX_BUFFER:I = 0x200000


# instance fields
.field private deadline:Lcom/android/okio/Deadline;

.field protected mInput:Lcom/android/okhttp/internal/http/MultiSocketInputStream;


# direct methods
.method public constructor <init>(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)V
    .locals 1
    .param p1, "in"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    sget-object v0, Lcom/android/okio/Deadline;->NONE:Lcom/android/okio/Deadline;

    iput-object v0, p0, Lcom/android/okhttp/internal/http/MultiratSource;->deadline:Lcom/android/okio/Deadline;

    .line 21
    iput-object p1, p0, Lcom/android/okhttp/internal/http/MultiratSource;->mInput:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    .line 22
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 44
    iget-object v0, p0, Lcom/android/okhttp/internal/http/MultiratSource;->mInput:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    invoke-static {v0}, Lcom/android/okhttp/internal/http/MultiratUtil;->closeQuietly(Ljava/io/Closeable;)V

    .line 45
    return-void
.end method

.method public deadline(Lcom/android/okio/Deadline;)Lcom/android/okio/Source;
    .locals 2
    .param p1, "deadline"    # Lcom/android/okio/Deadline;

    .prologue
    .line 37
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "deadline == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 38
    :cond_0
    iput-object p1, p0, Lcom/android/okhttp/internal/http/MultiratSource;->deadline:Lcom/android/okio/Deadline;

    .line 39
    return-object p0
.end method

.method public read(Lcom/android/okio/OkBuffer;J)J
    .locals 6
    .param p1, "sink"    # Lcom/android/okio/OkBuffer;
    .param p2, "byteCount"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 26
    const-wide/16 v2, 0x0

    cmp-long v2, p2, v2

    if-gez v2, :cond_0

    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "byteCount < 0: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 27
    :cond_0
    iget-object v2, p0, Lcom/android/okhttp/internal/http/MultiratSource;->deadline:Lcom/android/okio/Deadline;

    invoke-virtual {v2}, Lcom/android/okio/Deadline;->throwIfReached()V

    .line 28
    const-wide/32 v2, 0x200000

    invoke-static {p2, p3, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    long-to-int v2, v2

    new-array v0, v2, [B

    .line 29
    .local v0, "buffer":[B
    iget-object v2, p0, Lcom/android/okhttp/internal/http/MultiratSource;->mInput:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    array-length v3, v0

    invoke-virtual {v2, v0, v4, v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->read([BII)I

    move-result v1

    .line 30
    .local v1, "len":I
    if-lez v1, :cond_1

    .line 31
    invoke-virtual {p1, v0, v4, v1}, Lcom/android/okio/OkBuffer;->write([BII)Lcom/android/okio/OkBuffer;

    .line 32
    :cond_1
    int-to-long v2, v1

    return-wide v2
.end method
