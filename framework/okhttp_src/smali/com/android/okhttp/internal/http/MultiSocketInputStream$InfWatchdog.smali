.class Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;
.super Ljava/lang/Object;
.source "MultiSocketInputStream.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/okhttp/internal/http/MultiSocketInputStream;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "InfWatchdog"
.end annotation


# instance fields
.field bFileTested:Z

.field header:Lcom/android/okhttp/Headers;

.field final synthetic this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;


# direct methods
.method public constructor <init>(Lcom/android/okhttp/internal/http/MultiSocketInputStream;Lcom/android/okhttp/Headers;)V
    .locals 1
    .param p2, "h"    # Lcom/android/okhttp/Headers;

    .prologue
    .line 1042
    iput-object p1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1036
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->bFileTested:Z

    .line 1043
    iput-object p2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->header:Lcom/android/okhttp/Headers;

    .line 1044
    return-void
.end method

.method private testFileCaching()Z
    .locals 12

    .prologue
    const/4 v7, 0x1

    .line 1047
    iget-object v8, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-object v8, v8, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->tmpFileLocker:Ljava/lang/Object;

    monitor-enter v8

    .line 1048
    :try_start_0
    iget-boolean v9, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->bFileTested:Z

    if-eqz v9, :cond_0

    monitor-exit v8

    .line 1103
    :goto_0
    return v7

    .line 1049
    :cond_0
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, ".sbBuf_"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Thread;->getId()J

    move-result-wide v10

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "_"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1050
    .local v0, "bufFileName":Ljava/lang/String;
    sget-boolean v9, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v9, :cond_1

    iget-object v9, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v9}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "try to save buffer to file "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1051
    :cond_1
    const/4 v6, 0x0

    .line 1052
    .local v6, "fileBuf":Ljava/io/File;
    const/4 v1, 0x0

    .line 1053
    .local v1, "dis":Ljava/io/DataInputStream;
    const/4 v3, 0x0

    .line 1055
    .local v3, "dos":Ljava/io/DataOutputStream;
    :try_start_1
    iget-object v9, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    invoke-virtual {v9}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->createBufferDir()V

    .line 1056
    const/4 v9, 0x0

    iget-object v10, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-object v10, v10, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->parentDir:Ljava/io/File;

    invoke-static {v0, v9, v10}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v6

    .line 1057
    sget-boolean v9, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v9, :cond_2

    iget-object v9, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v9}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "resource check: test buffered file generated "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1058
    :cond_2
    new-instance v2, Ljava/io/DataInputStream;

    new-instance v9, Ljava/io/FileInputStream;

    invoke-direct {v9, v6}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v2, v9}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1059
    .end local v1    # "dis":Ljava/io/DataInputStream;
    .local v2, "dis":Ljava/io/DataInputStream;
    :try_start_2
    new-instance v4, Ljava/io/DataOutputStream;

    new-instance v9, Ljava/io/FileOutputStream;

    invoke-direct {v9, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v4, v9}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1060
    .end local v3    # "dos":Ljava/io/DataOutputStream;
    .local v4, "dos":Ljava/io/DataOutputStream;
    const/4 v9, 0x1

    :try_start_3
    invoke-virtual {v4, v9}, Ljava/io/DataOutputStream;->write(I)V

    .line 1061
    if-eqz v2, :cond_b

    .line 1062
    invoke-virtual {v2}, Ljava/io/DataInputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_5
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1063
    const/4 v1, 0x0

    .line 1065
    .end local v2    # "dis":Ljava/io/DataInputStream;
    .restart local v1    # "dis":Ljava/io/DataInputStream;
    :goto_1
    if-eqz v4, :cond_a

    .line 1066
    :try_start_4
    invoke-virtual {v4}, Ljava/io/DataOutputStream;->flush()V

    .line 1067
    invoke-virtual {v4}, Ljava/io/DataOutputStream;->close()V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_6
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1068
    const/4 v3, 0x0

    .line 1070
    .end local v4    # "dos":Ljava/io/DataOutputStream;
    .restart local v3    # "dos":Ljava/io/DataOutputStream;
    :goto_2
    if-eqz v6, :cond_4

    .line 1071
    :try_start_5
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    move-result v9

    if-eqz v9, :cond_5

    .line 1072
    sget-boolean v9, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v9, :cond_3

    iget-object v9, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v9}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "resource check: test buffered file removed "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1077
    :cond_3
    :goto_3
    const/4 v6, 0x0

    .line 1079
    :cond_4
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->bFileTested:Z
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1080
    :try_start_6
    monitor-exit v8

    goto/16 :goto_0

    .line 1105
    .end local v0    # "bufFileName":Ljava/lang/String;
    .end local v1    # "dis":Ljava/io/DataInputStream;
    .end local v3    # "dos":Ljava/io/DataOutputStream;
    .end local v6    # "fileBuf":Ljava/io/File;
    :catchall_0
    move-exception v7

    monitor-exit v8
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    throw v7

    .line 1075
    .restart local v0    # "bufFileName":Ljava/lang/String;
    .restart local v1    # "dis":Ljava/io/DataInputStream;
    .restart local v3    # "dos":Ljava/io/DataOutputStream;
    .restart local v6    # "fileBuf":Ljava/io/File;
    :cond_5
    :try_start_7
    sget-boolean v9, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v9, :cond_3

    iget-object v9, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v9}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "resource check: failed to remove buffered file (main) "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_3

    .line 1082
    :catch_0
    move-exception v5

    .line 1083
    .local v5, "e":Ljava/lang/Throwable;
    :goto_4
    :try_start_8
    sget-boolean v7, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v7, :cond_6

    iget-object v7, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v7

    invoke-virtual {v7, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 1085
    :cond_6
    if-eqz v1, :cond_7

    .line 1086
    :try_start_9
    invoke-virtual {v1}, Ljava/io/DataInputStream;->close()V
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 1087
    const/4 v1, 0x0

    .line 1092
    :cond_7
    :goto_5
    if-eqz v3, :cond_8

    .line 1093
    :try_start_a
    invoke-virtual {v3}, Ljava/io/DataOutputStream;->close()V
    :try_end_a
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_2
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 1094
    const/4 v3, 0x0

    .line 1099
    :cond_8
    :goto_6
    if-eqz v6, :cond_9

    .line 1100
    :try_start_b
    invoke-virtual {v6}, Ljava/io/File;->delete()Z
    :try_end_b
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_3
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 1103
    :cond_9
    :goto_7
    const/4 v7, 0x0

    :try_start_c
    monitor-exit v8
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    goto/16 :goto_0

    .line 1090
    :catch_1
    move-exception v7

    goto :goto_5

    .line 1097
    :catch_2
    move-exception v7

    goto :goto_6

    .line 1102
    :catch_3
    move-exception v7

    goto :goto_7

    .line 1082
    .end local v1    # "dis":Ljava/io/DataInputStream;
    .end local v5    # "e":Ljava/lang/Throwable;
    .restart local v2    # "dis":Ljava/io/DataInputStream;
    :catch_4
    move-exception v5

    move-object v1, v2

    .end local v2    # "dis":Ljava/io/DataInputStream;
    .restart local v1    # "dis":Ljava/io/DataInputStream;
    goto :goto_4

    .end local v1    # "dis":Ljava/io/DataInputStream;
    .end local v3    # "dos":Ljava/io/DataOutputStream;
    .restart local v2    # "dis":Ljava/io/DataInputStream;
    .restart local v4    # "dos":Ljava/io/DataOutputStream;
    :catch_5
    move-exception v5

    move-object v3, v4

    .end local v4    # "dos":Ljava/io/DataOutputStream;
    .restart local v3    # "dos":Ljava/io/DataOutputStream;
    move-object v1, v2

    .end local v2    # "dis":Ljava/io/DataInputStream;
    .restart local v1    # "dis":Ljava/io/DataInputStream;
    goto :goto_4

    .end local v3    # "dos":Ljava/io/DataOutputStream;
    .restart local v4    # "dos":Ljava/io/DataOutputStream;
    :catch_6
    move-exception v5

    move-object v3, v4

    .end local v4    # "dos":Ljava/io/DataOutputStream;
    .restart local v3    # "dos":Ljava/io/DataOutputStream;
    goto :goto_4

    .end local v3    # "dos":Ljava/io/DataOutputStream;
    .restart local v4    # "dos":Ljava/io/DataOutputStream;
    :cond_a
    move-object v3, v4

    .end local v4    # "dos":Ljava/io/DataOutputStream;
    .restart local v3    # "dos":Ljava/io/DataOutputStream;
    goto/16 :goto_2

    .end local v1    # "dis":Ljava/io/DataInputStream;
    .end local v3    # "dos":Ljava/io/DataOutputStream;
    .restart local v2    # "dis":Ljava/io/DataInputStream;
    .restart local v4    # "dos":Ljava/io/DataOutputStream;
    :cond_b
    move-object v1, v2

    .end local v2    # "dis":Ljava/io/DataInputStream;
    .restart local v1    # "dis":Ljava/io/DataInputStream;
    goto/16 :goto_1
.end method


# virtual methods
.method public run()V
    .locals 97

    .prologue
    .line 1110
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Thread;->getId()J

    move-result-wide v6

    # setter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainThreadID:J
    invoke-static {v4, v6, v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$102(Lcom/android/okhttp/internal/http/MultiSocketInputStream;J)J

    .line 1111
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "resource check: thread checking: start interface watchdog "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Thread;->getId()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1112
    :cond_0
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setup main thread id to be  "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainThreadID:J
    invoke-static {v6}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$100(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1114
    :cond_1
    const-string v10, "/"

    .line 1116
    .local v10, "sUrl":Ljava/lang/String;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->uri:Ljava/net/URI;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$200(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Ljava/net/URI;

    move-result-object v4

    invoke-virtual {v4}, Ljava/net/URI;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v10

    .line 1121
    :cond_2
    :goto_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainThreadID:J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$100(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->totalLengthToBeRead:J
    invoke-static {v6}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$300(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v6

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mOriOffset:J
    invoke-static {v8}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v8

    invoke-static/range {v4 .. v10}, Lcom/android/okhttp/internal/http/SBServiceAPI;->startSBDirectUsage(JJJLjava/lang/String;)Z

    move-result v32

    .line 1122
    .local v32, "bShallUseSB":Z
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "startSBUsage return "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v32

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", for thread "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainThreadID:J
    invoke-static {v6}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$100(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", size "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->totalLengthToBeRead:J
    invoke-static {v6}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$300(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1124
    :cond_3
    if-eqz v32, :cond_7

    .line 1125
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "start to getSBUsuageStatus for thread "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainThreadID:J
    invoke-static {v6}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$100(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1126
    :cond_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->sbUsedLocker:Ljava/lang/Object;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$500(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Ljava/lang/Object;

    move-result-object v5

    monitor-enter v5

    .line 1127
    :try_start_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    const/4 v6, 0x1

    # setter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bSBUsed:Z
    invoke-static {v4, v6}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$602(Lcom/android/okhttp/internal/http/MultiSocketInputStream;Z)Z

    .line 1129
    :cond_5
    :goto_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-boolean v4, v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bFinished:Z

    if-nez v4, :cond_6

    .line 1130
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainThreadID:J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$100(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v6

    invoke-static {v6, v7}, Lcom/android/okhttp/internal/http/SBServiceAPI;->getSBUsageStatus(J)I

    move-result v11

    .line 1131
    .local v11, "bSBStatus":I
    const/4 v4, 0x2

    if-ne v11, v4, :cond_10

    .line 1132
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    const/4 v6, 0x0

    # setter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bSBUsed:Z
    invoke-static {v4, v6}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$602(Lcom/android/okhttp/internal/http/MultiSocketInputStream;Z)Z

    .line 1133
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_6

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getSBUsuageStatus get NO for thread "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainThreadID:J
    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$100(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1151
    .end local v11    # "bSBStatus":I
    :cond_6
    :goto_2
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1155
    :cond_7
    :goto_3
    if-eqz v32, :cond_b

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bSBUsed:Z
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$600(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Z

    move-result v4

    if-eqz v4, :cond_b

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-boolean v4, v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bFinished:Z

    if-nez v4, :cond_b

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesRemaining:J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$700(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v4

    const-wide/32 v6, 0x200000

    cmp-long v4, v4, v6

    if-lez v4, :cond_b

    .line 1156
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mBlockManager:Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;

    move-result-object v6

    monitor-enter v6

    .line 1157
    :try_start_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->connection:Lcom/android/okhttp/Connection;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/Connection;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v5}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/okhttp/internal/http/SBServiceAPI;->getSBEnabledDirectByIP(Lcom/android/okhttp/Connection;Lcom/android/okhttp/internal/http/MultiratLog;)I

    move-result v52

    .line 1158
    .local v52, "iInf":I
    const-wide v60, 0x7fffffffffffffffL

    .line 1159
    .local v60, "maxBufDataSize":J
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-object v4, v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mRAdownloadedFile:Ljava/io/RandomAccessFile;

    if-nez v4, :cond_12

    invoke-direct/range {p0 .. p0}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->testFileCaching()Z

    move-result v4

    if-nez v4, :cond_12

    .line 1160
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_8

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "testFileCaching return false, continue while remain data size is "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesRemaining:J
    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$700(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v8

    invoke-virtual {v5, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1161
    :cond_8
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->sbUsedLocker:Ljava/lang/Object;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$500(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Ljava/lang/Object;

    move-result-object v5

    monitor-enter v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 1162
    :try_start_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bSBUsed:Z
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$600(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 1163
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_9

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Failed to create buffer file, current SBUsed is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bSBUsed:Z
    invoke-static {v8}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$600(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Z

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1164
    :cond_9
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainThreadID:J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$100(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v8

    const/4 v4, 0x2

    invoke-static {v8, v9, v4}, Lcom/android/okhttp/internal/http/SBServiceAPI;->stopSBUsageWithReason(JI)I

    .line 1165
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    const/4 v7, 0x0

    # setter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bSBUsed:Z
    invoke-static {v4, v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$602(Lcom/android/okhttp/internal/http/MultiSocketInputStream;Z)Z

    .line 1167
    :cond_a
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1168
    :try_start_4
    monitor-exit v6
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 1350
    .end local v52    # "iInf":I
    .end local v60    # "maxBufDataSize":J
    :cond_b
    :goto_4
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_c

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "finish interface watchdog with bMultisocket="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bMultiSocketStarted:Z
    invoke-static {v6}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3100(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1352
    :cond_c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->TEST_GET_TRAFFIC:Z
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3500(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Z

    move-result v4

    if-nez v4, :cond_d

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bSBUsed:Z
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$600(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Z

    move-result v4

    if-eqz v4, :cond_4b

    .line 1353
    :cond_d
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->MIN_TIME_TO_CHECK_STOP_SLOW:I
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3600(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v4

    int-to-long v0, v4

    move-wide/from16 v64, v0

    .line 1354
    .local v64, "minTimeToCheckStopSlow0":J
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->MIN_TIME_TO_CHECK_STOP_SLOW:I
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3600(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v4

    int-to-long v0, v4

    move-wide/from16 v66, v0

    .line 1355
    .local v66, "minTimeToCheckStopSlow1":J
    const/4 v4, 0x2

    new-array v0, v4, [J

    move-object/from16 v72, v0

    fill-array-data v72, :array_0

    .line 1356
    .local v72, "prevTime":[J
    const/4 v4, 0x2

    new-array v0, v4, [J

    move-object/from16 v70, v0

    fill-array-data v70, :array_1

    .line 1357
    .local v70, "prevBytes":[J
    const/4 v4, 0x2

    new-array v0, v4, [J

    move-object/from16 v71, v0

    fill-array-data v71, :array_2

    .line 1358
    .local v71, "prevSp":[J
    const/16 v62, 0x5

    .line 1359
    .local v62, "maxCountToSend":I
    const-wide/16 v38, 0x7d0

    .line 1360
    .local v38, "byteThresholdAs0":J
    const/4 v4, 0x2

    new-array v0, v4, [I

    move-object/from16 v63, v0

    fill-array-data v63, :array_3

    .line 1361
    .local v63, "noDataCount":[I
    :goto_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-boolean v4, v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bFinished:Z

    if-nez v4, :cond_4b

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-boolean v4, v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->closed:Z

    if-nez v4, :cond_4b

    .line 1362
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->TEST_GET_TRAFFIC:Z
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3500(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Z

    move-result v4

    if-eqz v4, :cond_3b

    .line 1363
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->sbUsedLocker:Ljava/lang/Object;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$500(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Ljava/lang/Object;

    move-result-object v5

    monitor-enter v5

    .line 1364
    :try_start_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bRangeRequestSuccess:I
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v4

    const/4 v6, 0x1

    if-ne v4, v6, :cond_f

    .line 1365
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bSBUsed:Z
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$600(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Z

    move-result v4

    if-eqz v4, :cond_f

    .line 1366
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_e

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Range Request not supported by server, current SBUsed is "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bSBUsed:Z
    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$600(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Z

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1367
    :cond_e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainThreadID:J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$100(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v6

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->failReasonInExtreme:I
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3700(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v4

    invoke-static {v6, v7, v4}, Lcom/android/okhttp/internal/http/SBServiceAPI;->stopSBUsageWithReason(JI)I

    .line 1368
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    const/4 v6, 0x0

    # setter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bSBUsed:Z
    invoke-static {v4, v6}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$602(Lcom/android/okhttp/internal/http/MultiSocketInputStream;Z)Z

    .line 1371
    :cond_f
    monitor-exit v5
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    .line 1372
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->socketNumber:I
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3300(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v4

    new-array v0, v4, [J

    move-object/from16 v55, v0

    .line 1373
    .local v55, "lens":[J
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->socketNumber:I
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3300(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v4

    new-array v0, v4, [J

    move-object/from16 v96, v0

    .line 1374
    .local v96, "times":[J
    const/16 v49, 0x0

    .local v49, "i":I
    :goto_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->socketNumber:I
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3300(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v4

    move/from16 v0, v49

    if-ge v0, v4, :cond_39

    .line 1375
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mDataDownloaded:[J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[J

    move-result-object v4

    aget-wide v6, v4, v49

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    move-result-object v4

    if-nez v4, :cond_37

    const-wide/16 v4, 0x0

    :goto_7
    add-long/2addr v4, v6

    aput-wide v4, v55, v49

    .line 1376
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mTimeForDownload:[J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$4000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[J

    move-result-object v4

    aget-wide v6, v4, v49

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    move-result-object v4

    if-nez v4, :cond_38

    const-wide/16 v4, 0x0

    :goto_8
    add-long/2addr v4, v6

    aput-wide v4, v96, v49

    .line 1374
    add-int/lit8 v49, v49, 0x1

    goto :goto_6

    .line 1118
    .end local v32    # "bShallUseSB":Z
    .end local v38    # "byteThresholdAs0":J
    .end local v49    # "i":I
    .end local v55    # "lens":[J
    .end local v62    # "maxCountToSend":I
    .end local v63    # "noDataCount":[I
    .end local v64    # "minTimeToCheckStopSlow0":J
    .end local v66    # "minTimeToCheckStopSlow1":J
    .end local v70    # "prevBytes":[J
    .end local v71    # "prevSp":[J
    .end local v72    # "prevTime":[J
    .end local v96    # "times":[J
    :catch_0
    move-exception v48

    .line 1119
    .local v48, "ex":Ljava/lang/Throwable;
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    move-object/from16 v0, v48

    invoke-virtual {v4, v0}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 1136
    .end local v48    # "ex":Ljava/lang/Throwable;
    .restart local v11    # "bSBStatus":I
    .restart local v32    # "bShallUseSB":Z
    :cond_10
    const/4 v4, 0x1

    if-ne v11, v4, :cond_11

    .line 1137
    :try_start_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    const/4 v6, 0x1

    # setter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bSBUsed:Z
    invoke-static {v4, v6}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$602(Lcom/android/okhttp/internal/http/MultiSocketInputStream;Z)Z

    .line 1138
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_6

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getSBUsuageStatus get YES for thread "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainThreadID:J
    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$100(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1151
    .end local v11    # "bSBStatus":I
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    throw v4

    .line 1143
    .restart local v11    # "bSBStatus":I
    :cond_11
    :try_start_7
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->sbUsedLocker:Ljava/lang/Object;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$500(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Ljava/lang/Object;

    move-result-object v4

    const-wide/16 v6, 0x64

    invoke-virtual {v4, v6, v7}, Ljava/lang/Object;->wait(J)V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_1

    .line 1145
    :catch_1
    move-exception v47

    .line 1146
    .local v47, "e":Ljava/lang/Throwable;
    :try_start_8
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_5

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    move-object/from16 v0, v47

    invoke-virtual {v4, v0}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_1

    .line 1167
    .end local v11    # "bSBStatus":I
    .end local v47    # "e":Ljava/lang/Throwable;
    .restart local v52    # "iInf":I
    .restart local v60    # "maxBufDataSize":J
    :catchall_1
    move-exception v4

    :try_start_9
    monitor-exit v5
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    :try_start_a
    throw v4

    .line 1347
    .end local v52    # "iInf":I
    .end local v60    # "maxBufDataSize":J
    :catchall_2
    move-exception v4

    monitor-exit v6
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    throw v4

    .line 1170
    .restart local v52    # "iInf":I
    .restart local v60    # "maxBufDataSize":J
    :cond_12
    const/4 v4, 0x3

    move/from16 v0, v52

    if-ne v0, v4, :cond_35

    :try_start_b
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget v4, v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mCacheType:I

    if-eqz v4, :cond_13

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->iCurrentMultiSocketNum:I
    invoke-static {}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1000()I

    move-result v4

    const/4 v5, 0x4

    if-ge v4, v5, :cond_35

    .line 1171
    :cond_13
    invoke-static {}, Lcom/android/okhttp/internal/http/SBServiceAPI;->getSpeedRatio()[J

    move-result-object v80

    .line 1172
    .local v80, "smartBondingData":[J
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesRemaining:J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$700(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v4

    const-wide/16 v8, 0x2

    div-long v78, v4, v8

    .line 1173
    .local v78, "sizeForExtremeCase":J
    if-eqz v80, :cond_14

    const/4 v4, 0x0

    aget-wide v4, v80, v4

    const-wide/16 v8, -0x1

    cmp-long v4, v4, v8

    if-eqz v4, :cond_14

    .line 1174
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->TESTSPEEDRATIO:I
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1100(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v4

    if-lez v4, :cond_17

    .line 1175
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->TESTSPEEDRATIO:I
    invoke-static {v5}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1100(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v5

    int-to-double v8, v5

    const-wide/high16 v12, 0x4059000000000000L    # 100.0

    div-double/2addr v8, v12

    # setter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mSpeedRatio:D
    invoke-static {v4, v8, v9}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1202(Lcom/android/okhttp/internal/http/MultiSocketInputStream;D)D

    .line 1176
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainSocketInterfaceID:I
    invoke-static {v5}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1300(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v5

    int-to-long v8, v5

    aput-wide v8, v80, v4

    .line 1181
    :goto_9
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    const-wide v8, 0x3fb999999999999aL    # 0.1

    const-wide/high16 v12, 0x4024000000000000L    # 10.0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mSpeedRatio:D
    invoke-static {v5}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1200(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)D

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-static {v12, v13, v0, v1}, Ljava/lang/Math;->min(DD)D

    move-result-wide v12

    invoke-static {v8, v9, v12, v13}, Ljava/lang/Math;->max(DD)D

    move-result-wide v8

    # setter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mSpeedRatio:D
    invoke-static {v4, v8, v9}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1202(Lcom/android/okhttp/internal/http/MultiSocketInputStream;D)D

    .line 1182
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-object v4, v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mRAdownloadedFile:Ljava/io/RandomAccessFile;

    if-nez v4, :cond_19

    .line 1183
    const/4 v4, 0x0

    aget-wide v4, v80, v4

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainSocketInterfaceID:I
    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1300(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v7

    int-to-long v8, v7

    cmp-long v4, v4, v8

    if-nez v4, :cond_18

    .line 1184
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesRemaining:J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$700(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v4

    long-to-double v4, v4

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mSpeedRatio:D
    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1200(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)D

    move-result-wide v8

    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    add-double/2addr v8, v12

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mSpeedRatio:D
    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1200(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)D

    move-result-wide v12

    sget-wide v16, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->B_Ratio_BUF_LTE:D

    div-double v12, v12, v16

    sub-double/2addr v8, v12

    div-double/2addr v4, v8

    double-to-long v0, v4

    move-wide/from16 v78, v0

    .line 1197
    :cond_14
    :goto_a
    const-wide/32 v4, 0x200000

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesRemaining:J
    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$700(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v8

    const-wide/32 v12, 0x200000

    sub-long/2addr v8, v12

    move-wide/from16 v0, v78

    invoke-static {v8, v9, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v8

    invoke-static {v4, v5, v8, v9}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v78

    .line 1198
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_15

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "before check exetThread, ratio: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mSpeedRatio:D
    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1200(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)D

    move-result-wide v8

    invoke-virtual {v5, v8, v9}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ", taillen: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v78

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ", maxTailLen: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v60

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ", total: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->totalLengthToBeRead:J
    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$300(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v8

    invoke-virtual {v5, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ", oriOffset: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mOffset:J
    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v8

    invoke-virtual {v5, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ", byteRemain: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesRemaining:J
    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$700(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v8

    invoke-virtual {v5, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ", B_Ratio_BUF_LTE: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-wide v8, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->B_Ratio_BUF_LTE:D

    invoke-virtual {v5, v8, v9}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ", mRAdownloadedFile = "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-object v7, v7, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mRAdownloadedFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1202
    :cond_15
    const-wide/32 v4, 0x200000

    cmp-long v4, v78, v4

    if-gez v4, :cond_1b

    .line 1203
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_16

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "tail length is too small, exit watchdog "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v78

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1204
    :cond_16
    monitor-exit v6

    goto/16 :goto_4

    .line 1179
    :cond_17
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    const/4 v5, 0x1

    aget-wide v8, v80, v5

    long-to-double v8, v8

    const-wide/high16 v12, 0x4059000000000000L    # 100.0

    div-double/2addr v8, v12

    # setter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mSpeedRatio:D
    invoke-static {v4, v8, v9}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1202(Lcom/android/okhttp/internal/http/MultiSocketInputStream;D)D

    goto/16 :goto_9

    .line 1186
    :cond_18
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesRemaining:J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$700(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v4

    long-to-double v4, v4

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mSpeedRatio:D
    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1200(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)D

    move-result-wide v8

    mul-double/2addr v4, v8

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mSpeedRatio:D
    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1200(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)D

    move-result-wide v8

    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    add-double/2addr v8, v12

    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    sget-wide v16, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->B_Ratio_BUF_LTE:D

    div-double v12, v12, v16

    sub-double/2addr v8, v12

    div-double/2addr v4, v8

    double-to-long v0, v4

    move-wide/from16 v78, v0

    goto/16 :goto_a

    .line 1190
    :cond_19
    const/4 v4, 0x0

    aget-wide v4, v80, v4

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainSocketInterfaceID:I
    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1300(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v7

    int-to-long v8, v7

    cmp-long v4, v4, v8

    if-nez v4, :cond_1a

    .line 1191
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesRemaining:J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$700(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v4

    long-to-double v4, v4

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mSpeedRatio:D
    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1200(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)D

    move-result-wide v8

    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    add-double/2addr v8, v12

    div-double/2addr v4, v8

    double-to-long v0, v4

    move-wide/from16 v78, v0

    goto/16 :goto_a

    .line 1193
    :cond_1a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesRemaining:J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$700(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v4

    long-to-double v4, v4

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mSpeedRatio:D
    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1200(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)D

    move-result-wide v8

    mul-double/2addr v4, v8

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mSpeedRatio:D
    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1200(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)D

    move-result-wide v8

    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    add-double/2addr v8, v12

    div-double/2addr v4, v8

    double-to-long v0, v4

    move-wide/from16 v78, v0

    goto/16 :goto_a

    .line 1206
    :cond_1b
    cmp-long v4, v78, v60

    if-gez v4, :cond_1e

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget v4, v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mCacheType:I

    if-nez v4, :cond_1e

    .line 1209
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-wide/from16 v0, v60

    move-wide/from16 v2, v78

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v8

    # setter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesForMultiSocket:J
    invoke-static {v4, v8, v9}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1502(Lcom/android/okhttp/internal/http/MultiSocketInputStream;J)J

    .line 1212
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mOffset:J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->totalLengthToBeRead:J
    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$300(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v8

    add-long/2addr v4, v8

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesForMultiSocket:J
    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1500(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v8

    sub-long v14, v4, v8

    .line 1214
    .local v14, "start":J
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mOffset:J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->totalLengthToBeRead:J
    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$300(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v8

    add-long/2addr v4, v8

    const-wide/16 v8, 0x1

    sub-long v23, v4, v8

    .line 1216
    .local v23, "end":J
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_1c

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "start exetThread, ratio: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mSpeedRatio:D
    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1200(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)D

    move-result-wide v8

    invoke-virtual {v5, v8, v9}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ", taillen: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesForMultiSocket:J
    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1500(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v8

    invoke-virtual {v5, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ", "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "-"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v23

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ", total: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->totalLengthToBeRead:J
    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$300(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v8

    invoke-virtual {v5, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ", oriOffset: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mOffset:J
    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v8

    invoke-virtual {v5, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1219
    :cond_1c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainSocketInterfaceID:I
    invoke-static {v5}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1300(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v5

    # invokes: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->getOtherSocketID(I)I
    invoke-static {v4, v5}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1600(Lcom/android/okhttp/internal/http/MultiSocketInputStream;I)I

    move-result v20

    .line 1220
    .local v20, "otherSocketId":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    new-instance v12, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    const/16 v16, 0x0

    sub-long v8, v23, v14

    const-wide/16 v18, 0x1

    add-long v8, v8, v18

    long-to-int v5, v8

    int-to-long v0, v5

    move-wide/from16 v17, v0

    new-instance v19, Ljava/util/LinkedList;

    invoke-direct/range {v19 .. v19}, Ljava/util/LinkedList;-><init>()V

    const/16 v21, 0x1

    invoke-direct/range {v12 .. v21}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;-><init>(Lcom/android/okhttp/internal/http/MultiSocketInputStream;JIJLjava/util/LinkedList;II)V

    # setter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mExtDBuf:Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;
    invoke-static {v4, v12}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1702(Lcom/android/okhttp/internal/http/MultiSocketInputStream;Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;)Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

    .line 1221
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->inBuffer:Ljava/util/HashMap;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Ljava/util/HashMap;

    move-result-object v4

    new-instance v5, Ljava/lang/Integer;

    const/4 v7, 0x0

    invoke-direct {v5, v7}, Ljava/lang/Integer;-><init>(I)V

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mExtDBuf:Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;
    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1700(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

    move-result-object v7

    invoke-virtual {v4, v5, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1222
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    const/4 v5, 0x1

    new-array v5, v5, [B

    # setter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static {v4, v5}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1902(Lcom/android/okhttp/internal/http/MultiSocketInputStream;[B)[B

    .line 1223
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B

    move-result-object v4

    const/4 v5, 0x0

    const/4 v7, 0x1

    aput-byte v7, v4, v5

    .line 1224
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    new-instance v18, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mExtDBuf:Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;
    invoke-static {v5}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1700(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

    move-result-object v25

    move-wide/from16 v21, v14

    invoke-direct/range {v18 .. v25}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;-><init>(Lcom/android/okhttp/internal/http/MultiSocketInputStream;IJJLcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;)V

    move-object/from16 v0, v18

    # setter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mExtThread:Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;
    invoke-static {v4, v0}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2002(Lcom/android/okhttp/internal/http/MultiSocketInputStream;Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;)Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;

    .line 1225
    new-instance v81, Ljava/lang/Thread;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mExtThread:Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ":ExtremeCondition_"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    # operator++ for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->threadID:I
    invoke-static {}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2108()I

    move-result v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v81

    invoke-direct {v0, v4, v5}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 1227
    .local v81, "t":Ljava/lang/Thread;
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_1d

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "resource check: finish reading startup data "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mOriOffset:J
    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v8

    invoke-virtual {v5, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "-"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mOriOffset:J
    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v8

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->totalLengthToBeRead:J
    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$300(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v12

    add-long/2addr v8, v12

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesRemaining:J
    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$700(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v12

    sub-long/2addr v8, v12

    const-wide/16 v12, 0x1

    sub-long/2addr v8, v12

    invoke-virtual {v5, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " with length "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->totalLengthToBeRead:J
    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$300(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v8

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesRemaining:J
    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$700(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v12

    sub-long/2addr v8, v12

    invoke-virtual {v5, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ", now ExtremeThread created: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " Main socket read "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mOriOffset:J
    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v8

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->totalLengthToBeRead:J
    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$300(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v12

    add-long/2addr v8, v12

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesRemaining:J
    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$700(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v12

    sub-long/2addr v8, v12

    invoke-virtual {v5, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "-"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-wide/16 v8, 0x1

    sub-long v8, v14, v8

    invoke-virtual {v5, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ", ExtremeThread read from "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "-"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v23

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1231
    :cond_1d
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    const/4 v5, 0x1

    # setter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bExtremThreadStarted:Z
    invoke-static {v4, v5}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2202(Lcom/android/okhttp/internal/http/MultiSocketInputStream;Z)Z

    .line 1232
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    const/4 v5, 0x1

    # setter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bExtremThreadExisted:Z
    invoke-static {v4, v5}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2302(Lcom/android/okhttp/internal/http/MultiSocketInputStream;Z)Z

    .line 1233
    invoke-virtual/range {v81 .. v81}, Ljava/lang/Thread;->start()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    .line 1235
    :try_start_c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->connection:Lcom/android/okhttp/Connection;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/Connection;

    move-result-object v4

    const/16 v5, 0x1388

    invoke-virtual {v4, v5}, Lcom/android/okhttp/Connection;->setSoTimeout(I)V
    :try_end_c
    .catch Ljava/lang/Throwable; {:try_start_c .. :try_end_c} :catch_9
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    .line 1238
    :goto_b
    :try_start_d
    monitor-exit v6

    goto/16 :goto_4

    .line 1242
    .end local v14    # "start":J
    .end local v20    # "otherSocketId":I
    .end local v23    # "end":J
    .end local v81    # "t":Ljava/lang/Thread;
    :cond_1e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesRemaining:J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$700(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v36

    .line 1243
    .local v36, "byteBeforeSample":J
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainSocketInterfaceID:I
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1300(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    rem-int/lit8 v26, v4, 0x2

    .line 1244
    .local v26, "otherInterface":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bRangeRequestSuccess:I
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_2

    move-result v4

    const/4 v5, 0x1

    if-eq v4, v5, :cond_1f

    .line 1245
    const/16 v73, 0x0

    .line 1247
    .local v73, "rsp":Lcom/android/okhttp/Response;
    :try_start_e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->connection:Lcom/android/okhttp/Connection;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/Connection;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-object v0, v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mOriginalRequestHeader:Lcom/android/okhttp/Request;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mOriOffset:J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v28

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mOriOffset:J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v4

    const-wide/16 v8, 0x1

    add-long v30, v4, v8

    invoke-virtual/range {v25 .. v31}, Lcom/android/okhttp/Connection;->sampleRequestConnect(ILcom/android/okhttp/Request;JJ)Lcom/android/okhttp/Response;

    move-result-object v73

    .line 1248
    if-nez v73, :cond_22

    .line 1249
    new-instance v4, Ljava/io/IOException;

    const-string v5, "Cannot Connect to Server"

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_2
    .catch Ljava/net/URISyntaxException; {:try_start_e .. :try_end_e} :catch_3
    .catchall {:try_start_e .. :try_end_e} :catchall_2

    .line 1265
    :catch_2
    move-exception v47

    .line 1266
    .local v47, "e":Ljava/io/IOException;
    :try_start_f
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    const/4 v5, 0x1

    # setter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bRangeRequestSuccess:I
    invoke-static {v4, v5}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2402(Lcom/android/okhttp/internal/http/MultiSocketInputStream;I)I

    .line 1267
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_1f

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    move-object/from16 v0, v47

    invoke-virtual {v4, v0}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V

    .line 1272
    .end local v47    # "e":Ljava/io/IOException;
    .end local v73    # "rsp":Lcom/android/okhttp/Response;
    :cond_1f
    :goto_c
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_20

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "mOtherSocketConnectTime:"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mOtherSocketConnectTime:J
    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2600(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v8

    invoke-virtual {v5, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " mMainSocketConnectTime:"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainSocketConnectTime:J
    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2700(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v8

    invoke-virtual {v5, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1274
    :cond_20
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mOtherSocketConnectTime:J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2600(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainSocketConnectTime:J
    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2700(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v8

    sget v7, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->HANDOVER_TIME_RATIO:I

    int-to-long v12, v7

    mul-long/2addr v8, v12

    cmp-long v4, v4, v8

    if-lez v4, :cond_27

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mOtherSocketConnectTime:J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2600(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainSocketConnectTime:J
    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2700(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v8

    sub-long/2addr v4, v8

    sget v7, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->MIN_DIFF_TIME_FOR_HANDOVER:I

    int-to-long v8, v7

    cmp-long v4, v4, v8

    if-lez v4, :cond_27

    .line 1275
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_21

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Exiting the Other interface ID: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v26

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " because interface connect time is slow. mMainSocketConnectTime="

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainSocketConnectTime:J
    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2700(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v8

    invoke-virtual {v5, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " currentConnectTime="

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mOtherSocketConnectTime:J
    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2600(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v8

    invoke-virtual {v5, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1281
    :cond_21
    monitor-exit v6
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_2

    goto/16 :goto_4

    .line 1250
    .restart local v73    # "rsp":Lcom/android/okhttp/Response;
    :cond_22
    :try_start_10
    invoke-virtual/range {v73 .. v73}, Lcom/android/okhttp/Response;->code()I

    move-result v74

    .line 1251
    .local v74, "rspCode":I
    invoke-static/range {v73 .. v73}, Lcom/android/okhttp/internal/http/OkHeaders;->contentLength(Lcom/android/okhttp/Response;)J

    move-result-wide v76

    .line 1252
    .local v76, "sampleRequestSize":J
    invoke-virtual/range {v73 .. v73}, Lcom/android/okhttp/Response;->headers()Lcom/android/okhttp/Headers;

    move-result-object v4

    const-wide/16 v8, -0x1

    invoke-static {v4, v8, v9}, Lcom/android/okhttp/internal/http/MultiratUtil;->getFullContentLength(Lcom/android/okhttp/Headers;J)J

    move-result-wide v50

    .line 1253
    .local v50, "fullSize":J
    const/16 v4, 0xc8

    move/from16 v0, v74

    if-eq v0, v4, :cond_23

    const/16 v4, 0xce

    move/from16 v0, v74

    if-ne v0, v4, :cond_24

    :cond_23
    const-wide/16 v4, 0x2

    cmp-long v4, v76, v4

    if-nez v4, :cond_24

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mFullConSize:J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2500(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v4

    cmp-long v4, v50, v4

    if-eqz v4, :cond_25

    .line 1254
    :cond_24
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    const/4 v5, 0x1

    # setter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bRangeRequestSuccess:I
    invoke-static {v4, v5}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2402(Lcom/android/okhttp/internal/http/MultiSocketInputStream;I)I

    .line 1255
    new-instance v4, Ljava/io/IOException;

    const-string v5, "Server Response not proper"

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 1269
    .end local v50    # "fullSize":J
    .end local v74    # "rspCode":I
    .end local v76    # "sampleRequestSize":J
    :catch_3
    move-exception v4

    goto/16 :goto_c

    .line 1257
    .restart local v50    # "fullSize":J
    .restart local v74    # "rspCode":I
    .restart local v76    # "sampleRequestSize":J
    :cond_25
    invoke-virtual/range {v73 .. v73}, Lcom/android/okhttp/Response;->headers()Lcom/android/okhttp/Headers;

    move-result-object v4

    const-string v5, "Connection"

    invoke-virtual {v4, v5}, Lcom/android/okhttp/Headers;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v75

    .line 1258
    .local v75, "sCon":Ljava/lang/String;
    if-eqz v75, :cond_26

    invoke-virtual/range {v75 .. v75}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    const-string v5, "close"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_26

    .line 1259
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    const/4 v5, 0x1

    iput-boolean v5, v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bConClose:Z

    .line 1262
    :cond_26
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->connection:Lcom/android/okhttp/Connection;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/Connection;

    move-result-object v4

    iget-wide v0, v4, Lcom/android/okhttp/Connection;->startSampleSocketCreation:J

    move-wide/from16 v90, v0

    .line 1263
    .local v90, "startTime":J
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sub-long v8, v8, v90

    # setter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mOtherSocketConnectTime:J
    invoke-static {v4, v8, v9}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2602(Lcom/android/okhttp/internal/http/MultiSocketInputStream;J)J
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_2
    .catch Ljava/net/URISyntaxException; {:try_start_10 .. :try_end_10} :catch_3
    .catchall {:try_start_10 .. :try_end_10} :catchall_2

    goto/16 :goto_c

    .line 1282
    .end local v50    # "fullSize":J
    .end local v73    # "rsp":Lcom/android/okhttp/Response;
    .end local v74    # "rspCode":I
    .end local v75    # "sCon":Ljava/lang/String;
    .end local v76    # "sampleRequestSize":J
    .end local v90    # "startTime":J
    :cond_27
    :try_start_11
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainSocketConnectTime:J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2700(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mOtherSocketConnectTime:J
    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2600(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v8

    sget v7, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->HANDOVER_TIME_RATIO:I

    int-to-long v12, v7

    mul-long/2addr v8, v12

    cmp-long v4, v4, v8

    if-lez v4, :cond_29

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainSocketConnectTime:J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2700(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mOtherSocketConnectTime:J
    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2600(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v8

    sub-long/2addr v4, v8

    sget v7, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->MIN_DIFF_TIME_FOR_HANDOVER:I

    int-to-long v8, v7

    cmp-long v4, v4, v8

    if-lez v4, :cond_29

    .line 1283
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_28

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Exiting the main interface : "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainSocketInterfaceID:I
    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1300(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " because interface connect time is slow. mMainSocketConnectTime="

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainSocketConnectTime:J
    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2700(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v8

    invoke-virtual {v5, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " currentConnectTime="

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mOtherSocketConnectTime:J
    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2600(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v8

    invoke-virtual {v5, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_2

    .line 1290
    :cond_28
    :try_start_12
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->connection:Lcom/android/okhttp/Connection;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/Connection;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/okhttp/Connection;->close()V
    :try_end_12
    .catch Ljava/lang/Throwable; {:try_start_12 .. :try_end_12} :catch_8
    .catchall {:try_start_12 .. :try_end_12} :catchall_2

    .line 1295
    :goto_d
    :try_start_13
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move/from16 v0, v26

    # setter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainSocketAfterHandover:I
    invoke-static {v4, v0}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2802(Lcom/android/okhttp/internal/http/MultiSocketInputStream;I)I

    .line 1296
    monitor-exit v6

    goto/16 :goto_4

    .line 1299
    :cond_29
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_2a

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "both interface connected while bytesRemaining="

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesRemaining:J
    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$700(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v8

    invoke-virtual {v5, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1301
    :cond_2a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesRemaining:J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$700(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v34

    .line 1302
    .local v34, "byteAfterSample":J
    sub-long v40, v36, v34

    .line 1303
    .local v40, "bytesExpected":J
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesRemaining:J
    invoke-static {v5}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$700(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v8

    sub-long v8, v8, v40

    # setter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesForMultiSocket:J
    invoke-static {v4, v8, v9}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1502(Lcom/android/okhttp/internal/http/MultiSocketInputStream;J)J

    .line 1304
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesForMultiSocket:J
    invoke-static {v5}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1500(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v8

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-boolean v5, v5, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bConClose:Z

    invoke-static {v8, v9, v5}, Lcom/android/okhttp/internal/http/MultiratUtil;->getBlockSize(JZ)J

    move-result-wide v8

    # setter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->BlockSize:J
    invoke-static {v4, v8, v9}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2902(Lcom/android/okhttp/internal/http/MultiSocketInputStream;J)J

    .line 1305
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_2b

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "chunk base download, bytesForMultiSocket="

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesForMultiSocket:J
    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1500(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v8

    invoke-virtual {v5, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ", bytesExpected="

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v40

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ", BlockSize="

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->BlockSize:J
    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v8

    invoke-virtual {v5, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1306
    :cond_2b
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesForMultiSocket:J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1500(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->BlockSize:J
    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v8

    const-wide/16 v12, 0x3

    mul-long/2addr v8, v12

    const-wide/16 v12, 0x2

    div-long/2addr v8, v12

    cmp-long v4, v4, v8

    if-lez v4, :cond_34

    .line 1307
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mOffset:J
    invoke-static {v5}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v8

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->totalLengthToBeRead:J
    invoke-static {v5}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$300(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v12

    add-long/2addr v8, v12

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesForMultiSocket:J
    invoke-static {v5}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1500(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v12

    sub-long/2addr v8, v12

    # setter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mOffset:J
    invoke-static {v4, v8, v9}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1402(Lcom/android/okhttp/internal/http/MultiSocketInputStream;J)J

    .line 1308
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesForMultiSocket:J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1500(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->BlockSize:J
    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v8

    rem-long/2addr v4, v8

    const-wide/16 v8, 0x0

    cmp-long v4, v4, v8

    if-nez v4, :cond_2d

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesForMultiSocket:J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1500(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->BlockSize:J
    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v8

    div-long/2addr v4, v8

    :goto_e
    long-to-int v0, v4

    move/from16 v33, v0

    .line 1309
    .local v33, "blockNum":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesForMultiSocket:J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1500(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->BlockSize:J
    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v8

    rem-long/2addr v4, v8

    const-wide/16 v8, 0x0

    cmp-long v4, v4, v8

    if-lez v4, :cond_2c

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesForMultiSocket:J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1500(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->BlockSize:J
    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v8

    rem-long/2addr v4, v8

    const-wide/32 v8, 0x500000

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->BlockSize:J
    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v12

    const-wide/16 v16, 0x2

    div-long v12, v12, v16

    invoke-static {v8, v9, v12, v13}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v8

    cmp-long v4, v4, v8

    if-gtz v4, :cond_2c

    .line 1310
    add-int/lit8 v33, v33, -0x1

    .line 1312
    :cond_2c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move/from16 v0, v33

    new-array v5, v0, [B

    # setter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static {v4, v5}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1902(Lcom/android/okhttp/internal/http/MultiSocketInputStream;[B)[B

    .line 1313
    const/16 v49, 0x0

    .restart local v49    # "i":I
    :goto_f
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B

    move-result-object v4

    array-length v4, v4

    move/from16 v0, v49

    if-ge v0, v4, :cond_2e

    .line 1314
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B

    move-result-object v4

    const/4 v5, 0x0

    aput-byte v5, v4, v49

    .line 1313
    add-int/lit8 v49, v49, 0x1

    goto :goto_f

    .line 1308
    .end local v33    # "blockNum":I
    .end local v49    # "i":I
    :cond_2d
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesForMultiSocket:J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1500(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->BlockSize:J
    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v8

    div-long/2addr v4, v8

    const-wide/16 v8, 0x1

    add-long/2addr v4, v8

    goto :goto_e

    .line 1316
    .restart local v33    # "blockNum":I
    .restart local v49    # "i":I
    :cond_2e
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_2f

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "block status is initialized with size: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B

    move-result-object v7

    array-length v7, v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1317
    :cond_2f
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    invoke-static {}, Lcom/android/okhttp/internal/http/SBServiceAPI;->getFasterInterface()I

    move-result v5

    # setter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mFasterInterface:I
    invoke-static {v4, v5}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3002(Lcom/android/okhttp/internal/http/MultiSocketInputStream;I)I

    .line 1318
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    const/4 v5, 0x1

    # setter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bMultiSocketStarted:Z
    invoke-static {v4, v5}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3102(Lcom/android/okhttp/internal/http/MultiSocketInputStream;Z)Z

    .line 1319
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    const/4 v5, 0x0

    # setter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->finishedThreadNum:I
    invoke-static {v4, v5}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3202(Lcom/android/okhttp/internal/http/MultiSocketInputStream;I)I

    .line 1320
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_30

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "start socket thread for interfaces "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesRemaining:J
    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$700(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v8

    invoke-virtual {v5, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ":"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesForMultiSocket:J
    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1500(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v8

    invoke-virtual {v5, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ":"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mOffset:J
    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v8

    invoke-virtual {v5, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ", faster sock is "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mFasterInterface:I
    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1322
    :cond_30
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->socketNumber:I
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3300(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v4

    new-array v0, v4, [Ljava/lang/Thread;

    move-object/from16 v81, v0

    .line 1323
    .local v81, "t":[Ljava/lang/Thread;
    const/16 v49, 0x0

    :goto_10
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->socketNumber:I
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3300(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v4

    move/from16 v0, v49

    if-ge v0, v4, :cond_31

    .line 1324
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->requestHandlers:[Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;

    move-result-object v4

    new-instance v5, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->header:Lcom/android/okhttp/Headers;

    move/from16 v0, v49

    invoke-direct {v5, v7, v8, v0}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;-><init>(Lcom/android/okhttp/internal/http/MultiSocketInputStream;Lcom/android/okhttp/Headers;I)V

    aput-object v5, v4, v49

    .line 1325
    new-instance v4, Ljava/lang/Thread;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->requestHandlers:[Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;
    invoke-static {v5}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;

    move-result-object v5

    aget-object v5, v5, v49

    invoke-direct {v4, v5}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    aput-object v4, v81, v49

    .line 1323
    add-int/lit8 v49, v49, 0x1

    goto :goto_10

    .line 1327
    :cond_31
    const/16 v49, 0x0

    :goto_11
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->socketNumber:I
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3300(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v4

    move/from16 v0, v49

    if-ge v0, v4, :cond_32

    .line 1328
    aget-object v4, v81, v49

    invoke-virtual {v4}, Ljava/lang/Thread;->start()V
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_2

    .line 1327
    add-int/lit8 v49, v49, 0x1

    goto :goto_11

    .line 1331
    :cond_32
    :try_start_14
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->connection:Lcom/android/okhttp/Connection;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/Connection;

    move-result-object v4

    const/16 v5, 0x1388

    invoke-virtual {v4, v5}, Lcom/android/okhttp/Connection;->setSoTimeout(I)V
    :try_end_14
    .catch Ljava/lang/Throwable; {:try_start_14 .. :try_end_14} :catch_7
    .catchall {:try_start_14 .. :try_end_14} :catchall_2

    .line 1338
    .end local v33    # "blockNum":I
    .end local v49    # "i":I
    .end local v81    # "t":[Ljava/lang/Thread;
    :cond_33
    :goto_12
    :try_start_15
    monitor-exit v6

    goto/16 :goto_4

    .line 1336
    :cond_34
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_33

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "rest size too small to start Multi socket: "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesForMultiSocket:J
    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1500(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v8

    invoke-virtual {v5, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_2

    goto :goto_12

    .line 1342
    .end local v26    # "otherInterface":I
    .end local v34    # "byteAfterSample":J
    .end local v36    # "byteBeforeSample":J
    .end local v40    # "bytesExpected":J
    .end local v78    # "sizeForExtremeCase":J
    .end local v80    # "smartBondingData":[J
    :cond_35
    :try_start_16
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mBlockManager:Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;

    move-result-object v4

    const-wide/16 v8, 0x14

    invoke-virtual {v4, v8, v9}, Ljava/lang/Object;->wait(J)V
    :try_end_16
    .catch Ljava/lang/Throwable; {:try_start_16 .. :try_end_16} :catch_4
    .catchall {:try_start_16 .. :try_end_16} :catchall_2

    .line 1347
    :cond_36
    :goto_13
    :try_start_17
    monitor-exit v6

    goto/16 :goto_3

    .line 1344
    :catch_4
    move-exception v47

    .line 1345
    .local v47, "e":Ljava/lang/Throwable;
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_36

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    move-object/from16 v0, v47

    invoke-virtual {v4, v0}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_2

    goto :goto_13

    .line 1371
    .end local v47    # "e":Ljava/lang/Throwable;
    .end local v52    # "iInf":I
    .end local v60    # "maxBufDataSize":J
    .restart local v38    # "byteThresholdAs0":J
    .restart local v62    # "maxCountToSend":I
    .restart local v63    # "noDataCount":[I
    .restart local v64    # "minTimeToCheckStopSlow0":J
    .restart local v66    # "minTimeToCheckStopSlow1":J
    .restart local v70    # "prevBytes":[J
    .restart local v71    # "prevSp":[J
    .restart local v72    # "prevTime":[J
    :catchall_3
    move-exception v4

    :try_start_18
    monitor-exit v5
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_3

    throw v4

    .line 1375
    .restart local v49    # "i":I
    .restart local v55    # "lens":[J
    .restart local v96    # "times":[J
    :cond_37
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    move-result-object v4

    move/from16 v0, v49

    invoke-virtual {v4, v0}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->getDownloadLen(I)J

    move-result-wide v4

    goto/16 :goto_7

    .line 1376
    :cond_38
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    move-result-object v4

    move/from16 v0, v49

    invoke-virtual {v4, v0}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->getDownloadTime(I)J

    move-result-wide v4

    goto/16 :goto_8

    .line 1378
    :cond_39
    const/4 v4, 0x0

    aget-wide v4, v72, v4

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-nez v4, :cond_51

    .line 1379
    const/16 v49, 0x0

    :goto_14
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->socketNumber:I
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3300(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v4

    move/from16 v0, v49

    if-ge v0, v4, :cond_3a

    .line 1380
    aget-wide v4, v96, v49

    aput-wide v4, v72, v49

    .line 1381
    aget-wide v4, v55, v49

    aput-wide v4, v70, v49

    .line 1379
    add-int/lit8 v49, v49, 0x1

    goto :goto_14

    .line 1383
    :cond_3a
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_3b

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[speed calc detail]init speed data: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x0

    aget-wide v6, v70, v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x0

    aget-wide v6, v72, v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ":"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x1

    aget-wide v6, v70, v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x1

    aget-wide v6, v72, v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1437
    .end local v49    # "i":I
    .end local v55    # "lens":[J
    .end local v96    # "times":[J
    :cond_3b
    :goto_15
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bSBUsed:Z
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$600(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Z

    move-result v4

    if-eqz v4, :cond_47

    .line 1438
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->sbUsedLocker:Ljava/lang/Object;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$500(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Ljava/lang/Object;

    move-result-object v5

    monitor-enter v5

    .line 1439
    :try_start_19
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bRangeRequestSuccess:I
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v4

    const/4 v6, 0x1

    if-ne v4, v6, :cond_3d

    .line 1440
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bSBUsed:Z
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$600(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Z

    move-result v4

    if-eqz v4, :cond_3d

    .line 1441
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_3c

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Range Request not supported by server, current SBUsed is "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bSBUsed:Z
    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$600(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Z

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1442
    :cond_3c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainThreadID:J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$100(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v6

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->failReasonInExtreme:I
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3700(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v4

    invoke-static {v6, v7, v4}, Lcom/android/okhttp/internal/http/SBServiceAPI;->stopSBUsageWithReason(JI)I

    .line 1443
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    const/4 v6, 0x0

    # setter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bSBUsed:Z
    invoke-static {v4, v6}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$602(Lcom/android/okhttp/internal/http/MultiSocketInputStream;Z)Z

    .line 1447
    :cond_3d
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bStopSecDecisionMade:Z
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$4300(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Z

    move-result v4

    if-nez v4, :cond_46

    .line 1448
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bExtremThreadStarted:Z
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2200(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Z

    move-result v4

    if-eqz v4, :cond_46

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bExtremThreadExisted:Z
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2300(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Z

    move-result v4

    if-eqz v4, :cond_46

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mExtThread:Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;

    move-result-object v4

    if-eqz v4, :cond_46

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    move-result-object v4

    if-nez v4, :cond_46

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bReadFromBuffer:Z
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$4400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Z

    move-result v4

    if-nez v4, :cond_46

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bRangeRequestSuccess:I
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v4

    const/4 v6, 0x4

    if-ne v4, v6, :cond_46

    .line 1450
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_3e

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "extremThread running, main socket reading from socket, remain bytes = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesRemaining:J
    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$700(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1451
    :cond_3e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainSocketInterfaceID:I
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1300(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v53

    .line 1452
    .local v53, "id0":I
    add-int/lit8 v4, v53, 0x1

    rem-int/lit8 v54, v4, 0x2

    .line 1453
    .local v54, "id1":I
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v44

    .line 1454
    .local v44, "curTime":J
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mStartReadTime:[J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$4500(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[J

    move-result-object v4

    aget-wide v6, v4, v53

    sub-long v92, v44, v6

    .line 1455
    .local v92, "time0":J
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mStartReadTime:[J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$4500(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[J

    move-result-object v4

    aget-wide v6, v4, v54

    sub-long v94, v44, v6

    .line 1457
    .local v94, "time1":J
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mStartDataOffsetForSpeedCalc:[J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$4600(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[J

    move-result-object v4

    aget-wide v6, v4, v53

    const-wide/16 v8, 0x0

    cmp-long v4, v6, v8

    if-nez v4, :cond_3f

    const-wide/16 v6, 0xbb8

    cmp-long v4, v92, v6

    if-ltz v4, :cond_3f

    .line 1458
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mStartTimeOffsetForSpeedCalc:[J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$4700(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[J

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mTimeForDownload:[J
    invoke-static {v6}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$4000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[J

    move-result-object v6

    aget-wide v6, v6, v53

    aput-wide v6, v4, v53

    .line 1459
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mStartDataOffsetForSpeedCalc:[J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$4600(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[J

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mDataDownloaded:[J
    invoke-static {v6}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[J

    move-result-object v6

    aget-wide v6, v6, v53

    aput-wide v6, v4, v53

    .line 1460
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mStartTimeOffsetForSpeedCalc:[J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$4700(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[J

    move-result-object v4

    aget-wide v6, v4, v53

    const-wide/16 v8, 0x0

    cmp-long v4, v6, v8

    if-nez v4, :cond_5a

    const-wide/16 v56, 0x0

    .line 1461
    .local v56, "initSpeed0":J
    :goto_16
    const-wide/16 v6, 0x28f5

    cmp-long v4, v56, v6

    if-lez v4, :cond_5b

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->MIN_TIME_TO_CHECK_STOP_SLOW:I
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3600(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v4

    int-to-long v0, v4

    move-wide/from16 v64, v0

    .line 1463
    :goto_17
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_3f

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[speed calc]set speed calc offset for inf "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v53

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mStartTimeOffsetForSpeedCalc:[J
    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$4700(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[J

    move-result-object v7

    aget-wide v8, v7, v53

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ":"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mStartDataOffsetForSpeedCalc:[J
    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$4600(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[J

    move-result-object v7

    aget-wide v8, v7, v53

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", initSpeed="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, v56

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", minTimeToCheckStopSlow0="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, v64

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1467
    .end local v56    # "initSpeed0":J
    :cond_3f
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mStartDataOffsetForSpeedCalc:[J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$4600(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[J

    move-result-object v4

    aget-wide v6, v4, v54

    const-wide/16 v8, 0x0

    cmp-long v4, v6, v8

    if-nez v4, :cond_40

    const-wide/16 v6, 0xbb8

    cmp-long v4, v94, v6

    if-ltz v4, :cond_40

    .line 1468
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mStartTimeOffsetForSpeedCalc:[J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$4700(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[J

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mTimeForDownload:[J
    invoke-static {v6}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$4000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[J

    move-result-object v6

    aget-wide v6, v6, v54

    aput-wide v6, v4, v54

    .line 1469
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mStartDataOffsetForSpeedCalc:[J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$4600(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[J

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mDataDownloaded:[J
    invoke-static {v6}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[J

    move-result-object v6

    aget-wide v6, v6, v54

    aput-wide v6, v4, v54

    .line 1470
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mStartTimeOffsetForSpeedCalc:[J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$4700(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[J

    move-result-object v4

    aget-wide v6, v4, v54

    const-wide/16 v8, 0x0

    cmp-long v4, v6, v8

    if-nez v4, :cond_5c

    const-wide/16 v58, 0x0

    .line 1471
    .local v58, "initSpeed1":J
    :goto_18
    const-wide/16 v6, 0x28f5

    cmp-long v4, v58, v6

    if-lez v4, :cond_5d

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->MIN_TIME_TO_CHECK_STOP_SLOW:I
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3600(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v4

    int-to-long v0, v4

    move-wide/from16 v66, v0

    .line 1473
    :goto_19
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_40

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[speed calc]set speed calc offset for inf "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v54

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mStartTimeOffsetForSpeedCalc:[J
    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$4700(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[J

    move-result-object v7

    aget-wide v8, v7, v54

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ":"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mStartDataOffsetForSpeedCalc:[J
    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$4600(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[J

    move-result-object v7

    aget-wide v8, v7, v54

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", initSpeed="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, v58

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", minTimeToCheckStopSlow1="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, v66

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1479
    .end local v58    # "initSpeed1":J
    :cond_40
    cmp-long v4, v92, v64

    if-ltz v4, :cond_46

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mStartTimeOffsetForSpeedCalc:[J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$4700(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[J

    move-result-object v4

    aget-wide v6, v4, v53

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mTimeForDownload:[J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$4000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[J

    move-result-object v4

    aget-wide v8, v4, v53

    cmp-long v4, v6, v8

    if-eqz v4, :cond_46

    .line 1480
    const-wide/16 v82, 0x0

    .line 1481
    .local v82, "sp0":J
    const-wide/16 v84, 0x0

    .line 1482
    .local v84, "sp1":J
    const-wide/16 v86, 0x0

    .line 1483
    .local v86, "spNoOffset0":J
    const-wide/16 v88, 0x0

    .line 1485
    .local v88, "spNoOffset1":J
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move/from16 v0, v53

    invoke-virtual {v4, v0}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->getSpeedWithOffset(I)J

    move-result-wide v82

    .line 1486
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move/from16 v0, v54

    invoke-virtual {v4, v0}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->getSpeedWithOffset(I)J

    move-result-wide v84

    .line 1487
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move/from16 v0, v53

    invoke-virtual {v4, v0}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->getSpeed(I)J

    move-result-wide v86

    .line 1488
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move/from16 v0, v54

    invoke-virtual {v4, v0}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->getSpeed(I)J

    move-result-wide v88

    .line 1496
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_41

    .line 1497
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[speed calc]current speed is "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, v82

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ":"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, v84

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " threshold ratio is "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->MIN_RATIO_FOR_ONLY_ONE_INF_0V1:I
    invoke-static {}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$4900()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " and "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->MIN_RATIO_FOR_ONLY_ONE_INF_1V0:I
    invoke-static {}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$5000()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", elapsed time is "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, v92

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " and "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, v94

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1500
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[speed calc]speed without offset is "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, v86

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ":"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, v88

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1503
    :cond_41
    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->MIN_RATIO_FOR_ONLY_ONE_INF_0V1:I
    invoke-static {}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$4900()I

    move-result v4

    if-ltz v4, :cond_60

    cmp-long v4, v94, v66

    if-ltz v4, :cond_42

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mStartTimeOffsetForSpeedCalc:[J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$4700(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[J

    move-result-object v4

    aget-wide v6, v4, v54

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mTimeForDownload:[J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$4000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[J

    move-result-object v4

    aget-wide v8, v4, v54

    cmp-long v4, v6, v8

    if-eqz v4, :cond_42

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->MIN_RATIO_FOR_ONLY_ONE_INF_0V1:I
    invoke-static {}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$4900()I

    move-result v4

    int-to-long v6, v4

    mul-long v6, v6, v84

    cmp-long v4, v82, v6

    if-lez v4, :cond_42

    const-wide/16 v6, 0x0

    cmp-long v4, v82, v6

    if-ltz v4, :cond_42

    const-wide/16 v6, 0x0

    cmp-long v4, v84, v6

    if-gez v4, :cond_43

    :cond_42
    const-wide/16 v6, 0xa

    cmp-long v4, v88, v6

    if-gtz v4, :cond_60

    const-wide/16 v6, 0x2

    div-long v6, v66, v6

    cmp-long v4, v94, v6

    if-ltz v4, :cond_60

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mDataDownloaded:[J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[J

    move-result-object v4

    aget-wide v6, v4, v54

    const-wide/16 v8, 0x0

    cmp-long v4, v6, v8

    if-lez v4, :cond_60

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mStartTimeOffsetForSpeedCalc:[J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$4700(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[J

    move-result-object v4

    aget-wide v6, v4, v54

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mTimeForDownload:[J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$4000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[J

    move-result-object v4

    aget-wide v8, v4, v54

    cmp-long v4, v6, v8

    if-eqz v4, :cond_60

    .line 1509
    :cond_43
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_44

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "now stop the interface [1st>2nd] "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v54

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " and swtich sec interface to "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v53

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1510
    :cond_44
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    const/4 v6, 0x1

    # setter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bStopSecDecisionMade:Z
    invoke-static {v4, v6}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$4302(Lcom/android/okhttp/internal/http/MultiSocketInputStream;Z)Z

    .line 1511
    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->WIFI2SESSION:Z
    invoke-static {}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$5100()Z

    move-result v4

    if-eqz v4, :cond_5e

    .line 1512
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mExtThread:Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;

    move-result-object v4

    move/from16 v0, v53

    invoke-virtual {v4, v0}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->switchSocket(I)Z

    move-result v4

    if-eqz v4, :cond_45

    .line 1513
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mExtThread:Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->closeSocketAndStreams()V

    .line 1523
    :cond_45
    :goto_1a
    if-nez v53, :cond_5f

    .line 1524
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainThreadID:J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$100(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v6

    const/4 v4, -0x1

    invoke-static {v6, v7, v4}, Lcom/android/okhttp/internal/http/SBServiceAPI;->stopSBUsageWithReason(JI)I

    .line 1548
    .end local v44    # "curTime":J
    .end local v53    # "id0":I
    .end local v54    # "id1":I
    .end local v82    # "sp0":J
    .end local v84    # "sp1":J
    .end local v86    # "spNoOffset0":J
    .end local v88    # "spNoOffset1":J
    .end local v92    # "time0":J
    .end local v94    # "time1":J
    :cond_46
    :goto_1b
    monitor-exit v5
    :try_end_19
    .catchall {:try_start_19 .. :try_end_19} :catchall_4

    .line 1551
    :cond_47
    const-wide/16 v68, 0x0

    .line 1552
    .local v68, "nonReadTime":J
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mLastReadTime:J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$5500(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-lez v4, :cond_48

    .line 1553
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mLastReadTime:J
    invoke-static {v6}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$5500(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v6

    sub-long v68, v4, v6

    .line 1554
    :cond_48
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_49

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "nonReadTime till now = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v68

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " bAppIsReadingNow="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bAppIsReadingNow:Z
    invoke-static {v6}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$5600(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1555
    :cond_49
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-boolean v4, v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->closed:Z

    if-nez v4, :cond_4a

    sget v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->WAIT_FOR_INTERFACE_TIME_OUT:I

    int-to-long v4, v4

    cmp-long v4, v68, v4

    if-lez v4, :cond_64

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bAppIsReadingNow:Z
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$5600(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Z

    move-result v4

    if-nez v4, :cond_64

    .line 1556
    :cond_4a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    const/4 v5, 0x1

    iput-boolean v5, v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bFinished:Z

    .line 1557
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_4b

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "close input stream since app is not reading data for "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v68

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1572
    .end local v38    # "byteThresholdAs0":J
    .end local v62    # "maxCountToSend":I
    .end local v63    # "noDataCount":[I
    .end local v64    # "minTimeToCheckStopSlow0":J
    .end local v66    # "minTimeToCheckStopSlow1":J
    .end local v68    # "nonReadTime":J
    .end local v70    # "prevBytes":[J
    .end local v71    # "prevSp":[J
    .end local v72    # "prevTime":[J
    :cond_4b
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-boolean v4, v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bFinished:Z

    if-eqz v4, :cond_4d

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-boolean v4, v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->closed:Z

    if-nez v4, :cond_4d

    .line 1573
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_4c

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    const-string v5, "close input stream for time out"

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1575
    :cond_4c
    :try_start_1a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    invoke-virtual {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->close()V

    .line 1576
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # invokes: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->closeMainSocket()V
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$5400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)V
    :try_end_1a
    .catch Ljava/lang/Throwable; {:try_start_1a .. :try_end_1a} :catch_6

    .line 1580
    :cond_4d
    :goto_1c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->sbUsedLocker:Ljava/lang/Object;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$500(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Ljava/lang/Object;

    move-result-object v5

    monitor-enter v5

    .line 1581
    :try_start_1b
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bSBUsed:Z
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$600(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Z

    move-result v4

    if-eqz v4, :cond_4f

    .line 1582
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_4e

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    const-string v6, "try to stopSBusage finally"

    invoke-virtual {v4, v6}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1583
    :cond_4e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesRemaining:J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$700(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v4, v6, v8

    if-nez v4, :cond_67

    .line 1584
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainThreadID:J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$100(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v6

    const/4 v4, 0x0

    invoke-static {v6, v7, v4}, Lcom/android/okhttp/internal/http/SBServiceAPI;->stopSBUsageWithReason(JI)I

    .line 1593
    :goto_1d
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    const/4 v6, 0x0

    # setter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bSBUsed:Z
    invoke-static {v4, v6}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$602(Lcom/android/okhttp/internal/http/MultiSocketInputStream;Z)Z

    .line 1595
    :cond_4f
    monitor-exit v5
    :try_end_1b
    .catchall {:try_start_1b .. :try_end_1b} :catchall_6

    .line 1597
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_50

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "resource check: thread checking: stop interface watchdog "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Thread;->getId()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1598
    :cond_50
    return-void

    .line 1386
    .restart local v38    # "byteThresholdAs0":J
    .restart local v49    # "i":I
    .restart local v55    # "lens":[J
    .restart local v62    # "maxCountToSend":I
    .restart local v63    # "noDataCount":[I
    .restart local v64    # "minTimeToCheckStopSlow0":J
    .restart local v66    # "minTimeToCheckStopSlow1":J
    .restart local v70    # "prevBytes":[J
    .restart local v71    # "prevSp":[J
    .restart local v72    # "prevTime":[J
    .restart local v96    # "times":[J
    :cond_51
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->socketNumber:I
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3300(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v4

    new-array v0, v4, [J

    move-object/from16 v43, v0

    .line 1387
    .local v43, "diffLen":[J
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->socketNumber:I
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3300(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v4

    new-array v0, v4, [J

    move-object/from16 v46, v0

    .line 1388
    .local v46, "diffTime":[J
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->socketNumber:I
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3300(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v4

    new-array v0, v4, [J

    move-object/from16 v42, v0

    .line 1389
    .local v42, "curSp":[J
    const/16 v49, 0x0

    :goto_1e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->socketNumber:I
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3300(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v4

    move/from16 v0, v49

    if-ge v0, v4, :cond_53

    .line 1390
    aget-wide v4, v55, v49

    aget-wide v6, v70, v49

    sub-long/2addr v4, v6

    aput-wide v4, v43, v49

    .line 1391
    aget-wide v4, v96, v49

    aget-wide v6, v72, v49

    sub-long/2addr v4, v6

    aput-wide v4, v46, v49

    .line 1392
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    aget-wide v6, v43, v49

    aget-wide v8, v46, v49

    invoke-virtual {v4, v6, v7, v8, v9}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->getSpeed(JJ)J

    move-result-wide v4

    aput-wide v4, v42, v49

    .line 1393
    aget-wide v4, v96, v49

    aput-wide v4, v72, v49

    .line 1394
    aget-wide v4, v55, v49

    aput-wide v4, v70, v49

    .line 1395
    aget-wide v4, v43, v49

    const-wide/16 v6, 0x7d0

    cmp-long v4, v4, v6

    if-gtz v4, :cond_52

    .line 1396
    aget v4, v63, v49

    add-int/lit8 v4, v4, 0x1

    aput v4, v63, v49

    .line 1389
    :goto_1f
    add-int/lit8 v49, v49, 0x1

    goto :goto_1e

    .line 1398
    :cond_52
    const/4 v4, 0x0

    aput v4, v63, v49

    goto :goto_1f

    .line 1400
    :cond_53
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_54

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[speed calc detail] reportSBUsage current speed:\t"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x0

    aget-wide v6, v42, v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\t:\t"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x1

    aget-wide v6, v42, v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\t("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x0

    aget-wide v6, v43, v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x0

    aget-wide v6, v46, v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x1

    aget-wide v6, v43, v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x1

    aget-wide v6, v46, v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\t noDataCount:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x0

    aget v6, v63, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x1

    aget v6, v63, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1403
    :cond_54
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->SEND_NO_TRAFFIC_WARN:Z
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$4100(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Z

    move-result v4

    if-eqz v4, :cond_56

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bSBUsed:Z
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$600(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Z

    move-result v4

    if-eqz v4, :cond_56

    const/4 v4, 0x0

    aget v4, v63, v4

    const/4 v5, 0x5

    if-lt v4, v5, :cond_56

    .line 1404
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # invokes: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bTwoInfDownloading()Z
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$4200(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Z

    move-result v4

    if-eqz v4, :cond_58

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bStopSecDecisionMade:Z
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$4300(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Z

    move-result v4

    if-nez v4, :cond_58

    .line 1405
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainThreadID:J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$100(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v4

    const/4 v6, -0x3

    invoke-static {v4, v5, v6}, Lcom/android/okhttp/internal/http/SBServiceAPI;->stopSBUsageWithReason(JI)I

    .line 1410
    :cond_55
    :goto_20
    const/4 v4, 0x0

    const/4 v5, 0x0

    aput v5, v63, v4

    .line 1412
    :cond_56
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->SEND_NO_TRAFFIC_WARN:Z
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$4100(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Z

    move-result v4

    if-eqz v4, :cond_3b

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bSBUsed:Z
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$600(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Z

    move-result v4

    if-eqz v4, :cond_3b

    const/4 v4, 0x1

    aget v4, v63, v4

    const/4 v5, 0x5

    if-lt v4, v5, :cond_3b

    .line 1413
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # invokes: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bTwoInfDownloading()Z
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$4200(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Z

    move-result v4

    if-eqz v4, :cond_59

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bStopSecDecisionMade:Z
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$4300(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Z

    move-result v4

    if-nez v4, :cond_59

    .line 1414
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainThreadID:J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$100(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v4

    const/4 v6, -0x4

    invoke-static {v4, v5, v6}, Lcom/android/okhttp/internal/http/SBServiceAPI;->stopSBUsageWithReason(JI)I

    .line 1419
    :cond_57
    :goto_21
    const/4 v4, 0x1

    const/4 v5, 0x0

    aput v5, v63, v4

    goto/16 :goto_15

    .line 1408
    :cond_58
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_55

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "noDatacount[0] goes to "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x0

    aget v6, v63, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", but only 1 inf is using now"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    goto :goto_20

    .line 1417
    :cond_59
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_57

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "noDataCount[1] goes to "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x1

    aget v6, v63, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", but only 1 inf is using now"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    goto :goto_21

    .line 1460
    .end local v42    # "curSp":[J
    .end local v43    # "diffLen":[J
    .end local v46    # "diffTime":[J
    .end local v49    # "i":I
    .end local v55    # "lens":[J
    .end local v96    # "times":[J
    .restart local v44    # "curTime":J
    .restart local v53    # "id0":I
    .restart local v54    # "id1":I
    .restart local v92    # "time0":J
    .restart local v94    # "time1":J
    :cond_5a
    :try_start_1c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mStartDataOffsetForSpeedCalc:[J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$4600(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[J

    move-result-object v4

    aget-wide v6, v4, v53

    const-wide/16 v8, 0x8

    mul-long/2addr v6, v8

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mStartTimeOffsetForSpeedCalc:[J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$4700(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[J

    move-result-object v4

    aget-wide v8, v4, v53

    div-long v56, v6, v8

    goto/16 :goto_16

    .line 1462
    .restart local v56    # "initSpeed0":J
    :cond_5b
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->MIN_TIME_TO_CHECK_STOP_SLOW_FOR_LOW_TH:I
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$4800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v4

    int-to-long v0, v4

    move-wide/from16 v64, v0

    goto/16 :goto_17

    .line 1470
    .end local v56    # "initSpeed0":J
    :cond_5c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mStartDataOffsetForSpeedCalc:[J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$4600(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[J

    move-result-object v4

    aget-wide v6, v4, v54

    const-wide/16 v8, 0x8

    mul-long/2addr v6, v8

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mStartTimeOffsetForSpeedCalc:[J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$4700(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[J

    move-result-object v4

    aget-wide v8, v4, v54

    div-long v58, v6, v8

    goto/16 :goto_18

    .line 1472
    .restart local v58    # "initSpeed1":J
    :cond_5d
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->MIN_TIME_TO_CHECK_STOP_SLOW_FOR_LOW_TH:I
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$4800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v4

    int-to-long v0, v4

    move-wide/from16 v66, v0

    goto/16 :goto_19

    .line 1517
    .end local v58    # "initSpeed1":J
    .restart local v82    # "sp0":J
    .restart local v84    # "sp1":J
    .restart local v86    # "spNoOffset0":J
    .restart local v88    # "spNoOffset1":J
    :cond_5e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    const/4 v6, 0x0

    # setter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bReadFromBuffer:Z
    invoke-static {v4, v6}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$4402(Lcom/android/okhttp/internal/http/MultiSocketInputStream;Z)Z

    .line 1518
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    const/4 v6, 0x0

    # setter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bMultiSocketStarted:Z
    invoke-static {v4, v6}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3102(Lcom/android/okhttp/internal/http/MultiSocketInputStream;Z)Z

    .line 1519
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    const/4 v6, 0x0

    # setter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bExtremThreadStarted:Z
    invoke-static {v4, v6}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2202(Lcom/android/okhttp/internal/http/MultiSocketInputStream;Z)Z

    .line 1520
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mExtThread:Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;

    move-result-object v4

    const/4 v6, 0x1

    # setter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->bDeprecated:Z
    invoke-static {v4, v6}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->access$5202(Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;Z)Z

    .line 1521
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mExtThread:Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->closeSocketAndStreams()V

    goto/16 :goto_1a

    .line 1548
    .end local v44    # "curTime":J
    .end local v53    # "id0":I
    .end local v54    # "id1":I
    .end local v82    # "sp0":J
    .end local v84    # "sp1":J
    .end local v86    # "spNoOffset0":J
    .end local v88    # "spNoOffset1":J
    .end local v92    # "time0":J
    .end local v94    # "time1":J
    :catchall_4
    move-exception v4

    monitor-exit v5
    :try_end_1c
    .catchall {:try_start_1c .. :try_end_1c} :catchall_4

    throw v4

    .line 1526
    .restart local v44    # "curTime":J
    .restart local v53    # "id0":I
    .restart local v54    # "id1":I
    .restart local v82    # "sp0":J
    .restart local v84    # "sp1":J
    .restart local v86    # "spNoOffset0":J
    .restart local v88    # "spNoOffset1":J
    .restart local v92    # "time0":J
    .restart local v94    # "time1":J
    :cond_5f
    :try_start_1d
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainThreadID:J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$100(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v6

    const/4 v4, -0x2

    invoke-static {v6, v7, v4}, Lcom/android/okhttp/internal/http/SBServiceAPI;->stopSBUsageWithReason(JI)I

    goto/16 :goto_1b

    .line 1529
    :cond_60
    cmp-long v4, v94, v66

    if-ltz v4, :cond_46

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->MIN_RATIO_FOR_ONLY_ONE_INF_1V0:I
    invoke-static {}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$5000()I

    move-result v4

    if-ltz v4, :cond_46

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->MIN_RATIO_FOR_ONLY_ONE_INF_1V0:I
    invoke-static {}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$5000()I

    move-result v4

    int-to-long v6, v4

    mul-long v6, v6, v82

    cmp-long v4, v84, v6

    if-lez v4, :cond_46

    const-wide/16 v6, 0x0

    cmp-long v4, v82, v6

    if-lez v4, :cond_46

    const-wide/16 v6, 0x0

    cmp-long v4, v84, v6

    if-lez v4, :cond_46

    .line 1531
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mExtThread:Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;

    move-result-object v4

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->bIsSocketWorkingFine:Z
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->access$5300(Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;)Z

    move-result v4

    if-eqz v4, :cond_63

    .line 1532
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    const/4 v6, 0x1

    # setter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bStopSecDecisionMade:Z
    invoke-static {v4, v6}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$4302(Lcom/android/okhttp/internal/http/MultiSocketInputStream;Z)Z

    .line 1533
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move/from16 v0, v54

    # setter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainSocketAfterHandover:I
    invoke-static {v4, v0}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2802(Lcom/android/okhttp/internal/http/MultiSocketInputStream;I)I

    .line 1534
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # invokes: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->closeMainSocket()V
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$5400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)V

    .line 1535
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_61

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "now stop the interface [2nd>1st]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v53

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " and switch main interface to "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v54

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1536
    :cond_61
    if-nez v54, :cond_62

    .line 1537
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainThreadID:J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$100(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v6

    const/4 v4, -0x1

    invoke-static {v6, v7, v4}, Lcom/android/okhttp/internal/http/SBServiceAPI;->stopSBUsageWithReason(JI)I

    goto/16 :goto_1b

    .line 1539
    :cond_62
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainThreadID:J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$100(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v6

    const/4 v4, -0x2

    invoke-static {v6, v7, v4}, Lcom/android/okhttp/internal/http/SBServiceAPI;->stopSBUsageWithReason(JI)I

    goto/16 :goto_1b

    .line 1542
    :cond_63
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_46

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "do not stop the interface [2nd>1st] since "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v54

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " is not working"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V
    :try_end_1d
    .catchall {:try_start_1d .. :try_end_1d} :catchall_4

    goto/16 :goto_1b

    .line 1560
    .end local v44    # "curTime":J
    .end local v53    # "id0":I
    .end local v54    # "id1":I
    .end local v82    # "sp0":J
    .end local v84    # "sp1":J
    .end local v86    # "spNoOffset0":J
    .end local v88    # "spNoOffset1":J
    .end local v92    # "time0":J
    .end local v94    # "time1":J
    .restart local v68    # "nonReadTime":J
    :cond_64
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bAppIsReadingNow:Z
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$5600(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Z

    move-result v4

    if-nez v4, :cond_66

    const-wide/16 v4, 0x7d0

    cmp-long v4, v68, v4

    if-lez v4, :cond_66

    .line 1561
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_65

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "stop reading data for second chunks for APP nonReadTime is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v68

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1562
    :cond_65
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    const/4 v5, 0x0

    iput-boolean v5, v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bAppReading:Z

    .line 1564
    :cond_66
    monitor-enter p0

    .line 1566
    const-wide/16 v4, 0x3e8

    :try_start_1e
    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v5}, Ljava/lang/Object;->wait(J)V
    :try_end_1e
    .catch Ljava/lang/Throwable; {:try_start_1e .. :try_end_1e} :catch_5
    .catchall {:try_start_1e .. :try_end_1e} :catchall_5

    .line 1569
    :goto_22
    :try_start_1f
    monitor-exit p0

    goto/16 :goto_5

    :catchall_5
    move-exception v4

    monitor-exit p0
    :try_end_1f
    .catchall {:try_start_1f .. :try_end_1f} :catchall_5

    throw v4

    .line 1585
    .end local v38    # "byteThresholdAs0":J
    .end local v62    # "maxCountToSend":I
    .end local v63    # "noDataCount":[I
    .end local v64    # "minTimeToCheckStopSlow0":J
    .end local v66    # "minTimeToCheckStopSlow1":J
    .end local v68    # "nonReadTime":J
    .end local v70    # "prevBytes":[J
    .end local v71    # "prevSp":[J
    .end local v72    # "prevTime":[J
    :cond_67
    :try_start_20
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bHasIOException:Z
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$5700(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Z

    move-result v4

    if-nez v4, :cond_68

    .line 1586
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainThreadID:J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$100(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v6

    const/4 v4, 0x1

    invoke-static {v6, v7, v4}, Lcom/android/okhttp/internal/http/SBServiceAPI;->stopSBUsageWithReason(JI)I

    goto/16 :goto_1d

    .line 1595
    :catchall_6
    move-exception v4

    monitor-exit v5
    :try_end_20
    .catchall {:try_start_20 .. :try_end_20} :catchall_6

    throw v4

    .line 1588
    :cond_68
    :try_start_21
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    move-result-object v4

    if-eqz v4, :cond_69

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->isServerReject()Z

    move-result v4

    if-eqz v4, :cond_69

    .line 1589
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    const/4 v6, 0x2

    # setter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->failReasonInExtreme:I
    invoke-static {v4, v6}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3702(Lcom/android/okhttp/internal/http/MultiSocketInputStream;I)I

    .line 1591
    :cond_69
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainThreadID:J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$100(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v6

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$InfWatchdog;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->failReasonInExtreme:I
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3700(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v4

    invoke-static {v6, v7, v4}, Lcom/android/okhttp/internal/http/SBServiceAPI;->stopSBUsageWithReason(JI)I
    :try_end_21
    .catchall {:try_start_21 .. :try_end_21} :catchall_6

    goto/16 :goto_1d

    .line 1568
    .restart local v38    # "byteThresholdAs0":J
    .restart local v62    # "maxCountToSend":I
    .restart local v63    # "noDataCount":[I
    .restart local v64    # "minTimeToCheckStopSlow0":J
    .restart local v66    # "minTimeToCheckStopSlow1":J
    .restart local v68    # "nonReadTime":J
    .restart local v70    # "prevBytes":[J
    .restart local v71    # "prevSp":[J
    .restart local v72    # "prevTime":[J
    :catch_5
    move-exception v4

    goto :goto_22

    .line 1578
    .end local v38    # "byteThresholdAs0":J
    .end local v62    # "maxCountToSend":I
    .end local v63    # "noDataCount":[I
    .end local v64    # "minTimeToCheckStopSlow0":J
    .end local v66    # "minTimeToCheckStopSlow1":J
    .end local v68    # "nonReadTime":J
    .end local v70    # "prevBytes":[J
    .end local v71    # "prevSp":[J
    .end local v72    # "prevTime":[J
    :catch_6
    move-exception v4

    goto/16 :goto_1c

    .line 1333
    .restart local v26    # "otherInterface":I
    .restart local v33    # "blockNum":I
    .restart local v34    # "byteAfterSample":J
    .restart local v36    # "byteBeforeSample":J
    .restart local v40    # "bytesExpected":J
    .restart local v49    # "i":I
    .restart local v52    # "iInf":I
    .restart local v60    # "maxBufDataSize":J
    .restart local v78    # "sizeForExtremeCase":J
    .restart local v80    # "smartBondingData":[J
    .restart local v81    # "t":[Ljava/lang/Thread;
    :catch_7
    move-exception v4

    goto/16 :goto_12

    .line 1292
    .end local v33    # "blockNum":I
    .end local v34    # "byteAfterSample":J
    .end local v40    # "bytesExpected":J
    .end local v49    # "i":I
    .end local v81    # "t":[Ljava/lang/Thread;
    :catch_8
    move-exception v4

    goto/16 :goto_d

    .line 1237
    .end local v26    # "otherInterface":I
    .end local v36    # "byteBeforeSample":J
    .restart local v14    # "start":J
    .restart local v20    # "otherSocketId":I
    .restart local v23    # "end":J
    .local v81, "t":Ljava/lang/Thread;
    :catch_9
    move-exception v4

    goto/16 :goto_b

    .line 1355
    :array_0
    .array-data 8
        0x0
        0x0
    .end array-data

    .line 1356
    :array_1
    .array-data 8
        0x0
        0x0
    .end array-data

    .line 1357
    :array_2
    .array-data 8
        0x0
        0x0
    .end array-data

    .line 1360
    :array_3
    .array-data 4
        0x0
        0x0
    .end array-data
.end method
