.class Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;
.super Ljava/lang/Object;
.source "MultiSocketInputStream.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/okhttp/internal/http/MultiSocketInputStream;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DataBuffer"
.end annotation


# instance fields
.field private bSavedInFile:Z

.field private bSwitchedToChild:Z

.field private final bufOffset:J

.field private childChunkEnd:J

.field private childChunkInput:Ljava/io/InputStream;

.field private childChunkStart:J

.field private dis:Ljava/io/DataInputStream;

.field private dos:Ljava/io/DataOutputStream;

.field private fileBuf:Ljava/io/File;

.field private firstBlockOffset:I

.field private fullRead:Z

.field private hasReadLen:J

.field private hasReadLenForSpeed:J

.field private lastTime:J

.field private mBuffer:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<[B>;"
        }
    .end annotation
.end field

.field private final mBufferLength:J

.field private final mID:I

.field private offset:J

.field private readOffset:J

.field private restLen:J

.field private sockID:I

.field private startReadCacheTime:J

.field private startTime:J

.field final synthetic this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

.field private totalContinuousChunk:I


# direct methods
.method public constructor <init>(Lcom/android/okhttp/internal/http/MultiSocketInputStream;JIJLjava/util/LinkedList;II)V
    .locals 10
    .param p2, "start"    # J
    .param p4, "id"    # I
    .param p5, "len"    # J
    .param p8, "sID"    # I
    .param p9, "totalBlocks"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JIJ",
            "Ljava/util/LinkedList",
            "<[B>;II)V"
        }
    .end annotation

    .prologue
    .line 3404
    .local p7, "buffer":Ljava/util/LinkedList;, "Ljava/util/LinkedList<[B>;"
    iput-object p1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3372
    const/4 v6, 0x0

    iput-object v6, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->dis:Ljava/io/DataInputStream;

    .line 3376
    const/4 v6, 0x0

    iput-object v6, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->dos:Ljava/io/DataOutputStream;

    .line 3380
    const/4 v6, 0x0

    iput-object v6, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->fileBuf:Ljava/io/File;

    .line 3385
    const/4 v6, 0x0

    iput v6, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->firstBlockOffset:I

    .line 3387
    const/4 v6, 0x0

    iput-object v6, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->childChunkInput:Ljava/io/InputStream;

    .line 3388
    const-wide/16 v6, 0x0

    iput-wide v6, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->childChunkStart:J

    .line 3389
    const-wide/16 v6, -0x1

    iput-wide v6, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->childChunkEnd:J

    .line 3392
    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->bSwitchedToChild:Z

    .line 3393
    const-wide/16 v6, 0x0

    iput-wide v6, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->startReadCacheTime:J

    .line 3405
    iput p4, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->mID:I

    .line 3406
    move/from16 v0, p8

    iput v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->sockID:I

    .line 3407
    iput-wide p5, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->mBufferLength:J

    .line 3408
    move-object/from16 v0, p7

    iput-object v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->mBuffer:Ljava/util/LinkedList;

    .line 3409
    iget-object v6, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->mBuffer:Ljava/util/LinkedList;

    if-nez v6, :cond_0

    new-instance v6, Ljava/util/LinkedList;

    invoke-direct {v6}, Ljava/util/LinkedList;-><init>()V

    iput-object v6, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->mBuffer:Ljava/util/LinkedList;

    .line 3410
    :cond_0
    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->fullRead:Z

    .line 3411
    const-wide/16 v6, 0x0

    iput-wide v6, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->hasReadLen:J

    .line 3412
    const-wide/16 v6, 0x0

    iput-wide v6, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->restLen:J

    .line 3413
    const-wide/16 v6, 0x0

    iput-wide v6, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->startTime:J

    .line 3414
    const-wide/16 v6, 0x0

    iput-wide v6, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->lastTime:J

    .line 3415
    iput-wide p2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->offset:J

    .line 3416
    iput-wide p2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->readOffset:J

    .line 3417
    iput-wide p2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->bufOffset:J

    .line 3418
    move/from16 v0, p9

    iput v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->totalContinuousChunk:I

    .line 3419
    sget-boolean v6, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v6, :cond_1

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {p1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "new DBuffer added with ["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "], from "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " with length "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-wide v0, p5

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 3421
    :cond_1
    iget v6, p1, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mCacheType:I

    if-nez v6, :cond_5

    .line 3422
    const/4 v2, 0x0

    .line 3423
    .local v2, "bCreated":Z
    iget-object v6, p1, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mRAdownloadedFile:Ljava/io/RandomAccessFile;

    if-eqz v6, :cond_3

    .line 3425
    :try_start_0
    sget-boolean v6, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v6, :cond_2

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {p1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "seek to file pos "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-wide v8, p1, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mDiff_Http_File_Pos:J

    sub-long v8, p2, v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 3426
    :cond_2
    iget-object v6, p1, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mRAdownloadedFile:Ljava/io/RandomAccessFile;

    iget-wide v8, p1, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mDiff_Http_File_Pos:J

    sub-long v8, p2, v8

    invoke-virtual {v6, v8, v9}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 3427
    const/4 v2, 0x1

    .line 3428
    sget-boolean v6, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v6, :cond_3

    .line 3437
    :cond_3
    :goto_0
    iget-object v6, p1, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mRAdownloadedFile:Ljava/io/RandomAccessFile;

    if-nez v6, :cond_4

    .line 3438
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    const/4 v6, 0x3

    if-ge v4, v6, :cond_4

    .line 3439
    invoke-direct {p0}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->createTempBufFile()Z

    move-result v6

    if-eqz v6, :cond_7

    .line 3440
    const/4 v2, 0x1

    .line 3455
    .end local v4    # "i":I
    :cond_4
    if-eqz v2, :cond_8

    iget v6, p1, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mCacheType:I

    if-nez v6, :cond_8

    const/4 v6, 0x1

    :goto_2
    iput-boolean v6, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->bSavedInFile:Z

    .line 3457
    .end local v2    # "bCreated":Z
    :cond_5
    return-void

    .line 3432
    .restart local v2    # "bCreated":Z
    :catch_0
    move-exception v3

    .line 3433
    .local v3, "ex":Ljava/lang/Exception;
    sget-boolean v6, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v6, :cond_6

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {p1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v6

    invoke-virtual {v6, v3}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V

    .line 3434
    :cond_6
    const/4 v6, 0x0

    iput-object v6, p1, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mRAdownloadedFile:Ljava/io/RandomAccessFile;

    goto :goto_0

    .line 3444
    .end local v3    # "ex":Ljava/lang/Exception;
    .restart local v4    # "i":I
    :cond_7
    invoke-direct {p0}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->closeAndDelFile()V

    .line 3445
    new-instance v5, Ljava/lang/Object;

    invoke-direct {v5}, Ljava/lang/Object;-><init>()V

    .line 3446
    .local v5, "obj":Ljava/lang/Object;
    monitor-enter v5

    .line 3448
    const-wide/16 v6, 0x1f4

    :try_start_1
    invoke-virtual {v5, v6, v7}, Ljava/lang/Object;->wait(J)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3451
    :goto_3
    :try_start_2
    monitor-exit v5

    .line 3438
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 3451
    :catchall_0
    move-exception v6

    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v6

    .line 3455
    .end local v4    # "i":I
    .end local v5    # "obj":Ljava/lang/Object;
    :cond_8
    const/4 v6, 0x0

    goto :goto_2

    .line 3450
    .restart local v4    # "i":I
    .restart local v5    # "obj":Ljava/lang/Object;
    :catch_1
    move-exception v6

    goto :goto_3
.end method

.method private closeAndDelFile()V
    .locals 5

    .prologue
    .line 3486
    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-object v2, v1, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->tmpFileLocker:Ljava/lang/Object;

    monitor-enter v2

    .line 3488
    :try_start_0
    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->fileBuf:Ljava/io/File;

    if-eqz v1, :cond_3

    .line 3489
    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->dis:Ljava/io/DataInputStream;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->dis:Ljava/io/DataInputStream;

    invoke-virtual {v1}, Ljava/io/DataInputStream;->close()V

    .line 3490
    :cond_0
    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->dos:Ljava/io/DataOutputStream;

    if-eqz v1, :cond_1

    .line 3491
    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->dos:Ljava/io/DataOutputStream;

    invoke-virtual {v1}, Ljava/io/DataOutputStream;->flush()V

    .line 3492
    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->dos:Ljava/io/DataOutputStream;

    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V

    .line 3494
    :cond_1
    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->fileBuf:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 3495
    sget-boolean v1, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "resource check: buffered file removed (main) "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->fileBuf:Ljava/io/File;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 3500
    :cond_2
    :goto_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->fileBuf:Ljava/io/File;

    .line 3502
    :cond_3
    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    invoke-virtual {v1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->clearBufferDir()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3507
    :cond_4
    :goto_1
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3508
    return-void

    .line 3498
    :cond_5
    :try_start_2
    sget-boolean v1, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "resource check: failed to remove buffered file (main) "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->fileBuf:Ljava/io/File;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 3504
    :catch_0
    move-exception v0

    .line 3505
    .local v0, "ex":Ljava/lang/Throwable;
    :try_start_3
    sget-boolean v1, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V

    goto :goto_1

    .line 3507
    .end local v0    # "ex":Ljava/lang/Throwable;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v1
.end method

.method private createTempBufFile()Z
    .locals 6

    .prologue
    .line 3463
    iget-object v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-object v3, v2, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->tmpFileLocker:Ljava/lang/Object;

    monitor-enter v3

    .line 3464
    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ".sbBuf_"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Thread;->getId()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "_"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 3465
    .local v0, "bufFileName":Ljava/lang/String;
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v2}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "try to save buffer to file "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3467
    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    invoke-virtual {v2}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->createBufferDir()V

    .line 3468
    const/4 v2, 0x0

    iget-object v4, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-object v4, v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->parentDir:Ljava/io/File;

    invoke-static {v0, v2, v4}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v2

    iput-object v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->fileBuf:Ljava/io/File;

    .line 3469
    new-instance v2, Ljava/io/DataInputStream;

    new-instance v4, Ljava/io/FileInputStream;

    iget-object v5, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->fileBuf:Ljava/io/File;

    invoke-direct {v4, v5}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v2, v4}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->dis:Ljava/io/DataInputStream;

    .line 3470
    new-instance v2, Ljava/io/DataOutputStream;

    new-instance v4, Ljava/io/FileOutputStream;

    iget-object v5, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->fileBuf:Ljava/io/File;

    invoke-direct {v4, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v2, v4}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    iput-object v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->dos:Ljava/io/DataOutputStream;

    .line 3471
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v2}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "resource check: buffered file generated (main) "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->fileBuf:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3472
    :cond_1
    const/4 v2, 0x1

    :try_start_2
    monitor-exit v3

    .line 3478
    :goto_0
    return v2

    .line 3474
    :catch_0
    move-exception v1

    .line 3475
    .local v1, "e":Ljava/lang/Throwable;
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v2}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V

    .line 3476
    :cond_2
    invoke-direct {p0}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->closeAndDelFile()V

    .line 3477
    const/4 v0, 0x0

    .line 3478
    const/4 v2, 0x0

    monitor-exit v3

    goto :goto_0

    .line 3480
    .end local v0    # "bufFileName":Ljava/lang/String;
    .end local v1    # "e":Ljava/lang/Throwable;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2
.end method


# virtual methods
.method public changeSockID(I)V
    .locals 0
    .param p1, "sid"    # I

    .prologue
    .line 3871
    iput p1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->sockID:I

    .line 3872
    return-void
.end method

.method public clearBuffer()V
    .locals 1

    .prologue
    .line 3889
    iget-boolean v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->bSavedInFile:Z

    if-eqz v0, :cond_0

    .line 3890
    invoke-direct {p0}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->closeAndDelFile()V

    .line 3895
    :goto_0
    return-void

    .line 3893
    :cond_0
    iget-object v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->mBuffer:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    goto :goto_0
.end method

.method public getHasReadLen()J
    .locals 2

    .prologue
    .line 3542
    iget-wide v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->hasReadLen:J

    return-wide v0
.end method

.method public getID()I
    .locals 1

    .prologue
    .line 3528
    iget v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->mID:I

    return v0
.end method

.method public getLength()J
    .locals 2

    .prologue
    .line 3521
    iget-wide v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->mBufferLength:J

    return-wide v0
.end method

.method public getOffset()J
    .locals 2

    .prologue
    .line 3941
    iget-wide v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->offset:J

    return-wide v0
.end method

.method public getRestLength()J
    .locals 2

    .prologue
    .line 3927
    iget-wide v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->restLen:J

    return-wide v0
.end method

.method public getSockID()I
    .locals 1

    .prologue
    .line 3535
    iget v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->sockID:I

    return v0
.end method

.method public getToBeReadLength()J
    .locals 4

    .prologue
    .line 3934
    iget-wide v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->mBufferLength:J

    iget-wide v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->hasReadLen:J

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method public getTotalChunks()I
    .locals 1

    .prologue
    .line 3950
    iget v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->totalContinuousChunk:I

    return v0
.end method

.method public isBufferInFile()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 3514
    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget v1, v1, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mCacheType:I

    if-eq v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isEmpty()Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 3878
    monitor-enter p0

    .line 3879
    :try_start_0
    iget-boolean v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->bSavedInFile:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->childChunkInput:Ljava/io/InputStream;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->fileBuf:Ljava/io/File;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-object v1, v1, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mRAdownloadedFile:Ljava/io/RandomAccessFile;

    if-nez v1, :cond_0

    .line 3880
    monitor-exit p0

    .line 3881
    :goto_0
    return v0

    :cond_0
    iget-wide v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->restLen:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-gtz v1, :cond_1

    :goto_1
    monitor-exit p0

    goto :goto_0

    .line 3882
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 3881
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public isFullRead()Z
    .locals 1

    .prologue
    .line 3919
    iget-boolean v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->fullRead:Z

    return v0
.end method

.method public push(I[BI)J
    .locals 9
    .param p1, "sid"    # I
    .param p2, "b"    # [B
    .param p3, "length"    # I

    .prologue
    const-wide/16 v4, -0x1

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 3780
    iget v6, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->sockID:I

    if-eq v6, p1, :cond_1

    .line 3834
    :cond_0
    :goto_0
    return-wide v4

    .line 3783
    :cond_1
    sget-boolean v6, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v6, :cond_2

    .line 3784
    :cond_2
    iget-boolean v6, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->bSavedInFile:Z

    if-eqz v6, :cond_9

    .line 3786
    :try_start_0
    iget-object v6, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-object v6, v6, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mRAdownloadedFile:Ljava/io/RandomAccessFile;

    if-nez v6, :cond_6

    .line 3787
    iget-object v6, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->dos:Ljava/io/DataOutputStream;

    const/4 v7, 0x0

    invoke-virtual {v6, p2, v7, p3}, Ljava/io/DataOutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 3820
    :cond_3
    :goto_1
    iget-wide v4, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->hasReadLen:J

    int-to-long v6, p3

    add-long/2addr v4, v6

    iput-wide v4, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->hasReadLen:J

    .line 3821
    iget-wide v4, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->restLen:J

    int-to-long v6, p3

    add-long/2addr v4, v6

    iput-wide v4, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->restLen:J

    .line 3822
    iget-wide v4, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->offset:J

    int-to-long v6, p3

    add-long/2addr v4, v6

    iput-wide v4, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->offset:J

    .line 3823
    iget-wide v4, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->hasReadLenForSpeed:J

    int-to-long v6, p3

    add-long/2addr v4, v6

    iput-wide v4, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->hasReadLenForSpeed:J

    .line 3824
    iget-wide v4, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->hasReadLen:J

    iget-wide v6, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->mBufferLength:J

    cmp-long v4, v4, v6

    if-ltz v4, :cond_5

    .line 3825
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Buffer full read "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->mID:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ":"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->mBufferLength:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 3826
    :cond_4
    iput-boolean v8, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->fullRead:Z

    .line 3827
    iget-object v4, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B

    move-result-object v4

    iget v5, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->mID:I

    const/4 v6, 0x3

    aput-byte v6, v4, v5

    .line 3829
    :cond_5
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->lastTime:J

    .line 3830
    iget-wide v4, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->lastTime:J

    iget-wide v6, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->startTime:J

    sub-long v2, v4, v6

    .line 3834
    .local v2, "t":J
    int-to-long v4, p3

    goto :goto_0

    .line 3790
    .end local v2    # "t":J
    :cond_6
    :try_start_1
    iget-object v6, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-object v6, v6, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mRAdownloadedFile:Ljava/io/RandomAccessFile;

    const/4 v7, 0x0

    invoke-virtual {v6, p2, v7, p3}, Ljava/io/RandomAccessFile;->write([BII)V

    .line 3791
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    if-eqz v4, :cond_3

    goto :goto_1

    .line 3796
    :catch_0
    move-exception v0

    .line 3797
    .local v0, "e":Ljava/lang/Throwable;
    iget-object v6, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-boolean v6, v6, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bFinished:Z

    if-eqz v6, :cond_8

    .line 3798
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_7

    iget-object v4, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    const-string v5, "resource check: buffered file is already removed since download cancelled "

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 3799
    :cond_7
    int-to-long v4, p3

    goto/16 :goto_0

    .line 3802
    :cond_8
    sget-boolean v6, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v6}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v6

    invoke-virtual {v6, v0}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 3807
    .end local v0    # "e":Ljava/lang/Throwable;
    :cond_9
    iget-object v4, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget v4, v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mCacheType:I

    if-ne v4, v8, :cond_3

    .line 3808
    array-length v4, p2

    if-ne v4, p3, :cond_a

    .line 3809
    iget-object v4, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->mBuffer:Ljava/util/LinkedList;

    invoke-virtual {v4, p2}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 3812
    :cond_a
    new-array v1, p3, [B

    .line 3813
    .local v1, "tmp":[B
    invoke-static {p2, v7, v1, v7, p3}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 3814
    iget-object v4, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->mBuffer:Ljava/util/LinkedList;

    invoke-virtual {v4, v1}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    goto/16 :goto_1
.end method

.method public push(Ljava/io/InputStream;JJ)V
    .locals 6
    .param p1, "cin"    # Ljava/io/InputStream;
    .param p2, "s"    # J
    .param p4, "e"    # J

    .prologue
    .line 3552
    monitor-enter p0

    .line 3553
    :try_start_0
    iput-object p1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->childChunkInput:Ljava/io/InputStream;

    .line 3554
    iput-wide p2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->childChunkStart:J

    .line 3555
    iput-wide p4, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->childChunkEnd:J

    .line 3557
    sub-long v2, p4, p2

    const-wide/16 v4, 0x1

    add-long v0, v2, v4

    .line 3558
    .local v0, "len":J
    iget-wide v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->hasReadLen:J

    add-long/2addr v2, v0

    iput-wide v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->hasReadLen:J

    .line 3559
    iget-wide v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->restLen:J

    add-long/2addr v2, v0

    iput-wide v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->restLen:J

    .line 3560
    iget-wide v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->offset:J

    add-long/2addr v2, v0

    iput-wide v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->offset:J

    .line 3561
    iget-wide v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->hasReadLenForSpeed:J

    add-long/2addr v2, v0

    iput-wide v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->hasReadLenForSpeed:J

    .line 3562
    iget-wide v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->hasReadLen:J

    iget-wide v4, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->mBufferLength:J

    cmp-long v2, v2, v4

    if-ltz v2, :cond_1

    .line 3563
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v2}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Buffer full read "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->mID:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->mBufferLength:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 3564
    :cond_0
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->fullRead:Z

    .line 3565
    iget-object v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static {v2}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B

    move-result-object v2

    iget v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->mID:I

    const/4 v4, 0x3

    aput-byte v4, v2, v3

    .line 3567
    :cond_1
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v2}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "push inputstream to data buffer from "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "-"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 3568
    :cond_2
    monitor-exit p0

    .line 3569
    return-void

    .line 3568
    .end local v0    # "len":J
    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public read([BII)I
    .locals 26
    .param p1, "buffer"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I

    .prologue
    .line 3581
    const/4 v13, 0x0

    .line 3582
    .local v13, "readLen":I
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->bSavedInFile:Z

    move/from16 v18, v0

    if-eqz v18, :cond_17

    .line 3584
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->isEmpty()Z

    move-result v18

    if-eqz v18, :cond_0

    const/16 v18, 0x0

    .line 3765
    :goto_0
    return v18

    .line 3585
    :cond_0
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->startReadCacheTime:J

    move-wide/from16 v18, v0

    const-wide/16 v20, 0x0

    cmp-long v18, v18, v20

    if-nez v18, :cond_1

    .line 3586
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->startReadCacheTime:J

    .line 3588
    :cond_1
    sget-boolean v18, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v18, :cond_2

    .line 3589
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->childChunkInput:Ljava/io/InputStream;

    move-object/from16 v18, v0

    if-nez v18, :cond_c

    .line 3590
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mRAdownloadedFile:Ljava/io/RandomAccessFile;

    move-object/from16 v18, v0

    if-nez v18, :cond_a

    .line 3591
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->dis:Ljava/io/DataInputStream;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    invoke-virtual {v0, v1, v2, v3}, Ljava/io/DataInputStream;->read([BII)I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v13

    .line 3729
    :cond_3
    :goto_1
    int-to-long v4, v13

    .line 3730
    .local v4, "actReadLen":J
    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    if-le v13, v0, :cond_4

    .line 3731
    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v18, v0

    sub-int v18, v13, v18

    move/from16 v0, v18

    int-to-long v4, v0

    .line 3732
    sget-boolean v18, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v18, :cond_4

    .line 3736
    :cond_4
    const-wide/16 v18, 0x0

    cmp-long v18, v4, v18

    if-lez v18, :cond_9

    .line 3737
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->readOffset:J

    move-wide/from16 v18, v0

    add-long v18, v18, v4

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->readOffset:J

    .line 3738
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->restLen:J

    move-wide/from16 v18, v0

    sub-long v18, v18, v4

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->restLen:J

    .line 3739
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->restLen:J

    move-wide/from16 v18, v0

    const-wide/16 v20, 0x0

    cmp-long v18, v18, v20

    if-nez v18, :cond_6

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->hasReadLen:J

    move-wide/from16 v18, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->mBufferLength:J

    move-wide/from16 v20, v0

    cmp-long v18, v18, v20

    if-ltz v18, :cond_6

    .line 3740
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v18, v0

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static/range {v18 .. v18}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B

    move-result-object v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->mID:I

    move/from16 v19, v0

    const/16 v20, 0x4

    aput-byte v20, v18, v19

    .line 3741
    sget-boolean v18, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v18, :cond_5

    .line 3742
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v18, v0

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static/range {v18 .. v18}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v18

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "resource check: finish reading chunk "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->bufOffset:J

    move-wide/from16 v20, v0

    invoke-virtual/range {v19 .. v21}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "-"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->bufOffset:J

    move-wide/from16 v20, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->mBufferLength:J

    move-wide/from16 v22, v0

    add-long v20, v20, v22

    const-wide/16 v22, 0x1

    sub-long v20, v20, v22

    invoke-virtual/range {v19 .. v21}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " with length "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->mBufferLength:J

    move-wide/from16 v20, v0

    invoke-virtual/range {v19 .. v21}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, ", in which "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->bufOffset:J

    move-wide/from16 v20, v0

    invoke-virtual/range {v19 .. v21}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "-"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->childChunkStart:J

    move-wide/from16 v20, v0

    const-wide/16 v22, 0x1

    sub-long v20, v20, v22

    invoke-virtual/range {v19 .. v21}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " is from cache file, and "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->childChunkStart:J

    move-wide/from16 v20, v0

    invoke-virtual/range {v19 .. v21}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "-"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->childChunkEnd:J

    move-wide/from16 v20, v0

    invoke-virtual/range {v19 .. v21}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " is from child input "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->childChunkInput:Ljava/io/InputStream;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 3747
    :cond_5
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->bSavedInFile:Z

    move/from16 v18, v0

    if-eqz v18, :cond_6

    .line 3748
    invoke-direct/range {p0 .. p0}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->closeAndDelFile()V

    .line 3751
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v18, v0

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bufReadSpeed:J
    invoke-static/range {v18 .. v18}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$6100(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v18

    const-wide/16 v20, 0x0

    cmp-long v18, v18, v20

    if-nez v18, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mRAdownloadedFile:Ljava/io/RandomAccessFile;

    move-object/from16 v18, v0

    if-nez v18, :cond_9

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->bSavedInFile:Z

    move/from16 v18, v0

    if-eqz v18, :cond_9

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->restLen:J

    move-wide/from16 v18, v0

    const-wide/16 v20, 0x0

    cmp-long v18, v18, v20

    if-nez v18, :cond_7

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->hasReadLen:J

    move-wide/from16 v18, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->mBufferLength:J

    move-wide/from16 v20, v0

    cmp-long v18, v18, v20

    if-gez v18, :cond_8

    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v18, v0

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mExtThread:Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;
    invoke-static/range {v18 .. v18}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;

    move-result-object v18

    if-eqz v18, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v18, v0

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mExtThread:Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;
    invoke-static/range {v18 .. v18}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;

    move-result-object v18

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->mSecThreadCreationResult:I
    invoke-static/range {v18 .. v18}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->access$6200(Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;)I

    move-result v18

    const/16 v19, -0x64

    move/from16 v0, v18

    move/from16 v1, v19

    if-eq v0, v1, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v18, v0

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mExtThread:Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;
    invoke-static/range {v18 .. v18}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;

    move-result-object v18

    move-object/from16 v0, v18

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->mNonBufOffset:J

    move-wide/from16 v18, v0

    const-wide/16 v20, 0x0

    cmp-long v18, v18, v20

    if-lez v18, :cond_9

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->readOffset:J

    move-wide/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v20, v0

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mExtThread:Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;
    invoke-static/range {v20 .. v20}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;

    move-result-object v20

    move-object/from16 v0, v20

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->mNonBufOffset:J

    move-wide/from16 v20, v0

    cmp-long v18, v18, v20

    if-ltz v18, :cond_9

    .line 3753
    :cond_8
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->startReadCacheTime:J

    move-wide/from16 v20, v0

    sub-long v14, v18, v20

    .line 3754
    .local v14, "time":J
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->readOffset:J

    move-wide/from16 v18, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->bufOffset:J

    move-wide/from16 v20, v0

    sub-long v10, v18, v20

    .line 3755
    .local v10, "len":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v10, v11, v14, v15}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->getSpeed(JJ)J

    move-result-wide v20

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    # setter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bufReadSpeed:J
    invoke-static {v0, v1, v2}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$6102(Lcom/android/okhttp/internal/http/MultiSocketInputStream;J)J

    .line 3756
    const-wide/16 v18, 0x0

    cmp-long v18, v14, v18

    if-lez v18, :cond_28

    const-wide/16 v18, 0x0

    cmp-long v18, v10, v18

    if-lez v18, :cond_28

    .line 3757
    sget-boolean v18, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v18, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v18, v0

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static/range {v18 .. v18}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v18

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "calculate buf read speed with len="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " and time="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " speed="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v20, v0

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bufReadSpeed:J
    invoke-static/range {v20 .. v20}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$6100(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v20

    invoke-virtual/range {v19 .. v21}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .end local v10    # "len":J
    .end local v14    # "time":J
    :cond_9
    :goto_2
    move/from16 v18, v13

    .line 3765
    goto/16 :goto_0

    .line 3594
    .end local v4    # "actReadLen":J
    :cond_a
    :try_start_1
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->restLen:J

    move-wide/from16 v18, v0

    const-wide/32 v20, 0x7fffffff

    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v22, v0

    move/from16 v0, v22

    int-to-long v0, v0

    move-wide/from16 v22, v0

    sub-long v20, v20, v22

    cmp-long v18, v18, v20

    if-ltz v18, :cond_b

    .line 3595
    const v13, 0x7fffffff

    .line 3600
    :goto_3
    sget-boolean v18, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v18, :cond_3

    goto/16 :goto_1

    .line 3598
    :cond_b
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->restLen:J

    move-wide/from16 v18, v0

    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    int-to-long v0, v0

    move-wide/from16 v20, v0

    add-long v18, v18, v20

    move-wide/from16 v0, v18

    long-to-int v13, v0

    goto :goto_3

    .line 3604
    :cond_c
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->readOffset:J

    move-wide/from16 v18, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->childChunkStart:J

    move-wide/from16 v20, v0

    cmp-long v18, v18, v20

    if-gez v18, :cond_f

    .line 3605
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mRAdownloadedFile:Ljava/io/RandomAccessFile;

    move-object/from16 v18, v0

    if-nez v18, :cond_d

    .line 3606
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->dis:Ljava/io/DataInputStream;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->childChunkStart:J

    move-wide/from16 v20, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->readOffset:J

    move-wide/from16 v22, v0

    sub-long v20, v20, v22

    move-wide/from16 v0, v20

    long-to-int v0, v0

    move/from16 v19, v0

    move/from16 v0, p3

    move/from16 v1, v19

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v19

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, v19

    invoke-virtual {v0, v1, v2, v3}, Ljava/io/DataInputStream;->read([BII)I

    move-result v13

    goto/16 :goto_1

    .line 3609
    :cond_d
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->childChunkStart:J

    move-wide/from16 v18, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->readOffset:J

    move-wide/from16 v20, v0

    sub-long v18, v18, v20

    const-wide/32 v20, 0x7fffffff

    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v22, v0

    move/from16 v0, v22

    int-to-long v0, v0

    move-wide/from16 v22, v0

    sub-long v20, v20, v22

    cmp-long v18, v18, v20

    if-ltz v18, :cond_e

    .line 3610
    const v13, 0x7fffffff

    .line 3615
    :goto_4
    sget-boolean v18, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v18, :cond_3

    goto/16 :goto_1

    .line 3613
    :cond_e
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->childChunkStart:J

    move-wide/from16 v18, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->readOffset:J

    move-wide/from16 v20, v0

    sub-long v18, v18, v20

    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    int-to-long v0, v0

    move-wide/from16 v20, v0

    add-long v18, v18, v20

    move-wide/from16 v0, v18

    long-to-int v13, v0

    goto :goto_4

    .line 3619
    :cond_f
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->bSwitchedToChild:Z

    move/from16 v18, v0

    if-nez v18, :cond_11

    .line 3620
    sget-boolean v18, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v18, :cond_10

    .line 3621
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->startReadCacheTime:J

    move-wide/from16 v20, v0

    sub-long v16, v18, v20

    .line 3622
    .local v16, "usedTime":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v18, v0

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static/range {v18 .. v18}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v20

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "resource check: finish reading chunk "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->bufOffset:J

    move-wide/from16 v22, v0

    move-object/from16 v0, v18

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "-"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->childChunkStart:J

    move-wide/from16 v22, v0

    const-wide/16 v24, 0x1

    sub-long v22, v22, v24

    move-object/from16 v0, v18

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " with length "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->childChunkStart:J

    move-wide/from16 v22, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->bufOffset:J

    move-wide/from16 v24, v0

    sub-long v22, v22, v24

    move-object/from16 v0, v18

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " using time "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ", so speed is "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-wide/16 v18, 0x0

    cmp-long v18, v16, v18

    if-nez v18, :cond_14

    const-wide/16 v18, 0x0

    :goto_5
    move-object/from16 v0, v21

    move-wide/from16 v1, v18

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " Mbps"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " from cache file, now bytesRemaining data "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->childChunkStart:J

    move-wide/from16 v22, v0

    move-object/from16 v0, v18

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "-"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->childChunkEnd:J

    move-wide/from16 v22, v0

    move-object/from16 v0, v18

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " will be read from child input "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->childChunkInput:Ljava/io/InputStream;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v20

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 3628
    .end local v16    # "usedTime":J
    :cond_10
    invoke-direct/range {p0 .. p0}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->closeAndDelFile()V

    .line 3629
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->bSwitchedToChild:Z

    .line 3631
    :cond_11
    sget-boolean v18, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v18, :cond_12

    .line 3632
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->readOffset:J

    move-wide/from16 v18, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->childChunkStart:J

    move-wide/from16 v20, v0

    cmp-long v18, v18, v20

    if-nez v18, :cond_12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->childChunkInput:Ljava/io/InputStream;

    move-object/from16 v18, v0

    if-eqz v18, :cond_12

    .line 3633
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v18, v0

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static/range {v18 .. v18}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v18

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "now start to read from childChunkInput "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->childChunkInput:Ljava/io/InputStream;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 3636
    :cond_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->childChunkInput:Ljava/io/InputStream;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    instance-of v0, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    move/from16 v18, v0

    if-eqz v18, :cond_15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->childChunkInput:Ljava/io/InputStream;

    move-object/from16 v18, v0

    check-cast v18, Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    invoke-virtual/range {v18 .. v18}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->isClosed()Z

    move-result v18

    if-eqz v18, :cond_15

    .line 3637
    sget-boolean v18, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v18, :cond_13

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v18, v0

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static/range {v18 .. v18}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v18

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "childChunkInput is closed : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->childChunkInput:Ljava/io/InputStream;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 3638
    :cond_13
    const/16 v18, -0x1

    goto/16 :goto_0

    .line 3622
    .restart local v16    # "usedTime":J
    :cond_14
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->readOffset:J

    move-wide/from16 v18, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->bufOffset:J

    move-wide/from16 v22, v0

    sub-long v18, v18, v22

    const-wide/16 v22, 0x8

    mul-long v18, v18, v22

    const-wide/16 v22, 0x3e8

    mul-long v18, v18, v22

    div-long v18, v18, v16

    const-wide/16 v22, 0x400

    div-long v18, v18, v22

    const-wide/16 v22, 0x400

    div-long v18, v18, v22

    goto/16 :goto_5

    .line 3640
    .end local v16    # "usedTime":J
    :cond_15
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->childChunkInput:Ljava/io/InputStream;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    invoke-virtual {v0, v1, v2, v3}, Ljava/io/InputStream;->read([BII)I
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    move-result v13

    goto/16 :goto_1

    .line 3644
    :catch_0
    move-exception v8

    .line 3645
    .local v8, "e":Ljava/lang/Throwable;
    sget-boolean v18, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v18, :cond_16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v18, v0

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static/range {v18 .. v18}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v8}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V

    .line 3646
    :cond_16
    invoke-direct/range {p0 .. p0}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->closeAndDelFile()V

    .line 3647
    const/4 v13, -0x1

    .line 3648
    goto/16 :goto_1

    .line 3650
    .end local v8    # "e":Ljava/lang/Throwable;
    :cond_17
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mCacheType:I

    move/from16 v18, v0

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_24

    .line 3651
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->mBuffer:Ljava/util/LinkedList;

    move-object/from16 v19, v0

    monitor-enter v19

    .line 3652
    :try_start_2
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->startReadCacheTime:J

    move-wide/from16 v20, v0

    const-wide/16 v22, 0x0

    cmp-long v18, v20, v22

    if-nez v18, :cond_18

    .line 3653
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v20

    move-wide/from16 v0, v20

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->startReadCacheTime:J

    .line 3655
    :cond_18
    move/from16 v6, p2

    .line 3656
    .local v6, "appOffset":I
    :cond_19
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->mBuffer:Ljava/util/LinkedList;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v18

    if-nez v18, :cond_1a

    .line 3657
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->mBuffer:Ljava/util/LinkedList;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [B

    .line 3658
    .local v7, "b":[B
    if-nez v7, :cond_1f

    .line 3659
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->firstBlockOffset:I

    .line 3683
    .end local v7    # "b":[B
    :cond_1a
    :goto_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->childChunkInput:Ljava/io/InputStream;

    move-object/from16 v18, v0

    if-eqz v18, :cond_22

    if-nez v13, :cond_22

    .line 3684
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->bSwitchedToChild:Z

    move/from16 v18, v0

    if-nez v18, :cond_1c

    .line 3685
    sget-boolean v18, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v18, :cond_1b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v18, v0

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static/range {v18 .. v18}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v18

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "resource check: finish reading chunk "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->bufOffset:J

    move-wide/from16 v22, v0

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "-"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->childChunkStart:J

    move-wide/from16 v22, v0

    const-wide/16 v24, 0x1

    sub-long v22, v22, v24

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " with length "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->childChunkStart:J

    move-wide/from16 v22, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->bufOffset:J

    move-wide/from16 v24, v0

    sub-long v22, v22, v24

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " using time "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v22

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->startReadCacheTime:J

    move-wide/from16 v24, v0

    sub-long v22, v22, v24

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " from RAM cache, now bytesRemaining data "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->childChunkStart:J

    move-wide/from16 v22, v0

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "-"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->childChunkEnd:J

    move-wide/from16 v22, v0

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " will be read from child input "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->childChunkInput:Ljava/io/InputStream;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 3689
    :cond_1b
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->bSwitchedToChild:Z

    .line 3691
    :cond_1c
    sget-boolean v18, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v18, :cond_1d

    .line 3692
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->readOffset:J

    move-wide/from16 v20, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->childChunkStart:J

    move-wide/from16 v22, v0

    cmp-long v18, v20, v22

    if-nez v18, :cond_1d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->childChunkInput:Ljava/io/InputStream;

    move-object/from16 v18, v0

    if-eqz v18, :cond_1d

    .line 3693
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v18, v0

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static/range {v18 .. v18}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v18

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "now start to read from childChunkInput "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->childChunkInput:Ljava/io/InputStream;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 3696
    :cond_1d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->childChunkInput:Ljava/io/InputStream;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    instance-of v0, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    move/from16 v18, v0

    if-eqz v18, :cond_21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->childChunkInput:Ljava/io/InputStream;

    move-object/from16 v18, v0

    check-cast v18, Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    invoke-virtual/range {v18 .. v18}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->isClosed()Z

    move-result v18

    if-eqz v18, :cond_21

    .line 3697
    sget-boolean v18, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v18, :cond_1e

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v18, v0

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static/range {v18 .. v18}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v18

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "childChunkInput is closed : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->childChunkInput:Ljava/io/InputStream;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 3698
    :cond_1e
    const/16 v18, -0x1

    monitor-exit v19

    goto/16 :goto_0

    .line 3708
    .end local v6    # "appOffset":I
    :catchall_0
    move-exception v18

    monitor-exit v19
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v18

    .line 3662
    .restart local v6    # "appOffset":I
    .restart local v7    # "b":[B
    :cond_1f
    :try_start_3
    array-length v0, v7

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->firstBlockOffset:I

    move/from16 v20, v0

    sub-int v12, v18, v20

    .line 3664
    .local v12, "notReadLen":I
    add-int v18, v13, v12

    move/from16 v0, v18

    move/from16 v1, p3

    if-gt v0, v1, :cond_20

    .line 3665
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->firstBlockOffset:I

    move/from16 v18, v0

    move/from16 v0, v18

    move-object/from16 v1, p1

    invoke-static {v7, v0, v1, v6, v12}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 3666
    add-int/2addr v13, v12

    .line 3667
    add-int/2addr v6, v12

    .line 3668
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->firstBlockOffset:I

    .line 3669
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->mBuffer:Ljava/util/LinkedList;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    .line 3670
    move/from16 v0, p3

    if-ne v13, v0, :cond_19

    goto/16 :goto_6

    .line 3675
    :cond_20
    sub-int v9, p3, v13

    .line 3676
    .local v9, "l":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->firstBlockOffset:I

    move/from16 v18, v0

    move/from16 v0, v18

    move-object/from16 v1, p1

    invoke-static {v7, v0, v1, v6, v9}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 3677
    add-int/2addr v13, v9

    .line 3678
    add-int/2addr v6, v9

    .line 3679
    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->firstBlockOffset:I

    move/from16 v18, v0

    add-int v18, v18, v9

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->firstBlockOffset:I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_6

    .line 3701
    .end local v7    # "b":[B
    .end local v9    # "l":I
    .end local v12    # "notReadLen":I
    :cond_21
    :try_start_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->childChunkInput:Ljava/io/InputStream;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    invoke-virtual {v0, v1, v2, v3}, Ljava/io/InputStream;->read([BII)I
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result v13

    .line 3708
    :cond_22
    :goto_7
    :try_start_5
    monitor-exit v19

    goto/16 :goto_1

    .line 3703
    :catch_1
    move-exception v8

    .line 3704
    .restart local v8    # "e":Ljava/lang/Throwable;
    sget-boolean v18, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v18, :cond_23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v18, v0

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static/range {v18 .. v18}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v8}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 3705
    :cond_23
    const/4 v13, -0x1

    goto :goto_7

    .line 3712
    .end local v6    # "appOffset":I
    .end local v8    # "e":Ljava/lang/Throwable;
    :cond_24
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->childChunkInput:Ljava/io/InputStream;

    move-object/from16 v18, v0

    if-nez v18, :cond_25

    .line 3713
    move/from16 v0, p3

    int-to-long v0, v0

    move-wide/from16 v18, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->restLen:J

    move-wide/from16 v20, v0

    invoke-static/range {v18 .. v21}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v18

    move-wide/from16 v0, v18

    long-to-int v13, v0

    goto/16 :goto_1

    .line 3715
    :cond_25
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->readOffset:J

    move-wide/from16 v18, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->childChunkStart:J

    move-wide/from16 v20, v0

    cmp-long v18, v18, v20

    if-gez v18, :cond_26

    .line 3716
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->childChunkStart:J

    move-wide/from16 v18, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->readOffset:J

    move-wide/from16 v20, v0

    sub-long v18, v18, v20

    move-wide/from16 v0, v18

    long-to-int v0, v0

    move/from16 v18, v0

    move/from16 v0, p3

    move/from16 v1, v18

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v13

    goto/16 :goto_1

    .line 3720
    :cond_26
    :try_start_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->childChunkInput:Ljava/io/InputStream;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    invoke-virtual {v0, v1, v2, v3}, Ljava/io/InputStream;->read([BII)I
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_2

    move-result v13

    goto/16 :goto_1

    .line 3722
    :catch_2
    move-exception v8

    .line 3723
    .restart local v8    # "e":Ljava/lang/Throwable;
    sget-boolean v18, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v18, :cond_27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v18, v0

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static/range {v18 .. v18}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v8}, Lcom/android/okhttp/internal/http/MultiratLog;->w(Ljava/lang/Throwable;)V

    .line 3724
    :cond_27
    const/4 v13, -0x1

    goto/16 :goto_1

    .line 3760
    .end local v8    # "e":Ljava/lang/Throwable;
    .restart local v4    # "actReadLen":J
    .restart local v10    # "len":J
    .restart local v14    # "time":J
    :cond_28
    sget-boolean v18, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v18, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v18, v0

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static/range {v18 .. v18}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v18

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "cannot calculate buf read speed with len="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " and time="

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    goto/16 :goto_2
.end method

.method public removeTail(I)V
    .locals 6
    .param p1, "count"    # I

    .prologue
    .line 3901
    iget-boolean v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->bSavedInFile:Z

    if-eqz v2, :cond_1

    .line 3913
    :cond_0
    return-void

    .line 3905
    :cond_1
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, p1, :cond_0

    .line 3906
    iget-object v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->mBuffer:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->removeLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 3907
    .local v0, "b":[B
    iget-wide v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->hasReadLen:J

    array-length v4, v0

    int-to-long v4, v4

    sub-long/2addr v2, v4

    iput-wide v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->hasReadLen:J

    .line 3908
    iget-wide v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->restLen:J

    array-length v4, v0

    int-to-long v4, v4

    sub-long/2addr v2, v4

    iput-wide v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->restLen:J

    .line 3909
    iget-wide v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->offset:J

    array-length v4, v0

    int-to-long v4, v4

    sub-long/2addr v2, v4

    iput-wide v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->offset:J

    .line 3910
    iget-wide v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->hasReadLenForSpeed:J

    array-length v4, v0

    int-to-long v4, v4

    sub-long/2addr v2, v4

    iput-wide v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->hasReadLenForSpeed:J

    .line 3905
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public setTotalChunks(I)V
    .locals 0
    .param p1, "totalChunks"    # I

    .prologue
    .line 3959
    iput p1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->totalContinuousChunk:I

    .line 3960
    return-void
.end method

.method public startReceiveData()V
    .locals 3

    .prologue
    .line 3771
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->startTime:J

    .line 3772
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->hasReadLenForSpeed:J

    .line 3773
    iget-object v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static {v0}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B

    move-result-object v0

    iget v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->mID:I

    const/4 v2, 0x2

    aput-byte v2, v0, v1

    .line 3774
    return-void
.end method

.method public switchSocket(IZ)[J
    .locals 6
    .param p1, "newSockID"    # I
    .param p2, "bForced"    # Z

    .prologue
    .line 3842
    monitor-enter p0

    .line 3843
    :try_start_0
    sget-boolean v1, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Socket try to switch for block["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->mID:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] from "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->sockID:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", tobeReadLen="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->getToBeReadLength()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 3844
    :cond_0
    invoke-virtual {p0}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->getToBeReadLength()J

    move-result-wide v2

    const-wide/32 v4, 0x20000

    cmp-long v1, v2, v4

    if-gtz v1, :cond_2

    if-nez p2, :cond_2

    .line 3845
    sget-boolean v1, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v1

    const-string v2, "Socket do not need to switch"

    invoke-virtual {v1, v2}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 3846
    :cond_1
    const/4 v1, 0x0

    new-array v0, v1, [J

    monitor-exit p0

    .line 3862
    :goto_0
    return-object v0

    .line 3849
    :cond_2
    sget-boolean v1, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Socket switch for block["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->mID:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] from "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->sockID:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 3850
    :cond_3
    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->requestHandlers:[Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;
    invoke-static {v1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;

    move-result-object v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->requestHandlers:[Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;
    invoke-static {v1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;

    move-result-object v1

    array-length v1, v1

    iget v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->sockID:I

    if-le v1, v2, :cond_5

    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->requestHandlers:[Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;
    invoke-static {v1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;

    move-result-object v1

    iget v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->sockID:I

    aget-object v1, v1, v2

    if-eqz v1, :cond_5

    .line 3851
    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->requestHandlers:[Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;
    invoke-static {v1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;

    move-result-object v1

    iget v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->sockID:I

    aget-object v1, v1, v2

    invoke-virtual {v1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$RangeRequest;->handOver()V

    .line 3856
    :cond_4
    :goto_1
    const/4 v1, 0x4

    new-array v0, v1, [J

    .line 3857
    .local v0, "range":[J
    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->offset:J

    aput-wide v2, v0, v1

    .line 3858
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->offset:J

    invoke-virtual {p0}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->getToBeReadLength()J

    move-result-wide v4

    add-long/2addr v2, v4

    const-wide/16 v4, 0x1

    sub-long/2addr v2, v4

    aput-wide v2, v0, v1

    .line 3859
    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->getToBeReadLength()J

    move-result-wide v2

    aput-wide v2, v0, v1

    .line 3860
    const/4 v1, 0x3

    iget v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->mID:I

    int-to-long v2, v2

    aput-wide v2, v0, v1

    .line 3861
    iput p1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->sockID:I

    .line 3862
    monitor-exit p0

    goto/16 :goto_0

    .line 3864
    .end local v0    # "range":[J
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 3854
    :cond_5
    :try_start_1
    sget-boolean v1, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v1

    const-string v2, "Socket can not be switch because requestHandler is null"

    invoke-virtual {v1, v2}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method
