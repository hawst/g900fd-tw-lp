.class Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;
.super Ljava/lang/Object;
.source "TwoChunkInputStream.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/okhttp/internal/http/TwoChunkInputStream;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SimpleDataBuffer"
.end annotation


# instance fields
.field private bSwitchedToChild:Z

.field private final bufOffset:J

.field private childChunkEnd:J

.field private childChunkInput:Ljava/io/InputStream;

.field private childChunkStart:J

.field private dis:Ljava/io/DataInputStream;

.field private dos:Ljava/io/DataOutputStream;

.field private fileBuf:Ljava/io/File;

.field private fullRead:Z

.field private hasReadLen:J

.field private final mBufferLength:J

.field private offset:J

.field private readOffset:J

.field private restLen:J

.field final synthetic this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;


# direct methods
.method public constructor <init>(Lcom/android/okhttp/internal/http/TwoChunkInputStream;JJ)V
    .locals 10
    .param p2, "start"    # J
    .param p4, "len"    # J

    .prologue
    const/4 v3, 0x0

    const-wide/16 v8, 0x0

    const/4 v6, 0x0

    .line 1181
    iput-object p1, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1156
    iput-object v6, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->dis:Ljava/io/DataInputStream;

    .line 1160
    iput-object v6, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->dos:Ljava/io/DataOutputStream;

    .line 1164
    iput-object v6, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->fileBuf:Ljava/io/File;

    .line 1166
    iput-object v6, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->childChunkInput:Ljava/io/InputStream;

    .line 1167
    iput-wide v8, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->childChunkStart:J

    .line 1168
    const-wide/16 v4, -0x1

    iput-wide v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->childChunkEnd:J

    .line 1171
    iput-boolean v3, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->bSwitchedToChild:Z

    .line 1182
    iput-wide p4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->mBufferLength:J

    .line 1183
    iput-boolean v3, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->fullRead:Z

    .line 1184
    iput-wide v8, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->hasReadLen:J

    .line 1185
    iput-wide v8, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->restLen:J

    .line 1186
    iput-wide p2, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->offset:J

    .line 1187
    iput-wide p2, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->readOffset:J

    .line 1188
    iput-wide p2, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->bufOffset:J

    .line 1189
    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->parentIS:Lcom/android/okhttp/internal/http/MultiSocketInputStream;
    invoke-static {p1}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$200(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-result-object v3

    iget v3, v3, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mCacheType:I

    const/4 v4, 0x2

    if-eq v3, v4, :cond_2

    .line 1190
    iget-object v3, p1, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mChildRAdownloadedFile:Ljava/io/RandomAccessFile;

    if-eqz v3, :cond_1

    .line 1192
    :try_start_0
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "seek to file pos "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->parentIS:Lcom/android/okhttp/internal/http/MultiSocketInputStream;
    invoke-static {p1}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$200(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-result-object v4

    iget-wide v4, v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mDiff_Http_File_Pos:J

    sub-long v4, p2, v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->log(Ljava/lang/String;)V
    invoke-static {p1, v3}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$000(Lcom/android/okhttp/internal/http/TwoChunkInputStream;Ljava/lang/String;)V

    .line 1193
    :cond_0
    iget-object v3, p1, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mChildRAdownloadedFile:Ljava/io/RandomAccessFile;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->parentIS:Lcom/android/okhttp/internal/http/MultiSocketInputStream;
    invoke-static {p1}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$200(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-result-object v4

    iget-wide v4, v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mDiff_Http_File_Pos:J

    sub-long v4, p2, v4

    invoke-virtual {v3, v4, v5}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 1194
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v3, :cond_1

    .line 1203
    :cond_1
    :goto_0
    iget-object v3, p1, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mChildRAdownloadedFile:Ljava/io/RandomAccessFile;

    if-nez v3, :cond_2

    .line 1204
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    const/4 v3, 0x3

    if-ge v1, v3, :cond_2

    .line 1205
    invoke-direct {p0}, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->createTempBufFile()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1221
    .end local v1    # "i":I
    :cond_2
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_3

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {p1}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$100(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "new SimpleDBuffer added from "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " with length "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1222
    :cond_3
    return-void

    .line 1198
    :catch_0
    move-exception v0

    .line 1199
    .local v0, "ex":Ljava/lang/Exception;
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_4

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {p1}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$100(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V

    .line 1200
    :cond_4
    iput-object v6, p1, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mChildRAdownloadedFile:Ljava/io/RandomAccessFile;

    goto :goto_0

    .line 1209
    .end local v0    # "ex":Ljava/lang/Exception;
    .restart local v1    # "i":I
    :cond_5
    invoke-direct {p0}, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->closeAndDelFile()V

    .line 1210
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    .line 1211
    .local v2, "obj":Ljava/lang/Object;
    monitor-enter v2

    .line 1213
    const-wide/16 v4, 0x1f4

    :try_start_1
    invoke-virtual {v2, v4, v5}, Ljava/lang/Object;->wait(J)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1216
    :goto_2
    :try_start_2
    monitor-exit v2

    .line 1204
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1216
    :catchall_0
    move-exception v3

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3

    .line 1215
    :catch_1
    move-exception v3

    goto :goto_2
.end method

.method private closeAndDelFile()V
    .locals 5

    .prologue
    .line 1251
    iget-object v1, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->parentIS:Lcom/android/okhttp/internal/http/MultiSocketInputStream;
    invoke-static {v1}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$200(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-result-object v1

    iget-object v2, v1, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->tmpFileLocker:Ljava/lang/Object;

    monitor-enter v2

    .line 1253
    :try_start_0
    iget-object v1, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->fileBuf:Ljava/io/File;

    if-eqz v1, :cond_3

    .line 1254
    iget-object v1, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->dis:Ljava/io/DataInputStream;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->dis:Ljava/io/DataInputStream;

    invoke-virtual {v1}, Ljava/io/DataInputStream;->close()V

    .line 1255
    :cond_0
    iget-object v1, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->dos:Ljava/io/DataOutputStream;

    if-eqz v1, :cond_1

    .line 1256
    iget-object v1, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->dos:Ljava/io/DataOutputStream;

    invoke-virtual {v1}, Ljava/io/DataOutputStream;->flush()V

    .line 1257
    iget-object v1, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->dos:Ljava/io/DataOutputStream;

    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V

    .line 1259
    :cond_1
    iget-object v1, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->fileBuf:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 1260
    sget-boolean v1, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v1}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$100(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "resource check: buffered file removed "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->fileBuf:Ljava/io/File;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1261
    :cond_2
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->fileBuf:Ljava/io/File;

    .line 1263
    :cond_3
    iget-object v1, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->parentIS:Lcom/android/okhttp/internal/http/MultiSocketInputStream;
    invoke-static {v1}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$200(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->clearBufferDir()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1268
    :cond_4
    :goto_0
    :try_start_1
    monitor-exit v2

    .line 1269
    return-void

    .line 1265
    :catch_0
    move-exception v0

    .line 1266
    .local v0, "ex":Ljava/lang/Throwable;
    sget-boolean v1, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v1}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$100(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1268
    .end local v0    # "ex":Ljava/lang/Throwable;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method private createTempBufFile()Z
    .locals 6

    .prologue
    .line 1228
    iget-object v2, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->parentIS:Lcom/android/okhttp/internal/http/MultiSocketInputStream;
    invoke-static {v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$200(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-result-object v2

    iget-object v3, v2, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->tmpFileLocker:Ljava/lang/Object;

    monitor-enter v3

    .line 1229
    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ".sbBuf_"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Thread;->getId()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "_"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1230
    .local v0, "bufFileName":Ljava/lang/String;
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$100(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "try to save buffer to file "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1232
    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->parentIS:Lcom/android/okhttp/internal/http/MultiSocketInputStream;
    invoke-static {v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$200(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->createBufferDir()V

    .line 1233
    const/4 v2, 0x0

    iget-object v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->parentIS:Lcom/android/okhttp/internal/http/MultiSocketInputStream;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$200(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-result-object v4

    iget-object v4, v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->parentDir:Ljava/io/File;

    invoke-static {v0, v2, v4}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v2

    iput-object v2, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->fileBuf:Ljava/io/File;

    .line 1234
    new-instance v2, Ljava/io/DataInputStream;

    new-instance v4, Ljava/io/FileInputStream;

    iget-object v5, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->fileBuf:Ljava/io/File;

    invoke-direct {v4, v5}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v2, v4}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v2, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->dis:Ljava/io/DataInputStream;

    .line 1235
    new-instance v2, Ljava/io/DataOutputStream;

    new-instance v4, Ljava/io/FileOutputStream;

    iget-object v5, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->fileBuf:Ljava/io/File;

    invoke-direct {v4, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v2, v4}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    iput-object v2, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->dos:Ljava/io/DataOutputStream;

    .line 1236
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$100(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "resource check: buffered file generated "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->fileBuf:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1237
    :cond_1
    const/4 v2, 0x1

    :try_start_2
    monitor-exit v3

    .line 1243
    :goto_0
    return v2

    .line 1239
    :catch_0
    move-exception v1

    .line 1240
    .local v1, "e":Ljava/lang/Throwable;
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$100(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V

    .line 1241
    :cond_2
    invoke-direct {p0}, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->closeAndDelFile()V

    .line 1242
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->fileBuf:Ljava/io/File;

    .line 1243
    const/4 v2, 0x0

    monitor-exit v3

    goto :goto_0

    .line 1245
    .end local v0    # "bufFileName":Ljava/lang/String;
    .end local v1    # "e":Ljava/lang/Throwable;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2
.end method


# virtual methods
.method public clearBuffer()V
    .locals 0

    .prologue
    .line 1515
    invoke-direct {p0}, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->closeAndDelFile()V

    .line 1516
    return-void
.end method

.method public getHasReadLen()J
    .locals 2

    .prologue
    .line 1282
    iget-wide v0, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->hasReadLen:J

    return-wide v0
.end method

.method public getLength()J
    .locals 2

    .prologue
    .line 1275
    iget-wide v0, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->mBufferLength:J

    return-wide v0
.end method

.method public getOffset()J
    .locals 2

    .prologue
    .line 1544
    iget-wide v0, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->offset:J

    return-wide v0
.end method

.method public getRestLength()J
    .locals 2

    .prologue
    .line 1530
    iget-wide v0, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->restLen:J

    return-wide v0
.end method

.method public getToBeReadLength()J
    .locals 4

    .prologue
    .line 1537
    iget-wide v0, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->mBufferLength:J

    iget-wide v2, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->hasReadLen:J

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method public isEmpty()Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 1504
    monitor-enter p0

    .line 1505
    :try_start_0
    iget-object v1, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->childChunkInput:Ljava/io/InputStream;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->fileBuf:Ljava/io/File;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    iget-object v1, v1, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mChildRAdownloadedFile:Ljava/io/RandomAccessFile;

    if-nez v1, :cond_0

    .line 1506
    monitor-exit p0

    .line 1507
    :goto_0
    return v0

    :cond_0
    iget-wide v2, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->restLen:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-gtz v1, :cond_1

    :goto_1
    monitor-exit p0

    goto :goto_0

    .line 1508
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1507
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public isFullRead()Z
    .locals 1

    .prologue
    .line 1522
    iget-boolean v0, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->fullRead:Z

    return v0
.end method

.method public push(I[BI)J
    .locals 10
    .param p1, "sid"    # I
    .param p2, "b"    # [B
    .param p3, "length"    # I

    .prologue
    const-wide/16 v8, -0x1

    .line 1423
    iget-object v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->parentIS:Lcom/android/okhttp/internal/http/MultiSocketInputStream;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$200(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-result-object v4

    iget v4, v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mCacheType:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_3

    .line 1490
    :cond_0
    :goto_0
    iget-wide v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->hasReadLen:J

    int-to-long v6, p3

    add-long/2addr v4, v6

    iput-wide v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->hasReadLen:J

    .line 1491
    iget-wide v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->restLen:J

    int-to-long v6, p3

    add-long/2addr v4, v6

    iput-wide v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->restLen:J

    .line 1492
    iget-wide v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->offset:J

    int-to-long v6, p3

    add-long/2addr v4, v6

    iput-wide v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->offset:J

    .line 1493
    iget-wide v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->hasReadLen:J

    iget-wide v6, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->mBufferLength:J

    cmp-long v4, v4, v6

    if-ltz v4, :cond_2

    .line 1494
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$100(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Buffer full read "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->mBufferLength:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1495
    :cond_1
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->fullRead:Z

    .line 1497
    :cond_2
    int-to-long v4, p3

    :goto_1
    return-wide v4

    .line 1428
    :cond_3
    :try_start_0
    iget-object v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    iget-object v4, v4, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mChildRAdownloadedFile:Ljava/io/RandomAccessFile;

    if-nez v4, :cond_5

    .line 1429
    iget-object v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->dos:Ljava/io/DataOutputStream;

    const/4 v5, 0x0

    invoke-virtual {v4, p2, v5, p3}, Ljava/io/DataOutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1438
    :catch_0
    move-exception v1

    .line 1439
    .local v1, "e":Ljava/lang/Throwable;
    iget-object v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->parentIS:Lcom/android/okhttp/internal/http/MultiSocketInputStream;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$200(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-result-object v4

    iget-boolean v4, v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bFinished:Z

    if-eqz v4, :cond_6

    .line 1440
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$100(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "resource check: buffered file is already removed since download cancelled "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->fileBuf:Ljava/io/File;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1441
    :cond_4
    int-to-long v4, p3

    goto :goto_1

    .line 1432
    .end local v1    # "e":Ljava/lang/Throwable;
    :cond_5
    :try_start_1
    iget-object v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    iget-object v4, v4, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mChildRAdownloadedFile:Ljava/io/RandomAccessFile;

    const/4 v5, 0x0

    invoke-virtual {v4, p2, v5, p3}, Ljava/io/RandomAccessFile;->write([BII)V

    .line 1433
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    if-eqz v4, :cond_0

    goto/16 :goto_0

    .line 1443
    .restart local v1    # "e":Ljava/lang/Throwable;
    :cond_6
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_7

    iget-object v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$100(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V

    .line 1444
    :cond_7
    const/4 v0, 0x1

    .line 1445
    .local v0, "bCreated":Z
    iget-object v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->fileBuf:Ljava/io/File;

    if-eqz v4, :cond_8

    iget-object v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->fileBuf:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_b

    .line 1446
    :cond_8
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_9

    iget-object v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$100(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "resource check: buffered file not found in push "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->fileBuf:Ljava/io/File;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1447
    :cond_9
    const/4 v0, 0x0

    .line 1448
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_2
    const/4 v4, 0x3

    if-ge v3, v4, :cond_a

    .line 1449
    invoke-direct {p0}, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->closeAndDelFile()V

    .line 1450
    invoke-direct {p0}, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->createTempBufFile()Z

    move-result v4

    if-eqz v4, :cond_f

    .line 1451
    const/4 v0, 0x1

    .line 1463
    :cond_a
    if-nez v0, :cond_b

    .line 1464
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_b

    iget-object v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$100(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    const-string v5, "resource check: failed to create temp buffered file for 3 times "

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1467
    .end local v3    # "i":I
    :cond_b
    if-eqz v0, :cond_12

    .line 1468
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_c

    iget-object v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$100(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "buffer file created again in push exception "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->fileBuf:Ljava/io/File;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1469
    :cond_c
    invoke-virtual {p0}, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_10

    .line 1470
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_d

    iget-object v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$100(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    const-string v5, "buffer is empty now, can keep on write"

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1472
    :cond_d
    :try_start_2
    iget-object v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->dos:Ljava/io/DataOutputStream;

    const/4 v5, 0x0

    invoke-virtual {v4, p2, v5, p3}, Ljava/io/DataOutputStream;->write([BII)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_0

    .line 1474
    :catch_1
    move-exception v2

    .line 1475
    .local v2, "ex":Ljava/lang/Throwable;
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_e

    iget-object v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$100(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V

    .line 1476
    :cond_e
    iget-wide v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->readOffset:J

    mul-long/2addr v4, v8

    goto/16 :goto_1

    .line 1455
    .end local v2    # "ex":Ljava/lang/Throwable;
    .restart local v3    # "i":I
    :cond_f
    monitor-enter p0

    .line 1457
    const-wide/16 v4, 0x1f4

    :try_start_3
    invoke-virtual {p0, v4, v5}, Ljava/lang/Object;->wait(J)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1460
    :goto_3
    :try_start_4
    monitor-exit p0

    .line 1448
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 1460
    :catchall_0
    move-exception v4

    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v4

    .line 1480
    .end local v3    # "i":I
    :cond_10
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_11

    iget-object v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$100(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "buffer file created again in push exception, but some bufferred data is missing "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->restLen:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1481
    :cond_11
    iget-wide v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->readOffset:J

    mul-long/2addr v4, v8

    goto/16 :goto_1

    .line 1485
    :cond_12
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_13

    iget-object v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$100(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    const-string v5, "buffer cannot be created again"

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1486
    :cond_13
    iget-wide v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->readOffset:J

    mul-long/2addr v4, v8

    goto/16 :goto_1

    .line 1459
    .restart local v3    # "i":I
    :catch_2
    move-exception v4

    goto :goto_3
.end method

.method public push(Ljava/io/InputStream;JJ)V
    .locals 6
    .param p1, "cin"    # Ljava/io/InputStream;
    .param p2, "s"    # J
    .param p4, "e"    # J

    .prologue
    .line 1291
    monitor-enter p0

    .line 1292
    :try_start_0
    iput-object p1, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->childChunkInput:Ljava/io/InputStream;

    .line 1293
    iput-wide p2, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->childChunkStart:J

    .line 1294
    iput-wide p4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->childChunkEnd:J

    .line 1296
    sub-long v2, p4, p2

    const-wide/16 v4, 0x1

    add-long v0, v2, v4

    .line 1297
    .local v0, "len":J
    iget-wide v2, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->hasReadLen:J

    add-long/2addr v2, v0

    iput-wide v2, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->hasReadLen:J

    .line 1298
    iget-wide v2, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->restLen:J

    add-long/2addr v2, v0

    iput-wide v2, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->restLen:J

    .line 1299
    iget-wide v2, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->offset:J

    add-long/2addr v2, v0

    iput-wide v2, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->offset:J

    .line 1300
    iget-wide v2, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->hasReadLen:J

    iget-wide v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->mBufferLength:J

    cmp-long v2, v2, v4

    if-ltz v2, :cond_1

    .line 1301
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$100(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Buffer full read : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->mBufferLength:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1302
    :cond_0
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->fullRead:Z

    .line 1304
    :cond_1
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$100(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "push inputstream to data buffer from "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "-"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1305
    :cond_2
    monitor-exit p0

    .line 1306
    return-void

    .line 1305
    .end local v0    # "len":J
    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public read([BII)I
    .locals 10
    .param p1, "buffer"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I

    .prologue
    .line 1317
    const/4 v3, 0x0

    .line 1318
    .local v3, "readLen":I
    iget-object v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->parentIS:Lcom/android/okhttp/internal/http/MultiSocketInputStream;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$200(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-result-object v4

    iget v4, v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mCacheType:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_7

    .line 1319
    iget-object v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->childChunkInput:Ljava/io/InputStream;

    if-nez v4, :cond_4

    .line 1320
    int-to-long v4, p3

    iget-wide v6, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->restLen:J

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    long-to-int v3, v4

    .line 1397
    :cond_0
    :goto_0
    int-to-long v0, v3

    .line 1398
    .local v0, "actReadLen":J
    array-length v4, p1

    if-le v3, v4, :cond_1

    .line 1399
    array-length v4, p1

    sub-int v4, v3, v4

    int-to-long v0, v4

    .line 1400
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_1

    .line 1404
    :cond_1
    const-wide/16 v4, 0x0

    cmp-long v4, v0, v4

    if-lez v4, :cond_3

    .line 1405
    iget-wide v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->readOffset:J

    add-long/2addr v4, v0

    iput-wide v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->readOffset:J

    .line 1406
    iget-wide v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->restLen:J

    sub-long/2addr v4, v0

    iput-wide v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->restLen:J

    .line 1407
    iget-wide v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->restLen:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-nez v4, :cond_3

    iget-wide v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->hasReadLen:J

    iget-wide v6, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->mBufferLength:J

    cmp-long v4, v4, v6

    if-ltz v4, :cond_3

    .line 1408
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$100(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "resource check: finish reading chunk "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->bufOffset:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "-"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->bufOffset:J

    iget-wide v8, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->mBufferLength:J

    add-long/2addr v6, v8

    const-wide/16 v8, 0x1

    sub-long/2addr v6, v8

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " with length "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->mBufferLength:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", in which "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->bufOffset:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "-"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->childChunkStart:J

    const-wide/16 v8, 0x1

    sub-long/2addr v6, v8

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " is from cache file "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->fileBuf:Ljava/io/File;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", and "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->childChunkStart:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "-"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->childChunkEnd:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " is from child input "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->childChunkInput:Ljava/io/InputStream;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1413
    :cond_2
    invoke-direct {p0}, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->closeAndDelFile()V

    :cond_3
    move v4, v3

    .line 1416
    .end local v0    # "actReadLen":J
    :goto_1
    return v4

    .line 1322
    :cond_4
    iget-wide v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->readOffset:J

    iget-wide v6, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->childChunkStart:J

    cmp-long v4, v4, v6

    if-gez v4, :cond_5

    .line 1323
    iget-wide v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->childChunkStart:J

    iget-wide v6, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->readOffset:J

    sub-long/2addr v4, v6

    long-to-int v4, v4

    invoke-static {p3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    goto/16 :goto_0

    .line 1327
    :cond_5
    :try_start_0
    iget-object v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->childChunkInput:Ljava/io/InputStream;

    invoke-virtual {v4, p1, p2, p3}, Ljava/io/InputStream;->read([BII)I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    goto/16 :goto_0

    .line 1329
    :catch_0
    move-exception v2

    .line 1330
    .local v2, "e":Ljava/lang/Throwable;
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_6

    iget-object v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$100(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V

    .line 1331
    :cond_6
    const/4 v3, -0x1

    .line 1332
    goto/16 :goto_0

    .line 1337
    .end local v2    # "e":Ljava/lang/Throwable;
    :cond_7
    :try_start_1
    invoke-virtual {p0}, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_8

    const/4 v4, 0x0

    goto :goto_1

    .line 1338
    :cond_8
    iget-object v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->childChunkInput:Ljava/io/InputStream;

    if-nez v4, :cond_c

    .line 1339
    iget-object v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    iget-object v4, v4, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mChildRAdownloadedFile:Ljava/io/RandomAccessFile;

    if-nez v4, :cond_a

    .line 1340
    iget-object v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->dis:Ljava/io/DataInputStream;

    invoke-virtual {v4, p1, p2, p3}, Ljava/io/DataInputStream;->read([BII)I

    move-result v3

    .line 1386
    :goto_2
    if-gez v3, :cond_0

    .line 1387
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_9

    iget-object v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$100(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "get read length < 0, but return 0 "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1388
    :cond_9
    const/4 v3, -0x1

    goto/16 :goto_0

    .line 1343
    :cond_a
    iget-wide v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->restLen:J

    const-wide/32 v6, 0x7fffffff

    array-length v8, p1

    int-to-long v8, v8

    sub-long/2addr v6, v8

    cmp-long v4, v4, v6

    if-ltz v4, :cond_b

    .line 1344
    const v3, 0x7fffffff

    goto :goto_2

    .line 1347
    :cond_b
    iget-wide v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->restLen:J

    array-length v6, p1

    int-to-long v6, v6

    add-long/2addr v4, v6

    long-to-int v3, v4

    goto :goto_2

    .line 1352
    :cond_c
    iget-wide v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->readOffset:J

    iget-wide v6, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->childChunkStart:J

    cmp-long v4, v4, v6

    if-gez v4, :cond_f

    .line 1353
    iget-object v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    iget-object v4, v4, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mChildRAdownloadedFile:Ljava/io/RandomAccessFile;

    if-nez v4, :cond_d

    .line 1354
    iget-object v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->dis:Ljava/io/DataInputStream;

    iget-wide v6, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->childChunkStart:J

    iget-wide v8, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->readOffset:J

    sub-long/2addr v6, v8

    long-to-int v5, v6

    invoke-static {p3, v5}, Ljava/lang/Math;->min(II)I

    move-result v5

    invoke-virtual {v4, p1, p2, v5}, Ljava/io/DataInputStream;->read([BII)I

    move-result v3

    goto :goto_2

    .line 1357
    :cond_d
    iget-wide v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->childChunkStart:J

    iget-wide v6, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->readOffset:J

    sub-long/2addr v4, v6

    const-wide/32 v6, 0x7fffffff

    array-length v8, p1

    int-to-long v8, v8

    sub-long/2addr v6, v8

    cmp-long v4, v4, v6

    if-ltz v4, :cond_e

    .line 1358
    const v3, 0x7fffffff

    goto :goto_2

    .line 1361
    :cond_e
    iget-wide v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->childChunkStart:J

    iget-wide v6, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->readOffset:J

    sub-long/2addr v4, v6

    array-length v6, p1

    int-to-long v6, v6

    add-long/2addr v4, v6

    long-to-int v3, v4

    goto :goto_2

    .line 1366
    :cond_f
    iget-boolean v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->bSwitchedToChild:Z

    if-nez v4, :cond_11

    .line 1367
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_10

    iget-object v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$100(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "resource check: finish reading sec chunk "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->bufOffset:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "-"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->childChunkStart:J

    const-wide/16 v8, 0x1

    sub-long/2addr v6, v8

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " with length "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->childChunkStart:J

    iget-wide v8, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->bufOffset:J

    sub-long/2addr v6, v8

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " from cache file "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->fileBuf:Ljava/io/File;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", now bytesRemaining data "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->childChunkStart:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "-"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->childChunkEnd:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " will be read from child input "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->childChunkInput:Ljava/io/InputStream;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1371
    :cond_10
    invoke-direct {p0}, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->closeAndDelFile()V

    .line 1372
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->bSwitchedToChild:Z

    .line 1374
    :cond_11
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_12

    .line 1375
    iget-wide v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->readOffset:J

    iget-wide v6, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->childChunkStart:J

    cmp-long v4, v4, v6

    if-nez v4, :cond_12

    iget-object v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->childChunkInput:Ljava/io/InputStream;

    if-eqz v4, :cond_12

    .line 1376
    iget-object v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "now start to read from childChunkInput "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->childChunkInput:Ljava/io/InputStream;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    # invokes: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->log(Ljava/lang/String;)V
    invoke-static {v4, v5}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$000(Lcom/android/okhttp/internal/http/TwoChunkInputStream;Ljava/lang/String;)V

    .line 1379
    :cond_12
    iget-object v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->childChunkInput:Ljava/io/InputStream;

    instance-of v4, v4, Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    if-eqz v4, :cond_13

    iget-object v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->childChunkInput:Ljava/io/InputStream;

    check-cast v4, Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    invoke-virtual {v4}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->isClosed()Z

    move-result v4

    if-eqz v4, :cond_13

    .line 1380
    iget-object v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "childChunkInput is closed : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->childChunkInput:Ljava/io/InputStream;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    # invokes: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->log(Ljava/lang/String;)V
    invoke-static {v4, v5}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$000(Lcom/android/okhttp/internal/http/TwoChunkInputStream;Ljava/lang/String;)V

    .line 1381
    const/4 v4, -0x1

    goto/16 :goto_1

    .line 1383
    :cond_13
    iget-object v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->childChunkInput:Ljava/io/InputStream;

    invoke-virtual {v4, p1, p2, p3}, Ljava/io/InputStream;->read([BII)I
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    move-result v3

    goto/16 :goto_2

    .line 1391
    :catch_1
    move-exception v2

    .line 1392
    .restart local v2    # "e":Ljava/lang/Throwable;
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_14

    iget-object v4, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$100(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V

    .line 1393
    :cond_14
    invoke-direct {p0}, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->closeAndDelFile()V

    .line 1394
    const/4 v3, 0x0

    goto/16 :goto_0
.end method
