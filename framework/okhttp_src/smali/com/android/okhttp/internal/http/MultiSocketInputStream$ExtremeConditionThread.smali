.class Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;
.super Ljava/lang/Object;
.source "MultiSocketInputStream.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/okhttp/internal/http/MultiSocketInputStream;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ExtremeConditionThread"
.end annotation


# instance fields
.field private bDeprecated:Z

.field private bIsSocketWorkingFine:Z

.field private bTryBoth:Z

.field dBuf:Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

.field end:J

.field private input:Ljava/io/InputStream;

.field isExceptionInResponse:Z

.field protected mNonBufOffset:J

.field private mSecThreadCreationResult:I

.field private mStartReadHeaderTime:J

.field protected mTotalReceivedBytes:J

.field private output:Ljava/io/OutputStream;

.field private s:Ljava/net/Socket;

.field private sockID:I

.field start:J

.field private switchToSocketID:I

.field final synthetic this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;


# direct methods
.method public constructor <init>(Lcom/android/okhttp/internal/http/MultiSocketInputStream;IJJLcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;)V
    .locals 7
    .param p2, "socketID"    # I
    .param p3, "startOffset"    # J
    .param p5, "endOffset"    # J
    .param p7, "buf"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

    .prologue
    const-wide/16 v4, 0x0

    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 4603
    iput-object p1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4587
    iput-object v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->output:Ljava/io/OutputStream;

    .line 4588
    iput-object v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->input:Ljava/io/InputStream;

    .line 4589
    iput-object v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->s:Ljava/net/Socket;

    .line 4590
    iput-boolean v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->bTryBoth:Z

    .line 4591
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->mStartReadHeaderTime:J

    .line 4592
    iput-boolean v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->bDeprecated:Z

    .line 4593
    const/16 v0, -0x64

    iput v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->mSecThreadCreationResult:I

    .line 4594
    iput-wide v4, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->mTotalReceivedBytes:J

    .line 4595
    iput-wide v4, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->mNonBufOffset:J

    .line 4598
    iput-boolean v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->bIsSocketWorkingFine:Z

    .line 4604
    iput p2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->sockID:I

    .line 4605
    iput-wide p3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->start:J

    .line 4606
    iput-wide p5, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->end:J

    .line 4607
    iput-object p7, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->dBuf:Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

    .line 4608
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->isExceptionInResponse:Z

    .line 4609
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->switchToSocketID:I

    .line 4610
    return-void
.end method

.method static synthetic access$5202(Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;
    .param p1, "x1"    # Z

    .prologue
    .line 4563
    iput-boolean p1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->bDeprecated:Z

    return p1
.end method

.method static synthetic access$5300(Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;

    .prologue
    .line 4563
    iget-boolean v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->bIsSocketWorkingFine:Z

    return v0
.end method

.method static synthetic access$6200(Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;)I
    .locals 1
    .param p0, "x0"    # Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;

    .prologue
    .line 4563
    iget v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->mSecThreadCreationResult:I

    return v0
.end method


# virtual methods
.method protected closeSocketAndStreams()V
    .locals 4

    .prologue
    .line 5034
    sget-boolean v0, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v0}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v0

    new-instance v1, Ljava/lang/Throwable;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "try to close extreme input stream in main, input is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->input:Ljava/io/InputStream;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V

    .line 5035
    :cond_0
    iget-object v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->input:Ljava/io/InputStream;

    invoke-static {v0}, Lcom/android/okhttp/internal/http/MultiratUtil;->closeQuietly(Ljava/io/Closeable;)V

    .line 5036
    iget-object v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->output:Ljava/io/OutputStream;

    invoke-static {v0}, Lcom/android/okhttp/internal/http/MultiratUtil;->closeQuietly(Ljava/io/Closeable;)V

    .line 5037
    iget-object v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->s:Ljava/net/Socket;

    invoke-static {v0}, Lcom/android/okhttp/internal/http/MultiratUtil;->closeQuietly(Ljava/net/Socket;)V

    .line 5038
    return-void
.end method

.method public createTwoChunkInput(Z)V
    .locals 29
    .param p1, "bReverseSock"    # Z

    .prologue
    .line 4633
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-boolean v3, v3, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bFinished:Z

    if-nez v3, :cond_0

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->bDeprecated:Z

    if-eqz v3, :cond_2

    .line 4634
    :cond_0
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v3

    const-string v4, "createTwoChunkInput: session is finished"

    invoke-virtual {v3, v4}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 4681
    :cond_1
    :goto_0
    return-void

    .line 4637
    :cond_2
    monitor-enter p0

    .line 4638
    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesRemaining:J
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$700(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesForMultiSocket:J
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1500(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v10

    cmp-long v3, v4, v10

    if-gtz v3, :cond_7

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    move-result-object v3

    if-nez v3, :cond_7

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bStopSecDecisionMade:Z
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$4300(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Z

    move-result v3

    if-nez v3, :cond_7

    .line 4640
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Main Socket is finished while mRemainBytes="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesRemaining:J
    invoke-static {v5}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$700(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v10

    invoke-virtual {v4, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", extrem read bytes"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 4643
    :cond_3
    if-eqz p1, :cond_4

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->mStartReadHeaderTime:J

    const-wide/16 v10, 0x0

    cmp-long v3, v4, v10

    if-lez v3, :cond_7

    .line 4644
    :cond_4
    move-object/from16 v0, p0

    iget v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->sockID:I

    .line 4645
    .local v7, "sock0":I
    const-wide/16 v14, 0x0

    .line 4646
    .local v14, "speed0":J
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    invoke-virtual {v3, v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->getSpeed(I)J

    move-result-wide v14

    .line 4647
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->sockID:I

    add-int/lit8 v3, v3, 0x1

    rem-int/lit8 v8, v3, 0x2

    .line 4648
    .local v8, "sock1":I
    const-wide/16 v16, 0x0

    .line 4649
    .local v16, "speed1":J
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    invoke-virtual {v3, v8}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->getSpeed(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v16

    .line 4651
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v28, v0

    new-instance v3, Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->input:Ljava/io/InputStream;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-object v5, v5, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->cr:Ljava/net/CacheRequest;
    invoke-static {v6}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$6400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Ljava/net/CacheRequest;

    move-result-object v6

    move-object/from16 v0, p0

    iget-wide v9, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->start:J

    move-object/from16 v0, p0

    iget-wide v11, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->end:J

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-object v13, v13, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mOriginalRequestHeader:Lcom/android/okhttp/Request;

    const/16 v18, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v20, v0

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mFullConSize:J
    invoke-static/range {v20 .. v20}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2500(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v20

    sget v22, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->WAIT_FOR_INTERFACE_TIME_OUT:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mRAdownloadedFile:Ljava/io/RandomAccessFile;

    move-object/from16 v23, v0

    if-nez v23, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->dBuf:Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->getHasReadLen()J

    move-result-wide v24

    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v23, v0

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static/range {v23 .. v23}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v23, v0

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->connection:Lcom/android/okhttp/Connection;
    invoke-static/range {v23 .. v23}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/Connection;

    move-result-object v27

    move/from16 v23, p1

    invoke-direct/range {v3 .. v27}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;-><init>(Ljava/io/InputStream;Lcom/android/okhttp/internal/http/HttpEngine;Ljava/net/CacheRequest;IIJJLcom/android/okhttp/Request;JJILcom/android/okhttp/internal/http/MultiSocketInputStream;JIZJLcom/android/okhttp/internal/http/MultiratLog;Lcom/android/okhttp/Connection;)V

    move-object/from16 v0, v28

    # setter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;
    invoke-static {v0, v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3902(Lcom/android/okhttp/internal/http/MultiSocketInputStream;Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    .line 4655
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->getNeedToExitSecThread()I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->mSecThreadCreationResult:I

    .line 4656
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->start:J

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->mNonBufOffset:J

    .line 4657
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->isSingleThreadRun()Z

    move-result v4

    invoke-virtual {v3, v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->startSingleThreadRunning(Z)V

    .line 4658
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->mSecThreadCreationResult:I

    if-nez v3, :cond_a

    .line 4659
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->bDeprecated:Z

    .line 4660
    if-eqz p1, :cond_9

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->mStartReadHeaderTime:J

    const-wide/16 v10, 0x0

    cmp-long v3, v4, v10

    if-lez v3, :cond_9

    .line 4661
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_5

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v3

    const-string v4, "Need to start another Extreme Input"

    invoke-virtual {v3, v4}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 4662
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->closeSocketAndStreams()V

    .line 4668
    :cond_6
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->dBuf:Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    move-result-object v19

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->start:J

    move-wide/from16 v20, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->end:J

    move-wide/from16 v22, v0

    invoke-virtual/range {v18 .. v23}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->push(Ljava/io/InputStream;JJ)V

    .line 4669
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->s:Ljava/net/Socket;

    invoke-virtual {v3, v4}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->startRun(Ljava/net/Socket;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 4680
    .end local v7    # "sock0":I
    .end local v8    # "sock1":I
    .end local v14    # "speed0":J
    .end local v16    # "speed1":J
    :cond_7
    :goto_3
    :try_start_2
    monitor-exit p0

    goto/16 :goto_0

    :catchall_0
    move-exception v3

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3

    .line 4651
    .restart local v7    # "sock0":I
    .restart local v8    # "sock1":I
    .restart local v14    # "speed0":J
    .restart local v16    # "speed1":J
    :cond_8
    const-wide/16 v24, 0x0

    goto/16 :goto_1

    .line 4665
    :cond_9
    :try_start_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->input:Ljava/io/InputStream;

    .line 4666
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_6

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "new input stream in create two chunk extremInput = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->input:Ljava/io/InputStream;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    .line 4675
    :catch_0
    move-exception v2

    .line 4676
    .local v2, "e":Ljava/lang/Throwable;
    :try_start_4
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_7

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_3

    .line 4672
    .end local v2    # "e":Ljava/lang/Throwable;
    :cond_a
    :try_start_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    const/4 v4, 0x0

    # setter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;
    invoke-static {v3, v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3902(Lcom/android/okhttp/internal/http/MultiSocketInputStream;Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/TwoChunkInputStream;
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_3
.end method

.method public getSocketID()I
    .locals 1

    .prologue
    .line 4613
    iget v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->sockID:I

    return v0
.end method

.method public run()V
    .locals 34

    .prologue
    .line 4686
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "resource check: thread checking: start ExtremThread "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Thread;->getId()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", start to download with socket "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->sockID:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", bytes: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->start:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " - "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->end:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 4687
    :cond_0
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->end:J

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->start:J

    sub-long/2addr v4, v6

    const-wide/16 v6, 0x1

    add-long v20, v4, v6

    .line 4688
    .local v20, "bytesToRead":J
    const-wide/16 v30, 0x0

    .line 4689
    .local v30, "totallen":J
    :goto_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-boolean v3, v3, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bFinished:Z

    if-nez v3, :cond_4

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->bDeprecated:Z

    if-nez v3, :cond_4

    cmp-long v3, v30, v20

    if-gez v3, :cond_4

    .line 4692
    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->mStartReadHeaderTime:J

    .line 4693
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->bIsSocketWorkingFine:Z

    .line 4694
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->switchToSocketID:I

    if-ltz v3, :cond_1

    .line 4695
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->switchToSocketID:I

    move-object/from16 v0, p0

    iput v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->sockID:I

    .line 4696
    const/4 v3, -0x1

    move-object/from16 v0, p0

    iput v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->switchToSocketID:I

    .line 4698
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v28

    .line 4699
    .local v28, "sTime":J
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    move-result-object v3

    if-nez v3, :cond_15

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->bDeprecated:Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_6

    if-nez v3, :cond_15

    .line 4701
    :try_start_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->connection:Lcom/android/okhttp/Connection;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/Connection;

    move-result-object v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->sockID:I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-object v5, v5, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mOriginalRequestHeader:Lcom/android/okhttp/Request;

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->start:J

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->end:J

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mFullConSize:J
    invoke-static {v10}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2500(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v10

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->bTryBoth:Z

    const/4 v14, 0x1

    invoke-virtual/range {v3 .. v14}, Lcom/android/okhttp/Connection;->extremeConditionConnect(ILcom/android/okhttp/Request;JJJLcom/android/okhttp/internal/http/MultiSocketInputStream;ZZ)Ljava/net/Socket;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->s:Ljava/net/Socket;

    .line 4703
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->s:Ljava/net/Socket;

    if-nez v3, :cond_d

    .line 4704
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    const/4 v4, 0x2

    # setter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->failReasonInExtreme:I
    invoke-static {v3, v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3702(Lcom/android/okhttp/internal/http/MultiSocketInputStream;I)I

    .line 4705
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->bDeprecated:Z

    .line 4706
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    const/4 v4, 0x1

    # setter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bRangeRequestSuccess:I
    invoke-static {v3, v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2402(Lcom/android/okhttp/internal/http/MultiSocketInputStream;I)I

    .line 4707
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v3

    const-string v4, "unexpected HTTP response in ExtremThread"

    invoke-virtual {v3, v4}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_6

    .line 5002
    :cond_2
    monitor-enter p0

    .line 5003
    :try_start_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    move-result-object v3

    if-nez v3, :cond_3

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->bDeprecated:Z

    if-nez v3, :cond_3

    .line 5004
    invoke-virtual/range {p0 .. p0}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->closeSocketAndStreams()V

    .line 5005
    :cond_3
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 5009
    .end local v28    # "sTime":J
    :cond_4
    :goto_1
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->bDeprecated:Z

    if-nez v3, :cond_6

    .line 5010
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->dBuf:Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

    monitor-enter v4

    .line 5011
    :try_start_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    const/4 v5, 0x0

    # setter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bExtremThreadExisted:Z
    invoke-static {v3, v5}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2302(Lcom/android/okhttp/internal/http/MultiSocketInputStream;Z)Z

    .line 5012
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->dBuf:Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

    invoke-virtual {v3}, Ljava/lang/Object;->notifyAll()V

    .line 5013
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-boolean v3, v3, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bFinished:Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_12

    if-nez v3, :cond_5

    .line 5015
    :try_start_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->dBuf:Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

    const-wide/16 v6, 0x7d0

    invoke-virtual {v3, v6, v7}, Ljava/lang/Object;->wait(J)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_5
    .catchall {:try_start_4 .. :try_end_4} :catchall_12

    .line 5021
    :cond_5
    :goto_2
    :try_start_5
    monitor-exit v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_12

    .line 5022
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-boolean v3, v3, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bFinished:Z

    if-nez v3, :cond_6

    .line 5023
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->createTwoChunkInputInMain(Z)V

    .line 5027
    :cond_6
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_7

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "resource check: thread checking: stop ExtremThread "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Thread;->getId()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", exit from socket "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->sockID:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 5028
    :cond_7
    return-void

    .line 5005
    .restart local v28    # "sTime":J
    :catchall_0
    move-exception v3

    :try_start_6
    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    throw v3

    .line 4711
    :catch_0
    move-exception v17

    .line 4712
    .local v17, "e":Ljava/lang/Exception;
    :try_start_7
    monitor-enter p0
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_6

    .line 4713
    :try_start_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    move-result-object v3

    if-nez v3, :cond_b

    .line 4714
    throw v17

    .line 4718
    :catchall_1
    move-exception v3

    monitor-exit p0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    :try_start_9
    throw v3
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_6

    .line 4956
    .end local v17    # "e":Ljava/lang/Exception;
    .end local v28    # "sTime":J
    :catch_1
    move-exception v25

    .line 4957
    .local v25, "t":Ljava/lang/Throwable;
    const/4 v3, 0x0

    :try_start_a
    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->bIsSocketWorkingFine:Z

    .line 4958
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_8

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v3

    move-object/from16 v0, v25

    invoke-virtual {v3, v0}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V

    .line 4959
    :cond_8
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->isExceptionInResponse:Z

    if-eqz v3, :cond_44

    .line 4960
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bRangeRequestSuccess:I
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v3

    if-eqz v3, :cond_9

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bRangeRequestSuccess:I
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v3

    const/4 v4, 0x3

    if-ne v3, v4, :cond_46

    .line 4961
    :cond_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    const/4 v4, 0x0

    # setter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bReadFromBuffer:Z
    invoke-static {v3, v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$4402(Lcom/android/okhttp/internal/http/MultiSocketInputStream;Z)Z

    .line 4962
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    const/4 v4, 0x0

    # setter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bMultiSocketStarted:Z
    invoke-static {v3, v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3102(Lcom/android/okhttp/internal/http/MultiSocketInputStream;Z)Z

    .line 4963
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    const/4 v4, 0x0

    # setter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bExtremThreadStarted:Z
    invoke-static {v3, v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2202(Lcom/android/okhttp/internal/http/MultiSocketInputStream;Z)Z

    .line 4964
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    const/4 v4, 0x3

    # setter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->failReasonInExtreme:I
    invoke-static {v3, v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3702(Lcom/android/okhttp/internal/http/MultiSocketInputStream;I)I

    .line 4965
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    const/4 v4, 0x1

    # setter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bRangeRequestSuccess:I
    invoke-static {v3, v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2402(Lcom/android/okhttp/internal/http/MultiSocketInputStream;I)I

    .line 4966
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mBlockManager:Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;

    move-result-object v4

    monitor-enter v4
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_6

    .line 4967
    :try_start_b
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mBlockManager:Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->notifyAll()V

    .line 4968
    monitor-exit v4
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_d

    .line 5002
    monitor-enter p0

    .line 5003
    :try_start_c
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    move-result-object v3

    if-nez v3, :cond_a

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->bDeprecated:Z

    if-nez v3, :cond_a

    .line 5004
    invoke-virtual/range {p0 .. p0}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->closeSocketAndStreams()V

    .line 5005
    :cond_a
    monitor-exit p0

    goto/16 :goto_1

    :catchall_2
    move-exception v3

    monitor-exit p0
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    throw v3

    .line 4716
    .end local v25    # "t":Ljava/lang/Throwable;
    .restart local v17    # "e":Ljava/lang/Exception;
    .restart local v28    # "sTime":J
    :cond_b
    :try_start_d
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_c

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v3

    const-string v4, "childIS is created before this exception"

    invoke-virtual {v3, v4}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 4718
    :cond_c
    monitor-exit p0
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    .line 4721
    .end local v17    # "e":Ljava/lang/Exception;
    :cond_d
    :try_start_e
    monitor-enter p0
    :try_end_e
    .catch Ljava/lang/Throwable; {:try_start_e .. :try_end_e} :catch_1
    .catchall {:try_start_e .. :try_end_e} :catchall_6

    .line 4722
    const-wide/16 v4, -0x1

    :try_start_f
    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->mStartReadHeaderTime:J

    .line 4723
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->bDeprecated:Z

    if-eqz v3, :cond_10

    .line 4724
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_e

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v3

    const-string v4, "thread is deprecated, break"

    invoke-virtual {v3, v4}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 4725
    :cond_e
    monitor-exit p0
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_5

    .line 5002
    monitor-enter p0

    .line 5003
    :try_start_10
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    move-result-object v3

    if-nez v3, :cond_f

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->bDeprecated:Z

    if-nez v3, :cond_f

    .line 5004
    invoke-virtual/range {p0 .. p0}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->closeSocketAndStreams()V

    .line 5005
    :cond_f
    monitor-exit p0

    goto/16 :goto_1

    :catchall_3
    move-exception v3

    monitor-exit p0
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_3

    throw v3

    .line 4727
    :cond_10
    :try_start_11
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    move-result-object v3

    if-nez v3, :cond_1a

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->bDeprecated:Z

    if-nez v3, :cond_1a

    .line 4728
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->bTryBoth:Z

    if-eqz v3, :cond_11

    .line 4729
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->s:Ljava/net/Socket;

    invoke-virtual {v3}, Ljava/net/Socket;->getLocalAddress()Ljava/net/InetAddress;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->connection:Lcom/android/okhttp/Connection;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/Connection;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/android/okhttp/Connection;->getLocalAddr(I)Ljava/net/InetAddress;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/net/InetAddress;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_18

    .line 4730
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->sockID:I

    .line 4737
    :cond_11
    :goto_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->s:Ljava/net/Socket;

    const/16 v4, 0x1388

    invoke-virtual {v3, v4}, Ljava/net/Socket;->setSoTimeout(I)V

    .line 4739
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->s:Ljava/net/Socket;

    invoke-virtual {v3}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->output:Ljava/io/OutputStream;

    .line 4740
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->s:Ljava/net/Socket;

    invoke-virtual {v3}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->input:Ljava/io/InputStream;

    .line 4741
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_12

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "new input stream in after reconnect extremInput = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->input:Ljava/io/InputStream;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 4742
    :cond_12
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->isExceptionInResponse:Z

    .line 4744
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bRangeRequestSuccess:I
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v3

    if-eqz v3, :cond_13

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bRangeRequestSuccess:I
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v3

    const/4 v4, 0x3

    if-ne v3, v4, :cond_14

    .line 4745
    :cond_13
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    const/4 v4, 0x4

    # setter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bRangeRequestSuccess:I
    invoke-static {v3, v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2402(Lcom/android/okhttp/internal/http/MultiSocketInputStream;I)I

    .line 4746
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mBlockManager:Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;

    move-result-object v4

    monitor-enter v4
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_5

    .line 4747
    :try_start_12
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mBlockManager:Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$800(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream$BlockManager;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->notifyAll()V

    .line 4748
    monitor-exit v4
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_7

    .line 4756
    :cond_14
    :goto_4
    :try_start_13
    monitor-exit p0
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_5

    .line 4759
    :cond_15
    :try_start_14
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->bDeprecated:Z

    if-eqz v3, :cond_1c

    .line 4760
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_16

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v3

    const-string v4, "thread is deprecated2, break"

    invoke-virtual {v3, v4}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V
    :try_end_14
    .catch Ljava/lang/Throwable; {:try_start_14 .. :try_end_14} :catch_1
    .catchall {:try_start_14 .. :try_end_14} :catchall_6

    .line 5002
    :cond_16
    monitor-enter p0

    .line 5003
    :try_start_15
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    move-result-object v3

    if-nez v3, :cond_17

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->bDeprecated:Z

    if-nez v3, :cond_17

    .line 5004
    invoke-virtual/range {p0 .. p0}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->closeSocketAndStreams()V

    .line 5005
    :cond_17
    monitor-exit p0

    goto/16 :goto_1

    :catchall_4
    move-exception v3

    monitor-exit p0
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_4

    throw v3

    .line 4733
    :cond_18
    const/4 v3, 0x1

    :try_start_16
    move-object/from16 v0, p0

    iput v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->sockID:I

    goto/16 :goto_3

    .line 4756
    :catchall_5
    move-exception v3

    monitor-exit p0
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_5

    :try_start_17
    throw v3
    :try_end_17
    .catch Ljava/lang/Throwable; {:try_start_17 .. :try_end_17} :catch_1
    .catchall {:try_start_17 .. :try_end_17} :catchall_6

    .line 5002
    .end local v28    # "sTime":J
    :catchall_6
    move-exception v3

    monitor-enter p0

    .line 5003
    :try_start_18
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    move-result-object v4

    if-nez v4, :cond_19

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->bDeprecated:Z

    if-nez v4, :cond_19

    .line 5004
    invoke-virtual/range {p0 .. p0}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->closeSocketAndStreams()V

    .line 5005
    :cond_19
    monitor-exit p0
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_11

    throw v3

    .line 4748
    .restart local v28    # "sTime":J
    :catchall_7
    move-exception v3

    :try_start_19
    monitor-exit v4
    :try_end_19
    .catchall {:try_start_19 .. :try_end_19} :catchall_7

    :try_start_1a
    throw v3

    .line 4753
    :cond_1a
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_1b

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v3

    const-string v4, "childIS is created before this connection"

    invoke-virtual {v3, v4}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 4754
    :cond_1b
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->bDeprecated:Z
    :try_end_1a
    .catchall {:try_start_1a .. :try_end_1a} :catchall_5

    goto :goto_4

    .line 4764
    :cond_1c
    :try_start_1b
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-object v3, v3, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->connectionTime:[J

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->sockID:I

    aget-wide v4, v3, v4

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-nez v3, :cond_1d

    .line 4765
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-object v3, v3, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->connectionTime:[J

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->sockID:I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v6, v6, v28

    aput-wide v6, v3, v4

    .line 4766
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_1d

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Socket["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->sockID:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "connection time is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-object v5, v5, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->connectionTime:[J

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->sockID:I

    aget-wide v6, v5, v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 4768
    :cond_1d
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mStartReadTime:[J
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$4500(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[J

    move-result-object v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->sockID:I

    aget-wide v4, v3, v4

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-nez v3, :cond_1e

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mStartReadTime:[J
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$4500(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[J

    move-result-object v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->sockID:I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    aput-wide v6, v3, v4

    .line 4771
    :cond_1e
    const/16 v16, 0x0

    .line 4772
    .local v16, "buf_offset":I
    const-wide/16 v18, 0x0

    .line 4775
    .local v18, "buf_ret":J
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->bDeprecated:Z

    if-nez v3, :cond_1f

    .line 4776
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->dBuf:Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

    monitor-enter v4
    :try_end_1b
    .catch Ljava/lang/Throwable; {:try_start_1b .. :try_end_1b} :catch_1
    .catchall {:try_start_1b .. :try_end_1b} :catchall_6

    .line 4777
    :try_start_1c
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->dBuf:Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

    invoke-virtual {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->startReceiveData()V

    .line 4778
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->dBuf:Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->sockID:I

    invoke-virtual {v3, v5}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->changeSockID(I)V

    .line 4779
    monitor-exit v4
    :try_end_1c
    .catchall {:try_start_1c .. :try_end_1c} :catchall_9

    .line 4786
    :cond_1f
    :try_start_1d
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_20

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Extreme Thread starts to read data from "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->start:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " to "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->end:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 4787
    :cond_20
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mSecChunkStart:J
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$6500(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-gez v3, :cond_21

    .line 4788
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->start:J

    # setter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mSecChunkStart:J
    invoke-static {v3, v4, v5}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$6502(Lcom/android/okhttp/internal/http/MultiSocketInputStream;J)J

    .line 4789
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->start:J

    # setter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mSecChunkEnd:J
    invoke-static {v3, v4, v5}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$6602(Lcom/android/okhttp/internal/http/MultiSocketInputStream;J)J

    .line 4791
    :cond_21
    sget v23, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->INIT_BUFFERLEN:I

    .line 4792
    .local v23, "iRealBlockSize":I
    sget v3, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->MAX_BUFFERLEN:I

    new-array v15, v3, [B

    .line 4793
    .local v15, "buf":[B
    :cond_22
    cmp-long v3, v30, v20

    if-gez v3, :cond_23

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-boolean v3, v3, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bFinished:Z

    if-nez v3, :cond_23

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->bDeprecated:Z

    if-nez v3, :cond_23

    .line 4794
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->mSecThreadCreationResult:I

    const/16 v4, -0x64

    if-ne v3, v4, :cond_27

    .line 4796
    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->createTwoChunkInput(Z)V

    .line 4797
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->bDeprecated:Z

    if-eqz v3, :cond_26

    .line 4798
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_23

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Extreme Thread deprecated at offset "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->start:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 4954
    :cond_23
    :goto_5
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_24

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Speed of current socket is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->sockID:I

    invoke-virtual {v5, v6}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->getSpeed(I)J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " for content size "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v30

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V
    :try_end_1d
    .catch Ljava/lang/Throwable; {:try_start_1d .. :try_end_1d} :catch_1
    .catchall {:try_start_1d .. :try_end_1d} :catchall_6

    .line 5002
    :cond_24
    monitor-enter p0

    .line 5003
    :try_start_1e
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    move-result-object v3

    if-nez v3, :cond_25

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->bDeprecated:Z

    if-nez v3, :cond_25

    .line 5004
    invoke-virtual/range {p0 .. p0}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->closeSocketAndStreams()V

    .line 5005
    :cond_25
    monitor-exit p0

    goto/16 :goto_0

    :catchall_8
    move-exception v3

    monitor-exit p0
    :try_end_1e
    .catchall {:try_start_1e .. :try_end_1e} :catchall_8

    throw v3

    .line 4779
    .end local v15    # "buf":[B
    .end local v23    # "iRealBlockSize":I
    :catchall_9
    move-exception v3

    :try_start_1f
    monitor-exit v4
    :try_end_1f
    .catchall {:try_start_1f .. :try_end_1f} :catchall_9

    :try_start_20
    throw v3

    .line 4803
    .restart local v15    # "buf":[B
    .restart local v23    # "iRealBlockSize":I
    :cond_26
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->mSecThreadCreationResult:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_27

    .line 4804
    invoke-virtual/range {p0 .. p0}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->closeSocketAndStreams()V

    .line 4805
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->sockID:I

    add-int/lit8 v3, v3, 0x1

    rem-int/lit8 v3, v3, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->switchSocket(I)Z

    goto :goto_5

    .line 4810
    :cond_27
    move/from16 v0, v23

    int-to-long v4, v0

    sub-long v6, v20, v30

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    long-to-int v2, v4

    .line 4812
    .local v2, "bsize":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->dBuf:Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

    if-eqz v3, :cond_28

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->dBuf:Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

    invoke-virtual {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->isBufferInFile()Z

    move-result v3

    if-nez v3, :cond_28

    .line 4813
    new-array v15, v2, [B

    .line 4814
    :cond_28
    sget v3, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->MAX_BUFFERLEN:I

    int-to-long v4, v3

    shl-int/lit8 v3, v23, 0x1

    int-to-long v6, v3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->BlockSize:J
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v8

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v6

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->min(JJ)J
    :try_end_20
    .catch Ljava/lang/Throwable; {:try_start_20 .. :try_end_20} :catch_1
    .catchall {:try_start_20 .. :try_end_20} :catchall_6

    move-result-wide v4

    long-to-int v0, v4

    move/from16 v23, v0

    .line 4815
    const/16 v16, 0x0

    .line 4816
    const-wide/16 v18, 0x0

    .line 4817
    const/16 v24, 0x0

    .line 4820
    .local v24, "len":I
    :try_start_21
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-object v4, v3, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->lockAppReading:Ljava/lang/Object;

    monitor-enter v4
    :try_end_21
    .catch Ljava/lang/Throwable; {:try_start_21 .. :try_end_21} :catch_3
    .catchall {:try_start_21 .. :try_end_21} :catchall_6

    .line 4821
    :cond_29
    :goto_6
    :try_start_22
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-boolean v3, v3, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bAppReading:Z

    if-nez v3, :cond_2e

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-boolean v3, v3, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bFinished:Z

    if-nez v3, :cond_2e

    .line 4822
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_2a

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v3

    const-string v5, "APP not reading, waiting"

    invoke-virtual {v3, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V
    :try_end_22
    .catchall {:try_start_22 .. :try_end_22} :catchall_a

    .line 4824
    :cond_2a
    :try_start_23
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-object v3, v3, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->lockAppReading:Ljava/lang/Object;

    const-wide/16 v6, 0x3e8

    invoke-virtual {v3, v6, v7}, Ljava/lang/Object;->wait(J)V
    :try_end_23
    .catch Ljava/lang/Throwable; {:try_start_23 .. :try_end_23} :catch_2
    .catchall {:try_start_23 .. :try_end_23} :catchall_a

    goto :goto_6

    .line 4826
    :catch_2
    move-exception v17

    .line 4827
    .local v17, "e":Ljava/lang/Throwable;
    :try_start_24
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_29

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V

    goto :goto_6

    .line 4830
    .end local v17    # "e":Ljava/lang/Throwable;
    :catchall_a
    move-exception v3

    monitor-exit v4
    :try_end_24
    .catchall {:try_start_24 .. :try_end_24} :catchall_a

    :try_start_25
    throw v3
    :try_end_25
    .catch Ljava/lang/Throwable; {:try_start_25 .. :try_end_25} :catch_3
    .catchall {:try_start_25 .. :try_end_25} :catchall_6

    .line 4864
    :catch_3
    move-exception v22

    .line 4865
    .local v22, "ex":Ljava/lang/Throwable;
    const/4 v3, 0x0

    :try_start_26
    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->bIsSocketWorkingFine:Z

    .line 4866
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-object v3, v3, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->disconnCount:[I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->sockID:I

    aget v5, v3, v4

    add-int/lit8 v5, v5, 0x1

    aput v5, v3, v4

    .line 4867
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_2b

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SocketTimeoutException Count: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-object v5, v5, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->disconnCount:[I

    const/4 v6, 0x0

    aget v5, v5, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-object v5, v5, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->disconnCount:[I

    const/4 v6, 0x1

    aget v5, v5, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 4868
    :cond_2b
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_2c

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception in getting block: buf_offset="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", buf.length="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    array-length v5, v15

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", len:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v24

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", bFinished="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-boolean v5, v5, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bFinished:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 4869
    :cond_2c
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_2d

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v3

    move-object/from16 v0, v22

    invoke-virtual {v3, v0}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V

    .line 4870
    :cond_2d
    throw v22
    :try_end_26
    .catch Ljava/lang/Throwable; {:try_start_26 .. :try_end_26} :catch_1
    .catchall {:try_start_26 .. :try_end_26} :catchall_6

    .line 4830
    .end local v22    # "ex":Ljava/lang/Throwable;
    :cond_2e
    :try_start_27
    monitor-exit v4
    :try_end_27
    .catchall {:try_start_27 .. :try_end_27} :catchall_a

    .line 4832
    const/4 v3, 0x1

    :try_start_28
    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->bIsSocketWorkingFine:Z

    .line 4834
    :cond_2f
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->bDeprecated:Z

    if-nez v3, :cond_32

    array-length v3, v15

    move/from16 v0, v16

    if-ge v0, v3, :cond_32

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->input:Ljava/io/InputStream;

    array-length v4, v15

    sub-int v4, v4, v16

    move/from16 v0, v16

    invoke-virtual {v3, v15, v0, v4}, Ljava/io/InputStream;->read([BII)I

    move-result v24

    if-lez v24, :cond_32

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    iget-boolean v3, v3, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bFinished:Z

    if-nez v3, :cond_32

    .line 4835
    add-int v16, v16, v24

    .line 4836
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->mTotalReceivedBytes:J

    move/from16 v0, v24

    int-to-long v6, v0

    add-long/2addr v4, v6

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->mTotalReceivedBytes:J

    .line 4837
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_30

    .line 4840
    :cond_30
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    move-result-object v3

    if-nez v3, :cond_31

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->bDeprecated:Z

    if-nez v3, :cond_31

    .line 4841
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->sockID:I

    invoke-virtual {v3, v4}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->setTime(I)V

    .line 4842
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->sockID:I

    move/from16 v0, v24

    int-to-long v6, v0

    invoke-virtual {v3, v4, v6, v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->incByte(IJ)V

    .line 4845
    :cond_31
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesRemaining:J
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$700(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesForMultiSocket:J
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1500(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-gtz v3, :cond_35

    .line 4846
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_32

    .line 4847
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->mSecThreadCreationResult:I

    const/16 v4, -0x64

    if-ne v3, v4, :cond_32

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bStopSecDecisionMade:Z
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$4300(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Z

    move-result v3

    if-nez v3, :cond_32

    .line 4848
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Now it is time to stop this thread, "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesRemaining:J
    invoke-static {v5}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$700(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "<="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bytesForMultiSocket:J
    invoke-static {v5}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1500(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", buf_offset:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/android/okhttp/internal/http/MultiratLog;->d(Ljava/lang/String;)V

    .line 4862
    :cond_32
    :goto_7
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z
    :try_end_28
    .catch Ljava/lang/Throwable; {:try_start_28 .. :try_end_28} :catch_3
    .catchall {:try_start_28 .. :try_end_28} :catchall_6

    if-eqz v3, :cond_33

    .line 4875
    :cond_33
    if-lez v16, :cond_40

    :try_start_29
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->bDeprecated:Z

    if-nez v3, :cond_40

    .line 4876
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->dBuf:Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

    monitor-enter v4
    :try_end_29
    .catch Ljava/lang/Throwable; {:try_start_29 .. :try_end_29} :catch_1
    .catchall {:try_start_29 .. :try_end_29} :catchall_6

    .line 4877
    :try_start_2a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->dBuf:Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->sockID:I

    move/from16 v0, v16

    invoke-virtual {v3, v5, v15, v0}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->push(I[BI)J

    move-result-wide v18

    .line 4879
    const-wide/16 v6, -0x1

    cmp-long v3, v18, v6

    if-nez v3, :cond_38

    .line 4880
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_34

    .line 4881
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v3

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "This block shall be read by another socket, this socket is slow: block[0], socket["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->sockID:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 4884
    :cond_34
    new-instance v3, Ljava/io/IOException;

    const-string v5, "Data is not pushed to buffer may be socket has changed."

    invoke-direct {v3, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 4901
    :catchall_b
    move-exception v3

    monitor-exit v4
    :try_end_2a
    .catchall {:try_start_2a .. :try_end_2a} :catchall_b

    :try_start_2b
    throw v3
    :try_end_2b
    .catch Ljava/lang/Throwable; {:try_start_2b .. :try_end_2b} :catch_1
    .catchall {:try_start_2b .. :try_end_2b} :catchall_6

    .line 4852
    :cond_35
    move/from16 v0, v16

    int-to-long v4, v0

    add-long v4, v4, v30

    cmp-long v3, v4, v20

    if-nez v3, :cond_36

    .line 4853
    :try_start_2c
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_32

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Full data read, break: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "+"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v30

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v20

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    goto/16 :goto_7

    .line 4856
    :cond_36
    move/from16 v0, v16

    int-to-long v4, v0

    add-long v4, v4, v30

    cmp-long v3, v4, v20

    if-lez v3, :cond_2f

    .line 4857
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_37

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Full data read, break: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "+"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v30

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ">"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v20

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V
    :try_end_2c
    .catch Ljava/lang/Throwable; {:try_start_2c .. :try_end_2c} :catch_3
    .catchall {:try_start_2c .. :try_end_2c} :catchall_6

    .line 4858
    :cond_37
    sub-long v4, v20, v30

    long-to-int v0, v4

    move/from16 v16, v0

    .line 4859
    goto/16 :goto_7

    .line 4886
    :cond_38
    const-wide/16 v6, 0x0

    cmp-long v3, v18, v6

    if-gez v3, :cond_3a

    .line 4887
    const-wide/16 v6, -0x1

    mul-long v6, v6, v18

    :try_start_2d
    move-object/from16 v0, p0

    iput-wide v6, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->start:J

    .line 4888
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_39

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v3

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "some buffered data is removed from cache file, read again from "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->start:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 4889
    :cond_39
    new-instance v3, Ljava/io/IOException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "some buffered data is removed from cache file, read again from "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->start:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 4892
    :cond_3a
    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->start:J

    add-long v6, v6, v18

    move-object/from16 v0, p0

    iput-wide v6, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->start:J

    .line 4893
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-wide/from16 v0, v18

    # += operator for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mSecChunkEnd:J
    invoke-static {v3, v0, v1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$6614(Lcom/android/okhttp/internal/http/MultiSocketInputStream;J)J
    :try_end_2d
    .catchall {:try_start_2d .. :try_end_2d} :catchall_b

    .line 4894
    add-long v30, v30, v18

    .line 4897
    :try_start_2e
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->dBuf:Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

    invoke-virtual {v3}, Ljava/lang/Object;->notifyAll()V
    :try_end_2e
    .catch Ljava/lang/Throwable; {:try_start_2e .. :try_end_2e} :catch_9
    .catchall {:try_start_2e .. :try_end_2e} :catchall_b

    .line 4901
    :goto_8
    :try_start_2f
    monitor-exit v4
    :try_end_2f
    .catchall {:try_start_2f .. :try_end_2f} :catchall_b

    .line 4902
    :try_start_30
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->dBuf:Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

    monitor-enter v4
    :try_end_30
    .catch Ljava/lang/Throwable; {:try_start_30 .. :try_end_30} :catch_1
    .catchall {:try_start_30 .. :try_end_30} :catchall_6

    .line 4903
    :try_start_31
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->dBuf:Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

    invoke-virtual {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->isBufferInFile()Z

    move-result v3

    if-nez v3, :cond_3e

    .line 4904
    :cond_3b
    :goto_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->dBuf:Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

    invoke-virtual {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->getRestLength()J

    move-result-wide v6

    const-wide/32 v8, 0x10000000

    cmp-long v3, v6, v8

    if-lez v3, :cond_3e

    .line 4905
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_3c

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v3

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "now rest length of the dBuf is over limit: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->dBuf:Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

    invoke-virtual {v6}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->getRestLength()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V
    :try_end_31
    .catchall {:try_start_31 .. :try_end_31} :catchall_c

    .line 4907
    :cond_3c
    :try_start_32
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->dBuf:Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

    const-wide/16 v6, 0x3e8

    invoke-virtual {v3, v6, v7}, Ljava/lang/Object;->wait(J)V
    :try_end_32
    .catch Ljava/lang/Throwable; {:try_start_32 .. :try_end_32} :catch_8
    .catchall {:try_start_32 .. :try_end_32} :catchall_c

    .line 4910
    :goto_a
    const-wide/high16 v26, 0x4059000000000000L    # 100.0

    .line 4911
    .local v26, "ratio":D
    :try_start_33
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mSpeedForEachInterface:[J
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$6700(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[J

    move-result-object v3

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->sockID:I

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v0, p0

    iget v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->sockID:I

    invoke-virtual {v6, v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->getSpeed(I)J

    move-result-wide v6

    aput-wide v6, v3, v5

    .line 4912
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mSpeedForEachInterface:[J
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$6700(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[J

    move-result-object v3

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->sockID:I

    add-int/lit8 v5, v5, 0x1

    rem-int/lit8 v5, v5, 0x2

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v0, p0

    iget v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->sockID:I

    add-int/lit8 v7, v7, 0x1

    rem-int/lit8 v7, v7, 0x2

    invoke-virtual {v6, v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->getSpeed(I)J

    move-result-wide v6

    aput-wide v6, v3, v5

    .line 4913
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mSpeedForEachInterface:[J
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$6700(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[J

    move-result-object v3

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->sockID:I

    add-int/lit8 v5, v5, 0x1

    rem-int/lit8 v5, v5, 0x2

    aget-wide v6, v3, v5

    const-wide/16 v8, 0x0

    cmp-long v3, v6, v8

    if-lez v3, :cond_3d

    .line 4914
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mSpeedForEachInterface:[J
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$6700(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[J

    move-result-object v3

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->sockID:I

    aget-wide v6, v3, v5

    long-to-double v6, v6

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mSpeedForEachInterface:[J
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$6700(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[J

    move-result-object v3

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->sockID:I

    add-int/lit8 v5, v5, 0x1

    rem-int/lit8 v5, v5, 0x2

    aget-wide v8, v3, v5

    long-to-double v8, v8

    div-double v26, v6, v8

    .line 4917
    :cond_3d
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->sockID:I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainSocketInterfaceID:I
    invoke-static {v5}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1300(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)I

    move-result v5

    if-eq v3, v5, :cond_3b

    sget v3, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->SPEED_RATIO_FOR_EXTREME_HO:I

    int-to-double v6, v3

    cmpl-double v3, v26, v6

    if-lez v3, :cond_3b

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bReadFromBuffer:Z
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$4400(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Z

    move-result v3

    if-nez v3, :cond_3b

    .line 4918
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->sockID:I

    # setter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mMainSocketAfterHandover:I
    invoke-static {v3, v5}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$2802(Lcom/android/okhttp/internal/http/MultiSocketInputStream;I)I
    :try_end_33
    .catchall {:try_start_33 .. :try_end_33} :catchall_c

    .line 4921
    :try_start_34
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->connection:Lcom/android/okhttp/Connection;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/Connection;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/okhttp/Connection;->close()V
    :try_end_34
    .catch Ljava/lang/Throwable; {:try_start_34 .. :try_end_34} :catch_7
    .catchall {:try_start_34 .. :try_end_34} :catchall_c

    .line 4926
    :goto_b
    :try_start_35
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_3b

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v3

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Close main socket Speed of current socket in waiting for MEM: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mSpeedForEachInterface:[J
    invoke-static {v6}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$6700(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[J

    move-result-object v6

    move-object/from16 v0, p0

    iget v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->sockID:I

    aget-wide v6, v6, v7

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " Speed of Other socket: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mSpeedForEachInterface:[J
    invoke-static {v6}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$6700(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[J

    move-result-object v6

    move-object/from16 v0, p0

    iget v7, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->sockID:I

    add-int/lit8 v7, v7, 0x1

    rem-int/lit8 v7, v7, 0x2

    aget-wide v6, v6, v7

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " Speed ratio is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, v26

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    goto/16 :goto_9

    .line 4934
    .end local v26    # "ratio":D
    :catchall_c
    move-exception v3

    monitor-exit v4
    :try_end_35
    .catchall {:try_start_35 .. :try_end_35} :catchall_c

    :try_start_36
    throw v3
    :try_end_36
    .catch Ljava/lang/Throwable; {:try_start_36 .. :try_end_36} :catch_1
    .catchall {:try_start_36 .. :try_end_36} :catchall_6

    :cond_3e
    :try_start_37
    monitor-exit v4
    :try_end_37
    .catchall {:try_start_37 .. :try_end_37} :catchall_c

    .line 4949
    :try_start_38
    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->switchToSocketID:I

    if-ltz v3, :cond_22

    .line 4950
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_3f

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Need to swtich to socket "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->switchToSocketID:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " while remain data offset "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->start:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 4951
    :cond_3f
    new-instance v3, Ljava/io/IOException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Need to swtich to socket "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->switchToSocketID:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 4937
    :cond_40
    const/4 v3, -0x1

    move/from16 v0, v24

    if-ne v0, v3, :cond_42

    cmp-long v3, v30, v20

    if-gez v3, :cond_42

    .line 4938
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_41

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ExtremeConditionThread read body Exception: totallen="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v30

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", bytesToRead= "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v20

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 4940
    :cond_41
    new-instance v3, Ljava/io/IOException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ExtremeContionThread read body Exception: totallen="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v30

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", bytesToRead= "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v20

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 4944
    :cond_42
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_43

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ExtremeConditionThread read body Exception2: totallen="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v30

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", bytesToRead= "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v20

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 4946
    :cond_43
    new-instance v3, Ljava/io/IOException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ExtremeContionThread read body Exception2: totallen="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v30

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", bytesToRead= "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, v20

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_38
    .catch Ljava/lang/Throwable; {:try_start_38 .. :try_end_38} :catch_1
    .catchall {:try_start_38 .. :try_end_38} :catchall_6

    .line 4968
    .end local v2    # "bsize":I
    .end local v15    # "buf":[B
    .end local v16    # "buf_offset":I
    .end local v18    # "buf_ret":J
    .end local v23    # "iRealBlockSize":I
    .end local v24    # "len":I
    .end local v28    # "sTime":J
    .restart local v25    # "t":Ljava/lang/Throwable;
    :catchall_d
    move-exception v3

    :try_start_39
    monitor-exit v4
    :try_end_39
    .catchall {:try_start_39 .. :try_end_39} :catchall_d

    :try_start_3a
    throw v3

    .line 4972
    :cond_44
    move-object/from16 v0, v25

    instance-of v3, v0, Ljava/lang/OutOfMemoryError;

    if-eqz v3, :cond_48

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->bDeprecated:Z

    if-nez v3, :cond_48

    .line 4973
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->dBuf:Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

    monitor-enter v4
    :try_end_3a
    .catchall {:try_start_3a .. :try_end_3a} :catchall_6

    .line 4974
    :try_start_3b
    invoke-virtual/range {p0 .. p0}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->closeSocketAndStreams()V
    :try_end_3b
    .catchall {:try_start_3b .. :try_end_3b} :catchall_f

    .line 4976
    :try_start_3c
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->dBuf:Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

    const/16 v5, 0xa

    invoke-virtual {v3, v5}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;->removeTail(I)V
    :try_end_3c
    .catch Ljava/lang/Throwable; {:try_start_3c .. :try_end_3c} :catch_4
    .catchall {:try_start_3c .. :try_end_3c} :catchall_f

    .line 4981
    :cond_45
    :goto_c
    :try_start_3d
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 4982
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B

    move-result-object v3

    const/4 v5, 0x0

    const/4 v6, -0x1

    aput-byte v6, v3, v5

    .line 4983
    monitor-exit v4
    :try_end_3d
    .catchall {:try_start_3d .. :try_end_3d} :catchall_f

    .line 5002
    :cond_46
    :goto_d
    monitor-enter p0

    .line 5003
    :try_start_3e
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    move-result-object v3

    if-nez v3, :cond_47

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->bDeprecated:Z

    if-nez v3, :cond_47

    .line 5004
    invoke-virtual/range {p0 .. p0}, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->closeSocketAndStreams()V

    .line 5005
    :cond_47
    monitor-exit p0

    goto/16 :goto_0

    :catchall_e
    move-exception v3

    monitor-exit p0
    :try_end_3e
    .catchall {:try_start_3e .. :try_end_3e} :catchall_e

    throw v3

    .line 4978
    :catch_4
    move-exception v22

    .line 4979
    .restart local v22    # "ex":Ljava/lang/Throwable;
    :try_start_3f
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_45

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v3

    move-object/from16 v0, v22

    invoke-virtual {v3, v0}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V

    goto :goto_c

    .line 4983
    .end local v22    # "ex":Ljava/lang/Throwable;
    :catchall_f
    move-exception v3

    monitor-exit v4
    :try_end_3f
    .catchall {:try_start_3f .. :try_end_3f} :catchall_f

    :try_start_40
    throw v3

    .line 4986
    :cond_48
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v28

    .line 4987
    .restart local v28    # "sTime":J
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->dBuf:Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

    monitor-enter v4
    :try_end_40
    .catchall {:try_start_40 .. :try_end_40} :catchall_6

    .line 4989
    :try_start_41
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->dBuf:Lcom/android/okhttp/internal/http/MultiSocketInputStream$DataBuffer;

    const-wide/16 v6, 0x1f4

    invoke-virtual {v3, v6, v7}, Ljava/lang/Object;->wait(J)V
    :try_end_41
    .catch Ljava/lang/Throwable; {:try_start_41 .. :try_end_41} :catch_6
    .catchall {:try_start_41 .. :try_end_41} :catchall_10

    .line 4993
    :goto_e
    :try_start_42
    monitor-exit v4
    :try_end_42
    .catchall {:try_start_42 .. :try_end_42} :catchall_10

    .line 4994
    :try_start_43
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v32, v4, v28

    .line 4995
    .local v32, "waitedTime":J
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->mTimeForDownload:[J
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$4000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[J

    move-result-object v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->sockID:I

    aget-wide v4, v3, v4

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-lez v3, :cond_49

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$3900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    move-result-object v3

    if-nez v3, :cond_49

    .line 4996
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->sockID:I

    move-wide/from16 v0, v32

    invoke-virtual {v3, v4, v0, v1}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->decTime(IJ)V

    .line 4997
    :cond_49
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->blockStatus:[B
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$1900(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)[B

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, -0x1

    aput-byte v5, v3, v4
    :try_end_43
    .catchall {:try_start_43 .. :try_end_43} :catchall_6

    goto :goto_d

    .line 4993
    .end local v32    # "waitedTime":J
    :catchall_10
    move-exception v3

    :try_start_44
    monitor-exit v4
    :try_end_44
    .catchall {:try_start_44 .. :try_end_44} :catchall_10

    :try_start_45
    throw v3
    :try_end_45
    .catchall {:try_start_45 .. :try_end_45} :catchall_6

    .line 5005
    .end local v25    # "t":Ljava/lang/Throwable;
    .end local v28    # "sTime":J
    :catchall_11
    move-exception v3

    :try_start_46
    monitor-exit p0
    :try_end_46
    .catchall {:try_start_46 .. :try_end_46} :catchall_11

    throw v3

    .line 5017
    :catch_5
    move-exception v17

    .line 5018
    .restart local v17    # "e":Ljava/lang/Throwable;
    :try_start_47
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_5

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V

    goto/16 :goto_2

    .line 5021
    .end local v17    # "e":Ljava/lang/Throwable;
    :catchall_12
    move-exception v3

    monitor-exit v4
    :try_end_47
    .catchall {:try_start_47 .. :try_end_47} :catchall_12

    throw v3

    .line 4991
    .restart local v25    # "t":Ljava/lang/Throwable;
    .restart local v28    # "sTime":J
    :catch_6
    move-exception v3

    goto :goto_e

    .line 4923
    .end local v25    # "t":Ljava/lang/Throwable;
    .restart local v2    # "bsize":I
    .restart local v15    # "buf":[B
    .restart local v16    # "buf_offset":I
    .restart local v18    # "buf_ret":J
    .restart local v23    # "iRealBlockSize":I
    .restart local v24    # "len":I
    .restart local v26    # "ratio":D
    :catch_7
    move-exception v3

    goto/16 :goto_b

    .line 4909
    .end local v26    # "ratio":D
    :catch_8
    move-exception v3

    goto/16 :goto_a

    .line 4899
    :catch_9
    move-exception v3

    goto/16 :goto_8
.end method

.method public startTryBoth(I)V
    .locals 3
    .param p1, "preferSock"    # I

    .prologue
    .line 4628
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->bTryBoth:Z

    .line 4629
    sget-boolean v0, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v0}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "start try both, prefer "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 4630
    :cond_0
    return-void
.end method

.method public switchSocket(I)Z
    .locals 3
    .param p1, "sid"    # I

    .prologue
    .line 4617
    sget-boolean v0, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->this$0:Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    # getter for: Lcom/android/okhttp/internal/http/MultiSocketInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v0}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->access$000(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "requested to switch socket id from "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->sockID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 4618
    :cond_0
    iget v0, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->sockID:I

    if-eq p1, v0, :cond_1

    .line 4619
    iput p1, p0, Lcom/android/okhttp/internal/http/MultiSocketInputStream$ExtremeConditionThread;->switchToSocketID:I

    .line 4620
    const/4 v0, 0x1

    .line 4623
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
