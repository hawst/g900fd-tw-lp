.class public Lcom/android/okhttp/internal/http/SBServiceAPI;
.super Ljava/lang/Object;
.source "SBServiceAPI.java"


# static fields
.field private static final CONNECTIVITY_SERVICE:Ljava/lang/String; = "sb_service"

.field private static final GET_IP_FROM_SERVICE:Z = true

.field private static final ICONNECTIVITY_MANAGER_STUB:Ljava/lang/String; = "com.samsung.android.smartbonding.ISmartBondingService$Stub"

.field private static final ICONNECTIVITY_MANAGER_STUB_AS_INTERFACE:Ljava/lang/String; = "asInterface"

.field private static final SERVICE_MANAGER:Ljava/lang/String; = "android.os.ServiceManager"

.field private static final SERVICE_MANAGER_METHOD_GET_SERVICE:Ljava/lang/String; = "getService"

.field private static TrafficStats:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static bGotTrafficStats:Z

.field private static bServiceGot:Z

.field private static sIConnectivityManager:Ljava/lang/Object;

.field private static sIConnectivityManagerClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static sMethodCounter:I

.field private static sMethodNames:[Ljava/lang/reflect/Method;

.field private static sServiceManager:Ljava/lang/Object;

.field private static sServiceManagerClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static setTag:Ljava/lang/reflect/Method;

.field private static setUid:Ljava/lang/reflect/Method;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 35
    sput-boolean v1, Lcom/android/okhttp/internal/http/SBServiceAPI;->bServiceGot:Z

    .line 37
    sput-object v0, Lcom/android/okhttp/internal/http/SBServiceAPI;->sServiceManagerClass:Ljava/lang/Class;

    .line 38
    sput-object v0, Lcom/android/okhttp/internal/http/SBServiceAPI;->sMethodNames:[Ljava/lang/reflect/Method;

    .line 39
    sput-object v0, Lcom/android/okhttp/internal/http/SBServiceAPI;->sServiceManager:Ljava/lang/Object;

    .line 40
    sput-object v0, Lcom/android/okhttp/internal/http/SBServiceAPI;->sIConnectivityManager:Ljava/lang/Object;

    .line 41
    sput-object v0, Lcom/android/okhttp/internal/http/SBServiceAPI;->sIConnectivityManagerClass:Ljava/lang/Class;

    .line 42
    sput v1, Lcom/android/okhttp/internal/http/SBServiceAPI;->sMethodCounter:I

    .line 706
    sput-boolean v1, Lcom/android/okhttp/internal/http/SBServiceAPI;->bGotTrafficStats:Z

    .line 707
    sput-object v0, Lcom/android/okhttp/internal/http/SBServiceAPI;->TrafficStats:Ljava/lang/Class;

    .line 708
    sput-object v0, Lcom/android/okhttp/internal/http/SBServiceAPI;->setTag:Ljava/lang/reflect/Method;

    .line 709
    sput-object v0, Lcom/android/okhttp/internal/http/SBServiceAPI;->setUid:Ljava/lang/reflect/Method;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Ljava/lang/Class;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/android/okhttp/internal/http/SBServiceAPI;->sIConnectivityManagerClass:Ljava/lang/Class;

    return-object v0
.end method

.method static synthetic access$100()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/android/okhttp/internal/http/SBServiceAPI;->sIConnectivityManager:Ljava/lang/Object;

    return-object v0
.end method

.method public static convertLongToByte(J[BII)V
    .locals 6
    .param p0, "l"    # J
    .param p2, "b"    # [B
    .param p3, "s"    # I
    .param p4, "e"    # I

    .prologue
    .line 748
    if-eqz p2, :cond_0

    array-length v1, p2

    if-lt v1, p4, :cond_0

    sub-int v1, p4, p3

    const/16 v4, 0x8

    if-eq v1, v4, :cond_1

    .line 764
    :cond_0
    :goto_0
    return-void

    .line 749
    :cond_1
    move v0, p3

    .local v0, "i":I
    :goto_1
    if-ge v0, p4, :cond_2

    .line 750
    sub-int v1, v0, p3

    mul-int/lit8 v1, v1, 0x8

    shr-long v2, p0, v1

    .line 751
    .local v2, "tmp":J
    const-wide/16 v4, 0xff

    and-long/2addr v2, v4

    .line 752
    long-to-int v1, v2

    int-to-byte v1, v1

    aput-byte v1, p2, v0

    .line 749
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 754
    .end local v2    # "tmp":J
    :cond_2
    sget-boolean v1, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v1, :cond_0

    goto :goto_0
.end method

.method public static getAddrsByHost(JLjava/lang/String;IZ)[Ljava/net/InetAddress;
    .locals 16
    .param p0, "threadID"    # J
    .param p2, "host"    # Ljava/lang/String;
    .param p3, "ipStyle"    # I
    .param p4, "isHTTPS"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/UnknownHostException;
        }
    .end annotation

    .prologue
    .line 174
    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-wide/16 v14, 0x4e20

    add-long v6, v4, v14

    .line 175
    .local v6, "timeOut":J
    new-instance v8, Ljava/util/LinkedList;

    invoke-direct {v8}, Ljava/util/LinkedList;-><init>()V

    .line 177
    .local v8, "retAddrs":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/net/InetAddress;>;"
    if-nez p3, :cond_0

    if-nez p4, :cond_0

    .line 179
    new-instance v10, Lcom/android/okhttp/internal/http/SBServiceAPI$1;

    move-object/from16 v0, p2

    invoke-direct {v10, v0, v8}, Lcom/android/okhttp/internal/http/SBServiceAPI$1;-><init>(Ljava/lang/String;Ljava/util/LinkedList;)V

    .line 200
    .local v10, "r2":Ljava/lang/Runnable;
    new-instance v12, Ljava/lang/Thread;

    invoke-direct {v12, v10}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 201
    .local v12, "t2":Ljava/lang/Thread;
    invoke-virtual {v12}, Ljava/lang/Thread;->start()V

    .line 205
    .end local v10    # "r2":Ljava/lang/Runnable;
    .end local v12    # "t2":Ljava/lang/Thread;
    :cond_0
    new-instance v2, Lcom/android/okhttp/internal/http/SBServiceAPI$2;

    move-wide/from16 v3, p0

    move-object/from16 v5, p2

    invoke-direct/range {v2 .. v8}, Lcom/android/okhttp/internal/http/SBServiceAPI$2;-><init>(JLjava/lang/String;JLjava/util/LinkedList;)V

    .line 265
    .local v2, "r1":Ljava/lang/Runnable;
    new-instance v11, Ljava/lang/Thread;

    invoke-direct {v11, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 266
    .local v11, "t1":Ljava/lang/Thread;
    invoke-virtual {v11}, Ljava/lang/Thread;->start()V

    .line 268
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    cmp-long v3, v4, v6

    if-gez v3, :cond_4

    .line 269
    monitor-enter v8
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 270
    :try_start_1
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SBServiceAPI: current result is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiratLog;->info(Ljava/lang/String;)V

    .line 271
    :cond_1
    invoke-virtual {v8}, Ljava/util/LinkedList;->size()I

    move-result v3

    if-lez v3, :cond_2

    .line 272
    invoke-virtual {v8}, Ljava/util/LinkedList;->size()I

    move-result v3

    new-array v3, v3, [Ljava/net/InetAddress;

    invoke-virtual {v8, v3}, Ljava/util/LinkedList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/net/InetAddress;

    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v3

    .line 275
    :cond_2
    const-wide/16 v4, 0x3e8

    :try_start_2
    invoke-virtual {v8, v4, v5}, Ljava/lang/Object;->wait(J)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 279
    :goto_1
    :try_start_3
    monitor-exit v8

    goto :goto_0

    :catchall_0
    move-exception v3

    monitor-exit v8
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v3
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_0

    .line 286
    .end local v2    # "r1":Ljava/lang/Runnable;
    .end local v6    # "timeOut":J
    .end local v8    # "retAddrs":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/net/InetAddress;>;"
    .end local v11    # "t1":Ljava/lang/Thread;
    :catch_0
    move-exception v9

    .line 287
    .local v9, "e":Ljava/lang/Throwable;
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_3

    invoke-static {v9}, Lcom/android/okhttp/internal/http/MultiratLog;->info(Ljava/lang/Throwable;)V

    .line 288
    :cond_3
    new-instance v3, Ljava/net/UnknownHostException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "cannot resolve host "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/net/UnknownHostException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 283
    .end local v9    # "e":Ljava/lang/Throwable;
    .restart local v2    # "r1":Ljava/lang/Runnable;
    .restart local v6    # "timeOut":J
    .restart local v8    # "retAddrs":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/net/InetAddress;>;"
    .restart local v11    # "t1":Ljava/lang/Thread;
    :cond_4
    :try_start_5
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_5

    const-string v3, "SBServiceAPI: responseGetAllByName time out"

    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiratLog;->info(Ljava/lang/String;)V

    .line 284
    :cond_5
    new-instance v3, Ljava/net/UnknownHostException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "cannot resolve host "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/net/UnknownHostException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_0

    .line 277
    :catch_1
    move-exception v3

    goto :goto_1
.end method

.method public static getFasterInterface()I
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 375
    const/4 v3, -0x1

    :try_start_0
    invoke-static {v3}, Lcom/android/okhttp/internal/http/SBServiceAPI;->getSmartBondingData(I)[J

    move-result-object v0

    .line 376
    .local v0, "data":[J
    if-eqz v0, :cond_0

    array-length v3, v0

    if-nez v3, :cond_1

    .line 386
    :cond_0
    :goto_0
    return v2

    .line 380
    :cond_1
    const/4 v3, 0x0

    aget-wide v4, v0, v3
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-gtz v3, :cond_2

    .line 381
    .local v2, "i":I
    :goto_1
    goto :goto_0

    .line 380
    .end local v2    # "i":I
    :cond_2
    const/4 v2, 0x1

    goto :goto_1

    .line 384
    :catch_0
    move-exception v1

    .line 385
    .local v1, "e":Ljava/lang/Throwable;
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_0

    invoke-static {v1}, Lcom/android/okhttp/internal/http/MultiratLog;->info(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static getHttpLogEnabled()Z
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 601
    :try_start_0
    invoke-static {}, Lcom/android/okhttp/internal/http/SBServiceAPI;->getService()Z

    .line 602
    sget-object v2, Lcom/android/okhttp/internal/http/SBServiceAPI;->sIConnectivityManagerClass:Ljava/lang/Class;

    const-string v4, "getHttpLogEnabled"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Class;

    invoke-virtual {v2, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 604
    .local v1, "getHttpLogEnabled":Ljava/lang/reflect/Method;
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_0

    const-string v2, "SBServiceAPI: getHttpLogEnabled("

    invoke-static {v2}, Lcom/android/okhttp/internal/http/MultiratLog;->info(Ljava/lang/String;)V

    .line 605
    :cond_0
    sget-object v2, Lcom/android/okhttp/internal/http/SBServiceAPI;->sIConnectivityManager:Ljava/lang/Object;

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 609
    :goto_0
    return v2

    .line 607
    :catch_0
    move-exception v0

    .line 608
    .local v0, "e":Ljava/lang/Throwable;
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_1

    invoke-static {v0}, Lcom/android/okhttp/internal/http/MultiratLog;->info(Ljava/lang/Throwable;)V

    :cond_1
    move v2, v3

    .line 609
    goto :goto_0
.end method

.method public static getHttpProxy(I)[Ljava/lang/String;
    .locals 14
    .param p0, "netType"    # I

    .prologue
    const/4 v13, 0x0

    .line 566
    :try_start_0
    invoke-static {}, Lcom/android/okhttp/internal/http/SBServiceAPI;->getService()Z

    .line 567
    sget-object v8, Lcom/android/okhttp/internal/http/SBServiceAPI;->sIConnectivityManagerClass:Ljava/lang/Class;

    const-string v9, "getProxyInfo"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Class;

    const/4 v11, 0x0

    sget-object v12, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v12, v10, v11

    invoke-virtual {v8, v9, v10}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 568
    .local v3, "getProxyInfo":Ljava/lang/reflect/Method;
    sget-boolean v8, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v8, :cond_0

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "SBServiceAPI: getProxyInfo("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/android/okhttp/internal/http/MultiratLog;->info(Ljava/lang/String;)V

    .line 569
    :cond_0
    sget-object v8, Lcom/android/okhttp/internal/http/SBServiceAPI;->sIConnectivityManager:Ljava/lang/Object;

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v3, v8, v9}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [Ljava/lang/String;

    move-object v0, v8

    check-cast v0, [Ljava/lang/String;

    move-object v6, v0

    .line 570
    .local v6, "ret":[Ljava/lang/String;
    sget-boolean v8, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v8, :cond_2

    .line 571
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "SBServiceAPI: getProxyInfo result "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    if-nez v6, :cond_1

    const-string v8, "NULL"

    :goto_0
    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/android/okhttp/internal/http/MultiratLog;->info(Ljava/lang/String;)V

    .line 572
    if-eqz v6, :cond_2

    .line 573
    move-object v1, v6

    .local v1, "arr$":[Ljava/lang/String;
    array-length v5, v1

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_1
    if-ge v4, v5, :cond_2

    aget-object v7, v1, v4

    .line 574
    .local v7, "s":Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "SBServiceAPI: getProxyInfo result: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/android/okhttp/internal/http/MultiratLog;->info(Ljava/lang/String;)V

    .line 573
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 571
    .end local v1    # "arr$":[Ljava/lang/String;
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    .end local v7    # "s":Ljava/lang/String;
    :cond_1
    array-length v8, v6

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    goto :goto_0

    .line 579
    :cond_2
    if-eqz v6, :cond_3

    array-length v8, v6

    const/4 v9, 0x3

    if-ne v8, v9, :cond_3

    const/4 v8, 0x0

    aget-object v8, v6, v8

    if-eqz v8, :cond_3

    const/4 v8, 0x1

    aget-object v8, v6, v8

    if-nez v8, :cond_4

    .line 580
    :cond_3
    const/4 v8, 0x0

    new-array v6, v8, [Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 588
    .end local v3    # "getProxyInfo":Ljava/lang/reflect/Method;
    .end local v6    # "ret":[Ljava/lang/String;
    :cond_4
    :goto_2
    return-object v6

    .line 586
    :catch_0
    move-exception v2

    .line 587
    .local v2, "e":Ljava/lang/Throwable;
    sget-boolean v8, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v8, :cond_5

    invoke-static {v2}, Lcom/android/okhttp/internal/http/MultiratLog;->info(Ljava/lang/Throwable;)V

    .line 588
    :cond_5
    new-array v6, v13, [Ljava/lang/String;

    goto :goto_2
.end method

.method public static getNBEnabled()Z
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 431
    :try_start_0
    invoke-static {}, Lcom/android/okhttp/internal/http/SBServiceAPI;->getService()Z

    .line 432
    sget-object v2, Lcom/android/okhttp/internal/http/SBServiceAPI;->sIConnectivityManagerClass:Ljava/lang/Class;

    const-string v4, "getSBEnabled"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Class;

    invoke-virtual {v2, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 433
    .local v1, "getSBEnabled":Ljava/lang/reflect/Method;
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_0

    const-string v2, "SBServiceAPI: getSBEnabled("

    invoke-static {v2}, Lcom/android/okhttp/internal/http/MultiratLog;->info(Ljava/lang/String;)V

    .line 434
    :cond_0
    sget-object v2, Lcom/android/okhttp/internal/http/SBServiceAPI;->sIConnectivityManager:Ljava/lang/Object;

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 438
    :goto_0
    return v2

    .line 436
    :catch_0
    move-exception v0

    .line 437
    .local v0, "e":Ljava/lang/Throwable;
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_1

    invoke-static {v0}, Lcom/android/okhttp/internal/http/MultiratLog;->info(Ljava/lang/Throwable;)V

    :cond_1
    move v2, v3

    .line 438
    goto :goto_0
.end method

.method public static getSBEnabledDirectByIP(Lcom/android/okhttp/Connection;Lcom/android/okhttp/internal/http/MultiratLog;)I
    .locals 1
    .param p0, "connection"    # Lcom/android/okhttp/Connection;
    .param p1, "logger"    # Lcom/android/okhttp/internal/http/MultiratLog;

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/android/okhttp/Connection;->bBothInfAvail()I

    move-result v0

    return v0
.end method

.method public static getSBInterface(II)Ljava/net/InetAddress;
    .locals 1
    .param p0, "netType"    # I
    .param p1, "IPver"    # I

    .prologue
    .line 113
    invoke-static {p0, p1}, Lcom/android/okhttp/internal/http/SBServiceAPI;->getSBInterfaceFromSBServiceEx(II)Ljava/net/InetAddress;

    move-result-object v0

    return-object v0
.end method

.method private static getSBInterfaceFromSBService(I)Ljava/net/InetAddress;
    .locals 9
    .param p0, "netType"    # I

    .prologue
    const/4 v3, 0x0

    .line 124
    :try_start_0
    invoke-static {}, Lcom/android/okhttp/internal/http/SBServiceAPI;->getService()Z

    .line 126
    sget-object v4, Lcom/android/okhttp/internal/http/SBServiceAPI;->sIConnectivityManagerClass:Ljava/lang/Class;

    const-string v5, "getSBInterface"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v7, 0x0

    sget-object v8, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 128
    .local v1, "getSBInterface":Ljava/lang/reflect/Method;
    sget-object v4, Lcom/android/okhttp/internal/http/SBServiceAPI;->sIConnectivityManager:Ljava/lang/Object;

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v1, v4, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 129
    .local v2, "ipAddress":Ljava/lang/String;
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SBServiceAPI: getSBInterface + "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiratLog;->info(Ljava/lang/String;)V

    .line 130
    :cond_0
    if-eqz v2, :cond_1

    .line 131
    invoke-static {v2}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 138
    .end local v1    # "getSBInterface":Ljava/lang/reflect/Method;
    .end local v2    # "ipAddress":Ljava/lang/String;
    :cond_1
    :goto_0
    return-object v3

    .line 136
    :catch_0
    move-exception v0

    .line 137
    .local v0, "e":Ljava/lang/Throwable;
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_1

    invoke-static {v0}, Lcom/android/okhttp/internal/http/MultiratLog;->info(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private static getSBInterfaceFromSBServiceEx(II)Ljava/net/InetAddress;
    .locals 8
    .param p0, "netType"    # I
    .param p1, "IPver"    # I

    .prologue
    .line 149
    :try_start_0
    invoke-static {}, Lcom/android/okhttp/internal/http/SBServiceAPI;->getService()Z

    .line 151
    sget-object v3, Lcom/android/okhttp/internal/http/SBServiceAPI;->sIConnectivityManagerClass:Ljava/lang/Class;

    const-string v4, "getSBInterfaceEx"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    sget-object v7, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v7, v5, v6

    const/4 v6, 0x1

    sget-object v7, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 153
    .local v1, "getSBInterface":Ljava/lang/reflect/Method;
    sget-object v3, Lcom/android/okhttp/internal/http/SBServiceAPI;->sIConnectivityManager:Ljava/lang/Object;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v1, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 154
    .local v2, "ipAddress":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 155
    invoke-static {v2}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 162
    .end local v1    # "getSBInterface":Ljava/lang/reflect/Method;
    .end local v2    # "ipAddress":Ljava/lang/String;
    :goto_0
    return-object v3

    .line 157
    .restart local v1    # "getSBInterface":Ljava/lang/reflect/Method;
    .restart local v2    # "ipAddress":Ljava/lang/String;
    :cond_0
    const/4 v3, 0x0

    goto :goto_0

    .line 160
    .end local v1    # "getSBInterface":Ljava/lang/reflect/Method;
    .end local v2    # "ipAddress":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 161
    .local v0, "e":Ljava/lang/Throwable;
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_1

    invoke-static {v0}, Lcom/android/okhttp/internal/http/MultiratLog;->info(Ljava/lang/Throwable;)V

    .line 162
    :cond_1
    invoke-static {p0}, Lcom/android/okhttp/internal/http/SBServiceAPI;->getSBInterfaceFromSBService(I)Ljava/net/InetAddress;

    move-result-object v3

    goto :goto_0
.end method

.method private static getSBInterfaceIPDirect(II)Ljava/net/InetAddress;
    .locals 10
    .param p0, "netType"    # I
    .param p1, "IPver"    # I

    .prologue
    const/4 v9, 0x1

    .line 78
    :try_start_0
    sget-boolean v7, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v7, :cond_0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getSBInterfaceIPDirect for net "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiratLog;->info(Ljava/lang/String;)V

    .line 80
    :cond_0
    if-nez p0, :cond_8

    .line 81
    const-string v3, "wlan0"

    .line 89
    .local v3, "iName":Ljava/lang/String;
    :goto_0
    const/4 v6, 0x0

    .line 90
    .local v6, "ret":Ljava/net/InetAddress;
    invoke-static {}, Ljava/net/NetworkInterface;->getNetworkInterfaces()Ljava/util/Enumeration;

    move-result-object v0

    .local v0, "en":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/NetworkInterface;>;"
    :cond_1
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v7

    if-eqz v7, :cond_7

    .line 91
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/net/NetworkInterface;

    .line 92
    .local v5, "intf":Ljava/net/NetworkInterface;
    invoke-virtual {v5}, Ljava/net/NetworkInterface;->isUp()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-virtual {v5}, Ljava/net/NetworkInterface;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 93
    invoke-virtual {v5}, Ljava/net/NetworkInterface;->getInetAddresses()Ljava/util/Enumeration;

    move-result-object v1

    .local v1, "enumIpAddr":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/InetAddress;>;"
    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 94
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/net/InetAddress;

    .line 95
    .local v4, "inetAddress":Ljava/net/InetAddress;
    sget-boolean v7, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v7, :cond_3

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "checking IP "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiratLog;->info(Ljava/lang/String;)V

    .line 96
    :cond_3
    invoke-virtual {v4}, Ljava/net/InetAddress;->isLoopbackAddress()Z

    move-result v7

    if-nez v7, :cond_2

    if-nez p1, :cond_4

    instance-of v7, v4, Ljava/net/Inet4Address;

    if-nez v7, :cond_5

    :cond_4
    if-ne p1, v9, :cond_2

    instance-of v7, v4, Ljava/net/Inet6Address;

    if-eqz v7, :cond_2

    .line 99
    :cond_5
    move-object v6, v4

    .line 100
    sget-boolean v7, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v7, :cond_2

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "get local ip "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " for interface "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/android/okhttp/internal/http/MultiratLog;->info(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 106
    .end local v0    # "en":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/NetworkInterface;>;"
    .end local v1    # "enumIpAddr":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/InetAddress;>;"
    .end local v3    # "iName":Ljava/lang/String;
    .end local v4    # "inetAddress":Ljava/net/InetAddress;
    .end local v5    # "intf":Ljava/net/NetworkInterface;
    .end local v6    # "ret":Ljava/net/InetAddress;
    :catch_0
    move-exception v2

    .line 107
    .local v2, "ex":Ljava/lang/Throwable;
    sget-boolean v7, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v7, :cond_6

    invoke-static {v2}, Lcom/android/okhttp/internal/http/MultiratLog;->info(Ljava/lang/Throwable;)V

    .line 108
    :cond_6
    invoke-static {p0, p1}, Lcom/android/okhttp/internal/http/SBServiceAPI;->getSBInterfaceFromSBServiceEx(II)Ljava/net/InetAddress;

    move-result-object v6

    .end local v2    # "ex":Ljava/lang/Throwable;
    :cond_7
    :goto_2
    return-object v6

    .line 83
    :cond_8
    if-ne p0, v9, :cond_9

    .line 84
    :try_start_1
    const-string v3, "rmnet0"
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    .restart local v3    # "iName":Ljava/lang/String;
    goto/16 :goto_0

    .line 87
    .end local v3    # "iName":Ljava/lang/String;
    :cond_9
    const/4 v6, 0x0

    goto :goto_2
.end method

.method public static getSBInterfaces()[Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 411
    :try_start_0
    invoke-static {}, Lcom/android/okhttp/internal/http/SBServiceAPI;->getService()Z

    .line 412
    sget-object v2, Lcom/android/okhttp/internal/http/SBServiceAPI;->sIConnectivityManagerClass:Ljava/lang/Class;

    const-string v3, "getSBInterfaces"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 414
    .local v1, "getSBInterfaces":Ljava/lang/reflect/Method;
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_0

    const-string v2, "SBServiceAPI: getSBInterfaces"

    invoke-static {v2}, Lcom/android/okhttp/internal/http/MultiratLog;->info(Ljava/lang/String;)V

    .line 415
    :cond_0
    sget-object v2, Lcom/android/okhttp/internal/http/SBServiceAPI;->sIConnectivityManager:Ljava/lang/Object;

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    check-cast v2, [Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 419
    :goto_0
    return-object v2

    .line 417
    :catch_0
    move-exception v0

    .line 418
    .local v0, "e":Ljava/lang/Throwable;
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_1

    invoke-static {v0}, Lcom/android/okhttp/internal/http/MultiratLog;->info(Ljava/lang/Throwable;)V

    .line 419
    :cond_1
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    aput-object v6, v2, v5

    const/4 v3, 0x1

    aput-object v6, v2, v3

    goto :goto_0
.end method

.method public static getSBUsageStatus(J)I
    .locals 8
    .param p0, "threadID"    # J

    .prologue
    .line 510
    :try_start_0
    invoke-static {}, Lcom/android/okhttp/internal/http/SBServiceAPI;->getService()Z

    .line 511
    sget-object v2, Lcom/android/okhttp/internal/http/SBServiceAPI;->sIConnectivityManagerClass:Ljava/lang/Class;

    const-string v3, "getSBUsageStatus"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    sget-object v6, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 513
    .local v1, "getSBUsageStatus":Ljava/lang/reflect/Method;
    sget-object v2, Lcom/android/okhttp/internal/http/SBServiceAPI;->sIConnectivityManager:Ljava/lang/Object;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 517
    .end local v1    # "getSBUsageStatus":Ljava/lang/reflect/Method;
    :goto_0
    return v2

    .line 515
    :catch_0
    move-exception v0

    .line 516
    .local v0, "e":Ljava/lang/Throwable;
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_0

    invoke-static {v0}, Lcom/android/okhttp/internal/http/MultiratLog;->info(Ljava/lang/Throwable;)V

    .line 517
    :cond_0
    const/4 v2, 0x2

    goto :goto_0
.end method

.method public static getService()Z
    .locals 8

    .prologue
    .line 619
    :try_start_0
    sget-boolean v3, Lcom/android/okhttp/internal/http/SBServiceAPI;->bServiceGot:Z

    if-nez v3, :cond_1

    .line 621
    const-string v3, "android.os.ServiceManager"

    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    sput-object v3, Lcom/android/okhttp/internal/http/SBServiceAPI;->sServiceManagerClass:Ljava/lang/Class;

    .line 623
    sget-object v3, Lcom/android/okhttp/internal/http/SBServiceAPI;->sServiceManagerClass:Ljava/lang/Class;

    const-string v4, "getService"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    const-class v7, Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 625
    .local v1, "getServiceMethod":Ljava/lang/reflect/Method;
    const/4 v3, 0x0

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "sb_service"

    aput-object v6, v4, v5

    invoke-virtual {v1, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    sput-object v3, Lcom/android/okhttp/internal/http/SBServiceAPI;->sServiceManager:Ljava/lang/Object;

    .line 629
    const-string v3, "com.samsung.android.smartbonding.ISmartBondingService$Stub"

    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    .line 630
    .local v2, "stubClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-virtual {v2}, Ljava/lang/Class;->getMethods()[Ljava/lang/reflect/Method;

    move-result-object v3

    sput-object v3, Lcom/android/okhttp/internal/http/SBServiceAPI;->sMethodNames:[Ljava/lang/reflect/Method;

    .line 632
    :goto_0
    sget v3, Lcom/android/okhttp/internal/http/SBServiceAPI;->sMethodCounter:I

    sget-object v4, Lcom/android/okhttp/internal/http/SBServiceAPI;->sMethodNames:[Ljava/lang/reflect/Method;

    array-length v4, v4

    if-ge v3, v4, :cond_0

    .line 634
    sget-object v3, Lcom/android/okhttp/internal/http/SBServiceAPI;->sMethodNames:[Ljava/lang/reflect/Method;

    sget v4, Lcom/android/okhttp/internal/http/SBServiceAPI;->sMethodCounter:I

    aget-object v3, v3, v4

    invoke-virtual {v3}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "asInterface"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 639
    :cond_0
    sget-object v3, Lcom/android/okhttp/internal/http/SBServiceAPI;->sMethodNames:[Ljava/lang/reflect/Method;

    sget v4, Lcom/android/okhttp/internal/http/SBServiceAPI;->sMethodCounter:I

    aget-object v3, v3, v4

    const/4 v4, 0x0

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    sget-object v7, Lcom/android/okhttp/internal/http/SBServiceAPI;->sServiceManager:Ljava/lang/Object;

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    sput-object v3, Lcom/android/okhttp/internal/http/SBServiceAPI;->sIConnectivityManager:Ljava/lang/Object;

    .line 641
    sget-object v3, Lcom/android/okhttp/internal/http/SBServiceAPI;->sIConnectivityManager:Ljava/lang/Object;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    sput-object v3, Lcom/android/okhttp/internal/http/SBServiceAPI;->sIConnectivityManagerClass:Ljava/lang/Class;

    .line 643
    const/4 v3, 0x1

    sput-boolean v3, Lcom/android/okhttp/internal/http/SBServiceAPI;->bServiceGot:Z

    .line 644
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SBServiceAPI: getService "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/android/okhttp/internal/http/SBServiceAPI;->sServiceManagerClass:Ljava/lang/Class;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiratLog;->info(Ljava/lang/String;)V

    .line 646
    :cond_1
    sget-boolean v3, Lcom/android/okhttp/internal/http/SBServiceAPI;->bServiceGot:Z

    .line 651
    .end local v2    # "stubClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :goto_1
    return v3

    .line 632
    .restart local v2    # "stubClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_2
    sget v3, Lcom/android/okhttp/internal/http/SBServiceAPI;->sMethodCounter:I

    add-int/lit8 v3, v3, 0x1

    sput v3, Lcom/android/okhttp/internal/http/SBServiceAPI;->sMethodCounter:I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 648
    .end local v2    # "stubClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :catch_0
    move-exception v0

    .line 649
    .local v0, "e":Ljava/lang/Throwable;
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_3

    invoke-static {v0}, Lcom/android/okhttp/internal/http/MultiratLog;->info(Ljava/lang/Throwable;)V

    .line 651
    :cond_3
    sget-boolean v3, Lcom/android/okhttp/internal/http/SBServiceAPI;->bServiceGot:Z

    goto :goto_1
.end method

.method private static getSmartBondingData(I)[J
    .locals 12
    .param p0, "pid"    # I

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 355
    :try_start_0
    invoke-static {}, Lcom/android/okhttp/internal/http/SBServiceAPI;->getService()Z

    .line 357
    sget-object v5, Lcom/android/okhttp/internal/http/SBServiceAPI;->sIConnectivityManagerClass:Ljava/lang/Class;

    const-string v6, "getSmartBondingData"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Class;

    const/4 v8, 0x0

    sget-object v9, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v9, v7, v8

    invoke-virtual {v5, v6, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 359
    .local v3, "getSmartBondingData":Ljava/lang/reflect/Method;
    sget-object v5, Lcom/android/okhttp/internal/http/SBServiceAPI;->sIConnectivityManager:Ljava/lang/Object;

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v3, v5, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [J

    move-object v0, v5

    check-cast v0, [J

    move-object v4, v0

    .line 360
    .local v4, "ret":[J
    sget-boolean v5, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v5, :cond_0

    const-string v5, "SBServiceAPI: getSmartBondingData"

    invoke-static {v5}, Lcom/android/okhttp/internal/http/MultiratLog;->info(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 365
    .end local v3    # "getSmartBondingData":Ljava/lang/reflect/Method;
    .end local v4    # "ret":[J
    :cond_0
    :goto_0
    return-object v4

    .line 363
    :catch_0
    move-exception v2

    .line 364
    .local v2, "e":Ljava/lang/Throwable;
    sget-boolean v5, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v5, :cond_1

    invoke-static {v2}, Lcom/android/okhttp/internal/http/MultiratLog;->info(Ljava/lang/Throwable;)V

    .line 365
    :cond_1
    new-array v4, v11, [J

    const-wide/16 v6, 0x0

    aput-wide v6, v4, v10

    goto :goto_0
.end method

.method public static getSpeedRatio()[J
    .locals 3

    .prologue
    .line 395
    const/4 v2, 0x2

    new-array v0, v2, [J

    fill-array-data v0, :array_0

    .line 397
    .local v0, "data":[J
    const/4 v2, -0x1

    :try_start_0
    invoke-static {v2}, Lcom/android/okhttp/internal/http/SBServiceAPI;->getSmartBondingData(I)[J
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 402
    :cond_0
    :goto_0
    return-object v0

    .line 399
    :catch_0
    move-exception v1

    .line 400
    .local v1, "e":Ljava/lang/Throwable;
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_0

    invoke-static {v1}, Lcom/android/okhttp/internal/http/MultiratLog;->info(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 395
    nop

    :array_0
    .array-data 8
        0x1
        0x1
    .end array-data
.end method

.method private static getTrafficStats()V
    .locals 6

    .prologue
    .line 712
    sget-boolean v1, Lcom/android/okhttp/internal/http/SBServiceAPI;->bGotTrafficStats:Z

    if-nez v1, :cond_0

    .line 714
    :try_start_0
    const-string v1, "android.net.TrafficStats"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    sput-object v1, Lcom/android/okhttp/internal/http/SBServiceAPI;->TrafficStats:Ljava/lang/Class;

    .line 715
    sget-object v1, Lcom/android/okhttp/internal/http/SBServiceAPI;->TrafficStats:Ljava/lang/Class;

    const-string v2, "setThreadStatsTag"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    sget-object v5, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    sput-object v1, Lcom/android/okhttp/internal/http/SBServiceAPI;->setTag:Ljava/lang/reflect/Method;

    .line 716
    sget-object v1, Lcom/android/okhttp/internal/http/SBServiceAPI;->TrafficStats:Ljava/lang/Class;

    const-string v2, "setThreadStatsUid"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    sget-object v5, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    sput-object v1, Lcom/android/okhttp/internal/http/SBServiceAPI;->setUid:Ljava/lang/reflect/Method;

    .line 717
    const/4 v1, 0x1

    sput-boolean v1, Lcom/android/okhttp/internal/http/SBServiceAPI;->bGotTrafficStats:Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 723
    .local v0, "e":Ljava/lang/Throwable;
    :cond_0
    :goto_0
    return-void

    .line 719
    .end local v0    # "e":Ljava/lang/Throwable;
    :catch_0
    move-exception v0

    .line 720
    .restart local v0    # "e":Ljava/lang/Throwable;
    sget-boolean v1, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v1, :cond_0

    invoke-static {v0}, Lcom/android/okhttp/internal/http/MultiratLog;->info(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static isSBSettingEnabled()Z
    .locals 10

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 673
    :try_start_0
    const-string v6, "android.os.SystemProperties"

    invoke-static {v6}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    .line 674
    .local v3, "sp":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v6, "get"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Class;

    const/4 v8, 0x0

    const-class v9, Ljava/lang/String;

    aput-object v9, v7, v8

    const/4 v8, 0x1

    const-class v9, Ljava/lang/String;

    aput-object v9, v7, v8

    invoke-virtual {v3, v6, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 675
    .local v1, "getMethod":Ljava/lang/reflect/Method;
    const/4 v6, 0x0

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    const-string v9, "persist.sys.sb.setting.enabled"

    aput-object v9, v7, v8

    const/4 v8, 0x1

    const-string v9, "false"

    aput-object v9, v7, v8

    invoke-virtual {v1, v6, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 676
    .local v2, "ret":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "isSBSettingEnabled "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/android/okhttp/internal/http/MultiratLog;->info(Ljava/lang/String;)V

    .line 677
    const-string v6, "true"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v6

    if-eqz v6, :cond_0

    .line 684
    .end local v1    # "getMethod":Ljava/lang/reflect/Method;
    .end local v2    # "ret":Ljava/lang/String;
    :goto_0
    return v4

    .restart local v1    # "getMethod":Ljava/lang/reflect/Method;
    .restart local v2    # "ret":Ljava/lang/String;
    :cond_0
    move v4, v5

    .line 680
    goto :goto_0

    .line 682
    .end local v1    # "getMethod":Ljava/lang/reflect/Method;
    .end local v2    # "ret":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 683
    .local v0, "e":Ljava/lang/Throwable;
    invoke-static {v0}, Lcom/android/okhttp/internal/http/MultiratLog;->info(Ljava/lang/Throwable;)V

    move v4, v5

    .line 684
    goto :goto_0
.end method

.method public static isSKTBuild()Z
    .locals 10

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 690
    :try_start_0
    const-string v6, "android.os.SystemProperties"

    invoke-static {v6}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    .line 691
    .local v3, "sp":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v6, "get"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Class;

    const/4 v8, 0x0

    const-class v9, Ljava/lang/String;

    aput-object v9, v7, v8

    const/4 v8, 0x1

    const-class v9, Ljava/lang/String;

    aput-object v9, v7, v8

    invoke-virtual {v3, v6, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 692
    .local v1, "getMethod":Ljava/lang/reflect/Method;
    const/4 v6, 0x0

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    const-string v9, "ro.csc.sales_code"

    aput-object v9, v7, v8

    const/4 v8, 0x1

    const-string v9, "false"

    aput-object v9, v7, v8

    invoke-virtual {v1, v6, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 693
    .local v2, "ret":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "isSKTBuild "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/android/okhttp/internal/http/MultiratLog;->info(Ljava/lang/String;)V

    .line 694
    const-string v6, "SKC"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v6

    if-eqz v6, :cond_0

    .line 701
    .end local v1    # "getMethod":Ljava/lang/reflect/Method;
    .end local v2    # "ret":Ljava/lang/String;
    :goto_0
    return v4

    .restart local v1    # "getMethod":Ljava/lang/reflect/Method;
    .restart local v2    # "ret":Ljava/lang/String;
    :cond_0
    move v4, v5

    .line 697
    goto :goto_0

    .line 699
    .end local v1    # "getMethod":Ljava/lang/reflect/Method;
    .end local v2    # "ret":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 700
    .local v0, "e":Ljava/lang/Throwable;
    invoke-static {v0}, Lcom/android/okhttp/internal/http/MultiratLog;->info(Ljava/lang/Throwable;)V

    move v4, v5

    .line 701
    goto :goto_0
.end method

.method public static isShipBuild()Z
    .locals 10

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 656
    :try_start_0
    const-string v6, "android.os.SystemProperties"

    invoke-static {v6}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    .line 657
    .local v3, "sp":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v6, "get"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Class;

    const/4 v8, 0x0

    const-class v9, Ljava/lang/String;

    aput-object v9, v7, v8

    const/4 v8, 0x1

    const-class v9, Ljava/lang/String;

    aput-object v9, v7, v8

    invoke-virtual {v3, v6, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 658
    .local v1, "getMethod":Ljava/lang/reflect/Method;
    const/4 v6, 0x0

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    const-string v9, "ro.product_ship"

    aput-object v9, v7, v8

    const/4 v8, 0x1

    const-string v9, "false"

    aput-object v9, v7, v8

    invoke-virtual {v1, v6, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 659
    .local v2, "ret":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "isShipBuild "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/android/okhttp/internal/http/MultiratLog;->info(Ljava/lang/String;)V

    .line 660
    const-string v6, "true"

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v6

    if-eqz v6, :cond_0

    .line 667
    .end local v1    # "getMethod":Ljava/lang/reflect/Method;
    .end local v2    # "ret":Ljava/lang/String;
    :goto_0
    return v4

    .restart local v1    # "getMethod":Ljava/lang/reflect/Method;
    .restart local v2    # "ret":Ljava/lang/String;
    :cond_0
    move v4, v5

    .line 663
    goto :goto_0

    .line 665
    .end local v1    # "getMethod":Ljava/lang/reflect/Method;
    .end local v2    # "ret":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 666
    .local v0, "e":Ljava/lang/Throwable;
    invoke-static {v0}, Lcom/android/okhttp/internal/http/MultiratLog;->info(Ljava/lang/Throwable;)V

    move v4, v5

    .line 667
    goto :goto_0
.end method

.method public static reportSBUsage(J[J)V
    .locals 10
    .param p0, "threadID"    # J
    .param p2, "data"    # [J

    .prologue
    .line 326
    :try_start_0
    invoke-static {}, Lcom/android/okhttp/internal/http/SBServiceAPI;->getService()Z

    .line 329
    sget-object v4, Lcom/android/okhttp/internal/http/SBServiceAPI;->sIConnectivityManagerClass:Ljava/lang/Class;

    const-string v5, "reportSBUsage"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v7, 0x0

    sget-object v8, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const-class v8, [J

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 331
    .local v2, "reportSBUsage":Ljava/lang/reflect/Method;
    sget-object v4, Lcom/android/okhttp/internal/http/SBServiceAPI;->sIConnectivityManager:Ljava/lang/Object;

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    aput-object p2, v5, v6

    invoke-virtual {v2, v4, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 332
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_1

    .line 333
    new-instance v3, Ljava/lang/StringBuffer;

    const-string v4, "SBServiceAPI: reportSBUsage"

    invoke-direct {v3, v4}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 334
    .local v3, "sBuf":Ljava/lang/StringBuffer;
    array-length v4, p2

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, "("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 335
    invoke-virtual {v3, p0, p1}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, ",["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 336
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v4, p2

    if-ge v1, v4, :cond_0

    .line 337
    aget-wide v4, p2, v1

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, ","

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 336
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 339
    :cond_0
    const-string v4, "])"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 340
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/okhttp/internal/http/MultiratLog;->info(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 346
    .end local v1    # "i":I
    .end local v2    # "reportSBUsage":Ljava/lang/reflect/Method;
    .end local v3    # "sBuf":Ljava/lang/StringBuffer;
    :cond_1
    :goto_1
    return-void

    .line 343
    :catch_0
    move-exception v0

    .line 344
    .local v0, "e":Ljava/lang/Throwable;
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_1

    invoke-static {v0}, Lcom/android/okhttp/internal/http/MultiratLog;->info(Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public static setThreadStatsTag(I)V
    .locals 6
    .param p0, "tag"    # I

    .prologue
    .line 727
    :try_start_0
    invoke-static {}, Lcom/android/okhttp/internal/http/SBServiceAPI;->getTrafficStats()V

    .line 728
    sget-object v1, Lcom/android/okhttp/internal/http/SBServiceAPI;->setTag:Ljava/lang/reflect/Method;

    if-eqz v1, :cond_0

    .line 729
    sget-object v1, Lcom/android/okhttp/internal/http/SBServiceAPI;->setTag:Ljava/lang/reflect/Method;

    const/4 v2, 0x0

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 730
    :cond_0
    sget-boolean v1, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setThreadStatsTag to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with method "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/android/okhttp/internal/http/SBServiceAPI;->setTag:Ljava/lang/reflect/Method;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/okhttp/internal/http/MultiratLog;->info(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 735
    :cond_1
    :goto_0
    return-void

    .line 732
    :catch_0
    move-exception v0

    .line 733
    .local v0, "e":Ljava/lang/Throwable;
    sget-boolean v1, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v1, :cond_1

    invoke-static {v0}, Lcom/android/okhttp/internal/http/MultiratLog;->info(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static setThreadStatsUid(I)V
    .locals 6
    .param p0, "uid"    # I

    .prologue
    .line 738
    :try_start_0
    invoke-static {}, Lcom/android/okhttp/internal/http/SBServiceAPI;->getTrafficStats()V

    .line 739
    sget-object v1, Lcom/android/okhttp/internal/http/SBServiceAPI;->setUid:Ljava/lang/reflect/Method;

    if-eqz v1, :cond_0

    .line 740
    sget-object v1, Lcom/android/okhttp/internal/http/SBServiceAPI;->setUid:Ljava/lang/reflect/Method;

    const/4 v2, 0x0

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 741
    :cond_0
    sget-boolean v1, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setThreadStatsTag to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with method "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/android/okhttp/internal/http/SBServiceAPI;->setUid:Ljava/lang/reflect/Method;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/android/okhttp/internal/http/MultiratLog;->info(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 746
    :cond_1
    :goto_0
    return-void

    .line 743
    :catch_0
    move-exception v0

    .line 744
    .local v0, "e":Ljava/lang/Throwable;
    sget-boolean v1, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v1, :cond_1

    invoke-static {v0}, Lcom/android/okhttp/internal/http/MultiratLog;->info(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static startSBDirectUsage(JJJLjava/lang/String;)Z
    .locals 8
    .param p0, "threadID"    # J
    .param p2, "filesize"    # J
    .param p4, "range"    # J
    .param p6, "url"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 490
    :try_start_0
    invoke-static {}, Lcom/android/okhttp/internal/http/SBServiceAPI;->getService()Z

    .line 491
    sget-object v2, Lcom/android/okhttp/internal/http/SBServiceAPI;->sIConnectivityManagerClass:Ljava/lang/Class;

    const-string v4, "startSBUsage"

    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    sget-object v7, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v7, v5, v6

    const/4 v6, 0x1

    sget-object v7, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v7, v5, v6

    const/4 v6, 0x2

    sget-object v7, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v7, v5, v6

    const/4 v6, 0x3

    const-class v7, Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-virtual {v2, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 493
    .local v1, "startSBUsage":Ljava/lang/reflect/Method;
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SBServiceAPI: startSBUsage("

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/okhttp/internal/http/MultiratLog;->info(Ljava/lang/String;)V

    .line 494
    :cond_0
    sget-object v2, Lcom/android/okhttp/internal/http/SBServiceAPI;->sIConnectivityManager:Ljava/lang/Object;

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x3

    aput-object p6, v4, v5

    invoke-virtual {v1, v2, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 498
    .end local v1    # "startSBUsage":Ljava/lang/reflect/Method;
    :goto_0
    return v2

    .line 496
    :catch_0
    move-exception v0

    .line 497
    .local v0, "e":Ljava/lang/Throwable;
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_1

    invoke-static {v0}, Lcom/android/okhttp/internal/http/MultiratLog;->info(Ljava/lang/Throwable;)V

    :cond_1
    move v2, v3

    .line 498
    goto :goto_0
.end method

.method public static startSBUsage(JJ)Z
    .locals 8
    .param p0, "threadID"    # J
    .param p2, "filesize"    # J

    .prologue
    const/4 v3, 0x0

    .line 450
    :try_start_0
    invoke-static {}, Lcom/android/okhttp/internal/http/SBServiceAPI;->getService()Z

    .line 451
    sget-object v2, Lcom/android/okhttp/internal/http/SBServiceAPI;->sIConnectivityManagerClass:Ljava/lang/Class;

    const-string v4, "startSBUsage"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    sget-object v7, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v7, v5, v6

    const/4 v6, 0x1

    sget-object v7, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v7, v5, v6

    invoke-virtual {v2, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 453
    .local v1, "startSBUsage":Ljava/lang/reflect/Method;
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SBServiceAPI: startSBUsage("

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/okhttp/internal/http/MultiratLog;->info(Ljava/lang/String;)V

    .line 454
    :cond_0
    sget-object v2, Lcom/android/okhttp/internal/http/SBServiceAPI;->sIConnectivityManager:Ljava/lang/Object;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v1, v2, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 458
    .end local v1    # "startSBUsage":Ljava/lang/reflect/Method;
    :goto_0
    return v2

    .line 456
    :catch_0
    move-exception v0

    .line 457
    .local v0, "e":Ljava/lang/Throwable;
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_1

    invoke-static {v0}, Lcom/android/okhttp/internal/http/MultiratLog;->info(Ljava/lang/Throwable;)V

    :cond_1
    move v2, v3

    .line 458
    goto :goto_0
.end method

.method public static startSBUsageURL(JJLjava/lang/String;)Z
    .locals 8
    .param p0, "threadID"    # J
    .param p2, "filesize"    # J
    .param p4, "url"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 470
    :try_start_0
    invoke-static {}, Lcom/android/okhttp/internal/http/SBServiceAPI;->getService()Z

    .line 471
    sget-object v2, Lcom/android/okhttp/internal/http/SBServiceAPI;->sIConnectivityManagerClass:Ljava/lang/Class;

    const-string v4, "startSBUsageURL"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    sget-object v7, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v7, v5, v6

    const/4 v6, 0x1

    sget-object v7, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-class v7, Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-virtual {v2, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 473
    .local v1, "startSBUsageURL":Ljava/lang/reflect/Method;
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SBServiceAPI: startSBUsageURL("

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", ???"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/okhttp/internal/http/MultiratLog;->info(Ljava/lang/String;)V

    .line 474
    :cond_0
    sget-object v2, Lcom/android/okhttp/internal/http/SBServiceAPI;->sIConnectivityManager:Ljava/lang/Object;

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    aput-object p4, v4, v5

    invoke-virtual {v1, v2, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 478
    .end local v1    # "startSBUsageURL":Ljava/lang/reflect/Method;
    :goto_0
    return v2

    .line 476
    :catch_0
    move-exception v0

    .line 477
    .local v0, "e":Ljava/lang/Throwable;
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_1

    invoke-static {v0}, Lcom/android/okhttp/internal/http/MultiratLog;->info(Ljava/lang/Throwable;)V

    :cond_1
    move v2, v3

    .line 478
    goto :goto_0
.end method

.method public static stopSBUsage(J)I
    .locals 8
    .param p0, "threadID"    # J

    .prologue
    const/4 v3, 0x0

    .line 528
    :try_start_0
    invoke-static {}, Lcom/android/okhttp/internal/http/SBServiceAPI;->getService()Z

    .line 529
    sget-object v2, Lcom/android/okhttp/internal/http/SBServiceAPI;->sIConnectivityManagerClass:Ljava/lang/Class;

    const-string v4, "stopSBUsage"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    sget-object v7, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v7, v5, v6

    invoke-virtual {v2, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 531
    .local v1, "stopSBUsage":Ljava/lang/reflect/Method;
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SBServiceAPI: stopSBUsage("

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/okhttp/internal/http/MultiratLog;->info(Ljava/lang/String;)V

    .line 532
    :cond_0
    sget-object v2, Lcom/android/okhttp/internal/http/SBServiceAPI;->sIConnectivityManager:Ljava/lang/Object;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v1, v2, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 536
    .end local v1    # "stopSBUsage":Ljava/lang/reflect/Method;
    :goto_0
    return v2

    .line 534
    :catch_0
    move-exception v0

    .line 535
    .local v0, "e":Ljava/lang/Throwable;
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_1

    invoke-static {v0}, Lcom/android/okhttp/internal/http/MultiratLog;->info(Ljava/lang/Throwable;)V

    :cond_1
    move v2, v3

    .line 536
    goto :goto_0
.end method

.method public static stopSBUsageWithReason(JI)I
    .locals 8
    .param p0, "threadID"    # J
    .param p2, "errorNo"    # I

    .prologue
    const/4 v3, 0x0

    .line 547
    :try_start_0
    invoke-static {}, Lcom/android/okhttp/internal/http/SBServiceAPI;->getService()Z

    .line 548
    sget-object v2, Lcom/android/okhttp/internal/http/SBServiceAPI;->sIConnectivityManagerClass:Ljava/lang/Class;

    const-string v4, "stopSBUsageWithReason"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    sget-object v7, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v7, v5, v6

    const/4 v6, 0x1

    sget-object v7, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v7, v5, v6

    invoke-virtual {v2, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 550
    .local v1, "stopSBUsageWithReason":Ljava/lang/reflect/Method;
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SBServiceAPI: stopSBUsageWithReason("

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/okhttp/internal/http/MultiratLog;->info(Ljava/lang/String;)V

    .line 551
    :cond_0
    sget-object v2, Lcom/android/okhttp/internal/http/SBServiceAPI;->sIConnectivityManager:Ljava/lang/Object;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v1, v2, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 555
    .end local v1    # "stopSBUsageWithReason":Ljava/lang/reflect/Method;
    :goto_0
    return v2

    .line 553
    :catch_0
    move-exception v0

    .line 554
    .local v0, "e":Ljava/lang/Throwable;
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_1

    invoke-static {v0}, Lcom/android/okhttp/internal/http/MultiratLog;->info(Ljava/lang/Throwable;)V

    :cond_1
    move v2, v3

    .line 555
    goto :goto_0
.end method

.method public static submitMultiSocketData([J[J)V
    .locals 7
    .param p0, "lens"    # [J
    .param p1, "times"    # [J

    .prologue
    .line 300
    :try_start_0
    invoke-static {}, Lcom/android/okhttp/internal/http/SBServiceAPI;->getService()Z

    .line 303
    sget-object v2, Lcom/android/okhttp/internal/http/SBServiceAPI;->sIConnectivityManagerClass:Ljava/lang/Class;

    const-string v3, "submitMultiSocketData"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    const-class v6, [J

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-class v6, [J

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 305
    .local v1, "submitData":Ljava/lang/reflect/Method;
    sget-object v2, Lcom/android/okhttp/internal/http/SBServiceAPI;->sIConnectivityManager:Ljava/lang/Object;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p0, v3, v4

    const/4 v4, 0x1

    aput-object p1, v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 306
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_0

    const-string v2, "SBServiceAPI: submitMultiSocketData"

    invoke-static {v2}, Lcom/android/okhttp/internal/http/MultiratLog;->info(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 311
    .end local v1    # "submitData":Ljava/lang/reflect/Method;
    :cond_0
    :goto_0
    return-void

    .line 308
    :catch_0
    move-exception v0

    .line 309
    .local v0, "e":Ljava/lang/Throwable;
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_0

    invoke-static {v0}, Lcom/android/okhttp/internal/http/MultiratLog;->info(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
