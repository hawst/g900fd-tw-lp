.class public final Lcom/android/okhttp/internal/http/HttpTransport;
.super Ljava/lang/Object;
.source "HttpTransport.java"

# interfaces
.implements Lcom/android/okhttp/internal/http/Transport;


# instance fields
.field private final httpConnection:Lcom/android/okhttp/internal/http/HttpConnection;

.field private final httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

.field private isMultirat:Z


# direct methods
.method public constructor <init>(Lcom/android/okhttp/internal/http/HttpEngine;Lcom/android/okhttp/internal/http/HttpConnection;)V
    .locals 1
    .param p1, "httpEngine"    # Lcom/android/okhttp/internal/http/HttpEngine;
    .param p2, "httpConnection"    # Lcom/android/okhttp/internal/http/HttpConnection;

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/okhttp/internal/http/HttpTransport;->isMultirat:Z

    .line 38
    iput-object p1, p0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    .line 39
    iput-object p2, p0, Lcom/android/okhttp/internal/http/HttpTransport;->httpConnection:Lcom/android/okhttp/internal/http/HttpConnection;

    .line 40
    return-void
.end method


# virtual methods
.method public canReuseConnection()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 132
    sget-boolean v1, Lcom/android/okhttp/internal/http/HttpEngine;->SMARTBONDING_ENABLED:Z

    if-eqz v1, :cond_2

    .line 133
    sget-boolean v1, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    iget-object v1, v1, Lcom/android/okhttp/internal/http/HttpEngine;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "canReuseConnection, isMultirat = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/android/okhttp/internal/http/HttpTransport;->isMultirat:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 134
    :cond_0
    iget-boolean v1, p0, Lcom/android/okhttp/internal/http/HttpTransport;->isMultirat:Z

    if-eqz v1, :cond_2

    .line 154
    :cond_1
    :goto_0
    return v0

    .line 141
    :cond_2
    const-string v1, "close"

    iget-object v2, p0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    invoke-virtual {v2}, Lcom/android/okhttp/internal/http/HttpEngine;->getRequest()Lcom/android/okhttp/Request;

    move-result-object v2

    const-string v3, "Connection"

    invoke-virtual {v2, v3}, Lcom/android/okhttp/Request;->header(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 146
    const-string v1, "close"

    iget-object v2, p0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    invoke-virtual {v2}, Lcom/android/okhttp/internal/http/HttpEngine;->getResponse()Lcom/android/okhttp/Response;

    move-result-object v2

    const-string v3, "Connection"

    invoke-virtual {v2, v3}, Lcom/android/okhttp/Response;->header(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 150
    iget-object v1, p0, Lcom/android/okhttp/internal/http/HttpTransport;->httpConnection:Lcom/android/okhttp/internal/http/HttpConnection;

    invoke-virtual {v1}, Lcom/android/okhttp/internal/http/HttpConnection;->isClosed()Z

    move-result v1

    if-nez v1, :cond_1

    .line 154
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public createRequestBody(Lcom/android/okhttp/Request;)Lcom/android/okio/Sink;
    .locals 6
    .param p1, "request"    # Lcom/android/okhttp/Request;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v4, -0x1

    .line 43
    invoke-static {p1}, Lcom/android/okhttp/internal/http/OkHeaders;->contentLength(Lcom/android/okhttp/Request;)J

    move-result-wide v0

    .line 45
    .local v0, "contentLength":J
    iget-object v2, p0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    iget-boolean v2, v2, Lcom/android/okhttp/internal/http/HttpEngine;->bufferRequestBody:Z

    if-eqz v2, :cond_2

    .line 46
    const-wide/32 v2, 0x7fffffff

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 47
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Use setFixedLengthStreamingMode() or setChunkedStreamingMode() for requests larger than 2 GiB."

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 51
    :cond_0
    cmp-long v2, v0, v4

    if-eqz v2, :cond_1

    .line 53
    invoke-virtual {p0, p1}, Lcom/android/okhttp/internal/http/HttpTransport;->writeRequestHeaders(Lcom/android/okhttp/Request;)V

    .line 54
    new-instance v2, Lcom/android/okhttp/internal/http/RetryableSink;

    long-to-int v3, v0

    invoke-direct {v2, v3}, Lcom/android/okhttp/internal/http/RetryableSink;-><init>(I)V

    .line 72
    :goto_0
    return-object v2

    .line 59
    :cond_1
    new-instance v2, Lcom/android/okhttp/internal/http/RetryableSink;

    invoke-direct {v2}, Lcom/android/okhttp/internal/http/RetryableSink;-><init>()V

    goto :goto_0

    .line 63
    :cond_2
    const-string v2, "chunked"

    const-string v3, "Transfer-Encoding"

    invoke-virtual {p1, v3}, Lcom/android/okhttp/Request;->header(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 65
    invoke-virtual {p0, p1}, Lcom/android/okhttp/internal/http/HttpTransport;->writeRequestHeaders(Lcom/android/okhttp/Request;)V

    .line 66
    iget-object v2, p0, Lcom/android/okhttp/internal/http/HttpTransport;->httpConnection:Lcom/android/okhttp/internal/http/HttpConnection;

    invoke-virtual {v2}, Lcom/android/okhttp/internal/http/HttpConnection;->newChunkedSink()Lcom/android/okio/Sink;

    move-result-object v2

    goto :goto_0

    .line 69
    :cond_3
    cmp-long v2, v0, v4

    if-eqz v2, :cond_4

    .line 71
    invoke-virtual {p0, p1}, Lcom/android/okhttp/internal/http/HttpTransport;->writeRequestHeaders(Lcom/android/okhttp/Request;)V

    .line 72
    iget-object v2, p0, Lcom/android/okhttp/internal/http/HttpTransport;->httpConnection:Lcom/android/okhttp/internal/http/HttpConnection;

    invoke-virtual {v2, v0, v1}, Lcom/android/okhttp/internal/http/HttpConnection;->newFixedLengthSink(J)Lcom/android/okio/Sink;

    move-result-object v2

    goto :goto_0

    .line 75
    :cond_4
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Cannot stream a request body without chunked encoding or a known content length!"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public emptyTransferStream()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 158
    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpTransport;->httpConnection:Lcom/android/okhttp/internal/http/HttpConnection;

    invoke-virtual {v0}, Lcom/android/okhttp/internal/http/HttpConnection;->emptyResponseBody()V

    .line 159
    return-void
.end method

.method public flushRequest()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 80
    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpTransport;->httpConnection:Lcom/android/okhttp/internal/http/HttpConnection;

    invoke-virtual {v0}, Lcom/android/okhttp/internal/http/HttpConnection;->flush()V

    .line 81
    return-void
.end method

.method public getTransferStream(Ljava/net/CacheRequest;)Lcom/android/okio/Source;
    .locals 47
    .param p1, "cacheRequest"    # Ljava/net/CacheRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 162
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    invoke-virtual {v6}, Lcom/android/okhttp/internal/http/HttpEngine;->hasResponseBody()Z

    move-result v6

    if-nez v6, :cond_0

    .line 163
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/okhttp/internal/http/HttpTransport;->httpConnection:Lcom/android/okhttp/internal/http/HttpConnection;

    const-wide/16 v8, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v6, v0, v8, v9}, Lcom/android/okhttp/internal/http/HttpConnection;->newFixedLengthSource(Ljava/net/CacheRequest;J)Lcom/android/okio/Source;

    move-result-object v6

    .line 309
    :goto_0
    return-object v6

    .line 166
    :cond_0
    const-string v6, "chunked"

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    invoke-virtual {v7}, Lcom/android/okhttp/internal/http/HttpEngine;->getResponse()Lcom/android/okhttp/Response;

    move-result-object v7

    const-string v8, "Transfer-Encoding"

    invoke-virtual {v7, v8}, Lcom/android/okhttp/Response;->header(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 167
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/okhttp/internal/http/HttpTransport;->httpConnection:Lcom/android/okhttp/internal/http/HttpConnection;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    move-object/from16 v0, p1

    invoke-virtual {v6, v0, v7}, Lcom/android/okhttp/internal/http/HttpConnection;->newChunkedSource(Ljava/net/CacheRequest;Lcom/android/okhttp/internal/http/HttpEngine;)Lcom/android/okio/Source;

    move-result-object v6

    goto :goto_0

    .line 170
    :cond_1
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    invoke-virtual {v6}, Lcom/android/okhttp/internal/http/HttpEngine;->getResponse()Lcom/android/okhttp/Response;

    move-result-object v6

    invoke-static {v6}, Lcom/android/okhttp/internal/http/OkHeaders;->contentLength(Lcom/android/okhttp/Response;)J

    move-result-wide v28

    .line 172
    .local v28, "contentLength":J
    sget-boolean v6, Lcom/android/okhttp/internal/http/HttpEngine;->SMARTBONDING_ENABLED:Z

    if-eqz v6, :cond_a

    .line 174
    :try_start_0
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    invoke-virtual {v6}, Lcom/android/okhttp/internal/http/HttpEngine;->getConnection()Lcom/android/okhttp/Connection;

    move-result-object v6

    if-eqz v6, :cond_1a

    .line 175
    move-wide/from16 v10, v28

    .line 176
    .local v10, "len":J
    sget-boolean v6, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v6, :cond_2

    .line 177
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    iget-object v6, v6, Lcom/android/okhttp/internal/http/HttpEngine;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "get TransferStream : responseHeaders.getContentLength()="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", method="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    invoke-virtual {v8}, Lcom/android/okhttp/internal/http/HttpEngine;->getRequest()Lcom/android/okhttp/Request;

    move-result-object v8

    invoke-virtual {v8}, Lcom/android/okhttp/Request;->method()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", HTTPVersion="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    invoke-virtual {v8}, Lcom/android/okhttp/internal/http/HttpEngine;->getConnection()Lcom/android/okhttp/Connection;

    move-result-object v8

    invoke-virtual {v8}, Lcom/android/okhttp/Connection;->getHttpMinorVersion()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 179
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    iget-object v6, v6, Lcom/android/okhttp/internal/http/HttpEngine;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "get TransferStream : main connection : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    invoke-virtual {v8}, Lcom/android/okhttp/internal/http/HttpEngine;->getConnection()Lcom/android/okhttp/Connection;

    move-result-object v8

    invoke-virtual {v8}, Lcom/android/okhttp/Connection;->getSocket()Ljava/net/Socket;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 180
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    iget-object v6, v6, Lcom/android/okhttp/internal/http/HttpEngine;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "get TransferStream : Is HTTPS SSL Socket "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    invoke-virtual {v8}, Lcom/android/okhttp/internal/http/HttpEngine;->getConnection()Lcom/android/okhttp/Connection;

    move-result-object v8

    invoke-virtual {v8}, Lcom/android/okhttp/Connection;->isSSLSocket()Z

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 182
    :cond_2
    const/16 v17, 0x0

    .line 183
    .local v17, "conClose":Z
    const-wide/16 v6, -0x1

    cmp-long v6, v10, v6

    if-eqz v6, :cond_a

    .line 184
    sget-boolean v6, Lcom/android/okhttp/internal/http/HttpEngine;->SMARTBONDING_ENABLED:Z

    if-eqz v6, :cond_a

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    invoke-virtual {v6}, Lcom/android/okhttp/internal/http/HttpEngine;->getRequest()Lcom/android/okhttp/Request;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/okhttp/Request;->method()Ljava/lang/String;

    move-result-object v6

    const-string v7, "GET"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_a

    const-wide/32 v6, 0x200000

    cmp-long v6, v10, v6

    if-ltz v6, :cond_a

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    invoke-virtual {v6}, Lcom/android/okhttp/internal/http/HttpEngine;->getConnection()Lcom/android/okhttp/Connection;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/okhttp/Connection;->isValidSocketForMultiRAT()Z

    move-result v6

    if-eqz v6, :cond_a

    .line 189
    const-wide/16 v15, 0x0

    .line 190
    .local v15, "offset":J
    const/16 v26, 0x1

    .line 191
    .local v26, "bMultiSocket":Z
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    invoke-virtual {v6}, Lcom/android/okhttp/internal/http/HttpEngine;->getRequest()Lcom/android/okhttp/Request;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/okhttp/Request;->getHeaders()Lcom/android/okhttp/Headers;

    move-result-object v37

    .line 192
    .local v37, "reqHeaders":Lcom/android/okhttp/Headers;
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    invoke-virtual {v6}, Lcom/android/okhttp/internal/http/HttpEngine;->getResponse()Lcom/android/okhttp/Response;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/okhttp/Response;->headers()Lcom/android/okhttp/Headers;

    move-result-object v39

    .line 193
    .local v39, "rspHeaders":Lcom/android/okhttp/Headers;
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    invoke-virtual {v6}, Lcom/android/okhttp/internal/http/HttpEngine;->getResponse()Lcom/android/okhttp/Response;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/okhttp/Response;->code()I

    move-result v38

    .line 194
    .local v38, "rspCode":I
    const-string v6, "Accept-Ranges"

    move-object/from16 v0, v39

    invoke-virtual {v0, v6}, Lcom/android/okhttp/Headers;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 195
    .local v4, "accRange":Ljava/lang/String;
    invoke-static {}, Lcom/android/okhttp/internal/http/SBServiceAPI;->getNBEnabled()Z

    move-result v6

    if-nez v6, :cond_b

    .line 196
    sget-boolean v6, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v6, :cond_3

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    iget-object v6, v6, Lcom/android/okhttp/internal/http/HttpEngine;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    const-string v7, "Download Booster Enabled Status in Setting is false"

    invoke-virtual {v6, v7}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 197
    :cond_3
    const/16 v26, 0x0

    .line 257
    :cond_4
    :goto_1
    sget-boolean v6, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v6, :cond_5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    iget-object v6, v6, Lcom/android/okhttp/internal/http/HttpEngine;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "get TransferStream : after getSBEnabled bMultiSocket="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, v26

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 258
    :cond_5
    if-eqz v26, :cond_a

    .line 259
    move/from16 v0, v17

    invoke-static {v10, v11, v0}, Lcom/android/okhttp/internal/http/MultiratUtil;->getBlockSize(JZ)J

    move-result-wide v12

    .line 260
    .local v12, "blockSize":J
    move-object/from16 v0, v39

    invoke-static {v0, v10, v11}, Lcom/android/okhttp/internal/http/MultiratUtil;->getFullContentLength(Lcom/android/okhttp/Headers;J)J

    move-result-wide v18

    .line 261
    .local v18, "fullContentLen":J
    sget-boolean v6, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v6, :cond_6

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    iget-object v6, v6, Lcom/android/okhttp/internal/http/HttpEngine;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "get TransferStream : new MultiSocketInputStream(connection, cacheRequest, this, responseHeaders.getContentLength()), blockSize="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", fullConSize="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-wide/from16 v0, v18

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 263
    :cond_6
    const/16 v24, 0x0

    .line 264
    .local v24, "cacheType":I
    new-instance v5, Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/okhttp/internal/http/HttpTransport;->httpConnection:Lcom/android/okhttp/internal/http/HttpConnection;

    invoke-virtual {v6}, Lcom/android/okhttp/internal/http/HttpConnection;->getSource()Lcom/android/okio/BufferedSource;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    invoke-virtual {v7}, Lcom/android/okhttp/internal/http/HttpEngine;->getConnection()Lcom/android/okhttp/Connection;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/okhttp/Connection;->getSocket()Ljava/net/Socket;

    move-result-object v7

    invoke-virtual {v7}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    invoke-virtual {v8}, Lcom/android/okhttp/internal/http/HttpEngine;->getRequest()Lcom/android/okhttp/Request;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    iget-wide v0, v8, Lcom/android/okhttp/internal/http/HttpEngine;->mainResponseTime:J

    move-wide/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    invoke-virtual {v8}, Lcom/android/okhttp/internal/http/HttpEngine;->getConnection()Lcom/android/okhttp/Connection;

    move-result-object v8

    invoke-virtual {v8}, Lcom/android/okhttp/Connection;->getSocket()Ljava/net/Socket;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    iget-object v8, v8, Lcom/android/okhttp/internal/http/HttpEngine;->mOriRequestHeader:Lcom/android/okhttp/Request;

    invoke-virtual {v8}, Lcom/android/okhttp/Request;->newBuilder()Lcom/android/okhttp/Request$Builder;

    move-result-object v8

    invoke-virtual {v8}, Lcom/android/okhttp/Request$Builder;->build()Lcom/android/okhttp/Request;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    iget-object v0, v8, Lcom/android/okhttp/internal/http/HttpEngine;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    move-object/from16 v25, v0

    move-object/from16 v8, p1

    invoke-direct/range {v5 .. v25}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;-><init>(Lcom/android/okio/BufferedSource;Ljava/io/InputStream;Ljava/net/CacheRequest;Lcom/android/okhttp/internal/http/HttpEngine;JJLcom/android/okhttp/Request;JZJJLjava/net/Socket;Lcom/android/okhttp/Request;ILcom/android/okhttp/internal/http/MultiratLog;)V

    .line 269
    .local v5, "mS":Lcom/android/okhttp/internal/http/MultiSocketInputStream;
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    invoke-virtual {v6}, Lcom/android/okhttp/internal/http/HttpEngine;->getConnection()Lcom/android/okhttp/Connection;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    invoke-virtual {v7}, Lcom/android/okhttp/internal/http/HttpEngine;->getRequest()Lcom/android/okhttp/Request;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/okhttp/Request;->uri()Ljava/net/URI;

    move-result-object v7

    invoke-virtual {v6, v5, v7}, Lcom/android/okhttp/Connection;->setMultiSocketInputStream(Lcom/android/okhttp/internal/http/MultiSocketInputStream;Ljava/net/URI;)V

    .line 270
    sget-boolean v6, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v6, :cond_7

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    iget-object v6, v6, Lcom/android/okhttp/internal/http/HttpEngine;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "get TransferStream : new MultiSocketInputStream() result instance "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 272
    :cond_7
    :try_start_1
    const-string v6, "NETWORKBOOSTER_LOCAL_FILE_TAG_UID"

    move-object/from16 v0, v37

    invoke-virtual {v0, v6}, Lcom/android/okhttp/Headers;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v45

    .line 273
    .local v45, "taguid":Ljava/lang/String;
    sget-boolean v6, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v6, :cond_8

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    iget-object v6, v6, Lcom/android/okhttp/internal/http/HttpEngine;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "get NETWORKBOOSTER_LOCAL_FILE_TAG_UID field "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v45

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 274
    :cond_8
    if-eqz v45, :cond_9

    invoke-virtual/range {v45 .. v45}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_9

    .line 275
    const-string v6, ":"

    move-object/from16 v0, v45

    invoke-virtual {v0, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v35

    .line 276
    .local v35, "index":I
    const/4 v6, 0x0

    move-object/from16 v0, v45

    move/from16 v1, v35

    invoke-virtual {v0, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v42

    .line 277
    .local v42, "sTag":Ljava/lang/String;
    add-int/lit8 v6, v35, 0x1

    move-object/from16 v0, v45

    invoke-virtual {v0, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v43

    .line 278
    .local v43, "sUid":Ljava/lang/String;
    invoke-static/range {v42 .. v42}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v44

    .line 279
    .local v44, "tag":I
    invoke-static/range {v43 .. v43}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v46

    .line 280
    .local v46, "uid":I
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    invoke-virtual {v6}, Lcom/android/okhttp/internal/http/HttpEngine;->getConnection()Lcom/android/okhttp/Connection;

    move-result-object v6

    move/from16 v0, v44

    move/from16 v1, v46

    invoke-virtual {v6, v0, v1}, Lcom/android/okhttp/Connection;->setTagUid(II)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_2

    .line 286
    .end local v35    # "index":I
    .end local v42    # "sTag":Ljava/lang/String;
    .end local v43    # "sUid":Ljava/lang/String;
    .end local v44    # "tag":I
    .end local v45    # "taguid":Ljava/lang/String;
    .end local v46    # "uid":I
    :cond_9
    :goto_2
    const/4 v6, 0x1

    :try_start_2
    move-object/from16 v0, p0

    iput-boolean v6, v0, Lcom/android/okhttp/internal/http/HttpTransport;->isMultirat:Z

    .line 287
    new-instance v6, Lcom/android/okhttp/internal/http/MultiratSource;

    invoke-direct {v6, v5}, Lcom/android/okhttp/internal/http/MultiratSource;-><init>(Lcom/android/okhttp/internal/http/MultiSocketInputStream;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0

    .line 296
    .end local v4    # "accRange":Ljava/lang/String;
    .end local v5    # "mS":Lcom/android/okhttp/internal/http/MultiSocketInputStream;
    .end local v10    # "len":J
    .end local v12    # "blockSize":J
    .end local v15    # "offset":J
    .end local v17    # "conClose":Z
    .end local v18    # "fullContentLen":J
    .end local v24    # "cacheType":I
    .end local v26    # "bMultiSocket":Z
    .end local v37    # "reqHeaders":Lcom/android/okhttp/Headers;
    .end local v38    # "rspCode":I
    .end local v39    # "rspHeaders":Lcom/android/okhttp/Headers;
    :catch_0
    move-exception v27

    .line 297
    .local v27, "e":Ljava/lang/Throwable;
    sget-boolean v6, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v6, :cond_a

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    iget-object v6, v6, Lcom/android/okhttp/internal/http/HttpEngine;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    move-object/from16 v0, v27

    invoke-virtual {v6, v0}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V

    .line 302
    .end local v27    # "e":Ljava/lang/Throwable;
    :cond_a
    :goto_3
    const-wide/16 v6, -0x1

    cmp-long v6, v28, v6

    if-eqz v6, :cond_1b

    .line 303
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/okhttp/internal/http/HttpTransport;->httpConnection:Lcom/android/okhttp/internal/http/HttpConnection;

    move-object/from16 v0, p1

    move-wide/from16 v1, v28

    invoke-virtual {v6, v0, v1, v2}, Lcom/android/okhttp/internal/http/HttpConnection;->newFixedLengthSource(Ljava/net/CacheRequest;J)Lcom/android/okio/Source;

    move-result-object v6

    goto/16 :goto_0

    .line 200
    .restart local v4    # "accRange":Ljava/lang/String;
    .restart local v10    # "len":J
    .restart local v15    # "offset":J
    .restart local v17    # "conClose":Z
    .restart local v26    # "bMultiSocket":Z
    .restart local v37    # "reqHeaders":Lcom/android/okhttp/Headers;
    .restart local v38    # "rspCode":I
    .restart local v39    # "rspHeaders":Lcom/android/okhttp/Headers;
    :cond_b
    const/16 v6, 0xc8

    move/from16 v0, v38

    if-eq v0, v6, :cond_d

    const/16 v6, 0xce

    move/from16 v0, v38

    if-eq v0, v6, :cond_d

    .line 201
    :try_start_3
    sget-boolean v6, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v6, :cond_c

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    iget-object v6, v6, Lcom/android/okhttp/internal/http/HttpEngine;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "get TransferStream : response code is not positive : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move/from16 v0, v38

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 202
    :cond_c
    const/16 v26, 0x0

    goto/16 :goto_1

    .line 204
    :cond_d
    if-eqz v4, :cond_f

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    const-string v7, "none"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_f

    .line 205
    sget-boolean v6, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v6, :cond_e

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    iget-object v6, v6, Lcom/android/okhttp/internal/http/HttpEngine;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    const-string v7, "get TransferStream : Accept-Ranges : none"

    invoke-virtual {v6, v7}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 206
    :cond_e
    const/16 v26, 0x0

    goto/16 :goto_1

    .line 209
    :cond_f
    const-string v6, "Connection"

    move-object/from16 v0, v39

    invoke-virtual {v0, v6}, Lcom/android/okhttp/Headers;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v40

    .line 210
    .local v40, "sCon":Ljava/lang/String;
    if-eqz v40, :cond_10

    invoke-virtual/range {v40 .. v40}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    const-string v7, "close"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_10

    .line 211
    const/16 v17, 0x1

    .line 212
    sget-boolean v6, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v6, :cond_10

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    iget-object v6, v6, Lcom/android/okhttp/internal/http/HttpEngine;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    const-string v7, "get TransferStream : connection will close each time"

    invoke-virtual {v6, v7}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 214
    :cond_10
    const-string v6, "range"

    move-object/from16 v0, v37

    invoke-virtual {v0, v6}, Lcom/android/okhttp/Headers;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v36

    .line 215
    .local v36, "ranges":Ljava/lang/String;
    if-eqz v36, :cond_19

    .line 216
    invoke-virtual/range {v36 .. v36}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v36

    .line 218
    const-string v6, "bytes"

    move-object/from16 v0, v36

    invoke-virtual {v0, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_17

    .line 219
    const-string v6, "="

    move-object/from16 v0, v36

    invoke-virtual {v0, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v31

    .line 220
    .local v31, "i1":I
    const-string v6, "-"

    move-object/from16 v0, v36

    invoke-virtual {v0, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v32

    .line 221
    .local v32, "i2":I
    const-string v6, "-"

    move-object/from16 v0, v36

    invoke-virtual {v0, v6}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v33

    .line 222
    .local v33, "i3":I
    const-string v6, ","

    move-object/from16 v0, v36

    invoke-virtual {v0, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v34

    .line 223
    .local v34, "i4":I
    if-ltz v31, :cond_11

    if-ltz v32, :cond_11

    add-int/lit8 v6, v31, 0x1

    move/from16 v0, v32

    if-ge v6, v0, :cond_11

    move/from16 v0, v32

    move/from16 v1, v33

    if-ne v0, v1, :cond_11

    if-ltz v34, :cond_13

    .line 224
    :cond_11
    sget-boolean v6, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v6, :cond_12

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    iget-object v6, v6, Lcom/android/okhttp/internal/http/HttpEngine;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "get TransferStream : a unexpected range request : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v36

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 225
    :cond_12
    const/16 v26, 0x0

    goto/16 :goto_1

    .line 228
    :cond_13
    add-int/lit8 v6, v31, 0x1

    move-object/from16 v0, v36

    move/from16 v1, v32

    invoke-virtual {v0, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v41

    .line 229
    .local v41, "sOffset":Ljava/lang/String;
    if-eqz v41, :cond_14

    invoke-virtual/range {v41 .. v41}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-nez v6, :cond_16

    .line 230
    :cond_14
    sget-boolean v6, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v6, :cond_15

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    iget-object v6, v6, Lcom/android/okhttp/internal/http/HttpEngine;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "get TransferStream : a unexpected range request : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v36

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0

    .line 231
    :cond_15
    const/16 v26, 0x0

    goto/16 :goto_1

    .line 235
    :cond_16
    :try_start_4
    invoke-static/range {v41 .. v41}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1

    move-result-wide v15

    .line 236
    const/16 v26, 0x1

    goto/16 :goto_1

    .line 238
    :catch_1
    move-exception v27

    .line 239
    .restart local v27    # "e":Ljava/lang/Throwable;
    const/16 v26, 0x0

    .line 240
    :try_start_5
    sget-boolean v6, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v6, :cond_4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    iget-object v6, v6, Lcom/android/okhttp/internal/http/HttpEngine;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    move-object/from16 v0, v27

    invoke-virtual {v6, v0}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V

    goto/16 :goto_1

    .line 247
    .end local v27    # "e":Ljava/lang/Throwable;
    .end local v31    # "i1":I
    .end local v32    # "i2":I
    .end local v33    # "i3":I
    .end local v34    # "i4":I
    .end local v41    # "sOffset":Ljava/lang/String;
    :cond_17
    sget-boolean v6, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v6, :cond_18

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    iget-object v6, v6, Lcom/android/okhttp/internal/http/HttpEngine;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "get TransferStream : a unexpected range request : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v36

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 248
    :cond_18
    const/16 v26, 0x0

    goto/16 :goto_1

    .line 252
    :cond_19
    const/16 v26, 0x1

    .line 253
    const-wide/16 v15, 0x0

    goto/16 :goto_1

    .line 283
    .end local v36    # "ranges":Ljava/lang/String;
    .end local v40    # "sCon":Ljava/lang/String;
    .restart local v5    # "mS":Lcom/android/okhttp/internal/http/MultiSocketInputStream;
    .restart local v12    # "blockSize":J
    .restart local v18    # "fullContentLen":J
    .restart local v24    # "cacheType":I
    :catch_2
    move-exception v30

    .line 284
    .local v30, "exx":Ljava/lang/Throwable;
    sget-boolean v6, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v6, :cond_9

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    iget-object v6, v6, Lcom/android/okhttp/internal/http/HttpEngine;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    move-object/from16 v0, v30

    invoke-virtual {v6, v0}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V

    goto/16 :goto_2

    .line 293
    .end local v4    # "accRange":Ljava/lang/String;
    .end local v5    # "mS":Lcom/android/okhttp/internal/http/MultiSocketInputStream;
    .end local v10    # "len":J
    .end local v12    # "blockSize":J
    .end local v15    # "offset":J
    .end local v17    # "conClose":Z
    .end local v18    # "fullContentLen":J
    .end local v24    # "cacheType":I
    .end local v26    # "bMultiSocket":Z
    .end local v30    # "exx":Ljava/lang/Throwable;
    .end local v37    # "reqHeaders":Lcom/android/okhttp/Headers;
    .end local v38    # "rspCode":I
    .end local v39    # "rspHeaders":Lcom/android/okhttp/Headers;
    :cond_1a
    sget-boolean v6, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v6, :cond_a

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    iget-object v6, v6, Lcom/android/okhttp/internal/http/HttpEngine;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    const-string v7, "get TransferStream : httpEngine.connection is null "

    invoke-virtual {v6, v7}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_0

    goto/16 :goto_3

    .line 309
    :cond_1b
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/okhttp/internal/http/HttpTransport;->httpConnection:Lcom/android/okhttp/internal/http/HttpConnection;

    move-object/from16 v0, p1

    invoke-virtual {v6, v0}, Lcom/android/okhttp/internal/http/HttpConnection;->newUnknownLengthSource(Ljava/net/CacheRequest;)Lcom/android/okio/Source;

    move-result-object v6

    goto/16 :goto_0
.end method

.method public readResponseHeaders()Lcom/android/okhttp/Response$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 119
    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpTransport;->httpConnection:Lcom/android/okhttp/internal/http/HttpConnection;

    invoke-virtual {v0}, Lcom/android/okhttp/internal/http/HttpConnection;->readResponse()Lcom/android/okhttp/Response$Builder;

    move-result-object v0

    return-object v0
.end method

.method public releaseConnectionOnIdle()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/android/okhttp/internal/http/HttpTransport;->canReuseConnection()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 124
    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpTransport;->httpConnection:Lcom/android/okhttp/internal/http/HttpConnection;

    invoke-virtual {v0}, Lcom/android/okhttp/internal/http/HttpConnection;->poolOnIdle()V

    .line 128
    :goto_0
    return-void

    .line 126
    :cond_0
    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpTransport;->httpConnection:Lcom/android/okhttp/internal/http/HttpConnection;

    invoke-virtual {v0}, Lcom/android/okhttp/internal/http/HttpConnection;->closeOnIdle()V

    goto :goto_0
.end method

.method public writeRequestBody(Lcom/android/okhttp/internal/http/RetryableSink;)V
    .locals 1
    .param p1, "requestBody"    # Lcom/android/okhttp/internal/http/RetryableSink;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 84
    iget-object v0, p0, Lcom/android/okhttp/internal/http/HttpTransport;->httpConnection:Lcom/android/okhttp/internal/http/HttpConnection;

    invoke-virtual {v0, p1}, Lcom/android/okhttp/internal/http/HttpConnection;->writeRequestBody(Lcom/android/okhttp/internal/http/RetryableSink;)V

    .line 85
    return-void
.end method

.method public writeRequestHeaders(Lcom/android/okhttp/Request;)V
    .locals 4
    .param p1, "request"    # Lcom/android/okhttp/Request;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 100
    iget-object v1, p0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    invoke-virtual {v1}, Lcom/android/okhttp/internal/http/HttpEngine;->writingRequestHeaders()V

    .line 101
    iget-object v1, p0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    invoke-virtual {v1}, Lcom/android/okhttp/internal/http/HttpEngine;->getConnection()Lcom/android/okhttp/Connection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/okhttp/Connection;->getRoute()Lcom/android/okhttp/Route;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/okhttp/Route;->getProxy()Ljava/net/Proxy;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v1

    iget-object v2, p0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    invoke-virtual {v2}, Lcom/android/okhttp/internal/http/HttpEngine;->getConnection()Lcom/android/okhttp/Connection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/okhttp/Connection;->getHttpMinorVersion()I

    move-result v2

    invoke-static {p1, v1, v2}, Lcom/android/okhttp/internal/http/RequestLine;->get(Lcom/android/okhttp/Request;Ljava/net/Proxy$Type;I)Ljava/lang/String;

    move-result-object v0

    .line 105
    .local v0, "requestLine":Ljava/lang/String;
    sget-boolean v1, Lcom/android/okhttp/internal/http/HttpEngine;->SMARTBONDING_ENABLED:Z

    if-eqz v1, :cond_0

    .line 107
    :try_start_0
    sget-boolean v1, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v1, :cond_0

    .line 108
    iget-object v1, p0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    iget-object v1, v1, Lcom/android/okhttp/internal/http/HttpEngine;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "writeRequestHeaders "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 109
    iget-object v1, p0, Lcom/android/okhttp/internal/http/HttpTransport;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    iget-object v1, v1, Lcom/android/okhttp/internal/http/HttpEngine;->logger:Lcom/android/okhttp/internal/http/MultiratLog;

    invoke-virtual {p1}, Lcom/android/okhttp/Request;->getHeaders()Lcom/android/okhttp/Headers;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/okhttp/Headers;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/okhttp/internal/http/MultiratLog;->d(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 115
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/android/okhttp/internal/http/HttpTransport;->httpConnection:Lcom/android/okhttp/internal/http/HttpConnection;

    invoke-virtual {p1}, Lcom/android/okhttp/Request;->getHeaders()Lcom/android/okhttp/Headers;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/android/okhttp/internal/http/HttpConnection;->writeRequest(Lcom/android/okhttp/Headers;Ljava/lang/String;)V

    .line 116
    return-void

    .line 112
    :catch_0
    move-exception v1

    goto :goto_0
.end method
