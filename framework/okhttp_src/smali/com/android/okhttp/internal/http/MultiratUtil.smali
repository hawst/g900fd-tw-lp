.class public Lcom/android/okhttp/internal/http/MultiratUtil;
.super Ljava/lang/Object;
.source "MultiratUtil.java"


# static fields
.field public static final CMD_GET_RANGE:I = -0x66

.field public static final CMD_SET_FILE_NAME:I = -0x64

.field private static MAX_MULTIRAT_BLOCK_SIZE:I = 0x0

.field private static MAX_MULTIRAT_BLOCK_SIZE_CON:I = 0x0

.field protected static final MIN_LENGTH_START_SB:I = 0x200000

.field private static MIN_MULTIRAT_BLOCK_SIZE:I = 0x0

.field private static MIN_MULTIRAT_BLOCK_SIZE_CON:I = 0x0

.field private static MULTIRAT_BLOCK_DIV:I = 0x0

.field private static MULTIRAT_BLOCK_DIV_CON:I = 0x0

.field public static final NETWORKBOOSTER_CANNOT_USE_WIFIORMOBILE:Ljava/lang/String; = "NETWORKBOOSTER_CANNOT_USE_WIFIORMOBILE"

.field public static final NETWORKBOOSTER_LOCAL_FILE_NAME:Ljava/lang/String; = "NETWORKBOOSTER_LOCAL_FILE_NAME"

.field public static final NETWORKBOOSTER_LOCAL_FILE_RANGE:Ljava/lang/String; = "NETWORKBOOSTER_LOCAL_FILE_RANGE"

.field public static final NETWORKBOOSTER_TAG_UID:Ljava/lang/String; = "NETWORKBOOSTER_LOCAL_FILE_TAG_UID"

.field public static final RET_GET_RANGE:I = -0x67

.field public static final RET_SET_FILE_NAME:I = -0x65


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const/high16 v0, 0x100000

    sput v0, Lcom/android/okhttp/internal/http/MultiratUtil;->MIN_MULTIRAT_BLOCK_SIZE:I

    .line 27
    const/high16 v0, 0x300000

    sput v0, Lcom/android/okhttp/internal/http/MultiratUtil;->MAX_MULTIRAT_BLOCK_SIZE:I

    .line 28
    const/16 v0, 0xa

    sput v0, Lcom/android/okhttp/internal/http/MultiratUtil;->MULTIRAT_BLOCK_DIV:I

    .line 30
    const/high16 v0, 0x200000

    sput v0, Lcom/android/okhttp/internal/http/MultiratUtil;->MIN_MULTIRAT_BLOCK_SIZE_CON:I

    .line 31
    const/high16 v0, 0xa00000

    sput v0, Lcom/android/okhttp/internal/http/MultiratUtil;->MAX_MULTIRAT_BLOCK_SIZE_CON:I

    .line 32
    const/4 v0, 0x2

    sput v0, Lcom/android/okhttp/internal/http/MultiratUtil;->MULTIRAT_BLOCK_DIV_CON:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method protected static checkOffsetAndCount(III)V
    .locals 3
    .param p0, "arrayLength"    # I
    .param p1, "offset"    # I
    .param p2, "count"    # I

    .prologue
    .line 127
    or-int v0, p1, p2

    if-ltz v0, :cond_0

    if-gt p1, p0, :cond_0

    sub-int v0, p0, p1

    if-ge v0, p2, :cond_1

    .line 128
    :cond_0
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "length="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; regionStart="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; regionLength="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 131
    :cond_1
    return-void
.end method

.method public static closeQuietly(Ljava/io/Closeable;)V
    .locals 1
    .param p0, "closeable"    # Ljava/io/Closeable;

    .prologue
    .line 99
    if-eqz p0, :cond_0

    .line 101
    :try_start_0
    invoke-interface {p0}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 105
    :cond_0
    :goto_0
    return-void

    .line 102
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public static closeQuietly(Ljava/net/Socket;)V
    .locals 1
    .param p0, "closeable"    # Ljava/net/Socket;

    .prologue
    .line 113
    if-eqz p0, :cond_0

    .line 115
    :try_start_0
    invoke-virtual {p0}, Ljava/net/Socket;->close()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 119
    :cond_0
    :goto_0
    return-void

    .line 116
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected static getBlockSize(JZ)J
    .locals 4
    .param p0, "contentLen"    # J
    .param p2, "recon"    # Z

    .prologue
    .line 57
    sget v2, Lcom/android/okhttp/internal/http/MultiratUtil;->MIN_MULTIRAT_BLOCK_SIZE:I

    int-to-long v0, v2

    .line 58
    .local v0, "tmp":J
    if-eqz p2, :cond_0

    .line 59
    sget v2, Lcom/android/okhttp/internal/http/MultiratUtil;->MULTIRAT_BLOCK_DIV_CON:I

    int-to-long v2, v2

    div-long v0, p0, v2

    .line 60
    sget v2, Lcom/android/okhttp/internal/http/MultiratUtil;->MAX_MULTIRAT_BLOCK_SIZE_CON:I

    int-to-long v2, v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    .line 61
    sget v2, Lcom/android/okhttp/internal/http/MultiratUtil;->MIN_MULTIRAT_BLOCK_SIZE_CON:I

    int-to-long v2, v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 68
    :goto_0
    return-wide v0

    .line 64
    :cond_0
    sget v2, Lcom/android/okhttp/internal/http/MultiratUtil;->MULTIRAT_BLOCK_DIV:I

    int-to-long v2, v2

    div-long v0, p0, v2

    .line 65
    sget v2, Lcom/android/okhttp/internal/http/MultiratUtil;->MAX_MULTIRAT_BLOCK_SIZE:I

    int-to-long v2, v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    .line 66
    sget v2, Lcom/android/okhttp/internal/http/MultiratUtil;->MIN_MULTIRAT_BLOCK_SIZE:I

    int-to-long v2, v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    goto :goto_0
.end method

.method public static getFullContentLength(Lcom/android/okhttp/Headers;J)J
    .locals 7
    .param p0, "h"    # Lcom/android/okhttp/Headers;
    .param p1, "len"    # J

    .prologue
    .line 78
    const-string v4, "Content-range"

    invoke-virtual {p0, v4}, Lcom/android/okhttp/Headers;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 79
    .local v0, "cr":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 89
    .end local p1    # "len":J
    :cond_0
    :goto_0
    return-wide p1

    .line 80
    .restart local p1    # "len":J
    :cond_1
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    .line 81
    .local v2, "index":I
    if-ltz v2, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-eq v2, v4, :cond_0

    .line 82
    add-int/lit8 v4, v2, 0x1

    invoke-virtual {v0, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 83
    .local v3, "subS":Ljava/lang/String;
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_0

    .line 85
    :try_start_0
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide p1

    goto :goto_0

    .line 87
    :catch_0
    move-exception v1

    .line 88
    .local v1, "e":Ljava/lang/Throwable;
    sget-boolean v4, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v4, :cond_0

    invoke-static {v1}, Lcom/android/okhttp/internal/http/MultiratLog;->info(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static getRequestHeadersBytes(Lcom/android/okhttp/Headers;Ljava/lang/String;)[B
    .locals 5
    .param p0, "headers"    # Lcom/android/okhttp/Headers;
    .param p1, "requestLine"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 152
    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x100

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 153
    .local v2, "result":Ljava/lang/StringBuilder;
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\r\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 154
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/android/okhttp/Headers;->size()I

    move-result v3

    if-ge v0, v3, :cond_3

    .line 155
    sget-boolean v3, Lcom/android/okhttp/internal/http/HttpEngine;->SMARTBONDING_ENABLED:Z

    if-eqz v3, :cond_2

    .line 156
    invoke-virtual {p0, v0}, Lcom/android/okhttp/Headers;->name(I)Ljava/lang/String;

    move-result-object v1

    .line 157
    .local v1, "name":Ljava/lang/String;
    const-string v3, "NETWORKBOOSTER_LOCAL_FILE_NAME"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "NETWORKBOOSTER_LOCAL_FILE_RANGE"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "NETWORKBOOSTER_LOCAL_FILE_TAG_UID"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "NETWORKBOOSTER_CANNOT_USE_WIFIORMOBILE"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 161
    :cond_0
    sget-boolean v3, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v3, :cond_1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "This is interval interface field, ignore: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/android/okhttp/internal/http/MultiratLog;->info(Ljava/lang/String;)V

    .line 154
    .end local v1    # "name":Ljava/lang/String;
    :cond_1
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 165
    :cond_2
    invoke-virtual {p0, v0}, Lcom/android/okhttp/Headers;->name(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0, v0}, Lcom/android/okhttp/Headers;->value(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\r\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 170
    :cond_3
    const-string v3, "\r\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 171
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/android/okhttp/internal/Util;->UTF_8:Ljava/nio/charset/Charset;

    invoke-virtual {v3, v4}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v3

    return-object v3
.end method

.method public static getRequestLine(Lcom/android/okhttp/Request;Ljava/net/Proxy$Type;I)Ljava/lang/String;
    .locals 1
    .param p0, "request"    # Lcom/android/okhttp/Request;
    .param p1, "proxyType"    # Ljava/net/Proxy$Type;
    .param p2, "httpMinorVersion"    # I

    .prologue
    .line 141
    invoke-static {p0, p1, p2}, Lcom/android/okhttp/internal/http/RequestLine;->get(Lcom/android/okhttp/Request;Ljava/net/Proxy$Type;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected static readHeaders(Lcom/android/okhttp/Headers$Builder;Ljava/io/InputStream;)V
    .locals 2
    .param p0, "builder"    # Lcom/android/okhttp/Headers$Builder;
    .param p1, "input"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 208
    :goto_0
    invoke-static {p1}, Lcom/android/okhttp/internal/http/MultiratUtil;->readUtf8Line(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v0

    .local v0, "line":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 209
    invoke-virtual {p0, v0}, Lcom/android/okhttp/Headers$Builder;->addLine(Ljava/lang/String;)Lcom/android/okhttp/Headers$Builder;

    goto :goto_0

    .line 211
    :cond_0
    return-void
.end method

.method public static readResponseHeaders(Ljava/io/InputStream;)Lcom/android/okhttp/Response$Builder;
    .locals 7
    .param p0, "input"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 183
    :cond_0
    invoke-static {p0}, Lcom/android/okhttp/internal/http/MultiratUtil;->readUtf8Line(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v3

    .line 184
    .local v3, "statusLineString":Ljava/lang/String;
    new-instance v2, Lcom/android/okhttp/internal/http/StatusLine;

    invoke-direct {v2, v3}, Lcom/android/okhttp/internal/http/StatusLine;-><init>(Ljava/lang/String;)V

    .line 186
    .local v2, "statusLine":Lcom/android/okhttp/internal/http/StatusLine;
    new-instance v4, Lcom/android/okhttp/Response$Builder;

    invoke-direct {v4}, Lcom/android/okhttp/Response$Builder;-><init>()V

    invoke-virtual {v4, v2}, Lcom/android/okhttp/Response$Builder;->statusLine(Lcom/android/okhttp/internal/http/StatusLine;)Lcom/android/okhttp/Response$Builder;

    move-result-object v4

    sget-object v5, Lcom/android/okhttp/internal/http/OkHeaders;->SELECTED_PROTOCOL:Ljava/lang/String;

    sget-object v6, Lcom/android/okhttp/Protocol;->HTTP_11:Lcom/android/okhttp/Protocol;

    iget-object v6, v6, Lcom/android/okhttp/Protocol;->name:Lcom/android/okio/ByteString;

    invoke-virtual {v6}, Lcom/android/okio/ByteString;->utf8()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/android/okhttp/Response$Builder;->header(Ljava/lang/String;Ljava/lang/String;)Lcom/android/okhttp/Response$Builder;

    move-result-object v1

    .line 190
    .local v1, "responseBuilder":Lcom/android/okhttp/Response$Builder;
    new-instance v0, Lcom/android/okhttp/Headers$Builder;

    invoke-direct {v0}, Lcom/android/okhttp/Headers$Builder;-><init>()V

    .line 191
    .local v0, "headersBuilder":Lcom/android/okhttp/Headers$Builder;
    invoke-static {v0, p0}, Lcom/android/okhttp/internal/http/MultiratUtil;->readHeaders(Lcom/android/okhttp/Headers$Builder;Ljava/io/InputStream;)V

    .line 192
    invoke-virtual {v0}, Lcom/android/okhttp/Headers$Builder;->build()Lcom/android/okhttp/Headers;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/android/okhttp/Response$Builder;->headers(Lcom/android/okhttp/Headers;)Lcom/android/okhttp/Response$Builder;

    .line 194
    invoke-virtual {v2}, Lcom/android/okhttp/internal/http/StatusLine;->code()I

    move-result v4

    const/16 v5, 0x64

    if-eq v4, v5, :cond_0

    .line 195
    return-object v1
.end method

.method private static readUtf8Line(Ljava/io/InputStream;)Ljava/lang/String;
    .locals 6
    .param p0, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 221
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 223
    .local v0, "byteOut":Ljava/io/ByteArrayOutputStream;
    :goto_0
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v1

    .line 224
    .local v1, "c":I
    const/4 v4, -0x1

    if-ne v1, v4, :cond_0

    .line 225
    new-instance v4, Ljava/io/EOFException;

    invoke-direct {v4}, Ljava/io/EOFException;-><init>()V

    throw v4

    .line 226
    :cond_0
    const/16 v4, 0xa

    if-ne v1, v4, :cond_2

    .line 231
    const-string v4, "UTF-8"

    invoke-virtual {v0, v4}, Ljava/io/ByteArrayOutputStream;->toString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 232
    .local v3, "result":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v2

    .line 233
    .local v2, "length":I
    if-lez v2, :cond_1

    add-int/lit8 v4, v2, -0x1

    invoke-virtual {v3, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const/16 v5, 0xd

    if-ne v4, v5, :cond_1

    .line 234
    const/4 v4, 0x0

    add-int/lit8 v5, v2, -0x1

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 236
    :cond_1
    return-object v3

    .line 229
    .end local v2    # "length":I
    .end local v3    # "result":Ljava/lang/String;
    :cond_2
    invoke-virtual {v0, v1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    goto :goto_0
.end method
