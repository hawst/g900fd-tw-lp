.class Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;
.super Ljava/lang/Object;
.source "TwoChunkInputStream.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/okhttp/internal/http/TwoChunkInputStream;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SecondChunkThread"
.end annotation


# instance fields
.field private bDeprecated:Z

.field private bTryBoth:Z

.field dBuf:Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;

.field end:J

.field private input:Ljava/io/InputStream;

.field private locker:Ljava/lang/Object;

.field private mSecThreadCreationResult:I

.field private mStartReadHeaderTime:J

.field private output:Ljava/io/OutputStream;

.field private s:Ljava/net/Socket;

.field private sockID:I

.field start:J

.field private switchToSocketID:I

.field final synthetic this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;


# direct methods
.method public constructor <init>(Lcom/android/okhttp/internal/http/TwoChunkInputStream;IJJLcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;)V
    .locals 3
    .param p2, "socketID"    # I
    .param p3, "startOffset"    # J
    .param p5, "endOffset"    # J
    .param p7, "buf"    # Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 779
    iput-object p1, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 767
    iput-object v0, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->output:Ljava/io/OutputStream;

    .line 768
    iput-object v0, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->input:Ljava/io/InputStream;

    .line 769
    iput-object v0, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->s:Ljava/net/Socket;

    .line 770
    iput-boolean v2, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->bTryBoth:Z

    .line 771
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->mStartReadHeaderTime:J

    .line 772
    iput-boolean v2, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->bDeprecated:Z

    .line 773
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->locker:Ljava/lang/Object;

    .line 774
    const/16 v0, -0x64

    iput v0, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->mSecThreadCreationResult:I

    .line 780
    iput p2, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->sockID:I

    .line 781
    iput-wide p3, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->start:J

    .line 782
    iput-wide p5, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->end:J

    .line 783
    iput-object p7, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->dBuf:Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;

    .line 784
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->switchToSocketID:I

    .line 785
    return-void
.end method


# virtual methods
.method protected closeSocketAndStreams()V
    .locals 3

    .prologue
    .line 1123
    sget-boolean v0, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v0}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$100(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "try to close sec input stream in depth "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mDepth:I
    invoke-static {v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$1000(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", input is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->input:Ljava/io/InputStream;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 1124
    :cond_0
    iget-object v0, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->input:Ljava/io/InputStream;

    invoke-static {v0}, Lcom/android/okhttp/internal/http/MultiratUtil;->closeQuietly(Ljava/io/Closeable;)V

    .line 1125
    iget-object v0, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->output:Ljava/io/OutputStream;

    invoke-static {v0}, Lcom/android/okhttp/internal/http/MultiratUtil;->closeQuietly(Ljava/io/Closeable;)V

    .line 1126
    iget-object v0, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->s:Ljava/net/Socket;

    invoke-static {v0}, Lcom/android/okhttp/internal/http/MultiratUtil;->closeQuietly(Ljava/net/Socket;)V

    .line 1127
    return-void
.end method

.method public createTwoChunkInSec(Z)V
    .locals 28
    .param p1, "bReverse"    # Z

    .prologue
    .line 808
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->parentIS:Lcom/android/okhttp/internal/http/MultiSocketInputStream;
    invoke-static {v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$200(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-result-object v2

    iget-boolean v2, v2, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bFinished:Z

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->bDeprecated:Z

    if-nez v2, :cond_1

    .line 809
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$100(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v2

    const-string v3, "createTwoChunkInSec: session is finished"

    invoke-virtual {v2, v3}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 850
    :cond_0
    :goto_0
    return-void

    .line 812
    :cond_1
    monitor-enter p0

    .line 813
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mRemainBytes:J
    invoke-static {v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$300(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)J

    move-result-wide v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mBytesForTail:J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$400(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-gez v2, :cond_6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;
    invoke-static {v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$500(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    move-result-object v2

    if-nez v2, :cond_6

    .line 814
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$100(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Chunk0 is finished while mRemainBytes="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mRemainBytes:J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$300(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " totallen="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mLength:J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$600(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " start offset="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mStart:J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$700(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 817
    :cond_2
    if-eqz p1, :cond_3

    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->mStartReadHeaderTime:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_6

    .line 818
    :cond_3
    move-object/from16 v0, p0

    iget v7, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->sockID:I

    .line 819
    .local v7, "sock0":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->parentIS:Lcom/android/okhttp/internal/http/MultiSocketInputStream;
    invoke-static {v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$200(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-result-object v2

    invoke-virtual {v2, v7}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->getSpeed(I)J

    move-result-wide v14

    .line 820
    .local v14, "speed0":J
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->sockID:I

    add-int/lit8 v2, v2, 0x1

    rem-int/lit8 v8, v2, 0x2

    .line 821
    .local v8, "sock1":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->parentIS:Lcom/android/okhttp/internal/http/MultiSocketInputStream;
    invoke-static {v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$200(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-result-object v2

    invoke-virtual {v2, v8}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->getSpeed(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v16

    .line 823
    .local v16, "speed1":J
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    new-instance v3, Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->input:Ljava/io/InputStream;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    iget-object v5, v5, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->httpEngine:Lcom/android/okhttp/internal/http/HttpEngine;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->cr:Ljava/net/CacheRequest;
    invoke-static {v6}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$800(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Ljava/net/CacheRequest;

    move-result-object v6

    move-object/from16 v0, p0

    iget-wide v9, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->start:J

    move-object/from16 v0, p0

    iget-wide v11, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->end:J

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mOriReqHeader:Lcom/android/okhttp/Request;
    invoke-static {v13}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$900(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/Request;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    move-object/from16 v18, v0

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mDepth:I
    invoke-static/range {v18 .. v18}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$1000(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)I

    move-result v18

    add-int/lit8 v18, v18, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    move-object/from16 v19, v0

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->parentIS:Lcom/android/okhttp/internal/http/MultiSocketInputStream;
    invoke-static/range {v19 .. v19}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$200(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    move-object/from16 v20, v0

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mFullConSize:J
    invoke-static/range {v20 .. v20}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$1100(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)J

    move-result-wide v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    move-object/from16 v22, v0

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mTimeOut:I
    invoke-static/range {v22 .. v22}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$1200(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)I

    move-result v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mChildRAdownloadedFile:Ljava/io/RandomAccessFile;

    move-object/from16 v23, v0

    if-nez v23, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->dBuf:Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->getHasReadLen()J

    move-result-wide v24

    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    move-object/from16 v23, v0

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static/range {v23 .. v23}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$100(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    move-object/from16 v23, v0

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->connection:Lcom/android/okhttp/Connection;
    invoke-static/range {v23 .. v23}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$1300(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/Connection;

    move-result-object v27

    move/from16 v23, p1

    invoke-direct/range {v3 .. v27}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;-><init>(Ljava/io/InputStream;Lcom/android/okhttp/internal/http/HttpEngine;Ljava/net/CacheRequest;IIJJLcom/android/okhttp/Request;JJILcom/android/okhttp/internal/http/MultiSocketInputStream;JIZJLcom/android/okhttp/internal/http/MultiratLog;Lcom/android/okhttp/Connection;)V

    # setter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;
    invoke-static {v2, v3}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$502(Lcom/android/okhttp/internal/http/TwoChunkInputStream;Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    .line 827
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;
    invoke-static {v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$500(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->getNeedToExitSecThread()I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->mSecThreadCreationResult:I

    .line 828
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->parentIS:Lcom/android/okhttp/internal/http/MultiSocketInputStream;
    invoke-static {v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$200(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$500(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->isSingleThreadRun()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->startSingleThreadRunning(Z)V

    .line 829
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->mSecThreadCreationResult:I

    if-nez v2, :cond_9

    .line 830
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->bDeprecated:Z

    .line 831
    if-eqz p1, :cond_8

    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->mStartReadHeaderTime:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_8

    .line 832
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$100(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v2

    const-string v3, "Need to start another sec chunk input"

    invoke-virtual {v2, v3}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 833
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->closeSocketAndStreams()V

    .line 839
    :cond_5
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->dBuf:Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;
    invoke-static {v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$500(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    move-result-object v19

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->start:J

    move-wide/from16 v20, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->end:J

    move-wide/from16 v22, v0

    invoke-virtual/range {v18 .. v23}, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->push(Ljava/io/InputStream;JJ)V

    .line 840
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;
    invoke-static {v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$500(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->s:Ljava/net/Socket;

    invoke-virtual {v2, v3}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->startRun(Ljava/net/Socket;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 849
    .end local v7    # "sock0":I
    .end local v8    # "sock1":I
    .end local v14    # "speed0":J
    .end local v16    # "speed1":J
    :cond_6
    :goto_3
    :try_start_2
    monitor-exit p0

    goto/16 :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    .line 823
    .restart local v7    # "sock0":I
    .restart local v8    # "sock1":I
    .restart local v14    # "speed0":J
    .restart local v16    # "speed1":J
    :cond_7
    const-wide/16 v24, 0x0

    goto/16 :goto_1

    .line 836
    :cond_8
    :try_start_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;
    invoke-static {v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$500(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->input:Ljava/io/InputStream;

    .line 837
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$100(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "new input stream in sec chunk create child input = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->input:Ljava/io/InputStream;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    goto :goto_2

    .line 846
    :catch_0
    move-exception v2

    goto :goto_3

    .line 843
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    const/4 v3, 0x0

    # setter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;
    invoke-static {v2, v3}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$502(Lcom/android/okhttp/internal/http/TwoChunkInputStream;Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/TwoChunkInputStream;
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_3
.end method

.method public getSocketID()I
    .locals 1

    .prologue
    .line 788
    iget v0, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->sockID:I

    return v0
.end method

.method public run()V
    .locals 36

    .prologue
    .line 855
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "resource check: thread checking: start SecondChunkThread_depth"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mDepth:I
    invoke-static {v4}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$1000(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Thread;->getId()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", start to download with socket "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->sockID:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", bytes: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->start:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->end:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->log(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$000(Lcom/android/okhttp/internal/http/TwoChunkInputStream;Ljava/lang/String;)V

    .line 856
    :cond_0
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->end:J

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->start:J

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x1

    add-long v20, v2, v4

    .line 857
    .local v20, "bytesToRead":J
    const-wide/16 v32, 0x0

    .line 858
    .local v32, "totallen":J
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->parentIS:Lcom/android/okhttp/internal/http/MultiSocketInputStream;
    invoke-static {v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$200(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-result-object v2

    iget-boolean v2, v2, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bFinished:Z

    if-nez v2, :cond_4

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->bDeprecated:Z

    if-nez v2, :cond_4

    cmp-long v2, v32, v20

    if-gez v2, :cond_4

    .line 861
    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->mStartReadHeaderTime:J

    .line 862
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->switchToSocketID:I

    if-ltz v2, :cond_1

    .line 863
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->switchToSocketID:I

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->sockID:I

    .line 864
    const/4 v2, -0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->switchToSocketID:I

    .line 866
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;
    invoke-static {v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$500(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    move-result-object v2

    if-nez v2, :cond_12

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->bDeprecated:Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_6

    if-nez v2, :cond_12

    .line 868
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->connection:Lcom/android/okhttp/Connection;
    invoke-static {v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$1300(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/Connection;

    move-result-object v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->sockID:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mOriReqHeader:Lcom/android/okhttp/Request;
    invoke-static {v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$900(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/Request;

    move-result-object v5

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->start:J

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->end:J

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mFullConSize:J
    invoke-static {v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$1100(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)J

    move-result-wide v10

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->parentIS:Lcom/android/okhttp/internal/http/MultiSocketInputStream;
    invoke-static {v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$200(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-result-object v12

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->bTryBoth:Z

    const/4 v14, 0x1

    invoke-virtual/range {v3 .. v14}, Lcom/android/okhttp/Connection;->extremeConditionConnect(ILcom/android/okhttp/Request;JJJLcom/android/okhttp/internal/http/MultiSocketInputStream;ZZ)Ljava/net/Socket;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->s:Ljava/net/Socket;

    .line 870
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->s:Ljava/net/Socket;

    if-nez v2, :cond_c

    .line 871
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->bDeprecated:Z

    .line 872
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    const/4 v3, 0x1

    # setter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->bFailedInSecChunk:Z
    invoke-static {v2, v3}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$1402(Lcom/android/okhttp/internal/http/TwoChunkInputStream;Z)Z

    .line 873
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$100(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v2

    const-string v3, "unexpected HTTP response in ExtremThread"

    invoke-virtual {v2, v3}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_6

    .line 1097
    :cond_2
    monitor-enter p0

    .line 1098
    :try_start_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;
    invoke-static {v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$500(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    move-result-object v2

    if-nez v2, :cond_3

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->bDeprecated:Z

    if-nez v2, :cond_3

    .line 1099
    invoke-virtual/range {p0 .. p0}, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->closeSocketAndStreams()V

    .line 1100
    :cond_3
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1103
    :cond_4
    :goto_1
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->bDeprecated:Z

    if-nez v2, :cond_5

    .line 1104
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    const/4 v3, 0x0

    # setter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->bSecThreadExisted:Z
    invoke-static {v2, v3}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$2102(Lcom/android/okhttp/internal/http/TwoChunkInputStream;Z)Z

    .line 1105
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->parentIS:Lcom/android/okhttp/internal/http/MultiSocketInputStream;
    invoke-static {v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$200(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-result-object v2

    iget-boolean v2, v2, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bFinished:Z

    if-nez v2, :cond_5

    .line 1106
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->locker:Ljava/lang/Object;

    monitor-enter v3

    .line 1108
    :try_start_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->locker:Ljava/lang/Object;

    const-wide/16 v4, 0x7d0

    invoke-virtual {v2, v4, v5}, Ljava/lang/Object;->wait(J)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_4
    .catchall {:try_start_3 .. :try_end_3} :catchall_d

    .line 1111
    :goto_2
    :try_start_4
    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_d

    .line 1112
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    const/4 v3, 0x1

    # invokes: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->createTwoChunkInTwoChunk(Z)V
    invoke-static {v2, v3}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$2200(Lcom/android/okhttp/internal/http/TwoChunkInputStream;Z)V

    .line 1116
    :cond_5
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "resource check: thread checking: stop SecondChunkThread_depth"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mDepth:I
    invoke-static {v4}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$1000(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Thread;->getId()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", finish to download with socket "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->sockID:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->log(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$000(Lcom/android/okhttp/internal/http/TwoChunkInputStream;Ljava/lang/String;)V

    .line 1117
    :cond_6
    return-void

    .line 1100
    :catchall_0
    move-exception v2

    :try_start_5
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    throw v2

    .line 877
    :catch_0
    move-exception v17

    .line 878
    .local v17, "e":Ljava/lang/Exception;
    :try_start_6
    monitor-enter p0
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_6

    .line 879
    :try_start_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;
    invoke-static {v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$500(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    move-result-object v2

    if-nez v2, :cond_a

    .line 880
    throw v17

    .line 884
    :catchall_1
    move-exception v2

    monitor-exit p0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :try_start_8
    throw v2
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_6

    .line 1081
    .end local v17    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v28

    .line 1082
    .local v28, "t":Ljava/lang/Throwable;
    :try_start_9
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$100(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v2

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V

    .line 1083
    :cond_7
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v24

    .line 1084
    .local v24, "sTime":J
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->locker:Ljava/lang/Object;

    monitor-enter v3
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_6

    .line 1086
    :try_start_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->locker:Ljava/lang/Object;

    const-wide/16 v4, 0x1f4

    invoke-virtual {v2, v4, v5}, Ljava/lang/Object;->wait(J)V
    :try_end_a
    .catch Ljava/lang/Throwable; {:try_start_a .. :try_end_a} :catch_5
    .catchall {:try_start_a .. :try_end_a} :catchall_b

    .line 1090
    :goto_3
    :try_start_b
    monitor-exit v3
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_b

    .line 1091
    :try_start_c
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long v34, v2, v24

    .line 1092
    .local v34, "waitedTime":J
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mTimeForDownload:[J
    invoke-static {v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$2000(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)[J

    move-result-object v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->sockID:I

    aget-wide v2, v2, v3

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;
    invoke-static {v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$500(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    move-result-object v2

    if-nez v2, :cond_8

    .line 1093
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->sockID:I

    move-wide/from16 v0, v34

    invoke-virtual {v2, v3, v0, v1}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->decTime(IJ)V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_6

    .line 1097
    :cond_8
    monitor-enter p0

    .line 1098
    :try_start_d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;
    invoke-static {v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$500(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    move-result-object v2

    if-nez v2, :cond_9

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->bDeprecated:Z

    if-nez v2, :cond_9

    .line 1099
    invoke-virtual/range {p0 .. p0}, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->closeSocketAndStreams()V

    .line 1100
    :cond_9
    monitor-exit p0

    goto/16 :goto_0

    :catchall_2
    move-exception v2

    monitor-exit p0
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_2

    throw v2

    .line 882
    .end local v24    # "sTime":J
    .end local v28    # "t":Ljava/lang/Throwable;
    .end local v34    # "waitedTime":J
    .restart local v17    # "e":Ljava/lang/Exception;
    :cond_a
    :try_start_e
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_b

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    const-string v3, "childIS is created before this exception"

    # invokes: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->log(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$000(Lcom/android/okhttp/internal/http/TwoChunkInputStream;Ljava/lang/String;)V

    .line 884
    :cond_b
    monitor-exit p0
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    .line 887
    .end local v17    # "e":Ljava/lang/Exception;
    :cond_c
    :try_start_f
    monitor-enter p0
    :try_end_f
    .catch Ljava/lang/Throwable; {:try_start_f .. :try_end_f} :catch_1
    .catchall {:try_start_f .. :try_end_f} :catchall_6

    .line 888
    const-wide/16 v2, -0x1

    :try_start_10
    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->mStartReadHeaderTime:J

    .line 889
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->bDeprecated:Z

    if-eqz v2, :cond_f

    .line 890
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_d

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    const-string v3, "thread is deprecated, break"

    # invokes: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->log(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$000(Lcom/android/okhttp/internal/http/TwoChunkInputStream;Ljava/lang/String;)V

    .line 891
    :cond_d
    monitor-exit p0
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_5

    .line 1097
    monitor-enter p0

    .line 1098
    :try_start_11
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;
    invoke-static {v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$500(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    move-result-object v2

    if-nez v2, :cond_e

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->bDeprecated:Z

    if-nez v2, :cond_e

    .line 1099
    invoke-virtual/range {p0 .. p0}, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->closeSocketAndStreams()V

    .line 1100
    :cond_e
    monitor-exit p0

    goto/16 :goto_1

    :catchall_3
    move-exception v2

    monitor-exit p0
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_3

    throw v2

    .line 893
    :cond_f
    :try_start_12
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;
    invoke-static {v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$500(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    move-result-object v2

    if-nez v2, :cond_17

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->bDeprecated:Z

    if-nez v2, :cond_17

    .line 894
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->bTryBoth:Z

    if-eqz v2, :cond_10

    .line 895
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->s:Ljava/net/Socket;

    invoke-virtual {v2}, Ljava/net/Socket;->getLocalAddress()Ljava/net/InetAddress;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->connection:Lcom/android/okhttp/Connection;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$1300(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/Connection;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/android/okhttp/Connection;->getLocalAddr(I)Ljava/net/InetAddress;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/net/InetAddress;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_15

    .line 896
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->sockID:I

    .line 903
    :cond_10
    :goto_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->s:Ljava/net/Socket;

    const/16 v3, 0x1388

    invoke-virtual {v2, v3}, Ljava/net/Socket;->setSoTimeout(I)V

    .line 905
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->s:Ljava/net/Socket;

    invoke-virtual {v2}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->output:Ljava/io/OutputStream;

    .line 906
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->s:Ljava/net/Socket;

    invoke-virtual {v2}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->input:Ljava/io/InputStream;

    .line 907
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_11

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$100(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "new input stream in sec chunk reconnect input = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->input:Ljava/io/InputStream;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 912
    :cond_11
    :goto_5
    monitor-exit p0
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_5

    .line 915
    :cond_12
    :try_start_13
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->bDeprecated:Z

    if-eqz v2, :cond_18

    .line 916
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_13

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    const-string v3, "thread is deprecated2, break"

    # invokes: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->log(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$000(Lcom/android/okhttp/internal/http/TwoChunkInputStream;Ljava/lang/String;)V
    :try_end_13
    .catch Ljava/lang/Throwable; {:try_start_13 .. :try_end_13} :catch_1
    .catchall {:try_start_13 .. :try_end_13} :catchall_6

    .line 1097
    :cond_13
    monitor-enter p0

    .line 1098
    :try_start_14
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;
    invoke-static {v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$500(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    move-result-object v2

    if-nez v2, :cond_14

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->bDeprecated:Z

    if-nez v2, :cond_14

    .line 1099
    invoke-virtual/range {p0 .. p0}, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->closeSocketAndStreams()V

    .line 1100
    :cond_14
    monitor-exit p0

    goto/16 :goto_1

    :catchall_4
    move-exception v2

    monitor-exit p0
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_4

    throw v2

    .line 899
    :cond_15
    const/4 v2, 0x1

    :try_start_15
    move-object/from16 v0, p0

    iput v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->sockID:I

    goto :goto_4

    .line 912
    :catchall_5
    move-exception v2

    monitor-exit p0
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_5

    :try_start_16
    throw v2
    :try_end_16
    .catch Ljava/lang/Throwable; {:try_start_16 .. :try_end_16} :catch_1
    .catchall {:try_start_16 .. :try_end_16} :catchall_6

    .line 1097
    :catchall_6
    move-exception v2

    monitor-enter p0

    .line 1098
    :try_start_17
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;
    invoke-static {v3}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$500(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    move-result-object v3

    if-nez v3, :cond_16

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->bDeprecated:Z

    if-nez v3, :cond_16

    .line 1099
    invoke-virtual/range {p0 .. p0}, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->closeSocketAndStreams()V

    .line 1100
    :cond_16
    monitor-exit p0
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_c

    throw v2

    .line 910
    :cond_17
    :try_start_18
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_11

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    const-string v3, "childIS is created before this connection"

    # invokes: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->log(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$000(Lcom/android/okhttp/internal/http/TwoChunkInputStream;Ljava/lang/String;)V
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_5

    goto :goto_5

    .line 920
    :cond_18
    :try_start_19
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->dBuf:Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;

    if-nez v2, :cond_21

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->parentIS:Lcom/android/okhttp/internal/http/MultiSocketInputStream;
    invoke-static {v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$200(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-result-object v2

    iget-boolean v2, v2, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bFinished:Z

    if-nez v2, :cond_21

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->bDeprecated:Z

    if-nez v2, :cond_21

    .line 921
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_19

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "connected for socket "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->sockID:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", need to use this socket to get actual data"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->log(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$000(Lcom/android/okhttp/internal/http/TwoChunkInputStream;Ljava/lang/String;)V

    .line 922
    :cond_19
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->parentIS:Lcom/android/okhttp/internal/http/MultiSocketInputStream;
    invoke-static {v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$200(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mSocket0:I
    invoke-static {v3}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$1500(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->getSpeed(I)J

    move-result-wide v26

    .line 923
    .local v26, "speed0":J
    const-wide/16 v2, 0x0

    cmp-long v2, v26, v2

    if-nez v2, :cond_1c

    const-wide v30, 0x7fffffffffffffffL

    .line 924
    .local v30, "timeForRemainBy0":J
    :goto_6
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_1a

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "socket0 speed="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, v26

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", timeForRemainBy0="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, v30

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->log(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$000(Lcom/android/okhttp/internal/http/TwoChunkInputStream;Ljava/lang/String;)V

    .line 925
    :cond_1a
    const-wide/16 v2, 0x1388

    cmp-long v2, v30, v2

    if-lez v2, :cond_1e

    .line 926
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    const-wide/16 v2, 0x0

    cmp-long v2, v26, v2

    if-nez v2, :cond_1d

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mRemainBytes:J
    invoke-static {v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$300(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)J

    move-result-wide v2

    const-wide/16 v6, 0x2

    mul-long/2addr v2, v6

    const-wide/16 v6, 0x3

    div-long/2addr v2, v6

    :goto_7
    # setter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mBytesForTail:J
    invoke-static {v4, v2, v3}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$402(Lcom/android/okhttp/internal/http/TwoChunkInputStream;J)J

    .line 927
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->end:J

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mBytesForTail:J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$400(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)J

    move-result-wide v4

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->start:J

    .line 928
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    new-instance v2, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->start:J

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mBytesForTail:J
    invoke-static {v6}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$400(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)J

    move-result-wide v6

    invoke-direct/range {v2 .. v7}, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;-><init>(Lcom/android/okhttp/internal/http/TwoChunkInputStream;JJ)V

    # setter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mDBuf:Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;
    invoke-static {v8, v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$1602(Lcom/android/okhttp/internal/http/TwoChunkInputStream;Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;)Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;

    .line 929
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mDBuf:Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;
    invoke-static {v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$1600(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->dBuf:Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;

    .line 931
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->end:J

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->start:J

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x1

    add-long v20, v2, v4

    .line 932
    const-wide/16 v32, 0x0

    .line 933
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_1b

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$100(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v2

    const-string v3, "do reconnect"

    invoke-virtual {v2, v3}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 934
    :cond_1b
    new-instance v2, Ljava/io/IOException;

    const-string v3, "please reconnect to get actual data"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 923
    .end local v30    # "timeForRemainBy0":J
    :cond_1c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mRemainBytes:J
    invoke-static {v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$300(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)J

    move-result-wide v2

    const-wide/16 v4, 0x8

    mul-long/2addr v2, v4

    div-long v30, v2, v26

    goto/16 :goto_6

    .line 926
    .restart local v30    # "timeForRemainBy0":J
    :cond_1d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mRemainBytes:J
    invoke-static {v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$300(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)J

    move-result-wide v2

    const-wide/16 v6, 0x3

    div-long/2addr v2, v6

    goto/16 :goto_7

    .line 937
    :cond_1e
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_1f

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$100(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v2

    const-string v3, "small data left, ignore me, exit thread"

    invoke-virtual {v2, v3}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 938
    :cond_1f
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->bDeprecated:Z

    .line 939
    invoke-virtual/range {p0 .. p0}, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->closeSocketAndStreams()V
    :try_end_19
    .catch Ljava/lang/Throwable; {:try_start_19 .. :try_end_19} :catch_1
    .catchall {:try_start_19 .. :try_end_19} :catchall_6

    .line 1097
    monitor-enter p0

    .line 1098
    :try_start_1a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;
    invoke-static {v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$500(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    move-result-object v2

    if-nez v2, :cond_20

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->bDeprecated:Z

    if-nez v2, :cond_20

    .line 1099
    invoke-virtual/range {p0 .. p0}, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->closeSocketAndStreams()V

    .line 1100
    :cond_20
    monitor-exit p0

    goto/16 :goto_1

    :catchall_7
    move-exception v2

    monitor-exit p0
    :try_end_1a
    .catchall {:try_start_1a .. :try_end_1a} :catchall_7

    throw v2

    .line 944
    .end local v26    # "speed0":J
    .end local v30    # "timeForRemainBy0":J
    :cond_21
    :try_start_1b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mStartReadTime:[J
    invoke-static {v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$1700(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)[J

    move-result-object v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->sockID:I

    aget-wide v2, v2, v3

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_22

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mStartReadTime:[J
    invoke-static {v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$1700(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)[J

    move-result-object v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->sockID:I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    aput-wide v4, v2, v3

    .line 947
    :cond_22
    const/16 v16, 0x0

    .line 948
    .local v16, "buf_offset":I
    const-wide/16 v18, 0x0

    .line 954
    .local v18, "buf_ret":J
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_23

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SecondChunkThread starts to read data from "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->start:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " to "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->end:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->log(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$000(Lcom/android/okhttp/internal/http/TwoChunkInputStream;Ljava/lang/String;)V

    .line 955
    :cond_23
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mSecChunkStart:J
    invoke-static {v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$1800(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-gez v2, :cond_24

    .line 956
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->start:J

    # setter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mSecChunkStart:J
    invoke-static {v2, v4, v5}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$1802(Lcom/android/okhttp/internal/http/TwoChunkInputStream;J)J

    .line 957
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->start:J

    # setter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mSecChunkEnd:J
    invoke-static {v2, v4, v5}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$1902(Lcom/android/okhttp/internal/http/TwoChunkInputStream;J)J

    .line 959
    :cond_24
    sget v2, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->MAX_BUFFERLEN:I

    new-array v15, v2, [B

    .line 960
    .local v15, "buf":[B
    :cond_25
    cmp-long v2, v32, v20

    if-gez v2, :cond_26

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->bDeprecated:Z

    if-nez v2, :cond_26

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->parentIS:Lcom/android/okhttp/internal/http/MultiSocketInputStream;
    invoke-static {v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$200(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-result-object v2

    iget-boolean v2, v2, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bFinished:Z

    if-nez v2, :cond_26

    .line 961
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->mSecThreadCreationResult:I

    const/16 v3, -0x64

    if-ne v2, v3, :cond_2a

    .line 962
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->createTwoChunkInSec(Z)V

    .line 963
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->bDeprecated:Z

    if-eqz v2, :cond_29

    .line 964
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_26

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Secchunk Thread deprecated at offset "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->start:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->log(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$000(Lcom/android/okhttp/internal/http/TwoChunkInputStream;Ljava/lang/String;)V

    .line 1079
    :cond_26
    :goto_8
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_27

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Speed of current socket is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->parentIS:Lcom/android/okhttp/internal/http/MultiSocketInputStream;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$200(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-result-object v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->sockID:I

    invoke-virtual {v4, v5}, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->getSpeed(I)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " for content size "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, v32

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->log(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$000(Lcom/android/okhttp/internal/http/TwoChunkInputStream;Ljava/lang/String;)V
    :try_end_1b
    .catch Ljava/lang/Throwable; {:try_start_1b .. :try_end_1b} :catch_1
    .catchall {:try_start_1b .. :try_end_1b} :catchall_6

    .line 1097
    :cond_27
    monitor-enter p0

    .line 1098
    :try_start_1c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;
    invoke-static {v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$500(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    move-result-object v2

    if-nez v2, :cond_28

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->bDeprecated:Z

    if-nez v2, :cond_28

    .line 1099
    invoke-virtual/range {p0 .. p0}, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->closeSocketAndStreams()V

    .line 1100
    :cond_28
    monitor-exit p0

    goto/16 :goto_0

    :catchall_8
    move-exception v2

    monitor-exit p0
    :try_end_1c
    .catchall {:try_start_1c .. :try_end_1c} :catchall_8

    throw v2

    .line 969
    :cond_29
    :try_start_1d
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->mSecThreadCreationResult:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_2a

    .line 970
    invoke-virtual/range {p0 .. p0}, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->closeSocketAndStreams()V

    .line 971
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->sockID:I

    add-int/lit8 v2, v2, 0x1

    rem-int/lit8 v2, v2, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->switchSocket(I)V
    :try_end_1d
    .catch Ljava/lang/Throwable; {:try_start_1d .. :try_end_1d} :catch_1
    .catchall {:try_start_1d .. :try_end_1d} :catchall_6

    goto :goto_8

    .line 977
    :cond_2a
    const/16 v16, 0x0

    .line 978
    const-wide/16 v18, 0x0

    .line 979
    const/16 v23, 0x0

    .line 982
    .local v23, "len":I
    :try_start_1e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->parentIS:Lcom/android/okhttp/internal/http/MultiSocketInputStream;
    invoke-static {v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$200(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-result-object v2

    iget-object v3, v2, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->lockAppReading:Ljava/lang/Object;

    monitor-enter v3
    :try_end_1e
    .catch Ljava/lang/Throwable; {:try_start_1e .. :try_end_1e} :catch_3
    .catchall {:try_start_1e .. :try_end_1e} :catchall_6

    .line 983
    :cond_2b
    :goto_9
    :try_start_1f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->parentIS:Lcom/android/okhttp/internal/http/MultiSocketInputStream;
    invoke-static {v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$200(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-result-object v2

    iget-boolean v2, v2, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bAppReading:Z

    if-nez v2, :cond_30

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->parentIS:Lcom/android/okhttp/internal/http/MultiSocketInputStream;
    invoke-static {v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$200(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-result-object v2

    iget-boolean v2, v2, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bFinished:Z

    if-nez v2, :cond_30

    .line 984
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_2c

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$100(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v2

    const-string v4, "APP not reading, waiting"

    invoke-virtual {v2, v4}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V
    :try_end_1f
    .catchall {:try_start_1f .. :try_end_1f} :catchall_9

    .line 986
    :cond_2c
    :try_start_20
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->parentIS:Lcom/android/okhttp/internal/http/MultiSocketInputStream;
    invoke-static {v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$200(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-result-object v2

    iget-object v2, v2, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->lockAppReading:Ljava/lang/Object;

    const-wide/16 v4, 0x3e8

    invoke-virtual {v2, v4, v5}, Ljava/lang/Object;->wait(J)V
    :try_end_20
    .catch Ljava/lang/Throwable; {:try_start_20 .. :try_end_20} :catch_2
    .catchall {:try_start_20 .. :try_end_20} :catchall_9

    goto :goto_9

    .line 988
    :catch_2
    move-exception v17

    .line 989
    .local v17, "e":Ljava/lang/Throwable;
    :try_start_21
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_2b

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$100(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V

    goto :goto_9

    .line 992
    .end local v17    # "e":Ljava/lang/Throwable;
    :catchall_9
    move-exception v2

    monitor-exit v3
    :try_end_21
    .catchall {:try_start_21 .. :try_end_21} :catchall_9

    :try_start_22
    throw v2
    :try_end_22
    .catch Ljava/lang/Throwable; {:try_start_22 .. :try_end_22} :catch_3
    .catchall {:try_start_22 .. :try_end_22} :catchall_6

    .line 1016
    :catch_3
    move-exception v22

    .line 1017
    .local v22, "ex":Ljava/lang/Throwable;
    :try_start_23
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->parentIS:Lcom/android/okhttp/internal/http/MultiSocketInputStream;
    invoke-static {v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$200(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-result-object v2

    iget-object v2, v2, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->disconnCount:[I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->sockID:I

    aget v4, v2, v3

    add-int/lit8 v4, v4, 0x1

    aput v4, v2, v3

    .line 1018
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_2d

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SocketTimeoutException Count: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->parentIS:Lcom/android/okhttp/internal/http/MultiSocketInputStream;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$200(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-result-object v4

    iget-object v4, v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->disconnCount:[I

    const/4 v5, 0x0

    aget v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->parentIS:Lcom/android/okhttp/internal/http/MultiSocketInputStream;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$200(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-result-object v4

    iget-object v4, v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->disconnCount:[I

    const/4 v5, 0x1

    aget v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->log(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$000(Lcom/android/okhttp/internal/http/TwoChunkInputStream;Ljava/lang/String;)V

    .line 1019
    :cond_2d
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_2e

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception in getting block: buf_offset="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", buf.length="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    array-length v4, v15

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", len:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v23

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", bFinished="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->parentIS:Lcom/android/okhttp/internal/http/MultiSocketInputStream;
    invoke-static {v4}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$200(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-result-object v4

    iget-boolean v4, v4, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bFinished:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->log(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$000(Lcom/android/okhttp/internal/http/TwoChunkInputStream;Ljava/lang/String;)V

    .line 1020
    :cond_2e
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_2f

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$100(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v2

    move-object/from16 v0, v22

    invoke-virtual {v2, v0}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/Throwable;)V

    .line 1021
    :cond_2f
    throw v22
    :try_end_23
    .catch Ljava/lang/Throwable; {:try_start_23 .. :try_end_23} :catch_1
    .catchall {:try_start_23 .. :try_end_23} :catchall_6

    .line 992
    .end local v22    # "ex":Ljava/lang/Throwable;
    :cond_30
    :try_start_24
    monitor-exit v3
    :try_end_24
    .catchall {:try_start_24 .. :try_end_24} :catchall_9

    .line 994
    :cond_31
    :try_start_25
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->bDeprecated:Z

    if-nez v2, :cond_33

    array-length v2, v15

    move/from16 v0, v16

    if-ge v0, v2, :cond_33

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->input:Ljava/io/InputStream;

    array-length v3, v15

    sub-int v3, v3, v16

    move/from16 v0, v16

    invoke-virtual {v2, v15, v0, v3}, Ljava/io/InputStream;->read([BII)I

    move-result v23

    if-lez v23, :cond_33

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->parentIS:Lcom/android/okhttp/internal/http/MultiSocketInputStream;
    invoke-static {v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$200(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiSocketInputStream;

    move-result-object v2

    iget-boolean v2, v2, Lcom/android/okhttp/internal/http/MultiSocketInputStream;->bFinished:Z

    if-nez v2, :cond_33

    .line 995
    add-int v16, v16, v23

    .line 996
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_32

    .line 997
    :cond_32
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mRemainBytes:J
    invoke-static {v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$300(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)J

    move-result-wide v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mBytesForTail:J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$400(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-gtz v2, :cond_35

    .line 998
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_33

    .line 999
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->mSecThreadCreationResult:I

    const/16 v3, -0x64

    if-ne v2, v3, :cond_33

    .line 1000
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$100(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Now it is time to stop this thread, "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mRemainBytes:J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$300(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "<="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mBytesForTail:J
    invoke-static {v4}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$400(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", buf_offset:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V
    :try_end_25
    .catch Ljava/lang/Throwable; {:try_start_25 .. :try_end_25} :catch_3
    .catchall {:try_start_25 .. :try_end_25} :catchall_6

    .line 1027
    :cond_33
    :goto_a
    if-lez v16, :cond_3d

    :try_start_26
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->bDeprecated:Z

    if-nez v2, :cond_3d

    .line 1028
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->dBuf:Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;

    monitor-enter v3
    :try_end_26
    .catch Ljava/lang/Throwable; {:try_start_26 .. :try_end_26} :catch_1
    .catchall {:try_start_26 .. :try_end_26} :catchall_6

    .line 1029
    :try_start_27
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->dBuf:Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->sockID:I

    move/from16 v0, v16

    invoke-virtual {v2, v4, v15, v0}, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;->push(I[BI)J

    move-result-wide v18

    .line 1030
    const-wide/16 v4, -0x1

    cmp-long v2, v18, v4

    if-nez v2, :cond_38

    .line 1031
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_34

    .line 1032
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "This block shall be read by another socket, this socket is slow: block[0], socket["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->sockID:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    # invokes: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->log(Ljava/lang/String;)V
    invoke-static {v2, v4}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$000(Lcom/android/okhttp/internal/http/TwoChunkInputStream;Ljava/lang/String;)V

    .line 1035
    :cond_34
    new-instance v2, Ljava/io/IOException;

    const-string v4, "Data is not pushed to buffer may be socket has changed."

    invoke-direct {v2, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1056
    :catchall_a
    move-exception v2

    monitor-exit v3
    :try_end_27
    .catchall {:try_start_27 .. :try_end_27} :catchall_a

    :try_start_28
    throw v2
    :try_end_28
    .catch Ljava/lang/Throwable; {:try_start_28 .. :try_end_28} :catch_1
    .catchall {:try_start_28 .. :try_end_28} :catchall_6

    .line 1004
    :cond_35
    move/from16 v0, v16

    int-to-long v2, v0

    add-long v2, v2, v32

    cmp-long v2, v2, v20

    if-nez v2, :cond_36

    .line 1005
    :try_start_29
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_33

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Full data read, break: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "+"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, v32

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, v20

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->log(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$000(Lcom/android/okhttp/internal/http/TwoChunkInputStream;Ljava/lang/String;)V

    goto/16 :goto_a

    .line 1008
    :cond_36
    move/from16 v0, v16

    int-to-long v2, v0

    add-long v2, v2, v32

    cmp-long v2, v2, v20

    if-lez v2, :cond_31

    .line 1009
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_37

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Full data read, break: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "+"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, v32

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ">"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, v20

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->log(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$000(Lcom/android/okhttp/internal/http/TwoChunkInputStream;Ljava/lang/String;)V
    :try_end_29
    .catch Ljava/lang/Throwable; {:try_start_29 .. :try_end_29} :catch_3
    .catchall {:try_start_29 .. :try_end_29} :catchall_6

    .line 1010
    :cond_37
    sub-long v2, v20, v32

    long-to-int v0, v2

    move/from16 v16, v0

    .line 1011
    goto/16 :goto_a

    .line 1037
    :cond_38
    const-wide/16 v4, 0x0

    cmp-long v2, v18, v4

    if-gez v2, :cond_3a

    .line 1038
    const-wide/16 v4, -0x1

    mul-long v4, v4, v18

    :try_start_2a
    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->start:J

    .line 1039
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_39

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "some buffered data is removed from cache file, read again from "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->start:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    # invokes: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->log(Ljava/lang/String;)V
    invoke-static {v2, v4}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$000(Lcom/android/okhttp/internal/http/TwoChunkInputStream;Ljava/lang/String;)V

    .line 1040
    :cond_39
    new-instance v2, Ljava/io/IOException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "some buffered data is removed from cache file, read again from "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->start:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1043
    :cond_3a
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->start:J

    add-long v4, v4, v18

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->start:J

    .line 1044
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    move-wide/from16 v0, v18

    # += operator for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mSecChunkEnd:J
    invoke-static {v2, v0, v1}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$1914(Lcom/android/okhttp/internal/http/TwoChunkInputStream;J)J

    .line 1045
    add-long v32, v32, v18

    .line 1046
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->sockID:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->mSocket0:I
    invoke-static {v4}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$1500(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)I

    move-result v4

    if-eq v2, v4, :cond_3b

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->childIS:Lcom/android/okhttp/internal/http/TwoChunkInputStream;
    invoke-static {v2}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$500(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    move-result-object v2

    if-nez v2, :cond_3b

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->bDeprecated:Z

    if-nez v2, :cond_3b

    .line 1047
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->sockID:I

    invoke-virtual {v2, v4}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->setTime(I)V

    .line 1048
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->sockID:I

    move/from16 v0, v16

    int-to-long v6, v0

    invoke-virtual {v2, v4, v6, v7}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->incByte(IJ)V
    :try_end_2a
    .catchall {:try_start_2a .. :try_end_2a} :catchall_a

    .line 1052
    :cond_3b
    :try_start_2b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->dBuf:Lcom/android/okhttp/internal/http/TwoChunkInputStream$SimpleDataBuffer;

    invoke-virtual {v2}, Ljava/lang/Object;->notifyAll()V
    :try_end_2b
    .catch Ljava/lang/Throwable; {:try_start_2b .. :try_end_2b} :catch_6
    .catchall {:try_start_2b .. :try_end_2b} :catchall_a

    .line 1056
    :goto_b
    :try_start_2c
    monitor-exit v3
    :try_end_2c
    .catchall {:try_start_2c .. :try_end_2c} :catchall_a

    .line 1074
    :try_start_2d
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->switchToSocketID:I

    if-ltz v2, :cond_25

    .line 1075
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_3c

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Need to swtich to socket "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->switchToSocketID:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " while remain data offset "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->start:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->log(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$000(Lcom/android/okhttp/internal/http/TwoChunkInputStream;Ljava/lang/String;)V

    .line 1076
    :cond_3c
    new-instance v2, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Need to swtich to socket "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->switchToSocketID:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1058
    :cond_3d
    const/4 v2, -0x1

    move/from16 v0, v23

    if-ne v0, v2, :cond_3f

    cmp-long v2, v32, v20

    if-gez v2, :cond_3f

    .line 1059
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_3e

    .line 1060
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SecondChunkThread read body Exception: totallen="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, v32

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", bytesToRead= "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, v20

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->log(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$000(Lcom/android/okhttp/internal/http/TwoChunkInputStream;Ljava/lang/String;)V

    .line 1063
    :cond_3e
    new-instance v2, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SecondChunkThread read body Exception: totallen="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, v32

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", bytesToRead= "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, v20

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1067
    :cond_3f
    sget-boolean v2, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v2, :cond_40

    .line 1068
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SecondChunkThread read body Exception2: totallen="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, v32

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", bytesToRead= "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, v20

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->log(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$000(Lcom/android/okhttp/internal/http/TwoChunkInputStream;Ljava/lang/String;)V

    .line 1071
    :cond_40
    new-instance v2, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SecondChunkThread read body Exception2: totallen="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, v32

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", bytesToRead= "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, v20

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_2d
    .catch Ljava/lang/Throwable; {:try_start_2d .. :try_end_2d} :catch_1
    .catchall {:try_start_2d .. :try_end_2d} :catchall_6

    .line 1090
    .end local v15    # "buf":[B
    .end local v16    # "buf_offset":I
    .end local v18    # "buf_ret":J
    .end local v23    # "len":I
    .restart local v24    # "sTime":J
    .restart local v28    # "t":Ljava/lang/Throwable;
    :catchall_b
    move-exception v2

    :try_start_2e
    monitor-exit v3
    :try_end_2e
    .catchall {:try_start_2e .. :try_end_2e} :catchall_b

    :try_start_2f
    throw v2
    :try_end_2f
    .catchall {:try_start_2f .. :try_end_2f} :catchall_6

    .line 1100
    .end local v24    # "sTime":J
    .end local v28    # "t":Ljava/lang/Throwable;
    :catchall_c
    move-exception v2

    :try_start_30
    monitor-exit p0
    :try_end_30
    .catchall {:try_start_30 .. :try_end_30} :catchall_c

    throw v2

    .line 1111
    :catchall_d
    move-exception v2

    :try_start_31
    monitor-exit v3
    :try_end_31
    .catchall {:try_start_31 .. :try_end_31} :catchall_d

    throw v2

    .line 1110
    :catch_4
    move-exception v2

    goto/16 :goto_2

    .line 1088
    .restart local v24    # "sTime":J
    .restart local v28    # "t":Ljava/lang/Throwable;
    :catch_5
    move-exception v2

    goto/16 :goto_3

    .line 1054
    .end local v24    # "sTime":J
    .end local v28    # "t":Ljava/lang/Throwable;
    .restart local v15    # "buf":[B
    .restart local v16    # "buf_offset":I
    .restart local v18    # "buf_ret":J
    .restart local v23    # "len":I
    :catch_6
    move-exception v2

    goto/16 :goto_b
.end method

.method public setInput(Ljava/io/InputStream;)V
    .locals 3
    .param p1, "in"    # Ljava/io/InputStream;

    .prologue
    .line 803
    iput-object p1, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->input:Ljava/io/InputStream;

    .line 804
    sget-boolean v0, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    # getter for: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->logger:Lcom/android/okhttp/internal/http/MultiratLog;
    invoke-static {v0}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$100(Lcom/android/okhttp/internal/http/TwoChunkInputStream;)Lcom/android/okhttp/internal/http/MultiratLog;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "new input stream in sec chunk setinput input = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->input:Ljava/io/InputStream;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/okhttp/internal/http/MultiratLog;->i(Ljava/lang/String;)V

    .line 805
    :cond_0
    return-void
.end method

.method public startTryBoth(I)V
    .locals 3
    .param p1, "preferSock"    # I

    .prologue
    .line 798
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->bTryBoth:Z

    .line 799
    sget-boolean v0, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "sec start try both, prefer "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->log(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$000(Lcom/android/okhttp/internal/http/TwoChunkInputStream;Ljava/lang/String;)V

    .line 800
    :cond_0
    return-void
.end method

.method public switchSocket(I)V
    .locals 3
    .param p1, "sid"    # I

    .prologue
    .line 792
    sget-boolean v0, Lcom/android/okhttp/internal/http/MultiratLog;->MRATLOG:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->this$0:Lcom/android/okhttp/internal/http/TwoChunkInputStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "requested to switch socket id from "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->sockID:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/android/okhttp/internal/http/TwoChunkInputStream;->log(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/android/okhttp/internal/http/TwoChunkInputStream;->access$000(Lcom/android/okhttp/internal/http/TwoChunkInputStream;Ljava/lang/String;)V

    .line 793
    :cond_0
    iget v0, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->sockID:I

    if-eq p1, v0, :cond_1

    .line 794
    iput p1, p0, Lcom/android/okhttp/internal/http/TwoChunkInputStream$SecondChunkThread;->switchToSocketID:I

    .line 795
    :cond_1
    return-void
.end method
