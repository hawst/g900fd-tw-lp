.class final Lcom/broadcom/nfc/NfcFactory;
.super Ljava/lang/Object;
.source "NfcFactory.java"


# static fields
.field static final SERVICE_NAME:Ljava/lang/String; = "com.broadcom.nfc.Factory"

.field static final TAG:Ljava/lang/String; = "NfcFacApi"

.field static sFactoryInterface:Lcom/broadcom/nfc/INfcFactory;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized getInstance()Lcom/broadcom/nfc/NfcFactory;
    .locals 2

    .prologue
    .line 66
    const-class v1, Lcom/broadcom/nfc/NfcFactory;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/broadcom/nfc/NfcFactory;->sFactoryInterface:Lcom/broadcom/nfc/INfcFactory;

    if-nez v0, :cond_0

    .line 67
    invoke-static {}, Lcom/broadcom/nfc/NfcFactory;->initService()V

    .line 68
    :cond_0
    sget-object v0, Lcom/broadcom/nfc/NfcFactory;->sFactoryInterface:Lcom/broadcom/nfc/INfcFactory;

    if-nez v0, :cond_1

    .line 69
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 66
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 71
    :cond_1
    :try_start_1
    new-instance v0, Lcom/broadcom/nfc/NfcFactory;

    invoke-direct {v0}, Lcom/broadcom/nfc/NfcFactory;-><init>()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v1

    return-object v0
.end method

.method private static initService()V
    .locals 3

    .prologue
    .line 112
    const-string v1, "NfcFacApi"

    const-string v2, "initService"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    const-string v1, "com.broadcom.nfc.Factory"

    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    .line 115
    .local v0, "b":Landroid/os/IBinder;
    if-nez v0, :cond_1

    .line 116
    const-string v1, "NfcFacApi"

    const-string v2, "initService; fail get binder"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    :cond_0
    :goto_0
    return-void

    .line 120
    :cond_1
    invoke-static {v0}, Lcom/broadcom/nfc/INfcFactory$Stub;->asInterface(Landroid/os/IBinder;)Lcom/broadcom/nfc/INfcFactory;

    move-result-object v1

    sput-object v1, Lcom/broadcom/nfc/NfcFactory;->sFactoryInterface:Lcom/broadcom/nfc/INfcFactory;

    .line 121
    sget-object v1, Lcom/broadcom/nfc/NfcFactory;->sFactoryInterface:Lcom/broadcom/nfc/INfcFactory;

    if-nez v1, :cond_0

    .line 122
    const-string v1, "NfcFacApi"

    const-string v2, "initService; fail get interface"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized getConfigInterface()Lcom/broadcom/nfc/INfcAdapterBrcmConfig;
    .locals 4

    .prologue
    .line 78
    monitor-enter p0

    :try_start_0
    sget-object v2, Lcom/broadcom/nfc/NfcFactory;->sFactoryInterface:Lcom/broadcom/nfc/INfcFactory;

    if-nez v2, :cond_0

    .line 79
    const-string v2, "NfcFacApi"

    const-string v3, "getConfigInterface; factory is null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    new-instance v2, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v2}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 78
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 84
    :cond_0
    :try_start_1
    sget-object v2, Lcom/broadcom/nfc/NfcFactory;->sFactoryInterface:Lcom/broadcom/nfc/INfcFactory;

    invoke-interface {v2}, Lcom/broadcom/nfc/INfcFactory;->getConfigInterface()Lcom/broadcom/nfc/INfcAdapterBrcmConfig;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 89
    .local v0, "config":Lcom/broadcom/nfc/INfcAdapterBrcmConfig;
    monitor-exit p0

    return-object v0

    .line 85
    .end local v0    # "config":Lcom/broadcom/nfc/INfcAdapterBrcmConfig;
    :catch_0
    move-exception v1

    .line 86
    .local v1, "e":Landroid/os/RemoteException;
    :try_start_2
    const-string v2, "NfcFacApi"

    const-string v3, "getConfigInterface; fail get config"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    new-instance v2, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v2}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method

.method public declared-synchronized getDtaInterface()Lcom/broadcom/nfc/INfcAdapterDta;
    .locals 4

    .prologue
    .line 96
    monitor-enter p0

    :try_start_0
    sget-object v2, Lcom/broadcom/nfc/NfcFactory;->sFactoryInterface:Lcom/broadcom/nfc/INfcFactory;

    if-nez v2, :cond_0

    .line 97
    const-string v2, "NfcFacApi"

    const-string v3, "getDtaInterface; factory is null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    new-instance v2, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v2}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 96
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 102
    :cond_0
    :try_start_1
    sget-object v2, Lcom/broadcom/nfc/NfcFactory;->sFactoryInterface:Lcom/broadcom/nfc/INfcFactory;

    invoke-interface {v2}, Lcom/broadcom/nfc/INfcFactory;->getDtaInterface()Lcom/broadcom/nfc/INfcAdapterDta;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 107
    .local v0, "dta":Lcom/broadcom/nfc/INfcAdapterDta;
    monitor-exit p0

    return-object v0

    .line 103
    .end local v0    # "dta":Lcom/broadcom/nfc/INfcAdapterDta;
    :catch_0
    move-exception v1

    .line 104
    .local v1, "e":Landroid/os/RemoteException;
    :try_start_2
    const-string v2, "NfcFacApi"

    const-string v3, "getConfigInterface; fail get dta"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    new-instance v2, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v2}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method
