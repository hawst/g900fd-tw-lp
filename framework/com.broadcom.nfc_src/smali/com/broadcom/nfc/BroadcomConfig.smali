.class public final Lcom/broadcom/nfc/BroadcomConfig;
.super Ljava/lang/Object;
.source "BroadcomConfig.java"


# static fields
.field static final TAG:Ljava/lang/String; = "BrcmCfgApi"

.field static mConfigInterface:Lcom/broadcom/nfc/INfcAdapterBrcmConfig;

.field static final sNfcConfig:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/nfc/NfcAdapter;",
            "Lcom/broadcom/nfc/BroadcomConfig;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final mAdapter:Landroid/nfc/NfcAdapter;

.field final mPackageName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 68
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/broadcom/nfc/BroadcomConfig;->sNfcConfig:Ljava/util/HashMap;

    return-void
.end method

.method constructor <init>(Landroid/nfc/NfcAdapter;)V
    .locals 2
    .param p1, "adapter"    # Landroid/nfc/NfcAdapter;

    .prologue
    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 112
    const-string v0, "BrcmCfgApi"

    const-string v1, "BroadcomConfig"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    iput-object p1, p0, Lcom/broadcom/nfc/BroadcomConfig;->mAdapter:Landroid/nfc/NfcAdapter;

    .line 114
    invoke-virtual {p1}, Landroid/nfc/NfcAdapter;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/broadcom/nfc/BroadcomConfig;->mPackageName:Ljava/lang/String;

    .line 115
    return-void
.end method

.method public static get(Landroid/nfc/NfcAdapter;)Lcom/broadcom/nfc/BroadcomConfig;
    .locals 4
    .param p0, "adapter"    # Landroid/nfc/NfcAdapter;

    .prologue
    .line 91
    const-string v2, "BrcmCfgApi"

    const-string v3, "get"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    invoke-virtual {p0}, Landroid/nfc/NfcAdapter;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 93
    .local v1, "context":Landroid/content/Context;
    if-nez v1, :cond_0

    .line 94
    new-instance v2, Ljava/lang/UnsupportedOperationException;

    const-string v3, "You must pass a context to your NfcAdapter to use the config APIs"

    invoke-direct {v2, v3}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 97
    :cond_0
    const-class v3, Lcom/broadcom/nfc/BroadcomConfig;

    monitor-enter v3

    .line 98
    :try_start_0
    sget-object v2, Lcom/broadcom/nfc/BroadcomConfig;->mConfigInterface:Lcom/broadcom/nfc/INfcAdapterBrcmConfig;

    if-nez v2, :cond_1

    .line 99
    invoke-static {p0}, Lcom/broadcom/nfc/BroadcomConfig;->initService(Landroid/nfc/NfcAdapter;)V

    .line 101
    :cond_1
    sget-object v2, Lcom/broadcom/nfc/BroadcomConfig;->sNfcConfig:Ljava/util/HashMap;

    invoke-virtual {v2, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/broadcom/nfc/BroadcomConfig;

    .line 102
    .local v0, "config":Lcom/broadcom/nfc/BroadcomConfig;
    if-nez v0, :cond_2

    .line 103
    new-instance v0, Lcom/broadcom/nfc/BroadcomConfig;

    .end local v0    # "config":Lcom/broadcom/nfc/BroadcomConfig;
    invoke-direct {v0, p0}, Lcom/broadcom/nfc/BroadcomConfig;-><init>(Landroid/nfc/NfcAdapter;)V

    .line 104
    .restart local v0    # "config":Lcom/broadcom/nfc/BroadcomConfig;
    sget-object v2, Lcom/broadcom/nfc/BroadcomConfig;->sNfcConfig:Ljava/util/HashMap;

    invoke-virtual {v2, p0, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    :cond_2
    monitor-exit v3

    return-object v0

    .line 107
    .end local v0    # "config":Lcom/broadcom/nfc/BroadcomConfig;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method static initService(Landroid/nfc/NfcAdapter;)V
    .locals 3
    .param p0, "adapter"    # Landroid/nfc/NfcAdapter;

    .prologue
    .line 72
    const-string v1, "BrcmCfgApi"

    const-string v2, "initService"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    invoke-static {}, Lcom/broadcom/nfc/NfcFactory;->getInstance()Lcom/broadcom/nfc/NfcFactory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/broadcom/nfc/NfcFactory;->getConfigInterface()Lcom/broadcom/nfc/INfcAdapterBrcmConfig;

    move-result-object v0

    .line 74
    .local v0, "service":Lcom/broadcom/nfc/INfcAdapterBrcmConfig;
    if-nez v0, :cond_0

    .line 75
    const-string v1, "BrcmCfgApi"

    const-string v2, "initService; fail get config interface"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v1

    .line 80
    :cond_0
    sput-object v0, Lcom/broadcom/nfc/BroadcomConfig;->mConfigInterface:Lcom/broadcom/nfc/INfcAdapterBrcmConfig;

    .line 82
    return-void
.end method


# virtual methods
.method public getConfig(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "configItem"    # Ljava/lang/String;

    .prologue
    .line 189
    const-string v1, "BrcmCfgApi"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getConfig; item="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " package name="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/broadcom/nfc/BroadcomConfig;->mPackageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    const-string v0, ""

    .line 193
    .local v0, "rval":Ljava/lang/String;
    :try_start_0
    sget-object v1, Lcom/broadcom/nfc/BroadcomConfig;->mConfigInterface:Lcom/broadcom/nfc/INfcAdapterBrcmConfig;

    iget-object v2, p0, Lcom/broadcom/nfc/BroadcomConfig;->mPackageName:Ljava/lang/String;

    invoke-interface {v1, v2, p1}, Lcom/broadcom/nfc/INfcAdapterBrcmConfig;->getConfig(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 198
    :goto_0
    return-object v0

    .line 195
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public getFirmwareConfig(I)[B
    .locals 4
    .param p1, "paramId"    # I

    .prologue
    .line 150
    const-string v1, "BrcmCfgApi"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getFirmwareConfig; package name="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/broadcom/nfc/BroadcomConfig;->mPackageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 154
    :try_start_0
    sget-object v1, Lcom/broadcom/nfc/BroadcomConfig;->mConfigInterface:Lcom/broadcom/nfc/INfcAdapterBrcmConfig;

    iget-object v2, p0, Lcom/broadcom/nfc/BroadcomConfig;->mPackageName:Ljava/lang/String;

    invoke-interface {v1, v2, p1}, Lcom/broadcom/nfc/INfcAdapterBrcmConfig;->getFirmwareConfig(Ljava/lang/String;I)[B
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 158
    :goto_0
    return-object v1

    .line 156
    :catch_0
    move-exception v0

    .line 158
    .local v0, "e":Landroid/os/RemoteException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method getService()Lcom/broadcom/nfc/INfcAdapterBrcmConfig;
    .locals 1

    .prologue
    .line 119
    sget-object v0, Lcom/broadcom/nfc/BroadcomConfig;->mConfigInterface:Lcom/broadcom/nfc/INfcAdapterBrcmConfig;

    return-object v0
.end method

.method public setConfig(Ljava/lang/String;)Z
    .locals 5
    .param p1, "configXml"    # Ljava/lang/String;

    .prologue
    .line 169
    const-string v2, "BrcmCfgApi"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setConfig; package name="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/broadcom/nfc/BroadcomConfig;->mPackageName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 170
    const/4 v1, 0x0

    .line 173
    .local v1, "rval":Z
    :try_start_0
    sget-object v2, Lcom/broadcom/nfc/BroadcomConfig;->mConfigInterface:Lcom/broadcom/nfc/INfcAdapterBrcmConfig;

    iget-object v3, p0, Lcom/broadcom/nfc/BroadcomConfig;->mPackageName:Ljava/lang/String;

    invoke-interface {v2, v3, p1}, Lcom/broadcom/nfc/INfcAdapterBrcmConfig;->setConfig(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 179
    :goto_0
    return v1

    .line 175
    :catch_0
    move-exception v0

    .line 177
    .local v0, "e":Landroid/os/RemoteException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setFirmwareConfig(I[B)Z
    .locals 5
    .param p1, "paramId"    # I
    .param p2, "data"    # [B

    .prologue
    .line 130
    const-string v2, "BrcmCfgApi"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setFirmwareConfig; package name="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/broadcom/nfc/BroadcomConfig;->mPackageName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    const/4 v1, 0x0

    .line 134
    .local v1, "rval":Z
    :try_start_0
    sget-object v2, Lcom/broadcom/nfc/BroadcomConfig;->mConfigInterface:Lcom/broadcom/nfc/INfcAdapterBrcmConfig;

    iget-object v3, p0, Lcom/broadcom/nfc/BroadcomConfig;->mPackageName:Ljava/lang/String;

    invoke-interface {v2, v3, p1, p2}, Lcom/broadcom/nfc/INfcAdapterBrcmConfig;->setFirmwareConfig(Ljava/lang/String;I[B)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 140
    :goto_0
    return v1

    .line 136
    :catch_0
    move-exception v0

    .line 138
    .local v0, "e":Landroid/os/RemoteException;
    const/4 v1, 0x0

    goto :goto_0
.end method
