.class public final Lcom/broadcom/nfc/NfcAdapterDta;
.super Ljava/lang/Object;
.source "NfcAdapterDta.java"


# static fields
.field static final TAG:Ljava/lang/String; = "BrcmDtaApi"

.field static mDtaInterface:Lcom/broadcom/nfc/INfcAdapterDta;

.field static final sNfcAdapterDta:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/nfc/NfcAdapter;",
            "Lcom/broadcom/nfc/NfcAdapterDta;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final mAdapter:Landroid/nfc/NfcAdapter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 68
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/broadcom/nfc/NfcAdapterDta;->sNfcAdapterDta:Ljava/util/HashMap;

    return-void
.end method

.method constructor <init>(Landroid/nfc/NfcAdapter;)V
    .locals 2
    .param p1, "adapter"    # Landroid/nfc/NfcAdapter;

    .prologue
    .line 119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 120
    const-string v0, "BrcmDtaApi"

    const-string v1, "NfcAdapterDta"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    iput-object p1, p0, Lcom/broadcom/nfc/NfcAdapterDta;->mAdapter:Landroid/nfc/NfcAdapter;

    .line 122
    return-void
.end method

.method public static get(Landroid/nfc/NfcAdapter;)Lcom/broadcom/nfc/NfcAdapterDta;
    .locals 4
    .param p0, "adapter"    # Landroid/nfc/NfcAdapter;

    .prologue
    .line 95
    invoke-virtual {p0}, Landroid/nfc/NfcAdapter;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 96
    .local v0, "context":Landroid/content/Context;
    if-nez v0, :cond_0

    .line 97
    new-instance v2, Ljava/lang/UnsupportedOperationException;

    const-string v3, "You must pass a context to your NfcAdapter to use the DTA APIs"

    invoke-direct {v2, v3}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 100
    :cond_0
    const-class v3, Lcom/broadcom/nfc/NfcAdapterDta;

    monitor-enter v3

    .line 101
    :try_start_0
    sget-object v2, Lcom/broadcom/nfc/NfcAdapterDta;->mDtaInterface:Lcom/broadcom/nfc/INfcAdapterDta;

    if-nez v2, :cond_1

    .line 102
    invoke-static {p0}, Lcom/broadcom/nfc/NfcAdapterDta;->initService(Landroid/nfc/NfcAdapter;)V

    .line 104
    :cond_1
    sget-object v2, Lcom/broadcom/nfc/NfcAdapterDta;->sNfcAdapterDta:Ljava/util/HashMap;

    invoke-virtual {v2, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/broadcom/nfc/NfcAdapterDta;

    .line 105
    .local v1, "dta":Lcom/broadcom/nfc/NfcAdapterDta;
    if-nez v1, :cond_2

    .line 106
    new-instance v1, Lcom/broadcom/nfc/NfcAdapterDta;

    .end local v1    # "dta":Lcom/broadcom/nfc/NfcAdapterDta;
    invoke-direct {v1, p0}, Lcom/broadcom/nfc/NfcAdapterDta;-><init>(Landroid/nfc/NfcAdapter;)V

    .line 107
    .restart local v1    # "dta":Lcom/broadcom/nfc/NfcAdapterDta;
    sget-object v2, Lcom/broadcom/nfc/NfcAdapterDta;->sNfcAdapterDta:Ljava/util/HashMap;

    invoke-virtual {v2, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    :cond_2
    monitor-exit v3

    return-object v1

    .line 110
    .end local v1    # "dta":Lcom/broadcom/nfc/NfcAdapterDta;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method static initService(Landroid/nfc/NfcAdapter;)V
    .locals 3
    .param p0, "adapter"    # Landroid/nfc/NfcAdapter;

    .prologue
    .line 76
    const-string v1, "BrcmDtaApi"

    const-string v2, "initService"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    invoke-static {}, Lcom/broadcom/nfc/NfcFactory;->getInstance()Lcom/broadcom/nfc/NfcFactory;

    move-result-object v1

    invoke-virtual {v1}, Lcom/broadcom/nfc/NfcFactory;->getDtaInterface()Lcom/broadcom/nfc/INfcAdapterDta;

    move-result-object v0

    .line 78
    .local v0, "service":Lcom/broadcom/nfc/INfcAdapterDta;
    if-nez v0, :cond_0

    .line 79
    const-string v1, "BrcmDtaApi"

    const-string v2, "initService; fail get DTA interface"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v1

    .line 84
    :cond_0
    sput-object v0, Lcom/broadcom/nfc/NfcAdapterDta;->mDtaInterface:Lcom/broadcom/nfc/INfcAdapterDta;

    .line 86
    return-void
.end method


# virtual methods
.method public config(II)Z
    .locals 3
    .param p1, "configItem"    # I
    .param p2, "configData"    # I

    .prologue
    .line 212
    const/4 v1, 0x0

    .line 215
    .local v1, "rval":Z
    :try_start_0
    sget-object v2, Lcom/broadcom/nfc/NfcAdapterDta;->mDtaInterface:Lcom/broadcom/nfc/INfcAdapterDta;

    invoke-interface {v2, p1, p2}, Lcom/broadcom/nfc/INfcAdapterDta;->config(II)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 221
    :goto_0
    return v1

    .line 217
    :catch_0
    move-exception v0

    .line 219
    .local v0, "e":Landroid/os/RemoteException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public disable()Z
    .locals 3

    .prologue
    .line 160
    const/4 v1, 0x0

    .line 163
    .local v1, "rval":Z
    :try_start_0
    sget-object v2, Lcom/broadcom/nfc/NfcAdapterDta;->mDtaInterface:Lcom/broadcom/nfc/INfcAdapterDta;

    invoke-interface {v2}, Lcom/broadcom/nfc/INfcAdapterDta;->disable()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 169
    :goto_0
    return v1

    .line 165
    :catch_0
    move-exception v0

    .line 167
    .local v0, "e":Landroid/os/RemoteException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public enable(Z)Z
    .locals 3
    .param p1, "autoStart"    # Z

    .prologue
    .line 141
    const/4 v1, 0x0

    .line 144
    .local v1, "rval":Z
    :try_start_0
    sget-object v2, Lcom/broadcom/nfc/NfcAdapterDta;->mDtaInterface:Lcom/broadcom/nfc/INfcAdapterDta;

    invoke-interface {v2, p1}, Lcom/broadcom/nfc/INfcAdapterDta;->enable(Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 150
    :goto_0
    return v1

    .line 146
    :catch_0
    move-exception v0

    .line 148
    .local v0, "e":Landroid/os/RemoteException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method getService()Lcom/broadcom/nfc/INfcAdapterDta;
    .locals 1

    .prologue
    .line 131
    sget-object v0, Lcom/broadcom/nfc/NfcAdapterDta;->mDtaInterface:Lcom/broadcom/nfc/INfcAdapterDta;

    return-object v0
.end method

.method public start(I[B)Z
    .locals 3
    .param p1, "patternNumber"    # I
    .param p2, "tlv"    # [B

    .prologue
    .line 179
    const/4 v1, 0x0

    .line 182
    .local v1, "rval":Z
    :try_start_0
    sget-object v2, Lcom/broadcom/nfc/NfcAdapterDta;->mDtaInterface:Lcom/broadcom/nfc/INfcAdapterDta;

    invoke-interface {v2, p1, p2}, Lcom/broadcom/nfc/INfcAdapterDta;->start(I[B)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 188
    :goto_0
    return v1

    .line 184
    :catch_0
    move-exception v0

    .line 186
    .local v0, "e":Landroid/os/RemoteException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public stop()Z
    .locals 3

    .prologue
    .line 198
    const/4 v1, 0x0

    .line 201
    .local v1, "rval":Z
    :try_start_0
    sget-object v2, Lcom/broadcom/nfc/NfcAdapterDta;->mDtaInterface:Lcom/broadcom/nfc/INfcAdapterDta;

    invoke-interface {v2}, Lcom/broadcom/nfc/INfcAdapterDta;->stop()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 207
    :goto_0
    return v1

    .line 203
    :catch_0
    move-exception v0

    .line 205
    .local v0, "e":Landroid/os/RemoteException;
    const/4 v1, 0x0

    goto :goto_0
.end method
