.class public Lcom/sec/android/seccamera/SecCamera;
.super Ljava/lang/Object;
.source "SecCamera.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/seccamera/SecCamera$1;,
        Lcom/sec/android/seccamera/SecCamera$Parameters;,
        Lcom/sec/android/seccamera/SecCamera$Area;,
        Lcom/sec/android/seccamera/SecCamera$Size;,
        Lcom/sec/android/seccamera/SecCamera$PafResult;,
        Lcom/sec/android/seccamera/SecCamera$CameraCurrentSet;,
        Lcom/sec/android/seccamera/SecCamera$ErrorCallback;,
        Lcom/sec/android/seccamera/SecCamera$Face;,
        Lcom/sec/android/seccamera/SecCamera$FaceDetectionListener;,
        Lcom/sec/android/seccamera/SecCamera$OnZoomChangeListener;,
        Lcom/sec/android/seccamera/SecCamera$PictureCallback;,
        Lcom/sec/android/seccamera/SecCamera$ShutterCallback;,
        Lcom/sec/android/seccamera/SecCamera$AutoFocusMoveCallback;,
        Lcom/sec/android/seccamera/SecCamera$AutoFocusCallback;,
        Lcom/sec/android/seccamera/SecCamera$EventHandler;,
        Lcom/sec/android/seccamera/SecCamera$PreviewCallback;,
        Lcom/sec/android/seccamera/SecCamera$OnAEResultListener;,
        Lcom/sec/android/seccamera/SecCamera$OnCameraCurrentSettingListener;,
        Lcom/sec/android/seccamera/SecCamera$OnPafResultListener;,
        Lcom/sec/android/seccamera/SecCamera$OnMagicShotModeEventListener;,
        Lcom/sec/android/seccamera/SecCamera$OnOutFocusShotModeEventListener;,
        Lcom/sec/android/seccamera/SecCamera$OnSecImagingEventListener;,
        Lcom/sec/android/seccamera/SecCamera$OnDualEventListener;,
        Lcom/sec/android/seccamera/SecCamera$OnLightConditionChangedListener;,
        Lcom/sec/android/seccamera/SecCamera$OnAutoLowLightDetectionListener;,
        Lcom/sec/android/seccamera/SecCamera$OnSecImageEffectListner;,
        Lcom/sec/android/seccamera/SecCamera$OnBeautyShotEventListener;,
        Lcom/sec/android/seccamera/SecCamera$OnDramaShotEventListener;,
        Lcom/sec/android/seccamera/SecCamera$OnGolfShotEventListener;,
        Lcom/sec/android/seccamera/SecCamera$OnNotifyFirstPreviewFrameEventListener;,
        Lcom/sec/android/seccamera/SecCamera$OnHazeRemovalEventListener;,
        Lcom/sec/android/seccamera/SecCamera$OnMultiFrameShotEventListener;,
        Lcom/sec/android/seccamera/SecCamera$OnHDRShotEventListener;,
        Lcom/sec/android/seccamera/SecCamera$OnSelfieShotDetectionSuccessListener;,
        Lcom/sec/android/seccamera/SecCamera$OnSmileShotDetectionSuccessListener;,
        Lcom/sec/android/seccamera/SecCamera$OnObjectTrackingMsgListener;,
        Lcom/sec/android/seccamera/SecCamera$OnCartoonShotEventListener;,
        Lcom/sec/android/seccamera/SecCamera$OnActionShotEventListener;,
        Lcom/sec/android/seccamera/SecCamera$OnBurstShotEventListener;,
        Lcom/sec/android/seccamera/SecCamera$OnContinuousShotEventListener;,
        Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;,
        Lcom/sec/android/seccamera/SecCamera$OnPanoramaEventListener;,
        Lcom/sec/android/seccamera/SecCamera$AutoExposureCallback;,
        Lcom/sec/android/seccamera/SecCamera$CameraCurrentSettingListener;,
        Lcom/sec/android/seccamera/SecCamera$PhaseAFCallback;,
        Lcom/sec/android/seccamera/SecCamera$ShotAndMoreEventListener;,
        Lcom/sec/android/seccamera/SecCamera$SelectiveFocusEventListener;,
        Lcom/sec/android/seccamera/SecCamera$SecImagingEventListener;,
        Lcom/sec/android/seccamera/SecCamera$DualEventListener;,
        Lcom/sec/android/seccamera/SecCamera$LightConditionChangedListener;,
        Lcom/sec/android/seccamera/SecCamera$AutoLowLightDetectionListener;,
        Lcom/sec/android/seccamera/SecCamera$SecImageEffectListner;,
        Lcom/sec/android/seccamera/SecCamera$BeautyEventListener;,
        Lcom/sec/android/seccamera/SecCamera$DramaShotEventListener;,
        Lcom/sec/android/seccamera/SecCamera$GolfShotEventListener;,
        Lcom/sec/android/seccamera/SecCamera$HazeRemovalEventListener;,
        Lcom/sec/android/seccamera/SecCamera$MultiFrameEventListener;,
        Lcom/sec/android/seccamera/SecCamera$HdrEventListener;,
        Lcom/sec/android/seccamera/SecCamera$SelfieDetectionListener;,
        Lcom/sec/android/seccamera/SecCamera$BurstEventListener;,
        Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;,
        Lcom/sec/android/seccamera/SecCamera$PanoramaEventListener;,
        Lcom/sec/android/seccamera/SecCamera$CameraInfo;
    }
.end annotation


# static fields
.field public static final ACTION_NEW_PICTURE:Ljava/lang/String; = "android.hardware.action.NEW_PICTURE"

.field public static final ACTION_NEW_VIDEO:Ljava/lang/String; = "android.hardware.action.NEW_VIDEO"

.field private static final ACTION_SHOT_CANCELSERIES:I = 0x46e

.field private static final ACTION_SHOT_CAPTURED:I = 0xf045

.field private static final ACTION_SHOT_CREATING_RESULT_COMPLETED:I = 0xf043

.field private static final ACTION_SHOT_CREATING_RESULT_PROGRESS:I = 0xf042

.field private static final ACTION_SHOT_CREATING_RESULT_STARTED:I = 0xf041

.field private static final ACTION_SHOT_FINALIZE:I = 0x470

.field private static final ACTION_SHOT_INITIALIZE:I = 0x46b

.field private static final ACTION_SHOT_PROGRESS_ACQUISITION:I = 0xf044

.field private static final ACTION_SHOT_RECT:I = 0xf046

.field private static final ACTION_SHOT_SETRESOLUTION:I = 0x46c

.field private static final ACTION_SHOT_STARTSERIES:I = 0x46d

.field private static final ACTION_SHOT_STOPSERIES:I = 0x46f

.field private static final ADDME_SHOT_CANCEL_CAPTURE:I = 0x479

.field private static final ADDME_SHOT_CAPUTRED_FIRSTPERSON:I = 0xf052

.field private static final ADDME_SHOT_ERR:I = 0xf051

.field private static final ADDME_SHOT_FINALIZE:I = 0x47a

.field private static final ADDME_SHOT_HANDLE_SNAPSHOT:I = 0x478

.field private static final ADDME_SHOT_INIT:I = 0x475

.field private static final ADDME_SHOT_PROGRESS_STITCHING:I = 0xf053

.field private static final ADDME_SHOT_START_CAPTURE:I = 0x476

.field private static final ADDME_SHOT_SWITCH_POSITION:I = 0x477

.field private static final AE_RESULT:I = 0xf351

.field private static final AUTO_LOW_LIGHT_DETECTION_CHANGED:I = 0xf231

.field private static final AUTO_LOW_LIGHT_SET:I = 0x547

.field private static final BATTERY_OVER_HEAT:I = 0x571

.field private static final BEAUTY_EYE_ENLARGE:I = 0x4a1

.field private static final BEAUTY_FACE_RETOUCH:I = 0x49d

.field private static final BEAUTY_LIVE_EFFECT:I = 0x49e

.field private static final BEAUTY_SHOT_MANUAL_MODE:I = 0x49f

.field private static final BEAUTY_SHOT_PROGRESS_RENDERING:I = 0xf151

.field private static final BEAUTY_SLIM_FACE:I = 0x4a0

.field private static final BURST_SHOT_CAPTURE:I = 0x48a

.field private static final BURST_SHOT_CAPTURING_PROGRESSED:I = 0xf091

.field private static final BURST_SHOT_CAPTURING_STOPPED:I = 0xf092

.field private static final BURST_SHOT_DURATION:I = 0x48e

.field private static final BURST_SHOT_FILE_STRING:I = 0xf094

.field private static final BURST_SHOT_SAVING_COMPLETED:I = 0xf093

.field private static final BURST_SHOT_SETUP:I = 0x48f

.field private static final BURST_SHOT_START_CAPTURE:I = 0x48b

.field private static final BURST_SHOT_STOP_AND_ENCODING:I = 0x48c

.field private static final BURST_SHOT_STORING:I = 0x489

.field private static final BURST_SHOT_TERMINATE:I = 0x48d

.field private static final CAMERA_CMD_GET_WB_CUSTOM_VALUE:I = 0x4cf

.field private static final CAMERA_CMD_RESET_WB_CUSTOM_VALUE:I = 0x56f

.field private static final CAMERA_CMD_SMART_AUTO_S1_PUSH:I = 0x4e3

.field private static final CAMERA_CMD_SMART_AUTO_S1_RELEASE:I = 0x4e4

.field public static final CAMERA_ERROR_PREVIEWFRAME_TIMEOUT:I = 0x3e9

.field public static final CAMERA_ERROR_PRIORITY_DIED:I = 0xc8

.field public static final CAMERA_ERROR_SERVER_DIED:I = 0x64

.field public static final CAMERA_ERROR_UNKNOWN:I = 0x1

.field private static final CAMERA_FACE_DETECTION_DMC_SW:I = 0x5

.field private static final CAMERA_FACE_DETECTION_HW:I = 0x0

.field private static final CAMERA_FACE_DETECTION_SAMSUNG_SW:I = 0x4

.field private static final CAMERA_FACE_DETECTION_SW:I = 0x1

.field private static final CAMERA_FACE_DETECTION_SW_ONE_EYE:I = 0x2

.field private static final CAMERA_FACE_DETECTION_SW_TWO_EYE:I = 0x3

.field public static final CAMERA_HAL_API_VERSION_1_0:I = 0x100

.field private static final CAMERA_HAL_API_VERSION_NORMAL_CONNECT:I = -0x2

.field private static final CAMERA_HAL_API_VERSION_UNSPECIFIED:I = -0x1

.field private static final CAMERA_MSG_ALL_MSGS:I = -0x1

.field private static final CAMERA_MSG_AUTO_PARAMETERS_NOTIFY:I = 0x20000

.field private static final CAMERA_MSG_COMPRESSED_IMAGE:I = 0x100

.field private static final CAMERA_MSG_ERROR:I = 0x1

.field private static final CAMERA_MSG_FIRMWARE_NOTIFY:I = 0x80000

.field private static final CAMERA_MSG_FOCUS:I = 0x4

.field private static final CAMERA_MSG_FOCUS_MOVE:I = 0x800

.field private static final CAMERA_MSG_MANUAL_FOCUS_NOTIFY:I = 0x100000

.field private static final CAMERA_MSG_POSTVIEW_FRAME:I = 0x40

.field private static final CAMERA_MSG_PREVIEW_FRAME:I = 0x10

.field private static final CAMERA_MSG_PREVIEW_METADATA:I = 0x400

.field private static final CAMERA_MSG_RAW_IMAGE:I = 0x80

.field private static final CAMERA_MSG_RAW_IMAGE_NOTIFY:I = 0x200

.field private static final CAMERA_MSG_SHOT_END:I = 0x40000

.field private static final CAMERA_MSG_SHUTTER:I = 0x2

.field private static final CAMERA_MSG_VIDEO_FRAME:I = 0x20

.field private static final CAMERA_MSG_ZOOM:I = 0x8

.field private static final CAMERA_MSG_ZOOM_STEP_NOTIFY:I = 0x10000

.field private static final CARTOON_SHOT_PROGRESS_RENDERING:I = 0xf071

.field private static final CARTOON_SHOT_SELECT_MODE:I = 0x47f

.field private static final CHANGE_MAIN_LCD:I = 0x570

.field private static final CHECK_MARKER_OF_SAMSUNG_DEFINED_CALLBACK_MSGS:I = 0xf000

.field private static final COMMON_CANCEL_TAKE_PICTURE:I = 0x5c0

.field private static final COMMON_STOP_TAKE_PICTURE:I = 0x5bf

.field private static final CONTINUOUS_SHOT_CAPTURING_PROGRESSED:I = 0xf031

.field private static final CONTINUOUS_SHOT_CAPTURING_STOPPED:I = 0xf032

.field private static final CONTINUOUS_SHOT_SAVING_COMPLETED:I = 0xf033

.field private static final CONTINUOUS_SHOT_SOUND:I = 0x464

.field private static final CONTINUOUS_SHOT_START_CAPTURE:I = 0x461

.field private static final CONTINUOUS_SHOT_STOP_AND_ENCODING:I = 0x462

.field private static final CONTINUOUS_SHOT_TERMINATE:I = 0x463

.field private static final DEVICE_ORIENTATION:I = 0x5f1

.field private static final DISTORTION_EFFECTS_SHOT_CAPTURE:I = 0x5ba

.field private static final DISTORTION_EFFECTS_SHOT_INFO:I = 0x5b8

.field private static final DISTORTION_EFFECTS_SHOT_SET_MODE:I = 0x5b9

.field private static final DRAMA_SHOT_CANCEL:I = 0x535

.field private static final DRAMA_SHOT_CAPTURING_PROGRESS:I = 0xf221

.field private static final DRAMA_SHOT_ERROR:I = 0xf223

.field private static final DRAMA_SHOT_INPUT_YUV_STRING:I = 0xf224

.field private static final DRAMA_SHOT_MODE:I = 0x536

.field private static final DRAMA_SHOT_PROGRESS_POSTPROCESSING:I = 0xf222

.field private static final DRAMA_SHOT_RESULT_YUV_STRING:I = 0xf225

.field private static final DRAMA_SHOT_START:I = 0x533

.field private static final DRAMA_SHOT_STOP:I = 0x534

.field private static final DRAMA_SHOT_STORAGE:I = 0x537

.field private static final DUAL_CAMERA_CAPTURE_STATUS_CHANGED:I = 0xf251

.field private static final DUAL_CAMERA_TRACKING_STATUS_CHANGED:I = 0xf253

.field private static final DUAL_MODE_SHOT_ASYNC_CAPTURE:I = 0x55e

.field private static final EACCESS:I = -0xd

.field private static final EBUSY:I = -0x10

.field public static final EFFECT_REAR_BOTTOM_FRONT_TOP:I = 0x0

.field public static final EFFECT_REAR_TOP_FRONT_BOTTOM:I = 0x1

.field private static final EINVAL:I = -0x16

.field private static final ENODEV:I = -0x13

.field private static final ENOSYS:I = -0x26

.field private static final EOPNOTSUPP:I = -0x5f

.field private static final EUSERS:I = -0x57

.field private static final EXTERNAL_SHOOTING_MODES_MAP_GENERIC_PARAM:Ljava/lang/String; = "[external_shooting_modes_map]"

.field private static final FACE_DETECTION_HINT:I = 0x4a7

.field public static final FOCUS_MODE_ALL_IN_FOCUS:I = 0x4

.field public static final FOCUS_MODE_OUT_FOCUS:I = 0x1

.field public static final FOCUS_MODE_OUT_FOCUS_FAST:I = 0x2

.field public static final FOCUS_MODE_REFOCUS:I = 0x3

.field private static final GOLF_SHOT_CAPTURED:I = 0xf195

.field private static final GOLF_SHOT_CREATING_RESULT_COMPLETED:I = 0xf193

.field private static final GOLF_SHOT_CREATING_RESULT_PROGRESS:I = 0xf192

.field private static final GOLF_SHOT_CREATING_RESULT_STARTED:I = 0xf191

.field private static final GOLF_SHOT_ERROR:I = 0xf196

.field private static final GOLF_SHOT_SAVE:I = 0x521

.field private static final GOLF_SHOT_SAVE_RESULT_PROGRESS:I = 0xf194

.field private static final GOLF_SHOT_START:I = 0x51f

.field private static final GOLF_SHOT_STOP:I = 0x520

.field private static final HAL_AE_AWB_LOCK_UNLOCK:I = 0x5dd

.field private static final HAL_AE_CONTROL:I = 0x661

.field private static final HAL_AE_POSITION:I = 0x65f

.field private static final HAL_AE_SIZE:I = 0x660

.field private static final HAL_AF_LAMP_CONTROL:I = 0x613

.field private static final HAL_CANCEL_AF_FOCUS_AREA:I = 0x611

.field private static final HAL_CAPTURE_END:I = 0x612

.field private static final HAL_DELETE_BURST_TAKE:I = 0x625

.field private static final HAL_DISABLE_POSTVIEW_TO_OVERLAY:I = 0x5e5

.field private static final HAL_DONE_CHK_DATALINE:I = 0xf002

.field private static final HAL_ENABLE_CURRENT_SET:I = 0x66b

.field private static final HAL_ENABLE_LIGHT_CONDITION:I = 0x709

.field private static final HAL_ENABLE_PAF_RESULT:I = 0x66c

.field private static final HAL_FACE_DETECT_LOCK_UNLOCK:I = 0x5de

.field private static final HAL_FLASH_POPUP:I = 0x616

.field private static final HAL_FLUSH_ION_MEMORY:I = 0x619

.field private static final HAL_MSG_OBJ_TRACKING:I = 0xf001

.field private static final HAL_OBJECT_POSITION:I = 0x5df

.field private static final HAL_OBJECT_TRACKING_STARTSTOP:I = 0x5e0

.field private static final HAL_ON_AE_AWB:I = 0x6a4

.field private static final HAL_QUICK_VIEW_CANCEL:I = 0x632

.field private static final HAL_SEND_EXT_MSG_START:I = 0x63a

.field private static final HAL_SEND_FACE_ORIENTATION:I = 0x5fa

.field private static final HAL_SET_3D_PREVIEW_DISPLAY:I = 0x638

.field private static final HAL_SET_BULK_PRAMETER:I = 0x63b

.field private static final HAL_SET_DEFAULT_IMEI:I = 0x5e3

.field private static final HAL_SET_FILTER_EFFECT:I = 0x641

.field private static final HAL_SET_FLASH_CHARGING_STATUS:I = 0x614

.field private static final HAL_SET_FOCUS_ICON_SIZE:I = 0x639

.field private static final HAL_SET_FRONT_SENSOR_MIRROR:I = 0x5e6

.field private static final HAL_SET_IFUNCTION_PUSH:I = 0x64b

.field private static final HAL_SET_IFUNCTION_RELEASE:I = 0x631

.field private static final HAL_SET_INTERVAL_SHOT_MANUAL_FOCUS:I = 0x64a

.field private static final HAL_SET_MANUAL_FOCUS_POSITION:I = 0x630

.field private static final HAL_SET_PROGRAM_SHIFT:I = 0x635

.field private static final HAL_SET_SAMSUNG_CAMERA:I = 0x5e4

.field private static final HAL_SET_ZOOM_STEP:I = 0x615

.field private static final HAL_SHOT_CAPTURE_START:I = 0x62d

.field private static final HAL_SHOT_CAPTURE_STOP:I = 0x62e

.field private static final HAL_SMART_CAPTURE_START:I = 0x633

.field private static final HAL_SMART_CAPTURE_STOP:I = 0x634

.field private static final HAL_SOUND_AND_SHOT_MIC_CONTROL:I = 0x642

.field private static final HAL_START_3D_PREVIEW:I = 0x637

.field private static final HAL_START_ANIMATED_PHOTO:I = 0x657

.field private static final HAL_START_BURST_TAKE:I = 0x623

.field private static final HAL_START_CONTINUOUS_AF:I = 0x60f

.field private static final HAL_START_EFFECT_RECORDING:I = 0x655

.field private static final HAL_START_FACEZOOM:I = 0x5fb

.field private static final HAL_START_PAF_CALLBACK:I = 0x672

.field private static final HAL_START_SENSER_CLEANING:I = 0x62f

.field private static final HAL_START_ZOOM:I = 0x67d

.field private static final HAL_STOP_ANIMATED_PHOTO:I = 0x658

.field private static final HAL_STOP_BURST_TAKE:I = 0x624

.field private static final HAL_STOP_CHK_DATALINE:I = 0x5e2

.field private static final HAL_STOP_CONTINUOUS_AF:I = 0x610

.field private static final HAL_STOP_EFFECT_RECORDING:I = 0x656

.field private static final HAL_STOP_FACEZOOM:I = 0x5fc

.field private static final HAL_STOP_PAF_CALLBACK:I = 0x673

.field private static final HAL_STOP_ZOOM:I = 0x67e

.field private static final HAL_TOUCH_AF_STARTSTOP:I = 0x5e1

.field private static final HAZE_REMOVAL_SHOT_PROGRESS_POSTPROCESSING:I = 0xf281

.field private static final HDR_PICTURE_MODE_CHANGE:I = 0x4f9

.field private static final HDR_SHOT_ALL_PROGRESS_COMPLETED:I = 0xf084

.field private static final HDR_SHOT_MODE_CHANGE:I = 0x4f7

.field private static final HDR_SHOT_RESULT_COMPLETED:I = 0xf083

.field private static final HDR_SHOT_RESULT_PROGRESS:I = 0xf082

.field private static final HDR_SHOT_RESULT_STARTED:I = 0xf081

.field private static final HDR_SHOT_YUV_MODE_CHANGE:I = 0x4f8

.field private static final HDR_SHOT_YUV_STRING:I = 0xf085

.field private static final HISTOGRAM_DATA:I = 0xf261

.field private static final HISTOGRAM_SET_INCREMENT:I = 0x553

.field private static final HISTOGRAM_SET_SKIP_RATE:I = 0x554

.field private static final HISTOGRAM_START:I = 0x551

.field private static final HISTOGRAM_STOP:I = 0x552

.field private static final LIGHT_CONDITION_CHANGED:I = 0xf232

.field private static final LIGHT_CONDITION_ENABLE:I = 0x549

.field private static final LOW_LIGHT_SHOT_SET:I = 0x4f0

.field private static final MAGICFRAME_SHOT_PROGRESS_RENDERING:I = 0xf131

.field private static final MAGIC_SHOT_APPLICABLE_MODES:I = 0xf334

.field private static final MAGIC_SHOT_CANCEL:I = 0x58f

.field private static final MAGIC_SHOT_CAPTURE_PROGRESS:I = 0xf331

.field private static final MAGIC_SHOT_CAPTURING_STOPPED:I = 0xf335

.field private static final MAGIC_SHOT_PROCESS_COMPLETE:I = 0xf333

.field private static final MAGIC_SHOT_PROCESS_PROGRESS:I = 0xf332

.field private static final MAGIC_SHOT_START:I = 0x58d

.field public static final MAGIC_SHOT_STATE_BESTPHOTO:I = 0x1

.field public static final MAGIC_SHOT_STATE_PICACTION:I = 0x8

.field public static final MAGIC_SHOT_STATE_PICBEST:I = 0x2

.field public static final MAGIC_SHOT_STATE_PICLEAR:I = 0x4

.field public static final MAGIC_SHOT_STATE_PICMOTION:I = 0x10

.field private static final MAGIC_SHOT_STOP:I = 0x58e

.field private static final METADATA_LENS_POSITION:I = 0xf342

.field private static final METADATA_SINGLEPAF:I = 0xf341

.field private static final MULTIPLE_MAINJPEG_COUNT:I = 0x4d9

.field private static final MULTI_FRAME_SHOT_CAPTURE:I = 0x4ee

.field private static final MULTI_FRAME_SHOT_CAPTURING_STOPPED:I = 0xf122

.field private static final MULTI_FRAME_SHOT_PROGRESS_POSTPROCESSING:I = 0xf123

.field private static final MULTI_FRAME_SHOT_START:I = 0x4ed

.field private static final MULTI_FRAME_SHOT_TERMINATE:I = 0x4ef

.field private static final NO_ERROR:I = 0x0

.field private static final Notify_FIRST_PREVIEW_FRAME_EVENT:I = 0xf171

.field public static final OUTFOCUS_ERR_AF:I = -0x1

.field public static final OUTFOCUS_ERR_INF:I = -0x2

.field public static final OUTFOCUS_ERR_NONE:I = 0x0

.field public static final OUTFOCUS_ERR_SEGMENTATION:I = -0x3

.field private static final OUTFOCUS_SHOT_CAPTURE_PROGRESS:I = 0xf322

.field private static final OUTFOCUS_SHOT_OPERATION_MODE:I = 0x583

.field private static final OUTFOCUS_SHOT_PROCESS_COMPLETE:I = 0xf323

.field private static final OUTFOCUS_SHOT_PROCESS_PROGRESS:I = 0xf321

.field private static final PANORAMA_3D_SHOT_CANCEL:I = 0x496

.field private static final PANORAMA_3D_SHOT_FINALIZE:I = 0x495

.field private static final PANORAMA_3D_SHOT_SHUTTER_START:I = 0x497

.field private static final PANORAMA_3D_SHOT_SHUTTER_STOP:I = 0x498

.field private static final PANORAMA_3D_SHOT_START:I = 0x493

.field private static final PANORAMA_3D_SHOT_STOP:I = 0x494

.field public static final PANORAMA_DIRECTION_DOWN:I = 0x8

.field public static final PANORAMA_DIRECTION_DOWN_LEFT:I = 0xa

.field public static final PANORAMA_DIRECTION_DOWN_RIGHT:I = 0x9

.field public static final PANORAMA_DIRECTION_LEFT:I = 0x2

.field public static final PANORAMA_DIRECTION_RIGHT:I = 0x1

.field public static final PANORAMA_DIRECTION_UP:I = 0x4

.field public static final PANORAMA_DIRECTION_UP_LEFT:I = 0x6

.field public static final PANORAMA_DIRECTION_UP_RIGHT:I = 0x5

.field private static final PANORAMA_SHOT_CANCEL:I = 0x45a

.field private static final PANORAMA_SHOT_CAPTURED:I = 0xf025

.field private static final PANORAMA_SHOT_CAPTURED_MAX_FRAMES:I = 0xf029

.field private static final PANORAMA_SHOT_CAPTURED_NEW:I = 0xf023

.field private static final PANORAMA_SHOT_DIR:I = 0xf026

.field private static final PANORAMA_SHOT_ERR:I = 0xf021

.field private static final PANORAMA_SHOT_FINALIZE:I = 0x459

.field private static final PANORAMA_SHOT_LIVE_PREVIEW_DATA:I = 0xf028

.field private static final PANORAMA_SHOT_LOW_RESOLUTION_DATA:I = 0xf027

.field private static final PANORAMA_SHOT_MOVE_SLOWLY:I = 0xf02a

.field private static final PANORAMA_SHOT_PROGRESS_STITCHING:I = 0xf024

.field private static final PANORAMA_SHOT_RECT_CENTER_POINT:I = 0xf022

.field private static final PANORAMA_SHOT_START:I = 0x457

.field private static final PANORAMA_SHOT_STOP:I = 0x458

.field private static final PANORAMA_SHOT_THUMBNAIL_SIZE:I = 0x456

.field private static final PREVIEW_CALLBACK_SIZE:I = 0x565

.field private static final PRO_SUGGEST_SEND_LAST_PREVIEW_FRAME:I = 0xf161

.field private static final RECORDING_TAKE_PICTURE:I = 0x4b1

.field private static final SAMSUNG_BURST_PANORAMA_SHOT_COMPRESSED_IMAGE:I = 0xf291

.field private static final SAMSUNG_SHOT_COMPRESSED_IMAGE:I = 0xf201

.field private static final SAMSUNG_SHOT_POSTVIEW_FRAME:I = 0xf202

.field private static final SAMSUNG_ULTRA_WIDE_SELFIE_SHOT_COMPRESSED_IMAGE:I = 0xf312

.field private static final SECIMAGING_CALLBACK_STRING:I = 0xf252

.field private static final SEC_IMAGE_EFFECT_SHOT_CREATING_RESULT_COMPLETED:I = 0xf243

.field private static final SEC_IMAGE_EFFECT_SHOT_CREATING_RESULT_PROGRESS:I = 0xf242

.field private static final SEC_IMAGE_EFFECT_SHOT_CREATING_RESULT_STARTED:I = 0xf241

.field private static final SELFIE_MODE_CHANGE:I = 0x541

.field private static final SELFIE_SHOT_HEADPOSE_DETECTION_SUCCESS:I = 0xf067

.field private static final SELFIE_SHOT_PALM_DETECTION_SUCCESS:I = 0xf068

.field private static final SELFIE_SHOT_SMILE_DETECTION_SUCCESS:I = 0xf066

.field private static final SELFIE_SHOT_WINK_DETECTION_SUCCESS:I = 0xf065

.field private static final SET_DISPLAY_ORIENTATION_MIRROR:I = 0x5e7

.field private static final SET_DUAL_MODE_SYNC:I = 0x55d

.field private static final SET_DUAL_TRACKING_COORDINATE:I = 0x55f

.field private static final SET_DUAL_TRACKING_MODE:I = 0x560

.field private static final SET_EFFECT_COORDINATE:I = 0x50d

.field private static final SET_EFFECT_EXTERNAL_MODE:I = 0x511

.field private static final SET_EFFECT_FILTER:I = 0x50b

.field private static final SET_EFFECT_LAYER_ORDER:I = 0x50e

.field private static final SET_EFFECT_MODE:I = 0x607

.field private static final SET_EFFECT_OPTION:I = 0x50c

.field private static final SET_EFFECT_ORIENTATION:I = 0x510

.field private static final SET_EFFECT_SAVE_AS_FLIPPED:I = 0x512

.field private static final SET_EFFECT_VISIBLE:I = 0x50f

.field private static final SET_EFFECT_VISIBLE_FOR_RECORDING:I = 0x548

.field private static final SET_ENABLE_SHUTTER_SOUND:I = 0x605

.field private static final SET_RECORDING_TAKE_PICTURE_CALLBACK:I = 0x4b2

.field private static final SET_SHUTTER_SOUND_VOLUME_LEVEL:I = 0x606

.field private static final SET_USE_PREVIEW_CALLBACK:I = 0x566

.field private static final SHOT_3DTOUR:I = 0x418

.field private static final SHOT_3D_PANORAMA:I = 0x3fc

.field private static final SHOT_3D_SINGLE:I = 0x3fb

.field private static final SHOT_ACTION:I = 0x3f2

.field private static final SHOT_ADDME:I = 0x3f1

.field private static final SHOT_AUTO:I = 0x406

.field private static final SHOT_BEAUTY:I = 0x3ef

.field private static final SHOT_BEST:I = 0x400

.field private static final SHOT_BESTGROUP:I = 0x401

.field private static final SHOT_BUDDY_PHOTOSHARING:I = 0x3fa

.field private static final SHOT_BURST:I = 0x3f9

.field private static final SHOT_CAPTURE_START:I = 0x55b

.field private static final SHOT_CAPTURE_STOP:I = 0x55c

.field private static final SHOT_CARTOON:I = 0x3f5

.field private static final SHOT_CONTINUOUS:I = 0x3e9

.field private static final SHOT_DRAMA:I = 0x407

.field private static final SHOT_DUAL:I = 0x417

.field private static final SHOT_DUAL_PIP:I = 0x414

.field private static final SHOT_FACESHOT:I = 0x3f8

.field private static final SHOT_FRAME:I = 0x3ed

.field private static final SHOT_GIFMAKER:I = 0x422

.field private static final SHOT_GOLF:I = 0x404

.field private static final SHOT_HAZE:I = 0x419

.field private static final SHOT_HDR:I = 0x3f6

.field private static final SHOT_MAGIC:I = 0x416

.field private static final SHOT_MAGICFRAME:I = 0x3fd

.field private static final SHOT_MOSAIC:I = 0x3ec

.field private static final SHOT_MULTI_FRAME:I = 0x3ff

.field private static final SHOT_OUTFOCUS:I = 0x415

.field private static final SHOT_PANORAMA:I = 0x3ea

.field private static final SHOT_PARTY:I = 0x3f4

.field private static final SHOT_SELF:I = 0x3ee

.field private static final SHOT_SELFIE:I = 0x41f

.field private static final SHOT_SELFIE_ALARM:I = 0x420

.field private static final SHOT_SHARESHOT:I = 0x3f7

.field private static final SHOT_SINGLE:I = 0x3e8

.field private static final SHOT_SMILE:I = 0x3eb

.field private static final SHOT_STOPMOTION:I = 0x3f3

.field private static final SHOT_THEME:I = 0x408

.field private static final SHOT_VINTAGE:I = 0x3f0

.field private static final SHUTTER_START:I = 0x45b

.field private static final SHUTTER_STOP:I = 0x45c

.field private static final SINGLE_SHOT_BRACKET_NEXT_SHOT_READY:I = 0xf273

.field private static final SINGLE_SHOT_ERROR:I = 0xf272

.field private static final SINGLE_SHOT_RAW_IMAGE_STRING:I = 0xf271

.field private static final SMARTSELFIE_BEAUTY_FACE:I = 0x572

.field private static final SMILE_SHOT_DETECTION_REINIT:I = 0x44f

.field private static final SMILE_SHOT_DETECTION_START:I = 0x44d

.field private static final SMILE_SHOT_DETECTION_STOP:I = 0x44e

.field private static final SMILE_SHOT_DETECTION_SUCCESS:I = 0xf061

.field private static final SMILE_SHOT_FACE_RECT:I = 0xf062

.field private static final SMILE_SHOT_SMILE_RECT:I = 0xf063

.field private static final START_COPY_LAST_PREVIEW_DATA:I = 0x501

.field private static final STOP_COPY_LAST_PREVIEW_DATA:I = 0x502

.field private static final SUPER_RESOLUTION_SHOT_SET:I = 0x4f1

.field private static final TAG:Ljava/lang/String; = "SecCamera-JNI-Java"

.field private static final THEME_SHOT_CAPTURE:I = 0x53f

.field private static final THEME_SHOT_INFO:I = 0x53e

.field private static final THEME_SHOT_MASK_SET:I = 0x53d

.field private static final ULTRA_WIDE_SELFIE_SHOT_BEAUTY_LEVEL:I = 0x5bc

.field private static final ULTRA_WIDE_SELFIE_SHOT_CANCEL:I = 0x5b7

.field private static final ULTRA_WIDE_SELFIE_SHOT_CAPTURED:I = 0xf305

.field private static final ULTRA_WIDE_SELFIE_SHOT_CAPTURED_MAX_FRAMES:I = 0xf309

.field private static final ULTRA_WIDE_SELFIE_SHOT_CAPTURED_NEW:I = 0xf303

.field private static final ULTRA_WIDE_SELFIE_SHOT_CAPTURE_ANGLE:I = 0x5bb

.field private static final ULTRA_WIDE_SELFIE_SHOT_DIR:I = 0xf306

.field private static final ULTRA_WIDE_SELFIE_SHOT_ERR:I = 0xf301

.field private static final ULTRA_WIDE_SELFIE_SHOT_LIVE_PREVIEW_DATA:I = 0xf308

.field private static final ULTRA_WIDE_SELFIE_SHOT_LOW_RESOLUTION_DATA:I = 0xf307

.field private static final ULTRA_WIDE_SELFIE_SHOT_MOVE_SLOWLY:I = 0xf30a

.field private static final ULTRA_WIDE_SELFIE_SHOT_NEXT_CAPTURE_POSITION:I = 0xf310

.field private static final ULTRA_WIDE_SELFIE_SHOT_PROGRESS_STITCHING:I = 0xf304

.field private static final ULTRA_WIDE_SELFIE_SHOT_RECT_CENTER_POINT:I = 0xf302

.field private static final ULTRA_WIDE_SELFIE_SHOT_SINGLE_CAPTURE_DONE:I = 0xf311

.field private static final ULTRA_WIDE_SELFIE_SHOT_START:I = 0x5b5

.field private static final ULTRA_WIDE_SELFIE_SHOT_STOP:I = 0x5b6


# instance fields
.field private mAutoExposureCallback:Lcom/sec/android/seccamera/SecCamera$AutoExposureCallback;

.field private mAutoFocusCallback:Lcom/sec/android/seccamera/SecCamera$AutoFocusCallback;

.field private final mAutoFocusCallbackLock:Ljava/lang/Object;

.field private mAutoFocusMoveCallback:Lcom/sec/android/seccamera/SecCamera$AutoFocusMoveCallback;

.field private mAutoLowLightDetectionListener:Lcom/sec/android/seccamera/SecCamera$AutoLowLightDetectionListener;

.field private mBeautyEventListener:Lcom/sec/android/seccamera/SecCamera$BeautyEventListener;

.field private mBurstEventListener:Lcom/sec/android/seccamera/SecCamera$BurstEventListener;

.field private mCameraCurrentSettingListener:Lcom/sec/android/seccamera/SecCamera$CameraCurrentSettingListener;

.field private mDramaShotEventListener:Lcom/sec/android/seccamera/SecCamera$DramaShotEventListener;

.field private mDualEventListener:Lcom/sec/android/seccamera/SecCamera$DualEventListener;

.field private mErrorCallback:Lcom/sec/android/seccamera/SecCamera$ErrorCallback;

.field private mEventHandler:Lcom/sec/android/seccamera/SecCamera$EventHandler;

.field private mFaceDetectionRunning:Z

.field private mFaceListener:Lcom/sec/android/seccamera/SecCamera$FaceDetectionListener;

.field private mGolfShotEventListener:Lcom/sec/android/seccamera/SecCamera$GolfShotEventListener;

.field private mHazeRemovalShotEventListener:Lcom/sec/android/seccamera/SecCamera$HazeRemovalEventListener;

.field private mHdrEventListener:Lcom/sec/android/seccamera/SecCamera$HdrEventListener;

.field private mJpegCallback:Lcom/sec/android/seccamera/SecCamera$PictureCallback;

.field private mLightConditionChangedListener:Lcom/sec/android/seccamera/SecCamera$LightConditionChangedListener;

.field private mMultiFrameEventListener:Lcom/sec/android/seccamera/SecCamera$MultiFrameEventListener;

.field private mNativeContext:J

.field private mOnAEResultListener:Lcom/sec/android/seccamera/SecCamera$OnAEResultListener;

.field private mOnActionShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnActionShotEventListener;

.field private mOnAutoLowLightDetectionListener:Lcom/sec/android/seccamera/SecCamera$OnAutoLowLightDetectionListener;

.field private mOnBeautyShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnBeautyShotEventListener;

.field private mOnBurstShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnBurstShotEventListener;

.field private mOnCameraCurrentSettingListener:Lcom/sec/android/seccamera/SecCamera$OnCameraCurrentSettingListener;

.field private mOnCartoonShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnCartoonShotEventListener;

.field private mOnContinuousShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnContinuousShotEventListener;

.field private mOnDramaShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnDramaShotEventListener;

.field private mOnDualEventListener:Lcom/sec/android/seccamera/SecCamera$OnDualEventListener;

.field private mOnGolfShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnGolfShotEventListener;

.field private mOnHDRShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnHDRShotEventListener;

.field private mOnHazeRemovalShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnHazeRemovalEventListener;

.field private mOnLightConditionChangedListener:Lcom/sec/android/seccamera/SecCamera$OnLightConditionChangedListener;

.field private mOnMagicShotModeEventListener:Lcom/sec/android/seccamera/SecCamera$OnMagicShotModeEventListener;

.field private mOnMultiFrameShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnMultiFrameShotEventListener;

.field private mOnNotifyFirstPreviewFrameEventListener:Lcom/sec/android/seccamera/SecCamera$OnNotifyFirstPreviewFrameEventListener;

.field private mOnObjectTrackingMsgListener:Lcom/sec/android/seccamera/SecCamera$OnObjectTrackingMsgListener;

.field private mOnOutFocusShotModeEventListener:Lcom/sec/android/seccamera/SecCamera$OnOutFocusShotModeEventListener;

.field private mOnPafResultListener:Lcom/sec/android/seccamera/SecCamera$OnPafResultListener;

.field private mOnPanoramaEventListener:Lcom/sec/android/seccamera/SecCamera$OnPanoramaEventListener;

.field private mOnSecImageEffectListner:Lcom/sec/android/seccamera/SecCamera$OnSecImageEffectListner;

.field private mOnSecImagingEventListener:Lcom/sec/android/seccamera/SecCamera$OnSecImagingEventListener;

.field private mOnSelfieShotDetectionSuccessListener:Lcom/sec/android/seccamera/SecCamera$OnSelfieShotDetectionSuccessListener;

.field private mOnSmileShotDetectionSuccessListener:Lcom/sec/android/seccamera/SecCamera$OnSmileShotDetectionSuccessListener;

.field private mOnUltraWideSelfieEventListener:Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;

.field private mOneShot:Z

.field private mPanoramaEventListener:Lcom/sec/android/seccamera/SecCamera$PanoramaEventListener;

.field private mPhaseAFCallback:Lcom/sec/android/seccamera/SecCamera$PhaseAFCallback;

.field private mPostviewCallback:Lcom/sec/android/seccamera/SecCamera$PictureCallback;

.field private mPreviewCallback:Lcom/sec/android/seccamera/SecCamera$PreviewCallback;

.field private mRawImageCallback:Lcom/sec/android/seccamera/SecCamera$PictureCallback;

.field private mSecImageEffectListner:Lcom/sec/android/seccamera/SecCamera$SecImageEffectListner;

.field private mSecImagingEventListener:Lcom/sec/android/seccamera/SecCamera$SecImagingEventListener;

.field private mSelectiveFocusEventListener:Lcom/sec/android/seccamera/SecCamera$SelectiveFocusEventListener;

.field private mSelfieDetectionListener:Lcom/sec/android/seccamera/SecCamera$SelfieDetectionListener;

.field private mShotAndMoreEventListener:Lcom/sec/android/seccamera/SecCamera$ShotAndMoreEventListener;

.field private mShutterCallback:Lcom/sec/android/seccamera/SecCamera$ShutterCallback;

.field private mUltraWideSelfieEventListener:Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;

.field private mUsingPreviewAllocation:Z

.field private mWithBuffer:Z

.field private mZoomListener:Lcom/sec/android/seccamera/SecCamera$OnZoomChangeListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 6681
    const-string v0, "seccamera_jni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 6682
    return-void
.end method

.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3345
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 210
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/seccamera/SecCamera;->mFaceDetectionRunning:Z

    .line 211
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mAutoFocusCallbackLock:Ljava/lang/Object;

    .line 970
    iput-object v1, p0, Lcom/sec/android/seccamera/SecCamera;->mPanoramaEventListener:Lcom/sec/android/seccamera/SecCamera$PanoramaEventListener;

    .line 1041
    iput-object v1, p0, Lcom/sec/android/seccamera/SecCamera;->mUltraWideSelfieEventListener:Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;

    .line 1091
    iput-object v1, p0, Lcom/sec/android/seccamera/SecCamera;->mBurstEventListener:Lcom/sec/android/seccamera/SecCamera$BurstEventListener;

    .line 1127
    iput-object v1, p0, Lcom/sec/android/seccamera/SecCamera;->mSelfieDetectionListener:Lcom/sec/android/seccamera/SecCamera$SelfieDetectionListener;

    .line 1183
    iput-object v1, p0, Lcom/sec/android/seccamera/SecCamera;->mHdrEventListener:Lcom/sec/android/seccamera/SecCamera$HdrEventListener;

    .line 1216
    iput-object v1, p0, Lcom/sec/android/seccamera/SecCamera;->mMultiFrameEventListener:Lcom/sec/android/seccamera/SecCamera$MultiFrameEventListener;

    .line 1251
    iput-object v1, p0, Lcom/sec/android/seccamera/SecCamera;->mHazeRemovalShotEventListener:Lcom/sec/android/seccamera/SecCamera$HazeRemovalEventListener;

    .line 1295
    iput-object v1, p0, Lcom/sec/android/seccamera/SecCamera;->mGolfShotEventListener:Lcom/sec/android/seccamera/SecCamera$GolfShotEventListener;

    .line 1351
    iput-object v1, p0, Lcom/sec/android/seccamera/SecCamera;->mDramaShotEventListener:Lcom/sec/android/seccamera/SecCamera$DramaShotEventListener;

    .line 1384
    iput-object v1, p0, Lcom/sec/android/seccamera/SecCamera;->mBeautyEventListener:Lcom/sec/android/seccamera/SecCamera$BeautyEventListener;

    .line 1426
    iput-object v1, p0, Lcom/sec/android/seccamera/SecCamera;->mSecImageEffectListner:Lcom/sec/android/seccamera/SecCamera$SecImageEffectListner;

    .line 1450
    iput-object v1, p0, Lcom/sec/android/seccamera/SecCamera;->mAutoLowLightDetectionListener:Lcom/sec/android/seccamera/SecCamera$AutoLowLightDetectionListener;

    .line 1470
    iput-object v1, p0, Lcom/sec/android/seccamera/SecCamera;->mLightConditionChangedListener:Lcom/sec/android/seccamera/SecCamera$LightConditionChangedListener;

    .line 1503
    iput-object v1, p0, Lcom/sec/android/seccamera/SecCamera;->mDualEventListener:Lcom/sec/android/seccamera/SecCamera$DualEventListener;

    .line 1533
    iput-object v1, p0, Lcom/sec/android/seccamera/SecCamera;->mSecImagingEventListener:Lcom/sec/android/seccamera/SecCamera$SecImagingEventListener;

    .line 1581
    iput-object v1, p0, Lcom/sec/android/seccamera/SecCamera;->mSelectiveFocusEventListener:Lcom/sec/android/seccamera/SecCamera$SelectiveFocusEventListener;

    .line 1635
    iput-object v1, p0, Lcom/sec/android/seccamera/SecCamera;->mShotAndMoreEventListener:Lcom/sec/android/seccamera/SecCamera$ShotAndMoreEventListener;

    .line 1666
    iput-object v1, p0, Lcom/sec/android/seccamera/SecCamera;->mPhaseAFCallback:Lcom/sec/android/seccamera/SecCamera$PhaseAFCallback;

    .line 1695
    iput-object v1, p0, Lcom/sec/android/seccamera/SecCamera;->mCameraCurrentSettingListener:Lcom/sec/android/seccamera/SecCamera$CameraCurrentSettingListener;

    .line 1715
    iput-object v1, p0, Lcom/sec/android/seccamera/SecCamera;->mAutoExposureCallback:Lcom/sec/android/seccamera/SecCamera$AutoExposureCallback;

    .line 1832
    iput-object v1, p0, Lcom/sec/android/seccamera/SecCamera;->mOnPanoramaEventListener:Lcom/sec/android/seccamera/SecCamera$OnPanoramaEventListener;

    .line 1944
    iput-object v1, p0, Lcom/sec/android/seccamera/SecCamera;->mOnUltraWideSelfieEventListener:Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;

    .line 1997
    iput-object v1, p0, Lcom/sec/android/seccamera/SecCamera;->mOnContinuousShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnContinuousShotEventListener;

    .line 2064
    iput-object v1, p0, Lcom/sec/android/seccamera/SecCamera;->mOnBurstShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnBurstShotEventListener;

    .line 2125
    iput-object v1, p0, Lcom/sec/android/seccamera/SecCamera;->mOnActionShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnActionShotEventListener;

    .line 2152
    iput-object v1, p0, Lcom/sec/android/seccamera/SecCamera;->mOnCartoonShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnCartoonShotEventListener;

    .line 2185
    iput-object v1, p0, Lcom/sec/android/seccamera/SecCamera;->mOnObjectTrackingMsgListener:Lcom/sec/android/seccamera/SecCamera$OnObjectTrackingMsgListener;

    .line 2235
    iput-object v1, p0, Lcom/sec/android/seccamera/SecCamera;->mOnSmileShotDetectionSuccessListener:Lcom/sec/android/seccamera/SecCamera$OnSmileShotDetectionSuccessListener;

    .line 2285
    iput-object v1, p0, Lcom/sec/android/seccamera/SecCamera;->mOnSelfieShotDetectionSuccessListener:Lcom/sec/android/seccamera/SecCamera$OnSelfieShotDetectionSuccessListener;

    .line 2356
    iput-object v1, p0, Lcom/sec/android/seccamera/SecCamera;->mOnHDRShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnHDRShotEventListener;

    .line 2396
    iput-object v1, p0, Lcom/sec/android/seccamera/SecCamera;->mOnMultiFrameShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnMultiFrameShotEventListener;

    .line 2436
    iput-object v1, p0, Lcom/sec/android/seccamera/SecCamera;->mOnHazeRemovalShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnHazeRemovalEventListener;

    .line 2468
    iput-object v1, p0, Lcom/sec/android/seccamera/SecCamera;->mOnNotifyFirstPreviewFrameEventListener:Lcom/sec/android/seccamera/SecCamera$OnNotifyFirstPreviewFrameEventListener;

    .line 2528
    iput-object v1, p0, Lcom/sec/android/seccamera/SecCamera;->mOnGolfShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnGolfShotEventListener;

    .line 2599
    iput-object v1, p0, Lcom/sec/android/seccamera/SecCamera;->mOnDramaShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnDramaShotEventListener;

    .line 2637
    iput-object v1, p0, Lcom/sec/android/seccamera/SecCamera;->mOnBeautyShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnBeautyShotEventListener;

    .line 2690
    iput-object v1, p0, Lcom/sec/android/seccamera/SecCamera;->mOnSecImageEffectListner:Lcom/sec/android/seccamera/SecCamera$OnSecImageEffectListner;

    .line 2721
    iput-object v1, p0, Lcom/sec/android/seccamera/SecCamera;->mOnAutoLowLightDetectionListener:Lcom/sec/android/seccamera/SecCamera$OnAutoLowLightDetectionListener;

    .line 2748
    iput-object v1, p0, Lcom/sec/android/seccamera/SecCamera;->mOnLightConditionChangedListener:Lcom/sec/android/seccamera/SecCamera$OnLightConditionChangedListener;

    .line 2790
    iput-object v1, p0, Lcom/sec/android/seccamera/SecCamera;->mOnDualEventListener:Lcom/sec/android/seccamera/SecCamera$OnDualEventListener;

    .line 2827
    iput-object v1, p0, Lcom/sec/android/seccamera/SecCamera;->mOnSecImagingEventListener:Lcom/sec/android/seccamera/SecCamera$OnSecImagingEventListener;

    .line 2886
    iput-object v1, p0, Lcom/sec/android/seccamera/SecCamera;->mOnOutFocusShotModeEventListener:Lcom/sec/android/seccamera/SecCamera$OnOutFocusShotModeEventListener;

    .line 2953
    iput-object v1, p0, Lcom/sec/android/seccamera/SecCamera;->mOnMagicShotModeEventListener:Lcom/sec/android/seccamera/SecCamera$OnMagicShotModeEventListener;

    .line 2993
    iput-object v1, p0, Lcom/sec/android/seccamera/SecCamera;->mOnPafResultListener:Lcom/sec/android/seccamera/SecCamera$OnPafResultListener;

    .line 3024
    iput-object v1, p0, Lcom/sec/android/seccamera/SecCamera;->mOnCameraCurrentSettingListener:Lcom/sec/android/seccamera/SecCamera$OnCameraCurrentSettingListener;

    .line 3051
    iput-object v1, p0, Lcom/sec/android/seccamera/SecCamera;->mOnAEResultListener:Lcom/sec/android/seccamera/SecCamera$OnAEResultListener;

    .line 3346
    return-void
.end method

.method private constructor <init>(II)V
    .locals 7
    .param p1, "cameraId"    # I
    .param p2, "halVersion"    # I

    .prologue
    const/4 v4, 0x0

    .line 3223
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 210
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/seccamera/SecCamera;->mFaceDetectionRunning:Z

    .line 211
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mAutoFocusCallbackLock:Ljava/lang/Object;

    .line 970
    iput-object v4, p0, Lcom/sec/android/seccamera/SecCamera;->mPanoramaEventListener:Lcom/sec/android/seccamera/SecCamera$PanoramaEventListener;

    .line 1041
    iput-object v4, p0, Lcom/sec/android/seccamera/SecCamera;->mUltraWideSelfieEventListener:Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;

    .line 1091
    iput-object v4, p0, Lcom/sec/android/seccamera/SecCamera;->mBurstEventListener:Lcom/sec/android/seccamera/SecCamera$BurstEventListener;

    .line 1127
    iput-object v4, p0, Lcom/sec/android/seccamera/SecCamera;->mSelfieDetectionListener:Lcom/sec/android/seccamera/SecCamera$SelfieDetectionListener;

    .line 1183
    iput-object v4, p0, Lcom/sec/android/seccamera/SecCamera;->mHdrEventListener:Lcom/sec/android/seccamera/SecCamera$HdrEventListener;

    .line 1216
    iput-object v4, p0, Lcom/sec/android/seccamera/SecCamera;->mMultiFrameEventListener:Lcom/sec/android/seccamera/SecCamera$MultiFrameEventListener;

    .line 1251
    iput-object v4, p0, Lcom/sec/android/seccamera/SecCamera;->mHazeRemovalShotEventListener:Lcom/sec/android/seccamera/SecCamera$HazeRemovalEventListener;

    .line 1295
    iput-object v4, p0, Lcom/sec/android/seccamera/SecCamera;->mGolfShotEventListener:Lcom/sec/android/seccamera/SecCamera$GolfShotEventListener;

    .line 1351
    iput-object v4, p0, Lcom/sec/android/seccamera/SecCamera;->mDramaShotEventListener:Lcom/sec/android/seccamera/SecCamera$DramaShotEventListener;

    .line 1384
    iput-object v4, p0, Lcom/sec/android/seccamera/SecCamera;->mBeautyEventListener:Lcom/sec/android/seccamera/SecCamera$BeautyEventListener;

    .line 1426
    iput-object v4, p0, Lcom/sec/android/seccamera/SecCamera;->mSecImageEffectListner:Lcom/sec/android/seccamera/SecCamera$SecImageEffectListner;

    .line 1450
    iput-object v4, p0, Lcom/sec/android/seccamera/SecCamera;->mAutoLowLightDetectionListener:Lcom/sec/android/seccamera/SecCamera$AutoLowLightDetectionListener;

    .line 1470
    iput-object v4, p0, Lcom/sec/android/seccamera/SecCamera;->mLightConditionChangedListener:Lcom/sec/android/seccamera/SecCamera$LightConditionChangedListener;

    .line 1503
    iput-object v4, p0, Lcom/sec/android/seccamera/SecCamera;->mDualEventListener:Lcom/sec/android/seccamera/SecCamera$DualEventListener;

    .line 1533
    iput-object v4, p0, Lcom/sec/android/seccamera/SecCamera;->mSecImagingEventListener:Lcom/sec/android/seccamera/SecCamera$SecImagingEventListener;

    .line 1581
    iput-object v4, p0, Lcom/sec/android/seccamera/SecCamera;->mSelectiveFocusEventListener:Lcom/sec/android/seccamera/SecCamera$SelectiveFocusEventListener;

    .line 1635
    iput-object v4, p0, Lcom/sec/android/seccamera/SecCamera;->mShotAndMoreEventListener:Lcom/sec/android/seccamera/SecCamera$ShotAndMoreEventListener;

    .line 1666
    iput-object v4, p0, Lcom/sec/android/seccamera/SecCamera;->mPhaseAFCallback:Lcom/sec/android/seccamera/SecCamera$PhaseAFCallback;

    .line 1695
    iput-object v4, p0, Lcom/sec/android/seccamera/SecCamera;->mCameraCurrentSettingListener:Lcom/sec/android/seccamera/SecCamera$CameraCurrentSettingListener;

    .line 1715
    iput-object v4, p0, Lcom/sec/android/seccamera/SecCamera;->mAutoExposureCallback:Lcom/sec/android/seccamera/SecCamera$AutoExposureCallback;

    .line 1832
    iput-object v4, p0, Lcom/sec/android/seccamera/SecCamera;->mOnPanoramaEventListener:Lcom/sec/android/seccamera/SecCamera$OnPanoramaEventListener;

    .line 1944
    iput-object v4, p0, Lcom/sec/android/seccamera/SecCamera;->mOnUltraWideSelfieEventListener:Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;

    .line 1997
    iput-object v4, p0, Lcom/sec/android/seccamera/SecCamera;->mOnContinuousShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnContinuousShotEventListener;

    .line 2064
    iput-object v4, p0, Lcom/sec/android/seccamera/SecCamera;->mOnBurstShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnBurstShotEventListener;

    .line 2125
    iput-object v4, p0, Lcom/sec/android/seccamera/SecCamera;->mOnActionShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnActionShotEventListener;

    .line 2152
    iput-object v4, p0, Lcom/sec/android/seccamera/SecCamera;->mOnCartoonShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnCartoonShotEventListener;

    .line 2185
    iput-object v4, p0, Lcom/sec/android/seccamera/SecCamera;->mOnObjectTrackingMsgListener:Lcom/sec/android/seccamera/SecCamera$OnObjectTrackingMsgListener;

    .line 2235
    iput-object v4, p0, Lcom/sec/android/seccamera/SecCamera;->mOnSmileShotDetectionSuccessListener:Lcom/sec/android/seccamera/SecCamera$OnSmileShotDetectionSuccessListener;

    .line 2285
    iput-object v4, p0, Lcom/sec/android/seccamera/SecCamera;->mOnSelfieShotDetectionSuccessListener:Lcom/sec/android/seccamera/SecCamera$OnSelfieShotDetectionSuccessListener;

    .line 2356
    iput-object v4, p0, Lcom/sec/android/seccamera/SecCamera;->mOnHDRShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnHDRShotEventListener;

    .line 2396
    iput-object v4, p0, Lcom/sec/android/seccamera/SecCamera;->mOnMultiFrameShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnMultiFrameShotEventListener;

    .line 2436
    iput-object v4, p0, Lcom/sec/android/seccamera/SecCamera;->mOnHazeRemovalShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnHazeRemovalEventListener;

    .line 2468
    iput-object v4, p0, Lcom/sec/android/seccamera/SecCamera;->mOnNotifyFirstPreviewFrameEventListener:Lcom/sec/android/seccamera/SecCamera$OnNotifyFirstPreviewFrameEventListener;

    .line 2528
    iput-object v4, p0, Lcom/sec/android/seccamera/SecCamera;->mOnGolfShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnGolfShotEventListener;

    .line 2599
    iput-object v4, p0, Lcom/sec/android/seccamera/SecCamera;->mOnDramaShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnDramaShotEventListener;

    .line 2637
    iput-object v4, p0, Lcom/sec/android/seccamera/SecCamera;->mOnBeautyShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnBeautyShotEventListener;

    .line 2690
    iput-object v4, p0, Lcom/sec/android/seccamera/SecCamera;->mOnSecImageEffectListner:Lcom/sec/android/seccamera/SecCamera$OnSecImageEffectListner;

    .line 2721
    iput-object v4, p0, Lcom/sec/android/seccamera/SecCamera;->mOnAutoLowLightDetectionListener:Lcom/sec/android/seccamera/SecCamera$OnAutoLowLightDetectionListener;

    .line 2748
    iput-object v4, p0, Lcom/sec/android/seccamera/SecCamera;->mOnLightConditionChangedListener:Lcom/sec/android/seccamera/SecCamera$OnLightConditionChangedListener;

    .line 2790
    iput-object v4, p0, Lcom/sec/android/seccamera/SecCamera;->mOnDualEventListener:Lcom/sec/android/seccamera/SecCamera$OnDualEventListener;

    .line 2827
    iput-object v4, p0, Lcom/sec/android/seccamera/SecCamera;->mOnSecImagingEventListener:Lcom/sec/android/seccamera/SecCamera$OnSecImagingEventListener;

    .line 2886
    iput-object v4, p0, Lcom/sec/android/seccamera/SecCamera;->mOnOutFocusShotModeEventListener:Lcom/sec/android/seccamera/SecCamera$OnOutFocusShotModeEventListener;

    .line 2953
    iput-object v4, p0, Lcom/sec/android/seccamera/SecCamera;->mOnMagicShotModeEventListener:Lcom/sec/android/seccamera/SecCamera$OnMagicShotModeEventListener;

    .line 2993
    iput-object v4, p0, Lcom/sec/android/seccamera/SecCamera;->mOnPafResultListener:Lcom/sec/android/seccamera/SecCamera$OnPafResultListener;

    .line 3024
    iput-object v4, p0, Lcom/sec/android/seccamera/SecCamera;->mOnCameraCurrentSettingListener:Lcom/sec/android/seccamera/SecCamera$OnCameraCurrentSettingListener;

    .line 3051
    iput-object v4, p0, Lcom/sec/android/seccamera/SecCamera;->mOnAEResultListener:Lcom/sec/android/seccamera/SecCamera$OnAEResultListener;

    .line 3224
    const/16 v3, 0x64

    const/4 v5, 0x1

    move-object v0, p0

    move v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/seccamera/SecCamera;->cameraInitVersion(IIILandroid/os/Looper;Z)I

    move-result v6

    .line 3225
    .local v6, "err":I
    invoke-static {v6}, Lcom/sec/android/seccamera/SecCamera;->checkInitErrors(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3226
    sparse-switch v6, :sswitch_data_0

    .line 3248
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unknown camera error"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3228
    :sswitch_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Fail to connect to camera service"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3230
    :sswitch_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Camera initialization failed"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3232
    :sswitch_2
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Camera initialization failed because some methods are not implemented"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3235
    :sswitch_3
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Camera initialization failed because the hal version is not supported by this device"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3238
    :sswitch_4
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Camera initialization failed because the input arugments are invalid"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3241
    :sswitch_5
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Camera initialization failed because the camera device was already opened"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3244
    :sswitch_6
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Camera initialization failed because the max number of camera devices were already opened"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3251
    :cond_0
    return-void

    .line 3226
    :sswitch_data_0
    .sparse-switch
        -0x5f -> :sswitch_3
        -0x57 -> :sswitch_6
        -0x26 -> :sswitch_2
        -0x16 -> :sswitch_4
        -0x13 -> :sswitch_1
        -0x10 -> :sswitch_5
        -0xd -> :sswitch_0
    .end sparse-switch
.end method

.method constructor <init>(IILandroid/os/Looper;Z)V
    .locals 3
    .param p1, "cameraId"    # I
    .param p2, "priority"    # I
    .param p3, "_looper"    # Landroid/os/Looper;
    .param p4, "halsettting"    # Z

    .prologue
    const/4 v2, 0x0

    .line 3312
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 210
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/seccamera/SecCamera;->mFaceDetectionRunning:Z

    .line 211
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/sec/android/seccamera/SecCamera;->mAutoFocusCallbackLock:Ljava/lang/Object;

    .line 970
    iput-object v2, p0, Lcom/sec/android/seccamera/SecCamera;->mPanoramaEventListener:Lcom/sec/android/seccamera/SecCamera$PanoramaEventListener;

    .line 1041
    iput-object v2, p0, Lcom/sec/android/seccamera/SecCamera;->mUltraWideSelfieEventListener:Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;

    .line 1091
    iput-object v2, p0, Lcom/sec/android/seccamera/SecCamera;->mBurstEventListener:Lcom/sec/android/seccamera/SecCamera$BurstEventListener;

    .line 1127
    iput-object v2, p0, Lcom/sec/android/seccamera/SecCamera;->mSelfieDetectionListener:Lcom/sec/android/seccamera/SecCamera$SelfieDetectionListener;

    .line 1183
    iput-object v2, p0, Lcom/sec/android/seccamera/SecCamera;->mHdrEventListener:Lcom/sec/android/seccamera/SecCamera$HdrEventListener;

    .line 1216
    iput-object v2, p0, Lcom/sec/android/seccamera/SecCamera;->mMultiFrameEventListener:Lcom/sec/android/seccamera/SecCamera$MultiFrameEventListener;

    .line 1251
    iput-object v2, p0, Lcom/sec/android/seccamera/SecCamera;->mHazeRemovalShotEventListener:Lcom/sec/android/seccamera/SecCamera$HazeRemovalEventListener;

    .line 1295
    iput-object v2, p0, Lcom/sec/android/seccamera/SecCamera;->mGolfShotEventListener:Lcom/sec/android/seccamera/SecCamera$GolfShotEventListener;

    .line 1351
    iput-object v2, p0, Lcom/sec/android/seccamera/SecCamera;->mDramaShotEventListener:Lcom/sec/android/seccamera/SecCamera$DramaShotEventListener;

    .line 1384
    iput-object v2, p0, Lcom/sec/android/seccamera/SecCamera;->mBeautyEventListener:Lcom/sec/android/seccamera/SecCamera$BeautyEventListener;

    .line 1426
    iput-object v2, p0, Lcom/sec/android/seccamera/SecCamera;->mSecImageEffectListner:Lcom/sec/android/seccamera/SecCamera$SecImageEffectListner;

    .line 1450
    iput-object v2, p0, Lcom/sec/android/seccamera/SecCamera;->mAutoLowLightDetectionListener:Lcom/sec/android/seccamera/SecCamera$AutoLowLightDetectionListener;

    .line 1470
    iput-object v2, p0, Lcom/sec/android/seccamera/SecCamera;->mLightConditionChangedListener:Lcom/sec/android/seccamera/SecCamera$LightConditionChangedListener;

    .line 1503
    iput-object v2, p0, Lcom/sec/android/seccamera/SecCamera;->mDualEventListener:Lcom/sec/android/seccamera/SecCamera$DualEventListener;

    .line 1533
    iput-object v2, p0, Lcom/sec/android/seccamera/SecCamera;->mSecImagingEventListener:Lcom/sec/android/seccamera/SecCamera$SecImagingEventListener;

    .line 1581
    iput-object v2, p0, Lcom/sec/android/seccamera/SecCamera;->mSelectiveFocusEventListener:Lcom/sec/android/seccamera/SecCamera$SelectiveFocusEventListener;

    .line 1635
    iput-object v2, p0, Lcom/sec/android/seccamera/SecCamera;->mShotAndMoreEventListener:Lcom/sec/android/seccamera/SecCamera$ShotAndMoreEventListener;

    .line 1666
    iput-object v2, p0, Lcom/sec/android/seccamera/SecCamera;->mPhaseAFCallback:Lcom/sec/android/seccamera/SecCamera$PhaseAFCallback;

    .line 1695
    iput-object v2, p0, Lcom/sec/android/seccamera/SecCamera;->mCameraCurrentSettingListener:Lcom/sec/android/seccamera/SecCamera$CameraCurrentSettingListener;

    .line 1715
    iput-object v2, p0, Lcom/sec/android/seccamera/SecCamera;->mAutoExposureCallback:Lcom/sec/android/seccamera/SecCamera$AutoExposureCallback;

    .line 1832
    iput-object v2, p0, Lcom/sec/android/seccamera/SecCamera;->mOnPanoramaEventListener:Lcom/sec/android/seccamera/SecCamera$OnPanoramaEventListener;

    .line 1944
    iput-object v2, p0, Lcom/sec/android/seccamera/SecCamera;->mOnUltraWideSelfieEventListener:Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;

    .line 1997
    iput-object v2, p0, Lcom/sec/android/seccamera/SecCamera;->mOnContinuousShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnContinuousShotEventListener;

    .line 2064
    iput-object v2, p0, Lcom/sec/android/seccamera/SecCamera;->mOnBurstShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnBurstShotEventListener;

    .line 2125
    iput-object v2, p0, Lcom/sec/android/seccamera/SecCamera;->mOnActionShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnActionShotEventListener;

    .line 2152
    iput-object v2, p0, Lcom/sec/android/seccamera/SecCamera;->mOnCartoonShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnCartoonShotEventListener;

    .line 2185
    iput-object v2, p0, Lcom/sec/android/seccamera/SecCamera;->mOnObjectTrackingMsgListener:Lcom/sec/android/seccamera/SecCamera$OnObjectTrackingMsgListener;

    .line 2235
    iput-object v2, p0, Lcom/sec/android/seccamera/SecCamera;->mOnSmileShotDetectionSuccessListener:Lcom/sec/android/seccamera/SecCamera$OnSmileShotDetectionSuccessListener;

    .line 2285
    iput-object v2, p0, Lcom/sec/android/seccamera/SecCamera;->mOnSelfieShotDetectionSuccessListener:Lcom/sec/android/seccamera/SecCamera$OnSelfieShotDetectionSuccessListener;

    .line 2356
    iput-object v2, p0, Lcom/sec/android/seccamera/SecCamera;->mOnHDRShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnHDRShotEventListener;

    .line 2396
    iput-object v2, p0, Lcom/sec/android/seccamera/SecCamera;->mOnMultiFrameShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnMultiFrameShotEventListener;

    .line 2436
    iput-object v2, p0, Lcom/sec/android/seccamera/SecCamera;->mOnHazeRemovalShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnHazeRemovalEventListener;

    .line 2468
    iput-object v2, p0, Lcom/sec/android/seccamera/SecCamera;->mOnNotifyFirstPreviewFrameEventListener:Lcom/sec/android/seccamera/SecCamera$OnNotifyFirstPreviewFrameEventListener;

    .line 2528
    iput-object v2, p0, Lcom/sec/android/seccamera/SecCamera;->mOnGolfShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnGolfShotEventListener;

    .line 2599
    iput-object v2, p0, Lcom/sec/android/seccamera/SecCamera;->mOnDramaShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnDramaShotEventListener;

    .line 2637
    iput-object v2, p0, Lcom/sec/android/seccamera/SecCamera;->mOnBeautyShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnBeautyShotEventListener;

    .line 2690
    iput-object v2, p0, Lcom/sec/android/seccamera/SecCamera;->mOnSecImageEffectListner:Lcom/sec/android/seccamera/SecCamera$OnSecImageEffectListner;

    .line 2721
    iput-object v2, p0, Lcom/sec/android/seccamera/SecCamera;->mOnAutoLowLightDetectionListener:Lcom/sec/android/seccamera/SecCamera$OnAutoLowLightDetectionListener;

    .line 2748
    iput-object v2, p0, Lcom/sec/android/seccamera/SecCamera;->mOnLightConditionChangedListener:Lcom/sec/android/seccamera/SecCamera$OnLightConditionChangedListener;

    .line 2790
    iput-object v2, p0, Lcom/sec/android/seccamera/SecCamera;->mOnDualEventListener:Lcom/sec/android/seccamera/SecCamera$OnDualEventListener;

    .line 2827
    iput-object v2, p0, Lcom/sec/android/seccamera/SecCamera;->mOnSecImagingEventListener:Lcom/sec/android/seccamera/SecCamera$OnSecImagingEventListener;

    .line 2886
    iput-object v2, p0, Lcom/sec/android/seccamera/SecCamera;->mOnOutFocusShotModeEventListener:Lcom/sec/android/seccamera/SecCamera$OnOutFocusShotModeEventListener;

    .line 2953
    iput-object v2, p0, Lcom/sec/android/seccamera/SecCamera;->mOnMagicShotModeEventListener:Lcom/sec/android/seccamera/SecCamera$OnMagicShotModeEventListener;

    .line 2993
    iput-object v2, p0, Lcom/sec/android/seccamera/SecCamera;->mOnPafResultListener:Lcom/sec/android/seccamera/SecCamera$OnPafResultListener;

    .line 3024
    iput-object v2, p0, Lcom/sec/android/seccamera/SecCamera;->mOnCameraCurrentSettingListener:Lcom/sec/android/seccamera/SecCamera$OnCameraCurrentSettingListener;

    .line 3051
    iput-object v2, p0, Lcom/sec/android/seccamera/SecCamera;->mOnAEResultListener:Lcom/sec/android/seccamera/SecCamera$OnAEResultListener;

    .line 3313
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/seccamera/SecCamera;->cameraInitNormal(IILandroid/os/Looper;Z)I

    move-result v0

    .line 3314
    .local v0, "err":I
    invoke-static {v0}, Lcom/sec/android/seccamera/SecCamera;->checkInitErrors(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3315
    sparse-switch v0, :sswitch_data_0

    .line 3322
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Unknown camera error"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 3317
    :sswitch_0
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Fail to connect to camera service"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 3319
    :sswitch_1
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Camera initialization failed"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 3325
    :cond_0
    return-void

    .line 3315
    :sswitch_data_0
    .sparse-switch
        -0x13 -> :sswitch_1
        -0xd -> :sswitch_0
    .end sparse-switch
.end method

.method private final native _addCallbackBuffer([BI)V
.end method

.method private final native _enableShutterSound(Z)Z
.end method

.method private static native _getCameraInfo(ILcom/sec/android/seccamera/SecCamera$CameraInfo;)V
.end method

.method private native _setOutputFile(Ljava/io/FileDescriptor;)V
.end method

.method private native _setOutputFileArray([Ljava/io/FileDescriptor;I)V
.end method

.method private final native _startFaceDetection(I)V
.end method

.method private final native _stopFaceDetection()V
.end method

.method private final native _stopPreview()V
.end method

.method static synthetic access$000(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$ShutterCallback;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mShutterCallback:Lcom/sec/android/seccamera/SecCamera$ShutterCallback;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$PictureCallback;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mRawImageCallback:Lcom/sec/android/seccamera/SecCamera$PictureCallback;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnZoomChangeListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mZoomListener:Lcom/sec/android/seccamera/SecCamera$OnZoomChangeListener;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$FaceDetectionListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mFaceListener:Lcom/sec/android/seccamera/SecCamera$FaceDetectionListener;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/seccamera/SecCamera;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    .line 166
    iget-boolean v0, p0, Lcom/sec/android/seccamera/SecCamera;->mFaceDetectionRunning:Z

    return v0
.end method

.method static synthetic access$1202(Lcom/sec/android/seccamera/SecCamera;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/seccamera/SecCamera;
    .param p1, "x1"    # Z

    .prologue
    .line 166
    iput-boolean p1, p0, Lcom/sec/android/seccamera/SecCamera;->mFaceDetectionRunning:Z

    return p1
.end method

.method static synthetic access$1300(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$ErrorCallback;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mErrorCallback:Lcom/sec/android/seccamera/SecCamera$ErrorCallback;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/android/seccamera/SecCamera;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    .line 166
    invoke-direct {p0}, Lcom/sec/android/seccamera/SecCamera;->native_release()V

    return-void
.end method

.method static synthetic access$1500(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$AutoFocusMoveCallback;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mAutoFocusMoveCallback:Lcom/sec/android/seccamera/SecCamera$AutoFocusMoveCallback;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnPanoramaEventListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mOnPanoramaEventListener:Lcom/sec/android/seccamera/SecCamera$OnPanoramaEventListener;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$PanoramaEventListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mPanoramaEventListener:Lcom/sec/android/seccamera/SecCamera$PanoramaEventListener;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnContinuousShotEventListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mOnContinuousShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnContinuousShotEventListener;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnActionShotEventListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mOnActionShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnActionShotEventListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$PictureCallback;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mJpegCallback:Lcom/sec/android/seccamera/SecCamera$PictureCallback;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnCartoonShotEventListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mOnCartoonShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnCartoonShotEventListener;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnSmileShotDetectionSuccessListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mOnSmileShotDetectionSuccessListener:Lcom/sec/android/seccamera/SecCamera$OnSmileShotDetectionSuccessListener;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnSelfieShotDetectionSuccessListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mOnSelfieShotDetectionSuccessListener:Lcom/sec/android/seccamera/SecCamera$OnSelfieShotDetectionSuccessListener;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$SelfieDetectionListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mSelfieDetectionListener:Lcom/sec/android/seccamera/SecCamera$SelfieDetectionListener;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnHDRShotEventListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mOnHDRShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnHDRShotEventListener;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$HdrEventListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mHdrEventListener:Lcom/sec/android/seccamera/SecCamera$HdrEventListener;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnObjectTrackingMsgListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mOnObjectTrackingMsgListener:Lcom/sec/android/seccamera/SecCamera$OnObjectTrackingMsgListener;

    return-object v0
.end method

.method static synthetic access$2700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnBurstShotEventListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mOnBurstShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnBurstShotEventListener;

    return-object v0
.end method

.method static synthetic access$2800(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$BurstEventListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mBurstEventListener:Lcom/sec/android/seccamera/SecCamera$BurstEventListener;

    return-object v0
.end method

.method static synthetic access$2900(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnMultiFrameShotEventListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mOnMultiFrameShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnMultiFrameShotEventListener;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$PreviewCallback;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mPreviewCallback:Lcom/sec/android/seccamera/SecCamera$PreviewCallback;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$MultiFrameEventListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mMultiFrameEventListener:Lcom/sec/android/seccamera/SecCamera$MultiFrameEventListener;

    return-object v0
.end method

.method static synthetic access$302(Lcom/sec/android/seccamera/SecCamera;Lcom/sec/android/seccamera/SecCamera$PreviewCallback;)Lcom/sec/android/seccamera/SecCamera$PreviewCallback;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/seccamera/SecCamera;
    .param p1, "x1"    # Lcom/sec/android/seccamera/SecCamera$PreviewCallback;

    .prologue
    .line 166
    iput-object p1, p0, Lcom/sec/android/seccamera/SecCamera;->mPreviewCallback:Lcom/sec/android/seccamera/SecCamera$PreviewCallback;

    return-object p1
.end method

.method static synthetic access$3100(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnNotifyFirstPreviewFrameEventListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mOnNotifyFirstPreviewFrameEventListener:Lcom/sec/android/seccamera/SecCamera$OnNotifyFirstPreviewFrameEventListener;

    return-object v0
.end method

.method static synthetic access$3200(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnGolfShotEventListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mOnGolfShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnGolfShotEventListener;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$GolfShotEventListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mGolfShotEventListener:Lcom/sec/android/seccamera/SecCamera$GolfShotEventListener;

    return-object v0
.end method

.method static synthetic access$3400(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnDramaShotEventListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mOnDramaShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnDramaShotEventListener;

    return-object v0
.end method

.method static synthetic access$3500(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$DramaShotEventListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mDramaShotEventListener:Lcom/sec/android/seccamera/SecCamera$DramaShotEventListener;

    return-object v0
.end method

.method static synthetic access$3600(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnBeautyShotEventListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mOnBeautyShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnBeautyShotEventListener;

    return-object v0
.end method

.method static synthetic access$3700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$BeautyEventListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mBeautyEventListener:Lcom/sec/android/seccamera/SecCamera$BeautyEventListener;

    return-object v0
.end method

.method static synthetic access$3800(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnAutoLowLightDetectionListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mOnAutoLowLightDetectionListener:Lcom/sec/android/seccamera/SecCamera$OnAutoLowLightDetectionListener;

    return-object v0
.end method

.method static synthetic access$3900(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$AutoLowLightDetectionListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mAutoLowLightDetectionListener:Lcom/sec/android/seccamera/SecCamera$AutoLowLightDetectionListener;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/seccamera/SecCamera;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    .line 166
    iget-boolean v0, p0, Lcom/sec/android/seccamera/SecCamera;->mOneShot:Z

    return v0
.end method

.method static synthetic access$4000(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnLightConditionChangedListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mOnLightConditionChangedListener:Lcom/sec/android/seccamera/SecCamera$OnLightConditionChangedListener;

    return-object v0
.end method

.method static synthetic access$4100(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$LightConditionChangedListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mLightConditionChangedListener:Lcom/sec/android/seccamera/SecCamera$LightConditionChangedListener;

    return-object v0
.end method

.method static synthetic access$4200(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnSecImageEffectListner;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mOnSecImageEffectListner:Lcom/sec/android/seccamera/SecCamera$OnSecImageEffectListner;

    return-object v0
.end method

.method static synthetic access$4300(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnDualEventListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mOnDualEventListener:Lcom/sec/android/seccamera/SecCamera$OnDualEventListener;

    return-object v0
.end method

.method static synthetic access$4400(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$DualEventListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mDualEventListener:Lcom/sec/android/seccamera/SecCamera$DualEventListener;

    return-object v0
.end method

.method static synthetic access$4500(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnSecImagingEventListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mOnSecImagingEventListener:Lcom/sec/android/seccamera/SecCamera$OnSecImagingEventListener;

    return-object v0
.end method

.method static synthetic access$4600(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$SecImagingEventListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mSecImagingEventListener:Lcom/sec/android/seccamera/SecCamera$SecImagingEventListener;

    return-object v0
.end method

.method static synthetic access$4700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnOutFocusShotModeEventListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mOnOutFocusShotModeEventListener:Lcom/sec/android/seccamera/SecCamera$OnOutFocusShotModeEventListener;

    return-object v0
.end method

.method static synthetic access$4800(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$SelectiveFocusEventListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mSelectiveFocusEventListener:Lcom/sec/android/seccamera/SecCamera$SelectiveFocusEventListener;

    return-object v0
.end method

.method static synthetic access$4900(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnMagicShotModeEventListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mOnMagicShotModeEventListener:Lcom/sec/android/seccamera/SecCamera$OnMagicShotModeEventListener;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/seccamera/SecCamera;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    .line 166
    iget-boolean v0, p0, Lcom/sec/android/seccamera/SecCamera;->mWithBuffer:Z

    return v0
.end method

.method static synthetic access$5000(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$ShotAndMoreEventListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mShotAndMoreEventListener:Lcom/sec/android/seccamera/SecCamera$ShotAndMoreEventListener;

    return-object v0
.end method

.method static synthetic access$5100(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnHazeRemovalEventListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mOnHazeRemovalShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnHazeRemovalEventListener;

    return-object v0
.end method

.method static synthetic access$5200(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$HazeRemovalEventListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mHazeRemovalShotEventListener:Lcom/sec/android/seccamera/SecCamera$HazeRemovalEventListener;

    return-object v0
.end method

.method static synthetic access$5300(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnCameraCurrentSettingListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mOnCameraCurrentSettingListener:Lcom/sec/android/seccamera/SecCamera$OnCameraCurrentSettingListener;

    return-object v0
.end method

.method static synthetic access$5400(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$CameraCurrentSettingListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mCameraCurrentSettingListener:Lcom/sec/android/seccamera/SecCamera$CameraCurrentSettingListener;

    return-object v0
.end method

.method static synthetic access$5500(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnPafResultListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mOnPafResultListener:Lcom/sec/android/seccamera/SecCamera$OnPafResultListener;

    return-object v0
.end method

.method static synthetic access$5600(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$PhaseAFCallback;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mPhaseAFCallback:Lcom/sec/android/seccamera/SecCamera$PhaseAFCallback;

    return-object v0
.end method

.method static synthetic access$5700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mOnUltraWideSelfieEventListener:Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;

    return-object v0
.end method

.method static synthetic access$5800(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mUltraWideSelfieEventListener:Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;

    return-object v0
.end method

.method static synthetic access$5900(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnAEResultListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mOnAEResultListener:Lcom/sec/android/seccamera/SecCamera$OnAEResultListener;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/seccamera/SecCamera;ZZZ)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/seccamera/SecCamera;
    .param p1, "x1"    # Z
    .param p2, "x2"    # Z
    .param p3, "x3"    # Z

    .prologue
    .line 166
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/seccamera/SecCamera;->setHasPreviewCallback(ZZZ)V

    return-void
.end method

.method static synthetic access$6000(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$AutoExposureCallback;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mAutoExposureCallback:Lcom/sec/android/seccamera/SecCamera$AutoExposureCallback;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$PictureCallback;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mPostviewCallback:Lcom/sec/android/seccamera/SecCamera$PictureCallback;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/seccamera/SecCamera;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mAutoFocusCallbackLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$AutoFocusCallback;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mAutoFocusCallback:Lcom/sec/android/seccamera/SecCamera$AutoFocusCallback;

    return-object v0
.end method

.method private final addCallbackBuffer([BI)V
    .locals 3
    .param p1, "callbackBuffer"    # [B
    .param p2, "msgType"    # I

    .prologue
    .line 3756
    const/16 v0, 0x10

    if-eq p2, v0, :cond_0

    const/16 v0, 0x80

    if-eq p2, v0, :cond_0

    .line 3758
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported message type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3762
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/sec/android/seccamera/SecCamera;->_addCallbackBuffer([BI)V

    .line 3763
    return-void
.end method

.method private cameraInitNormal(IILandroid/os/Looper;Z)I
    .locals 6
    .param p1, "cameraId"    # I
    .param p2, "priority"    # I
    .param p3, "_looper"    # Landroid/os/Looper;
    .param p4, "halsettting"    # Z

    .prologue
    .line 3286
    const/4 v2, -0x2

    move-object v0, p0

    move v1, p1

    move v3, p2

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/seccamera/SecCamera;->cameraInitVersion(IIILandroid/os/Looper;Z)I

    move-result v0

    return v0
.end method

.method private cameraInitVersion(IIILandroid/os/Looper;Z)I
    .locals 9
    .param p1, "cameraId"    # I
    .param p2, "halVersion"    # I
    .param p3, "priority"    # I
    .param p4, "_looper"    # Landroid/os/Looper;
    .param p5, "halsettting"    # Z

    .prologue
    const/4 v8, 0x0

    const/4 v0, 0x0

    .line 3254
    iput-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mShutterCallback:Lcom/sec/android/seccamera/SecCamera$ShutterCallback;

    .line 3255
    iput-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mRawImageCallback:Lcom/sec/android/seccamera/SecCamera$PictureCallback;

    .line 3256
    iput-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mJpegCallback:Lcom/sec/android/seccamera/SecCamera$PictureCallback;

    .line 3257
    iput-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mPreviewCallback:Lcom/sec/android/seccamera/SecCamera$PreviewCallback;

    .line 3258
    iput-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mPostviewCallback:Lcom/sec/android/seccamera/SecCamera$PictureCallback;

    .line 3259
    iput-boolean v8, p0, Lcom/sec/android/seccamera/SecCamera;->mUsingPreviewAllocation:Z

    .line 3260
    iput-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mZoomListener:Lcom/sec/android/seccamera/SecCamera$OnZoomChangeListener;

    .line 3263
    if-eqz p4, :cond_2

    .line 3265
    new-instance v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;

    invoke-direct {v0, p0, p0, p4}, Lcom/sec/android/seccamera/SecCamera$EventHandler;-><init>(Lcom/sec/android/seccamera/SecCamera;Lcom/sec/android/seccamera/SecCamera;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mEventHandler:Lcom/sec/android/seccamera/SecCamera$EventHandler;

    .line 3274
    :goto_0
    invoke-static {}, Landroid/app/ActivityThread;->currentPackageName()Ljava/lang/String;

    move-result-object v4

    .line 3275
    .local v4, "packageName":Ljava/lang/String;
    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v0

    const/16 v1, 0x3e8

    if-ne v0, v1, :cond_0

    if-nez v4, :cond_0

    const-string v4, "android"

    .line 3277
    :cond_0
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    move-object v0, p0

    move v2, p1

    move v3, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/seccamera/SecCamera;->native_setup(Ljava/lang/Object;IILjava/lang/String;I)I

    move-result v7

    .line 3278
    .local v7, "res":I
    if-nez v7, :cond_1

    .line 3279
    if-eqz p5, :cond_1

    const/16 v0, 0x5e4

    invoke-virtual {p0, v0, v8, v8}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 3281
    :cond_1
    return v7

    .line 3266
    .end local v4    # "packageName":Ljava/lang/String;
    .end local v7    # "res":I
    :cond_2
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v6

    .local v6, "looper":Landroid/os/Looper;
    if-eqz v6, :cond_3

    .line 3267
    new-instance v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;

    invoke-direct {v0, p0, p0, v6}, Lcom/sec/android/seccamera/SecCamera$EventHandler;-><init>(Lcom/sec/android/seccamera/SecCamera;Lcom/sec/android/seccamera/SecCamera;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mEventHandler:Lcom/sec/android/seccamera/SecCamera$EventHandler;

    goto :goto_0

    .line 3268
    :cond_3
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v6

    if-eqz v6, :cond_4

    .line 3269
    new-instance v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;

    invoke-direct {v0, p0, p0, v6}, Lcom/sec/android/seccamera/SecCamera$EventHandler;-><init>(Lcom/sec/android/seccamera/SecCamera;Lcom/sec/android/seccamera/SecCamera;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mEventHandler:Lcom/sec/android/seccamera/SecCamera$EventHandler;

    goto :goto_0

    .line 3271
    :cond_4
    iput-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mEventHandler:Lcom/sec/android/seccamera/SecCamera$EventHandler;

    goto :goto_0
.end method

.method public static checkInitErrors(I)Z
    .locals 1
    .param p0, "err"    # I

    .prologue
    .line 3332
    if-eqz p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private native enableFocusMoveCallback(I)V
.end method

.method public static getCameraInfo(ILcom/sec/android/seccamera/SecCamera$CameraInfo;)V
    .locals 5
    .param p0, "cameraId"    # I
    .param p1, "cameraInfo"    # Lcom/sec/android/seccamera/SecCamera$CameraInfo;

    .prologue
    .line 294
    invoke-static {p0, p1}, Lcom/sec/android/seccamera/SecCamera;->_getCameraInfo(ILcom/sec/android/seccamera/SecCamera$CameraInfo;)V

    .line 295
    const-string v3, "audio"

    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    .line 296
    .local v1, "b":Landroid/os/IBinder;
    invoke-static {v1}, Landroid/media/IAudioService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/media/IAudioService;

    move-result-object v0

    .line 298
    .local v0, "audioService":Landroid/media/IAudioService;
    if-eqz v0, :cond_0

    .line 299
    :try_start_0
    invoke-interface {v0}, Landroid/media/IAudioService;->isCameraSoundForced()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 302
    const/4 v3, 0x0

    iput-boolean v3, p1, Lcom/sec/android/seccamera/SecCamera$CameraInfo;->canDisableShutterSound:Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 309
    :cond_0
    :goto_0
    return-void

    .line 306
    :catch_0
    move-exception v2

    .line 307
    .local v2, "e":Landroid/os/RemoteException;
    const-string v3, "SecCamera-JNI-Java"

    const-string v4, "Audio service is unavailable for queries"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static getEmptyParameters()Lcom/sec/android/seccamera/SecCamera$Parameters;
    .locals 3

    .prologue
    .line 6694
    new-instance v0, Lcom/sec/android/seccamera/SecCamera;

    invoke-direct {v0}, Lcom/sec/android/seccamera/SecCamera;-><init>()V

    .line 6695
    .local v0, "camera":Lcom/sec/android/seccamera/SecCamera;
    new-instance v1, Lcom/sec/android/seccamera/SecCamera$Parameters;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Lcom/sec/android/seccamera/SecCamera$Parameters;-><init>(Lcom/sec/android/seccamera/SecCamera;Lcom/sec/android/seccamera/SecCamera$1;)V

    return-object v1
.end method

.method public static native getNumberOfCameras()I
.end method

.method public static getParametersCopy(Lcom/sec/android/seccamera/SecCamera$Parameters;)Lcom/sec/android/seccamera/SecCamera$Parameters;
    .locals 4
    .param p0, "parameters"    # Lcom/sec/android/seccamera/SecCamera$Parameters;

    .prologue
    .line 6708
    if-nez p0, :cond_0

    .line 6709
    new-instance v2, Ljava/lang/NullPointerException;

    const-string v3, "parameters must not be null"

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 6712
    :cond_0
    # invokes: Lcom/sec/android/seccamera/SecCamera$Parameters;->getOuter()Lcom/sec/android/seccamera/SecCamera;
    invoke-static {p0}, Lcom/sec/android/seccamera/SecCamera$Parameters;->access$6200(Lcom/sec/android/seccamera/SecCamera$Parameters;)Lcom/sec/android/seccamera/SecCamera;

    move-result-object v0

    .line 6713
    .local v0, "camera":Lcom/sec/android/seccamera/SecCamera;
    new-instance v1, Lcom/sec/android/seccamera/SecCamera$Parameters;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Lcom/sec/android/seccamera/SecCamera$Parameters;-><init>(Lcom/sec/android/seccamera/SecCamera;Lcom/sec/android/seccamera/SecCamera$1;)V

    .line 6714
    .local v1, "p":Lcom/sec/android/seccamera/SecCamera$Parameters;
    invoke-virtual {v1, p0}, Lcom/sec/android/seccamera/SecCamera$Parameters;->copyFrom(Lcom/sec/android/seccamera/SecCamera$Parameters;)V

    .line 6716
    return-object v1
.end method

.method private final native native_autoFocus()V
.end method

.method private final native native_cancelAutoFocus()V
.end method

.method private final native native_getParameters()Ljava/lang/String;
.end method

.method private final native native_release()V
.end method

.method private final native native_setParameters(Ljava/lang/String;)V
.end method

.method private final native native_setup(Ljava/lang/Object;IILjava/lang/String;I)I
.end method

.method private final native native_takePicture(I)V
.end method

.method public static open()Lcom/sec/android/seccamera/SecCamera;
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 3156
    invoke-static {}, Landroid/hardware/Camera;->checkCameraEnabled()Z

    .line 3158
    invoke-static {}, Lcom/sec/android/seccamera/SecCamera;->getNumberOfCameras()I

    move-result v2

    .line 3159
    .local v2, "numberOfCameras":I
    new-instance v0, Lcom/sec/android/seccamera/SecCamera$CameraInfo;

    invoke-direct {v0}, Lcom/sec/android/seccamera/SecCamera$CameraInfo;-><init>()V

    .line 3160
    .local v0, "cameraInfo":Lcom/sec/android/seccamera/SecCamera$CameraInfo;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 3161
    invoke-static {v1, v0}, Lcom/sec/android/seccamera/SecCamera;->getCameraInfo(ILcom/sec/android/seccamera/SecCamera$CameraInfo;)V

    .line 3162
    iget v3, v0, Lcom/sec/android/seccamera/SecCamera$CameraInfo;->facing:I

    if-nez v3, :cond_0

    .line 3163
    new-instance v3, Lcom/sec/android/seccamera/SecCamera;

    const/16 v5, 0x64

    const/4 v6, 0x1

    invoke-direct {v3, v1, v5, v4, v6}, Lcom/sec/android/seccamera/SecCamera;-><init>(IILandroid/os/Looper;Z)V

    .line 3166
    :goto_1
    return-object v3

    .line 3160
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    move-object v3, v4

    .line 3166
    goto :goto_1
.end method

.method public static open(I)Lcom/sec/android/seccamera/SecCamera;
    .locals 4
    .param p0, "cameraId"    # I

    .prologue
    .line 3100
    const-string v0, "SecCamera-JNI-Java"

    const-string v1, "SecCamera.open()"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3102
    invoke-static {}, Landroid/hardware/Camera;->checkCameraEnabled()Z

    .line 3104
    new-instance v0, Lcom/sec/android/seccamera/SecCamera;

    const/16 v1, 0x64

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/sec/android/seccamera/SecCamera;-><init>(IILandroid/os/Looper;Z)V

    return-object v0
.end method

.method public static open(II)Lcom/sec/android/seccamera/SecCamera;
    .locals 3
    .param p0, "cameraId"    # I
    .param p1, "priority"    # I

    .prologue
    .line 3119
    const-string v0, "SecCamera-JNI-Java"

    const-string v1, "SecCamera.open()"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3121
    invoke-static {}, Landroid/hardware/Camera;->checkCameraEnabled()Z

    .line 3123
    new-instance v0, Lcom/sec/android/seccamera/SecCamera;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {v0, p0, p1, v1, v2}, Lcom/sec/android/seccamera/SecCamera;-><init>(IILandroid/os/Looper;Z)V

    return-object v0
.end method

.method public static open(IILandroid/os/Looper;)Lcom/sec/android/seccamera/SecCamera;
    .locals 2
    .param p0, "cameraId"    # I
    .param p1, "priority"    # I
    .param p2, "looper"    # Landroid/os/Looper;

    .prologue
    .line 3130
    const-string v0, "SecCamera-JNI-Java"

    const-string v1, "SecCamera.open()"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3132
    invoke-static {}, Landroid/hardware/Camera;->checkCameraEnabled()Z

    .line 3134
    new-instance v0, Lcom/sec/android/seccamera/SecCamera;

    const/4 v1, 0x1

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/sec/android/seccamera/SecCamera;-><init>(IILandroid/os/Looper;Z)V

    return-object v0
.end method

.method public static open(IILandroid/os/Looper;Z)Lcom/sec/android/seccamera/SecCamera;
    .locals 2
    .param p0, "cameraId"    # I
    .param p1, "priority"    # I
    .param p2, "looper"    # Landroid/os/Looper;
    .param p3, "halsetting"    # Z

    .prologue
    .line 3141
    const-string v0, "SecCamera-JNI-Java"

    const-string v1, "SecCamera.open()"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3143
    invoke-static {}, Landroid/hardware/Camera;->checkCameraEnabled()Z

    .line 3145
    new-instance v0, Lcom/sec/android/seccamera/SecCamera;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/sec/android/seccamera/SecCamera;-><init>(IILandroid/os/Looper;Z)V

    return-object v0
.end method

.method public static open(ILandroid/os/Looper;)Lcom/sec/android/seccamera/SecCamera;
    .locals 3
    .param p0, "cameraId"    # I
    .param p1, "looper"    # Landroid/os/Looper;

    .prologue
    .line 3108
    const-string v0, "SecCamera-JNI-Java"

    const-string v1, "SecCamera.open()"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3110
    invoke-static {}, Landroid/hardware/Camera;->checkCameraEnabled()Z

    .line 3112
    new-instance v0, Lcom/sec/android/seccamera/SecCamera;

    const/16 v1, 0x64

    const/4 v2, 0x1

    invoke-direct {v0, p0, v1, p1, v2}, Lcom/sec/android/seccamera/SecCamera;-><init>(IILandroid/os/Looper;Z)V

    return-object v0
.end method

.method public static openLegacy(II)Lcom/sec/android/seccamera/SecCamera;
    .locals 3
    .param p0, "cameraId"    # I
    .param p1, "halVersion"    # I

    .prologue
    .line 3208
    const/16 v0, 0x100

    if-ge p1, v0, :cond_0

    .line 3209
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid HAL version "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3212
    :cond_0
    new-instance v0, Lcom/sec/android/seccamera/SecCamera;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/seccamera/SecCamera;-><init>(II)V

    return-object v0
.end method

.method public static openUninitialized()Lcom/sec/android/seccamera/SecCamera;
    .locals 1

    .prologue
    .line 3339
    new-instance v0, Lcom/sec/android/seccamera/SecCamera;

    invoke-direct {v0}, Lcom/sec/android/seccamera/SecCamera;-><init>()V

    return-object v0
.end method

.method private static postEventFromNative(Ljava/lang/Object;IIILjava/lang/Object;)V
    .locals 4
    .param p0, "camera_ref"    # Ljava/lang/Object;
    .param p1, "what"    # I
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "obj"    # Ljava/lang/Object;

    .prologue
    .line 4721
    check-cast p0, Ljava/lang/ref/WeakReference;

    .end local p0    # "camera_ref":Ljava/lang/Object;
    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/seccamera/SecCamera;

    .line 4722
    .local v0, "c":Lcom/sec/android/seccamera/SecCamera;
    if-nez v0, :cond_0

    .line 4742
    :goto_0
    return-void

    .line 4725
    :cond_0
    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera;->mEventHandler:Lcom/sec/android/seccamera/SecCamera$EventHandler;

    if-eqz v2, :cond_1

    .line 4726
    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera;->mEventHandler:Lcom/sec/android/seccamera/SecCamera$EventHandler;

    invoke-virtual {v2, p1, p2, p3, p4}, Lcom/sec/android/seccamera/SecCamera$EventHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 4737
    .local v1, "m":Landroid/os/Message;
    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera;->mEventHandler:Lcom/sec/android/seccamera/SecCamera$EventHandler;

    invoke-virtual {v2, v1}, Lcom/sec/android/seccamera/SecCamera$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 4740
    .end local v1    # "m":Landroid/os/Message;
    :cond_1
    const-string v2, "SecCamera-JNI-Java"

    const-string v3, "mEventHandler is null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private final native setHasPreviewCallback(ZZZ)V
.end method

.method private final native setPreviewCallbackSurface(Landroid/view/Surface;)V
.end method


# virtual methods
.method public final addCallbackBuffer([B)V
    .locals 1
    .param p1, "callbackBuffer"    # [B

    .prologue
    .line 3707
    const/16 v0, 0x10

    invoke-direct {p0, p1, v0}, Lcom/sec/android/seccamera/SecCamera;->_addCallbackBuffer([BI)V

    .line 3708
    return-void
.end method

.method public final addRawImageCallbackBuffer([B)V
    .locals 1
    .param p1, "callbackBuffer"    # [B

    .prologue
    .line 3750
    const/16 v0, 0x80

    invoke-direct {p0, p1, v0}, Lcom/sec/android/seccamera/SecCamera;->addCallbackBuffer([BI)V

    .line 3751
    return-void
.end method

.method public final autoFocus(Lcom/sec/android/seccamera/SecCamera$AutoFocusCallback;)V
    .locals 2
    .param p1, "cb"    # Lcom/sec/android/seccamera/SecCamera$AutoFocusCallback;

    .prologue
    .line 4821
    iget-object v1, p0, Lcom/sec/android/seccamera/SecCamera;->mAutoFocusCallbackLock:Ljava/lang/Object;

    monitor-enter v1

    .line 4822
    :try_start_0
    iput-object p1, p0, Lcom/sec/android/seccamera/SecCamera;->mAutoFocusCallback:Lcom/sec/android/seccamera/SecCamera$AutoFocusCallback;

    .line 4823
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4824
    invoke-direct {p0}, Lcom/sec/android/seccamera/SecCamera;->native_autoFocus()V

    .line 4825
    return-void

    .line 4823
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public cameraInitUnspecified(I)I
    .locals 6
    .param p1, "cameraId"    # I

    .prologue
    .line 3304
    const/4 v2, -0x1

    const/16 v3, 0x64

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v0, p0

    move v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/seccamera/SecCamera;->cameraInitVersion(IIILandroid/os/Looper;Z)I

    move-result v0

    return v0
.end method

.method public final cancelAutoFocus()V
    .locals 2

    .prologue
    .line 4840
    invoke-direct {p0}, Lcom/sec/android/seccamera/SecCamera;->native_cancelAutoFocus()V

    .line 4856
    iget-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mEventHandler:Lcom/sec/android/seccamera/SecCamera$EventHandler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/android/seccamera/SecCamera$EventHandler;->removeMessages(I)V

    .line 4857
    return-void
.end method

.method public cancelDramaShot()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 6227
    const/16 v0, 0x535

    invoke-virtual {p0, v0, v1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 6228
    return-void
.end method

.method public cancelMagicShot()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 6475
    const/16 v0, 0x58f

    invoke-virtual {p0, v0, v1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 6476
    return-void
.end method

.method public cancelPanorama()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 5659
    const/16 v0, 0x45a

    invoke-virtual {p0, v0, v1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 5660
    return-void
.end method

.method public cancelSeriesActionShot()V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 5815
    const/16 v0, 0x46e

    invoke-virtual {p0, v0, v1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 5816
    return-void
.end method

.method public cancelTakePicture()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 6654
    const/16 v0, 0x5c0

    invoke-virtual {p0, v0, v1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 6655
    return-void
.end method

.method public cancelUltraWideSelfie()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 6507
    const/16 v0, 0x5b7

    invoke-virtual {p0, v0, v1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 6508
    return-void
.end method

.method public captureBurstShot()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 5759
    const/16 v0, 0x48a

    invoke-virtual {p0, v0, v1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 5760
    return-void
.end method

.method public captureBurstShot(I)V
    .locals 2
    .param p1, "duration"    # I

    .prologue
    .line 5769
    const/16 v0, 0x48a

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 5770
    return-void
.end method

.method public final createPreviewAllocation(Landroid/renderscript/RenderScript;I)Landroid/renderscript/Allocation;
    .locals 6
    .param p1, "rs"    # Landroid/renderscript/RenderScript;
    .param p2, "usage"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/renderscript/RSIllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 3795
    invoke-virtual {p0}, Lcom/sec/android/seccamera/SecCamera;->getParameters()Lcom/sec/android/seccamera/SecCamera$Parameters;

    move-result-object v1

    .line 3796
    .local v1, "p":Lcom/sec/android/seccamera/SecCamera$Parameters;
    invoke-virtual {v1}, Lcom/sec/android/seccamera/SecCamera$Parameters;->getPreviewSize()Lcom/sec/android/seccamera/SecCamera$Size;

    move-result-object v2

    .line 3797
    .local v2, "previewSize":Lcom/sec/android/seccamera/SecCamera$Size;
    new-instance v3, Landroid/renderscript/Type$Builder;

    sget-object v4, Landroid/renderscript/Element$DataType;->UNSIGNED_8:Landroid/renderscript/Element$DataType;

    sget-object v5, Landroid/renderscript/Element$DataKind;->PIXEL_YUV:Landroid/renderscript/Element$DataKind;

    invoke-static {p1, v4, v5}, Landroid/renderscript/Element;->createPixel(Landroid/renderscript/RenderScript;Landroid/renderscript/Element$DataType;Landroid/renderscript/Element$DataKind;)Landroid/renderscript/Element;

    move-result-object v4

    invoke-direct {v3, p1, v4}, Landroid/renderscript/Type$Builder;-><init>(Landroid/renderscript/RenderScript;Landroid/renderscript/Element;)V

    .line 3803
    .local v3, "yuvBuilder":Landroid/renderscript/Type$Builder;
    const v4, 0x32315659

    invoke-virtual {v3, v4}, Landroid/renderscript/Type$Builder;->setYuvFormat(I)Landroid/renderscript/Type$Builder;

    .line 3804
    iget v4, v2, Lcom/sec/android/seccamera/SecCamera$Size;->width:I

    invoke-virtual {v3, v4}, Landroid/renderscript/Type$Builder;->setX(I)Landroid/renderscript/Type$Builder;

    .line 3805
    iget v4, v2, Lcom/sec/android/seccamera/SecCamera$Size;->height:I

    invoke-virtual {v3, v4}, Landroid/renderscript/Type$Builder;->setY(I)Landroid/renderscript/Type$Builder;

    .line 3807
    invoke-virtual {v3}, Landroid/renderscript/Type$Builder;->create()Landroid/renderscript/Type;

    move-result-object v4

    or-int/lit8 v5, p2, 0x20

    invoke-static {p1, v4, v5}, Landroid/renderscript/Allocation;->createTyped(Landroid/renderscript/RenderScript;Landroid/renderscript/Type;I)Landroid/renderscript/Allocation;

    move-result-object v0

    .line 3810
    .local v0, "a":Landroid/renderscript/Allocation;
    return-object v0
.end method

.method public enableCurrentSet(Z)V
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    const/16 v2, 0x66b

    const/4 v1, 0x0

    .line 6581
    if-eqz p1, :cond_0

    .line 6582
    const/4 v0, 0x1

    invoke-virtual {p0, v2, v0, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 6585
    :goto_0
    return-void

    .line 6584
    :cond_0
    invoke-virtual {p0, v2, v1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    goto :goto_0
.end method

.method public enableDualTrackingCoordinate(Z)V
    .locals 3
    .param p1, "isDualTrackingCoordi"    # Z

    .prologue
    const/16 v2, 0x55f

    const/4 v1, 0x0

    .line 6320
    if-eqz p1, :cond_0

    .line 6321
    const/4 v0, 0x1

    invoke-virtual {p0, v2, v0, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 6324
    :goto_0
    return-void

    .line 6323
    :cond_0
    invoke-virtual {p0, v2, v1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    goto :goto_0
.end method

.method public enableDualTrackingMode(Z)V
    .locals 3
    .param p1, "isDualTrackingMode"    # Z

    .prologue
    const/16 v2, 0x560

    const/4 v1, 0x0

    .line 6343
    if-eqz p1, :cond_0

    .line 6344
    const/4 v0, 0x1

    invoke-virtual {p0, v2, v0, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 6347
    :goto_0
    return-void

    .line 6346
    :cond_0
    invoke-virtual {p0, v2, v1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    goto :goto_0
.end method

.method public enableLightConditionDetect(Z)V
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    const/16 v2, 0x549

    const/4 v1, 0x0

    .line 6625
    if-eqz p1, :cond_0

    .line 6626
    const/4 v0, 0x1

    invoke-virtual {p0, v2, v0, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 6629
    :goto_0
    return-void

    .line 6628
    :cond_0
    invoke-virtual {p0, v2, v1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    goto :goto_0
.end method

.method public enablePafReuslt(Z)V
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    const/16 v2, 0x66c

    const/4 v1, 0x0

    .line 6592
    if-eqz p1, :cond_0

    .line 6593
    const/4 v0, 0x1

    invoke-virtual {p0, v2, v0, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 6596
    :goto_0
    return-void

    .line 6595
    :cond_0
    invoke-virtual {p0, v2, v1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    goto :goto_0
.end method

.method public enablePanoramaShutterSound(Z)V
    .locals 2
    .param p1, "start"    # Z

    .prologue
    const/4 v1, 0x0

    .line 5630
    if-eqz p1, :cond_0

    .line 5631
    const/16 v0, 0x45b

    invoke-virtual {p0, v0, v1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 5635
    :goto_0
    return-void

    .line 5633
    :cond_0
    const/16 v0, 0x45c

    invoke-virtual {p0, v0, v1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    goto :goto_0
.end method

.method public final enableShutterSound(Z)Z
    .locals 5
    .param p1, "enabled"    # Z

    .prologue
    .line 5114
    if-nez p1, :cond_0

    .line 5115
    const-string v3, "audio"

    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    .line 5116
    .local v1, "b":Landroid/os/IBinder;
    invoke-static {v1}, Landroid/media/IAudioService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/media/IAudioService;

    move-result-object v0

    .line 5118
    .local v0, "audioService":Landroid/media/IAudioService;
    if-eqz v0, :cond_0

    .line 5119
    :try_start_0
    invoke-interface {v0}, Landroid/media/IAudioService;->isCameraSoundForced()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x0

    .line 5125
    .end local v0    # "audioService":Landroid/media/IAudioService;
    .end local v1    # "b":Landroid/os/IBinder;
    :goto_0
    return v3

    .line 5121
    .restart local v0    # "audioService":Landroid/media/IAudioService;
    .restart local v1    # "b":Landroid/os/IBinder;
    :catch_0
    move-exception v2

    .line 5122
    .local v2, "e":Landroid/os/RemoteException;
    const-string v3, "SecCamera-JNI-Java"

    const-string v4, "Audio service is unavailable for queries"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 5125
    .end local v0    # "audioService":Landroid/media/IAudioService;
    .end local v1    # "b":Landroid/os/IBinder;
    .end local v2    # "e":Landroid/os/RemoteException;
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/android/seccamera/SecCamera;->_enableShutterSound(Z)Z

    move-result v3

    goto :goto_0
.end method

.method protected finalize()V
    .locals 0

    .prologue
    .line 3349
    invoke-virtual {p0}, Lcom/sec/android/seccamera/SecCamera;->release()V

    .line 3350
    return-void
.end method

.method public finishActionShot()V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 5824
    const/16 v0, 0x470

    invoke-virtual {p0, v0, v1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 5825
    return-void
.end method

.method public declared-synchronized flatten(Ljava/util/HashMap;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 6727
    .local p1, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    invoke-virtual {p1}, Ljava/util/HashMap;->size()I

    move-result v3

    if-nez v3, :cond_1

    .line 6728
    :cond_0
    const-string v3, ""
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6740
    :goto_0
    monitor-exit p0

    return-object v3

    .line 6731
    :cond_1
    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v3, 0x80

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 6732
    .local v0, "flattened":Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 6733
    .local v2, "k":Ljava/lang/String;
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 6734
    const-string v3, "="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 6735
    invoke-virtual {p1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 6736
    const-string v3, ";"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 6727
    .end local v0    # "flattened":Ljava/lang/StringBuilder;
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "k":Ljava/lang/String;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 6739
    .restart local v0    # "flattened":Ljava/lang/StringBuilder;
    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_2
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 6740
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v3

    goto :goto_0
.end method

.method public flipFrontCapturedImage(ZI)V
    .locals 4
    .param p1, "mirror"    # Z
    .param p2, "orientation"    # I

    .prologue
    const/16 v3, 0x5e7

    const/16 v2, 0x5e6

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 6008
    if-eqz p1, :cond_0

    .line 6009
    invoke-virtual {p0, v2, v1, v0}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 6010
    invoke-virtual {p0, v3, p2, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 6015
    :goto_0
    return-void

    .line 6012
    :cond_0
    invoke-virtual {p0, v2, v0, v0}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 6013
    invoke-virtual {p0, v3, p2, v0}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    goto :goto_0
.end method

.method public getBurstShotString()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 5741
    const/16 v0, 0x489

    invoke-virtual {p0, v0, v1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 5742
    return-void
.end method

.method public getParameters()Lcom/sec/android/seccamera/SecCamera$Parameters;
    .locals 3

    .prologue
    .line 5498
    new-instance v0, Lcom/sec/android/seccamera/SecCamera$Parameters;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v2}, Lcom/sec/android/seccamera/SecCamera$Parameters;-><init>(Lcom/sec/android/seccamera/SecCamera;Lcom/sec/android/seccamera/SecCamera$1;)V

    .line 5499
    .local v0, "p":Lcom/sec/android/seccamera/SecCamera$Parameters;
    invoke-direct {p0}, Lcom/sec/android/seccamera/SecCamera;->native_getParameters()Ljava/lang/String;

    move-result-object v1

    .line 5500
    .local v1, "s":Ljava/lang/String;
    invoke-virtual {v0, v1}, Lcom/sec/android/seccamera/SecCamera$Parameters;->unflatten(Ljava/lang/String;)V

    .line 5501
    return-object v0
.end method

.method public initializeActionShot()V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 5779
    const/16 v0, 0x46b

    invoke-virtual {p0, v0, v1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 5780
    return-void
.end method

.method public final native lock()V
.end method

.method public final native native_sendcommand(III)V
.end method

.method public final native previewEnabled()Z
.end method

.method public final native reconnect()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public final release()V
    .locals 2

    .prologue
    .line 3362
    invoke-direct {p0}, Lcom/sec/android/seccamera/SecCamera;->native_release()V

    .line 3363
    iget-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mEventHandler:Lcom/sec/android/seccamera/SecCamera$EventHandler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mEventHandler:Lcom/sec/android/seccamera/SecCamera$EventHandler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/seccamera/SecCamera$EventHandler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 3364
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/seccamera/SecCamera;->mFaceDetectionRunning:Z

    .line 3365
    return-void
.end method

.method public saveGolfShot(I)V
    .locals 2
    .param p1, "Storage"    # I

    .prologue
    .line 6165
    const/16 v0, 0x521

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 6166
    return-void
.end method

.method public sendCommand(III)V
    .locals 0
    .param p1, "codeA"    # I
    .param p2, "codeB"    # I
    .param p3, "codeC"    # I

    .prologue
    .line 6672
    invoke-virtual {p0, p1, p2, p3}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 6673
    return-void
.end method

.method public sendOrientaionInfotoHAL(I)V
    .locals 2
    .param p1, "orientation"    # I

    .prologue
    .line 6421
    const/16 v0, 0x5f1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 6422
    return-void
.end method

.method public setAutoExposureCallback(Lcom/sec/android/seccamera/SecCamera$AutoExposureCallback;)V
    .locals 0
    .param p1, "cb"    # Lcom/sec/android/seccamera/SecCamera$AutoExposureCallback;

    .prologue
    .line 1721
    iput-object p1, p0, Lcom/sec/android/seccamera/SecCamera;->mAutoExposureCallback:Lcom/sec/android/seccamera/SecCamera$AutoExposureCallback;

    .line 1722
    return-void
.end method

.method public final setAutoFocusCb(Lcom/sec/android/seccamera/SecCamera$AutoFocusCallback;)V
    .locals 2
    .param p1, "cb"    # Lcom/sec/android/seccamera/SecCamera$AutoFocusCallback;

    .prologue
    .line 4900
    iget-object v1, p0, Lcom/sec/android/seccamera/SecCamera;->mAutoFocusCallbackLock:Ljava/lang/Object;

    monitor-enter v1

    .line 4901
    :try_start_0
    iput-object p1, p0, Lcom/sec/android/seccamera/SecCamera;->mAutoFocusCallback:Lcom/sec/android/seccamera/SecCamera$AutoFocusCallback;

    .line 4902
    monitor-exit v1

    .line 4903
    return-void

    .line 4902
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setAutoFocusMoveCallback(Lcom/sec/android/seccamera/SecCamera$AutoFocusMoveCallback;)V
    .locals 1
    .param p1, "cb"    # Lcom/sec/android/seccamera/SecCamera$AutoFocusMoveCallback;

    .prologue
    .line 4885
    iput-object p1, p0, Lcom/sec/android/seccamera/SecCamera;->mAutoFocusMoveCallback:Lcom/sec/android/seccamera/SecCamera$AutoFocusMoveCallback;

    .line 4886
    iget-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mAutoFocusMoveCallback:Lcom/sec/android/seccamera/SecCamera$AutoFocusMoveCallback;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/sec/android/seccamera/SecCamera;->enableFocusMoveCallback(I)V

    .line 4887
    return-void

    .line 4886
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setAutoLowLight(Z)V
    .locals 3
    .param p1, "setting"    # Z

    .prologue
    const/16 v2, 0x547

    const/4 v1, 0x0

    .line 6202
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v2, v0, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 6204
    :goto_0
    return-void

    .line 6203
    :cond_0
    invoke-virtual {p0, v2, v1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    goto :goto_0
.end method

.method public setAutoLowLightDetectionListener(Lcom/sec/android/seccamera/SecCamera$AutoLowLightDetectionListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/seccamera/SecCamera$AutoLowLightDetectionListener;

    .prologue
    .line 1456
    iput-object p1, p0, Lcom/sec/android/seccamera/SecCamera;->mAutoLowLightDetectionListener:Lcom/sec/android/seccamera/SecCamera$AutoLowLightDetectionListener;

    .line 1457
    return-void
.end method

.method public setBeautyEffect(Z)V
    .locals 3
    .param p1, "mLive"    # Z

    .prologue
    const/16 v2, 0x49e

    const/4 v1, 0x0

    .line 5899
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v2, v0, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 5901
    :goto_0
    return-void

    .line 5900
    :cond_0
    invoke-virtual {p0, v2, v1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    goto :goto_0
.end method

.method public setBeautyEventListener(Lcom/sec/android/seccamera/SecCamera$BeautyEventListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/seccamera/SecCamera$BeautyEventListener;

    .prologue
    .line 1394
    iput-object p1, p0, Lcom/sec/android/seccamera/SecCamera;->mBeautyEventListener:Lcom/sec/android/seccamera/SecCamera$BeautyEventListener;

    .line 1395
    return-void
.end method

.method public setBeautyLevel(ZI)V
    .locals 3
    .param p1, "effect"    # Z
    .param p2, "retouchLevel"    # I

    .prologue
    const/16 v2, 0x49e

    const/4 v1, 0x0

    .line 5853
    if-eqz p1, :cond_0

    .line 5854
    const/4 v0, 0x1

    invoke-virtual {p0, v2, v0, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 5857
    :goto_0
    const/16 v0, 0x49d

    invoke-virtual {p0, v0, p2, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 5859
    return-void

    .line 5856
    :cond_0
    invoke-virtual {p0, v2, v1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    goto :goto_0
.end method

.method public setBeautyShotManualMode(Z)V
    .locals 3
    .param p1, "mManual"    # Z

    .prologue
    const/16 v2, 0x49f

    const/4 v1, 0x0

    .line 5910
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v2, v0, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 5912
    :goto_0
    return-void

    .line 5911
    :cond_0
    invoke-virtual {p0, v2, v1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    goto :goto_0
.end method

.method public setBurstEventListener(Lcom/sec/android/seccamera/SecCamera$BurstEventListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/seccamera/SecCamera$BurstEventListener;

    .prologue
    .line 1101
    iput-object p1, p0, Lcom/sec/android/seccamera/SecCamera;->mBurstEventListener:Lcom/sec/android/seccamera/SecCamera$BurstEventListener;

    .line 1102
    return-void
.end method

.method public setBurstShot(IIII)V
    .locals 1
    .param p1, "bestpic"    # I
    .param p2, "storage"    # I
    .param p3, "duration"    # I
    .param p4, "maxcount"    # I

    .prologue
    .line 5698
    const/16 v0, 0x48f

    invoke-virtual {p0, v0, p1, p2}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 5699
    const/16 v0, 0x48e

    invoke-virtual {p0, v0, p3, p4}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 5700
    return-void
.end method

.method public setBurstShotStoring()V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 5750
    const-string v0, "SecCamera-JNI-Java"

    const-string v1, "deprecated. replace to getBurstShotStoring"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 5751
    invoke-virtual {p0}, Lcom/sec/android/seccamera/SecCamera;->getBurstShotString()V

    .line 5752
    return-void
.end method

.method public setCameraCurrentSettingListener(Lcom/sec/android/seccamera/SecCamera$CameraCurrentSettingListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/seccamera/SecCamera$CameraCurrentSettingListener;

    .prologue
    .line 1701
    iput-object p1, p0, Lcom/sec/android/seccamera/SecCamera;->mCameraCurrentSettingListener:Lcom/sec/android/seccamera/SecCamera$CameraCurrentSettingListener;

    .line 1702
    return-void
.end method

.method public setDefaultIMEI(I)V
    .locals 2
    .param p1, "bIsDefaultIMEI"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 5977
    const/16 v0, 0x5e3

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 5978
    return-void
.end method

.method public final native setDisplayOrientation(I)V
.end method

.method public setDistortionEffectsInfo(II)V
    .locals 1
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 6514
    const/16 v0, 0x5b8

    invoke-virtual {p0, v0, p1, p2}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 6515
    return-void
.end method

.method public setDistortionEffectsMode(II)V
    .locals 1
    .param p1, "mode"    # I
    .param p2, "orientation"    # I

    .prologue
    .line 6521
    const/16 v0, 0x5b9

    invoke-virtual {p0, v0, p1, p2}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 6522
    return-void
.end method

.method public setDramaShotEventListener(Lcom/sec/android/seccamera/SecCamera$DramaShotEventListener;)V
    .locals 2
    .param p1, "l"    # Lcom/sec/android/seccamera/SecCamera$DramaShotEventListener;

    .prologue
    .line 1361
    const-string v0, "SecCamera-JNI-Java"

    const-string v1, "setDramaShotEventListener"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1362
    iput-object p1, p0, Lcom/sec/android/seccamera/SecCamera;->mDramaShotEventListener:Lcom/sec/android/seccamera/SecCamera$DramaShotEventListener;

    .line 1363
    return-void
.end method

.method public setDramaShotMode(I)V
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 6237
    const/16 v0, 0x536

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 6238
    return-void
.end method

.method public setDualEffectCoordinate(II)V
    .locals 1
    .param p1, "currnet"    # I
    .param p2, "destination"    # I

    .prologue
    .line 6244
    const/16 v0, 0x50d

    invoke-virtual {p0, v0, p1, p2}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 6245
    return-void
.end method

.method public setDualEffectLayerOrder(Z)V
    .locals 3
    .param p1, "isRearGoesBottom"    # Z

    .prologue
    const/16 v2, 0x50e

    const/4 v1, 0x0

    .line 6261
    if-eqz p1, :cond_0

    .line 6262
    invoke-virtual {p0, v2, v1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 6265
    :goto_0
    return-void

    .line 6264
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v2, v0, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    goto :goto_0
.end method

.method public setDualEventListener(Lcom/sec/android/seccamera/SecCamera$DualEventListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/seccamera/SecCamera$DualEventListener;

    .prologue
    .line 1513
    iput-object p1, p0, Lcom/sec/android/seccamera/SecCamera;->mDualEventListener:Lcom/sec/android/seccamera/SecCamera$DualEventListener;

    .line 1514
    return-void
.end method

.method public setDualShotMode(I)V
    .locals 2
    .param p1, "isDualSyncMode"    # I

    .prologue
    .line 6284
    const/16 v0, 0x55d

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 6285
    return-void
.end method

.method public setDualTrackingCoordinate(Z)V
    .locals 2
    .param p1, "isDualTrackingCoordi"    # Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 6332
    const-string v0, "SecCamera-JNI-Java"

    const-string v1, "deprecated. replace to enableDualTrackingCoordinate"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 6333
    invoke-virtual {p0, p1}, Lcom/sec/android/seccamera/SecCamera;->enableDualTrackingCoordinate(Z)V

    .line 6334
    return-void
.end method

.method public setDualTrackingMode(Z)V
    .locals 2
    .param p1, "isDualTrackingMode"    # Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 6355
    const-string v0, "SecCamera-JNI-Java"

    const-string v1, "deprecated. replace to enableDualTrackingMode"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 6356
    invoke-virtual {p0, p1}, Lcom/sec/android/seccamera/SecCamera;->enableDualTrackingMode(Z)V

    .line 6357
    return-void
.end method

.method public setEffect(I)V
    .locals 2
    .param p1, "pfilterId"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 6081
    const-string v0, "SecCamera-JNI-Java"

    const-string v1, "deprecated. replace to setSecImagingEffect"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 6082
    invoke-virtual {p0, p1}, Lcom/sec/android/seccamera/SecCamera;->setSecImagingEffect(I)V

    .line 6083
    return-void
.end method

.method public setEffect(Ljava/lang/String;)V
    .locals 2
    .param p1, "str"    # Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 6102
    const-string v0, "SecCamera-JNI-Java"

    const-string v1, "deprecated. replace to setSecImagingEffect"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 6103
    invoke-virtual {p0, p1}, Lcom/sec/android/seccamera/SecCamera;->setSecImagingEffect(Ljava/lang/String;)V

    .line 6104
    return-void
.end method

.method public setEffectCoordinate(II)V
    .locals 2
    .param p1, "currnet"    # I
    .param p2, "destination"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 6253
    const-string v0, "SecCamera-JNI-Java"

    const-string v1, "deprecated. replace to setDualEffectCoordinate"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 6254
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/seccamera/SecCamera;->setDualEffectCoordinate(II)V

    .line 6255
    return-void
.end method

.method public setEffectLayerOrder(Z)V
    .locals 2
    .param p1, "isRearGoesBottom"    # Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 6273
    const-string v0, "SecCamera-JNI-Java"

    const-string v1, "deprecated. replace to setDualEffectLayerOrder"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 6274
    invoke-virtual {p0, p1}, Lcom/sec/android/seccamera/SecCamera;->setDualEffectLayerOrder(Z)V

    .line 6275
    return-void
.end method

.method public setEffectMode(I)V
    .locals 2
    .param p1, "pMode"    # I

    .prologue
    .line 6410
    const/16 v0, 0x607

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 6411
    return-void
.end method

.method public setEffectOrientation(I)V
    .locals 2
    .param p1, "orientation"    # I

    .prologue
    .line 6398
    const/16 v0, 0x510

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 6399
    return-void
.end method

.method public setEffectSaveAsFlipped(I)V
    .locals 2
    .param p1, "isSaveAsFlipped"    # I

    .prologue
    .line 6137
    const/16 v0, 0x512

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 6138
    return-void
.end method

.method public setEffectVisible(Z)V
    .locals 3
    .param p1, "isVisible"    # Z

    .prologue
    const/16 v2, 0x50f

    const/4 v1, 0x0

    .line 6366
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v2, v0, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 6368
    :goto_0
    return-void

    .line 6367
    :cond_0
    invoke-virtual {p0, v2, v1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    goto :goto_0
.end method

.method public setEffectVisibleForRecording(Z)V
    .locals 2
    .param p1, "isVisible"    # Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 6387
    const-string v0, "SecCamera-JNI-Java"

    const-string v1, "deprecated. replace to setSecImagingEffectVisibleForRecording"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 6388
    invoke-virtual {p0, p1}, Lcom/sec/android/seccamera/SecCamera;->setSecImagingEffectVisibleForRecording(Z)V

    .line 6389
    return-void
.end method

.method public final setErrorCallback(Lcom/sec/android/seccamera/SecCamera$ErrorCallback;)V
    .locals 0
    .param p1, "cb"    # Lcom/sec/android/seccamera/SecCamera$ErrorCallback;

    .prologue
    .line 5453
    iput-object p1, p0, Lcom/sec/android/seccamera/SecCamera;->mErrorCallback:Lcom/sec/android/seccamera/SecCamera$ErrorCallback;

    .line 5454
    return-void
.end method

.method public setExternalEffect(Z)V
    .locals 2
    .param p1, "isExternal"    # Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 6127
    const-string v0, "SecCamera-JNI-Java"

    const-string v1, "deprecated. replace to setExternalSecImagingEffect"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 6128
    invoke-virtual {p0, p1}, Lcom/sec/android/seccamera/SecCamera;->setExternalSecImagingEffect(Z)V

    .line 6129
    return-void
.end method

.method public setExternalSecImagingEffect(Z)V
    .locals 3
    .param p1, "isExternal"    # Z

    .prologue
    const/16 v2, 0x511

    const/4 v1, 0x0

    .line 6113
    if-eqz p1, :cond_0

    .line 6114
    const/4 v0, 0x1

    invoke-virtual {p0, v2, v0, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 6119
    :goto_0
    return-void

    .line 6117
    :cond_0
    invoke-virtual {p0, v2, v1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    goto :goto_0
.end method

.method public declared-synchronized setExternalShootingModes(Ljava/util/HashMap;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 5543
    .local p1, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[external_shooting_modes_map]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, p1}, Lcom/sec/android/seccamera/SecCamera;->flatten(Ljava/util/HashMap;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/seccamera/SecCamera;->setGenericParameters(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5544
    monitor-exit p0

    return-void

    .line 5543
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setEyeEnlargeLevel(I)V
    .locals 2
    .param p1, "eyeEnlargeLevel"    # I

    .prologue
    .line 5882
    const/16 v0, 0x4a1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 5883
    return-void
.end method

.method public final setFaceDetectionListener(Lcom/sec/android/seccamera/SecCamera$FaceDetectionListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/seccamera/SecCamera$FaceDetectionListener;

    .prologue
    .line 5186
    iput-object p1, p0, Lcom/sec/android/seccamera/SecCamera;->mFaceListener:Lcom/sec/android/seccamera/SecCamera$FaceDetectionListener;

    .line 5187
    return-void
.end method

.method public setFaceRetouchLevel(I)V
    .locals 2
    .param p1, "retouchLevel"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 5867
    const-string v0, "SecCamera-JNI-Java"

    const-string v1, "deprecated. replace setBeautyLevel with live value. default is false"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 5868
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Lcom/sec/android/seccamera/SecCamera;->setBeautyLevel(ZI)V

    .line 5869
    return-void
.end method

.method public setFrontSensorMirror(ZI)V
    .locals 2
    .param p1, "mirror"    # Z
    .param p2, "orientation"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 6023
    const-string v0, "SecCamera-JNI-Java"

    const-string v1, "deprecated. replace to flipFrontCapturedImage"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 6024
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/seccamera/SecCamera;->flipFrontCapturedImage(ZI)V

    .line 6025
    return-void
.end method

.method public setGenericParameters(Ljava/lang/String;)V
    .locals 4
    .param p1, "params"    # Ljava/lang/String;

    .prologue
    .line 5463
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Generic-Param=1;"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "=0;"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 5464
    .local v0, "mParams":Ljava/lang/String;
    const-string v1, "SecCamera-JNI-Java"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setGenericParameters : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5465
    invoke-direct {p0, v0}, Lcom/sec/android/seccamera/SecCamera;->native_setParameters(Ljava/lang/String;)V

    .line 5466
    return-void
.end method

.method public setGolfShotEventListener(Lcom/sec/android/seccamera/SecCamera$GolfShotEventListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/seccamera/SecCamera$GolfShotEventListener;

    .prologue
    .line 1301
    iput-object p1, p0, Lcom/sec/android/seccamera/SecCamera;->mGolfShotEventListener:Lcom/sec/android/seccamera/SecCamera$GolfShotEventListener;

    .line 1302
    return-void
.end method

.method public setHazeRemovalShotEventListener(Lcom/sec/android/seccamera/SecCamera$HazeRemovalEventListener;)V
    .locals 2
    .param p1, "l"    # Lcom/sec/android/seccamera/SecCamera$HazeRemovalEventListener;

    .prologue
    .line 1261
    const-string v0, "SecCamera-JNI-Java"

    const-string v1, "setHazeRemovalShotEventListener"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1262
    iput-object p1, p0, Lcom/sec/android/seccamera/SecCamera;->mHazeRemovalShotEventListener:Lcom/sec/android/seccamera/SecCamera$HazeRemovalEventListener;

    .line 1263
    return-void
.end method

.method public setHdrEventListener(Lcom/sec/android/seccamera/SecCamera$HdrEventListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/seccamera/SecCamera$HdrEventListener;

    .prologue
    .line 1193
    iput-object p1, p0, Lcom/sec/android/seccamera/SecCamera;->mHdrEventListener:Lcom/sec/android/seccamera/SecCamera$HdrEventListener;

    .line 1194
    return-void
.end method

.method public setHdrSavingMode(I)V
    .locals 2
    .param p1, "picMode"    # I

    .prologue
    .line 5924
    const/16 v0, 0x4f9

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 5925
    return-void
.end method

.method public setLightConditionChangedListener(Lcom/sec/android/seccamera/SecCamera$LightConditionChangedListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/seccamera/SecCamera$LightConditionChangedListener;

    .prologue
    .line 1476
    iput-object p1, p0, Lcom/sec/android/seccamera/SecCamera;->mLightConditionChangedListener:Lcom/sec/android/seccamera/SecCamera$LightConditionChangedListener;

    .line 1477
    return-void
.end method

.method public setLowLightShot(I)V
    .locals 2
    .param p1, "set"    # I

    .prologue
    .line 5834
    const/16 v0, 0x4f0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 5835
    return-void
.end method

.method public setMultiFrameEventListener(Lcom/sec/android/seccamera/SecCamera$MultiFrameEventListener;)V
    .locals 2
    .param p1, "l"    # Lcom/sec/android/seccamera/SecCamera$MultiFrameEventListener;

    .prologue
    .line 1227
    const-string v0, "SecCamera-JNI-Java"

    const-string v1, "setMultiFrameShotEventListener"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1228
    iput-object p1, p0, Lcom/sec/android/seccamera/SecCamera;->mMultiFrameEventListener:Lcom/sec/android/seccamera/SecCamera$MultiFrameEventListener;

    .line 1229
    return-void
.end method

.method public setObjectTrackingMsgListener(Lcom/sec/android/seccamera/SecCamera$OnObjectTrackingMsgListener;)V
    .locals 2
    .param p1, "l"    # Lcom/sec/android/seccamera/SecCamera$OnObjectTrackingMsgListener;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2197
    const-string v0, "SecCamera-JNI-Java"

    const-string v1, "deprecated."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2198
    iput-object p1, p0, Lcom/sec/android/seccamera/SecCamera;->mOnObjectTrackingMsgListener:Lcom/sec/android/seccamera/SecCamera$OnObjectTrackingMsgListener;

    .line 2199
    return-void
.end method

.method public setOnAEResultListener(Lcom/sec/android/seccamera/SecCamera$OnAEResultListener;)V
    .locals 2
    .param p1, "l"    # Lcom/sec/android/seccamera/SecCamera$OnAEResultListener;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 3059
    const-string v0, "SecCamera-JNI-Java"

    const-string v1, "deprecated."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 3060
    iput-object p1, p0, Lcom/sec/android/seccamera/SecCamera;->mOnAEResultListener:Lcom/sec/android/seccamera/SecCamera$OnAEResultListener;

    .line 3061
    return-void
.end method

.method public setOnActionShotEventListener(Lcom/sec/android/seccamera/SecCamera$OnActionShotEventListener;)V
    .locals 2
    .param p1, "l"    # Lcom/sec/android/seccamera/SecCamera$OnActionShotEventListener;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2133
    const-string v0, "SecCamera-JNI-Java"

    const-string v1, "deprecated."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2134
    iput-object p1, p0, Lcom/sec/android/seccamera/SecCamera;->mOnActionShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnActionShotEventListener;

    .line 2135
    return-void
.end method

.method public setOnAutoLowLightDetectionListener(Lcom/sec/android/seccamera/SecCamera$OnAutoLowLightDetectionListener;)V
    .locals 2
    .param p1, "l"    # Lcom/sec/android/seccamera/SecCamera$OnAutoLowLightDetectionListener;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2729
    const-string v0, "SecCamera-JNI-Java"

    const-string v1, "deprecated."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2730
    iput-object p1, p0, Lcom/sec/android/seccamera/SecCamera;->mOnAutoLowLightDetectionListener:Lcom/sec/android/seccamera/SecCamera$OnAutoLowLightDetectionListener;

    .line 2731
    return-void
.end method

.method public setOnBeautyShotEventListener(Lcom/sec/android/seccamera/SecCamera$OnBeautyShotEventListener;)V
    .locals 2
    .param p1, "l"    # Lcom/sec/android/seccamera/SecCamera$OnBeautyShotEventListener;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2649
    const-string v0, "SecCamera-JNI-Java"

    const-string v1, "deprecated."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2650
    iput-object p1, p0, Lcom/sec/android/seccamera/SecCamera;->mOnBeautyShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnBeautyShotEventListener;

    .line 2651
    return-void
.end method

.method public setOnBurstShotEventListener(Lcom/sec/android/seccamera/SecCamera$OnBurstShotEventListener;)V
    .locals 2
    .param p1, "l"    # Lcom/sec/android/seccamera/SecCamera$OnBurstShotEventListener;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2076
    const-string v0, "SecCamera-JNI-Java"

    const-string v1, "deprecated."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2077
    iput-object p1, p0, Lcom/sec/android/seccamera/SecCamera;->mOnBurstShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnBurstShotEventListener;

    .line 2078
    return-void
.end method

.method public setOnCameraCurrentSettingListener(Lcom/sec/android/seccamera/SecCamera$OnCameraCurrentSettingListener;)V
    .locals 2
    .param p1, "l"    # Lcom/sec/android/seccamera/SecCamera$OnCameraCurrentSettingListener;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 3032
    const-string v0, "SecCamera-JNI-Java"

    const-string v1, "deprecated."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 3033
    iput-object p1, p0, Lcom/sec/android/seccamera/SecCamera;->mOnCameraCurrentSettingListener:Lcom/sec/android/seccamera/SecCamera$OnCameraCurrentSettingListener;

    .line 3034
    return-void
.end method

.method public setOnCartoonShotEventListener(Lcom/sec/android/seccamera/SecCamera$OnCartoonShotEventListener;)V
    .locals 2
    .param p1, "l"    # Lcom/sec/android/seccamera/SecCamera$OnCartoonShotEventListener;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2160
    const-string v0, "SecCamera-JNI-Java"

    const-string v1, "deprecated."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2161
    iput-object p1, p0, Lcom/sec/android/seccamera/SecCamera;->mOnCartoonShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnCartoonShotEventListener;

    .line 2162
    return-void
.end method

.method public setOnContinuousShotEventListener(Lcom/sec/android/seccamera/SecCamera$OnContinuousShotEventListener;)V
    .locals 2
    .param p1, "l"    # Lcom/sec/android/seccamera/SecCamera$OnContinuousShotEventListener;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2009
    const-string v0, "SecCamera-JNI-Java"

    const-string v1, "deprecated."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2010
    iput-object p1, p0, Lcom/sec/android/seccamera/SecCamera;->mOnContinuousShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnContinuousShotEventListener;

    .line 2011
    return-void
.end method

.method public setOnDramaShotEventListener(Lcom/sec/android/seccamera/SecCamera$OnDramaShotEventListener;)V
    .locals 2
    .param p1, "l"    # Lcom/sec/android/seccamera/SecCamera$OnDramaShotEventListener;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2611
    const-string v0, "SecCamera-JNI-Java"

    const-string v1, "deprecated."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2612
    iput-object p1, p0, Lcom/sec/android/seccamera/SecCamera;->mOnDramaShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnDramaShotEventListener;

    .line 2613
    return-void
.end method

.method public setOnDualEventListener(Lcom/sec/android/seccamera/SecCamera$OnDualEventListener;)V
    .locals 2
    .param p1, "l"    # Lcom/sec/android/seccamera/SecCamera$OnDualEventListener;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2802
    const-string v0, "SecCamera-JNI-Java"

    const-string v1, "deprecated."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2803
    iput-object p1, p0, Lcom/sec/android/seccamera/SecCamera;->mOnDualEventListener:Lcom/sec/android/seccamera/SecCamera$OnDualEventListener;

    .line 2804
    return-void
.end method

.method public setOnGolfShotEventListener(Lcom/sec/android/seccamera/SecCamera$OnGolfShotEventListener;)V
    .locals 2
    .param p1, "l"    # Lcom/sec/android/seccamera/SecCamera$OnGolfShotEventListener;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2536
    const-string v0, "SecCamera-JNI-Java"

    const-string v1, "deprecated."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2537
    iput-object p1, p0, Lcom/sec/android/seccamera/SecCamera;->mOnGolfShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnGolfShotEventListener;

    .line 2538
    return-void
.end method

.method public setOnHDRShotEventListener(Lcom/sec/android/seccamera/SecCamera$OnHDRShotEventListener;)V
    .locals 2
    .param p1, "l"    # Lcom/sec/android/seccamera/SecCamera$OnHDRShotEventListener;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2368
    const-string v0, "SecCamera-JNI-Java"

    const-string v1, "deprecated."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2369
    iput-object p1, p0, Lcom/sec/android/seccamera/SecCamera;->mOnHDRShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnHDRShotEventListener;

    .line 2370
    return-void
.end method

.method public setOnHazeRemovalShotEventListener(Lcom/sec/android/seccamera/SecCamera$OnHazeRemovalEventListener;)V
    .locals 2
    .param p1, "l"    # Lcom/sec/android/seccamera/SecCamera$OnHazeRemovalEventListener;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2448
    const-string v0, "SecCamera-JNI-Java"

    const-string v1, "deprecated."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2449
    iput-object p1, p0, Lcom/sec/android/seccamera/SecCamera;->mOnHazeRemovalShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnHazeRemovalEventListener;

    .line 2450
    return-void
.end method

.method public setOnLightConditionChangedListener(Lcom/sec/android/seccamera/SecCamera$OnLightConditionChangedListener;)V
    .locals 2
    .param p1, "l"    # Lcom/sec/android/seccamera/SecCamera$OnLightConditionChangedListener;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2756
    const-string v0, "SecCamera-JNI-Java"

    const-string v1, "deprecated."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2757
    iput-object p1, p0, Lcom/sec/android/seccamera/SecCamera;->mOnLightConditionChangedListener:Lcom/sec/android/seccamera/SecCamera$OnLightConditionChangedListener;

    .line 2758
    return-void
.end method

.method public setOnMagicShotModeEventListener(Lcom/sec/android/seccamera/SecCamera$OnMagicShotModeEventListener;)V
    .locals 2
    .param p1, "l"    # Lcom/sec/android/seccamera/SecCamera$OnMagicShotModeEventListener;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2965
    const-string v0, "SecCamera-JNI-Java"

    const-string v1, "deprecated."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2966
    iput-object p1, p0, Lcom/sec/android/seccamera/SecCamera;->mOnMagicShotModeEventListener:Lcom/sec/android/seccamera/SecCamera$OnMagicShotModeEventListener;

    .line 2967
    return-void
.end method

.method public setOnMultiFrameShotEventListener(Lcom/sec/android/seccamera/SecCamera$OnMultiFrameShotEventListener;)V
    .locals 2
    .param p1, "l"    # Lcom/sec/android/seccamera/SecCamera$OnMultiFrameShotEventListener;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2408
    const-string v0, "SecCamera-JNI-Java"

    const-string v1, "deprecated."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2409
    iput-object p1, p0, Lcom/sec/android/seccamera/SecCamera;->mOnMultiFrameShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnMultiFrameShotEventListener;

    .line 2410
    return-void
.end method

.method public setOnNotifyFirstPreviewFrameEventListener(Lcom/sec/android/seccamera/SecCamera$OnNotifyFirstPreviewFrameEventListener;)V
    .locals 2
    .param p1, "l"    # Lcom/sec/android/seccamera/SecCamera$OnNotifyFirstPreviewFrameEventListener;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2480
    const-string v0, "SecCamera-JNI-Java"

    const-string v1, "deprecated."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2481
    iput-object p1, p0, Lcom/sec/android/seccamera/SecCamera;->mOnNotifyFirstPreviewFrameEventListener:Lcom/sec/android/seccamera/SecCamera$OnNotifyFirstPreviewFrameEventListener;

    .line 2482
    return-void
.end method

.method public setOnOutFocusShotModeEventListener(Lcom/sec/android/seccamera/SecCamera$OnOutFocusShotModeEventListener;)V
    .locals 2
    .param p1, "l"    # Lcom/sec/android/seccamera/SecCamera$OnOutFocusShotModeEventListener;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2898
    const-string v0, "SecCamera-JNI-Java"

    const-string v1, "deprecated."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2899
    iput-object p1, p0, Lcom/sec/android/seccamera/SecCamera;->mOnOutFocusShotModeEventListener:Lcom/sec/android/seccamera/SecCamera$OnOutFocusShotModeEventListener;

    .line 2900
    return-void
.end method

.method public setOnPafResultListener(Lcom/sec/android/seccamera/SecCamera$OnPafResultListener;)V
    .locals 2
    .param p1, "l"    # Lcom/sec/android/seccamera/SecCamera$OnPafResultListener;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 3005
    const-string v0, "SecCamera-JNI-Java"

    const-string v1, "deprecated."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 3006
    iput-object p1, p0, Lcom/sec/android/seccamera/SecCamera;->mOnPafResultListener:Lcom/sec/android/seccamera/SecCamera$OnPafResultListener;

    .line 3007
    return-void
.end method

.method public setOnPanoramaEventListener(Lcom/sec/android/seccamera/SecCamera$OnPanoramaEventListener;)V
    .locals 2
    .param p1, "l"    # Lcom/sec/android/seccamera/SecCamera$OnPanoramaEventListener;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1844
    const-string v0, "SecCamera-JNI-Java"

    const-string v1, "deprecated."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1845
    iput-object p1, p0, Lcom/sec/android/seccamera/SecCamera;->mOnPanoramaEventListener:Lcom/sec/android/seccamera/SecCamera$OnPanoramaEventListener;

    .line 1846
    return-void
.end method

.method public setOnSecImageEffectListner(Lcom/sec/android/seccamera/SecCamera$OnSecImageEffectListner;)V
    .locals 2
    .param p1, "l"    # Lcom/sec/android/seccamera/SecCamera$OnSecImageEffectListner;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2702
    const-string v0, "SecCamera-JNI-Java"

    const-string v1, "deprecated."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2703
    iput-object p1, p0, Lcom/sec/android/seccamera/SecCamera;->mOnSecImageEffectListner:Lcom/sec/android/seccamera/SecCamera$OnSecImageEffectListner;

    .line 2704
    return-void
.end method

.method public setOnSecImagingEventListener(Lcom/sec/android/seccamera/SecCamera$OnSecImagingEventListener;)V
    .locals 2
    .param p1, "l"    # Lcom/sec/android/seccamera/SecCamera$OnSecImagingEventListener;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2839
    const-string v0, "SecCamera-JNI-Java"

    const-string v1, "deprecated."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2840
    iput-object p1, p0, Lcom/sec/android/seccamera/SecCamera;->mOnSecImagingEventListener:Lcom/sec/android/seccamera/SecCamera$OnSecImagingEventListener;

    .line 2841
    return-void
.end method

.method public setOnSelfieShotDetectionSuccessListener(Lcom/sec/android/seccamera/SecCamera$OnSelfieShotDetectionSuccessListener;)V
    .locals 2
    .param p1, "l"    # Lcom/sec/android/seccamera/SecCamera$OnSelfieShotDetectionSuccessListener;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2292
    const-string v0, "SecCamera-JNI-Java"

    const-string v1, "deprecated."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2293
    iput-object p1, p0, Lcom/sec/android/seccamera/SecCamera;->mOnSelfieShotDetectionSuccessListener:Lcom/sec/android/seccamera/SecCamera$OnSelfieShotDetectionSuccessListener;

    .line 2294
    return-void
.end method

.method public setOnSmileShotDetectionSuccessListener(Lcom/sec/android/seccamera/SecCamera$OnSmileShotDetectionSuccessListener;)V
    .locals 2
    .param p1, "l"    # Lcom/sec/android/seccamera/SecCamera$OnSmileShotDetectionSuccessListener;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 2247
    const-string v0, "SecCamera-JNI-Java"

    const-string v1, "deprecated."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2248
    iput-object p1, p0, Lcom/sec/android/seccamera/SecCamera;->mOnSmileShotDetectionSuccessListener:Lcom/sec/android/seccamera/SecCamera$OnSmileShotDetectionSuccessListener;

    .line 2249
    return-void
.end method

.method public setOnUltraWideSelfieEventListener(Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;)V
    .locals 2
    .param p1, "l"    # Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1952
    const-string v0, "SecCamera-JNI-Java"

    const-string v1, "deprecated."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1953
    iput-object p1, p0, Lcom/sec/android/seccamera/SecCamera;->mOnUltraWideSelfieEventListener:Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;

    .line 1954
    return-void
.end method

.method public final setOneShotPreviewCallback(Lcom/sec/android/seccamera/SecCamera$PreviewCallback;)V
    .locals 2
    .param p1, "cb"    # Lcom/sec/android/seccamera/SecCamera$PreviewCallback;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3614
    iput-object p1, p0, Lcom/sec/android/seccamera/SecCamera;->mPreviewCallback:Lcom/sec/android/seccamera/SecCamera$PreviewCallback;

    .line 3615
    iput-boolean v0, p0, Lcom/sec/android/seccamera/SecCamera;->mOneShot:Z

    .line 3616
    iput-boolean v1, p0, Lcom/sec/android/seccamera/SecCamera;->mWithBuffer:Z

    .line 3617
    if-eqz p1, :cond_0

    .line 3618
    iput-boolean v1, p0, Lcom/sec/android/seccamera/SecCamera;->mUsingPreviewAllocation:Z

    .line 3620
    :cond_0
    if-eqz p1, :cond_1

    :goto_0
    invoke-direct {p0, v0, v1, v1}, Lcom/sec/android/seccamera/SecCamera;->setHasPreviewCallback(ZZZ)V

    .line 3621
    return-void

    :cond_1
    move v0, v1

    .line 3620
    goto :goto_0
.end method

.method public setOutputFile(Ljava/lang/String;)V
    .locals 2
    .param p1, "filepath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 6535
    new-instance v0, Ljava/io/RandomAccessFile;

    const-string v1, "rw"

    invoke-direct {v0, p1, v1}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 6537
    .local v0, "fos":Ljava/io/RandomAccessFile;
    :try_start_0
    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->getFD()Ljava/io/FileDescriptor;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/android/seccamera/SecCamera;->_setOutputFile(Ljava/io/FileDescriptor;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6539
    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->close()V

    .line 6541
    return-void

    .line 6539
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->close()V

    throw v1
.end method

.method public setOutputFileArray([Ljava/lang/String;)V
    .locals 9
    .param p1, "filepathArray"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 6550
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 6551
    .local v3, "fosArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/RandomAccessFile;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 6553
    .local v2, "fdArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/FileDescriptor;>;"
    array-length v5, p1

    .line 6556
    .local v5, "sizeOfArray":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-ge v4, v5, :cond_0

    .line 6557
    new-instance v6, Ljava/io/RandomAccessFile;

    aget-object v7, p1, v4

    const-string v8, "rw"

    invoke-direct {v6, v7, v8}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 6559
    :try_start_0
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/io/RandomAccessFile;

    invoke-virtual {v6}, Ljava/io/RandomAccessFile;->getFD()Ljava/io/FileDescriptor;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 6556
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 6560
    :catch_0
    move-exception v0

    .line 6561
    .local v0, "e":Ljava/lang/Exception;
    const-string v6, "SecCamera-JNI-Java"

    const-string v7, "setOutputFileArray :"

    invoke-static {v6, v7, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 6562
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/io/RandomAccessFile;

    invoke-virtual {v6}, Ljava/io/RandomAccessFile;->close()V

    goto :goto_1

    .line 6566
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v6

    new-array v1, v6, [Ljava/io/FileDescriptor;

    .line 6567
    .local v1, "fdArray":[Ljava/io/FileDescriptor;
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Ljava/io/FileDescriptor;

    move-object v1, v6

    check-cast v1, [Ljava/io/FileDescriptor;

    .line 6568
    invoke-direct {p0, v1, v5}, Lcom/sec/android/seccamera/SecCamera;->_setOutputFileArray([Ljava/io/FileDescriptor;I)V

    .line 6570
    const/4 v4, 0x0

    :goto_2
    if-ge v4, v5, :cond_1

    .line 6571
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/io/RandomAccessFile;

    invoke-virtual {v6}, Ljava/io/RandomAccessFile;->close()V

    .line 6570
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 6573
    :cond_1
    return-void
.end method

.method public setPanoramaEventListener(Lcom/sec/android/seccamera/SecCamera$PanoramaEventListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/seccamera/SecCamera$PanoramaEventListener;

    .prologue
    .line 980
    iput-object p1, p0, Lcom/sec/android/seccamera/SecCamera;->mPanoramaEventListener:Lcom/sec/android/seccamera/SecCamera$PanoramaEventListener;

    .line 981
    return-void
.end method

.method public setPanoramaThumbnailSize(II)V
    .locals 1
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 5651
    const/16 v0, 0x456

    invoke-virtual {p0, v0, p1, p2}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 5652
    return-void
.end method

.method public declared-synchronized setParameters(Lcom/sec/android/seccamera/SecCamera$Parameters;)V
    .locals 4
    .param p1, "params"    # Lcom/sec/android/seccamera/SecCamera$Parameters;

    .prologue
    .line 5477
    monitor-enter p0

    :try_start_0
    iget-boolean v2, p0, Lcom/sec/android/seccamera/SecCamera;->mUsingPreviewAllocation:Z

    if-eqz v2, :cond_1

    .line 5478
    invoke-virtual {p1}, Lcom/sec/android/seccamera/SecCamera$Parameters;->getPreviewSize()Lcom/sec/android/seccamera/SecCamera$Size;

    move-result-object v1

    .line 5479
    .local v1, "newPreviewSize":Lcom/sec/android/seccamera/SecCamera$Size;
    invoke-virtual {p0}, Lcom/sec/android/seccamera/SecCamera;->getParameters()Lcom/sec/android/seccamera/SecCamera$Parameters;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/seccamera/SecCamera$Parameters;->getPreviewSize()Lcom/sec/android/seccamera/SecCamera$Size;

    move-result-object v0

    .line 5480
    .local v0, "currentPreviewSize":Lcom/sec/android/seccamera/SecCamera$Size;
    iget v2, v1, Lcom/sec/android/seccamera/SecCamera$Size;->width:I

    iget v3, v0, Lcom/sec/android/seccamera/SecCamera$Size;->width:I

    if-ne v2, v3, :cond_0

    iget v2, v1, Lcom/sec/android/seccamera/SecCamera$Size;->height:I

    iget v3, v0, Lcom/sec/android/seccamera/SecCamera$Size;->height:I

    if-eq v2, v3, :cond_1

    .line 5482
    :cond_0
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Cannot change preview size while a preview allocation is configured."

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5477
    .end local v0    # "currentPreviewSize":Lcom/sec/android/seccamera/SecCamera$Size;
    .end local v1    # "newPreviewSize":Lcom/sec/android/seccamera/SecCamera$Size;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 5487
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Lcom/sec/android/seccamera/SecCamera$Parameters;->flatten()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/sec/android/seccamera/SecCamera;->native_setParameters(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 5488
    monitor-exit p0

    return-void
.end method

.method public setPhaseAFCallback(Lcom/sec/android/seccamera/SecCamera$PhaseAFCallback;)V
    .locals 3
    .param p1, "cb"    # Lcom/sec/android/seccamera/SecCamera$PhaseAFCallback;

    .prologue
    const/16 v2, 0x66c

    const/4 v1, 0x0

    .line 1675
    if-eqz p1, :cond_0

    .line 1676
    iput-object p1, p0, Lcom/sec/android/seccamera/SecCamera;->mPhaseAFCallback:Lcom/sec/android/seccamera/SecCamera$PhaseAFCallback;

    .line 1677
    const/4 v0, 0x1

    invoke-virtual {p0, v2, v0, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 1682
    :goto_0
    return-void

    .line 1679
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mPhaseAFCallback:Lcom/sec/android/seccamera/SecCamera$PhaseAFCallback;

    .line 1680
    invoke-virtual {p0, v2, v1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    goto :goto_0
.end method

.method public setPictureMode(I)V
    .locals 2
    .param p1, "picMode"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 5933
    const-string v0, "SecCamera-JNI-Java"

    const-string v1, "deprecated. replace to setHdrSavingMode"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 5934
    invoke-virtual {p0, p1}, Lcom/sec/android/seccamera/SecCamera;->setHdrSavingMode(I)V

    .line 5935
    return-void
.end method

.method public final setPreviewCallback(Lcom/sec/android/seccamera/SecCamera$PreviewCallback;)V
    .locals 2
    .param p1, "cb"    # Lcom/sec/android/seccamera/SecCamera$PreviewCallback;

    .prologue
    const/4 v1, 0x0

    .line 3588
    iput-object p1, p0, Lcom/sec/android/seccamera/SecCamera;->mPreviewCallback:Lcom/sec/android/seccamera/SecCamera$PreviewCallback;

    .line 3589
    iput-boolean v1, p0, Lcom/sec/android/seccamera/SecCamera;->mOneShot:Z

    .line 3590
    iput-boolean v1, p0, Lcom/sec/android/seccamera/SecCamera;->mWithBuffer:Z

    .line 3591
    if-eqz p1, :cond_0

    .line 3592
    iput-boolean v1, p0, Lcom/sec/android/seccamera/SecCamera;->mUsingPreviewAllocation:Z

    .line 3596
    :cond_0
    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0, v1, v1}, Lcom/sec/android/seccamera/SecCamera;->setHasPreviewCallback(ZZZ)V

    .line 3597
    return-void

    :cond_1
    move v0, v1

    .line 3596
    goto :goto_0
.end method

.method public final setPreviewCallbackAllocation(Landroid/renderscript/Allocation;)V
    .locals 6
    .param p1, "previewAllocation"    # Landroid/renderscript/Allocation;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3861
    const/4 v2, 0x0

    .line 3862
    .local v2, "previewSurface":Landroid/view/Surface;
    if-eqz p1, :cond_4

    .line 3863
    invoke-virtual {p0}, Lcom/sec/android/seccamera/SecCamera;->getParameters()Lcom/sec/android/seccamera/SecCamera$Parameters;

    move-result-object v0

    .line 3864
    .local v0, "p":Lcom/sec/android/seccamera/SecCamera$Parameters;
    invoke-virtual {v0}, Lcom/sec/android/seccamera/SecCamera$Parameters;->getPreviewSize()Lcom/sec/android/seccamera/SecCamera$Size;

    move-result-object v1

    .line 3865
    .local v1, "previewSize":Lcom/sec/android/seccamera/SecCamera$Size;
    iget v3, v1, Lcom/sec/android/seccamera/SecCamera$Size;->width:I

    invoke-virtual {p1}, Landroid/renderscript/Allocation;->getType()Landroid/renderscript/Type;

    move-result-object v4

    invoke-virtual {v4}, Landroid/renderscript/Type;->getX()I

    move-result v4

    if-ne v3, v4, :cond_0

    iget v3, v1, Lcom/sec/android/seccamera/SecCamera$Size;->height:I

    invoke-virtual {p1}, Landroid/renderscript/Allocation;->getType()Landroid/renderscript/Type;

    move-result-object v4

    invoke-virtual {v4}, Landroid/renderscript/Type;->getY()I

    move-result v4

    if-eq v3, v4, :cond_1

    .line 3867
    :cond_0
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Allocation dimensions don\'t match preview dimensions: Allocation is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Landroid/renderscript/Allocation;->getType()Landroid/renderscript/Type;

    move-result-object v5

    invoke-virtual {v5}, Landroid/renderscript/Type;->getX()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Landroid/renderscript/Allocation;->getType()Landroid/renderscript/Type;

    move-result-object v5

    invoke-virtual {v5}, Landroid/renderscript/Type;->getY()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ". Preview is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v1, Lcom/sec/android/seccamera/SecCamera$Size;->width:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v1, Lcom/sec/android/seccamera/SecCamera$Size;->height:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 3876
    :cond_1
    invoke-virtual {p1}, Landroid/renderscript/Allocation;->getUsage()I

    move-result v3

    and-int/lit8 v3, v3, 0x20

    if-nez v3, :cond_2

    .line 3878
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Allocation usage does not include USAGE_IO_INPUT"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 3881
    :cond_2
    invoke-virtual {p1}, Landroid/renderscript/Allocation;->getType()Landroid/renderscript/Type;

    move-result-object v3

    invoke-virtual {v3}, Landroid/renderscript/Type;->getElement()Landroid/renderscript/Element;

    move-result-object v3

    invoke-virtual {v3}, Landroid/renderscript/Element;->getDataKind()Landroid/renderscript/Element$DataKind;

    move-result-object v3

    sget-object v4, Landroid/renderscript/Element$DataKind;->PIXEL_YUV:Landroid/renderscript/Element$DataKind;

    if-eq v3, v4, :cond_3

    .line 3883
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Allocation is not of a YUV type"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 3886
    :cond_3
    invoke-virtual {p1}, Landroid/renderscript/Allocation;->getSurface()Landroid/view/Surface;

    move-result-object v2

    .line 3887
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/sec/android/seccamera/SecCamera;->mUsingPreviewAllocation:Z

    .line 3891
    .end local v0    # "p":Lcom/sec/android/seccamera/SecCamera$Parameters;
    .end local v1    # "previewSize":Lcom/sec/android/seccamera/SecCamera$Size;
    :goto_0
    invoke-direct {p0, v2}, Lcom/sec/android/seccamera/SecCamera;->setPreviewCallbackSurface(Landroid/view/Surface;)V

    .line 3892
    return-void

    .line 3889
    :cond_4
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/sec/android/seccamera/SecCamera;->mUsingPreviewAllocation:Z

    goto :goto_0
.end method

.method public setPreviewCallbackSize(II)V
    .locals 1
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 6434
    const/16 v0, 0x565

    invoke-virtual {p0, v0, p1, p2}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 6435
    return-void
.end method

.method public final setPreviewCallbackWithBuffer(Lcom/sec/android/seccamera/SecCamera$PreviewCallback;)V
    .locals 3
    .param p1, "cb"    # Lcom/sec/android/seccamera/SecCamera$PreviewCallback;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 3653
    iput-object p1, p0, Lcom/sec/android/seccamera/SecCamera;->mPreviewCallback:Lcom/sec/android/seccamera/SecCamera$PreviewCallback;

    .line 3654
    iput-boolean v2, p0, Lcom/sec/android/seccamera/SecCamera;->mOneShot:Z

    .line 3655
    iput-boolean v1, p0, Lcom/sec/android/seccamera/SecCamera;->mWithBuffer:Z

    .line 3656
    if-eqz p1, :cond_0

    .line 3657
    iput-boolean v2, p0, Lcom/sec/android/seccamera/SecCamera;->mUsingPreviewAllocation:Z

    .line 3659
    :cond_0
    if-eqz p1, :cond_1

    move v0, v1

    :goto_0
    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/seccamera/SecCamera;->setHasPreviewCallback(ZZZ)V

    .line 3660
    return-void

    :cond_1
    move v0, v2

    .line 3659
    goto :goto_0
.end method

.method public final setPreviewCallbackWithBufferNoDisable(Lcom/sec/android/seccamera/SecCamera$PreviewCallback;)V
    .locals 2
    .param p1, "cb"    # Lcom/sec/android/seccamera/SecCamera$PreviewCallback;

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 3666
    iput-object p1, p0, Lcom/sec/android/seccamera/SecCamera;->mPreviewCallback:Lcom/sec/android/seccamera/SecCamera$PreviewCallback;

    .line 3667
    iput-boolean v0, p0, Lcom/sec/android/seccamera/SecCamera;->mOneShot:Z

    .line 3668
    iput-boolean v1, p0, Lcom/sec/android/seccamera/SecCamera;->mWithBuffer:Z

    .line 3669
    if-eqz p1, :cond_0

    move v0, v1

    :cond_0
    invoke-direct {p0, v0, v1, v1}, Lcom/sec/android/seccamera/SecCamera;->setHasPreviewCallback(ZZZ)V

    .line 3670
    return-void
.end method

.method public final setPreviewDisplay(Landroid/view/SurfaceHolder;)V
    .locals 1
    .param p1, "holder"    # Landroid/view/SurfaceHolder;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3448
    if-eqz p1, :cond_0

    .line 3449
    invoke-interface {p1}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/seccamera/SecCamera;->setPreviewSurface(Landroid/view/Surface;)V

    .line 3453
    :goto_0
    return-void

    .line 3451
    :cond_0
    const/4 v0, 0x0

    check-cast v0, Landroid/view/Surface;

    invoke-virtual {p0, v0}, Lcom/sec/android/seccamera/SecCamera;->setPreviewSurface(Landroid/view/Surface;)V

    goto :goto_0
.end method

.method public final native setPreviewSurface(Landroid/view/Surface;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public final native setPreviewTexture(Landroid/graphics/SurfaceTexture;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public setResolutionActionShot(II)V
    .locals 1
    .param p1, "width"    # I
    .param p2, "height"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 5788
    const/16 v0, 0x46c

    invoke-virtual {p0, v0, p1, p2}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 5789
    return-void
.end method

.method public setSecImageEffectListner(Lcom/sec/android/seccamera/SecCamera$SecImageEffectListner;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/seccamera/SecCamera$SecImageEffectListner;

    .prologue
    .line 1436
    iput-object p1, p0, Lcom/sec/android/seccamera/SecCamera;->mSecImageEffectListner:Lcom/sec/android/seccamera/SecCamera$SecImageEffectListner;

    .line 1437
    return-void
.end method

.method public setSecImagingEffect(I)V
    .locals 2
    .param p1, "pfilterId"    # I

    .prologue
    .line 6072
    const/16 v0, 0x50b

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 6073
    return-void
.end method

.method public setSecImagingEffect(Ljava/lang/String;)V
    .locals 0
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 6093
    invoke-virtual {p0, p1}, Lcom/sec/android/seccamera/SecCamera;->setGenericParameters(Ljava/lang/String;)V

    .line 6094
    return-void
.end method

.method public setSecImagingEffectVisibleForRecording(Z)V
    .locals 3
    .param p1, "isVisible"    # Z

    .prologue
    const/16 v2, 0x548

    const/4 v1, 0x0

    .line 6377
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v2, v0, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 6379
    :goto_0
    return-void

    .line 6378
    :cond_0
    invoke-virtual {p0, v2, v1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    goto :goto_0
.end method

.method public setSecImagingEventListener(Lcom/sec/android/seccamera/SecCamera$SecImagingEventListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/seccamera/SecCamera$SecImagingEventListener;

    .prologue
    .line 1543
    iput-object p1, p0, Lcom/sec/android/seccamera/SecCamera;->mSecImagingEventListener:Lcom/sec/android/seccamera/SecCamera$SecImagingEventListener;

    .line 1544
    return-void
.end method

.method public setSelectiveFocusEventListener(Lcom/sec/android/seccamera/SecCamera$SelectiveFocusEventListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/seccamera/SecCamera$SelectiveFocusEventListener;

    .prologue
    .line 1591
    iput-object p1, p0, Lcom/sec/android/seccamera/SecCamera;->mSelectiveFocusEventListener:Lcom/sec/android/seccamera/SecCamera$SelectiveFocusEventListener;

    .line 1592
    return-void
.end method

.method public setSelfieDetectionListener(Lcom/sec/android/seccamera/SecCamera$SelfieDetectionListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/seccamera/SecCamera$SelfieDetectionListener;

    .prologue
    .line 1132
    iput-object p1, p0, Lcom/sec/android/seccamera/SecCamera;->mSelfieDetectionListener:Lcom/sec/android/seccamera/SecCamera$SelfieDetectionListener;

    .line 1133
    return-void
.end method

.method public setSelfieMode(I)V
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 6194
    const/16 v0, 0x541

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 6195
    return-void
.end method

.method public setShootingMode(I)V
    .locals 2
    .param p1, "shootingMode"    # I

    .prologue
    const/4 v1, 0x0

    .line 5513
    add-int/lit16 v0, p1, 0x3e8

    invoke-virtual {p0, v0, v1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 5514
    return-void
.end method

.method public setShootingMode(III)V
    .locals 1
    .param p1, "shootingMode"    # I
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I

    .prologue
    .line 5530
    add-int/lit16 v0, p1, 0x3e8

    invoke-virtual {p0, v0, p2, p3}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 5531
    return-void
.end method

.method public setShootingModeCallbacks(Lcom/sec/android/seccamera/SecCamera$ShutterCallback;Lcom/sec/android/seccamera/SecCamera$PictureCallback;Lcom/sec/android/seccamera/SecCamera$PictureCallback;)V
    .locals 1
    .param p1, "shutter"    # Lcom/sec/android/seccamera/SecCamera$ShutterCallback;
    .param p2, "raw"    # Lcom/sec/android/seccamera/SecCamera$PictureCallback;
    .param p3, "jpeg"    # Lcom/sec/android/seccamera/SecCamera$PictureCallback;

    .prologue
    .line 5575
    iput-object p1, p0, Lcom/sec/android/seccamera/SecCamera;->mShutterCallback:Lcom/sec/android/seccamera/SecCamera$ShutterCallback;

    .line 5576
    iput-object p2, p0, Lcom/sec/android/seccamera/SecCamera;->mRawImageCallback:Lcom/sec/android/seccamera/SecCamera$PictureCallback;

    .line 5577
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mPostviewCallback:Lcom/sec/android/seccamera/SecCamera$PictureCallback;

    .line 5578
    iput-object p3, p0, Lcom/sec/android/seccamera/SecCamera;->mJpegCallback:Lcom/sec/android/seccamera/SecCamera$PictureCallback;

    .line 5579
    return-void
.end method

.method public setShotAndMoreEventListener(Lcom/sec/android/seccamera/SecCamera$ShotAndMoreEventListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/seccamera/SecCamera$ShotAndMoreEventListener;

    .prologue
    .line 1645
    iput-object p1, p0, Lcom/sec/android/seccamera/SecCamera;->mShotAndMoreEventListener:Lcom/sec/android/seccamera/SecCamera$ShotAndMoreEventListener;

    .line 1646
    return-void
.end method

.method public setShutterSoundEnable(Z)V
    .locals 3
    .param p1, "mode"    # Z

    .prologue
    const/16 v2, 0x605

    const/4 v1, 0x0

    .line 5553
    if-eqz p1, :cond_0

    .line 5554
    const/4 v0, 0x1

    invoke-virtual {p0, v2, v0, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 5557
    :goto_0
    return-void

    .line 5556
    :cond_0
    invoke-virtual {p0, v2, v1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    goto :goto_0
.end method

.method public setShutterSoundVolumeLevel(I)V
    .locals 2
    .param p1, "Level"    # I

    .prologue
    .line 5567
    const/16 v0, 0x606

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 5568
    return-void
.end method

.method public setSlimFaceLevel(I)V
    .locals 2
    .param p1, "slimFaceLevel"    # I

    .prologue
    .line 5875
    const/16 v0, 0x4a0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 5876
    return-void
.end method

.method public setSmartSelfieBeautyLevel(I)V
    .locals 2
    .param p1, "nBeautyLevel"    # I

    .prologue
    .line 5889
    const/16 v0, 0x572

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 5890
    return-void
.end method

.method public setSuperResolutionMode(I)V
    .locals 2
    .param p1, "set"    # I

    .prologue
    .line 6638
    const/16 v0, 0x4f1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 6639
    return-void
.end method

.method public setThemeMask(I)V
    .locals 2
    .param p1, "mask"    # I

    .prologue
    .line 6175
    const/16 v0, 0x53d

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 6176
    return-void
.end method

.method public setThemeOrientationInfo(II)V
    .locals 1
    .param p1, "orientation"    # I
    .param p2, "cameraid"    # I

    .prologue
    .line 6187
    const/16 v0, 0x53e

    invoke-virtual {p0, v0, p1, p2}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 6188
    return-void
.end method

.method public setUltraWideSelfieBeautyLevel(I)V
    .locals 2
    .param p1, "nBeautyLevel"    # I

    .prologue
    .line 6500
    const/16 v0, 0x5bc

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 6501
    return-void
.end method

.method public setUltraWideSelfieCaptureAngle(I)V
    .locals 2
    .param p1, "nAngle"    # I

    .prologue
    .line 6493
    const/16 v0, 0x5bb

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 6494
    return-void
.end method

.method public setUltraWideSelfieEventListener(Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;

    .prologue
    .line 1047
    iput-object p1, p0, Lcom/sec/android/seccamera/SecCamera;->mUltraWideSelfieEventListener:Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;

    .line 1048
    return-void
.end method

.method public setUsePreviewCallback(ZI)V
    .locals 2
    .param p1, "enable"    # Z
    .param p2, "format"    # I

    .prologue
    const/16 v1, 0x566

    .line 6446
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v1, v0, p2}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 6448
    :goto_0
    return-void

    .line 6447
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v1, v0, p2}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    goto :goto_0
.end method

.method public final setZoomChangeListener(Lcom/sec/android/seccamera/SecCamera$OnZoomChangeListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/seccamera/SecCamera$OnZoomChangeListener;

    .prologue
    .line 5159
    iput-object p1, p0, Lcom/sec/android/seccamera/SecCamera;->mZoomListener:Lcom/sec/android/seccamera/SecCamera$OnZoomChangeListener;

    .line 5160
    return-void
.end method

.method public smileDetectionReinit()V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 5607
    const/16 v0, 0x44f

    invoke-virtual {p0, v0, v1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 5608
    return-void
.end method

.method public startAnimatedPhoto()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 5985
    const-string v0, "SecCamera-JNI-Java"

    const-string v1, "startAnimatedPhoto"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 5986
    const/16 v0, 0x657

    invoke-virtual {p0, v0, v2, v2}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 5987
    return-void
.end method

.method public startBurstShot(ZZI)V
    .locals 4
    .param p1, "start"    # Z
    .param p2, "bestpic"    # Z
    .param p3, "storage"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/16 v3, 0x48b

    const/4 v2, 0x0

    .line 5714
    const-string v0, "SecCamera-JNI-Java"

    const-string v1, "deprecated. replace to setBurstShot"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 5715
    if-eqz p1, :cond_1

    .line 5716
    if-eqz p2, :cond_0

    .line 5717
    const/4 v0, 0x1

    invoke-virtual {p0, v3, v0, v2}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 5723
    :goto_0
    return-void

    .line 5719
    :cond_0
    invoke-virtual {p0, v3, v2, p3}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    goto :goto_0

    .line 5721
    :cond_1
    const/16 v0, 0x48c

    invoke-virtual {p0, v0, v2, v2}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    goto :goto_0
.end method

.method public startContinuousAF()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 6053
    const/16 v0, 0x60f

    invoke-virtual {p0, v0, v1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 6054
    return-void
.end method

.method public startContinuousShot(Z)V
    .locals 2
    .param p1, "start"    # Z

    .prologue
    const/4 v1, 0x0

    .line 5671
    if-eqz p1, :cond_0

    .line 5672
    const/16 v0, 0x461

    invoke-virtual {p0, v0, v1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 5676
    :goto_0
    return-void

    .line 5674
    :cond_0
    const/16 v0, 0x462

    invoke-virtual {p0, v0, v1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    goto :goto_0
.end method

.method public startDramaShot()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 6211
    const/16 v0, 0x533

    invoke-virtual {p0, v0, v1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 6212
    return-void
.end method

.method public startDualModeAsyncShot(Z)V
    .locals 2
    .param p1, "isCapture"    # Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 6308
    const-string v0, "SecCamera-JNI-Java"

    const-string v1, "deprecated. replace to takeDualModeAsyncShot"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 6309
    invoke-virtual {p0, p1}, Lcom/sec/android/seccamera/SecCamera;->takeDualModeAsyncShot(Z)V

    .line 6310
    return-void
.end method

.method public startEffectRecording()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 5956
    const/16 v0, 0x655

    invoke-virtual {p0, v0, v1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 5957
    return-void
.end method

.method public final startFaceDetection()V
    .locals 2

    .prologue
    .line 5222
    iget-boolean v0, p0, Lcom/sec/android/seccamera/SecCamera;->mFaceDetectionRunning:Z

    if-eqz v0, :cond_0

    .line 5223
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Face detection is already running"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 5225
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/seccamera/SecCamera;->_startFaceDetection(I)V

    .line 5226
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/seccamera/SecCamera;->mFaceDetectionRunning:Z

    .line 5227
    return-void
.end method

.method public final startFaceDetectionSW()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 5241
    iget-boolean v0, p0, Lcom/sec/android/seccamera/SecCamera;->mFaceDetectionRunning:Z

    if-eqz v0, :cond_0

    .line 5242
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Face detection is already running"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 5244
    :cond_0
    invoke-direct {p0, v1}, Lcom/sec/android/seccamera/SecCamera;->_startFaceDetection(I)V

    .line 5245
    iput-boolean v1, p0, Lcom/sec/android/seccamera/SecCamera;->mFaceDetectionRunning:Z

    .line 5246
    return-void
.end method

.method public final startFaceDetectionSW_ForVoiceGuide()V
    .locals 2

    .prologue
    .line 5277
    iget-boolean v0, p0, Lcom/sec/android/seccamera/SecCamera;->mFaceDetectionRunning:Z

    if-eqz v0, :cond_0

    .line 5278
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Face detection is already running"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 5280
    :cond_0
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/sec/android/seccamera/SecCamera;->_startFaceDetection(I)V

    .line 5281
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/seccamera/SecCamera;->mFaceDetectionRunning:Z

    .line 5282
    return-void
.end method

.method public startFaceZoom(II)V
    .locals 1
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 6037
    const/16 v0, 0x5fb

    invoke-virtual {p0, v0, p1, p2}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 6038
    return-void
.end method

.method public startGolfShot()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 6144
    const/16 v0, 0x51f

    invoke-virtual {p0, v0, v1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 6145
    return-void
.end method

.method public startGolfShot(I)V
    .locals 2
    .param p1, "direction"    # I

    .prologue
    .line 6151
    const/16 v0, 0x51f

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 6152
    return-void
.end method

.method public startMagicShot()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 6457
    const/16 v0, 0x58d

    invoke-virtual {p0, v0, v1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 6458
    return-void
.end method

.method public startMultiFrameShot()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 5842
    const/16 v0, 0x4ed

    invoke-virtual {p0, v0, v1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 5843
    return-void
.end method

.method public startPanorama(Z)V
    .locals 2
    .param p1, "start"    # Z

    .prologue
    const/4 v1, 0x0

    .line 5616
    if-eqz p1, :cond_0

    .line 5617
    const/16 v0, 0x457

    invoke-virtual {p0, v0, v1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 5621
    :goto_0
    return-void

    .line 5619
    :cond_0
    const/16 v0, 0x458

    invoke-virtual {p0, v0, v1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    goto :goto_0
.end method

.method public final native startPreview()V
.end method

.method public final startSamsungFaceDetectionSW()V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 5262
    iget-boolean v0, p0, Lcom/sec/android/seccamera/SecCamera;->mFaceDetectionRunning:Z

    if-eqz v0, :cond_0

    .line 5263
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Face detection is already running"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 5265
    :cond_0
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/sec/android/seccamera/SecCamera;->_startFaceDetection(I)V

    .line 5266
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/seccamera/SecCamera;->mFaceDetectionRunning:Z

    .line 5267
    return-void
.end method

.method public startSeriesActionShot()V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 5797
    const/16 v0, 0x46d

    invoke-virtual {p0, v0, v1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 5798
    return-void
.end method

.method public startShutterSound(Z)V
    .locals 2
    .param p1, "start"    # Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 5643
    const-string v0, "SecCamera-JNI-Java"

    const-string v1, "deprecated. replace to startShutterSound"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 5644
    invoke-virtual {p0, p1}, Lcom/sec/android/seccamera/SecCamera;->enablePanoramaShutterSound(Z)V

    .line 5645
    return-void
.end method

.method public startSmileDetection(Z)V
    .locals 2
    .param p1, "start"    # Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 5592
    if-eqz p1, :cond_0

    .line 5593
    const/16 v0, 0x44d

    invoke-virtual {p0, v0, v1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 5597
    :goto_0
    return-void

    .line 5595
    :cond_0
    const/16 v0, 0x44e

    invoke-virtual {p0, v0, v1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    goto :goto_0
.end method

.method public final native startSmoothZoom(I)V
.end method

.method public startTouchAutoFocus()V
    .locals 3

    .prologue
    .line 5941
    const/16 v0, 0x5e1

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 5942
    return-void
.end method

.method public startUltraWideSelfie(ZII)V
    .locals 2
    .param p1, "start"    # Z
    .param p2, "flip"    # I
    .param p3, "nOrientation"    # I

    .prologue
    const/4 v1, 0x0

    .line 6482
    if-eqz p1, :cond_0

    .line 6483
    const/16 v0, 0x5b5

    invoke-virtual {p0, v0, p2, p3}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 6487
    :goto_0
    return-void

    .line 6485
    :cond_0
    const/16 v0, 0x5b6

    invoke-virtual {p0, v0, v1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    goto :goto_0
.end method

.method public startZoom()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 6606
    const/16 v0, 0x67d

    invoke-virtual {p0, v0, v1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 6607
    return-void
.end method

.method public stopAnimatedPhoto()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 5994
    const-string v0, "SecCamera-JNI-Java"

    const-string v1, "stopAnimatedPhoto"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 5995
    const/16 v0, 0x658

    invoke-virtual {p0, v0, v2, v2}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 5996
    return-void
.end method

.method public stopContinuousAF()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 6060
    const/16 v0, 0x610

    invoke-virtual {p0, v0, v1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 6061
    return-void
.end method

.method public stopDramaShot()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 6219
    const/16 v0, 0x534

    invoke-virtual {p0, v0, v1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 6220
    return-void
.end method

.method public stopEffectRecording()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 5964
    const/16 v0, 0x656

    invoke-virtual {p0, v0, v1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 5965
    return-void
.end method

.method public final stopFaceDetection()V
    .locals 2

    .prologue
    .line 5290
    invoke-direct {p0}, Lcom/sec/android/seccamera/SecCamera;->_stopFaceDetection()V

    .line 5291
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/seccamera/SecCamera;->mFaceDetectionRunning:Z

    .line 5292
    iget-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mEventHandler:Lcom/sec/android/seccamera/SecCamera$EventHandler;

    if-eqz v0, :cond_0

    .line 5294
    iget-object v0, p0, Lcom/sec/android/seccamera/SecCamera;->mEventHandler:Lcom/sec/android/seccamera/SecCamera$EventHandler;

    const/16 v1, 0x400

    invoke-virtual {v0, v1}, Lcom/sec/android/seccamera/SecCamera$EventHandler;->removeMessages(I)V

    .line 5296
    :cond_0
    return-void
.end method

.method public stopFaceZoom()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 6046
    const/16 v0, 0x5fc

    invoke-virtual {p0, v0, v1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 6047
    return-void
.end method

.method public stopGolfShot()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 6158
    const/16 v0, 0x520

    invoke-virtual {p0, v0, v1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 6159
    return-void
.end method

.method public stopMagicShot()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 6466
    const/16 v0, 0x58e

    invoke-virtual {p0, v0, v1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 6467
    return-void
.end method

.method public final stopPreview()V
    .locals 2

    .prologue
    .line 3546
    invoke-direct {p0}, Lcom/sec/android/seccamera/SecCamera;->_stopPreview()V

    .line 3547
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/seccamera/SecCamera;->mFaceDetectionRunning:Z

    .line 3549
    const-string v0, "SecCamera-JNI-Java"

    const-string v1, "stopPreview"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3560
    return-void
.end method

.method public stopSeriesActionShot()V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 5806
    const/16 v0, 0x46f

    invoke-virtual {p0, v0, v1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 5807
    return-void
.end method

.method public final native stopSmoothZoom()V
.end method

.method public stopTakePicture()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 6646
    const/16 v0, 0x5bf

    invoke-virtual {p0, v0, v1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 6647
    return-void
.end method

.method public stopTouchAutoFocus()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 5948
    const/16 v0, 0x5e1

    invoke-virtual {p0, v0, v1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 5949
    return-void
.end method

.method public stopZoom()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 6617
    const/16 v0, 0x67e

    invoke-virtual {p0, v0, v1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 6618
    return-void
.end method

.method public takeDualModeAsyncShot(Z)V
    .locals 3
    .param p1, "isCapture"    # Z

    .prologue
    const/16 v2, 0x55e

    const/4 v1, 0x0

    .line 6296
    if-eqz p1, :cond_0

    .line 6297
    const/4 v0, 0x1

    invoke-virtual {p0, v2, v0, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 6300
    :goto_0
    return-void

    .line 6299
    :cond_0
    const/4 v0, 0x3

    invoke-virtual {p0, v2, v0, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    goto :goto_0
.end method

.method public final takePicture(Lcom/sec/android/seccamera/SecCamera$ShutterCallback;Lcom/sec/android/seccamera/SecCamera$PictureCallback;Lcom/sec/android/seccamera/SecCamera$PictureCallback;)V
    .locals 1
    .param p1, "shutter"    # Lcom/sec/android/seccamera/SecCamera$ShutterCallback;
    .param p2, "raw"    # Lcom/sec/android/seccamera/SecCamera$PictureCallback;
    .param p3, "jpeg"    # Lcom/sec/android/seccamera/SecCamera$PictureCallback;

    .prologue
    .line 4946
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0, p3}, Lcom/sec/android/seccamera/SecCamera;->takePicture(Lcom/sec/android/seccamera/SecCamera$ShutterCallback;Lcom/sec/android/seccamera/SecCamera$PictureCallback;Lcom/sec/android/seccamera/SecCamera$PictureCallback;Lcom/sec/android/seccamera/SecCamera$PictureCallback;)V

    .line 4947
    return-void
.end method

.method public final takePicture(Lcom/sec/android/seccamera/SecCamera$ShutterCallback;Lcom/sec/android/seccamera/SecCamera$PictureCallback;Lcom/sec/android/seccamera/SecCamera$PictureCallback;Lcom/sec/android/seccamera/SecCamera$PictureCallback;)V
    .locals 2
    .param p1, "shutter"    # Lcom/sec/android/seccamera/SecCamera$ShutterCallback;
    .param p2, "raw"    # Lcom/sec/android/seccamera/SecCamera$PictureCallback;
    .param p3, "postview"    # Lcom/sec/android/seccamera/SecCamera$PictureCallback;
    .param p4, "jpeg"    # Lcom/sec/android/seccamera/SecCamera$PictureCallback;

    .prologue
    .line 4981
    iput-object p1, p0, Lcom/sec/android/seccamera/SecCamera;->mShutterCallback:Lcom/sec/android/seccamera/SecCamera$ShutterCallback;

    .line 4982
    iput-object p2, p0, Lcom/sec/android/seccamera/SecCamera;->mRawImageCallback:Lcom/sec/android/seccamera/SecCamera$PictureCallback;

    .line 4983
    iput-object p3, p0, Lcom/sec/android/seccamera/SecCamera;->mPostviewCallback:Lcom/sec/android/seccamera/SecCamera$PictureCallback;

    .line 4984
    iput-object p4, p0, Lcom/sec/android/seccamera/SecCamera;->mJpegCallback:Lcom/sec/android/seccamera/SecCamera$PictureCallback;

    .line 4987
    const/4 v0, 0x0

    .line 4988
    .local v0, "msgType":I
    iget-object v1, p0, Lcom/sec/android/seccamera/SecCamera;->mShutterCallback:Lcom/sec/android/seccamera/SecCamera$ShutterCallback;

    if-eqz v1, :cond_0

    .line 4989
    or-int/lit8 v0, v0, 0x2

    .line 4991
    :cond_0
    iget-object v1, p0, Lcom/sec/android/seccamera/SecCamera;->mRawImageCallback:Lcom/sec/android/seccamera/SecCamera$PictureCallback;

    if-eqz v1, :cond_1

    .line 4992
    or-int/lit16 v0, v0, 0x80

    .line 4994
    :cond_1
    iget-object v1, p0, Lcom/sec/android/seccamera/SecCamera;->mPostviewCallback:Lcom/sec/android/seccamera/SecCamera$PictureCallback;

    if-eqz v1, :cond_2

    .line 4995
    or-int/lit8 v0, v0, 0x40

    .line 4997
    :cond_2
    iget-object v1, p0, Lcom/sec/android/seccamera/SecCamera;->mJpegCallback:Lcom/sec/android/seccamera/SecCamera$PictureCallback;

    if-eqz v1, :cond_3

    .line 4998
    or-int/lit16 v0, v0, 0x100

    .line 5001
    :cond_3
    invoke-direct {p0, v0}, Lcom/sec/android/seccamera/SecCamera;->native_takePicture(I)V

    .line 5002
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/seccamera/SecCamera;->mFaceDetectionRunning:Z

    .line 5003
    return-void
.end method

.method public terminateBurstShot()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 5732
    const/16 v0, 0x48d

    invoke-virtual {p0, v0, v1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 5733
    return-void
.end method

.method public terminateContinuousShot()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 5683
    const/16 v0, 0x463

    invoke-virtual {p0, v0, v1, v1}, Lcom/sec/android/seccamera/SecCamera;->native_sendcommand(III)V

    .line 5684
    return-void
.end method

.method public declared-synchronized unflatten(Ljava/lang/String;)Ljava/util/HashMap;
    .locals 8
    .param p1, "flattened"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 6751
    monitor-enter p0

    :try_start_0
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 6753
    .local v3, "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v5, Landroid/text/TextUtils$SimpleStringSplitter;

    const/16 v7, 0x3b

    invoke-direct {v5, v7}, Landroid/text/TextUtils$SimpleStringSplitter;-><init>(C)V

    .line 6754
    .local v5, "splitter":Landroid/text/TextUtils$StringSplitter;
    invoke-interface {v5, p1}, Landroid/text/TextUtils$StringSplitter;->setString(Ljava/lang/String;)V

    .line 6755
    invoke-interface {v5}, Landroid/text/TextUtils$StringSplitter;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 6756
    .local v2, "kv":Ljava/lang/String;
    const/16 v7, 0x3d

    invoke-virtual {v2, v7}, Ljava/lang/String;->indexOf(I)I

    move-result v4

    .line 6757
    .local v4, "pos":I
    const/4 v7, -0x1

    if-eq v4, v7, :cond_0

    .line 6760
    const/4 v7, 0x0

    invoke-virtual {v2, v7, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 6761
    .local v1, "k":Ljava/lang/String;
    add-int/lit8 v7, v4, 0x1

    invoke-virtual {v2, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    .line 6762
    .local v6, "v":Ljava/lang/String;
    invoke-virtual {v3, v1, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 6751
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "k":Ljava/lang/String;
    .end local v2    # "kv":Ljava/lang/String;
    .end local v3    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v4    # "pos":I
    .end local v5    # "splitter":Landroid/text/TextUtils$StringSplitter;
    .end local v6    # "v":Ljava/lang/String;
    :catchall_0
    move-exception v7

    monitor-exit p0

    throw v7

    .line 6764
    .restart local v0    # "i$":Ljava/util/Iterator;
    .restart local v3    # "map":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v5    # "splitter":Landroid/text/TextUtils$StringSplitter;
    :cond_1
    monitor-exit p0

    return-object v3
.end method

.method public final native unlock()V
.end method
