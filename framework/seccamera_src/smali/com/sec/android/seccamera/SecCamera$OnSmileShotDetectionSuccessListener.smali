.class public interface abstract Lcom/sec/android/seccamera/SecCamera$OnSmileShotDetectionSuccessListener;
.super Ljava/lang/Object;
.source "SecCamera.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/seccamera/SecCamera;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnSmileShotDetectionSuccessListener"
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# virtual methods
.method public abstract onSmileShotDetectionSuccess()V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract onSmileShotFaceRectChanged([B)V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract onSmileShotSmileRectChanged([B)V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method
