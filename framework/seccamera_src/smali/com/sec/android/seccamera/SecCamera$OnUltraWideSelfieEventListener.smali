.class public interface abstract Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;
.super Ljava/lang/Object;
.source "SecCamera.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/seccamera/SecCamera;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnUltraWideSelfieEventListener"
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# virtual methods
.method public abstract onUltraWideSelfieCaptured()V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract onUltraWideSelfieCapturedMaxFrames()V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract onUltraWideSelfieCapturedNew()V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract onUltraWideSelfieDirectionChanged(I)V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract onUltraWideSelfieError(I)V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract onUltraWideSelfieLivePreviewData([B)V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract onUltraWideSelfieLowResolutionData([B)V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract onUltraWideSelfieMoveSlowly()V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract onUltraWideSelfieNextCapturePosition(II)V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract onUltraWideSelfieProgressStitching(I)V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract onUltraWideSelfieRectChanged([B)V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract onUltraWideSelfieSingleCaptureDone()V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method
