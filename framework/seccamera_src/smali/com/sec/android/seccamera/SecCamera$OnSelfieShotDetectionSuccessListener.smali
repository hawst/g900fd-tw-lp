.class public interface abstract Lcom/sec/android/seccamera/SecCamera$OnSelfieShotDetectionSuccessListener;
.super Ljava/lang/Object;
.source "SecCamera.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/seccamera/SecCamera;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnSelfieShotDetectionSuccessListener"
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# virtual methods
.method public abstract onSelfieHeadPoseDetectionSuccess([B)V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract onSelfieShotPalmDetectionSuccess()V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract onSelfieShotSmileDetectionSuccess()V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public abstract onSelfieShotWinkDetectionSuccess()V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method
