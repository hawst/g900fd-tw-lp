.class Lcom/sec/android/seccamera/SecCamera$EventHandler;
.super Landroid/os/Handler;
.source "SecCamera.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/seccamera/SecCamera;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EventHandler"
.end annotation


# instance fields
.field private final mCamera:Lcom/sec/android/seccamera/SecCamera;

.field final synthetic this$0:Lcom/sec/android/seccamera/SecCamera;


# direct methods
.method public constructor <init>(Lcom/sec/android/seccamera/SecCamera;Lcom/sec/android/seccamera/SecCamera;Landroid/os/Looper;)V
    .locals 0
    .param p2, "c"    # Lcom/sec/android/seccamera/SecCamera;
    .param p3, "looper"    # Landroid/os/Looper;

    .prologue
    .line 3900
    iput-object p1, p0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    .line 3901
    invoke-direct {p0, p3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 3902
    iput-object p2, p0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->mCamera:Lcom/sec/android/seccamera/SecCamera;

    .line 3903
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 18
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 3907
    move-object/from16 v0, p1

    iget v2, v0, Landroid/os/Message;->what:I

    const/16 v3, 0x400

    if-ge v2, v3, :cond_0

    const-string v2, "SecCamera-JNI-Java"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "handleMessage: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p1

    iget v4, v0, Landroid/os/Message;->what:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3908
    :cond_0
    move-object/from16 v0, p1

    iget v2, v0, Landroid/os/Message;->what:I

    sparse-switch v2, :sswitch_data_0

    .line 4710
    const-string v2, "SecCamera-JNI-Java"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unknown message type "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p1

    iget v4, v0, Landroid/os/Message;->what:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 4713
    :cond_1
    :goto_0
    return-void

    .line 3910
    :sswitch_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mShutterCallback:Lcom/sec/android/seccamera/SecCamera$ShutterCallback;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$000(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$ShutterCallback;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 3911
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mShutterCallback:Lcom/sec/android/seccamera/SecCamera$ShutterCallback;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$000(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$ShutterCallback;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/seccamera/SecCamera$ShutterCallback;->onShutter()V

    goto :goto_0

    .line 3916
    :sswitch_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mRawImageCallback:Lcom/sec/android/seccamera/SecCamera$PictureCallback;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$100(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$PictureCallback;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 3917
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mRawImageCallback:Lcom/sec/android/seccamera/SecCamera$PictureCallback;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$100(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$PictureCallback;

    move-result-object v3

    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, [B

    check-cast v2, [B

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->mCamera:Lcom/sec/android/seccamera/SecCamera;

    invoke-interface {v3, v2, v4}, Lcom/sec/android/seccamera/SecCamera$PictureCallback;->onPictureTaken([BLcom/sec/android/seccamera/SecCamera;)V

    goto :goto_0

    .line 3922
    :sswitch_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mJpegCallback:Lcom/sec/android/seccamera/SecCamera$PictureCallback;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$200(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$PictureCallback;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 3923
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mJpegCallback:Lcom/sec/android/seccamera/SecCamera$PictureCallback;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$200(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$PictureCallback;

    move-result-object v3

    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, [B

    check-cast v2, [B

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->mCamera:Lcom/sec/android/seccamera/SecCamera;

    invoke-interface {v3, v2, v4}, Lcom/sec/android/seccamera/SecCamera$PictureCallback;->onPictureTaken([BLcom/sec/android/seccamera/SecCamera;)V

    goto :goto_0

    .line 3929
    :sswitch_3
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mPreviewCallback:Lcom/sec/android/seccamera/SecCamera$PreviewCallback;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$300(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$PreviewCallback;

    move-result-object v17

    .line 3930
    .local v17, "pCb":Lcom/sec/android/seccamera/SecCamera$PreviewCallback;
    if-eqz v17, :cond_1

    .line 3931
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOneShot:Z
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$400(Lcom/sec/android/seccamera/SecCamera;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 3935
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    const/4 v3, 0x0

    # setter for: Lcom/sec/android/seccamera/SecCamera;->mPreviewCallback:Lcom/sec/android/seccamera/SecCamera$PreviewCallback;
    invoke-static {v2, v3}, Lcom/sec/android/seccamera/SecCamera;->access$302(Lcom/sec/android/seccamera/SecCamera;Lcom/sec/android/seccamera/SecCamera$PreviewCallback;)Lcom/sec/android/seccamera/SecCamera$PreviewCallback;

    .line 3942
    :cond_2
    :goto_1
    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, [B

    check-cast v2, [B

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->mCamera:Lcom/sec/android/seccamera/SecCamera;

    move-object/from16 v0, v17

    invoke-interface {v0, v2, v3}, Lcom/sec/android/seccamera/SecCamera$PreviewCallback;->onPreviewFrame([BLcom/sec/android/seccamera/SecCamera;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 3945
    .end local v17    # "pCb":Lcom/sec/android/seccamera/SecCamera$PreviewCallback;
    :catch_0
    move-exception v12

    .local v12, "e":Ljava/lang/Exception;
    const-string v2, "SecCamera-JNI-Java"

    const-string v3, "CAMERA_MSG_PREVIEW_FRAME"

    invoke-static {v2, v3, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3948
    .end local v12    # "e":Ljava/lang/Exception;
    :sswitch_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mPostviewCallback:Lcom/sec/android/seccamera/SecCamera$PictureCallback;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$PictureCallback;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 3949
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mPostviewCallback:Lcom/sec/android/seccamera/SecCamera$PictureCallback;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$PictureCallback;

    move-result-object v3

    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, [B

    check-cast v2, [B

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->mCamera:Lcom/sec/android/seccamera/SecCamera;

    invoke-interface {v3, v2, v4}, Lcom/sec/android/seccamera/SecCamera$PictureCallback;->onPictureTaken([BLcom/sec/android/seccamera/SecCamera;)V

    goto/16 :goto_0

    .line 3936
    .restart local v17    # "pCb":Lcom/sec/android/seccamera/SecCamera$PreviewCallback;
    :cond_3
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mWithBuffer:Z
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$500(Lcom/sec/android/seccamera/SecCamera;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 3940
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    # invokes: Lcom/sec/android/seccamera/SecCamera;->setHasPreviewCallback(ZZZ)V
    invoke-static {v2, v3, v4, v5}, Lcom/sec/android/seccamera/SecCamera;->access$600(Lcom/sec/android/seccamera/SecCamera;ZZZ)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 3954
    .end local v17    # "pCb":Lcom/sec/android/seccamera/SecCamera$PreviewCallback;
    :sswitch_5
    const/4 v11, 0x0

    .line 3955
    .local v11, "cb":Lcom/sec/android/seccamera/SecCamera$AutoFocusCallback;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mAutoFocusCallbackLock:Ljava/lang/Object;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$800(Lcom/sec/android/seccamera/SecCamera;)Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    .line 3956
    :try_start_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mAutoFocusCallback:Lcom/sec/android/seccamera/SecCamera$AutoFocusCallback;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$900(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$AutoFocusCallback;

    move-result-object v11

    .line 3957
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 3958
    if-eqz v11, :cond_1

    .line 3960
    move-object/from16 v0, p1

    iget v2, v0, Landroid/os/Message;->arg1:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->mCamera:Lcom/sec/android/seccamera/SecCamera;

    invoke-interface {v11, v2, v3}, Lcom/sec/android/seccamera/SecCamera$AutoFocusCallback;->onAutoFocus(ILcom/sec/android/seccamera/SecCamera;)V

    goto/16 :goto_0

    .line 3957
    :catchall_0
    move-exception v2

    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v2

    .line 3965
    .end local v11    # "cb":Lcom/sec/android/seccamera/SecCamera$AutoFocusCallback;
    :sswitch_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mZoomListener:Lcom/sec/android/seccamera/SecCamera$OnZoomChangeListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1000(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnZoomChangeListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 3966
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mZoomListener:Lcom/sec/android/seccamera/SecCamera$OnZoomChangeListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1000(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnZoomChangeListener;

    move-result-object v3

    move-object/from16 v0, p1

    iget v4, v0, Landroid/os/Message;->arg1:I

    move-object/from16 v0, p1

    iget v2, v0, Landroid/os/Message;->arg2:I

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    :goto_2
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->mCamera:Lcom/sec/android/seccamera/SecCamera;

    invoke-interface {v3, v4, v2, v5}, Lcom/sec/android/seccamera/SecCamera$OnZoomChangeListener;->onZoomChange(IZLcom/sec/android/seccamera/SecCamera;)V

    goto/16 :goto_0

    :cond_4
    const/4 v2, 0x0

    goto :goto_2

    .line 3971
    :sswitch_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mFaceListener:Lcom/sec/android/seccamera/SecCamera$FaceDetectionListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1100(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$FaceDetectionListener;

    move-result-object v2

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mFaceDetectionRunning:Z
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1200(Lcom/sec/android/seccamera/SecCamera;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 3972
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mFaceListener:Lcom/sec/android/seccamera/SecCamera$FaceDetectionListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1100(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$FaceDetectionListener;

    move-result-object v3

    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, [Lcom/sec/android/seccamera/SecCamera$Face;

    check-cast v2, [Lcom/sec/android/seccamera/SecCamera$Face;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->mCamera:Lcom/sec/android/seccamera/SecCamera;

    invoke-interface {v3, v2, v4}, Lcom/sec/android/seccamera/SecCamera$FaceDetectionListener;->onFaceDetection([Lcom/sec/android/seccamera/SecCamera$Face;Lcom/sec/android/seccamera/SecCamera;)V

    goto/16 :goto_0

    .line 3977
    :sswitch_8
    const-string v2, "SecCamera-JNI-Java"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p1

    iget v4, v0, Landroid/os/Message;->arg1:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 3978
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mErrorCallback:Lcom/sec/android/seccamera/SecCamera$ErrorCallback;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1300(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$ErrorCallback;

    move-result-object v2

    if-eqz v2, :cond_5

    .line 3979
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mErrorCallback:Lcom/sec/android/seccamera/SecCamera$ErrorCallback;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1300(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$ErrorCallback;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->mCamera:Lcom/sec/android/seccamera/SecCamera;

    invoke-interface {v2, v3, v4}, Lcom/sec/android/seccamera/SecCamera$ErrorCallback;->onError(ILcom/sec/android/seccamera/SecCamera;)V

    .line 3982
    :cond_5
    move-object/from16 v0, p1

    iget v2, v0, Landroid/os/Message;->arg1:I

    const/16 v3, 0xc8

    if-ne v2, v3, :cond_1

    .line 3984
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # invokes: Lcom/sec/android/seccamera/SecCamera;->native_release()V
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1400(Lcom/sec/android/seccamera/SecCamera;)V

    .line 3985
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    const/4 v3, 0x0

    # setter for: Lcom/sec/android/seccamera/SecCamera;->mFaceDetectionRunning:Z
    invoke-static {v2, v3}, Lcom/sec/android/seccamera/SecCamera;->access$1202(Lcom/sec/android/seccamera/SecCamera;Z)Z

    goto/16 :goto_0

    .line 3990
    :sswitch_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mAutoFocusMoveCallback:Lcom/sec/android/seccamera/SecCamera$AutoFocusMoveCallback;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1500(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$AutoFocusMoveCallback;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 3991
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mAutoFocusMoveCallback:Lcom/sec/android/seccamera/SecCamera$AutoFocusMoveCallback;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1500(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$AutoFocusMoveCallback;

    move-result-object v3

    move-object/from16 v0, p1

    iget v2, v0, Landroid/os/Message;->arg1:I

    if-nez v2, :cond_6

    const/4 v2, 0x0

    :goto_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->mCamera:Lcom/sec/android/seccamera/SecCamera;

    invoke-interface {v3, v2, v4}, Lcom/sec/android/seccamera/SecCamera$AutoFocusMoveCallback;->onAutoFocusMoving(ZLcom/sec/android/seccamera/SecCamera;)V

    goto/16 :goto_0

    :cond_6
    const/4 v2, 0x1

    goto :goto_3

    .line 3998
    :sswitch_a
    const-string v2, "SecCamera-JNI-Java"

    const-string v3, "SAMSUNG_SHOT_COMPRESSED_IMAGE"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 3999
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mJpegCallback:Lcom/sec/android/seccamera/SecCamera$PictureCallback;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$200(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$PictureCallback;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4000
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mJpegCallback:Lcom/sec/android/seccamera/SecCamera$PictureCallback;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$200(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$PictureCallback;

    move-result-object v3

    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, [B

    check-cast v2, [B

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->mCamera:Lcom/sec/android/seccamera/SecCamera;

    invoke-interface {v3, v2, v4}, Lcom/sec/android/seccamera/SecCamera$PictureCallback;->onPictureTaken([BLcom/sec/android/seccamera/SecCamera;)V

    goto/16 :goto_0

    .line 4005
    :sswitch_b
    const-string v2, "SecCamera-JNI-Java"

    const-string v3, "SAMSUNG_BURST_PANORAMA_SHOT_COMPRESSED_IMAGE"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 4006
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mJpegCallback:Lcom/sec/android/seccamera/SecCamera$PictureCallback;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$200(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$PictureCallback;

    move-result-object v2

    if-eqz v2, :cond_7

    .line 4007
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mJpegCallback:Lcom/sec/android/seccamera/SecCamera$PictureCallback;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$200(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$PictureCallback;

    move-result-object v3

    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, [B

    check-cast v2, [B

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->mCamera:Lcom/sec/android/seccamera/SecCamera;

    invoke-interface {v3, v2, v4}, Lcom/sec/android/seccamera/SecCamera$PictureCallback;->onPictureTaken([BLcom/sec/android/seccamera/SecCamera;)V

    goto/16 :goto_0

    .line 4009
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnPanoramaEventListener:Lcom/sec/android/seccamera/SecCamera$OnPanoramaEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1600(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnPanoramaEventListener;

    move-result-object v2

    if-eqz v2, :cond_8

    .line 4010
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnPanoramaEventListener:Lcom/sec/android/seccamera/SecCamera$OnPanoramaEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1600(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnPanoramaEventListener;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Lcom/sec/android/seccamera/SecCamera$OnPanoramaEventListener;->onPanoramaError(I)V

    .line 4012
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mPanoramaEventListener:Lcom/sec/android/seccamera/SecCamera$PanoramaEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$PanoramaEventListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4013
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mPanoramaEventListener:Lcom/sec/android/seccamera/SecCamera$PanoramaEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$PanoramaEventListener;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Lcom/sec/android/seccamera/SecCamera$PanoramaEventListener;->onPanoramaError(I)V

    goto/16 :goto_0

    .line 4019
    :sswitch_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnPanoramaEventListener:Lcom/sec/android/seccamera/SecCamera$OnPanoramaEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1600(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnPanoramaEventListener;

    move-result-object v2

    if-eqz v2, :cond_9

    .line 4020
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnPanoramaEventListener:Lcom/sec/android/seccamera/SecCamera$OnPanoramaEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1600(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnPanoramaEventListener;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    invoke-interface {v2, v3}, Lcom/sec/android/seccamera/SecCamera$OnPanoramaEventListener;->onPanoramaError(I)V

    .line 4022
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mPanoramaEventListener:Lcom/sec/android/seccamera/SecCamera$PanoramaEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$PanoramaEventListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4023
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mPanoramaEventListener:Lcom/sec/android/seccamera/SecCamera$PanoramaEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$PanoramaEventListener;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    invoke-interface {v2, v3}, Lcom/sec/android/seccamera/SecCamera$PanoramaEventListener;->onPanoramaError(I)V

    goto/16 :goto_0

    .line 4027
    :sswitch_d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnPanoramaEventListener:Lcom/sec/android/seccamera/SecCamera$OnPanoramaEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1600(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnPanoramaEventListener;

    move-result-object v2

    if-eqz v2, :cond_a

    .line 4028
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnPanoramaEventListener:Lcom/sec/android/seccamera/SecCamera$OnPanoramaEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1600(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnPanoramaEventListener;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    invoke-interface {v2, v3}, Lcom/sec/android/seccamera/SecCamera$OnPanoramaEventListener;->onPanoramaDirectionChanged(I)V

    .line 4030
    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mPanoramaEventListener:Lcom/sec/android/seccamera/SecCamera$PanoramaEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$PanoramaEventListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4031
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mPanoramaEventListener:Lcom/sec/android/seccamera/SecCamera$PanoramaEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$PanoramaEventListener;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    invoke-interface {v2, v3}, Lcom/sec/android/seccamera/SecCamera$PanoramaEventListener;->onPanoramaDirectionChanged(I)V

    goto/16 :goto_0

    .line 4035
    :sswitch_e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnPanoramaEventListener:Lcom/sec/android/seccamera/SecCamera$OnPanoramaEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1600(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnPanoramaEventListener;

    move-result-object v2

    if-eqz v2, :cond_b

    .line 4036
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnPanoramaEventListener:Lcom/sec/android/seccamera/SecCamera$OnPanoramaEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1600(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnPanoramaEventListener;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    move-object/from16 v0, p1

    iget v4, v0, Landroid/os/Message;->arg2:I

    invoke-interface {v2, v3, v4}, Lcom/sec/android/seccamera/SecCamera$OnPanoramaEventListener;->onPanoramaRectChanged(II)V

    .line 4038
    :cond_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mPanoramaEventListener:Lcom/sec/android/seccamera/SecCamera$PanoramaEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$PanoramaEventListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4039
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mPanoramaEventListener:Lcom/sec/android/seccamera/SecCamera$PanoramaEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$PanoramaEventListener;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    move-object/from16 v0, p1

    iget v4, v0, Landroid/os/Message;->arg2:I

    invoke-interface {v2, v3, v4}, Lcom/sec/android/seccamera/SecCamera$PanoramaEventListener;->onPanoramaRectChanged(II)V

    goto/16 :goto_0

    .line 4043
    :sswitch_f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnPanoramaEventListener:Lcom/sec/android/seccamera/SecCamera$OnPanoramaEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1600(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnPanoramaEventListener;

    move-result-object v2

    if-eqz v2, :cond_c

    .line 4044
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnPanoramaEventListener:Lcom/sec/android/seccamera/SecCamera$OnPanoramaEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1600(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnPanoramaEventListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/seccamera/SecCamera$OnPanoramaEventListener;->onPanoramaCapturedNew()V

    .line 4046
    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mPanoramaEventListener:Lcom/sec/android/seccamera/SecCamera$PanoramaEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$PanoramaEventListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4047
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mPanoramaEventListener:Lcom/sec/android/seccamera/SecCamera$PanoramaEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$PanoramaEventListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/seccamera/SecCamera$PanoramaEventListener;->onPanoramaCapturedNew()V

    goto/16 :goto_0

    .line 4051
    :sswitch_10
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnPanoramaEventListener:Lcom/sec/android/seccamera/SecCamera$OnPanoramaEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1600(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnPanoramaEventListener;

    move-result-object v2

    if-eqz v2, :cond_d

    .line 4052
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnPanoramaEventListener:Lcom/sec/android/seccamera/SecCamera$OnPanoramaEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1600(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnPanoramaEventListener;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    invoke-interface {v2, v3}, Lcom/sec/android/seccamera/SecCamera$OnPanoramaEventListener;->onPanoramaProgressStitching(I)V

    .line 4054
    :cond_d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mPanoramaEventListener:Lcom/sec/android/seccamera/SecCamera$PanoramaEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$PanoramaEventListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4055
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mPanoramaEventListener:Lcom/sec/android/seccamera/SecCamera$PanoramaEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$PanoramaEventListener;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    invoke-interface {v2, v3}, Lcom/sec/android/seccamera/SecCamera$PanoramaEventListener;->onPanoramaProgressStitching(I)V

    goto/16 :goto_0

    .line 4059
    :sswitch_11
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnPanoramaEventListener:Lcom/sec/android/seccamera/SecCamera$OnPanoramaEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1600(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnPanoramaEventListener;

    move-result-object v2

    if-eqz v2, :cond_e

    .line 4060
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnPanoramaEventListener:Lcom/sec/android/seccamera/SecCamera$OnPanoramaEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1600(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnPanoramaEventListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/seccamera/SecCamera$OnPanoramaEventListener;->onPanoramaCaptured()V

    .line 4062
    :cond_e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mPanoramaEventListener:Lcom/sec/android/seccamera/SecCamera$PanoramaEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$PanoramaEventListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4063
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mPanoramaEventListener:Lcom/sec/android/seccamera/SecCamera$PanoramaEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$PanoramaEventListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/seccamera/SecCamera$PanoramaEventListener;->onPanoramaCaptured()V

    goto/16 :goto_0

    .line 4067
    :sswitch_12
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnPanoramaEventListener:Lcom/sec/android/seccamera/SecCamera$OnPanoramaEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1600(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnPanoramaEventListener;

    move-result-object v2

    if-eqz v2, :cond_f

    .line 4068
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnPanoramaEventListener:Lcom/sec/android/seccamera/SecCamera$OnPanoramaEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1600(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnPanoramaEventListener;

    move-result-object v3

    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, [B

    check-cast v2, [B

    invoke-interface {v3, v2}, Lcom/sec/android/seccamera/SecCamera$OnPanoramaEventListener;->onPanoramaLowResolutionData([B)V

    .line 4070
    :cond_f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mPanoramaEventListener:Lcom/sec/android/seccamera/SecCamera$PanoramaEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$PanoramaEventListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4071
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mPanoramaEventListener:Lcom/sec/android/seccamera/SecCamera$PanoramaEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$PanoramaEventListener;

    move-result-object v3

    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, [B

    check-cast v2, [B

    invoke-interface {v3, v2}, Lcom/sec/android/seccamera/SecCamera$PanoramaEventListener;->onPanoramaLowResolutionData([B)V

    goto/16 :goto_0

    .line 4075
    :sswitch_13
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnPanoramaEventListener:Lcom/sec/android/seccamera/SecCamera$OnPanoramaEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1600(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnPanoramaEventListener;

    move-result-object v2

    if-eqz v2, :cond_10

    .line 4076
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnPanoramaEventListener:Lcom/sec/android/seccamera/SecCamera$OnPanoramaEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1600(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnPanoramaEventListener;

    move-result-object v3

    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, [B

    check-cast v2, [B

    invoke-interface {v3, v2}, Lcom/sec/android/seccamera/SecCamera$OnPanoramaEventListener;->onPanoramaLivePreviewData([B)V

    .line 4078
    :cond_10
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mPanoramaEventListener:Lcom/sec/android/seccamera/SecCamera$PanoramaEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$PanoramaEventListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4079
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mPanoramaEventListener:Lcom/sec/android/seccamera/SecCamera$PanoramaEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$PanoramaEventListener;

    move-result-object v3

    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, [B

    check-cast v2, [B

    invoke-interface {v3, v2}, Lcom/sec/android/seccamera/SecCamera$PanoramaEventListener;->onPanoramaLivePreviewData([B)V

    goto/16 :goto_0

    .line 4083
    :sswitch_14
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnPanoramaEventListener:Lcom/sec/android/seccamera/SecCamera$OnPanoramaEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1600(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnPanoramaEventListener;

    move-result-object v2

    if-eqz v2, :cond_11

    .line 4084
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnPanoramaEventListener:Lcom/sec/android/seccamera/SecCamera$OnPanoramaEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1600(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnPanoramaEventListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/seccamera/SecCamera$OnPanoramaEventListener;->onPanoramaCapturedMaxFrames()V

    .line 4086
    :cond_11
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mPanoramaEventListener:Lcom/sec/android/seccamera/SecCamera$PanoramaEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$PanoramaEventListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4087
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mPanoramaEventListener:Lcom/sec/android/seccamera/SecCamera$PanoramaEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$PanoramaEventListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/seccamera/SecCamera$PanoramaEventListener;->onPanoramaCapturedMaxFrames()V

    goto/16 :goto_0

    .line 4091
    :sswitch_15
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnPanoramaEventListener:Lcom/sec/android/seccamera/SecCamera$OnPanoramaEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1600(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnPanoramaEventListener;

    move-result-object v2

    if-eqz v2, :cond_12

    .line 4092
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnPanoramaEventListener:Lcom/sec/android/seccamera/SecCamera$OnPanoramaEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1600(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnPanoramaEventListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/seccamera/SecCamera$OnPanoramaEventListener;->onPanoramaMoveSlowly()V

    .line 4094
    :cond_12
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mPanoramaEventListener:Lcom/sec/android/seccamera/SecCamera$PanoramaEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$PanoramaEventListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4095
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mPanoramaEventListener:Lcom/sec/android/seccamera/SecCamera$PanoramaEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$PanoramaEventListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/seccamera/SecCamera$PanoramaEventListener;->onPanoramaMoveSlowly()V

    goto/16 :goto_0

    .line 4099
    :sswitch_16
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnContinuousShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnContinuousShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1800(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnContinuousShotEventListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4100
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnContinuousShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnContinuousShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1800(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnContinuousShotEventListener;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    move-object/from16 v0, p1

    iget v4, v0, Landroid/os/Message;->arg2:I

    invoke-interface {v2, v3, v4}, Lcom/sec/android/seccamera/SecCamera$OnContinuousShotEventListener;->onContinuousShotCapturingProgressed(II)V

    goto/16 :goto_0

    .line 4104
    :sswitch_17
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnContinuousShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnContinuousShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1800(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnContinuousShotEventListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4105
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnContinuousShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnContinuousShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1800(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnContinuousShotEventListener;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    invoke-interface {v2, v3}, Lcom/sec/android/seccamera/SecCamera$OnContinuousShotEventListener;->onContinuousShotCapturingStopped(I)V

    goto/16 :goto_0

    .line 4109
    :sswitch_18
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnContinuousShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnContinuousShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1800(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnContinuousShotEventListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4110
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnContinuousShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnContinuousShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1800(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnContinuousShotEventListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/seccamera/SecCamera$OnContinuousShotEventListener;->onContinuousShotSavingCompleted()V

    goto/16 :goto_0

    .line 4114
    :sswitch_19
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnActionShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnActionShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1900(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnActionShotEventListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4115
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnActionShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnActionShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1900(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnActionShotEventListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/seccamera/SecCamera$OnActionShotEventListener;->onActionShotCreatingResultStarted()V

    goto/16 :goto_0

    .line 4119
    :sswitch_1a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnActionShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnActionShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1900(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnActionShotEventListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4120
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnActionShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnActionShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1900(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnActionShotEventListener;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    invoke-interface {v2, v3}, Lcom/sec/android/seccamera/SecCamera$OnActionShotEventListener;->onActionShotCreatingResultProgress(I)V

    goto/16 :goto_0

    .line 4124
    :sswitch_1b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnActionShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnActionShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1900(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnActionShotEventListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4125
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnActionShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnActionShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1900(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnActionShotEventListener;

    move-result-object v3

    move-object/from16 v0, p1

    iget v2, v0, Landroid/os/Message;->arg1:I

    const/4 v4, 0x1

    if-ne v2, v4, :cond_13

    const/4 v2, 0x1

    :goto_4
    invoke-interface {v3, v2}, Lcom/sec/android/seccamera/SecCamera$OnActionShotEventListener;->onActionShotCreatingResultCompleted(Z)V

    goto/16 :goto_0

    :cond_13
    const/4 v2, 0x0

    goto :goto_4

    .line 4129
    :sswitch_1c
    const-string v2, "SecCamera-JNI-Java"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onActionShotAcquisitionProgress "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p1

    iget v4, v0, Landroid/os/Message;->arg1:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 4130
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnActionShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnActionShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1900(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnActionShotEventListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4131
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnActionShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnActionShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1900(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnActionShotEventListener;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    invoke-interface {v2, v3}, Lcom/sec/android/seccamera/SecCamera$OnActionShotEventListener;->onActionShotAcquisitionProgress(I)V

    goto/16 :goto_0

    .line 4135
    :sswitch_1d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnActionShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnActionShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1900(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnActionShotEventListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4136
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnActionShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnActionShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1900(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnActionShotEventListener;

    move-result-object v3

    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, [B

    check-cast v2, [B

    invoke-interface {v3, v2}, Lcom/sec/android/seccamera/SecCamera$OnActionShotEventListener;->onActionShotRectChanged([B)V

    goto/16 :goto_0

    .line 4140
    :sswitch_1e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnActionShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnActionShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1900(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnActionShotEventListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4141
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnActionShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnActionShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$1900(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnActionShotEventListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/seccamera/SecCamera$OnActionShotEventListener;->onActionShotCaptured()V

    goto/16 :goto_0

    .line 4145
    :sswitch_1f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnCartoonShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnCartoonShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$2000(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnCartoonShotEventListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4146
    const-string v2, "SecCamera-JNI-Java"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "CARTOON_SHOT_PROGRESS_RENDERING :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p1

    iget v4, v0, Landroid/os/Message;->arg1:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4147
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnCartoonShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnCartoonShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$2000(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnCartoonShotEventListener;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    invoke-interface {v2, v3}, Lcom/sec/android/seccamera/SecCamera$OnCartoonShotEventListener;->onCartoonShotProgressRendering(I)V

    goto/16 :goto_0

    .line 4151
    :sswitch_20
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnSmileShotDetectionSuccessListener:Lcom/sec/android/seccamera/SecCamera$OnSmileShotDetectionSuccessListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$2100(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnSmileShotDetectionSuccessListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4152
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnSmileShotDetectionSuccessListener:Lcom/sec/android/seccamera/SecCamera$OnSmileShotDetectionSuccessListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$2100(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnSmileShotDetectionSuccessListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/seccamera/SecCamera$OnSmileShotDetectionSuccessListener;->onSmileShotDetectionSuccess()V

    goto/16 :goto_0

    .line 4156
    :sswitch_21
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnSmileShotDetectionSuccessListener:Lcom/sec/android/seccamera/SecCamera$OnSmileShotDetectionSuccessListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$2100(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnSmileShotDetectionSuccessListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4157
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnSmileShotDetectionSuccessListener:Lcom/sec/android/seccamera/SecCamera$OnSmileShotDetectionSuccessListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$2100(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnSmileShotDetectionSuccessListener;

    move-result-object v3

    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, [B

    check-cast v2, [B

    invoke-interface {v3, v2}, Lcom/sec/android/seccamera/SecCamera$OnSmileShotDetectionSuccessListener;->onSmileShotFaceRectChanged([B)V

    goto/16 :goto_0

    .line 4161
    :sswitch_22
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnSmileShotDetectionSuccessListener:Lcom/sec/android/seccamera/SecCamera$OnSmileShotDetectionSuccessListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$2100(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnSmileShotDetectionSuccessListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4162
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnSmileShotDetectionSuccessListener:Lcom/sec/android/seccamera/SecCamera$OnSmileShotDetectionSuccessListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$2100(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnSmileShotDetectionSuccessListener;

    move-result-object v3

    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, [B

    check-cast v2, [B

    invoke-interface {v3, v2}, Lcom/sec/android/seccamera/SecCamera$OnSmileShotDetectionSuccessListener;->onSmileShotSmileRectChanged([B)V

    goto/16 :goto_0

    .line 4166
    :sswitch_23
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnSelfieShotDetectionSuccessListener:Lcom/sec/android/seccamera/SecCamera$OnSelfieShotDetectionSuccessListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$2200(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnSelfieShotDetectionSuccessListener;

    move-result-object v2

    if-eqz v2, :cond_14

    .line 4167
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnSelfieShotDetectionSuccessListener:Lcom/sec/android/seccamera/SecCamera$OnSelfieShotDetectionSuccessListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$2200(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnSelfieShotDetectionSuccessListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/seccamera/SecCamera$OnSelfieShotDetectionSuccessListener;->onSelfieShotWinkDetectionSuccess()V

    .line 4169
    :cond_14
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mSelfieDetectionListener:Lcom/sec/android/seccamera/SecCamera$SelfieDetectionListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$2300(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$SelfieDetectionListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4170
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mSelfieDetectionListener:Lcom/sec/android/seccamera/SecCamera$SelfieDetectionListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$2300(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$SelfieDetectionListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/seccamera/SecCamera$SelfieDetectionListener;->onWinkDetected()V

    goto/16 :goto_0

    .line 4174
    :sswitch_24
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnSelfieShotDetectionSuccessListener:Lcom/sec/android/seccamera/SecCamera$OnSelfieShotDetectionSuccessListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$2200(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnSelfieShotDetectionSuccessListener;

    move-result-object v2

    if-eqz v2, :cond_15

    .line 4175
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnSelfieShotDetectionSuccessListener:Lcom/sec/android/seccamera/SecCamera$OnSelfieShotDetectionSuccessListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$2200(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnSelfieShotDetectionSuccessListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/seccamera/SecCamera$OnSelfieShotDetectionSuccessListener;->onSelfieShotPalmDetectionSuccess()V

    .line 4177
    :cond_15
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mSelfieDetectionListener:Lcom/sec/android/seccamera/SecCamera$SelfieDetectionListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$2300(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$SelfieDetectionListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4178
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mSelfieDetectionListener:Lcom/sec/android/seccamera/SecCamera$SelfieDetectionListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$2300(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$SelfieDetectionListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/seccamera/SecCamera$SelfieDetectionListener;->onPalmDetected()V

    goto/16 :goto_0

    .line 4182
    :sswitch_25
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnSelfieShotDetectionSuccessListener:Lcom/sec/android/seccamera/SecCamera$OnSelfieShotDetectionSuccessListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$2200(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnSelfieShotDetectionSuccessListener;

    move-result-object v2

    if-eqz v2, :cond_16

    .line 4183
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnSelfieShotDetectionSuccessListener:Lcom/sec/android/seccamera/SecCamera$OnSelfieShotDetectionSuccessListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$2200(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnSelfieShotDetectionSuccessListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/seccamera/SecCamera$OnSelfieShotDetectionSuccessListener;->onSelfieShotSmileDetectionSuccess()V

    .line 4185
    :cond_16
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mSelfieDetectionListener:Lcom/sec/android/seccamera/SecCamera$SelfieDetectionListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$2300(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$SelfieDetectionListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4186
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mSelfieDetectionListener:Lcom/sec/android/seccamera/SecCamera$SelfieDetectionListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$2300(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$SelfieDetectionListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/seccamera/SecCamera$SelfieDetectionListener;->onSmileDetected()V

    goto/16 :goto_0

    .line 4190
    :sswitch_26
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnSelfieShotDetectionSuccessListener:Lcom/sec/android/seccamera/SecCamera$OnSelfieShotDetectionSuccessListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$2200(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnSelfieShotDetectionSuccessListener;

    move-result-object v2

    if-eqz v2, :cond_17

    .line 4191
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnSelfieShotDetectionSuccessListener:Lcom/sec/android/seccamera/SecCamera$OnSelfieShotDetectionSuccessListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$2200(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnSelfieShotDetectionSuccessListener;

    move-result-object v3

    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, [B

    check-cast v2, [B

    invoke-interface {v3, v2}, Lcom/sec/android/seccamera/SecCamera$OnSelfieShotDetectionSuccessListener;->onSelfieHeadPoseDetectionSuccess([B)V

    .line 4193
    :cond_17
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mSelfieDetectionListener:Lcom/sec/android/seccamera/SecCamera$SelfieDetectionListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$2300(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$SelfieDetectionListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4194
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mSelfieDetectionListener:Lcom/sec/android/seccamera/SecCamera$SelfieDetectionListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$2300(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$SelfieDetectionListener;

    move-result-object v3

    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, [B

    check-cast v2, [B

    invoke-interface {v3, v2}, Lcom/sec/android/seccamera/SecCamera$SelfieDetectionListener;->onHeadPoseDetected([B)V

    goto/16 :goto_0

    .line 4198
    :sswitch_27
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnHDRShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnHDRShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$2400(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnHDRShotEventListener;

    move-result-object v2

    if-eqz v2, :cond_18

    .line 4199
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnHDRShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnHDRShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$2400(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnHDRShotEventListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/seccamera/SecCamera$OnHDRShotEventListener;->onHDRShotResultStarted()V

    .line 4201
    :cond_18
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mHdrEventListener:Lcom/sec/android/seccamera/SecCamera$HdrEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$2500(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$HdrEventListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4202
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mHdrEventListener:Lcom/sec/android/seccamera/SecCamera$HdrEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$2500(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$HdrEventListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/seccamera/SecCamera$HdrEventListener;->onHdrResultStarted()V

    goto/16 :goto_0

    .line 4207
    :sswitch_28
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnHDRShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnHDRShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$2400(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnHDRShotEventListener;

    move-result-object v2

    if-eqz v2, :cond_19

    .line 4208
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnHDRShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnHDRShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$2400(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnHDRShotEventListener;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    invoke-interface {v2, v3}, Lcom/sec/android/seccamera/SecCamera$OnHDRShotEventListener;->onHDRShotResultProgress(I)V

    .line 4210
    :cond_19
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mHdrEventListener:Lcom/sec/android/seccamera/SecCamera$HdrEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$2500(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$HdrEventListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4211
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mHdrEventListener:Lcom/sec/android/seccamera/SecCamera$HdrEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$2500(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$HdrEventListener;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    invoke-interface {v2, v3}, Lcom/sec/android/seccamera/SecCamera$HdrEventListener;->onHdrResultProgress(I)V

    goto/16 :goto_0

    .line 4216
    :sswitch_29
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnHDRShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnHDRShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$2400(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnHDRShotEventListener;

    move-result-object v2

    if-eqz v2, :cond_1a

    .line 4217
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnHDRShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnHDRShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$2400(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnHDRShotEventListener;

    move-result-object v3

    move-object/from16 v0, p1

    iget v2, v0, Landroid/os/Message;->arg1:I

    const/4 v4, 0x1

    if-ne v2, v4, :cond_1b

    const/4 v2, 0x1

    :goto_5
    invoke-interface {v3, v2}, Lcom/sec/android/seccamera/SecCamera$OnHDRShotEventListener;->onHDRShotResultCompleted(Z)V

    .line 4219
    :cond_1a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mHdrEventListener:Lcom/sec/android/seccamera/SecCamera$HdrEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$2500(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$HdrEventListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4220
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mHdrEventListener:Lcom/sec/android/seccamera/SecCamera$HdrEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$2500(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$HdrEventListener;

    move-result-object v3

    move-object/from16 v0, p1

    iget v2, v0, Landroid/os/Message;->arg1:I

    const/4 v4, 0x1

    if-ne v2, v4, :cond_1c

    const/4 v2, 0x1

    :goto_6
    invoke-interface {v3, v2}, Lcom/sec/android/seccamera/SecCamera$HdrEventListener;->onHdrResultCompleted(Z)V

    goto/16 :goto_0

    .line 4217
    :cond_1b
    const/4 v2, 0x0

    goto :goto_5

    .line 4220
    :cond_1c
    const/4 v2, 0x0

    goto :goto_6

    .line 4225
    :sswitch_2a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnHDRShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnHDRShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$2400(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnHDRShotEventListener;

    move-result-object v2

    if-eqz v2, :cond_1d

    .line 4226
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnHDRShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnHDRShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$2400(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnHDRShotEventListener;

    move-result-object v3

    move-object/from16 v0, p1

    iget v2, v0, Landroid/os/Message;->arg1:I

    const/4 v4, 0x1

    if-ne v2, v4, :cond_1e

    const/4 v2, 0x1

    :goto_7
    invoke-interface {v3, v2}, Lcom/sec/android/seccamera/SecCamera$OnHDRShotEventListener;->onHDRShotAllProgressCompleted(Z)V

    .line 4228
    :cond_1d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mHdrEventListener:Lcom/sec/android/seccamera/SecCamera$HdrEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$2500(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$HdrEventListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4229
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mHdrEventListener:Lcom/sec/android/seccamera/SecCamera$HdrEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$2500(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$HdrEventListener;

    move-result-object v3

    move-object/from16 v0, p1

    iget v2, v0, Landroid/os/Message;->arg1:I

    const/4 v4, 0x1

    if-ne v2, v4, :cond_1f

    const/4 v2, 0x1

    :goto_8
    invoke-interface {v3, v2}, Lcom/sec/android/seccamera/SecCamera$HdrEventListener;->onHdrAllProgressCompleted(Z)V

    goto/16 :goto_0

    .line 4226
    :cond_1e
    const/4 v2, 0x0

    goto :goto_7

    .line 4229
    :cond_1f
    const/4 v2, 0x0

    goto :goto_8

    .line 4234
    :sswitch_2b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnHDRShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnHDRShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$2400(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnHDRShotEventListener;

    move-result-object v2

    if-eqz v2, :cond_20

    .line 4235
    new-instance v14, Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, [B

    check-cast v2, [B

    invoke-direct {v14, v2}, Ljava/lang/String;-><init>([B)V

    .line 4236
    .local v14, "filename":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnHDRShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnHDRShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$2400(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnHDRShotEventListener;

    move-result-object v2

    invoke-interface {v2, v14}, Lcom/sec/android/seccamera/SecCamera$OnHDRShotEventListener;->onHDRShotYUVFileString(Ljava/lang/String;)V

    .line 4238
    .end local v14    # "filename":Ljava/lang/String;
    :cond_20
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mHdrEventListener:Lcom/sec/android/seccamera/SecCamera$HdrEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$2500(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$HdrEventListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4239
    new-instance v14, Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, [B

    check-cast v2, [B

    invoke-direct {v14, v2}, Ljava/lang/String;-><init>([B)V

    .line 4240
    .restart local v14    # "filename":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mHdrEventListener:Lcom/sec/android/seccamera/SecCamera$HdrEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$2500(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$HdrEventListener;

    move-result-object v2

    invoke-interface {v2, v14}, Lcom/sec/android/seccamera/SecCamera$HdrEventListener;->onHdrYUVFileString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 4245
    .end local v14    # "filename":Ljava/lang/String;
    :sswitch_2c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnObjectTrackingMsgListener:Lcom/sec/android/seccamera/SecCamera$OnObjectTrackingMsgListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$2600(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnObjectTrackingMsgListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4246
    const-string v2, "SecCamera-JNI-Java"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "HAL_MSG_OBJ_TRACKING :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p1

    iget v4, v0, Landroid/os/Message;->arg1:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4247
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnObjectTrackingMsgListener:Lcom/sec/android/seccamera/SecCamera$OnObjectTrackingMsgListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$2600(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnObjectTrackingMsgListener;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    invoke-interface {v2, v3}, Lcom/sec/android/seccamera/SecCamera$OnObjectTrackingMsgListener;->onObjectTrackingStatus(I)V

    goto/16 :goto_0

    .line 4252
    :sswitch_2d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnBurstShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnBurstShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$2700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnBurstShotEventListener;

    move-result-object v2

    if-eqz v2, :cond_21

    .line 4253
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnBurstShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnBurstShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$2700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnBurstShotEventListener;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    move-object/from16 v0, p1

    iget v4, v0, Landroid/os/Message;->arg2:I

    invoke-interface {v2, v3, v4}, Lcom/sec/android/seccamera/SecCamera$OnBurstShotEventListener;->onBurstShotCapturingProgressed(II)V

    .line 4255
    :cond_21
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mBurstEventListener:Lcom/sec/android/seccamera/SecCamera$BurstEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$2800(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$BurstEventListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4256
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mBurstEventListener:Lcom/sec/android/seccamera/SecCamera$BurstEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$2800(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$BurstEventListener;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    move-object/from16 v0, p1

    iget v4, v0, Landroid/os/Message;->arg2:I

    invoke-interface {v2, v3, v4}, Lcom/sec/android/seccamera/SecCamera$BurstEventListener;->onBurstCapturingProgressed(II)V

    goto/16 :goto_0

    .line 4261
    :sswitch_2e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnBurstShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnBurstShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$2700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnBurstShotEventListener;

    move-result-object v2

    if-eqz v2, :cond_22

    .line 4262
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnBurstShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnBurstShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$2700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnBurstShotEventListener;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    invoke-interface {v2, v3}, Lcom/sec/android/seccamera/SecCamera$OnBurstShotEventListener;->onBurstShotCapturingStopped(I)V

    .line 4264
    :cond_22
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mBurstEventListener:Lcom/sec/android/seccamera/SecCamera$BurstEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$2800(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$BurstEventListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4265
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mBurstEventListener:Lcom/sec/android/seccamera/SecCamera$BurstEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$2800(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$BurstEventListener;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    invoke-interface {v2, v3}, Lcom/sec/android/seccamera/SecCamera$BurstEventListener;->onBurstCapturingStopped(I)V

    goto/16 :goto_0

    .line 4270
    :sswitch_2f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnBurstShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnBurstShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$2700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnBurstShotEventListener;

    move-result-object v2

    if-eqz v2, :cond_23

    .line 4271
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnBurstShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnBurstShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$2700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnBurstShotEventListener;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    invoke-interface {v2, v3}, Lcom/sec/android/seccamera/SecCamera$OnBurstShotEventListener;->onBurstShotSavingCompleted(I)V

    .line 4273
    :cond_23
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mBurstEventListener:Lcom/sec/android/seccamera/SecCamera$BurstEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$2800(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$BurstEventListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4274
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mBurstEventListener:Lcom/sec/android/seccamera/SecCamera$BurstEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$2800(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$BurstEventListener;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    invoke-interface {v2, v3}, Lcom/sec/android/seccamera/SecCamera$BurstEventListener;->onBurstSavingCompleted(I)V

    goto/16 :goto_0

    .line 4279
    :sswitch_30
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnBurstShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnBurstShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$2700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnBurstShotEventListener;

    move-result-object v2

    if-eqz v2, :cond_24

    .line 4280
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnBurstShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnBurstShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$2700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnBurstShotEventListener;

    move-result-object v3

    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, [B

    check-cast v2, [B

    invoke-interface {v3, v2}, Lcom/sec/android/seccamera/SecCamera$OnBurstShotEventListener;->onBurstShotStringProgressed([B)V

    .line 4282
    :cond_24
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mBurstEventListener:Lcom/sec/android/seccamera/SecCamera$BurstEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$2800(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$BurstEventListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4283
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mBurstEventListener:Lcom/sec/android/seccamera/SecCamera$BurstEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$2800(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$BurstEventListener;

    move-result-object v3

    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, [B

    check-cast v2, [B

    invoke-interface {v3, v2}, Lcom/sec/android/seccamera/SecCamera$BurstEventListener;->onBurstStringProgressed([B)V

    goto/16 :goto_0

    .line 4287
    :sswitch_31
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnMultiFrameShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnMultiFrameShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$2900(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnMultiFrameShotEventListener;

    move-result-object v2

    if-eqz v2, :cond_25

    .line 4288
    const-string v2, "SecCamera-JNI-Java"

    const-string v3, "seccamera MULTI_FRAME_SHOT_PROGRESS_POSTPROCESSING "

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4289
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnMultiFrameShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnMultiFrameShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$2900(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnMultiFrameShotEventListener;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    move-object/from16 v0, p1

    iget v4, v0, Landroid/os/Message;->arg2:I

    invoke-interface {v2, v3, v4}, Lcom/sec/android/seccamera/SecCamera$OnMultiFrameShotEventListener;->onMultiFrameShotCapturingProgressed(II)V

    .line 4291
    :cond_25
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mMultiFrameEventListener:Lcom/sec/android/seccamera/SecCamera$MultiFrameEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$3000(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$MultiFrameEventListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4292
    const-string v2, "SecCamera-JNI-Java"

    const-string v3, "seccamera MULTI_FRAME_SHOT_PROGRESS_POSTPROCESSING "

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4293
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mMultiFrameEventListener:Lcom/sec/android/seccamera/SecCamera$MultiFrameEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$3000(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$MultiFrameEventListener;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    move-object/from16 v0, p1

    iget v4, v0, Landroid/os/Message;->arg2:I

    invoke-interface {v2, v3, v4}, Lcom/sec/android/seccamera/SecCamera$MultiFrameEventListener;->onMultiFrameCapturingProgressed(II)V

    goto/16 :goto_0

    .line 4297
    :sswitch_32
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnNotifyFirstPreviewFrameEventListener:Lcom/sec/android/seccamera/SecCamera$OnNotifyFirstPreviewFrameEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$3100(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnNotifyFirstPreviewFrameEventListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4298
    const-string v2, "SecCamera-JNI-Java"

    const-string v3, "Notify to get the first preview frame "

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4299
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnNotifyFirstPreviewFrameEventListener:Lcom/sec/android/seccamera/SecCamera$OnNotifyFirstPreviewFrameEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$3100(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnNotifyFirstPreviewFrameEventListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/seccamera/SecCamera$OnNotifyFirstPreviewFrameEventListener;->OnNotifyFirstPreviewFrame()V

    goto/16 :goto_0

    .line 4303
    :sswitch_33
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnGolfShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnGolfShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$3200(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnGolfShotEventListener;

    move-result-object v2

    if-eqz v2, :cond_26

    .line 4304
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnGolfShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnGolfShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$3200(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnGolfShotEventListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/seccamera/SecCamera$OnGolfShotEventListener;->onGolfShotCaptuered()V

    .line 4306
    :cond_26
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mGolfShotEventListener:Lcom/sec/android/seccamera/SecCamera$GolfShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$3300(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$GolfShotEventListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4307
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mGolfShotEventListener:Lcom/sec/android/seccamera/SecCamera$GolfShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$3300(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$GolfShotEventListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/seccamera/SecCamera$GolfShotEventListener;->onGolfShotCaptuered()V

    goto/16 :goto_0

    .line 4311
    :sswitch_34
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnGolfShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnGolfShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$3200(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnGolfShotEventListener;

    move-result-object v2

    if-eqz v2, :cond_27

    .line 4312
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnGolfShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnGolfShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$3200(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnGolfShotEventListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/seccamera/SecCamera$OnGolfShotEventListener;->onGolfShotCreatingStarted()V

    .line 4314
    :cond_27
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mGolfShotEventListener:Lcom/sec/android/seccamera/SecCamera$GolfShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$3300(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$GolfShotEventListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4315
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mGolfShotEventListener:Lcom/sec/android/seccamera/SecCamera$GolfShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$3300(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$GolfShotEventListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/seccamera/SecCamera$GolfShotEventListener;->onGolfShotCreatingStarted()V

    goto/16 :goto_0

    .line 4319
    :sswitch_35
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnGolfShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnGolfShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$3200(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnGolfShotEventListener;

    move-result-object v2

    if-eqz v2, :cond_28

    .line 4320
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnGolfShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnGolfShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$3200(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnGolfShotEventListener;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    invoke-interface {v2, v3}, Lcom/sec/android/seccamera/SecCamera$OnGolfShotEventListener;->onGolfShotCreatingProgress(I)V

    .line 4322
    :cond_28
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mGolfShotEventListener:Lcom/sec/android/seccamera/SecCamera$GolfShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$3300(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$GolfShotEventListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4323
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mGolfShotEventListener:Lcom/sec/android/seccamera/SecCamera$GolfShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$3300(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$GolfShotEventListener;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    invoke-interface {v2, v3}, Lcom/sec/android/seccamera/SecCamera$GolfShotEventListener;->onGolfShotCreatingProgress(I)V

    goto/16 :goto_0

    .line 4327
    :sswitch_36
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnGolfShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnGolfShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$3200(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnGolfShotEventListener;

    move-result-object v2

    if-eqz v2, :cond_29

    .line 4328
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnGolfShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnGolfShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$3200(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnGolfShotEventListener;

    move-result-object v3

    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, [B

    check-cast v2, [B

    invoke-interface {v3, v2}, Lcom/sec/android/seccamera/SecCamera$OnGolfShotEventListener;->onGolfShotCreatingCompleted([B)V

    .line 4330
    :cond_29
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mGolfShotEventListener:Lcom/sec/android/seccamera/SecCamera$GolfShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$3300(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$GolfShotEventListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4331
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mGolfShotEventListener:Lcom/sec/android/seccamera/SecCamera$GolfShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$3300(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$GolfShotEventListener;

    move-result-object v3

    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, [B

    check-cast v2, [B

    invoke-interface {v3, v2}, Lcom/sec/android/seccamera/SecCamera$GolfShotEventListener;->onGolfShotCreatingCompleted([B)V

    goto/16 :goto_0

    .line 4335
    :sswitch_37
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnGolfShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnGolfShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$3200(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnGolfShotEventListener;

    move-result-object v2

    if-eqz v2, :cond_2a

    .line 4336
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnGolfShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnGolfShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$3200(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnGolfShotEventListener;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    invoke-interface {v2, v3}, Lcom/sec/android/seccamera/SecCamera$OnGolfShotEventListener;->onGolfShotSavingProgress(I)V

    .line 4338
    :cond_2a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mGolfShotEventListener:Lcom/sec/android/seccamera/SecCamera$GolfShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$3300(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$GolfShotEventListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4339
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mGolfShotEventListener:Lcom/sec/android/seccamera/SecCamera$GolfShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$3300(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$GolfShotEventListener;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    invoke-interface {v2, v3}, Lcom/sec/android/seccamera/SecCamera$GolfShotEventListener;->onGolfShotSavingProgress(I)V

    goto/16 :goto_0

    .line 4343
    :sswitch_38
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnGolfShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnGolfShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$3200(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnGolfShotEventListener;

    move-result-object v2

    if-eqz v2, :cond_2b

    .line 4344
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnGolfShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnGolfShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$3200(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnGolfShotEventListener;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    invoke-interface {v2, v3}, Lcom/sec/android/seccamera/SecCamera$OnGolfShotEventListener;->onGolfShotError(I)V

    .line 4346
    :cond_2b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mGolfShotEventListener:Lcom/sec/android/seccamera/SecCamera$GolfShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$3300(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$GolfShotEventListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4347
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mGolfShotEventListener:Lcom/sec/android/seccamera/SecCamera$GolfShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$3300(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$GolfShotEventListener;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    invoke-interface {v2, v3}, Lcom/sec/android/seccamera/SecCamera$GolfShotEventListener;->onGolfShotError(I)V

    goto/16 :goto_0

    .line 4352
    :sswitch_39
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnDramaShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnDramaShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$3400(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnDramaShotEventListener;

    move-result-object v2

    if-eqz v2, :cond_2c

    .line 4353
    const-string v2, "SecCamera-JNI-Java"

    const-string v3, "DRAMA_SHOT_CAPTURING_PROGRESS"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 4354
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnDramaShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnDramaShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$3400(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnDramaShotEventListener;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    move-object/from16 v0, p1

    iget v4, v0, Landroid/os/Message;->arg2:I

    invoke-interface {v2, v3, v4}, Lcom/sec/android/seccamera/SecCamera$OnDramaShotEventListener;->onDramaShotCapturingProgress(II)V

    .line 4358
    :goto_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mDramaShotEventListener:Lcom/sec/android/seccamera/SecCamera$DramaShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$3500(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$DramaShotEventListener;

    move-result-object v2

    if-eqz v2, :cond_2d

    .line 4359
    const-string v2, "SecCamera-JNI-Java"

    const-string v3, "DRAMA_SHOT_CAPTURING_PROGRESS"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 4360
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mDramaShotEventListener:Lcom/sec/android/seccamera/SecCamera$DramaShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$3500(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$DramaShotEventListener;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    move-object/from16 v0, p1

    iget v4, v0, Landroid/os/Message;->arg2:I

    invoke-interface {v2, v3, v4}, Lcom/sec/android/seccamera/SecCamera$DramaShotEventListener;->onDramaShotCapturingProgress(II)V

    goto/16 :goto_0

    .line 4356
    :cond_2c
    const-string v2, "SecCamera-JNI-Java"

    const-string v3, "DRAMA_SHOT_CAPTURING_PROGRESS, mOnDramaShotEventListener is null !!!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_9

    .line 4362
    :cond_2d
    const-string v2, "SecCamera-JNI-Java"

    const-string v3, "DRAMA_SHOT_CAPTURING_PROGRESS, mOnDramaShotEventListener is null !!!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 4366
    :sswitch_3a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnDramaShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnDramaShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$3400(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnDramaShotEventListener;

    move-result-object v2

    if-eqz v2, :cond_2e

    .line 4367
    const-string v2, "SecCamera-JNI-Java"

    const-string v3, "DRAMA_SHOT_PROGRESS_POSTPROCESSING"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 4368
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnDramaShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnDramaShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$3400(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnDramaShotEventListener;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    invoke-interface {v2, v3}, Lcom/sec/android/seccamera/SecCamera$OnDramaShotEventListener;->onDramaShotSavingProgress(I)V

    .line 4372
    :goto_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mDramaShotEventListener:Lcom/sec/android/seccamera/SecCamera$DramaShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$3500(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$DramaShotEventListener;

    move-result-object v2

    if-eqz v2, :cond_2f

    .line 4373
    const-string v2, "SecCamera-JNI-Java"

    const-string v3, "DRAMA_SHOT_PROGRESS_POSTPROCESSING"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 4374
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mDramaShotEventListener:Lcom/sec/android/seccamera/SecCamera$DramaShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$3500(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$DramaShotEventListener;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    invoke-interface {v2, v3}, Lcom/sec/android/seccamera/SecCamera$DramaShotEventListener;->onDramaShotSavingProgress(I)V

    goto/16 :goto_0

    .line 4370
    :cond_2e
    const-string v2, "SecCamera-JNI-Java"

    const-string v3, "DRAMA_SHOT_PROGRESS_POSTPROCESSING, mOnDramaShotEventListener is null !!!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_a

    .line 4376
    :cond_2f
    const-string v2, "SecCamera-JNI-Java"

    const-string v3, "DRAMA_SHOT_PROGRESS_POSTPROCESSING, mOnDramaShotEventListener is null !!!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 4380
    :sswitch_3b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnDramaShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnDramaShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$3400(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnDramaShotEventListener;

    move-result-object v2

    if-eqz v2, :cond_30

    .line 4381
    const-string v2, "SecCamera-JNI-Java"

    const-string v3, "DRAMA_SHOT_ERROR"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 4382
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnDramaShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnDramaShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$3400(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnDramaShotEventListener;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    invoke-interface {v2, v3}, Lcom/sec/android/seccamera/SecCamera$OnDramaShotEventListener;->onDramaShotError(I)V

    .line 4386
    :goto_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mDramaShotEventListener:Lcom/sec/android/seccamera/SecCamera$DramaShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$3500(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$DramaShotEventListener;

    move-result-object v2

    if-eqz v2, :cond_31

    .line 4387
    const-string v2, "SecCamera-JNI-Java"

    const-string v3, "DRAMA_SHOT_ERROR"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 4388
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mDramaShotEventListener:Lcom/sec/android/seccamera/SecCamera$DramaShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$3500(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$DramaShotEventListener;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    invoke-interface {v2, v3}, Lcom/sec/android/seccamera/SecCamera$DramaShotEventListener;->onDramaShotError(I)V

    goto/16 :goto_0

    .line 4384
    :cond_30
    const-string v2, "SecCamera-JNI-Java"

    const-string v3, "DRAMA_SHOT_ERROR, mOnDramaShotEventListener is null !!!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_b

    .line 4390
    :cond_31
    const-string v2, "SecCamera-JNI-Java"

    const-string v3, "DRAMA_SHOT_ERROR, mOnDramaShotEventListener is null !!!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 4394
    :sswitch_3c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnDramaShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnDramaShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$3400(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnDramaShotEventListener;

    move-result-object v2

    if-eqz v2, :cond_32

    .line 4395
    const-string v2, "SecCamera-JNI-Java"

    const-string v3, "DRAMA_SHOT_INPUT_YUV_STRING"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 4396
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnDramaShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnDramaShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$3400(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnDramaShotEventListener;

    move-result-object v3

    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, [B

    check-cast v2, [B

    invoke-interface {v3, v2}, Lcom/sec/android/seccamera/SecCamera$OnDramaShotEventListener;->onDramaShotInputString([B)V

    .line 4400
    :goto_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mDramaShotEventListener:Lcom/sec/android/seccamera/SecCamera$DramaShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$3500(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$DramaShotEventListener;

    move-result-object v2

    if-eqz v2, :cond_33

    .line 4401
    const-string v2, "SecCamera-JNI-Java"

    const-string v3, "DRAMA_SHOT_INPUT_YUV_STRING"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 4402
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mDramaShotEventListener:Lcom/sec/android/seccamera/SecCamera$DramaShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$3500(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$DramaShotEventListener;

    move-result-object v3

    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, [B

    check-cast v2, [B

    invoke-interface {v3, v2}, Lcom/sec/android/seccamera/SecCamera$DramaShotEventListener;->onDramaShotInputString([B)V

    goto/16 :goto_0

    .line 4398
    :cond_32
    const-string v2, "SecCamera-JNI-Java"

    const-string v3, "DRAMA_SHOT_INPUT_YUV_STRING, mOnDramaShotEventListener is null !!!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_c

    .line 4404
    :cond_33
    const-string v2, "SecCamera-JNI-Java"

    const-string v3, "DRAMA_SHOT_INPUT_YUV_STRING, mOnDramaShotEventListener is null !!!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 4408
    :sswitch_3d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnDramaShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnDramaShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$3400(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnDramaShotEventListener;

    move-result-object v2

    if-eqz v2, :cond_34

    .line 4409
    const-string v2, "SecCamera-JNI-Java"

    const-string v3, "DRAMA_SHOT_RESULT_YUV_STRING"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 4410
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnDramaShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnDramaShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$3400(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnDramaShotEventListener;

    move-result-object v3

    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, [B

    check-cast v2, [B

    invoke-interface {v3, v2}, Lcom/sec/android/seccamera/SecCamera$OnDramaShotEventListener;->onDramaShotResultString([B)V

    .line 4414
    :goto_d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mDramaShotEventListener:Lcom/sec/android/seccamera/SecCamera$DramaShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$3500(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$DramaShotEventListener;

    move-result-object v2

    if-eqz v2, :cond_35

    .line 4415
    const-string v2, "SecCamera-JNI-Java"

    const-string v3, "DRAMA_SHOT_RESULT_YUV_STRING"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 4416
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mDramaShotEventListener:Lcom/sec/android/seccamera/SecCamera$DramaShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$3500(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$DramaShotEventListener;

    move-result-object v3

    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, [B

    check-cast v2, [B

    invoke-interface {v3, v2}, Lcom/sec/android/seccamera/SecCamera$DramaShotEventListener;->onDramaShotResultString([B)V

    goto/16 :goto_0

    .line 4412
    :cond_34
    const-string v2, "SecCamera-JNI-Java"

    const-string v3, "DRAMA_SHOT_RESULT_YUV_STRING, mOnDramaShotEventListener is null !!!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_d

    .line 4418
    :cond_35
    const-string v2, "SecCamera-JNI-Java"

    const-string v3, "DRAMA_SHOT_RESULT_YUV_STRING, mOnDramaShotEventListener is null !!!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 4423
    :sswitch_3e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnBeautyShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnBeautyShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$3600(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnBeautyShotEventListener;

    move-result-object v2

    if-eqz v2, :cond_36

    .line 4424
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnBeautyShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnBeautyShotEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$3600(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnBeautyShotEventListener;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    invoke-interface {v2, v3}, Lcom/sec/android/seccamera/SecCamera$OnBeautyShotEventListener;->onBeautyShotSavingProgress(I)V

    .line 4426
    :cond_36
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mBeautyEventListener:Lcom/sec/android/seccamera/SecCamera$BeautyEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$3700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$BeautyEventListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4427
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mBeautyEventListener:Lcom/sec/android/seccamera/SecCamera$BeautyEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$3700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$BeautyEventListener;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    invoke-interface {v2, v3}, Lcom/sec/android/seccamera/SecCamera$BeautyEventListener;->onBeautySavingProgress(I)V

    goto/16 :goto_0

    .line 4431
    :sswitch_3f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnAutoLowLightDetectionListener:Lcom/sec/android/seccamera/SecCamera$OnAutoLowLightDetectionListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$3800(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnAutoLowLightDetectionListener;

    move-result-object v2

    if-eqz v2, :cond_37

    .line 4432
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnAutoLowLightDetectionListener:Lcom/sec/android/seccamera/SecCamera$OnAutoLowLightDetectionListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$3800(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnAutoLowLightDetectionListener;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    invoke-interface {v2, v3}, Lcom/sec/android/seccamera/SecCamera$OnAutoLowLightDetectionListener;->onAutoLowLightDetectionChanged(I)V

    .line 4434
    :cond_37
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mAutoLowLightDetectionListener:Lcom/sec/android/seccamera/SecCamera$AutoLowLightDetectionListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$3900(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$AutoLowLightDetectionListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4435
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mAutoLowLightDetectionListener:Lcom/sec/android/seccamera/SecCamera$AutoLowLightDetectionListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$3900(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$AutoLowLightDetectionListener;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    invoke-interface {v2, v3}, Lcom/sec/android/seccamera/SecCamera$AutoLowLightDetectionListener;->onAutoLowLightDetectionChanged(I)V

    goto/16 :goto_0

    .line 4439
    :sswitch_40
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnLightConditionChangedListener:Lcom/sec/android/seccamera/SecCamera$OnLightConditionChangedListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$4000(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnLightConditionChangedListener;

    move-result-object v2

    if-eqz v2, :cond_38

    .line 4440
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnLightConditionChangedListener:Lcom/sec/android/seccamera/SecCamera$OnLightConditionChangedListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$4000(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnLightConditionChangedListener;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    invoke-interface {v2, v3}, Lcom/sec/android/seccamera/SecCamera$OnLightConditionChangedListener;->onLightConditionChanged(I)V

    .line 4442
    :cond_38
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mLightConditionChangedListener:Lcom/sec/android/seccamera/SecCamera$LightConditionChangedListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$4100(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$LightConditionChangedListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4443
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mLightConditionChangedListener:Lcom/sec/android/seccamera/SecCamera$LightConditionChangedListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$4100(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$LightConditionChangedListener;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    invoke-interface {v2, v3}, Lcom/sec/android/seccamera/SecCamera$LightConditionChangedListener;->onLightConditionChanged(I)V

    goto/16 :goto_0

    .line 4447
    :sswitch_41
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnSecImageEffectListner:Lcom/sec/android/seccamera/SecCamera$OnSecImageEffectListner;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$4200(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnSecImageEffectListner;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4448
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnSecImageEffectListner:Lcom/sec/android/seccamera/SecCamera$OnSecImageEffectListner;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$4200(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnSecImageEffectListner;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/seccamera/SecCamera$OnSecImageEffectListner;->onEffectShotCreatingStarted()V

    goto/16 :goto_0

    .line 4452
    :sswitch_42
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnSecImageEffectListner:Lcom/sec/android/seccamera/SecCamera$OnSecImageEffectListner;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$4200(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnSecImageEffectListner;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4453
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnSecImageEffectListner:Lcom/sec/android/seccamera/SecCamera$OnSecImageEffectListner;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$4200(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnSecImageEffectListner;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    invoke-interface {v2, v3}, Lcom/sec/android/seccamera/SecCamera$OnSecImageEffectListner;->onEffectShotCreatingProgress(I)V

    goto/16 :goto_0

    .line 4457
    :sswitch_43
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnSecImageEffectListner:Lcom/sec/android/seccamera/SecCamera$OnSecImageEffectListner;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$4200(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnSecImageEffectListner;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4458
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnSecImageEffectListner:Lcom/sec/android/seccamera/SecCamera$OnSecImageEffectListner;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$4200(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnSecImageEffectListner;

    move-result-object v3

    move-object/from16 v0, p1

    iget v2, v0, Landroid/os/Message;->arg1:I

    const/4 v4, 0x1

    if-ne v2, v4, :cond_39

    const/4 v2, 0x1

    :goto_e
    invoke-interface {v3, v2}, Lcom/sec/android/seccamera/SecCamera$OnSecImageEffectListner;->onEffectShotCreatingCompleted(Z)V

    goto/16 :goto_0

    :cond_39
    const/4 v2, 0x0

    goto :goto_e

    .line 4462
    :sswitch_44
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnDualEventListener:Lcom/sec/android/seccamera/SecCamera$OnDualEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$4300(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnDualEventListener;

    move-result-object v2

    if-eqz v2, :cond_3a

    .line 4463
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnDualEventListener:Lcom/sec/android/seccamera/SecCamera$OnDualEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$4300(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnDualEventListener;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    invoke-interface {v2, v3}, Lcom/sec/android/seccamera/SecCamera$OnDualEventListener;->onDualCaptureAvailable(I)V

    .line 4465
    :cond_3a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mDualEventListener:Lcom/sec/android/seccamera/SecCamera$DualEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$4400(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$DualEventListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4466
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mDualEventListener:Lcom/sec/android/seccamera/SecCamera$DualEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$4400(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$DualEventListener;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    invoke-interface {v2, v3}, Lcom/sec/android/seccamera/SecCamera$DualEventListener;->onDualCaptureAvailable(I)V

    goto/16 :goto_0

    .line 4470
    :sswitch_45
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnDualEventListener:Lcom/sec/android/seccamera/SecCamera$OnDualEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$4300(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnDualEventListener;

    move-result-object v2

    if-eqz v2, :cond_3b

    .line 4471
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnDualEventListener:Lcom/sec/android/seccamera/SecCamera$OnDualEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$4300(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnDualEventListener;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    invoke-interface {v2, v3}, Lcom/sec/android/seccamera/SecCamera$OnDualEventListener;->onDualTrackingAvailable(I)V

    .line 4473
    :cond_3b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mDualEventListener:Lcom/sec/android/seccamera/SecCamera$DualEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$4400(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$DualEventListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4474
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mDualEventListener:Lcom/sec/android/seccamera/SecCamera$DualEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$4400(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$DualEventListener;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    invoke-interface {v2, v3}, Lcom/sec/android/seccamera/SecCamera$DualEventListener;->onDualTrackingAvailable(I)V

    goto/16 :goto_0

    .line 4478
    :sswitch_46
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnSecImagingEventListener:Lcom/sec/android/seccamera/SecCamera$OnSecImagingEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$4500(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnSecImagingEventListener;

    move-result-object v2

    if-eqz v2, :cond_3c

    .line 4479
    new-instance v14, Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, [B

    check-cast v2, [B

    invoke-direct {v14, v2}, Ljava/lang/String;-><init>([B)V

    .line 4480
    .restart local v14    # "filename":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnSecImagingEventListener:Lcom/sec/android/seccamera/SecCamera$OnSecImagingEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$4500(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnSecImagingEventListener;

    move-result-object v2

    invoke-interface {v2, v14}, Lcom/sec/android/seccamera/SecCamera$OnSecImagingEventListener;->onSecImagingString(Ljava/lang/String;)V

    .line 4482
    .end local v14    # "filename":Ljava/lang/String;
    :cond_3c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mSecImagingEventListener:Lcom/sec/android/seccamera/SecCamera$SecImagingEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$4600(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$SecImagingEventListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4483
    new-instance v14, Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, [B

    check-cast v2, [B

    invoke-direct {v14, v2}, Ljava/lang/String;-><init>([B)V

    .line 4484
    .restart local v14    # "filename":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mSecImagingEventListener:Lcom/sec/android/seccamera/SecCamera$SecImagingEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$4600(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$SecImagingEventListener;

    move-result-object v2

    invoke-interface {v2, v14}, Lcom/sec/android/seccamera/SecCamera$SecImagingEventListener;->onSecImagingString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 4489
    .end local v14    # "filename":Ljava/lang/String;
    :sswitch_47
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnOutFocusShotModeEventListener:Lcom/sec/android/seccamera/SecCamera$OnOutFocusShotModeEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$4700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnOutFocusShotModeEventListener;

    move-result-object v2

    if-eqz v2, :cond_3d

    .line 4490
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnOutFocusShotModeEventListener:Lcom/sec/android/seccamera/SecCamera$OnOutFocusShotModeEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$4700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnOutFocusShotModeEventListener;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    move-object/from16 v0, p1

    iget v4, v0, Landroid/os/Message;->arg2:I

    invoke-interface {v2, v3, v4}, Lcom/sec/android/seccamera/SecCamera$OnOutFocusShotModeEventListener;->onOutFocusProcessProgress(II)V

    .line 4492
    :cond_3d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mSelectiveFocusEventListener:Lcom/sec/android/seccamera/SecCamera$SelectiveFocusEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$4800(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$SelectiveFocusEventListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4493
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mSelectiveFocusEventListener:Lcom/sec/android/seccamera/SecCamera$SelectiveFocusEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$4800(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$SelectiveFocusEventListener;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    move-object/from16 v0, p1

    iget v4, v0, Landroid/os/Message;->arg2:I

    invoke-interface {v2, v3, v4}, Lcom/sec/android/seccamera/SecCamera$SelectiveFocusEventListener;->onSelectiveFocusProcessProgress(II)V

    goto/16 :goto_0

    .line 4497
    :sswitch_48
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnOutFocusShotModeEventListener:Lcom/sec/android/seccamera/SecCamera$OnOutFocusShotModeEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$4700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnOutFocusShotModeEventListener;

    move-result-object v2

    if-eqz v2, :cond_3e

    .line 4498
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnOutFocusShotModeEventListener:Lcom/sec/android/seccamera/SecCamera$OnOutFocusShotModeEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$4700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnOutFocusShotModeEventListener;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    move-object/from16 v0, p1

    iget v4, v0, Landroid/os/Message;->arg2:I

    invoke-interface {v2, v3, v4}, Lcom/sec/android/seccamera/SecCamera$OnOutFocusShotModeEventListener;->onOutFocusCaptureProgress(II)V

    .line 4500
    :cond_3e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mSelectiveFocusEventListener:Lcom/sec/android/seccamera/SecCamera$SelectiveFocusEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$4800(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$SelectiveFocusEventListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4501
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mSelectiveFocusEventListener:Lcom/sec/android/seccamera/SecCamera$SelectiveFocusEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$4800(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$SelectiveFocusEventListener;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    move-object/from16 v0, p1

    iget v4, v0, Landroid/os/Message;->arg2:I

    invoke-interface {v2, v3, v4}, Lcom/sec/android/seccamera/SecCamera$SelectiveFocusEventListener;->onSelectiveFocusCaptureProgress(II)V

    goto/16 :goto_0

    .line 4505
    :sswitch_49
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnOutFocusShotModeEventListener:Lcom/sec/android/seccamera/SecCamera$OnOutFocusShotModeEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$4700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnOutFocusShotModeEventListener;

    move-result-object v2

    if-eqz v2, :cond_44

    .line 4506
    const/4 v13, 0x0

    .line 4507
    .local v13, "err_msg":I
    move-object/from16 v0, p1

    iget v2, v0, Landroid/os/Message;->arg1:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_3f

    move-object/from16 v0, p1

    iget v2, v0, Landroid/os/Message;->arg1:I

    const/4 v3, -0x3

    if-ne v2, v3, :cond_40

    :cond_3f
    const/4 v13, -0x1

    .line 4508
    :cond_40
    move-object/from16 v0, p1

    iget v2, v0, Landroid/os/Message;->arg1:I

    const/4 v3, -0x2

    if-ne v2, v3, :cond_41

    const/4 v13, -0x2

    .line 4509
    :cond_41
    move-object/from16 v0, p1

    iget v2, v0, Landroid/os/Message;->arg1:I

    const/16 v3, 0xb

    if-ge v2, v3, :cond_42

    move-object/from16 v0, p1

    iget v2, v0, Landroid/os/Message;->arg1:I

    const/4 v3, -0x4

    if-ne v2, v3, :cond_43

    :cond_42
    const/4 v13, -0x3

    .line 4510
    :cond_43
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnOutFocusShotModeEventListener:Lcom/sec/android/seccamera/SecCamera$OnOutFocusShotModeEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$4700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnOutFocusShotModeEventListener;

    move-result-object v2

    invoke-interface {v2, v13}, Lcom/sec/android/seccamera/SecCamera$OnOutFocusShotModeEventListener;->onOutFocusComplete(I)V

    .line 4512
    .end local v13    # "err_msg":I
    :cond_44
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mSelectiveFocusEventListener:Lcom/sec/android/seccamera/SecCamera$SelectiveFocusEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$4800(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$SelectiveFocusEventListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4513
    const/4 v13, 0x0

    .line 4514
    .restart local v13    # "err_msg":I
    move-object/from16 v0, p1

    iget v2, v0, Landroid/os/Message;->arg1:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_45

    move-object/from16 v0, p1

    iget v2, v0, Landroid/os/Message;->arg1:I

    const/4 v3, -0x3

    if-ne v2, v3, :cond_46

    :cond_45
    const/4 v13, -0x1

    .line 4515
    :cond_46
    move-object/from16 v0, p1

    iget v2, v0, Landroid/os/Message;->arg1:I

    const/4 v3, -0x2

    if-ne v2, v3, :cond_47

    const/4 v13, -0x2

    .line 4516
    :cond_47
    move-object/from16 v0, p1

    iget v2, v0, Landroid/os/Message;->arg1:I

    const/16 v3, 0xb

    if-ge v2, v3, :cond_48

    move-object/from16 v0, p1

    iget v2, v0, Landroid/os/Message;->arg1:I

    const/4 v3, -0x4

    if-ne v2, v3, :cond_49

    :cond_48
    const/4 v13, -0x3

    .line 4517
    :cond_49
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mSelectiveFocusEventListener:Lcom/sec/android/seccamera/SecCamera$SelectiveFocusEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$4800(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$SelectiveFocusEventListener;

    move-result-object v2

    invoke-interface {v2, v13}, Lcom/sec/android/seccamera/SecCamera$SelectiveFocusEventListener;->onSelectiveFocusComplete(I)V

    goto/16 :goto_0

    .line 4523
    .end local v13    # "err_msg":I
    :sswitch_4a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnMagicShotModeEventListener:Lcom/sec/android/seccamera/SecCamera$OnMagicShotModeEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$4900(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnMagicShotModeEventListener;

    move-result-object v2

    if-eqz v2, :cond_4a

    .line 4524
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnMagicShotModeEventListener:Lcom/sec/android/seccamera/SecCamera$OnMagicShotModeEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$4900(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnMagicShotModeEventListener;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    move-object/from16 v0, p1

    iget v4, v0, Landroid/os/Message;->arg2:I

    invoke-interface {v2, v3, v4}, Lcom/sec/android/seccamera/SecCamera$OnMagicShotModeEventListener;->onMagicShotCaptureProgress(II)V

    .line 4526
    :cond_4a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mShotAndMoreEventListener:Lcom/sec/android/seccamera/SecCamera$ShotAndMoreEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5000(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$ShotAndMoreEventListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4527
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mShotAndMoreEventListener:Lcom/sec/android/seccamera/SecCamera$ShotAndMoreEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5000(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$ShotAndMoreEventListener;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    move-object/from16 v0, p1

    iget v4, v0, Landroid/os/Message;->arg2:I

    invoke-interface {v2, v3, v4}, Lcom/sec/android/seccamera/SecCamera$ShotAndMoreEventListener;->onShotAndMoreCaptureProgress(II)V

    goto/16 :goto_0

    .line 4531
    :sswitch_4b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnMagicShotModeEventListener:Lcom/sec/android/seccamera/SecCamera$OnMagicShotModeEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$4900(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnMagicShotModeEventListener;

    move-result-object v2

    if-eqz v2, :cond_4b

    .line 4532
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnMagicShotModeEventListener:Lcom/sec/android/seccamera/SecCamera$OnMagicShotModeEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$4900(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnMagicShotModeEventListener;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    move-object/from16 v0, p1

    iget v4, v0, Landroid/os/Message;->arg2:I

    invoke-interface {v2, v3, v4}, Lcom/sec/android/seccamera/SecCamera$OnMagicShotModeEventListener;->onMagicShotProcessProgress(II)V

    .line 4534
    :cond_4b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mShotAndMoreEventListener:Lcom/sec/android/seccamera/SecCamera$ShotAndMoreEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5000(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$ShotAndMoreEventListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4535
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mShotAndMoreEventListener:Lcom/sec/android/seccamera/SecCamera$ShotAndMoreEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5000(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$ShotAndMoreEventListener;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    move-object/from16 v0, p1

    iget v4, v0, Landroid/os/Message;->arg2:I

    invoke-interface {v2, v3, v4}, Lcom/sec/android/seccamera/SecCamera$ShotAndMoreEventListener;->onShotAndMoreProcessProgress(II)V

    goto/16 :goto_0

    .line 4539
    :sswitch_4c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnMagicShotModeEventListener:Lcom/sec/android/seccamera/SecCamera$OnMagicShotModeEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$4900(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnMagicShotModeEventListener;

    move-result-object v2

    if-eqz v2, :cond_4c

    .line 4540
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnMagicShotModeEventListener:Lcom/sec/android/seccamera/SecCamera$OnMagicShotModeEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$4900(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnMagicShotModeEventListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/seccamera/SecCamera$OnMagicShotModeEventListener;->onMagicShotComplete()V

    .line 4542
    :cond_4c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mShotAndMoreEventListener:Lcom/sec/android/seccamera/SecCamera$ShotAndMoreEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5000(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$ShotAndMoreEventListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4543
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mShotAndMoreEventListener:Lcom/sec/android/seccamera/SecCamera$ShotAndMoreEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5000(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$ShotAndMoreEventListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/seccamera/SecCamera$ShotAndMoreEventListener;->onShotAndMoreComplete()V

    goto/16 :goto_0

    .line 4547
    :sswitch_4d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnMagicShotModeEventListener:Lcom/sec/android/seccamera/SecCamera$OnMagicShotModeEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$4900(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnMagicShotModeEventListener;

    move-result-object v2

    if-eqz v2, :cond_4d

    .line 4548
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnMagicShotModeEventListener:Lcom/sec/android/seccamera/SecCamera$OnMagicShotModeEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$4900(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnMagicShotModeEventListener;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    invoke-interface {v2, v3}, Lcom/sec/android/seccamera/SecCamera$OnMagicShotModeEventListener;->onMagicShotApplicableMode(I)V

    .line 4550
    :cond_4d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mShotAndMoreEventListener:Lcom/sec/android/seccamera/SecCamera$ShotAndMoreEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5000(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$ShotAndMoreEventListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4551
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mShotAndMoreEventListener:Lcom/sec/android/seccamera/SecCamera$ShotAndMoreEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5000(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$ShotAndMoreEventListener;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    invoke-interface {v2, v3}, Lcom/sec/android/seccamera/SecCamera$ShotAndMoreEventListener;->onShotAndMoreApplicableMode(I)V

    goto/16 :goto_0

    .line 4555
    :sswitch_4e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnHazeRemovalShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnHazeRemovalEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5100(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnHazeRemovalEventListener;

    move-result-object v2

    if-eqz v2, :cond_4e

    .line 4556
    const-string v2, "SecCamera-JNI-Java"

    const-string v3, "seccamera HAZE_REMOVAL_SHOT_PROGRESS_POSTPROCESSING "

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4557
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnHazeRemovalShotEventListener:Lcom/sec/android/seccamera/SecCamera$OnHazeRemovalEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5100(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnHazeRemovalEventListener;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    move-object/from16 v0, p1

    iget v4, v0, Landroid/os/Message;->arg2:I

    invoke-interface {v2, v3, v4}, Lcom/sec/android/seccamera/SecCamera$OnHazeRemovalEventListener;->onHazeRemovalCapturingProgressed(II)V

    .line 4559
    :cond_4e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mHazeRemovalShotEventListener:Lcom/sec/android/seccamera/SecCamera$HazeRemovalEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5200(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$HazeRemovalEventListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4560
    const-string v2, "SecCamera-JNI-Java"

    const-string v3, "seccamera HAZE_REMOVAL_SHOT_PROGRESS_POSTPROCESSING "

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4561
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mHazeRemovalShotEventListener:Lcom/sec/android/seccamera/SecCamera$HazeRemovalEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5200(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$HazeRemovalEventListener;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    move-object/from16 v0, p1

    iget v4, v0, Landroid/os/Message;->arg2:I

    invoke-interface {v2, v3, v4}, Lcom/sec/android/seccamera/SecCamera$HazeRemovalEventListener;->onHazeRemovalCapturingProgressed(II)V

    goto/16 :goto_0

    .line 4565
    :sswitch_4f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnCameraCurrentSettingListener:Lcom/sec/android/seccamera/SecCamera$OnCameraCurrentSettingListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5300(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnCameraCurrentSettingListener;

    move-result-object v2

    if-eqz v2, :cond_4f

    .line 4566
    new-instance v15, Lcom/sec/android/seccamera/SecCamera$CameraCurrentSet;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, [B

    check-cast v2, [B

    invoke-direct {v15, v3, v2}, Lcom/sec/android/seccamera/SecCamera$CameraCurrentSet;-><init>(Lcom/sec/android/seccamera/SecCamera;[B)V

    .line 4567
    .local v15, "mCurrentSet":Lcom/sec/android/seccamera/SecCamera$CameraCurrentSet;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnCameraCurrentSettingListener:Lcom/sec/android/seccamera/SecCamera$OnCameraCurrentSettingListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5300(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnCameraCurrentSettingListener;

    move-result-object v3

    iget-wide v4, v15, Lcom/sec/android/seccamera/SecCamera$CameraCurrentSet;->exposure_time:J

    iget-short v6, v15, Lcom/sec/android/seccamera/SecCamera$CameraCurrentSet;->iso:S

    iget-short v7, v15, Lcom/sec/android/seccamera/SecCamera$CameraCurrentSet;->lens_position_min:S

    iget-short v8, v15, Lcom/sec/android/seccamera/SecCamera$CameraCurrentSet;->lens_position_max:S

    iget-short v9, v15, Lcom/sec/android/seccamera/SecCamera$CameraCurrentSet;->lens_position_current:S

    iget-short v10, v15, Lcom/sec/android/seccamera/SecCamera$CameraCurrentSet;->driver_resolution:S

    invoke-interface/range {v3 .. v10}, Lcom/sec/android/seccamera/SecCamera$OnCameraCurrentSettingListener;->onCameraCurrentSet(JSSSSS)V

    .line 4571
    .end local v15    # "mCurrentSet":Lcom/sec/android/seccamera/SecCamera$CameraCurrentSet;
    :cond_4f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mCameraCurrentSettingListener:Lcom/sec/android/seccamera/SecCamera$CameraCurrentSettingListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5400(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$CameraCurrentSettingListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4572
    new-instance v15, Lcom/sec/android/seccamera/SecCamera$CameraCurrentSet;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, [B

    check-cast v2, [B

    invoke-direct {v15, v3, v2}, Lcom/sec/android/seccamera/SecCamera$CameraCurrentSet;-><init>(Lcom/sec/android/seccamera/SecCamera;[B)V

    .line 4573
    .restart local v15    # "mCurrentSet":Lcom/sec/android/seccamera/SecCamera$CameraCurrentSet;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mCameraCurrentSettingListener:Lcom/sec/android/seccamera/SecCamera$CameraCurrentSettingListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5400(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$CameraCurrentSettingListener;

    move-result-object v3

    iget-wide v4, v15, Lcom/sec/android/seccamera/SecCamera$CameraCurrentSet;->exposure_time:J

    iget-short v6, v15, Lcom/sec/android/seccamera/SecCamera$CameraCurrentSet;->iso:S

    iget-short v7, v15, Lcom/sec/android/seccamera/SecCamera$CameraCurrentSet;->lens_position_min:S

    iget-short v8, v15, Lcom/sec/android/seccamera/SecCamera$CameraCurrentSet;->lens_position_max:S

    iget-short v9, v15, Lcom/sec/android/seccamera/SecCamera$CameraCurrentSet;->lens_position_current:S

    iget-short v10, v15, Lcom/sec/android/seccamera/SecCamera$CameraCurrentSet;->driver_resolution:S

    invoke-interface/range {v3 .. v10}, Lcom/sec/android/seccamera/SecCamera$CameraCurrentSettingListener;->onCameraCurrentSet(JSSSSS)V

    goto/16 :goto_0

    .line 4579
    .end local v15    # "mCurrentSet":Lcom/sec/android/seccamera/SecCamera$CameraCurrentSet;
    :sswitch_50
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnPafResultListener:Lcom/sec/android/seccamera/SecCamera$OnPafResultListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5500(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnPafResultListener;

    move-result-object v2

    if-eqz v2, :cond_50

    .line 4580
    new-instance v16, Lcom/sec/android/seccamera/SecCamera$PafResult;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, [B

    check-cast v2, [B

    move-object/from16 v0, v16

    invoke-direct {v0, v3, v2}, Lcom/sec/android/seccamera/SecCamera$PafResult;-><init>(Lcom/sec/android/seccamera/SecCamera;[B)V

    .line 4581
    .local v16, "mPafResult":Lcom/sec/android/seccamera/SecCamera$PafResult;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnPafResultListener:Lcom/sec/android/seccamera/SecCamera$OnPafResultListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5500(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnPafResultListener;

    move-result-object v2

    move-object/from16 v0, v16

    iget-short v3, v0, Lcom/sec/android/seccamera/SecCamera$PafResult;->mode:S

    move-object/from16 v0, v16

    iget-short v4, v0, Lcom/sec/android/seccamera/SecCamera$PafResult;->goal_pos:S

    move-object/from16 v0, v16

    iget-short v5, v0, Lcom/sec/android/seccamera/SecCamera$PafResult;->reliability:S

    move-object/from16 v0, v16

    iget-short v6, v0, Lcom/sec/android/seccamera/SecCamera$PafResult;->lens_position_current:S

    move-object/from16 v0, v16

    iget-short v7, v0, Lcom/sec/android/seccamera/SecCamera$PafResult;->driver_resolution:S

    invoke-interface/range {v2 .. v7}, Lcom/sec/android/seccamera/SecCamera$OnPafResultListener;->onPafResult(SSSSS)V

    .line 4584
    .end local v16    # "mPafResult":Lcom/sec/android/seccamera/SecCamera$PafResult;
    :cond_50
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mPhaseAFCallback:Lcom/sec/android/seccamera/SecCamera$PhaseAFCallback;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5600(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$PhaseAFCallback;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4585
    new-instance v16, Lcom/sec/android/seccamera/SecCamera$PafResult;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, [B

    check-cast v2, [B

    move-object/from16 v0, v16

    invoke-direct {v0, v3, v2}, Lcom/sec/android/seccamera/SecCamera$PafResult;-><init>(Lcom/sec/android/seccamera/SecCamera;[B)V

    .line 4586
    .restart local v16    # "mPafResult":Lcom/sec/android/seccamera/SecCamera$PafResult;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mPhaseAFCallback:Lcom/sec/android/seccamera/SecCamera$PhaseAFCallback;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5600(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$PhaseAFCallback;

    move-result-object v2

    move-object/from16 v0, v16

    iget-short v3, v0, Lcom/sec/android/seccamera/SecCamera$PafResult;->mode:S

    move-object/from16 v0, v16

    iget-short v4, v0, Lcom/sec/android/seccamera/SecCamera$PafResult;->goal_pos:S

    move-object/from16 v0, v16

    iget-short v5, v0, Lcom/sec/android/seccamera/SecCamera$PafResult;->reliability:S

    move-object/from16 v0, v16

    iget-short v6, v0, Lcom/sec/android/seccamera/SecCamera$PafResult;->lens_position_current:S

    move-object/from16 v0, v16

    iget-short v7, v0, Lcom/sec/android/seccamera/SecCamera$PafResult;->driver_resolution:S

    invoke-interface/range {v2 .. v7}, Lcom/sec/android/seccamera/SecCamera$PhaseAFCallback;->onPhaseAF(SSSSS)V

    goto/16 :goto_0

    .line 4591
    .end local v16    # "mPafResult":Lcom/sec/android/seccamera/SecCamera$PafResult;
    :sswitch_51
    const-string v2, "SecCamera-JNI-Java"

    const-string v3, "SAMSUNG_ULTRA_WIDE_SELFIE_SHOT_COMPRESSED_IMAGE"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 4592
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mJpegCallback:Lcom/sec/android/seccamera/SecCamera$PictureCallback;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$200(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$PictureCallback;

    move-result-object v2

    if-eqz v2, :cond_51

    .line 4593
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mJpegCallback:Lcom/sec/android/seccamera/SecCamera$PictureCallback;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$200(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$PictureCallback;

    move-result-object v3

    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, [B

    check-cast v2, [B

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->mCamera:Lcom/sec/android/seccamera/SecCamera;

    invoke-interface {v3, v2, v4}, Lcom/sec/android/seccamera/SecCamera$PictureCallback;->onPictureTaken([BLcom/sec/android/seccamera/SecCamera;)V

    goto/16 :goto_0

    .line 4595
    :cond_51
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnUltraWideSelfieEventListener:Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;

    move-result-object v2

    if-eqz v2, :cond_52

    .line 4596
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnUltraWideSelfieEventListener:Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;->onUltraWideSelfieError(I)V

    .line 4598
    :cond_52
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mUltraWideSelfieEventListener:Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5800(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4599
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mUltraWideSelfieEventListener:Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5800(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;->onUltraWideSelfieError(I)V

    goto/16 :goto_0

    .line 4604
    :sswitch_52
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnUltraWideSelfieEventListener:Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;

    move-result-object v2

    if-eqz v2, :cond_53

    .line 4605
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnUltraWideSelfieEventListener:Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    invoke-interface {v2, v3}, Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;->onUltraWideSelfieError(I)V

    .line 4607
    :cond_53
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mUltraWideSelfieEventListener:Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5800(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4608
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mUltraWideSelfieEventListener:Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5800(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    invoke-interface {v2, v3}, Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;->onUltraWideSelfieError(I)V

    goto/16 :goto_0

    .line 4612
    :sswitch_53
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnUltraWideSelfieEventListener:Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;

    move-result-object v2

    if-eqz v2, :cond_54

    .line 4613
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnUltraWideSelfieEventListener:Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    invoke-interface {v2, v3}, Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;->onUltraWideSelfieDirectionChanged(I)V

    .line 4615
    :cond_54
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mUltraWideSelfieEventListener:Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5800(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4616
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mUltraWideSelfieEventListener:Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5800(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    invoke-interface {v2, v3}, Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;->onUltraWideSelfieDirectionChanged(I)V

    goto/16 :goto_0

    .line 4620
    :sswitch_54
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnUltraWideSelfieEventListener:Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;

    move-result-object v2

    if-eqz v2, :cond_55

    .line 4621
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnUltraWideSelfieEventListener:Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;

    move-result-object v3

    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, [B

    check-cast v2, [B

    invoke-interface {v3, v2}, Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;->onUltraWideSelfieRectChanged([B)V

    .line 4623
    :cond_55
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mUltraWideSelfieEventListener:Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5800(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4624
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mUltraWideSelfieEventListener:Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5800(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;

    move-result-object v3

    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, [B

    check-cast v2, [B

    invoke-interface {v3, v2}, Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;->onUltraWideSelfieRectChanged([B)V

    goto/16 :goto_0

    .line 4628
    :sswitch_55
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnUltraWideSelfieEventListener:Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;

    move-result-object v2

    if-eqz v2, :cond_56

    .line 4629
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnUltraWideSelfieEventListener:Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;->onUltraWideSelfieCapturedNew()V

    .line 4631
    :cond_56
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mUltraWideSelfieEventListener:Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5800(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4632
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mUltraWideSelfieEventListener:Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5800(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;->onUltraWideSelfieCapturedNew()V

    goto/16 :goto_0

    .line 4636
    :sswitch_56
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnUltraWideSelfieEventListener:Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;

    move-result-object v2

    if-eqz v2, :cond_57

    .line 4637
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnUltraWideSelfieEventListener:Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    invoke-interface {v2, v3}, Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;->onUltraWideSelfieProgressStitching(I)V

    .line 4639
    :cond_57
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mUltraWideSelfieEventListener:Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5800(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4640
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mUltraWideSelfieEventListener:Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5800(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    invoke-interface {v2, v3}, Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;->onUltraWideSelfieProgressStitching(I)V

    goto/16 :goto_0

    .line 4644
    :sswitch_57
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnUltraWideSelfieEventListener:Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;

    move-result-object v2

    if-eqz v2, :cond_58

    .line 4645
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnUltraWideSelfieEventListener:Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;->onUltraWideSelfieCaptured()V

    .line 4647
    :cond_58
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mUltraWideSelfieEventListener:Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5800(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4648
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mUltraWideSelfieEventListener:Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5800(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;->onUltraWideSelfieCaptured()V

    goto/16 :goto_0

    .line 4652
    :sswitch_58
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnUltraWideSelfieEventListener:Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;

    move-result-object v2

    if-eqz v2, :cond_59

    .line 4653
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnUltraWideSelfieEventListener:Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;

    move-result-object v3

    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, [B

    check-cast v2, [B

    invoke-interface {v3, v2}, Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;->onUltraWideSelfieLowResolutionData([B)V

    .line 4655
    :cond_59
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mUltraWideSelfieEventListener:Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5800(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4656
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mUltraWideSelfieEventListener:Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5800(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;

    move-result-object v3

    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, [B

    check-cast v2, [B

    invoke-interface {v3, v2}, Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;->onUltraWideSelfieLowResolutionData([B)V

    goto/16 :goto_0

    .line 4660
    :sswitch_59
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnUltraWideSelfieEventListener:Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;

    move-result-object v2

    if-eqz v2, :cond_5a

    .line 4661
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnUltraWideSelfieEventListener:Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;

    move-result-object v3

    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, [B

    check-cast v2, [B

    invoke-interface {v3, v2}, Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;->onUltraWideSelfieLivePreviewData([B)V

    .line 4663
    :cond_5a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mUltraWideSelfieEventListener:Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5800(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4664
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mUltraWideSelfieEventListener:Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5800(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;

    move-result-object v3

    move-object/from16 v0, p1

    iget-object v2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, [B

    check-cast v2, [B

    invoke-interface {v3, v2}, Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;->onUltraWideSelfieLivePreviewData([B)V

    goto/16 :goto_0

    .line 4668
    :sswitch_5a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnUltraWideSelfieEventListener:Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;

    move-result-object v2

    if-eqz v2, :cond_5b

    .line 4669
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnUltraWideSelfieEventListener:Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;->onUltraWideSelfieCapturedMaxFrames()V

    .line 4671
    :cond_5b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mUltraWideSelfieEventListener:Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5800(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4672
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mUltraWideSelfieEventListener:Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5800(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;->onUltraWideSelfieCapturedMaxFrames()V

    goto/16 :goto_0

    .line 4676
    :sswitch_5b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnUltraWideSelfieEventListener:Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;

    move-result-object v2

    if-eqz v2, :cond_5c

    .line 4677
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnUltraWideSelfieEventListener:Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;->onUltraWideSelfieMoveSlowly()V

    .line 4679
    :cond_5c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mUltraWideSelfieEventListener:Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5800(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4680
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mUltraWideSelfieEventListener:Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5800(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;->onUltraWideSelfieMoveSlowly()V

    goto/16 :goto_0

    .line 4684
    :sswitch_5c
    const-string v2, "SecCamera-JNI-Java"

    const-string v3, "ULTRA_WIDE_SELFIE_SHOT_NEXT_CAPTURE_POSITION"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 4685
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnUltraWideSelfieEventListener:Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;

    move-result-object v2

    if-eqz v2, :cond_5d

    .line 4686
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnUltraWideSelfieEventListener:Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    move-object/from16 v0, p1

    iget v4, v0, Landroid/os/Message;->arg2:I

    invoke-interface {v2, v3, v4}, Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;->onUltraWideSelfieNextCapturePosition(II)V

    .line 4688
    :cond_5d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mUltraWideSelfieEventListener:Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5800(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4689
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mUltraWideSelfieEventListener:Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5800(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    move-object/from16 v0, p1

    iget v4, v0, Landroid/os/Message;->arg2:I

    invoke-interface {v2, v3, v4}, Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;->onUltraWideSelfieNextCapturePosition(II)V

    goto/16 :goto_0

    .line 4693
    :sswitch_5d
    const-string v2, "SecCamera-JNI-Java"

    const-string v3, "ULTRA_WIDE_SELFIE_SHOT_SINGLE_CAPTURE_DONE"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 4694
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnUltraWideSelfieEventListener:Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;

    move-result-object v2

    if-eqz v2, :cond_5e

    .line 4695
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnUltraWideSelfieEventListener:Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5700(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/seccamera/SecCamera$OnUltraWideSelfieEventListener;->onUltraWideSelfieSingleCaptureDone()V

    .line 4697
    :cond_5e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mUltraWideSelfieEventListener:Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5800(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4698
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mUltraWideSelfieEventListener:Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5800(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;->onUltraWideSelfieSingleCaptureDone()V

    goto/16 :goto_0

    .line 4702
    :sswitch_5e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnAEResultListener:Lcom/sec/android/seccamera/SecCamera$OnAEResultListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5900(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnAEResultListener;

    move-result-object v2

    if-eqz v2, :cond_5f

    .line 4703
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mOnAEResultListener:Lcom/sec/android/seccamera/SecCamera$OnAEResultListener;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$5900(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$OnAEResultListener;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    invoke-interface {v2, v3}, Lcom/sec/android/seccamera/SecCamera$OnAEResultListener;->onAEResult(I)V

    .line 4705
    :cond_5f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mAutoExposureCallback:Lcom/sec/android/seccamera/SecCamera$AutoExposureCallback;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$6000(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$AutoExposureCallback;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 4706
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/seccamera/SecCamera$EventHandler;->this$0:Lcom/sec/android/seccamera/SecCamera;

    # getter for: Lcom/sec/android/seccamera/SecCamera;->mAutoExposureCallback:Lcom/sec/android/seccamera/SecCamera$AutoExposureCallback;
    invoke-static {v2}, Lcom/sec/android/seccamera/SecCamera;->access$6000(Lcom/sec/android/seccamera/SecCamera;)Lcom/sec/android/seccamera/SecCamera$AutoExposureCallback;

    move-result-object v2

    move-object/from16 v0, p1

    iget v3, v0, Landroid/os/Message;->arg1:I

    invoke-interface {v2, v3}, Lcom/sec/android/seccamera/SecCamera$AutoExposureCallback;->onAutoExposure(I)V

    goto/16 :goto_0

    .line 3908
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_8
        0x2 -> :sswitch_0
        0x4 -> :sswitch_5
        0x8 -> :sswitch_6
        0x10 -> :sswitch_3
        0x40 -> :sswitch_4
        0x80 -> :sswitch_1
        0x100 -> :sswitch_2
        0x400 -> :sswitch_7
        0x800 -> :sswitch_9
        0xf001 -> :sswitch_2c
        0xf021 -> :sswitch_c
        0xf022 -> :sswitch_e
        0xf023 -> :sswitch_f
        0xf024 -> :sswitch_10
        0xf025 -> :sswitch_11
        0xf026 -> :sswitch_d
        0xf027 -> :sswitch_12
        0xf028 -> :sswitch_13
        0xf029 -> :sswitch_14
        0xf02a -> :sswitch_15
        0xf031 -> :sswitch_16
        0xf032 -> :sswitch_17
        0xf033 -> :sswitch_18
        0xf041 -> :sswitch_19
        0xf042 -> :sswitch_1a
        0xf043 -> :sswitch_1b
        0xf044 -> :sswitch_1c
        0xf045 -> :sswitch_1e
        0xf046 -> :sswitch_1d
        0xf061 -> :sswitch_20
        0xf062 -> :sswitch_21
        0xf063 -> :sswitch_22
        0xf065 -> :sswitch_23
        0xf066 -> :sswitch_25
        0xf067 -> :sswitch_26
        0xf068 -> :sswitch_24
        0xf071 -> :sswitch_1f
        0xf081 -> :sswitch_27
        0xf082 -> :sswitch_28
        0xf083 -> :sswitch_29
        0xf084 -> :sswitch_2a
        0xf085 -> :sswitch_2b
        0xf091 -> :sswitch_2d
        0xf092 -> :sswitch_2e
        0xf093 -> :sswitch_2f
        0xf094 -> :sswitch_30
        0xf123 -> :sswitch_31
        0xf151 -> :sswitch_3e
        0xf171 -> :sswitch_32
        0xf191 -> :sswitch_34
        0xf192 -> :sswitch_35
        0xf193 -> :sswitch_36
        0xf194 -> :sswitch_37
        0xf195 -> :sswitch_33
        0xf196 -> :sswitch_38
        0xf201 -> :sswitch_a
        0xf221 -> :sswitch_39
        0xf222 -> :sswitch_3a
        0xf223 -> :sswitch_3b
        0xf224 -> :sswitch_3c
        0xf225 -> :sswitch_3d
        0xf231 -> :sswitch_3f
        0xf232 -> :sswitch_40
        0xf241 -> :sswitch_41
        0xf242 -> :sswitch_42
        0xf243 -> :sswitch_43
        0xf251 -> :sswitch_44
        0xf252 -> :sswitch_46
        0xf253 -> :sswitch_45
        0xf281 -> :sswitch_4e
        0xf291 -> :sswitch_b
        0xf301 -> :sswitch_52
        0xf302 -> :sswitch_54
        0xf303 -> :sswitch_55
        0xf304 -> :sswitch_56
        0xf305 -> :sswitch_57
        0xf306 -> :sswitch_53
        0xf307 -> :sswitch_58
        0xf308 -> :sswitch_59
        0xf309 -> :sswitch_5a
        0xf30a -> :sswitch_5b
        0xf310 -> :sswitch_5c
        0xf311 -> :sswitch_5d
        0xf312 -> :sswitch_51
        0xf321 -> :sswitch_47
        0xf322 -> :sswitch_48
        0xf323 -> :sswitch_49
        0xf331 -> :sswitch_4a
        0xf332 -> :sswitch_4b
        0xf333 -> :sswitch_4c
        0xf334 -> :sswitch_4d
        0xf341 -> :sswitch_50
        0xf342 -> :sswitch_4f
        0xf351 -> :sswitch_5e
    .end sparse-switch
.end method
