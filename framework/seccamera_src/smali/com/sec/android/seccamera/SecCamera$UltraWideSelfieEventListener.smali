.class public interface abstract Lcom/sec/android/seccamera/SecCamera$UltraWideSelfieEventListener;
.super Ljava/lang/Object;
.source "SecCamera.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/seccamera/SecCamera;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "UltraWideSelfieEventListener"
.end annotation


# virtual methods
.method public abstract onUltraWideSelfieCaptured()V
.end method

.method public abstract onUltraWideSelfieCapturedMaxFrames()V
.end method

.method public abstract onUltraWideSelfieCapturedNew()V
.end method

.method public abstract onUltraWideSelfieDirectionChanged(I)V
.end method

.method public abstract onUltraWideSelfieError(I)V
.end method

.method public abstract onUltraWideSelfieLivePreviewData([B)V
.end method

.method public abstract onUltraWideSelfieLowResolutionData([B)V
.end method

.method public abstract onUltraWideSelfieMoveSlowly()V
.end method

.method public abstract onUltraWideSelfieNextCapturePosition(II)V
.end method

.method public abstract onUltraWideSelfieProgressStitching(I)V
.end method

.method public abstract onUltraWideSelfieRectChanged([B)V
.end method

.method public abstract onUltraWideSelfieSingleCaptureDone()V
.end method
