.class public interface abstract Lcom/sec/android/seccamera/SecCamera$SelfieDetectionListener;
.super Ljava/lang/Object;
.source "SecCamera.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/seccamera/SecCamera;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SelfieDetectionListener"
.end annotation


# virtual methods
.method public abstract onHeadPoseDetected([B)V
.end method

.method public abstract onPalmDetected()V
.end method

.method public abstract onSmileDetected()V
.end method

.method public abstract onWinkDetected()V
.end method
