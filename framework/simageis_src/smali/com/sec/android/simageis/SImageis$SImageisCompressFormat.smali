.class public final enum Lcom/sec/android/simageis/SImageis$SImageisCompressFormat;
.super Ljava/lang/Enum;
.source "SImageis.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/simageis/SImageis;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SImageisCompressFormat"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/simageis/SImageis$SImageisCompressFormat;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/simageis/SImageis$SImageisCompressFormat;

.field public static final enum JPEGSQ:Lcom/sec/android/simageis/SImageis$SImageisCompressFormat;


# instance fields
.field final nativeInt:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 50
    new-instance v0, Lcom/sec/android/simageis/SImageis$SImageisCompressFormat;

    const-string v1, "JPEGSQ"

    const/16 v2, 0x64

    invoke-direct {v0, v1, v3, v2}, Lcom/sec/android/simageis/SImageis$SImageisCompressFormat;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/android/simageis/SImageis$SImageisCompressFormat;->JPEGSQ:Lcom/sec/android/simageis/SImageis$SImageisCompressFormat;

    .line 49
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/sec/android/simageis/SImageis$SImageisCompressFormat;

    sget-object v1, Lcom/sec/android/simageis/SImageis$SImageisCompressFormat;->JPEGSQ:Lcom/sec/android/simageis/SImageis$SImageisCompressFormat;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/android/simageis/SImageis$SImageisCompressFormat;->$VALUES:[Lcom/sec/android/simageis/SImageis$SImageisCompressFormat;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "nativeInt"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 53
    iput p3, p0, Lcom/sec/android/simageis/SImageis$SImageisCompressFormat;->nativeInt:I

    .line 54
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/simageis/SImageis$SImageisCompressFormat;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 49
    const-class v0, Lcom/sec/android/simageis/SImageis$SImageisCompressFormat;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/simageis/SImageis$SImageisCompressFormat;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/simageis/SImageis$SImageisCompressFormat;
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/sec/android/simageis/SImageis$SImageisCompressFormat;->$VALUES:[Lcom/sec/android/simageis/SImageis$SImageisCompressFormat;

    invoke-virtual {v0}, [Lcom/sec/android/simageis/SImageis$SImageisCompressFormat;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/simageis/SImageis$SImageisCompressFormat;

    return-object v0
.end method
