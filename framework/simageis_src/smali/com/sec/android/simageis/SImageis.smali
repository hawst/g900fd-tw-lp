.class public Lcom/sec/android/simageis/SImageis;
.super Ljava/lang/Object;
.source "SImageis.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/simageis/SImageis$SImageisCompressFormat;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "SImageis"

.field private static final WORKING_COMPRESS_STORAGE:I = 0x1000


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 34
    :try_start_0
    const-string v1, "simageis_jni"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 38
    .local v0, "ule":Ljava/lang/UnsatisfiedLinkError;
    :goto_0
    return-void

    .line 35
    .end local v0    # "ule":Ljava/lang/UnsatisfiedLinkError;
    :catch_0
    move-exception v0

    .line 36
    .restart local v0    # "ule":Ljava/lang/UnsatisfiedLinkError;
    const-string v1, "SImageis"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to load the native library : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    return-void
.end method

.method private static native compress(JIILjava/io/OutputStream;[B)Z
.end method

.method public static compress(Landroid/graphics/Bitmap;Lcom/sec/android/simageis/SImageis$SImageisCompressFormat;ILjava/io/OutputStream;)Z
    .locals 6
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;
    .param p1, "format"    # Lcom/sec/android/simageis/SImageis$SImageisCompressFormat;
    .param p2, "quality"    # I
    .param p3, "stream"    # Ljava/io/OutputStream;

    .prologue
    .line 79
    if-nez p3, :cond_0

    .line 80
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 83
    :cond_0
    if-ltz p2, :cond_1

    const/16 v0, 0x64

    if-le p2, v0, :cond_2

    .line 84
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "quality must be 0..100"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 87
    :cond_2
    iget-wide v0, p0, Landroid/graphics/Bitmap;->mNativeBitmap:J

    iget v2, p1, Lcom/sec/android/simageis/SImageis$SImageisCompressFormat;->nativeInt:I

    const/16 v3, 0x1000

    new-array v5, v3, [B

    move v3, p2

    move-object v4, p3

    invoke-static/range {v0 .. v5}, Lcom/sec/android/simageis/SImageis;->compress(JIILjava/io/OutputStream;[B)Z

    move-result v0

    return v0
.end method

.method private static native transcode(Ljava/lang/String;IILjava/io/OutputStream;[B)Z
.end method

.method public static transcode(Ljava/lang/String;Lcom/sec/android/simageis/SImageis$SImageisCompressFormat;ILjava/io/OutputStream;)Z
    .locals 2
    .param p0, "imagePath"    # Ljava/lang/String;
    .param p1, "format"    # Lcom/sec/android/simageis/SImageis$SImageisCompressFormat;
    .param p2, "quality"    # I
    .param p3, "stream"    # Ljava/io/OutputStream;

    .prologue
    .line 107
    if-nez p3, :cond_0

    .line 108
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 111
    :cond_0
    if-ltz p2, :cond_1

    const/16 v0, 0x64

    if-le p2, v0, :cond_2

    .line 112
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "quality must be 0..100"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 115
    :cond_2
    iget v0, p1, Lcom/sec/android/simageis/SImageis$SImageisCompressFormat;->nativeInt:I

    const/16 v1, 0x1000

    new-array v1, v1, [B

    invoke-static {p0, v0, p2, p3, v1}, Lcom/sec/android/simageis/SImageis;->transcode(Ljava/lang/String;IILjava/io/OutputStream;[B)Z

    move-result v0

    return v0
.end method
