.class Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;
.super Ljava/lang/Object;
.source "TwIndexScrollView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/touchwiz/widget/TwIndexScrollView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "IndexScroll"
.end annotation


# static fields
.field public static final DARK_THEME:I = 0x0

.field public static final FIRST_LETTER_NOT_RELEVANT_NOT_MULTI_LANGUAGE:I = -0x1

.field public static final LAST_LETTER_NOT_RELEVANT_NOT_MULTI_LANGUAGE:I = -0x1

.field public static final LEFT_HANDLE:I = 0x0

.field public static final LIGHT_THEME:I = 0x1

.field public static final NO_SELECTED_DOT_INDEX:I = -0x1

.field public static final NO_SELECTED_INDEX:I = -0x1

.field public static final RIGHT_HANDLE:I = 0x1

.field private static final TAG:Ljava/lang/String; = "IndexScroll"


# instance fields
.field private FLOAT_DIV_MULT_ERROR:F

.field private final debug:Z

.field private mAlphabetArray:[Ljava/lang/String;

.field private mAlphabetArrayFirstLetterIndex:I

.field private mAlphabetArrayLastLetterIndex:I

.field private mAlphabetArrayToDraw:[Ljava/lang/String;

.field private mAlphabetArrayWithDots:[Ljava/lang/String;

.field private mAlphabetSize:I

.field private mAlphabetToDrawSize:I

.field private mAlphabetWithDotsSize:I

.field private mBgDrawableDefault:Landroid/graphics/drawable/Drawable;

.field private mBgDrawablePressed:Landroid/graphics/drawable/Drawable;

.field private mBgDrawablePressed2ndAct:Landroid/graphics/drawable/Drawable;

.field private mBgRect:Landroid/graphics/Rect;

.field private mBgRectParamsSet:Z

.field private mBgRectWidth:I

.field private mContext:Landroid/content/Context;

.field private mDensity:F

.field private mDepth:I

.field private mDotRepresentations:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "[",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mHandleBgScrollDrawable:Landroid/graphics/drawable/Drawable;

.field private mHandleBgScrollDrawable2ndAct:Landroid/graphics/drawable/Drawable;

.field private mHandleTextColorPressed:I

.field private mHandleTextColorPressed2ndAct:I

.field private mHeight:I

.field private mIndexScrollViewTheme:I

.field private mItemHeight:F

.field private mItemWidth:I

.field private mItemWidthGap:I

.field private mMaxDepth:I

.field private mMinSeparatorHeight:F

.field private mOrigSelectedDotIndex:I

.field private mOrigSelectedIndex:I

.field private mPaint:Landroid/graphics/Paint;

.field private mPosition:I

.field private mScreenHeight:I

.field private mScrollTop:I

.field private mSelectedIndex:I

.field private mSelectedIndexPositionInOrigAlphabet:[I

.field private mSeparatorColor:I

.field private mSeparatorHeight:F

.field private mSeparatorLeftPos:I

.field private mSeparatorWidth:I

.field private mSubIndexScroll:Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;

.field private mTextBounds:Landroid/graphics/Rect;

.field private mTextColorDimmed:I

.field private mTextSize:I

.field private mWidth:I

.field private mWidthShift:I

.field private m_bAlphabetArrayWithDotsUsed:Z

.field private m_bIsAlphabetInit:Z

.field private m_bSubIndexScrollExists:Z

.field private mbSetDimensionns:Z

.field final synthetic this$0:Lcom/sec/android/touchwiz/widget/TwIndexScrollView;


# direct methods
.method public constructor <init>(Lcom/sec/android/touchwiz/widget/TwIndexScrollView;Landroid/content/Context;II)V
    .locals 4
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "height"    # I
    .param p4, "width"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v0, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1469
    iput-object p1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->this$0:Lcom/sec/android/touchwiz/widget/TwIndexScrollView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1334
    iput-boolean v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->debug:Z

    .line 1354
    iput-object v2, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetArray:[Ljava/lang/String;

    .line 1361
    iput v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetArrayFirstLetterIndex:I

    .line 1370
    iput v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetArrayLastLetterIndex:I

    .line 1378
    iput-object v2, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mDotRepresentations:Ljava/util/HashMap;

    .line 1388
    iput v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSelectedIndex:I

    .line 1394
    iput v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mOrigSelectedDotIndex:I

    .line 1399
    iput v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mOrigSelectedIndex:I

    .line 1423
    iput v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mPosition:I

    .line 1424
    iput v3, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mIndexScrollViewTheme:I

    .line 1427
    iput-object v2, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mBgDrawableDefault:Landroid/graphics/drawable/Drawable;

    .line 1428
    iput-object v2, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mBgDrawablePressed:Landroid/graphics/drawable/Drawable;

    .line 1434
    const v0, 0x3a83126f    # 0.001f

    iput v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->FLOAT_DIV_MULT_ERROR:F

    .line 1440
    iput v3, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSeparatorLeftPos:I

    .line 1449
    iput-object v2, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mHandleBgScrollDrawable2ndAct:Landroid/graphics/drawable/Drawable;

    .line 1450
    iput-object v2, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mBgDrawablePressed2ndAct:Landroid/graphics/drawable/Drawable;

    .line 1456
    iput-boolean v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->m_bIsAlphabetInit:Z

    .line 1470
    iput p3, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mHeight:I

    .line 1471
    iput p4, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mWidth:I

    .line 1472
    iput-boolean v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->m_bSubIndexScrollExists:Z

    .line 1473
    iput v3, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mDepth:I

    .line 1474
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mMaxDepth:I

    .line 1475
    iput v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mWidthShift:I

    .line 1476
    iput v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mScrollTop:I

    .line 1477
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mTextBounds:Landroid/graphics/Rect;

    .line 1478
    iput-boolean v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mBgRectParamsSet:Z

    .line 1479
    iput-object p2, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mContext:Landroid/content/Context;

    .line 1480
    invoke-direct {p0}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->init()V

    .line 1481
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/touchwiz/widget/TwIndexScrollView;Landroid/content/Context;IIII)V
    .locals 4
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "height"    # I
    .param p4, "width"    # I
    .param p5, "position"    # I
    .param p6, "theme"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v0, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1493
    iput-object p1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->this$0:Lcom/sec/android/touchwiz/widget/TwIndexScrollView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1334
    iput-boolean v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->debug:Z

    .line 1354
    iput-object v2, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetArray:[Ljava/lang/String;

    .line 1361
    iput v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetArrayFirstLetterIndex:I

    .line 1370
    iput v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetArrayLastLetterIndex:I

    .line 1378
    iput-object v2, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mDotRepresentations:Ljava/util/HashMap;

    .line 1388
    iput v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSelectedIndex:I

    .line 1394
    iput v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mOrigSelectedDotIndex:I

    .line 1399
    iput v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mOrigSelectedIndex:I

    .line 1423
    iput v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mPosition:I

    .line 1424
    iput v3, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mIndexScrollViewTheme:I

    .line 1427
    iput-object v2, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mBgDrawableDefault:Landroid/graphics/drawable/Drawable;

    .line 1428
    iput-object v2, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mBgDrawablePressed:Landroid/graphics/drawable/Drawable;

    .line 1434
    const v0, 0x3a83126f    # 0.001f

    iput v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->FLOAT_DIV_MULT_ERROR:F

    .line 1440
    iput v3, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSeparatorLeftPos:I

    .line 1449
    iput-object v2, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mHandleBgScrollDrawable2ndAct:Landroid/graphics/drawable/Drawable;

    .line 1450
    iput-object v2, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mBgDrawablePressed2ndAct:Landroid/graphics/drawable/Drawable;

    .line 1456
    iput-boolean v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->m_bIsAlphabetInit:Z

    .line 1494
    iput p3, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mHeight:I

    .line 1495
    iput p4, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mWidth:I

    .line 1496
    iput p5, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mPosition:I

    .line 1497
    iput p6, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mIndexScrollViewTheme:I

    .line 1499
    iput-boolean v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->m_bSubIndexScrollExists:Z

    .line 1500
    iput v3, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mDepth:I

    .line 1501
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mMaxDepth:I

    .line 1502
    iput v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mWidthShift:I

    .line 1503
    iput v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mScrollTop:I

    .line 1504
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mTextBounds:Landroid/graphics/Rect;

    .line 1505
    iput-boolean v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mBgRectParamsSet:Z

    .line 1506
    iput-object p2, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mContext:Landroid/content/Context;

    .line 1507
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mDensity:F

    .line 1508
    invoke-direct {p0}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->init()V

    .line 1510
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/touchwiz/widget/TwIndexScrollView;Landroid/content/Context;II[Ljava/lang/String;IIIIIII)V
    .locals 2
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "theme"    # I
    .param p4, "position"    # I
    .param p5, "alphabetArray"    # [Ljava/lang/String;
    .param p6, "scrollTop"    # I
    .param p7, "screenHeight"    # I
    .param p8, "height"    # I
    .param p9, "width"    # I
    .param p10, "widthShift"    # I
    .param p11, "maxDepth"    # I
    .param p12, "depth"    # I

    .prologue
    .line 1530
    iput-object p1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->this$0:Lcom/sec/android/touchwiz/widget/TwIndexScrollView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1334
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->debug:Z

    .line 1354
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetArray:[Ljava/lang/String;

    .line 1361
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetArrayFirstLetterIndex:I

    .line 1370
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetArrayLastLetterIndex:I

    .line 1378
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mDotRepresentations:Ljava/util/HashMap;

    .line 1388
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSelectedIndex:I

    .line 1394
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mOrigSelectedDotIndex:I

    .line 1399
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mOrigSelectedIndex:I

    .line 1423
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mPosition:I

    .line 1424
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mIndexScrollViewTheme:I

    .line 1427
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mBgDrawableDefault:Landroid/graphics/drawable/Drawable;

    .line 1428
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mBgDrawablePressed:Landroid/graphics/drawable/Drawable;

    .line 1434
    const v0, 0x3a83126f    # 0.001f

    iput v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->FLOAT_DIV_MULT_ERROR:F

    .line 1440
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSeparatorLeftPos:I

    .line 1449
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mHandleBgScrollDrawable2ndAct:Landroid/graphics/drawable/Drawable;

    .line 1450
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mBgDrawablePressed2ndAct:Landroid/graphics/drawable/Drawable;

    .line 1456
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->m_bIsAlphabetInit:Z

    .line 1531
    iput p7, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mScreenHeight:I

    .line 1532
    iput p8, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mHeight:I

    .line 1533
    iput p9, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mWidth:I

    .line 1534
    iput p10, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mWidthShift:I

    .line 1535
    iput p3, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mIndexScrollViewTheme:I

    .line 1536
    iput p4, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mPosition:I

    .line 1537
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mBgRectParamsSet:Z

    .line 1538
    iput p11, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mMaxDepth:I

    .line 1539
    iput p12, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mDepth:I

    .line 1540
    iput p6, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mScrollTop:I

    .line 1541
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->m_bSubIndexScrollExists:Z

    .line 1542
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mTextBounds:Landroid/graphics/Rect;

    .line 1543
    iput-object p2, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mContext:Landroid/content/Context;

    .line 1544
    invoke-direct {p0}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->init()V

    .line 1545
    const/4 v0, -0x1

    const/4 v1, -0x1

    invoke-virtual {p0, p5, v0, v1}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->setAlphabetArray([Ljava/lang/String;II)V

    .line 1546
    invoke-direct {p0}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->initAlphabetArrayWithDotsIfRequired()V

    .line 1547
    return-void
.end method

.method private adjustSeparatorHeightIfRequired()V
    .locals 3

    .prologue
    .line 1843
    iget v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mHeight:I

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSeparatorHeight:F

    iget v2, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mMinSeparatorHeight:F

    invoke-static {v1, v2}, Ljava/lang/Float;->compare(FF)I

    move-result v1

    if-nez v1, :cond_0

    .line 1845
    iget v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mHeight:I

    int-to-float v1, v1

    iget v2, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSeparatorHeight:F

    div-float/2addr v1, v2

    float-to-int v0, v1

    .line 1846
    .local v0, "numberOfCharacters":I
    iget v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mHeight:I

    int-to-float v1, v1

    int-to-float v2, v0

    div-float/2addr v1, v2

    iput v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSeparatorHeight:F

    .line 1848
    .end local v0    # "numberOfCharacters":I
    :cond_0
    return-void
.end method

.method private allocateBgRectangle()V
    .locals 6

    .prologue
    .line 2650
    const/4 v0, 0x0

    .line 2651
    .local v0, "left":I
    const/4 v1, 0x0

    .line 2653
    .local v1, "right":I
    iget v2, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mPosition:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 2655
    iget v2, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mWidth:I

    iget v3, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mWidthShift:I

    sub-int v1, v2, v3

    .line 2656
    iget v2, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mBgRectWidth:I

    sub-int v0, v1, v2

    .line 2661
    :goto_0
    new-instance v2, Landroid/graphics/Rect;

    iget v3, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mScrollTop:I

    iget v4, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mHeight:I

    iget v5, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mScrollTop:I

    add-int/2addr v4, v5

    invoke-direct {v2, v0, v3, v1, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v2, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mBgRect:Landroid/graphics/Rect;

    .line 2662
    return-void

    .line 2658
    :cond_0
    iget v2, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mWidthShift:I

    iget v3, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mBgRectWidth:I

    add-int v1, v2, v3

    .line 2659
    iget v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mWidthShift:I

    goto :goto_0
.end method

.method private calculateShift(III)I
    .locals 5
    .param p1, "lastIndexShift"    # I
    .param p2, "origFirstDotLocation"    # I
    .param p3, "origLastDotLocation"    # I

    .prologue
    .line 2058
    const/4 v0, 0x0

    .line 2059
    .local v0, "calculatedShift":I
    move v2, p2

    .line 2060
    .local v2, "upperGap":I
    iget v3, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetWithDotsSize:I

    add-int/lit8 v3, v3, -0x1

    sub-int v1, v3, p3

    .line 2063
    .local v1, "lowerGap":I
    sub-int v3, v2, v1

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    const/4 v4, 0x1

    if-le v3, v4, :cond_0

    .line 2066
    if-le v1, v2, :cond_1

    .line 2069
    sub-int v3, v1, v2

    div-int/lit8 v3, v3, 0x2

    iget v4, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetWithDotsSize:I

    sub-int/2addr v4, p1

    sub-int/2addr v4, p3

    add-int/lit8 v4, v4, -0x1

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 2078
    :cond_0
    :goto_0
    return v0

    .line 2075
    :cond_1
    sub-int v3, v1, v2

    div-int/lit8 v3, v3, 0x2

    iget v4, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetArrayFirstLetterIndex:I

    sub-int/2addr v4, p2

    add-int/lit8 v4, v4, 0x1

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_0
.end method

.method private drawAlphabetCharacters(Landroid/graphics/Canvas;)V
    .locals 12
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 2751
    iget-object v5, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mPaint:Landroid/graphics/Paint;

    iget v6, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mTextColorDimmed:I

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 2752
    iget-object v5, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mPaint:Landroid/graphics/Paint;

    iget v6, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mTextSize:I

    int-to-float v6, v6

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 2754
    const/4 v0, 0x0

    .local v0, "index":I
    :goto_0
    iget-object v5, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetArrayToDraw:[Ljava/lang/String;

    array-length v5, v5

    if-ge v0, v5, :cond_2

    .line 2756
    iget-object v5, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetArrayToDraw:[Ljava/lang/String;

    aget-object v1, v5, v0

    .line 2757
    .local v1, "text":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mPaint:Landroid/graphics/Paint;

    const/4 v6, 0x0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mTextBounds:Landroid/graphics/Rect;

    invoke-virtual {v5, v1, v6, v7, v8}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 2758
    iget-object v5, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v5, v1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v4

    .line 2759
    .local v4, "width":F
    iget-object v5, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mBgRect:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->centerX()I

    move-result v5

    int-to-float v5, v5

    const/high16 v6, 0x3f000000    # 0.5f

    mul-float/2addr v6, v4

    sub-float v2, v5, v6

    .line 2760
    .local v2, "textPosX":F
    iget v5, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mScrollTop:I

    int-to-float v5, v5

    iget v6, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSeparatorHeight:F

    float-to-double v6, v6

    int-to-double v8, v0

    const-wide/high16 v10, 0x3fe0000000000000L    # 0.5

    add-double/2addr v8, v10

    mul-double/2addr v6, v8

    iget-object v8, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mTextBounds:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->top:I

    int-to-float v8, v8

    const/high16 v9, 0x3f000000    # 0.5f

    mul-float/2addr v8, v9

    float-to-double v8, v8

    sub-double/2addr v6, v8

    double-to-float v6, v6

    add-float v3, v5, v6

    .line 2763
    .local v3, "textPosY":F
    iget v5, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSelectedIndex:I

    if-ne v0, v5, :cond_1

    .line 2765
    iget v5, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mDepth:I

    const/4 v6, 0x1

    if-ne v5, v6, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->getDepth()I

    move-result v5

    const/4 v6, 0x1

    if-le v5, v6, :cond_0

    .line 2766
    iget-object v5, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mPaint:Landroid/graphics/Paint;

    iget v6, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mHandleTextColorPressed2ndAct:I

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 2770
    :goto_1
    iget-object v5, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mPaint:Landroid/graphics/Paint;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    .line 2771
    iget-object v5, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetArrayToDraw:[Ljava/lang/String;

    aget-object v5, v5, v0

    iget-object v6, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v5, v2, v3, v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 2773
    iget-object v5, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mPaint:Landroid/graphics/Paint;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    .line 2774
    iget-object v5, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mPaint:Landroid/graphics/Paint;

    iget v6, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mTextColorDimmed:I

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 2754
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2768
    :cond_0
    iget-object v5, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mPaint:Landroid/graphics/Paint;

    iget v6, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mHandleTextColorPressed:I

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_1

    .line 2777
    :cond_1
    iget-object v5, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetArrayToDraw:[Ljava/lang/String;

    aget-object v5, v5, v0

    iget-object v6, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v5, v2, v3, v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto :goto_2

    .line 2780
    .end local v1    # "text":Ljava/lang/String;
    .end local v2    # "textPosX":F
    .end local v3    # "textPosY":F
    .end local v4    # "width":F
    :cond_2
    return-void
.end method

.method private drawBgRectangle(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v2, 0x1

    .line 2671
    iget-boolean v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mBgRectParamsSet:Z

    if-nez v0, :cond_0

    .line 2673
    invoke-direct {p0}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->setBgRectParams()V

    .line 2674
    iput-boolean v2, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mBgRectParamsSet:Z

    .line 2677
    :cond_0
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mBgDrawablePressed:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mDepth:I

    if-ne v0, v2, :cond_1

    invoke-virtual {p0}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->getDepth()I

    move-result v0

    if-ne v0, v2, :cond_1

    iget v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSelectedIndex:I

    if-ltz v0, :cond_1

    iget v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSelectedIndex:I

    iget v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetSize:I

    if-ge v0, v1, :cond_1

    .line 2679
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mBgDrawablePressed:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 2685
    :goto_0
    return-void

    .line 2680
    :cond_1
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mBgDrawablePressed2ndAct:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mDepth:I

    if-ne v0, v2, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->getDepth()I

    move-result v0

    if-le v0, v2, :cond_2

    iget v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSelectedIndex:I

    if-ltz v0, :cond_2

    iget v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSelectedIndex:I

    iget v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetSize:I

    if-ge v0, v1, :cond_2

    .line 2681
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mBgDrawablePressed2ndAct:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0

    .line 2683
    :cond_2
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mBgDrawableDefault:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method private drawSelectedIndexRect(Landroid/graphics/Canvas;)V
    .locals 10
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const-wide/high16 v8, 0x3ff8000000000000L    # 1.5

    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    .line 2789
    iget v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSelectedIndex:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSelectedIndex:I

    iget v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetSize:I

    if-lt v0, v1, :cond_1

    .line 2803
    :cond_0
    :goto_0
    return-void

    .line 2794
    :cond_1
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mHandleBgScrollDrawable2ndAct:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->getDepth()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_2

    .line 2795
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mHandleBgScrollDrawable2ndAct:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mBgRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget v2, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSeparatorHeight:F

    float-to-double v2, v2

    iget v4, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSelectedIndex:I

    int-to-double v4, v4

    sub-double/2addr v4, v6

    mul-double/2addr v2, v4

    double-to-int v2, v2

    iget-object v3, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mBgRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    iget v4, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSeparatorHeight:F

    float-to-double v4, v4

    iget v6, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSelectedIndex:I

    int-to-double v6, v6

    add-double/2addr v6, v8

    mul-double/2addr v4, v6

    double-to-int v4, v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 2797
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mHandleBgScrollDrawable2ndAct:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0

    .line 2799
    :cond_2
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mHandleBgScrollDrawable:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mBgRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget v2, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSeparatorHeight:F

    float-to-double v2, v2

    iget v4, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSelectedIndex:I

    int-to-double v4, v4

    sub-double/2addr v4, v6

    mul-double/2addr v2, v4

    double-to-int v2, v2

    iget-object v3, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mBgRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    iget v4, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSeparatorHeight:F

    float-to-double v4, v4

    iget v6, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSelectedIndex:I

    int-to-double v6, v6

    add-double/2addr v6, v8

    mul-double/2addr v4, v6

    double-to-int v4, v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 2801
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mHandleBgScrollDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method private drawSeparators(Landroid/graphics/Canvas;)V
    .locals 7
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 2725
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mBgRect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iput v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSeparatorLeftPos:I

    .line 2726
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSeparatorColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2728
    const/4 v6, 0x1

    .local v6, "index":I
    :goto_0
    iget v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetToDrawSize:I

    if-ge v6, v0, :cond_2

    .line 2731
    iget v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mDepth:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSelectedIndex:I

    if-eq v6, v0, :cond_0

    iget v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSelectedIndex:I

    add-int/lit8 v0, v0, 0x1

    if-ne v6, v0, :cond_1

    .line 2728
    :cond_0
    :goto_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 2736
    :cond_1
    iget v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSeparatorLeftPos:I

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mDensity:F

    add-float/2addr v1, v0

    iget v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mScrollTop:I

    int-to-float v0, v0

    iget v2, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSeparatorHeight:F

    int-to-float v3, v6

    mul-float/2addr v2, v3

    add-float/2addr v0, v2

    float-to-int v0, v0

    int-to-float v2, v0

    iget v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSeparatorLeftPos:I

    iget v3, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mBgRectWidth:I

    add-int/2addr v0, v3

    add-int/lit8 v0, v0, -0x1

    int-to-float v3, v0

    iget v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mScrollTop:I

    int-to-float v0, v0

    iget v4, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSeparatorHeight:F

    int-to-float v5, v6

    mul-float/2addr v4, v5

    add-float/2addr v0, v4

    float-to-int v0, v0

    int-to-float v4, v0

    iget-object v5, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto :goto_1

    .line 2741
    :cond_2
    return-void
.end method

.method private getDotIndexByY(I)Ljava/lang/String;
    .locals 14
    .param p1, "y"    # I

    .prologue
    .line 2415
    iget-boolean v6, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->m_bSubIndexScrollExists:Z

    if-eqz v6, :cond_0

    iget v6, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSelectedIndex:I

    iget v7, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mOrigSelectedIndex:I

    if-ne v6, v7, :cond_0

    iget v6, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mOrigSelectedDotIndex:I

    const/4 v7, -0x1

    if-eq v6, v7, :cond_0

    iget v7, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mOrigSelectedDotIndex:I

    iget-object v6, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mDotRepresentations:Ljava/util/HashMap;

    iget v8, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSelectedIndex:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Ljava/lang/String;

    array-length v6, v6

    if-ge v7, v6, :cond_0

    .line 2419
    iget-object v6, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mDotRepresentations:Ljava/util/HashMap;

    iget v7, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSelectedIndex:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Ljava/lang/String;

    iget v7, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mOrigSelectedDotIndex:I

    aget-object v6, v6, v7

    .line 2454
    :goto_0
    return-object v6

    .line 2422
    :cond_0
    iget-object v6, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mDotRepresentations:Ljava/util/HashMap;

    iget v7, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSelectedIndex:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Ljava/lang/String;

    array-length v4, v6

    .line 2426
    .local v4, "numberOfElements":I
    iget v6, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mDepth:I

    const/4 v7, 0x1

    if-ne v6, v7, :cond_2

    iget-object v6, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->this$0:Lcom/sec/android/touchwiz/widget/TwIndexScrollView;

    # getter for: Lcom/sec/android/touchwiz/widget/TwIndexScrollView;->MANY_ELEMENTS_REPRESENTED_BY_DOT:I
    invoke-static {v6}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView;->access$900(Lcom/sec/android/touchwiz/widget/TwIndexScrollView;)I

    move-result v6

    if-lt v4, v6, :cond_2

    .line 2428
    iget-object v6, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->this$0:Lcom/sec/android/touchwiz/widget/TwIndexScrollView;

    iget-object v7, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->this$0:Lcom/sec/android/touchwiz/widget/TwIndexScrollView;

    # getter for: Lcom/sec/android/touchwiz/widget/TwIndexScrollView;->MANY_ELEMENT_LOGIC:I
    invoke-static {v7}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView;->access$1100(Lcom/sec/android/touchwiz/widget/TwIndexScrollView;)I

    move-result v7

    # setter for: Lcom/sec/android/touchwiz/widget/TwIndexScrollView;->mScrollLogic:I
    invoke-static {v6, v7}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView;->access$1002(Lcom/sec/android/touchwiz/widget/TwIndexScrollView;I)I

    .line 2429
    iget v6, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mScrollTop:I

    int-to-double v6, v6

    iget v8, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSeparatorHeight:F

    float-to-double v8, v8

    iget v10, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSelectedIndex:I

    int-to-double v10, v10

    const-wide/high16 v12, 0x3fe0000000000000L    # 0.5

    sub-double/2addr v10, v12

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    double-to-int v5, v6

    .line 2430
    .local v5, "selectedIndexTop":I
    iget v6, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSeparatorHeight:F

    const/high16 v7, 0x40000000    # 2.0f

    mul-float v1, v6, v7

    .line 2439
    .local v1, "generalHeight":F
    :goto_1
    int-to-float v6, p1

    int-to-float v7, v5

    sub-float v3, v6, v7

    .line 2440
    .local v3, "internalHeight":F
    int-to-float v6, v4

    mul-float/2addr v6, v3

    div-float v0, v6, v1

    .line 2441
    .local v0, "floatIndex":F
    float-to-double v6, v0

    invoke-static {v6, v7}, Ljava/lang/Math;->floor(D)D

    move-result-wide v6

    double-to-int v2, v6

    .line 2444
    .local v2, "index":I
    if-lt v2, v4, :cond_3

    .line 2446
    add-int/lit8 v2, v4, -0x1

    .line 2452
    :cond_1
    :goto_2
    iput v2, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mOrigSelectedDotIndex:I

    .line 2453
    iget v6, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSelectedIndex:I

    iput v6, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mOrigSelectedIndex:I

    .line 2454
    iget-object v6, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mDotRepresentations:Ljava/util/HashMap;

    iget v7, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSelectedIndex:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Ljava/lang/String;

    aget-object v6, v6, v2

    goto :goto_0

    .line 2434
    .end local v0    # "floatIndex":F
    .end local v1    # "generalHeight":F
    .end local v2    # "index":I
    .end local v3    # "internalHeight":F
    .end local v5    # "selectedIndexTop":I
    :cond_2
    iget-object v6, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->this$0:Lcom/sec/android/touchwiz/widget/TwIndexScrollView;

    iget-object v7, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->this$0:Lcom/sec/android/touchwiz/widget/TwIndexScrollView;

    # getter for: Lcom/sec/android/touchwiz/widget/TwIndexScrollView;->FEW_ELEMENT_LOGIC:I
    invoke-static {v7}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView;->access$1200(Lcom/sec/android/touchwiz/widget/TwIndexScrollView;)I

    move-result v7

    # setter for: Lcom/sec/android/touchwiz/widget/TwIndexScrollView;->mScrollLogic:I
    invoke-static {v6, v7}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView;->access$1002(Lcom/sec/android/touchwiz/widget/TwIndexScrollView;I)I

    .line 2435
    iget v6, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mScrollTop:I

    int-to-float v6, v6

    iget v7, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSeparatorHeight:F

    iget v8, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSelectedIndex:I

    int-to-float v8, v8

    mul-float/2addr v7, v8

    add-float/2addr v6, v7

    float-to-int v5, v6

    .line 2436
    .restart local v5    # "selectedIndexTop":I
    iget v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSeparatorHeight:F

    .restart local v1    # "generalHeight":F
    goto :goto_1

    .line 2448
    .restart local v0    # "floatIndex":F
    .restart local v2    # "index":I
    .restart local v3    # "internalHeight":F
    :cond_3
    if-gez v2, :cond_1

    .line 2450
    const/4 v2, 0x0

    goto :goto_2
.end method

.method private getIndexByY(I)Ljava/lang/String;
    .locals 2
    .param p1, "y"    # I

    .prologue
    .line 2360
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mBgRect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    if-ge p1, v0, :cond_3

    .line 2362
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSelectedIndex:I

    .line 2378
    :cond_0
    :goto_0
    iget v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSelectedIndex:I

    iget v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetToDrawSize:I

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSelectedIndex:I

    iget v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetToDrawSize:I

    add-int/lit8 v1, v1, 0x1

    if-ne v0, v1, :cond_2

    .line 2380
    :cond_1
    iget v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetToDrawSize:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSelectedIndex:I

    .line 2383
    :cond_2
    iget v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSelectedIndex:I

    invoke-direct {p0, v0}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->isSelectedIndexDot(I)Z

    move-result v0

    if-nez v0, :cond_6

    .line 2385
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetArrayToDraw:[Ljava/lang/String;

    if-nez v0, :cond_5

    .line 2387
    const-string v0, ""

    .line 2396
    :goto_1
    return-object v0

    .line 2364
    :cond_3
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mBgRect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    if-le p1, v0, :cond_4

    .line 2366
    iget v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetToDrawSize:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSelectedIndex:I

    goto :goto_0

    .line 2370
    :cond_4
    iget v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mScrollTop:I

    sub-int v0, p1, v0

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSeparatorHeight:F

    div-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSelectedIndex:I

    .line 2371
    iget v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSelectedIndex:I

    iget v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetToDrawSize:I

    if-ne v0, v1, :cond_0

    .line 2373
    iget v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSelectedIndex:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSelectedIndex:I

    goto :goto_0

    .line 2391
    :cond_5
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetArrayToDraw:[Ljava/lang/String;

    iget v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSelectedIndex:I

    aget-object v0, v0, v1

    goto :goto_1

    .line 2396
    :cond_6
    invoke-direct {p0, p1}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->getDotIndexByY(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private init()V
    .locals 4

    .prologue
    const v3, 0x206000d

    .line 1790
    iget-object v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1791
    .local v0, "rsrc":Landroid/content/res/Resources;
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mPaint:Landroid/graphics/Paint;

    .line 1792
    iget-object v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mPaint:Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 1793
    iget-object v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 1795
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mItemWidth:I

    .line 1796
    const v1, 0x2060010

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mItemWidthGap:I

    .line 1798
    const v1, 0x206000b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mTextSize:I

    .line 1799
    const v1, 0x2060027

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    iput v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mMinSeparatorHeight:F

    .line 1800
    const v1, 0x2060028

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSeparatorWidth:I

    .line 1801
    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mBgRectWidth:I

    .line 1802
    iget v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mIndexScrollViewTheme:I

    invoke-virtual {p0, v1}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->setIndexScrollViewTheme(I)V

    .line 1804
    invoke-direct {p0}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->allocateBgRectangle()V

    .line 1806
    return-void
.end method

.method private initAlphabetArrayWithDotsIfRequired()V
    .locals 24

    .prologue
    .line 1860
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->m_bIsAlphabetInit:Z

    move/from16 v21, v0

    if-nez v21, :cond_0

    .line 2043
    :goto_0
    return-void

    .line 1864
    :cond_0
    const/16 v21, 0x0

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->m_bAlphabetArrayWithDotsUsed:Z

    .line 1866
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSeparatorHeight:F

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetSize:I

    move/from16 v22, v0

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    mul-float v21, v21, v22

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mHeight:I

    move/from16 v22, v0

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->FLOAT_DIV_MULT_ERROR:F

    move/from16 v23, v0

    add-float v22, v22, v23

    cmpg-float v21, v21, v22

    if-gtz v21, :cond_1

    .line 1868
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetArray:[Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetArrayToDraw:[Ljava/lang/String;

    .line 1869
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetSize:I

    move/from16 v21, v0

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetToDrawSize:I

    goto :goto_0

    .line 1875
    :cond_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mHeight:I

    move/from16 v21, v0

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSeparatorHeight:F

    move/from16 v22, v0

    div-float v21, v21, v22

    move/from16 v0, v21

    float-to-int v5, v0

    .line 1876
    .local v5, "alphabetWithDotsSizeCandidate":I
    const/16 v21, 0x5

    move/from16 v0, v21

    if-ge v5, v0, :cond_2

    .line 1878
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetArray:[Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetArrayToDraw:[Ljava/lang/String;

    .line 1879
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetSize:I

    move/from16 v21, v0

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetToDrawSize:I

    goto :goto_0

    .line 1882
    :cond_2
    new-instance v21, Ljava/util/HashMap;

    invoke-direct/range {v21 .. v21}, Ljava/util/HashMap;-><init>()V

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mDotRepresentations:Ljava/util/HashMap;

    .line 1883
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mHeight:I

    move/from16 v21, v0

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSeparatorHeight:F

    move/from16 v22, v0

    div-float v21, v21, v22

    move/from16 v0, v21

    float-to-int v0, v0

    move/from16 v21, v0

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetWithDotsSize:I

    .line 1884
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetWithDotsSize:I

    move/from16 v21, v0

    move/from16 v0, v21

    new-array v0, v0, [I

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSelectedIndexPositionInOrigAlphabet:[I

    .line 1886
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetWithDotsSize:I

    move/from16 v21, v0

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetArrayWithDots:[Ljava/lang/String;

    .line 1888
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetSize:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetWithDotsSize:I

    move/from16 v22, v0

    sub-int v15, v21, v22

    .line 1890
    .local v15, "numberOfMissingElements":I
    int-to-double v0, v15

    move-wide/from16 v22, v0

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v22

    move-wide/from16 v0, v22

    double-to-int v0, v0

    move/from16 v21, v0

    add-int/lit8 v12, v21, 0x1

    .line 1895
    .local v12, "numberOfDots":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetArrayLastLetterIndex:I

    move/from16 v21, v0

    const/16 v22, -0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_6

    .line 1897
    const/4 v11, 0x1

    .line 1907
    .local v11, "lastIndexShift":I
    :goto_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetWithDotsSize:I

    move/from16 v21, v0

    sub-int v21, v21, v11

    div-int v21, v21, v12

    const/16 v22, 0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-gt v0, v1, :cond_3

    .line 1909
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetWithDotsSize:I

    move/from16 v21, v0

    sub-int v21, v21, v11

    div-int/lit8 v12, v21, 0x2

    .line 1919
    :cond_3
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetWithDotsSize:I

    move/from16 v21, v0

    sub-int v21, v21, v11

    div-int v21, v21, v12

    const/16 v22, 0x2

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_4

    .line 1921
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetWithDotsSize:I

    move/from16 v21, v0

    sub-int v21, v21, v11

    div-int/lit8 v12, v21, 0x3

    .line 1924
    if-nez v12, :cond_4

    .line 1925
    const/4 v12, 0x1

    .line 1933
    :cond_4
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetArrayFirstLetterIndex:I

    move/from16 v21, v0

    const/16 v22, -0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-eq v0, v1, :cond_5

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetWithDotsSize:I

    move/from16 v21, v0

    sub-int v21, v21, v11

    div-int v21, v21, v12

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetArrayFirstLetterIndex:I

    move/from16 v22, v0

    add-int/lit8 v22, v22, 0x2

    move/from16 v0, v21

    move/from16 v1, v22

    if-ge v0, v1, :cond_5

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetWithDotsSize:I

    move/from16 v21, v0

    sub-int v21, v21, v11

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetArrayFirstLetterIndex:I

    move/from16 v22, v0

    add-int/lit8 v22, v22, 0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-le v0, v1, :cond_5

    .line 1937
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetWithDotsSize:I

    move/from16 v21, v0

    sub-int v21, v21, v11

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetArrayFirstLetterIndex:I

    move/from16 v22, v0

    add-int/lit8 v22, v22, 0x2

    div-int v12, v21, v22

    .line 1940
    :cond_5
    move/from16 v16, v12

    .line 1944
    .local v16, "numberOfRemainingDots":I
    add-int v21, v15, v12

    div-int v21, v21, v12

    add-int/lit8 v13, v21, 0x1

    .line 1951
    .local v13, "numberOfElementsRepresentedByEachDot":I
    :goto_2
    add-int/lit8 v21, v13, -0x1

    mul-int v21, v21, v12

    add-int v22, v15, v12

    move/from16 v0, v21

    move/from16 v1, v22

    if-lt v0, v1, :cond_7

    .line 1953
    add-int/lit8 v13, v13, -0x1

    goto :goto_2

    .line 1901
    .end local v11    # "lastIndexShift":I
    .end local v13    # "numberOfElementsRepresentedByEachDot":I
    .end local v16    # "numberOfRemainingDots":I
    :cond_6
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetArrayLastLetterIndex:I

    move/from16 v21, v0

    add-int/lit8 v11, v21, 0x1

    .restart local v11    # "lastIndexShift":I
    goto/16 :goto_1

    .line 1955
    .restart local v13    # "numberOfElementsRepresentedByEachDot":I
    .restart local v16    # "numberOfRemainingDots":I
    :cond_7
    const/4 v9, 0x0

    .line 1956
    .local v9, "index":I
    const/4 v4, 0x0

    .line 1962
    .local v4, "alphabetIndex":I
    add-int v21, v15, v12

    rem-int v14, v21, v13

    .line 1964
    .local v14, "numberOfElementsRepresentedByLastDot":I
    const/4 v6, 0x0

    .line 1966
    .local v6, "bPerfectDotsSpreadingExists":Z
    const/4 v10, 0x0

    .line 1970
    .local v10, "indexShift":I
    const/16 v21, 0x1

    move/from16 v0, v21

    if-ne v14, v0, :cond_c

    .line 1972
    add-int/lit8 v21, v12, -0x1

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-direct {v0, v1, v11}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->perfectDotsSpreadingExists(II)Z

    move-result v6

    .line 1973
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetWithDotsSize:I

    move/from16 v21, v0

    add-int/lit8 v21, v21, 0x1

    div-int v7, v21, v12

    .line 1982
    .local v7, "distanceBetweenConsecutiveDots":I
    :goto_3
    const/16 v17, 0x0

    .line 1983
    .local v17, "origFirstDotLocation":I
    const/16 v18, 0x0

    .line 1984
    .local v18, "origLastDotLocation":I
    if-nez v6, :cond_8

    .line 1986
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetWithDotsSize:I

    move/from16 v21, v0

    sub-int v21, v21, v11

    div-int v7, v21, v12

    .line 1987
    add-int/lit8 v17, v7, -0x1

    .line 1988
    const/16 v21, 0x1

    move/from16 v0, v21

    if-ne v14, v0, :cond_d

    add-int/lit8 v21, v12, -0x1

    mul-int v21, v21, v7

    add-int/lit8 v18, v21, -0x1

    .line 1989
    :goto_4
    move-object/from16 v0, p0

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-direct {v0, v11, v1, v2}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->calculateShift(III)I

    move-result v10

    .line 1992
    :cond_8
    :goto_5
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetWithDotsSize:I

    move/from16 v21, v0

    move/from16 v0, v21

    if-ge v9, v0, :cond_11

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetSize:I

    move/from16 v21, v0

    move/from16 v0, v21

    if-ge v4, v0, :cond_11

    .line 1994
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSelectedIndexPositionInOrigAlphabet:[I

    move-object/from16 v21, v0

    aput v4, v21, v9

    .line 1998
    if-eqz v6, :cond_9

    add-int/lit8 v21, v9, 0x1

    rem-int v21, v21, v7

    if-gtz v21, :cond_b

    :cond_9
    if-nez v6, :cond_e

    if-lez v10, :cond_a

    move/from16 v0, v17

    if-le v9, v0, :cond_b

    :cond_a
    add-int/lit8 v21, v9, 0x1

    sub-int v21, v21, v10

    rem-int v21, v21, v7

    if-gtz v21, :cond_b

    if-eqz v16, :cond_b

    const/16 v21, 0x1

    move/from16 v0, v16

    move/from16 v1, v21

    if-ne v0, v1, :cond_e

    const/16 v21, 0x1

    move/from16 v0, v21

    if-ne v14, v0, :cond_e

    .line 2009
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetArrayWithDots:[Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetArray:[Ljava/lang/String;

    move-object/from16 v22, v0

    aget-object v22, v22, v4

    aput-object v22, v21, v9

    .line 2010
    add-int/lit8 v4, v4, 0x1

    .line 2037
    :goto_6
    add-int/lit8 v9, v9, 0x1

    goto :goto_5

    .line 1978
    .end local v7    # "distanceBetweenConsecutiveDots":I
    .end local v17    # "origFirstDotLocation":I
    .end local v18    # "origLastDotLocation":I
    :cond_c
    move-object/from16 v0, p0

    invoke-direct {v0, v12, v11}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->perfectDotsSpreadingExists(II)Z

    move-result v6

    .line 1979
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetWithDotsSize:I

    move/from16 v21, v0

    add-int/lit8 v21, v21, 0x1

    add-int/lit8 v22, v12, 0x1

    div-int v7, v21, v22

    .restart local v7    # "distanceBetweenConsecutiveDots":I
    goto/16 :goto_3

    .line 1988
    .restart local v17    # "origFirstDotLocation":I
    .restart local v18    # "origLastDotLocation":I
    :cond_d
    mul-int v21, v7, v12

    add-int/lit8 v18, v21, -0x1

    goto :goto_4

    .line 2014
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetArrayWithDots:[Ljava/lang/String;

    move-object/from16 v21, v0

    const-string v22, "."

    aput-object v22, v21, v9

    .line 2015
    move/from16 v19, v4

    .line 2017
    .local v19, "prevAlphabetIndex":I
    const/16 v21, 0x1

    move/from16 v0, v16

    move/from16 v1, v21

    if-le v0, v1, :cond_f

    .line 2019
    add-int/2addr v4, v13

    .line 2029
    :goto_7
    sub-int v21, v4, v19

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v20, v0

    .line 2030
    .local v20, "tempDotAlphaBet":[Ljava/lang/String;
    move/from16 v8, v19

    .local v8, "i":I
    :goto_8
    if-ge v8, v4, :cond_10

    .line 2031
    sub-int v21, v8, v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetArray:[Ljava/lang/String;

    move-object/from16 v22, v0

    aget-object v22, v22, v8

    aput-object v22, v20, v21

    .line 2030
    add-int/lit8 v8, v8, 0x1

    goto :goto_8

    .line 2025
    .end local v8    # "i":I
    .end local v20    # "tempDotAlphaBet":[Ljava/lang/String;
    :cond_f
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetSize:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetWithDotsSize:I

    move/from16 v22, v0

    add-int/lit8 v22, v22, -0x1

    sub-int v22, v22, v9

    sub-int v4, v21, v22

    goto :goto_7

    .line 2033
    .restart local v8    # "i":I
    .restart local v20    # "tempDotAlphaBet":[Ljava/lang/String;
    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mDotRepresentations:Ljava/util/HashMap;

    move-object/from16 v21, v0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2034
    add-int/lit8 v16, v16, -0x1

    goto :goto_6

    .line 2040
    .end local v8    # "i":I
    .end local v19    # "prevAlphabetIndex":I
    .end local v20    # "tempDotAlphaBet":[Ljava/lang/String;
    :cond_11
    const/16 v21, 0x1

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->m_bAlphabetArrayWithDotsUsed:Z

    .line 2041
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetArrayWithDots:[Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetArrayToDraw:[Ljava/lang/String;

    .line 2042
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetArrayToDraw:[Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    array-length v0, v0

    move/from16 v21, v0

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetToDrawSize:I

    goto/16 :goto_0
.end method

.method private isInSelectedIndexRect(I)Z
    .locals 8
    .param p1, "y"    # I

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2486
    iget v2, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSelectedIndex:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    iget v2, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSelectedIndex:I

    iget v3, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetToDrawSize:I

    if-lt v2, v3, :cond_2

    :cond_0
    move v0, v1

    .line 2497
    :cond_1
    :goto_0
    return v0

    .line 2490
    :cond_2
    iget-object v2, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->this$0:Lcom/sec/android/touchwiz/widget/TwIndexScrollView;

    # getter for: Lcom/sec/android/touchwiz/widget/TwIndexScrollView;->mScrollLogic:I
    invoke-static {v2}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView;->access$1000(Lcom/sec/android/touchwiz/widget/TwIndexScrollView;)I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->this$0:Lcom/sec/android/touchwiz/widget/TwIndexScrollView;

    # getter for: Lcom/sec/android/touchwiz/widget/TwIndexScrollView;->FEW_ELEMENT_LOGIC:I
    invoke-static {v3}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView;->access$1200(Lcom/sec/android/touchwiz/widget/TwIndexScrollView;)I

    move-result v3

    if-ne v2, v3, :cond_4

    .line 2492
    iget v2, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSeparatorHeight:F

    iget v3, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSelectedIndex:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    float-to-int v2, v2

    if-lt p1, v2, :cond_3

    iget v2, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSeparatorHeight:F

    iget v3, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSelectedIndex:I

    add-int/lit8 v3, v3, 0x1

    int-to-float v3, v3

    mul-float/2addr v2, v3

    float-to-int v2, v2

    if-le p1, v2, :cond_1

    :cond_3
    move v0, v1

    goto :goto_0

    .line 2497
    :cond_4
    iget v2, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSeparatorHeight:F

    float-to-double v2, v2

    iget v4, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSelectedIndex:I

    int-to-double v4, v4

    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    sub-double/2addr v4, v6

    mul-double/2addr v2, v4

    double-to-int v2, v2

    if-lt p1, v2, :cond_5

    iget v2, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSeparatorHeight:F

    float-to-double v2, v2

    iget v4, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSelectedIndex:I

    int-to-double v4, v4

    const-wide/high16 v6, 0x3ff8000000000000L    # 1.5

    add-double/2addr v4, v6

    mul-double/2addr v2, v4

    double-to-int v2, v2

    if-le p1, v2, :cond_1

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method private isInSelectedIndexVerticalRange(I)Z
    .locals 1
    .param p1, "y"    # I

    .prologue
    .line 2475
    const/4 v0, 0x1

    return v0
.end method

.method private isSelectedIndexDot(I)Z
    .locals 3
    .param p1, "mSelectedIndex"    # I

    .prologue
    const/4 v0, 0x1

    .line 2345
    iget-boolean v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->m_bAlphabetArrayWithDotsUsed:Z

    if-ne v1, v0, :cond_0

    iget-object v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mDotRepresentations:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private perfectDotsSpreadingExists(II)Z
    .locals 4
    .param p1, "numberOfDots"    # I
    .param p2, "lastIndexShift"    # I

    .prologue
    const/4 v1, 0x0

    .line 2092
    iget v2, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetWithDotsSize:I

    add-int/lit8 v2, v2, 0x1

    add-int/lit8 v3, p1, 0x1

    rem-int/2addr v2, v3

    if-lez v2, :cond_1

    .line 2100
    :cond_0
    :goto_0
    return v1

    .line 2096
    :cond_1
    iget v2, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetWithDotsSize:I

    add-int/lit8 v2, v2, 0x1

    add-int/lit8 v3, p1, 0x1

    div-int v0, v2, v3

    .line 2100
    .local v0, "dotsGap":I
    add-int/lit8 v2, v0, -0x1

    iget v3, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetArrayFirstLetterIndex:I

    if-le v2, v3, :cond_0

    iget v2, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetWithDotsSize:I

    sub-int/2addr v2, v0

    iget v3, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetWithDotsSize:I

    sub-int/2addr v3, p2

    if-ge v2, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method private setBgRectParams()V
    .locals 6

    .prologue
    .line 2694
    const/4 v0, 0x0

    .line 2695
    .local v0, "left":I
    const/4 v1, 0x0

    .line 2697
    .local v1, "right":I
    iget v2, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mPosition:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    .line 2699
    iget v2, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mWidth:I

    iget v3, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mWidthShift:I

    sub-int v1, v2, v3

    .line 2700
    iget v2, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mBgRectWidth:I

    sub-int v0, v1, v2

    .line 2708
    :goto_0
    new-instance v2, Landroid/graphics/Rect;

    iget v3, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mScrollTop:I

    iget v4, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mHeight:I

    iget v5, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mScrollTop:I

    add-int/2addr v4, v5

    invoke-direct {v2, v0, v3, v1, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v2, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mBgRect:Landroid/graphics/Rect;

    .line 2709
    iget-object v2, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mBgDrawableDefault:Landroid/graphics/drawable/Drawable;

    iget-object v3, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mBgRect:Landroid/graphics/Rect;

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 2711
    iget-object v2, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mBgDrawablePressed:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_0

    .line 2712
    iget-object v2, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mBgDrawablePressed:Landroid/graphics/drawable/Drawable;

    iget-object v3, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mBgRect:Landroid/graphics/Rect;

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 2714
    :cond_0
    iget-object v2, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mBgDrawablePressed2ndAct:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_1

    .line 2715
    iget-object v2, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mBgDrawablePressed2ndAct:Landroid/graphics/drawable/Drawable;

    iget-object v3, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mBgRect:Landroid/graphics/Rect;

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 2716
    :cond_1
    return-void

    .line 2704
    :cond_2
    iget v2, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mWidthShift:I

    iget v3, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mBgRectWidth:I

    add-int v1, v2, v3

    .line 2705
    iget v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mWidthShift:I

    goto :goto_0
.end method


# virtual methods
.method public addSubIndex([Ljava/lang/String;)V
    .locals 14
    .param p1, "alphabetArray"    # [Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    .line 2112
    iget-boolean v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->m_bIsAlphabetInit:Z

    if-nez v0, :cond_1

    .line 2155
    :cond_0
    :goto_0
    return-void

    .line 2117
    :cond_1
    iget v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mDepth:I

    iget v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mMaxDepth:I

    if-eq v0, v1, :cond_0

    .line 2122
    if-eqz p1, :cond_0

    array-length v0, p1

    if-eqz v0, :cond_0

    .line 2127
    iget-boolean v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->m_bSubIndexScrollExists:Z

    if-ne v0, v2, :cond_2

    .line 2129
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSubIndexScroll:Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;

    invoke-virtual {v0, p1}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->addSubIndex([Ljava/lang/String;)V

    goto :goto_0

    .line 2134
    :cond_2
    iget v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSelectedIndex:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 2139
    iput-boolean v2, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->m_bSubIndexScrollExists:Z

    .line 2140
    array-length v13, p1

    .line 2143
    .local v13, "alphabetSize":I
    iget v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSeparatorHeight:F

    int-to-float v1, v13

    mul-float/2addr v0, v1

    const/high16 v1, 0x3f800000    # 1.0f

    add-float/2addr v0, v1

    float-to-int v0, v0

    iget v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mScreenHeight:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v8

    .line 2150
    .local v8, "height":I
    const/4 v0, 0x0

    iget v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mScrollTop:I

    iget v2, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSeparatorHeight:F

    float-to-double v2, v2

    iget v4, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSelectedIndex:I

    int-to-double v4, v4

    const-wide/high16 v10, 0x3fe0000000000000L    # 0.5

    add-double/2addr v4, v10

    mul-double/2addr v2, v4

    double-to-int v2, v2

    add-int/2addr v1, v2

    iget v2, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mScreenHeight:I

    sub-int/2addr v2, v8

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v6

    .line 2152
    .local v6, "scrollTop":I
    new-instance v0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;

    iget-object v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->this$0:Lcom/sec/android/touchwiz/widget/TwIndexScrollView;

    iget-object v2, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mContext:Landroid/content/Context;

    iget v3, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mIndexScrollViewTheme:I

    iget v4, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mPosition:I

    iget v7, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mScreenHeight:I

    iget v9, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mWidth:I

    iget v5, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mWidthShift:I

    iget v10, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mItemWidth:I

    iget v11, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mItemWidthGap:I

    add-int/2addr v10, v11

    add-int/2addr v10, v5

    iget v11, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mMaxDepth:I

    iget v5, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mDepth:I

    add-int/lit8 v12, v5, 0x1

    move-object v5, p1

    invoke-direct/range {v0 .. v12}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;-><init>(Lcom/sec/android/touchwiz/widget/TwIndexScrollView;Landroid/content/Context;II[Ljava/lang/String;IIIIIII)V

    iput-object v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSubIndexScroll:Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;

    goto :goto_0
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 1
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 2617
    iget-boolean v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->m_bIsAlphabetInit:Z

    if-nez v0, :cond_1

    .line 2624
    :cond_0
    :goto_0
    return-void

    .line 2621
    :cond_1
    invoke-virtual {p0, p1}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->drawScroll(Landroid/graphics/Canvas;)V

    .line 2622
    iget-boolean v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->m_bSubIndexScrollExists:Z

    if-eqz v0, :cond_0

    .line 2623
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSubIndexScroll:Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;

    invoke-virtual {v0, p1}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method public drawScroll(Landroid/graphics/Canvas;)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 2634
    invoke-direct {p0, p1}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->drawBgRectangle(Landroid/graphics/Canvas;)V

    .line 2635
    iget v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mDepth:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 2637
    invoke-direct {p0, p1}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->drawSelectedIndexRect(Landroid/graphics/Canvas;)V

    .line 2639
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->drawSeparators(Landroid/graphics/Canvas;)V

    .line 2640
    invoke-direct {p0, p1}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->drawAlphabetCharacters(Landroid/graphics/Canvas;)V

    .line 2641
    return-void
.end method

.method public getDepth()I
    .locals 1

    .prologue
    .line 1617
    iget-boolean v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->m_bSubIndexScrollExists:Z

    if-nez v0, :cond_0

    .line 1619
    iget v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mDepth:I

    .line 1623
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSubIndexScroll:Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;

    invoke-virtual {v0}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->getDepth()I

    move-result v0

    goto :goto_0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 1686
    iget v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mHeight:I

    return v0
.end method

.method public getIndexByPosition(II)Ljava/lang/String;
    .locals 8
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    const/4 v3, 0x0

    const/4 v7, 0x1

    .line 2244
    iget-object v4, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mBgRect:Landroid/graphics/Rect;

    if-nez v4, :cond_1

    .line 2245
    const-string v3, ""

    .line 2334
    :cond_0
    :goto_0
    return-object v3

    .line 2247
    :cond_1
    iget-boolean v4, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->m_bIsAlphabetInit:Z

    if-nez v4, :cond_2

    .line 2249
    const-string v3, ""

    goto :goto_0

    .line 2251
    :cond_2
    iget v4, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mPosition:I

    if-nez v4, :cond_3

    iget-object v4, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mBgRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    if-lt p1, v4, :cond_4

    :cond_3
    iget v4, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mPosition:I

    if-ne v4, v7, :cond_5

    iget-object v4, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mBgRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    if-le p1, v4, :cond_5

    .line 2253
    :cond_4
    const-string v3, ""

    goto :goto_0

    .line 2255
    :cond_5
    iget-object v4, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mBgRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    if-lt p1, v4, :cond_a

    iget-object v4, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mBgRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    if-gt p1, v4, :cond_a

    .line 2258
    iget v3, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mDepth:I

    if-ne v3, v7, :cond_9

    invoke-direct {p0, p2}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->isInSelectedIndexRect(I)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 2260
    iget v3, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSelectedIndex:I

    invoke-direct {p0, v3}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->isSelectedIndexDot(I)Z

    move-result v3

    if-nez v3, :cond_8

    .line 2262
    iget-object v3, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetArrayToDraw:[Ljava/lang/String;

    if-eqz v3, :cond_6

    iget v3, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSelectedIndex:I

    if-ltz v3, :cond_6

    iget v3, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSelectedIndex:I

    iget-object v4, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetArrayToDraw:[Ljava/lang/String;

    array-length v4, v4

    if-lt v3, v4, :cond_7

    .line 2263
    :cond_6
    const-string v3, ""

    goto :goto_0

    .line 2265
    :cond_7
    iget-object v3, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetArrayToDraw:[Ljava/lang/String;

    iget v4, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSelectedIndex:I

    aget-object v3, v3, v4

    goto :goto_0

    .line 2268
    :cond_8
    invoke-direct {p0, p2}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->getDotIndexByY(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 2272
    :cond_9
    invoke-direct {p0, p2}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->getIndexByY(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 2278
    :cond_a
    iget-boolean v4, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->m_bSubIndexScrollExists:Z

    if-nez v4, :cond_11

    .line 2280
    iget v4, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mDepth:I

    iget v5, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mMaxDepth:I

    if-ge v4, v5, :cond_c

    iget v4, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mPosition:I

    if-nez v4, :cond_b

    iget v4, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mWidthShift:I

    iget v5, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mItemWidth:I

    add-int/2addr v4, v5

    iget v5, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mItemWidthGap:I

    add-int/2addr v4, v5

    if-ge p1, v4, :cond_0

    :cond_b
    iget v4, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mPosition:I

    if-ne v4, v7, :cond_c

    iget v4, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mWidth:I

    iget v5, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mWidthShift:I

    sub-int/2addr v4, v5

    iget v5, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mItemWidth:I

    iget v6, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mItemWidthGap:I

    add-int/2addr v5, v6

    sub-int/2addr v4, v5

    if-le p1, v4, :cond_0

    .line 2289
    :cond_c
    iget v3, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mDepth:I

    if-ne v3, v7, :cond_10

    invoke-direct {p0, p2}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->isInSelectedIndexRect(I)Z

    move-result v3

    if-eqz v3, :cond_10

    .line 2291
    iget v3, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSelectedIndex:I

    invoke-direct {p0, v3}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->isSelectedIndexDot(I)Z

    move-result v3

    if-nez v3, :cond_f

    .line 2293
    iget-object v3, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetArrayToDraw:[Ljava/lang/String;

    if-eqz v3, :cond_d

    iget v3, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSelectedIndex:I

    if-ltz v3, :cond_d

    iget v3, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSelectedIndex:I

    iget-object v4, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetArrayToDraw:[Ljava/lang/String;

    array-length v4, v4

    if-lt v3, v4, :cond_e

    .line 2294
    :cond_d
    const-string v3, ""

    goto/16 :goto_0

    .line 2296
    :cond_e
    iget-object v3, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetArrayToDraw:[Ljava/lang/String;

    iget v4, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSelectedIndex:I

    aget-object v3, v3, v4

    goto/16 :goto_0

    .line 2299
    :cond_f
    invoke-direct {p0, p2}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->getDotIndexByY(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_0

    .line 2303
    :cond_10
    invoke-direct {p0, p2}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->getIndexByY(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_0

    .line 2311
    :cond_11
    iget v4, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSelectedIndex:I

    const/4 v5, -0x1

    if-ne v4, v5, :cond_12

    .line 2312
    const-string v0, ""

    .line 2330
    .local v0, "currentSubIndexString":Ljava/lang/String;
    :goto_1
    iget-object v4, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSubIndexScroll:Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;

    invoke-virtual {v4, p1, p2}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->getIndexByPosition(II)Ljava/lang/String;

    move-result-object v2

    .line 2331
    .local v2, "nextSubIndexStrings":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 2334
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_0

    .line 2315
    .end local v0    # "currentSubIndexString":Ljava/lang/String;
    .end local v2    # "nextSubIndexStrings":Ljava/lang/String;
    :cond_12
    iget v4, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSelectedIndex:I

    invoke-direct {p0, v4}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->isSelectedIndexDot(I)Z

    move-result v4

    if-nez v4, :cond_15

    .line 2318
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetArrayToDraw:[Ljava/lang/String;

    if-eqz v4, :cond_13

    iget v4, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSelectedIndex:I

    if-ltz v4, :cond_13

    iget v4, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSelectedIndex:I

    iget-object v5, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetArrayToDraw:[Ljava/lang/String;

    array-length v5, v5

    if-lt v4, v5, :cond_14

    .line 2319
    :cond_13
    const-string v3, ""

    goto/16 :goto_0

    .line 2321
    :cond_14
    iget-object v4, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetArrayToDraw:[Ljava/lang/String;

    iget v5, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSelectedIndex:I

    aget-object v0, v4, v5
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .restart local v0    # "currentSubIndexString":Ljava/lang/String;
    goto :goto_1

    .line 2322
    .end local v0    # "currentSubIndexString":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 2323
    .local v1, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    const-string v0, ""

    .line 2324
    .restart local v0    # "currentSubIndexString":Ljava/lang/String;
    goto :goto_1

    .line 2326
    .end local v0    # "currentSubIndexString":Ljava/lang/String;
    .end local v1    # "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    :cond_15
    invoke-direct {p0, p2}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->getDotIndexByY(I)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "currentSubIndexString":Ljava/lang/String;
    goto :goto_1
.end method

.method public getItemHeight()F
    .locals 1

    .prologue
    .line 1677
    iget v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mItemHeight:F

    return v0
.end method

.method public getItemPlusSpaceWidth()I
    .locals 2

    .prologue
    .line 1598
    iget v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mItemWidth:I

    iget v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mItemWidthGap:I

    add-int/2addr v0, v1

    return v0
.end method

.method public getItemWidth()I
    .locals 1

    .prologue
    .line 1607
    iget v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mItemWidth:I

    return v0
.end method

.method public getLangAlphabetArray()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 1576
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetArray:[Ljava/lang/String;

    return-object v0
.end method

.method public getMaxDepth()I
    .locals 1

    .prologue
    .line 1567
    iget v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mMaxDepth:I

    return v0
.end method

.method public getNumberOfSmallerOrEqualIndexes(I)I
    .locals 3
    .param p1, "x"    # I

    .prologue
    .line 1587
    iget v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mPosition:I

    if-nez v1, :cond_0

    move v0, p1

    .line 1588
    .local v0, "xCord":I
    :goto_0
    iget v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mItemWidth:I

    iget v2, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mItemWidthGap:I

    add-int/2addr v1, v2

    div-int v1, v0, v1

    add-int/lit8 v1, v1, 0x1

    return v1

    .line 1587
    .end local v0    # "xCord":I
    :cond_0
    iget v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mWidth:I

    sub-int v0, v1, p1

    goto :goto_0
.end method

.method public getPosition()I
    .locals 1

    .prologue
    .line 1634
    iget v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mPosition:I

    return v0
.end method

.method public getSelectedIndex()I
    .locals 3

    .prologue
    const/4 v0, -0x1

    .line 1650
    iget-boolean v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->m_bAlphabetArrayWithDotsUsed:Z

    if-nez v1, :cond_1

    .line 1652
    iget v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSelectedIndex:I

    .line 1665
    :cond_0
    :goto_0
    return v0

    .line 1656
    :cond_1
    iget-object v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSelectedIndexPositionInOrigAlphabet:[I

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSelectedIndex:I

    if-ltz v1, :cond_0

    iget v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSelectedIndex:I

    iget-object v2, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSelectedIndexPositionInOrigAlphabet:[I

    array-length v2, v2

    if-ge v1, v2, :cond_0

    .line 1659
    iget v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSelectedIndex:I

    invoke-direct {p0, v1}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->isSelectedIndexDot(I)Z

    move-result v1

    if-eqz v1, :cond_2

    iget v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mOrigSelectedDotIndex:I

    if-eq v1, v0, :cond_2

    .line 1661
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSelectedIndexPositionInOrigAlphabet:[I

    iget v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSelectedIndex:I

    aget v0, v0, v1

    iget v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mOrigSelectedDotIndex:I

    add-int/2addr v0, v1

    goto :goto_0

    .line 1665
    :cond_2
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSelectedIndexPositionInOrigAlphabet:[I

    iget v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSelectedIndex:I

    aget v0, v0, v1

    goto :goto_0
.end method

.method public hasSubIndex()Z
    .locals 1

    .prologue
    .line 2212
    iget-boolean v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->m_bSubIndexScrollExists:Z

    return v0
.end method

.method public isAlphabetInit()Z
    .locals 1

    .prologue
    .line 1556
    iget-boolean v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->m_bIsAlphabetInit:Z

    return v0
.end method

.method public removeAllSubIndexes()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 2186
    iget-boolean v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->m_bSubIndexScrollExists:Z

    if-nez v0, :cond_0

    .line 2203
    :goto_0
    return-void

    .line 2188
    :cond_0
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSubIndexScroll:Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;

    invoke-virtual {v0}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->hasSubIndex()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2190
    iput v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mOrigSelectedIndex:I

    .line 2191
    iput v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mOrigSelectedDotIndex:I

    .line 2192
    iput-boolean v2, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->m_bSubIndexScrollExists:Z

    .line 2193
    iput-object v3, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSubIndexScroll:Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;

    goto :goto_0

    .line 2197
    :cond_1
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSubIndexScroll:Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;

    invoke-virtual {v0}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->removeAllSubIndexes()V

    .line 2198
    iput v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mOrigSelectedIndex:I

    .line 2199
    iput v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mOrigSelectedDotIndex:I

    .line 2200
    iput-boolean v2, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->m_bSubIndexScrollExists:Z

    .line 2201
    iput-object v3, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSubIndexScroll:Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;

    goto :goto_0
.end method

.method public removeSubIndex()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 2163
    iget-boolean v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->m_bSubIndexScrollExists:Z

    if-nez v0, :cond_0

    .line 2178
    :goto_0
    return-void

    .line 2167
    :cond_0
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSubIndexScroll:Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;

    invoke-virtual {v0}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->hasSubIndex()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2169
    iput v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mOrigSelectedIndex:I

    .line 2170
    iput v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mOrigSelectedDotIndex:I

    .line 2171
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->m_bSubIndexScrollExists:Z

    .line 2172
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSubIndexScroll:Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;

    goto :goto_0

    .line 2176
    :cond_1
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSubIndexScroll:Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;

    invoke-virtual {v0}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->removeSubIndex()V

    goto :goto_0
.end method

.method public resetSelectedIndex()V
    .locals 1

    .prologue
    .line 2606
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSelectedIndex:I

    .line 2607
    return-void
.end method

.method public setAlphabetArray([Ljava/lang/String;II)V
    .locals 3
    .param p1, "alphabetArray"    # [Ljava/lang/String;
    .param p2, "alphabetArrayFirstLetterIndex"    # I
    .param p3, "alphabetArrayLastLetterIndex"    # I

    .prologue
    const/4 v2, 0x1

    .line 1818
    if-nez p1, :cond_0

    .line 1834
    :goto_0
    return-void

    .line 1822
    :cond_0
    iput-object p1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetArray:[Ljava/lang/String;

    .line 1823
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetArray:[Ljava/lang/String;

    array-length v0, v0

    iput v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetSize:I

    .line 1824
    iput p2, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetArrayFirstLetterIndex:I

    .line 1825
    iput p3, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetArrayLastLetterIndex:I

    .line 1829
    iget v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mHeight:I

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetSize:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mItemHeight:F

    .line 1830
    iget v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mItemHeight:F

    iget v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mMinSeparatorHeight:F

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSeparatorHeight:F

    .line 1832
    iput-boolean v2, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->m_bIsAlphabetInit:Z

    .line 1833
    iput-boolean v2, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mbSetDimensionns:Z

    goto :goto_0
.end method

.method public setDefaultIndexScrollWidth()V
    .locals 3

    .prologue
    const v2, 0x206000d

    .line 1712
    iget-object v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1713
    .local v0, "rsrc":Landroid/content/res/Resources;
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mItemWidth:I

    .line 1714
    const v1, 0x2060028

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSeparatorWidth:I

    .line 1715
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mBgRectWidth:I

    .line 1716
    invoke-direct {p0}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->allocateBgRectangle()V

    .line 1717
    return-void
.end method

.method public setDimensionns(II)V
    .locals 2
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 1763
    iget-boolean v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->m_bIsAlphabetInit:Z

    if-nez v0, :cond_1

    .line 1781
    :cond_0
    :goto_0
    return-void

    .line 1768
    :cond_1
    iget v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mWidth:I

    if-ne v0, p1, :cond_2

    iget v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mHeight:I

    if-ne v0, p2, :cond_2

    iget-boolean v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mbSetDimensionns:Z

    if-eqz v0, :cond_0

    .line 1772
    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mbSetDimensionns:Z

    .line 1773
    iput p1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mWidth:I

    .line 1774
    iput p2, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mHeight:I

    .line 1775
    iput p2, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mScreenHeight:I

    .line 1776
    iget v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mHeight:I

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mAlphabetSize:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mItemHeight:F

    .line 1777
    iget v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mItemHeight:F

    iget v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mMinSeparatorHeight:F

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSeparatorHeight:F

    .line 1778
    invoke-direct {p0}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->adjustSeparatorHeightIfRequired()V

    .line 1779
    invoke-direct {p0}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->setBgRectParams()V

    .line 1780
    invoke-direct {p0}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->initAlphabetArrayWithDotsIfRequired()V

    goto :goto_0
.end method

.method public setIndexScrollViewTheme(I)V
    .locals 5
    .param p1, "theme"    # I

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 2509
    iget-boolean v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->m_bSubIndexScrollExists:Z

    if-eqz v1, :cond_0

    .line 2598
    :goto_0
    return-void

    .line 2514
    :cond_0
    iput p1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mIndexScrollViewTheme:I

    .line 2515
    iget-object v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2518
    .local v0, "rsrc":Landroid/content/res/Resources;
    iget v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mIndexScrollViewTheme:I

    if-nez v1, :cond_4

    .line 2520
    const v1, 0x20500c9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSeparatorColor:I

    .line 2522
    const v1, 0x20500c4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mTextColorDimmed:I

    .line 2523
    const v1, 0x20500c3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mHandleTextColorPressed:I

    .line 2524
    const v1, 0x20500c5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mHandleTextColorPressed2ndAct:I

    .line 2526
    iget v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mPosition:I

    if-ne v1, v2, :cond_3

    .line 2528
    const v1, 0x2020239

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mBgDrawableDefault:Landroid/graphics/drawable/Drawable;

    .line 2529
    const v1, 0x2020227

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mBgDrawablePressed:Landroid/graphics/drawable/Drawable;

    .line 2530
    const v1, 0x2020237

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mHandleBgScrollDrawable:Landroid/graphics/drawable/Drawable;

    .line 2532
    const v1, 0x2020225

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mBgDrawablePressed2ndAct:Landroid/graphics/drawable/Drawable;

    .line 2533
    const v1, 0x2020235

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mHandleBgScrollDrawable2ndAct:Landroid/graphics/drawable/Drawable;

    .line 2544
    :goto_1
    iget v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mDepth:I

    if-le v1, v2, :cond_1

    .line 2546
    const v1, 0x202022b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mBgDrawableDefault:Landroid/graphics/drawable/Drawable;

    .line 2583
    :cond_1
    :goto_2
    iget-object v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->this$0:Lcom/sec/android/touchwiz/widget/TwIndexScrollView;

    # getter for: Lcom/sec/android/touchwiz/widget/TwIndexScrollView;->mEnableCustomTheme:Z
    invoke-static {v1}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView;->access$1300(Lcom/sec/android/touchwiz/widget/TwIndexScrollView;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2584
    iget-object v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->this$0:Lcom/sec/android/touchwiz/widget/TwIndexScrollView;

    # getter for: Lcom/sec/android/touchwiz/widget/TwIndexScrollView;->mCstmBgDrawableDefault:Landroid/graphics/drawable/Drawable;
    invoke-static {v1}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView;->access$1400(Lcom/sec/android/touchwiz/widget/TwIndexScrollView;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->this$0:Lcom/sec/android/touchwiz/widget/TwIndexScrollView;

    # getter for: Lcom/sec/android/touchwiz/widget/TwIndexScrollView;->mCstmBgDrawableDefault:Landroid/graphics/drawable/Drawable;
    invoke-static {v1}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView;->access$1400(Lcom/sec/android/touchwiz/widget/TwIndexScrollView;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    :goto_3
    iput-object v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mBgDrawableDefault:Landroid/graphics/drawable/Drawable;

    .line 2585
    iget-object v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->this$0:Lcom/sec/android/touchwiz/widget/TwIndexScrollView;

    # getter for: Lcom/sec/android/touchwiz/widget/TwIndexScrollView;->mCstmHandleBgScrollDrawable:Landroid/graphics/drawable/Drawable;
    invoke-static {v1}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView;->access$1500(Lcom/sec/android/touchwiz/widget/TwIndexScrollView;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->this$0:Lcom/sec/android/touchwiz/widget/TwIndexScrollView;

    # getter for: Lcom/sec/android/touchwiz/widget/TwIndexScrollView;->mCstmHandleBgScrollDrawable:Landroid/graphics/drawable/Drawable;
    invoke-static {v1}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView;->access$1500(Lcom/sec/android/touchwiz/widget/TwIndexScrollView;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    :goto_4
    iput-object v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mHandleBgScrollDrawable:Landroid/graphics/drawable/Drawable;

    .line 2586
    iget-object v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->this$0:Lcom/sec/android/touchwiz/widget/TwIndexScrollView;

    # getter for: Lcom/sec/android/touchwiz/widget/TwIndexScrollView;->mCstmSeparatorColor:I
    invoke-static {v1}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView;->access$1600(Lcom/sec/android/touchwiz/widget/TwIndexScrollView;)I

    move-result v1

    if-eq v1, v4, :cond_8

    iget-object v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->this$0:Lcom/sec/android/touchwiz/widget/TwIndexScrollView;

    # getter for: Lcom/sec/android/touchwiz/widget/TwIndexScrollView;->mCstmSeparatorColor:I
    invoke-static {v1}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView;->access$1600(Lcom/sec/android/touchwiz/widget/TwIndexScrollView;)I

    move-result v1

    :goto_5
    iput v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSeparatorColor:I

    .line 2587
    iget-object v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->this$0:Lcom/sec/android/touchwiz/widget/TwIndexScrollView;

    # getter for: Lcom/sec/android/touchwiz/widget/TwIndexScrollView;->mCstmTextColorDimmed:I
    invoke-static {v1}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView;->access$1700(Lcom/sec/android/touchwiz/widget/TwIndexScrollView;)I

    move-result v1

    if-eq v1, v4, :cond_9

    iget-object v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->this$0:Lcom/sec/android/touchwiz/widget/TwIndexScrollView;

    # getter for: Lcom/sec/android/touchwiz/widget/TwIndexScrollView;->mCstmTextColorDimmed:I
    invoke-static {v1}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView;->access$1700(Lcom/sec/android/touchwiz/widget/TwIndexScrollView;)I

    move-result v1

    :goto_6
    iput v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mTextColorDimmed:I

    .line 2588
    iput-object v3, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mBgDrawablePressed2ndAct:Landroid/graphics/drawable/Drawable;

    .line 2589
    iput-object v3, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mHandleBgScrollDrawable2ndAct:Landroid/graphics/drawable/Drawable;

    .line 2591
    iget v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mDepth:I

    if-le v1, v2, :cond_2

    .line 2593
    iget-object v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->this$0:Lcom/sec/android/touchwiz/widget/TwIndexScrollView;

    # getter for: Lcom/sec/android/touchwiz/widget/TwIndexScrollView;->mCstmBgDrawableDefaultMoreDepth:Landroid/graphics/drawable/Drawable;
    invoke-static {v1}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView;->access$1800(Lcom/sec/android/touchwiz/widget/TwIndexScrollView;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v1, :cond_a

    iget-object v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->this$0:Lcom/sec/android/touchwiz/widget/TwIndexScrollView;

    # getter for: Lcom/sec/android/touchwiz/widget/TwIndexScrollView;->mCstmBgDrawableDefaultMoreDepth:Landroid/graphics/drawable/Drawable;
    invoke-static {v1}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView;->access$1800(Lcom/sec/android/touchwiz/widget/TwIndexScrollView;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    :goto_7
    iput-object v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mBgDrawableDefault:Landroid/graphics/drawable/Drawable;

    .line 2597
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->setBgRectParams()V

    goto/16 :goto_0

    .line 2537
    :cond_3
    const v1, 0x2020231

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mBgDrawableDefault:Landroid/graphics/drawable/Drawable;

    .line 2538
    const v1, 0x2020223

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mBgDrawablePressed:Landroid/graphics/drawable/Drawable;

    .line 2539
    const v1, 0x202022f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mHandleBgScrollDrawable:Landroid/graphics/drawable/Drawable;

    .line 2541
    const v1, 0x2020221

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mBgDrawablePressed2ndAct:Landroid/graphics/drawable/Drawable;

    .line 2542
    const v1, 0x202022d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mHandleBgScrollDrawable2ndAct:Landroid/graphics/drawable/Drawable;

    goto/16 :goto_1

    .line 2553
    :cond_4
    const v1, 0x20500d4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSeparatorColor:I

    .line 2554
    const v1, 0x20500cf

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mTextColorDimmed:I

    .line 2555
    const v1, 0x20500ce

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mHandleTextColorPressed:I

    .line 2556
    const v1, 0x20500d0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mHandleTextColorPressed2ndAct:I

    .line 2558
    iget v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mPosition:I

    if-ne v1, v2, :cond_5

    .line 2560
    const v1, 0x20202bb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mBgDrawableDefault:Landroid/graphics/drawable/Drawable;

    .line 2561
    const v1, 0x20202b9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mBgDrawablePressed:Landroid/graphics/drawable/Drawable;

    .line 2562
    const v1, 0x20202ba

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mHandleBgScrollDrawable:Landroid/graphics/drawable/Drawable;

    .line 2564
    iput-object v3, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mBgDrawablePressed2ndAct:Landroid/graphics/drawable/Drawable;

    .line 2565
    iput-object v3, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mHandleBgScrollDrawable2ndAct:Landroid/graphics/drawable/Drawable;

    .line 2576
    :goto_8
    iget v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mDepth:I

    if-le v1, v2, :cond_1

    .line 2578
    const v1, 0x202022c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mBgDrawableDefault:Landroid/graphics/drawable/Drawable;

    goto/16 :goto_2

    .line 2569
    :cond_5
    const v1, 0x2020232

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mBgDrawableDefault:Landroid/graphics/drawable/Drawable;

    .line 2570
    const v1, 0x2020224

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mBgDrawablePressed:Landroid/graphics/drawable/Drawable;

    .line 2571
    const v1, 0x2020230

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mHandleBgScrollDrawable:Landroid/graphics/drawable/Drawable;

    .line 2573
    const v1, 0x2020222

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mBgDrawablePressed2ndAct:Landroid/graphics/drawable/Drawable;

    .line 2574
    const v1, 0x202022e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mHandleBgScrollDrawable2ndAct:Landroid/graphics/drawable/Drawable;

    goto :goto_8

    .line 2584
    :cond_6
    iget-object v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mBgDrawableDefault:Landroid/graphics/drawable/Drawable;

    goto/16 :goto_3

    .line 2585
    :cond_7
    iget-object v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mHandleBgScrollDrawable:Landroid/graphics/drawable/Drawable;

    goto/16 :goto_4

    .line 2586
    :cond_8
    iget v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSeparatorColor:I

    goto/16 :goto_5

    .line 2587
    :cond_9
    iget v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mTextColorDimmed:I

    goto/16 :goto_6

    .line 2593
    :cond_a
    iget-object v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mBgDrawableDefault:Landroid/graphics/drawable/Drawable;

    goto/16 :goto_7
.end method

.method public setMaxDepth(I)V
    .locals 0
    .param p1, "maxDepth"    # I

    .prologue
    .line 1726
    iput p1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mMaxDepth:I

    .line 1727
    return-void
.end method

.method public setPosition(I)V
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 1736
    iget-boolean v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->m_bSubIndexScrollExists:Z

    if-eqz v0, :cond_0

    .line 1753
    :goto_0
    return-void

    .line 1741
    :cond_0
    iput p1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mPosition:I

    .line 1742
    iget v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mIndexScrollViewTheme:I

    invoke-virtual {p0, v0}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->setIndexScrollViewTheme(I)V

    .line 1743
    iget v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mPosition:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 1745
    iget v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mWidth:I

    iget v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mWidthShift:I

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSeparatorWidth:I

    sub-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSeparatorLeftPos:I

    .line 1751
    :goto_1
    invoke-direct {p0}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->setBgRectParams()V

    goto :goto_0

    .line 1749
    :cond_1
    iget v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mWidthShift:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSeparatorLeftPos:I

    goto :goto_1
.end method

.method public setSimpleIndexScrollWidth(I)V
    .locals 0
    .param p1, "itemWidth"    # I

    .prologue
    .line 1698
    if-gtz p1, :cond_0

    .line 1705
    :goto_0
    return-void

    .line 1701
    :cond_0
    iput p1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mItemWidth:I

    .line 1702
    iput p1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mSeparatorWidth:I

    .line 1703
    iput p1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->mBgRectWidth:I

    .line 1704
    invoke-direct {p0}, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$IndexScroll;->allocateBgRectangle()V

    goto :goto_0
.end method
