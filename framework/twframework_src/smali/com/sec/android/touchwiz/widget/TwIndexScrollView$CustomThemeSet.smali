.class public Lcom/sec/android/touchwiz/widget/TwIndexScrollView$CustomThemeSet;
.super Ljava/lang/Object;
.source "TwIndexScrollView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/touchwiz/widget/TwIndexScrollView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CustomThemeSet"
.end annotation


# instance fields
.field private bgDrawableDefault:Landroid/graphics/drawable/Drawable;

.field private bgDrawableDefaultMoreDepth:Landroid/graphics/drawable/Drawable;

.field private bgIndexRectDrawable:Landroid/graphics/drawable/Drawable;

.field private handleBgScrollDrawable:Landroid/graphics/drawable/Drawable;

.field private handleTextColorPressed:I

.field private separatorColor:I

.field private textColorDimmed:I


# direct methods
.method public constructor <init>(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V
    .locals 2
    .param p1, "bgIndexRectDrawable"    # Landroid/graphics/drawable/Drawable;
    .param p2, "bgDrawableDefault"    # Landroid/graphics/drawable/Drawable;
    .param p3, "bgDrawableDefaultMoreDepth"    # Landroid/graphics/drawable/Drawable;
    .param p4, "handleBgScrollDrawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 600
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 592
    iput-object v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$CustomThemeSet;->bgIndexRectDrawable:Landroid/graphics/drawable/Drawable;

    .line 593
    iput-object v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$CustomThemeSet;->bgDrawableDefault:Landroid/graphics/drawable/Drawable;

    .line 594
    iput-object v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$CustomThemeSet;->bgDrawableDefaultMoreDepth:Landroid/graphics/drawable/Drawable;

    .line 595
    iput-object v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$CustomThemeSet;->handleBgScrollDrawable:Landroid/graphics/drawable/Drawable;

    .line 596
    iput v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$CustomThemeSet;->separatorColor:I

    .line 597
    iput v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$CustomThemeSet;->textColorDimmed:I

    .line 598
    iput v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$CustomThemeSet;->handleTextColorPressed:I

    .line 601
    iput-object p1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$CustomThemeSet;->bgIndexRectDrawable:Landroid/graphics/drawable/Drawable;

    .line 602
    iput-object p2, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$CustomThemeSet;->bgDrawableDefault:Landroid/graphics/drawable/Drawable;

    .line 603
    iput-object p3, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$CustomThemeSet;->bgDrawableDefaultMoreDepth:Landroid/graphics/drawable/Drawable;

    .line 604
    iput-object p4, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$CustomThemeSet;->handleBgScrollDrawable:Landroid/graphics/drawable/Drawable;

    .line 605
    iput v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$CustomThemeSet;->separatorColor:I

    .line 606
    iput v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$CustomThemeSet;->textColorDimmed:I

    .line 607
    iput v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$CustomThemeSet;->handleTextColorPressed:I

    .line 608
    return-void
.end method

.method public constructor <init>(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;III)V
    .locals 2
    .param p1, "bgIndexRectDrawable"    # Landroid/graphics/drawable/Drawable;
    .param p2, "bgDrawableDefault"    # Landroid/graphics/drawable/Drawable;
    .param p3, "bgDrawableDefaultMoreDepth"    # Landroid/graphics/drawable/Drawable;
    .param p4, "handleBgScrollDrawable"    # Landroid/graphics/drawable/Drawable;
    .param p5, "separatorColor"    # I
    .param p6, "textColorDimmed"    # I
    .param p7, "handleTextColorPressed"    # I

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 611
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 592
    iput-object v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$CustomThemeSet;->bgIndexRectDrawable:Landroid/graphics/drawable/Drawable;

    .line 593
    iput-object v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$CustomThemeSet;->bgDrawableDefault:Landroid/graphics/drawable/Drawable;

    .line 594
    iput-object v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$CustomThemeSet;->bgDrawableDefaultMoreDepth:Landroid/graphics/drawable/Drawable;

    .line 595
    iput-object v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$CustomThemeSet;->handleBgScrollDrawable:Landroid/graphics/drawable/Drawable;

    .line 596
    iput v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$CustomThemeSet;->separatorColor:I

    .line 597
    iput v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$CustomThemeSet;->textColorDimmed:I

    .line 598
    iput v1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$CustomThemeSet;->handleTextColorPressed:I

    .line 612
    iput-object p1, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$CustomThemeSet;->bgIndexRectDrawable:Landroid/graphics/drawable/Drawable;

    .line 613
    iput-object p2, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$CustomThemeSet;->bgDrawableDefault:Landroid/graphics/drawable/Drawable;

    .line 614
    iput-object p3, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$CustomThemeSet;->bgDrawableDefaultMoreDepth:Landroid/graphics/drawable/Drawable;

    .line 615
    iput-object p4, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$CustomThemeSet;->handleBgScrollDrawable:Landroid/graphics/drawable/Drawable;

    .line 616
    iput p5, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$CustomThemeSet;->separatorColor:I

    .line 617
    iput p6, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$CustomThemeSet;->textColorDimmed:I

    .line 618
    iput p7, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$CustomThemeSet;->handleTextColorPressed:I

    .line 619
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/touchwiz/widget/TwIndexScrollView$CustomThemeSet;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/touchwiz/widget/TwIndexScrollView$CustomThemeSet;

    .prologue
    .line 590
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$CustomThemeSet;->bgIndexRectDrawable:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/touchwiz/widget/TwIndexScrollView$CustomThemeSet;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/touchwiz/widget/TwIndexScrollView$CustomThemeSet;

    .prologue
    .line 590
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$CustomThemeSet;->bgDrawableDefault:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/touchwiz/widget/TwIndexScrollView$CustomThemeSet;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/touchwiz/widget/TwIndexScrollView$CustomThemeSet;

    .prologue
    .line 590
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$CustomThemeSet;->bgDrawableDefaultMoreDepth:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/touchwiz/widget/TwIndexScrollView$CustomThemeSet;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/touchwiz/widget/TwIndexScrollView$CustomThemeSet;

    .prologue
    .line 590
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$CustomThemeSet;->handleBgScrollDrawable:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/touchwiz/widget/TwIndexScrollView$CustomThemeSet;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/touchwiz/widget/TwIndexScrollView$CustomThemeSet;

    .prologue
    .line 590
    iget v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$CustomThemeSet;->separatorColor:I

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/touchwiz/widget/TwIndexScrollView$CustomThemeSet;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/touchwiz/widget/TwIndexScrollView$CustomThemeSet;

    .prologue
    .line 590
    iget v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$CustomThemeSet;->textColorDimmed:I

    return v0
.end method

.method static synthetic access$600(Lcom/sec/android/touchwiz/widget/TwIndexScrollView$CustomThemeSet;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/touchwiz/widget/TwIndexScrollView$CustomThemeSet;

    .prologue
    .line 590
    iget v0, p0, Lcom/sec/android/touchwiz/widget/TwIndexScrollView$CustomThemeSet;->handleTextColorPressed:I

    return v0
.end method
