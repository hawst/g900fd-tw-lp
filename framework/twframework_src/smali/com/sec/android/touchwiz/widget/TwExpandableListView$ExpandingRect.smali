.class Lcom/sec/android/touchwiz/widget/TwExpandableListView$ExpandingRect;
.super Ljava/lang/Object;
.source "TwExpandableListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/touchwiz/widget/TwExpandableListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ExpandingRect"
.end annotation


# instance fields
.field destinationRect:Landroid/graphics/RectF;

.field endRect:Landroid/graphics/RectF;

.field startY:I

.field final synthetic this$0:Lcom/sec/android/touchwiz/widget/TwExpandableListView;


# direct methods
.method constructor <init>(Lcom/sec/android/touchwiz/widget/TwExpandableListView;ILandroid/graphics/RectF;Landroid/graphics/RectF;)V
    .locals 0
    .param p2, "startY"    # I
    .param p3, "endRect"    # Landroid/graphics/RectF;
    .param p4, "destRect"    # Landroid/graphics/RectF;

    .prologue
    .line 1292
    iput-object p1, p0, Lcom/sec/android/touchwiz/widget/TwExpandableListView$ExpandingRect;->this$0:Lcom/sec/android/touchwiz/widget/TwExpandableListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1293
    iput p2, p0, Lcom/sec/android/touchwiz/widget/TwExpandableListView$ExpandingRect;->startY:I

    .line 1294
    iput-object p3, p0, Lcom/sec/android/touchwiz/widget/TwExpandableListView$ExpandingRect;->endRect:Landroid/graphics/RectF;

    .line 1295
    iput-object p4, p0, Lcom/sec/android/touchwiz/widget/TwExpandableListView$ExpandingRect;->destinationRect:Landroid/graphics/RectF;

    .line 1296
    return-void
.end method


# virtual methods
.method update(F)V
    .locals 4
    .param p1, "fraction"    # F

    .prologue
    .line 1299
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwExpandableListView$ExpandingRect;->destinationRect:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/sec/android/touchwiz/widget/TwExpandableListView$ExpandingRect;->endRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    iput v1, v0, Landroid/graphics/RectF;->left:F

    .line 1300
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwExpandableListView$ExpandingRect;->destinationRect:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/sec/android/touchwiz/widget/TwExpandableListView$ExpandingRect;->endRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->right:F

    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 1302
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwExpandableListView$ExpandingRect;->destinationRect:Landroid/graphics/RectF;

    iget v1, p0, Lcom/sec/android/touchwiz/widget/TwExpandableListView$ExpandingRect;->startY:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/android/touchwiz/widget/TwExpandableListView$ExpandingRect;->endRect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    iget v3, p0, Lcom/sec/android/touchwiz/widget/TwExpandableListView$ExpandingRect;->startY:I

    int-to-float v3, v3

    sub-float/2addr v2, v3

    mul-float/2addr v2, p1

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->top:F

    .line 1303
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwExpandableListView$ExpandingRect;->destinationRect:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/sec/android/touchwiz/widget/TwExpandableListView$ExpandingRect;->destinationRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    iget-object v2, p0, Lcom/sec/android/touchwiz/widget/TwExpandableListView$ExpandingRect;->endRect:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    mul-float/2addr v2, p1

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    .line 1304
    return-void
.end method
