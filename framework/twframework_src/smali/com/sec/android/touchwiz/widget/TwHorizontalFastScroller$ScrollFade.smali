.class public Lcom/sec/android/touchwiz/widget/TwHorizontalFastScroller$ScrollFade;
.super Ljava/lang/Object;
.source "TwHorizontalFastScroller.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/touchwiz/widget/TwHorizontalFastScroller;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ScrollFade"
.end annotation


# static fields
.field static final ALPHA_MAX:I = 0xff

.field static final FADE_DURATION:J = 0xc8L


# instance fields
.field mFadeDuration:J

.field mStartTime:J

.field final synthetic this$0:Lcom/sec/android/touchwiz/widget/TwHorizontalFastScroller;


# direct methods
.method public constructor <init>(Lcom/sec/android/touchwiz/widget/TwHorizontalFastScroller;)V
    .locals 0

    .prologue
    .line 878
    iput-object p1, p0, Lcom/sec/android/touchwiz/widget/TwHorizontalFastScroller$ScrollFade;->this$0:Lcom/sec/android/touchwiz/widget/TwHorizontalFastScroller;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method getAlpha()I
    .locals 10

    .prologue
    const-wide/16 v8, 0xff

    .line 892
    iget-object v1, p0, Lcom/sec/android/touchwiz/widget/TwHorizontalFastScroller$ScrollFade;->this$0:Lcom/sec/android/touchwiz/widget/TwHorizontalFastScroller;

    invoke-virtual {v1}, Lcom/sec/android/touchwiz/widget/TwHorizontalFastScroller;->getState()I

    move-result v1

    const/4 v4, 0x4

    if-eq v1, v4, :cond_0

    .line 893
    const/16 v0, 0xff

    .line 902
    :goto_0
    return v0

    .line 896
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 897
    .local v2, "now":J
    iget-wide v4, p0, Lcom/sec/android/touchwiz/widget/TwHorizontalFastScroller$ScrollFade;->mStartTime:J

    iget-wide v6, p0, Lcom/sec/android/touchwiz/widget/TwHorizontalFastScroller$ScrollFade;->mFadeDuration:J

    add-long/2addr v4, v6

    cmp-long v1, v2, v4

    if-lez v1, :cond_1

    .line 898
    const/4 v0, 0x0

    .local v0, "alpha":I
    goto :goto_0

    .line 900
    .end local v0    # "alpha":I
    :cond_1
    iget-wide v4, p0, Lcom/sec/android/touchwiz/widget/TwHorizontalFastScroller$ScrollFade;->mStartTime:J

    sub-long v4, v2, v4

    mul-long/2addr v4, v8

    iget-wide v6, p0, Lcom/sec/android/touchwiz/widget/TwHorizontalFastScroller$ScrollFade;->mFadeDuration:J

    div-long/2addr v4, v6

    sub-long v4, v8, v4

    long-to-int v0, v4

    .restart local v0    # "alpha":I
    goto :goto_0
.end method

.method public run()V
    .locals 2

    .prologue
    .line 906
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHorizontalFastScroller$ScrollFade;->this$0:Lcom/sec/android/touchwiz/widget/TwHorizontalFastScroller;

    invoke-virtual {v0}, Lcom/sec/android/touchwiz/widget/TwHorizontalFastScroller;->getState()I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 907
    invoke-virtual {p0}, Lcom/sec/android/touchwiz/widget/TwHorizontalFastScroller$ScrollFade;->startFade()V

    .line 916
    :goto_0
    return-void

    .line 911
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/touchwiz/widget/TwHorizontalFastScroller$ScrollFade;->getAlpha()I

    move-result v0

    if-lez v0, :cond_1

    .line 912
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHorizontalFastScroller$ScrollFade;->this$0:Lcom/sec/android/touchwiz/widget/TwHorizontalFastScroller;

    iget-object v0, v0, Lcom/sec/android/touchwiz/widget/TwHorizontalFastScroller;->mList:Lcom/sec/android/touchwiz/widget/TwHorizontalAbsListView;

    invoke-virtual {v0}, Lcom/sec/android/touchwiz/widget/TwHorizontalAbsListView;->invalidate()V

    goto :goto_0

    .line 914
    :cond_1
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHorizontalFastScroller$ScrollFade;->this$0:Lcom/sec/android/touchwiz/widget/TwHorizontalFastScroller;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/widget/TwHorizontalFastScroller;->setState(I)V

    goto :goto_0
.end method

.method startFade()V
    .locals 2

    .prologue
    .line 886
    const-wide/16 v0, 0xc8

    iput-wide v0, p0, Lcom/sec/android/touchwiz/widget/TwHorizontalFastScroller$ScrollFade;->mFadeDuration:J

    .line 887
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/android/touchwiz/widget/TwHorizontalFastScroller$ScrollFade;->mStartTime:J

    .line 888
    iget-object v0, p0, Lcom/sec/android/touchwiz/widget/TwHorizontalFastScroller$ScrollFade;->this$0:Lcom/sec/android/touchwiz/widget/TwHorizontalFastScroller;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/widget/TwHorizontalFastScroller;->setState(I)V

    .line 889
    return-void
.end method
