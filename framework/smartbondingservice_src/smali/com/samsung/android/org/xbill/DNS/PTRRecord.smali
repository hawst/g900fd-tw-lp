.class public Lcom/samsung/android/org/xbill/DNS/PTRRecord;
.super Lcom/samsung/android/org/xbill/DNS/SingleCompressedNameBase;
.source "PTRRecord.java"


# static fields
.field private static final serialVersionUID:J = -0x737c6468424b3050L


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/samsung/android/org/xbill/DNS/SingleCompressedNameBase;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/org/xbill/DNS/Name;IJLcom/samsung/android/org/xbill/DNS/Name;)V
    .locals 9
    .param p1, "name"    # Lcom/samsung/android/org/xbill/DNS/Name;
    .param p2, "dclass"    # I
    .param p3, "ttl"    # J
    .param p5, "target"    # Lcom/samsung/android/org/xbill/DNS/Name;

    .prologue
    .line 29
    const/16 v2, 0xc

    const-string v7, "target"

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move-wide v4, p3

    move-object v6, p5

    invoke-direct/range {v0 .. v7}, Lcom/samsung/android/org/xbill/DNS/SingleCompressedNameBase;-><init>(Lcom/samsung/android/org/xbill/DNS/Name;IIJLcom/samsung/android/org/xbill/DNS/Name;Ljava/lang/String;)V

    .line 30
    return-void
.end method


# virtual methods
.method getObject()Lcom/samsung/android/org/xbill/DNS/Record;
    .locals 1

    .prologue
    .line 20
    new-instance v0, Lcom/samsung/android/org/xbill/DNS/PTRRecord;

    invoke-direct {v0}, Lcom/samsung/android/org/xbill/DNS/PTRRecord;-><init>()V

    return-object v0
.end method

.method public getTarget()Lcom/samsung/android/org/xbill/DNS/Name;
    .locals 1

    .prologue
    .line 35
    invoke-virtual {p0}, Lcom/samsung/android/org/xbill/DNS/PTRRecord;->getSingleName()Lcom/samsung/android/org/xbill/DNS/Name;

    move-result-object v0

    return-object v0
.end method
