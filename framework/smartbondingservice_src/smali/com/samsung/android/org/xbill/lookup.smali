.class public Lcom/samsung/android/org/xbill/lookup;
.super Ljava/lang/Object;
.source "lookup.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static main([Ljava/lang/String;)V
    .locals 6
    .param p0, "args"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 36
    const/4 v3, 0x1

    .line 37
    .local v3, "type":I
    const/4 v2, 0x0

    .line 38
    .local v2, "start":I
    array-length v4, p0

    const/4 v5, 0x2

    if-le v4, v5, :cond_1

    const/4 v4, 0x0

    aget-object v4, p0, v4

    const-string v5, "-t"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 39
    const/4 v4, 0x1

    aget-object v4, p0, v4

    invoke-static {v4}, Lcom/samsung/android/org/xbill/DNS/Type;->value(Ljava/lang/String;)I

    move-result v3

    .line 40
    if-gez v3, :cond_0

    .line 41
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "invalid type"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 42
    :cond_0
    const/4 v2, 0x2

    .line 44
    :cond_1
    move v0, v2

    .local v0, "i":I
    :goto_0
    array-length v4, p0

    if-lt v0, v4, :cond_2

    .line 49
    return-void

    .line 45
    :cond_2
    new-instance v1, Lcom/samsung/android/org/xbill/DNS/Lookup;

    aget-object v4, p0, v0

    invoke-direct {v1, v4, v3}, Lcom/samsung/android/org/xbill/DNS/Lookup;-><init>(Ljava/lang/String;I)V

    .line 46
    .local v1, "l":Lcom/samsung/android/org/xbill/DNS/Lookup;
    invoke-virtual {v1}, Lcom/samsung/android/org/xbill/DNS/Lookup;->run()[Lcom/samsung/android/org/xbill/DNS/Record;

    .line 47
    aget-object v4, p0, v0

    invoke-static {v4, v1}, Lcom/samsung/android/org/xbill/lookup;->printAnswer(Ljava/lang/String;Lcom/samsung/android/org/xbill/DNS/Lookup;)V

    .line 44
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static printAnswer(Ljava/lang/String;Lcom/samsung/android/org/xbill/DNS/Lookup;)V
    .locals 7
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "lookup"    # Lcom/samsung/android/org/xbill/DNS/Lookup;

    .prologue
    .line 12
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, ":"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 13
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/Lookup;->getResult()I

    move-result v3

    .line 14
    .local v3, "result":I
    if-eqz v3, :cond_0

    .line 15
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, " "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/Lookup;->getErrorString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 16
    :cond_0
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v4}, Ljava/io/PrintStream;->println()V

    .line 17
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/Lookup;->getAliases()[Lcom/samsung/android/org/xbill/DNS/Name;

    move-result-object v0

    .line 18
    .local v0, "aliases":[Lcom/samsung/android/org/xbill/DNS/Name;
    array-length v4, v0

    if-lez v4, :cond_1

    .line 19
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v5, "# aliases: "

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 20
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v4, v0

    if-lt v2, v4, :cond_3

    .line 25
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v4}, Ljava/io/PrintStream;->println()V

    .line 27
    .end local v2    # "i":I
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/Lookup;->getResult()I

    move-result v4

    if-nez v4, :cond_2

    .line 28
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/Lookup;->getAnswers()[Lcom/samsung/android/org/xbill/DNS/Record;

    move-result-object v1

    .line 29
    .local v1, "answers":[Lcom/samsung/android/org/xbill/DNS/Record;
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_1
    array-length v4, v1

    if-lt v2, v4, :cond_5

    .line 32
    .end local v1    # "answers":[Lcom/samsung/android/org/xbill/DNS/Record;
    .end local v2    # "i":I
    :cond_2
    return-void

    .line 21
    .restart local v2    # "i":I
    :cond_3
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    aget-object v5, v0, v2

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->print(Ljava/lang/Object;)V

    .line 22
    array-length v4, v0

    add-int/lit8 v4, v4, -0x1

    if-ge v2, v4, :cond_4

    .line 23
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 20
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 30
    .restart local v1    # "answers":[Lcom/samsung/android/org/xbill/DNS/Record;
    :cond_5
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    aget-object v5, v1, v2

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    .line 29
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method
