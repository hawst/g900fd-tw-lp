.class public Lcom/samsung/android/org/xbill/DNS/DHCIDRecord;
.super Lcom/samsung/android/org/xbill/DNS/Record;
.source "DHCIDRecord.java"


# static fields
.field private static final serialVersionUID:J = -0x7200e772daec634bL


# instance fields
.field private data:[B


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/samsung/android/org/xbill/DNS/Record;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/org/xbill/DNS/Name;IJ[B)V
    .locals 7
    .param p1, "name"    # Lcom/samsung/android/org/xbill/DNS/Name;
    .param p2, "dclass"    # I
    .param p3, "ttl"    # J
    .param p5, "data"    # [B

    .prologue
    .line 34
    const/16 v2, 0x31

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/org/xbill/DNS/Record;-><init>(Lcom/samsung/android/org/xbill/DNS/Name;IIJ)V

    .line 35
    iput-object p5, p0, Lcom/samsung/android/org/xbill/DNS/DHCIDRecord;->data:[B

    .line 36
    return-void
.end method


# virtual methods
.method public getData()[B
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/DHCIDRecord;->data:[B

    return-object v0
.end method

.method getObject()Lcom/samsung/android/org/xbill/DNS/Record;
    .locals 1

    .prologue
    .line 25
    new-instance v0, Lcom/samsung/android/org/xbill/DNS/DHCIDRecord;

    invoke-direct {v0}, Lcom/samsung/android/org/xbill/DNS/DHCIDRecord;-><init>()V

    return-object v0
.end method

.method rdataFromString(Lcom/samsung/android/org/xbill/DNS/Tokenizer;Lcom/samsung/android/org/xbill/DNS/Name;)V
    .locals 1
    .param p1, "st"    # Lcom/samsung/android/org/xbill/DNS/Tokenizer;
    .param p2, "origin"    # Lcom/samsung/android/org/xbill/DNS/Name;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 45
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->getBase64()[B

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/org/xbill/DNS/DHCIDRecord;->data:[B

    .line 46
    return-void
.end method

.method rrFromWire(Lcom/samsung/android/org/xbill/DNS/DNSInput;)V
    .locals 1
    .param p1, "in"    # Lcom/samsung/android/org/xbill/DNS/DNSInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 40
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/DNSInput;->readByteArray()[B

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/org/xbill/DNS/DHCIDRecord;->data:[B

    .line 41
    return-void
.end method

.method rrToString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/DHCIDRecord;->data:[B

    invoke-static {v0}, Lcom/samsung/android/org/xbill/DNS/utils/base64;->toString([B)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method rrToWire(Lcom/samsung/android/org/xbill/DNS/DNSOutput;Lcom/samsung/android/org/xbill/DNS/Compression;Z)V
    .locals 1
    .param p1, "out"    # Lcom/samsung/android/org/xbill/DNS/DNSOutput;
    .param p2, "c"    # Lcom/samsung/android/org/xbill/DNS/Compression;
    .param p3, "canonical"    # Z

    .prologue
    .line 50
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/DHCIDRecord;->data:[B

    invoke-virtual {p1, v0}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeByteArray([B)V

    .line 51
    return-void
.end method
