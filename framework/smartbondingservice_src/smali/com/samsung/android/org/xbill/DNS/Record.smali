.class public abstract Lcom/samsung/android/org/xbill/DNS/Record;
.super Ljava/lang/Object;
.source "Record.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;
.implements Ljava/lang/Comparable;


# static fields
.field private static final byteFormat:Ljava/text/DecimalFormat;

.field private static final serialVersionUID:J = 0x25663ac63c372e5aL


# instance fields
.field protected dclass:I

.field protected name:Lcom/samsung/android/org/xbill/DNS/Name;

.field protected ttl:J

.field protected type:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 26
    new-instance v0, Ljava/text/DecimalFormat;

    invoke-direct {v0}, Ljava/text/DecimalFormat;-><init>()V

    sput-object v0, Lcom/samsung/android/org/xbill/DNS/Record;->byteFormat:Ljava/text/DecimalFormat;

    .line 29
    sget-object v0, Lcom/samsung/android/org/xbill/DNS/Record;->byteFormat:Ljava/text/DecimalFormat;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Ljava/text/DecimalFormat;->setMinimumIntegerDigits(I)V

    .line 30
    return-void
.end method

.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method constructor <init>(Lcom/samsung/android/org/xbill/DNS/Name;IIJ)V
    .locals 2
    .param p1, "name"    # Lcom/samsung/android/org/xbill/DNS/Name;
    .param p2, "type"    # I
    .param p3, "dclass"    # I
    .param p4, "ttl"    # J

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/Name;->isAbsolute()Z

    move-result v0

    if-nez v0, :cond_0

    .line 37
    new-instance v0, Lcom/samsung/android/org/xbill/DNS/RelativeNameException;

    invoke-direct {v0, p1}, Lcom/samsung/android/org/xbill/DNS/RelativeNameException;-><init>(Lcom/samsung/android/org/xbill/DNS/Name;)V

    throw v0

    .line 38
    :cond_0
    invoke-static {p2}, Lcom/samsung/android/org/xbill/DNS/Type;->check(I)V

    .line 39
    invoke-static {p3}, Lcom/samsung/android/org/xbill/DNS/DClass;->check(I)V

    .line 40
    invoke-static {p4, p5}, Lcom/samsung/android/org/xbill/DNS/TTL;->check(J)V

    .line 41
    iput-object p1, p0, Lcom/samsung/android/org/xbill/DNS/Record;->name:Lcom/samsung/android/org/xbill/DNS/Name;

    .line 42
    iput p2, p0, Lcom/samsung/android/org/xbill/DNS/Record;->type:I

    .line 43
    iput p3, p0, Lcom/samsung/android/org/xbill/DNS/Record;->dclass:I

    .line 44
    iput-wide p4, p0, Lcom/samsung/android/org/xbill/DNS/Record;->ttl:J

    .line 45
    return-void
.end method

.method protected static byteArrayFromString(Ljava/lang/String;)[B
    .locals 12
    .param p0, "s"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/org/xbill/DNS/TextParseException;
        }
    .end annotation

    .prologue
    const/16 v11, 0x5c

    const/16 v10, 0xff

    const/4 v9, 0x3

    .line 340
    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    .line 341
    .local v0, "array":[B
    const/4 v3, 0x0

    .line 342
    .local v3, "escaped":Z
    const/4 v4, 0x0

    .line 344
    .local v4, "hasEscapes":Z
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    array-length v8, v0

    if-lt v5, v8, :cond_0

    .line 350
    :goto_1
    if-nez v4, :cond_3

    .line 351
    array-length v8, v0

    if-le v8, v10, :cond_2

    .line 352
    new-instance v8, Lcom/samsung/android/org/xbill/DNS/TextParseException;

    const-string v9, "text string too long"

    invoke-direct {v8, v9}, Lcom/samsung/android/org/xbill/DNS/TextParseException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 345
    :cond_0
    aget-byte v8, v0, v5

    if-ne v8, v11, :cond_1

    .line 346
    const/4 v4, 0x1

    .line 347
    goto :goto_1

    .line 344
    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_2
    move-object v8, v0

    .line 395
    :goto_2
    return-object v8

    .line 357
    :cond_3
    new-instance v7, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v7}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 359
    .local v7, "os":Ljava/io/ByteArrayOutputStream;
    const/4 v2, 0x0

    .line 360
    .local v2, "digits":I
    const/4 v6, 0x0

    .line 361
    .local v6, "intval":I
    const/4 v5, 0x0

    :goto_3
    array-length v8, v0

    if-lt v5, v8, :cond_4

    .line 388
    if-lez v2, :cond_b

    if-ge v2, v9, :cond_b

    .line 389
    new-instance v8, Lcom/samsung/android/org/xbill/DNS/TextParseException;

    const-string v9, "bad escape"

    invoke-direct {v8, v9}, Lcom/samsung/android/org/xbill/DNS/TextParseException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 362
    :cond_4
    aget-byte v1, v0, v5

    .line 363
    .local v1, "b":B
    if-eqz v3, :cond_9

    .line 364
    const/16 v8, 0x30

    if-lt v1, v8, :cond_8

    const/16 v8, 0x39

    if-gt v1, v8, :cond_8

    if-ge v2, v9, :cond_8

    .line 365
    add-int/lit8 v2, v2, 0x1

    .line 366
    mul-int/lit8 v6, v6, 0xa

    .line 367
    add-int/lit8 v8, v1, -0x30

    add-int/2addr v6, v8

    .line 368
    if-le v6, v10, :cond_5

    .line 369
    new-instance v8, Lcom/samsung/android/org/xbill/DNS/TextParseException;

    .line 370
    const-string v9, "bad escape"

    .line 369
    invoke-direct {v8, v9}, Lcom/samsung/android/org/xbill/DNS/TextParseException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 371
    :cond_5
    if-ge v2, v9, :cond_6

    .line 361
    :goto_4
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    .line 373
    :cond_6
    int-to-byte v1, v6

    .line 377
    :cond_7
    invoke-virtual {v7, v1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 378
    const/4 v3, 0x0

    .line 379
    goto :goto_4

    .line 375
    :cond_8
    if-lez v2, :cond_7

    if-ge v2, v9, :cond_7

    .line 376
    new-instance v8, Lcom/samsung/android/org/xbill/DNS/TextParseException;

    const-string v9, "bad escape"

    invoke-direct {v8, v9}, Lcom/samsung/android/org/xbill/DNS/TextParseException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 380
    :cond_9
    aget-byte v8, v0, v5

    if-ne v8, v11, :cond_a

    .line 381
    const/4 v3, 0x1

    .line 382
    const/4 v2, 0x0

    .line 383
    const/4 v6, 0x0

    .line 384
    goto :goto_4

    .line 386
    :cond_a
    aget-byte v8, v0, v5

    invoke-virtual {v7, v8}, Ljava/io/ByteArrayOutputStream;->write(I)V

    goto :goto_4

    .line 390
    .end local v1    # "b":B
    :cond_b
    invoke-virtual {v7}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    .line 391
    array-length v8, v0

    if-le v8, v10, :cond_c

    .line 392
    new-instance v8, Lcom/samsung/android/org/xbill/DNS/TextParseException;

    const-string v9, "text string too long"

    invoke-direct {v8, v9}, Lcom/samsung/android/org/xbill/DNS/TextParseException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 395
    :cond_c
    invoke-virtual {v7}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v8

    goto :goto_2
.end method

.method protected static byteArrayToString([BZ)Ljava/lang/String;
    .locals 8
    .param p0, "array"    # [B
    .param p1, "quote"    # Z

    .prologue
    const/16 v7, 0x5c

    const/16 v6, 0x22

    .line 403
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 404
    .local v2, "sb":Ljava/lang/StringBuffer;
    if-eqz p1, :cond_0

    .line 405
    invoke-virtual {v2, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 406
    :cond_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v3, p0

    if-lt v1, v3, :cond_2

    .line 417
    if-eqz p1, :cond_1

    .line 418
    invoke-virtual {v2, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 419
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 407
    :cond_2
    aget-byte v3, p0, v1

    and-int/lit16 v0, v3, 0xff

    .line 408
    .local v0, "b":I
    const/16 v3, 0x20

    if-lt v0, v3, :cond_3

    const/16 v3, 0x7f

    if-lt v0, v3, :cond_4

    .line 409
    :cond_3
    invoke-virtual {v2, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 410
    sget-object v3, Lcom/samsung/android/org/xbill/DNS/Record;->byteFormat:Ljava/text/DecimalFormat;

    int-to-long v4, v0

    invoke-virtual {v3, v4, v5}, Ljava/text/DecimalFormat;->format(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 406
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 411
    :cond_4
    if-eq v0, v6, :cond_5

    if-ne v0, v7, :cond_6

    .line 412
    :cond_5
    invoke-virtual {v2, v7}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 413
    int-to-char v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 415
    :cond_6
    int-to-char v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_1
.end method

.method static checkByteArrayLength(Ljava/lang/String;[BI)[B
    .locals 4
    .param p0, "field"    # Ljava/lang/String;
    .param p1, "array"    # [B
    .param p2, "maxLength"    # I

    .prologue
    const/4 v3, 0x0

    .line 728
    array-length v1, p1

    const v2, 0xffff

    if-le v1, v2, :cond_0

    .line 729
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "\""

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\" array "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 730
    const-string v3, "must have no more than "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 731
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " elements"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 729
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 732
    :cond_0
    array-length v1, p1

    new-array v0, v1, [B

    .line 733
    .local v0, "out":[B
    array-length v1, p1

    invoke-static {p1, v3, v0, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 734
    return-object v0
.end method

.method static checkName(Ljava/lang/String;Lcom/samsung/android/org/xbill/DNS/Name;)Lcom/samsung/android/org/xbill/DNS/Name;
    .locals 1
    .param p0, "field"    # Ljava/lang/String;
    .param p1, "name"    # Lcom/samsung/android/org/xbill/DNS/Name;

    .prologue
    .line 721
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/Name;->isAbsolute()Z

    move-result v0

    if-nez v0, :cond_0

    .line 722
    new-instance v0, Lcom/samsung/android/org/xbill/DNS/RelativeNameException;

    invoke-direct {v0, p1}, Lcom/samsung/android/org/xbill/DNS/RelativeNameException;-><init>(Lcom/samsung/android/org/xbill/DNS/Name;)V

    throw v0

    .line 723
    :cond_0
    return-object p1
.end method

.method static checkU16(Ljava/lang/String;I)I
    .locals 3
    .param p0, "field"    # Ljava/lang/String;
    .param p1, "val"    # I

    .prologue
    .line 701
    if-ltz p1, :cond_0

    const v0, 0xffff

    if-le p1, v0, :cond_1

    .line 702
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "\""

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\" "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 703
    const-string v2, " must be an unsigned 16 "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 704
    const-string v2, "bit value"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 702
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 705
    :cond_1
    return p1
.end method

.method static checkU32(Ljava/lang/String;J)J
    .locals 3
    .param p0, "field"    # Ljava/lang/String;
    .param p1, "val"    # J

    .prologue
    .line 711
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    const-wide v0, 0xffffffffL

    cmp-long v0, p1, v0

    if-lez v0, :cond_1

    .line 712
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "\""

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\" "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 713
    const-string v2, " must be an unsigned 32 "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 714
    const-string v2, "bit value"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 712
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 715
    :cond_1
    return-wide p1
.end method

.method static checkU8(Ljava/lang/String;I)I
    .locals 3
    .param p0, "field"    # Ljava/lang/String;
    .param p1, "val"    # I

    .prologue
    .line 691
    if-ltz p1, :cond_0

    const/16 v0, 0xff

    if-le p1, v0, :cond_1

    .line 692
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "\""

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\" "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 693
    const-string v2, " must be an unsigned 8 "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 694
    const-string v2, "bit value"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 692
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 695
    :cond_1
    return p1
.end method

.method public static fromString(Lcom/samsung/android/org/xbill/DNS/Name;IIJLcom/samsung/android/org/xbill/DNS/Tokenizer;Lcom/samsung/android/org/xbill/DNS/Name;)Lcom/samsung/android/org/xbill/DNS/Record;
    .locals 19
    .param p0, "name"    # Lcom/samsung/android/org/xbill/DNS/Name;
    .param p1, "type"    # I
    .param p2, "dclass"    # I
    .param p3, "ttl"    # J
    .param p5, "st"    # Lcom/samsung/android/org/xbill/DNS/Tokenizer;
    .param p6, "origin"    # Lcom/samsung/android/org/xbill/DNS/Name;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 452
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/org/xbill/DNS/Name;->isAbsolute()Z

    move-result v3

    if-nez v3, :cond_0

    .line 453
    new-instance v3, Lcom/samsung/android/org/xbill/DNS/RelativeNameException;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/samsung/android/org/xbill/DNS/RelativeNameException;-><init>(Lcom/samsung/android/org/xbill/DNS/Name;)V

    throw v3

    .line 454
    :cond_0
    invoke-static/range {p1 .. p1}, Lcom/samsung/android/org/xbill/DNS/Type;->check(I)V

    .line 455
    invoke-static/range {p2 .. p2}, Lcom/samsung/android/org/xbill/DNS/DClass;->check(I)V

    .line 456
    invoke-static/range {p3 .. p4}, Lcom/samsung/android/org/xbill/DNS/TTL;->check(J)V

    .line 458
    invoke-virtual/range {p5 .. p5}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->get()Lcom/samsung/android/org/xbill/DNS/Tokenizer$Token;

    move-result-object v17

    .line 459
    .local v17, "t":Lcom/samsung/android/org/xbill/DNS/Tokenizer$Token;
    move-object/from16 v0, v17

    iget v3, v0, Lcom/samsung/android/org/xbill/DNS/Tokenizer$Token;->type:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_4

    move-object/from16 v0, v17

    iget-object v3, v0, Lcom/samsung/android/org/xbill/DNS/Tokenizer$Token;->value:Ljava/lang/String;

    const-string v4, "\\#"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 460
    invoke-virtual/range {p5 .. p5}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->getUInt16()I

    move-result v8

    .line 461
    .local v8, "length":I
    invoke-virtual/range {p5 .. p5}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->getHex()[B

    move-result-object v2

    .line 462
    .local v2, "data":[B
    if-nez v2, :cond_1

    .line 463
    const/4 v3, 0x0

    new-array v2, v3, [B

    .line 465
    :cond_1
    array-length v3, v2

    if-eq v8, v3, :cond_2

    .line 466
    const-string v3, "invalid unknown RR encoding: length mismatch"

    move-object/from16 v0, p5

    invoke-virtual {v0, v3}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->exception(Ljava/lang/String;)Lcom/samsung/android/org/xbill/DNS/TextParseException;

    move-result-object v3

    throw v3

    .line 468
    :cond_2
    new-instance v9, Lcom/samsung/android/org/xbill/DNS/DNSInput;

    invoke-direct {v9, v2}, Lcom/samsung/android/org/xbill/DNS/DNSInput;-><init>([B)V

    .local v9, "in":Lcom/samsung/android/org/xbill/DNS/DNSInput;
    move-object/from16 v3, p0

    move/from16 v4, p1

    move/from16 v5, p2

    move-wide/from16 v6, p3

    .line 469
    invoke-static/range {v3 .. v9}, Lcom/samsung/android/org/xbill/DNS/Record;->newRecord(Lcom/samsung/android/org/xbill/DNS/Name;IIJILcom/samsung/android/org/xbill/DNS/DNSInput;)Lcom/samsung/android/org/xbill/DNS/Record;

    move-result-object v10

    .line 478
    .end local v2    # "data":[B
    .end local v8    # "length":I
    .end local v9    # "in":Lcom/samsung/android/org/xbill/DNS/DNSInput;
    :cond_3
    return-object v10

    .line 471
    :cond_4
    invoke-virtual/range {p5 .. p5}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->unget()V

    .line 472
    const/16 v16, 0x1

    move-object/from16 v11, p0

    move/from16 v12, p1

    move/from16 v13, p2

    move-wide/from16 v14, p3

    invoke-static/range {v11 .. v16}, Lcom/samsung/android/org/xbill/DNS/Record;->getEmptyRecord(Lcom/samsung/android/org/xbill/DNS/Name;IIJZ)Lcom/samsung/android/org/xbill/DNS/Record;

    move-result-object v10

    .line 473
    .local v10, "rec":Lcom/samsung/android/org/xbill/DNS/Record;
    move-object/from16 v0, p5

    move-object/from16 v1, p6

    invoke-virtual {v10, v0, v1}, Lcom/samsung/android/org/xbill/DNS/Record;->rdataFromString(Lcom/samsung/android/org/xbill/DNS/Tokenizer;Lcom/samsung/android/org/xbill/DNS/Name;)V

    .line 474
    invoke-virtual/range {p5 .. p5}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->get()Lcom/samsung/android/org/xbill/DNS/Tokenizer$Token;

    move-result-object v17

    .line 475
    move-object/from16 v0, v17

    iget v3, v0, Lcom/samsung/android/org/xbill/DNS/Tokenizer$Token;->type:I

    const/4 v4, 0x1

    if-eq v3, v4, :cond_3

    move-object/from16 v0, v17

    iget v3, v0, Lcom/samsung/android/org/xbill/DNS/Tokenizer$Token;->type:I

    if-eqz v3, :cond_3

    .line 476
    const-string v3, "unexpected tokens at end of record"

    move-object/from16 v0, p5

    invoke-virtual {v0, v3}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->exception(Ljava/lang/String;)Lcom/samsung/android/org/xbill/DNS/TextParseException;

    move-result-object v3

    throw v3
.end method

.method public static fromString(Lcom/samsung/android/org/xbill/DNS/Name;IIJLjava/lang/String;Lcom/samsung/android/org/xbill/DNS/Name;)Lcom/samsung/android/org/xbill/DNS/Record;
    .locals 9
    .param p0, "name"    # Lcom/samsung/android/org/xbill/DNS/Name;
    .param p1, "type"    # I
    .param p2, "dclass"    # I
    .param p3, "ttl"    # J
    .param p5, "s"    # Ljava/lang/String;
    .param p6, "origin"    # Lcom/samsung/android/org/xbill/DNS/Name;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 496
    new-instance v6, Lcom/samsung/android/org/xbill/DNS/Tokenizer;

    invoke-direct {v6, p5}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;-><init>(Ljava/lang/String;)V

    move-object v1, p0

    move v2, p1

    move v3, p2

    move-wide v4, p3

    move-object v7, p6

    invoke-static/range {v1 .. v7}, Lcom/samsung/android/org/xbill/DNS/Record;->fromString(Lcom/samsung/android/org/xbill/DNS/Name;IIJLcom/samsung/android/org/xbill/DNS/Tokenizer;Lcom/samsung/android/org/xbill/DNS/Name;)Lcom/samsung/android/org/xbill/DNS/Record;

    move-result-object v0

    return-object v0
.end method

.method static fromWire(Lcom/samsung/android/org/xbill/DNS/DNSInput;I)Lcom/samsung/android/org/xbill/DNS/Record;
    .locals 1
    .param p0, "in"    # Lcom/samsung/android/org/xbill/DNS/DNSInput;
    .param p1, "section"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 202
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/samsung/android/org/xbill/DNS/Record;->fromWire(Lcom/samsung/android/org/xbill/DNS/DNSInput;IZ)Lcom/samsung/android/org/xbill/DNS/Record;

    move-result-object v0

    return-object v0
.end method

.method static fromWire(Lcom/samsung/android/org/xbill/DNS/DNSInput;IZ)Lcom/samsung/android/org/xbill/DNS/Record;
    .locals 8
    .param p0, "in"    # Lcom/samsung/android/org/xbill/DNS/DNSInput;
    .param p1, "section"    # I
    .param p2, "isUpdate"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 184
    new-instance v1, Lcom/samsung/android/org/xbill/DNS/Name;

    invoke-direct {v1, p0}, Lcom/samsung/android/org/xbill/DNS/Name;-><init>(Lcom/samsung/android/org/xbill/DNS/DNSInput;)V

    .line 185
    .local v1, "name":Lcom/samsung/android/org/xbill/DNS/Name;
    invoke-virtual {p0}, Lcom/samsung/android/org/xbill/DNS/DNSInput;->readU16()I

    move-result v2

    .line 186
    .local v2, "type":I
    invoke-virtual {p0}, Lcom/samsung/android/org/xbill/DNS/DNSInput;->readU16()I

    move-result v3

    .line 188
    .local v3, "dclass":I
    if-nez p1, :cond_0

    .line 189
    invoke-static {v1, v2, v3}, Lcom/samsung/android/org/xbill/DNS/Record;->newRecord(Lcom/samsung/android/org/xbill/DNS/Name;II)Lcom/samsung/android/org/xbill/DNS/Record;

    move-result-object v0

    .line 197
    :goto_0
    return-object v0

    .line 191
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/org/xbill/DNS/DNSInput;->readU32()J

    move-result-wide v4

    .line 192
    .local v4, "ttl":J
    invoke-virtual {p0}, Lcom/samsung/android/org/xbill/DNS/DNSInput;->readU16()I

    move-result v6

    .line 193
    .local v6, "length":I
    if-nez v6, :cond_2

    if-eqz p2, :cond_2

    .line 194
    const/4 v7, 0x1

    if-eq p1, v7, :cond_1

    const/4 v7, 0x2

    if-ne p1, v7, :cond_2

    .line 195
    :cond_1
    invoke-static {v1, v2, v3, v4, v5}, Lcom/samsung/android/org/xbill/DNS/Record;->newRecord(Lcom/samsung/android/org/xbill/DNS/Name;IIJ)Lcom/samsung/android/org/xbill/DNS/Record;

    move-result-object v0

    goto :goto_0

    :cond_2
    move-object v7, p0

    .line 196
    invoke-static/range {v1 .. v7}, Lcom/samsung/android/org/xbill/DNS/Record;->newRecord(Lcom/samsung/android/org/xbill/DNS/Name;IIJILcom/samsung/android/org/xbill/DNS/DNSInput;)Lcom/samsung/android/org/xbill/DNS/Record;

    move-result-object v0

    .line 197
    .local v0, "rec":Lcom/samsung/android/org/xbill/DNS/Record;
    goto :goto_0
.end method

.method public static fromWire([BI)Lcom/samsung/android/org/xbill/DNS/Record;
    .locals 2
    .param p0, "b"    # [B
    .param p1, "section"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 210
    new-instance v0, Lcom/samsung/android/org/xbill/DNS/DNSInput;

    invoke-direct {v0, p0}, Lcom/samsung/android/org/xbill/DNS/DNSInput;-><init>([B)V

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Lcom/samsung/android/org/xbill/DNS/Record;->fromWire(Lcom/samsung/android/org/xbill/DNS/DNSInput;IZ)Lcom/samsung/android/org/xbill/DNS/Record;

    move-result-object v0

    return-object v0
.end method

.method private static final getEmptyRecord(Lcom/samsung/android/org/xbill/DNS/Name;IIJZ)Lcom/samsung/android/org/xbill/DNS/Record;
    .locals 3
    .param p0, "name"    # Lcom/samsung/android/org/xbill/DNS/Name;
    .param p1, "type"    # I
    .param p2, "dclass"    # I
    .param p3, "ttl"    # J
    .param p5, "hasData"    # Z

    .prologue
    .line 57
    if-eqz p5, :cond_1

    .line 58
    invoke-static {p1}, Lcom/samsung/android/org/xbill/DNS/Type;->getProto(I)Lcom/samsung/android/org/xbill/DNS/Record;

    move-result-object v0

    .line 59
    .local v0, "proto":Lcom/samsung/android/org/xbill/DNS/Record;
    if-eqz v0, :cond_0

    .line 60
    invoke-virtual {v0}, Lcom/samsung/android/org/xbill/DNS/Record;->getObject()Lcom/samsung/android/org/xbill/DNS/Record;

    move-result-object v1

    .line 65
    .end local v0    # "proto":Lcom/samsung/android/org/xbill/DNS/Record;
    .local v1, "rec":Lcom/samsung/android/org/xbill/DNS/Record;
    :goto_0
    iput-object p0, v1, Lcom/samsung/android/org/xbill/DNS/Record;->name:Lcom/samsung/android/org/xbill/DNS/Name;

    .line 66
    iput p1, v1, Lcom/samsung/android/org/xbill/DNS/Record;->type:I

    .line 67
    iput p2, v1, Lcom/samsung/android/org/xbill/DNS/Record;->dclass:I

    .line 68
    iput-wide p3, v1, Lcom/samsung/android/org/xbill/DNS/Record;->ttl:J

    .line 69
    return-object v1

    .line 62
    .end local v1    # "rec":Lcom/samsung/android/org/xbill/DNS/Record;
    .restart local v0    # "proto":Lcom/samsung/android/org/xbill/DNS/Record;
    :cond_0
    new-instance v1, Lcom/samsung/android/org/xbill/DNS/UNKRecord;

    invoke-direct {v1}, Lcom/samsung/android/org/xbill/DNS/UNKRecord;-><init>()V

    .line 63
    .restart local v1    # "rec":Lcom/samsung/android/org/xbill/DNS/Record;
    goto :goto_0

    .line 64
    .end local v0    # "proto":Lcom/samsung/android/org/xbill/DNS/Record;
    .end local v1    # "rec":Lcom/samsung/android/org/xbill/DNS/Record;
    :cond_1
    new-instance v1, Lcom/samsung/android/org/xbill/DNS/EmptyRecord;

    invoke-direct {v1}, Lcom/samsung/android/org/xbill/DNS/EmptyRecord;-><init>()V

    .restart local v1    # "rec":Lcom/samsung/android/org/xbill/DNS/Record;
    goto :goto_0
.end method

.method public static newRecord(Lcom/samsung/android/org/xbill/DNS/Name;II)Lcom/samsung/android/org/xbill/DNS/Record;
    .locals 2
    .param p0, "name"    # Lcom/samsung/android/org/xbill/DNS/Name;
    .param p1, "type"    # I
    .param p2, "dclass"    # I

    .prologue
    .line 173
    const-wide/16 v0, 0x0

    invoke-static {p0, p1, p2, v0, v1}, Lcom/samsung/android/org/xbill/DNS/Record;->newRecord(Lcom/samsung/android/org/xbill/DNS/Name;IIJ)Lcom/samsung/android/org/xbill/DNS/Record;

    move-result-object v0

    return-object v0
.end method

.method public static newRecord(Lcom/samsung/android/org/xbill/DNS/Name;IIJ)Lcom/samsung/android/org/xbill/DNS/Record;
    .locals 7
    .param p0, "name"    # Lcom/samsung/android/org/xbill/DNS/Name;
    .param p1, "type"    # I
    .param p2, "dclass"    # I
    .param p3, "ttl"    # J

    .prologue
    .line 153
    invoke-virtual {p0}, Lcom/samsung/android/org/xbill/DNS/Name;->isAbsolute()Z

    move-result v0

    if-nez v0, :cond_0

    .line 154
    new-instance v0, Lcom/samsung/android/org/xbill/DNS/RelativeNameException;

    invoke-direct {v0, p0}, Lcom/samsung/android/org/xbill/DNS/RelativeNameException;-><init>(Lcom/samsung/android/org/xbill/DNS/Name;)V

    throw v0

    .line 155
    :cond_0
    invoke-static {p1}, Lcom/samsung/android/org/xbill/DNS/Type;->check(I)V

    .line 156
    invoke-static {p2}, Lcom/samsung/android/org/xbill/DNS/DClass;->check(I)V

    .line 157
    invoke-static {p3, p4}, Lcom/samsung/android/org/xbill/DNS/TTL;->check(J)V

    .line 159
    const/4 v6, 0x0

    move-object v1, p0

    move v2, p1

    move v3, p2

    move-wide v4, p3

    invoke-static/range {v1 .. v6}, Lcom/samsung/android/org/xbill/DNS/Record;->getEmptyRecord(Lcom/samsung/android/org/xbill/DNS/Name;IIJZ)Lcom/samsung/android/org/xbill/DNS/Record;

    move-result-object v0

    return-object v0
.end method

.method private static newRecord(Lcom/samsung/android/org/xbill/DNS/Name;IIJILcom/samsung/android/org/xbill/DNS/DNSInput;)Lcom/samsung/android/org/xbill/DNS/Record;
    .locals 7
    .param p0, "name"    # Lcom/samsung/android/org/xbill/DNS/Name;
    .param p1, "type"    # I
    .param p2, "dclass"    # I
    .param p3, "ttl"    # J
    .param p5, "length"    # I
    .param p6, "in"    # Lcom/samsung/android/org/xbill/DNS/DNSInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 83
    if-eqz p6, :cond_0

    const/4 v6, 0x1

    :goto_0
    move-object v1, p0

    move v2, p1

    move v3, p2

    move-wide v4, p3

    invoke-static/range {v1 .. v6}, Lcom/samsung/android/org/xbill/DNS/Record;->getEmptyRecord(Lcom/samsung/android/org/xbill/DNS/Name;IIJZ)Lcom/samsung/android/org/xbill/DNS/Record;

    move-result-object v0

    .line 84
    .local v0, "rec":Lcom/samsung/android/org/xbill/DNS/Record;
    if-eqz p6, :cond_3

    .line 85
    invoke-virtual {p6}, Lcom/samsung/android/org/xbill/DNS/DNSInput;->remaining()I

    move-result v1

    if-ge v1, p5, :cond_1

    .line 86
    new-instance v1, Lcom/samsung/android/org/xbill/DNS/WireParseException;

    const-string v2, "truncated record"

    invoke-direct {v1, v2}, Lcom/samsung/android/org/xbill/DNS/WireParseException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 83
    .end local v0    # "rec":Lcom/samsung/android/org/xbill/DNS/Record;
    :cond_0
    const/4 v6, 0x0

    goto :goto_0

    .line 87
    .restart local v0    # "rec":Lcom/samsung/android/org/xbill/DNS/Record;
    :cond_1
    invoke-virtual {p6, p5}, Lcom/samsung/android/org/xbill/DNS/DNSInput;->setActive(I)V

    .line 89
    invoke-virtual {v0, p6}, Lcom/samsung/android/org/xbill/DNS/Record;->rrFromWire(Lcom/samsung/android/org/xbill/DNS/DNSInput;)V

    .line 91
    invoke-virtual {p6}, Lcom/samsung/android/org/xbill/DNS/DNSInput;->remaining()I

    move-result v1

    if-lez v1, :cond_2

    .line 92
    new-instance v1, Lcom/samsung/android/org/xbill/DNS/WireParseException;

    const-string v2, "invalid record length"

    invoke-direct {v1, v2}, Lcom/samsung/android/org/xbill/DNS/WireParseException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 93
    :cond_2
    invoke-virtual {p6}, Lcom/samsung/android/org/xbill/DNS/DNSInput;->clearActive()V

    .line 95
    :cond_3
    return-object v0
.end method

.method public static newRecord(Lcom/samsung/android/org/xbill/DNS/Name;IIJI[B)Lcom/samsung/android/org/xbill/DNS/Record;
    .locals 9
    .param p0, "name"    # Lcom/samsung/android/org/xbill/DNS/Name;
    .param p1, "type"    # I
    .param p2, "dclass"    # I
    .param p3, "ttl"    # J
    .param p5, "length"    # I
    .param p6, "data"    # [B

    .prologue
    .line 110
    invoke-virtual {p0}, Lcom/samsung/android/org/xbill/DNS/Name;->isAbsolute()Z

    move-result v1

    if-nez v1, :cond_0

    .line 111
    new-instance v1, Lcom/samsung/android/org/xbill/DNS/RelativeNameException;

    invoke-direct {v1, p0}, Lcom/samsung/android/org/xbill/DNS/RelativeNameException;-><init>(Lcom/samsung/android/org/xbill/DNS/Name;)V

    throw v1

    .line 112
    :cond_0
    invoke-static {p1}, Lcom/samsung/android/org/xbill/DNS/Type;->check(I)V

    .line 113
    invoke-static {p2}, Lcom/samsung/android/org/xbill/DNS/DClass;->check(I)V

    .line 114
    invoke-static {p3, p4}, Lcom/samsung/android/org/xbill/DNS/TTL;->check(J)V

    .line 117
    if-eqz p6, :cond_1

    .line 118
    new-instance v7, Lcom/samsung/android/org/xbill/DNS/DNSInput;

    invoke-direct {v7, p6}, Lcom/samsung/android/org/xbill/DNS/DNSInput;-><init>([B)V

    .local v7, "in":Lcom/samsung/android/org/xbill/DNS/DNSInput;
    :goto_0
    move-object v1, p0

    move v2, p1

    move v3, p2

    move-wide v4, p3

    move v6, p5

    .line 122
    :try_start_0
    invoke-static/range {v1 .. v7}, Lcom/samsung/android/org/xbill/DNS/Record;->newRecord(Lcom/samsung/android/org/xbill/DNS/Name;IIJILcom/samsung/android/org/xbill/DNS/DNSInput;)Lcom/samsung/android/org/xbill/DNS/Record;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 125
    :goto_1
    return-object v1

    .line 120
    .end local v7    # "in":Lcom/samsung/android/org/xbill/DNS/DNSInput;
    :cond_1
    const/4 v7, 0x0

    .restart local v7    # "in":Lcom/samsung/android/org/xbill/DNS/DNSInput;
    goto :goto_0

    .line 124
    :catch_0
    move-exception v0

    .line 125
    .local v0, "e":Ljava/io/IOException;
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static newRecord(Lcom/samsung/android/org/xbill/DNS/Name;IIJ[B)Lcom/samsung/android/org/xbill/DNS/Record;
    .locals 9
    .param p0, "name"    # Lcom/samsung/android/org/xbill/DNS/Name;
    .param p1, "type"    # I
    .param p2, "dclass"    # I
    .param p3, "ttl"    # J
    .param p5, "data"    # [B

    .prologue
    .line 140
    array-length v6, p5

    move-object v1, p0

    move v2, p1

    move v3, p2

    move-wide v4, p3

    move-object v7, p5

    invoke-static/range {v1 .. v7}, Lcom/samsung/android/org/xbill/DNS/Record;->newRecord(Lcom/samsung/android/org/xbill/DNS/Name;IIJI[B)Lcom/samsung/android/org/xbill/DNS/Record;

    move-result-object v0

    return-object v0
.end method

.method private toWireCanonical(Lcom/samsung/android/org/xbill/DNS/DNSOutput;Z)V
    .locals 4
    .param p1, "out"    # Lcom/samsung/android/org/xbill/DNS/DNSOutput;
    .param p2, "noTTL"    # Z

    .prologue
    .line 240
    iget-object v2, p0, Lcom/samsung/android/org/xbill/DNS/Record;->name:Lcom/samsung/android/org/xbill/DNS/Name;

    invoke-virtual {v2, p1}, Lcom/samsung/android/org/xbill/DNS/Name;->toWireCanonical(Lcom/samsung/android/org/xbill/DNS/DNSOutput;)V

    .line 241
    iget v2, p0, Lcom/samsung/android/org/xbill/DNS/Record;->type:I

    invoke-virtual {p1, v2}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeU16(I)V

    .line 242
    iget v2, p0, Lcom/samsung/android/org/xbill/DNS/Record;->dclass:I

    invoke-virtual {p1, v2}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeU16(I)V

    .line 243
    if-eqz p2, :cond_0

    .line 244
    const-wide/16 v2, 0x0

    invoke-virtual {p1, v2, v3}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeU32(J)V

    .line 248
    :goto_0
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->current()I

    move-result v0

    .line 249
    .local v0, "lengthPosition":I
    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeU16(I)V

    .line 250
    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {p0, p1, v2, v3}, Lcom/samsung/android/org/xbill/DNS/Record;->rrToWire(Lcom/samsung/android/org/xbill/DNS/DNSOutput;Lcom/samsung/android/org/xbill/DNS/Compression;Z)V

    .line 251
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->current()I

    move-result v2

    sub-int/2addr v2, v0

    add-int/lit8 v1, v2, -0x2

    .line 252
    .local v1, "rrlength":I
    invoke-virtual {p1, v1, v0}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeU16At(II)V

    .line 253
    return-void

    .line 246
    .end local v0    # "lengthPosition":I
    .end local v1    # "rrlength":I
    :cond_0
    iget-wide v2, p0, Lcom/samsung/android/org/xbill/DNS/Record;->ttl:J

    invoke-virtual {p1, v2, v3}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeU32(J)V

    goto :goto_0
.end method

.method private toWireCanonical(Z)[B
    .locals 2
    .param p1, "noTTL"    # Z

    .prologue
    .line 261
    new-instance v0, Lcom/samsung/android/org/xbill/DNS/DNSOutput;

    invoke-direct {v0}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;-><init>()V

    .line 262
    .local v0, "out":Lcom/samsung/android/org/xbill/DNS/DNSOutput;
    invoke-direct {p0, v0, p1}, Lcom/samsung/android/org/xbill/DNS/Record;->toWireCanonical(Lcom/samsung/android/org/xbill/DNS/DNSOutput;Z)V

    .line 263
    invoke-virtual {v0}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->toByteArray()[B

    move-result-object v1

    return-object v1
.end method

.method protected static unknownToString([B)Ljava/lang/String;
    .locals 2
    .param p0, "data"    # [B

    .prologue
    .line 427
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 428
    .local v0, "sb":Ljava/lang/StringBuffer;
    const-string v1, "\\# "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 429
    array-length v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 430
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 431
    invoke-static {p0}, Lcom/samsung/android/org/xbill/DNS/utils/base16;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 432
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method cloneRecord()Lcom/samsung/android/org/xbill/DNS/Record;
    .locals 2

    .prologue
    .line 602
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/org/xbill/DNS/Record;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 604
    :catch_0
    move-exception v0

    .line 605
    .local v0, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1}, Ljava/lang/IllegalStateException;-><init>()V

    throw v1
.end method

.method public compareTo(Ljava/lang/Object;)I
    .locals 7
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 652
    move-object v0, p1

    check-cast v0, Lcom/samsung/android/org/xbill/DNS/Record;

    .line 654
    .local v0, "arg":Lcom/samsung/android/org/xbill/DNS/Record;
    if-ne p0, v0, :cond_1

    .line 655
    const/4 v2, 0x0

    .line 673
    :cond_0
    :goto_0
    return v2

    .line 657
    :cond_1
    iget-object v5, p0, Lcom/samsung/android/org/xbill/DNS/Record;->name:Lcom/samsung/android/org/xbill/DNS/Name;

    iget-object v6, v0, Lcom/samsung/android/org/xbill/DNS/Record;->name:Lcom/samsung/android/org/xbill/DNS/Name;

    invoke-virtual {v5, v6}, Lcom/samsung/android/org/xbill/DNS/Name;->compareTo(Ljava/lang/Object;)I

    move-result v2

    .line 658
    .local v2, "n":I
    if-nez v2, :cond_0

    .line 660
    iget v5, p0, Lcom/samsung/android/org/xbill/DNS/Record;->dclass:I

    iget v6, v0, Lcom/samsung/android/org/xbill/DNS/Record;->dclass:I

    sub-int v2, v5, v6

    .line 661
    if-nez v2, :cond_0

    .line 663
    iget v5, p0, Lcom/samsung/android/org/xbill/DNS/Record;->type:I

    iget v6, v0, Lcom/samsung/android/org/xbill/DNS/Record;->type:I

    sub-int v2, v5, v6

    .line 664
    if-nez v2, :cond_0

    .line 666
    invoke-virtual {p0}, Lcom/samsung/android/org/xbill/DNS/Record;->rdataToWireCanonical()[B

    move-result-object v3

    .line 667
    .local v3, "rdata1":[B
    invoke-virtual {v0}, Lcom/samsung/android/org/xbill/DNS/Record;->rdataToWireCanonical()[B

    move-result-object v4

    .line 668
    .local v4, "rdata2":[B
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    array-length v5, v3

    if-ge v1, v5, :cond_2

    array-length v5, v4

    if-lt v1, v5, :cond_3

    .line 673
    :cond_2
    array-length v5, v3

    array-length v6, v4

    sub-int v2, v5, v6

    goto :goto_0

    .line 669
    :cond_3
    aget-byte v5, v3, v1

    and-int/lit16 v5, v5, 0xff

    aget-byte v6, v4, v1

    and-int/lit16 v6, v6, 0xff

    sub-int v2, v5, v6

    .line 670
    if-nez v2, :cond_0

    .line 668
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "arg"    # Ljava/lang/Object;

    .prologue
    const/4 v3, 0x0

    .line 577
    if-eqz p1, :cond_0

    instance-of v4, p1, Lcom/samsung/android/org/xbill/DNS/Record;

    if-nez v4, :cond_1

    .line 584
    :cond_0
    :goto_0
    return v3

    :cond_1
    move-object v2, p1

    .line 579
    check-cast v2, Lcom/samsung/android/org/xbill/DNS/Record;

    .line 580
    .local v2, "r":Lcom/samsung/android/org/xbill/DNS/Record;
    iget v4, p0, Lcom/samsung/android/org/xbill/DNS/Record;->type:I

    iget v5, v2, Lcom/samsung/android/org/xbill/DNS/Record;->type:I

    if-ne v4, v5, :cond_0

    iget v4, p0, Lcom/samsung/android/org/xbill/DNS/Record;->dclass:I

    iget v5, v2, Lcom/samsung/android/org/xbill/DNS/Record;->dclass:I

    if-ne v4, v5, :cond_0

    iget-object v4, p0, Lcom/samsung/android/org/xbill/DNS/Record;->name:Lcom/samsung/android/org/xbill/DNS/Name;

    iget-object v5, v2, Lcom/samsung/android/org/xbill/DNS/Record;->name:Lcom/samsung/android/org/xbill/DNS/Name;

    invoke-virtual {v4, v5}, Lcom/samsung/android/org/xbill/DNS/Name;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 582
    invoke-virtual {p0}, Lcom/samsung/android/org/xbill/DNS/Record;->rdataToWireCanonical()[B

    move-result-object v0

    .line 583
    .local v0, "array1":[B
    invoke-virtual {v2}, Lcom/samsung/android/org/xbill/DNS/Record;->rdataToWireCanonical()[B

    move-result-object v1

    .line 584
    .local v1, "array2":[B
    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v3

    goto :goto_0
.end method

.method public getAdditionalName()Lcom/samsung/android/org/xbill/DNS/Name;
    .locals 1

    .prologue
    .line 685
    const/4 v0, 0x0

    return-object v0
.end method

.method public getDClass()I
    .locals 1

    .prologue
    .line 540
    iget v0, p0, Lcom/samsung/android/org/xbill/DNS/Record;->dclass:I

    return v0
.end method

.method public getName()Lcom/samsung/android/org/xbill/DNS/Name;
    .locals 1

    .prologue
    .line 505
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/Record;->name:Lcom/samsung/android/org/xbill/DNS/Name;

    return-object v0
.end method

.method abstract getObject()Lcom/samsung/android/org/xbill/DNS/Record;
.end method

.method public getRRsetType()I
    .locals 3

    .prologue
    .line 528
    iget v1, p0, Lcom/samsung/android/org/xbill/DNS/Record;->type:I

    const/16 v2, 0x2e

    if-ne v1, v2, :cond_0

    move-object v0, p0

    .line 529
    check-cast v0, Lcom/samsung/android/org/xbill/DNS/RRSIGRecord;

    .line 530
    .local v0, "sig":Lcom/samsung/android/org/xbill/DNS/RRSIGRecord;
    invoke-virtual {v0}, Lcom/samsung/android/org/xbill/DNS/RRSIGRecord;->getTypeCovered()I

    move-result v1

    .line 532
    .end local v0    # "sig":Lcom/samsung/android/org/xbill/DNS/RRSIGRecord;
    :goto_0
    return v1

    :cond_0
    iget v1, p0, Lcom/samsung/android/org/xbill/DNS/Record;->type:I

    goto :goto_0
.end method

.method public getTTL()J
    .locals 2

    .prologue
    .line 548
    iget-wide v0, p0, Lcom/samsung/android/org/xbill/DNS/Record;->ttl:J

    return-wide v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 514
    iget v0, p0, Lcom/samsung/android/org/xbill/DNS/Record;->type:I

    return v0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    .line 592
    const/4 v3, 0x1

    invoke-direct {p0, v3}, Lcom/samsung/android/org/xbill/DNS/Record;->toWireCanonical(Z)[B

    move-result-object v0

    .line 593
    .local v0, "array":[B
    const/4 v1, 0x0

    .line 594
    .local v1, "code":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v3, v0

    if-lt v2, v3, :cond_0

    .line 596
    return v1

    .line 595
    :cond_0
    shl-int/lit8 v3, v1, 0x3

    aget-byte v4, v0, v2

    and-int/lit16 v4, v4, 0xff

    add-int/2addr v3, v4

    add-int/2addr v1, v3

    .line 594
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method abstract rdataFromString(Lcom/samsung/android/org/xbill/DNS/Tokenizer;Lcom/samsung/android/org/xbill/DNS/Name;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public rdataToString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 296
    invoke-virtual {p0}, Lcom/samsung/android/org/xbill/DNS/Record;->rrToString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public rdataToWireCanonical()[B
    .locals 3

    .prologue
    .line 281
    new-instance v0, Lcom/samsung/android/org/xbill/DNS/DNSOutput;

    invoke-direct {v0}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;-><init>()V

    .line 282
    .local v0, "out":Lcom/samsung/android/org/xbill/DNS/DNSOutput;
    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/samsung/android/org/xbill/DNS/Record;->rrToWire(Lcom/samsung/android/org/xbill/DNS/DNSOutput;Lcom/samsung/android/org/xbill/DNS/Compression;Z)V

    .line 283
    invoke-virtual {v0}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->toByteArray()[B

    move-result-object v1

    return-object v1
.end method

.method abstract rrFromWire(Lcom/samsung/android/org/xbill/DNS/DNSInput;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method abstract rrToString()Ljava/lang/String;
.end method

.method abstract rrToWire(Lcom/samsung/android/org/xbill/DNS/DNSOutput;Lcom/samsung/android/org/xbill/DNS/Compression;Z)V
.end method

.method public sameRRset(Lcom/samsung/android/org/xbill/DNS/Record;)Z
    .locals 2
    .param p1, "rec"    # Lcom/samsung/android/org/xbill/DNS/Record;

    .prologue
    .line 564
    invoke-virtual {p0}, Lcom/samsung/android/org/xbill/DNS/Record;->getRRsetType()I

    move-result v0

    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/Record;->getRRsetType()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 565
    iget v0, p0, Lcom/samsung/android/org/xbill/DNS/Record;->dclass:I

    iget v1, p1, Lcom/samsung/android/org/xbill/DNS/Record;->dclass:I

    if-ne v0, v1, :cond_0

    .line 566
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/Record;->name:Lcom/samsung/android/org/xbill/DNS/Name;

    iget-object v1, p1, Lcom/samsung/android/org/xbill/DNS/Record;->name:Lcom/samsung/android/org/xbill/DNS/Name;

    invoke-virtual {v0, v1}, Lcom/samsung/android/org/xbill/DNS/Name;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 564
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method setTTL(J)V
    .locals 1
    .param p1, "ttl"    # J

    .prologue
    .line 637
    iput-wide p1, p0, Lcom/samsung/android/org/xbill/DNS/Record;->ttl:J

    .line 638
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 304
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 305
    .local v1, "sb":Ljava/lang/StringBuffer;
    iget-object v2, p0, Lcom/samsung/android/org/xbill/DNS/Record;->name:Lcom/samsung/android/org/xbill/DNS/Name;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    .line 306
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->length()I

    move-result v2

    const/16 v3, 0x8

    if-ge v2, v3, :cond_0

    .line 307
    const-string v2, "\t"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 308
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->length()I

    move-result v2

    const/16 v3, 0x10

    if-ge v2, v3, :cond_1

    .line 309
    const-string v2, "\t"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 310
    :cond_1
    const-string v2, "\t"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 311
    const-string v2, "BINDTTL"

    invoke-static {v2}, Lcom/samsung/android/org/xbill/DNS/Options;->check(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 312
    iget-wide v2, p0, Lcom/samsung/android/org/xbill/DNS/Record;->ttl:J

    invoke-static {v2, v3}, Lcom/samsung/android/org/xbill/DNS/TTL;->format(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 315
    :goto_0
    const-string v2, "\t"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 316
    iget v2, p0, Lcom/samsung/android/org/xbill/DNS/Record;->dclass:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    const-string v2, "noPrintIN"

    invoke-static {v2}, Lcom/samsung/android/org/xbill/DNS/Options;->check(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 317
    :cond_2
    iget v2, p0, Lcom/samsung/android/org/xbill/DNS/Record;->dclass:I

    invoke-static {v2}, Lcom/samsung/android/org/xbill/DNS/DClass;->string(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 318
    const-string v2, "\t"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 320
    :cond_3
    iget v2, p0, Lcom/samsung/android/org/xbill/DNS/Record;->type:I

    invoke-static {v2}, Lcom/samsung/android/org/xbill/DNS/Type;->string(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 321
    invoke-virtual {p0}, Lcom/samsung/android/org/xbill/DNS/Record;->rrToString()Ljava/lang/String;

    move-result-object v0

    .line 322
    .local v0, "rdata":Ljava/lang/String;
    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 323
    const-string v2, "\t"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 324
    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 326
    :cond_4
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 314
    .end local v0    # "rdata":Ljava/lang/String;
    :cond_5
    iget-wide v2, p0, Lcom/samsung/android/org/xbill/DNS/Record;->ttl:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    goto :goto_0
.end method

.method toWire(Lcom/samsung/android/org/xbill/DNS/DNSOutput;ILcom/samsung/android/org/xbill/DNS/Compression;)V
    .locals 5
    .param p1, "out"    # Lcom/samsung/android/org/xbill/DNS/DNSOutput;
    .param p2, "section"    # I
    .param p3, "c"    # Lcom/samsung/android/org/xbill/DNS/Compression;

    .prologue
    const/4 v4, 0x0

    .line 215
    iget-object v2, p0, Lcom/samsung/android/org/xbill/DNS/Record;->name:Lcom/samsung/android/org/xbill/DNS/Name;

    invoke-virtual {v2, p1, p3}, Lcom/samsung/android/org/xbill/DNS/Name;->toWire(Lcom/samsung/android/org/xbill/DNS/DNSOutput;Lcom/samsung/android/org/xbill/DNS/Compression;)V

    .line 216
    iget v2, p0, Lcom/samsung/android/org/xbill/DNS/Record;->type:I

    invoke-virtual {p1, v2}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeU16(I)V

    .line 217
    iget v2, p0, Lcom/samsung/android/org/xbill/DNS/Record;->dclass:I

    invoke-virtual {p1, v2}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeU16(I)V

    .line 218
    if-nez p2, :cond_0

    .line 226
    :goto_0
    return-void

    .line 220
    :cond_0
    iget-wide v2, p0, Lcom/samsung/android/org/xbill/DNS/Record;->ttl:J

    invoke-virtual {p1, v2, v3}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeU32(J)V

    .line 221
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->current()I

    move-result v0

    .line 222
    .local v0, "lengthPosition":I
    invoke-virtual {p1, v4}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeU16(I)V

    .line 223
    invoke-virtual {p0, p1, p3, v4}, Lcom/samsung/android/org/xbill/DNS/Record;->rrToWire(Lcom/samsung/android/org/xbill/DNS/DNSOutput;Lcom/samsung/android/org/xbill/DNS/Compression;Z)V

    .line 224
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->current()I

    move-result v2

    sub-int/2addr v2, v0

    add-int/lit8 v1, v2, -0x2

    .line 225
    .local v1, "rrlength":I
    invoke-virtual {p1, v1, v0}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeU16At(II)V

    goto :goto_0
.end method

.method public toWire(I)[B
    .locals 2
    .param p1, "section"    # I

    .prologue
    .line 233
    new-instance v0, Lcom/samsung/android/org/xbill/DNS/DNSOutput;

    invoke-direct {v0}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;-><init>()V

    .line 234
    .local v0, "out":Lcom/samsung/android/org/xbill/DNS/DNSOutput;
    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Lcom/samsung/android/org/xbill/DNS/Record;->toWire(Lcom/samsung/android/org/xbill/DNS/DNSOutput;ILcom/samsung/android/org/xbill/DNS/Compression;)V

    .line 235
    invoke-virtual {v0}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->toByteArray()[B

    move-result-object v1

    return-object v1
.end method

.method public toWireCanonical()[B
    .locals 1

    .prologue
    .line 272
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/android/org/xbill/DNS/Record;->toWireCanonical(Z)[B

    move-result-object v0

    return-object v0
.end method

.method withDClass(IJ)Lcom/samsung/android/org/xbill/DNS/Record;
    .locals 2
    .param p1, "dclass"    # I
    .param p2, "ttl"    # J

    .prologue
    .line 628
    invoke-virtual {p0}, Lcom/samsung/android/org/xbill/DNS/Record;->cloneRecord()Lcom/samsung/android/org/xbill/DNS/Record;

    move-result-object v0

    .line 629
    .local v0, "rec":Lcom/samsung/android/org/xbill/DNS/Record;
    iput p1, v0, Lcom/samsung/android/org/xbill/DNS/Record;->dclass:I

    .line 630
    iput-wide p2, v0, Lcom/samsung/android/org/xbill/DNS/Record;->ttl:J

    .line 631
    return-object v0
.end method

.method public withName(Lcom/samsung/android/org/xbill/DNS/Name;)Lcom/samsung/android/org/xbill/DNS/Record;
    .locals 2
    .param p1, "name"    # Lcom/samsung/android/org/xbill/DNS/Name;

    .prologue
    .line 615
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/Name;->isAbsolute()Z

    move-result v1

    if-nez v1, :cond_0

    .line 616
    new-instance v1, Lcom/samsung/android/org/xbill/DNS/RelativeNameException;

    invoke-direct {v1, p1}, Lcom/samsung/android/org/xbill/DNS/RelativeNameException;-><init>(Lcom/samsung/android/org/xbill/DNS/Name;)V

    throw v1

    .line 617
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/org/xbill/DNS/Record;->cloneRecord()Lcom/samsung/android/org/xbill/DNS/Record;

    move-result-object v0

    .line 618
    .local v0, "rec":Lcom/samsung/android/org/xbill/DNS/Record;
    iput-object p1, v0, Lcom/samsung/android/org/xbill/DNS/Record;->name:Lcom/samsung/android/org/xbill/DNS/Name;

    .line 619
    return-object v0
.end method
