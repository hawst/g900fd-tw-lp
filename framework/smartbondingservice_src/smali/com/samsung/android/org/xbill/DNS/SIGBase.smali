.class abstract Lcom/samsung/android/org/xbill/DNS/SIGBase;
.super Lcom/samsung/android/org/xbill/DNS/Record;
.source "SIGBase.java"


# static fields
.field private static final serialVersionUID:J = -0x33e19f5df1ec9a91L


# instance fields
.field protected alg:I

.field protected covered:I

.field protected expire:Ljava/util/Date;

.field protected footprint:I

.field protected labels:I

.field protected origttl:J

.field protected signature:[B

.field protected signer:Lcom/samsung/android/org/xbill/DNS/Name;

.field protected timeSigned:Ljava/util/Date;


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/samsung/android/org/xbill/DNS/Record;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/org/xbill/DNS/Name;IIJIIJLjava/util/Date;Ljava/util/Date;ILcom/samsung/android/org/xbill/DNS/Name;[B)V
    .locals 4
    .param p1, "name"    # Lcom/samsung/android/org/xbill/DNS/Name;
    .param p2, "type"    # I
    .param p3, "dclass"    # I
    .param p4, "ttl"    # J
    .param p6, "covered"    # I
    .param p7, "alg"    # I
    .param p8, "origttl"    # J
    .param p10, "expire"    # Ljava/util/Date;
    .param p11, "timeSigned"    # Ljava/util/Date;
    .param p12, "footprint"    # I
    .param p13, "signer"    # Lcom/samsung/android/org/xbill/DNS/Name;
    .param p14, "signature"    # [B

    .prologue
    .line 36
    invoke-direct/range {p0 .. p5}, Lcom/samsung/android/org/xbill/DNS/Record;-><init>(Lcom/samsung/android/org/xbill/DNS/Name;IIJ)V

    .line 37
    invoke-static {p6}, Lcom/samsung/android/org/xbill/DNS/Type;->check(I)V

    .line 38
    invoke-static {p8, p9}, Lcom/samsung/android/org/xbill/DNS/TTL;->check(J)V

    .line 39
    iput p6, p0, Lcom/samsung/android/org/xbill/DNS/SIGBase;->covered:I

    .line 40
    const-string v1, "alg"

    invoke-static {v1, p7}, Lcom/samsung/android/org/xbill/DNS/SIGBase;->checkU8(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/org/xbill/DNS/SIGBase;->alg:I

    .line 41
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/Name;->labels()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/samsung/android/org/xbill/DNS/SIGBase;->labels:I

    .line 42
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/Name;->isWild()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 43
    iget v1, p0, Lcom/samsung/android/org/xbill/DNS/SIGBase;->labels:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/samsung/android/org/xbill/DNS/SIGBase;->labels:I

    .line 44
    :cond_0
    iput-wide p8, p0, Lcom/samsung/android/org/xbill/DNS/SIGBase;->origttl:J

    .line 45
    iput-object p10, p0, Lcom/samsung/android/org/xbill/DNS/SIGBase;->expire:Ljava/util/Date;

    .line 46
    iput-object p11, p0, Lcom/samsung/android/org/xbill/DNS/SIGBase;->timeSigned:Ljava/util/Date;

    .line 47
    const-string v1, "footprint"

    move/from16 v0, p12

    invoke-static {v1, v0}, Lcom/samsung/android/org/xbill/DNS/SIGBase;->checkU16(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/org/xbill/DNS/SIGBase;->footprint:I

    .line 48
    const-string v1, "signer"

    move-object/from16 v0, p13

    invoke-static {v1, v0}, Lcom/samsung/android/org/xbill/DNS/SIGBase;->checkName(Ljava/lang/String;Lcom/samsung/android/org/xbill/DNS/Name;)Lcom/samsung/android/org/xbill/DNS/Name;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/org/xbill/DNS/SIGBase;->signer:Lcom/samsung/android/org/xbill/DNS/Name;

    .line 49
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/samsung/android/org/xbill/DNS/SIGBase;->signature:[B

    .line 50
    return-void
.end method


# virtual methods
.method public getAlgorithm()I
    .locals 1

    .prologue
    .line 127
    iget v0, p0, Lcom/samsung/android/org/xbill/DNS/SIGBase;->alg:I

    return v0
.end method

.method public getExpire()Ljava/util/Date;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/SIGBase;->expire:Ljava/util/Date;

    return-object v0
.end method

.method public getFootprint()I
    .locals 1

    .prologue
    .line 161
    iget v0, p0, Lcom/samsung/android/org/xbill/DNS/SIGBase;->footprint:I

    return v0
.end method

.method public getLabels()I
    .locals 1

    .prologue
    .line 137
    iget v0, p0, Lcom/samsung/android/org/xbill/DNS/SIGBase;->labels:I

    return v0
.end method

.method public getOrigTTL()J
    .locals 2

    .prologue
    .line 143
    iget-wide v0, p0, Lcom/samsung/android/org/xbill/DNS/SIGBase;->origttl:J

    return-wide v0
.end method

.method public getSignature()[B
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/SIGBase;->signature:[B

    return-object v0
.end method

.method public getSigner()Lcom/samsung/android/org/xbill/DNS/Name;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/SIGBase;->signer:Lcom/samsung/android/org/xbill/DNS/Name;

    return-object v0
.end method

.method public getTimeSigned()Ljava/util/Date;
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/SIGBase;->timeSigned:Ljava/util/Date;

    return-object v0
.end method

.method public getTypeCovered()I
    .locals 1

    .prologue
    .line 119
    iget v0, p0, Lcom/samsung/android/org/xbill/DNS/SIGBase;->covered:I

    return v0
.end method

.method rdataFromString(Lcom/samsung/android/org/xbill/DNS/Tokenizer;Lcom/samsung/android/org/xbill/DNS/Name;)V
    .locals 4
    .param p1, "st"    # Lcom/samsung/android/org/xbill/DNS/Tokenizer;
    .param p2, "origin"    # Lcom/samsung/android/org/xbill/DNS/Name;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 67
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->getString()Ljava/lang/String;

    move-result-object v1

    .line 68
    .local v1, "typeString":Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/android/org/xbill/DNS/Type;->value(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/samsung/android/org/xbill/DNS/SIGBase;->covered:I

    .line 69
    iget v2, p0, Lcom/samsung/android/org/xbill/DNS/SIGBase;->covered:I

    if-gez v2, :cond_0

    .line 70
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->exception(Ljava/lang/String;)Lcom/samsung/android/org/xbill/DNS/TextParseException;

    move-result-object v2

    throw v2

    .line 71
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->getString()Ljava/lang/String;

    move-result-object v0

    .line 72
    .local v0, "algString":Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/android/org/xbill/DNS/DNSSEC$Algorithm;->value(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/samsung/android/org/xbill/DNS/SIGBase;->alg:I

    .line 73
    iget v2, p0, Lcom/samsung/android/org/xbill/DNS/SIGBase;->alg:I

    if-gez v2, :cond_1

    .line 74
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid algorithm: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->exception(Ljava/lang/String;)Lcom/samsung/android/org/xbill/DNS/TextParseException;

    move-result-object v2

    throw v2

    .line 75
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->getUInt8()I

    move-result v2

    iput v2, p0, Lcom/samsung/android/org/xbill/DNS/SIGBase;->labels:I

    .line 76
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->getTTL()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/samsung/android/org/xbill/DNS/SIGBase;->origttl:J

    .line 77
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->getString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/android/org/xbill/DNS/FormattedTime;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/org/xbill/DNS/SIGBase;->expire:Ljava/util/Date;

    .line 78
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->getString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/android/org/xbill/DNS/FormattedTime;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/org/xbill/DNS/SIGBase;->timeSigned:Ljava/util/Date;

    .line 79
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->getUInt16()I

    move-result v2

    iput v2, p0, Lcom/samsung/android/org/xbill/DNS/SIGBase;->footprint:I

    .line 80
    invoke-virtual {p1, p2}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->getName(Lcom/samsung/android/org/xbill/DNS/Name;)Lcom/samsung/android/org/xbill/DNS/Name;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/org/xbill/DNS/SIGBase;->signer:Lcom/samsung/android/org/xbill/DNS/Name;

    .line 81
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->getBase64()[B

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/org/xbill/DNS/SIGBase;->signature:[B

    .line 82
    return-void
.end method

.method rrFromWire(Lcom/samsung/android/org/xbill/DNS/DNSInput;)V
    .locals 6
    .param p1, "in"    # Lcom/samsung/android/org/xbill/DNS/DNSInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x3e8

    .line 54
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/DNSInput;->readU16()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/org/xbill/DNS/SIGBase;->covered:I

    .line 55
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/DNSInput;->readU8()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/org/xbill/DNS/SIGBase;->alg:I

    .line 56
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/DNSInput;->readU8()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/org/xbill/DNS/SIGBase;->labels:I

    .line 57
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/DNSInput;->readU32()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/org/xbill/DNS/SIGBase;->origttl:J

    .line 58
    new-instance v0, Ljava/util/Date;

    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/DNSInput;->readU32()J

    move-result-wide v2

    mul-long/2addr v2, v4

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    iput-object v0, p0, Lcom/samsung/android/org/xbill/DNS/SIGBase;->expire:Ljava/util/Date;

    .line 59
    new-instance v0, Ljava/util/Date;

    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/DNSInput;->readU32()J

    move-result-wide v2

    mul-long/2addr v2, v4

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    iput-object v0, p0, Lcom/samsung/android/org/xbill/DNS/SIGBase;->timeSigned:Ljava/util/Date;

    .line 60
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/DNSInput;->readU16()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/org/xbill/DNS/SIGBase;->footprint:I

    .line 61
    new-instance v0, Lcom/samsung/android/org/xbill/DNS/Name;

    invoke-direct {v0, p1}, Lcom/samsung/android/org/xbill/DNS/Name;-><init>(Lcom/samsung/android/org/xbill/DNS/DNSInput;)V

    iput-object v0, p0, Lcom/samsung/android/org/xbill/DNS/SIGBase;->signer:Lcom/samsung/android/org/xbill/DNS/Name;

    .line 62
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/DNSInput;->readByteArray()[B

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/org/xbill/DNS/SIGBase;->signature:[B

    .line 63
    return-void
.end method

.method rrToString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 87
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 88
    .local v0, "sb":Ljava/lang/StringBuffer;
    iget v1, p0, Lcom/samsung/android/org/xbill/DNS/SIGBase;->covered:I

    invoke-static {v1}, Lcom/samsung/android/org/xbill/DNS/Type;->string(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 89
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 90
    iget v1, p0, Lcom/samsung/android/org/xbill/DNS/SIGBase;->alg:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 91
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 92
    iget v1, p0, Lcom/samsung/android/org/xbill/DNS/SIGBase;->labels:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 93
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 94
    iget-wide v2, p0, Lcom/samsung/android/org/xbill/DNS/SIGBase;->origttl:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    .line 95
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 96
    const-string v1, "multiline"

    invoke-static {v1}, Lcom/samsung/android/org/xbill/DNS/Options;->check(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 97
    const-string v1, "(\n\t"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 98
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/org/xbill/DNS/SIGBase;->expire:Ljava/util/Date;

    invoke-static {v1}, Lcom/samsung/android/org/xbill/DNS/FormattedTime;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 99
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 100
    iget-object v1, p0, Lcom/samsung/android/org/xbill/DNS/SIGBase;->timeSigned:Ljava/util/Date;

    invoke-static {v1}, Lcom/samsung/android/org/xbill/DNS/FormattedTime;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 101
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 102
    iget v1, p0, Lcom/samsung/android/org/xbill/DNS/SIGBase;->footprint:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 103
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 104
    iget-object v1, p0, Lcom/samsung/android/org/xbill/DNS/SIGBase;->signer:Lcom/samsung/android/org/xbill/DNS/Name;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    .line 105
    const-string v1, "multiline"

    invoke-static {v1}, Lcom/samsung/android/org/xbill/DNS/Options;->check(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 106
    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 107
    iget-object v1, p0, Lcom/samsung/android/org/xbill/DNS/SIGBase;->signature:[B

    const/16 v2, 0x40

    const-string v3, "\t"

    .line 108
    const/4 v4, 0x1

    .line 107
    invoke-static {v1, v2, v3, v4}, Lcom/samsung/android/org/xbill/DNS/utils/base64;->formatString([BILjava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 113
    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 110
    :cond_1
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 111
    iget-object v1, p0, Lcom/samsung/android/org/xbill/DNS/SIGBase;->signature:[B

    invoke-static {v1}, Lcom/samsung/android/org/xbill/DNS/utils/base64;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0
.end method

.method rrToWire(Lcom/samsung/android/org/xbill/DNS/DNSOutput;Lcom/samsung/android/org/xbill/DNS/Compression;Z)V
    .locals 4
    .param p1, "out"    # Lcom/samsung/android/org/xbill/DNS/DNSOutput;
    .param p2, "c"    # Lcom/samsung/android/org/xbill/DNS/Compression;
    .param p3, "canonical"    # Z

    .prologue
    const-wide/16 v2, 0x3e8

    .line 183
    iget v0, p0, Lcom/samsung/android/org/xbill/DNS/SIGBase;->covered:I

    invoke-virtual {p1, v0}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeU16(I)V

    .line 184
    iget v0, p0, Lcom/samsung/android/org/xbill/DNS/SIGBase;->alg:I

    invoke-virtual {p1, v0}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeU8(I)V

    .line 185
    iget v0, p0, Lcom/samsung/android/org/xbill/DNS/SIGBase;->labels:I

    invoke-virtual {p1, v0}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeU8(I)V

    .line 186
    iget-wide v0, p0, Lcom/samsung/android/org/xbill/DNS/SIGBase;->origttl:J

    invoke-virtual {p1, v0, v1}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeU32(J)V

    .line 187
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/SIGBase;->expire:Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    div-long/2addr v0, v2

    invoke-virtual {p1, v0, v1}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeU32(J)V

    .line 188
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/SIGBase;->timeSigned:Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    div-long/2addr v0, v2

    invoke-virtual {p1, v0, v1}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeU32(J)V

    .line 189
    iget v0, p0, Lcom/samsung/android/org/xbill/DNS/SIGBase;->footprint:I

    invoke-virtual {p1, v0}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeU16(I)V

    .line 190
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/SIGBase;->signer:Lcom/samsung/android/org/xbill/DNS/Name;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1, p3}, Lcom/samsung/android/org/xbill/DNS/Name;->toWire(Lcom/samsung/android/org/xbill/DNS/DNSOutput;Lcom/samsung/android/org/xbill/DNS/Compression;Z)V

    .line 191
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/SIGBase;->signature:[B

    invoke-virtual {p1, v0}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeByteArray([B)V

    .line 192
    return-void
.end method

.method setSignature([B)V
    .locals 0
    .param p1, "signature"    # [B

    .prologue
    .line 178
    iput-object p1, p0, Lcom/samsung/android/org/xbill/DNS/SIGBase;->signature:[B

    .line 179
    return-void
.end method
