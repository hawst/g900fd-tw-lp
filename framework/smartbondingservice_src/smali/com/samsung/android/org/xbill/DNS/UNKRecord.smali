.class public Lcom/samsung/android/org/xbill/DNS/UNKRecord;
.super Lcom/samsung/android/org/xbill/DNS/Record;
.source "UNKRecord.java"


# static fields
.field private static final serialVersionUID:J = -0x3a3299cda79a9f63L


# instance fields
.field private data:[B


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/samsung/android/org/xbill/DNS/Record;-><init>()V

    return-void
.end method


# virtual methods
.method public getData()[B
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/UNKRecord;->data:[B

    return-object v0
.end method

.method getObject()Lcom/samsung/android/org/xbill/DNS/Record;
    .locals 1

    .prologue
    .line 24
    new-instance v0, Lcom/samsung/android/org/xbill/DNS/UNKRecord;

    invoke-direct {v0}, Lcom/samsung/android/org/xbill/DNS/UNKRecord;-><init>()V

    return-object v0
.end method

.method rdataFromString(Lcom/samsung/android/org/xbill/DNS/Tokenizer;Lcom/samsung/android/org/xbill/DNS/Name;)V
    .locals 1
    .param p1, "st"    # Lcom/samsung/android/org/xbill/DNS/Tokenizer;
    .param p2, "origin"    # Lcom/samsung/android/org/xbill/DNS/Name;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 34
    const-string v0, "invalid unknown RR encoding"

    invoke-virtual {p1, v0}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->exception(Ljava/lang/String;)Lcom/samsung/android/org/xbill/DNS/TextParseException;

    move-result-object v0

    throw v0
.end method

.method rrFromWire(Lcom/samsung/android/org/xbill/DNS/DNSInput;)V
    .locals 1
    .param p1, "in"    # Lcom/samsung/android/org/xbill/DNS/DNSInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 29
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/DNSInput;->readByteArray()[B

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/org/xbill/DNS/UNKRecord;->data:[B

    .line 30
    return-void
.end method

.method rrToString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/UNKRecord;->data:[B

    invoke-static {v0}, Lcom/samsung/android/org/xbill/DNS/UNKRecord;->unknownToString([B)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method rrToWire(Lcom/samsung/android/org/xbill/DNS/DNSOutput;Lcom/samsung/android/org/xbill/DNS/Compression;Z)V
    .locals 1
    .param p1, "out"    # Lcom/samsung/android/org/xbill/DNS/DNSOutput;
    .param p2, "c"    # Lcom/samsung/android/org/xbill/DNS/Compression;
    .param p3, "canonical"    # Z

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/UNKRecord;->data:[B

    invoke-virtual {p1, v0}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeByteArray([B)V

    .line 52
    return-void
.end method
