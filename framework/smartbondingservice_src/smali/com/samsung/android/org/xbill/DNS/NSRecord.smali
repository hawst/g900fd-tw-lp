.class public Lcom/samsung/android/org/xbill/DNS/NSRecord;
.super Lcom/samsung/android/org/xbill/DNS/SingleCompressedNameBase;
.source "NSRecord.java"


# static fields
.field private static final serialVersionUID:J = 0x6c2c7392fca0ca6L


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/samsung/android/org/xbill/DNS/SingleCompressedNameBase;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/org/xbill/DNS/Name;IJLcom/samsung/android/org/xbill/DNS/Name;)V
    .locals 9
    .param p1, "name"    # Lcom/samsung/android/org/xbill/DNS/Name;
    .param p2, "dclass"    # I
    .param p3, "ttl"    # J
    .param p5, "target"    # Lcom/samsung/android/org/xbill/DNS/Name;

    .prologue
    .line 28
    const/4 v2, 0x2

    const-string v7, "target"

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move-wide v4, p3

    move-object v6, p5

    invoke-direct/range {v0 .. v7}, Lcom/samsung/android/org/xbill/DNS/SingleCompressedNameBase;-><init>(Lcom/samsung/android/org/xbill/DNS/Name;IIJLcom/samsung/android/org/xbill/DNS/Name;Ljava/lang/String;)V

    .line 29
    return-void
.end method


# virtual methods
.method public getAdditionalName()Lcom/samsung/android/org/xbill/DNS/Name;
    .locals 1

    .prologue
    .line 39
    invoke-virtual {p0}, Lcom/samsung/android/org/xbill/DNS/NSRecord;->getSingleName()Lcom/samsung/android/org/xbill/DNS/Name;

    move-result-object v0

    return-object v0
.end method

.method getObject()Lcom/samsung/android/org/xbill/DNS/Record;
    .locals 1

    .prologue
    .line 19
    new-instance v0, Lcom/samsung/android/org/xbill/DNS/NSRecord;

    invoke-direct {v0}, Lcom/samsung/android/org/xbill/DNS/NSRecord;-><init>()V

    return-object v0
.end method

.method public getTarget()Lcom/samsung/android/org/xbill/DNS/Name;
    .locals 1

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/samsung/android/org/xbill/DNS/NSRecord;->getSingleName()Lcom/samsung/android/org/xbill/DNS/Name;

    move-result-object v0

    return-object v0
.end method
