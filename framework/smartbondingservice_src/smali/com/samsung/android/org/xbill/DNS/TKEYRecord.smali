.class public Lcom/samsung/android/org/xbill/DNS/TKEYRecord;
.super Lcom/samsung/android/org/xbill/DNS/Record;
.source "TKEYRecord.java"


# static fields
.field public static final DELETE:I = 0x5

.field public static final DIFFIEHELLMAN:I = 0x2

.field public static final GSSAPI:I = 0x3

.field public static final RESOLVERASSIGNED:I = 0x4

.field public static final SERVERASSIGNED:I = 0x1

.field private static final serialVersionUID:J = 0x7a84fbe2ffd5b7ccL


# instance fields
.field private alg:Lcom/samsung/android/org/xbill/DNS/Name;

.field private error:I

.field private key:[B

.field private mode:I

.field private other:[B

.field private timeExpire:Ljava/util/Date;

.field private timeInception:Ljava/util/Date;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/samsung/android/org/xbill/DNS/Record;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/org/xbill/DNS/Name;IJLcom/samsung/android/org/xbill/DNS/Name;Ljava/util/Date;Ljava/util/Date;II[B[B)V
    .locals 9
    .param p1, "name"    # Lcom/samsung/android/org/xbill/DNS/Name;
    .param p2, "dclass"    # I
    .param p3, "ttl"    # J
    .param p5, "alg"    # Lcom/samsung/android/org/xbill/DNS/Name;
    .param p6, "timeInception"    # Ljava/util/Date;
    .param p7, "timeExpire"    # Ljava/util/Date;
    .param p8, "mode"    # I
    .param p9, "error"    # I
    .param p10, "key"    # [B
    .param p11, "other"    # [B

    .prologue
    .line 69
    const/16 v4, 0xf9

    move-object v2, p0

    move-object v3, p1

    move v5, p2

    move-wide v6, p3

    invoke-direct/range {v2 .. v7}, Lcom/samsung/android/org/xbill/DNS/Record;-><init>(Lcom/samsung/android/org/xbill/DNS/Name;IIJ)V

    .line 70
    const-string v2, "alg"

    invoke-static {v2, p5}, Lcom/samsung/android/org/xbill/DNS/TKEYRecord;->checkName(Ljava/lang/String;Lcom/samsung/android/org/xbill/DNS/Name;)Lcom/samsung/android/org/xbill/DNS/Name;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/org/xbill/DNS/TKEYRecord;->alg:Lcom/samsung/android/org/xbill/DNS/Name;

    .line 71
    iput-object p6, p0, Lcom/samsung/android/org/xbill/DNS/TKEYRecord;->timeInception:Ljava/util/Date;

    .line 72
    move-object/from16 v0, p7

    iput-object v0, p0, Lcom/samsung/android/org/xbill/DNS/TKEYRecord;->timeExpire:Ljava/util/Date;

    .line 73
    const-string v2, "mode"

    move/from16 v0, p8

    invoke-static {v2, v0}, Lcom/samsung/android/org/xbill/DNS/TKEYRecord;->checkU16(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/samsung/android/org/xbill/DNS/TKEYRecord;->mode:I

    .line 74
    const-string v2, "error"

    move/from16 v0, p9

    invoke-static {v2, v0}, Lcom/samsung/android/org/xbill/DNS/TKEYRecord;->checkU16(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/samsung/android/org/xbill/DNS/TKEYRecord;->error:I

    .line 75
    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/samsung/android/org/xbill/DNS/TKEYRecord;->key:[B

    .line 76
    move-object/from16 v0, p11

    iput-object v0, p0, Lcom/samsung/android/org/xbill/DNS/TKEYRecord;->other:[B

    .line 77
    return-void
.end method


# virtual methods
.method public getAlgorithm()Lcom/samsung/android/org/xbill/DNS/Name;
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/TKEYRecord;->alg:Lcom/samsung/android/org/xbill/DNS/Name;

    return-object v0
.end method

.method public getError()I
    .locals 1

    .prologue
    .line 186
    iget v0, p0, Lcom/samsung/android/org/xbill/DNS/TKEYRecord;->error:I

    return v0
.end method

.method public getKey()[B
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/TKEYRecord;->key:[B

    return-object v0
.end method

.method public getMode()I
    .locals 1

    .prologue
    .line 180
    iget v0, p0, Lcom/samsung/android/org/xbill/DNS/TKEYRecord;->mode:I

    return v0
.end method

.method getObject()Lcom/samsung/android/org/xbill/DNS/Record;
    .locals 1

    .prologue
    .line 48
    new-instance v0, Lcom/samsung/android/org/xbill/DNS/TKEYRecord;

    invoke-direct {v0}, Lcom/samsung/android/org/xbill/DNS/TKEYRecord;-><init>()V

    return-object v0
.end method

.method public getOther()[B
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/TKEYRecord;->other:[B

    return-object v0
.end method

.method public getTimeExpire()Ljava/util/Date;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/TKEYRecord;->timeExpire:Ljava/util/Date;

    return-object v0
.end method

.method public getTimeInception()Ljava/util/Date;
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/TKEYRecord;->timeInception:Ljava/util/Date;

    return-object v0
.end method

.method protected modeString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 107
    iget v0, p0, Lcom/samsung/android/org/xbill/DNS/TKEYRecord;->mode:I

    packed-switch v0, :pswitch_data_0

    .line 113
    iget v0, p0, Lcom/samsung/android/org/xbill/DNS/TKEYRecord;->mode:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 108
    :pswitch_0
    const-string v0, "SERVERASSIGNED"

    goto :goto_0

    .line 109
    :pswitch_1
    const-string v0, "DIFFIEHELLMAN"

    goto :goto_0

    .line 110
    :pswitch_2
    const-string v0, "GSSAPI"

    goto :goto_0

    .line 111
    :pswitch_3
    const-string v0, "RESOLVERASSIGNED"

    goto :goto_0

    .line 112
    :pswitch_4
    const-string v0, "DELETE"

    goto :goto_0

    .line 107
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method rdataFromString(Lcom/samsung/android/org/xbill/DNS/Tokenizer;Lcom/samsung/android/org/xbill/DNS/Name;)V
    .locals 1
    .param p1, "st"    # Lcom/samsung/android/org/xbill/DNS/Tokenizer;
    .param p2, "origin"    # Lcom/samsung/android/org/xbill/DNS/Name;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 102
    const-string v0, "no text format defined for TKEY"

    invoke-virtual {p1, v0}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->exception(Ljava/lang/String;)Lcom/samsung/android/org/xbill/DNS/TextParseException;

    move-result-object v0

    throw v0
.end method

.method rrFromWire(Lcom/samsung/android/org/xbill/DNS/DNSInput;)V
    .locals 8
    .param p1, "in"    # Lcom/samsung/android/org/xbill/DNS/DNSInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v6, 0x3e8

    const/4 v3, 0x0

    .line 81
    new-instance v2, Lcom/samsung/android/org/xbill/DNS/Name;

    invoke-direct {v2, p1}, Lcom/samsung/android/org/xbill/DNS/Name;-><init>(Lcom/samsung/android/org/xbill/DNS/DNSInput;)V

    iput-object v2, p0, Lcom/samsung/android/org/xbill/DNS/TKEYRecord;->alg:Lcom/samsung/android/org/xbill/DNS/Name;

    .line 82
    new-instance v2, Ljava/util/Date;

    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/DNSInput;->readU32()J

    move-result-wide v4

    mul-long/2addr v4, v6

    invoke-direct {v2, v4, v5}, Ljava/util/Date;-><init>(J)V

    iput-object v2, p0, Lcom/samsung/android/org/xbill/DNS/TKEYRecord;->timeInception:Ljava/util/Date;

    .line 83
    new-instance v2, Ljava/util/Date;

    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/DNSInput;->readU32()J

    move-result-wide v4

    mul-long/2addr v4, v6

    invoke-direct {v2, v4, v5}, Ljava/util/Date;-><init>(J)V

    iput-object v2, p0, Lcom/samsung/android/org/xbill/DNS/TKEYRecord;->timeExpire:Ljava/util/Date;

    .line 84
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/DNSInput;->readU16()I

    move-result v2

    iput v2, p0, Lcom/samsung/android/org/xbill/DNS/TKEYRecord;->mode:I

    .line 85
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/DNSInput;->readU16()I

    move-result v2

    iput v2, p0, Lcom/samsung/android/org/xbill/DNS/TKEYRecord;->error:I

    .line 87
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/DNSInput;->readU16()I

    move-result v0

    .line 88
    .local v0, "keylen":I
    if-lez v0, :cond_0

    .line 89
    invoke-virtual {p1, v0}, Lcom/samsung/android/org/xbill/DNS/DNSInput;->readByteArray(I)[B

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/org/xbill/DNS/TKEYRecord;->key:[B

    .line 93
    :goto_0
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/DNSInput;->readU16()I

    move-result v1

    .line 94
    .local v1, "otherlen":I
    if-lez v1, :cond_1

    .line 95
    invoke-virtual {p1, v1}, Lcom/samsung/android/org/xbill/DNS/DNSInput;->readByteArray(I)[B

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/org/xbill/DNS/TKEYRecord;->other:[B

    .line 98
    :goto_1
    return-void

    .line 91
    .end local v1    # "otherlen":I
    :cond_0
    iput-object v3, p0, Lcom/samsung/android/org/xbill/DNS/TKEYRecord;->key:[B

    goto :goto_0

    .line 97
    .restart local v1    # "otherlen":I
    :cond_1
    iput-object v3, p0, Lcom/samsung/android/org/xbill/DNS/TKEYRecord;->other:[B

    goto :goto_1
.end method

.method rrToString()Ljava/lang/String;
    .locals 5

    .prologue
    const/16 v4, 0x40

    const/4 v3, 0x0

    .line 120
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 121
    .local v0, "sb":Ljava/lang/StringBuffer;
    iget-object v1, p0, Lcom/samsung/android/org/xbill/DNS/TKEYRecord;->alg:Lcom/samsung/android/org/xbill/DNS/Name;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    .line 122
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 123
    const-string v1, "multiline"

    invoke-static {v1}, Lcom/samsung/android/org/xbill/DNS/Options;->check(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 124
    const-string v1, "(\n\t"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 125
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/org/xbill/DNS/TKEYRecord;->timeInception:Ljava/util/Date;

    invoke-static {v1}, Lcom/samsung/android/org/xbill/DNS/FormattedTime;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 126
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 127
    iget-object v1, p0, Lcom/samsung/android/org/xbill/DNS/TKEYRecord;->timeExpire:Ljava/util/Date;

    invoke-static {v1}, Lcom/samsung/android/org/xbill/DNS/FormattedTime;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 128
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 129
    invoke-virtual {p0}, Lcom/samsung/android/org/xbill/DNS/TKEYRecord;->modeString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 130
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 131
    iget v1, p0, Lcom/samsung/android/org/xbill/DNS/TKEYRecord;->error:I

    invoke-static {v1}, Lcom/samsung/android/org/xbill/DNS/Rcode;->TSIGstring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 132
    const-string v1, "multiline"

    invoke-static {v1}, Lcom/samsung/android/org/xbill/DNS/Options;->check(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 133
    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 134
    iget-object v1, p0, Lcom/samsung/android/org/xbill/DNS/TKEYRecord;->key:[B

    if-eqz v1, :cond_1

    .line 135
    iget-object v1, p0, Lcom/samsung/android/org/xbill/DNS/TKEYRecord;->key:[B

    const-string v2, "\t"

    invoke-static {v1, v4, v2, v3}, Lcom/samsung/android/org/xbill/DNS/utils/base64;->formatString([BILjava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 136
    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 138
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/org/xbill/DNS/TKEYRecord;->other:[B

    if-eqz v1, :cond_2

    .line 139
    iget-object v1, p0, Lcom/samsung/android/org/xbill/DNS/TKEYRecord;->other:[B

    const-string v2, "\t"

    invoke-static {v1, v4, v2, v3}, Lcom/samsung/android/org/xbill/DNS/utils/base64;->formatString([BILjava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 140
    :cond_2
    const-string v1, " )"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 150
    :cond_3
    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 142
    :cond_4
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 143
    iget-object v1, p0, Lcom/samsung/android/org/xbill/DNS/TKEYRecord;->key:[B

    if-eqz v1, :cond_5

    .line 144
    iget-object v1, p0, Lcom/samsung/android/org/xbill/DNS/TKEYRecord;->key:[B

    invoke-static {v1}, Lcom/samsung/android/org/xbill/DNS/utils/base64;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 145
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 147
    :cond_5
    iget-object v1, p0, Lcom/samsung/android/org/xbill/DNS/TKEYRecord;->other:[B

    if-eqz v1, :cond_3

    .line 148
    iget-object v1, p0, Lcom/samsung/android/org/xbill/DNS/TKEYRecord;->other:[B

    invoke-static {v1}, Lcom/samsung/android/org/xbill/DNS/utils/base64;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0
.end method

.method rrToWire(Lcom/samsung/android/org/xbill/DNS/DNSOutput;Lcom/samsung/android/org/xbill/DNS/Compression;Z)V
    .locals 6
    .param p1, "out"    # Lcom/samsung/android/org/xbill/DNS/DNSOutput;
    .param p2, "c"    # Lcom/samsung/android/org/xbill/DNS/Compression;
    .param p3, "canonical"    # Z

    .prologue
    const-wide/16 v4, 0x3e8

    const/4 v2, 0x0

    .line 203
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/TKEYRecord;->alg:Lcom/samsung/android/org/xbill/DNS/Name;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1, p3}, Lcom/samsung/android/org/xbill/DNS/Name;->toWire(Lcom/samsung/android/org/xbill/DNS/DNSOutput;Lcom/samsung/android/org/xbill/DNS/Compression;Z)V

    .line 205
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/TKEYRecord;->timeInception:Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    div-long/2addr v0, v4

    invoke-virtual {p1, v0, v1}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeU32(J)V

    .line 206
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/TKEYRecord;->timeExpire:Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    div-long/2addr v0, v4

    invoke-virtual {p1, v0, v1}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeU32(J)V

    .line 208
    iget v0, p0, Lcom/samsung/android/org/xbill/DNS/TKEYRecord;->mode:I

    invoke-virtual {p1, v0}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeU16(I)V

    .line 209
    iget v0, p0, Lcom/samsung/android/org/xbill/DNS/TKEYRecord;->error:I

    invoke-virtual {p1, v0}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeU16(I)V

    .line 211
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/TKEYRecord;->key:[B

    if-eqz v0, :cond_0

    .line 212
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/TKEYRecord;->key:[B

    array-length v0, v0

    invoke-virtual {p1, v0}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeU16(I)V

    .line 213
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/TKEYRecord;->key:[B

    invoke-virtual {p1, v0}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeByteArray([B)V

    .line 218
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/TKEYRecord;->other:[B

    if-eqz v0, :cond_1

    .line 219
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/TKEYRecord;->other:[B

    array-length v0, v0

    invoke-virtual {p1, v0}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeU16(I)V

    .line 220
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/TKEYRecord;->other:[B

    invoke-virtual {p1, v0}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeByteArray([B)V

    .line 224
    :goto_1
    return-void

    .line 216
    :cond_0
    invoke-virtual {p1, v2}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeU16(I)V

    goto :goto_0

    .line 223
    :cond_1
    invoke-virtual {p1, v2}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeU16(I)V

    goto :goto_1
.end method
