.class public Lcom/samsung/android/org/xbill/DNS/SSHFPRecord;
.super Lcom/samsung/android/org/xbill/DNS/Record;
.source "SSHFPRecord.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/org/xbill/DNS/SSHFPRecord$Algorithm;,
        Lcom/samsung/android/org/xbill/DNS/SSHFPRecord$Digest;
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = -0x7079aefc33d7bf31L


# instance fields
.field private alg:I

.field private digestType:I

.field private fingerprint:[B


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/samsung/android/org/xbill/DNS/Record;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/org/xbill/DNS/Name;IJII[B)V
    .locals 7
    .param p1, "name"    # Lcom/samsung/android/org/xbill/DNS/Name;
    .param p2, "dclass"    # I
    .param p3, "ttl"    # J
    .param p5, "alg"    # I
    .param p6, "digestType"    # I
    .param p7, "fingerprint"    # [B

    .prologue
    .line 53
    const/16 v2, 0x2c

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/org/xbill/DNS/Record;-><init>(Lcom/samsung/android/org/xbill/DNS/Name;IIJ)V

    .line 54
    const-string v0, "alg"

    invoke-static {v0, p5}, Lcom/samsung/android/org/xbill/DNS/SSHFPRecord;->checkU8(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/org/xbill/DNS/SSHFPRecord;->alg:I

    .line 55
    const-string v0, "digestType"

    invoke-static {v0, p6}, Lcom/samsung/android/org/xbill/DNS/SSHFPRecord;->checkU8(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/org/xbill/DNS/SSHFPRecord;->digestType:I

    .line 56
    iput-object p7, p0, Lcom/samsung/android/org/xbill/DNS/SSHFPRecord;->fingerprint:[B

    .line 57
    return-void
.end method


# virtual methods
.method public getAlgorithm()I
    .locals 1

    .prologue
    .line 87
    iget v0, p0, Lcom/samsung/android/org/xbill/DNS/SSHFPRecord;->alg:I

    return v0
.end method

.method public getDigestType()I
    .locals 1

    .prologue
    .line 93
    iget v0, p0, Lcom/samsung/android/org/xbill/DNS/SSHFPRecord;->digestType:I

    return v0
.end method

.method public getFingerPrint()[B
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/SSHFPRecord;->fingerprint:[B

    return-object v0
.end method

.method getObject()Lcom/samsung/android/org/xbill/DNS/Record;
    .locals 1

    .prologue
    .line 40
    new-instance v0, Lcom/samsung/android/org/xbill/DNS/SSHFPRecord;

    invoke-direct {v0}, Lcom/samsung/android/org/xbill/DNS/SSHFPRecord;-><init>()V

    return-object v0
.end method

.method rdataFromString(Lcom/samsung/android/org/xbill/DNS/Tokenizer;Lcom/samsung/android/org/xbill/DNS/Name;)V
    .locals 1
    .param p1, "st"    # Lcom/samsung/android/org/xbill/DNS/Tokenizer;
    .param p2, "origin"    # Lcom/samsung/android/org/xbill/DNS/Name;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 68
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->getUInt8()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/org/xbill/DNS/SSHFPRecord;->alg:I

    .line 69
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->getUInt8()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/org/xbill/DNS/SSHFPRecord;->digestType:I

    .line 70
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->getHex(Z)[B

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/org/xbill/DNS/SSHFPRecord;->fingerprint:[B

    .line 71
    return-void
.end method

.method rrFromWire(Lcom/samsung/android/org/xbill/DNS/DNSInput;)V
    .locals 1
    .param p1, "in"    # Lcom/samsung/android/org/xbill/DNS/DNSInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 61
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/DNSInput;->readU8()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/org/xbill/DNS/SSHFPRecord;->alg:I

    .line 62
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/DNSInput;->readU8()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/org/xbill/DNS/SSHFPRecord;->digestType:I

    .line 63
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/DNSInput;->readByteArray()[B

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/org/xbill/DNS/SSHFPRecord;->fingerprint:[B

    .line 64
    return-void
.end method

.method rrToString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 75
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 76
    .local v0, "sb":Ljava/lang/StringBuffer;
    iget v1, p0, Lcom/samsung/android/org/xbill/DNS/SSHFPRecord;->alg:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 77
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 78
    iget v1, p0, Lcom/samsung/android/org/xbill/DNS/SSHFPRecord;->digestType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 79
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 80
    iget-object v1, p0, Lcom/samsung/android/org/xbill/DNS/SSHFPRecord;->fingerprint:[B

    invoke-static {v1}, Lcom/samsung/android/org/xbill/DNS/utils/base16;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 81
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method rrToWire(Lcom/samsung/android/org/xbill/DNS/DNSOutput;Lcom/samsung/android/org/xbill/DNS/Compression;Z)V
    .locals 1
    .param p1, "out"    # Lcom/samsung/android/org/xbill/DNS/DNSOutput;
    .param p2, "c"    # Lcom/samsung/android/org/xbill/DNS/Compression;
    .param p3, "canonical"    # Z

    .prologue
    .line 104
    iget v0, p0, Lcom/samsung/android/org/xbill/DNS/SSHFPRecord;->alg:I

    invoke-virtual {p1, v0}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeU8(I)V

    .line 105
    iget v0, p0, Lcom/samsung/android/org/xbill/DNS/SSHFPRecord;->digestType:I

    invoke-virtual {p1, v0}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeU8(I)V

    .line 106
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/SSHFPRecord;->fingerprint:[B

    invoke-virtual {p1, v0}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeByteArray([B)V

    .line 107
    return-void
.end method
