.class public Lcom/samsung/android/org/xbill/DNS/TSIG;
.super Ljava/lang/Object;
.source "TSIG.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/org/xbill/DNS/TSIG$StreamVerifier;
    }
.end annotation


# static fields
.field public static final FUDGE:S = 0x12cs

.field public static final HMAC:Lcom/samsung/android/org/xbill/DNS/Name;

.field public static final HMAC_MD5:Lcom/samsung/android/org/xbill/DNS/Name;

.field private static final HMAC_MD5_STR:Ljava/lang/String; = "HMAC-MD5.SIG-ALG.REG.INT."

.field public static final HMAC_SHA1:Lcom/samsung/android/org/xbill/DNS/Name;

.field private static final HMAC_SHA1_STR:Ljava/lang/String; = "hmac-sha1."

.field public static final HMAC_SHA224:Lcom/samsung/android/org/xbill/DNS/Name;

.field private static final HMAC_SHA224_STR:Ljava/lang/String; = "hmac-sha224."

.field public static final HMAC_SHA256:Lcom/samsung/android/org/xbill/DNS/Name;

.field private static final HMAC_SHA256_STR:Ljava/lang/String; = "hmac-sha256."

.field public static final HMAC_SHA384:Lcom/samsung/android/org/xbill/DNS/Name;

.field private static final HMAC_SHA384_STR:Ljava/lang/String; = "hmac-sha384."

.field public static final HMAC_SHA512:Lcom/samsung/android/org/xbill/DNS/Name;

.field private static final HMAC_SHA512_STR:Ljava/lang/String; = "hmac-sha512."


# instance fields
.field private alg:Lcom/samsung/android/org/xbill/DNS/Name;

.field private digest:Ljava/lang/String;

.field private digestBlockLength:I

.field private key:[B

.field private name:Lcom/samsung/android/org/xbill/DNS/Name;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-string v0, "HMAC-MD5.SIG-ALG.REG.INT."

    invoke-static {v0}, Lcom/samsung/android/org/xbill/DNS/Name;->fromConstantString(Ljava/lang/String;)Lcom/samsung/android/org/xbill/DNS/Name;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/org/xbill/DNS/TSIG;->HMAC_MD5:Lcom/samsung/android/org/xbill/DNS/Name;

    .line 30
    sget-object v0, Lcom/samsung/android/org/xbill/DNS/TSIG;->HMAC_MD5:Lcom/samsung/android/org/xbill/DNS/Name;

    sput-object v0, Lcom/samsung/android/org/xbill/DNS/TSIG;->HMAC:Lcom/samsung/android/org/xbill/DNS/Name;

    .line 33
    const-string v0, "hmac-sha1."

    invoke-static {v0}, Lcom/samsung/android/org/xbill/DNS/Name;->fromConstantString(Ljava/lang/String;)Lcom/samsung/android/org/xbill/DNS/Name;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/org/xbill/DNS/TSIG;->HMAC_SHA1:Lcom/samsung/android/org/xbill/DNS/Name;

    .line 40
    const-string v0, "hmac-sha224."

    invoke-static {v0}, Lcom/samsung/android/org/xbill/DNS/Name;->fromConstantString(Ljava/lang/String;)Lcom/samsung/android/org/xbill/DNS/Name;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/org/xbill/DNS/TSIG;->HMAC_SHA224:Lcom/samsung/android/org/xbill/DNS/Name;

    .line 43
    const-string v0, "hmac-sha256."

    invoke-static {v0}, Lcom/samsung/android/org/xbill/DNS/Name;->fromConstantString(Ljava/lang/String;)Lcom/samsung/android/org/xbill/DNS/Name;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/org/xbill/DNS/TSIG;->HMAC_SHA256:Lcom/samsung/android/org/xbill/DNS/Name;

    .line 46
    const-string v0, "hmac-sha384."

    invoke-static {v0}, Lcom/samsung/android/org/xbill/DNS/Name;->fromConstantString(Ljava/lang/String;)Lcom/samsung/android/org/xbill/DNS/Name;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/org/xbill/DNS/TSIG;->HMAC_SHA384:Lcom/samsung/android/org/xbill/DNS/Name;

    .line 49
    const-string v0, "hmac-sha512."

    invoke-static {v0}, Lcom/samsung/android/org/xbill/DNS/Name;->fromConstantString(Ljava/lang/String;)Lcom/samsung/android/org/xbill/DNS/Name;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/org/xbill/DNS/TSIG;->HMAC_SHA512:Lcom/samsung/android/org/xbill/DNS/Name;

    .line 55
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/org/xbill/DNS/Name;Lcom/samsung/android/org/xbill/DNS/Name;[B)V
    .locals 0
    .param p1, "algorithm"    # Lcom/samsung/android/org/xbill/DNS/Name;
    .param p2, "name"    # Lcom/samsung/android/org/xbill/DNS/Name;
    .param p3, "key"    # [B

    .prologue
    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    iput-object p2, p0, Lcom/samsung/android/org/xbill/DNS/TSIG;->name:Lcom/samsung/android/org/xbill/DNS/Name;

    .line 95
    iput-object p1, p0, Lcom/samsung/android/org/xbill/DNS/TSIG;->alg:Lcom/samsung/android/org/xbill/DNS/Name;

    .line 96
    iput-object p3, p0, Lcom/samsung/android/org/xbill/DNS/TSIG;->key:[B

    .line 97
    invoke-direct {p0}, Lcom/samsung/android/org/xbill/DNS/TSIG;->getDigest()V

    .line 98
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/org/xbill/DNS/Name;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "algorithm"    # Lcom/samsung/android/org/xbill/DNS/Name;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "key"    # Ljava/lang/String;

    .prologue
    .line 119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 120
    invoke-static {p3}, Lcom/samsung/android/org/xbill/DNS/utils/base64;->fromString(Ljava/lang/String;)[B

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/org/xbill/DNS/TSIG;->key:[B

    .line 121
    iget-object v1, p0, Lcom/samsung/android/org/xbill/DNS/TSIG;->key:[B

    if-nez v1, :cond_0

    .line 122
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Invalid TSIG key string"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 124
    :cond_0
    :try_start_0
    sget-object v1, Lcom/samsung/android/org/xbill/DNS/Name;->root:Lcom/samsung/android/org/xbill/DNS/Name;

    invoke-static {p2, v1}, Lcom/samsung/android/org/xbill/DNS/Name;->fromString(Ljava/lang/String;Lcom/samsung/android/org/xbill/DNS/Name;)Lcom/samsung/android/org/xbill/DNS/Name;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/org/xbill/DNS/TSIG;->name:Lcom/samsung/android/org/xbill/DNS/Name;
    :try_end_0
    .catch Lcom/samsung/android/org/xbill/DNS/TextParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 129
    iput-object p1, p0, Lcom/samsung/android/org/xbill/DNS/TSIG;->alg:Lcom/samsung/android/org/xbill/DNS/Name;

    .line 130
    invoke-direct {p0}, Lcom/samsung/android/org/xbill/DNS/TSIG;->getDigest()V

    .line 131
    return-void

    .line 126
    :catch_0
    move-exception v0

    .line 127
    .local v0, "e":Lcom/samsung/android/org/xbill/DNS/TextParseException;
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Invalid TSIG key name"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public constructor <init>(Lcom/samsung/android/org/xbill/DNS/Name;[B)V
    .locals 1
    .param p1, "name"    # Lcom/samsung/android/org/xbill/DNS/Name;
    .param p2, "key"    # [B

    .prologue
    .line 108
    sget-object v0, Lcom/samsung/android/org/xbill/DNS/TSIG;->HMAC_MD5:Lcom/samsung/android/org/xbill/DNS/Name;

    invoke-direct {p0, v0, p1, p2}, Lcom/samsung/android/org/xbill/DNS/TSIG;-><init>(Lcom/samsung/android/org/xbill/DNS/Name;Lcom/samsung/android/org/xbill/DNS/Name;[B)V

    .line 109
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 173
    sget-object v0, Lcom/samsung/android/org/xbill/DNS/TSIG;->HMAC_MD5:Lcom/samsung/android/org/xbill/DNS/Name;

    invoke-direct {p0, v0, p1, p2}, Lcom/samsung/android/org/xbill/DNS/TSIG;-><init>(Lcom/samsung/android/org/xbill/DNS/Name;Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "algorithm"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "key"    # Ljava/lang/String;

    .prologue
    .line 145
    sget-object v0, Lcom/samsung/android/org/xbill/DNS/TSIG;->HMAC_MD5:Lcom/samsung/android/org/xbill/DNS/Name;

    invoke-direct {p0, v0, p2, p3}, Lcom/samsung/android/org/xbill/DNS/TSIG;-><init>(Lcom/samsung/android/org/xbill/DNS/Name;Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    const-string v0, "hmac-md5"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 147
    sget-object v0, Lcom/samsung/android/org/xbill/DNS/TSIG;->HMAC_MD5:Lcom/samsung/android/org/xbill/DNS/Name;

    iput-object v0, p0, Lcom/samsung/android/org/xbill/DNS/TSIG;->alg:Lcom/samsung/android/org/xbill/DNS/Name;

    .line 160
    :goto_0
    invoke-direct {p0}, Lcom/samsung/android/org/xbill/DNS/TSIG;->getDigest()V

    .line 161
    return-void

    .line 148
    :cond_0
    const-string v0, "hmac-sha1"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 149
    sget-object v0, Lcom/samsung/android/org/xbill/DNS/TSIG;->HMAC_SHA1:Lcom/samsung/android/org/xbill/DNS/Name;

    iput-object v0, p0, Lcom/samsung/android/org/xbill/DNS/TSIG;->alg:Lcom/samsung/android/org/xbill/DNS/Name;

    goto :goto_0

    .line 150
    :cond_1
    const-string v0, "hmac-sha224"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 151
    sget-object v0, Lcom/samsung/android/org/xbill/DNS/TSIG;->HMAC_SHA224:Lcom/samsung/android/org/xbill/DNS/Name;

    iput-object v0, p0, Lcom/samsung/android/org/xbill/DNS/TSIG;->alg:Lcom/samsung/android/org/xbill/DNS/Name;

    goto :goto_0

    .line 152
    :cond_2
    const-string v0, "hmac-sha256"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 153
    sget-object v0, Lcom/samsung/android/org/xbill/DNS/TSIG;->HMAC_SHA256:Lcom/samsung/android/org/xbill/DNS/Name;

    iput-object v0, p0, Lcom/samsung/android/org/xbill/DNS/TSIG;->alg:Lcom/samsung/android/org/xbill/DNS/Name;

    goto :goto_0

    .line 154
    :cond_3
    const-string v0, "hmac-sha384"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 155
    sget-object v0, Lcom/samsung/android/org/xbill/DNS/TSIG;->HMAC_SHA384:Lcom/samsung/android/org/xbill/DNS/Name;

    iput-object v0, p0, Lcom/samsung/android/org/xbill/DNS/TSIG;->alg:Lcom/samsung/android/org/xbill/DNS/Name;

    goto :goto_0

    .line 156
    :cond_4
    const-string v0, "hmac-sha512"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 157
    sget-object v0, Lcom/samsung/android/org/xbill/DNS/TSIG;->HMAC_SHA512:Lcom/samsung/android/org/xbill/DNS/Name;

    iput-object v0, p0, Lcom/samsung/android/org/xbill/DNS/TSIG;->alg:Lcom/samsung/android/org/xbill/DNS/Name;

    goto :goto_0

    .line 159
    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid TSIG algorithm"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method static synthetic access$0(Lcom/samsung/android/org/xbill/DNS/TSIG;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/TSIG;->digest:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/org/xbill/DNS/TSIG;)I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lcom/samsung/android/org/xbill/DNS/TSIG;->digestBlockLength:I

    return v0
.end method

.method static synthetic access$2(Lcom/samsung/android/org/xbill/DNS/TSIG;)[B
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/TSIG;->key:[B

    return-object v0
.end method

.method static synthetic access$3(Lcom/samsung/android/org/xbill/DNS/TSIG;)Lcom/samsung/android/org/xbill/DNS/Name;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/TSIG;->name:Lcom/samsung/android/org/xbill/DNS/Name;

    return-object v0
.end method

.method static synthetic access$4(Lcom/samsung/android/org/xbill/DNS/TSIG;)Lcom/samsung/android/org/xbill/DNS/Name;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/TSIG;->alg:Lcom/samsung/android/org/xbill/DNS/Name;

    return-object v0
.end method

.method public static fromString(Ljava/lang/String;)Lcom/samsung/android/org/xbill/DNS/TSIG;
    .locals 9
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x3

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x2

    .line 188
    const-string v2, "[:/]"

    invoke-virtual {p0, v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v1

    .line 189
    .local v1, "parts":[Ljava/lang/String;
    array-length v2, v1

    if-ge v2, v6, :cond_0

    .line 190
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Invalid TSIG key specification"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 192
    :cond_0
    array-length v2, v1

    if-ne v2, v3, :cond_1

    .line 194
    :try_start_0
    new-instance v2, Lcom/samsung/android/org/xbill/DNS/TSIG;

    const/4 v3, 0x0

    aget-object v3, v1, v3

    const/4 v4, 0x1

    aget-object v4, v1, v4

    const/4 v5, 0x2

    aget-object v5, v1, v5

    invoke-direct {v2, v3, v4, v5}, Lcom/samsung/android/org/xbill/DNS/TSIG;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 199
    :goto_0
    return-object v2

    .line 195
    :catch_0
    move-exception v0

    .line 196
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v2, "[:/]"

    invoke-virtual {p0, v2, v6}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v1

    .line 199
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :cond_1
    new-instance v2, Lcom/samsung/android/org/xbill/DNS/TSIG;

    sget-object v3, Lcom/samsung/android/org/xbill/DNS/TSIG;->HMAC_MD5:Lcom/samsung/android/org/xbill/DNS/Name;

    aget-object v4, v1, v7

    aget-object v5, v1, v8

    invoke-direct {v2, v3, v4, v5}, Lcom/samsung/android/org/xbill/DNS/TSIG;-><init>(Lcom/samsung/android/org/xbill/DNS/Name;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private getDigest()V
    .locals 4

    .prologue
    const/16 v3, 0x80

    const/16 v2, 0x40

    .line 64
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/TSIG;->alg:Lcom/samsung/android/org/xbill/DNS/Name;

    sget-object v1, Lcom/samsung/android/org/xbill/DNS/TSIG;->HMAC_MD5:Lcom/samsung/android/org/xbill/DNS/Name;

    invoke-virtual {v0, v1}, Lcom/samsung/android/org/xbill/DNS/Name;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 65
    const-string v0, "md5"

    iput-object v0, p0, Lcom/samsung/android/org/xbill/DNS/TSIG;->digest:Ljava/lang/String;

    .line 66
    iput v2, p0, Lcom/samsung/android/org/xbill/DNS/TSIG;->digestBlockLength:I

    .line 84
    :goto_0
    return-void

    .line 67
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/TSIG;->alg:Lcom/samsung/android/org/xbill/DNS/Name;

    sget-object v1, Lcom/samsung/android/org/xbill/DNS/TSIG;->HMAC_SHA1:Lcom/samsung/android/org/xbill/DNS/Name;

    invoke-virtual {v0, v1}, Lcom/samsung/android/org/xbill/DNS/Name;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 68
    const-string v0, "sha-1"

    iput-object v0, p0, Lcom/samsung/android/org/xbill/DNS/TSIG;->digest:Ljava/lang/String;

    .line 69
    iput v2, p0, Lcom/samsung/android/org/xbill/DNS/TSIG;->digestBlockLength:I

    goto :goto_0

    .line 70
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/TSIG;->alg:Lcom/samsung/android/org/xbill/DNS/Name;

    sget-object v1, Lcom/samsung/android/org/xbill/DNS/TSIG;->HMAC_SHA224:Lcom/samsung/android/org/xbill/DNS/Name;

    invoke-virtual {v0, v1}, Lcom/samsung/android/org/xbill/DNS/Name;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 71
    const-string v0, "sha-224"

    iput-object v0, p0, Lcom/samsung/android/org/xbill/DNS/TSIG;->digest:Ljava/lang/String;

    .line 72
    iput v2, p0, Lcom/samsung/android/org/xbill/DNS/TSIG;->digestBlockLength:I

    goto :goto_0

    .line 73
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/TSIG;->alg:Lcom/samsung/android/org/xbill/DNS/Name;

    sget-object v1, Lcom/samsung/android/org/xbill/DNS/TSIG;->HMAC_SHA256:Lcom/samsung/android/org/xbill/DNS/Name;

    invoke-virtual {v0, v1}, Lcom/samsung/android/org/xbill/DNS/Name;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 74
    const-string v0, "sha-256"

    iput-object v0, p0, Lcom/samsung/android/org/xbill/DNS/TSIG;->digest:Ljava/lang/String;

    .line 75
    iput v2, p0, Lcom/samsung/android/org/xbill/DNS/TSIG;->digestBlockLength:I

    goto :goto_0

    .line 76
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/TSIG;->alg:Lcom/samsung/android/org/xbill/DNS/Name;

    sget-object v1, Lcom/samsung/android/org/xbill/DNS/TSIG;->HMAC_SHA512:Lcom/samsung/android/org/xbill/DNS/Name;

    invoke-virtual {v0, v1}, Lcom/samsung/android/org/xbill/DNS/Name;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 77
    const-string v0, "sha-512"

    iput-object v0, p0, Lcom/samsung/android/org/xbill/DNS/TSIG;->digest:Ljava/lang/String;

    .line 78
    iput v3, p0, Lcom/samsung/android/org/xbill/DNS/TSIG;->digestBlockLength:I

    goto :goto_0

    .line 79
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/TSIG;->alg:Lcom/samsung/android/org/xbill/DNS/Name;

    sget-object v1, Lcom/samsung/android/org/xbill/DNS/TSIG;->HMAC_SHA384:Lcom/samsung/android/org/xbill/DNS/Name;

    invoke-virtual {v0, v1}, Lcom/samsung/android/org/xbill/DNS/Name;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 80
    const-string v0, "sha-384"

    iput-object v0, p0, Lcom/samsung/android/org/xbill/DNS/TSIG;->digest:Ljava/lang/String;

    .line 81
    iput v3, p0, Lcom/samsung/android/org/xbill/DNS/TSIG;->digestBlockLength:I

    goto :goto_0

    .line 83
    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid algorithm"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public apply(Lcom/samsung/android/org/xbill/DNS/Message;ILcom/samsung/android/org/xbill/DNS/TSIGRecord;)V
    .locals 3
    .param p1, "m"    # Lcom/samsung/android/org/xbill/DNS/Message;
    .param p2, "error"    # I
    .param p3, "old"    # Lcom/samsung/android/org/xbill/DNS/TSIGRecord;

    .prologue
    const/4 v2, 0x3

    .line 288
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/Message;->toWire()[B

    move-result-object v1

    invoke-virtual {p0, p1, v1, p2, p3}, Lcom/samsung/android/org/xbill/DNS/TSIG;->generate(Lcom/samsung/android/org/xbill/DNS/Message;[BILcom/samsung/android/org/xbill/DNS/TSIGRecord;)Lcom/samsung/android/org/xbill/DNS/TSIGRecord;

    move-result-object v0

    .line 289
    .local v0, "r":Lcom/samsung/android/org/xbill/DNS/Record;
    invoke-virtual {p1, v0, v2}, Lcom/samsung/android/org/xbill/DNS/Message;->addRecord(Lcom/samsung/android/org/xbill/DNS/Record;I)V

    .line 290
    iput v2, p1, Lcom/samsung/android/org/xbill/DNS/Message;->tsigState:I

    .line 291
    return-void
.end method

.method public apply(Lcom/samsung/android/org/xbill/DNS/Message;Lcom/samsung/android/org/xbill/DNS/TSIGRecord;)V
    .locals 1
    .param p1, "m"    # Lcom/samsung/android/org/xbill/DNS/Message;
    .param p2, "old"    # Lcom/samsung/android/org/xbill/DNS/TSIGRecord;

    .prologue
    .line 300
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lcom/samsung/android/org/xbill/DNS/TSIG;->apply(Lcom/samsung/android/org/xbill/DNS/Message;ILcom/samsung/android/org/xbill/DNS/TSIGRecord;)V

    .line 301
    return-void
.end method

.method public applyStream(Lcom/samsung/android/org/xbill/DNS/Message;Lcom/samsung/android/org/xbill/DNS/TSIGRecord;Z)V
    .locals 22
    .param p1, "m"    # Lcom/samsung/android/org/xbill/DNS/Message;
    .param p2, "old"    # Lcom/samsung/android/org/xbill/DNS/TSIGRecord;
    .param p3, "first"    # Z

    .prologue
    .line 310
    if-eqz p3, :cond_0

    .line 311
    invoke-virtual/range {p0 .. p2}, Lcom/samsung/android/org/xbill/DNS/TSIG;->apply(Lcom/samsung/android/org/xbill/DNS/Message;Lcom/samsung/android/org/xbill/DNS/TSIGRecord;)V

    .line 348
    :goto_0
    return-void

    .line 314
    :cond_0
    new-instance v9, Ljava/util/Date;

    invoke-direct {v9}, Ljava/util/Date;-><init>()V

    .line 316
    .local v9, "timeSigned":Ljava/util/Date;
    new-instance v2, Lcom/samsung/android/org/xbill/DNS/utils/HMAC;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/org/xbill/DNS/TSIG;->digest:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/android/org/xbill/DNS/TSIG;->digestBlockLength:I

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/org/xbill/DNS/TSIG;->key:[B

    invoke-direct {v2, v4, v5, v6}, Lcom/samsung/android/org/xbill/DNS/utils/HMAC;-><init>(Ljava/lang/String;I[B)V

    .line 318
    .local v2, "hmac":Lcom/samsung/android/org/xbill/DNS/utils/HMAC;
    const-string v4, "tsigfudge"

    invoke-static {v4}, Lcom/samsung/android/org/xbill/DNS/Options;->intValue(Ljava/lang/String;)I

    move-result v10

    .line 319
    .local v10, "fudge":I
    if-ltz v10, :cond_1

    const/16 v4, 0x7fff

    if-le v10, v4, :cond_2

    .line 320
    :cond_1
    const/16 v10, 0x12c

    .line 322
    :cond_2
    new-instance v15, Lcom/samsung/android/org/xbill/DNS/DNSOutput;

    invoke-direct {v15}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;-><init>()V

    .line 323
    .local v15, "out":Lcom/samsung/android/org/xbill/DNS/DNSOutput;
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/org/xbill/DNS/TSIGRecord;->getSignature()[B

    move-result-object v4

    array-length v4, v4

    invoke-virtual {v15, v4}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeU16(I)V

    .line 324
    invoke-virtual {v15}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->toByteArray()[B

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/samsung/android/org/xbill/DNS/utils/HMAC;->update([B)V

    .line 325
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/org/xbill/DNS/TSIGRecord;->getSignature()[B

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/samsung/android/org/xbill/DNS/utils/HMAC;->update([B)V

    .line 328
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/org/xbill/DNS/Message;->toWire()[B

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/samsung/android/org/xbill/DNS/utils/HMAC;->update([B)V

    .line 330
    new-instance v15, Lcom/samsung/android/org/xbill/DNS/DNSOutput;

    .end local v15    # "out":Lcom/samsung/android/org/xbill/DNS/DNSOutput;
    invoke-direct {v15}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;-><init>()V

    .line 331
    .restart local v15    # "out":Lcom/samsung/android/org/xbill/DNS/DNSOutput;
    invoke-virtual {v9}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long v16, v4, v6

    .line 332
    .local v16, "time":J
    const/16 v4, 0x20

    shr-long v4, v16, v4

    long-to-int v0, v4

    move/from16 v18, v0

    .line 333
    .local v18, "timeHigh":I
    const-wide v4, 0xffffffffL

    and-long v20, v16, v4

    .line 334
    .local v20, "timeLow":J
    move/from16 v0, v18

    invoke-virtual {v15, v0}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeU16(I)V

    .line 335
    move-wide/from16 v0, v20

    invoke-virtual {v15, v0, v1}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeU32(J)V

    .line 336
    invoke-virtual {v15, v10}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeU16(I)V

    .line 338
    invoke-virtual {v15}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->toByteArray()[B

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/samsung/android/org/xbill/DNS/utils/HMAC;->update([B)V

    .line 340
    invoke-virtual {v2}, Lcom/samsung/android/org/xbill/DNS/utils/HMAC;->sign()[B

    move-result-object v11

    .line 341
    .local v11, "signature":[B
    const/4 v14, 0x0

    .line 343
    .local v14, "other":[B
    new-instance v3, Lcom/samsung/android/org/xbill/DNS/TSIGRecord;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/org/xbill/DNS/TSIG;->name:Lcom/samsung/android/org/xbill/DNS/Name;

    const/16 v5, 0xff

    const-wide/16 v6, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/org/xbill/DNS/TSIG;->alg:Lcom/samsung/android/org/xbill/DNS/Name;

    .line 344
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/org/xbill/DNS/Message;->getHeader()Lcom/samsung/android/org/xbill/DNS/Header;

    move-result-object v12

    invoke-virtual {v12}, Lcom/samsung/android/org/xbill/DNS/Header;->getID()I

    move-result v12

    .line 345
    const/4 v13, 0x0

    .line 343
    invoke-direct/range {v3 .. v14}, Lcom/samsung/android/org/xbill/DNS/TSIGRecord;-><init>(Lcom/samsung/android/org/xbill/DNS/Name;IJLcom/samsung/android/org/xbill/DNS/Name;Ljava/util/Date;I[BII[B)V

    .line 346
    .local v3, "r":Lcom/samsung/android/org/xbill/DNS/Record;
    const/4 v4, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, Lcom/samsung/android/org/xbill/DNS/Message;->addRecord(Lcom/samsung/android/org/xbill/DNS/Record;I)V

    .line 347
    const/4 v4, 0x3

    move-object/from16 v0, p1

    iput v4, v0, Lcom/samsung/android/org/xbill/DNS/Message;->tsigState:I

    goto/16 :goto_0
.end method

.method public generate(Lcom/samsung/android/org/xbill/DNS/Message;[BILcom/samsung/android/org/xbill/DNS/TSIGRecord;)Lcom/samsung/android/org/xbill/DNS/TSIGRecord;
    .locals 22
    .param p1, "m"    # Lcom/samsung/android/org/xbill/DNS/Message;
    .param p2, "b"    # [B
    .param p3, "error"    # I
    .param p4, "old"    # Lcom/samsung/android/org/xbill/DNS/TSIGRecord;

    .prologue
    .line 214
    const/16 v3, 0x12

    move/from16 v0, p3

    if-eq v0, v3, :cond_8

    .line 215
    new-instance v9, Ljava/util/Date;

    invoke-direct {v9}, Ljava/util/Date;-><init>()V

    .line 219
    .local v9, "timeSigned":Ljava/util/Date;
    :goto_0
    const/4 v2, 0x0

    .line 220
    .local v2, "hmac":Lcom/samsung/android/org/xbill/DNS/utils/HMAC;
    if-eqz p3, :cond_0

    const/16 v3, 0x12

    move/from16 v0, p3

    if-ne v0, v3, :cond_1

    .line 221
    :cond_0
    new-instance v2, Lcom/samsung/android/org/xbill/DNS/utils/HMAC;

    .end local v2    # "hmac":Lcom/samsung/android/org/xbill/DNS/utils/HMAC;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/org/xbill/DNS/TSIG;->digest:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/org/xbill/DNS/TSIG;->digestBlockLength:I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/org/xbill/DNS/TSIG;->key:[B

    invoke-direct {v2, v3, v4, v5}, Lcom/samsung/android/org/xbill/DNS/utils/HMAC;-><init>(Ljava/lang/String;I[B)V

    .line 223
    .restart local v2    # "hmac":Lcom/samsung/android/org/xbill/DNS/utils/HMAC;
    :cond_1
    const-string v3, "tsigfudge"

    invoke-static {v3}, Lcom/samsung/android/org/xbill/DNS/Options;->intValue(Ljava/lang/String;)I

    move-result v10

    .line 224
    .local v10, "fudge":I
    if-ltz v10, :cond_2

    const/16 v3, 0x7fff

    if-le v10, v3, :cond_3

    .line 225
    :cond_2
    const/16 v10, 0x12c

    .line 227
    :cond_3
    if-eqz p4, :cond_4

    .line 228
    new-instance v15, Lcom/samsung/android/org/xbill/DNS/DNSOutput;

    invoke-direct {v15}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;-><init>()V

    .line 229
    .local v15, "out":Lcom/samsung/android/org/xbill/DNS/DNSOutput;
    invoke-virtual/range {p4 .. p4}, Lcom/samsung/android/org/xbill/DNS/TSIGRecord;->getSignature()[B

    move-result-object v3

    array-length v3, v3

    invoke-virtual {v15, v3}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeU16(I)V

    .line 230
    if-eqz v2, :cond_4

    .line 231
    invoke-virtual {v15}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->toByteArray()[B

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/org/xbill/DNS/utils/HMAC;->update([B)V

    .line 232
    invoke-virtual/range {p4 .. p4}, Lcom/samsung/android/org/xbill/DNS/TSIGRecord;->getSignature()[B

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/org/xbill/DNS/utils/HMAC;->update([B)V

    .line 237
    .end local v15    # "out":Lcom/samsung/android/org/xbill/DNS/DNSOutput;
    :cond_4
    if-eqz v2, :cond_5

    .line 238
    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Lcom/samsung/android/org/xbill/DNS/utils/HMAC;->update([B)V

    .line 240
    :cond_5
    new-instance v15, Lcom/samsung/android/org/xbill/DNS/DNSOutput;

    invoke-direct {v15}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;-><init>()V

    .line 241
    .restart local v15    # "out":Lcom/samsung/android/org/xbill/DNS/DNSOutput;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/org/xbill/DNS/TSIG;->name:Lcom/samsung/android/org/xbill/DNS/Name;

    invoke-virtual {v3, v15}, Lcom/samsung/android/org/xbill/DNS/Name;->toWireCanonical(Lcom/samsung/android/org/xbill/DNS/DNSOutput;)V

    .line 242
    const/16 v3, 0xff

    invoke-virtual {v15, v3}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeU16(I)V

    .line 243
    const-wide/16 v4, 0x0

    invoke-virtual {v15, v4, v5}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeU32(J)V

    .line 244
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/org/xbill/DNS/TSIG;->alg:Lcom/samsung/android/org/xbill/DNS/Name;

    invoke-virtual {v3, v15}, Lcom/samsung/android/org/xbill/DNS/Name;->toWireCanonical(Lcom/samsung/android/org/xbill/DNS/DNSOutput;)V

    .line 245
    invoke-virtual {v9}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long v16, v4, v6

    .line 246
    .local v16, "time":J
    const/16 v3, 0x20

    shr-long v4, v16, v3

    long-to-int v0, v4

    move/from16 v18, v0

    .line 247
    .local v18, "timeHigh":I
    const-wide v4, 0xffffffffL

    and-long v20, v16, v4

    .line 248
    .local v20, "timeLow":J
    move/from16 v0, v18

    invoke-virtual {v15, v0}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeU16(I)V

    .line 249
    move-wide/from16 v0, v20

    invoke-virtual {v15, v0, v1}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeU32(J)V

    .line 250
    invoke-virtual {v15, v10}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeU16(I)V

    .line 252
    move/from16 v0, p3

    invoke-virtual {v15, v0}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeU16(I)V

    .line 253
    const/4 v3, 0x0

    invoke-virtual {v15, v3}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeU16(I)V

    .line 255
    if-eqz v2, :cond_6

    .line 256
    invoke-virtual {v15}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->toByteArray()[B

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/org/xbill/DNS/utils/HMAC;->update([B)V

    .line 259
    :cond_6
    if-eqz v2, :cond_9

    .line 260
    invoke-virtual {v2}, Lcom/samsung/android/org/xbill/DNS/utils/HMAC;->sign()[B

    move-result-object v11

    .line 264
    .local v11, "signature":[B
    :goto_1
    const/4 v14, 0x0

    .line 265
    .local v14, "other":[B
    const/16 v3, 0x12

    move/from16 v0, p3

    if-ne v0, v3, :cond_7

    .line 266
    new-instance v15, Lcom/samsung/android/org/xbill/DNS/DNSOutput;

    .end local v15    # "out":Lcom/samsung/android/org/xbill/DNS/DNSOutput;
    invoke-direct {v15}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;-><init>()V

    .line 267
    .restart local v15    # "out":Lcom/samsung/android/org/xbill/DNS/DNSOutput;
    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long v16, v4, v6

    .line 268
    const/16 v3, 0x20

    shr-long v4, v16, v3

    long-to-int v0, v4

    move/from16 v18, v0

    .line 269
    const-wide v4, 0xffffffffL

    and-long v20, v16, v4

    .line 270
    move/from16 v0, v18

    invoke-virtual {v15, v0}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeU16(I)V

    .line 271
    move-wide/from16 v0, v20

    invoke-virtual {v15, v0, v1}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeU32(J)V

    .line 272
    invoke-virtual {v15}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->toByteArray()[B

    move-result-object v14

    .line 275
    :cond_7
    new-instance v3, Lcom/samsung/android/org/xbill/DNS/TSIGRecord;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/org/xbill/DNS/TSIG;->name:Lcom/samsung/android/org/xbill/DNS/Name;

    const/16 v5, 0xff

    const-wide/16 v6, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/org/xbill/DNS/TSIG;->alg:Lcom/samsung/android/org/xbill/DNS/Name;

    .line 276
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/org/xbill/DNS/Message;->getHeader()Lcom/samsung/android/org/xbill/DNS/Header;

    move-result-object v12

    invoke-virtual {v12}, Lcom/samsung/android/org/xbill/DNS/Header;->getID()I

    move-result v12

    move/from16 v13, p3

    .line 275
    invoke-direct/range {v3 .. v14}, Lcom/samsung/android/org/xbill/DNS/TSIGRecord;-><init>(Lcom/samsung/android/org/xbill/DNS/Name;IJLcom/samsung/android/org/xbill/DNS/Name;Ljava/util/Date;I[BII[B)V

    return-object v3

    .line 217
    .end local v2    # "hmac":Lcom/samsung/android/org/xbill/DNS/utils/HMAC;
    .end local v9    # "timeSigned":Ljava/util/Date;
    .end local v10    # "fudge":I
    .end local v11    # "signature":[B
    .end local v14    # "other":[B
    .end local v15    # "out":Lcom/samsung/android/org/xbill/DNS/DNSOutput;
    .end local v16    # "time":J
    .end local v18    # "timeHigh":I
    .end local v20    # "timeLow":J
    :cond_8
    invoke-virtual/range {p4 .. p4}, Lcom/samsung/android/org/xbill/DNS/TSIGRecord;->getTimeSigned()Ljava/util/Date;

    move-result-object v9

    .restart local v9    # "timeSigned":Ljava/util/Date;
    goto/16 :goto_0

    .line 262
    .restart local v2    # "hmac":Lcom/samsung/android/org/xbill/DNS/utils/HMAC;
    .restart local v10    # "fudge":I
    .restart local v15    # "out":Lcom/samsung/android/org/xbill/DNS/DNSOutput;
    .restart local v16    # "time":J
    .restart local v18    # "timeHigh":I
    .restart local v20    # "timeLow":J
    :cond_9
    const/4 v3, 0x0

    new-array v11, v3, [B

    .restart local v11    # "signature":[B
    goto :goto_1
.end method

.method public recordLength()I
    .locals 2

    .prologue
    .line 469
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/TSIG;->name:Lcom/samsung/android/org/xbill/DNS/Name;

    invoke-virtual {v0}, Lcom/samsung/android/org/xbill/DNS/Name;->length()S

    move-result v0

    add-int/lit8 v0, v0, 0xa

    .line 470
    iget-object v1, p0, Lcom/samsung/android/org/xbill/DNS/TSIG;->alg:Lcom/samsung/android/org/xbill/DNS/Name;

    invoke-virtual {v1}, Lcom/samsung/android/org/xbill/DNS/Name;->length()S

    move-result v1

    .line 469
    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x8

    add-int/lit8 v0, v0, 0x12

    add-int/lit8 v0, v0, 0x4

    add-int/lit8 v0, v0, 0x8

    return v0
.end method

.method public verify(Lcom/samsung/android/org/xbill/DNS/Message;[BILcom/samsung/android/org/xbill/DNS/TSIGRecord;)B
    .locals 28
    .param p1, "m"    # Lcom/samsung/android/org/xbill/DNS/Message;
    .param p2, "b"    # [B
    .param p3, "length"    # I
    .param p4, "old"    # Lcom/samsung/android/org/xbill/DNS/TSIGRecord;

    .prologue
    .line 366
    const/16 v23, 0x4

    move/from16 v0, v23

    move-object/from16 v1, p1

    iput v0, v1, Lcom/samsung/android/org/xbill/DNS/Message;->tsigState:I

    .line 367
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/org/xbill/DNS/Message;->getTSIG()Lcom/samsung/android/org/xbill/DNS/TSIGRecord;

    move-result-object v22

    .line 368
    .local v22, "tsig":Lcom/samsung/android/org/xbill/DNS/TSIGRecord;
    new-instance v8, Lcom/samsung/android/org/xbill/DNS/utils/HMAC;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/org/xbill/DNS/TSIG;->digest:Ljava/lang/String;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/org/xbill/DNS/TSIG;->digestBlockLength:I

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/org/xbill/DNS/TSIG;->key:[B

    move-object/from16 v25, v0

    move-object/from16 v0, v23

    move/from16 v1, v24

    move-object/from16 v2, v25

    invoke-direct {v8, v0, v1, v2}, Lcom/samsung/android/org/xbill/DNS/utils/HMAC;-><init>(Ljava/lang/String;I[B)V

    .line 369
    .local v8, "hmac":Lcom/samsung/android/org/xbill/DNS/utils/HMAC;
    if-nez v22, :cond_0

    .line 370
    const/16 v23, 0x1

    .line 442
    :goto_0
    return v23

    .line 372
    :cond_0
    invoke-virtual/range {v22 .. v22}, Lcom/samsung/android/org/xbill/DNS/TSIGRecord;->getName()Lcom/samsung/android/org/xbill/DNS/Name;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/org/xbill/DNS/TSIG;->name:Lcom/samsung/android/org/xbill/DNS/Name;

    move-object/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Lcom/samsung/android/org/xbill/DNS/Name;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_1

    invoke-virtual/range {v22 .. v22}, Lcom/samsung/android/org/xbill/DNS/TSIGRecord;->getAlgorithm()Lcom/samsung/android/org/xbill/DNS/Name;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/org/xbill/DNS/TSIG;->alg:Lcom/samsung/android/org/xbill/DNS/Name;

    move-object/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Lcom/samsung/android/org/xbill/DNS/Name;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-nez v23, :cond_3

    .line 373
    :cond_1
    const-string v23, "verbose"

    invoke-static/range {v23 .. v23}, Lcom/samsung/android/org/xbill/DNS/Options;->check(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_2

    .line 374
    sget-object v23, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string v24, "BADKEY failure"

    invoke-virtual/range {v23 .. v24}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 375
    :cond_2
    const/16 v23, 0x11

    goto :goto_0

    .line 377
    :cond_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    .line 378
    .local v12, "now":J
    invoke-virtual/range {v22 .. v22}, Lcom/samsung/android/org/xbill/DNS/TSIGRecord;->getTimeSigned()Ljava/util/Date;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/util/Date;->getTime()J

    move-result-wide v16

    .line 379
    .local v16, "then":J
    invoke-virtual/range {v22 .. v22}, Lcom/samsung/android/org/xbill/DNS/TSIGRecord;->getFudge()I

    move-result v23

    move/from16 v0, v23

    int-to-long v6, v0

    .line 380
    .local v6, "fudge":J
    sub-long v24, v12, v16

    invoke-static/range {v24 .. v25}, Ljava/lang/Math;->abs(J)J

    move-result-wide v24

    const-wide/16 v26, 0x3e8

    mul-long v26, v26, v6

    cmp-long v23, v24, v26

    if-lez v23, :cond_5

    .line 381
    const-string v23, "verbose"

    invoke-static/range {v23 .. v23}, Lcom/samsung/android/org/xbill/DNS/Options;->check(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_4

    .line 382
    sget-object v23, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string v24, "BADTIME failure"

    invoke-virtual/range {v23 .. v24}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 383
    :cond_4
    const/16 v23, 0x12

    goto :goto_0

    .line 386
    :cond_5
    if-eqz p4, :cond_6

    invoke-virtual/range {v22 .. v22}, Lcom/samsung/android/org/xbill/DNS/TSIGRecord;->getError()I

    move-result v23

    const/16 v24, 0x11

    move/from16 v0, v23

    move/from16 v1, v24

    if-eq v0, v1, :cond_6

    .line 387
    invoke-virtual/range {v22 .. v22}, Lcom/samsung/android/org/xbill/DNS/TSIGRecord;->getError()I

    move-result v23

    const/16 v24, 0x10

    move/from16 v0, v23

    move/from16 v1, v24

    if-eq v0, v1, :cond_6

    .line 389
    new-instance v11, Lcom/samsung/android/org/xbill/DNS/DNSOutput;

    invoke-direct {v11}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;-><init>()V

    .line 390
    .local v11, "out":Lcom/samsung/android/org/xbill/DNS/DNSOutput;
    invoke-virtual/range {p4 .. p4}, Lcom/samsung/android/org/xbill/DNS/TSIGRecord;->getSignature()[B

    move-result-object v23

    move-object/from16 v0, v23

    array-length v0, v0

    move/from16 v23, v0

    move/from16 v0, v23

    invoke-virtual {v11, v0}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeU16(I)V

    .line 391
    invoke-virtual {v11}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->toByteArray()[B

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v8, v0}, Lcom/samsung/android/org/xbill/DNS/utils/HMAC;->update([B)V

    .line 392
    invoke-virtual/range {p4 .. p4}, Lcom/samsung/android/org/xbill/DNS/TSIGRecord;->getSignature()[B

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v8, v0}, Lcom/samsung/android/org/xbill/DNS/utils/HMAC;->update([B)V

    .line 394
    .end local v11    # "out":Lcom/samsung/android/org/xbill/DNS/DNSOutput;
    :cond_6
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/org/xbill/DNS/Message;->getHeader()Lcom/samsung/android/org/xbill/DNS/Header;

    move-result-object v23

    const/16 v24, 0x3

    invoke-virtual/range {v23 .. v24}, Lcom/samsung/android/org/xbill/DNS/Header;->decCount(I)V

    .line 395
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/org/xbill/DNS/Message;->getHeader()Lcom/samsung/android/org/xbill/DNS/Header;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/org/xbill/DNS/Header;->toWire()[B

    move-result-object v5

    .line 396
    .local v5, "header":[B
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/org/xbill/DNS/Message;->getHeader()Lcom/samsung/android/org/xbill/DNS/Header;

    move-result-object v23

    const/16 v24, 0x3

    invoke-virtual/range {v23 .. v24}, Lcom/samsung/android/org/xbill/DNS/Header;->incCount(I)V

    .line 397
    invoke-virtual {v8, v5}, Lcom/samsung/android/org/xbill/DNS/utils/HMAC;->update([B)V

    .line 399
    move-object/from16 v0, p1

    iget v0, v0, Lcom/samsung/android/org/xbill/DNS/Message;->tsigstart:I

    move/from16 v23, v0

    array-length v0, v5

    move/from16 v24, v0

    sub-int v9, v23, v24

    .line 400
    .local v9, "len":I
    array-length v0, v5

    move/from16 v23, v0

    move-object/from16 v0, p2

    move/from16 v1, v23

    invoke-virtual {v8, v0, v1, v9}, Lcom/samsung/android/org/xbill/DNS/utils/HMAC;->update([BII)V

    .line 402
    new-instance v11, Lcom/samsung/android/org/xbill/DNS/DNSOutput;

    invoke-direct {v11}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;-><init>()V

    .line 403
    .restart local v11    # "out":Lcom/samsung/android/org/xbill/DNS/DNSOutput;
    invoke-virtual/range {v22 .. v22}, Lcom/samsung/android/org/xbill/DNS/TSIGRecord;->getName()Lcom/samsung/android/org/xbill/DNS/Name;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v11}, Lcom/samsung/android/org/xbill/DNS/Name;->toWireCanonical(Lcom/samsung/android/org/xbill/DNS/DNSOutput;)V

    .line 404
    move-object/from16 v0, v22

    iget v0, v0, Lcom/samsung/android/org/xbill/DNS/TSIGRecord;->dclass:I

    move/from16 v23, v0

    move/from16 v0, v23

    invoke-virtual {v11, v0}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeU16(I)V

    .line 405
    move-object/from16 v0, v22

    iget-wide v0, v0, Lcom/samsung/android/org/xbill/DNS/TSIGRecord;->ttl:J

    move-wide/from16 v24, v0

    move-wide/from16 v0, v24

    invoke-virtual {v11, v0, v1}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeU32(J)V

    .line 406
    invoke-virtual/range {v22 .. v22}, Lcom/samsung/android/org/xbill/DNS/TSIGRecord;->getAlgorithm()Lcom/samsung/android/org/xbill/DNS/Name;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v11}, Lcom/samsung/android/org/xbill/DNS/Name;->toWireCanonical(Lcom/samsung/android/org/xbill/DNS/DNSOutput;)V

    .line 407
    invoke-virtual/range {v22 .. v22}, Lcom/samsung/android/org/xbill/DNS/TSIGRecord;->getTimeSigned()Ljava/util/Date;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/util/Date;->getTime()J

    move-result-wide v24

    const-wide/16 v26, 0x3e8

    div-long v18, v24, v26

    .line 408
    .local v18, "time":J
    const/16 v23, 0x20

    shr-long v24, v18, v23

    move-wide/from16 v0, v24

    long-to-int v15, v0

    .line 409
    .local v15, "timeHigh":I
    const-wide v24, 0xffffffffL

    and-long v20, v18, v24

    .line 410
    .local v20, "timeLow":J
    invoke-virtual {v11, v15}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeU16(I)V

    .line 411
    move-wide/from16 v0, v20

    invoke-virtual {v11, v0, v1}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeU32(J)V

    .line 412
    invoke-virtual/range {v22 .. v22}, Lcom/samsung/android/org/xbill/DNS/TSIGRecord;->getFudge()I

    move-result v23

    move/from16 v0, v23

    invoke-virtual {v11, v0}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeU16(I)V

    .line 413
    invoke-virtual/range {v22 .. v22}, Lcom/samsung/android/org/xbill/DNS/TSIGRecord;->getError()I

    move-result v23

    move/from16 v0, v23

    invoke-virtual {v11, v0}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeU16(I)V

    .line 414
    invoke-virtual/range {v22 .. v22}, Lcom/samsung/android/org/xbill/DNS/TSIGRecord;->getOther()[B

    move-result-object v23

    if-eqz v23, :cond_8

    .line 415
    invoke-virtual/range {v22 .. v22}, Lcom/samsung/android/org/xbill/DNS/TSIGRecord;->getOther()[B

    move-result-object v23

    move-object/from16 v0, v23

    array-length v0, v0

    move/from16 v23, v0

    move/from16 v0, v23

    invoke-virtual {v11, v0}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeU16(I)V

    .line 416
    invoke-virtual/range {v22 .. v22}, Lcom/samsung/android/org/xbill/DNS/TSIGRecord;->getOther()[B

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v11, v0}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeByteArray([B)V

    .line 421
    :goto_1
    invoke-virtual {v11}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->toByteArray()[B

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v8, v0}, Lcom/samsung/android/org/xbill/DNS/utils/HMAC;->update([B)V

    .line 423
    invoke-virtual/range {v22 .. v22}, Lcom/samsung/android/org/xbill/DNS/TSIGRecord;->getSignature()[B

    move-result-object v14

    .line 424
    .local v14, "signature":[B
    invoke-virtual {v8}, Lcom/samsung/android/org/xbill/DNS/utils/HMAC;->digestLength()I

    move-result v4

    .line 425
    .local v4, "digestLength":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/org/xbill/DNS/TSIG;->digest:Ljava/lang/String;

    move-object/from16 v23, v0

    const-string v24, "md5"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_9

    const/16 v10, 0xa

    .line 427
    .local v10, "minDigestLength":I
    :goto_2
    array-length v0, v14

    move/from16 v23, v0

    move/from16 v0, v23

    if-le v0, v4, :cond_a

    .line 428
    const-string v23, "verbose"

    invoke-static/range {v23 .. v23}, Lcom/samsung/android/org/xbill/DNS/Options;->check(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_7

    .line 429
    sget-object v23, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string v24, "BADSIG: signature too long"

    invoke-virtual/range {v23 .. v24}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 430
    :cond_7
    const/16 v23, 0x10

    goto/16 :goto_0

    .line 418
    .end local v4    # "digestLength":I
    .end local v10    # "minDigestLength":I
    .end local v14    # "signature":[B
    :cond_8
    const/16 v23, 0x0

    move/from16 v0, v23

    invoke-virtual {v11, v0}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeU16(I)V

    goto :goto_1

    .line 425
    .restart local v4    # "digestLength":I
    .restart local v14    # "signature":[B
    :cond_9
    div-int/lit8 v10, v4, 0x2

    goto :goto_2

    .line 431
    .restart local v10    # "minDigestLength":I
    :cond_a
    array-length v0, v14

    move/from16 v23, v0

    move/from16 v0, v23

    if-ge v0, v10, :cond_c

    .line 432
    const-string v23, "verbose"

    invoke-static/range {v23 .. v23}, Lcom/samsung/android/org/xbill/DNS/Options;->check(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_b

    .line 433
    sget-object v23, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string v24, "BADSIG: signature too short"

    invoke-virtual/range {v23 .. v24}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 434
    :cond_b
    const/16 v23, 0x10

    goto/16 :goto_0

    .line 435
    :cond_c
    const/16 v23, 0x1

    move/from16 v0, v23

    invoke-virtual {v8, v14, v0}, Lcom/samsung/android/org/xbill/DNS/utils/HMAC;->verify([BZ)Z

    move-result v23

    if-nez v23, :cond_e

    .line 436
    const-string v23, "verbose"

    invoke-static/range {v23 .. v23}, Lcom/samsung/android/org/xbill/DNS/Options;->check(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_d

    .line 437
    sget-object v23, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string v24, "BADSIG: signature verification"

    invoke-virtual/range {v23 .. v24}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 438
    :cond_d
    const/16 v23, 0x10

    goto/16 :goto_0

    .line 441
    :cond_e
    const/16 v23, 0x1

    move/from16 v0, v23

    move-object/from16 v1, p1

    iput v0, v1, Lcom/samsung/android/org/xbill/DNS/Message;->tsigState:I

    .line 442
    const/16 v23, 0x0

    goto/16 :goto_0
.end method

.method public verify(Lcom/samsung/android/org/xbill/DNS/Message;[BLcom/samsung/android/org/xbill/DNS/TSIGRecord;)I
    .locals 1
    .param p1, "m"    # Lcom/samsung/android/org/xbill/DNS/Message;
    .param p2, "b"    # [B
    .param p3, "old"    # Lcom/samsung/android/org/xbill/DNS/TSIGRecord;

    .prologue
    .line 460
    array-length v0, p2

    invoke-virtual {p0, p1, p2, v0, p3}, Lcom/samsung/android/org/xbill/DNS/TSIG;->verify(Lcom/samsung/android/org/xbill/DNS/Message;[BILcom/samsung/android/org/xbill/DNS/TSIGRecord;)B

    move-result v0

    return v0
.end method
