.class public Lcom/samsung/android/org/xbill/DNS/MINFORecord;
.super Lcom/samsung/android/org/xbill/DNS/Record;
.source "MINFORecord.java"


# static fields
.field private static final serialVersionUID:J = -0x36fc5fdc632a4b04L


# instance fields
.field private errorAddress:Lcom/samsung/android/org/xbill/DNS/Name;

.field private responsibleAddress:Lcom/samsung/android/org/xbill/DNS/Name;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/samsung/android/org/xbill/DNS/Record;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/org/xbill/DNS/Name;IJLcom/samsung/android/org/xbill/DNS/Name;Lcom/samsung/android/org/xbill/DNS/Name;)V
    .locals 7
    .param p1, "name"    # Lcom/samsung/android/org/xbill/DNS/Name;
    .param p2, "dclass"    # I
    .param p3, "ttl"    # J
    .param p5, "responsibleAddress"    # Lcom/samsung/android/org/xbill/DNS/Name;
    .param p6, "errorAddress"    # Lcom/samsung/android/org/xbill/DNS/Name;

    .prologue
    .line 40
    const/16 v2, 0xe

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/org/xbill/DNS/Record;-><init>(Lcom/samsung/android/org/xbill/DNS/Name;IIJ)V

    .line 42
    const-string v0, "responsibleAddress"

    invoke-static {v0, p5}, Lcom/samsung/android/org/xbill/DNS/MINFORecord;->checkName(Ljava/lang/String;Lcom/samsung/android/org/xbill/DNS/Name;)Lcom/samsung/android/org/xbill/DNS/Name;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/org/xbill/DNS/MINFORecord;->responsibleAddress:Lcom/samsung/android/org/xbill/DNS/Name;

    .line 44
    const-string v0, "errorAddress"

    invoke-static {v0, p6}, Lcom/samsung/android/org/xbill/DNS/MINFORecord;->checkName(Ljava/lang/String;Lcom/samsung/android/org/xbill/DNS/Name;)Lcom/samsung/android/org/xbill/DNS/Name;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/org/xbill/DNS/MINFORecord;->errorAddress:Lcom/samsung/android/org/xbill/DNS/Name;

    .line 45
    return-void
.end method


# virtual methods
.method public getErrorAddress()Lcom/samsung/android/org/xbill/DNS/Name;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/MINFORecord;->errorAddress:Lcom/samsung/android/org/xbill/DNS/Name;

    return-object v0
.end method

.method getObject()Lcom/samsung/android/org/xbill/DNS/Record;
    .locals 1

    .prologue
    .line 26
    new-instance v0, Lcom/samsung/android/org/xbill/DNS/MINFORecord;

    invoke-direct {v0}, Lcom/samsung/android/org/xbill/DNS/MINFORecord;-><init>()V

    return-object v0
.end method

.method public getResponsibleAddress()Lcom/samsung/android/org/xbill/DNS/Name;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/MINFORecord;->responsibleAddress:Lcom/samsung/android/org/xbill/DNS/Name;

    return-object v0
.end method

.method rdataFromString(Lcom/samsung/android/org/xbill/DNS/Tokenizer;Lcom/samsung/android/org/xbill/DNS/Name;)V
    .locals 1
    .param p1, "st"    # Lcom/samsung/android/org/xbill/DNS/Tokenizer;
    .param p2, "origin"    # Lcom/samsung/android/org/xbill/DNS/Name;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 55
    invoke-virtual {p1, p2}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->getName(Lcom/samsung/android/org/xbill/DNS/Name;)Lcom/samsung/android/org/xbill/DNS/Name;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/org/xbill/DNS/MINFORecord;->responsibleAddress:Lcom/samsung/android/org/xbill/DNS/Name;

    .line 56
    invoke-virtual {p1, p2}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->getName(Lcom/samsung/android/org/xbill/DNS/Name;)Lcom/samsung/android/org/xbill/DNS/Name;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/org/xbill/DNS/MINFORecord;->errorAddress:Lcom/samsung/android/org/xbill/DNS/Name;

    .line 57
    return-void
.end method

.method rrFromWire(Lcom/samsung/android/org/xbill/DNS/DNSInput;)V
    .locals 1
    .param p1, "in"    # Lcom/samsung/android/org/xbill/DNS/DNSInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 49
    new-instance v0, Lcom/samsung/android/org/xbill/DNS/Name;

    invoke-direct {v0, p1}, Lcom/samsung/android/org/xbill/DNS/Name;-><init>(Lcom/samsung/android/org/xbill/DNS/DNSInput;)V

    iput-object v0, p0, Lcom/samsung/android/org/xbill/DNS/MINFORecord;->responsibleAddress:Lcom/samsung/android/org/xbill/DNS/Name;

    .line 50
    new-instance v0, Lcom/samsung/android/org/xbill/DNS/Name;

    invoke-direct {v0, p1}, Lcom/samsung/android/org/xbill/DNS/Name;-><init>(Lcom/samsung/android/org/xbill/DNS/DNSInput;)V

    iput-object v0, p0, Lcom/samsung/android/org/xbill/DNS/MINFORecord;->errorAddress:Lcom/samsung/android/org/xbill/DNS/Name;

    .line 51
    return-void
.end method

.method rrToString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 62
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 63
    .local v0, "sb":Ljava/lang/StringBuffer;
    iget-object v1, p0, Lcom/samsung/android/org/xbill/DNS/MINFORecord;->responsibleAddress:Lcom/samsung/android/org/xbill/DNS/Name;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    .line 64
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 65
    iget-object v1, p0, Lcom/samsung/android/org/xbill/DNS/MINFORecord;->errorAddress:Lcom/samsung/android/org/xbill/DNS/Name;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    .line 66
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method rrToWire(Lcom/samsung/android/org/xbill/DNS/DNSOutput;Lcom/samsung/android/org/xbill/DNS/Compression;Z)V
    .locals 2
    .param p1, "out"    # Lcom/samsung/android/org/xbill/DNS/DNSOutput;
    .param p2, "c"    # Lcom/samsung/android/org/xbill/DNS/Compression;
    .param p3, "canonical"    # Z

    .prologue
    const/4 v1, 0x0

    .line 86
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/MINFORecord;->responsibleAddress:Lcom/samsung/android/org/xbill/DNS/Name;

    invoke-virtual {v0, p1, v1, p3}, Lcom/samsung/android/org/xbill/DNS/Name;->toWire(Lcom/samsung/android/org/xbill/DNS/DNSOutput;Lcom/samsung/android/org/xbill/DNS/Compression;Z)V

    .line 87
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/MINFORecord;->errorAddress:Lcom/samsung/android/org/xbill/DNS/Name;

    invoke-virtual {v0, p1, v1, p3}, Lcom/samsung/android/org/xbill/DNS/Name;->toWire(Lcom/samsung/android/org/xbill/DNS/DNSOutput;Lcom/samsung/android/org/xbill/DNS/Compression;Z)V

    .line 88
    return-void
.end method
