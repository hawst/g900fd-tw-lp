.class abstract Lcom/samsung/android/org/xbill/DNS/SingleNameBase;
.super Lcom/samsung/android/org/xbill/DNS/Record;
.source "SingleNameBase.java"


# static fields
.field private static final serialVersionUID:J = -0x10e97ee72325L


# instance fields
.field protected singleName:Lcom/samsung/android/org/xbill/DNS/Name;


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/samsung/android/org/xbill/DNS/Record;-><init>()V

    return-void
.end method

.method protected constructor <init>(Lcom/samsung/android/org/xbill/DNS/Name;IIJ)V
    .locals 0
    .param p1, "name"    # Lcom/samsung/android/org/xbill/DNS/Name;
    .param p2, "type"    # I
    .param p3, "dclass"    # I
    .param p4, "ttl"    # J

    .prologue
    .line 25
    invoke-direct/range {p0 .. p5}, Lcom/samsung/android/org/xbill/DNS/Record;-><init>(Lcom/samsung/android/org/xbill/DNS/Name;IIJ)V

    .line 26
    return-void
.end method

.method protected constructor <init>(Lcom/samsung/android/org/xbill/DNS/Name;IIJLcom/samsung/android/org/xbill/DNS/Name;Ljava/lang/String;)V
    .locals 2
    .param p1, "name"    # Lcom/samsung/android/org/xbill/DNS/Name;
    .param p2, "type"    # I
    .param p3, "dclass"    # I
    .param p4, "ttl"    # J
    .param p6, "singleName"    # Lcom/samsung/android/org/xbill/DNS/Name;
    .param p7, "description"    # Ljava/lang/String;

    .prologue
    .line 32
    invoke-direct/range {p0 .. p5}, Lcom/samsung/android/org/xbill/DNS/Record;-><init>(Lcom/samsung/android/org/xbill/DNS/Name;IIJ)V

    .line 33
    invoke-static {p7, p6}, Lcom/samsung/android/org/xbill/DNS/SingleNameBase;->checkName(Ljava/lang/String;Lcom/samsung/android/org/xbill/DNS/Name;)Lcom/samsung/android/org/xbill/DNS/Name;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/org/xbill/DNS/SingleNameBase;->singleName:Lcom/samsung/android/org/xbill/DNS/Name;

    .line 34
    return-void
.end method


# virtual methods
.method protected getSingleName()Lcom/samsung/android/org/xbill/DNS/Name;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/SingleNameBase;->singleName:Lcom/samsung/android/org/xbill/DNS/Name;

    return-object v0
.end method

.method rdataFromString(Lcom/samsung/android/org/xbill/DNS/Tokenizer;Lcom/samsung/android/org/xbill/DNS/Name;)V
    .locals 1
    .param p1, "st"    # Lcom/samsung/android/org/xbill/DNS/Tokenizer;
    .param p2, "origin"    # Lcom/samsung/android/org/xbill/DNS/Name;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 43
    invoke-virtual {p1, p2}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->getName(Lcom/samsung/android/org/xbill/DNS/Name;)Lcom/samsung/android/org/xbill/DNS/Name;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/org/xbill/DNS/SingleNameBase;->singleName:Lcom/samsung/android/org/xbill/DNS/Name;

    .line 44
    return-void
.end method

.method rrFromWire(Lcom/samsung/android/org/xbill/DNS/DNSInput;)V
    .locals 1
    .param p1, "in"    # Lcom/samsung/android/org/xbill/DNS/DNSInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 38
    new-instance v0, Lcom/samsung/android/org/xbill/DNS/Name;

    invoke-direct {v0, p1}, Lcom/samsung/android/org/xbill/DNS/Name;-><init>(Lcom/samsung/android/org/xbill/DNS/DNSInput;)V

    iput-object v0, p0, Lcom/samsung/android/org/xbill/DNS/SingleNameBase;->singleName:Lcom/samsung/android/org/xbill/DNS/Name;

    .line 39
    return-void
.end method

.method rrToString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/SingleNameBase;->singleName:Lcom/samsung/android/org/xbill/DNS/Name;

    invoke-virtual {v0}, Lcom/samsung/android/org/xbill/DNS/Name;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method rrToWire(Lcom/samsung/android/org/xbill/DNS/DNSOutput;Lcom/samsung/android/org/xbill/DNS/Compression;Z)V
    .locals 2
    .param p1, "out"    # Lcom/samsung/android/org/xbill/DNS/DNSOutput;
    .param p2, "c"    # Lcom/samsung/android/org/xbill/DNS/Compression;
    .param p3, "canonical"    # Z

    .prologue
    .line 58
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/SingleNameBase;->singleName:Lcom/samsung/android/org/xbill/DNS/Name;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1, p3}, Lcom/samsung/android/org/xbill/DNS/Name;->toWire(Lcom/samsung/android/org/xbill/DNS/DNSOutput;Lcom/samsung/android/org/xbill/DNS/Compression;Z)V

    .line 59
    return-void
.end method
