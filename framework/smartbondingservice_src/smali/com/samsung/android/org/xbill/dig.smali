.class public Lcom/samsung/android/org/xbill/dig;
.super Ljava/lang/Object;
.source "dig.java"


# static fields
.field static dclass:I

.field static name:Lcom/samsung/android/org/xbill/DNS/Name;

.field static type:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 13
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/org/xbill/dig;->name:Lcom/samsung/android/org/xbill/DNS/Name;

    .line 14
    sput v1, Lcom/samsung/android/org/xbill/dig;->type:I

    sput v1, Lcom/samsung/android/org/xbill/dig;->dclass:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static doAXFR(Lcom/samsung/android/org/xbill/DNS/Message;)V
    .locals 6
    .param p0, "response"    # Lcom/samsung/android/org/xbill/DNS/Message;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 32
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "; java dig 0.0 <> "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v4, Lcom/samsung/android/org/xbill/dig;->name:Lcom/samsung/android/org/xbill/DNS/Name;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " axfr"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 33
    invoke-virtual {p0}, Lcom/samsung/android/org/xbill/DNS/Message;->isSigned()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 34
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, ";; TSIG "

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 35
    invoke-virtual {p0}, Lcom/samsung/android/org/xbill/DNS/Message;->isVerified()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 36
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "ok"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 41
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/org/xbill/DNS/Message;->getRcode()I

    move-result v2

    if-eqz v2, :cond_2

    .line 42
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v2, p0}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    .line 55
    :goto_1
    return-void

    .line 38
    :cond_1
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "failed"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0

    .line 46
    :cond_2
    invoke-virtual {p0, v5}, Lcom/samsung/android/org/xbill/DNS/Message;->getSectionArray(I)[Lcom/samsung/android/org/xbill/DNS/Record;

    move-result-object v1

    .line 47
    .local v1, "records":[Lcom/samsung/android/org/xbill/DNS/Record;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_2
    array-length v2, v1

    if-lt v0, v2, :cond_3

    .line 50
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, ";; done ("

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 51
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {p0}, Lcom/samsung/android/org/xbill/DNS/Message;->getHeader()Lcom/samsung/android/org/xbill/DNS/Header;

    move-result-object v3

    invoke-virtual {v3, v5}, Lcom/samsung/android/org/xbill/DNS/Header;->getCount(I)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->print(I)V

    .line 52
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, " records, "

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 53
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {p0}, Lcom/samsung/android/org/xbill/DNS/Message;->getHeader()Lcom/samsung/android/org/xbill/DNS/Header;

    move-result-object v3

    const/4 v4, 0x3

    invoke-virtual {v3, v4}, Lcom/samsung/android/org/xbill/DNS/Header;->getCount(I)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->print(I)V

    .line 54
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, " additional)"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_1

    .line 48
    :cond_3
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    aget-object v3, v1, v0

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    .line 47
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method

.method static doQuery(Lcom/samsung/android/org/xbill/DNS/Message;J)V
    .locals 3
    .param p0, "response"    # Lcom/samsung/android/org/xbill/DNS/Message;
    .param p1, "ms"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 25
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "; java dig 0.0"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 26
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v0, p0}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    .line 27
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, ";; Query time: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 28
    return-void
.end method

.method public static main([Ljava/lang/String;)V
    .locals 32
    .param p0, "argv"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 59
    const/16 v25, 0x0

    .line 63
    .local v25, "server":Ljava/lang/String;
    const/16 v22, 0x0

    .line 64
    .local v22, "res":Lcom/samsung/android/org/xbill/DNS/SimpleResolver;
    const/16 v19, 0x0

    .line 67
    .local v19, "printQuery":Z
    move-object/from16 v0, p0

    array-length v0, v0

    move/from16 v28, v0

    const/16 v29, 0x1

    move/from16 v0, v28

    move/from16 v1, v29

    if-ge v0, v1, :cond_0

    .line 68
    invoke-static {}, Lcom/samsung/android/org/xbill/dig;->usage()V

    .line 72
    :cond_0
    const/4 v8, 0x0

    .line 73
    .local v8, "arg":I
    :try_start_0
    aget-object v28, p0, v8

    const-string v29, "@"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v28

    if-eqz v28, :cond_12

    .line 74
    add-int/lit8 v9, v8, 0x1

    .end local v8    # "arg":I
    .local v9, "arg":I
    :try_start_1
    aget-object v28, p0, v8

    const/16 v29, 0x1

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v25

    .line 76
    :goto_0
    if-eqz v25, :cond_4

    .line 77
    new-instance v23, Lcom/samsung/android/org/xbill/DNS/SimpleResolver;

    move-object/from16 v0, v23

    move-object/from16 v1, v25

    invoke-direct {v0, v1}, Lcom/samsung/android/org/xbill/DNS/SimpleResolver;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_2

    .end local v22    # "res":Lcom/samsung/android/org/xbill/DNS/SimpleResolver;
    .local v23, "res":Lcom/samsung/android/org/xbill/DNS/SimpleResolver;
    move-object/from16 v22, v23

    .line 81
    .end local v23    # "res":Lcom/samsung/android/org/xbill/DNS/SimpleResolver;
    .restart local v22    # "res":Lcom/samsung/android/org/xbill/DNS/SimpleResolver;
    :goto_1
    add-int/lit8 v8, v9, 0x1

    .end local v9    # "arg":I
    .restart local v8    # "arg":I
    :try_start_2
    aget-object v16, p0, v9

    .line 82
    .local v16, "nameString":Ljava/lang/String;
    const-string v28, "-x"

    move-object/from16 v0, v16

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_2 .. :try_end_2} :catch_0

    move-result v28

    if-eqz v28, :cond_5

    .line 83
    add-int/lit8 v9, v8, 0x1

    .end local v8    # "arg":I
    .restart local v9    # "arg":I
    :try_start_3
    aget-object v28, p0, v8

    invoke-static/range {v28 .. v28}, Lcom/samsung/android/org/xbill/DNS/ReverseMap;->fromAddress(Ljava/lang/String;)Lcom/samsung/android/org/xbill/DNS/Name;

    move-result-object v28

    sput-object v28, Lcom/samsung/android/org/xbill/dig;->name:Lcom/samsung/android/org/xbill/DNS/Name;

    .line 84
    const/16 v28, 0xc

    sput v28, Lcom/samsung/android/org/xbill/dig;->type:I

    .line 85
    const/16 v28, 0x1

    sput v28, Lcom/samsung/android/org/xbill/dig;->dclass:I
    :try_end_3
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_3 .. :try_end_3} :catch_2

    move v8, v9

    .line 102
    .end local v9    # "arg":I
    .restart local v8    # "arg":I
    :goto_2
    :try_start_4
    aget-object v28, p0, v8

    const-string v29, "-"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v28

    if-eqz v28, :cond_1

    aget-object v28, p0, v8

    invoke-virtual/range {v28 .. v28}, Ljava/lang/String;->length()I
    :try_end_4
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_4 .. :try_end_4} :catch_0

    move-result v28

    const/16 v29, 0x1

    move/from16 v0, v28

    move/from16 v1, v29

    if-gt v0, v1, :cond_8

    .line 190
    .end local v16    # "nameString":Ljava/lang/String;
    :cond_1
    :goto_3
    if-nez v22, :cond_2

    .line 191
    new-instance v22, Lcom/samsung/android/org/xbill/DNS/SimpleResolver;

    .end local v22    # "res":Lcom/samsung/android/org/xbill/DNS/SimpleResolver;
    invoke-direct/range {v22 .. v22}, Lcom/samsung/android/org/xbill/DNS/SimpleResolver;-><init>()V

    .line 193
    .restart local v22    # "res":Lcom/samsung/android/org/xbill/DNS/SimpleResolver;
    :cond_2
    sget-object v28, Lcom/samsung/android/org/xbill/dig;->name:Lcom/samsung/android/org/xbill/DNS/Name;

    sget v29, Lcom/samsung/android/org/xbill/dig;->type:I

    sget v30, Lcom/samsung/android/org/xbill/dig;->dclass:I

    invoke-static/range {v28 .. v30}, Lcom/samsung/android/org/xbill/DNS/Record;->newRecord(Lcom/samsung/android/org/xbill/DNS/Name;II)Lcom/samsung/android/org/xbill/DNS/Record;

    move-result-object v21

    .line 194
    .local v21, "rec":Lcom/samsung/android/org/xbill/DNS/Record;
    invoke-static/range {v21 .. v21}, Lcom/samsung/android/org/xbill/DNS/Message;->newQuery(Lcom/samsung/android/org/xbill/DNS/Record;)Lcom/samsung/android/org/xbill/DNS/Message;

    move-result-object v20

    .line 195
    .local v20, "query":Lcom/samsung/android/org/xbill/DNS/Message;
    if-eqz v19, :cond_3

    .line 196
    sget-object v28, Ljava/lang/System;->out:Ljava/io/PrintStream;

    move-object/from16 v0, v28

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    .line 197
    :cond_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v26

    .line 198
    .local v26, "startTime":J
    move-object/from16 v0, v22

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/org/xbill/DNS/SimpleResolver;->send(Lcom/samsung/android/org/xbill/DNS/Message;)Lcom/samsung/android/org/xbill/DNS/Message;

    move-result-object v24

    .line 199
    .local v24, "response":Lcom/samsung/android/org/xbill/DNS/Message;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    .line 201
    .local v14, "endTime":J
    sget v28, Lcom/samsung/android/org/xbill/dig;->type:I

    const/16 v29, 0xfc

    move/from16 v0, v28

    move/from16 v1, v29

    if-ne v0, v1, :cond_11

    .line 202
    invoke-static/range {v24 .. v24}, Lcom/samsung/android/org/xbill/dig;->doAXFR(Lcom/samsung/android/org/xbill/DNS/Message;)V

    .line 205
    .end local v14    # "endTime":J
    .end local v20    # "query":Lcom/samsung/android/org/xbill/DNS/Message;
    .end local v21    # "rec":Lcom/samsung/android/org/xbill/DNS/Record;
    .end local v24    # "response":Lcom/samsung/android/org/xbill/DNS/Message;
    .end local v26    # "startTime":J
    :goto_4
    return-void

    .line 79
    .end local v8    # "arg":I
    .restart local v9    # "arg":I
    :cond_4
    :try_start_5
    new-instance v23, Lcom/samsung/android/org/xbill/DNS/SimpleResolver;

    invoke-direct/range {v23 .. v23}, Lcom/samsung/android/org/xbill/DNS/SimpleResolver;-><init>()V
    :try_end_5
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_5 .. :try_end_5} :catch_2

    .end local v22    # "res":Lcom/samsung/android/org/xbill/DNS/SimpleResolver;
    .restart local v23    # "res":Lcom/samsung/android/org/xbill/DNS/SimpleResolver;
    move-object/from16 v22, v23

    .end local v23    # "res":Lcom/samsung/android/org/xbill/DNS/SimpleResolver;
    .restart local v22    # "res":Lcom/samsung/android/org/xbill/DNS/SimpleResolver;
    goto :goto_1

    .line 88
    .end local v9    # "arg":I
    .restart local v8    # "arg":I
    .restart local v16    # "nameString":Ljava/lang/String;
    :cond_5
    :try_start_6
    sget-object v28, Lcom/samsung/android/org/xbill/DNS/Name;->root:Lcom/samsung/android/org/xbill/DNS/Name;

    move-object/from16 v0, v16

    move-object/from16 v1, v28

    invoke-static {v0, v1}, Lcom/samsung/android/org/xbill/DNS/Name;->fromString(Ljava/lang/String;Lcom/samsung/android/org/xbill/DNS/Name;)Lcom/samsung/android/org/xbill/DNS/Name;

    move-result-object v28

    sput-object v28, Lcom/samsung/android/org/xbill/dig;->name:Lcom/samsung/android/org/xbill/DNS/Name;

    .line 89
    aget-object v28, p0, v8

    invoke-static/range {v28 .. v28}, Lcom/samsung/android/org/xbill/DNS/Type;->value(Ljava/lang/String;)I

    move-result v28

    sput v28, Lcom/samsung/android/org/xbill/dig;->type:I

    .line 90
    sget v28, Lcom/samsung/android/org/xbill/dig;->type:I

    if-gez v28, :cond_6

    .line 91
    const/16 v28, 0x1

    sput v28, Lcom/samsung/android/org/xbill/dig;->type:I

    .line 95
    :goto_5
    aget-object v28, p0, v8

    invoke-static/range {v28 .. v28}, Lcom/samsung/android/org/xbill/DNS/DClass;->value(Ljava/lang/String;)I

    move-result v28

    sput v28, Lcom/samsung/android/org/xbill/dig;->dclass:I

    .line 96
    sget v28, Lcom/samsung/android/org/xbill/dig;->dclass:I

    if-gez v28, :cond_7

    .line 97
    const/16 v28, 0x1

    sput v28, Lcom/samsung/android/org/xbill/dig;->dclass:I
    :try_end_6
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_6 .. :try_end_6} :catch_0

    goto/16 :goto_2

    .line 186
    .end local v16    # "nameString":Ljava/lang/String;
    :catch_0
    move-exception v10

    .line 187
    .local v10, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    :goto_6
    sget-object v28, Lcom/samsung/android/org/xbill/dig;->name:Lcom/samsung/android/org/xbill/DNS/Name;

    if-nez v28, :cond_1

    .line 188
    invoke-static {}, Lcom/samsung/android/org/xbill/dig;->usage()V

    goto :goto_3

    .line 93
    .end local v10    # "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    .restart local v16    # "nameString":Ljava/lang/String;
    :cond_6
    add-int/lit8 v8, v8, 0x1

    goto :goto_5

    .line 99
    :cond_7
    add-int/lit8 v8, v8, 0x1

    .line 102
    goto/16 :goto_2

    .line 103
    :cond_8
    :try_start_7
    aget-object v28, p0, v8

    const/16 v29, 0x1

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->charAt(I)C

    move-result v28

    packed-switch v28, :pswitch_data_0

    .line 179
    :pswitch_0
    sget-object v28, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v29, "Invalid option: "

    invoke-virtual/range {v28 .. v29}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 180
    sget-object v28, Ljava/lang/System;->out:Ljava/io/PrintStream;

    aget-object v29, p0, v8

    invoke-virtual/range {v28 .. v29}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 182
    :goto_7
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_2

    .line 107
    :pswitch_1
    aget-object v28, p0, v8

    invoke-virtual/range {v28 .. v28}, Ljava/lang/String;->length()I

    move-result v28

    const/16 v29, 0x2

    move/from16 v0, v28

    move/from16 v1, v29

    if-le v0, v1, :cond_a

    .line 108
    aget-object v28, p0, v8

    const/16 v29, 0x2

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v18

    .line 111
    .local v18, "portStr":Ljava/lang/String;
    :goto_8
    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v17

    .line 112
    .local v17, "port":I
    if-ltz v17, :cond_9

    const/high16 v28, 0x10000

    move/from16 v0, v17

    move/from16 v1, v28

    if-le v0, v1, :cond_b

    .line 113
    :cond_9
    sget-object v28, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v29, "Invalid port"

    invoke-virtual/range {v28 .. v29}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_4

    .line 110
    .end local v17    # "port":I
    .end local v18    # "portStr":Ljava/lang/String;
    :cond_a
    add-int/lit8 v8, v8, 0x1

    aget-object v18, p0, v8

    .restart local v18    # "portStr":Ljava/lang/String;
    goto :goto_8

    .line 116
    .restart local v17    # "port":I
    :cond_b
    move-object/from16 v0, v22

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/android/org/xbill/DNS/SimpleResolver;->setPort(I)V

    goto :goto_7

    .line 121
    .end local v17    # "port":I
    .end local v18    # "portStr":Ljava/lang/String;
    :pswitch_2
    aget-object v28, p0, v8

    invoke-virtual/range {v28 .. v28}, Ljava/lang/String;->length()I

    move-result v28

    const/16 v29, 0x2

    move/from16 v0, v28

    move/from16 v1, v29

    if-le v0, v1, :cond_c

    .line 122
    aget-object v28, p0, v8

    const/16 v29, 0x2

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->substring(I)Ljava/lang/String;
    :try_end_7
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_7 .. :try_end_7} :catch_0

    move-result-object v7

    .line 127
    .local v7, "addrStr":Ljava/lang/String;
    :goto_9
    :try_start_8
    invoke-static {v7}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_8 .. :try_end_8} :catch_0

    move-result-object v6

    .line 133
    .local v6, "addr":Ljava/net/InetAddress;
    :try_start_9
    move-object/from16 v0, v22

    invoke-virtual {v0, v6}, Lcom/samsung/android/org/xbill/DNS/SimpleResolver;->setLocalAddress(Ljava/net/InetAddress;)V

    goto :goto_7

    .line 124
    .end local v6    # "addr":Ljava/net/InetAddress;
    .end local v7    # "addrStr":Ljava/lang/String;
    :cond_c
    add-int/lit8 v8, v8, 0x1

    aget-object v7, p0, v8

    .restart local v7    # "addrStr":Ljava/lang/String;
    goto :goto_9

    .line 129
    :catch_1
    move-exception v10

    .line 130
    .local v10, "e":Ljava/lang/Exception;
    sget-object v28, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v29, "Invalid address"

    invoke-virtual/range {v28 .. v29}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_4

    .line 138
    .end local v7    # "addrStr":Ljava/lang/String;
    .end local v10    # "e":Ljava/lang/Exception;
    :pswitch_3
    aget-object v28, p0, v8

    invoke-virtual/range {v28 .. v28}, Ljava/lang/String;->length()I

    move-result v28

    const/16 v29, 0x2

    move/from16 v0, v28

    move/from16 v1, v29

    if-le v0, v1, :cond_d

    .line 139
    aget-object v28, p0, v8

    const/16 v29, 0x2

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v13

    .line 142
    .local v13, "key":Ljava/lang/String;
    :goto_a
    invoke-static {v13}, Lcom/samsung/android/org/xbill/DNS/TSIG;->fromString(Ljava/lang/String;)Lcom/samsung/android/org/xbill/DNS/TSIG;

    move-result-object v28

    move-object/from16 v0, v22

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/samsung/android/org/xbill/DNS/SimpleResolver;->setTSIGKey(Lcom/samsung/android/org/xbill/DNS/TSIG;)V

    goto/16 :goto_7

    .line 141
    .end local v13    # "key":Ljava/lang/String;
    :cond_d
    add-int/lit8 v8, v8, 0x1

    aget-object v13, p0, v8

    .restart local v13    # "key":Ljava/lang/String;
    goto :goto_a

    .line 146
    .end local v13    # "key":Ljava/lang/String;
    :pswitch_4
    const/16 v28, 0x1

    move-object/from16 v0, v22

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/samsung/android/org/xbill/DNS/SimpleResolver;->setTCP(Z)V

    goto/16 :goto_7

    .line 150
    :pswitch_5
    const/16 v28, 0x1

    move-object/from16 v0, v22

    move/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/samsung/android/org/xbill/DNS/SimpleResolver;->setIgnoreTruncation(Z)V

    goto/16 :goto_7

    .line 156
    :pswitch_6
    aget-object v28, p0, v8

    invoke-virtual/range {v28 .. v28}, Ljava/lang/String;->length()I

    move-result v28

    const/16 v29, 0x2

    move/from16 v0, v28

    move/from16 v1, v29

    if-le v0, v1, :cond_f

    .line 157
    aget-object v28, p0, v8

    const/16 v29, 0x2

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v12

    .line 160
    .local v12, "ednsStr":Ljava/lang/String;
    :goto_b
    invoke-static {v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v11

    .line 161
    .local v11, "edns":I
    if-ltz v11, :cond_e

    const/16 v28, 0x1

    move/from16 v0, v28

    if-le v11, v0, :cond_10

    .line 162
    :cond_e
    sget-object v28, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v29, Ljava/lang/StringBuilder;

    const-string v30, "Unsupported EDNS level: "

    invoke-direct/range {v29 .. v30}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 164
    move-object/from16 v0, v29

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    .line 162
    invoke-virtual/range {v28 .. v29}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_4

    .line 159
    .end local v11    # "edns":I
    .end local v12    # "ednsStr":Ljava/lang/String;
    :cond_f
    add-int/lit8 v8, v8, 0x1

    aget-object v12, p0, v8

    .restart local v12    # "ednsStr":Ljava/lang/String;
    goto :goto_b

    .line 167
    .restart local v11    # "edns":I
    :cond_10
    move-object/from16 v0, v22

    invoke-virtual {v0, v11}, Lcom/samsung/android/org/xbill/DNS/SimpleResolver;->setEDNS(I)V

    goto/16 :goto_7

    .line 171
    .end local v11    # "edns":I
    .end local v12    # "ednsStr":Ljava/lang/String;
    :pswitch_7
    const/16 v28, 0x0

    const/16 v29, 0x0

    const v30, 0x8000

    const/16 v31, 0x0

    move-object/from16 v0, v22

    move/from16 v1, v28

    move/from16 v2, v29

    move/from16 v3, v30

    move-object/from16 v4, v31

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/android/org/xbill/DNS/SimpleResolver;->setEDNS(IIILjava/util/List;)V
    :try_end_9
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_9 .. :try_end_9} :catch_0

    goto/16 :goto_7

    .line 175
    :pswitch_8
    const/16 v19, 0x1

    .line 176
    goto/16 :goto_7

    .line 204
    .end local v16    # "nameString":Ljava/lang/String;
    .restart local v14    # "endTime":J
    .restart local v20    # "query":Lcom/samsung/android/org/xbill/DNS/Message;
    .restart local v21    # "rec":Lcom/samsung/android/org/xbill/DNS/Record;
    .restart local v24    # "response":Lcom/samsung/android/org/xbill/DNS/Message;
    .restart local v26    # "startTime":J
    :cond_11
    sub-long v28, v14, v26

    move-object/from16 v0, v24

    move-wide/from16 v1, v28

    invoke-static {v0, v1, v2}, Lcom/samsung/android/org/xbill/dig;->doQuery(Lcom/samsung/android/org/xbill/DNS/Message;J)V

    goto/16 :goto_4

    .line 186
    .end local v8    # "arg":I
    .end local v14    # "endTime":J
    .end local v20    # "query":Lcom/samsung/android/org/xbill/DNS/Message;
    .end local v21    # "rec":Lcom/samsung/android/org/xbill/DNS/Record;
    .end local v24    # "response":Lcom/samsung/android/org/xbill/DNS/Message;
    .end local v26    # "startTime":J
    .restart local v9    # "arg":I
    :catch_2
    move-exception v10

    move v8, v9

    .end local v9    # "arg":I
    .restart local v8    # "arg":I
    goto/16 :goto_6

    :cond_12
    move v9, v8

    .end local v8    # "arg":I
    .restart local v9    # "arg":I
    goto/16 :goto_0

    .line 103
    nop

    :pswitch_data_0
    .packed-switch 0x62
        :pswitch_2
        :pswitch_0
        :pswitch_7
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method static usage()V
    .locals 2

    .prologue
    .line 18
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "Usage: dig [@server] name [<type>] [<class>] [options]"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 20
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/System;->exit(I)V

    .line 21
    return-void
.end method
