.class public Lcom/samsung/android/org/xbill/DNS/NSECRecord;
.super Lcom/samsung/android/org/xbill/DNS/Record;
.source "NSECRecord.java"


# static fields
.field private static final serialVersionUID:J = -0x47adfff377d2c8a9L


# instance fields
.field private next:Lcom/samsung/android/org/xbill/DNS/Name;

.field private types:Lcom/samsung/android/org/xbill/DNS/TypeBitmap;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/samsung/android/org/xbill/DNS/Record;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/org/xbill/DNS/Name;IJLcom/samsung/android/org/xbill/DNS/Name;[I)V
    .locals 7
    .param p1, "name"    # Lcom/samsung/android/org/xbill/DNS/Name;
    .param p2, "dclass"    # I
    .param p3, "ttl"    # J
    .param p5, "next"    # Lcom/samsung/android/org/xbill/DNS/Name;
    .param p6, "types"    # [I

    .prologue
    .line 40
    const/16 v2, 0x2f

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/org/xbill/DNS/Record;-><init>(Lcom/samsung/android/org/xbill/DNS/Name;IIJ)V

    .line 41
    const-string v0, "next"

    invoke-static {v0, p5}, Lcom/samsung/android/org/xbill/DNS/NSECRecord;->checkName(Ljava/lang/String;Lcom/samsung/android/org/xbill/DNS/Name;)Lcom/samsung/android/org/xbill/DNS/Name;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/org/xbill/DNS/NSECRecord;->next:Lcom/samsung/android/org/xbill/DNS/Name;

    .line 42
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    array-length v0, p6

    if-lt v6, v0, :cond_0

    .line 45
    new-instance v0, Lcom/samsung/android/org/xbill/DNS/TypeBitmap;

    invoke-direct {v0, p6}, Lcom/samsung/android/org/xbill/DNS/TypeBitmap;-><init>([I)V

    iput-object v0, p0, Lcom/samsung/android/org/xbill/DNS/NSECRecord;->types:Lcom/samsung/android/org/xbill/DNS/TypeBitmap;

    .line 46
    return-void

    .line 43
    :cond_0
    aget v0, p6, v6

    invoke-static {v0}, Lcom/samsung/android/org/xbill/DNS/Type;->check(I)V

    .line 42
    add-int/lit8 v6, v6, 0x1

    goto :goto_0
.end method


# virtual methods
.method public getNext()Lcom/samsung/android/org/xbill/DNS/Name;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/NSECRecord;->next:Lcom/samsung/android/org/xbill/DNS/Name;

    return-object v0
.end method

.method getObject()Lcom/samsung/android/org/xbill/DNS/Record;
    .locals 1

    .prologue
    .line 30
    new-instance v0, Lcom/samsung/android/org/xbill/DNS/NSECRecord;

    invoke-direct {v0}, Lcom/samsung/android/org/xbill/DNS/NSECRecord;-><init>()V

    return-object v0
.end method

.method public getTypes()[I
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/NSECRecord;->types:Lcom/samsung/android/org/xbill/DNS/TypeBitmap;

    invoke-virtual {v0}, Lcom/samsung/android/org/xbill/DNS/TypeBitmap;->toArray()[I

    move-result-object v0

    return-object v0
.end method

.method public hasType(I)Z
    .locals 1
    .param p1, "type"    # I

    .prologue
    .line 95
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/NSECRecord;->types:Lcom/samsung/android/org/xbill/DNS/TypeBitmap;

    invoke-virtual {v0, p1}, Lcom/samsung/android/org/xbill/DNS/TypeBitmap;->contains(I)Z

    move-result v0

    return v0
.end method

.method rdataFromString(Lcom/samsung/android/org/xbill/DNS/Tokenizer;Lcom/samsung/android/org/xbill/DNS/Name;)V
    .locals 1
    .param p1, "st"    # Lcom/samsung/android/org/xbill/DNS/Tokenizer;
    .param p2, "origin"    # Lcom/samsung/android/org/xbill/DNS/Name;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 63
    invoke-virtual {p1, p2}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->getName(Lcom/samsung/android/org/xbill/DNS/Name;)Lcom/samsung/android/org/xbill/DNS/Name;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/org/xbill/DNS/NSECRecord;->next:Lcom/samsung/android/org/xbill/DNS/Name;

    .line 64
    new-instance v0, Lcom/samsung/android/org/xbill/DNS/TypeBitmap;

    invoke-direct {v0, p1}, Lcom/samsung/android/org/xbill/DNS/TypeBitmap;-><init>(Lcom/samsung/android/org/xbill/DNS/Tokenizer;)V

    iput-object v0, p0, Lcom/samsung/android/org/xbill/DNS/NSECRecord;->types:Lcom/samsung/android/org/xbill/DNS/TypeBitmap;

    .line 65
    return-void
.end method

.method rrFromWire(Lcom/samsung/android/org/xbill/DNS/DNSInput;)V
    .locals 1
    .param p1, "in"    # Lcom/samsung/android/org/xbill/DNS/DNSInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 50
    new-instance v0, Lcom/samsung/android/org/xbill/DNS/Name;

    invoke-direct {v0, p1}, Lcom/samsung/android/org/xbill/DNS/Name;-><init>(Lcom/samsung/android/org/xbill/DNS/DNSInput;)V

    iput-object v0, p0, Lcom/samsung/android/org/xbill/DNS/NSECRecord;->next:Lcom/samsung/android/org/xbill/DNS/Name;

    .line 51
    new-instance v0, Lcom/samsung/android/org/xbill/DNS/TypeBitmap;

    invoke-direct {v0, p1}, Lcom/samsung/android/org/xbill/DNS/TypeBitmap;-><init>(Lcom/samsung/android/org/xbill/DNS/DNSInput;)V

    iput-object v0, p0, Lcom/samsung/android/org/xbill/DNS/NSECRecord;->types:Lcom/samsung/android/org/xbill/DNS/TypeBitmap;

    .line 52
    return-void
.end method

.method rrToString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 71
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 72
    .local v0, "sb":Ljava/lang/StringBuffer;
    iget-object v1, p0, Lcom/samsung/android/org/xbill/DNS/NSECRecord;->next:Lcom/samsung/android/org/xbill/DNS/Name;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    .line 73
    iget-object v1, p0, Lcom/samsung/android/org/xbill/DNS/NSECRecord;->types:Lcom/samsung/android/org/xbill/DNS/TypeBitmap;

    invoke-virtual {v1}, Lcom/samsung/android/org/xbill/DNS/TypeBitmap;->empty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 74
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 75
    iget-object v1, p0, Lcom/samsung/android/org/xbill/DNS/NSECRecord;->types:Lcom/samsung/android/org/xbill/DNS/TypeBitmap;

    invoke-virtual {v1}, Lcom/samsung/android/org/xbill/DNS/TypeBitmap;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 77
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method rrToWire(Lcom/samsung/android/org/xbill/DNS/DNSOutput;Lcom/samsung/android/org/xbill/DNS/Compression;Z)V
    .locals 3
    .param p1, "out"    # Lcom/samsung/android/org/xbill/DNS/DNSOutput;
    .param p2, "c"    # Lcom/samsung/android/org/xbill/DNS/Compression;
    .param p3, "canonical"    # Z

    .prologue
    .line 57
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/NSECRecord;->next:Lcom/samsung/android/org/xbill/DNS/Name;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Lcom/samsung/android/org/xbill/DNS/Name;->toWire(Lcom/samsung/android/org/xbill/DNS/DNSOutput;Lcom/samsung/android/org/xbill/DNS/Compression;Z)V

    .line 58
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/NSECRecord;->types:Lcom/samsung/android/org/xbill/DNS/TypeBitmap;

    invoke-virtual {v0, p1}, Lcom/samsung/android/org/xbill/DNS/TypeBitmap;->toWire(Lcom/samsung/android/org/xbill/DNS/DNSOutput;)V

    .line 59
    return-void
.end method
