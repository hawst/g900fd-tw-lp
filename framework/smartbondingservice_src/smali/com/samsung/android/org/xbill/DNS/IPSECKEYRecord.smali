.class public Lcom/samsung/android/org/xbill/DNS/IPSECKEYRecord;
.super Lcom/samsung/android/org/xbill/DNS/Record;
.source "IPSECKEYRecord.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/org/xbill/DNS/IPSECKEYRecord$Algorithm;,
        Lcom/samsung/android/org/xbill/DNS/IPSECKEYRecord$Gateway;
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x2a555fd7ba8ed6b7L


# instance fields
.field private algorithmType:I

.field private gateway:Ljava/lang/Object;

.field private gatewayType:I

.field private key:[B

.field private precedence:I


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/samsung/android/org/xbill/DNS/Record;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/org/xbill/DNS/Name;IJIIILjava/lang/Object;[B)V
    .locals 9
    .param p1, "name"    # Lcom/samsung/android/org/xbill/DNS/Name;
    .param p2, "dclass"    # I
    .param p3, "ttl"    # J
    .param p5, "precedence"    # I
    .param p6, "gatewayType"    # I
    .param p7, "algorithmType"    # I
    .param p8, "gateway"    # Ljava/lang/Object;
    .param p9, "key"    # [B

    .prologue
    .line 62
    const/16 v4, 0x2d

    move-object v2, p0

    move-object v3, p1

    move v5, p2

    move-wide v6, p3

    invoke-direct/range {v2 .. v7}, Lcom/samsung/android/org/xbill/DNS/Record;-><init>(Lcom/samsung/android/org/xbill/DNS/Name;IIJ)V

    .line 63
    const-string v2, "precedence"

    invoke-static {v2, p5}, Lcom/samsung/android/org/xbill/DNS/IPSECKEYRecord;->checkU8(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/samsung/android/org/xbill/DNS/IPSECKEYRecord;->precedence:I

    .line 64
    const-string v2, "gatewayType"

    invoke-static {v2, p6}, Lcom/samsung/android/org/xbill/DNS/IPSECKEYRecord;->checkU8(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/samsung/android/org/xbill/DNS/IPSECKEYRecord;->gatewayType:I

    .line 65
    const-string v2, "algorithmType"

    move/from16 v0, p7

    invoke-static {v2, v0}, Lcom/samsung/android/org/xbill/DNS/IPSECKEYRecord;->checkU8(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/samsung/android/org/xbill/DNS/IPSECKEYRecord;->algorithmType:I

    .line 66
    packed-switch p6, :pswitch_data_0

    .line 92
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "\"gatewayType\" must be between 0 and 3"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 68
    :pswitch_0
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/samsung/android/org/xbill/DNS/IPSECKEYRecord;->gateway:Ljava/lang/Object;

    .line 96
    .end local p8    # "gateway":Ljava/lang/Object;
    :goto_0
    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/samsung/android/org/xbill/DNS/IPSECKEYRecord;->key:[B

    .line 97
    return-void

    .line 71
    .restart local p8    # "gateway":Ljava/lang/Object;
    :pswitch_1
    move-object/from16 v0, p8

    instance-of v2, v0, Ljava/net/InetAddress;

    if-nez v2, :cond_0

    .line 72
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "\"gateway\" must be an IPv4 address"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 75
    :cond_0
    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/samsung/android/org/xbill/DNS/IPSECKEYRecord;->gateway:Ljava/lang/Object;

    goto :goto_0

    .line 78
    :pswitch_2
    move-object/from16 v0, p8

    instance-of v2, v0, Ljava/net/Inet6Address;

    if-nez v2, :cond_1

    .line 79
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "\"gateway\" must be an IPv6 address"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 82
    :cond_1
    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/samsung/android/org/xbill/DNS/IPSECKEYRecord;->gateway:Ljava/lang/Object;

    goto :goto_0

    .line 85
    :pswitch_3
    move-object/from16 v0, p8

    instance-of v2, v0, Lcom/samsung/android/org/xbill/DNS/Name;

    if-nez v2, :cond_2

    .line 86
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "\"gateway\" must be a DNS name"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 89
    :cond_2
    const-string v2, "gateway"

    check-cast p8, Lcom/samsung/android/org/xbill/DNS/Name;

    .end local p8    # "gateway":Ljava/lang/Object;
    move-object/from16 v0, p8

    invoke-static {v2, v0}, Lcom/samsung/android/org/xbill/DNS/IPSECKEYRecord;->checkName(Ljava/lang/String;Lcom/samsung/android/org/xbill/DNS/Name;)Lcom/samsung/android/org/xbill/DNS/Name;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/org/xbill/DNS/IPSECKEYRecord;->gateway:Ljava/lang/Object;

    goto :goto_0

    .line 66
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public getAlgorithmType()I
    .locals 1

    .prologue
    .line 195
    iget v0, p0, Lcom/samsung/android/org/xbill/DNS/IPSECKEYRecord;->algorithmType:I

    return v0
.end method

.method public getGateway()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/IPSECKEYRecord;->gateway:Ljava/lang/Object;

    return-object v0
.end method

.method public getGatewayType()I
    .locals 1

    .prologue
    .line 189
    iget v0, p0, Lcom/samsung/android/org/xbill/DNS/IPSECKEYRecord;->gatewayType:I

    return v0
.end method

.method public getKey()[B
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/IPSECKEYRecord;->key:[B

    return-object v0
.end method

.method getObject()Lcom/samsung/android/org/xbill/DNS/Record;
    .locals 1

    .prologue
    .line 46
    new-instance v0, Lcom/samsung/android/org/xbill/DNS/IPSECKEYRecord;

    invoke-direct {v0}, Lcom/samsung/android/org/xbill/DNS/IPSECKEYRecord;-><init>()V

    return-object v0
.end method

.method public getPrecedence()I
    .locals 1

    .prologue
    .line 183
    iget v0, p0, Lcom/samsung/android/org/xbill/DNS/IPSECKEYRecord;->precedence:I

    return v0
.end method

.method rdataFromString(Lcom/samsung/android/org/xbill/DNS/Tokenizer;Lcom/samsung/android/org/xbill/DNS/Name;)V
    .locals 3
    .param p1, "st"    # Lcom/samsung/android/org/xbill/DNS/Tokenizer;
    .param p2, "origin"    # Lcom/samsung/android/org/xbill/DNS/Name;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 126
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->getUInt8()I

    move-result v1

    iput v1, p0, Lcom/samsung/android/org/xbill/DNS/IPSECKEYRecord;->precedence:I

    .line 127
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->getUInt8()I

    move-result v1

    iput v1, p0, Lcom/samsung/android/org/xbill/DNS/IPSECKEYRecord;->gatewayType:I

    .line 128
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->getUInt8()I

    move-result v1

    iput v1, p0, Lcom/samsung/android/org/xbill/DNS/IPSECKEYRecord;->algorithmType:I

    .line 129
    iget v1, p0, Lcom/samsung/android/org/xbill/DNS/IPSECKEYRecord;->gatewayType:I

    packed-switch v1, :pswitch_data_0

    .line 146
    new-instance v1, Lcom/samsung/android/org/xbill/DNS/WireParseException;

    const-string v2, "invalid gateway type"

    invoke-direct {v1, v2}, Lcom/samsung/android/org/xbill/DNS/WireParseException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 131
    :pswitch_0
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->getString()Ljava/lang/String;

    move-result-object v0

    .line 132
    .local v0, "s":Ljava/lang/String;
    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 133
    new-instance v1, Lcom/samsung/android/org/xbill/DNS/TextParseException;

    const-string v2, "invalid gateway format"

    invoke-direct {v1, v2}, Lcom/samsung/android/org/xbill/DNS/TextParseException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 134
    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/org/xbill/DNS/IPSECKEYRecord;->gateway:Ljava/lang/Object;

    .line 148
    .end local v0    # "s":Ljava/lang/String;
    :goto_0
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->getBase64(Z)[B

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/org/xbill/DNS/IPSECKEYRecord;->key:[B

    .line 149
    return-void

    .line 137
    :pswitch_1
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->getAddress(I)Ljava/net/InetAddress;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/org/xbill/DNS/IPSECKEYRecord;->gateway:Ljava/lang/Object;

    goto :goto_0

    .line 140
    :pswitch_2
    const/4 v1, 0x2

    invoke-virtual {p1, v1}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->getAddress(I)Ljava/net/InetAddress;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/org/xbill/DNS/IPSECKEYRecord;->gateway:Ljava/lang/Object;

    goto :goto_0

    .line 143
    :pswitch_3
    invoke-virtual {p1, p2}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->getName(Lcom/samsung/android/org/xbill/DNS/Name;)Lcom/samsung/android/org/xbill/DNS/Name;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/org/xbill/DNS/IPSECKEYRecord;->gateway:Ljava/lang/Object;

    goto :goto_0

    .line 129
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method rrFromWire(Lcom/samsung/android/org/xbill/DNS/DNSInput;)V
    .locals 2
    .param p1, "in"    # Lcom/samsung/android/org/xbill/DNS/DNSInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 101
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/DNSInput;->readU8()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/org/xbill/DNS/IPSECKEYRecord;->precedence:I

    .line 102
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/DNSInput;->readU8()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/org/xbill/DNS/IPSECKEYRecord;->gatewayType:I

    .line 103
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/DNSInput;->readU8()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/org/xbill/DNS/IPSECKEYRecord;->algorithmType:I

    .line 104
    iget v0, p0, Lcom/samsung/android/org/xbill/DNS/IPSECKEYRecord;->gatewayType:I

    packed-switch v0, :pswitch_data_0

    .line 118
    new-instance v0, Lcom/samsung/android/org/xbill/DNS/WireParseException;

    const-string v1, "invalid gateway type"

    invoke-direct {v0, v1}, Lcom/samsung/android/org/xbill/DNS/WireParseException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 106
    :pswitch_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/org/xbill/DNS/IPSECKEYRecord;->gateway:Ljava/lang/Object;

    .line 120
    :goto_0
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/DNSInput;->remaining()I

    move-result v0

    if-lez v0, :cond_0

    .line 121
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/DNSInput;->readByteArray()[B

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/org/xbill/DNS/IPSECKEYRecord;->key:[B

    .line 122
    :cond_0
    return-void

    .line 109
    :pswitch_1
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Lcom/samsung/android/org/xbill/DNS/DNSInput;->readByteArray(I)[B

    move-result-object v0

    invoke-static {v0}, Ljava/net/InetAddress;->getByAddress([B)Ljava/net/InetAddress;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/org/xbill/DNS/IPSECKEYRecord;->gateway:Ljava/lang/Object;

    goto :goto_0

    .line 112
    :pswitch_2
    const/16 v0, 0x10

    invoke-virtual {p1, v0}, Lcom/samsung/android/org/xbill/DNS/DNSInput;->readByteArray(I)[B

    move-result-object v0

    invoke-static {v0}, Ljava/net/InetAddress;->getByAddress([B)Ljava/net/InetAddress;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/org/xbill/DNS/IPSECKEYRecord;->gateway:Ljava/lang/Object;

    goto :goto_0

    .line 115
    :pswitch_3
    new-instance v0, Lcom/samsung/android/org/xbill/DNS/Name;

    invoke-direct {v0, p1}, Lcom/samsung/android/org/xbill/DNS/Name;-><init>(Lcom/samsung/android/org/xbill/DNS/DNSInput;)V

    iput-object v0, p0, Lcom/samsung/android/org/xbill/DNS/IPSECKEYRecord;->gateway:Ljava/lang/Object;

    goto :goto_0

    .line 104
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method rrToString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 153
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 154
    .local v1, "sb":Ljava/lang/StringBuffer;
    iget v2, p0, Lcom/samsung/android/org/xbill/DNS/IPSECKEYRecord;->precedence:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 155
    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 156
    iget v2, p0, Lcom/samsung/android/org/xbill/DNS/IPSECKEYRecord;->gatewayType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 157
    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 158
    iget v2, p0, Lcom/samsung/android/org/xbill/DNS/IPSECKEYRecord;->algorithmType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 159
    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 160
    iget v2, p0, Lcom/samsung/android/org/xbill/DNS/IPSECKEYRecord;->gatewayType:I

    packed-switch v2, :pswitch_data_0

    .line 173
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/org/xbill/DNS/IPSECKEYRecord;->key:[B

    if-eqz v2, :cond_0

    .line 174
    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 175
    iget-object v2, p0, Lcom/samsung/android/org/xbill/DNS/IPSECKEYRecord;->key:[B

    invoke-static {v2}, Lcom/samsung/android/org/xbill/DNS/utils/base64;->toString([B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 177
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 162
    :pswitch_0
    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 166
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/IPSECKEYRecord;->gateway:Ljava/lang/Object;

    check-cast v0, Ljava/net/InetAddress;

    .line 167
    .local v0, "gatewayAddr":Ljava/net/InetAddress;
    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 170
    .end local v0    # "gatewayAddr":Ljava/net/InetAddress;
    :pswitch_2
    iget-object v2, p0, Lcom/samsung/android/org/xbill/DNS/IPSECKEYRecord;->gateway:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 160
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method rrToWire(Lcom/samsung/android/org/xbill/DNS/DNSOutput;Lcom/samsung/android/org/xbill/DNS/Compression;Z)V
    .locals 3
    .param p1, "out"    # Lcom/samsung/android/org/xbill/DNS/DNSOutput;
    .param p2, "c"    # Lcom/samsung/android/org/xbill/DNS/Compression;
    .param p3, "canonical"    # Z

    .prologue
    .line 212
    iget v2, p0, Lcom/samsung/android/org/xbill/DNS/IPSECKEYRecord;->precedence:I

    invoke-virtual {p1, v2}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeU8(I)V

    .line 213
    iget v2, p0, Lcom/samsung/android/org/xbill/DNS/IPSECKEYRecord;->gatewayType:I

    invoke-virtual {p1, v2}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeU8(I)V

    .line 214
    iget v2, p0, Lcom/samsung/android/org/xbill/DNS/IPSECKEYRecord;->algorithmType:I

    invoke-virtual {p1, v2}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeU8(I)V

    .line 215
    iget v2, p0, Lcom/samsung/android/org/xbill/DNS/IPSECKEYRecord;->gatewayType:I

    packed-switch v2, :pswitch_data_0

    .line 228
    :goto_0
    :pswitch_0
    iget-object v2, p0, Lcom/samsung/android/org/xbill/DNS/IPSECKEYRecord;->key:[B

    if-eqz v2, :cond_0

    .line 229
    iget-object v2, p0, Lcom/samsung/android/org/xbill/DNS/IPSECKEYRecord;->key:[B

    invoke-virtual {p1, v2}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeByteArray([B)V

    .line 230
    :cond_0
    return-void

    .line 220
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/IPSECKEYRecord;->gateway:Ljava/lang/Object;

    check-cast v0, Ljava/net/InetAddress;

    .line 221
    .local v0, "gatewayAddr":Ljava/net/InetAddress;
    invoke-virtual {v0}, Ljava/net/InetAddress;->getAddress()[B

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeByteArray([B)V

    goto :goto_0

    .line 224
    .end local v0    # "gatewayAddr":Ljava/net/InetAddress;
    :pswitch_2
    iget-object v1, p0, Lcom/samsung/android/org/xbill/DNS/IPSECKEYRecord;->gateway:Ljava/lang/Object;

    check-cast v1, Lcom/samsung/android/org/xbill/DNS/Name;

    .line 225
    .local v1, "gatewayName":Lcom/samsung/android/org/xbill/DNS/Name;
    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2, p3}, Lcom/samsung/android/org/xbill/DNS/Name;->toWire(Lcom/samsung/android/org/xbill/DNS/DNSOutput;Lcom/samsung/android/org/xbill/DNS/Compression;Z)V

    goto :goto_0

    .line 215
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
