.class public Lcom/samsung/android/org/xbill/DNS/NSAPRecord;
.super Lcom/samsung/android/org/xbill/DNS/Record;
.source "NSAPRecord.java"


# static fields
.field private static final serialVersionUID:J = -0xe64e874f2937ae1L


# instance fields
.field private address:[B


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/samsung/android/org/xbill/DNS/Record;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/org/xbill/DNS/Name;IJLjava/lang/String;)V
    .locals 7
    .param p1, "name"    # Lcom/samsung/android/org/xbill/DNS/Name;
    .param p2, "dclass"    # I
    .param p3, "ttl"    # J
    .param p5, "address"    # Ljava/lang/String;

    .prologue
    .line 68
    const/16 v2, 0x16

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/org/xbill/DNS/Record;-><init>(Lcom/samsung/android/org/xbill/DNS/Name;IIJ)V

    .line 69
    invoke-static {p5}, Lcom/samsung/android/org/xbill/DNS/NSAPRecord;->checkAndConvertAddress(Ljava/lang/String;)[B

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/org/xbill/DNS/NSAPRecord;->address:[B

    .line 70
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/NSAPRecord;->address:[B

    if-nez v0, :cond_0

    .line 71
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "invalid NSAP address "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 72
    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 71
    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 73
    :cond_0
    return-void
.end method

.method private static final checkAndConvertAddress(Ljava/lang/String;)[B
    .locals 9
    .param p0, "address"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 30
    const/4 v7, 0x0

    const/4 v8, 0x2

    invoke-virtual {p0, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    const-string v8, "0x"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 58
    :cond_0
    :goto_0
    return-object v6

    .line 33
    :cond_1
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 34
    .local v0, "bytes":Ljava/io/ByteArrayOutputStream;
    const/4 v4, 0x0

    .line 35
    .local v4, "partial":Z
    const/4 v2, 0x0

    .line 36
    .local v2, "current":I
    const/4 v3, 0x2

    .local v3, "i":I
    :goto_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v7

    if-lt v3, v7, :cond_2

    .line 55
    if-nez v4, :cond_0

    .line 58
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v6

    goto :goto_0

    .line 37
    :cond_2
    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 38
    .local v1, "c":C
    const/16 v7, 0x2e

    if-ne v1, v7, :cond_3

    .line 36
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 41
    :cond_3
    const/16 v7, 0x10

    invoke-static {v1, v7}, Ljava/lang/Character;->digit(CI)I

    move-result v5

    .line 42
    .local v5, "value":I
    const/4 v7, -0x1

    if-eq v5, v7, :cond_0

    .line 45
    if-eqz v4, :cond_4

    .line 46
    add-int/2addr v2, v5

    .line 47
    invoke-virtual {v0, v2}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 48
    const/4 v4, 0x0

    .line 49
    goto :goto_2

    .line 50
    :cond_4
    shl-int/lit8 v2, v5, 0x4

    .line 51
    const/4 v4, 0x1

    goto :goto_2
.end method


# virtual methods
.method public getAddress()Ljava/lang/String;
    .locals 2

    .prologue
    .line 94
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/NSAPRecord;->address:[B

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/samsung/android/org/xbill/DNS/NSAPRecord;->byteArrayToString([BZ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getObject()Lcom/samsung/android/org/xbill/DNS/Record;
    .locals 1

    .prologue
    .line 25
    new-instance v0, Lcom/samsung/android/org/xbill/DNS/NSAPRecord;

    invoke-direct {v0}, Lcom/samsung/android/org/xbill/DNS/NSAPRecord;-><init>()V

    return-object v0
.end method

.method rdataFromString(Lcom/samsung/android/org/xbill/DNS/Tokenizer;Lcom/samsung/android/org/xbill/DNS/Name;)V
    .locals 3
    .param p1, "st"    # Lcom/samsung/android/org/xbill/DNS/Tokenizer;
    .param p2, "origin"    # Lcom/samsung/android/org/xbill/DNS/Name;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 83
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->getString()Ljava/lang/String;

    move-result-object v0

    .line 84
    .local v0, "addr":Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/android/org/xbill/DNS/NSAPRecord;->checkAndConvertAddress(Ljava/lang/String;)[B

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/org/xbill/DNS/NSAPRecord;->address:[B

    .line 85
    iget-object v1, p0, Lcom/samsung/android/org/xbill/DNS/NSAPRecord;->address:[B

    if-nez v1, :cond_0

    .line 86
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "invalid NSAP address "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->exception(Ljava/lang/String;)Lcom/samsung/android/org/xbill/DNS/TextParseException;

    move-result-object v1

    throw v1

    .line 87
    :cond_0
    return-void
.end method

.method rrFromWire(Lcom/samsung/android/org/xbill/DNS/DNSInput;)V
    .locals 1
    .param p1, "in"    # Lcom/samsung/android/org/xbill/DNS/DNSInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 78
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/DNSInput;->readByteArray()[B

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/org/xbill/DNS/NSAPRecord;->address:[B

    .line 79
    return-void
.end method

.method rrToString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 104
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "0x"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsung/android/org/xbill/DNS/NSAPRecord;->address:[B

    invoke-static {v1}, Lcom/samsung/android/org/xbill/DNS/utils/base16;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method rrToWire(Lcom/samsung/android/org/xbill/DNS/DNSOutput;Lcom/samsung/android/org/xbill/DNS/Compression;Z)V
    .locals 1
    .param p1, "out"    # Lcom/samsung/android/org/xbill/DNS/DNSOutput;
    .param p2, "c"    # Lcom/samsung/android/org/xbill/DNS/Compression;
    .param p3, "canonical"    # Z

    .prologue
    .line 99
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/NSAPRecord;->address:[B

    invoke-virtual {p1, v0}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeByteArray([B)V

    .line 100
    return-void
.end method
