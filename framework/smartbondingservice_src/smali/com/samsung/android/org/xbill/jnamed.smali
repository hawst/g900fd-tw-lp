.class public Lcom/samsung/android/org/xbill/jnamed;
.super Ljava/lang/Object;
.source "jnamed.java"


# static fields
.field static final FLAG_DNSSECOK:I = 0x1

.field static final FLAG_SIGONLY:I = 0x2


# instance fields
.field TSIGs:Ljava/util/Map;

.field caches:Ljava/util/Map;

.field znames:Ljava/util/Map;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 23
    .param p1, "conffile"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/samsung/android/org/xbill/DNS/ZoneTransferException;
        }
    .end annotation

    .prologue
    .line 27
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 31
    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    .line 32
    .local v16, "ports":Ljava/util/List;
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 34
    .local v5, "addresses":Ljava/util/List;
    :try_start_0
    new-instance v9, Ljava/io/FileInputStream;

    move-object/from16 v0, p1

    invoke-direct {v9, v0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 35
    .local v9, "fs":Ljava/io/FileInputStream;
    new-instance v12, Ljava/io/InputStreamReader;

    invoke-direct {v12, v9}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    .line 36
    .local v12, "isr":Ljava/io/InputStreamReader;
    new-instance v6, Ljava/io/BufferedReader;

    invoke-direct {v6, v12}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 44
    .local v6, "br":Ljava/io/BufferedReader;
    :try_start_1
    new-instance v20, Ljava/util/HashMap;

    invoke-direct/range {v20 .. v20}, Ljava/util/HashMap;-><init>()V

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/org/xbill/jnamed;->caches:Ljava/util/Map;

    .line 45
    new-instance v20, Ljava/util/HashMap;

    invoke-direct/range {v20 .. v20}, Ljava/util/HashMap;-><init>()V

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/org/xbill/jnamed;->znames:Ljava/util/Map;

    .line 46
    new-instance v20, Ljava/util/HashMap;

    invoke-direct/range {v20 .. v20}, Ljava/util/HashMap;-><init>()V

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/org/xbill/jnamed;->TSIGs:Ljava/util/Map;

    .line 48
    const/4 v14, 0x0

    .line 49
    .local v14, "line":Ljava/lang/String;
    :cond_0
    :goto_0
    invoke-virtual {v6}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v14

    if-nez v14, :cond_4

    .line 87
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->size()I

    move-result v20

    if-nez v20, :cond_1

    .line 88
    new-instance v20, Ljava/lang/Integer;

    const/16 v21, 0x35

    invoke-direct/range {v20 .. v21}, Ljava/lang/Integer;-><init>(I)V

    move-object/from16 v0, v16

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 90
    :cond_1
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v20

    if-nez v20, :cond_2

    .line 91
    const-string v20, "0.0.0.0"

    invoke-static/range {v20 .. v20}, Lcom/samsung/android/org/xbill/DNS/Address;->getByAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 93
    :cond_2
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .line 94
    .local v10, "iaddr":Ljava/util/Iterator;
    :cond_3
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-nez v20, :cond_d

    .line 105
    sget-object v20, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v21, "jnamed: running"

    invoke-virtual/range {v20 .. v21}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 108
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V

    .line 109
    .end local v6    # "br":Ljava/io/BufferedReader;
    .end local v9    # "fs":Ljava/io/FileInputStream;
    .end local v10    # "iaddr":Ljava/util/Iterator;
    .end local v12    # "isr":Ljava/io/InputStreamReader;
    .end local v14    # "line":Ljava/lang/String;
    :goto_1
    return-void

    .line 38
    :catch_0
    move-exception v8

    .line 39
    .local v8, "e":Ljava/lang/Exception;
    sget-object v20, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v21, Ljava/lang/StringBuilder;

    const-string v22, "Cannot open "

    invoke-direct/range {v21 .. v22}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v21

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_1

    .line 50
    .end local v8    # "e":Ljava/lang/Exception;
    .restart local v6    # "br":Ljava/io/BufferedReader;
    .restart local v9    # "fs":Ljava/io/FileInputStream;
    .restart local v12    # "isr":Ljava/io/InputStreamReader;
    .restart local v14    # "line":Ljava/lang/String;
    :cond_4
    :try_start_2
    new-instance v19, Ljava/util/StringTokenizer;

    move-object/from16 v0, v19

    invoke-direct {v0, v14}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;)V

    .line 51
    .local v19, "st":Ljava/util/StringTokenizer;
    invoke-virtual/range {v19 .. v19}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v20

    if-eqz v20, :cond_0

    .line 53
    invoke-virtual/range {v19 .. v19}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v13

    .line 54
    .local v13, "keyword":Ljava/lang/String;
    invoke-virtual/range {v19 .. v19}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v20

    if-nez v20, :cond_5

    .line 55
    sget-object v20, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v21, Ljava/lang/StringBuilder;

    const-string v22, "Invalid line: "

    invoke-direct/range {v21 .. v22}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v21

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 107
    .end local v13    # "keyword":Ljava/lang/String;
    .end local v14    # "line":Ljava/lang/String;
    .end local v19    # "st":Ljava/util/StringTokenizer;
    :catchall_0
    move-exception v20

    .line 108
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V

    .line 109
    throw v20

    .line 58
    .restart local v13    # "keyword":Ljava/lang/String;
    .restart local v14    # "line":Ljava/lang/String;
    .restart local v19    # "st":Ljava/util/StringTokenizer;
    :cond_5
    const/16 v20, 0x0

    :try_start_3
    move/from16 v0, v20

    invoke-virtual {v13, v0}, Ljava/lang/String;->charAt(I)C

    move-result v20

    const/16 v21, 0x23

    move/from16 v0, v20

    move/from16 v1, v21

    if-eq v0, v1, :cond_0

    .line 60
    const-string v20, "primary"

    move-object/from16 v0, v20

    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_6

    .line 61
    invoke-virtual/range {v19 .. v19}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v19}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/org/xbill/jnamed;->addPrimaryZone(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 62
    :cond_6
    const-string v20, "secondary"

    move-object/from16 v0, v20

    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_7

    .line 63
    invoke-virtual/range {v19 .. v19}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v20

    .line 64
    invoke-virtual/range {v19 .. v19}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v21

    .line 63
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/org/xbill/jnamed;->addSecondaryZone(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 65
    :cond_7
    const-string v20, "cache"

    move-object/from16 v0, v20

    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_8

    .line 66
    new-instance v7, Lcom/samsung/android/org/xbill/DNS/Cache;

    invoke-virtual/range {v19 .. v19}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-direct {v7, v0}, Lcom/samsung/android/org/xbill/DNS/Cache;-><init>(Ljava/lang/String;)V

    .line 67
    .local v7, "cache":Lcom/samsung/android/org/xbill/DNS/Cache;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/org/xbill/jnamed;->caches:Ljava/util/Map;

    move-object/from16 v20, v0

    new-instance v21, Ljava/lang/Integer;

    const/16 v22, 0x1

    invoke-direct/range {v21 .. v22}, Ljava/lang/Integer;-><init>(I)V

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-interface {v0, v1, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 68
    .end local v7    # "cache":Lcom/samsung/android/org/xbill/DNS/Cache;
    :cond_8
    const-string v20, "key"

    move-object/from16 v0, v20

    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_a

    .line 69
    invoke-virtual/range {v19 .. v19}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v17

    .line 70
    .local v17, "s1":Ljava/lang/String;
    invoke-virtual/range {v19 .. v19}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v18

    .line 71
    .local v18, "s2":Ljava/lang/String;
    invoke-virtual/range {v19 .. v19}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v20

    if-eqz v20, :cond_9

    .line 72
    invoke-virtual/range {v19 .. v19}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    move-object/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/org/xbill/jnamed;->addTSIG(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 74
    :cond_9
    const-string v20, "hmac-md5"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v17

    move-object/from16 v3, v18

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/org/xbill/jnamed;->addTSIG(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 75
    .end local v17    # "s1":Ljava/lang/String;
    .end local v18    # "s2":Ljava/lang/String;
    :cond_a
    const-string v20, "port"

    move-object/from16 v0, v20

    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_b

    .line 76
    invoke-virtual/range {v19 .. v19}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v20

    move-object/from16 v0, v16

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 77
    :cond_b
    const-string v20, "address"

    move-object/from16 v0, v20

    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_c

    .line 78
    invoke-virtual/range {v19 .. v19}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v4

    .line 79
    .local v4, "addr":Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/org/xbill/DNS/Address;->getByAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 81
    .end local v4    # "addr":Ljava/lang/String;
    :cond_c
    sget-object v20, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v21, Ljava/lang/StringBuilder;

    const-string v22, "unknown keyword: "

    invoke-direct/range {v21 .. v22}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 82
    move-object/from16 v0, v21

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    .line 81
    invoke-virtual/range {v20 .. v21}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 95
    .end local v13    # "keyword":Ljava/lang/String;
    .end local v19    # "st":Ljava/util/StringTokenizer;
    .restart local v10    # "iaddr":Ljava/util/Iterator;
    :cond_d
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/net/InetAddress;

    .line 96
    .local v4, "addr":Ljava/net/InetAddress;
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .line 97
    .local v11, "iport":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_3

    .line 98
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/lang/Integer;

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Integer;->intValue()I

    move-result v15

    .line 99
    .local v15, "port":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v15}, Lcom/samsung/android/org/xbill/jnamed;->addUDP(Ljava/net/InetAddress;I)V

    .line 100
    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v15}, Lcom/samsung/android/org/xbill/jnamed;->addTCP(Ljava/net/InetAddress;I)V

    .line 101
    sget-object v20, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v21, Ljava/lang/StringBuilder;

    const-string v22, "jnamed: listening on "

    invoke-direct/range {v21 .. v22}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 102
    invoke-static {v4, v15}, Lcom/samsung/android/org/xbill/jnamed;->addrport(Ljava/net/InetAddress;I)Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    .line 101
    invoke-virtual/range {v20 .. v21}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2
.end method

.method private final addAdditional(Lcom/samsung/android/org/xbill/DNS/Message;I)V
    .locals 1
    .param p1, "response"    # Lcom/samsung/android/org/xbill/DNS/Message;
    .param p2, "flags"    # I

    .prologue
    .line 252
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0, p2}, Lcom/samsung/android/org/xbill/jnamed;->addAdditional2(Lcom/samsung/android/org/xbill/DNS/Message;II)V

    .line 253
    const/4 v0, 0x2

    invoke-direct {p0, p1, v0, p2}, Lcom/samsung/android/org/xbill/jnamed;->addAdditional2(Lcom/samsung/android/org/xbill/DNS/Message;II)V

    .line 254
    return-void
.end method

.method private addAdditional2(Lcom/samsung/android/org/xbill/DNS/Message;II)V
    .locals 5
    .param p1, "response"    # Lcom/samsung/android/org/xbill/DNS/Message;
    .param p2, "section"    # I
    .param p3, "flags"    # I

    .prologue
    .line 241
    invoke-virtual {p1, p2}, Lcom/samsung/android/org/xbill/DNS/Message;->getSectionArray(I)[Lcom/samsung/android/org/xbill/DNS/Record;

    move-result-object v3

    .line 242
    .local v3, "records":[Lcom/samsung/android/org/xbill/DNS/Record;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v4, v3

    if-lt v1, v4, :cond_0

    .line 248
    return-void

    .line 243
    :cond_0
    aget-object v2, v3, v1

    .line 244
    .local v2, "r":Lcom/samsung/android/org/xbill/DNS/Record;
    invoke-virtual {v2}, Lcom/samsung/android/org/xbill/DNS/Record;->getAdditionalName()Lcom/samsung/android/org/xbill/DNS/Name;

    move-result-object v0

    .line 245
    .local v0, "glueName":Lcom/samsung/android/org/xbill/DNS/Name;
    if-eqz v0, :cond_1

    .line 246
    invoke-direct {p0, p1, v0, p3}, Lcom/samsung/android/org/xbill/jnamed;->addGlue(Lcom/samsung/android/org/xbill/DNS/Message;Lcom/samsung/android/org/xbill/DNS/Name;I)V

    .line 242
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private final addCacheNS(Lcom/samsung/android/org/xbill/DNS/Message;Lcom/samsung/android/org/xbill/DNS/Cache;Lcom/samsung/android/org/xbill/DNS/Name;)V
    .locals 6
    .param p1, "response"    # Lcom/samsung/android/org/xbill/DNS/Message;
    .param p2, "cache"    # Lcom/samsung/android/org/xbill/DNS/Cache;
    .param p3, "name"    # Lcom/samsung/android/org/xbill/DNS/Name;

    .prologue
    const/4 v5, 0x2

    .line 220
    const/4 v4, 0x0

    invoke-virtual {p2, p3, v5, v4}, Lcom/samsung/android/org/xbill/DNS/Cache;->lookupRecords(Lcom/samsung/android/org/xbill/DNS/Name;II)Lcom/samsung/android/org/xbill/DNS/SetResponse;

    move-result-object v3

    .line 221
    .local v3, "sr":Lcom/samsung/android/org/xbill/DNS/SetResponse;
    invoke-virtual {v3}, Lcom/samsung/android/org/xbill/DNS/SetResponse;->isDelegation()Z

    move-result v4

    if-nez v4, :cond_1

    .line 229
    :cond_0
    return-void

    .line 223
    :cond_1
    invoke-virtual {v3}, Lcom/samsung/android/org/xbill/DNS/SetResponse;->getNS()Lcom/samsung/android/org/xbill/DNS/RRset;

    move-result-object v1

    .line 224
    .local v1, "nsRecords":Lcom/samsung/android/org/xbill/DNS/RRset;
    invoke-virtual {v1}, Lcom/samsung/android/org/xbill/DNS/RRset;->rrs()Ljava/util/Iterator;

    move-result-object v0

    .line 225
    .local v0, "it":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 226
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/org/xbill/DNS/Record;

    .line 227
    .local v2, "r":Lcom/samsung/android/org/xbill/DNS/Record;
    invoke-virtual {p1, v2, v5}, Lcom/samsung/android/org/xbill/DNS/Message;->addRecord(Lcom/samsung/android/org/xbill/DNS/Record;I)V

    goto :goto_0
.end method

.method private addGlue(Lcom/samsung/android/org/xbill/DNS/Message;Lcom/samsung/android/org/xbill/DNS/Name;I)V
    .locals 6
    .param p1, "response"    # Lcom/samsung/android/org/xbill/DNS/Message;
    .param p2, "name"    # Lcom/samsung/android/org/xbill/DNS/Name;
    .param p3, "flags"    # I

    .prologue
    const/4 v0, 0x1

    .line 233
    invoke-virtual {p0, p2, v0, v0, v0}, Lcom/samsung/android/org/xbill/jnamed;->findExactMatch(Lcom/samsung/android/org/xbill/DNS/Name;IIZ)Lcom/samsung/android/org/xbill/DNS/RRset;

    move-result-object v3

    .line 234
    .local v3, "a":Lcom/samsung/android/org/xbill/DNS/RRset;
    if-nez v3, :cond_0

    .line 237
    :goto_0
    return-void

    .line 236
    :cond_0
    const/4 v4, 0x3

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    move v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/org/xbill/jnamed;->addRRset(Lcom/samsung/android/org/xbill/DNS/Name;Lcom/samsung/android/org/xbill/DNS/Message;Lcom/samsung/android/org/xbill/DNS/RRset;II)V

    goto :goto_0
.end method

.method private final addNS(Lcom/samsung/android/org/xbill/DNS/Message;Lcom/samsung/android/org/xbill/DNS/Zone;I)V
    .locals 6
    .param p1, "response"    # Lcom/samsung/android/org/xbill/DNS/Message;
    .param p2, "zone"    # Lcom/samsung/android/org/xbill/DNS/Zone;
    .param p3, "flags"    # I

    .prologue
    .line 213
    invoke-virtual {p2}, Lcom/samsung/android/org/xbill/DNS/Zone;->getNS()Lcom/samsung/android/org/xbill/DNS/RRset;

    move-result-object v3

    .line 214
    .local v3, "nsRecords":Lcom/samsung/android/org/xbill/DNS/RRset;
    invoke-virtual {v3}, Lcom/samsung/android/org/xbill/DNS/RRset;->getName()Lcom/samsung/android/org/xbill/DNS/Name;

    move-result-object v1

    .line 215
    const/4 v4, 0x2

    move-object v0, p0

    move-object v2, p1

    move v5, p3

    .line 214
    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/org/xbill/jnamed;->addRRset(Lcom/samsung/android/org/xbill/DNS/Name;Lcom/samsung/android/org/xbill/DNS/Message;Lcom/samsung/android/org/xbill/DNS/RRset;II)V

    .line 216
    return-void
.end method

.method private final addSOA(Lcom/samsung/android/org/xbill/DNS/Message;Lcom/samsung/android/org/xbill/DNS/Zone;)V
    .locals 2
    .param p1, "response"    # Lcom/samsung/android/org/xbill/DNS/Message;
    .param p2, "zone"    # Lcom/samsung/android/org/xbill/DNS/Zone;

    .prologue
    .line 208
    invoke-virtual {p2}, Lcom/samsung/android/org/xbill/DNS/Zone;->getSOA()Lcom/samsung/android/org/xbill/DNS/SOARecord;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {p1, v0, v1}, Lcom/samsung/android/org/xbill/DNS/Message;->addRecord(Lcom/samsung/android/org/xbill/DNS/Record;I)V

    .line 209
    return-void
.end method

.method private static addrport(Ljava/net/InetAddress;I)Ljava/lang/String;
    .locals 2
    .param p0, "addr"    # Ljava/net/InetAddress;
    .param p1, "port"    # I

    .prologue
    .line 23
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "#"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static main([Ljava/lang/String;)V
    .locals 7
    .param p0, "args"    # [Ljava/lang/String;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 621
    array-length v3, p0

    if-le v3, v6, :cond_0

    .line 622
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "usage: jnamed [conf]"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 623
    invoke-static {v5}, Ljava/lang/System;->exit(I)V

    .line 628
    :cond_0
    :try_start_0
    array-length v3, p0

    if-ne v3, v6, :cond_1

    .line 629
    const/4 v3, 0x0

    aget-object v0, p0, v3

    .line 632
    .local v0, "conf":Ljava/lang/String;
    :goto_0
    new-instance v2, Lcom/samsung/android/org/xbill/jnamed;

    invoke-direct {v2, v0}, Lcom/samsung/android/org/xbill/jnamed;-><init>(Ljava/lang/String;)V

    .line 640
    .end local v0    # "conf":Ljava/lang/String;
    :goto_1
    return-void

    .line 631
    :cond_1
    const-string v0, "jnamed.conf"
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/samsung/android/org/xbill/DNS/ZoneTransferException; {:try_start_0 .. :try_end_0} :catch_1

    .restart local v0    # "conf":Ljava/lang/String;
    goto :goto_0

    .line 634
    .end local v0    # "conf":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 635
    .local v1, "e":Ljava/io/IOException;
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v3, v1}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    goto :goto_1

    .line 637
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 638
    .local v1, "e":Lcom/samsung/android/org/xbill/DNS/ZoneTransferException;
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v3, v1}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    goto :goto_1
.end method


# virtual methods
.method public TCPclient(Ljava/net/Socket;)V
    .locals 12
    .param p1, "s"    # Ljava/net/Socket;

    .prologue
    .line 502
    :try_start_0
    invoke-virtual {p1}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v5

    .line 503
    .local v5, "is":Ljava/io/InputStream;
    new-instance v0, Ljava/io/DataInputStream;

    invoke-direct {v0, v5}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 504
    .local v0, "dataIn":Ljava/io/DataInputStream;
    invoke-virtual {v0}, Ljava/io/DataInputStream;->readUnsignedShort()I

    move-result v4

    .line 505
    .local v4, "inLength":I
    new-array v3, v4, [B

    .line 506
    .local v3, "in":[B
    invoke-virtual {v0, v3}, Ljava/io/DataInputStream;->readFully([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 509
    const/4 v7, 0x0

    .line 511
    .local v7, "response":[B
    :try_start_1
    new-instance v6, Lcom/samsung/android/org/xbill/DNS/Message;

    invoke-direct {v6, v3}, Lcom/samsung/android/org/xbill/DNS/Message;-><init>([B)V

    .line 512
    .local v6, "query":Lcom/samsung/android/org/xbill/DNS/Message;
    array-length v8, v3

    invoke-virtual {p0, v6, v3, v8, p1}, Lcom/samsung/android/org/xbill/jnamed;->generateReply(Lcom/samsung/android/org/xbill/DNS/Message;[BILjava/net/Socket;)[B
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v7

    .line 513
    if-nez v7, :cond_0

    .line 531
    :try_start_2
    invoke-virtual {p1}, Ljava/net/Socket;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4

    .line 535
    .end local v0    # "dataIn":Ljava/io/DataInputStream;
    .end local v3    # "in":[B
    .end local v4    # "inLength":I
    .end local v5    # "is":Ljava/io/InputStream;
    .end local v6    # "query":Lcom/samsung/android/org/xbill/DNS/Message;
    .end local v7    # "response":[B
    :goto_0
    return-void

    .line 516
    .restart local v0    # "dataIn":Ljava/io/DataInputStream;
    .restart local v3    # "in":[B
    .restart local v4    # "inLength":I
    .restart local v5    # "is":Ljava/io/InputStream;
    .restart local v7    # "response":[B
    :catch_0
    move-exception v2

    .line 517
    .local v2, "e":Ljava/io/IOException;
    :try_start_3
    invoke-virtual {p0, v3}, Lcom/samsung/android/org/xbill/jnamed;->formerrMessage([B)[B

    move-result-object v7

    .line 519
    .end local v2    # "e":Ljava/io/IOException;
    :cond_0
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-virtual {p1}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v8

    invoke-direct {v1, v8}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 520
    .local v1, "dataOut":Ljava/io/DataOutputStream;
    array-length v8, v7

    invoke-virtual {v1, v8}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 521
    invoke-virtual {v1, v7}, Ljava/io/DataOutputStream;->write([B)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 531
    :try_start_4
    invoke-virtual {p1}, Ljava/net/Socket;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    .line 533
    :catch_1
    move-exception v8

    goto :goto_0

    .line 523
    .end local v0    # "dataIn":Ljava/io/DataInputStream;
    .end local v1    # "dataOut":Ljava/io/DataOutputStream;
    .end local v3    # "in":[B
    .end local v4    # "inLength":I
    .end local v5    # "is":Ljava/io/InputStream;
    .end local v7    # "response":[B
    :catch_2
    move-exception v2

    .line 524
    .restart local v2    # "e":Ljava/io/IOException;
    :try_start_5
    sget-object v8, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "TCPclient("

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 525
    invoke-virtual {p1}, Ljava/net/Socket;->getLocalAddress()Ljava/net/InetAddress;

    move-result-object v10

    .line 526
    invoke-virtual {p1}, Ljava/net/Socket;->getLocalPort()I

    move-result v11

    .line 525
    invoke-static {v10, v11}, Lcom/samsung/android/org/xbill/jnamed;->addrport(Ljava/net/InetAddress;I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 527
    const-string v10, "): "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 524
    invoke-virtual {v8, v9}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 531
    :try_start_6
    invoke-virtual {p1}, Ljava/net/Socket;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    goto :goto_0

    .line 533
    :catch_3
    move-exception v8

    goto :goto_0

    .line 529
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v8

    .line 531
    :try_start_7
    invoke-virtual {p1}, Ljava/net/Socket;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    .line 534
    :goto_1
    throw v8

    .line 533
    .restart local v0    # "dataIn":Ljava/io/DataInputStream;
    .restart local v3    # "in":[B
    .restart local v4    # "inLength":I
    .restart local v5    # "is":Ljava/io/InputStream;
    .restart local v6    # "query":Lcom/samsung/android/org/xbill/DNS/Message;
    .restart local v7    # "response":[B
    :catch_4
    move-exception v8

    goto :goto_0

    .end local v0    # "dataIn":Ljava/io/DataInputStream;
    .end local v3    # "in":[B
    .end local v4    # "inLength":I
    .end local v5    # "is":Ljava/io/InputStream;
    .end local v6    # "query":Lcom/samsung/android/org/xbill/DNS/Message;
    .end local v7    # "response":[B
    :catch_5
    move-exception v9

    goto :goto_1
.end method

.method addAnswer(Lcom/samsung/android/org/xbill/DNS/Message;Lcom/samsung/android/org/xbill/DNS/Name;IIII)B
    .locals 32
    .param p1, "response"    # Lcom/samsung/android/org/xbill/DNS/Message;
    .param p2, "name"    # Lcom/samsung/android/org/xbill/DNS/Name;
    .param p3, "type"    # I
    .param p4, "dclass"    # I
    .param p5, "iterations"    # I
    .param p6, "flags"    # I

    .prologue
    .line 261
    const/16 v28, 0x0

    .line 263
    .local v28, "rcode":B
    const/4 v4, 0x6

    move/from16 v0, p5

    if-le v0, v4, :cond_0

    .line 264
    const/4 v4, 0x0

    .line 343
    :goto_0
    return v4

    .line 266
    :cond_0
    const/16 v4, 0x18

    move/from16 v0, p3

    if-eq v0, v4, :cond_1

    const/16 v4, 0x2e

    move/from16 v0, p3

    if-ne v0, v4, :cond_2

    .line 267
    :cond_1
    const/16 p3, 0xff

    .line 268
    or-int/lit8 p6, p6, 0x2

    .line 271
    :cond_2
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/samsung/android/org/xbill/jnamed;->findBestZone(Lcom/samsung/android/org/xbill/DNS/Name;)Lcom/samsung/android/org/xbill/DNS/Zone;

    move-result-object v31

    .line 272
    .local v31, "zone":Lcom/samsung/android/org/xbill/DNS/Zone;
    if-eqz v31, :cond_6

    .line 273
    move-object/from16 v0, v31

    move-object/from16 v1, p2

    move/from16 v2, p3

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/org/xbill/DNS/Zone;->findRecords(Lcom/samsung/android/org/xbill/DNS/Name;I)Lcom/samsung/android/org/xbill/DNS/SetResponse;

    move-result-object v30

    .line 279
    .local v30, "sr":Lcom/samsung/android/org/xbill/DNS/SetResponse;
    :goto_1
    invoke-virtual/range {v30 .. v30}, Lcom/samsung/android/org/xbill/DNS/SetResponse;->isUnknown()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 280
    move-object/from16 v0, p0

    move/from16 v1, p4

    invoke-virtual {v0, v1}, Lcom/samsung/android/org/xbill/jnamed;->getCache(I)Lcom/samsung/android/org/xbill/DNS/Cache;

    move-result-object v4

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v4, v2}, Lcom/samsung/android/org/xbill/jnamed;->addCacheNS(Lcom/samsung/android/org/xbill/DNS/Message;Lcom/samsung/android/org/xbill/DNS/Cache;Lcom/samsung/android/org/xbill/DNS/Name;)V

    .line 282
    :cond_3
    invoke-virtual/range {v30 .. v30}, Lcom/samsung/android/org/xbill/DNS/SetResponse;->isNXDOMAIN()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 283
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/org/xbill/DNS/Message;->getHeader()Lcom/samsung/android/org/xbill/DNS/Header;

    move-result-object v4

    const/4 v5, 0x3

    invoke-virtual {v4, v5}, Lcom/samsung/android/org/xbill/DNS/Header;->setRcode(I)V

    .line 284
    if-eqz v31, :cond_4

    .line 285
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v31

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/org/xbill/jnamed;->addSOA(Lcom/samsung/android/org/xbill/DNS/Message;Lcom/samsung/android/org/xbill/DNS/Zone;)V

    .line 286
    if-nez p5, :cond_4

    .line 287
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/org/xbill/DNS/Message;->getHeader()Lcom/samsung/android/org/xbill/DNS/Header;

    move-result-object v4

    const/4 v5, 0x5

    invoke-virtual {v4, v5}, Lcom/samsung/android/org/xbill/DNS/Header;->setFlag(I)V

    .line 289
    :cond_4
    const/16 v28, 0x3

    :cond_5
    :goto_2
    move/from16 v4, v28

    .line 343
    goto :goto_0

    .line 275
    .end local v30    # "sr":Lcom/samsung/android/org/xbill/DNS/SetResponse;
    :cond_6
    move-object/from16 v0, p0

    move/from16 v1, p4

    invoke-virtual {v0, v1}, Lcom/samsung/android/org/xbill/jnamed;->getCache(I)Lcom/samsung/android/org/xbill/DNS/Cache;

    move-result-object v23

    .line 276
    .local v23, "cache":Lcom/samsung/android/org/xbill/DNS/Cache;
    const/4 v4, 0x3

    move-object/from16 v0, v23

    move-object/from16 v1, p2

    move/from16 v2, p3

    invoke-virtual {v0, v1, v2, v4}, Lcom/samsung/android/org/xbill/DNS/Cache;->lookupRecords(Lcom/samsung/android/org/xbill/DNS/Name;II)Lcom/samsung/android/org/xbill/DNS/SetResponse;

    move-result-object v30

    .restart local v30    # "sr":Lcom/samsung/android/org/xbill/DNS/SetResponse;
    goto :goto_1

    .line 291
    .end local v23    # "cache":Lcom/samsung/android/org/xbill/DNS/Cache;
    :cond_7
    invoke-virtual/range {v30 .. v30}, Lcom/samsung/android/org/xbill/DNS/SetResponse;->isNXRRSET()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 292
    if-eqz v31, :cond_5

    .line 293
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v31

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/org/xbill/jnamed;->addSOA(Lcom/samsung/android/org/xbill/DNS/Message;Lcom/samsung/android/org/xbill/DNS/Zone;)V

    .line 294
    if-nez p5, :cond_5

    .line 295
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/org/xbill/DNS/Message;->getHeader()Lcom/samsung/android/org/xbill/DNS/Header;

    move-result-object v4

    const/4 v5, 0x5

    invoke-virtual {v4, v5}, Lcom/samsung/android/org/xbill/DNS/Header;->setFlag(I)V

    goto :goto_2

    .line 298
    :cond_8
    invoke-virtual/range {v30 .. v30}, Lcom/samsung/android/org/xbill/DNS/SetResponse;->isDelegation()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 299
    invoke-virtual/range {v30 .. v30}, Lcom/samsung/android/org/xbill/DNS/SetResponse;->getNS()Lcom/samsung/android/org/xbill/DNS/RRset;

    move-result-object v7

    .line 300
    .local v7, "nsRecords":Lcom/samsung/android/org/xbill/DNS/RRset;
    invoke-virtual {v7}, Lcom/samsung/android/org/xbill/DNS/RRset;->getName()Lcom/samsung/android/org/xbill/DNS/Name;

    move-result-object v5

    .line 301
    const/4 v8, 0x2

    move-object/from16 v4, p0

    move-object/from16 v6, p1

    move/from16 v9, p6

    .line 300
    invoke-virtual/range {v4 .. v9}, Lcom/samsung/android/org/xbill/jnamed;->addRRset(Lcom/samsung/android/org/xbill/DNS/Name;Lcom/samsung/android/org/xbill/DNS/Message;Lcom/samsung/android/org/xbill/DNS/RRset;II)V

    goto :goto_2

    .line 303
    .end local v7    # "nsRecords":Lcom/samsung/android/org/xbill/DNS/RRset;
    :cond_9
    invoke-virtual/range {v30 .. v30}, Lcom/samsung/android/org/xbill/DNS/SetResponse;->isCNAME()Z

    move-result v4

    if-eqz v4, :cond_b

    .line 304
    invoke-virtual/range {v30 .. v30}, Lcom/samsung/android/org/xbill/DNS/SetResponse;->getCNAME()Lcom/samsung/android/org/xbill/DNS/CNAMERecord;

    move-result-object v24

    .line 305
    .local v24, "cname":Lcom/samsung/android/org/xbill/DNS/CNAMERecord;
    new-instance v11, Lcom/samsung/android/org/xbill/DNS/RRset;

    move-object/from16 v0, v24

    invoke-direct {v11, v0}, Lcom/samsung/android/org/xbill/DNS/RRset;-><init>(Lcom/samsung/android/org/xbill/DNS/Record;)V

    .line 306
    .local v11, "rrset":Lcom/samsung/android/org/xbill/DNS/RRset;
    const/4 v12, 0x1

    move-object/from16 v8, p0

    move-object/from16 v9, p2

    move-object/from16 v10, p1

    move/from16 v13, p6

    invoke-virtual/range {v8 .. v13}, Lcom/samsung/android/org/xbill/jnamed;->addRRset(Lcom/samsung/android/org/xbill/DNS/Name;Lcom/samsung/android/org/xbill/DNS/Message;Lcom/samsung/android/org/xbill/DNS/RRset;II)V

    .line 307
    if-eqz v31, :cond_a

    if-nez p5, :cond_a

    .line 308
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/org/xbill/DNS/Message;->getHeader()Lcom/samsung/android/org/xbill/DNS/Header;

    move-result-object v4

    const/4 v5, 0x5

    invoke-virtual {v4, v5}, Lcom/samsung/android/org/xbill/DNS/Header;->setFlag(I)V

    .line 309
    :cond_a
    invoke-virtual/range {v24 .. v24}, Lcom/samsung/android/org/xbill/DNS/CNAMERecord;->getTarget()Lcom/samsung/android/org/xbill/DNS/Name;

    move-result-object v14

    .line 310
    add-int/lit8 v17, p5, 0x1

    move-object/from16 v12, p0

    move-object/from16 v13, p1

    move/from16 v15, p3

    move/from16 v16, p4

    move/from16 v18, p6

    .line 309
    invoke-virtual/range {v12 .. v18}, Lcom/samsung/android/org/xbill/jnamed;->addAnswer(Lcom/samsung/android/org/xbill/DNS/Message;Lcom/samsung/android/org/xbill/DNS/Name;IIII)B

    move-result v28

    .line 311
    goto/16 :goto_2

    .line 312
    .end local v11    # "rrset":Lcom/samsung/android/org/xbill/DNS/RRset;
    .end local v24    # "cname":Lcom/samsung/android/org/xbill/DNS/CNAMERecord;
    :cond_b
    invoke-virtual/range {v30 .. v30}, Lcom/samsung/android/org/xbill/DNS/SetResponse;->isDNAME()Z

    move-result v4

    if-eqz v4, :cond_d

    .line 313
    invoke-virtual/range {v30 .. v30}, Lcom/samsung/android/org/xbill/DNS/SetResponse;->getDNAME()Lcom/samsung/android/org/xbill/DNS/DNAMERecord;

    move-result-object v25

    .line 314
    .local v25, "dname":Lcom/samsung/android/org/xbill/DNS/DNAMERecord;
    new-instance v11, Lcom/samsung/android/org/xbill/DNS/RRset;

    move-object/from16 v0, v25

    invoke-direct {v11, v0}, Lcom/samsung/android/org/xbill/DNS/RRset;-><init>(Lcom/samsung/android/org/xbill/DNS/Record;)V

    .line 315
    .restart local v11    # "rrset":Lcom/samsung/android/org/xbill/DNS/RRset;
    const/4 v12, 0x1

    move-object/from16 v8, p0

    move-object/from16 v9, p2

    move-object/from16 v10, p1

    move/from16 v13, p6

    invoke-virtual/range {v8 .. v13}, Lcom/samsung/android/org/xbill/jnamed;->addRRset(Lcom/samsung/android/org/xbill/DNS/Name;Lcom/samsung/android/org/xbill/DNS/Message;Lcom/samsung/android/org/xbill/DNS/RRset;II)V

    .line 318
    :try_start_0
    move-object/from16 v0, p2

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/samsung/android/org/xbill/DNS/Name;->fromDNAME(Lcom/samsung/android/org/xbill/DNS/DNAMERecord;)Lcom/samsung/android/org/xbill/DNS/Name;
    :try_end_0
    .catch Lcom/samsung/android/org/xbill/DNS/NameTooLongException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v18

    .line 323
    .local v18, "newname":Lcom/samsung/android/org/xbill/DNS/Name;
    new-instance v11, Lcom/samsung/android/org/xbill/DNS/RRset;

    .end local v11    # "rrset":Lcom/samsung/android/org/xbill/DNS/RRset;
    new-instance v13, Lcom/samsung/android/org/xbill/DNS/CNAMERecord;

    const-wide/16 v16, 0x0

    move-object/from16 v14, p2

    move/from16 v15, p4

    invoke-direct/range {v13 .. v18}, Lcom/samsung/android/org/xbill/DNS/CNAMERecord;-><init>(Lcom/samsung/android/org/xbill/DNS/Name;IJLcom/samsung/android/org/xbill/DNS/Name;)V

    invoke-direct {v11, v13}, Lcom/samsung/android/org/xbill/DNS/RRset;-><init>(Lcom/samsung/android/org/xbill/DNS/Record;)V

    .line 324
    .restart local v11    # "rrset":Lcom/samsung/android/org/xbill/DNS/RRset;
    const/4 v12, 0x1

    move-object/from16 v8, p0

    move-object/from16 v9, p2

    move-object/from16 v10, p1

    move/from16 v13, p6

    invoke-virtual/range {v8 .. v13}, Lcom/samsung/android/org/xbill/jnamed;->addRRset(Lcom/samsung/android/org/xbill/DNS/Name;Lcom/samsung/android/org/xbill/DNS/Message;Lcom/samsung/android/org/xbill/DNS/RRset;II)V

    .line 325
    if-eqz v31, :cond_c

    if-nez p5, :cond_c

    .line 326
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/org/xbill/DNS/Message;->getHeader()Lcom/samsung/android/org/xbill/DNS/Header;

    move-result-object v4

    const/4 v5, 0x5

    invoke-virtual {v4, v5}, Lcom/samsung/android/org/xbill/DNS/Header;->setFlag(I)V

    .line 328
    :cond_c
    add-int/lit8 v21, p5, 0x1

    move-object/from16 v16, p0

    move-object/from16 v17, p1

    move/from16 v19, p3

    move/from16 v20, p4

    move/from16 v22, p6

    .line 327
    invoke-virtual/range {v16 .. v22}, Lcom/samsung/android/org/xbill/jnamed;->addAnswer(Lcom/samsung/android/org/xbill/DNS/Message;Lcom/samsung/android/org/xbill/DNS/Name;IIII)B

    move-result v28

    .line 329
    goto/16 :goto_2

    .line 320
    .end local v18    # "newname":Lcom/samsung/android/org/xbill/DNS/Name;
    :catch_0
    move-exception v26

    .line 321
    .local v26, "e":Lcom/samsung/android/org/xbill/DNS/NameTooLongException;
    const/4 v4, 0x6

    goto/16 :goto_0

    .line 330
    .end local v11    # "rrset":Lcom/samsung/android/org/xbill/DNS/RRset;
    .end local v25    # "dname":Lcom/samsung/android/org/xbill/DNS/DNAMERecord;
    .end local v26    # "e":Lcom/samsung/android/org/xbill/DNS/NameTooLongException;
    :cond_d
    invoke-virtual/range {v30 .. v30}, Lcom/samsung/android/org/xbill/DNS/SetResponse;->isSuccessful()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 331
    invoke-virtual/range {v30 .. v30}, Lcom/samsung/android/org/xbill/DNS/SetResponse;->answers()[Lcom/samsung/android/org/xbill/DNS/RRset;

    move-result-object v29

    .line 332
    .local v29, "rrsets":[Lcom/samsung/android/org/xbill/DNS/RRset;
    const/16 v27, 0x0

    .local v27, "i":I
    :goto_3
    move-object/from16 v0, v29

    array-length v4, v0

    move/from16 v0, v27

    if-lt v0, v4, :cond_e

    .line 335
    if-eqz v31, :cond_f

    .line 336
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v31

    move/from16 v3, p6

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/org/xbill/jnamed;->addNS(Lcom/samsung/android/org/xbill/DNS/Message;Lcom/samsung/android/org/xbill/DNS/Zone;I)V

    .line 337
    if-nez p5, :cond_5

    .line 338
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/org/xbill/DNS/Message;->getHeader()Lcom/samsung/android/org/xbill/DNS/Header;

    move-result-object v4

    const/4 v5, 0x5

    invoke-virtual {v4, v5}, Lcom/samsung/android/org/xbill/DNS/Header;->setFlag(I)V

    goto/16 :goto_2

    .line 333
    :cond_e
    aget-object v15, v29, v27

    .line 334
    const/16 v16, 0x1

    move-object/from16 v12, p0

    move-object/from16 v13, p2

    move-object/from16 v14, p1

    move/from16 v17, p6

    .line 333
    invoke-virtual/range {v12 .. v17}, Lcom/samsung/android/org/xbill/jnamed;->addRRset(Lcom/samsung/android/org/xbill/DNS/Name;Lcom/samsung/android/org/xbill/DNS/Message;Lcom/samsung/android/org/xbill/DNS/RRset;II)V

    .line 332
    add-int/lit8 v27, v27, 0x1

    goto :goto_3

    .line 341
    :cond_f
    move-object/from16 v0, p0

    move/from16 v1, p4

    invoke-virtual {v0, v1}, Lcom/samsung/android/org/xbill/jnamed;->getCache(I)Lcom/samsung/android/org/xbill/DNS/Cache;

    move-result-object v4

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v4, v2}, Lcom/samsung/android/org/xbill/jnamed;->addCacheNS(Lcom/samsung/android/org/xbill/DNS/Message;Lcom/samsung/android/org/xbill/DNS/Cache;Lcom/samsung/android/org/xbill/DNS/Name;)V

    goto/16 :goto_2
.end method

.method public addPrimaryZone(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "zname"    # Ljava/lang/String;
    .param p2, "zonefile"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 114
    const/4 v1, 0x0

    .line 115
    .local v1, "origin":Lcom/samsung/android/org/xbill/DNS/Name;
    if-eqz p1, :cond_0

    .line 116
    sget-object v2, Lcom/samsung/android/org/xbill/DNS/Name;->root:Lcom/samsung/android/org/xbill/DNS/Name;

    invoke-static {p1, v2}, Lcom/samsung/android/org/xbill/DNS/Name;->fromString(Ljava/lang/String;Lcom/samsung/android/org/xbill/DNS/Name;)Lcom/samsung/android/org/xbill/DNS/Name;

    move-result-object v1

    .line 117
    :cond_0
    new-instance v0, Lcom/samsung/android/org/xbill/DNS/Zone;

    invoke-direct {v0, v1, p2}, Lcom/samsung/android/org/xbill/DNS/Zone;-><init>(Lcom/samsung/android/org/xbill/DNS/Name;Ljava/lang/String;)V

    .line 118
    .local v0, "newzone":Lcom/samsung/android/org/xbill/DNS/Zone;
    iget-object v2, p0, Lcom/samsung/android/org/xbill/jnamed;->znames:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/samsung/android/org/xbill/DNS/Zone;->getOrigin()Lcom/samsung/android/org/xbill/DNS/Name;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 119
    return-void
.end method

.method addRRset(Lcom/samsung/android/org/xbill/DNS/Name;Lcom/samsung/android/org/xbill/DNS/Message;Lcom/samsung/android/org/xbill/DNS/RRset;II)V
    .locals 4
    .param p1, "name"    # Lcom/samsung/android/org/xbill/DNS/Name;
    .param p2, "response"    # Lcom/samsung/android/org/xbill/DNS/Message;
    .param p3, "rrset"    # Lcom/samsung/android/org/xbill/DNS/RRset;
    .param p4, "section"    # I
    .param p5, "flags"    # I

    .prologue
    .line 183
    const/4 v2, 0x1

    .local v2, "s":I
    :goto_0
    if-le v2, p4, :cond_2

    .line 186
    and-int/lit8 v3, p5, 0x2

    if-nez v3, :cond_0

    .line 187
    invoke-virtual {p3}, Lcom/samsung/android/org/xbill/DNS/RRset;->rrs()Ljava/util/Iterator;

    move-result-object v0

    .line 188
    .local v0, "it":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_3

    .line 195
    .end local v0    # "it":Ljava/util/Iterator;
    :cond_0
    and-int/lit8 v3, p5, 0x3

    if-eqz v3, :cond_1

    .line 196
    invoke-virtual {p3}, Lcom/samsung/android/org/xbill/DNS/RRset;->sigs()Ljava/util/Iterator;

    move-result-object v0

    .line 197
    .restart local v0    # "it":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_5

    .line 204
    .end local v0    # "it":Ljava/util/Iterator;
    :cond_1
    return-void

    .line 184
    :cond_2
    invoke-virtual {p3}, Lcom/samsung/android/org/xbill/DNS/RRset;->getType()I

    move-result v3

    invoke-virtual {p2, p1, v3, v2}, Lcom/samsung/android/org/xbill/DNS/Message;->findRRset(Lcom/samsung/android/org/xbill/DNS/Name;II)Z

    move-result v3

    if-nez v3, :cond_1

    .line 183
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 189
    .restart local v0    # "it":Ljava/util/Iterator;
    :cond_3
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/org/xbill/DNS/Record;

    .line 190
    .local v1, "r":Lcom/samsung/android/org/xbill/DNS/Record;
    invoke-virtual {v1}, Lcom/samsung/android/org/xbill/DNS/Record;->getName()Lcom/samsung/android/org/xbill/DNS/Name;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/org/xbill/DNS/Name;->isWild()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/Name;->isWild()Z

    move-result v3

    if-nez v3, :cond_4

    .line 191
    invoke-virtual {v1, p1}, Lcom/samsung/android/org/xbill/DNS/Record;->withName(Lcom/samsung/android/org/xbill/DNS/Name;)Lcom/samsung/android/org/xbill/DNS/Record;

    move-result-object v1

    .line 192
    :cond_4
    invoke-virtual {p2, v1, p4}, Lcom/samsung/android/org/xbill/DNS/Message;->addRecord(Lcom/samsung/android/org/xbill/DNS/Record;I)V

    goto :goto_1

    .line 198
    .end local v1    # "r":Lcom/samsung/android/org/xbill/DNS/Record;
    :cond_5
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/org/xbill/DNS/Record;

    .line 199
    .restart local v1    # "r":Lcom/samsung/android/org/xbill/DNS/Record;
    invoke-virtual {v1}, Lcom/samsung/android/org/xbill/DNS/Record;->getName()Lcom/samsung/android/org/xbill/DNS/Name;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/org/xbill/DNS/Name;->isWild()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/Name;->isWild()Z

    move-result v3

    if-nez v3, :cond_6

    .line 200
    invoke-virtual {v1, p1}, Lcom/samsung/android/org/xbill/DNS/Record;->withName(Lcom/samsung/android/org/xbill/DNS/Name;)Lcom/samsung/android/org/xbill/DNS/Record;

    move-result-object v1

    .line 201
    :cond_6
    invoke-virtual {p2, v1, p4}, Lcom/samsung/android/org/xbill/DNS/Message;->addRecord(Lcom/samsung/android/org/xbill/DNS/Record;I)V

    goto :goto_2
.end method

.method public addSecondaryZone(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "zone"    # Ljava/lang/String;
    .param p2, "remote"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/samsung/android/org/xbill/DNS/ZoneTransferException;
        }
    .end annotation

    .prologue
    .line 125
    sget-object v2, Lcom/samsung/android/org/xbill/DNS/Name;->root:Lcom/samsung/android/org/xbill/DNS/Name;

    invoke-static {p1, v2}, Lcom/samsung/android/org/xbill/DNS/Name;->fromString(Ljava/lang/String;Lcom/samsung/android/org/xbill/DNS/Name;)Lcom/samsung/android/org/xbill/DNS/Name;

    move-result-object v1

    .line 126
    .local v1, "zname":Lcom/samsung/android/org/xbill/DNS/Name;
    new-instance v0, Lcom/samsung/android/org/xbill/DNS/Zone;

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2, p2}, Lcom/samsung/android/org/xbill/DNS/Zone;-><init>(Lcom/samsung/android/org/xbill/DNS/Name;ILjava/lang/String;)V

    .line 127
    .local v0, "newzone":Lcom/samsung/android/org/xbill/DNS/Zone;
    iget-object v2, p0, Lcom/samsung/android/org/xbill/jnamed;->znames:Ljava/util/Map;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    return-void
.end method

.method public addTCP(Ljava/net/InetAddress;I)V
    .locals 2
    .param p1, "addr"    # Ljava/net/InetAddress;
    .param p2, "port"    # I

    .prologue
    .line 607
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/android/org/xbill/jnamed$2;

    invoke-direct {v1, p0, p1, p2}, Lcom/samsung/android/org/xbill/jnamed$2;-><init>(Lcom/samsung/android/org/xbill/jnamed;Ljava/net/InetAddress;I)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 609
    .local v0, "t":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 610
    return-void
.end method

.method public addTSIG(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "algstr"    # Ljava/lang/String;
    .param p2, "namestr"    # Ljava/lang/String;
    .param p3, "key"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 132
    sget-object v1, Lcom/samsung/android/org/xbill/DNS/Name;->root:Lcom/samsung/android/org/xbill/DNS/Name;

    invoke-static {p2, v1}, Lcom/samsung/android/org/xbill/DNS/Name;->fromString(Ljava/lang/String;Lcom/samsung/android/org/xbill/DNS/Name;)Lcom/samsung/android/org/xbill/DNS/Name;

    move-result-object v0

    .line 133
    .local v0, "name":Lcom/samsung/android/org/xbill/DNS/Name;
    iget-object v1, p0, Lcom/samsung/android/org/xbill/jnamed;->TSIGs:Ljava/util/Map;

    new-instance v2, Lcom/samsung/android/org/xbill/DNS/TSIG;

    invoke-direct {v2, p1, p2, p3}, Lcom/samsung/android/org/xbill/DNS/TSIG;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 134
    return-void
.end method

.method public addUDP(Ljava/net/InetAddress;I)V
    .locals 2
    .param p1, "addr"    # Ljava/net/InetAddress;
    .param p2, "port"    # I

    .prologue
    .line 615
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/android/org/xbill/jnamed$3;

    invoke-direct {v1, p0, p1, p2}, Lcom/samsung/android/org/xbill/jnamed$3;-><init>(Lcom/samsung/android/org/xbill/jnamed;Ljava/net/InetAddress;I)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 617
    .local v0, "t":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 618
    return-void
.end method

.method buildErrorMessage(Lcom/samsung/android/org/xbill/DNS/Header;ILcom/samsung/android/org/xbill/DNS/Record;)[B
    .locals 3
    .param p1, "header"    # Lcom/samsung/android/org/xbill/DNS/Header;
    .param p2, "rcode"    # I
    .param p3, "question"    # Lcom/samsung/android/org/xbill/DNS/Record;

    .prologue
    .line 466
    new-instance v1, Lcom/samsung/android/org/xbill/DNS/Message;

    invoke-direct {v1}, Lcom/samsung/android/org/xbill/DNS/Message;-><init>()V

    .line 467
    .local v1, "response":Lcom/samsung/android/org/xbill/DNS/Message;
    invoke-virtual {v1, p1}, Lcom/samsung/android/org/xbill/DNS/Message;->setHeader(Lcom/samsung/android/org/xbill/DNS/Header;)V

    .line 468
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v2, 0x4

    if-lt v0, v2, :cond_1

    .line 470
    const/4 v2, 0x2

    if-ne p2, v2, :cond_0

    .line 471
    const/4 v2, 0x0

    invoke-virtual {v1, p3, v2}, Lcom/samsung/android/org/xbill/DNS/Message;->addRecord(Lcom/samsung/android/org/xbill/DNS/Record;I)V

    .line 472
    :cond_0
    invoke-virtual {p1, p2}, Lcom/samsung/android/org/xbill/DNS/Header;->setRcode(I)V

    .line 473
    invoke-virtual {v1}, Lcom/samsung/android/org/xbill/DNS/Message;->toWire()[B

    move-result-object v2

    return-object v2

    .line 469
    :cond_1
    invoke-virtual {v1, v0}, Lcom/samsung/android/org/xbill/DNS/Message;->removeAllRecords(I)V

    .line 468
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method doAXFR(Lcom/samsung/android/org/xbill/DNS/Name;Lcom/samsung/android/org/xbill/DNS/Message;Lcom/samsung/android/org/xbill/DNS/TSIG;Lcom/samsung/android/org/xbill/DNS/TSIGRecord;Ljava/net/Socket;)[B
    .locals 16
    .param p1, "name"    # Lcom/samsung/android/org/xbill/DNS/Name;
    .param p2, "query"    # Lcom/samsung/android/org/xbill/DNS/Message;
    .param p3, "tsig"    # Lcom/samsung/android/org/xbill/DNS/TSIG;
    .param p4, "qtsig"    # Lcom/samsung/android/org/xbill/DNS/TSIGRecord;
    .param p5, "s"    # Ljava/net/Socket;

    .prologue
    .line 348
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/org/xbill/jnamed;->znames:Ljava/util/Map;

    move-object/from16 v0, p1

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/samsung/android/org/xbill/DNS/Zone;

    .line 349
    .local v15, "zone":Lcom/samsung/android/org/xbill/DNS/Zone;
    const/4 v10, 0x1

    .line 350
    .local v10, "first":Z
    if-nez v15, :cond_0

    .line 351
    const/4 v2, 0x5

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/org/xbill/jnamed;->errorMessage(Lcom/samsung/android/org/xbill/DNS/Message;I)[B

    move-result-object v2

    .line 383
    :goto_0
    return-object v2

    .line 352
    :cond_0
    invoke-virtual {v15}, Lcom/samsung/android/org/xbill/DNS/Zone;->AXFR()Ljava/util/Iterator;

    move-result-object v13

    .line 355
    .local v13, "it":Ljava/util/Iterator;
    :try_start_0
    new-instance v8, Ljava/io/DataOutputStream;

    invoke-virtual/range {p5 .. p5}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v2

    invoke-direct {v8, v2}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 356
    .local v8, "dataOut":Ljava/io/DataOutputStream;
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/org/xbill/DNS/Message;->getHeader()Lcom/samsung/android/org/xbill/DNS/Header;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/org/xbill/DNS/Header;->getID()I

    move-result v12

    .line 357
    .local v12, "id":I
    :goto_1
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-nez v2, :cond_1

    .line 379
    .end local v8    # "dataOut":Ljava/io/DataOutputStream;
    .end local v12    # "id":I
    :goto_2
    :try_start_1
    invoke-virtual/range {p5 .. p5}, Ljava/net/Socket;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 383
    :goto_3
    const/4 v2, 0x0

    goto :goto_0

    .line 358
    .restart local v8    # "dataOut":Ljava/io/DataOutputStream;
    .restart local v12    # "id":I
    :cond_1
    :try_start_2
    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/org/xbill/DNS/RRset;

    .line 359
    .local v5, "rrset":Lcom/samsung/android/org/xbill/DNS/RRset;
    new-instance v4, Lcom/samsung/android/org/xbill/DNS/Message;

    invoke-direct {v4, v12}, Lcom/samsung/android/org/xbill/DNS/Message;-><init>(I)V

    .line 360
    .local v4, "response":Lcom/samsung/android/org/xbill/DNS/Message;
    invoke-virtual {v4}, Lcom/samsung/android/org/xbill/DNS/Message;->getHeader()Lcom/samsung/android/org/xbill/DNS/Header;

    move-result-object v11

    .line 361
    .local v11, "header":Lcom/samsung/android/org/xbill/DNS/Header;
    const/4 v2, 0x0

    invoke-virtual {v11, v2}, Lcom/samsung/android/org/xbill/DNS/Header;->setFlag(I)V

    .line 362
    const/4 v2, 0x5

    invoke-virtual {v11, v2}, Lcom/samsung/android/org/xbill/DNS/Header;->setFlag(I)V

    .line 363
    invoke-virtual {v5}, Lcom/samsung/android/org/xbill/DNS/RRset;->getName()Lcom/samsung/android/org/xbill/DNS/Name;

    move-result-object v3

    .line 364
    const/4 v6, 0x1

    const/4 v7, 0x1

    move-object/from16 v2, p0

    .line 363
    invoke-virtual/range {v2 .. v7}, Lcom/samsung/android/org/xbill/jnamed;->addRRset(Lcom/samsung/android/org/xbill/DNS/Name;Lcom/samsung/android/org/xbill/DNS/Message;Lcom/samsung/android/org/xbill/DNS/RRset;II)V

    .line 365
    if-eqz p3, :cond_2

    .line 366
    move-object/from16 v0, p3

    move-object/from16 v1, p4

    invoke-virtual {v0, v4, v1, v10}, Lcom/samsung/android/org/xbill/DNS/TSIG;->applyStream(Lcom/samsung/android/org/xbill/DNS/Message;Lcom/samsung/android/org/xbill/DNS/TSIGRecord;Z)V

    .line 367
    invoke-virtual {v4}, Lcom/samsung/android/org/xbill/DNS/Message;->getTSIG()Lcom/samsung/android/org/xbill/DNS/TSIGRecord;

    move-result-object p4

    .line 369
    :cond_2
    const/4 v10, 0x0

    .line 370
    invoke-virtual {v4}, Lcom/samsung/android/org/xbill/DNS/Message;->toWire()[B

    move-result-object v14

    .line 371
    .local v14, "out":[B
    array-length v2, v14

    invoke-virtual {v8, v2}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 372
    invoke-virtual {v8, v14}, Ljava/io/DataOutputStream;->write([B)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .line 375
    .end local v4    # "response":Lcom/samsung/android/org/xbill/DNS/Message;
    .end local v5    # "rrset":Lcom/samsung/android/org/xbill/DNS/RRset;
    .end local v8    # "dataOut":Ljava/io/DataOutputStream;
    .end local v11    # "header":Lcom/samsung/android/org/xbill/DNS/Header;
    .end local v12    # "id":I
    .end local v14    # "out":[B
    :catch_0
    move-exception v9

    .line 376
    .local v9, "ex":Ljava/io/IOException;
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "AXFR failed"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_2

    .line 381
    .end local v9    # "ex":Ljava/io/IOException;
    :catch_1
    move-exception v2

    goto :goto_3
.end method

.method public errorMessage(Lcom/samsung/android/org/xbill/DNS/Message;I)[B
    .locals 2
    .param p1, "query"    # Lcom/samsung/android/org/xbill/DNS/Message;
    .param p2, "rcode"    # I

    .prologue
    .line 490
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/Message;->getHeader()Lcom/samsung/android/org/xbill/DNS/Header;

    move-result-object v0

    .line 491
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/Message;->getQuestion()Lcom/samsung/android/org/xbill/DNS/Record;

    move-result-object v1

    .line 490
    invoke-virtual {p0, v0, p2, v1}, Lcom/samsung/android/org/xbill/jnamed;->buildErrorMessage(Lcom/samsung/android/org/xbill/DNS/Header;ILcom/samsung/android/org/xbill/DNS/Record;)[B

    move-result-object v0

    return-object v0
.end method

.method public findBestZone(Lcom/samsung/android/org/xbill/DNS/Name;)Lcom/samsung/android/org/xbill/DNS/Zone;
    .locals 5
    .param p1, "name"    # Lcom/samsung/android/org/xbill/DNS/Name;

    .prologue
    .line 148
    const/4 v0, 0x0

    .line 149
    .local v0, "foundzone":Lcom/samsung/android/org/xbill/DNS/Zone;
    iget-object v4, p0, Lcom/samsung/android/org/xbill/jnamed;->znames:Ljava/util/Map;

    invoke-interface {v4, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "foundzone":Lcom/samsung/android/org/xbill/DNS/Zone;
    check-cast v0, Lcom/samsung/android/org/xbill/DNS/Zone;

    .line 150
    .restart local v0    # "foundzone":Lcom/samsung/android/org/xbill/DNS/Zone;
    if-eqz v0, :cond_0

    move-object v4, v0

    .line 159
    :goto_0
    return-object v4

    .line 152
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/Name;->labels()I

    move-result v2

    .line 153
    .local v2, "labels":I
    const/4 v1, 0x1

    .local v1, "i":I
    :goto_1
    if-lt v1, v2, :cond_1

    .line 159
    const/4 v4, 0x0

    goto :goto_0

    .line 154
    :cond_1
    new-instance v3, Lcom/samsung/android/org/xbill/DNS/Name;

    invoke-direct {v3, p1, v1}, Lcom/samsung/android/org/xbill/DNS/Name;-><init>(Lcom/samsung/android/org/xbill/DNS/Name;I)V

    .line 155
    .local v3, "tname":Lcom/samsung/android/org/xbill/DNS/Name;
    iget-object v4, p0, Lcom/samsung/android/org/xbill/jnamed;->znames:Ljava/util/Map;

    invoke-interface {v4, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "foundzone":Lcom/samsung/android/org/xbill/DNS/Zone;
    check-cast v0, Lcom/samsung/android/org/xbill/DNS/Zone;

    .line 156
    .restart local v0    # "foundzone":Lcom/samsung/android/org/xbill/DNS/Zone;
    if-eqz v0, :cond_2

    move-object v4, v0

    .line 157
    goto :goto_0

    .line 153
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public findExactMatch(Lcom/samsung/android/org/xbill/DNS/Name;IIZ)Lcom/samsung/android/org/xbill/DNS/RRset;
    .locals 4
    .param p1, "name"    # Lcom/samsung/android/org/xbill/DNS/Name;
    .param p2, "type"    # I
    .param p3, "dclass"    # I
    .param p4, "glue"    # Z

    .prologue
    .line 164
    invoke-virtual {p0, p1}, Lcom/samsung/android/org/xbill/jnamed;->findBestZone(Lcom/samsung/android/org/xbill/DNS/Name;)Lcom/samsung/android/org/xbill/DNS/Zone;

    move-result-object v2

    .line 165
    .local v2, "zone":Lcom/samsung/android/org/xbill/DNS/Zone;
    if-eqz v2, :cond_0

    .line 166
    invoke-virtual {v2, p1, p2}, Lcom/samsung/android/org/xbill/DNS/Zone;->findExactMatch(Lcom/samsung/android/org/xbill/DNS/Name;I)Lcom/samsung/android/org/xbill/DNS/RRset;

    move-result-object v3

    .line 177
    :goto_0
    return-object v3

    .line 169
    :cond_0
    invoke-virtual {p0, p3}, Lcom/samsung/android/org/xbill/jnamed;->getCache(I)Lcom/samsung/android/org/xbill/DNS/Cache;

    move-result-object v0

    .line 170
    .local v0, "cache":Lcom/samsung/android/org/xbill/DNS/Cache;
    if-eqz p4, :cond_1

    .line 171
    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/org/xbill/DNS/Cache;->findAnyRecords(Lcom/samsung/android/org/xbill/DNS/Name;I)[Lcom/samsung/android/org/xbill/DNS/RRset;

    move-result-object v1

    .line 174
    .local v1, "rrsets":[Lcom/samsung/android/org/xbill/DNS/RRset;
    :goto_1
    if-nez v1, :cond_2

    .line 175
    const/4 v3, 0x0

    goto :goto_0

    .line 173
    .end local v1    # "rrsets":[Lcom/samsung/android/org/xbill/DNS/RRset;
    :cond_1
    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/org/xbill/DNS/Cache;->findRecords(Lcom/samsung/android/org/xbill/DNS/Name;I)[Lcom/samsung/android/org/xbill/DNS/RRset;

    move-result-object v1

    .restart local v1    # "rrsets":[Lcom/samsung/android/org/xbill/DNS/RRset;
    goto :goto_1

    .line 177
    :cond_2
    const/4 v3, 0x0

    aget-object v3, v1, v3

    goto :goto_0
.end method

.method public formerrMessage([B)[B
    .locals 4
    .param p1, "in"    # [B

    .prologue
    const/4 v2, 0x0

    .line 480
    :try_start_0
    new-instance v1, Lcom/samsung/android/org/xbill/DNS/Header;

    invoke-direct {v1, p1}, Lcom/samsung/android/org/xbill/DNS/Header;-><init>([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 485
    .local v1, "header":Lcom/samsung/android/org/xbill/DNS/Header;
    const/4 v3, 0x1

    invoke-virtual {p0, v1, v3, v2}, Lcom/samsung/android/org/xbill/jnamed;->buildErrorMessage(Lcom/samsung/android/org/xbill/DNS/Header;ILcom/samsung/android/org/xbill/DNS/Record;)[B

    move-result-object v2

    .end local v1    # "header":Lcom/samsung/android/org/xbill/DNS/Header;
    :goto_0
    return-object v2

    .line 482
    :catch_0
    move-exception v0

    .line 483
    .local v0, "e":Ljava/io/IOException;
    goto :goto_0
.end method

.method generateReply(Lcom/samsung/android/org/xbill/DNS/Message;[BILjava/net/Socket;)[B
    .locals 22
    .param p1, "query"    # Lcom/samsung/android/org/xbill/DNS/Message;
    .param p2, "in"    # [B
    .param p3, "length"    # I
    .param p4, "s"    # Ljava/net/Socket;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 398
    const/4 v14, 0x0

    .line 400
    .local v14, "flags":I
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/org/xbill/DNS/Message;->getHeader()Lcom/samsung/android/org/xbill/DNS/Header;

    move-result-object v15

    .line 401
    .local v15, "header":Lcom/samsung/android/org/xbill/DNS/Header;
    const/4 v3, 0x0

    invoke-virtual {v15, v3}, Lcom/samsung/android/org/xbill/DNS/Header;->getFlag(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 402
    const/4 v3, 0x0

    .line 461
    :goto_0
    return-object v3

    .line 403
    :cond_0
    invoke-virtual {v15}, Lcom/samsung/android/org/xbill/DNS/Header;->getRcode()I

    move-result v3

    if-eqz v3, :cond_1

    .line 404
    const/4 v3, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v3}, Lcom/samsung/android/org/xbill/jnamed;->errorMessage(Lcom/samsung/android/org/xbill/DNS/Message;I)[B

    move-result-object v3

    goto :goto_0

    .line 405
    :cond_1
    invoke-virtual {v15}, Lcom/samsung/android/org/xbill/DNS/Header;->getOpcode()I

    move-result v3

    if-eqz v3, :cond_2

    .line 406
    const/4 v3, 0x4

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v3}, Lcom/samsung/android/org/xbill/jnamed;->errorMessage(Lcom/samsung/android/org/xbill/DNS/Message;I)[B

    move-result-object v3

    goto :goto_0

    .line 408
    :cond_2
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/org/xbill/DNS/Message;->getQuestion()Lcom/samsung/android/org/xbill/DNS/Record;

    move-result-object v20

    .line 410
    .local v20, "queryRecord":Lcom/samsung/android/org/xbill/DNS/Record;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/org/xbill/DNS/Message;->getTSIG()Lcom/samsung/android/org/xbill/DNS/TSIGRecord;

    move-result-object v7

    .line 411
    .local v7, "queryTSIG":Lcom/samsung/android/org/xbill/DNS/TSIGRecord;
    const/4 v6, 0x0

    .line 412
    .local v6, "tsig":Lcom/samsung/android/org/xbill/DNS/TSIG;
    if-eqz v7, :cond_4

    .line 413
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/org/xbill/jnamed;->TSIGs:Ljava/util/Map;

    invoke-virtual {v7}, Lcom/samsung/android/org/xbill/DNS/TSIGRecord;->getName()Lcom/samsung/android/org/xbill/DNS/Name;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "tsig":Lcom/samsung/android/org/xbill/DNS/TSIG;
    check-cast v6, Lcom/samsung/android/org/xbill/DNS/TSIG;

    .line 414
    .restart local v6    # "tsig":Lcom/samsung/android/org/xbill/DNS/TSIG;
    if-eqz v6, :cond_3

    .line 415
    const/4 v3, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move/from16 v2, p3

    invoke-virtual {v6, v0, v1, v2, v3}, Lcom/samsung/android/org/xbill/DNS/TSIG;->verify(Lcom/samsung/android/org/xbill/DNS/Message;[BILcom/samsung/android/org/xbill/DNS/TSIGRecord;)B

    move-result v3

    if-eqz v3, :cond_4

    .line 416
    :cond_3
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/samsung/android/org/xbill/jnamed;->formerrMessage([B)[B

    move-result-object v3

    goto :goto_0

    .line 419
    :cond_4
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/org/xbill/DNS/Message;->getOPT()Lcom/samsung/android/org/xbill/DNS/OPTRecord;

    move-result-object v19

    .line 420
    .local v19, "queryOPT":Lcom/samsung/android/org/xbill/DNS/OPTRecord;
    if-eqz v19, :cond_5

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/org/xbill/DNS/OPTRecord;->getVersion()I

    move-result v3

    if-lez v3, :cond_5

    .line 423
    :cond_5
    if-eqz p4, :cond_8

    .line 424
    const v16, 0xffff

    .line 430
    .local v16, "maxLength":I
    :goto_1
    if-eqz v19, :cond_6

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/org/xbill/DNS/OPTRecord;->getFlags()I

    move-result v3

    const v5, 0x8000

    and-int/2addr v3, v5

    if-eqz v3, :cond_6

    .line 431
    const/4 v14, 0x1

    .line 433
    :cond_6
    new-instance v9, Lcom/samsung/android/org/xbill/DNS/Message;

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/org/xbill/DNS/Message;->getHeader()Lcom/samsung/android/org/xbill/DNS/Header;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/org/xbill/DNS/Header;->getID()I

    move-result v3

    invoke-direct {v9, v3}, Lcom/samsung/android/org/xbill/DNS/Message;-><init>(I)V

    .line 434
    .local v9, "response":Lcom/samsung/android/org/xbill/DNS/Message;
    invoke-virtual {v9}, Lcom/samsung/android/org/xbill/DNS/Message;->getHeader()Lcom/samsung/android/org/xbill/DNS/Header;

    move-result-object v3

    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Lcom/samsung/android/org/xbill/DNS/Header;->setFlag(I)V

    .line 435
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/org/xbill/DNS/Message;->getHeader()Lcom/samsung/android/org/xbill/DNS/Header;

    move-result-object v3

    const/4 v5, 0x7

    invoke-virtual {v3, v5}, Lcom/samsung/android/org/xbill/DNS/Header;->getFlag(I)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 436
    invoke-virtual {v9}, Lcom/samsung/android/org/xbill/DNS/Message;->getHeader()Lcom/samsung/android/org/xbill/DNS/Header;

    move-result-object v3

    const/4 v5, 0x7

    invoke-virtual {v3, v5}, Lcom/samsung/android/org/xbill/DNS/Header;->setFlag(I)V

    .line 437
    :cond_7
    const/4 v3, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v9, v0, v3}, Lcom/samsung/android/org/xbill/DNS/Message;->addRecord(Lcom/samsung/android/org/xbill/DNS/Record;I)V

    .line 439
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/org/xbill/DNS/Record;->getName()Lcom/samsung/android/org/xbill/DNS/Name;

    move-result-object v4

    .line 440
    .local v4, "name":Lcom/samsung/android/org/xbill/DNS/Name;
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/org/xbill/DNS/Record;->getType()I

    move-result v11

    .line 441
    .local v11, "type":I
    invoke-virtual/range {v20 .. v20}, Lcom/samsung/android/org/xbill/DNS/Record;->getDClass()I

    move-result v12

    .line 442
    .local v12, "dclass":I
    const/16 v3, 0xfc

    if-ne v11, v3, :cond_a

    if-eqz p4, :cond_a

    move-object/from16 v3, p0

    move-object/from16 v5, p1

    move-object/from16 v8, p4

    .line 443
    invoke-virtual/range {v3 .. v8}, Lcom/samsung/android/org/xbill/jnamed;->doAXFR(Lcom/samsung/android/org/xbill/DNS/Name;Lcom/samsung/android/org/xbill/DNS/Message;Lcom/samsung/android/org/xbill/DNS/TSIG;Lcom/samsung/android/org/xbill/DNS/TSIGRecord;Ljava/net/Socket;)[B

    move-result-object v3

    goto/16 :goto_0

    .line 425
    .end local v4    # "name":Lcom/samsung/android/org/xbill/DNS/Name;
    .end local v9    # "response":Lcom/samsung/android/org/xbill/DNS/Message;
    .end local v11    # "type":I
    .end local v12    # "dclass":I
    .end local v16    # "maxLength":I
    :cond_8
    if-eqz v19, :cond_9

    .line 426
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/org/xbill/DNS/OPTRecord;->getPayloadSize()I

    move-result v3

    const/16 v5, 0x200

    invoke-static {v3, v5}, Ljava/lang/Math;->max(II)I

    move-result v16

    .restart local v16    # "maxLength":I
    goto :goto_1

    .line 428
    .end local v16    # "maxLength":I
    :cond_9
    const/16 v16, 0x200

    .restart local v16    # "maxLength":I
    goto :goto_1

    .line 444
    .restart local v4    # "name":Lcom/samsung/android/org/xbill/DNS/Name;
    .restart local v9    # "response":Lcom/samsung/android/org/xbill/DNS/Message;
    .restart local v11    # "type":I
    .restart local v12    # "dclass":I
    :cond_a
    invoke-static {v11}, Lcom/samsung/android/org/xbill/DNS/Type;->isRR(I)Z

    move-result v3

    if-nez v3, :cond_b

    const/16 v3, 0xff

    if-eq v11, v3, :cond_b

    .line 445
    const/4 v3, 0x4

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v3}, Lcom/samsung/android/org/xbill/jnamed;->errorMessage(Lcom/samsung/android/org/xbill/DNS/Message;I)[B

    move-result-object v3

    goto/16 :goto_0

    .line 447
    :cond_b
    const/4 v13, 0x0

    move-object/from16 v8, p0

    move-object v10, v4

    invoke-virtual/range {v8 .. v14}, Lcom/samsung/android/org/xbill/jnamed;->addAnswer(Lcom/samsung/android/org/xbill/DNS/Message;Lcom/samsung/android/org/xbill/DNS/Name;IIII)B

    move-result v21

    .line 448
    .local v21, "rcode":B
    if-eqz v21, :cond_c

    const/4 v3, 0x3

    move/from16 v0, v21

    if-eq v0, v3, :cond_c

    .line 449
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/org/xbill/jnamed;->errorMessage(Lcom/samsung/android/org/xbill/DNS/Message;I)[B

    move-result-object v3

    goto/16 :goto_0

    .line 451
    :cond_c
    move-object/from16 v0, p0

    invoke-direct {v0, v9, v14}, Lcom/samsung/android/org/xbill/jnamed;->addAdditional(Lcom/samsung/android/org/xbill/DNS/Message;I)V

    .line 453
    if-eqz v19, :cond_d

    .line 454
    const/4 v3, 0x1

    if-ne v14, v3, :cond_e

    const v18, 0x8000

    .line 455
    .local v18, "optflags":I
    :goto_2
    new-instance v17, Lcom/samsung/android/org/xbill/DNS/OPTRecord;

    const/16 v3, 0x1000

    const/4 v5, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v21

    move/from16 v2, v18

    invoke-direct {v0, v3, v1, v5, v2}, Lcom/samsung/android/org/xbill/DNS/OPTRecord;-><init>(IIII)V

    .line 457
    .local v17, "opt":Lcom/samsung/android/org/xbill/DNS/OPTRecord;
    const/4 v3, 0x3

    move-object/from16 v0, v17

    invoke-virtual {v9, v0, v3}, Lcom/samsung/android/org/xbill/DNS/Message;->addRecord(Lcom/samsung/android/org/xbill/DNS/Record;I)V

    .line 460
    .end local v17    # "opt":Lcom/samsung/android/org/xbill/DNS/OPTRecord;
    .end local v18    # "optflags":I
    :cond_d
    const/4 v3, 0x0

    invoke-virtual {v9, v6, v3, v7}, Lcom/samsung/android/org/xbill/DNS/Message;->setTSIG(Lcom/samsung/android/org/xbill/DNS/TSIG;ILcom/samsung/android/org/xbill/DNS/TSIGRecord;)V

    .line 461
    move/from16 v0, v16

    invoke-virtual {v9, v0}, Lcom/samsung/android/org/xbill/DNS/Message;->toWire(I)[B

    move-result-object v3

    goto/16 :goto_0

    .line 454
    :cond_e
    const/16 v18, 0x0

    goto :goto_2
.end method

.method public getCache(I)Lcom/samsung/android/org/xbill/DNS/Cache;
    .locals 3
    .param p1, "dclass"    # I

    .prologue
    .line 138
    iget-object v1, p0, Lcom/samsung/android/org/xbill/jnamed;->caches:Ljava/util/Map;

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, p1}, Ljava/lang/Integer;-><init>(I)V

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/org/xbill/DNS/Cache;

    .line 139
    .local v0, "c":Lcom/samsung/android/org/xbill/DNS/Cache;
    if-nez v0, :cond_0

    .line 140
    new-instance v0, Lcom/samsung/android/org/xbill/DNS/Cache;

    .end local v0    # "c":Lcom/samsung/android/org/xbill/DNS/Cache;
    invoke-direct {v0, p1}, Lcom/samsung/android/org/xbill/DNS/Cache;-><init>(I)V

    .line 141
    .restart local v0    # "c":Lcom/samsung/android/org/xbill/DNS/Cache;
    iget-object v1, p0, Lcom/samsung/android/org/xbill/jnamed;->caches:Ljava/util/Map;

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, p1}, Ljava/lang/Integer;-><init>(I)V

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 143
    :cond_0
    return-object v0
.end method

.method public serveTCP(Ljava/net/InetAddress;I)V
    .locals 7
    .param p1, "addr"    # Ljava/net/InetAddress;
    .param p2, "port"    # I

    .prologue
    .line 540
    :try_start_0
    new-instance v2, Ljava/net/ServerSocket;

    const/16 v4, 0x80

    invoke-direct {v2, p2, v4, p1}, Ljava/net/ServerSocket;-><init>(IILjava/net/InetAddress;)V

    .line 542
    .local v2, "sock":Ljava/net/ServerSocket;
    :goto_0
    invoke-virtual {v2}, Ljava/net/ServerSocket;->accept()Ljava/net/Socket;

    move-result-object v1

    .line 544
    .local v1, "s":Ljava/net/Socket;
    new-instance v3, Ljava/lang/Thread;

    new-instance v4, Lcom/samsung/android/org/xbill/jnamed$1;

    invoke-direct {v4, p0, v1}, Lcom/samsung/android/org/xbill/jnamed$1;-><init>(Lcom/samsung/android/org/xbill/jnamed;Ljava/net/Socket;)V

    invoke-direct {v3, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 546
    .local v3, "t":Ljava/lang/Thread;
    invoke-virtual {v3}, Ljava/lang/Thread;->start()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 549
    .end local v1    # "s":Ljava/net/Socket;
    .end local v2    # "sock":Ljava/net/ServerSocket;
    .end local v3    # "t":Ljava/lang/Thread;
    :catch_0
    move-exception v0

    .line 550
    .local v0, "e":Ljava/io/IOException;
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "serveTCP("

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1, p2}, Lcom/samsung/android/org/xbill/jnamed;->addrport(Ljava/net/InetAddress;I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "): "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 551
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 550
    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 553
    return-void
.end method

.method public serveUDP(Ljava/net/InetAddress;I)V
    .locals 11
    .param p1, "addr"    # Ljava/net/InetAddress;
    .param p2, "port"    # I

    .prologue
    .line 558
    :try_start_0
    new-instance v6, Ljava/net/DatagramSocket;

    invoke-direct {v6, p2, p1}, Ljava/net/DatagramSocket;-><init>(ILjava/net/InetAddress;)V

    .line 559
    .local v6, "sock":Ljava/net/DatagramSocket;
    const/16 v7, 0x200

    .line 560
    .local v7, "udpLength":S
    const/16 v8, 0x200

    new-array v1, v8, [B

    .line 561
    .local v1, "in":[B
    new-instance v2, Ljava/net/DatagramPacket;

    array-length v8, v1

    invoke-direct {v2, v1, v8}, Ljava/net/DatagramPacket;-><init>([BI)V

    .line 562
    .local v2, "indp":Ljava/net/DatagramPacket;
    const/4 v3, 0x0

    .line 564
    .local v3, "outdp":Ljava/net/DatagramPacket;
    :cond_0
    :goto_0
    array-length v8, v1

    invoke-virtual {v2, v8}, Ljava/net/DatagramPacket;->setLength(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 566
    :try_start_1
    invoke-virtual {v6, v2}, Ljava/net/DatagramSocket;->receive(Ljava/net/DatagramPacket;)V
    :try_end_1
    .catch Ljava/io/InterruptedIOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 572
    const/4 v5, 0x0

    .line 574
    .local v5, "response":[B
    :try_start_2
    new-instance v4, Lcom/samsung/android/org/xbill/DNS/Message;

    invoke-direct {v4, v1}, Lcom/samsung/android/org/xbill/DNS/Message;-><init>([B)V

    .line 576
    .local v4, "query":Lcom/samsung/android/org/xbill/DNS/Message;
    invoke-virtual {v2}, Ljava/net/DatagramPacket;->getLength()I

    move-result v8

    .line 577
    const/4 v9, 0x0

    .line 575
    invoke-virtual {p0, v4, v1, v8, v9}, Lcom/samsung/android/org/xbill/jnamed;->generateReply(Lcom/samsung/android/org/xbill/DNS/Message;[BILjava/net/Socket;)[B
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v5

    .line 578
    if-eqz v5, :cond_0

    .line 584
    .end local v4    # "query":Lcom/samsung/android/org/xbill/DNS/Message;
    :goto_1
    if-nez v3, :cond_1

    .line 585
    :try_start_3
    new-instance v3, Ljava/net/DatagramPacket;

    .line 586
    .end local v3    # "outdp":Ljava/net/DatagramPacket;
    array-length v8, v5

    .line 587
    invoke-virtual {v2}, Ljava/net/DatagramPacket;->getAddress()Ljava/net/InetAddress;

    move-result-object v9

    .line 588
    invoke-virtual {v2}, Ljava/net/DatagramPacket;->getPort()I

    move-result v10

    .line 585
    invoke-direct {v3, v5, v8, v9, v10}, Ljava/net/DatagramPacket;-><init>([BILjava/net/InetAddress;I)V

    .line 595
    .restart local v3    # "outdp":Ljava/net/DatagramPacket;
    :goto_2
    invoke-virtual {v6, v3}, Ljava/net/DatagramSocket;->send(Ljava/net/DatagramPacket;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    .line 598
    .end local v1    # "in":[B
    .end local v2    # "indp":Ljava/net/DatagramPacket;
    .end local v3    # "outdp":Ljava/net/DatagramPacket;
    .end local v5    # "response":[B
    .end local v6    # "sock":Ljava/net/DatagramSocket;
    .end local v7    # "udpLength":S
    :catch_0
    move-exception v0

    .line 599
    .local v0, "e":Ljava/io/IOException;
    sget-object v8, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "serveUDP("

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1, p2}, Lcom/samsung/android/org/xbill/jnamed;->addrport(Ljava/net/InetAddress;I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "): "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 600
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 599
    invoke-virtual {v8, v9}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 602
    return-void

    .line 568
    .end local v0    # "e":Ljava/io/IOException;
    .restart local v1    # "in":[B
    .restart local v2    # "indp":Ljava/net/DatagramPacket;
    .restart local v3    # "outdp":Ljava/net/DatagramPacket;
    .restart local v6    # "sock":Ljava/net/DatagramSocket;
    .restart local v7    # "udpLength":S
    :catch_1
    move-exception v0

    .line 569
    .local v0, "e":Ljava/io/InterruptedIOException;
    goto :goto_0

    .line 581
    .end local v0    # "e":Ljava/io/InterruptedIOException;
    .restart local v5    # "response":[B
    :catch_2
    move-exception v0

    .line 582
    .local v0, "e":Ljava/io/IOException;
    :try_start_4
    invoke-virtual {p0, v1}, Lcom/samsung/android/org/xbill/jnamed;->formerrMessage([B)[B

    move-result-object v5

    goto :goto_1

    .line 590
    .end local v0    # "e":Ljava/io/IOException;
    :cond_1
    invoke-virtual {v3, v5}, Ljava/net/DatagramPacket;->setData([B)V

    .line 591
    array-length v8, v5

    invoke-virtual {v3, v8}, Ljava/net/DatagramPacket;->setLength(I)V

    .line 592
    invoke-virtual {v2}, Ljava/net/DatagramPacket;->getAddress()Ljava/net/InetAddress;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/net/DatagramPacket;->setAddress(Ljava/net/InetAddress;)V

    .line 593
    invoke-virtual {v2}, Ljava/net/DatagramPacket;->getPort()I

    move-result v8

    invoke-virtual {v3, v8}, Ljava/net/DatagramPacket;->setPort(I)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_2
.end method
