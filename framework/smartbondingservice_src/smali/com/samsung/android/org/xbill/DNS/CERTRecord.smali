.class public Lcom/samsung/android/org/xbill/DNS/CERTRecord;
.super Lcom/samsung/android/org/xbill/DNS/Record;
.source "CERTRecord.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/org/xbill/DNS/CERTRecord$CertificateType;
    }
.end annotation


# static fields
.field public static final OID:I = 0xfe

.field public static final PGP:I = 0x3

.field public static final PKIX:I = 0x1

.field public static final SPKI:I = 0x2

.field public static final URI:I = 0xfd

.field private static final serialVersionUID:J = 0x4219a095e1a12903L


# instance fields
.field private alg:I

.field private cert:[B

.field private certType:I

.field private keyTag:I


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 114
    invoke-direct {p0}, Lcom/samsung/android/org/xbill/DNS/Record;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/org/xbill/DNS/Name;IJIII[B)V
    .locals 7
    .param p1, "name"    # Lcom/samsung/android/org/xbill/DNS/Name;
    .param p2, "dclass"    # I
    .param p3, "ttl"    # J
    .param p5, "certType"    # I
    .param p6, "keyTag"    # I
    .param p7, "alg"    # I
    .param p8, "cert"    # [B

    .prologue
    .line 132
    const/16 v2, 0x25

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/org/xbill/DNS/Record;-><init>(Lcom/samsung/android/org/xbill/DNS/Name;IIJ)V

    .line 133
    const-string v0, "certType"

    invoke-static {v0, p5}, Lcom/samsung/android/org/xbill/DNS/CERTRecord;->checkU16(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/org/xbill/DNS/CERTRecord;->certType:I

    .line 134
    const-string v0, "keyTag"

    invoke-static {v0, p6}, Lcom/samsung/android/org/xbill/DNS/CERTRecord;->checkU16(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/org/xbill/DNS/CERTRecord;->keyTag:I

    .line 135
    const-string v0, "alg"

    invoke-static {v0, p7}, Lcom/samsung/android/org/xbill/DNS/CERTRecord;->checkU8(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/org/xbill/DNS/CERTRecord;->alg:I

    .line 136
    iput-object p8, p0, Lcom/samsung/android/org/xbill/DNS/CERTRecord;->cert:[B

    .line 137
    return-void
.end method


# virtual methods
.method public getAlgorithm()I
    .locals 1

    .prologue
    .line 206
    iget v0, p0, Lcom/samsung/android/org/xbill/DNS/CERTRecord;->alg:I

    return v0
.end method

.method public getCert()[B
    .locals 1

    .prologue
    .line 214
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/CERTRecord;->cert:[B

    return-object v0
.end method

.method public getCertType()I
    .locals 1

    .prologue
    .line 190
    iget v0, p0, Lcom/samsung/android/org/xbill/DNS/CERTRecord;->certType:I

    return v0
.end method

.method public getKeyTag()I
    .locals 1

    .prologue
    .line 198
    iget v0, p0, Lcom/samsung/android/org/xbill/DNS/CERTRecord;->keyTag:I

    return v0
.end method

.method getObject()Lcom/samsung/android/org/xbill/DNS/Record;
    .locals 1

    .prologue
    .line 118
    new-instance v0, Lcom/samsung/android/org/xbill/DNS/CERTRecord;

    invoke-direct {v0}, Lcom/samsung/android/org/xbill/DNS/CERTRecord;-><init>()V

    return-object v0
.end method

.method rdataFromString(Lcom/samsung/android/org/xbill/DNS/Tokenizer;Lcom/samsung/android/org/xbill/DNS/Name;)V
    .locals 4
    .param p1, "st"    # Lcom/samsung/android/org/xbill/DNS/Tokenizer;
    .param p2, "origin"    # Lcom/samsung/android/org/xbill/DNS/Name;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 149
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->getString()Ljava/lang/String;

    move-result-object v1

    .line 150
    .local v1, "certTypeString":Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/android/org/xbill/DNS/CERTRecord$CertificateType;->value(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/samsung/android/org/xbill/DNS/CERTRecord;->certType:I

    .line 151
    iget v2, p0, Lcom/samsung/android/org/xbill/DNS/CERTRecord;->certType:I

    if-gez v2, :cond_0

    .line 152
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid certificate type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 153
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 152
    invoke-virtual {p1, v2}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->exception(Ljava/lang/String;)Lcom/samsung/android/org/xbill/DNS/TextParseException;

    move-result-object v2

    throw v2

    .line 154
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->getUInt16()I

    move-result v2

    iput v2, p0, Lcom/samsung/android/org/xbill/DNS/CERTRecord;->keyTag:I

    .line 155
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->getString()Ljava/lang/String;

    move-result-object v0

    .line 156
    .local v0, "algString":Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/android/org/xbill/DNS/DNSSEC$Algorithm;->value(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/samsung/android/org/xbill/DNS/CERTRecord;->alg:I

    .line 157
    iget v2, p0, Lcom/samsung/android/org/xbill/DNS/CERTRecord;->alg:I

    if-gez v2, :cond_1

    .line 158
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid algorithm: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->exception(Ljava/lang/String;)Lcom/samsung/android/org/xbill/DNS/TextParseException;

    move-result-object v2

    throw v2

    .line 159
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->getBase64()[B

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/org/xbill/DNS/CERTRecord;->cert:[B

    .line 160
    return-void
.end method

.method rrFromWire(Lcom/samsung/android/org/xbill/DNS/DNSInput;)V
    .locals 1
    .param p1, "in"    # Lcom/samsung/android/org/xbill/DNS/DNSInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 141
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/DNSInput;->readU16()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/org/xbill/DNS/CERTRecord;->certType:I

    .line 142
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/DNSInput;->readU16()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/org/xbill/DNS/CERTRecord;->keyTag:I

    .line 143
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/DNSInput;->readU8()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/org/xbill/DNS/CERTRecord;->alg:I

    .line 144
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/DNSInput;->readByteArray()[B

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/org/xbill/DNS/CERTRecord;->cert:[B

    .line 145
    return-void
.end method

.method rrToString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 167
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 168
    .local v0, "sb":Ljava/lang/StringBuffer;
    iget v1, p0, Lcom/samsung/android/org/xbill/DNS/CERTRecord;->certType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 169
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 170
    iget v1, p0, Lcom/samsung/android/org/xbill/DNS/CERTRecord;->keyTag:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 171
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 172
    iget v1, p0, Lcom/samsung/android/org/xbill/DNS/CERTRecord;->alg:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 173
    iget-object v1, p0, Lcom/samsung/android/org/xbill/DNS/CERTRecord;->cert:[B

    if-eqz v1, :cond_0

    .line 174
    const-string v1, "multiline"

    invoke-static {v1}, Lcom/samsung/android/org/xbill/DNS/Options;->check(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 175
    const-string v1, " (\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 176
    iget-object v1, p0, Lcom/samsung/android/org/xbill/DNS/CERTRecord;->cert:[B

    const/16 v2, 0x40

    const-string v3, "\t"

    const/4 v4, 0x1

    invoke-static {v1, v2, v3, v4}, Lcom/samsung/android/org/xbill/DNS/utils/base64;->formatString([BILjava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 182
    :cond_0
    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 178
    :cond_1
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 179
    iget-object v1, p0, Lcom/samsung/android/org/xbill/DNS/CERTRecord;->cert:[B

    invoke-static {v1}, Lcom/samsung/android/org/xbill/DNS/utils/base64;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0
.end method

.method rrToWire(Lcom/samsung/android/org/xbill/DNS/DNSOutput;Lcom/samsung/android/org/xbill/DNS/Compression;Z)V
    .locals 1
    .param p1, "out"    # Lcom/samsung/android/org/xbill/DNS/DNSOutput;
    .param p2, "c"    # Lcom/samsung/android/org/xbill/DNS/Compression;
    .param p3, "canonical"    # Z

    .prologue
    .line 219
    iget v0, p0, Lcom/samsung/android/org/xbill/DNS/CERTRecord;->certType:I

    invoke-virtual {p1, v0}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeU16(I)V

    .line 220
    iget v0, p0, Lcom/samsung/android/org/xbill/DNS/CERTRecord;->keyTag:I

    invoke-virtual {p1, v0}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeU16(I)V

    .line 221
    iget v0, p0, Lcom/samsung/android/org/xbill/DNS/CERTRecord;->alg:I

    invoke-virtual {p1, v0}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeU8(I)V

    .line 222
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/CERTRecord;->cert:[B

    invoke-virtual {p1, v0}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeByteArray([B)V

    .line 223
    return-void
.end method
