.class public Lcom/samsung/android/org/xbill/DNS/tests/xfrin;
.super Ljava/lang/Object;
.source "xfrin.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static main([Ljava/lang/String;)V
    .locals 22
    .param p0, "args"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 22
    const/4 v9, 0x0

    .line 23
    .local v9, "key":Lcom/samsung/android/org/xbill/DNS/TSIG;
    const/4 v14, -0x1

    .line 24
    .local v14, "ixfr_serial":I
    const/4 v7, 0x0

    .line 25
    .local v7, "server":Ljava/lang/String;
    const/16 v8, 0x35

    .line 26
    .local v8, "port":I
    const/4 v6, 0x0

    .line 29
    .local v6, "fallback":Z
    const/4 v2, 0x0

    .line 30
    .local v2, "arg":I
    :goto_0
    move-object/from16 v0, p0

    array-length v4, v0

    if-lt v2, v4, :cond_5

    .line 57
    :cond_0
    move-object/from16 v0, p0

    array-length v4, v0

    if-lt v2, v4, :cond_1

    .line 58
    const-string v4, "no zone name specified"

    invoke-static {v4}, Lcom/samsung/android/org/xbill/DNS/tests/xfrin;->usage(Ljava/lang/String;)V

    .line 59
    :cond_1
    aget-object v4, p0, v2

    invoke-static {v4}, Lcom/samsung/android/org/xbill/DNS/Name;->fromString(Ljava/lang/String;)Lcom/samsung/android/org/xbill/DNS/Name;

    move-result-object v3

    .line 61
    .local v3, "zname":Lcom/samsung/android/org/xbill/DNS/Name;
    if-nez v7, :cond_3

    .line 62
    new-instance v15, Lcom/samsung/android/org/xbill/DNS/Lookup;

    const/4 v4, 0x2

    invoke-direct {v15, v3, v4}, Lcom/samsung/android/org/xbill/DNS/Lookup;-><init>(Lcom/samsung/android/org/xbill/DNS/Name;I)V

    .line 63
    .local v15, "l":Lcom/samsung/android/org/xbill/DNS/Lookup;
    invoke-virtual {v15}, Lcom/samsung/android/org/xbill/DNS/Lookup;->run()[Lcom/samsung/android/org/xbill/DNS/Record;

    move-result-object v16

    .line 64
    .local v16, "ns":[Lcom/samsung/android/org/xbill/DNS/Record;
    if-nez v16, :cond_2

    .line 65
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v20, "failed to look up NS record: "

    move-object/from16 v0, v20

    invoke-direct {v5, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 66
    invoke-virtual {v15}, Lcom/samsung/android/org/xbill/DNS/Lookup;->getErrorString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 65
    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 67
    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/System;->exit(I)V

    .line 69
    :cond_2
    const/4 v4, 0x0

    aget-object v4, v16, v4

    invoke-virtual {v4}, Lcom/samsung/android/org/xbill/DNS/Record;->rdataToString()Ljava/lang/String;

    move-result-object v7

    .line 70
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v20, "sending to server \'"

    move-object/from16 v0, v20

    invoke-direct {v5, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v20, "\'"

    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 73
    .end local v15    # "l":Lcom/samsung/android/org/xbill/DNS/Lookup;
    .end local v16    # "ns":[Lcom/samsung/android/org/xbill/DNS/Record;
    :cond_3
    if-ltz v14, :cond_e

    .line 74
    int-to-long v4, v14

    invoke-static/range {v3 .. v9}, Lcom/samsung/android/org/xbill/DNS/ZoneTransferIn;->newIXFR(Lcom/samsung/android/org/xbill/DNS/Name;JZLjava/lang/String;ILcom/samsung/android/org/xbill/DNS/TSIG;)Lcom/samsung/android/org/xbill/DNS/ZoneTransferIn;

    move-result-object v19

    .line 79
    .local v19, "xfrin":Lcom/samsung/android/org/xbill/DNS/ZoneTransferIn;
    :goto_1
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/org/xbill/DNS/ZoneTransferIn;->run()Ljava/util/List;

    move-result-object v17

    .line 80
    .local v17, "response":Ljava/util/List;
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/org/xbill/DNS/ZoneTransferIn;->isAXFR()Z

    move-result v4

    if-eqz v4, :cond_11

    .line 81
    if-ltz v14, :cond_f

    .line 82
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v5, "AXFR-like IXFR response"

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 85
    :goto_2
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .line 86
    .local v12, "it":Ljava/util/Iterator;
    :goto_3
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_10

    .line 108
    .end local v12    # "it":Ljava/util/Iterator;
    :cond_4
    :goto_4
    return-void

    .line 31
    .end local v3    # "zname":Lcom/samsung/android/org/xbill/DNS/Name;
    .end local v17    # "response":Ljava/util/List;
    .end local v19    # "xfrin":Lcom/samsung/android/org/xbill/DNS/ZoneTransferIn;
    :cond_5
    aget-object v4, p0, v2

    const-string v5, "-i"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 32
    add-int/lit8 v2, v2, 0x1

    aget-object v4, p0, v2

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v14

    .line 33
    if-gez v14, :cond_6

    .line 34
    const-string v4, "invalid serial number"

    invoke-static {v4}, Lcom/samsung/android/org/xbill/DNS/tests/xfrin;->usage(Ljava/lang/String;)V

    .line 55
    :cond_6
    :goto_5
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    .line 35
    :cond_7
    aget-object v4, p0, v2

    const-string v5, "-k"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 36
    add-int/lit8 v2, v2, 0x1

    aget-object v18, p0, v2

    .line 37
    .local v18, "s":Ljava/lang/String;
    const/16 v4, 0x2f

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v11

    .line 38
    .local v11, "index":I
    if-gez v11, :cond_8

    .line 39
    const-string v4, "invalid key"

    invoke-static {v4}, Lcom/samsung/android/org/xbill/DNS/tests/xfrin;->usage(Ljava/lang/String;)V

    .line 40
    :cond_8
    new-instance v9, Lcom/samsung/android/org/xbill/DNS/TSIG;

    .end local v9    # "key":Lcom/samsung/android/org/xbill/DNS/TSIG;
    const/4 v4, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v4, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 41
    add-int/lit8 v5, v11, 0x1

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 40
    invoke-direct {v9, v4, v5}, Lcom/samsung/android/org/xbill/DNS/TSIG;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    .restart local v9    # "key":Lcom/samsung/android/org/xbill/DNS/TSIG;
    goto :goto_5

    .end local v11    # "index":I
    .end local v18    # "s":Ljava/lang/String;
    :cond_9
    aget-object v4, p0, v2

    const-string v5, "-s"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 43
    add-int/lit8 v2, v2, 0x1

    aget-object v7, p0, v2

    .line 44
    goto :goto_5

    :cond_a
    aget-object v4, p0, v2

    const-string v5, "-p"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 45
    add-int/lit8 v2, v2, 0x1

    aget-object v4, p0, v2

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    .line 46
    if-ltz v8, :cond_b

    const v4, 0xffff

    if-le v8, v4, :cond_6

    .line 47
    :cond_b
    const-string v4, "invalid port"

    invoke-static {v4}, Lcom/samsung/android/org/xbill/DNS/tests/xfrin;->usage(Ljava/lang/String;)V

    goto :goto_5

    .line 48
    :cond_c
    aget-object v4, p0, v2

    const-string v5, "-f"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 49
    const/4 v6, 0x1

    .line 50
    goto :goto_5

    :cond_d
    aget-object v4, p0, v2

    const-string v5, "-"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 51
    const-string v4, "invalid option"

    invoke-static {v4}, Lcom/samsung/android/org/xbill/DNS/tests/xfrin;->usage(Ljava/lang/String;)V

    goto :goto_5

    .line 77
    .restart local v3    # "zname":Lcom/samsung/android/org/xbill/DNS/Name;
    :cond_e
    invoke-static {v3, v7, v8, v9}, Lcom/samsung/android/org/xbill/DNS/ZoneTransferIn;->newAXFR(Lcom/samsung/android/org/xbill/DNS/Name;Ljava/lang/String;ILcom/samsung/android/org/xbill/DNS/TSIG;)Lcom/samsung/android/org/xbill/DNS/ZoneTransferIn;

    move-result-object v19

    .restart local v19    # "xfrin":Lcom/samsung/android/org/xbill/DNS/ZoneTransferIn;
    goto/16 :goto_1

    .line 84
    .restart local v17    # "response":Ljava/util/List;
    :cond_f
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v5, "AXFR response"

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 87
    .restart local v12    # "it":Ljava/util/Iterator;
    :cond_10
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    goto/16 :goto_3

    .line 88
    .end local v12    # "it":Ljava/util/Iterator;
    :cond_11
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/org/xbill/DNS/ZoneTransferIn;->isIXFR()Z

    move-result v4

    if-eqz v4, :cond_14

    .line 89
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v5, "IXFR response"

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 90
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .line 91
    .restart local v12    # "it":Ljava/util/Iterator;
    :cond_12
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 93
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/samsung/android/org/xbill/DNS/ZoneTransferIn$Delta;

    .line 94
    .local v10, "delta":Lcom/samsung/android/org/xbill/DNS/ZoneTransferIn$Delta;
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v20, "delta from "

    move-object/from16 v0, v20

    invoke-direct {v5, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v0, v10, Lcom/samsung/android/org/xbill/DNS/ZoneTransferIn$Delta;->start:J

    move-wide/from16 v20, v0

    move-wide/from16 v0, v20

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 95
    const-string v20, " to "

    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v0, v10, Lcom/samsung/android/org/xbill/DNS/ZoneTransferIn$Delta;->end:J

    move-wide/from16 v20, v0

    move-wide/from16 v0, v20

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 94
    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 96
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v5, "deletes"

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 97
    iget-object v4, v10, Lcom/samsung/android/org/xbill/DNS/ZoneTransferIn$Delta;->deletes:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    .line 98
    .local v13, "it2":Ljava/util/Iterator;
    :goto_6
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_13

    .line 100
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v5, "adds"

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 101
    iget-object v4, v10, Lcom/samsung/android/org/xbill/DNS/ZoneTransferIn$Delta;->adds:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    .line 102
    :goto_7
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_12

    .line 103
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    goto :goto_7

    .line 99
    :cond_13
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    goto :goto_6

    .line 105
    .end local v10    # "delta":Lcom/samsung/android/org/xbill/DNS/ZoneTransferIn$Delta;
    .end local v12    # "it":Ljava/util/Iterator;
    .end local v13    # "it2":Ljava/util/Iterator;
    :cond_14
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/org/xbill/DNS/ZoneTransferIn;->isCurrent()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 106
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v5, "up to date"

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_4
.end method

.method private static usage(Ljava/lang/String;)V
    .locals 3
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 13
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Error: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 14
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "usage: xfrin [-i serial] [-k keyname/secret] [-s server] [-p port] [-f] zone"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 16
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/System;->exit(I)V

    .line 17
    return-void
.end method
