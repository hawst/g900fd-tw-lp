.class public Lcom/samsung/android/org/xbill/DNS/TLSARecord;
.super Lcom/samsung/android/org/xbill/DNS/Record;
.source "TLSARecord.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/org/xbill/DNS/TLSARecord$CertificateUsage;,
        Lcom/samsung/android/org/xbill/DNS/TLSARecord$MatchingType;,
        Lcom/samsung/android/org/xbill/DNS/TLSARecord$Selector;
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x4f285a5a6b3a749L


# instance fields
.field private certificateAssociationData:[B

.field private certificateUsage:I

.field private matchingType:I

.field private selector:I


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/samsung/android/org/xbill/DNS/Record;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/org/xbill/DNS/Name;IJIII[B)V
    .locals 7
    .param p1, "name"    # Lcom/samsung/android/org/xbill/DNS/Name;
    .param p2, "dclass"    # I
    .param p3, "ttl"    # J
    .param p5, "certificateUsage"    # I
    .param p6, "selector"    # I
    .param p7, "matchingType"    # I
    .param p8, "certificateAssociationData"    # [B

    .prologue
    .line 84
    const/16 v2, 0x34

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/org/xbill/DNS/Record;-><init>(Lcom/samsung/android/org/xbill/DNS/Name;IIJ)V

    .line 85
    const-string v0, "certificateUsage"

    invoke-static {v0, p5}, Lcom/samsung/android/org/xbill/DNS/TLSARecord;->checkU8(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/org/xbill/DNS/TLSARecord;->certificateUsage:I

    .line 86
    const-string v0, "selector"

    invoke-static {v0, p6}, Lcom/samsung/android/org/xbill/DNS/TLSARecord;->checkU8(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/org/xbill/DNS/TLSARecord;->selector:I

    .line 87
    const-string v0, "matchingType"

    invoke-static {v0, p7}, Lcom/samsung/android/org/xbill/DNS/TLSARecord;->checkU8(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/org/xbill/DNS/TLSARecord;->matchingType:I

    .line 89
    const-string v0, "certificateAssociationData"

    .line 91
    const v1, 0xffff

    .line 88
    invoke-static {v0, p8, v1}, Lcom/samsung/android/org/xbill/DNS/TLSARecord;->checkByteArrayLength(Ljava/lang/String;[BI)[B

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/org/xbill/DNS/TLSARecord;->certificateAssociationData:[B

    .line 91
    return-void
.end method


# virtual methods
.method public final getCertificateAssociationData()[B
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/TLSARecord;->certificateAssociationData:[B

    return-object v0
.end method

.method public getCertificateUsage()I
    .locals 1

    .prologue
    .line 136
    iget v0, p0, Lcom/samsung/android/org/xbill/DNS/TLSARecord;->certificateUsage:I

    return v0
.end method

.method public getMatchingType()I
    .locals 1

    .prologue
    .line 148
    iget v0, p0, Lcom/samsung/android/org/xbill/DNS/TLSARecord;->matchingType:I

    return v0
.end method

.method getObject()Lcom/samsung/android/org/xbill/DNS/Record;
    .locals 1

    .prologue
    .line 66
    new-instance v0, Lcom/samsung/android/org/xbill/DNS/TLSARecord;

    invoke-direct {v0}, Lcom/samsung/android/org/xbill/DNS/TLSARecord;-><init>()V

    return-object v0
.end method

.method public getSelector()I
    .locals 1

    .prologue
    .line 142
    iget v0, p0, Lcom/samsung/android/org/xbill/DNS/TLSARecord;->selector:I

    return v0
.end method

.method rdataFromString(Lcom/samsung/android/org/xbill/DNS/Tokenizer;Lcom/samsung/android/org/xbill/DNS/Name;)V
    .locals 1
    .param p1, "st"    # Lcom/samsung/android/org/xbill/DNS/Tokenizer;
    .param p2, "origin"    # Lcom/samsung/android/org/xbill/DNS/Name;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 104
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->getUInt8()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/org/xbill/DNS/TLSARecord;->certificateUsage:I

    .line 105
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->getUInt8()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/org/xbill/DNS/TLSARecord;->selector:I

    .line 106
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->getUInt8()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/org/xbill/DNS/TLSARecord;->matchingType:I

    .line 107
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->getHex()[B

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/org/xbill/DNS/TLSARecord;->certificateAssociationData:[B

    .line 108
    return-void
.end method

.method rrFromWire(Lcom/samsung/android/org/xbill/DNS/DNSInput;)V
    .locals 1
    .param p1, "in"    # Lcom/samsung/android/org/xbill/DNS/DNSInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 96
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/DNSInput;->readU8()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/org/xbill/DNS/TLSARecord;->certificateUsage:I

    .line 97
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/DNSInput;->readU8()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/org/xbill/DNS/TLSARecord;->selector:I

    .line 98
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/DNSInput;->readU8()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/org/xbill/DNS/TLSARecord;->matchingType:I

    .line 99
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/DNSInput;->readByteArray()[B

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/org/xbill/DNS/TLSARecord;->certificateAssociationData:[B

    .line 100
    return-void
.end method

.method rrToString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 113
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 114
    .local v0, "sb":Ljava/lang/StringBuffer;
    iget v1, p0, Lcom/samsung/android/org/xbill/DNS/TLSARecord;->certificateUsage:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 115
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 116
    iget v1, p0, Lcom/samsung/android/org/xbill/DNS/TLSARecord;->selector:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 117
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 118
    iget v1, p0, Lcom/samsung/android/org/xbill/DNS/TLSARecord;->matchingType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 119
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 120
    iget-object v1, p0, Lcom/samsung/android/org/xbill/DNS/TLSARecord;->certificateAssociationData:[B

    invoke-static {v1}, Lcom/samsung/android/org/xbill/DNS/utils/base16;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 122
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method rrToWire(Lcom/samsung/android/org/xbill/DNS/DNSOutput;Lcom/samsung/android/org/xbill/DNS/Compression;Z)V
    .locals 1
    .param p1, "out"    # Lcom/samsung/android/org/xbill/DNS/DNSOutput;
    .param p2, "c"    # Lcom/samsung/android/org/xbill/DNS/Compression;
    .param p3, "canonical"    # Z

    .prologue
    .line 127
    iget v0, p0, Lcom/samsung/android/org/xbill/DNS/TLSARecord;->certificateUsage:I

    invoke-virtual {p1, v0}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeU8(I)V

    .line 128
    iget v0, p0, Lcom/samsung/android/org/xbill/DNS/TLSARecord;->selector:I

    invoke-virtual {p1, v0}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeU8(I)V

    .line 129
    iget v0, p0, Lcom/samsung/android/org/xbill/DNS/TLSARecord;->matchingType:I

    invoke-virtual {p1, v0}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeU8(I)V

    .line 130
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/TLSARecord;->certificateAssociationData:[B

    invoke-virtual {p1, v0}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeByteArray([B)V

    .line 131
    return-void
.end method
