.class public Lcom/samsung/android/org/xbill/update;
.super Ljava/lang/Object;
.source "update.java"


# instance fields
.field defaultClass:I

.field defaultTTL:J

.field log:Ljava/io/PrintStream;

.field query:Lcom/samsung/android/org/xbill/DNS/Message;

.field res:Lcom/samsung/android/org/xbill/DNS/Resolver;

.field response:Lcom/samsung/android/org/xbill/DNS/Message;

.field server:Ljava/lang/String;

.field zone:Lcom/samsung/android/org/xbill/DNS/Name;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 30
    .param p1, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 37
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 16
    const/16 v27, 0x0

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/org/xbill/update;->server:Ljava/lang/String;

    .line 17
    sget-object v27, Lcom/samsung/android/org/xbill/DNS/Name;->root:Lcom/samsung/android/org/xbill/DNS/Name;

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/org/xbill/update;->zone:Lcom/samsung/android/org/xbill/DNS/Name;

    .line 19
    const/16 v27, 0x1

    move/from16 v0, v27

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/org/xbill/update;->defaultClass:I

    .line 20
    const/16 v27, 0x0

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/org/xbill/update;->log:Ljava/io/PrintStream;

    .line 38
    new-instance v7, Ljava/util/LinkedList;

    invoke-direct {v7}, Ljava/util/LinkedList;-><init>()V

    .line 39
    .local v7, "inputs":Ljava/util/List;
    new-instance v13, Ljava/util/LinkedList;

    invoke-direct {v13}, Ljava/util/LinkedList;-><init>()V

    .line 41
    .local v13, "istreams":Ljava/util/List;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/org/xbill/update;->newMessage()Lcom/samsung/android/org/xbill/DNS/Message;

    move-result-object v27

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/org/xbill/update;->query:Lcom/samsung/android/org/xbill/DNS/Message;

    .line 43
    new-instance v12, Ljava/io/InputStreamReader;

    move-object/from16 v0, p1

    invoke-direct {v12, v0}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    .line 44
    .local v12, "isr":Ljava/io/InputStreamReader;
    new-instance v4, Ljava/io/BufferedReader;

    invoke-direct {v4, v12}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 46
    .local v4, "br":Ljava/io/BufferedReader;
    invoke-interface {v7, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 47
    move-object/from16 v0, p1

    invoke-interface {v13, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 51
    :cond_0
    :goto_0
    const/16 v17, 0x0

    .line 54
    .local v17, "line":Ljava/lang/String;
    :cond_1
    const/16 v27, 0x0

    :try_start_0
    move/from16 v0, v27

    invoke-interface {v13, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/io/InputStream;

    .line 55
    .local v11, "is":Ljava/io/InputStream;
    const/16 v27, 0x0

    move/from16 v0, v27

    invoke-interface {v7, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v27

    move-object/from16 v0, v27

    check-cast v0, Ljava/io/BufferedReader;

    move-object v4, v0

    .line 57
    sget-object v27, Ljava/lang/System;->in:Ljava/io/InputStream;

    move-object/from16 v0, v27

    if-ne v11, v0, :cond_2

    .line 58
    sget-object v27, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v28, "> "

    invoke-virtual/range {v27 .. v28}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 60
    :cond_2
    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v17

    .line 61
    if-nez v17, :cond_3

    .line 62
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V

    .line 63
    const/16 v27, 0x0

    move/from16 v0, v27

    invoke-interface {v7, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 64
    const/16 v27, 0x0

    move/from16 v0, v27

    invoke-interface {v13, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 65
    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v27

    if-eqz v27, :cond_3

    .line 206
    :goto_1
    return-void

    .line 68
    :cond_3
    if-eqz v17, :cond_1

    .line 70
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/org/xbill/update;->log:Ljava/io/PrintStream;

    move-object/from16 v27, v0

    if-eqz v27, :cond_4

    .line 71
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/org/xbill/update;->log:Ljava/io/PrintStream;

    move-object/from16 v27, v0

    new-instance v28, Ljava/lang/StringBuilder;

    const-string v29, "> "

    invoke-direct/range {v28 .. v29}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v28

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 73
    :cond_4
    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v27

    if-eqz v27, :cond_0

    const/16 v27, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v27

    const/16 v28, 0x23

    move/from16 v0, v27

    move/from16 v1, v28

    if-eq v0, v1, :cond_0

    .line 77
    const/16 v27, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v27

    const/16 v28, 0x3e

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_5

    .line 78
    const/16 v27, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v17

    .line 80
    :cond_5
    new-instance v23, Lcom/samsung/android/org/xbill/DNS/Tokenizer;

    move-object/from16 v0, v23

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;-><init>(Ljava/lang/String;)V

    .line 81
    .local v23, "st":Lcom/samsung/android/org/xbill/DNS/Tokenizer;
    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->get()Lcom/samsung/android/org/xbill/DNS/Tokenizer$Token;

    move-result-object v25

    .line 83
    .local v25, "token":Lcom/samsung/android/org/xbill/DNS/Tokenizer$Token;
    invoke-virtual/range {v25 .. v25}, Lcom/samsung/android/org/xbill/DNS/Tokenizer$Token;->isEOL()Z

    move-result v27

    if-nez v27, :cond_0

    .line 85
    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/samsung/android/org/xbill/DNS/Tokenizer$Token;->value:Ljava/lang/String;

    move-object/from16 v20, v0

    .line 87
    .local v20, "operation":Ljava/lang/String;
    const-string v27, "server"

    move-object/from16 v0, v20

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_6

    .line 88
    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->getString()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/org/xbill/update;->server:Ljava/lang/String;

    .line 89
    new-instance v27, Lcom/samsung/android/org/xbill/DNS/SimpleResolver;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/org/xbill/update;->server:Ljava/lang/String;

    move-object/from16 v28, v0

    invoke-direct/range {v27 .. v28}, Lcom/samsung/android/org/xbill/DNS/SimpleResolver;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/org/xbill/update;->res:Lcom/samsung/android/org/xbill/DNS/Resolver;

    .line 90
    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->get()Lcom/samsung/android/org/xbill/DNS/Tokenizer$Token;

    move-result-object v25

    .line 91
    invoke-virtual/range {v25 .. v25}, Lcom/samsung/android/org/xbill/DNS/Tokenizer$Token;->isString()Z

    move-result v27

    if-eqz v27, :cond_0

    .line 92
    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/samsung/android/org/xbill/DNS/Tokenizer$Token;->value:Ljava/lang/String;

    move-object/from16 v21, v0

    .line 93
    .local v21, "portstr":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/org/xbill/update;->res:Lcom/samsung/android/org/xbill/DNS/Resolver;

    move-object/from16 v27, v0

    invoke-static/range {v21 .. v21}, Ljava/lang/Short;->parseShort(Ljava/lang/String;)S

    move-result v28

    invoke-interface/range {v27 .. v28}, Lcom/samsung/android/org/xbill/DNS/Resolver;->setPort(I)V
    :try_end_0
    .catch Lcom/samsung/android/org/xbill/DNS/TextParseException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/InterruptedIOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    goto/16 :goto_0

    .line 231
    .end local v11    # "is":Ljava/io/InputStream;
    .end local v20    # "operation":Ljava/lang/String;
    .end local v21    # "portstr":Ljava/lang/String;
    .end local v23    # "st":Lcom/samsung/android/org/xbill/DNS/Tokenizer;
    .end local v25    # "token":Lcom/samsung/android/org/xbill/DNS/Tokenizer$Token;
    :catch_0
    move-exception v26

    .line 232
    .local v26, "tpe":Lcom/samsung/android/org/xbill/DNS/TextParseException;
    sget-object v27, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual/range {v26 .. v26}, Lcom/samsung/android/org/xbill/DNS/TextParseException;->getMessage()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 97
    .end local v26    # "tpe":Lcom/samsung/android/org/xbill/DNS/TextParseException;
    .restart local v11    # "is":Ljava/io/InputStream;
    .restart local v20    # "operation":Ljava/lang/String;
    .restart local v23    # "st":Lcom/samsung/android/org/xbill/DNS/Tokenizer;
    .restart local v25    # "token":Lcom/samsung/android/org/xbill/DNS/Tokenizer$Token;
    :cond_6
    :try_start_1
    const-string v27, "key"

    move-object/from16 v0, v20

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_8

    .line 98
    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->getString()Ljava/lang/String;

    move-result-object v16

    .line 99
    .local v16, "keyname":Ljava/lang/String;
    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->getString()Ljava/lang/String;

    move-result-object v15

    .line 100
    .local v15, "keydata":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/org/xbill/update;->res:Lcom/samsung/android/org/xbill/DNS/Resolver;

    move-object/from16 v27, v0

    if-nez v27, :cond_7

    .line 101
    new-instance v27, Lcom/samsung/android/org/xbill/DNS/SimpleResolver;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/org/xbill/update;->server:Ljava/lang/String;

    move-object/from16 v28, v0

    invoke-direct/range {v27 .. v28}, Lcom/samsung/android/org/xbill/DNS/SimpleResolver;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/org/xbill/update;->res:Lcom/samsung/android/org/xbill/DNS/Resolver;

    .line 102
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/org/xbill/update;->res:Lcom/samsung/android/org/xbill/DNS/Resolver;

    move-object/from16 v27, v0

    new-instance v28, Lcom/samsung/android/org/xbill/DNS/TSIG;

    move-object/from16 v0, v28

    move-object/from16 v1, v16

    invoke-direct {v0, v1, v15}, Lcom/samsung/android/org/xbill/DNS/TSIG;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface/range {v27 .. v28}, Lcom/samsung/android/org/xbill/DNS/Resolver;->setTSIGKey(Lcom/samsung/android/org/xbill/DNS/TSIG;)V
    :try_end_1
    .catch Lcom/samsung/android/org/xbill/DNS/TextParseException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/InterruptedIOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/net/SocketException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    goto/16 :goto_0

    .line 234
    .end local v11    # "is":Ljava/io/InputStream;
    .end local v15    # "keydata":Ljava/lang/String;
    .end local v16    # "keyname":Ljava/lang/String;
    .end local v20    # "operation":Ljava/lang/String;
    .end local v23    # "st":Lcom/samsung/android/org/xbill/DNS/Tokenizer;
    .end local v25    # "token":Lcom/samsung/android/org/xbill/DNS/Tokenizer$Token;
    :catch_1
    move-exception v6

    .line 235
    .local v6, "iioe":Ljava/io/InterruptedIOException;
    sget-object v27, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v28, "Operation timed out"

    invoke-virtual/range {v27 .. v28}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 105
    .end local v6    # "iioe":Ljava/io/InterruptedIOException;
    .restart local v11    # "is":Ljava/io/InputStream;
    .restart local v20    # "operation":Ljava/lang/String;
    .restart local v23    # "st":Lcom/samsung/android/org/xbill/DNS/Tokenizer;
    .restart local v25    # "token":Lcom/samsung/android/org/xbill/DNS/Tokenizer$Token;
    :cond_8
    :try_start_2
    const-string v27, "edns"

    move-object/from16 v0, v20

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_a

    .line 106
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/org/xbill/update;->res:Lcom/samsung/android/org/xbill/DNS/Resolver;

    move-object/from16 v27, v0

    if-nez v27, :cond_9

    .line 107
    new-instance v27, Lcom/samsung/android/org/xbill/DNS/SimpleResolver;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/org/xbill/update;->server:Ljava/lang/String;

    move-object/from16 v28, v0

    invoke-direct/range {v27 .. v28}, Lcom/samsung/android/org/xbill/DNS/SimpleResolver;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/org/xbill/update;->res:Lcom/samsung/android/org/xbill/DNS/Resolver;

    .line 108
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/org/xbill/update;->res:Lcom/samsung/android/org/xbill/DNS/Resolver;

    move-object/from16 v27, v0

    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->getUInt16()I

    move-result v28

    invoke-interface/range {v27 .. v28}, Lcom/samsung/android/org/xbill/DNS/Resolver;->setEDNS(I)V
    :try_end_2
    .catch Lcom/samsung/android/org/xbill/DNS/TextParseException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/InterruptedIOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/net/SocketException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    goto/16 :goto_0

    .line 237
    .end local v11    # "is":Ljava/io/InputStream;
    .end local v20    # "operation":Ljava/lang/String;
    .end local v23    # "st":Lcom/samsung/android/org/xbill/DNS/Tokenizer;
    .end local v25    # "token":Lcom/samsung/android/org/xbill/DNS/Tokenizer$Token;
    :catch_2
    move-exception v22

    .line 238
    .local v22, "se":Ljava/net/SocketException;
    sget-object v27, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v28, "Socket error"

    invoke-virtual/range {v27 .. v28}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 111
    .end local v22    # "se":Ljava/net/SocketException;
    .restart local v11    # "is":Ljava/io/InputStream;
    .restart local v20    # "operation":Ljava/lang/String;
    .restart local v23    # "st":Lcom/samsung/android/org/xbill/DNS/Tokenizer;
    .restart local v25    # "token":Lcom/samsung/android/org/xbill/DNS/Tokenizer$Token;
    :cond_a
    :try_start_3
    const-string v27, "port"

    move-object/from16 v0, v20

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_c

    .line 112
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/org/xbill/update;->res:Lcom/samsung/android/org/xbill/DNS/Resolver;

    move-object/from16 v27, v0

    if-nez v27, :cond_b

    .line 113
    new-instance v27, Lcom/samsung/android/org/xbill/DNS/SimpleResolver;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/org/xbill/update;->server:Ljava/lang/String;

    move-object/from16 v28, v0

    invoke-direct/range {v27 .. v28}, Lcom/samsung/android/org/xbill/DNS/SimpleResolver;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/org/xbill/update;->res:Lcom/samsung/android/org/xbill/DNS/Resolver;

    .line 114
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/org/xbill/update;->res:Lcom/samsung/android/org/xbill/DNS/Resolver;

    move-object/from16 v27, v0

    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->getUInt16()I

    move-result v28

    invoke-interface/range {v27 .. v28}, Lcom/samsung/android/org/xbill/DNS/Resolver;->setPort(I)V
    :try_end_3
    .catch Lcom/samsung/android/org/xbill/DNS/TextParseException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/InterruptedIOException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/net/SocketException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    goto/16 :goto_0

    .line 240
    .end local v11    # "is":Ljava/io/InputStream;
    .end local v20    # "operation":Ljava/lang/String;
    .end local v23    # "st":Lcom/samsung/android/org/xbill/DNS/Tokenizer;
    .end local v25    # "token":Lcom/samsung/android/org/xbill/DNS/Tokenizer$Token;
    :catch_3
    move-exception v10

    .line 241
    .local v10, "ioe":Ljava/io/IOException;
    sget-object v27, Ljava/lang/System;->out:Ljava/io/PrintStream;

    move-object/from16 v0, v27

    invoke-virtual {v0, v10}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 117
    .end local v10    # "ioe":Ljava/io/IOException;
    .restart local v11    # "is":Ljava/io/InputStream;
    .restart local v20    # "operation":Ljava/lang/String;
    .restart local v23    # "st":Lcom/samsung/android/org/xbill/DNS/Tokenizer;
    .restart local v25    # "token":Lcom/samsung/android/org/xbill/DNS/Tokenizer$Token;
    :cond_c
    :try_start_4
    const-string v27, "tcp"

    move-object/from16 v0, v20

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_e

    .line 118
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/org/xbill/update;->res:Lcom/samsung/android/org/xbill/DNS/Resolver;

    move-object/from16 v27, v0

    if-nez v27, :cond_d

    .line 119
    new-instance v27, Lcom/samsung/android/org/xbill/DNS/SimpleResolver;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/org/xbill/update;->server:Ljava/lang/String;

    move-object/from16 v28, v0

    invoke-direct/range {v27 .. v28}, Lcom/samsung/android/org/xbill/DNS/SimpleResolver;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/org/xbill/update;->res:Lcom/samsung/android/org/xbill/DNS/Resolver;

    .line 120
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/org/xbill/update;->res:Lcom/samsung/android/org/xbill/DNS/Resolver;

    move-object/from16 v27, v0

    const/16 v28, 0x1

    invoke-interface/range {v27 .. v28}, Lcom/samsung/android/org/xbill/DNS/Resolver;->setTCP(Z)V

    goto/16 :goto_0

    .line 123
    :cond_e
    const-string v27, "class"

    move-object/from16 v0, v20

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_10

    .line 124
    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->getString()Ljava/lang/String;

    move-result-object v5

    .line 125
    .local v5, "classStr":Ljava/lang/String;
    invoke-static {v5}, Lcom/samsung/android/org/xbill/DNS/DClass;->value(Ljava/lang/String;)I

    move-result v18

    .line 126
    .local v18, "newClass":I
    if-lez v18, :cond_f

    .line 127
    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/org/xbill/update;->defaultClass:I

    goto/16 :goto_0

    .line 129
    :cond_f
    new-instance v27, Ljava/lang/StringBuilder;

    const-string v28, "Invalid class "

    invoke-direct/range {v27 .. v28}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v27

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/samsung/android/org/xbill/update;->print(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 132
    .end local v5    # "classStr":Ljava/lang/String;
    .end local v18    # "newClass":I
    :cond_10
    const-string v27, "ttl"

    move-object/from16 v0, v20

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_11

    .line 133
    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->getTTL()J

    move-result-wide v28

    move-wide/from16 v0, v28

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/samsung/android/org/xbill/update;->defaultTTL:J

    goto/16 :goto_0

    .line 135
    :cond_11
    const-string v27, "origin"

    move-object/from16 v0, v20

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-nez v27, :cond_12

    .line 136
    const-string v27, "zone"

    move-object/from16 v0, v20

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_13

    .line 138
    :cond_12
    sget-object v27, Lcom/samsung/android/org/xbill/DNS/Name;->root:Lcom/samsung/android/org/xbill/DNS/Name;

    move-object/from16 v0, v23

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->getName(Lcom/samsung/android/org/xbill/DNS/Name;)Lcom/samsung/android/org/xbill/DNS/Name;

    move-result-object v27

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/org/xbill/update;->zone:Lcom/samsung/android/org/xbill/DNS/Name;

    goto/16 :goto_0

    .line 141
    :cond_13
    const-string v27, "require"

    move-object/from16 v0, v20

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_14

    .line 142
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/android/org/xbill/update;->doRequire(Lcom/samsung/android/org/xbill/DNS/Tokenizer;)V

    goto/16 :goto_0

    .line 144
    :cond_14
    const-string v27, "prohibit"

    move-object/from16 v0, v20

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_15

    .line 145
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/android/org/xbill/update;->doProhibit(Lcom/samsung/android/org/xbill/DNS/Tokenizer;)V

    goto/16 :goto_0

    .line 147
    :cond_15
    const-string v27, "add"

    move-object/from16 v0, v20

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_16

    .line 148
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/android/org/xbill/update;->doAdd(Lcom/samsung/android/org/xbill/DNS/Tokenizer;)V

    goto/16 :goto_0

    .line 150
    :cond_16
    const-string v27, "delete"

    move-object/from16 v0, v20

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_17

    .line 151
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/android/org/xbill/update;->doDelete(Lcom/samsung/android/org/xbill/DNS/Tokenizer;)V

    goto/16 :goto_0

    .line 153
    :cond_17
    const-string v27, "glue"

    move-object/from16 v0, v20

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_18

    .line 154
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/android/org/xbill/update;->doGlue(Lcom/samsung/android/org/xbill/DNS/Tokenizer;)V

    goto/16 :goto_0

    .line 156
    :cond_18
    const-string v27, "help"

    move-object/from16 v0, v20

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-nez v27, :cond_19

    .line 157
    const-string v27, "?"

    move-object/from16 v0, v20

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_1b

    .line 159
    :cond_19
    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->get()Lcom/samsung/android/org/xbill/DNS/Tokenizer$Token;

    move-result-object v25

    .line 160
    invoke-virtual/range {v25 .. v25}, Lcom/samsung/android/org/xbill/DNS/Tokenizer$Token;->isString()Z

    move-result v27

    if-eqz v27, :cond_1a

    .line 161
    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/samsung/android/org/xbill/DNS/Tokenizer$Token;->value:Ljava/lang/String;

    move-object/from16 v27, v0

    invoke-static/range {v27 .. v27}, Lcom/samsung/android/org/xbill/update;->help(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 163
    :cond_1a
    const/16 v27, 0x0

    invoke-static/range {v27 .. v27}, Lcom/samsung/android/org/xbill/update;->help(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 166
    :cond_1b
    const-string v27, "echo"

    move-object/from16 v0, v20

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_1c

    .line 167
    const/16 v27, 0x4

    move-object/from16 v0, v17

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/samsung/android/org/xbill/update;->print(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 169
    :cond_1c
    const-string v27, "send"

    move-object/from16 v0, v20

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_1d

    .line 170
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/org/xbill/update;->sendUpdate()V

    .line 171
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/org/xbill/update;->newMessage()Lcom/samsung/android/org/xbill/DNS/Message;

    move-result-object v27

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/org/xbill/update;->query:Lcom/samsung/android/org/xbill/DNS/Message;

    goto/16 :goto_0

    .line 174
    :cond_1d
    const-string v27, "show"

    move-object/from16 v0, v20

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_1e

    .line 175
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/org/xbill/update;->query:Lcom/samsung/android/org/xbill/DNS/Message;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/samsung/android/org/xbill/update;->print(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 178
    :cond_1e
    const-string v27, "clear"

    move-object/from16 v0, v20

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_1f

    .line 179
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/org/xbill/update;->newMessage()Lcom/samsung/android/org/xbill/DNS/Message;

    move-result-object v27

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/org/xbill/update;->query:Lcom/samsung/android/org/xbill/DNS/Message;

    goto/16 :goto_0

    .line 181
    :cond_1f
    const-string v27, "query"

    move-object/from16 v0, v20

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_20

    .line 182
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/android/org/xbill/update;->doQuery(Lcom/samsung/android/org/xbill/DNS/Tokenizer;)V

    goto/16 :goto_0

    .line 184
    :cond_20
    const-string v27, "quit"

    move-object/from16 v0, v20

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-nez v27, :cond_21

    .line 185
    const-string v27, "q"

    move-object/from16 v0, v20

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_24

    .line 187
    :cond_21
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/org/xbill/update;->log:Ljava/io/PrintStream;

    move-object/from16 v27, v0

    if-eqz v27, :cond_22

    .line 188
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/org/xbill/update;->log:Ljava/io/PrintStream;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Ljava/io/PrintStream;->close()V

    .line 189
    :cond_22
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    .line 190
    .local v14, "it":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v27

    if-nez v27, :cond_23

    .line 195
    const/16 v27, 0x0

    invoke-static/range {v27 .. v27}, Ljava/lang/System;->exit(I)V

    goto/16 :goto_0

    .line 192
    :cond_23
    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Ljava/io/BufferedReader;

    .line 193
    .local v24, "tbr":Ljava/io/BufferedReader;
    invoke-virtual/range {v24 .. v24}, Ljava/io/BufferedReader;->close()V

    goto :goto_2

    .line 198
    .end local v14    # "it":Ljava/util/Iterator;
    .end local v24    # "tbr":Ljava/io/BufferedReader;
    :cond_24
    const-string v27, "file"

    move-object/from16 v0, v20

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_25

    .line 199
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1, v7, v13}, Lcom/samsung/android/org/xbill/update;->doFile(Lcom/samsung/android/org/xbill/DNS/Tokenizer;Ljava/util/List;Ljava/util/List;)V

    goto/16 :goto_0

    .line 201
    :cond_25
    const-string v27, "log"

    move-object/from16 v0, v20

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_26

    .line 202
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/android/org/xbill/update;->doLog(Lcom/samsung/android/org/xbill/DNS/Tokenizer;)V

    goto/16 :goto_0

    .line 204
    :cond_26
    const-string v27, "assert"

    move-object/from16 v0, v20

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_27

    .line 205
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/android/org/xbill/update;->doAssert(Lcom/samsung/android/org/xbill/DNS/Tokenizer;)Z

    move-result v27

    if-nez v27, :cond_0

    goto/16 :goto_1

    .line 209
    :cond_27
    const-string v27, "sleep"

    move-object/from16 v0, v20

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_28

    .line 210
    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->getUInt32()J
    :try_end_4
    .catch Lcom/samsung/android/org/xbill/DNS/TextParseException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/InterruptedIOException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/net/SocketException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    move-result-wide v8

    .line 212
    .local v8, "interval":J
    :try_start_5
    invoke-static {v8, v9}, Ljava/lang/Thread;->sleep(J)V
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_4
    .catch Lcom/samsung/android/org/xbill/DNS/TextParseException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/io/InterruptedIOException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/net/SocketException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    goto/16 :goto_0

    .line 214
    :catch_4
    move-exception v27

    goto/16 :goto_0

    .line 218
    .end local v8    # "interval":J
    :cond_28
    :try_start_6
    const-string v27, "date"

    move-object/from16 v0, v20

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_2a

    .line 219
    new-instance v19, Ljava/util/Date;

    invoke-direct/range {v19 .. v19}, Ljava/util/Date;-><init>()V

    .line 220
    .local v19, "now":Ljava/util/Date;
    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->get()Lcom/samsung/android/org/xbill/DNS/Tokenizer$Token;

    move-result-object v25

    .line 221
    invoke-virtual/range {v25 .. v25}, Lcom/samsung/android/org/xbill/DNS/Tokenizer$Token;->isString()Z

    move-result v27

    if-eqz v27, :cond_29

    .line 222
    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/samsung/android/org/xbill/DNS/Tokenizer$Token;->value:Ljava/lang/String;

    move-object/from16 v27, v0

    const-string v28, "-ms"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v27

    if-eqz v27, :cond_29

    .line 223
    invoke-virtual/range {v19 .. v19}, Ljava/util/Date;->getTime()J

    move-result-wide v28

    invoke-static/range {v28 .. v29}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/samsung/android/org/xbill/update;->print(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 225
    :cond_29
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/samsung/android/org/xbill/update;->print(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 229
    .end local v19    # "now":Ljava/util/Date;
    :cond_2a
    new-instance v27, Ljava/lang/StringBuilder;

    const-string v28, "invalid keyword: "

    invoke-direct/range {v27 .. v28}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v27

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p0

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/samsung/android/org/xbill/update;->print(Ljava/lang/Object;)V
    :try_end_6
    .catch Lcom/samsung/android/org/xbill/DNS/TextParseException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/InterruptedIOException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/net/SocketException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    goto/16 :goto_0
.end method

.method static help(Ljava/lang/String;)V
    .locals 3
    .param p0, "topic"    # Ljava/lang/String;

    .prologue
    .line 542
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v0}, Ljava/io/PrintStream;->println()V

    .line 543
    if-nez p0, :cond_0

    .line 544
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "The following are supported commands:\nadd      assert   class    clear    date     delete\necho     edns     file     glue     help     key\nlog      port     prohibit query    quit     require\nsend     server   show     sleep    tcp      ttl\nzone     #\n"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 675
    :goto_0
    return-void

    .line 552
    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p0

    .line 554
    const-string v0, "add"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 555
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    .line 556
    const-string v1, "add <name> [ttl] [class] <type> <data>\n\nspecify a record to be added\n"

    .line 555
    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0

    .line 558
    :cond_1
    const-string v0, "assert"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 559
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    .line 560
    const-string v1, "assert <field> <value> [msg]\n\nasserts that the value of the field in the last\nresponse matches the value specified.  If not,\nthe message is printed (if present) and the\nprogram exits.  The field may be any of <rcode>,\n<serial>, <tsig>, <qu>, <an>, <au>, or <ad>.\n"

    .line 559
    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0

    .line 566
    :cond_2
    const-string v0, "class"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 567
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    .line 568
    const-string v1, "class <class>\n\nclass of the zone to be updated (default: IN)\n"

    .line 567
    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0

    .line 570
    :cond_3
    const-string v0, "clear"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 571
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    .line 572
    const-string v1, "clear\n\nclears the current update packet\n"

    .line 571
    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0

    .line 574
    :cond_4
    const-string v0, "date"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 575
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    .line 576
    const-string v1, "date [-ms]\n\nprints the current date and time in human readable\nformat or as the number of milliseconds since the\nepoch"

    .line 575
    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0

    .line 580
    :cond_5
    const-string v0, "delete"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 581
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    .line 582
    const-string v1, "delete <name> [ttl] [class] <type> <data> \ndelete <name> <type> \ndelete <name>\n\nspecify a record or set to be deleted, or that\nall records at a name should be deleted\n"

    .line 581
    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0

    .line 587
    :cond_6
    const-string v0, "echo"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 588
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    .line 589
    const-string v1, "echo <text>\n\nprints the text\n"

    .line 588
    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0

    .line 591
    :cond_7
    const-string v0, "edns"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 592
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    .line 593
    const-string v1, "edns <level>\n\nEDNS level specified when sending messages\n"

    .line 592
    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 595
    :cond_8
    const-string v0, "file"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 596
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    .line 597
    const-string v1, "file <file>\n\nopens the specified file as the new input source\n(- represents stdin)\n"

    .line 596
    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 600
    :cond_9
    const-string v0, "glue"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 601
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    .line 602
    const-string v1, "glue <name> [ttl] [class] <type> <data>\n\nspecify an additional record\n"

    .line 601
    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 604
    :cond_a
    const-string v0, "help"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 605
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    .line 606
    const-string v1, "help\nhelp [topic]\n\nprints a list of commands or help about a specific\ncommand\n"

    .line 605
    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 610
    :cond_b
    const-string v0, "key"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 611
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    .line 612
    const-string v1, "key <name> <data>\n\nTSIG key used to sign messages\n"

    .line 611
    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 614
    :cond_c
    const-string v0, "log"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 615
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    .line 616
    const-string v1, "log <file>\n\nopens the specified file and uses it to log output\n"

    .line 615
    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 618
    :cond_d
    const-string v0, "port"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 619
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    .line 620
    const-string v1, "port <port>\n\nUDP/TCP port messages are sent to (default: 53)\n"

    .line 619
    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 622
    :cond_e
    const-string v0, "prohibit"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 623
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    .line 624
    const-string v1, "prohibit <name> <type> \nprohibit <name>\n\nrequire that a set or name is not present\n"

    .line 623
    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 627
    :cond_f
    const-string v0, "query"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 628
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    .line 629
    const-string v1, "query <name> [type [class]] \n\nissues a query\n"

    .line 628
    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 631
    :cond_10
    const-string v0, "q"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_11

    const-string v0, "quit"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 632
    :cond_11
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    .line 633
    const-string v1, "quit\n\nquits the program\n"

    .line 632
    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 635
    :cond_12
    const-string v0, "require"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 636
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    .line 637
    const-string v1, "require <name> [ttl] [class] <type> <data> \nrequire <name> <type> \nrequire <name>\n\nrequire that a record, set, or name is present\n"

    .line 636
    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 641
    :cond_13
    const-string v0, "send"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 642
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    .line 643
    const-string v1, "send\n\nsends and resets the current update packet\n"

    .line 642
    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 645
    :cond_14
    const-string v0, "server"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 646
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    .line 647
    const-string v1, "server <name> [port]\n\nserver that receives send updates/queries\n"

    .line 646
    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 649
    :cond_15
    const-string v0, "show"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 650
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    .line 651
    const-string v1, "show\n\nshows the current update packet\n"

    .line 650
    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 653
    :cond_16
    const-string v0, "sleep"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 654
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    .line 655
    const-string v1, "sleep <milliseconds>\n\npause for interval before next command\n"

    .line 654
    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 657
    :cond_17
    const-string v0, "tcp"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 658
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    .line 659
    const-string v1, "tcp\n\nTCP should be used to send all messages\n"

    .line 658
    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 661
    :cond_18
    const-string v0, "ttl"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 662
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    .line 663
    const-string v1, "ttl <ttl>\n\ndefault ttl of added records (default: 0)\n"

    .line 662
    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 665
    :cond_19
    const-string v0, "zone"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1a

    const-string v0, "origin"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 666
    :cond_1a
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    .line 667
    const-string v1, "zone <zone>\n\nzone to update (default: .\n"

    .line 666
    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 669
    :cond_1b
    const-string v0, "#"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 670
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    .line 671
    const-string v1, "# <text>\n\na comment\n"

    .line 670
    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 674
    :cond_1c
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Topic \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' unrecognized\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public static main([Ljava/lang/String;)V
    .locals 8
    .param p0, "args"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 680
    const/4 v1, 0x0

    .line 681
    .local v1, "in":Ljava/io/InputStream;
    array-length v4, p0

    if-lt v4, v7, :cond_0

    .line 683
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    const/4 v4, 0x0

    aget-object v4, p0, v4

    invoke-direct {v2, v4}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v1    # "in":Ljava/io/InputStream;
    .local v2, "in":Ljava/io/InputStream;
    move-object v1, v2

    .line 692
    .end local v2    # "in":Ljava/io/InputStream;
    .restart local v1    # "in":Ljava/io/InputStream;
    :goto_0
    new-instance v3, Lcom/samsung/android/org/xbill/update;

    invoke-direct {v3, v1}, Lcom/samsung/android/org/xbill/update;-><init>(Ljava/io/InputStream;)V

    .line 693
    .local v3, "u":Lcom/samsung/android/org/xbill/update;
    return-void

    .line 685
    .end local v3    # "u":Lcom/samsung/android/org/xbill/update;
    :catch_0
    move-exception v0

    .line 686
    .local v0, "e":Ljava/io/FileNotFoundException;
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    aget-object v6, p0, v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, " not found."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 687
    invoke-static {v7}, Ljava/lang/System;->exit(I)V

    goto :goto_0

    .line 691
    .end local v0    # "e":Ljava/io/FileNotFoundException;
    :cond_0
    sget-object v1, Ljava/lang/System;->in:Ljava/io/InputStream;

    goto :goto_0
.end method


# virtual methods
.method doAdd(Lcom/samsung/android/org/xbill/DNS/Tokenizer;)V
    .locals 4
    .param p1, "st"    # Lcom/samsung/android/org/xbill/DNS/Tokenizer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 367
    iget v1, p0, Lcom/samsung/android/org/xbill/update;->defaultClass:I

    iget-wide v2, p0, Lcom/samsung/android/org/xbill/update;->defaultTTL:J

    invoke-virtual {p0, p1, v1, v2, v3}, Lcom/samsung/android/org/xbill/update;->parseRR(Lcom/samsung/android/org/xbill/DNS/Tokenizer;IJ)Lcom/samsung/android/org/xbill/DNS/Record;

    move-result-object v0

    .line 368
    .local v0, "record":Lcom/samsung/android/org/xbill/DNS/Record;
    iget-object v1, p0, Lcom/samsung/android/org/xbill/update;->query:Lcom/samsung/android/org/xbill/DNS/Message;

    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, Lcom/samsung/android/org/xbill/DNS/Message;->addRecord(Lcom/samsung/android/org/xbill/DNS/Record;I)V

    .line 369
    invoke-virtual {p0, v0}, Lcom/samsung/android/org/xbill/update;->print(Ljava/lang/Object;)V

    .line 370
    return-void
.end method

.method doAssert(Lcom/samsung/android/org/xbill/DNS/Tokenizer;)Z
    .locals 14
    .param p1, "st"    # Lcom/samsung/android/org/xbill/DNS/Tokenizer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 474
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->getString()Ljava/lang/String;

    move-result-object v3

    .line 475
    .local v3, "field":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->getString()Ljava/lang/String;

    move-result-object v2

    .line 476
    .local v2, "expected":Ljava/lang/String;
    const/4 v11, 0x0

    .line 477
    .local v11, "value":Ljava/lang/String;
    const/4 v4, 0x1

    .line 480
    .local v4, "flag":Z
    iget-object v12, p0, Lcom/samsung/android/org/xbill/update;->response:Lcom/samsung/android/org/xbill/DNS/Message;

    if-nez v12, :cond_0

    .line 481
    const-string v12, "No response has been received"

    invoke-virtual {p0, v12}, Lcom/samsung/android/org/xbill/update;->print(Ljava/lang/Object;)V

    .line 482
    const/4 v12, 0x1

    .line 537
    :goto_0
    return v12

    .line 484
    :cond_0
    const-string v12, "rcode"

    invoke-virtual {v3, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 485
    iget-object v12, p0, Lcom/samsung/android/org/xbill/update;->response:Lcom/samsung/android/org/xbill/DNS/Message;

    invoke-virtual {v12}, Lcom/samsung/android/org/xbill/DNS/Message;->getHeader()Lcom/samsung/android/org/xbill/DNS/Header;

    move-result-object v12

    invoke-virtual {v12}, Lcom/samsung/android/org/xbill/DNS/Header;->getRcode()I

    move-result v5

    .line 486
    .local v5, "rcode":I
    invoke-static {v2}, Lcom/samsung/android/org/xbill/DNS/Rcode;->value(Ljava/lang/String;)I

    move-result v12

    if-eq v5, v12, :cond_1

    .line 487
    invoke-static {v5}, Lcom/samsung/android/org/xbill/DNS/Rcode;->string(I)Ljava/lang/String;

    move-result-object v11

    .line 488
    const/4 v4, 0x0

    .line 526
    .end local v5    # "rcode":I
    :cond_1
    :goto_1
    if-nez v4, :cond_2

    .line 527
    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "Expected "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    .line 528
    const-string v13, ", received "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 527
    invoke-virtual {p0, v12}, Lcom/samsung/android/org/xbill/update;->print(Ljava/lang/Object;)V

    .line 530
    :goto_2
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->get()Lcom/samsung/android/org/xbill/DNS/Tokenizer$Token;

    move-result-object v10

    .line 531
    .local v10, "token":Lcom/samsung/android/org/xbill/DNS/Tokenizer$Token;
    invoke-virtual {v10}, Lcom/samsung/android/org/xbill/DNS/Tokenizer$Token;->isString()Z

    move-result v12

    if-nez v12, :cond_b

    .line 535
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->unget()V

    .end local v10    # "token":Lcom/samsung/android/org/xbill/DNS/Tokenizer$Token;
    :cond_2
    move v12, v4

    .line 537
    goto :goto_0

    .line 491
    :cond_3
    const-string v12, "serial"

    invoke-virtual {v3, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 492
    iget-object v12, p0, Lcom/samsung/android/org/xbill/update;->response:Lcom/samsung/android/org/xbill/DNS/Message;

    const/4 v13, 0x1

    invoke-virtual {v12, v13}, Lcom/samsung/android/org/xbill/DNS/Message;->getSectionArray(I)[Lcom/samsung/android/org/xbill/DNS/Record;

    move-result-object v0

    .line 493
    .local v0, "answers":[Lcom/samsung/android/org/xbill/DNS/Record;
    array-length v12, v0

    const/4 v13, 0x1

    if-lt v12, v13, :cond_4

    const/4 v12, 0x0

    aget-object v12, v0, v12

    instance-of v12, v12, Lcom/samsung/android/org/xbill/DNS/SOARecord;

    if-nez v12, :cond_5

    .line 494
    :cond_4
    const-string v12, "Invalid response (no SOA)"

    invoke-virtual {p0, v12}, Lcom/samsung/android/org/xbill/update;->print(Ljava/lang/Object;)V

    goto :goto_1

    .line 496
    :cond_5
    const/4 v12, 0x0

    aget-object v7, v0, v12

    check-cast v7, Lcom/samsung/android/org/xbill/DNS/SOARecord;

    .line 497
    .local v7, "soa":Lcom/samsung/android/org/xbill/DNS/SOARecord;
    invoke-virtual {v7}, Lcom/samsung/android/org/xbill/DNS/SOARecord;->getSerial()J

    move-result-wide v8

    .line 498
    .local v8, "serial":J
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v12

    cmp-long v12, v8, v12

    if-eqz v12, :cond_1

    .line 499
    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v11

    .line 500
    const/4 v4, 0x0

    .line 503
    goto :goto_1

    .line 504
    .end local v0    # "answers":[Lcom/samsung/android/org/xbill/DNS/Record;
    .end local v7    # "soa":Lcom/samsung/android/org/xbill/DNS/SOARecord;
    .end local v8    # "serial":J
    :cond_6
    const-string v12, "tsig"

    invoke-virtual {v3, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_9

    .line 505
    iget-object v12, p0, Lcom/samsung/android/org/xbill/update;->response:Lcom/samsung/android/org/xbill/DNS/Message;

    invoke-virtual {v12}, Lcom/samsung/android/org/xbill/DNS/Message;->isSigned()Z

    move-result v12

    if-eqz v12, :cond_8

    .line 506
    iget-object v12, p0, Lcom/samsung/android/org/xbill/update;->response:Lcom/samsung/android/org/xbill/DNS/Message;

    invoke-virtual {v12}, Lcom/samsung/android/org/xbill/DNS/Message;->isVerified()Z

    move-result v12

    if-eqz v12, :cond_7

    .line 507
    const-string v11, "ok"

    .line 513
    :goto_3
    invoke-virtual {v11, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-nez v12, :cond_1

    .line 514
    const/4 v4, 0x0

    .line 515
    goto/16 :goto_1

    .line 509
    :cond_7
    const-string v11, "failed"

    .line 510
    goto :goto_3

    .line 512
    :cond_8
    const-string v11, "unsigned"

    goto :goto_3

    .line 516
    :cond_9
    invoke-static {v3}, Lcom/samsung/android/org/xbill/DNS/Section;->value(Ljava/lang/String;)I

    move-result v6

    .local v6, "section":I
    if-ltz v6, :cond_a

    .line 517
    iget-object v12, p0, Lcom/samsung/android/org/xbill/update;->response:Lcom/samsung/android/org/xbill/DNS/Message;

    invoke-virtual {v12}, Lcom/samsung/android/org/xbill/DNS/Message;->getHeader()Lcom/samsung/android/org/xbill/DNS/Header;

    move-result-object v12

    invoke-virtual {v12, v6}, Lcom/samsung/android/org/xbill/DNS/Header;->getCount(I)I

    move-result v1

    .line 518
    .local v1, "count":I
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v12

    if-eq v1, v12, :cond_1

    .line 519
    new-instance v12, Ljava/lang/Integer;

    invoke-direct {v12, v1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v12}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v11

    .line 520
    const/4 v4, 0x0

    .line 522
    goto/16 :goto_1

    .line 524
    .end local v1    # "count":I
    :cond_a
    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "Invalid assertion keyword: "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {p0, v12}, Lcom/samsung/android/org/xbill/update;->print(Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 533
    .end local v6    # "section":I
    .restart local v10    # "token":Lcom/samsung/android/org/xbill/DNS/Tokenizer$Token;
    :cond_b
    iget-object v12, v10, Lcom/samsung/android/org/xbill/DNS/Tokenizer$Token;->value:Ljava/lang/String;

    invoke-virtual {p0, v12}, Lcom/samsung/android/org/xbill/update;->print(Ljava/lang/Object;)V

    goto/16 :goto_2
.end method

.method doDelete(Lcom/samsung/android/org/xbill/DNS/Tokenizer;)V
    .locals 11
    .param p1, "st"    # Lcom/samsung/android/org/xbill/DNS/Tokenizer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x0

    const/16 v6, 0xff

    .line 380
    iget-object v3, p0, Lcom/samsung/android/org/xbill/update;->zone:Lcom/samsung/android/org/xbill/DNS/Name;

    invoke-virtual {p1, v3}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->getName(Lcom/samsung/android/org/xbill/DNS/Name;)Lcom/samsung/android/org/xbill/DNS/Name;

    move-result-object v1

    .line 381
    .local v1, "name":Lcom/samsung/android/org/xbill/DNS/Name;
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->get()Lcom/samsung/android/org/xbill/DNS/Tokenizer$Token;

    move-result-object v10

    .line 382
    .local v10, "token":Lcom/samsung/android/org/xbill/DNS/Tokenizer$Token;
    invoke-virtual {v10}, Lcom/samsung/android/org/xbill/DNS/Tokenizer$Token;->isString()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 383
    iget-object v9, v10, Lcom/samsung/android/org/xbill/DNS/Tokenizer$Token;->value:Ljava/lang/String;

    .line 384
    .local v9, "s":Ljava/lang/String;
    invoke-static {v9}, Lcom/samsung/android/org/xbill/DNS/DClass;->value(Ljava/lang/String;)I

    move-result v3

    if-ltz v3, :cond_0

    .line 385
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->getString()Ljava/lang/String;

    move-result-object v9

    .line 387
    :cond_0
    invoke-static {v9}, Lcom/samsung/android/org/xbill/DNS/Type;->value(Ljava/lang/String;)I

    move-result v2

    .local v2, "type":I
    if-gez v2, :cond_1

    .line 388
    new-instance v3, Ljava/io/IOException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Invalid type: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 389
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->get()Lcom/samsung/android/org/xbill/DNS/Tokenizer$Token;

    move-result-object v10

    .line 390
    invoke-virtual {v10}, Lcom/samsung/android/org/xbill/DNS/Tokenizer$Token;->isEOL()Z

    move-result v0

    .line 391
    .local v0, "iseol":Z
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->unget()V

    .line 392
    if-nez v0, :cond_2

    .line 393
    const/16 v3, 0xfe

    .line 394
    iget-object v7, p0, Lcom/samsung/android/org/xbill/update;->zone:Lcom/samsung/android/org/xbill/DNS/Name;

    move-object v6, p1

    .line 393
    invoke-static/range {v1 .. v7}, Lcom/samsung/android/org/xbill/DNS/Record;->fromString(Lcom/samsung/android/org/xbill/DNS/Name;IIJLcom/samsung/android/org/xbill/DNS/Tokenizer;Lcom/samsung/android/org/xbill/DNS/Name;)Lcom/samsung/android/org/xbill/DNS/Record;

    move-result-object v8

    .line 401
    .end local v0    # "iseol":Z
    .end local v2    # "type":I
    .end local v9    # "s":Ljava/lang/String;
    .local v8, "record":Lcom/samsung/android/org/xbill/DNS/Record;
    :goto_0
    iget-object v3, p0, Lcom/samsung/android/org/xbill/update;->query:Lcom/samsung/android/org/xbill/DNS/Message;

    const/4 v4, 0x2

    invoke-virtual {v3, v8, v4}, Lcom/samsung/android/org/xbill/DNS/Message;->addRecord(Lcom/samsung/android/org/xbill/DNS/Record;I)V

    .line 402
    invoke-virtual {p0, v8}, Lcom/samsung/android/org/xbill/update;->print(Ljava/lang/Object;)V

    .line 403
    return-void

    .line 396
    .end local v8    # "record":Lcom/samsung/android/org/xbill/DNS/Record;
    .restart local v0    # "iseol":Z
    .restart local v2    # "type":I
    .restart local v9    # "s":Ljava/lang/String;
    :cond_2
    invoke-static {v1, v2, v6, v4, v5}, Lcom/samsung/android/org/xbill/DNS/Record;->newRecord(Lcom/samsung/android/org/xbill/DNS/Name;IIJ)Lcom/samsung/android/org/xbill/DNS/Record;

    move-result-object v8

    .line 397
    .restart local v8    # "record":Lcom/samsung/android/org/xbill/DNS/Record;
    goto :goto_0

    .line 399
    .end local v0    # "iseol":Z
    .end local v2    # "type":I
    .end local v8    # "record":Lcom/samsung/android/org/xbill/DNS/Record;
    .end local v9    # "s":Ljava/lang/String;
    :cond_3
    invoke-static {v1, v6, v6, v4, v5}, Lcom/samsung/android/org/xbill/DNS/Record;->newRecord(Lcom/samsung/android/org/xbill/DNS/Name;IIJ)Lcom/samsung/android/org/xbill/DNS/Record;

    move-result-object v8

    .restart local v8    # "record":Lcom/samsung/android/org/xbill/DNS/Record;
    goto :goto_0
.end method

.method doFile(Lcom/samsung/android/org/xbill/DNS/Tokenizer;Ljava/util/List;Ljava/util/List;)V
    .locals 6
    .param p1, "st"    # Lcom/samsung/android/org/xbill/DNS/Tokenizer;
    .param p2, "inputs"    # Ljava/util/List;
    .param p3, "istreams"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 445
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->getString()Ljava/lang/String;

    move-result-object v2

    .line 448
    .local v2, "s":Ljava/lang/String;
    :try_start_0
    const-string v3, "-"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 449
    sget-object v1, Ljava/lang/System;->in:Ljava/io/InputStream;

    .line 452
    .local v1, "is":Ljava/io/InputStream;
    :goto_0
    const/4 v3, 0x0

    invoke-interface {p3, v3, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 453
    const/4 v3, 0x0

    new-instance v4, Ljava/io/BufferedReader;

    new-instance v5, Ljava/io/InputStreamReader;

    invoke-direct {v5, v1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v4, v5}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    invoke-interface {p2, v3, v4}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 458
    .end local v1    # "is":Ljava/io/InputStream;
    :goto_1
    return-void

    .line 451
    :cond_0
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, v2}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .restart local v1    # "is":Ljava/io/InputStream;
    goto :goto_0

    .line 455
    .end local v1    # "is":Ljava/io/InputStream;
    :catch_0
    move-exception v0

    .line 456
    .local v0, "e":Ljava/io/FileNotFoundException;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, " not found"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/samsung/android/org/xbill/update;->print(Ljava/lang/Object;)V

    goto :goto_1
.end method

.method doGlue(Lcom/samsung/android/org/xbill/DNS/Tokenizer;)V
    .locals 4
    .param p1, "st"    # Lcom/samsung/android/org/xbill/DNS/Tokenizer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 407
    iget v1, p0, Lcom/samsung/android/org/xbill/update;->defaultClass:I

    iget-wide v2, p0, Lcom/samsung/android/org/xbill/update;->defaultTTL:J

    invoke-virtual {p0, p1, v1, v2, v3}, Lcom/samsung/android/org/xbill/update;->parseRR(Lcom/samsung/android/org/xbill/DNS/Tokenizer;IJ)Lcom/samsung/android/org/xbill/DNS/Record;

    move-result-object v0

    .line 408
    .local v0, "record":Lcom/samsung/android/org/xbill/DNS/Record;
    iget-object v1, p0, Lcom/samsung/android/org/xbill/update;->query:Lcom/samsung/android/org/xbill/DNS/Message;

    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, Lcom/samsung/android/org/xbill/DNS/Message;->addRecord(Lcom/samsung/android/org/xbill/DNS/Record;I)V

    .line 409
    invoke-virtual {p0, v0}, Lcom/samsung/android/org/xbill/update;->print(Ljava/lang/Object;)V

    .line 410
    return-void
.end method

.method doLog(Lcom/samsung/android/org/xbill/DNS/Tokenizer;)V
    .locals 5
    .param p1, "st"    # Lcom/samsung/android/org/xbill/DNS/Tokenizer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 462
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->getString()Ljava/lang/String;

    move-result-object v2

    .line 464
    .local v2, "s":Ljava/lang/String;
    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 465
    .local v1, "fos":Ljava/io/FileOutputStream;
    new-instance v3, Ljava/io/PrintStream;

    invoke-direct {v3, v1}, Ljava/io/PrintStream;-><init>(Ljava/io/OutputStream;)V

    iput-object v3, p0, Lcom/samsung/android/org/xbill/update;->log:Ljava/io/PrintStream;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 470
    .end local v1    # "fos":Ljava/io/FileOutputStream;
    :goto_0
    return-void

    .line 467
    :catch_0
    move-exception v0

    .line 468
    .local v0, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Error opening "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/samsung/android/org/xbill/update;->print(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method doProhibit(Lcom/samsung/android/org/xbill/DNS/Tokenizer;)V
    .locals 8
    .param p1, "st"    # Lcom/samsung/android/org/xbill/DNS/Tokenizer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 353
    iget-object v4, p0, Lcom/samsung/android/org/xbill/update;->zone:Lcom/samsung/android/org/xbill/DNS/Name;

    invoke-virtual {p1, v4}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->getName(Lcom/samsung/android/org/xbill/DNS/Name;)Lcom/samsung/android/org/xbill/DNS/Name;

    move-result-object v0

    .line 354
    .local v0, "name":Lcom/samsung/android/org/xbill/DNS/Name;
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->get()Lcom/samsung/android/org/xbill/DNS/Tokenizer$Token;

    move-result-object v2

    .line 355
    .local v2, "token":Lcom/samsung/android/org/xbill/DNS/Tokenizer$Token;
    invoke-virtual {v2}, Lcom/samsung/android/org/xbill/DNS/Tokenizer$Token;->isString()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 356
    iget-object v4, v2, Lcom/samsung/android/org/xbill/DNS/Tokenizer$Token;->value:Ljava/lang/String;

    invoke-static {v4}, Lcom/samsung/android/org/xbill/DNS/Type;->value(Ljava/lang/String;)I

    move-result v3

    .local v3, "type":I
    if-gez v3, :cond_1

    .line 357
    new-instance v4, Ljava/io/IOException;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Invalid type: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, v2, Lcom/samsung/android/org/xbill/DNS/Tokenizer$Token;->value:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 359
    .end local v3    # "type":I
    :cond_0
    const/16 v3, 0xff

    .line 360
    .restart local v3    # "type":I
    :cond_1
    const/16 v4, 0xfe

    const-wide/16 v6, 0x0

    invoke-static {v0, v3, v4, v6, v7}, Lcom/samsung/android/org/xbill/DNS/Record;->newRecord(Lcom/samsung/android/org/xbill/DNS/Name;IIJ)Lcom/samsung/android/org/xbill/DNS/Record;

    move-result-object v1

    .line 361
    .local v1, "record":Lcom/samsung/android/org/xbill/DNS/Record;
    iget-object v4, p0, Lcom/samsung/android/org/xbill/update;->query:Lcom/samsung/android/org/xbill/DNS/Message;

    const/4 v5, 0x1

    invoke-virtual {v4, v1, v5}, Lcom/samsung/android/org/xbill/DNS/Message;->addRecord(Lcom/samsung/android/org/xbill/DNS/Record;I)V

    .line 362
    invoke-virtual {p0, v1}, Lcom/samsung/android/org/xbill/update;->print(Ljava/lang/Object;)V

    .line 363
    return-void
.end method

.method doQuery(Lcom/samsung/android/org/xbill/DNS/Tokenizer;)V
    .locals 8
    .param p1, "st"    # Lcom/samsung/android/org/xbill/DNS/Tokenizer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 417
    const/4 v1, 0x0

    .line 418
    .local v1, "name":Lcom/samsung/android/org/xbill/DNS/Name;
    const/4 v5, 0x1

    .line 419
    .local v5, "type":I
    iget v0, p0, Lcom/samsung/android/org/xbill/update;->defaultClass:I

    .line 421
    .local v0, "dclass":I
    iget-object v6, p0, Lcom/samsung/android/org/xbill/update;->zone:Lcom/samsung/android/org/xbill/DNS/Name;

    invoke-virtual {p1, v6}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->getName(Lcom/samsung/android/org/xbill/DNS/Name;)Lcom/samsung/android/org/xbill/DNS/Name;

    move-result-object v1

    .line 422
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->get()Lcom/samsung/android/org/xbill/DNS/Tokenizer$Token;

    move-result-object v4

    .line 423
    .local v4, "token":Lcom/samsung/android/org/xbill/DNS/Tokenizer$Token;
    invoke-virtual {v4}, Lcom/samsung/android/org/xbill/DNS/Tokenizer$Token;->isString()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 424
    iget-object v6, v4, Lcom/samsung/android/org/xbill/DNS/Tokenizer$Token;->value:Ljava/lang/String;

    invoke-static {v6}, Lcom/samsung/android/org/xbill/DNS/Type;->value(Ljava/lang/String;)I

    move-result v5

    .line 425
    if-gez v5, :cond_0

    .line 426
    new-instance v6, Ljava/io/IOException;

    const-string v7, "Invalid type"

    invoke-direct {v6, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 427
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->get()Lcom/samsung/android/org/xbill/DNS/Tokenizer$Token;

    move-result-object v4

    .line 428
    invoke-virtual {v4}, Lcom/samsung/android/org/xbill/DNS/Tokenizer$Token;->isString()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 429
    iget-object v6, v4, Lcom/samsung/android/org/xbill/DNS/Tokenizer$Token;->value:Ljava/lang/String;

    invoke-static {v6}, Lcom/samsung/android/org/xbill/DNS/DClass;->value(Ljava/lang/String;)I

    move-result v0

    .line 430
    if-gez v0, :cond_1

    .line 431
    new-instance v6, Ljava/io/IOException;

    const-string v7, "Invalid class"

    invoke-direct {v6, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 435
    :cond_1
    invoke-static {v1, v5, v0}, Lcom/samsung/android/org/xbill/DNS/Record;->newRecord(Lcom/samsung/android/org/xbill/DNS/Name;II)Lcom/samsung/android/org/xbill/DNS/Record;

    move-result-object v3

    .line 436
    .local v3, "rec":Lcom/samsung/android/org/xbill/DNS/Record;
    invoke-static {v3}, Lcom/samsung/android/org/xbill/DNS/Message;->newQuery(Lcom/samsung/android/org/xbill/DNS/Record;)Lcom/samsung/android/org/xbill/DNS/Message;

    move-result-object v2

    .line 437
    .local v2, "newQuery":Lcom/samsung/android/org/xbill/DNS/Message;
    iget-object v6, p0, Lcom/samsung/android/org/xbill/update;->res:Lcom/samsung/android/org/xbill/DNS/Resolver;

    if-nez v6, :cond_2

    .line 438
    new-instance v6, Lcom/samsung/android/org/xbill/DNS/SimpleResolver;

    iget-object v7, p0, Lcom/samsung/android/org/xbill/update;->server:Ljava/lang/String;

    invoke-direct {v6, v7}, Lcom/samsung/android/org/xbill/DNS/SimpleResolver;-><init>(Ljava/lang/String;)V

    iput-object v6, p0, Lcom/samsung/android/org/xbill/update;->res:Lcom/samsung/android/org/xbill/DNS/Resolver;

    .line 439
    :cond_2
    iget-object v6, p0, Lcom/samsung/android/org/xbill/update;->res:Lcom/samsung/android/org/xbill/DNS/Resolver;

    invoke-interface {v6, v2}, Lcom/samsung/android/org/xbill/DNS/Resolver;->send(Lcom/samsung/android/org/xbill/DNS/Message;)Lcom/samsung/android/org/xbill/DNS/Message;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/android/org/xbill/update;->response:Lcom/samsung/android/org/xbill/DNS/Message;

    .line 440
    iget-object v6, p0, Lcom/samsung/android/org/xbill/update;->response:Lcom/samsung/android/org/xbill/DNS/Message;

    invoke-virtual {p0, v6}, Lcom/samsung/android/org/xbill/update;->print(Ljava/lang/Object;)V

    .line 441
    return-void
.end method

.method doRequire(Lcom/samsung/android/org/xbill/DNS/Tokenizer;)V
    .locals 10
    .param p1, "st"    # Lcom/samsung/android/org/xbill/DNS/Tokenizer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x0

    const/16 v6, 0xff

    .line 325
    iget-object v3, p0, Lcom/samsung/android/org/xbill/update;->zone:Lcom/samsung/android/org/xbill/DNS/Name;

    invoke-virtual {p1, v3}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->getName(Lcom/samsung/android/org/xbill/DNS/Name;)Lcom/samsung/android/org/xbill/DNS/Name;

    move-result-object v1

    .line 326
    .local v1, "name":Lcom/samsung/android/org/xbill/DNS/Name;
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->get()Lcom/samsung/android/org/xbill/DNS/Tokenizer$Token;

    move-result-object v9

    .line 327
    .local v9, "token":Lcom/samsung/android/org/xbill/DNS/Tokenizer$Token;
    invoke-virtual {v9}, Lcom/samsung/android/org/xbill/DNS/Tokenizer$Token;->isString()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 328
    iget-object v3, v9, Lcom/samsung/android/org/xbill/DNS/Tokenizer$Token;->value:Ljava/lang/String;

    invoke-static {v3}, Lcom/samsung/android/org/xbill/DNS/Type;->value(Ljava/lang/String;)I

    move-result v2

    .local v2, "type":I
    if-gez v2, :cond_0

    .line 329
    new-instance v3, Ljava/io/IOException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Invalid type: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, v9, Lcom/samsung/android/org/xbill/DNS/Tokenizer$Token;->value:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 330
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->get()Lcom/samsung/android/org/xbill/DNS/Tokenizer$Token;

    move-result-object v9

    .line 331
    invoke-virtual {v9}, Lcom/samsung/android/org/xbill/DNS/Tokenizer$Token;->isEOL()Z

    move-result v0

    .line 332
    .local v0, "iseol":Z
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->unget()V

    .line 333
    if-nez v0, :cond_1

    .line 334
    iget v3, p0, Lcom/samsung/android/org/xbill/update;->defaultClass:I

    .line 335
    iget-object v7, p0, Lcom/samsung/android/org/xbill/update;->zone:Lcom/samsung/android/org/xbill/DNS/Name;

    move-object v6, p1

    .line 334
    invoke-static/range {v1 .. v7}, Lcom/samsung/android/org/xbill/DNS/Record;->fromString(Lcom/samsung/android/org/xbill/DNS/Name;IIJLcom/samsung/android/org/xbill/DNS/Tokenizer;Lcom/samsung/android/org/xbill/DNS/Name;)Lcom/samsung/android/org/xbill/DNS/Record;

    move-result-object v8

    .line 342
    .end local v0    # "iseol":Z
    .end local v2    # "type":I
    .local v8, "record":Lcom/samsung/android/org/xbill/DNS/Record;
    :goto_0
    iget-object v3, p0, Lcom/samsung/android/org/xbill/update;->query:Lcom/samsung/android/org/xbill/DNS/Message;

    const/4 v4, 0x1

    invoke-virtual {v3, v8, v4}, Lcom/samsung/android/org/xbill/DNS/Message;->addRecord(Lcom/samsung/android/org/xbill/DNS/Record;I)V

    .line 343
    invoke-virtual {p0, v8}, Lcom/samsung/android/org/xbill/update;->print(Ljava/lang/Object;)V

    .line 344
    return-void

    .line 337
    .end local v8    # "record":Lcom/samsung/android/org/xbill/DNS/Record;
    .restart local v0    # "iseol":Z
    .restart local v2    # "type":I
    :cond_1
    invoke-static {v1, v2, v6, v4, v5}, Lcom/samsung/android/org/xbill/DNS/Record;->newRecord(Lcom/samsung/android/org/xbill/DNS/Name;IIJ)Lcom/samsung/android/org/xbill/DNS/Record;

    move-result-object v8

    .line 339
    .restart local v8    # "record":Lcom/samsung/android/org/xbill/DNS/Record;
    goto :goto_0

    .line 340
    .end local v0    # "iseol":Z
    .end local v2    # "type":I
    .end local v8    # "record":Lcom/samsung/android/org/xbill/DNS/Record;
    :cond_2
    invoke-static {v1, v6, v6, v4, v5}, Lcom/samsung/android/org/xbill/DNS/Record;->newRecord(Lcom/samsung/android/org/xbill/DNS/Name;IIJ)Lcom/samsung/android/org/xbill/DNS/Record;

    move-result-object v8

    .restart local v8    # "record":Lcom/samsung/android/org/xbill/DNS/Record;
    goto :goto_0
.end method

.method public newMessage()Lcom/samsung/android/org/xbill/DNS/Message;
    .locals 3

    .prologue
    .line 31
    new-instance v0, Lcom/samsung/android/org/xbill/DNS/Message;

    invoke-direct {v0}, Lcom/samsung/android/org/xbill/DNS/Message;-><init>()V

    .line 32
    .local v0, "msg":Lcom/samsung/android/org/xbill/DNS/Message;
    invoke-virtual {v0}, Lcom/samsung/android/org/xbill/DNS/Message;->getHeader()Lcom/samsung/android/org/xbill/DNS/Header;

    move-result-object v1

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Lcom/samsung/android/org/xbill/DNS/Header;->setOpcode(I)V

    .line 33
    return-object v0
.end method

.method parseRR(Lcom/samsung/android/org/xbill/DNS/Tokenizer;IJ)Lcom/samsung/android/org/xbill/DNS/Record;
    .locals 11
    .param p1, "st"    # Lcom/samsung/android/org/xbill/DNS/Tokenizer;
    .param p2, "classValue"    # I
    .param p3, "TTLValue"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 288
    iget-object v3, p0, Lcom/samsung/android/org/xbill/update;->zone:Lcom/samsung/android/org/xbill/DNS/Name;

    invoke-virtual {p1, v3}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->getName(Lcom/samsung/android/org/xbill/DNS/Name;)Lcom/samsung/android/org/xbill/DNS/Name;

    move-result-object v1

    .line 293
    .local v1, "name":Lcom/samsung/android/org/xbill/DNS/Name;
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->getString()Ljava/lang/String;

    move-result-object v9

    .line 296
    .local v9, "s":Ljava/lang/String;
    :try_start_0
    invoke-static {v9}, Lcom/samsung/android/org/xbill/DNS/TTL;->parseTTL(Ljava/lang/String;)J

    move-result-wide v4

    .line 297
    .local v4, "ttl":J
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->getString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v9

    .line 303
    :goto_0
    invoke-static {v9}, Lcom/samsung/android/org/xbill/DNS/DClass;->value(Ljava/lang/String;)I

    move-result v3

    if-ltz v3, :cond_0

    .line 304
    invoke-static {v9}, Lcom/samsung/android/org/xbill/DNS/DClass;->value(Ljava/lang/String;)I

    move-result p2

    .line 305
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->getString()Ljava/lang/String;

    move-result-object v9

    .line 308
    :cond_0
    invoke-static {v9}, Lcom/samsung/android/org/xbill/DNS/Type;->value(Ljava/lang/String;)I

    move-result v2

    .local v2, "type":I
    if-gez v2, :cond_1

    .line 309
    new-instance v3, Ljava/io/IOException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Invalid type: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 299
    .end local v2    # "type":I
    .end local v4    # "ttl":J
    :catch_0
    move-exception v0

    .line 300
    .local v0, "e":Ljava/lang/NumberFormatException;
    move-wide v4, p3

    .restart local v4    # "ttl":J
    goto :goto_0

    .line 311
    .end local v0    # "e":Ljava/lang/NumberFormatException;
    .restart local v2    # "type":I
    :cond_1
    iget-object v7, p0, Lcom/samsung/android/org/xbill/update;->zone:Lcom/samsung/android/org/xbill/DNS/Name;

    move v3, p2

    move-object v6, p1

    invoke-static/range {v1 .. v7}, Lcom/samsung/android/org/xbill/DNS/Record;->fromString(Lcom/samsung/android/org/xbill/DNS/Name;IIJLcom/samsung/android/org/xbill/DNS/Tokenizer;Lcom/samsung/android/org/xbill/DNS/Name;)Lcom/samsung/android/org/xbill/DNS/Record;

    move-result-object v8

    .line 312
    .local v8, "record":Lcom/samsung/android/org/xbill/DNS/Record;
    if-eqz v8, :cond_2

    .line 313
    return-object v8

    .line 315
    :cond_2
    new-instance v3, Ljava/io/IOException;

    const-string v6, "Parse error"

    invoke-direct {v3, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method print(Ljava/lang/Object;)V
    .locals 1
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 24
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v0, p1}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    .line 25
    iget-object v0, p0, Lcom/samsung/android/org/xbill/update;->log:Ljava/io/PrintStream;

    if-eqz v0, :cond_0

    .line 26
    iget-object v0, p0, Lcom/samsung/android/org/xbill/update;->log:Ljava/io/PrintStream;

    invoke-virtual {v0, p1}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    .line 27
    :cond_0
    return-void
.end method

.method sendUpdate()V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x2

    const/4 v7, 0x0

    .line 248
    iget-object v5, p0, Lcom/samsung/android/org/xbill/update;->query:Lcom/samsung/android/org/xbill/DNS/Message;

    invoke-virtual {v5}, Lcom/samsung/android/org/xbill/DNS/Message;->getHeader()Lcom/samsung/android/org/xbill/DNS/Header;

    move-result-object v5

    invoke-virtual {v5, v6}, Lcom/samsung/android/org/xbill/DNS/Header;->getCount(I)I

    move-result v5

    if-nez v5, :cond_0

    .line 249
    const-string v5, "Empty update message.  Ignoring."

    invoke-virtual {p0, v5}, Lcom/samsung/android/org/xbill/update;->print(Ljava/lang/Object;)V

    .line 278
    :goto_0
    return-void

    .line 252
    :cond_0
    iget-object v5, p0, Lcom/samsung/android/org/xbill/update;->query:Lcom/samsung/android/org/xbill/DNS/Message;

    invoke-virtual {v5}, Lcom/samsung/android/org/xbill/DNS/Message;->getHeader()Lcom/samsung/android/org/xbill/DNS/Header;

    move-result-object v5

    invoke-virtual {v5, v7}, Lcom/samsung/android/org/xbill/DNS/Header;->getCount(I)I

    move-result v5

    if-nez v5, :cond_2

    .line 254
    iget-object v4, p0, Lcom/samsung/android/org/xbill/update;->zone:Lcom/samsung/android/org/xbill/DNS/Name;

    .line 255
    .local v4, "updzone":Lcom/samsung/android/org/xbill/DNS/Name;
    iget v0, p0, Lcom/samsung/android/org/xbill/update;->defaultClass:I

    .line 256
    .local v0, "dclass":I
    if-nez v4, :cond_1

    .line 257
    iget-object v5, p0, Lcom/samsung/android/org/xbill/update;->query:Lcom/samsung/android/org/xbill/DNS/Message;

    invoke-virtual {v5, v6}, Lcom/samsung/android/org/xbill/DNS/Message;->getSectionArray(I)[Lcom/samsung/android/org/xbill/DNS/Record;

    move-result-object v2

    .line 258
    .local v2, "recs":[Lcom/samsung/android/org/xbill/DNS/Record;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    array-length v5, v2

    if-lt v1, v5, :cond_4

    .line 270
    .end local v1    # "i":I
    .end local v2    # "recs":[Lcom/samsung/android/org/xbill/DNS/Record;
    :cond_1
    :goto_2
    const/4 v5, 0x6

    invoke-static {v4, v5, v0}, Lcom/samsung/android/org/xbill/DNS/Record;->newRecord(Lcom/samsung/android/org/xbill/DNS/Name;II)Lcom/samsung/android/org/xbill/DNS/Record;

    move-result-object v3

    .line 271
    .local v3, "soa":Lcom/samsung/android/org/xbill/DNS/Record;
    iget-object v5, p0, Lcom/samsung/android/org/xbill/update;->query:Lcom/samsung/android/org/xbill/DNS/Message;

    invoke-virtual {v5, v3, v7}, Lcom/samsung/android/org/xbill/DNS/Message;->addRecord(Lcom/samsung/android/org/xbill/DNS/Record;I)V

    .line 274
    .end local v0    # "dclass":I
    .end local v3    # "soa":Lcom/samsung/android/org/xbill/DNS/Record;
    .end local v4    # "updzone":Lcom/samsung/android/org/xbill/DNS/Name;
    :cond_2
    iget-object v5, p0, Lcom/samsung/android/org/xbill/update;->res:Lcom/samsung/android/org/xbill/DNS/Resolver;

    if-nez v5, :cond_3

    .line 275
    new-instance v5, Lcom/samsung/android/org/xbill/DNS/SimpleResolver;

    iget-object v6, p0, Lcom/samsung/android/org/xbill/update;->server:Ljava/lang/String;

    invoke-direct {v5, v6}, Lcom/samsung/android/org/xbill/DNS/SimpleResolver;-><init>(Ljava/lang/String;)V

    iput-object v5, p0, Lcom/samsung/android/org/xbill/update;->res:Lcom/samsung/android/org/xbill/DNS/Resolver;

    .line 276
    :cond_3
    iget-object v5, p0, Lcom/samsung/android/org/xbill/update;->res:Lcom/samsung/android/org/xbill/DNS/Resolver;

    iget-object v6, p0, Lcom/samsung/android/org/xbill/update;->query:Lcom/samsung/android/org/xbill/DNS/Message;

    invoke-interface {v5, v6}, Lcom/samsung/android/org/xbill/DNS/Resolver;->send(Lcom/samsung/android/org/xbill/DNS/Message;)Lcom/samsung/android/org/xbill/DNS/Message;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/org/xbill/update;->response:Lcom/samsung/android/org/xbill/DNS/Message;

    .line 277
    iget-object v5, p0, Lcom/samsung/android/org/xbill/update;->response:Lcom/samsung/android/org/xbill/DNS/Message;

    invoke-virtual {p0, v5}, Lcom/samsung/android/org/xbill/update;->print(Ljava/lang/Object;)V

    goto :goto_0

    .line 259
    .restart local v0    # "dclass":I
    .restart local v1    # "i":I
    .restart local v2    # "recs":[Lcom/samsung/android/org/xbill/DNS/Record;
    .restart local v4    # "updzone":Lcom/samsung/android/org/xbill/DNS/Name;
    :cond_4
    if-nez v4, :cond_5

    .line 260
    new-instance v4, Lcom/samsung/android/org/xbill/DNS/Name;

    .end local v4    # "updzone":Lcom/samsung/android/org/xbill/DNS/Name;
    aget-object v5, v2, v1

    invoke-virtual {v5}, Lcom/samsung/android/org/xbill/DNS/Record;->getName()Lcom/samsung/android/org/xbill/DNS/Name;

    move-result-object v5

    .line 261
    const/4 v6, 0x1

    .line 260
    invoke-direct {v4, v5, v6}, Lcom/samsung/android/org/xbill/DNS/Name;-><init>(Lcom/samsung/android/org/xbill/DNS/Name;I)V

    .line 262
    .restart local v4    # "updzone":Lcom/samsung/android/org/xbill/DNS/Name;
    :cond_5
    aget-object v5, v2, v1

    invoke-virtual {v5}, Lcom/samsung/android/org/xbill/DNS/Record;->getDClass()I

    move-result v5

    const/16 v6, 0xfe

    if-eq v5, v6, :cond_6

    .line 263
    aget-object v5, v2, v1

    invoke-virtual {v5}, Lcom/samsung/android/org/xbill/DNS/Record;->getDClass()I

    move-result v5

    const/16 v6, 0xff

    if-eq v5, v6, :cond_6

    .line 265
    aget-object v5, v2, v1

    invoke-virtual {v5}, Lcom/samsung/android/org/xbill/DNS/Record;->getDClass()I

    move-result v0

    .line 266
    goto :goto_2

    .line 258
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method
