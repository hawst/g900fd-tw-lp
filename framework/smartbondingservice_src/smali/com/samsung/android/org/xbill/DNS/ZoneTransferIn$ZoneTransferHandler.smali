.class public interface abstract Lcom/samsung/android/org/xbill/DNS/ZoneTransferIn$ZoneTransferHandler;
.super Ljava/lang/Object;
.source "ZoneTransferIn.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/org/xbill/DNS/ZoneTransferIn;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ZoneTransferHandler"
.end annotation


# virtual methods
.method public abstract handleRecord(Lcom/samsung/android/org/xbill/DNS/Record;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/org/xbill/DNS/ZoneTransferException;
        }
    .end annotation
.end method

.method public abstract startAXFR()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/org/xbill/DNS/ZoneTransferException;
        }
    .end annotation
.end method

.method public abstract startIXFR()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/org/xbill/DNS/ZoneTransferException;
        }
    .end annotation
.end method

.method public abstract startIXFRAdds(Lcom/samsung/android/org/xbill/DNS/Record;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/org/xbill/DNS/ZoneTransferException;
        }
    .end annotation
.end method

.method public abstract startIXFRDeletes(Lcom/samsung/android/org/xbill/DNS/Record;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/org/xbill/DNS/ZoneTransferException;
        }
    .end annotation
.end method
