.class public Lcom/samsung/android/org/xbill/DNS/PXRecord;
.super Lcom/samsung/android/org/xbill/DNS/Record;
.source "PXRecord.java"


# static fields
.field private static final serialVersionUID:J = 0x1923e20a41543a3bL


# instance fields
.field private map822:Lcom/samsung/android/org/xbill/DNS/Name;

.field private mapX400:Lcom/samsung/android/org/xbill/DNS/Name;

.field private preference:I


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/samsung/android/org/xbill/DNS/Record;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/org/xbill/DNS/Name;IJILcom/samsung/android/org/xbill/DNS/Name;Lcom/samsung/android/org/xbill/DNS/Name;)V
    .locals 7
    .param p1, "name"    # Lcom/samsung/android/org/xbill/DNS/Name;
    .param p2, "dclass"    # I
    .param p3, "ttl"    # J
    .param p5, "preference"    # I
    .param p6, "map822"    # Lcom/samsung/android/org/xbill/DNS/Name;
    .param p7, "mapX400"    # Lcom/samsung/android/org/xbill/DNS/Name;

    .prologue
    .line 38
    const/16 v2, 0x1a

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/org/xbill/DNS/Record;-><init>(Lcom/samsung/android/org/xbill/DNS/Name;IIJ)V

    .line 40
    const-string v0, "preference"

    invoke-static {v0, p5}, Lcom/samsung/android/org/xbill/DNS/PXRecord;->checkU16(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/org/xbill/DNS/PXRecord;->preference:I

    .line 41
    const-string v0, "map822"

    invoke-static {v0, p6}, Lcom/samsung/android/org/xbill/DNS/PXRecord;->checkName(Ljava/lang/String;Lcom/samsung/android/org/xbill/DNS/Name;)Lcom/samsung/android/org/xbill/DNS/Name;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/org/xbill/DNS/PXRecord;->map822:Lcom/samsung/android/org/xbill/DNS/Name;

    .line 42
    const-string v0, "mapX400"

    invoke-static {v0, p7}, Lcom/samsung/android/org/xbill/DNS/PXRecord;->checkName(Ljava/lang/String;Lcom/samsung/android/org/xbill/DNS/Name;)Lcom/samsung/android/org/xbill/DNS/Name;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/org/xbill/DNS/PXRecord;->mapX400:Lcom/samsung/android/org/xbill/DNS/Name;

    .line 43
    return-void
.end method


# virtual methods
.method public getMap822()Lcom/samsung/android/org/xbill/DNS/Name;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/PXRecord;->map822:Lcom/samsung/android/org/xbill/DNS/Name;

    return-object v0
.end method

.method public getMapX400()Lcom/samsung/android/org/xbill/DNS/Name;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/PXRecord;->mapX400:Lcom/samsung/android/org/xbill/DNS/Name;

    return-object v0
.end method

.method getObject()Lcom/samsung/android/org/xbill/DNS/Record;
    .locals 1

    .prologue
    .line 25
    new-instance v0, Lcom/samsung/android/org/xbill/DNS/PXRecord;

    invoke-direct {v0}, Lcom/samsung/android/org/xbill/DNS/PXRecord;-><init>()V

    return-object v0
.end method

.method public getPreference()I
    .locals 1

    .prologue
    .line 81
    iget v0, p0, Lcom/samsung/android/org/xbill/DNS/PXRecord;->preference:I

    return v0
.end method

.method rdataFromString(Lcom/samsung/android/org/xbill/DNS/Tokenizer;Lcom/samsung/android/org/xbill/DNS/Name;)V
    .locals 1
    .param p1, "st"    # Lcom/samsung/android/org/xbill/DNS/Tokenizer;
    .param p2, "origin"    # Lcom/samsung/android/org/xbill/DNS/Name;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 54
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->getUInt16()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/org/xbill/DNS/PXRecord;->preference:I

    .line 55
    invoke-virtual {p1, p2}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->getName(Lcom/samsung/android/org/xbill/DNS/Name;)Lcom/samsung/android/org/xbill/DNS/Name;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/org/xbill/DNS/PXRecord;->map822:Lcom/samsung/android/org/xbill/DNS/Name;

    .line 56
    invoke-virtual {p1, p2}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->getName(Lcom/samsung/android/org/xbill/DNS/Name;)Lcom/samsung/android/org/xbill/DNS/Name;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/org/xbill/DNS/PXRecord;->mapX400:Lcom/samsung/android/org/xbill/DNS/Name;

    .line 57
    return-void
.end method

.method rrFromWire(Lcom/samsung/android/org/xbill/DNS/DNSInput;)V
    .locals 1
    .param p1, "in"    # Lcom/samsung/android/org/xbill/DNS/DNSInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 47
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/DNSInput;->readU16()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/org/xbill/DNS/PXRecord;->preference:I

    .line 48
    new-instance v0, Lcom/samsung/android/org/xbill/DNS/Name;

    invoke-direct {v0, p1}, Lcom/samsung/android/org/xbill/DNS/Name;-><init>(Lcom/samsung/android/org/xbill/DNS/DNSInput;)V

    iput-object v0, p0, Lcom/samsung/android/org/xbill/DNS/PXRecord;->map822:Lcom/samsung/android/org/xbill/DNS/Name;

    .line 49
    new-instance v0, Lcom/samsung/android/org/xbill/DNS/Name;

    invoke-direct {v0, p1}, Lcom/samsung/android/org/xbill/DNS/Name;-><init>(Lcom/samsung/android/org/xbill/DNS/DNSInput;)V

    iput-object v0, p0, Lcom/samsung/android/org/xbill/DNS/PXRecord;->mapX400:Lcom/samsung/android/org/xbill/DNS/Name;

    .line 50
    return-void
.end method

.method rrToString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 62
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 63
    .local v0, "sb":Ljava/lang/StringBuffer;
    iget v1, p0, Lcom/samsung/android/org/xbill/DNS/PXRecord;->preference:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 64
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 65
    iget-object v1, p0, Lcom/samsung/android/org/xbill/DNS/PXRecord;->map822:Lcom/samsung/android/org/xbill/DNS/Name;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    .line 66
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 67
    iget-object v1, p0, Lcom/samsung/android/org/xbill/DNS/PXRecord;->mapX400:Lcom/samsung/android/org/xbill/DNS/Name;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    .line 68
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method rrToWire(Lcom/samsung/android/org/xbill/DNS/DNSOutput;Lcom/samsung/android/org/xbill/DNS/Compression;Z)V
    .locals 2
    .param p1, "out"    # Lcom/samsung/android/org/xbill/DNS/DNSOutput;
    .param p2, "c"    # Lcom/samsung/android/org/xbill/DNS/Compression;
    .param p3, "canonical"    # Z

    .prologue
    const/4 v1, 0x0

    .line 73
    iget v0, p0, Lcom/samsung/android/org/xbill/DNS/PXRecord;->preference:I

    invoke-virtual {p1, v0}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeU16(I)V

    .line 74
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/PXRecord;->map822:Lcom/samsung/android/org/xbill/DNS/Name;

    invoke-virtual {v0, p1, v1, p3}, Lcom/samsung/android/org/xbill/DNS/Name;->toWire(Lcom/samsung/android/org/xbill/DNS/DNSOutput;Lcom/samsung/android/org/xbill/DNS/Compression;Z)V

    .line 75
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/PXRecord;->mapX400:Lcom/samsung/android/org/xbill/DNS/Name;

    invoke-virtual {v0, p1, v1, p3}, Lcom/samsung/android/org/xbill/DNS/Name;->toWire(Lcom/samsung/android/org/xbill/DNS/DNSOutput;Lcom/samsung/android/org/xbill/DNS/Compression;Z)V

    .line 76
    return-void
.end method
