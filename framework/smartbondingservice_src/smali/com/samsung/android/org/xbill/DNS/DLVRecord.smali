.class public Lcom/samsung/android/org/xbill/DNS/DLVRecord;
.super Lcom/samsung/android/org/xbill/DNS/Record;
.source "DLVRecord.java"


# static fields
.field public static final SHA1_DIGEST_ID:I = 0x1

.field public static final SHA256_DIGEST_ID:I = 0x1

.field private static final serialVersionUID:J = 0x1b35f4cd5e509fc4L


# instance fields
.field private alg:I

.field private digest:[B

.field private digestid:I

.field private footprint:I


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/samsung/android/org/xbill/DNS/Record;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/org/xbill/DNS/Name;IJIII[B)V
    .locals 7
    .param p1, "name"    # Lcom/samsung/android/org/xbill/DNS/Name;
    .param p2, "dclass"    # I
    .param p3, "ttl"    # J
    .param p5, "footprint"    # I
    .param p6, "alg"    # I
    .param p7, "digestid"    # I
    .param p8, "digest"    # [B

    .prologue
    .line 49
    const v2, 0x8001

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/org/xbill/DNS/Record;-><init>(Lcom/samsung/android/org/xbill/DNS/Name;IIJ)V

    .line 50
    const-string v0, "footprint"

    invoke-static {v0, p5}, Lcom/samsung/android/org/xbill/DNS/DLVRecord;->checkU16(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/org/xbill/DNS/DLVRecord;->footprint:I

    .line 51
    const-string v0, "alg"

    invoke-static {v0, p6}, Lcom/samsung/android/org/xbill/DNS/DLVRecord;->checkU8(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/org/xbill/DNS/DLVRecord;->alg:I

    .line 52
    const-string v0, "digestid"

    invoke-static {v0, p7}, Lcom/samsung/android/org/xbill/DNS/DLVRecord;->checkU8(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/org/xbill/DNS/DLVRecord;->digestid:I

    .line 53
    iput-object p8, p0, Lcom/samsung/android/org/xbill/DNS/DLVRecord;->digest:[B

    .line 54
    return-void
.end method


# virtual methods
.method public getAlgorithm()I
    .locals 1

    .prologue
    .line 96
    iget v0, p0, Lcom/samsung/android/org/xbill/DNS/DLVRecord;->alg:I

    return v0
.end method

.method public getDigest()[B
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/DLVRecord;->digest:[B

    return-object v0
.end method

.method public getDigestID()I
    .locals 1

    .prologue
    .line 105
    iget v0, p0, Lcom/samsung/android/org/xbill/DNS/DLVRecord;->digestid:I

    return v0
.end method

.method public getFootprint()I
    .locals 1

    .prologue
    .line 121
    iget v0, p0, Lcom/samsung/android/org/xbill/DNS/DLVRecord;->footprint:I

    return v0
.end method

.method getObject()Lcom/samsung/android/org/xbill/DNS/Record;
    .locals 1

    .prologue
    .line 35
    new-instance v0, Lcom/samsung/android/org/xbill/DNS/DLVRecord;

    invoke-direct {v0}, Lcom/samsung/android/org/xbill/DNS/DLVRecord;-><init>()V

    return-object v0
.end method

.method rdataFromString(Lcom/samsung/android/org/xbill/DNS/Tokenizer;Lcom/samsung/android/org/xbill/DNS/Name;)V
    .locals 1
    .param p1, "st"    # Lcom/samsung/android/org/xbill/DNS/Tokenizer;
    .param p2, "origin"    # Lcom/samsung/android/org/xbill/DNS/Name;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 66
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->getUInt16()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/org/xbill/DNS/DLVRecord;->footprint:I

    .line 67
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->getUInt8()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/org/xbill/DNS/DLVRecord;->alg:I

    .line 68
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->getUInt8()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/org/xbill/DNS/DLVRecord;->digestid:I

    .line 69
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->getHex()[B

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/org/xbill/DNS/DLVRecord;->digest:[B

    .line 70
    return-void
.end method

.method rrFromWire(Lcom/samsung/android/org/xbill/DNS/DNSInput;)V
    .locals 1
    .param p1, "in"    # Lcom/samsung/android/org/xbill/DNS/DNSInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 58
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/DNSInput;->readU16()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/org/xbill/DNS/DLVRecord;->footprint:I

    .line 59
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/DNSInput;->readU8()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/org/xbill/DNS/DLVRecord;->alg:I

    .line 60
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/DNSInput;->readU8()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/org/xbill/DNS/DLVRecord;->digestid:I

    .line 61
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/DNSInput;->readByteArray()[B

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/org/xbill/DNS/DLVRecord;->digest:[B

    .line 62
    return-void
.end method

.method rrToString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 77
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 78
    .local v0, "sb":Ljava/lang/StringBuffer;
    iget v1, p0, Lcom/samsung/android/org/xbill/DNS/DLVRecord;->footprint:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 79
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 80
    iget v1, p0, Lcom/samsung/android/org/xbill/DNS/DLVRecord;->alg:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 81
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 82
    iget v1, p0, Lcom/samsung/android/org/xbill/DNS/DLVRecord;->digestid:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    .line 83
    iget-object v1, p0, Lcom/samsung/android/org/xbill/DNS/DLVRecord;->digest:[B

    if-eqz v1, :cond_0

    .line 84
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 85
    iget-object v1, p0, Lcom/samsung/android/org/xbill/DNS/DLVRecord;->digest:[B

    invoke-static {v1}, Lcom/samsung/android/org/xbill/DNS/utils/base16;->toString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 88
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method rrToWire(Lcom/samsung/android/org/xbill/DNS/DNSOutput;Lcom/samsung/android/org/xbill/DNS/Compression;Z)V
    .locals 1
    .param p1, "out"    # Lcom/samsung/android/org/xbill/DNS/DNSOutput;
    .param p2, "c"    # Lcom/samsung/android/org/xbill/DNS/Compression;
    .param p3, "canonical"    # Z

    .prologue
    .line 126
    iget v0, p0, Lcom/samsung/android/org/xbill/DNS/DLVRecord;->footprint:I

    invoke-virtual {p1, v0}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeU16(I)V

    .line 127
    iget v0, p0, Lcom/samsung/android/org/xbill/DNS/DLVRecord;->alg:I

    invoke-virtual {p1, v0}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeU8(I)V

    .line 128
    iget v0, p0, Lcom/samsung/android/org/xbill/DNS/DLVRecord;->digestid:I

    invoke-virtual {p1, v0}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeU8(I)V

    .line 129
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/DLVRecord;->digest:[B

    if-eqz v0, :cond_0

    .line 130
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/DLVRecord;->digest:[B

    invoke-virtual {p1, v0}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeByteArray([B)V

    .line 131
    :cond_0
    return-void
.end method
