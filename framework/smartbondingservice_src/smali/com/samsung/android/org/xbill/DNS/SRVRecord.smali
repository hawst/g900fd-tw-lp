.class public Lcom/samsung/android/org/xbill/DNS/SRVRecord;
.super Lcom/samsung/android/org/xbill/DNS/Record;
.source "SRVRecord.java"


# static fields
.field private static final serialVersionUID:J = -0x35ef7ae628ad0604L


# instance fields
.field private port:I

.field private priority:I

.field private target:Lcom/samsung/android/org/xbill/DNS/Name;

.field private weight:I


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/samsung/android/org/xbill/DNS/Record;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/org/xbill/DNS/Name;IJIIILcom/samsung/android/org/xbill/DNS/Name;)V
    .locals 7
    .param p1, "name"    # Lcom/samsung/android/org/xbill/DNS/Name;
    .param p2, "dclass"    # I
    .param p3, "ttl"    # J
    .param p5, "priority"    # I
    .param p6, "weight"    # I
    .param p7, "port"    # I
    .param p8, "target"    # Lcom/samsung/android/org/xbill/DNS/Name;

    .prologue
    .line 43
    const/16 v2, 0x21

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/org/xbill/DNS/Record;-><init>(Lcom/samsung/android/org/xbill/DNS/Name;IIJ)V

    .line 44
    const-string v0, "priority"

    invoke-static {v0, p5}, Lcom/samsung/android/org/xbill/DNS/SRVRecord;->checkU16(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/org/xbill/DNS/SRVRecord;->priority:I

    .line 45
    const-string v0, "weight"

    invoke-static {v0, p6}, Lcom/samsung/android/org/xbill/DNS/SRVRecord;->checkU16(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/org/xbill/DNS/SRVRecord;->weight:I

    .line 46
    const-string v0, "port"

    invoke-static {v0, p7}, Lcom/samsung/android/org/xbill/DNS/SRVRecord;->checkU16(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/org/xbill/DNS/SRVRecord;->port:I

    .line 47
    const-string v0, "target"

    invoke-static {v0, p8}, Lcom/samsung/android/org/xbill/DNS/SRVRecord;->checkName(Ljava/lang/String;Lcom/samsung/android/org/xbill/DNS/Name;)Lcom/samsung/android/org/xbill/DNS/Name;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/org/xbill/DNS/SRVRecord;->target:Lcom/samsung/android/org/xbill/DNS/Name;

    .line 48
    return-void
.end method


# virtual methods
.method public getAdditionalName()Lcom/samsung/android/org/xbill/DNS/Name;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/SRVRecord;->target:Lcom/samsung/android/org/xbill/DNS/Name;

    return-object v0
.end method

.method getObject()Lcom/samsung/android/org/xbill/DNS/Record;
    .locals 1

    .prologue
    .line 27
    new-instance v0, Lcom/samsung/android/org/xbill/DNS/SRVRecord;

    invoke-direct {v0}, Lcom/samsung/android/org/xbill/DNS/SRVRecord;-><init>()V

    return-object v0
.end method

.method public getPort()I
    .locals 1

    .prologue
    .line 92
    iget v0, p0, Lcom/samsung/android/org/xbill/DNS/SRVRecord;->port:I

    return v0
.end method

.method public getPriority()I
    .locals 1

    .prologue
    .line 80
    iget v0, p0, Lcom/samsung/android/org/xbill/DNS/SRVRecord;->priority:I

    return v0
.end method

.method public getTarget()Lcom/samsung/android/org/xbill/DNS/Name;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/SRVRecord;->target:Lcom/samsung/android/org/xbill/DNS/Name;

    return-object v0
.end method

.method public getWeight()I
    .locals 1

    .prologue
    .line 86
    iget v0, p0, Lcom/samsung/android/org/xbill/DNS/SRVRecord;->weight:I

    return v0
.end method

.method rdataFromString(Lcom/samsung/android/org/xbill/DNS/Tokenizer;Lcom/samsung/android/org/xbill/DNS/Name;)V
    .locals 1
    .param p1, "st"    # Lcom/samsung/android/org/xbill/DNS/Tokenizer;
    .param p2, "origin"    # Lcom/samsung/android/org/xbill/DNS/Name;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 60
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->getUInt16()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/org/xbill/DNS/SRVRecord;->priority:I

    .line 61
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->getUInt16()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/org/xbill/DNS/SRVRecord;->weight:I

    .line 62
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->getUInt16()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/org/xbill/DNS/SRVRecord;->port:I

    .line 63
    invoke-virtual {p1, p2}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->getName(Lcom/samsung/android/org/xbill/DNS/Name;)Lcom/samsung/android/org/xbill/DNS/Name;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/org/xbill/DNS/SRVRecord;->target:Lcom/samsung/android/org/xbill/DNS/Name;

    .line 64
    return-void
.end method

.method rrFromWire(Lcom/samsung/android/org/xbill/DNS/DNSInput;)V
    .locals 1
    .param p1, "in"    # Lcom/samsung/android/org/xbill/DNS/DNSInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 52
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/DNSInput;->readU16()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/org/xbill/DNS/SRVRecord;->priority:I

    .line 53
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/DNSInput;->readU16()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/org/xbill/DNS/SRVRecord;->weight:I

    .line 54
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/DNSInput;->readU16()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/org/xbill/DNS/SRVRecord;->port:I

    .line 55
    new-instance v0, Lcom/samsung/android/org/xbill/DNS/Name;

    invoke-direct {v0, p1}, Lcom/samsung/android/org/xbill/DNS/Name;-><init>(Lcom/samsung/android/org/xbill/DNS/DNSInput;)V

    iput-object v0, p0, Lcom/samsung/android/org/xbill/DNS/SRVRecord;->target:Lcom/samsung/android/org/xbill/DNS/Name;

    .line 56
    return-void
.end method

.method rrToString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 69
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 70
    .local v0, "sb":Ljava/lang/StringBuffer;
    new-instance v1, Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/samsung/android/org/xbill/DNS/SRVRecord;->priority:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 71
    new-instance v1, Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/samsung/android/org/xbill/DNS/SRVRecord;->weight:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 72
    new-instance v1, Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/samsung/android/org/xbill/DNS/SRVRecord;->port:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 73
    iget-object v1, p0, Lcom/samsung/android/org/xbill/DNS/SRVRecord;->target:Lcom/samsung/android/org/xbill/DNS/Name;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    .line 74
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method rrToWire(Lcom/samsung/android/org/xbill/DNS/DNSOutput;Lcom/samsung/android/org/xbill/DNS/Compression;Z)V
    .locals 2
    .param p1, "out"    # Lcom/samsung/android/org/xbill/DNS/DNSOutput;
    .param p2, "c"    # Lcom/samsung/android/org/xbill/DNS/Compression;
    .param p3, "canonical"    # Z

    .prologue
    .line 103
    iget v0, p0, Lcom/samsung/android/org/xbill/DNS/SRVRecord;->priority:I

    invoke-virtual {p1, v0}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeU16(I)V

    .line 104
    iget v0, p0, Lcom/samsung/android/org/xbill/DNS/SRVRecord;->weight:I

    invoke-virtual {p1, v0}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeU16(I)V

    .line 105
    iget v0, p0, Lcom/samsung/android/org/xbill/DNS/SRVRecord;->port:I

    invoke-virtual {p1, v0}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeU16(I)V

    .line 106
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/SRVRecord;->target:Lcom/samsung/android/org/xbill/DNS/Name;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1, p3}, Lcom/samsung/android/org/xbill/DNS/Name;->toWire(Lcom/samsung/android/org/xbill/DNS/DNSOutput;Lcom/samsung/android/org/xbill/DNS/Compression;Z)V

    .line 107
    return-void
.end method
