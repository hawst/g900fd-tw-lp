.class public Lcom/samsung/android/org/xbill/DNS/APLRecord;
.super Lcom/samsung/android/org/xbill/DNS/Record;
.source "APLRecord.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/org/xbill/DNS/APLRecord$Element;
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = -0x12b5acebae8a7fb8L


# instance fields
.field private elements:Ljava/util/List;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 93
    invoke-direct {p0}, Lcom/samsung/android/org/xbill/DNS/Record;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/org/xbill/DNS/Name;IJLjava/util/List;)V
    .locals 9
    .param p1, "name"    # Lcom/samsung/android/org/xbill/DNS/Name;
    .param p2, "dclass"    # I
    .param p3, "ttl"    # J
    .param p5, "elements"    # Ljava/util/List;

    .prologue
    .line 116
    const/16 v2, 0x2a

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/org/xbill/DNS/Record;-><init>(Lcom/samsung/android/org/xbill/DNS/Name;IIJ)V

    .line 117
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p5}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/samsung/android/org/xbill/DNS/APLRecord;->elements:Ljava/util/List;

    .line 118
    invoke-interface {p5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "it":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 131
    return-void

    .line 119
    :cond_0
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    .line 120
    .local v8, "o":Ljava/lang/Object;
    instance-of v0, v8, Lcom/samsung/android/org/xbill/DNS/APLRecord$Element;

    if-nez v0, :cond_1

    .line 121
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "illegal element"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    move-object v6, v8

    .line 123
    check-cast v6, Lcom/samsung/android/org/xbill/DNS/APLRecord$Element;

    .line 124
    .local v6, "element":Lcom/samsung/android/org/xbill/DNS/APLRecord$Element;
    iget v0, v6, Lcom/samsung/android/org/xbill/DNS/APLRecord$Element;->family:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    .line 125
    iget v0, v6, Lcom/samsung/android/org/xbill/DNS/APLRecord$Element;->family:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    .line 127
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "unknown family"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 129
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/APLRecord;->elements:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method static synthetic access$0(II)Z
    .locals 1

    .prologue
    .line 101
    invoke-static {p0, p1}, Lcom/samsung/android/org/xbill/DNS/APLRecord;->validatePrefixLength(II)Z

    move-result v0

    return v0
.end method

.method private static addressLength([B)I
    .locals 2
    .param p0, "addr"    # [B

    .prologue
    .line 254
    array-length v1, p0

    add-int/lit8 v0, v1, -0x1

    .local v0, "i":I
    :goto_0
    if-gez v0, :cond_0

    .line 258
    const/4 v1, 0x0

    :goto_1
    return v1

    .line 255
    :cond_0
    aget-byte v1, p0, v0

    if-eqz v1, :cond_1

    .line 256
    add-int/lit8 v1, v0, 0x1

    goto :goto_1

    .line 254
    :cond_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method private static parseAddress([BI)[B
    .locals 3
    .param p0, "in"    # [B
    .param p1, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/org/xbill/DNS/WireParseException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 136
    array-length v1, p0

    if-le v1, p1, :cond_0

    .line 137
    new-instance v1, Lcom/samsung/android/org/xbill/DNS/WireParseException;

    const-string v2, "invalid address length"

    invoke-direct {v1, v2}, Lcom/samsung/android/org/xbill/DNS/WireParseException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 138
    :cond_0
    array-length v1, p0

    if-ne v1, p1, :cond_1

    .line 142
    .end local p0    # "in":[B
    :goto_0
    return-object p0

    .line 140
    .restart local p0    # "in":[B
    :cond_1
    new-array v0, p1, [B

    .line 141
    .local v0, "out":[B
    array-length v1, p0

    invoke-static {p0, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object p0, v0

    .line 142
    goto :goto_0
.end method

.method private static validatePrefixLength(II)Z
    .locals 3
    .param p0, "family"    # I
    .param p1, "prefixLength"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 102
    if-ltz p1, :cond_0

    const/16 v2, 0x100

    if-lt p1, v2, :cond_1

    .line 107
    :cond_0
    :goto_0
    return v0

    .line 104
    :cond_1
    if-ne p0, v1, :cond_2

    const/16 v2, 0x20

    if-gt p1, v2, :cond_0

    .line 105
    :cond_2
    const/4 v2, 0x2

    if-ne p0, v2, :cond_3

    const/16 v2, 0x80

    if-gt p1, v2, :cond_0

    :cond_3
    move v0, v1

    .line 107
    goto :goto_0
.end method


# virtual methods
.method public getElements()Ljava/util/List;
    .locals 1

    .prologue
    .line 249
    iget-object v0, p0, Lcom/samsung/android/org/xbill/DNS/APLRecord;->elements:Ljava/util/List;

    return-object v0
.end method

.method getObject()Lcom/samsung/android/org/xbill/DNS/Record;
    .locals 1

    .prologue
    .line 97
    new-instance v0, Lcom/samsung/android/org/xbill/DNS/APLRecord;

    invoke-direct {v0}, Lcom/samsung/android/org/xbill/DNS/APLRecord;-><init>()V

    return-object v0
.end method

.method rdataFromString(Lcom/samsung/android/org/xbill/DNS/Tokenizer;Lcom/samsung/android/org/xbill/DNS/Name;)V
    .locals 17
    .param p1, "st"    # Lcom/samsung/android/org/xbill/DNS/Tokenizer;
    .param p2, "origin"    # Lcom/samsung/android/org/xbill/DNS/Name;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 176
    new-instance v15, Ljava/util/ArrayList;

    const/16 v16, 0x1

    invoke-direct/range {v15 .. v16}, Ljava/util/ArrayList;-><init>(I)V

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/samsung/android/org/xbill/DNS/APLRecord;->elements:Ljava/util/List;

    .line 178
    :goto_0
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->get()Lcom/samsung/android/org/xbill/DNS/Tokenizer$Token;

    move-result-object v14

    .line 179
    .local v14, "t":Lcom/samsung/android/org/xbill/DNS/Tokenizer$Token;
    invoke-virtual {v14}, Lcom/samsung/android/org/xbill/DNS/Tokenizer$Token;->isString()Z

    move-result v15

    if-nez v15, :cond_0

    .line 231
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->unget()V

    .line 232
    return-void

    .line 182
    :cond_0
    const/4 v8, 0x0

    .line 183
    .local v8, "negative":Z
    const/4 v6, 0x0

    .line 184
    .local v6, "family":I
    const/4 v9, 0x0

    .line 186
    .local v9, "prefix":I
    iget-object v11, v14, Lcom/samsung/android/org/xbill/DNS/Tokenizer$Token;->value:Ljava/lang/String;

    .line 187
    .local v11, "s":Ljava/lang/String;
    const/4 v13, 0x0

    .line 188
    .local v13, "start":I
    const-string v15, "!"

    invoke-virtual {v11, v15}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_1

    .line 189
    const/4 v8, 0x1

    .line 190
    const/4 v13, 0x1

    .line 192
    :cond_1
    const/16 v15, 0x3a

    invoke-virtual {v11, v15, v13}, Ljava/lang/String;->indexOf(II)I

    move-result v4

    .line 193
    .local v4, "colon":I
    if-gez v4, :cond_2

    .line 194
    const-string v15, "invalid address prefix element"

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->exception(Ljava/lang/String;)Lcom/samsung/android/org/xbill/DNS/TextParseException;

    move-result-object v15

    throw v15

    .line 195
    :cond_2
    const/16 v15, 0x2f

    invoke-virtual {v11, v15, v4}, Ljava/lang/String;->indexOf(II)I

    move-result v12

    .line 196
    .local v12, "slash":I
    if-gez v12, :cond_3

    .line 197
    const-string v15, "invalid address prefix element"

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->exception(Ljava/lang/String;)Lcom/samsung/android/org/xbill/DNS/TextParseException;

    move-result-object v15

    throw v15

    .line 199
    :cond_3
    invoke-virtual {v11, v13, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    .line 200
    .local v7, "familyString":Ljava/lang/String;
    add-int/lit8 v15, v4, 0x1

    invoke-virtual {v11, v15, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 201
    .local v2, "addressString":Ljava/lang/String;
    add-int/lit8 v15, v12, 0x1

    invoke-virtual {v11, v15}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v10

    .line 204
    .local v10, "prefixString":Ljava/lang/String;
    :try_start_0
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v6

    .line 209
    const/4 v15, 0x1

    if-eq v6, v15, :cond_4

    const/4 v15, 0x2

    if-eq v6, v15, :cond_4

    .line 210
    const-string v15, "unknown family"

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->exception(Ljava/lang/String;)Lcom/samsung/android/org/xbill/DNS/TextParseException;

    move-result-object v15

    throw v15

    .line 206
    :catch_0
    move-exception v5

    .line 207
    .local v5, "e":Ljava/lang/NumberFormatException;
    const-string v15, "invalid family"

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->exception(Ljava/lang/String;)Lcom/samsung/android/org/xbill/DNS/TextParseException;

    move-result-object v15

    throw v15

    .line 213
    .end local v5    # "e":Ljava/lang/NumberFormatException;
    :cond_4
    :try_start_1
    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v9

    .line 219
    invoke-static {v6, v9}, Lcom/samsung/android/org/xbill/DNS/APLRecord;->validatePrefixLength(II)Z

    move-result v15

    if-nez v15, :cond_5

    .line 220
    const-string v15, "invalid prefix length"

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->exception(Ljava/lang/String;)Lcom/samsung/android/org/xbill/DNS/TextParseException;

    move-result-object v15

    throw v15

    .line 215
    :catch_1
    move-exception v5

    .line 216
    .restart local v5    # "e":Ljava/lang/NumberFormatException;
    const-string v15, "invalid prefix length"

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->exception(Ljava/lang/String;)Lcom/samsung/android/org/xbill/DNS/TextParseException;

    move-result-object v15

    throw v15

    .line 223
    .end local v5    # "e":Ljava/lang/NumberFormatException;
    :cond_5
    invoke-static {v2, v6}, Lcom/samsung/android/org/xbill/DNS/Address;->toByteArray(Ljava/lang/String;I)[B

    move-result-object v3

    .line 224
    .local v3, "bytes":[B
    if-nez v3, :cond_6

    .line 225
    new-instance v15, Ljava/lang/StringBuilder;

    const-string v16, "invalid IP address "

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 226
    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    .line 225
    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Lcom/samsung/android/org/xbill/DNS/Tokenizer;->exception(Ljava/lang/String;)Lcom/samsung/android/org/xbill/DNS/TextParseException;

    move-result-object v15

    throw v15

    .line 228
    :cond_6
    invoke-static {v3}, Ljava/net/InetAddress;->getByAddress([B)Ljava/net/InetAddress;

    move-result-object v1

    .line 229
    .local v1, "address":Ljava/net/InetAddress;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/org/xbill/DNS/APLRecord;->elements:Ljava/util/List;

    new-instance v16, Lcom/samsung/android/org/xbill/DNS/APLRecord$Element;

    move-object/from16 v0, v16

    invoke-direct {v0, v8, v1, v9}, Lcom/samsung/android/org/xbill/DNS/APLRecord$Element;-><init>(ZLjava/net/InetAddress;I)V

    invoke-interface/range {v15 .. v16}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method

.method rrFromWire(Lcom/samsung/android/org/xbill/DNS/DNSInput;)V
    .locals 9
    .param p1, "in"    # Lcom/samsung/android/org/xbill/DNS/DNSInput;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    .line 147
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, v8}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v5, p0, Lcom/samsung/android/org/xbill/DNS/APLRecord;->elements:Ljava/util/List;

    .line 148
    :goto_0
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/DNSInput;->remaining()I

    move-result v5

    if-nez v5, :cond_0

    .line 172
    return-void

    .line 149
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/DNSInput;->readU16()I

    move-result v1

    .line 150
    .local v1, "family":I
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/DNSInput;->readU8()I

    move-result v4

    .line 151
    .local v4, "prefix":I
    invoke-virtual {p1}, Lcom/samsung/android/org/xbill/DNS/DNSInput;->readU8()I

    move-result v7

    .line 152
    .local v7, "length":I
    and-int/lit16 v5, v7, 0x80

    if-eqz v5, :cond_1

    move v2, v8

    .line 153
    .local v2, "negative":Z
    :goto_1
    and-int/lit16 v7, v7, -0x81

    .line 155
    invoke-virtual {p1, v7}, Lcom/samsung/android/org/xbill/DNS/DNSInput;->readByteArray(I)[B

    move-result-object v3

    .line 157
    .local v3, "data":[B
    invoke-static {v1, v4}, Lcom/samsung/android/org/xbill/DNS/APLRecord;->validatePrefixLength(II)Z

    move-result v5

    if-nez v5, :cond_2

    .line 158
    new-instance v5, Lcom/samsung/android/org/xbill/DNS/WireParseException;

    const-string v8, "invalid prefix length"

    invoke-direct {v5, v8}, Lcom/samsung/android/org/xbill/DNS/WireParseException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 152
    .end local v2    # "negative":Z
    .end local v3    # "data":[B
    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    .line 161
    .restart local v2    # "negative":Z
    .restart local v3    # "data":[B
    :cond_2
    if-eq v1, v8, :cond_3

    const/4 v5, 0x2

    if-ne v1, v5, :cond_4

    .line 163
    :cond_3
    invoke-static {v1}, Lcom/samsung/android/org/xbill/DNS/Address;->addressLength(I)I

    move-result v5

    .line 162
    invoke-static {v3, v5}, Lcom/samsung/android/org/xbill/DNS/APLRecord;->parseAddress([BI)[B

    move-result-object v3

    .line 164
    invoke-static {v3}, Ljava/net/InetAddress;->getByAddress([B)Ljava/net/InetAddress;

    move-result-object v6

    .line 165
    .local v6, "addr":Ljava/net/InetAddress;
    new-instance v0, Lcom/samsung/android/org/xbill/DNS/APLRecord$Element;

    invoke-direct {v0, v2, v6, v4}, Lcom/samsung/android/org/xbill/DNS/APLRecord$Element;-><init>(ZLjava/net/InetAddress;I)V

    .line 169
    .end local v6    # "addr":Ljava/net/InetAddress;
    .local v0, "element":Lcom/samsung/android/org/xbill/DNS/APLRecord$Element;
    :goto_2
    iget-object v5, p0, Lcom/samsung/android/org/xbill/DNS/APLRecord;->elements:Ljava/util/List;

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 167
    .end local v0    # "element":Lcom/samsung/android/org/xbill/DNS/APLRecord$Element;
    :cond_4
    new-instance v0, Lcom/samsung/android/org/xbill/DNS/APLRecord$Element;

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/org/xbill/DNS/APLRecord$Element;-><init>(IZLjava/lang/Object;ILcom/samsung/android/org/xbill/DNS/APLRecord$Element;)V

    .restart local v0    # "element":Lcom/samsung/android/org/xbill/DNS/APLRecord$Element;
    goto :goto_2
.end method

.method rrToString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 236
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 237
    .local v2, "sb":Ljava/lang/StringBuffer;
    iget-object v3, p0, Lcom/samsung/android/org/xbill/DNS/APLRecord;->elements:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "it":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    .line 243
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 238
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/org/xbill/DNS/APLRecord$Element;

    .line 239
    .local v0, "element":Lcom/samsung/android/org/xbill/DNS/APLRecord$Element;
    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    .line 240
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 241
    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0
.end method

.method rrToWire(Lcom/samsung/android/org/xbill/DNS/DNSOutput;Lcom/samsung/android/org/xbill/DNS/Compression;Z)V
    .locals 8
    .param p1, "out"    # Lcom/samsung/android/org/xbill/DNS/DNSOutput;
    .param p2, "c"    # Lcom/samsung/android/org/xbill/DNS/Compression;
    .param p3, "canonical"    # Z

    .prologue
    .line 263
    iget-object v6, p0, Lcom/samsung/android/org/xbill/DNS/APLRecord;->elements:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "it":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_0

    .line 286
    return-void

    .line 264
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/org/xbill/DNS/APLRecord$Element;

    .line 265
    .local v2, "element":Lcom/samsung/android/org/xbill/DNS/APLRecord$Element;
    const/4 v4, 0x0

    .line 267
    .local v4, "length":I
    iget v6, v2, Lcom/samsung/android/org/xbill/DNS/APLRecord$Element;->family:I

    const/4 v7, 0x1

    if-eq v6, v7, :cond_1

    .line 268
    iget v6, v2, Lcom/samsung/android/org/xbill/DNS/APLRecord$Element;->family:I

    const/4 v7, 0x2

    if-ne v6, v7, :cond_3

    .line 270
    :cond_1
    iget-object v0, v2, Lcom/samsung/android/org/xbill/DNS/APLRecord$Element;->address:Ljava/lang/Object;

    check-cast v0, Ljava/net/InetAddress;

    .line 271
    .local v0, "addr":Ljava/net/InetAddress;
    invoke-virtual {v0}, Ljava/net/InetAddress;->getAddress()[B

    move-result-object v1

    .line 272
    .local v1, "data":[B
    invoke-static {v1}, Lcom/samsung/android/org/xbill/DNS/APLRecord;->addressLength([B)I

    move-result v4

    .line 277
    .end local v0    # "addr":Ljava/net/InetAddress;
    :goto_1
    move v5, v4

    .line 278
    .local v5, "wlength":I
    iget-boolean v6, v2, Lcom/samsung/android/org/xbill/DNS/APLRecord$Element;->negative:Z

    if-eqz v6, :cond_2

    .line 279
    or-int/lit16 v5, v5, 0x80

    .line 281
    :cond_2
    iget v6, v2, Lcom/samsung/android/org/xbill/DNS/APLRecord$Element;->family:I

    invoke-virtual {p1, v6}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeU16(I)V

    .line 282
    iget v6, v2, Lcom/samsung/android/org/xbill/DNS/APLRecord$Element;->prefixLength:I

    invoke-virtual {p1, v6}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeU8(I)V

    .line 283
    invoke-virtual {p1, v5}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeU8(I)V

    .line 284
    const/4 v6, 0x0

    invoke-virtual {p1, v1, v6, v4}, Lcom/samsung/android/org/xbill/DNS/DNSOutput;->writeByteArray([BII)V

    goto :goto_0

    .line 274
    .end local v1    # "data":[B
    .end local v5    # "wlength":I
    :cond_3
    iget-object v1, v2, Lcom/samsung/android/org/xbill/DNS/APLRecord$Element;->address:Ljava/lang/Object;

    check-cast v1, [B

    .line 275
    .restart local v1    # "data":[B
    array-length v4, v1

    goto :goto_1
.end method
