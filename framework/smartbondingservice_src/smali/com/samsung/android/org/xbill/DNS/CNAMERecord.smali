.class public Lcom/samsung/android/org/xbill/DNS/CNAMERecord;
.super Lcom/samsung/android/org/xbill/DNS/SingleCompressedNameBase;
.source "CNAMERecord.java"


# static fields
.field private static final serialVersionUID:J = -0x37cb3cbfd07c7ad4L


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/samsung/android/org/xbill/DNS/SingleCompressedNameBase;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/org/xbill/DNS/Name;IJLcom/samsung/android/org/xbill/DNS/Name;)V
    .locals 9
    .param p1, "name"    # Lcom/samsung/android/org/xbill/DNS/Name;
    .param p2, "dclass"    # I
    .param p3, "ttl"    # J
    .param p5, "alias"    # Lcom/samsung/android/org/xbill/DNS/Name;

    .prologue
    .line 28
    const/4 v2, 0x5

    const-string v7, "alias"

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move-wide v4, p3

    move-object v6, p5

    invoke-direct/range {v0 .. v7}, Lcom/samsung/android/org/xbill/DNS/SingleCompressedNameBase;-><init>(Lcom/samsung/android/org/xbill/DNS/Name;IIJLcom/samsung/android/org/xbill/DNS/Name;Ljava/lang/String;)V

    .line 29
    return-void
.end method


# virtual methods
.method public getAlias()Lcom/samsung/android/org/xbill/DNS/Name;
    .locals 1

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/samsung/android/org/xbill/DNS/CNAMERecord;->getSingleName()Lcom/samsung/android/org/xbill/DNS/Name;

    move-result-object v0

    return-object v0
.end method

.method getObject()Lcom/samsung/android/org/xbill/DNS/Record;
    .locals 1

    .prologue
    .line 19
    new-instance v0, Lcom/samsung/android/org/xbill/DNS/CNAMERecord;

    invoke-direct {v0}, Lcom/samsung/android/org/xbill/DNS/CNAMERecord;-><init>()V

    return-object v0
.end method

.method public getTarget()Lcom/samsung/android/org/xbill/DNS/Name;
    .locals 1

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/samsung/android/org/xbill/DNS/CNAMERecord;->getSingleName()Lcom/samsung/android/org/xbill/DNS/Name;

    move-result-object v0

    return-object v0
.end method
