.class public Lcom/samsung/android/smartbonding/Dns46NameResolver;
.super Ljava/lang/Object;
.source "Dns46NameResolver.java"


# static fields
.field private static DEBUG:Z


# instance fields
.field private context:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x1

    sput-boolean v0, Lcom/samsung/android/smartbonding/Dns46NameResolver;->DEBUG:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object p1, p0, Lcom/samsung/android/smartbonding/Dns46NameResolver;->context:Landroid/content/Context;

    .line 56
    return-void
.end method

.method private static convertToIpv6(Ljava/net/InetAddress;Ljava/net/InetAddress;)Ljava/net/InetAddress;
    .locals 6
    .param p0, "prefix"    # Ljava/net/InetAddress;
    .param p1, "ip"    # Ljava/net/InetAddress;

    .prologue
    const/4 v3, 0x0

    .line 197
    instance-of v4, p0, Ljava/net/Inet6Address;

    if-eqz v4, :cond_0

    instance-of v4, p1, Ljava/net/Inet4Address;

    if-nez v4, :cond_1

    .line 213
    :cond_0
    :goto_0
    return-object v3

    .line 200
    :cond_1
    invoke-virtual {p0}, Ljava/net/InetAddress;->getAddress()[B

    move-result-object v2

    .line 201
    .local v2, "prefixBytes":[B
    invoke-virtual {p1}, Ljava/net/InetAddress;->getAddress()[B

    move-result-object v1

    .line 203
    .local v1, "ipBytes":[B
    const/16 v4, 0xc

    const/4 v5, 0x0

    aget-byte v5, v1, v5

    aput-byte v5, v2, v4

    .line 204
    const/16 v4, 0xd

    const/4 v5, 0x1

    aget-byte v5, v1, v5

    aput-byte v5, v2, v4

    .line 205
    const/16 v4, 0xe

    const/4 v5, 0x2

    aget-byte v5, v1, v5

    aput-byte v5, v2, v4

    .line 206
    const/16 v4, 0xf

    const/4 v5, 0x3

    aget-byte v5, v1, v5

    aput-byte v5, v2, v4

    .line 209
    :try_start_0
    invoke-static {v2}, Ljava/net/InetAddress;->getByAddress([B)Ljava/net/InetAddress;
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    goto :goto_0

    .line 210
    :catch_0
    move-exception v0

    .line 211
    .local v0, "e":Ljava/net/UnknownHostException;
    invoke-virtual {v0}, Ljava/net/UnknownHostException;->printStackTrace()V

    goto :goto_0
.end method

.method private getDNSAddress()[Ljava/net/InetAddress;
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 220
    iget-object v5, p0, Lcom/samsung/android/smartbonding/Dns46NameResolver;->context:Landroid/content/Context;

    const-string v6, "connectivity"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 222
    .local v0, "cm":Landroid/net/ConnectivityManager;
    const/4 v5, 0x5

    invoke-virtual {v0, v5}, Landroid/net/ConnectivityManager;->getLinkProperties(I)Landroid/net/LinkProperties;

    move-result-object v4

    .line 224
    .local v4, "linkProp":Landroid/net/LinkProperties;
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Landroid/net/LinkProperties;->hasGlobalIPv6Address()Z

    move-result v5

    if-nez v5, :cond_1

    .line 225
    :cond_0
    const-string v5, "cannot get hipri connection"

    invoke-static {v5}, Lcom/samsung/android/smartbonding/Dns46NameResolver;->log(Ljava/lang/String;)V

    .line 226
    invoke-virtual {v0, v7}, Landroid/net/ConnectivityManager;->getLinkProperties(I)Landroid/net/LinkProperties;

    move-result-object v4

    .line 228
    :cond_1
    if-eqz v4, :cond_2

    invoke-virtual {v4}, Landroid/net/LinkProperties;->hasGlobalIPv6Address()Z

    move-result v5

    if-nez v5, :cond_3

    .line 229
    :cond_2
    new-array v5, v7, [Ljava/net/InetAddress;

    .line 238
    :goto_0
    return-object v5

    .line 231
    :cond_3
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "linkProperties: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/android/smartbonding/Dns46NameResolver;->log(Ljava/lang/String;)V

    .line 233
    invoke-virtual {v4}, Landroid/net/LinkProperties;->getDnses()Ljava/util/Collection;

    move-result-object v1

    .line 234
    .local v1, "dnses":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/net/InetAddress;>;"
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/net/InetAddress;

    .line 235
    .local v3, "ia":Ljava/net/InetAddress;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "dns: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Ljava/net/InetAddress;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/android/smartbonding/Dns46NameResolver;->log(Ljava/lang/String;)V

    goto :goto_1

    .line 238
    .end local v3    # "ia":Ljava/net/InetAddress;
    :cond_4
    new-array v5, v7, [Ljava/net/InetAddress;

    invoke-interface {v1, v5}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Ljava/net/InetAddress;

    goto :goto_0
.end method

.method private getPlatPrefix(Lcom/samsung/android/org/xbill/DNS/Resolver;)Ljava/net/InetAddress;
    .locals 12
    .param p1, "resolver"    # Lcom/samsung/android/org/xbill/DNS/Resolver;

    .prologue
    const/4 v11, 0x0

    .line 115
    const-string v4, "ipv4only.arpa"

    .line 117
    .local v4, "ipv4only":Ljava/lang/String;
    const/4 v8, 0x0

    .line 120
    .local v8, "prefixRecords":[Lcom/samsung/android/org/xbill/DNS/Record;
    const/4 v6, 0x0

    .line 121
    .local v6, "prefixLookup":Lcom/samsung/android/org/xbill/DNS/Lookup;
    :try_start_0
    new-instance v7, Lcom/samsung/android/org/xbill/DNS/Lookup;

    const-string v9, "ipv4only.arpa"

    const/16 v10, 0x1c

    invoke-direct {v7, v9, v10}, Lcom/samsung/android/org/xbill/DNS/Lookup;-><init>(Ljava/lang/String;I)V
    :try_end_0
    .catch Lcom/samsung/android/org/xbill/DNS/TextParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 122
    .end local v6    # "prefixLookup":Lcom/samsung/android/org/xbill/DNS/Lookup;
    .local v7, "prefixLookup":Lcom/samsung/android/org/xbill/DNS/Lookup;
    :try_start_1
    invoke-virtual {v7, p1}, Lcom/samsung/android/org/xbill/DNS/Lookup;->setResolver(Lcom/samsung/android/org/xbill/DNS/Resolver;)V

    .line 124
    const-string v9, "ORAFIX lookup prefix"

    invoke-static {v9}, Lcom/samsung/android/smartbonding/Dns46NameResolver;->log(Ljava/lang/String;)V

    .line 125
    invoke-virtual {v7}, Lcom/samsung/android/org/xbill/DNS/Lookup;->run()[Lcom/samsung/android/org/xbill/DNS/Record;
    :try_end_1
    .catch Lcom/samsung/android/org/xbill/DNS/TextParseException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v8

    .line 130
    .end local v7    # "prefixLookup":Lcom/samsung/android/org/xbill/DNS/Lookup;
    :goto_0
    if-nez v8, :cond_3

    .line 131
    const-string v9, "prefixRecords null"

    invoke-static {v9}, Lcom/samsung/android/smartbonding/Dns46NameResolver;->log(Ljava/lang/String;)V

    .line 139
    :cond_0
    const/4 v5, 0x0

    .line 140
    .local v5, "ipv6prefix":Ljava/net/InetAddress;
    if-eqz v8, :cond_1

    .line 141
    aget-object v9, v8, v11

    instance-of v9, v9, Lcom/samsung/android/org/xbill/DNS/AAAARecord;

    if-eqz v9, :cond_1

    .line 142
    aget-object v0, v8, v11

    check-cast v0, Lcom/samsung/android/org/xbill/DNS/AAAARecord;

    .line 143
    .local v0, "aaaaRecord":Lcom/samsung/android/org/xbill/DNS/AAAARecord;
    if-eqz v0, :cond_1

    .line 144
    invoke-virtual {v0}, Lcom/samsung/android/org/xbill/DNS/AAAARecord;->getAddress()Ljava/net/InetAddress;

    move-result-object v5

    .line 148
    .end local v0    # "aaaaRecord":Lcom/samsung/android/org/xbill/DNS/AAAARecord;
    :cond_1
    if-nez v5, :cond_2

    .line 150
    :try_start_2
    const-string v9, "2001:db8:1:2:3:4::"

    invoke-static {v9}, Ljava/net/Inet6Address;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;
    :try_end_2
    .catch Ljava/net/UnknownHostException; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v5

    .line 154
    :goto_1
    const-string v9, "missing prefix, get default 2001:db8:1:2:3:4::"

    invoke-static {v9}, Lcom/samsung/android/smartbonding/Dns46NameResolver;->log(Ljava/lang/String;)V

    .line 156
    :cond_2
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "prefix: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/samsung/android/smartbonding/Dns46NameResolver;->log(Ljava/lang/String;)V

    .line 158
    return-object v5

    .line 126
    .end local v5    # "ipv6prefix":Ljava/net/InetAddress;
    .restart local v6    # "prefixLookup":Lcom/samsung/android/org/xbill/DNS/Lookup;
    :catch_0
    move-exception v2

    .line 127
    .local v2, "e1":Lcom/samsung/android/org/xbill/DNS/TextParseException;
    :goto_2
    invoke-virtual {v2}, Lcom/samsung/android/org/xbill/DNS/TextParseException;->printStackTrace()V

    goto :goto_0

    .line 133
    .end local v2    # "e1":Lcom/samsung/android/org/xbill/DNS/TextParseException;
    .end local v6    # "prefixLookup":Lcom/samsung/android/org/xbill/DNS/Lookup;
    :cond_3
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_3
    array-length v9, v8

    if-ge v3, v9, :cond_0

    .line 134
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "record "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    aget-object v10, v8, v3

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/samsung/android/smartbonding/Dns46NameResolver;->log(Ljava/lang/String;)V

    .line 133
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 151
    .end local v3    # "i":I
    .restart local v5    # "ipv6prefix":Ljava/net/InetAddress;
    :catch_1
    move-exception v1

    .line 152
    .local v1, "e":Ljava/net/UnknownHostException;
    invoke-virtual {v1}, Ljava/net/UnknownHostException;->printStackTrace()V

    goto :goto_1

    .line 126
    .end local v1    # "e":Ljava/net/UnknownHostException;
    .end local v5    # "ipv6prefix":Ljava/net/InetAddress;
    .restart local v7    # "prefixLookup":Lcom/samsung/android/org/xbill/DNS/Lookup;
    :catch_2
    move-exception v2

    move-object v6, v7

    .end local v7    # "prefixLookup":Lcom/samsung/android/org/xbill/DNS/Lookup;
    .restart local v6    # "prefixLookup":Lcom/samsung/android/org/xbill/DNS/Lookup;
    goto :goto_2
.end method

.method private static log(Ljava/lang/String;)V
    .locals 2
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 243
    const-string v0, "DNS46"

    .line 244
    .local v0, "TAG":Ljava/lang/String;
    sget-boolean v1, Lcom/samsung/android/smartbonding/Dns46NameResolver;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 245
    const-string v1, "DNS46"

    invoke-static {v1, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 246
    :cond_0
    return-void
.end method

.method private setupResolver()Lcom/samsung/android/org/xbill/DNS/Resolver;
    .locals 11

    .prologue
    .line 162
    const-string v10, "setup resolver"

    invoke-static {v10}, Lcom/samsung/android/smartbonding/Dns46NameResolver;->log(Ljava/lang/String;)V

    .line 163
    invoke-direct {p0}, Lcom/samsung/android/smartbonding/Dns46NameResolver;->getDNSAddress()[Ljava/net/InetAddress;

    move-result-object v2

    .line 164
    .local v2, "dnses":[Ljava/net/InetAddress;
    invoke-virtual {p0, v2}, Lcom/samsung/android/smartbonding/Dns46NameResolver;->setupRouting([Ljava/net/InetAddress;)V

    .line 165
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 167
    .local v8, "resolvers":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/org/xbill/DNS/SimpleResolver;>;"
    move-object v0, v2

    .local v0, "arr$":[Ljava/net/InetAddress;
    :try_start_0
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_0

    aget-object v1, v0, v4

    .line 168
    .local v1, "dns":Ljava/net/InetAddress;
    new-instance v9, Lcom/samsung/android/org/xbill/DNS/SimpleResolver;

    invoke-virtual {v1}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Lcom/samsung/android/org/xbill/DNS/SimpleResolver;-><init>(Ljava/lang/String;)V

    .line 169
    .local v9, "simple":Lcom/samsung/android/org/xbill/DNS/SimpleResolver;
    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0

    .line 167
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 171
    .end local v1    # "dns":Ljava/net/InetAddress;
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    .end local v9    # "simple":Lcom/samsung/android/org/xbill/DNS/SimpleResolver;
    :catch_0
    move-exception v3

    .line 172
    .local v3, "e":Ljava/net/UnknownHostException;
    invoke-virtual {v3}, Ljava/net/UnknownHostException;->printStackTrace()V

    .line 174
    .end local v3    # "e":Ljava/net/UnknownHostException;
    :cond_0
    const/4 v6, 0x0

    .line 176
    .local v6, "resolver":Lcom/samsung/android/org/xbill/DNS/ExtendedResolver;
    :try_start_1
    new-instance v7, Lcom/samsung/android/org/xbill/DNS/ExtendedResolver;

    const/4 v10, 0x0

    new-array v10, v10, [Lcom/samsung/android/org/xbill/DNS/SimpleResolver;

    invoke-interface {v8, v10}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v10

    check-cast v10, [Lcom/samsung/android/org/xbill/DNS/Resolver;

    invoke-direct {v7, v10}, Lcom/samsung/android/org/xbill/DNS/ExtendedResolver;-><init>([Lcom/samsung/android/org/xbill/DNS/Resolver;)V
    :try_end_1
    .catch Ljava/net/UnknownHostException; {:try_start_1 .. :try_end_1} :catch_1

    .end local v6    # "resolver":Lcom/samsung/android/org/xbill/DNS/ExtendedResolver;
    .local v7, "resolver":Lcom/samsung/android/org/xbill/DNS/ExtendedResolver;
    move-object v6, v7

    .line 181
    .end local v7    # "resolver":Lcom/samsung/android/org/xbill/DNS/ExtendedResolver;
    .restart local v6    # "resolver":Lcom/samsung/android/org/xbill/DNS/ExtendedResolver;
    :goto_1
    return-object v6

    .line 177
    :catch_1
    move-exception v3

    .line 178
    .restart local v3    # "e":Ljava/net/UnknownHostException;
    invoke-virtual {v3}, Ljava/net/UnknownHostException;->printStackTrace()V

    goto :goto_1
.end method


# virtual methods
.method public getAllByNameWithPlat(Ljava/lang/String;)[Ljava/net/InetAddress;
    .locals 19
    .param p1, "host"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/UnknownHostException;
        }
    .end annotation

    .prologue
    .line 60
    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "getAllByNameWithPlat "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Lcom/samsung/android/smartbonding/Dns46NameResolver;->log(Ljava/lang/String;)V

    .line 62
    const/4 v11, 0x0

    .line 64
    .local v11, "name":Lcom/samsung/android/org/xbill/DNS/Name;
    :try_start_0
    new-instance v11, Lcom/samsung/android/org/xbill/DNS/Name;

    .end local v11    # "name":Lcom/samsung/android/org/xbill/DNS/Name;
    move-object/from16 v0, p1

    invoke-direct {v11, v0}, Lcom/samsung/android/org/xbill/DNS/Name;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/samsung/android/org/xbill/DNS/TextParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 69
    .restart local v11    # "name":Lcom/samsung/android/org/xbill/DNS/Name;
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/smartbonding/Dns46NameResolver;->setupResolver()Lcom/samsung/android/org/xbill/DNS/Resolver;

    move-result-object v14

    .line 71
    .local v14, "resolver":Lcom/samsung/android/org/xbill/DNS/Resolver;
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/samsung/android/smartbonding/Dns46NameResolver;->getPlatPrefix(Lcom/samsung/android/org/xbill/DNS/Resolver;)Ljava/net/InetAddress;

    move-result-object v9

    .line 72
    .local v9, "ipv6prefix":Ljava/net/InetAddress;
    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "plat prefix: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Lcom/samsung/android/smartbonding/Dns46NameResolver;->log(Ljava/lang/String;)V

    .line 74
    new-instance v5, Lcom/samsung/android/org/xbill/DNS/Lookup;

    const/16 v16, 0x1

    move/from16 v0, v16

    invoke-direct {v5, v11, v0}, Lcom/samsung/android/org/xbill/DNS/Lookup;-><init>(Lcom/samsung/android/org/xbill/DNS/Name;I)V

    .line 75
    .local v5, "hostLookup":Lcom/samsung/android/org/xbill/DNS/Lookup;
    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "lookup host: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Lcom/samsung/android/smartbonding/Dns46NameResolver;->log(Ljava/lang/String;)V

    .line 77
    invoke-virtual {v5, v14}, Lcom/samsung/android/org/xbill/DNS/Lookup;->setResolver(Lcom/samsung/android/org/xbill/DNS/Resolver;)V

    .line 78
    new-instance v16, Lcom/samsung/android/org/xbill/DNS/Cache;

    invoke-direct/range {v16 .. v16}, Lcom/samsung/android/org/xbill/DNS/Cache;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Lcom/samsung/android/org/xbill/DNS/Lookup;->setCache(Lcom/samsung/android/org/xbill/DNS/Cache;)V

    .line 79
    invoke-virtual {v5}, Lcom/samsung/android/org/xbill/DNS/Lookup;->run()[Lcom/samsung/android/org/xbill/DNS/Record;

    move-result-object v6

    .line 81
    .local v6, "hostRecords":[Lcom/samsung/android/org/xbill/DNS/Record;
    if-nez v6, :cond_0

    .line 82
    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "cannot find "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v11}, Lcom/samsung/android/org/xbill/DNS/Name;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Lcom/samsung/android/smartbonding/Dns46NameResolver;->log(Ljava/lang/String;)V

    .line 83
    new-instance v16, Ljava/net/UnknownHostException;

    move-object/from16 v0, v16

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Ljava/net/UnknownHostException;-><init>(Ljava/lang/String;)V

    throw v16

    .line 65
    .end local v5    # "hostLookup":Lcom/samsung/android/org/xbill/DNS/Lookup;
    .end local v6    # "hostRecords":[Lcom/samsung/android/org/xbill/DNS/Record;
    .end local v9    # "ipv6prefix":Ljava/net/InetAddress;
    .end local v11    # "name":Lcom/samsung/android/org/xbill/DNS/Name;
    .end local v14    # "resolver":Lcom/samsung/android/org/xbill/DNS/Resolver;
    :catch_0
    move-exception v4

    .line 66
    .local v4, "e":Lcom/samsung/android/org/xbill/DNS/TextParseException;
    new-instance v16, Ljava/net/UnknownHostException;

    move-object/from16 v0, v16

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Ljava/net/UnknownHostException;-><init>(Ljava/lang/String;)V

    throw v16

    .line 87
    .end local v4    # "e":Lcom/samsung/android/org/xbill/DNS/TextParseException;
    .restart local v5    # "hostLookup":Lcom/samsung/android/org/xbill/DNS/Lookup;
    .restart local v6    # "hostRecords":[Lcom/samsung/android/org/xbill/DNS/Record;
    .restart local v9    # "ipv6prefix":Ljava/net/InetAddress;
    .restart local v11    # "name":Lcom/samsung/android/org/xbill/DNS/Name;
    .restart local v14    # "resolver":Lcom/samsung/android/org/xbill/DNS/Resolver;
    :cond_0
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 88
    .local v15, "solution":Ljava/util/List;, "Ljava/util/List<Ljava/net/InetAddress;>;"
    move-object v3, v6

    .local v3, "arr$":[Lcom/samsung/android/org/xbill/DNS/Record;
    array-length v10, v3

    .local v10, "len$":I
    const/4 v7, 0x0

    .local v7, "i$":I
    :goto_0
    if-ge v7, v10, :cond_2

    aget-object v13, v3, v7

    .line 89
    .local v13, "record":Lcom/samsung/android/org/xbill/DNS/Record;
    const/4 v2, 0x0

    .line 90
    .local v2, "aRecord":Lcom/samsung/android/org/xbill/DNS/ARecord;
    instance-of v0, v13, Lcom/samsung/android/org/xbill/DNS/ARecord;

    move/from16 v16, v0

    if-eqz v16, :cond_1

    move-object v2, v13

    .line 91
    check-cast v2, Lcom/samsung/android/org/xbill/DNS/ARecord;

    .line 95
    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "ORAFIX record: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v13}, Lcom/samsung/android/org/xbill/DNS/Record;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Lcom/samsung/android/smartbonding/Dns46NameResolver;->log(Ljava/lang/String;)V

    .line 97
    invoke-virtual {v2}, Lcom/samsung/android/org/xbill/DNS/ARecord;->getAddress()Ljava/net/InetAddress;

    move-result-object v8

    .line 98
    .local v8, "inet":Ljava/net/InetAddress;
    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "ORAFIX inet: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v8}, Ljava/net/InetAddress;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Lcom/samsung/android/smartbonding/Dns46NameResolver;->log(Ljava/lang/String;)V

    .line 99
    invoke-static {v9, v8}, Lcom/samsung/android/smartbonding/Dns46NameResolver;->convertToIpv6(Ljava/net/InetAddress;Ljava/net/InetAddress;)Ljava/net/InetAddress;

    move-result-object v12

    .line 100
    .local v12, "newAddress":Ljava/net/InetAddress;
    if-eqz v12, :cond_1

    .line 101
    invoke-interface {v15, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 88
    .end local v8    # "inet":Ljava/net/InetAddress;
    .end local v12    # "newAddress":Ljava/net/InetAddress;
    :cond_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 104
    .end local v2    # "aRecord":Lcom/samsung/android/org/xbill/DNS/ARecord;
    .end local v13    # "record":Lcom/samsung/android/org/xbill/DNS/Record;
    :cond_2
    const-string v16, "finish"

    invoke-static/range {v16 .. v16}, Lcom/samsung/android/smartbonding/Dns46NameResolver;->log(Ljava/lang/String;)V

    .line 105
    invoke-interface {v15}, Ljava/util/List;->isEmpty()Z

    move-result v16

    if-eqz v16, :cond_3

    .line 106
    new-instance v16, Ljava/net/UnknownHostException;

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    const-string v18, "cannot convert "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, " to IPv6"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-direct/range {v16 .. v17}, Ljava/net/UnknownHostException;-><init>(Ljava/lang/String;)V

    throw v16

    .line 108
    :cond_3
    const/16 v16, 0x0

    move/from16 v0, v16

    new-array v0, v0, [Ljava/net/InetAddress;

    move-object/from16 v16, v0

    invoke-interface/range {v15 .. v16}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v16

    check-cast v16, [Ljava/net/InetAddress;

    return-object v16
.end method

.method public setupRouting([Ljava/net/InetAddress;)V
    .locals 8
    .param p1, "adresses"    # [Ljava/net/InetAddress;

    .prologue
    .line 185
    const-string v6, "setup routing"

    invoke-static {v6}, Lcom/samsung/android/smartbonding/Dns46NameResolver;->log(Ljava/lang/String;)V

    .line 186
    iget-object v6, p0, Lcom/samsung/android/smartbonding/Dns46NameResolver;->context:Landroid/content/Context;

    const-string v7, "connectivity"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/ConnectivityManager;

    .line 187
    .local v2, "cm":Landroid/net/ConnectivityManager;
    move-object v1, p1

    .local v1, "arr$":[Ljava/net/InetAddress;
    array-length v4, v1

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v0, v1, v3

    .line 188
    .local v0, "addres":Ljava/net/InetAddress;
    const/4 v6, 0x5

    invoke-virtual {v2, v6, v0}, Landroid/net/ConnectivityManager;->requestRouteToHostAddress(ILjava/net/InetAddress;)Z

    move-result v5

    .line 189
    .local v5, "result":Z
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " -> "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/net/InetAddress;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/android/smartbonding/Dns46NameResolver;->log(Ljava/lang/String;)V

    .line 187
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 191
    .end local v0    # "addres":Ljava/net/InetAddress;
    .end local v5    # "result":Z
    :cond_0
    return-void
.end method
