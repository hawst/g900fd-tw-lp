.class public Lcom/samsung/android/smartbonding/TrafficMonitorController;
.super Ljava/lang/Object;
.source "TrafficMonitorController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/smartbonding/TrafficMonitorController$TrafficManagerHandler;
    }
.end annotation


# static fields
.field private static DBG:Z = false

.field private static final EVENT_CREATE_TRAFFIC_TOAST:I = 0x4

.field private static final EVENT_HIDE_TRAFFIC_TOAST:I = 0x5

.field private static final EVENT_START_TRAFFIC_MONITOR:I = 0x1

.field private static final EVENT_STOP_TRAFFIC_MONITOR:I = 0x2

.field private static final EVENT_UPDATE_TRAFFIC_NOTIFICATION:I = 0x3

.field private static final MAX_TOAST_COUNT:I = 0x3

.field private static final MAX_TRAFFIC_SPEED_MBPS:I = 0xc8

.field private static final NETWORK_TYPE_LTE:I = 0x1

.field private static final NETWORK_TYPE_UMTS:I = 0x2

.field private static final SB_BOTH:I = 0x2

.field private static final SB_MOBILE:I = 0x1

.field private static final SB_WIFI:I = 0x0

.field private static SHIP_BUILD:Z = false

.field private static final TAG:Ljava/lang/String; = "TrafficMonitorController"

.field private static final TIME_START_MONITOR_DELAY:I = 0x1f4

.field private static final TIME_TRAFFIC_UPDATE:I = 0x3e8

.field private static VDBG:Z


# instance fields
.field private final MAX_SPEED_OF_DECIMAL_PART:D

.field private final MAX_TRAFFIC_SPEED:[I

.field private final MAX_TRAFFIC_SPEED_CHECK_POINT:[I

.field private isDecimalPart:Z

.field private mBmBar:Landroid/graphics/Bitmap;

.field private mBmBarBg:Landroid/graphics/Bitmap;

.field private mContext:Landroid/content/Context;

.field private mCurBoosterDecimalPartSpeed:D

.field private mCurBoosterSpeed:I

.field private mCurMaxSpeed:I

.field private mCurMobileBytes:J

.field private mCurMobileDecimalPartSpeed:D

.field private mCurMobileSpeed:I

.field private mCurTotalBoosterBytes:J

.field private mCurTotalMobileBytes:J

.field private mCurTotalTrafficTime:J

.field private mCurTotalWifiBytes:J

.field private mCurTrafficTime:J

.field private mCurWifiBytes:J

.field private mCurWifiDecimalPartSpeed:D

.field private mCurWifiSpeed:I

.field private mHandler:Lcom/samsung/android/smartbonding/TrafficMonitorController$TrafficManagerHandler;

.field private mMobileBarBitmap:Landroid/graphics/Bitmap;

.field private mNeedShowTrafficToast:Z

.field private mNetworkType:I

.field private mPhoneStateListener:Landroid/telephony/PhoneStateListener;

.field private mPreMobileBytes:J

.field private mPreTrafficTime:J

.field private mPreWifiBytes:J

.field private mPrevMaxSpeed:I

.field private mSBNotification:Landroid/app/Notification;

.field private mSBToast:Landroid/widget/Toast;

.field private mStartTrafficMonitor:Z

.field private mTm:Landroid/telephony/TelephonyManager;

.field private mToastClickListener:Landroid/view/View$OnClickListener;

.field private mToastLayout:Landroid/view/LayoutInflater;

.field private mTrafficCount:I

.field private mWifiBarBitmap:Landroid/graphics/Bitmap;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 61
    sput-boolean v0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->DBG:Z

    .line 62
    sput-boolean v0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->VDBG:Z

    .line 64
    const-string v0, "true"

    const-string v1, "ro.product_ship"

    const-string v2, "false"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->SHIP_BUILD:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x1

    const-wide/16 v6, 0x0

    const/4 v1, 0x0

    const/4 v4, 0x0

    const-wide/16 v2, 0x0

    .line 127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    iput-boolean v4, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mStartTrafficMonitor:Z

    .line 75
    iput-wide v2, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mPreMobileBytes:J

    .line 76
    iput-wide v2, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mPreWifiBytes:J

    .line 77
    iput-wide v2, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mPreTrafficTime:J

    .line 78
    iput-wide v2, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurMobileBytes:J

    .line 79
    iput-wide v2, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurWifiBytes:J

    .line 80
    iput-wide v2, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurTrafficTime:J

    .line 81
    iput v4, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurMobileSpeed:I

    .line 82
    iput v4, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurWifiSpeed:I

    .line 83
    iput v4, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurBoosterSpeed:I

    .line 84
    iput-wide v2, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurTotalWifiBytes:J

    .line 85
    iput-wide v2, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurTotalMobileBytes:J

    .line 86
    iput-wide v2, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurTotalBoosterBytes:J

    .line 87
    iput-wide v2, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurTotalTrafficTime:J

    .line 88
    iput v4, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mTrafficCount:I

    .line 89
    iput-boolean v5, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mNeedShowTrafficToast:Z

    .line 94
    iput-object v1, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mBmBarBg:Landroid/graphics/Bitmap;

    .line 95
    iput-object v1, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mBmBar:Landroid/graphics/Bitmap;

    .line 96
    iput-object v1, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mWifiBarBitmap:Landroid/graphics/Bitmap;

    .line 97
    iput-object v1, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mMobileBarBitmap:Landroid/graphics/Bitmap;

    .line 99
    iput v4, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mPrevMaxSpeed:I

    .line 100
    iput v4, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurMaxSpeed:I

    .line 101
    const/4 v1, 0x6

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    iput-object v1, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->MAX_TRAFFIC_SPEED_CHECK_POINT:[I

    .line 102
    const/4 v1, 0x6

    new-array v1, v1, [I

    fill-array-data v1, :array_1

    iput-object v1, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->MAX_TRAFFIC_SPEED:[I

    .line 103
    iput-wide v6, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurWifiDecimalPartSpeed:D

    .line 104
    iput-wide v6, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurMobileDecimalPartSpeed:D

    .line 105
    iput-wide v6, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurBoosterDecimalPartSpeed:D

    .line 106
    const-wide/high16 v2, 0x4024000000000000L    # 10.0

    iput-wide v2, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->MAX_SPEED_OF_DECIMAL_PART:D

    .line 107
    iput-boolean v4, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->isDecimalPart:Z

    .line 260
    new-instance v1, Lcom/samsung/android/smartbonding/TrafficMonitorController$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/smartbonding/TrafficMonitorController$1;-><init>(Lcom/samsung/android/smartbonding/TrafficMonitorController;)V

    iput-object v1, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mToastClickListener:Landroid/view/View$OnClickListener;

    .line 715
    new-instance v1, Lcom/samsung/android/smartbonding/TrafficMonitorController$2;

    invoke-direct {v1, p0}, Lcom/samsung/android/smartbonding/TrafficMonitorController$2;-><init>(Lcom/samsung/android/smartbonding/TrafficMonitorController;)V

    iput-object v1, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    .line 128
    sget-boolean v1, Lcom/samsung/android/smartbonding/TrafficMonitorController;->DBG:Z

    if-eqz v1, :cond_0

    const-string v1, "TrafficMonitorController starting up"

    invoke-static {v1}, Lcom/samsung/android/smartbonding/TrafficMonitorController;->log(Ljava/lang/String;)V

    .line 130
    :cond_0
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "TrafficMonitorController"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 131
    .local v0, "handlerThread":Landroid/os/HandlerThread;
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 132
    new-instance v1, Lcom/samsung/android/smartbonding/TrafficMonitorController$TrafficManagerHandler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/samsung/android/smartbonding/TrafficMonitorController$TrafficManagerHandler;-><init>(Lcom/samsung/android/smartbonding/TrafficMonitorController;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mHandler:Lcom/samsung/android/smartbonding/TrafficMonitorController$TrafficManagerHandler;

    .line 133
    iput-object p1, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mContext:Landroid/content/Context;

    .line 135
    invoke-direct {p0}, Lcom/samsung/android/smartbonding/TrafficMonitorController;->initSpeedBarRes()V

    .line 136
    iget-object v1, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mContext:Landroid/content/Context;

    invoke-direct {p0, v1}, Lcom/samsung/android/smartbonding/TrafficMonitorController;->registerPhoneStateListener(Landroid/content/Context;)V

    .line 137
    iput v5, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mNetworkType:I

    .line 139
    sget-boolean v1, Lcom/samsung/android/smartbonding/TrafficMonitorController;->SHIP_BUILD:Z

    if-eqz v1, :cond_1

    .line 140
    sput-boolean v4, Lcom/samsung/android/smartbonding/TrafficMonitorController;->VDBG:Z

    .line 142
    :cond_1
    const-string v1, "TrafficMonitorController: start done"

    invoke-static {v1}, Lcom/samsung/android/smartbonding/TrafficMonitorController;->log(Ljava/lang/String;)V

    .line 143
    return-void

    .line 101
    :array_0
    .array-data 4
        0x96
        0x64
        0x32
        0xf
        0x5
        0x0
    .end array-data

    .line 102
    :array_1
    .array-data 4
        0xc8
        0x96
        0x64
        0x32
        0xf
        0x5
    .end array-data
.end method

.method static synthetic access$000(Lcom/samsung/android/smartbonding/TrafficMonitorController;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/smartbonding/TrafficMonitorController;

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/samsung/android/smartbonding/TrafficMonitorController;->startTrafficMonitor()V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/android/smartbonding/TrafficMonitorController;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/smartbonding/TrafficMonitorController;

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/samsung/android/smartbonding/TrafficMonitorController;->stopTrafficMonitor()V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/android/smartbonding/TrafficMonitorController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/smartbonding/TrafficMonitorController;

    .prologue
    .line 58
    iget-boolean v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mStartTrafficMonitor:Z

    return v0
.end method

.method static synthetic access$300(Lcom/samsung/android/smartbonding/TrafficMonitorController;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/smartbonding/TrafficMonitorController;

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/samsung/android/smartbonding/TrafficMonitorController;->updateTrafficMonitor()V

    return-void
.end method

.method static synthetic access$400(Lcom/samsung/android/smartbonding/TrafficMonitorController;)Lcom/samsung/android/smartbonding/TrafficMonitorController$TrafficManagerHandler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/smartbonding/TrafficMonitorController;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mHandler:Lcom/samsung/android/smartbonding/TrafficMonitorController$TrafficManagerHandler;

    return-object v0
.end method

.method static synthetic access$502(Lcom/samsung/android/smartbonding/TrafficMonitorController;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/smartbonding/TrafficMonitorController;
    .param p1, "x1"    # I

    .prologue
    .line 58
    iput p1, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mNetworkType:I

    return p1
.end method

.method private checkDecimalPart()V
    .locals 4

    .prologue
    .line 436
    iget-wide v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurMobileDecimalPartSpeed:D

    iget-wide v2, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurWifiDecimalPartSpeed:D

    add-double/2addr v0, v2

    const-wide/high16 v2, 0x4024000000000000L    # 10.0

    cmpg-double v0, v0, v2

    if-gez v0, :cond_0

    iget-wide v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurMobileDecimalPartSpeed:D

    iget-wide v2, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurWifiDecimalPartSpeed:D

    add-double/2addr v0, v2

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_0

    .line 438
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->isDecimalPart:Z

    .line 442
    :goto_0
    return-void

    .line 440
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->isDecimalPart:Z

    goto :goto_0
.end method

.method private checkTrafficMonitorCondition()Z
    .locals 3

    .prologue
    .line 252
    const/4 v0, 0x1

    .line 254
    .local v0, "result":Z
    const-string v1, "persist.sb.hide.trafficmonitor"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 255
    const/4 v0, 0x0

    .line 257
    :cond_0
    return v0
.end method

.method private clearNotification()V
    .locals 3

    .prologue
    .line 693
    sget-boolean v1, Lcom/samsung/android/smartbonding/TrafficMonitorController;->VDBG:Z

    if-eqz v1, :cond_0

    const-string v1, "clearNotification"

    invoke-static {v1}, Lcom/samsung/android/smartbonding/TrafficMonitorController;->log(Ljava/lang/String;)V

    .line 694
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mContext:Landroid/content/Context;

    const-string v2, "notification"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 696
    .local v0, "notificationManager":Landroid/app/NotificationManager;
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mSBNotification:Landroid/app/Notification;

    if-eqz v1, :cond_1

    .line 697
    iget-object v1, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mSBNotification:Landroid/app/Notification;

    iget v1, v1, Landroid/app/Notification;->icon:I

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 698
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mSBNotification:Landroid/app/Notification;

    .line 700
    :cond_1
    return-void
.end method

.method private clearSpeedBarRes()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 405
    sget-boolean v0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->DBG:Z

    if-eqz v0, :cond_0

    const-string v0, "clearSpeedBarRes"

    invoke-static {v0}, Lcom/samsung/android/smartbonding/TrafficMonitorController;->log(Ljava/lang/String;)V

    .line 406
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mBmBarBg:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 407
    iput-object v1, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mBmBarBg:Landroid/graphics/Bitmap;

    .line 409
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mBmBar:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_2

    .line 410
    iput-object v1, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mBmBar:Landroid/graphics/Bitmap;

    .line 412
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mWifiBarBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_3

    .line 413
    iput-object v1, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mWifiBarBitmap:Landroid/graphics/Bitmap;

    .line 415
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mMobileBarBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_4

    .line 416
    iput-object v1, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mMobileBarBitmap:Landroid/graphics/Bitmap;

    .line 418
    :cond_4
    return-void
.end method

.method private getBarBitmap(II)Landroid/graphics/Bitmap;
    .locals 13
    .param p1, "speed"    # I
    .param p2, "type"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v12, 0x0

    .line 445
    const/4 v6, 0x0

    .line 446
    .local v6, "bitmap":Landroid/graphics/Bitmap;
    const/4 v7, 0x0

    .line 448
    .local v7, "drawable":Landroid/graphics/drawable/GradientDrawable;
    if-nez p2, :cond_2

    .line 449
    iget-object v6, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mWifiBarBitmap:Landroid/graphics/Bitmap;

    .line 450
    iget-object v2, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x1080124

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    .end local v7    # "drawable":Landroid/graphics/drawable/GradientDrawable;
    check-cast v7, Landroid/graphics/drawable/GradientDrawable;

    .line 456
    .restart local v7    # "drawable":Landroid/graphics/drawable/GradientDrawable;
    :goto_0
    if-eqz v6, :cond_0

    if-nez v7, :cond_3

    .line 457
    :cond_0
    sget-boolean v1, Lcom/samsung/android/smartbonding/TrafficMonitorController;->DBG:Z

    if-eqz v1, :cond_1

    const-string v1, "getBarBitmap : bitmap or drawable is null"

    invoke-static {v1}, Lcom/samsung/android/smartbonding/TrafficMonitorController;->log(Ljava/lang/String;)V

    .line 458
    :cond_1
    const/4 v6, 0x0

    .line 484
    .end local v6    # "bitmap":Landroid/graphics/Bitmap;
    :goto_1
    return-object v6

    .line 452
    .restart local v6    # "bitmap":Landroid/graphics/Bitmap;
    :cond_2
    iget-object v6, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mMobileBarBitmap:Landroid/graphics/Bitmap;

    .line 453
    iget-object v2, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x1080121

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    .end local v7    # "drawable":Landroid/graphics/drawable/GradientDrawable;
    check-cast v7, Landroid/graphics/drawable/GradientDrawable;

    .restart local v7    # "drawable":Landroid/graphics/drawable/GradientDrawable;
    goto :goto_0

    .line 462
    :cond_3
    iget v2, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurMaxSpeed:I

    invoke-static {v2, p1}, Ljava/lang/Math;->min(II)I

    move-result p1

    .line 463
    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    mul-int/2addr v2, p1

    int-to-double v2, v2

    iget v4, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurMaxSpeed:I

    int-to-double v10, v4

    div-double/2addr v2, v10

    double-to-int v9, v2

    .line 465
    .local v9, "width":I
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v6}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 466
    .local v0, "tempCanvas":Landroid/graphics/Canvas;
    invoke-virtual {v0}, Landroid/graphics/Canvas;->save()I

    .line 467
    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    .line 468
    .local v5, "p":Landroid/graphics/Paint;
    new-instance v2, Landroid/graphics/PorterDuffXfermode;

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v2, v3}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v5, v2}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 469
    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-float v3, v2

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v4, v2

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 470
    invoke-virtual {v0}, Landroid/graphics/Canvas;->restore()V

    .line 472
    new-instance v8, Landroid/graphics/Rect;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-direct {v8, v12, v12, v9, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 473
    .local v8, "rect":Landroid/graphics/Rect;
    invoke-virtual {v7, v8}, Landroid/graphics/drawable/GradientDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 474
    invoke-virtual {v0}, Landroid/graphics/Canvas;->save()I

    .line 475
    invoke-virtual {v7, v0}, Landroid/graphics/drawable/GradientDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 476
    invoke-virtual {v0}, Landroid/graphics/Canvas;->restore()V

    .line 478
    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->DST_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v5, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 479
    invoke-virtual {v0}, Landroid/graphics/Canvas;->save()I

    .line 480
    iget-object v1, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mBmBar:Landroid/graphics/Bitmap;

    new-instance v2, Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mBmBar:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mBmBar:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-direct {v2, v12, v12, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v3, Landroid/graphics/Rect;

    iget-object v4, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mBmBar:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-direct {v3, v12, v12, v9, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v0, v1, v2, v3, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 482
    invoke-virtual {v0}, Landroid/graphics/Canvas;->restore()V

    goto/16 :goto_1
.end method

.method private getMobileBytes()J
    .locals 4

    .prologue
    .line 377
    :try_start_0
    invoke-static {}, Landroid/net/TrafficStats;->getMobileRxBytes()J
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 380
    :goto_0
    return-wide v2

    .line 378
    :catch_0
    move-exception v0

    .line 379
    .local v0, "ex":Ljava/lang/Throwable;
    sget-boolean v1, Lcom/samsung/android/smartbonding/TrafficMonitorController;->DBG:Z

    if-eqz v1, :cond_0

    const-string v1, "getMobileBytes : exception"

    invoke-static {v1}, Lcom/samsung/android/smartbonding/TrafficMonitorController;->log(Ljava/lang/String;)V

    .line 380
    :cond_0
    const-wide/16 v2, 0x0

    goto :goto_0
.end method

.method private getTrafficMonitorType()I
    .locals 2

    .prologue
    .line 385
    const-string v0, "persist.sb.monitortype"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method private getWifiBytes()J
    .locals 10

    .prologue
    const-wide/16 v4, 0x0

    .line 366
    :try_start_0
    invoke-static {}, Landroid/net/TrafficStats;->getTotalRxBytes()J

    move-result-wide v6

    invoke-static {}, Landroid/net/TrafficStats;->getMobileRxBytes()J
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v8

    sub-long v2, v6, v8

    .line 367
    .local v2, "wifiBytes":J
    cmp-long v1, v2, v4

    if-gez v1, :cond_0

    const-wide/16 v2, 0x0

    .line 371
    .end local v2    # "wifiBytes":J
    :cond_0
    :goto_0
    return-wide v2

    .line 369
    :catch_0
    move-exception v0

    .line 370
    .local v0, "ex":Ljava/lang/Throwable;
    sget-boolean v1, Lcom/samsung/android/smartbonding/TrafficMonitorController;->DBG:Z

    if-eqz v1, :cond_1

    const-string v1, "getWifiBytes : exception"

    invoke-static {v1}, Lcom/samsung/android/smartbonding/TrafficMonitorController;->log(Ljava/lang/String;)V

    :cond_1
    move-wide v2, v4

    .line 371
    goto :goto_0
.end method

.method private initSpeedBarRes()V
    .locals 3

    .prologue
    .line 389
    sget-boolean v0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->DBG:Z

    if-eqz v0, :cond_0

    const-string v0, "initSpeedBarRes"

    invoke-static {v0}, Lcom/samsung/android/smartbonding/TrafficMonitorController;->log(Ljava/lang/String;)V

    .line 390
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mBmBarBg:Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    .line 391
    iget-object v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x108072b

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mBmBarBg:Landroid/graphics/Bitmap;

    .line 393
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mBmBar:Landroid/graphics/Bitmap;

    if-nez v0, :cond_2

    .line 394
    iget-object v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x108072a

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mBmBar:Landroid/graphics/Bitmap;

    .line 396
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mWifiBarBitmap:Landroid/graphics/Bitmap;

    if-nez v0, :cond_3

    .line 397
    iget-object v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mBmBarBg:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mBmBarBg:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mWifiBarBitmap:Landroid/graphics/Bitmap;

    .line 399
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mMobileBarBitmap:Landroid/graphics/Bitmap;

    if-nez v0, :cond_4

    .line 400
    iget-object v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mBmBarBg:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mBmBarBg:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mMobileBarBitmap:Landroid/graphics/Bitmap;

    .line 402
    :cond_4
    return-void
.end method

.method private initTrafficMonitor()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const-wide/16 v0, 0x0

    .line 198
    iput-wide v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mPreMobileBytes:J

    .line 199
    iput-wide v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mPreWifiBytes:J

    .line 200
    iput-wide v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mPreTrafficTime:J

    .line 201
    iput-wide v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurMobileBytes:J

    .line 202
    iput-wide v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurWifiBytes:J

    .line 203
    iput-wide v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurTrafficTime:J

    .line 204
    iput v2, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurMobileSpeed:I

    .line 205
    iput v2, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurWifiSpeed:I

    .line 206
    iput v2, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurBoosterSpeed:I

    .line 208
    iput-wide v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurTotalWifiBytes:J

    .line 209
    iput-wide v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurTotalMobileBytes:J

    .line 210
    iput-wide v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurTotalBoosterBytes:J

    .line 211
    iput-wide v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurTotalTrafficTime:J

    .line 213
    iput v2, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mPrevMaxSpeed:I

    .line 214
    iput v2, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurMaxSpeed:I

    .line 215
    iput-boolean v2, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->isDecimalPart:Z

    .line 216
    return-void
.end method

.method private static log(Ljava/lang/String;)V
    .locals 1
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 703
    const-string v0, "TrafficMonitorController"

    invoke-static {v0, p0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 704
    return-void
.end method

.method private static loge(Ljava/lang/String;)V
    .locals 1
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 707
    const-string v0, "TrafficMonitorController"

    invoke-static {v0, p0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 708
    return-void
.end method

.method private registerPhoneStateListener(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 729
    iget-object v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mTm:Landroid/telephony/TelephonyManager;

    if-nez v0, :cond_0

    .line 730
    const-string v0, "phone"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mTm:Landroid/telephony/TelephonyManager;

    .line 732
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mTm:Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 733
    return-void
.end method

.method private setCurMaxSpeed(I)V
    .locals 2
    .param p1, "speed"    # I

    .prologue
    .line 421
    iget v1, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mPrevMaxSpeed:I

    if-ge p1, v1, :cond_0

    .line 433
    :goto_0
    return-void

    .line 425
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v1, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->MAX_TRAFFIC_SPEED_CHECK_POINT:[I

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 426
    iget-object v1, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->MAX_TRAFFIC_SPEED_CHECK_POINT:[I

    aget v1, v1, v0

    if-lt p1, v1, :cond_2

    .line 427
    iget-object v1, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->MAX_TRAFFIC_SPEED:[I

    aget v1, v1, v0

    iput v1, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurMaxSpeed:I

    .line 432
    :cond_1
    iget v1, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurMaxSpeed:I

    iput v1, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mPrevMaxSpeed:I

    goto :goto_0

    .line 425
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private setSpeedTextOfBar()V
    .locals 13

    .prologue
    const v12, 0x10203e7

    const v11, 0x10203e6

    const v10, 0x10203e4

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 631
    iget-boolean v1, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->isDecimalPart:Z

    if-eqz v1, :cond_0

    .line 632
    iget-object v1, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x104091e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 634
    .local v0, "mbps":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mSBNotification:Landroid/app/Notification;

    iget-object v1, v1, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "%.1f "

    new-array v4, v9, [Ljava/lang/Object;

    iget-wide v6, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurMobileDecimalPartSpeed:D

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v12, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 635
    iget-object v1, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mSBNotification:Landroid/app/Notification;

    iget-object v1, v1, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "%.1f "

    new-array v4, v9, [Ljava/lang/Object;

    iget-wide v6, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurWifiDecimalPartSpeed:D

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v11, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 636
    iget-object v1, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mSBNotification:Landroid/app/Notification;

    iget-object v1, v1, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "%.1f "

    new-array v4, v9, [Ljava/lang/Object;

    iget-wide v6, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurBoosterDecimalPartSpeed:D

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v10, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 638
    iget-object v1, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mSBNotification:Landroid/app/Notification;

    iget-object v1, v1, Landroid/app/Notification;->bigContentView:Landroid/widget/RemoteViews;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "%.1f "

    new-array v4, v9, [Ljava/lang/Object;

    iget-wide v6, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurMobileDecimalPartSpeed:D

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v12, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 639
    iget-object v1, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mSBNotification:Landroid/app/Notification;

    iget-object v1, v1, Landroid/app/Notification;->bigContentView:Landroid/widget/RemoteViews;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "%.1f "

    new-array v4, v9, [Ljava/lang/Object;

    iget-wide v6, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurWifiDecimalPartSpeed:D

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v11, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 640
    iget-object v1, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mSBNotification:Landroid/app/Notification;

    iget-object v1, v1, Landroid/app/Notification;->bigContentView:Landroid/widget/RemoteViews;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "%.1f "

    new-array v4, v9, [Ljava/lang/Object;

    iget-wide v6, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurBoosterDecimalPartSpeed:D

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v10, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 653
    :goto_0
    return-void

    .line 643
    .end local v0    # "mbps":Ljava/lang/String;
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x104091f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 645
    .restart local v0    # "mbps":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mSBNotification:Landroid/app/Notification;

    iget-object v1, v1, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    new-array v2, v9, [Ljava/lang/Object;

    iget v3, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurMobileSpeed:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v8

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v12, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 646
    iget-object v1, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mSBNotification:Landroid/app/Notification;

    iget-object v1, v1, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    new-array v2, v9, [Ljava/lang/Object;

    iget v3, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurWifiSpeed:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v8

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v11, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 647
    iget-object v1, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mSBNotification:Landroid/app/Notification;

    iget-object v1, v1, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    new-array v2, v9, [Ljava/lang/Object;

    iget v3, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurBoosterSpeed:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v8

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v10, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 649
    iget-object v1, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mSBNotification:Landroid/app/Notification;

    iget-object v1, v1, Landroid/app/Notification;->bigContentView:Landroid/widget/RemoteViews;

    new-array v2, v9, [Ljava/lang/Object;

    iget v3, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurMobileSpeed:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v8

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v12, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 650
    iget-object v1, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mSBNotification:Landroid/app/Notification;

    iget-object v1, v1, Landroid/app/Notification;->bigContentView:Landroid/widget/RemoteViews;

    new-array v2, v9, [Ljava/lang/Object;

    iget v3, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurWifiSpeed:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v8

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v11, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 651
    iget-object v1, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mSBNotification:Landroid/app/Notification;

    iget-object v1, v1, Landroid/app/Notification;->bigContentView:Landroid/widget/RemoteViews;

    new-array v2, v9, [Ljava/lang/Object;

    iget v3, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurBoosterSpeed:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v8

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v10, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_0
.end method

.method private showSpeedBarNotification(Z)V
    .locals 13
    .param p1, "isOnGoing"    # Z

    .prologue
    .line 562
    sget-boolean v0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->DBG:Z

    if-eqz v0, :cond_0

    const-string v0, "showSpeedBarNotification"

    invoke-static {v0}, Lcom/samsung/android/smartbonding/TrafficMonitorController;->log(Ljava/lang/String;)V

    .line 564
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mContext:Landroid/content/Context;

    const-string v1, "notification"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/app/NotificationManager;

    .line 567
    .local v10, "notificationManager":Landroid/app/NotificationManager;
    if-nez v10, :cond_2

    .line 568
    sget-boolean v0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->VDBG:Z

    if-eqz v0, :cond_1

    const-string v0, "showSpeedBarNotification : notification manager is null"

    invoke-static {v0}, Lcom/samsung/android/smartbonding/TrafficMonitorController;->log(Ljava/lang/String;)V

    .line 628
    :cond_1
    :goto_0
    return-void

    .line 572
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mSBNotification:Landroid/app/Notification;

    if-eqz v0, :cond_3

    .line 573
    sget-boolean v0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->VDBG:Z

    if-eqz v0, :cond_1

    const-string v0, "showSpeedBarNotification : notification already exist"

    invoke-static {v0}, Lcom/samsung/android/smartbonding/TrafficMonitorController;->log(Ljava/lang/String;)V

    goto :goto_0

    .line 577
    :cond_3
    new-instance v7, Landroid/widget/RemoteViews;

    iget-object v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const v1, 0x109008f

    invoke-direct {v7, v0, v1}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 578
    .local v7, "contentView":Landroid/widget/RemoteViews;
    new-instance v6, Landroid/widget/RemoteViews;

    iget-object v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const v1, 0x109008e

    invoke-direct {v6, v0, v1}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 580
    .local v6, "bigContentView":Landroid/widget/RemoteViews;
    const v0, 0x10203e9

    const-string v1, "setImageBitmap"

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {p0, v3, v4}, Lcom/samsung/android/smartbonding/TrafficMonitorController;->getBarBitmap(II)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v6, v0, v1, v3}, Landroid/widget/RemoteViews;->setBitmap(ILjava/lang/String;Landroid/graphics/Bitmap;)V

    .line 581
    const v0, 0x10203ea

    const-string v1, "setImageBitmap"

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-direct {p0, v3, v4}, Lcom/samsung/android/smartbonding/TrafficMonitorController;->getBarBitmap(II)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v6, v0, v1, v3}, Landroid/widget/RemoteViews;->setBitmap(ILjava/lang/String;Landroid/graphics/Bitmap;)V

    .line 583
    iget-object v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x104091f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 584
    .local v9, "mbps":Ljava/lang/String;
    iget-object v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x104091c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 585
    .local v8, "lte":Ljava/lang/String;
    iget-object v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x104091d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 587
    .local v12, "umts":Ljava/lang/String;
    iget v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mNetworkType:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_6

    .line 588
    const v0, 0x10203e3

    invoke-virtual {v7, v0, v8}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 589
    const v0, 0x10203e3

    invoke-virtual {v6, v0, v8}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 595
    :cond_4
    :goto_1
    const v0, 0x10203e7

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v3

    invoke-static {v9, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 596
    const v0, 0x10203e6

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v3

    invoke-static {v9, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 597
    const v0, 0x10203e4

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v3

    invoke-static {v9, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 599
    const v0, 0x10203e7

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v3

    invoke-static {v9, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 600
    const v0, 0x10203e6

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v3

    invoke-static {v9, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 601
    const v0, 0x10203e4

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v3

    invoke-static {v9, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 604
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 605
    .local v2, "intent":Landroid/content/Intent;
    const-string v0, "com.android.settings.smartbonding"

    const-string v1, "com.android.settings.smartbonding.SmartBondingSettings"

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 606
    const/high16 v0, 0x14000000

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 608
    iget-object v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mContext:Landroid/content/Context;

    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    sget-object v5, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-static/range {v0 .. v5}, Landroid/app/PendingIntent;->getActivityAsUser(Landroid/content/Context;ILandroid/content/Intent;ILandroid/os/Bundle;Landroid/os/UserHandle;)Landroid/app/PendingIntent;

    move-result-object v11

    .line 611
    .local v11, "pi":Landroid/app/PendingIntent;
    iget-object v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mSBNotification:Landroid/app/Notification;

    if-nez v0, :cond_5

    .line 612
    new-instance v0, Landroid/app/Notification;

    invoke-direct {v0}, Landroid/app/Notification;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mSBNotification:Landroid/app/Notification;

    .line 613
    iget-object v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mSBNotification:Landroid/app/Notification;

    const-wide/16 v4, 0x0

    iput-wide v4, v0, Landroid/app/Notification;->when:J

    .line 616
    :cond_5
    iget-object v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mSBNotification:Landroid/app/Notification;

    const v1, 0x1080727

    iput v1, v0, Landroid/app/Notification;->icon:I

    .line 617
    if-eqz p1, :cond_7

    .line 618
    iget-object v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mSBNotification:Landroid/app/Notification;

    const/4 v1, 0x2

    iput v1, v0, Landroid/app/Notification;->flags:I

    .line 623
    :goto_2
    iget-object v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mSBNotification:Landroid/app/Notification;

    const/4 v1, -0x2

    iput v1, v0, Landroid/app/Notification;->priority:I

    .line 624
    iget-object v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mSBNotification:Landroid/app/Notification;

    iput-object v7, v0, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    .line 625
    iget-object v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mSBNotification:Landroid/app/Notification;

    iput-object v6, v0, Landroid/app/Notification;->bigContentView:Landroid/widget/RemoteViews;

    .line 626
    iget-object v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mSBNotification:Landroid/app/Notification;

    const/16 v1, 0x108

    iput v1, v0, Landroid/app/Notification;->twQuickPanelEvent:I

    .line 627
    iget-object v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mSBNotification:Landroid/app/Notification;

    iget v0, v0, Landroid/app/Notification;->icon:I

    iget-object v1, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mSBNotification:Landroid/app/Notification;

    invoke-virtual {v10, v0, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto/16 :goto_0

    .line 590
    .end local v2    # "intent":Landroid/content/Intent;
    .end local v11    # "pi":Landroid/app/PendingIntent;
    :cond_6
    iget v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mNetworkType:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    .line 591
    const v0, 0x10203e3

    invoke-virtual {v7, v0, v12}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 592
    const v0, 0x10203e3

    invoke-virtual {v6, v0, v12}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 620
    .restart local v2    # "intent":Landroid/content/Intent;
    .restart local v11    # "pi":Landroid/app/PendingIntent;
    :cond_7
    iget-object v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mSBNotification:Landroid/app/Notification;

    const/4 v1, 0x4

    iput v1, v0, Landroid/app/Notification;->flags:I

    goto :goto_2
.end method

.method private startTrafficMonitor()V
    .locals 4

    .prologue
    .line 287
    monitor-enter p0

    .line 288
    :try_start_0
    sget-boolean v0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->DBG:Z

    if-eqz v0, :cond_0

    const-string v0, "startTrafficMonitor"

    invoke-static {v0}, Lcom/samsung/android/smartbonding/TrafficMonitorController;->log(Ljava/lang/String;)V

    .line 289
    :cond_0
    iget-boolean v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mStartTrafficMonitor:Z

    if-nez v0, :cond_1

    .line 290
    iget-object v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mHandler:Lcom/samsung/android/smartbonding/TrafficMonitorController$TrafficManagerHandler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/samsung/android/smartbonding/TrafficMonitorController$TrafficManagerHandler;->removeMessages(I)V

    .line 291
    invoke-direct {p0}, Lcom/samsung/android/smartbonding/TrafficMonitorController;->initTrafficMonitor()V

    .line 292
    iget-boolean v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mNeedShowTrafficToast:Z

    if-eqz v0, :cond_2

    .line 293
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mTrafficCount:I

    .line 294
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mNeedShowTrafficToast:Z

    .line 298
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurTrafficTime:J

    .line 299
    invoke-direct {p0}, Lcom/samsung/android/smartbonding/TrafficMonitorController;->getMobileBytes()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurMobileBytes:J

    .line 300
    invoke-direct {p0}, Lcom/samsung/android/smartbonding/TrafficMonitorController;->getWifiBytes()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurWifiBytes:J

    .line 301
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mStartTrafficMonitor:Z

    .line 303
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/samsung/android/smartbonding/TrafficMonitorController;->showSpeedBarNotification(Z)V

    .line 312
    iget-object v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mHandler:Lcom/samsung/android/smartbonding/TrafficMonitorController$TrafficManagerHandler;

    iget-object v1, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mHandler:Lcom/samsung/android/smartbonding/TrafficMonitorController$TrafficManagerHandler;

    const/4 v2, 0x3

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/smartbonding/TrafficMonitorController$TrafficManagerHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/smartbonding/TrafficMonitorController$TrafficManagerHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 314
    :cond_1
    monitor-exit p0

    .line 315
    return-void

    .line 296
    :cond_2
    const/4 v0, 0x3

    iput v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mTrafficCount:I

    goto :goto_0

    .line 314
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private stopTrafficMonitor()V
    .locals 2

    .prologue
    .line 352
    monitor-enter p0

    .line 353
    :try_start_0
    iget-boolean v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mStartTrafficMonitor:Z

    if-eqz v0, :cond_0

    .line 354
    invoke-direct {p0}, Lcom/samsung/android/smartbonding/TrafficMonitorController;->initTrafficMonitor()V

    .line 355
    invoke-direct {p0}, Lcom/samsung/android/smartbonding/TrafficMonitorController;->clearNotification()V

    .line 357
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mTrafficCount:I

    .line 358
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mStartTrafficMonitor:Z

    .line 359
    iget-object v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mHandler:Lcom/samsung/android/smartbonding/TrafficMonitorController$TrafficManagerHandler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/samsung/android/smartbonding/TrafficMonitorController$TrafficManagerHandler;->removeMessages(I)V

    .line 361
    :cond_0
    monitor-exit p0

    .line 362
    return-void

    .line 361
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private updateSpeedBarNotification()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const v5, 0x10203e3

    .line 656
    iget-object v3, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mContext:Landroid/content/Context;

    const-string v4, "notification"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    .line 659
    .local v1, "notificationManager":Landroid/app/NotificationManager;
    if-nez v1, :cond_1

    .line 660
    sget-boolean v3, Lcom/samsung/android/smartbonding/TrafficMonitorController;->DBG:Z

    if-eqz v3, :cond_0

    const-string v3, "updateSpeedNotification : notification manager is null"

    invoke-static {v3}, Lcom/samsung/android/smartbonding/TrafficMonitorController;->log(Ljava/lang/String;)V

    .line 690
    :cond_0
    :goto_0
    return-void

    .line 664
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mSBNotification:Landroid/app/Notification;

    if-nez v3, :cond_2

    .line 665
    sget-boolean v3, Lcom/samsung/android/smartbonding/TrafficMonitorController;->DBG:Z

    if-eqz v3, :cond_0

    const-string v3, "updateSpeedNotification : no notification"

    invoke-static {v3}, Lcom/samsung/android/smartbonding/TrafficMonitorController;->log(Ljava/lang/String;)V

    goto :goto_0

    .line 669
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x104091c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 670
    .local v0, "lte":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x104091d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 672
    .local v2, "umts":Ljava/lang/String;
    iget v3, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mNetworkType:I

    if-ne v3, v8, :cond_4

    .line 673
    iget-object v3, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mSBNotification:Landroid/app/Notification;

    iget-object v3, v3, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    invoke-virtual {v3, v5, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 674
    iget-object v3, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mSBNotification:Landroid/app/Notification;

    iget-object v3, v3, Landroid/app/Notification;->bigContentView:Landroid/widget/RemoteViews;

    invoke-virtual {v3, v5, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 680
    :cond_3
    :goto_1
    iget v3, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurWifiSpeed:I

    iget v4, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurMobileSpeed:I

    if-le v3, v4, :cond_5

    iget v3, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurWifiSpeed:I

    :goto_2
    invoke-direct {p0, v3}, Lcom/samsung/android/smartbonding/TrafficMonitorController;->setCurMaxSpeed(I)V

    .line 682
    invoke-direct {p0}, Lcom/samsung/android/smartbonding/TrafficMonitorController;->setSpeedTextOfBar()V

    .line 684
    iget-object v3, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mSBNotification:Landroid/app/Notification;

    iget-object v3, v3, Landroid/app/Notification;->bigContentView:Landroid/widget/RemoteViews;

    const v4, 0x10203e9

    const-string v5, "setImageBitmap"

    iget v6, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurWifiSpeed:I

    const/4 v7, 0x0

    invoke-direct {p0, v6, v7}, Lcom/samsung/android/smartbonding/TrafficMonitorController;->getBarBitmap(II)Landroid/graphics/Bitmap;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v6}, Landroid/widget/RemoteViews;->setBitmap(ILjava/lang/String;Landroid/graphics/Bitmap;)V

    .line 685
    iget-object v3, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mSBNotification:Landroid/app/Notification;

    iget-object v3, v3, Landroid/app/Notification;->bigContentView:Landroid/widget/RemoteViews;

    const v4, 0x10203ea

    const-string v5, "setImageBitmap"

    iget v6, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurMobileSpeed:I

    invoke-direct {p0, v6, v8}, Lcom/samsung/android/smartbonding/TrafficMonitorController;->getBarBitmap(II)Landroid/graphics/Bitmap;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v6}, Landroid/widget/RemoteViews;->setBitmap(ILjava/lang/String;Landroid/graphics/Bitmap;)V

    .line 687
    iget-object v3, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mSBNotification:Landroid/app/Notification;

    const/4 v4, -0x2

    iput v4, v3, Landroid/app/Notification;->priority:I

    .line 688
    iget-object v3, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mSBNotification:Landroid/app/Notification;

    const/16 v4, 0x108

    iput v4, v3, Landroid/app/Notification;->twQuickPanelEvent:I

    .line 689
    iget-object v3, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mSBNotification:Landroid/app/Notification;

    iget v3, v3, Landroid/app/Notification;->icon:I

    iget-object v4, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mSBNotification:Landroid/app/Notification;

    invoke-virtual {v1, v3, v4}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto/16 :goto_0

    .line 675
    :cond_4
    iget v3, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mNetworkType:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_3

    .line 676
    iget-object v3, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mSBNotification:Landroid/app/Notification;

    iget-object v3, v3, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    invoke-virtual {v3, v5, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 677
    iget-object v3, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mSBNotification:Landroid/app/Notification;

    iget-object v3, v3, Landroid/app/Notification;->bigContentView:Landroid/widget/RemoteViews;

    invoke-virtual {v3, v5, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto :goto_1

    .line 680
    :cond_5
    iget v3, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurMobileSpeed:I

    goto :goto_2
.end method

.method private updateTraffic()V
    .locals 20

    .prologue
    .line 220
    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurTrafficTime:J

    move-object/from16 v0, p0

    iput-wide v14, v0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mPreTrafficTime:J

    .line 221
    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurMobileBytes:J

    move-object/from16 v0, p0

    iput-wide v14, v0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mPreMobileBytes:J

    .line 222
    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurWifiBytes:J

    move-object/from16 v0, p0

    iput-wide v14, v0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mPreWifiBytes:J

    .line 224
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    move-object/from16 v0, p0

    iput-wide v14, v0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurTrafficTime:J

    .line 225
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/smartbonding/TrafficMonitorController;->getMobileBytes()J

    move-result-wide v14

    move-object/from16 v0, p0

    iput-wide v14, v0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurMobileBytes:J

    .line 226
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/smartbonding/TrafficMonitorController;->getWifiBytes()J

    move-result-wide v14

    move-object/from16 v0, p0

    iput-wide v14, v0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurWifiBytes:J

    .line 228
    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurTrafficTime:J

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mPreTrafficTime:J

    move-wide/from16 v16, v0

    sub-long v4, v14, v16

    .line 230
    .local v4, "diffTrafficTime":J
    const-wide/16 v14, 0x0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurMobileBytes:J

    move-wide/from16 v16, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mPreMobileBytes:J

    move-wide/from16 v18, v0

    sub-long v16, v16, v18

    invoke-static/range {v14 .. v17}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    .line 231
    .local v2, "diffMobileTrafficBytes":J
    const-wide/16 v14, 0x0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurWifiBytes:J

    move-wide/from16 v16, v0

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mPreWifiBytes:J

    move-wide/from16 v18, v0

    sub-long v16, v16, v18

    invoke-static/range {v14 .. v17}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v6

    .line 233
    .local v6, "diffWifiTrafficBytes":J
    sget-boolean v14, Lcom/samsung/android/smartbonding/TrafficMonitorController;->VDBG:Z

    if-eqz v14, :cond_0

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "updateTraffic : difftime ("

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ") mobilebytes ("

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ") wifibytes ("

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ")"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/samsung/android/smartbonding/TrafficMonitorController;->log(Ljava/lang/String;)V

    .line 235
    :cond_0
    const-wide/16 v14, 0x3e8

    mul-long/2addr v14, v2

    long-to-double v14, v14

    long-to-double v0, v4

    move-wide/from16 v16, v0

    div-double v8, v14, v16

    .line 236
    .local v8, "valueMobileSpeed":D
    const-wide/16 v14, 0x3e8

    mul-long/2addr v14, v6

    long-to-double v14, v14

    long-to-double v0, v4

    move-wide/from16 v16, v0

    div-double v12, v14, v16

    .line 237
    .local v12, "valueWifiSpeed":D
    add-double v10, v8, v12

    .line 239
    .local v10, "valueSBSpeed":D
    const-wide/high16 v14, 0x4020000000000000L    # 8.0

    mul-double/2addr v14, v8

    const-wide v16, 0x408f400000000000L    # 1000.0

    div-double v14, v14, v16

    const-wide v16, 0x408f400000000000L    # 1000.0

    div-double v14, v14, v16

    double-to-int v14, v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurMobileSpeed:I

    .line 240
    const-wide/high16 v14, 0x4020000000000000L    # 8.0

    mul-double/2addr v14, v12

    const-wide v16, 0x408f400000000000L    # 1000.0

    div-double v14, v14, v16

    const-wide v16, 0x408f400000000000L    # 1000.0

    div-double v14, v14, v16

    double-to-int v14, v14

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurWifiSpeed:I

    .line 241
    const-wide/high16 v14, 0x4020000000000000L    # 8.0

    mul-double/2addr v14, v8

    const-wide v16, 0x408f400000000000L    # 1000.0

    div-double v14, v14, v16

    const-wide v16, 0x408f400000000000L    # 1000.0

    div-double v14, v14, v16

    move-object/from16 v0, p0

    iput-wide v14, v0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurMobileDecimalPartSpeed:D

    .line 242
    const-wide/high16 v14, 0x4020000000000000L    # 8.0

    mul-double/2addr v14, v12

    const-wide v16, 0x408f400000000000L    # 1000.0

    div-double v14, v14, v16

    const-wide v16, 0x408f400000000000L    # 1000.0

    div-double v14, v14, v16

    move-object/from16 v0, p0

    iput-wide v14, v0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurWifiDecimalPartSpeed:D

    .line 245
    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurMobileSpeed:I

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurWifiSpeed:I

    add-int/2addr v14, v15

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurBoosterSpeed:I

    .line 246
    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurMobileDecimalPartSpeed:D

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurWifiDecimalPartSpeed:D

    move-wide/from16 v16, v0

    add-double v14, v14, v16

    move-object/from16 v0, p0

    iput-wide v14, v0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mCurBoosterDecimalPartSpeed:D

    .line 248
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/smartbonding/TrafficMonitorController;->checkDecimalPart()V

    .line 249
    return-void
.end method

.method private updateTrafficMonitor()V
    .locals 4

    .prologue
    .line 318
    monitor-enter p0

    .line 319
    :try_start_0
    invoke-direct {p0}, Lcom/samsung/android/smartbonding/TrafficMonitorController;->updateTraffic()V

    .line 320
    invoke-direct {p0}, Lcom/samsung/android/smartbonding/TrafficMonitorController;->updateSpeedBarNotification()V

    .line 330
    iget v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mTrafficCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mTrafficCount:I

    .line 331
    iget-object v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mHandler:Lcom/samsung/android/smartbonding/TrafficMonitorController$TrafficManagerHandler;

    iget-object v1, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mHandler:Lcom/samsung/android/smartbonding/TrafficMonitorController$TrafficManagerHandler;

    const/4 v2, 0x3

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/smartbonding/TrafficMonitorController$TrafficManagerHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/smartbonding/TrafficMonitorController$TrafficManagerHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 332
    monitor-exit p0

    .line 333
    return-void

    .line 332
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method protected finalize()V
    .locals 1

    .prologue
    .line 146
    sget-boolean v0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->DBG:Z

    if-eqz v0, :cond_0

    const-string v0, "TrafficMonitorController ending"

    invoke-static {v0}, Lcom/samsung/android/smartbonding/TrafficMonitorController;->log(Ljava/lang/String;)V

    .line 147
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/smartbonding/TrafficMonitorController;->clearSpeedBarRes()V

    .line 148
    const-string v0, "TrafficMonitorController: end done"

    invoke-static {v0}, Lcom/samsung/android/smartbonding/TrafficMonitorController;->log(Ljava/lang/String;)V

    .line 149
    return-void
.end method

.method public hideToast()V
    .locals 4

    .prologue
    .line 283
    iget-object v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mHandler:Lcom/samsung/android/smartbonding/TrafficMonitorController$TrafficManagerHandler;

    iget-object v1, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mHandler:Lcom/samsung/android/smartbonding/TrafficMonitorController$TrafficManagerHandler;

    const/4 v2, 0x5

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/smartbonding/TrafficMonitorController$TrafficManagerHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/smartbonding/TrafficMonitorController$TrafficManagerHandler;->sendMessage(Landroid/os/Message;)Z

    .line 284
    return-void
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 268
    iget-boolean v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mStartTrafficMonitor:Z

    return v0
.end method

.method public startMonitor(Z)V
    .locals 4
    .param p1, "showtoast"    # Z

    .prologue
    .line 272
    sget-boolean v0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->DBG:Z

    if-eqz v0, :cond_0

    const-string v0, "startMonitor"

    invoke-static {v0}, Lcom/samsung/android/smartbonding/TrafficMonitorController;->log(Ljava/lang/String;)V

    .line 273
    :cond_0
    iput-boolean p1, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mNeedShowTrafficToast:Z

    .line 274
    iget-object v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mHandler:Lcom/samsung/android/smartbonding/TrafficMonitorController$TrafficManagerHandler;

    iget-object v1, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mHandler:Lcom/samsung/android/smartbonding/TrafficMonitorController$TrafficManagerHandler;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/smartbonding/TrafficMonitorController$TrafficManagerHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/smartbonding/TrafficMonitorController$TrafficManagerHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 275
    return-void
.end method

.method public stopMonitor()V
    .locals 4

    .prologue
    .line 278
    sget-boolean v0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->DBG:Z

    if-eqz v0, :cond_0

    const-string v0, "stopMonitor"

    invoke-static {v0}, Lcom/samsung/android/smartbonding/TrafficMonitorController;->log(Ljava/lang/String;)V

    .line 279
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mHandler:Lcom/samsung/android/smartbonding/TrafficMonitorController$TrafficManagerHandler;

    iget-object v1, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController;->mHandler:Lcom/samsung/android/smartbonding/TrafficMonitorController$TrafficManagerHandler;

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/smartbonding/TrafficMonitorController$TrafficManagerHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/smartbonding/TrafficMonitorController$TrafficManagerHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 280
    return-void
.end method
