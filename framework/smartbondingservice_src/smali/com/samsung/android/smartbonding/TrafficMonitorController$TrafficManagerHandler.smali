.class Lcom/samsung/android/smartbonding/TrafficMonitorController$TrafficManagerHandler;
.super Landroid/os/Handler;
.source "TrafficMonitorController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/smartbonding/TrafficMonitorController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TrafficManagerHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/smartbonding/TrafficMonitorController;


# direct methods
.method public constructor <init>(Lcom/samsung/android/smartbonding/TrafficMonitorController;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .prologue
    .line 152
    iput-object p1, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController$TrafficManagerHandler;->this$0:Lcom/samsung/android/smartbonding/TrafficMonitorController;

    .line 153
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 154
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 157
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 194
    :goto_0
    :pswitch_0
    return-void

    .line 159
    :pswitch_1
    iget-object v1, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController$TrafficManagerHandler;->this$0:Lcom/samsung/android/smartbonding/TrafficMonitorController;

    monitor-enter v1

    .line 160
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController$TrafficManagerHandler;->this$0:Lcom/samsung/android/smartbonding/TrafficMonitorController;

    # invokes: Lcom/samsung/android/smartbonding/TrafficMonitorController;->startTrafficMonitor()V
    invoke-static {v0}, Lcom/samsung/android/smartbonding/TrafficMonitorController;->access$000(Lcom/samsung/android/smartbonding/TrafficMonitorController;)V

    .line 161
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 165
    :pswitch_2
    iget-object v1, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController$TrafficManagerHandler;->this$0:Lcom/samsung/android/smartbonding/TrafficMonitorController;

    monitor-enter v1

    .line 166
    :try_start_1
    iget-object v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController$TrafficManagerHandler;->this$0:Lcom/samsung/android/smartbonding/TrafficMonitorController;

    # invokes: Lcom/samsung/android/smartbonding/TrafficMonitorController;->stopTrafficMonitor()V
    invoke-static {v0}, Lcom/samsung/android/smartbonding/TrafficMonitorController;->access$100(Lcom/samsung/android/smartbonding/TrafficMonitorController;)V

    .line 167
    monitor-exit v1

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v0

    .line 171
    :pswitch_3
    iget-object v1, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController$TrafficManagerHandler;->this$0:Lcom/samsung/android/smartbonding/TrafficMonitorController;

    monitor-enter v1

    .line 173
    :try_start_2
    monitor-exit v1

    goto :goto_0

    :catchall_2
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    throw v0

    .line 181
    :pswitch_4
    iget-object v1, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController$TrafficManagerHandler;->this$0:Lcom/samsung/android/smartbonding/TrafficMonitorController;

    monitor-enter v1

    .line 182
    :try_start_3
    iget-object v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController$TrafficManagerHandler;->this$0:Lcom/samsung/android/smartbonding/TrafficMonitorController;

    # getter for: Lcom/samsung/android/smartbonding/TrafficMonitorController;->mStartTrafficMonitor:Z
    invoke-static {v0}, Lcom/samsung/android/smartbonding/TrafficMonitorController;->access$200(Lcom/samsung/android/smartbonding/TrafficMonitorController;)Z

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    .line 183
    iget-object v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController$TrafficManagerHandler;->this$0:Lcom/samsung/android/smartbonding/TrafficMonitorController;

    # invokes: Lcom/samsung/android/smartbonding/TrafficMonitorController;->updateTrafficMonitor()V
    invoke-static {v0}, Lcom/samsung/android/smartbonding/TrafficMonitorController;->access$300(Lcom/samsung/android/smartbonding/TrafficMonitorController;)V

    .line 187
    :goto_1
    monitor-exit v1

    goto :goto_0

    :catchall_3
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    throw v0

    .line 185
    :cond_0
    :try_start_4
    iget-object v0, p0, Lcom/samsung/android/smartbonding/TrafficMonitorController$TrafficManagerHandler;->this$0:Lcom/samsung/android/smartbonding/TrafficMonitorController;

    # invokes: Lcom/samsung/android/smartbonding/TrafficMonitorController;->stopTrafficMonitor()V
    invoke-static {v0}, Lcom/samsung/android/smartbonding/TrafficMonitorController;->access$100(Lcom/samsung/android/smartbonding/TrafficMonitorController;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    goto :goto_1

    .line 157
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method
