.class Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;
.super Landroid/os/Handler;
.source "SmartBondingService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/smartbonding/SmartBondingService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SmartBondingHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/smartbonding/SmartBondingService;


# direct methods
.method public constructor <init>(Lcom/samsung/android/smartbonding/SmartBondingService;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .prologue
    .line 361
    iput-object p1, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    .line 362
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 363
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 14
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v13, 0x3

    const/4 v2, 0x6

    const/4 v5, 0x1

    const/4 v4, 0x2

    .line 366
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 555
    :goto_0
    :pswitch_0
    return-void

    .line 368
    :pswitch_1
    # getter for: Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z
    invoke-static {}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$000()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "EVENT_ENABLE_SB_INTERFACES called"

    # invokes: Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V
    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$100(Ljava/lang/String;)V

    .line 369
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    monitor-enter v2

    .line 370
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    # invokes: Lcom/samsung/android/smartbonding/SmartBondingService;->checkSiopToastCondition()Z
    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$200(Lcom/samsung/android/smartbonding/SmartBondingService;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 371
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    # getter for: Lcom/samsung/android/smartbonding/SmartBondingService;->mHandler:Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;
    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$300(Lcom/samsung/android/smartbonding/SmartBondingService;)Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;

    move-result-object v0

    const/4 v3, 0x4

    invoke-virtual {v0, v3}, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v8

    .line 372
    .local v8, "siopMessage":Landroid/os/Message;
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    # getter for: Lcom/samsung/android/smartbonding/SmartBondingService;->mWifiConnected:Z
    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$400(Lcom/samsung/android/smartbonding/SmartBondingService;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 373
    const/4 v0, 0x4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v8, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 377
    :goto_1
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    # getter for: Lcom/samsung/android/smartbonding/SmartBondingService;->mHandler:Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;
    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$300(Lcom/samsung/android/smartbonding/SmartBondingService;)Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;

    move-result-object v0

    invoke-virtual {v0, v8}, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->sendMessage(Landroid/os/Message;)Z

    .line 379
    .end local v8    # "siopMessage":Landroid/os/Message;
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    # invokes: Lcom/samsung/android/smartbonding/SmartBondingService;->getSBUsageEnabled()Z
    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$500(Lcom/samsung/android/smartbonding/SmartBondingService;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    # getter for: Lcom/samsung/android/smartbonding/SmartBondingService;->mDisableBySIOP:Z
    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$600(Lcom/samsung/android/smartbonding/SmartBondingService;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 380
    # getter for: Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z
    invoke-static {}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$000()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "mDisableBySIOP is true so that convert sb state as idle"

    # invokes: Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V
    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$100(Ljava/lang/String;)V

    .line 381
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    # getter for: Lcom/samsung/android/smartbonding/SmartBondingService;->mSBUsageStatus:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$700(Lcom/samsung/android/smartbonding/SmartBondingService;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 383
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    # getter for: Lcom/samsung/android/smartbonding/SmartBondingService;->mHandler:Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;
    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$300(Lcom/samsung/android/smartbonding/SmartBondingService;)Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;

    move-result-object v0

    const/4 v3, 0x6

    invoke-virtual {v0, v3}, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v6

    .line 384
    .local v6, "message":Landroid/os/Message;
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    # getter for: Lcom/samsung/android/smartbonding/SmartBondingService;->mHandler:Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;
    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$300(Lcom/samsung/android/smartbonding/SmartBondingService;)Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->sendMessage(Landroid/os/Message;)Z

    .line 385
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    iget-object v3, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    # invokes: Lcom/samsung/android/smartbonding/SmartBondingService;->checkSBEnableCondition()Z
    invoke-static {v3}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$800(Lcom/samsung/android/smartbonding/SmartBondingService;)Z

    move-result v3

    # invokes: Lcom/samsung/android/smartbonding/SmartBondingService;->setSBInterfacesEnabled(Z)I
    invoke-static {v0, v3}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$900(Lcom/samsung/android/smartbonding/SmartBondingService;Z)I

    .line 386
    monitor-exit v2

    goto :goto_0

    .end local v6    # "message":Landroid/os/Message;
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 375
    .restart local v8    # "siopMessage":Landroid/os/Message;
    :cond_4
    const/4 v0, 0x5

    :try_start_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v8, Landroid/os/Message;->obj:Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 390
    .end local v8    # "siopMessage":Landroid/os/Message;
    :pswitch_2
    iget-object v2, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    monitor-enter v2

    .line 391
    :try_start_2
    # getter for: Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z
    invoke-static {}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$000()Z

    move-result v0

    if-eqz v0, :cond_5

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "EVENT_BOOT_COMPLETED called / enable : "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    invoke-virtual {v3}, Lcom/samsung/android/smartbonding/SmartBondingService;->getSBEnabled()Z

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    # invokes: Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V
    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$100(Ljava/lang/String;)V

    .line 392
    :cond_5
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    invoke-virtual {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->getSBEnabled()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 393
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    # getter for: Lcom/samsung/android/smartbonding/SmartBondingService;->mSBUsageStatus:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$700(Lcom/samsung/android/smartbonding/SmartBondingService;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 394
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    # getter for: Lcom/samsung/android/smartbonding/SmartBondingService;->mSBUrlStatus:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$1000(Lcom/samsung/android/smartbonding/SmartBondingService;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 395
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    # getter for: Lcom/samsung/android/smartbonding/SmartBondingService;->mSBDataStatistics:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$1100(Lcom/samsung/android/smartbonding/SmartBondingService;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 396
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    # getter for: Lcom/samsung/android/smartbonding/SmartBondingService;->mHandler:Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;
    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$300(Lcom/samsung/android/smartbonding/SmartBondingService;)Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;

    move-result-object v0

    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v6

    .line 397
    .restart local v6    # "message":Landroid/os/Message;
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    # getter for: Lcom/samsung/android/smartbonding/SmartBondingService;->mHandler:Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;
    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$300(Lcom/samsung/android/smartbonding/SmartBondingService;)Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->sendMessage(Landroid/os/Message;)Z

    .line 399
    .end local v6    # "message":Landroid/os/Message;
    :cond_6
    monitor-exit v2

    goto/16 :goto_0

    :catchall_1
    move-exception v0

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    .line 403
    :pswitch_3
    iget-object v13, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    monitor-enter v13

    .line 404
    :try_start_3
    # getter for: Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z
    invoke-static {}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$000()Z

    move-result v0

    if-eqz v0, :cond_7

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "EVENT_ENABLE_SB called / enable : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    invoke-virtual {v2}, Lcom/samsung/android/smartbonding/SmartBondingService;->getSBEnabled()Z

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    # invokes: Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V
    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$100(Ljava/lang/String;)V

    .line 405
    :cond_7
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    # getter for: Lcom/samsung/android/smartbonding/SmartBondingService;->mSBUsageStatus:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$700(Lcom/samsung/android/smartbonding/SmartBondingService;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 406
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    # getter for: Lcom/samsung/android/smartbonding/SmartBondingService;->mSBUrlStatus:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$1000(Lcom/samsung/android/smartbonding/SmartBondingService;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 407
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    # getter for: Lcom/samsung/android/smartbonding/SmartBondingService;->mSBDataStatistics:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$1100(Lcom/samsung/android/smartbonding/SmartBondingService;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 410
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    invoke-virtual {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->getSBEnabled()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 411
    const/4 v1, 0x1

    .line 417
    .local v1, "enabled":I
    :goto_2
    const-string v0, "persist.sys.sb.setting.enabled"

    iget-object v2, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    invoke-virtual {v2}, Lcom/samsung/android/smartbonding/SmartBondingService;->getSBEnabled()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 420
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    invoke-virtual {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->getSBEnabled()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    # getter for: Lcom/samsung/android/smartbonding/SmartBondingService;->mWifiManager:Landroid/net/wifi/WifiManager;
    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$1200(Lcom/samsung/android/smartbonding/SmartBondingService;)Landroid/net/wifi/WifiManager;

    move-result-object v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    # getter for: Lcom/samsung/android/smartbonding/SmartBondingService;->mWifiManager:Landroid/net/wifi/WifiManager;
    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$1200(Lcom/samsung/android/smartbonding/SmartBondingService;)Landroid/net/wifi/WifiManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    # getter for: Lcom/samsung/android/smartbonding/SmartBondingService;->mIsNoneSettingMode:Z
    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$1300(Lcom/samsung/android/smartbonding/SmartBondingService;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 426
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    # getter for: Lcom/samsung/android/smartbonding/SmartBondingService;->mWifiManager:Landroid/net/wifi/WifiManager;
    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$1200(Lcom/samsung/android/smartbonding/SmartBondingService;)Landroid/net/wifi/WifiManager;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    .line 429
    :cond_8
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    invoke-virtual {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->getSBEnabled()Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    # invokes: Lcom/samsung/android/smartbonding/SmartBondingService;->getMobileDataEnabled()Z
    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$1400(Lcom/samsung/android/smartbonding/SmartBondingService;)Z

    move-result v0

    if-nez v0, :cond_9

    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    # getter for: Lcom/samsung/android/smartbonding/SmartBondingService;->mIsNoneSettingMode:Z
    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$1300(Lcom/samsung/android/smartbonding/SmartBondingService;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 435
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    const/4 v2, 0x1

    # invokes: Lcom/samsung/android/smartbonding/SmartBondingService;->setMobileDataEnabled(Z)V
    invoke-static {v0, v2}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$1500(Lcom/samsung/android/smartbonding/SmartBondingService;Z)V

    .line 438
    :cond_9
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    invoke-virtual {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->getSBEnabled()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 439
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    const/4 v2, 0x0

    # setter for: Lcom/samsung/android/smartbonding/SmartBondingService;->mIsNoneSettingMode:Z
    invoke-static {v0, v2}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$1302(Lcom/samsung/android/smartbonding/SmartBondingService;Z)Z

    .line 447
    :cond_a
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    # getter for: Lcom/samsung/android/smartbonding/SmartBondingService;->mHandler:Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;
    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$300(Lcom/samsung/android/smartbonding/SmartBondingService;)Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;

    move-result-object v0

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v6

    .line 448
    .restart local v6    # "message":Landroid/os/Message;
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    # getter for: Lcom/samsung/android/smartbonding/SmartBondingService;->mHandler:Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;
    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$300(Lcom/samsung/android/smartbonding/SmartBondingService;)Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->sendMessage(Landroid/os/Message;)Z

    .line 449
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x0

    # invokes: Lcom/samsung/android/smartbonding/SmartBondingService;->sendSBVzwStateChangedIntent(IJJ)V
    invoke-static/range {v0 .. v5}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$1600(Lcom/samsung/android/smartbonding/SmartBondingService;IJJ)V

    .line 450
    monitor-exit v13

    goto/16 :goto_0

    .end local v1    # "enabled":I
    .end local v6    # "message":Landroid/os/Message;
    :catchall_2
    move-exception v0

    monitor-exit v13
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    throw v0

    .line 413
    :cond_b
    const/4 v1, 0x0

    .restart local v1    # "enabled":I
    goto/16 :goto_2

    .line 454
    .end local v1    # "enabled":I
    :pswitch_4
    # getter for: Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z
    invoke-static {}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$000()Z

    move-result v0

    if-eqz v0, :cond_c

    const-string v0, "EVENT_MOBILE_CONNECTION_TURN_OFF_WITH_DELAYED"

    # invokes: Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V
    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$100(Ljava/lang/String;)V

    .line 455
    :cond_c
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    # invokes: Lcom/samsung/android/smartbonding/SmartBondingService;->turnOffMobileConnection()Z
    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$1700(Lcom/samsung/android/smartbonding/SmartBondingService;)Z

    .line 456
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    # getter for: Lcom/samsung/android/smartbonding/SmartBondingService;->mHandler:Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;
    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$300(Lcom/samsung/android/smartbonding/SmartBondingService;)Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v6

    .line 457
    .restart local v6    # "message":Landroid/os/Message;
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    # getter for: Lcom/samsung/android/smartbonding/SmartBondingService;->mHandler:Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;
    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$300(Lcom/samsung/android/smartbonding/SmartBondingService;)Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 461
    .end local v6    # "message":Landroid/os/Message;
    :pswitch_5
    iget-object v2, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    monitor-enter v2

    .line 462
    :try_start_4
    # getter for: Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z
    invoke-static {}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$000()Z

    move-result v0

    if-eqz v0, :cond_d

    const-string v0, "EVENT_MOBILE_CONNECTION_RENEW called"

    # invokes: Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V
    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$100(Ljava/lang/String;)V

    .line 463
    :cond_d
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    # invokes: Lcom/samsung/android/smartbonding/SmartBondingService;->checkRemovedProcess()Z
    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$1800(Lcom/samsung/android/smartbonding/SmartBondingService;)Z

    move-result v0

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    # invokes: Lcom/samsung/android/smartbonding/SmartBondingService;->getSBUsageEnabled()Z
    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$500(Lcom/samsung/android/smartbonding/SmartBondingService;)Z

    move-result v0

    if-nez v0, :cond_f

    .line 464
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    iget-object v3, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    # invokes: Lcom/samsung/android/smartbonding/SmartBondingService;->checkSBEnableCondition()Z
    invoke-static {v3}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$800(Lcom/samsung/android/smartbonding/SmartBondingService;)Z

    move-result v3

    # invokes: Lcom/samsung/android/smartbonding/SmartBondingService;->setSBInterfacesEnabled(Z)I
    invoke-static {v0, v3}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$900(Lcom/samsung/android/smartbonding/SmartBondingService;Z)I

    .line 470
    :cond_e
    :goto_3
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    # getter for: Lcom/samsung/android/smartbonding/SmartBondingService;->mHandler:Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;
    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$300(Lcom/samsung/android/smartbonding/SmartBondingService;)Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;

    move-result-object v0

    const/4 v3, 0x6

    invoke-virtual {v0, v3}, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v6

    .line 471
    .restart local v6    # "message":Landroid/os/Message;
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    # getter for: Lcom/samsung/android/smartbonding/SmartBondingService;->mHandler:Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;
    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$300(Lcom/samsung/android/smartbonding/SmartBondingService;)Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->sendMessage(Landroid/os/Message;)Z

    .line 472
    monitor-exit v2

    goto/16 :goto_0

    .end local v6    # "message":Landroid/os/Message;
    :catchall_3
    move-exception v0

    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    throw v0

    .line 466
    :cond_f
    :try_start_5
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    invoke-virtual {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->getSBInterfacesEnabled()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 467
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    # invokes: Lcom/samsung/android/smartbonding/SmartBondingService;->turnOnMobileConnection()Z
    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$1900(Lcom/samsung/android/smartbonding/SmartBondingService;)Z
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    goto :goto_3

    .line 476
    :pswitch_6
    iget-object v2, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    monitor-enter v2

    .line 477
    :try_start_6
    # getter for: Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z
    invoke-static {}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$000()Z

    move-result v0

    if-eqz v0, :cond_10

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "EVENT_UPDATE_SB_STATE called / mDisableBySIOP "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    # getter for: Lcom/samsung/android/smartbonding/SmartBondingService;->mDisableBySIOP:Z
    invoke-static {v3}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$600(Lcom/samsung/android/smartbonding/SmartBondingService;)Z

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    # invokes: Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V
    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$100(Ljava/lang/String;)V

    .line 478
    :cond_10
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    # invokes: Lcom/samsung/android/smartbonding/SmartBondingService;->checkSBWorkingCondition()Z
    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$2000(Lcom/samsung/android/smartbonding/SmartBondingService;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 479
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    # getter for: Lcom/samsung/android/smartbonding/SmartBondingService;->mSBState:I
    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$2100(Lcom/samsung/android/smartbonding/SmartBondingService;)I

    move-result v0

    if-eq v0, v13, :cond_12

    .line 480
    # getter for: Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z
    invoke-static {}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$000()Z

    move-result v0

    if-eqz v0, :cond_11

    const-string v0, "sb state is on working"

    # invokes: Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V
    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$100(Ljava/lang/String;)V

    .line 481
    :cond_11
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    const/4 v3, 0x3

    # setter for: Lcom/samsung/android/smartbonding/SmartBondingService;->mSBState:I
    invoke-static {v0, v3}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$2102(Lcom/samsung/android/smartbonding/SmartBondingService;I)I

    .line 482
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    # invokes: Lcom/samsung/android/smartbonding/SmartBondingService;->sendSBStartIntent()V
    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$2200(Lcom/samsung/android/smartbonding/SmartBondingService;)V

    .line 509
    :cond_12
    :goto_4
    monitor-exit v2

    goto/16 :goto_0

    :catchall_4
    move-exception v0

    monitor-exit v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_4

    throw v0

    .line 484
    :cond_13
    :try_start_7
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    # invokes: Lcom/samsung/android/smartbonding/SmartBondingService;->checkSBEnableCondition()Z
    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$800(Lcom/samsung/android/smartbonding/SmartBondingService;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 485
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    # getter for: Lcom/samsung/android/smartbonding/SmartBondingService;->mSBState:I
    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$2100(Lcom/samsung/android/smartbonding/SmartBondingService;)I

    move-result v0

    if-eq v0, v4, :cond_12

    .line 486
    # getter for: Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z
    invoke-static {}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$000()Z

    move-result v0

    if-eqz v0, :cond_14

    const-string v0, "sb state is on enabled"

    # invokes: Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V
    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$100(Ljava/lang/String;)V

    .line 493
    :cond_14
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    # invokes: Lcom/samsung/android/smartbonding/SmartBondingService;->sendSBStopIntent()V
    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$2300(Lcom/samsung/android/smartbonding/SmartBondingService;)V

    .line 494
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    const/4 v3, 0x2

    # setter for: Lcom/samsung/android/smartbonding/SmartBondingService;->mSBState:I
    invoke-static {v0, v3}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$2102(Lcom/samsung/android/smartbonding/SmartBondingService;I)I

    goto :goto_4

    .line 496
    :cond_15
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    invoke-virtual {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->getSBEnabled()Z

    move-result v0

    if-eqz v0, :cond_17

    .line 497
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    # getter for: Lcom/samsung/android/smartbonding/SmartBondingService;->mSBState:I
    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$2100(Lcom/samsung/android/smartbonding/SmartBondingService;)I

    move-result v0

    if-eq v0, v5, :cond_12

    .line 498
    # getter for: Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z
    invoke-static {}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$000()Z

    move-result v0

    if-eqz v0, :cond_16

    const-string v0, "sb state is on idle"

    # invokes: Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V
    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$100(Ljava/lang/String;)V

    .line 499
    :cond_16
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    # invokes: Lcom/samsung/android/smartbonding/SmartBondingService;->sendSBStopIntent()V
    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$2300(Lcom/samsung/android/smartbonding/SmartBondingService;)V

    .line 500
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    const/4 v3, 0x1

    # setter for: Lcom/samsung/android/smartbonding/SmartBondingService;->mSBState:I
    invoke-static {v0, v3}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$2102(Lcom/samsung/android/smartbonding/SmartBondingService;I)I

    goto :goto_4

    .line 503
    :cond_17
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    # getter for: Lcom/samsung/android/smartbonding/SmartBondingService;->mSBState:I
    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$2100(Lcom/samsung/android/smartbonding/SmartBondingService;)I

    move-result v0

    if-eqz v0, :cond_12

    .line 504
    # getter for: Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z
    invoke-static {}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$000()Z

    move-result v0

    if-eqz v0, :cond_18

    const-string v0, "sb state is on none"

    # invokes: Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V
    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$100(Ljava/lang/String;)V

    .line 505
    :cond_18
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    # invokes: Lcom/samsung/android/smartbonding/SmartBondingService;->sendSBStopIntent()V
    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$2300(Lcom/samsung/android/smartbonding/SmartBondingService;)V

    .line 506
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    const/4 v3, 0x0

    # setter for: Lcom/samsung/android/smartbonding/SmartBondingService;->mSBState:I
    invoke-static {v0, v3}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$2102(Lcom/samsung/android/smartbonding/SmartBondingService;I)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_4

    goto :goto_4

    .line 513
    :pswitch_7
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 514
    .local v7, "msgid":I
    const/4 v9, 0x1

    .line 515
    .local v9, "type":I
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    iget-object v2, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    # getter for: Lcom/samsung/android/smartbonding/SmartBondingService;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$2400(Lcom/samsung/android/smartbonding/SmartBondingService;)Landroid/content/Context;

    move-result-object v2

    # invokes: Lcom/samsung/android/smartbonding/SmartBondingService;->showMessage(Landroid/content/Context;II)V
    invoke-static {v0, v2, v7, v9}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$2500(Lcom/samsung/android/smartbonding/SmartBondingService;Landroid/content/Context;II)V

    goto/16 :goto_0

    .line 519
    .end local v7    # "msgid":I
    .end local v9    # "type":I
    :pswitch_8
    iget-object v2, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    monitor-enter v2

    .line 520
    :try_start_8
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    iget-object v3, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    # invokes: Lcom/samsung/android/smartbonding/SmartBondingService;->getNetworkEnabled()Z
    invoke-static {v3}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$2700(Lcom/samsung/android/smartbonding/SmartBondingService;)Z

    move-result v3

    # setter for: Lcom/samsung/android/smartbonding/SmartBondingService;->mNetworkEnabled:Z
    invoke-static {v0, v3}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$2602(Lcom/samsung/android/smartbonding/SmartBondingService;Z)Z

    .line 521
    monitor-exit v2

    goto/16 :goto_0

    :catchall_5
    move-exception v0

    monitor-exit v2
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_5

    throw v0

    .line 525
    :pswitch_9
    iget-object v2, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    monitor-enter v2

    .line 526
    :try_start_9
    iget-object v12, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v12, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;

    .line 527
    .local v12, "usageStatus":Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;
    if-eqz v12, :cond_19

    .line 528
    iget-wide v10, v12, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;->mThreadID:J

    .line 529
    .local v10, "threadId":J
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    iget-object v3, v12, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;->mHost:Ljava/lang/String;

    # invokes: Lcom/samsung/android/smartbonding/SmartBondingService;->getAllByName(Ljava/lang/String;J)[Ljava/net/InetAddress;
    invoke-static {v0, v3, v10, v11}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$2800(Lcom/samsung/android/smartbonding/SmartBondingService;Ljava/lang/String;J)[Ljava/net/InetAddress;

    move-result-object v0

    iput-object v0, v12, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;->mInetAddresses:[Ljava/net/InetAddress;

    .line 531
    .end local v10    # "threadId":J
    :cond_19
    monitor-exit v2

    goto/16 :goto_0

    .end local v12    # "usageStatus":Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;
    :catchall_6
    move-exception v0

    monitor-exit v2
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_6

    throw v0

    .line 535
    :pswitch_a
    iget-object v2, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    monitor-enter v2

    .line 536
    :try_start_a
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    # getter for: Lcom/samsung/android/smartbonding/SmartBondingService;->mCurrentActivity:Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$2900(Lcom/samsung/android/smartbonding/SmartBondingService;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_1c

    .line 537
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    # getter for: Lcom/samsung/android/smartbonding/SmartBondingService;->mCurrentActivity:Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$2900(Lcom/samsung/android/smartbonding/SmartBondingService;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "start_poll"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 538
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    iget-object v3, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    # invokes: Lcom/samsung/android/smartbonding/SmartBondingService;->getCurrentActivity()Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$3000(Lcom/samsung/android/smartbonding/SmartBondingService;)Ljava/lang/String;

    move-result-object v3

    # setter for: Lcom/samsung/android/smartbonding/SmartBondingService;->mCurrentActivity:Ljava/lang/String;
    invoke-static {v0, v3}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$2902(Lcom/samsung/android/smartbonding/SmartBondingService;Ljava/lang/String;)Ljava/lang/String;

    .line 540
    :cond_1a
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    # getter for: Lcom/samsung/android/smartbonding/SmartBondingService;->mCurrentActivity:Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$2900(Lcom/samsung/android/smartbonding/SmartBondingService;)Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    # invokes: Lcom/samsung/android/smartbonding/SmartBondingService;->getCurrentActivity()Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$3000(Lcom/samsung/android/smartbonding/SmartBondingService;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 541
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    # getter for: Lcom/samsung/android/smartbonding/SmartBondingService;->mHandler:Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;
    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$300(Lcom/samsung/android/smartbonding/SmartBondingService;)Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;

    move-result-object v0

    const/16 v3, 0xb

    invoke-virtual {v0, v3}, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v6

    .line 542
    .restart local v6    # "message":Landroid/os/Message;
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    # getter for: Lcom/samsung/android/smartbonding/SmartBondingService;->mHandler:Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;
    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$300(Lcom/samsung/android/smartbonding/SmartBondingService;)Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;

    move-result-object v0

    const-wide/16 v4, 0x2710

    invoke-virtual {v0, v6, v4, v5}, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 548
    .end local v6    # "message":Landroid/os/Message;
    :cond_1b
    :goto_5
    monitor-exit v2

    goto/16 :goto_0

    :catchall_7
    move-exception v0

    monitor-exit v2
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_7

    throw v0

    .line 545
    :cond_1c
    :try_start_b
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    const-string v3, ""

    # setter for: Lcom/samsung/android/smartbonding/SmartBondingService;->mCurrentActivity:Ljava/lang/String;
    invoke-static {v0, v3}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$2902(Lcom/samsung/android/smartbonding/SmartBondingService;Ljava/lang/String;)Ljava/lang/String;

    .line 546
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    # getter for: Lcom/samsung/android/smartbonding/SmartBondingService;->mHandler:Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;
    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$300(Lcom/samsung/android/smartbonding/SmartBondingService;)Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;

    move-result-object v0

    iget-object v3, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    # getter for: Lcom/samsung/android/smartbonding/SmartBondingService;->mHandler:Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;
    invoke-static {v3}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$300(Lcom/samsung/android/smartbonding/SmartBondingService;)Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;

    move-result-object v3

    const/4 v4, 0x2

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->sendMessage(Landroid/os/Message;)Z
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_7

    goto :goto_5

    .line 366
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_1
        :pswitch_5
        :pswitch_7
        :pswitch_0
        :pswitch_6
        :pswitch_8
        :pswitch_9
        :pswitch_4
        :pswitch_2
        :pswitch_a
    .end packed-switch
.end method
