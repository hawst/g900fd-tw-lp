.class Lcom/samsung/android/smartbonding/SmartBondingService$1;
.super Landroid/telephony/PhoneStateListener;
.source "SmartBondingService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/smartbonding/SmartBondingService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/smartbonding/SmartBondingService;


# direct methods
.method constructor <init>(Lcom/samsung/android/smartbonding/SmartBondingService;)V
    .locals 0

    .prologue
    .line 2502
    iput-object p1, p0, Lcom/samsung/android/smartbonding/SmartBondingService$1;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceStateChanged(Landroid/telephony/ServiceState;)V
    .locals 5
    .param p1, "state"    # Landroid/telephony/ServiceState;

    .prologue
    const/4 v4, 0x0

    .line 2505
    # getter for: Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z
    invoke-static {}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$000()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "onServiceStateChanged state= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    # invokes: Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V
    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$100(Ljava/lang/String;)V

    .line 2507
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$1;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    # getter for: Lcom/samsung/android/smartbonding/SmartBondingService;->mHandler:Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;
    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$300(Lcom/samsung/android/smartbonding/SmartBondingService;)Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/smartbonding/SmartBondingService$1;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    # getter for: Lcom/samsung/android/smartbonding/SmartBondingService;->mHandler:Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;
    invoke-static {v1}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$300(Lcom/samsung/android/smartbonding/SmartBondingService;)Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;

    move-result-object v1

    const/4 v2, 0x7

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->sendMessage(Landroid/os/Message;)Z

    .line 2509
    invoke-virtual {p1}, Landroid/telephony/ServiceState;->getRoaming()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2510
    # getter for: Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z
    invoke-static {}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$000()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "onServiceStateChanged : roaming is enabled. disable smart bonding"

    # invokes: Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V
    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$100(Ljava/lang/String;)V

    .line 2511
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$1;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    invoke-virtual {v0, v4}, Lcom/samsung/android/smartbonding/SmartBondingService;->setSBEnabled(Z)V

    .line 2513
    :cond_2
    return-void
.end method
