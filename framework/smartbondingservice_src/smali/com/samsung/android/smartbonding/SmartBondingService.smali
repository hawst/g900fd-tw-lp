.class public final Lcom/samsung/android/smartbonding/SmartBondingService;
.super Lcom/samsung/android/smartbonding/ISmartBondingService$Stub;
.source "SmartBondingService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingReceiver;,
        Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingMobileSettingObserver;,
        Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingSettingObserver;,
        Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingAirplaneModeSettingObserver;,
        Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;,
        Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUrlStatus;,
        Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;
    }
.end annotation


# static fields
.field public static final CAUSE_AIRPLANE_MODE:I = 0xe

.field public static final CAUSE_BLOCKED_PROCESS:I = 0x10

.field public static final CAUSE_CONNECTION_FAILURE:I = 0x3

.field public static final CAUSE_DOWNLOAD_CANCEL:I = 0x1

.field public static final CAUSE_LTE_ABNORMAL_PERFORMANCE:I = -0x3

.field public static final CAUSE_MOBILE_OR_WIFI_OFF:I = 0xb

.field public static final CAUSE_NO_ERROR:I = 0x0

.field public static final CAUSE_NO_LTE:I = 0x11

.field public static final CAUSE_NO_PERMISSION:I = 0x12

.field public static final CAUSE_NO_SIM:I = 0xc

.field public static final CAUSE_ROAMING:I = 0xd

.field public static final CAUSE_SERVER_NOT_SUPPORTED:I = 0x2

.field public static final CAUSE_SETTING_OFF:I = 0xa

.field public static final CAUSE_SMALL_CONTENT_SIZE:I = 0xf

.field public static final CAUSE_USE_ONLY_LTE_INTERFACE:I = -0x2

.field public static final CAUSE_USE_ONLY_WIFI_INTERFACE:I = -0x1

.field public static final CAUSE_WIFI_ABNORMAL_PERFORMANCE:I = -0x4

.field private static CscFeatureConfigSmartBonding:Ljava/lang/String; = null

.field private static DBG:Z = false

.field private static final EVENT_BOOT_COMPLETED:I = 0xa

.field private static final EVENT_CREATE_LOG_FOLDER:I = 0x5

.field private static final EVENT_ENABLE_SB:I = 0x1

.field private static final EVENT_ENABLE_SB_INTERFACES:I = 0x2

.field private static final EVENT_GET_HOST_ADDRESS:I = 0x8

.field private static final EVENT_MOBILE_CONNECTION_RENEW:I = 0x3

.field private static final EVENT_MOBILE_CONNECTION_TURN_OFF_WITH_DELAYED:I = 0x9

.field private static final EVENT_POLL_CURRENT_ACTIVITY:I = 0xb

.field private static final EVENT_SHOW_MSG_MESSAGE:I = 0x4

.field private static final EVENT_UPDATE_NETWORK_ENABLED:I = 0x7

.field private static final EVENT_UPDATE_SB_STATE:I = 0x6

.field private static final INT_TOTAL:Ljava/lang/Integer;

.field private static IsDCM:Z = false

.field private static IsKOR:Z = false

.field private static final MAX_SERVICES:I = 0x3e8

.field public static final MSG_ACTIVATE_SB:I = 0x6

.field public static final MSG_DOWNLOAD_MOBILE_ONLY:I = 0xa

.field public static final MSG_DOWNLOAD_WIFI_ONLY:I = 0x9

.field public static final MSG_OVERHEAT_MOBILE_DOWNLOAD:I = 0x5

.field public static final MSG_OVERHEAT_WIFI_DOWNLOAD:I = 0x4

.field public static final MSG_SERVER_ERROR_MOBILE_DOWNLOAD:I = 0x3

.field public static final MSG_SERVER_ERROR_WIFI_DOWNLOAD:I = 0x2

.field public static final MSG_START_SB:I = 0x1

.field public static final MSG_TURN_ON_MOBILE:I = 0x8

.field public static final MSG_TURN_ON_WIFI:I = 0x7

.field public static final MSG_WARNING_LTE:I = 0xb

.field public static final MSG_WARNING_WIFI:I = 0xc

.field public static final NOTIFICATION_THRESHOLD_SPEED:I = 0x2710

.field private static final SB_BOTH:I = 0x2

.field public static final SB_BOTH_CONNECTED:I = 0x3

.field public static final SB_BOTH_DISCONNECTED:I = 0x0

.field private static final SB_EXT_STATE_BLOCKED:I = 0x5

.field private static final SB_EXT_STATE_DISABLED:I = 0x0

.field private static final SB_EXT_STATE_DOWNLOADING:I = 0x4

.field private static final SB_EXT_STATE_DOWNLOAD_FINISHED:I = 0x3

.field private static final SB_EXT_STATE_DOWNLOAD_STARTED:I = 0x2

.field private static final SB_EXT_STATE_ENABLED:I = 0x1

.field public static final SB_INTENT_HIDE_DIALOG:Ljava/lang/String; = "android.intent.action.SB_HIDE_DIALOG"

.field public static final SB_INTENT_SHOW_DIALOG:Ljava/lang/String; = "android.intent.action.SB_SHOW_DIALOG"

.field public static final SB_INTENT_SHOW_MSG:Ljava/lang/String; = "android.intent.action.SB_SHOW_MSG"

.field public static final SB_INTENT_START:Ljava/lang/String; = "android.intent.action.START_NETWORK_BOOSTER"

.field public static final SB_INTENT_STATE_CHANGED:Ljava/lang/String; = "android.intent.action.SB_STATE_CHANGED"

.field public static final SB_INTENT_STOP:Ljava/lang/String; = "android.intent.action.STOP_NETWORK_BOOSTER"

.field private static final SB_INTENT_VZW_STATE_CHANGED:Ljava/lang/String; = "android.intent.action.RI_Aggregation_state_changed"

.field public static final SB_INVALID:I = -0x1

.field private static final SB_IPV4_TYPE:I = 0x0

.field private static final SB_IPV6_TYPE:I = 0x1

.field private static final SB_MOBILE:I = 0x1

.field public static final SB_RADIO_CONNECTED:I = 0x2

.field private static final SB_STATUS_ENABLE:I = 0x2

.field private static final SB_STATUS_IDLE:I = 0x1

.field private static final SB_STATUS_NONE:I = 0x0

.field private static final SB_STATUS_WORKING:I = 0x3

.field public static final SB_USAGE_CANCEL:I = 0x3

.field public static final SB_USAGE_NO:I = 0x2

.field public static final SB_USAGE_NONE:I = 0x0

.field public static final SB_USAGE_NOT_SUPPORTED:I = 0x4

.field public static final SB_USAGE_YES:I = 0x1

.field private static final SB_WIFI:I = 0x0

.field public static final SB_WIFI_CONNECTED:I = 0x1

.field static final SHIP_BUILD:Z

.field public static final SMARTBONDING_SERVICE:Ljava/lang/String; = "sb_service"

.field private static final TAG:Ljava/lang/String; = "SmartBondingService"

.field private static TAG_CSCFEATURE_CONFIG_SMARTBONDING:Ljava/lang/String; = null

.field private static final TIME_MOBILE_CONNECTION_RENEW:I = 0x9c40

.field private static final TIME_MOBILE_CONNECTION_TURN_OFF_DELAY:I = 0xbb8

.field private static final TIME_POLL_CURRENT_ACTIVITY:I = 0x2710

.field private static final UID_MEDIA:I = 0x3f5

.field private static VDBG:Z

.field private static mTrafficMonitorController:Lcom/samsung/android/smartbonding/TrafficMonitorController;


# instance fields
.field private final DEBUG_LEVEL_FILE:Ljava/lang/String;

.field private final DEBUG_LEVEL_FILE2:Ljava/lang/String;

.field private isKioskContainer:Z

.field private mBlockedPackages:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mCm:Landroid/net/ConnectivityManager;

.field private mContext:Landroid/content/Context;

.field private mCurrentActivity:Ljava/lang/String;

.field private mCurrentUserId:I

.field private mCurrentWifiBSSID:Ljava/lang/String;

.field private mDisableBySIOP:Z

.field private mFasterInterface:I

.field private mHandler:Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;

.field private mHttpLogEnabled:Z

.field private mIsConnectMobileCalled:Z

.field private mIsNoneSettingMode:Z

.field private mIsStoppedByGooglePlay:Z

.field private mLastErrorCause:I

.field private mMobileConnected:Z

.field private mMobileLp:Landroid/net/LinkProperties;

.field private mNeedShowTrafficToast:Z

.field private mNetworkEnabled:Z

.field private mPhoneStateListener:Landroid/telephony/PhoneStateListener;

.field private mPreSBSetting:Z

.field private mSBDataStatistics:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "[J>;"
        }
    .end annotation
.end field

.field private mSBInterfacesEnabled:Z

.field private mSBState:I

.field private mSBUrlStatus:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUrlStatus;",
            ">;"
        }
    .end annotation
.end field

.field private mSBUsageStatus:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;",
            ">;"
        }
    .end annotation
.end field

.field private mSmartBondingAirplaneModeSettingObserver:Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingAirplaneModeSettingObserver;

.field private mSmartBondingMobileSettingObserver:Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingMobileSettingObserver;

.field private mSmartBondingSettingObserver:Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingSettingObserver;

.field private mSpeedRatio:D

.field private mTm:Landroid/telephony/TelephonyManager;

.field private mWhiteListPackages:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mWifiConnected:Z

.field private mWifiLp:Landroid/net/LinkProperties;

.field private mWifiManager:Landroid/net/wifi/WifiManager;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 113
    sput-boolean v0, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    .line 114
    sput-boolean v0, Lcom/samsung/android/smartbonding/SmartBondingService;->VDBG:Z

    .line 201
    const/16 v1, -0x64

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sput-object v1, Lcom/samsung/android/smartbonding/SmartBondingService;->INT_TOTAL:Ljava/lang/Integer;

    .line 267
    const-string v1, "CscFeature_RIL_ConfigSmartBonding"

    sput-object v1, Lcom/samsung/android/smartbonding/SmartBondingService;->TAG_CSCFEATURE_CONFIG_SMARTBONDING:Ljava/lang/String;

    .line 268
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/smartbonding/SmartBondingService;->TAG_CSCFEATURE_CONFIG_SMARTBONDING:Ljava/lang/String;

    const-string v3, "NA"

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/samsung/android/smartbonding/SmartBondingService;->CscFeatureConfigSmartBonding:Ljava/lang/String;

    .line 269
    const-string v1, "DCM"

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    sput-boolean v1, Lcom/samsung/android/smartbonding/SmartBondingService;->IsDCM:Z

    .line 270
    const-string v1, "SKT"

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "KTT"

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "LGT"

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    sput-boolean v0, Lcom/samsung/android/smartbonding/SmartBondingService;->IsKOR:Z

    .line 274
    const-string v0, "true"

    const-string v1, "ro.product_ship"

    const-string v2, "false"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/android/smartbonding/SmartBondingService;->SHIP_BUILD:Z

    .line 283
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/smartbonding/SmartBondingService;->mTrafficMonitorController:Lcom/samsung/android/smartbonding/TrafficMonitorController;

    return-void

    .line 270
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 285
    invoke-direct {p0}, Lcom/samsung/android/smartbonding/ISmartBondingService$Stub;-><init>()V

    .line 202
    iput v2, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mFasterInterface:I

    .line 203
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    iput-wide v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSpeedRatio:D

    .line 207
    iput-boolean v2, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mDisableBySIOP:Z

    .line 208
    iput-boolean v2, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSBInterfacesEnabled:Z

    .line 209
    iput-object v3, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSmartBondingSettingObserver:Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingSettingObserver;

    .line 210
    iput-object v3, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSmartBondingMobileSettingObserver:Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingMobileSettingObserver;

    .line 212
    iput-object v3, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSmartBondingAirplaneModeSettingObserver:Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingAirplaneModeSettingObserver;

    .line 213
    iput-boolean v2, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mWifiConnected:Z

    .line 214
    iput-boolean v2, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mMobileConnected:Z

    .line 215
    iput-boolean v2, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mHttpLogEnabled:Z

    .line 216
    iput-boolean v2, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mNetworkEnabled:Z

    .line 217
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mCurrentWifiBSSID:Ljava/lang/String;

    .line 218
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mCurrentActivity:Ljava/lang/String;

    .line 219
    iput v2, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSBState:I

    .line 220
    iput-boolean v2, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mPreSBSetting:Z

    .line 221
    iput-boolean v2, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mIsNoneSettingMode:Z

    .line 222
    iput-boolean v2, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mIsStoppedByGooglePlay:Z

    .line 224
    iput-object v3, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mMobileLp:Landroid/net/LinkProperties;

    .line 225
    iput-object v3, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mWifiLp:Landroid/net/LinkProperties;

    .line 226
    iput-boolean v2, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mIsConnectMobileCalled:Z

    .line 230
    iput v2, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mLastErrorCause:I

    .line 232
    iput-boolean v2, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mNeedShowTrafficToast:Z

    .line 234
    iput v2, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mCurrentUserId:I

    .line 275
    const-string v0, "/sys/devices/virtual/misc/level/control"

    iput-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->DEBUG_LEVEL_FILE:Ljava/lang/String;

    .line 276
    const-string v0, "/mnt/.lfs/debug_level.inf"

    iput-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->DEBUG_LEVEL_FILE2:Ljava/lang/String;

    .line 278
    iput-boolean v2, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->isKioskContainer:Z

    .line 2502
    new-instance v0, Lcom/samsung/android/smartbonding/SmartBondingService$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/smartbonding/SmartBondingService$1;-><init>(Lcom/samsung/android/smartbonding/SmartBondingService;)V

    iput-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    .line 285
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 287
    invoke-direct {p0}, Lcom/samsung/android/smartbonding/ISmartBondingService$Stub;-><init>()V

    .line 202
    iput v6, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mFasterInterface:I

    .line 203
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    iput-wide v4, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSpeedRatio:D

    .line 207
    iput-boolean v6, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mDisableBySIOP:Z

    .line 208
    iput-boolean v6, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSBInterfacesEnabled:Z

    .line 209
    iput-object v7, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSmartBondingSettingObserver:Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingSettingObserver;

    .line 210
    iput-object v7, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSmartBondingMobileSettingObserver:Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingMobileSettingObserver;

    .line 212
    iput-object v7, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSmartBondingAirplaneModeSettingObserver:Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingAirplaneModeSettingObserver;

    .line 213
    iput-boolean v6, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mWifiConnected:Z

    .line 214
    iput-boolean v6, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mMobileConnected:Z

    .line 215
    iput-boolean v6, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mHttpLogEnabled:Z

    .line 216
    iput-boolean v6, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mNetworkEnabled:Z

    .line 217
    const-string v3, ""

    iput-object v3, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mCurrentWifiBSSID:Ljava/lang/String;

    .line 218
    const-string v3, ""

    iput-object v3, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mCurrentActivity:Ljava/lang/String;

    .line 219
    iput v6, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSBState:I

    .line 220
    iput-boolean v6, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mPreSBSetting:Z

    .line 221
    iput-boolean v6, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mIsNoneSettingMode:Z

    .line 222
    iput-boolean v6, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mIsStoppedByGooglePlay:Z

    .line 224
    iput-object v7, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mMobileLp:Landroid/net/LinkProperties;

    .line 225
    iput-object v7, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mWifiLp:Landroid/net/LinkProperties;

    .line 226
    iput-boolean v6, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mIsConnectMobileCalled:Z

    .line 230
    iput v6, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mLastErrorCause:I

    .line 232
    iput-boolean v6, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mNeedShowTrafficToast:Z

    .line 234
    iput v6, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mCurrentUserId:I

    .line 275
    const-string v3, "/sys/devices/virtual/misc/level/control"

    iput-object v3, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->DEBUG_LEVEL_FILE:Ljava/lang/String;

    .line 276
    const-string v3, "/mnt/.lfs/debug_level.inf"

    iput-object v3, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->DEBUG_LEVEL_FILE2:Ljava/lang/String;

    .line 278
    iput-boolean v6, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->isKioskContainer:Z

    .line 2502
    new-instance v3, Lcom/samsung/android/smartbonding/SmartBondingService$1;

    invoke-direct {v3, p0}, Lcom/samsung/android/smartbonding/SmartBondingService$1;-><init>(Lcom/samsung/android/smartbonding/SmartBondingService;)V

    iput-object v3, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    .line 288
    sget-boolean v3, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v3, :cond_0

    const-string v3, "SmartBondingService starting up"

    invoke-static {v3}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 290
    :cond_0
    new-instance v0, Landroid/os/HandlerThread;

    const-string v3, "SmartBondingService"

    invoke-direct {v0, v3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 291
    .local v0, "handlerThread":Landroid/os/HandlerThread;
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 292
    new-instance v3, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v4

    invoke-direct {v3, p0, v4}, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;-><init>(Lcom/samsung/android/smartbonding/SmartBondingService;Landroid/os/Looper;)V

    iput-object v3, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mHandler:Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;

    .line 293
    new-instance v4, Landroid/view/ContextThemeWrapper;

    invoke-direct {p0}, Lcom/samsung/android/smartbonding/SmartBondingService;->isLightTheme()Z

    move-result v3

    if-eqz v3, :cond_4

    const v3, 0x103012b

    :goto_0
    invoke-direct {v4, p1, v3}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v4, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mContext:Landroid/content/Context;

    .line 294
    iget-object v3, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mContext:Landroid/content/Context;

    const-string v4, "connectivity"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/ConnectivityManager;

    iput-object v3, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mCm:Landroid/net/ConnectivityManager;

    .line 295
    iget-object v3, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mContext:Landroid/content/Context;

    const-string v4, "wifi"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/wifi/WifiManager;

    iput-object v3, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mWifiManager:Landroid/net/wifi/WifiManager;

    .line 296
    iget-object v3, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mContext:Landroid/content/Context;

    const-string v4, "phone"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/telephony/TelephonyManager;

    iput-object v3, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mTm:Landroid/telephony/TelephonyManager;

    .line 297
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    iput-object v3, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSBDataStatistics:Ljava/util/HashMap;

    .line 298
    iget-object v3, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSBDataStatistics:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->clear()V

    .line 299
    iget-object v3, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSBDataStatistics:Ljava/util/HashMap;

    sget-object v4, Lcom/samsung/android/smartbonding/SmartBondingService;->INT_TOTAL:Ljava/lang/Integer;

    const/4 v5, 0x4

    new-array v5, v5, [J

    fill-array-data v5, :array_0

    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 300
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSBUsageStatus:Ljava/util/ArrayList;

    .line 301
    iget-object v3, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSBUsageStatus:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 302
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSBUrlStatus:Ljava/util/ArrayList;

    .line 303
    iget-object v3, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSBUrlStatus:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 304
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mBlockedPackages:Ljava/util/ArrayList;

    .line 305
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mWhiteListPackages:Ljava/util/ArrayList;

    .line 306
    invoke-direct {p0}, Lcom/samsung/android/smartbonding/SmartBondingService;->initBlockedPackages()V

    .line 308
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 309
    .local v1, "sbFilter":Landroid/content/IntentFilter;
    const-string v3, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 310
    const-string v3, "android.net.conn.CONNECTIVITY_CHANGE_IMMEDIATE"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 311
    const-string v3, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 312
    const-string v3, "android.intent.action.SIOP_LEVEL_CHANGED"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 313
    const-string v3, "android.intent.action.USER_SWITCHED"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 314
    const-string v3, "android.net.wifi.LINK_CONFIGURATION_CHANGED"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 315
    const-string v3, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 316
    const-string v3, "com.android.systemui.statusbar.EXPANDED"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 317
    iget-object v3, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mContext:Landroid/content/Context;

    new-instance v4, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingReceiver;

    invoke-direct {v4, p0}, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingReceiver;-><init>(Lcom/samsung/android/smartbonding/SmartBondingService;)V

    invoke-virtual {v3, v4, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 319
    new-instance v3, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingSettingObserver;

    iget-object v4, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mHandler:Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;

    invoke-direct {v3, p0, v4}, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingSettingObserver;-><init>(Lcom/samsung/android/smartbonding/SmartBondingService;Landroid/os/Handler;)V

    iput-object v3, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSmartBondingSettingObserver:Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingSettingObserver;

    .line 320
    iget-object v3, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSmartBondingSettingObserver:Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingSettingObserver;

    invoke-virtual {v3, p1}, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingSettingObserver;->register(Landroid/content/Context;)V

    .line 322
    new-instance v3, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingMobileSettingObserver;

    iget-object v4, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mHandler:Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;

    invoke-direct {v3, p0, v4}, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingMobileSettingObserver;-><init>(Lcom/samsung/android/smartbonding/SmartBondingService;Landroid/os/Handler;)V

    iput-object v3, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSmartBondingMobileSettingObserver:Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingMobileSettingObserver;

    .line 323
    iget-object v3, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSmartBondingMobileSettingObserver:Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingMobileSettingObserver;

    invoke-virtual {v3, p1}, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingMobileSettingObserver;->register(Landroid/content/Context;)V

    .line 330
    new-instance v3, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingAirplaneModeSettingObserver;

    iget-object v4, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mHandler:Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;

    invoke-direct {v3, p0, v4}, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingAirplaneModeSettingObserver;-><init>(Lcom/samsung/android/smartbonding/SmartBondingService;Landroid/os/Handler;)V

    iput-object v3, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSmartBondingAirplaneModeSettingObserver:Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingAirplaneModeSettingObserver;

    .line 331
    iget-object v3, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSmartBondingAirplaneModeSettingObserver:Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingAirplaneModeSettingObserver;

    invoke-virtual {v3, p1}, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingAirplaneModeSettingObserver;->register(Landroid/content/Context;)V

    .line 333
    iget-object v3, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mContext:Landroid/content/Context;

    invoke-direct {p0, v3}, Lcom/samsung/android/smartbonding/SmartBondingService;->registerPhoneStateListener(Landroid/content/Context;)V

    .line 335
    iget-object v3, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mWifiManager:Landroid/net/wifi/WifiManager;

    if-eqz v3, :cond_1

    .line 336
    iget-object v3, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v3}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v2

    .line 337
    .local v2, "wifiInfo":Landroid/net/wifi/WifiInfo;
    if-eqz v2, :cond_1

    .line 338
    invoke-virtual {v2}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mCurrentWifiBSSID:Ljava/lang/String;

    .line 342
    .end local v2    # "wifiInfo":Landroid/net/wifi/WifiInfo;
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/smartbonding/SmartBondingService;->getNetworkEnabled()Z

    move-result v3

    iput-boolean v3, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mNetworkEnabled:Z

    .line 343
    const-string v3, "persist.sys.sb.setting.enabled"

    invoke-virtual {p0}, Lcom/samsung/android/smartbonding/SmartBondingService;->getSBEnabled()Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 344
    invoke-direct {p0}, Lcom/samsung/android/smartbonding/SmartBondingService;->initSpeedRatio()V

    .line 346
    const-string v3, "sb_service"

    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v3

    if-nez v3, :cond_2

    .line 347
    const-string v3, "sb_service"

    invoke-static {v3, p0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V

    .line 350
    :cond_2
    invoke-direct {p0}, Lcom/samsung/android/smartbonding/SmartBondingService;->loadPreference()V

    .line 352
    const-string v3, "true"

    const-string v4, "isKioskModeEnabled"

    invoke-static {p1, v4}, Landroid/os/PersonaManager;->getKnoxInfoForApp(Landroid/content/Context;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "isKioskModeEnabled"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    iput-boolean v3, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->isKioskContainer:Z

    .line 354
    sget-boolean v3, Lcom/samsung/android/smartbonding/SmartBondingService;->SHIP_BUILD:Z

    if-eqz v3, :cond_3

    .line 355
    sput-boolean v6, Lcom/samsung/android/smartbonding/SmartBondingService;->VDBG:Z

    .line 357
    :cond_3
    const-string v3, "SmartBondingService: start done"

    invoke-static {v3}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 358
    return-void

    .line 293
    .end local v1    # "sbFilter":Landroid/content/IntentFilter;
    :cond_4
    const v3, 0x1030128

    goto/16 :goto_0

    .line 299
    nop

    :array_0
    .array-data 8
        0x0
        0x0
        0x0
        0x0
    .end array-data
.end method

.method static synthetic access$000()Z
    .locals 1

    .prologue
    .line 110
    sget-boolean v0, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    return v0
.end method

.method static synthetic access$100(Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 110
    invoke-static {p0}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/samsung/android/smartbonding/SmartBondingService;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/smartbonding/SmartBondingService;

    .prologue
    .line 110
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSBUrlStatus:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/samsung/android/smartbonding/SmartBondingService;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/smartbonding/SmartBondingService;

    .prologue
    .line 110
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSBDataStatistics:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/samsung/android/smartbonding/SmartBondingService;)Landroid/net/wifi/WifiManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/smartbonding/SmartBondingService;

    .prologue
    .line 110
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mWifiManager:Landroid/net/wifi/WifiManager;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/samsung/android/smartbonding/SmartBondingService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/smartbonding/SmartBondingService;

    .prologue
    .line 110
    iget-boolean v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mIsNoneSettingMode:Z

    return v0
.end method

.method static synthetic access$1302(Lcom/samsung/android/smartbonding/SmartBondingService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/smartbonding/SmartBondingService;
    .param p1, "x1"    # Z

    .prologue
    .line 110
    iput-boolean p1, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mIsNoneSettingMode:Z

    return p1
.end method

.method static synthetic access$1400(Lcom/samsung/android/smartbonding/SmartBondingService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/smartbonding/SmartBondingService;

    .prologue
    .line 110
    invoke-direct {p0}, Lcom/samsung/android/smartbonding/SmartBondingService;->getMobileDataEnabled()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1500(Lcom/samsung/android/smartbonding/SmartBondingService;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/smartbonding/SmartBondingService;
    .param p1, "x1"    # Z

    .prologue
    .line 110
    invoke-direct {p0, p1}, Lcom/samsung/android/smartbonding/SmartBondingService;->setMobileDataEnabled(Z)V

    return-void
.end method

.method static synthetic access$1600(Lcom/samsung/android/smartbonding/SmartBondingService;IJJ)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/smartbonding/SmartBondingService;
    .param p1, "x1"    # I
    .param p2, "x2"    # J
    .param p4, "x3"    # J

    .prologue
    .line 110
    invoke-direct/range {p0 .. p5}, Lcom/samsung/android/smartbonding/SmartBondingService;->sendSBVzwStateChangedIntent(IJJ)V

    return-void
.end method

.method static synthetic access$1700(Lcom/samsung/android/smartbonding/SmartBondingService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/smartbonding/SmartBondingService;

    .prologue
    .line 110
    invoke-direct {p0}, Lcom/samsung/android/smartbonding/SmartBondingService;->turnOffMobileConnection()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1800(Lcom/samsung/android/smartbonding/SmartBondingService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/smartbonding/SmartBondingService;

    .prologue
    .line 110
    invoke-direct {p0}, Lcom/samsung/android/smartbonding/SmartBondingService;->checkRemovedProcess()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1900(Lcom/samsung/android/smartbonding/SmartBondingService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/smartbonding/SmartBondingService;

    .prologue
    .line 110
    invoke-direct {p0}, Lcom/samsung/android/smartbonding/SmartBondingService;->turnOnMobileConnection()Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/samsung/android/smartbonding/SmartBondingService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/smartbonding/SmartBondingService;

    .prologue
    .line 110
    invoke-direct {p0}, Lcom/samsung/android/smartbonding/SmartBondingService;->checkSiopToastCondition()Z

    move-result v0

    return v0
.end method

.method static synthetic access$2000(Lcom/samsung/android/smartbonding/SmartBondingService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/smartbonding/SmartBondingService;

    .prologue
    .line 110
    invoke-direct {p0}, Lcom/samsung/android/smartbonding/SmartBondingService;->checkSBWorkingCondition()Z

    move-result v0

    return v0
.end method

.method static synthetic access$2100(Lcom/samsung/android/smartbonding/SmartBondingService;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/smartbonding/SmartBondingService;

    .prologue
    .line 110
    iget v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSBState:I

    return v0
.end method

.method static synthetic access$2102(Lcom/samsung/android/smartbonding/SmartBondingService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/smartbonding/SmartBondingService;
    .param p1, "x1"    # I

    .prologue
    .line 110
    iput p1, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSBState:I

    return p1
.end method

.method static synthetic access$2200(Lcom/samsung/android/smartbonding/SmartBondingService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/smartbonding/SmartBondingService;

    .prologue
    .line 110
    invoke-direct {p0}, Lcom/samsung/android/smartbonding/SmartBondingService;->sendSBStartIntent()V

    return-void
.end method

.method static synthetic access$2300(Lcom/samsung/android/smartbonding/SmartBondingService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/smartbonding/SmartBondingService;

    .prologue
    .line 110
    invoke-direct {p0}, Lcom/samsung/android/smartbonding/SmartBondingService;->sendSBStopIntent()V

    return-void
.end method

.method static synthetic access$2400(Lcom/samsung/android/smartbonding/SmartBondingService;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/smartbonding/SmartBondingService;

    .prologue
    .line 110
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/samsung/android/smartbonding/SmartBondingService;Landroid/content/Context;II)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/smartbonding/SmartBondingService;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # I
    .param p3, "x3"    # I

    .prologue
    .line 110
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/smartbonding/SmartBondingService;->showMessage(Landroid/content/Context;II)V

    return-void
.end method

.method static synthetic access$2602(Lcom/samsung/android/smartbonding/SmartBondingService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/smartbonding/SmartBondingService;
    .param p1, "x1"    # Z

    .prologue
    .line 110
    iput-boolean p1, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mNetworkEnabled:Z

    return p1
.end method

.method static synthetic access$2700(Lcom/samsung/android/smartbonding/SmartBondingService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/smartbonding/SmartBondingService;

    .prologue
    .line 110
    invoke-direct {p0}, Lcom/samsung/android/smartbonding/SmartBondingService;->getNetworkEnabled()Z

    move-result v0

    return v0
.end method

.method static synthetic access$2800(Lcom/samsung/android/smartbonding/SmartBondingService;Ljava/lang/String;J)[Ljava/net/InetAddress;
    .locals 2
    .param p0, "x0"    # Lcom/samsung/android/smartbonding/SmartBondingService;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # J

    .prologue
    .line 110
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/smartbonding/SmartBondingService;->getAllByName(Ljava/lang/String;J)[Ljava/net/InetAddress;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2900(Lcom/samsung/android/smartbonding/SmartBondingService;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/smartbonding/SmartBondingService;

    .prologue
    .line 110
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mCurrentActivity:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2902(Lcom/samsung/android/smartbonding/SmartBondingService;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/smartbonding/SmartBondingService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 110
    iput-object p1, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mCurrentActivity:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$300(Lcom/samsung/android/smartbonding/SmartBondingService;)Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/smartbonding/SmartBondingService;

    .prologue
    .line 110
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mHandler:Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/samsung/android/smartbonding/SmartBondingService;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/smartbonding/SmartBondingService;

    .prologue
    .line 110
    invoke-direct {p0}, Lcom/samsung/android/smartbonding/SmartBondingService;->getCurrentActivity()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3100(Lcom/samsung/android/smartbonding/SmartBondingService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/smartbonding/SmartBondingService;

    .prologue
    .line 110
    invoke-direct {p0}, Lcom/samsung/android/smartbonding/SmartBondingService;->isAirPlaneMode()Z

    move-result v0

    return v0
.end method

.method static synthetic access$3200(Lcom/samsung/android/smartbonding/SmartBondingService;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/smartbonding/SmartBondingService;

    .prologue
    .line 110
    iget v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mCurrentUserId:I

    return v0
.end method

.method static synthetic access$3202(Lcom/samsung/android/smartbonding/SmartBondingService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/smartbonding/SmartBondingService;
    .param p1, "x1"    # I

    .prologue
    .line 110
    iput p1, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mCurrentUserId:I

    return p1
.end method

.method static synthetic access$3300(Lcom/samsung/android/smartbonding/SmartBondingService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/smartbonding/SmartBondingService;

    .prologue
    .line 110
    iget-boolean v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->isKioskContainer:Z

    return v0
.end method

.method static synthetic access$3302(Lcom/samsung/android/smartbonding/SmartBondingService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/smartbonding/SmartBondingService;
    .param p1, "x1"    # Z

    .prologue
    .line 110
    iput-boolean p1, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->isKioskContainer:Z

    return p1
.end method

.method static synthetic access$3400()Z
    .locals 1

    .prologue
    .line 110
    sget-boolean v0, Lcom/samsung/android/smartbonding/SmartBondingService;->VDBG:Z

    return v0
.end method

.method static synthetic access$3500(Lcom/samsung/android/smartbonding/SmartBondingService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/smartbonding/SmartBondingService;

    .prologue
    .line 110
    iget-boolean v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mPreSBSetting:Z

    return v0
.end method

.method static synthetic access$3502(Lcom/samsung/android/smartbonding/SmartBondingService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/smartbonding/SmartBondingService;
    .param p1, "x1"    # Z

    .prologue
    .line 110
    iput-boolean p1, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mPreSBSetting:Z

    return p1
.end method

.method static synthetic access$3602(Lcom/samsung/android/smartbonding/SmartBondingService;Landroid/net/LinkProperties;)Landroid/net/LinkProperties;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/smartbonding/SmartBondingService;
    .param p1, "x1"    # Landroid/net/LinkProperties;

    .prologue
    .line 110
    iput-object p1, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mWifiLp:Landroid/net/LinkProperties;

    return-object p1
.end method

.method static synthetic access$3700(Lcom/samsung/android/smartbonding/SmartBondingService;I)Landroid/net/LinkProperties;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/smartbonding/SmartBondingService;
    .param p1, "x1"    # I

    .prologue
    .line 110
    invoke-direct {p0, p1}, Lcom/samsung/android/smartbonding/SmartBondingService;->getLinkProperties(I)Landroid/net/LinkProperties;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3800(Lcom/samsung/android/smartbonding/SmartBondingService;II)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/smartbonding/SmartBondingService;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 110
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/smartbonding/SmartBondingService;->getSBInterfaceDirect(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3900(Lcom/samsung/android/smartbonding/SmartBondingService;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/smartbonding/SmartBondingService;

    .prologue
    .line 110
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mCurrentWifiBSSID:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3902(Lcom/samsung/android/smartbonding/SmartBondingService;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/smartbonding/SmartBondingService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 110
    iput-object p1, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mCurrentWifiBSSID:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$400(Lcom/samsung/android/smartbonding/SmartBondingService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/smartbonding/SmartBondingService;

    .prologue
    .line 110
    iget-boolean v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mWifiConnected:Z

    return v0
.end method

.method static synthetic access$4000(Lcom/samsung/android/smartbonding/SmartBondingService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/smartbonding/SmartBondingService;

    .prologue
    .line 110
    invoke-direct {p0}, Lcom/samsung/android/smartbonding/SmartBondingService;->initSpeedRatio()V

    return-void
.end method

.method static synthetic access$402(Lcom/samsung/android/smartbonding/SmartBondingService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/smartbonding/SmartBondingService;
    .param p1, "x1"    # Z

    .prologue
    .line 110
    iput-boolean p1, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mWifiConnected:Z

    return p1
.end method

.method static synthetic access$4100(Lcom/samsung/android/smartbonding/SmartBondingService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/smartbonding/SmartBondingService;

    .prologue
    .line 110
    iget-boolean v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mMobileConnected:Z

    return v0
.end method

.method static synthetic access$4102(Lcom/samsung/android/smartbonding/SmartBondingService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/smartbonding/SmartBondingService;
    .param p1, "x1"    # Z

    .prologue
    .line 110
    iput-boolean p1, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mMobileConnected:Z

    return p1
.end method

.method static synthetic access$4202(Lcom/samsung/android/smartbonding/SmartBondingService;Landroid/net/LinkProperties;)Landroid/net/LinkProperties;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/smartbonding/SmartBondingService;
    .param p1, "x1"    # Landroid/net/LinkProperties;

    .prologue
    .line 110
    iput-object p1, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mMobileLp:Landroid/net/LinkProperties;

    return-object p1
.end method

.method static synthetic access$500(Lcom/samsung/android/smartbonding/SmartBondingService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/smartbonding/SmartBondingService;

    .prologue
    .line 110
    invoke-direct {p0}, Lcom/samsung/android/smartbonding/SmartBondingService;->getSBUsageEnabled()Z

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/samsung/android/smartbonding/SmartBondingService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/smartbonding/SmartBondingService;

    .prologue
    .line 110
    iget-boolean v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mDisableBySIOP:Z

    return v0
.end method

.method static synthetic access$602(Lcom/samsung/android/smartbonding/SmartBondingService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/smartbonding/SmartBondingService;
    .param p1, "x1"    # Z

    .prologue
    .line 110
    iput-boolean p1, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mDisableBySIOP:Z

    return p1
.end method

.method static synthetic access$700(Lcom/samsung/android/smartbonding/SmartBondingService;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/smartbonding/SmartBondingService;

    .prologue
    .line 110
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSBUsageStatus:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$800(Lcom/samsung/android/smartbonding/SmartBondingService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/smartbonding/SmartBondingService;

    .prologue
    .line 110
    invoke-direct {p0}, Lcom/samsung/android/smartbonding/SmartBondingService;->checkSBEnableCondition()Z

    move-result v0

    return v0
.end method

.method static synthetic access$900(Lcom/samsung/android/smartbonding/SmartBondingService;Z)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/smartbonding/SmartBondingService;
    .param p1, "x1"    # Z

    .prologue
    .line 110
    invoke-direct {p0, p1}, Lcom/samsung/android/smartbonding/SmartBondingService;->setSBInterfacesEnabled(Z)I

    move-result v0

    return v0
.end method

.method private checkRemovedProcess()Z
    .locals 6

    .prologue
    .line 2097
    const/4 v2, 0x0

    .line 2099
    .local v2, "removed":Z
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2101
    .local v1, "mRemoveSBUsageStatus":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;>;"
    iget-object v4, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSBUsageStatus:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;

    .line 2102
    .local v3, "usageStatus":Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;
    iget v4, v3, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;->mPid:I

    invoke-direct {p0, v4}, Lcom/samsung/android/smartbonding/SmartBondingService;->isRunningProcess(I)Z

    move-result v4

    if-nez v4, :cond_0

    .line 2103
    iget-object v4, v3, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;->mUrl:Ljava/lang/String;

    invoke-direct {p0, v4}, Lcom/samsung/android/smartbonding/SmartBondingService;->isYouTubeContent(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    const-string v4, "com.google.android.youtube"

    invoke-direct {p0, v4}, Lcom/samsung/android/smartbonding/SmartBondingService;->isRunningProcess(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2104
    sget-boolean v4, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v4, :cond_1

    const-string v4, "checkRemovedProcess : youtube process is running so that don\'t need to remove list"

    invoke-static {v4}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 2105
    :cond_1
    const-string v4, ""

    iput-object v4, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mCurrentActivity:Ljava/lang/String;

    goto :goto_0

    .line 2108
    :cond_2
    sget-boolean v4, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v4, :cond_3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "checkRemovedProcess : process "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v3, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;->mPid:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " is stopped. add it in remove usage list"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 2109
    :cond_3
    const/4 v2, 0x1

    .line 2110
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2114
    .end local v3    # "usageStatus":Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;
    :cond_4
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;

    .line 2115
    .restart local v3    # "usageStatus":Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;
    iget-object v4, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSBUsageStatus:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2118
    .end local v3    # "usageStatus":Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;
    :cond_5
    if-eqz v2, :cond_7

    .line 2119
    sget-boolean v4, Lcom/samsung/android/smartbonding/SmartBondingService;->VDBG:Z

    if-eqz v4, :cond_6

    const-string v4, "checkRemovedProcess : some process are remvoed"

    invoke-static {v4}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 2123
    :cond_6
    :goto_2
    return v2

    .line 2121
    :cond_7
    sget-boolean v4, Lcom/samsung/android/smartbonding/SmartBondingService;->VDBG:Z

    if-eqz v4, :cond_6

    const-string v4, "checkRemovedProcess : no process is remvoed"

    invoke-static {v4}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    goto :goto_2
.end method

.method private checkSBEnableCondition()Z
    .locals 1

    .prologue
    .line 749
    invoke-virtual {p0}, Lcom/samsung/android/smartbonding/SmartBondingService;->getSBEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/samsung/android/smartbonding/SmartBondingService;->getSBUsageEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 750
    const/4 v0, 0x1

    .line 759
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private checkSBWorkingCondition()Z
    .locals 2

    .prologue
    .line 763
    invoke-virtual {p0}, Lcom/samsung/android/smartbonding/SmartBondingService;->getSBEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/samsung/android/smartbonding/SmartBondingService;->getSBUsageEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/smartbonding/SmartBondingService;->getSBInterfaceState()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 764
    const/4 v0, 0x1

    .line 773
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private checkSiopToastCondition()Z
    .locals 8

    .prologue
    .line 1240
    const/4 v1, 0x0

    .line 1241
    .local v1, "isShowToast":Z
    invoke-virtual {p0}, Lcom/samsung/android/smartbonding/SmartBondingService;->getSBEnabled()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1242
    invoke-direct {p0}, Lcom/samsung/android/smartbonding/SmartBondingService;->getSBUsageEnabled()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-boolean v3, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mDisableBySIOP:Z

    if-eqz v3, :cond_1

    .line 1243
    iget-object v3, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSBUsageStatus:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;

    .line 1244
    .local v2, "usageStatus":Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;
    iget-wide v4, v2, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;->mStartRange:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-nez v3, :cond_0

    iget-object v3, v2, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;->mUrl:Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/samsung/android/smartbonding/SmartBondingService;->isFirstCombinedRequest(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1245
    const/4 v1, 0x1

    goto :goto_0

    .line 1250
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v2    # "usageStatus":Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;
    :cond_1
    return v1
.end method

.method private convertCombinedUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 1276
    invoke-direct {p0, p1}, Lcom/samsung/android/smartbonding/SmartBondingService;->isGooglePlayContent(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1277
    invoke-direct {p0, p1}, Lcom/samsung/android/smartbonding/SmartBondingService;->getGooglePlayContentUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 1279
    .end local p1    # "url":Ljava/lang/String;
    :cond_0
    return-object p1
.end method

.method private convertDetailUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 1283
    invoke-direct {p0, p1}, Lcom/samsung/android/smartbonding/SmartBondingService;->isGooglePlayContent(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1284
    invoke-direct {p0, p1}, Lcom/samsung/android/smartbonding/SmartBondingService;->getGooglePlayContentUrlWithParam(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 1286
    .end local p1    # "url":Ljava/lang/String;
    :cond_0
    return-object p1
.end method

.method private convertSBInterfaceState(ZZ)I
    .locals 1
    .param p1, "mobile"    # Z
    .param p2, "wifi"    # Z

    .prologue
    .line 916
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 917
    const/4 v0, 0x3

    .line 923
    :goto_0
    return v0

    .line 918
    :cond_0
    if-eqz p1, :cond_1

    if-nez p2, :cond_1

    .line 919
    const/4 v0, 0x2

    goto :goto_0

    .line 920
    :cond_1
    if-nez p1, :cond_2

    if-eqz p2, :cond_2

    .line 921
    const/4 v0, 0x1

    goto :goto_0

    .line 923
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private enforceAccessPermission()V
    .locals 3

    .prologue
    .line 1028
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/smartbonding/SmartBondingService;->isPermissionAllowed(II)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1029
    sget-boolean v0, Lcom/samsung/android/smartbonding/SmartBondingService;->VDBG:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "enforceAccessPermission : allowed process : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/smartbonding/SmartBondingService;->getProcessName(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", UID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 1035
    :cond_0
    :goto_0
    return-void

    .line 1032
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mContext:Landroid/content/Context;

    const-string v1, "android.permission.ACCESS_NETWORK_STATE"

    const-string v2, "SmartBondingService"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private getAllByName(Ljava/lang/String;J)[Ljava/net/InetAddress;
    .locals 18
    .param p1, "host"    # Ljava/lang/String;
    .param p2, "threadId"    # J

    .prologue
    .line 2222
    const/4 v14, 0x0

    new-array v13, v14, [Ljava/net/InetAddress;

    .line 2224
    .local v13, "ret":[Ljava/net/InetAddress;
    :try_start_0
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "getAllByName: thread"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-wide/from16 v0, p2

    invoke-virtual {v14, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 2225
    sget-boolean v14, Lcom/samsung/android/smartbonding/SmartBondingService;->VDBG:Z

    if-eqz v14, :cond_0

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "getAllByName : start to get IP for host "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " at time "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 2226
    :cond_0
    invoke-static/range {p1 .. p1}, Ljava/net/InetAddress;->getAllByName(Ljava/lang/String;)[Ljava/net/InetAddress;

    move-result-object v13

    .line 2228
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v14

    const-string v15, "CscFeature_RIL_HandleUnsupportedDns64DuringDownBooster"

    invoke-virtual {v14, v15}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_4

    .line 2230
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/samsung/android/smartbonding/SmartBondingService;->mMobileConnected:Z

    if-eqz v14, :cond_4

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/smartbonding/SmartBondingService;->mMobileLp:Landroid/net/LinkProperties;

    if-eqz v14, :cond_4

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/smartbonding/SmartBondingService;->mMobileLp:Landroid/net/LinkProperties;

    invoke-virtual {v14}, Landroid/net/LinkProperties;->hasGlobalIPv6Address()Z

    move-result v14

    if-eqz v14, :cond_4

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/smartbonding/SmartBondingService;->mMobileLp:Landroid/net/LinkProperties;

    invoke-virtual {v14}, Landroid/net/LinkProperties;->hasIPv4Address()Z

    move-result v14

    if-nez v14, :cond_4

    .line 2232
    array-length v14, v13

    new-array v8, v14, [Ljava/net/InetAddress;

    .line 2233
    .local v8, "filtered":[Ljava/net/InetAddress;
    const/4 v4, 0x0

    .line 2234
    .local v4, "countOfIPv6":I
    move-object v3, v13

    .local v3, "arr$":[Ljava/net/InetAddress;
    array-length v11, v3

    .local v11, "len$":I
    const/4 v10, 0x0

    .local v10, "i$":I
    move v5, v4

    .end local v4    # "countOfIPv6":I
    .local v5, "countOfIPv6":I
    :goto_0
    if-ge v10, v11, :cond_1

    aget-object v2, v3, v10

    .line 2235
    .local v2, "address":Ljava/net/InetAddress;
    instance-of v14, v2, Ljava/net/Inet6Address;

    if-eqz v14, :cond_a

    .line 2236
    add-int/lit8 v4, v5, 0x1

    .end local v5    # "countOfIPv6":I
    .restart local v4    # "countOfIPv6":I
    aput-object v2, v8, v5

    .line 2234
    :goto_1
    add-int/lit8 v10, v10, 0x1

    move v5, v4

    .end local v4    # "countOfIPv6":I
    .restart local v5    # "countOfIPv6":I
    goto :goto_0

    .line 2240
    .end local v2    # "address":Ljava/net/InetAddress;
    :cond_1
    if-nez v5, :cond_7

    .line 2241
    new-instance v14, Ljava/net/UnknownHostException;

    const-string v15, "IPv6 network cannot connect to IPv4 without clat"

    invoke-direct {v14, v15}, Ljava/net/UnknownHostException;-><init>(Ljava/lang/String;)V

    throw v14
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2248
    .end local v3    # "arr$":[Ljava/net/InetAddress;
    .end local v5    # "countOfIPv6":I
    .end local v8    # "filtered":[Ljava/net/InetAddress;
    .end local v10    # "i$":I
    .end local v11    # "len$":I
    :catch_0
    move-exception v6

    .line 2249
    .local v6, "e":Ljava/net/UnknownHostException;
    sget-boolean v14, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v14, :cond_2

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "getAllByName : UnknownHostException "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 2250
    :cond_2
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v14

    const-string v15, "CscFeature_RIL_HandleUnsupportedDns64DuringDownBooster"

    invoke-virtual {v14, v15}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_4

    .line 2252
    :try_start_1
    sget-boolean v14, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v14, :cond_3

    const-string v14, "getAllByName: get host from A records - DNS64 missing extension"

    invoke-static {v14}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 2253
    :cond_3
    new-instance v12, Lcom/samsung/android/smartbonding/Dns46NameResolver;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/smartbonding/SmartBondingService;->mContext:Landroid/content/Context;

    invoke-direct {v12, v14}, Lcom/samsung/android/smartbonding/Dns46NameResolver;-><init>(Landroid/content/Context;)V

    .line 2254
    .local v12, "resolver":Lcom/samsung/android/smartbonding/Dns46NameResolver;
    move-object/from16 v0, p1

    invoke-virtual {v12, v0}, Lcom/samsung/android/smartbonding/Dns46NameResolver;->getAllByNameWithPlat(Ljava/lang/String;)[Ljava/net/InetAddress;
    :try_end_1
    .catch Ljava/net/UnknownHostException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v13

    .line 2260
    .end local v6    # "e":Ljava/net/UnknownHostException;
    .end local v12    # "resolver":Lcom/samsung/android/smartbonding/Dns46NameResolver;
    :cond_4
    :goto_2
    sget-boolean v14, Lcom/samsung/android/smartbonding/SmartBondingService;->VDBG:Z

    if-eqz v14, :cond_5

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "finish to get IP for host "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " at time "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ", result number "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    if-nez v13, :cond_8

    const/4 v14, 0x0

    :goto_3
    invoke-virtual {v15, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 2262
    :cond_5
    if-eqz v13, :cond_9

    .line 2263
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_4
    array-length v14, v13

    if-ge v9, v14, :cond_9

    .line 2264
    sget-boolean v14, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v14, :cond_6

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "getAllByName : dst address : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    aget-object v15, v13, v9

    invoke-virtual {v15}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 2263
    :cond_6
    add-int/lit8 v9, v9, 0x1

    goto :goto_4

    .line 2244
    .end local v9    # "i":I
    .restart local v3    # "arr$":[Ljava/net/InetAddress;
    .restart local v5    # "countOfIPv6":I
    .restart local v8    # "filtered":[Ljava/net/InetAddress;
    .restart local v10    # "i$":I
    .restart local v11    # "len$":I
    :cond_7
    move-object v13, v8

    goto :goto_2

    .line 2255
    .end local v3    # "arr$":[Ljava/net/InetAddress;
    .end local v5    # "countOfIPv6":I
    .end local v8    # "filtered":[Ljava/net/InetAddress;
    .end local v10    # "i$":I
    .end local v11    # "len$":I
    .restart local v6    # "e":Ljava/net/UnknownHostException;
    :catch_1
    move-exception v7

    .line 2256
    .local v7, "e2":Ljava/net/UnknownHostException;
    sget-boolean v14, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v14, :cond_4

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "getAllByName : UnknownHostException "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    goto :goto_2

    .line 2260
    .end local v6    # "e":Ljava/net/UnknownHostException;
    .end local v7    # "e2":Ljava/net/UnknownHostException;
    :cond_8
    array-length v14, v13

    goto :goto_3

    .line 2267
    :cond_9
    return-object v13

    .restart local v2    # "address":Ljava/net/InetAddress;
    .restart local v3    # "arr$":[Ljava/net/InetAddress;
    .restart local v5    # "countOfIPv6":I
    .restart local v8    # "filtered":[Ljava/net/InetAddress;
    .restart local v10    # "i$":I
    .restart local v11    # "len$":I
    :cond_a
    move v4, v5

    .end local v5    # "countOfIPv6":I
    .restart local v4    # "countOfIPv6":I
    goto/16 :goto_1
.end method

.method private getApplicationName(ILjava/lang/String;)Ljava/lang/String;
    .locals 12
    .param p1, "pid"    # I
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    const/4 v11, 0x0

    .line 1776
    const-string v2, ""

    .line 1778
    .local v2, "appName":Ljava/lang/String;
    iget-object v9, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mContext:Landroid/content/Context;

    iget-object v10, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mContext:Landroid/content/Context;

    const-string v10, "activity"

    invoke-virtual {v9, v10}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager;

    .line 1779
    .local v1, "am":Landroid/app/ActivityManager;
    iget-object v9, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    .line 1780
    .local v6, "pm":Landroid/content/pm/PackageManager;
    invoke-virtual {v1}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v8

    .line 1782
    .local v8, "raInfos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 1783
    .local v7, "raInfo":Landroid/app/ActivityManager$RunningAppProcessInfo;
    iget-object v9, v7, Landroid/app/ActivityManager$RunningAppProcessInfo;->pkgList:[Ljava/lang/String;

    array-length v9, v9

    if-eqz v9, :cond_0

    .line 1788
    invoke-direct {p0, p2}, Lcom/samsung/android/smartbonding/SmartBondingService;->isGooglePlayContent(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_3

    const-string v9, "com.android.vending"

    iget-object v10, v7, Landroid/app/ActivityManager$RunningAppProcessInfo;->pkgList:[Ljava/lang/String;

    aget-object v10, v10, v11

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 1790
    :try_start_0
    const-string v9, "com.android.vending"

    const/4 v10, 0x1

    invoke-virtual {v6, v9, v10}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v5

    .line 1791
    .local v5, "pkgInfo":Landroid/content/pm/PackageInfo;
    iget-object v9, v5, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v6, v9}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v9

    move-object v0, v9

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1818
    .end local v5    # "pkgInfo":Landroid/content/pm/PackageInfo;
    .end local v7    # "raInfo":Landroid/app/ActivityManager$RunningAppProcessInfo;
    :cond_1
    :goto_1
    sget-boolean v9, Lcom/samsung/android/smartbonding/SmartBondingService;->VDBG:Z

    if-eqz v9, :cond_2

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "getApplicationName : calling application name : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " for pid "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 1819
    :cond_2
    return-object v2

    .line 1793
    .restart local v7    # "raInfo":Landroid/app/ActivityManager$RunningAppProcessInfo;
    :catch_0
    move-exception v3

    .line 1794
    .local v3, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    sget-boolean v9, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v9, :cond_3

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "getApplicationName : error "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 1798
    .end local v3    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_3
    invoke-direct {p0, p2}, Lcom/samsung/android/smartbonding/SmartBondingService;->isYouTubeContent(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_4

    const-string v9, "com.google.android.youtube"

    iget-object v10, v7, Landroid/app/ActivityManager$RunningAppProcessInfo;->pkgList:[Ljava/lang/String;

    aget-object v10, v10, v11

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 1800
    :try_start_1
    const-string v9, "com.google.android.youtube"

    const/4 v10, 0x1

    invoke-virtual {v6, v9, v10}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v5

    .line 1801
    .restart local v5    # "pkgInfo":Landroid/content/pm/PackageInfo;
    iget-object v9, v5, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v6, v9}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v9

    move-object v0, v9

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 1803
    .end local v5    # "pkgInfo":Landroid/content/pm/PackageInfo;
    :catch_1
    move-exception v3

    .line 1804
    .restart local v3    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    sget-boolean v9, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v9, :cond_4

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "getApplicationName : error "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 1808
    .end local v3    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_4
    if-eqz p1, :cond_0

    iget v9, v7, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    if-ne v9, p1, :cond_0

    .line 1810
    :try_start_2
    iget-object v9, v7, Landroid/app/ActivityManager$RunningAppProcessInfo;->pkgList:[Ljava/lang/String;

    const/4 v10, 0x0

    aget-object v9, v9, v10

    const/4 v10, 0x1

    invoke-virtual {v6, v9, v10}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v5

    .line 1811
    .restart local v5    # "pkgInfo":Landroid/content/pm/PackageInfo;
    iget-object v9, v5, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v6, v9}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v2

    .end local v2    # "appName":Ljava/lang/String;
    check-cast v2, Ljava/lang/String;
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_2

    .restart local v2    # "appName":Ljava/lang/String;
    goto/16 :goto_0

    .line 1812
    .end local v2    # "appName":Ljava/lang/String;
    .end local v5    # "pkgInfo":Landroid/content/pm/PackageInfo;
    :catch_2
    move-exception v3

    .line 1813
    .restart local v3    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    sget-boolean v9, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v9, :cond_5

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "getApplicationName : error "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 1814
    :cond_5
    const-string v2, ""

    .restart local v2    # "appName":Ljava/lang/String;
    goto/16 :goto_0
.end method

.method private getCurrentActivity()Ljava/lang/String;
    .locals 7

    .prologue
    .line 2032
    const-string v2, ""

    .line 2034
    .local v2, "result":Ljava/lang/String;
    :try_start_0
    iget-object v5, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mContext:Landroid/content/Context;

    const-string v6, "activity"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 2035
    .local v0, "am":Landroid/app/ActivityManager;
    if-eqz v0, :cond_1

    .line 2036
    const/4 v5, 0x1

    invoke-virtual {v0, v5}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v3

    .line 2037
    .local v3, "taskInfo":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    const/4 v5, 0x0

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v4, v5, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    .line 2038
    .local v4, "topActivity":Landroid/content/ComponentName;
    if-eqz v4, :cond_1

    .line 2039
    invoke-virtual {v4}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v2

    .line 2040
    sget-boolean v5, Lcom/samsung/android/smartbonding/SmartBondingService;->VDBG:Z

    if-eqz v5, :cond_0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getCurrentActivityName : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 2041
    :cond_0
    if-nez v2, :cond_1

    .line 2042
    const-string v2, ""
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2049
    .end local v0    # "am":Landroid/app/ActivityManager;
    .end local v3    # "taskInfo":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    .end local v4    # "topActivity":Landroid/content/ComponentName;
    :cond_1
    :goto_0
    return-object v2

    .line 2046
    :catch_0
    move-exception v1

    .line 2047
    .local v1, "e":Ljava/lang/Exception;
    sget-boolean v5, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v5, :cond_1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "exception to get activity name : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private getGooglePlayContentUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 1331
    move-object v0, p1

    .line 1333
    .local v0, "googlePlayUrl":Ljava/lang/String;
    invoke-direct {p0, p1}, Lcom/samsung/android/smartbonding/SmartBondingService;->isGooglePlayContent(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1334
    const-string v4, "\\/"

    invoke-virtual {p1, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 1335
    .local v3, "urls":[Ljava/lang/String;
    const-string v2, ""

    .line 1338
    .local v2, "newUrl":Ljava/lang/String;
    aget-object v4, v3, v5

    if-eqz v4, :cond_1

    aget-object v4, v3, v5

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_1

    .line 1340
    const/4 v1, 0x3

    .local v1, "i":I
    :goto_0
    const/4 v4, 0x7

    if-ge v1, v4, :cond_0

    .line 1341
    const-string v4, "/"

    invoke-virtual {v2, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1342
    aget-object v4, v3, v1

    invoke-virtual {v2, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1340
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1345
    :cond_0
    move-object v0, v2

    .line 1349
    .end local v1    # "i":I
    .end local v2    # "newUrl":Ljava/lang/String;
    .end local v3    # "urls":[Ljava/lang/String;
    :cond_1
    return-object v0
.end method

.method private getGooglePlayContentUrlWithParam(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 1353
    move-object v0, p1

    .line 1354
    .local v0, "googlePlayUrl":Ljava/lang/String;
    invoke-direct {p0, p1}, Lcom/samsung/android/smartbonding/SmartBondingService;->isGooglePlayContent(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1355
    const-string v2, "\\?"

    invoke-virtual {p1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 1356
    .local v1, "urls":[Ljava/lang/String;
    sget-boolean v2, Lcom/samsung/android/smartbonding/SmartBondingService;->VDBG:Z

    if-eqz v2, :cond_0

    const-string v2, "getGooglePlayContentUrlWithParam : google play store contents"

    invoke-static {v2}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 1357
    :cond_0
    aget-object v2, v1, v3

    if-eqz v2, :cond_1

    aget-object v2, v1, v3

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_1

    .line 1358
    aget-object v0, v1, v3

    .line 1361
    .end local v1    # "urls":[Ljava/lang/String;
    :cond_1
    return-object v0
.end method

.method private getLinkProperties(I)Landroid/net/LinkProperties;
    .locals 9
    .param p1, "netType"    # I

    .prologue
    const/4 v8, 0x1

    .line 975
    const/4 v1, 0x0

    .line 977
    .local v1, "lp":Landroid/net/LinkProperties;
    :try_start_0
    sget-boolean v6, Lcom/samsung/android/smartbonding/SmartBondingService;->VDBG:Z

    if-eqz v6, :cond_0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getLinkProperties "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    if-nez p1, :cond_3

    const-string v6, "WIFI"

    :goto_0
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 978
    :cond_0
    if-nez p1, :cond_5

    .line 979
    const/4 v4, 0x0

    .line 980
    .local v4, "wifiConnected":Z
    iget-object v6, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mCm:Landroid/net/ConnectivityManager;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v5

    .line 981
    .local v5, "wifiNetworkInfo":Landroid/net/NetworkInfo;
    if-eqz v5, :cond_1

    .line 982
    invoke-virtual {v5}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v4

    .line 984
    :cond_1
    if-nez v4, :cond_4

    .line 985
    sget-boolean v6, Lcom/samsung/android/smartbonding/SmartBondingService;->VDBG:Z

    if-eqz v6, :cond_2

    const-string v6, "getLinkProperties : wifi is not connected"

    invoke-static {v6}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .end local v4    # "wifiConnected":Z
    .end local v5    # "wifiNetworkInfo":Landroid/net/NetworkInfo;
    :cond_2
    :goto_1
    move-object v6, v1

    .line 1005
    :goto_2
    return-object v6

    .line 977
    :cond_3
    const-string v6, "MOBILE"

    goto :goto_0

    .line 987
    .restart local v4    # "wifiConnected":Z
    .restart local v5    # "wifiNetworkInfo":Landroid/net/NetworkInfo;
    :cond_4
    iget-object v6, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mCm:Landroid/net/ConnectivityManager;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Landroid/net/ConnectivityManager;->getLinkProperties(I)Landroid/net/LinkProperties;

    move-result-object v1

    goto :goto_1

    .line 990
    .end local v4    # "wifiConnected":Z
    .end local v5    # "wifiNetworkInfo":Landroid/net/NetworkInfo;
    :cond_5
    if-ne p1, v8, :cond_2

    .line 991
    const/4 v2, 0x0

    .line 992
    .local v2, "mobileConnected":Z
    iget-object v6, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mCm:Landroid/net/ConnectivityManager;

    const/4 v7, 0x5

    invoke-virtual {v6, v7}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v3

    .line 993
    .local v3, "mobileNetworkInfo":Landroid/net/NetworkInfo;
    if-eqz v3, :cond_6

    .line 994
    invoke-virtual {v3}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    .line 996
    :cond_6
    if-nez v2, :cond_7

    .line 997
    sget-boolean v6, Lcom/samsung/android/smartbonding/SmartBondingService;->VDBG:Z

    if-eqz v6, :cond_2

    const-string v6, "getLinkProperties : mobile is not connected"

    invoke-static {v6}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1003
    .end local v2    # "mobileConnected":Z
    .end local v3    # "mobileNetworkInfo":Landroid/net/NetworkInfo;
    :catch_0
    move-exception v0

    .line 1004
    .local v0, "e":Ljava/lang/Throwable;
    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/Throwable;)V

    .line 1005
    const/4 v6, 0x0

    goto :goto_2

    .line 999
    .end local v0    # "e":Ljava/lang/Throwable;
    .restart local v2    # "mobileConnected":Z
    .restart local v3    # "mobileNetworkInfo":Landroid/net/NetworkInfo;
    :cond_7
    :try_start_1
    iget-object v6, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mCm:Landroid/net/ConnectivityManager;

    const/4 v7, 0x5

    invoke-virtual {v6, v7}, Landroid/net/ConnectivityManager;->getLinkProperties(I)Landroid/net/LinkProperties;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v1

    goto :goto_1
.end method

.method private getLocalAddress(Landroid/net/LinkProperties;)Ljava/lang/String;
    .locals 2
    .param p1, "lp"    # Landroid/net/LinkProperties;

    .prologue
    .line 2377
    invoke-direct {p0, p1}, Lcom/samsung/android/smartbonding/SmartBondingService;->getLocalInetAddress(Landroid/net/LinkProperties;)Ljava/net/InetAddress;

    move-result-object v0

    .line 2378
    .local v0, "addr":Ljava/net/InetAddress;
    if-nez v0, :cond_0

    .line 2379
    const/4 v1, 0x0

    .line 2381
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private getLocalAddress(Landroid/net/LinkProperties;I)Ljava/lang/String;
    .locals 2
    .param p1, "lp"    # Landroid/net/LinkProperties;
    .param p2, "preferIpType"    # I

    .prologue
    .line 2368
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/smartbonding/SmartBondingService;->getLocalInetAddress(Landroid/net/LinkProperties;I)Ljava/net/InetAddress;

    move-result-object v0

    .line 2369
    .local v0, "addr":Ljava/net/InetAddress;
    if-nez v0, :cond_0

    .line 2370
    const/4 v1, 0x0

    .line 2372
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private getLocalInetAddress(Landroid/net/LinkProperties;)Ljava/net/InetAddress;
    .locals 4
    .param p1, "lp"    # Landroid/net/LinkProperties;

    .prologue
    const/4 v2, 0x0

    .line 2422
    if-nez p1, :cond_1

    .line 2423
    sget-boolean v3, Lcom/samsung/android/smartbonding/SmartBondingService;->VDBG:Z

    if-eqz v3, :cond_0

    const-string v3, "getLocalAddress : link properties is null"

    invoke-static {v3}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    :cond_0
    move-object v0, v2

    .line 2432
    :goto_0
    return-object v0

    .line 2426
    :cond_1
    invoke-virtual {p1}, Landroid/net/LinkProperties;->getAddresses()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/InetAddress;

    .line 2427
    .local v0, "addr":Ljava/net/InetAddress;
    invoke-virtual {v0}, Ljava/net/InetAddress;->isAnyLocalAddress()Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {v0}, Ljava/net/InetAddress;->isLinkLocalAddress()Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {v0}, Ljava/net/InetAddress;->isLoopbackAddress()Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {v0}, Ljava/net/InetAddress;->isMulticastAddress()Z

    move-result v3

    if-nez v3, :cond_2

    goto :goto_0

    .end local v0    # "addr":Ljava/net/InetAddress;
    :cond_3
    move-object v0, v2

    .line 2432
    goto :goto_0
.end method

.method private getLocalInetAddress(Landroid/net/LinkProperties;I)Ljava/net/InetAddress;
    .locals 4
    .param p1, "lp"    # Landroid/net/LinkProperties;
    .param p2, "preferIpType"    # I

    .prologue
    .line 2386
    const/4 v2, 0x0

    .line 2387
    .local v2, "result":Ljava/net/InetAddress;
    if-nez p1, :cond_1

    .line 2388
    sget-boolean v3, Lcom/samsung/android/smartbonding/SmartBondingService;->VDBG:Z

    if-eqz v3, :cond_0

    const-string v3, "getLocalAddress : link properties is null"

    invoke-static {v3}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 2389
    :cond_0
    const/4 v3, 0x0

    .line 2418
    :goto_0
    return-object v3

    .line 2391
    :cond_1
    invoke-virtual {p1}, Landroid/net/LinkProperties;->getAddresses()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/InetAddress;

    .line 2392
    .local v0, "addr":Ljava/net/InetAddress;
    if-nez p2, :cond_5

    .line 2393
    instance-of v3, v0, Ljava/net/Inet4Address;

    if-eqz v3, :cond_2

    .line 2394
    invoke-virtual {v0}, Ljava/net/InetAddress;->isAnyLocalAddress()Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {v0}, Ljava/net/InetAddress;->isLinkLocalAddress()Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {v0}, Ljava/net/InetAddress;->isLoopbackAddress()Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {v0}, Ljava/net/InetAddress;->isMulticastAddress()Z

    move-result v3

    if-nez v3, :cond_2

    .line 2396
    move-object v2, v0

    .line 2410
    .end local v0    # "addr":Ljava/net/InetAddress;
    :cond_3
    :goto_1
    if-nez v2, :cond_6

    .line 2411
    invoke-virtual {p1}, Landroid/net/LinkProperties;->getAddresses()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/InetAddress;

    .line 2412
    .restart local v0    # "addr":Ljava/net/InetAddress;
    invoke-virtual {v0}, Ljava/net/InetAddress;->isAnyLocalAddress()Z

    move-result v3

    if-nez v3, :cond_4

    invoke-virtual {v0}, Ljava/net/InetAddress;->isLinkLocalAddress()Z

    move-result v3

    if-nez v3, :cond_4

    invoke-virtual {v0}, Ljava/net/InetAddress;->isLoopbackAddress()Z

    move-result v3

    if-nez v3, :cond_4

    invoke-virtual {v0}, Ljava/net/InetAddress;->isMulticastAddress()Z

    move-result v3

    if-nez v3, :cond_4

    .line 2414
    move-object v2, v0

    goto :goto_2

    .line 2401
    :cond_5
    instance-of v3, v0, Ljava/net/Inet6Address;

    if-eqz v3, :cond_2

    .line 2402
    invoke-virtual {v0}, Ljava/net/InetAddress;->isAnyLocalAddress()Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {v0}, Ljava/net/InetAddress;->isLinkLocalAddress()Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {v0}, Ljava/net/InetAddress;->isLoopbackAddress()Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {v0}, Ljava/net/InetAddress;->isMulticastAddress()Z

    move-result v3

    if-nez v3, :cond_2

    .line 2404
    move-object v2, v0

    .line 2405
    goto :goto_1

    .end local v0    # "addr":Ljava/net/InetAddress;
    :cond_6
    move-object v3, v2

    .line 2418
    goto/16 :goto_0
.end method

.method private getMobileDataEnabled()Z
    .locals 2

    .prologue
    .line 938
    const/4 v0, 0x0

    .line 939
    .local v0, "result":Z
    iget-object v1, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mCm:Landroid/net/ConnectivityManager;

    if-eqz v1, :cond_0

    .line 941
    iget-object v1, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mCm:Landroid/net/ConnectivityManager;

    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getMobileDataEnabled()Z

    move-result v0

    .line 946
    :cond_0
    return v0
.end method

.method private getMobilePolicyDataEnable()Z
    .locals 3

    .prologue
    .line 950
    const/4 v0, 0x1

    .line 951
    .local v0, "result":Z
    iget-object v1, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mCm:Landroid/net/ConnectivityManager;

    if-eqz v1, :cond_0

    .line 953
    iget-object v1, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mCm:Landroid/net/ConnectivityManager;

    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->isMobilePolicyDataEnable()Z

    move-result v0

    .line 958
    :cond_0
    sget-boolean v1, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getMobilePolicyDataEnable : result is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 959
    :cond_1
    return v0
.end method

.method private getNetworkEnabled()Z
    .locals 4

    .prologue
    .line 963
    const/4 v1, 0x0

    .line 964
    .local v1, "isWifiEnabled":Z
    const/4 v0, 0x0

    .line 965
    .local v0, "isMobileEnabled":Z
    iget-object v2, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mWifiManager:Landroid/net/wifi/WifiManager;

    if-eqz v2, :cond_0

    .line 966
    iget-object v2, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v2}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v1

    .line 968
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/smartbonding/SmartBondingService;->getMobileDataEnabled()Z

    move-result v0

    .line 970
    sget-boolean v2, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v2, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getNetworkEnabled : wifi : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " mobile : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 971
    :cond_1
    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private getPackageName(I)Ljava/lang/String;
    .locals 8
    .param p1, "pid"    # I

    .prologue
    .line 1823
    const-string v2, ""

    .line 1825
    .local v2, "pkgName":Ljava/lang/String;
    iget-object v6, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mContext:Landroid/content/Context;

    iget-object v7, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mContext:Landroid/content/Context;

    const-string v7, "activity"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 1826
    .local v0, "am":Landroid/app/ActivityManager;
    iget-object v6, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 1827
    .local v3, "pm":Landroid/content/pm/PackageManager;
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v5

    .line 1829
    .local v5, "raInfos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 1830
    .local v4, "raInfo":Landroid/app/ActivityManager$RunningAppProcessInfo;
    iget-object v6, v4, Landroid/app/ActivityManager$RunningAppProcessInfo;->pkgList:[Ljava/lang/String;

    array-length v6, v6

    if-eqz v6, :cond_0

    .line 1834
    if-eqz p1, :cond_0

    iget v6, v4, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    if-ne v6, p1, :cond_0

    .line 1835
    iget-object v6, v4, Landroid/app/ActivityManager$RunningAppProcessInfo;->pkgList:[Ljava/lang/String;

    const/4 v7, 0x0

    aget-object v2, v6, v7

    .line 1839
    .end local v4    # "raInfo":Landroid/app/ActivityManager$RunningAppProcessInfo;
    :cond_1
    sget-boolean v6, Lcom/samsung/android/smartbonding/SmartBondingService;->VDBG:Z

    if-eqz v6, :cond_2

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getPackageName : package name "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " for pid "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 1840
    :cond_2
    return-object v2
.end method

.method private getProcessName(I)Ljava/lang/String;
    .locals 8
    .param p1, "pid"    # I

    .prologue
    .line 1844
    const-string v3, ""

    .line 1846
    .local v3, "processName":Ljava/lang/String;
    iget-object v6, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mContext:Landroid/content/Context;

    iget-object v7, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mContext:Landroid/content/Context;

    const-string v7, "activity"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 1847
    .local v0, "am":Landroid/app/ActivityManager;
    iget-object v6, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 1848
    .local v2, "pm":Landroid/content/pm/PackageManager;
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v5

    .line 1850
    .local v5, "raInfos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 1851
    .local v4, "raInfo":Landroid/app/ActivityManager$RunningAppProcessInfo;
    if-eqz p1, :cond_0

    iget v6, v4, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    if-ne v6, p1, :cond_0

    .line 1852
    iget-object v3, v4, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    .line 1857
    .end local v4    # "raInfo":Landroid/app/ActivityManager$RunningAppProcessInfo;
    :cond_1
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1858
    sget-boolean v6, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v6, :cond_2

    const-string v6, "getProcessName : unable to get process name by activity manager"

    invoke-static {v6}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 1859
    :cond_2
    invoke-direct {p0, p1}, Lcom/samsung/android/smartbonding/SmartBondingService;->getProcessNameByProc(I)Ljava/lang/String;

    move-result-object v3

    .line 1861
    :cond_3
    return-object v3
.end method

.method private getProcessNameByProc(I)Ljava/lang/String;
    .locals 8
    .param p1, "pid"    # I

    .prologue
    .line 1865
    const-string v3, ""

    .line 1866
    .local v3, "processName":Ljava/lang/String;
    const/4 v0, 0x0

    .line 1868
    .local v0, "bufferReader":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v5, Ljava/io/FileReader;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "/proc/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/comm"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v5}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1869
    .end local v0    # "bufferReader":Ljava/io/BufferedReader;
    .local v1, "bufferReader":Ljava/io/BufferedReader;
    if-eqz v1, :cond_0

    .line 1870
    :try_start_1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    .line 1871
    .local v4, "result":Ljava/lang/String;
    if-eqz v4, :cond_0

    .line 1872
    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v3

    .line 1878
    .end local v4    # "result":Ljava/lang/String;
    :cond_0
    if-eqz v1, :cond_6

    .line 1880
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v0, v1

    .line 1886
    .end local v1    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v0    # "bufferReader":Ljava/io/BufferedReader;
    :cond_1
    :goto_0
    sget-boolean v5, Lcom/samsung/android/smartbonding/SmartBondingService;->VDBG:Z

    if-eqz v5, :cond_2

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getProcessNameProc : process name "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " for pid "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 1887
    :cond_2
    return-object v3

    .line 1881
    .end local v0    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v1    # "bufferReader":Ljava/io/BufferedReader;
    :catch_0
    move-exception v2

    .line 1882
    .local v2, "e":Ljava/io/IOException;
    sget-boolean v5, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v5, :cond_3

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "exception to close buffer reader : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    :cond_3
    move-object v0, v1

    .line 1883
    .end local v1    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v0    # "bufferReader":Ljava/io/BufferedReader;
    goto :goto_0

    .line 1875
    .end local v2    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v2

    .line 1876
    .local v2, "e":Ljava/lang/Exception;
    :goto_1
    :try_start_3
    sget-boolean v5, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v5, :cond_4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "exception to get process name by proc : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1878
    :cond_4
    if-eqz v0, :cond_1

    .line 1880
    :try_start_4
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 1881
    :catch_2
    move-exception v2

    .line 1882
    .local v2, "e":Ljava/io/IOException;
    sget-boolean v5, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v5, :cond_1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "exception to close buffer reader : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1878
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v5

    :goto_2
    if-eqz v0, :cond_5

    .line 1880
    :try_start_5
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 1883
    :cond_5
    :goto_3
    throw v5

    .line 1881
    :catch_3
    move-exception v2

    .line 1882
    .restart local v2    # "e":Ljava/io/IOException;
    sget-boolean v6, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v6, :cond_5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "exception to close buffer reader : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    goto :goto_3

    .line 1878
    .end local v0    # "bufferReader":Ljava/io/BufferedReader;
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v1    # "bufferReader":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v5

    move-object v0, v1

    .end local v1    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v0    # "bufferReader":Ljava/io/BufferedReader;
    goto :goto_2

    .line 1875
    .end local v0    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v1    # "bufferReader":Ljava/io/BufferedReader;
    :catch_4
    move-exception v2

    move-object v0, v1

    .end local v1    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v0    # "bufferReader":Ljava/io/BufferedReader;
    goto :goto_1

    .end local v0    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v1    # "bufferReader":Ljava/io/BufferedReader;
    :cond_6
    move-object v0, v1

    .end local v1    # "bufferReader":Ljava/io/BufferedReader;
    .restart local v0    # "bufferReader":Ljava/io/BufferedReader;
    goto/16 :goto_0
.end method

.method private getSBInterfaceDirect(II)Ljava/lang/String;
    .locals 2
    .param p1, "netType"    # I
    .param p2, "preferIpType"    # I

    .prologue
    .line 1014
    const/4 v1, 0x0

    .line 1015
    .local v1, "lp":Landroid/net/LinkProperties;
    if-nez p1, :cond_0

    .line 1016
    iget-object v1, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mWifiLp:Landroid/net/LinkProperties;

    .line 1020
    :goto_0
    if-eqz v1, :cond_1

    .line 1021
    invoke-direct {p0, v1, p2}, Lcom/samsung/android/smartbonding/SmartBondingService;->getLocalAddress(Landroid/net/LinkProperties;I)Ljava/lang/String;

    move-result-object v0

    .line 1024
    :goto_1
    return-object v0

    .line 1018
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mMobileLp:Landroid/net/LinkProperties;

    goto :goto_0

    .line 1024
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private getSBLimitFileSize()J
    .locals 2

    .prologue
    .line 745
    const-wide/32 v0, 0x1e00000

    return-wide v0
.end method

.method private getSBUsageEnabled()Z
    .locals 5

    .prologue
    .line 1765
    const/4 v1, 0x0

    .line 1766
    .local v1, "enabled":Z
    iget-object v3, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSBUsageStatus:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;

    .line 1767
    .local v0, "UsageStatus":Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;
    iget v3, v0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;->mStatus:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 1768
    const/4 v1, 0x1

    goto :goto_0

    .line 1771
    .end local v0    # "UsageStatus":Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;
    :cond_1
    sget-boolean v3, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v3, :cond_2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getSBUsageEnabled() is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 1772
    :cond_2
    return v1
.end method

.method private static getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;
    .locals 4
    .param p0, "tr"    # Ljava/lang/Throwable;

    .prologue
    .line 2820
    if-nez p0, :cond_0

    .line 2821
    const-string v3, ""

    .line 2837
    :goto_0
    return-object v3

    .line 2826
    :cond_0
    move-object v2, p0

    .line 2827
    .local v2, "t":Ljava/lang/Throwable;
    :goto_1
    if-eqz v2, :cond_2

    .line 2828
    instance-of v3, v2, Ljava/net/UnknownHostException;

    if-eqz v3, :cond_1

    .line 2829
    const-string v3, ""

    goto :goto_0

    .line 2831
    :cond_1
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    goto :goto_1

    .line 2834
    :cond_2
    new-instance v1, Ljava/io/StringWriter;

    invoke-direct {v1}, Ljava/io/StringWriter;-><init>()V

    .line 2835
    .local v1, "sw":Ljava/io/StringWriter;
    new-instance v0, Ljava/io/PrintWriter;

    invoke-direct {v0, v1}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    .line 2836
    .local v0, "pw":Ljava/io/PrintWriter;
    invoke-virtual {p0, v0}, Ljava/lang/Throwable;->printStackTrace(Ljava/io/PrintWriter;)V

    .line 2837
    invoke-virtual {v1}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method private static getTrafficMonitorController(Landroid/content/Context;)Lcom/samsung/android/smartbonding/TrafficMonitorController;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2844
    sget-object v0, Lcom/samsung/android/smartbonding/SmartBondingService;->mTrafficMonitorController:Lcom/samsung/android/smartbonding/TrafficMonitorController;

    if-nez v0, :cond_0

    .line 2845
    new-instance v0, Lcom/samsung/android/smartbonding/TrafficMonitorController;

    invoke-direct {v0, p0}, Lcom/samsung/android/smartbonding/TrafficMonitorController;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/android/smartbonding/SmartBondingService;->mTrafficMonitorController:Lcom/samsung/android/smartbonding/TrafficMonitorController;

    .line 2847
    :cond_0
    sget-object v0, Lcom/samsung/android/smartbonding/SmartBondingService;->mTrafficMonitorController:Lcom/samsung/android/smartbonding/TrafficMonitorController;

    return-object v0
.end method

.method private initBlockedPackages()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 1892
    const-string v5, "NA"

    sget-object v6, Lcom/samsung/android/smartbonding/SmartBondingService;->CscFeatureConfigSmartBonding:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 1893
    sget-object v5, Lcom/samsung/android/smartbonding/SmartBondingService;->CscFeatureConfigSmartBonding:Ljava/lang/String;

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 1894
    .local v4, "listApps":[Ljava/lang/String;
    move-object v0, v4

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_4

    aget-object v1, v0, v2

    .line 1895
    .local v1, "curList":Ljava/lang/String;
    sget-boolean v5, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v5, :cond_0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "initBlockedPackages: Read Package Form Feature= "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 1896
    :cond_0
    const-string v5, "+"

    invoke-virtual {v1, v8, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1898
    sget-boolean v5, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v5, :cond_1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Add White List Form Feature : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 1899
    :cond_1
    iget-object v5, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mWhiteListPackages:Ljava/util/ArrayList;

    invoke-virtual {v1, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1894
    :cond_2
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1900
    :cond_3
    const-string v5, "-"

    invoke-virtual {v1, v8, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1902
    iget-object v5, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mBlockedPackages:Ljava/util/ArrayList;

    invoke-virtual {v1, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1903
    sget-boolean v5, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v5, :cond_2

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Add Block List Form Feature : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    goto :goto_1

    .line 1908
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "curList":Ljava/lang/String;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v4    # "listApps":[Ljava/lang/String;
    :cond_4
    iget-object v5, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mWhiteListPackages:Ljava/util/ArrayList;

    if-eqz v5, :cond_6

    .line 1909
    sget-boolean v5, Lcom/samsung/android/smartbonding/SmartBondingService;->IsDCM:Z

    if-eqz v5, :cond_6

    .line 1910
    sget-boolean v5, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v5, :cond_5

    const-string v5, "initBlockedPackages: add DCM WhiteList Package"

    invoke-static {v5}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 1911
    :cond_5
    iget-object v5, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mWhiteListPackages:Ljava/util/ArrayList;

    const-string v6, "com.nttdocomo.android.store"

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1912
    iget-object v5, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mWhiteListPackages:Ljava/util/ArrayList;

    const-string v6, "com.nttdocomo.android.schedulememo"

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1913
    iget-object v5, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mWhiteListPackages:Ljava/util/ArrayList;

    const-string v6, "com.nttdocomo.android.dhome"

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1917
    :cond_6
    iget-object v5, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mBlockedPackages:Ljava/util/ArrayList;

    if-eqz v5, :cond_b

    .line 1925
    sget-boolean v5, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v5, :cond_7

    const-string v5, "initBlockedPackages: add Black List packages"

    invoke-static {v5}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 1933
    :cond_7
    iget-object v5, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mBlockedPackages:Ljava/util/ArrayList;

    const-string v6, "org.zwanoo.android.speedtest"

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1934
    iget-object v5, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mBlockedPackages:Ljava/util/ArrayList;

    const-string v6, "pl.speedtest.android"

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1935
    iget-object v5, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mBlockedPackages:Ljava/util/ArrayList;

    const-string v6, "com.kbudev.speedtest"

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1936
    iget-object v5, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mBlockedPackages:Ljava/util/ArrayList;

    const-string v6, "ru.qip.speedtest"

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1937
    iget-object v5, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mBlockedPackages:Ljava/util/ArrayList;

    const-string v6, "eu.vspeed.android"

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1938
    iget-object v5, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mBlockedPackages:Ljava/util/ArrayList;

    const-string v6, "com.quickbird.speedtestmaster"

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1939
    iget-object v5, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mBlockedPackages:Ljava/util/ArrayList;

    const-string v6, "com.quickbird.speedtest"

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1940
    iget-object v5, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mBlockedPackages:Ljava/util/ArrayList;

    const-string v6, "com.quickbird.speed"

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1941
    iget-object v5, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mBlockedPackages:Ljava/util/ArrayList;

    const-string v6, "com.krnet.LteTracker"

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1942
    iget-object v5, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mBlockedPackages:Ljava/util/ArrayList;

    const-string v6, "com.sensorly.viewer"

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1946
    sget-boolean v5, Lcom/samsung/android/smartbonding/SmartBondingService;->IsDCM:Z

    if-eqz v5, :cond_9

    .line 1947
    sget-boolean v5, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v5, :cond_8

    const-string v5, "initBlockedPackages: add DCM Black List Package"

    invoke-static {v5}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 1948
    :cond_8
    iget-object v5, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mBlockedPackages:Ljava/util/ArrayList;

    const-string v6, "com.threelm.dm.app.docomo"

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1949
    iget-object v5, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mBlockedPackages:Ljava/util/ArrayList;

    const-string v6, "com.mcafee.android.scanservice"

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1950
    iget-object v5, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mBlockedPackages:Ljava/util/ArrayList;

    const-string v6, "com.rsupport.rsperm.ntt"

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1951
    iget-object v5, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mBlockedPackages:Ljava/util/ArrayList;

    const-string v6, "jp.co.omronsoft.android.decoemojimanager_docomo"

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1952
    iget-object v5, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mBlockedPackages:Ljava/util/ArrayList;

    const-string v6, "jp.ne.docomo.smt.contents_search_widget"

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1953
    iget-object v5, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mBlockedPackages:Ljava/util/ArrayList;

    const-string v6, "jp.id_credit_sp.android"

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1954
    iget-object v5, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mBlockedPackages:Ljava/util/ArrayList;

    const-string v6, "com.felicanetworks.mfs.addon.uicc.d"

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1955
    iget-object v5, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mBlockedPackages:Ljava/util/ArrayList;

    const-string v6, "com.nextbit.app"

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1956
    iget-object v5, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mBlockedPackages:Ljava/util/ArrayList;

    const-string v6, "jp.co.nttdocomo.mailtranslationwifi"

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1957
    iget-object v5, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mBlockedPackages:Ljava/util/ArrayList;

    const-string v6, "com.android.contacts"

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1958
    iget-object v5, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mBlockedPackages:Ljava/util/ArrayList;

    const-string v6, "com.android.dialer"

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1959
    iget-object v5, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mBlockedPackages:Ljava/util/ArrayList;

    const-string v6, "com.dcm_gate.premierclub"

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1960
    iget-object v5, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mBlockedPackages:Ljava/util/ArrayList;

    const-string v6, "com.felicanetworks.mfs.addon.uicc.d"

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1961
    iget-object v5, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mBlockedPackages:Ljava/util/ArrayList;

    const-string v6, "com.mcafee.android.scanservice"

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1962
    iget-object v5, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mBlockedPackages:Ljava/util/ArrayList;

    const-string v6, "com.nttdocomo.communicase.carriermail"

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1963
    iget-object v5, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mBlockedPackages:Ljava/util/ArrayList;

    const-string v6, "com.nttdocomo.mmb.android.mmbsv.process"

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1964
    iget-object v5, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mBlockedPackages:Ljava/util/ArrayList;

    const-string v6, "com.nttdocomo.osaifu.tsmproxy"

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1965
    iget-object v5, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mBlockedPackages:Ljava/util/ArrayList;

    const-string v6, "com.nttdocomo.shoplat"

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1966
    iget-object v5, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mBlockedPackages:Ljava/util/ArrayList;

    const-string v6, "com.rsupport.rsperm.ntt"

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1967
    iget-object v5, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mBlockedPackages:Ljava/util/ArrayList;

    const-string v6, "com.threelm.dm.app.docomo"

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1968
    iget-object v5, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mBlockedPackages:Ljava/util/ArrayList;

    const-string v6, "jp.co.ntt_q.q_book.docomo.catalog"

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1969
    iget-object v5, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mBlockedPackages:Ljava/util/ArrayList;

    const-string v6, "jp.co.omronsoft.android.decoemojimanager_docomo"

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1970
    iget-object v5, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mBlockedPackages:Ljava/util/ArrayList;

    const-string v6, "jp.id_credit_sp.android"

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1971
    iget-object v5, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mBlockedPackages:Ljava/util/ArrayList;

    const-string v6, "jp.ne.docomo.smt.contents_search_widget"

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1973
    :cond_9
    sget-boolean v5, Lcom/samsung/android/smartbonding/SmartBondingService;->IsKOR:Z

    if-eqz v5, :cond_b

    .line 1974
    sget-boolean v5, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v5, :cond_a

    const-string v5, "initBlockedPackages: add KOR Black List Package"

    invoke-static {v5}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 1975
    :cond_a
    iget-object v5, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mBlockedPackages:Ljava/util/ArrayList;

    const-string v6, "com.uplus.onphone"

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1978
    :cond_b
    return-void
.end method

.method private initSpeedRatio()V
    .locals 2

    .prologue
    .line 1235
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mFasterInterface:I

    .line 1236
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    iput-wide v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSpeedRatio:D

    .line 1237
    return-void
.end method

.method private isAirPlaneMode()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1373
    iget-object v2, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "airplane_mode_on"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 1377
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private isBlockedProcess(I)Z
    .locals 6
    .param p1, "pid"    # I

    .prologue
    .line 1996
    const/4 v3, 0x0

    .line 1998
    .local v3, "result":Z
    invoke-direct {p0, p1}, Lcom/samsung/android/smartbonding/SmartBondingService;->getPackageName(I)Ljava/lang/String;

    move-result-object v2

    .line 2000
    .local v2, "pkgName":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mWhiteListPackages:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-eqz v4, :cond_2

    .line 2001
    sget-boolean v4, Lcom/samsung/android/smartbonding/SmartBondingService;->IsDCM:Z

    if-eqz v4, :cond_7

    .line 2002
    const-string v4, "com.nttdocomo.android"

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "jp.co.nttdocomo"

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2003
    :cond_0
    iget-object v4, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mWhiteListPackages:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 2004
    sget-boolean v4, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v4, :cond_1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "This package is Unblocked Docomo App : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 2005
    :cond_1
    const/4 v3, 0x0

    .line 2022
    :cond_2
    :goto_0
    iget-object v4, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mBlockedPackages:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2023
    .local v0, "blockedPkg":Ljava/lang/String;
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2024
    sget-boolean v4, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v4, :cond_4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "This package is blocked list : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 2025
    :cond_4
    const/4 v3, 0x1

    goto :goto_1

    .line 2007
    .end local v0    # "blockedPkg":Ljava/lang/String;
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_5
    sget-boolean v4, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v4, :cond_6

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "This package is Blocked Docomo App : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 2008
    :cond_6
    const/4 v3, 0x1

    goto :goto_0

    .line 2012
    :cond_7
    iget-object v4, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mWhiteListPackages:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 2013
    sget-boolean v4, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v4, :cond_8

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "This package is included in white list"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 2014
    :cond_8
    const/4 v3, 0x0

    goto :goto_0

    .line 2016
    :cond_9
    sget-boolean v4, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v4, :cond_a

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "This package is not included in white list : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 2017
    :cond_a
    const/4 v3, 0x1

    goto/16 :goto_0

    .line 2028
    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_b
    return v3
.end method

.method private isFirstCombinedRequest(Ljava/lang/String;)Z
    .locals 5
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    .line 1290
    invoke-direct {p0, p1}, Lcom/samsung/android/smartbonding/SmartBondingService;->isGooglePlayContent(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1291
    invoke-direct {p0, p1}, Lcom/samsung/android/smartbonding/SmartBondingService;->isGooglePlayFirstContent(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1304
    :cond_0
    :goto_0
    return v2

    .line 1295
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSBUrlStatus:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUrlStatus;

    .line 1296
    .local v1, "urlStatus":Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUrlStatus;
    iget-object v3, v1, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUrlStatus;->mUrl:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/samsung/android/smartbonding/SmartBondingService;->convertCombinedUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1297
    iget-object v3, v1, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUrlStatus;->mStartUrl:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/samsung/android/smartbonding/SmartBondingService;->convertDetailUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1300
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private isGooglePlayContent(Ljava/lang/String;)Z
    .locals 1
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 1324
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    const-string v0, "/market/GetBinary/GetBinary"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1325
    const/4 v0, 0x1

    .line 1327
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isGooglePlayFirstContent(Ljava/lang/String;)Z
    .locals 4
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 1308
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_2

    .line 1309
    const-string v2, "\\?"

    invoke-virtual {p1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 1311
    .local v0, "urls":[Ljava/lang/String;
    aget-object v2, v0, v1

    if-eqz v2, :cond_2

    aget-object v2, v0, v1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_2

    aget-object v2, v0, v1

    const-string v3, "/market/GetBinary/GetBinary"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1313
    aget-object v2, v0, v1

    const-string v3, "/o"

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1314
    sget-boolean v1, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v1, :cond_0

    const-string v1, "isGooglePlayFirstContent : it\'s first content"

    invoke-static {v1}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 1315
    :cond_0
    const/4 v1, 0x1

    .line 1320
    .end local v0    # "urls":[Ljava/lang/String;
    :cond_1
    :goto_0
    return v1

    .line 1319
    :cond_2
    sget-boolean v2, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v2, :cond_1

    const-string v2, "isGooglePlayFirstContent : it\'s not a first content"

    invoke-static {v2}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private isLightTheme()Z
    .locals 1

    .prologue
    .line 642
    const/4 v0, 0x0

    return v0
.end method

.method private isMidHighDebugLevel()Z
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 646
    const-string v5, "ro.debug_level"

    invoke-static {v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 648
    .local v0, "debugLevel":Ljava/lang/String;
    const-string v5, "0x494d"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    const-string v5, "0x4948"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    :cond_0
    move v3, v4

    .line 667
    :cond_1
    :goto_0
    return v3

    .line 650
    :cond_2
    const-string v5, "0x4f4c"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 653
    new-instance v1, Ljava/io/File;

    const-string v5, "/sys/devices/virtual/misc/level/control"

    invoke-direct {v1, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 654
    .local v1, "file1":Ljava/io/File;
    new-instance v2, Ljava/io/File;

    const-string v5, "/mnt/.lfs/debug_level.inf"

    invoke-direct {v2, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 656
    .local v2, "file2":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 657
    const-string v5, "/sys/devices/virtual/misc/level/control"

    invoke-direct {p0, v5}, Lcom/samsung/android/smartbonding/SmartBondingService;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 658
    const-string v5, "0xB0B0"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    const-string v5, "0xC0C0"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    :cond_3
    move v3, v4

    .line 659
    goto :goto_0

    .line 661
    :cond_4
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 662
    const-string v5, "/mnt/.lfs/debug_level.inf"

    invoke-direct {p0, v5}, Lcom/samsung/android/smartbonding/SmartBondingService;->readOneLine(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 663
    const-string v5, "DMID"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_5

    const-string v5, "DHIG"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    :cond_5
    move v3, v4

    .line 664
    goto :goto_0
.end method

.method private isPermissionAllowed(II)Z
    .locals 3
    .param p1, "pid"    # I
    .param p2, "uid"    # I

    .prologue
    .line 1981
    invoke-direct {p0, p1}, Lcom/samsung/android/smartbonding/SmartBondingService;->getProcessName(I)Ljava/lang/String;

    move-result-object v0

    .line 1983
    .local v0, "processName":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1984
    const-string v1, "mediaserver"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0x3f5

    if-eq p2, v1, :cond_1

    :cond_0
    const-string v1, "com.google.android.youtube"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1987
    :cond_1
    sget-boolean v1, Lcom/samsung/android/smartbonding/SmartBondingService;->VDBG:Z

    if-eqz v1, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isPermissionAllowed is TRUE for processName: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " and UID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 1988
    :cond_2
    const/4 v1, 0x1

    .line 1992
    :goto_0
    return v1

    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private isRunningProcess(I)Z
    .locals 9
    .param p1, "pid"    # I

    .prologue
    .line 2070
    const/4 v4, 0x0

    .line 2072
    .local v4, "result":Z
    iget-object v7, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mContext:Landroid/content/Context;

    iget-object v8, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mContext:Landroid/content/Context;

    const-string v8, "activity"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 2073
    .local v0, "am":Landroid/app/ActivityManager;
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v3

    .line 2074
    .local v3, "raInfos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
    const/16 v7, 0x3e8

    invoke-virtual {v0, v7}, Landroid/app/ActivityManager;->getRunningServices(I)Ljava/util/List;

    move-result-object v6

    .line 2076
    .local v6, "rsInfos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningServiceInfo;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 2077
    .local v2, "raInfo":Landroid/app/ActivityManager$RunningAppProcessInfo;
    iget-object v7, v2, Landroid/app/ActivityManager$RunningAppProcessInfo;->pkgList:[Ljava/lang/String;

    array-length v7, v7

    if-eqz v7, :cond_0

    .line 2080
    if-eqz p1, :cond_0

    iget v7, v2, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    if-ne v7, p1, :cond_0

    .line 2081
    const/4 v4, 0x1

    goto :goto_0

    .line 2085
    .end local v2    # "raInfo":Landroid/app/ActivityManager$RunningAppProcessInfo;
    :cond_1
    if-nez v4, :cond_3

    .line 2086
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/ActivityManager$RunningServiceInfo;

    .line 2087
    .local v5, "rsInfo":Landroid/app/ActivityManager$RunningServiceInfo;
    if-eqz p1, :cond_2

    iget v7, v5, Landroid/app/ActivityManager$RunningServiceInfo;->pid:I

    if-ne v7, p1, :cond_2

    .line 2088
    const/4 v4, 0x1

    goto :goto_1

    .line 2093
    .end local v5    # "rsInfo":Landroid/app/ActivityManager$RunningServiceInfo;
    :cond_3
    return v4
.end method

.method private isRunningProcess(Ljava/lang/String;)Z
    .locals 7
    .param p1, "pkgName"    # Ljava/lang/String;

    .prologue
    .line 2053
    const/4 v4, 0x0

    .line 2055
    .local v4, "result":Z
    iget-object v5, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mContext:Landroid/content/Context;

    const-string v6, "activity"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 2056
    .local v0, "am":Landroid/app/ActivityManager;
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v3

    .line 2058
    .local v3, "raInfos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 2059
    .local v2, "raInfo":Landroid/app/ActivityManager$RunningAppProcessInfo;
    iget-object v5, v2, Landroid/app/ActivityManager$RunningAppProcessInfo;->pkgList:[Ljava/lang/String;

    array-length v5, v5

    if-eqz v5, :cond_0

    .line 2062
    iget-object v5, v2, Landroid/app/ActivityManager$RunningAppProcessInfo;->pkgList:[Ljava/lang/String;

    const/4 v6, 0x0

    aget-object v5, v5, v6

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2063
    const/4 v4, 0x1

    goto :goto_0

    .line 2066
    .end local v2    # "raInfo":Landroid/app/ActivityManager$RunningAppProcessInfo;
    :cond_1
    return v4
.end method

.method private isYouTubeContent(Ljava/lang/String;)Z
    .locals 1
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 1366
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    const-string v0, "/videoplayback?"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1367
    const/4 v0, 0x1

    .line 1369
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private loadPreference()V
    .locals 2

    .prologue
    .line 2778
    sget-boolean v0, Lcom/samsung/android/smartbonding/SmartBondingService;->VDBG:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "load Preference :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mHttpLogEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 2779
    :cond_0
    const-string v0, "persist.sys.sb.log.enabled"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mHttpLogEnabled:Z

    .line 2780
    return-void
.end method

.method private static log(Ljava/lang/String;)V
    .locals 1
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 2804
    const-string v0, "SmartBondingService"

    invoke-static {v0, p0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2805
    return-void
.end method

.method private static log(Ljava/lang/Throwable;)V
    .locals 2
    .param p0, "e"    # Ljava/lang/Throwable;

    .prologue
    .line 2812
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p0}, Lcom/samsung/android/smartbonding/SmartBondingService;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->loge(Ljava/lang/String;)V

    .line 2813
    return-void
.end method

.method private static loge(Ljava/lang/String;)V
    .locals 1
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 2808
    const-string v0, "SmartBondingService"

    invoke-static {v0, p0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2809
    return-void
.end method

.method private readOneLine(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "filepath"    # Ljava/lang/String;

    .prologue
    .line 672
    const-string v3, ""

    .line 673
    .local v3, "result":Ljava/lang/String;
    const/4 v0, 0x0

    .line 676
    .local v0, "buf":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/FileReader;

    invoke-direct {v4, p1}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    const/16 v5, 0x1fa0

    invoke-direct {v1, v4, v5}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 677
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .local v1, "buf":Ljava/io/BufferedReader;
    if-eqz v1, :cond_0

    .line 678
    :try_start_1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    .line 679
    if-eqz v3, :cond_0

    .line 680
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v3

    .line 686
    :cond_0
    if-eqz v1, :cond_4

    .line 688
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v0, v1

    .line 695
    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    :cond_1
    :goto_0
    if-nez v3, :cond_2

    .line 696
    const-string v3, ""

    .line 698
    .end local v3    # "result":Ljava/lang/String;
    :cond_2
    return-object v3

    .line 689
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v3    # "result":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 690
    .local v2, "e":Ljava/io/IOException;
    const-string v4, "SmartBondingService"

    const-string v5, "IOException"

    invoke-static {v4, v5}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 691
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    move-object v0, v1

    .line 692
    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_0

    .line 682
    .end local v2    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v2

    .line 683
    .restart local v2    # "e":Ljava/io/IOException;
    :goto_1
    :try_start_3
    const-string v4, "SmartBondingService"

    const-string v5, "IOException"

    invoke-static {v4, v5}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 684
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 686
    if-eqz v0, :cond_1

    .line 688
    :try_start_4
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 689
    :catch_2
    move-exception v2

    .line 690
    const-string v4, "SmartBondingService"

    const-string v5, "IOException"

    invoke-static {v4, v5}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 691
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 686
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    :goto_2
    if-eqz v0, :cond_3

    .line 688
    :try_start_5
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 692
    :cond_3
    :goto_3
    throw v4

    .line 689
    :catch_3
    move-exception v2

    .line 690
    .restart local v2    # "e":Ljava/io/IOException;
    const-string v5, "SmartBondingService"

    const-string v6, "IOException"

    invoke-static {v5, v6}, Landroid/util/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    .line 691
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 686
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v4

    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_2

    .line 682
    .end local v0    # "buf":Ljava/io/BufferedReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    :catch_4
    move-exception v2

    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_1

    .end local v0    # "buf":Ljava/io/BufferedReader;
    .restart local v1    # "buf":Ljava/io/BufferedReader;
    :cond_4
    move-object v0, v1

    .end local v1    # "buf":Ljava/io/BufferedReader;
    .restart local v0    # "buf":Ljava/io/BufferedReader;
    goto :goto_0
.end method

.method private registerPhoneStateListener(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 2517
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mTm:Landroid/telephony/TelephonyManager;

    if-nez v0, :cond_0

    .line 2518
    const-string v0, "phone"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mTm:Landroid/telephony/TelephonyManager;

    .line 2520
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mTm:Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 2521
    return-void
.end method

.method private removeSBUsage(J)V
    .locals 9
    .param p1, "threadID"    # J

    .prologue
    .line 1738
    const/4 v2, 0x0

    .line 1739
    .local v2, "removed":Z
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1741
    .local v1, "mRemoveSBUsageStatus":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;>;"
    const-string v3, ""

    .line 1742
    .local v3, "removedUrl":Ljava/lang/String;
    iget-object v5, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSBUsageStatus:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;

    .line 1743
    .local v4, "usageStatus":Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;
    iget-wide v6, v4, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;->mThreadID:J

    cmp-long v5, v6, p1

    if-nez v5, :cond_0

    .line 1744
    iget-object v3, v4, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;->mUrl:Ljava/lang/String;

    .line 1745
    sget-boolean v5, Lcom/samsung/android/smartbonding/SmartBondingService;->VDBG:Z

    if-eqz v5, :cond_1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "removeSBUsage : url "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " is founded. add it in remove usage list"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 1750
    .end local v4    # "usageStatus":Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;
    :cond_1
    iget-object v5, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSBUsageStatus:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;

    .line 1751
    .restart local v4    # "usageStatus":Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;
    iget-object v5, v4, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;->mUrl:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1752
    sget-boolean v5, Lcom/samsung/android/smartbonding/SmartBondingService;->VDBG:Z

    if-eqz v5, :cond_3

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "removeSBUsage : thread id "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, v4, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;->mThreadID:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " is added in remove list."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 1753
    :cond_3
    const/4 v2, 0x1

    .line 1754
    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1758
    .end local v4    # "usageStatus":Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;
    :cond_4
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;

    .line 1759
    .restart local v4    # "usageStatus":Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;
    sget-boolean v5, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v5, :cond_5

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "removeSBUsage : thread id "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-wide v6, v4, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;->mThreadID:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " is removed"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 1760
    :cond_5
    iget-object v5, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSBUsageStatus:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1762
    .end local v4    # "usageStatus":Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;
    :cond_6
    return-void
.end method

.method private sendSBStartIntent()V
    .locals 3

    .prologue
    .line 2309
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.START_NETWORK_BOOSTER"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2310
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "showtoast"

    iget-boolean v2, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mNeedShowTrafficToast:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2311
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2312
    iget-object v1, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 2314
    iget-object v1, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/android/smartbonding/SmartBondingService;->getTrafficMonitorController(Landroid/content/Context;)Lcom/samsung/android/smartbonding/TrafficMonitorController;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/smartbonding/TrafficMonitorController;->isShowing()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2315
    const-string v1, "SmartBondingService"

    const-string v2, "start monitor"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2316
    iget-object v1, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/android/smartbonding/SmartBondingService;->getTrafficMonitorController(Landroid/content/Context;)Lcom/samsung/android/smartbonding/TrafficMonitorController;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/samsung/android/smartbonding/TrafficMonitorController;->startMonitor(Z)V

    .line 2318
    :cond_0
    return-void
.end method

.method private sendSBStopIntent()V
    .locals 2

    .prologue
    .line 2321
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.STOP_NETWORK_BOOSTER"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2322
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2323
    iget-object v1, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 2325
    iget-object v1, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/android/smartbonding/SmartBondingService;->getTrafficMonitorController(Landroid/content/Context;)Lcom/samsung/android/smartbonding/TrafficMonitorController;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/smartbonding/TrafficMonitorController;->stopMonitor()V

    .line 2326
    return-void
.end method

.method private sendSBVzwStateChangedIntent(IJJ)V
    .locals 4
    .param p1, "status"    # I
    .param p2, "wifiUsage"    # J
    .param p4, "mobileUsage"    # J

    .prologue
    .line 2355
    const-string v1, "VZW-CDMA"

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2356
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.RI_Aggregation_state_changed"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2357
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "status"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2358
    const-string v1, "totalUsage"

    add-long v2, p2, p4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 2359
    const-string v1, "wifiUsage"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 2360
    const-string v1, "mobileUsage"

    invoke-virtual {v0, v1, p4, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 2361
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2362
    iget-object v1, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 2363
    sget-boolean v1, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Notify SB Status change: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 2365
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method private sendStateChangedIntent(JLjava/lang/String;Ljava/lang/String;IIJJ)V
    .locals 3
    .param p1, "tid"    # J
    .param p3, "appName"    # Ljava/lang/String;
    .param p4, "url"    # Ljava/lang/String;
    .param p5, "status"    # I
    .param p6, "reason"    # I
    .param p7, "wifiUsage"    # J
    .param p9, "mobileUsage"    # J

    .prologue
    .line 2330
    iput p6, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mLastErrorCause:I

    .line 2333
    sget-boolean v1, Lcom/samsung/android/smartbonding/SmartBondingService;->SHIP_BUILD:Z

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mHttpLogEnabled:Z

    if-nez v1, :cond_1

    .line 2352
    :cond_0
    :goto_0
    return-void

    .line 2337
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SB_STATE_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2338
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "tid"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 2339
    sget-boolean v1, Lcom/samsung/android/smartbonding/SmartBondingService;->SHIP_BUILD:Z

    if-nez v1, :cond_2

    .line 2340
    const-string v1, "appname"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2341
    const-string v1, "url"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2343
    :cond_2
    const-string v1, "status"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2344
    const-string v1, "reason"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2345
    sget-boolean v1, Lcom/samsung/android/smartbonding/SmartBondingService;->SHIP_BUILD:Z

    if-nez v1, :cond_3

    .line 2346
    const-string v1, "wifiUsage"

    invoke-virtual {v0, v1, p7, p8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 2347
    const-string v1, "mobileUsage"

    invoke-virtual {v0, v1, p9, p10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 2349
    :cond_3
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2350
    iget-object v1, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 2351
    sget-boolean v1, Lcom/samsung/android/smartbonding/SmartBondingService;->VDBG:Z

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "send state changed intent : status "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setMobileDataEnabled(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 928
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mTm:Landroid/telephony/TelephonyManager;

    if-eqz v0, :cond_0

    .line 930
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mTm:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0, p1}, Landroid/telephony/TelephonyManager;->setDataEnabled(Z)V

    .line 935
    :cond_0
    return-void
.end method

.method private setSBInterfacesEnabled(Z)I
    .locals 4
    .param p1, "enabled"    # Z

    .prologue
    .line 802
    iget-boolean v1, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSBInterfacesEnabled:Z

    if-ne v1, p1, :cond_2

    .line 803
    sget-boolean v1, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unable to setSBInterfacesEnabled : already "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-eqz p1, :cond_1

    const-string v1, "enabled"

    :goto_0
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 804
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/smartbonding/SmartBondingService;->getSBInterfaceState()I

    move-result v1

    .line 825
    :goto_1
    return v1

    .line 803
    :cond_1
    const-string v1, "disabled"

    goto :goto_0

    .line 807
    :cond_2
    monitor-enter p0

    .line 808
    :try_start_0
    iput-boolean p1, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSBInterfacesEnabled:Z

    .line 809
    sget-boolean v1, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v1, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setSBInterfacesEnabled ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSBInterfacesEnabled:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 810
    :cond_3
    if-eqz p1, :cond_4

    .line 811
    iget-object v1, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mHandler:Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;

    const/16 v2, 0x9

    invoke-virtual {v1, v2}, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->removeMessages(I)V

    .line 812
    invoke-direct {p0}, Lcom/samsung/android/smartbonding/SmartBondingService;->turnOnMobileConnection()Z

    .line 824
    :goto_2
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 825
    invoke-virtual {p0}, Lcom/samsung/android/smartbonding/SmartBondingService;->getSBInterfaceState()I

    move-result v1

    goto :goto_1

    .line 814
    :cond_4
    :try_start_1
    iget-object v1, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mHandler:Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->removeMessages(I)V

    .line 815
    iget-boolean v1, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mIsStoppedByGooglePlay:Z

    if-eqz v1, :cond_6

    .line 816
    sget-boolean v1, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v1, :cond_5

    const-string v1, "setSBInterfacesEnabled : because it\'s google play contents. it tries to disconnect with delay"

    invoke-static {v1}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 817
    :cond_5
    iget-object v1, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mHandler:Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;

    const/16 v2, 0x9

    invoke-virtual {v1, v2}, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 818
    .local v0, "mobileMessage":Landroid/os/Message;
    iget-object v1, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mHandler:Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;

    const-wide/16 v2, 0xbb8

    invoke-virtual {v1, v0, v2, v3}, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 819
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mIsStoppedByGooglePlay:Z

    goto :goto_2

    .line 824
    .end local v0    # "mobileMessage":Landroid/os/Message;
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 821
    :cond_6
    :try_start_2
    invoke-direct {p0}, Lcom/samsung/android/smartbonding/SmartBondingService;->turnOffMobileConnection()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2
.end method

.method private showMessage(Landroid/content/Context;II)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "msgid"    # I
    .param p3, "type"    # I

    .prologue
    .line 2852
    const-string v0, ""

    .line 2853
    .local v0, "message":Ljava/lang/String;
    sget-boolean v2, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "show message : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 2855
    :cond_0
    const/4 v2, 0x2

    if-ne p2, v2, :cond_1

    .line 2856
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x104090f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2858
    :cond_1
    const/4 v2, 0x3

    if-ne p2, v2, :cond_2

    .line 2859
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x1040910

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2861
    :cond_2
    const/4 v2, 0x1

    if-ne p2, v2, :cond_3

    .line 2862
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x104090d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2864
    :cond_3
    const/4 v2, 0x4

    if-ne p2, v2, :cond_4

    .line 2865
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x1040913

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2867
    :cond_4
    const/4 v2, 0x5

    if-ne p2, v2, :cond_5

    .line 2868
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x1040914

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2870
    :cond_5
    const/4 v2, 0x6

    if-ne p2, v2, :cond_6

    .line 2871
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x104090e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2873
    :cond_6
    const/4 v2, 0x7

    if-ne p2, v2, :cond_7

    .line 2874
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x1040911

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2876
    :cond_7
    const/16 v2, 0x8

    if-ne p2, v2, :cond_8

    .line 2877
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x1040912

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2879
    :cond_8
    const/16 v2, 0x9

    if-ne p2, v2, :cond_9

    .line 2880
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x1040917

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2882
    :cond_9
    const/16 v2, 0xa

    if-ne p2, v2, :cond_a

    .line 2883
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x1040918

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2885
    :cond_a
    invoke-static {p1, v0, p3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    .line 2886
    .local v1, "toast":Landroid/widget/Toast;
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 2887
    return-void
.end method

.method private turnOffMobileConnection()Z
    .locals 4

    .prologue
    .line 859
    sget-boolean v1, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v1, :cond_0

    const-string v1, "turnOffMobileConnection"

    invoke-static {v1}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 861
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mCm:Landroid/net/ConnectivityManager;

    const/4 v2, 0x0

    const-string v3, "enableHIPRI"

    invoke-virtual {v1, v2, v3}, Landroid/net/ConnectivityManager;->stopUsingNetworkFeature(ILjava/lang/String;)I

    .line 862
    sget-boolean v1, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v1, :cond_1

    const-string v1, "turnOffMobileConnection : Call stopUsingNetworkFeature for Hipri Types "

    invoke-static {v1}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 863
    :cond_1
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mIsConnectMobileCalled:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 868
    :cond_2
    :goto_0
    const/4 v1, 0x1

    return v1

    .line 865
    :catch_0
    move-exception v0

    .line 866
    .local v0, "e":Ljava/lang/Exception;
    sget-boolean v1, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v1, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "exception turnOffMobileConnection : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private turnOnMobileConnection()Z
    .locals 8

    .prologue
    .line 829
    sget-boolean v4, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v4, :cond_0

    const-string v4, "turnOnMobileConnection"

    invoke-static {v4}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 830
    :cond_0
    const/4 v2, 0x3

    .line 831
    .local v2, "result":I
    const/4 v3, 0x0

    .line 833
    .local v3, "ret":Z
    :try_start_0
    iget-object v4, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mCm:Landroid/net/ConnectivityManager;

    const/4 v5, 0x0

    const-string v6, "enableHIPRI"

    invoke-virtual {v4, v5, v6}, Landroid/net/ConnectivityManager;->startUsingNetworkFeature(ILjava/lang/String;)I

    move-result v2

    .line 834
    sget-boolean v4, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v4, :cond_1

    const-string v4, "turnOnMobileConnection : Call startUsingNetworkFeature for Hipri Types "

    invoke-static {v4}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 835
    :cond_1
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mIsConnectMobileCalled:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 839
    :cond_2
    :goto_0
    packed-switch v2, :pswitch_data_0

    .line 855
    :goto_1
    :pswitch_0
    return v3

    .line 836
    :catch_0
    move-exception v0

    .line 837
    .local v0, "e":Ljava/lang/Exception;
    sget-boolean v4, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v4, :cond_2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "exception turnOnMobileConnection : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    goto :goto_0

    .line 842
    .end local v0    # "e":Ljava/lang/Exception;
    :pswitch_1
    iget-object v4, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mHandler:Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;

    const/4 v5, 0x3

    invoke-virtual {v4, v5}, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 843
    .local v1, "message":Landroid/os/Message;
    iget-object v4, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mHandler:Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;

    const-wide/32 v6, 0x9c40

    invoke-virtual {v4, v1, v6, v7}, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 848
    const/4 v3, 0x1

    .line 849
    goto :goto_1

    .line 839
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private updateStatisticsData(Ljava/lang/Integer;[J[J)V
    .locals 12
    .param p1, "pid"    # Ljava/lang/Integer;
    .param p2, "lens"    # [J
    .param p3, "times"    # [J

    .prologue
    .line 1202
    :try_start_0
    iget-object v7, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSBDataStatistics:Ljava/util/HashMap;

    monitor-enter v7
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 1203
    :try_start_1
    sget-boolean v6, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v6, :cond_0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "updateStatisticsData for pid="

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ", WiFi len="

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/4 v8, 0x0

    aget-wide v8, p2, v8

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ", LTE len="

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/4 v8, 0x1

    aget-wide v8, p2, v8

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ", WiFi time="

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/4 v8, 0x0

    aget-wide v8, p3, v8

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ", LTE time="

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/4 v8, 0x1

    aget-wide v8, p3, v8

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 1204
    :cond_0
    iget-object v6, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSBDataStatistics:Ljava/util/HashMap;

    invoke-virtual {v6, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [J

    .line 1205
    .local v0, "data":[J
    if-eqz v0, :cond_1

    array-length v6, v0

    const/4 v8, 0x4

    if-ge v6, v8, :cond_2

    .line 1206
    :cond_1
    const/4 v6, 0x4

    new-array v0, v6, [J

    .end local v0    # "data":[J
    fill-array-data v0, :array_0

    .line 1208
    .restart local v0    # "data":[J
    :cond_2
    const/4 v6, 0x0

    aget-wide v8, v0, v6

    const/4 v10, 0x0

    aget-wide v10, p2, v10

    add-long/2addr v8, v10

    aput-wide v8, v0, v6

    .line 1209
    const/4 v6, 0x1

    aget-wide v8, v0, v6

    const/4 v10, 0x1

    aget-wide v10, p2, v10

    add-long/2addr v8, v10

    aput-wide v8, v0, v6

    .line 1210
    const/4 v6, 0x2

    aget-wide v8, v0, v6

    const/4 v10, 0x0

    aget-wide v10, p3, v10

    add-long/2addr v8, v10

    aput-wide v8, v0, v6

    .line 1211
    const/4 v6, 0x3

    aget-wide v8, v0, v6

    const/4 v10, 0x1

    aget-wide v10, p3, v10

    add-long/2addr v8, v10

    aput-wide v8, v0, v6

    .line 1212
    iget-object v6, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSBDataStatistics:Ljava/util/HashMap;

    invoke-virtual {v6, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1213
    sget-object v6, Lcom/samsung/android/smartbonding/SmartBondingService;->INT_TOTAL:Ljava/lang/Integer;

    invoke-virtual {p1, v6}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 1214
    const/4 v6, 0x0

    aget-wide v8, p3, v6

    const-wide/16 v10, 0x0

    cmp-long v6, v8, v10

    if-lez v6, :cond_3

    const/4 v6, 0x1

    aget-wide v8, p3, v6

    const-wide/16 v10, 0x0

    cmp-long v6, v8, v10

    if-lez v6, :cond_3

    .line 1215
    const/4 v6, 0x1

    aget-wide v8, p2, v6

    long-to-double v8, v8

    const/4 v6, 0x1

    aget-wide v10, p3, v6

    long-to-double v10, v10

    div-double v2, v8, v10

    .line 1216
    .local v2, "mobileRatio":D
    const/4 v6, 0x0

    aget-wide v8, p2, v6

    long-to-double v8, v8

    const/4 v6, 0x0

    aget-wide v10, p3, v6

    long-to-double v10, v10

    div-double v4, v8, v10

    .line 1217
    .local v4, "wifiRatio":D
    cmpl-double v6, v4, v2

    if-ltz v6, :cond_5

    .line 1218
    const/4 v6, 0x0

    iput v6, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mFasterInterface:I

    .line 1219
    div-double v8, v4, v2

    iput-wide v8, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSpeedRatio:D

    .line 1226
    .end local v2    # "mobileRatio":D
    .end local v4    # "wifiRatio":D
    :cond_3
    :goto_0
    sget-boolean v6, Lcom/samsung/android/smartbonding/SmartBondingService;->VDBG:Z

    if-eqz v6, :cond_4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "now faster interface is "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v8, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mFasterInterface:I

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " speed ratio is "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-wide v8, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSpeedRatio:D

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 1228
    :cond_4
    monitor-exit v7

    .line 1232
    .end local v0    # "data":[J
    :goto_1
    return-void

    .line 1222
    .restart local v0    # "data":[J
    .restart local v2    # "mobileRatio":D
    .restart local v4    # "wifiRatio":D
    :cond_5
    const/4 v6, 0x1

    iput v6, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mFasterInterface:I

    .line 1223
    div-double v8, v2, v4

    iput-wide v8, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSpeedRatio:D

    goto :goto_0

    .line 1228
    .end local v0    # "data":[J
    .end local v2    # "mobileRatio":D
    .end local v4    # "wifiRatio":D
    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v6
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    .line 1229
    :catch_0
    move-exception v1

    .line 1230
    .local v1, "e":Ljava/lang/Throwable;
    invoke-static {v1}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/Throwable;)V

    goto :goto_1

    .line 1206
    nop

    :array_0
    .array-data 8
        0x0
        0x0
        0x0
        0x0
    .end array-data
.end method


# virtual methods
.method public getAllSmartBondingData()[J
    .locals 1

    .prologue
    .line 1168
    sget-object v0, Lcom/samsung/android/smartbonding/SmartBondingService;->INT_TOTAL:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->getSmartBondingData(I)[J

    move-result-object v0

    return-object v0
.end method

.method public getHttpLogEnabled()Z
    .locals 1

    .prologue
    .line 2800
    iget-boolean v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mHttpLogEnabled:Z

    return v0
.end method

.method public getProxyInfo(I)[Ljava/lang/String;
    .locals 8
    .param p1, "netType"    # I

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 1095
    invoke-direct {p0}, Lcom/samsung/android/smartbonding/SmartBondingService;->enforceAccessPermission()V

    .line 1096
    const/4 v0, 0x0

    .line 1097
    .local v0, "lp":Landroid/net/LinkProperties;
    if-nez p1, :cond_1

    .line 1098
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mWifiLp:Landroid/net/LinkProperties;

    .line 1102
    :goto_0
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/net/LinkProperties;->getHttpProxy()Landroid/net/ProxyInfo;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 1103
    new-array v1, v7, [Ljava/lang/String;

    invoke-virtual {v0}, Landroid/net/LinkProperties;->getHttpProxy()Landroid/net/ProxyInfo;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/ProxyInfo;->getHost()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0}, Landroid/net/LinkProperties;->getHttpProxy()Landroid/net/ProxyInfo;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/ProxyInfo;->getPort()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-virtual {v0}, Landroid/net/LinkProperties;->getHttpProxy()Landroid/net/ProxyInfo;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/ProxyInfo;->getExclusionListAsString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    .line 1104
    .local v1, "proxyinfo":[Ljava/lang/String;
    sget-boolean v2, Lcom/samsung/android/smartbonding/SmartBondingService;->VDBG:Z

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getProxyInfo : type ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-nez p1, :cond_2

    const-string v2, "WIFI"

    :goto_1
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] + proxt addr ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/net/LinkProperties;->getHttpProxy()Landroid/net/ProxyInfo;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/ProxyInfo;->getHost()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 1107
    .end local v1    # "proxyinfo":[Ljava/lang/String;
    :cond_0
    :goto_2
    return-object v1

    .line 1100
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mMobileLp:Landroid/net/LinkProperties;

    goto :goto_0

    .line 1104
    .restart local v1    # "proxyinfo":[Ljava/lang/String;
    :cond_2
    const-string v2, "MOBILE"

    goto :goto_1

    .line 1107
    .end local v1    # "proxyinfo":[Ljava/lang/String;
    :cond_3
    new-array v1, v7, [Ljava/lang/String;

    aput-object v3, v1, v4

    aput-object v3, v1, v5

    aput-object v3, v1, v6

    goto :goto_2
.end method

.method public getSBEnabled()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 599
    :try_start_0
    iget v3, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mCurrentUserId:I

    if-eqz v3, :cond_1

    iget-boolean v3, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->isKioskContainer:Z

    if-nez v3, :cond_1

    .line 600
    sget-boolean v3, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v3, :cond_0

    const-string v3, "unable to get Smart Bonding enabled : disabled for sub users"

    invoke-static {v3}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    :cond_0
    move v1, v2

    .line 612
    :goto_0
    return v1

    .line 604
    :cond_1
    monitor-enter p0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 605
    :try_start_1
    iget-object v3, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "smart_bonding"

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-ne v3, v1, :cond_3

    .line 607
    .local v1, "enabled":Z
    :goto_1
    sget-boolean v3, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v3, :cond_2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getSBEnabled() enabled ="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 608
    :cond_2
    monitor-exit p0

    goto :goto_0

    .line 609
    .end local v1    # "enabled":Z
    :catchall_0
    move-exception v3

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v3
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    .line 610
    :catch_0
    move-exception v0

    .line 611
    .local v0, "e":Ljava/lang/Throwable;
    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/Throwable;)V

    move v1, v2

    .line 612
    goto :goto_0

    .end local v0    # "e":Ljava/lang/Throwable;
    :cond_3
    move v1, v2

    .line 605
    goto :goto_1
.end method

.method public getSBInterface(I)Ljava/lang/String;
    .locals 3
    .param p1, "netType"    # I

    .prologue
    const/4 v0, 0x0

    .line 1073
    invoke-direct {p0}, Lcom/samsung/android/smartbonding/SmartBondingService;->enforceAccessPermission()V

    .line 1074
    const/4 v1, 0x0

    .line 1075
    .local v1, "lp":Landroid/net/LinkProperties;
    if-nez p1, :cond_1

    .line 1076
    iget-object v1, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mWifiLp:Landroid/net/LinkProperties;

    .line 1087
    :goto_0
    if-eqz v1, :cond_0

    .line 1088
    invoke-direct {p0, v1}, Lcom/samsung/android/smartbonding/SmartBondingService;->getLocalAddress(Landroid/net/LinkProperties;)Ljava/lang/String;

    move-result-object v0

    .line 1091
    :cond_0
    :goto_1
    return-object v0

    .line 1078
    :cond_1
    iget-boolean v2, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mIsConnectMobileCalled:Z

    if-eqz v2, :cond_2

    .line 1080
    iget-object v1, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mMobileLp:Landroid/net/LinkProperties;

    goto :goto_0

    .line 1082
    :cond_2
    sget-boolean v2, Lcom/samsung/android/smartbonding/SmartBondingService;->VDBG:Z

    if-eqz v2, :cond_0

    const-string v2, "getSBInterface : startUsingNetworkFeature is not called so it can\'t get mobile lp"

    invoke-static {v2}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public getSBInterfaceEx(II)Ljava/lang/String;
    .locals 3
    .param p1, "netType"    # I
    .param p2, "preferIpType"    # I

    .prologue
    const/4 v0, 0x0

    .line 1042
    invoke-direct {p0}, Lcom/samsung/android/smartbonding/SmartBondingService;->enforceAccessPermission()V

    .line 1043
    invoke-virtual {p0}, Lcom/samsung/android/smartbonding/SmartBondingService;->getSBEnabled()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1061
    :cond_0
    :goto_0
    return-object v0

    .line 1044
    :cond_1
    const/4 v1, 0x0

    .line 1045
    .local v1, "lp":Landroid/net/LinkProperties;
    if-nez p1, :cond_2

    .line 1046
    iget-object v1, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mWifiLp:Landroid/net/LinkProperties;

    .line 1057
    :goto_1
    if-eqz v1, :cond_0

    .line 1058
    invoke-direct {p0, v1, p2}, Lcom/samsung/android/smartbonding/SmartBondingService;->getLocalAddress(Landroid/net/LinkProperties;I)Ljava/lang/String;

    move-result-object v0

    .line 1059
    .local v0, "addr":Ljava/lang/String;
    goto :goto_0

    .line 1048
    .end local v0    # "addr":Ljava/lang/String;
    :cond_2
    iget-boolean v2, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mIsConnectMobileCalled:Z

    if-eqz v2, :cond_3

    .line 1050
    iget-object v1, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mMobileLp:Landroid/net/LinkProperties;

    goto :goto_1

    .line 1052
    :cond_3
    sget-boolean v2, Lcom/samsung/android/smartbonding/SmartBondingService;->VDBG:Z

    if-eqz v2, :cond_0

    const-string v2, "getSBInterfaceEx : startUsingNetworkFeature is not called so it can\'t get mobile lp"

    invoke-static {v2}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getSBInterfaceState()I
    .locals 7

    .prologue
    .line 885
    const/4 v1, 0x0

    .line 887
    .local v1, "mobileConnected":Z
    :try_start_0
    iget-object v5, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mCm:Landroid/net/ConnectivityManager;

    const/4 v6, 0x5

    invoke-virtual {v5, v6}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v2

    .line 888
    .local v2, "mobileNetworkInfo":Landroid/net/NetworkInfo;
    if-eqz v2, :cond_0

    .line 889
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v1

    .line 891
    :cond_0
    iget-boolean v5, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mIsConnectMobileCalled:Z

    if-nez v5, :cond_2

    .line 892
    sget-boolean v5, Lcom/samsung/android/smartbonding/SmartBondingService;->VDBG:Z

    if-eqz v5, :cond_1

    const-string v5, "getSBInterfaceState : mobile is not connected because startUsingNetworkFeature is not called"

    invoke-static {v5}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 895
    :cond_1
    const/4 v1, 0x0

    .line 901
    .end local v2    # "mobileNetworkInfo":Landroid/net/NetworkInfo;
    :cond_2
    :goto_0
    const/4 v3, 0x0

    .line 903
    .local v3, "wifiConnected":Z
    :try_start_1
    iget-object v5, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mCm:Landroid/net/ConnectivityManager;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v4

    .line 904
    .local v4, "wifiNetworkInfo":Landroid/net/NetworkInfo;
    if-eqz v4, :cond_3

    .line 905
    invoke-virtual {v4}, Landroid/net/NetworkInfo;->isConnected()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result v3

    .line 911
    .end local v4    # "wifiNetworkInfo":Landroid/net/NetworkInfo;
    :cond_3
    :goto_1
    sget-boolean v5, Lcom/samsung/android/smartbonding/SmartBondingService;->VDBG:Z

    if-eqz v5, :cond_4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getSBInterfaceState : mobile ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] wifi  ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 912
    :cond_4
    invoke-direct {p0, v1, v3}, Lcom/samsung/android/smartbonding/SmartBondingService;->convertSBInterfaceState(ZZ)I

    move-result v5

    return v5

    .line 897
    .end local v3    # "wifiConnected":Z
    :catch_0
    move-exception v0

    .line 898
    .local v0, "e":Ljava/lang/Exception;
    sget-boolean v5, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v5, :cond_2

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "exception getSBInterfaceState : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    goto :goto_0

    .line 907
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v3    # "wifiConnected":Z
    :catch_1
    move-exception v0

    .line 908
    .restart local v0    # "e":Ljava/lang/Exception;
    sget-boolean v5, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v5, :cond_3

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "exception getSBInterfaceState : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public getSBInterfaces()[Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1147
    invoke-direct {p0}, Lcom/samsung/android/smartbonding/SmartBondingService;->enforceAccessPermission()V

    .line 1149
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSBDataStatistics:Ljava/util/HashMap;

    monitor-enter v2
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 1150
    :try_start_1
    iget v1, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mFasterInterface:I

    if-nez v1, :cond_0

    .line 1151
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {p0, v4}, Lcom/samsung/android/smartbonding/SmartBondingService;->getSBInterface(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v3

    const/4 v3, 0x1

    const/4 v4, 0x1

    invoke-virtual {p0, v4}, Lcom/samsung/android/smartbonding/SmartBondingService;->getSBInterface(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v3

    monitor-exit v2

    .line 1159
    :goto_0
    return-object v1

    .line 1154
    :cond_0
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-virtual {p0, v4}, Lcom/samsung/android/smartbonding/SmartBondingService;->getSBInterface(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v3

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {p0, v4}, Lcom/samsung/android/smartbonding/SmartBondingService;->getSBInterface(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v3

    monitor-exit v2

    goto :goto_0

    .line 1156
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v1
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    .line 1158
    :catch_0
    move-exception v0

    .line 1159
    .local v0, "e":Ljava/lang/Throwable;
    new-array v1, v7, [Ljava/lang/String;

    aput-object v8, v1, v5

    aput-object v8, v1, v6

    goto :goto_0
.end method

.method public getSBInterfacesEnabled()Z
    .locals 1

    .prologue
    .line 793
    iget-boolean v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSBInterfacesEnabled:Z

    return v0
.end method

.method public getSBUsageStatus(J)I
    .locals 7
    .param p1, "threadID"    # J

    .prologue
    .line 2138
    const/4 v2, 0x0

    .line 2139
    .local v2, "status":I
    iget-object v3, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSBUsageStatus:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;

    .line 2140
    .local v0, "UsageStatus":Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;
    iget-wide v4, v0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;->mThreadID:J

    cmp-long v3, v4, p1

    if-nez v3, :cond_0

    .line 2141
    iget v2, v0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;->mStatus:I

    goto :goto_0

    .line 2144
    .end local v0    # "UsageStatus":Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;
    :cond_1
    return v2
.end method

.method public getSmartBondingData(I)[J
    .locals 8
    .param p1, "pid"    # I

    .prologue
    .line 1123
    iget-object v1, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSBDataStatistics:Ljava/util/HashMap;

    monitor-enter v1

    .line 1125
    const/4 v0, -0x1

    if-ne p1, v0, :cond_2

    .line 1126
    :try_start_0
    iget v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mFasterInterface:I

    invoke-virtual {p0, v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->getSBInterface(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1127
    const/4 v0, 0x2

    new-array v0, v0, [J

    const/4 v2, 0x0

    iget v3, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mFasterInterface:I

    int-to-long v4, v3

    aput-wide v4, v0, v2

    const/4 v2, 0x1

    iget-wide v4, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSpeedRatio:D

    const-wide/high16 v6, 0x4059000000000000L    # 100.0

    mul-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-long v4, v4

    aput-wide v4, v0, v2

    monitor-exit v1

    .line 1137
    :goto_0
    return-object v0

    .line 1129
    :cond_0
    iget v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mFasterInterface:I

    add-int/lit8 v0, v0, 0x1

    rem-int/lit8 v0, v0, 0x2

    invoke-virtual {p0, v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->getSBInterface(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1130
    const/4 v0, 0x2

    new-array v0, v0, [J

    const/4 v2, 0x0

    iget v3, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mFasterInterface:I

    add-int/lit8 v3, v3, 0x1

    rem-int/lit8 v3, v3, 0x2

    int-to-long v4, v3

    aput-wide v4, v0, v2

    const/4 v2, 0x1

    const-wide/16 v4, 0x64

    aput-wide v4, v0, v2

    monitor-exit v1

    goto :goto_0

    .line 1139
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1133
    :cond_1
    const/4 v0, 0x2

    :try_start_1
    new-array v0, v0, [J

    fill-array-data v0, :array_0

    monitor-exit v1

    goto :goto_0

    .line 1137
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSBDataStatistics:Ljava/util/HashMap;

    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, p1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [J

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1133
    :array_0
    .array-data 8
        -0x1
        0x1
    .end array-data
.end method

.method public getWarningToastEnabled()Z
    .locals 3

    .prologue
    .line 2794
    sget-boolean v1, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v1, :cond_0

    const-string v1, "getWarningToastEnabled called :"

    invoke-static {v1}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 2795
    :cond_0
    const-string v1, "persist.sys.sb.warning.show"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 2796
    .local v0, "enable":Z
    return v0
.end method

.method public reportSBUsage(J[J)V
    .locals 21
    .param p1, "threadID"    # J
    .param p3, "data"    # [J

    .prologue
    .line 1705
    sget-boolean v2, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "reportSBUsage : sb usauge is reported : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-wide/from16 v0, p1

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 1707
    :cond_0
    monitor-enter p0

    .line 1709
    const/16 v19, 0x0

    .line 1711
    .local v19, "usageStatus":Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSBUsageStatus:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v15

    .local v15, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;

    .line 1712
    .local v18, "us":Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;
    move-object/from16 v0, v18

    iget-wide v2, v0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;->mThreadID:J

    cmp-long v2, v2, p1

    if-nez v2, :cond_1

    .line 1714
    move-object/from16 v19, v18

    goto :goto_0

    .line 1717
    .end local v18    # "us":Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;
    :cond_2
    if-eqz v19, :cond_5

    .line 1718
    move-object/from16 v16, p3

    .line 1719
    .local v16, "statistic":[J
    move-object/from16 v0, v16

    move-object/from16 v1, v19

    iput-object v0, v1, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;->mStatistics:[J

    .line 1721
    move-object/from16 v0, v19

    iget-object v2, v0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;->mUrl:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/samsung/android/smartbonding/SmartBondingService;->convertCombinedUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 1724
    .local v14, "combinedUrl":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSBUrlStatus:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :cond_3
    :goto_1
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUrlStatus;

    .line 1725
    .local v17, "urlStatus":Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUrlStatus;
    move-object/from16 v0, v17

    iget-object v2, v0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUrlStatus;->mUrl:Ljava/lang/String;

    invoke-virtual {v2, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1726
    invoke-virtual/range {v17 .. v17}, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUrlStatus;->getAllLinkedStatistics()[J

    move-result-object v16

    goto :goto_1

    .line 1730
    .end local v17    # "urlStatus":Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUrlStatus;
    :cond_4
    move-object/from16 v0, v19

    iget-wide v2, v0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;->mThreadID:J

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/samsung/android/smartbonding/SmartBondingService;->removeSBUsage(J)V

    .line 1731
    move-object/from16 v0, v19

    iget v2, v0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;->mPid:I

    move-object/from16 v0, v19

    iget-object v3, v0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;->mUrl:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/samsung/android/smartbonding/SmartBondingService;->getApplicationName(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v19

    iget-object v7, v0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;->mUrl:Ljava/lang/String;

    const/4 v8, 0x3

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/android/smartbonding/SmartBondingService;->mLastErrorCause:I

    const/4 v2, 0x0

    aget-wide v10, p3, v2

    const/4 v2, 0x2

    aget-wide v12, p3, v2

    move-object/from16 v3, p0

    move-wide/from16 v4, p1

    invoke-direct/range {v3 .. v13}, Lcom/samsung/android/smartbonding/SmartBondingService;->sendStateChangedIntent(JLjava/lang/String;Ljava/lang/String;IIJJ)V

    .line 1732
    const/4 v3, 0x3

    const/4 v2, 0x0

    aget-wide v4, p3, v2

    const/4 v2, 0x2

    aget-wide v6, p3, v2

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v7}, Lcom/samsung/android/smartbonding/SmartBondingService;->sendSBVzwStateChangedIntent(IJJ)V

    .line 1734
    .end local v14    # "combinedUrl":Ljava/lang/String;
    .end local v16    # "statistic":[J
    :cond_5
    monitor-exit p0

    .line 1735
    return-void

    .line 1734
    .end local v15    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public requestGetAllByName(JLjava/lang/String;)V
    .locals 7
    .param p1, "threadID"    # J
    .param p3, "host"    # Ljava/lang/String;

    .prologue
    const/16 v6, 0x8

    .line 2209
    iget-object v3, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSBUsageStatus:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;

    .line 2210
    .local v2, "usageStatus":Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;
    iget-wide v4, v2, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;->mThreadID:J

    cmp-long v3, v4, p1

    if-nez v3, :cond_0

    .line 2211
    iput-object p3, v2, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;->mHost:Ljava/lang/String;

    .line 2212
    const/4 v3, 0x0

    iput-object v3, v2, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;->mInetAddresses:[Ljava/net/InetAddress;

    .line 2213
    iget-object v3, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mHandler:Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;

    invoke-virtual {v3, v6}, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->removeMessages(I)V

    .line 2214
    iget-object v3, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mHandler:Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;

    invoke-virtual {v3, v6}, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 2215
    .local v1, "message":Landroid/os/Message;
    iput-object v2, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 2216
    iget-object v3, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mHandler:Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;

    invoke-virtual {v3, v1}, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 2219
    .end local v1    # "message":Landroid/os/Message;
    .end local v2    # "usageStatus":Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;
    :cond_1
    return-void
.end method

.method public responseGetAllByName(J)[Ljava/lang/String;
    .locals 13
    .param p1, "threadID"    # J

    .prologue
    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    const/4 v9, 0x4

    const/4 v8, 0x0

    .line 2271
    const/4 v0, 0x0

    .line 2272
    .local v0, "addresses":[Ljava/net/InetAddress;
    iget-object v5, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSBUsageStatus:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;

    .line 2273
    .local v4, "usageStatus":Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;
    iget-wide v6, v4, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;->mThreadID:J

    cmp-long v5, v6, p1

    if-nez v5, :cond_0

    .line 2274
    iget-object v0, v4, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;->mInetAddresses:[Ljava/net/InetAddress;

    goto :goto_0

    .line 2277
    .end local v4    # "usageStatus":Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;
    :cond_1
    if-eqz v0, :cond_2

    .line 2278
    new-array v3, v9, [Ljava/lang/String;

    aput-object v8, v3, v10

    aput-object v8, v3, v11

    aput-object v8, v3, v12

    const/4 v5, 0x3

    aput-object v8, v3, v5

    .line 2279
    .local v3, "ret":[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    array-length v5, v0

    invoke-static {v5, v9}, Ljava/lang/Math;->min(II)I

    move-result v5

    if-ge v1, v5, :cond_3

    .line 2280
    aget-object v5, v0, v1

    invoke-virtual {v5}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v1

    .line 2279
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2284
    .end local v1    # "i":I
    .end local v3    # "ret":[Ljava/lang/String;
    :cond_2
    new-array v3, v9, [Ljava/lang/String;

    aput-object v8, v3, v10

    aput-object v8, v3, v11

    aput-object v8, v3, v12

    const/4 v5, 0x3

    aput-object v8, v3, v5

    :cond_3
    return-object v3
.end method

.method public setHttpLogEnabled(Z)V
    .locals 2
    .param p1, "enabled"    # Z

    .prologue
    .line 2783
    sget-boolean v0, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "setHttpLogEnabled called :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 2784
    :cond_0
    iput-boolean p1, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mHttpLogEnabled:Z

    .line 2785
    const-string v0, "persist.sys.sb.log.enabled"

    invoke-static {p1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 2786
    return-void
.end method

.method public setSBEnabled(Z)V
    .locals 4
    .param p1, "enabled"    # Z

    .prologue
    .line 576
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/smartbonding/SmartBondingService;->getSBEnabled()Z

    move-result v1

    if-ne v1, p1, :cond_2

    .line 577
    sget-boolean v1, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Smart Bonding already "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-eqz p1, :cond_1

    const-string v1, "enabled"

    :goto_0
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 586
    :cond_0
    :goto_1
    return-void

    .line 577
    :cond_1
    const-string v1, "disabled"

    goto :goto_0

    .line 581
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "smart_bonding"

    if-eqz p1, :cond_3

    const/4 v1, 0x1

    :goto_2
    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 583
    :catch_0
    move-exception v0

    .line 584
    .local v0, "e":Ljava/lang/Throwable;
    sget-boolean v1, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v1, :cond_0

    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/Throwable;)V

    goto :goto_1

    .line 581
    .end local v0    # "e":Ljava/lang/Throwable;
    :cond_3
    const/4 v1, 0x0

    goto :goto_2
.end method

.method public setSBInternalUsageStatus(IJ)V
    .locals 6
    .param p1, "status"    # I
    .param p2, "threadID"    # J

    .prologue
    .line 2196
    sget-boolean v2, Lcom/samsung/android/smartbonding/SmartBondingService;->VDBG:Z

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setSBInternalUsageStatus : status : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " threadID : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 2198
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSBUsageStatus:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;

    .line 2199
    .local v0, "UsageStatus":Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;
    iget-wide v2, v0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;->mThreadID:J

    cmp-long v2, v2, p2

    if-nez v2, :cond_1

    .line 2200
    iput p1, v0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;->mStatus:I

    goto :goto_0

    .line 2204
    .end local v0    # "UsageStatus":Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mHandler:Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;

    iget-object v3, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mHandler:Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;

    const/4 v4, 0x2

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->sendMessage(Landroid/os/Message;)Z

    .line 2205
    sget-boolean v2, Lcom/samsung/android/smartbonding/SmartBondingService;->VDBG:Z

    if-eqz v2, :cond_3

    const-string v2, "setSBInternalUsageStatus : send SB interfaces intent"

    invoke-static {v2}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 2206
    :cond_3
    return-void
.end method

.method public setSBUsageStatus(IJ)V
    .locals 18
    .param p1, "status"    # I
    .param p2, "threadID"    # J

    .prologue
    .line 2153
    sget-boolean v3, Lcom/samsung/android/smartbonding/SmartBondingService;->VDBG:Z

    if-eqz v3, :cond_0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setSBUsageStatus : status : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " threadID : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, p2

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 2155
    :cond_0
    const-string v7, ""

    .line 2156
    .local v7, "url":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSBUsageStatus:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v14

    .local v14, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;

    .line 2157
    .local v16, "usageStatus":Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;
    move-object/from16 v0, v16

    iget-wide v4, v0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;->mThreadID:J

    cmp-long v3, v4, p2

    if-nez v3, :cond_1

    .line 2158
    move/from16 v0, p1

    move-object/from16 v1, v16

    iput v0, v1, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;->mStatus:I

    .line 2159
    move-object/from16 v0, v16

    iget-object v7, v0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;->mUrl:Ljava/lang/String;

    .line 2160
    move-object/from16 v0, v16

    iget v3, v0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;->mStatus:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    .line 2161
    move-object/from16 v0, v16

    iget v3, v0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;->mPid:I

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v7}, Lcom/samsung/android/smartbonding/SmartBondingService;->getApplicationName(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const/4 v8, 0x2

    const/4 v9, 0x0

    const-wide/16 v10, 0x0

    const-wide/16 v12, 0x0

    move-object/from16 v3, p0

    move-wide/from16 v4, p2

    invoke-direct/range {v3 .. v13}, Lcom/samsung/android/smartbonding/SmartBondingService;->sendStateChangedIntent(JLjava/lang/String;Ljava/lang/String;IIJJ)V

    .line 2162
    const/4 v9, 0x2

    const-wide/16 v10, 0x0

    const-wide/16 v12, 0x0

    move-object/from16 v8, p0

    invoke-direct/range {v8 .. v13}, Lcom/samsung/android/smartbonding/SmartBondingService;->sendSBVzwStateChangedIntent(IJJ)V

    goto :goto_0

    .line 2167
    .end local v16    # "usageStatus":Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;
    :cond_2
    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/samsung/android/smartbonding/SmartBondingService;->convertCombinedUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2168
    .local v2, "combinedUrl":Ljava/lang/String;
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_5

    .line 2169
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSBUrlStatus:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :cond_3
    :goto_1
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUrlStatus;

    .line 2170
    .local v15, "urlStatus":Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUrlStatus;
    iget-object v3, v15, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUrlStatus;->mUrl:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2171
    sget-boolean v3, Lcom/samsung/android/smartbonding/SmartBondingService;->VDBG:Z

    if-eqz v3, :cond_4

    const-string v3, "setSBUsageStatus : same url is found"

    invoke-static {v3}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 2172
    :cond_4
    move/from16 v0, p1

    iput v0, v15, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUrlStatus;->mStatus:I

    goto :goto_1

    .line 2186
    .end local v15    # "urlStatus":Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUrlStatus;
    :cond_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/smartbonding/SmartBondingService;->mHandler:Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/smartbonding/SmartBondingService;->mHandler:Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;

    const/4 v5, 0x2

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->sendMessage(Landroid/os/Message;)Z

    .line 2187
    sget-boolean v3, Lcom/samsung/android/smartbonding/SmartBondingService;->VDBG:Z

    if-eqz v3, :cond_6

    const-string v3, "setSBUsageStatus : send SB interfaces enable event"

    invoke-static {v3}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 2188
    :cond_6
    return-void
.end method

.method public setWarningToast(Z)V
    .locals 2
    .param p1, "enabled"    # Z

    .prologue
    .line 2789
    sget-boolean v0, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "setWarningToast called :"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 2790
    :cond_0
    const-string v0, "persist.sys.sb.warning.show"

    invoke-static {p1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 2791
    return-void
.end method

.method public startSBUsage(JJJLjava/lang/String;)Z
    .locals 29
    .param p1, "threadID"    # J
    .param p3, "fileSize"    # J
    .param p5, "startRange"    # J
    .param p7, "url"    # Ljava/lang/String;

    .prologue
    .line 1386
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/smartbonding/SmartBondingService;->isAirPlaneMode()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1387
    sget-boolean v5, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v5, :cond_0

    const-string v5, "startSBUsage : is airplane mode"

    invoke-static {v5}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 1388
    :cond_0
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v5

    move-object/from16 v0, p0

    move-object/from16 v1, p7

    invoke-direct {v0, v5, v1}, Lcom/samsung/android/smartbonding/SmartBondingService;->getApplicationName(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const/4 v10, 0x5

    const/16 v11, 0xe

    const-wide/16 v12, 0x0

    const-wide/16 v14, 0x0

    move-object/from16 v5, p0

    move-wide/from16 v6, p1

    move-object/from16 v9, p7

    invoke-direct/range {v5 .. v15}, Lcom/samsung/android/smartbonding/SmartBondingService;->sendStateChangedIntent(JLjava/lang/String;Ljava/lang/String;IIJJ)V

    .line 1389
    const/4 v5, 0x0

    .line 1547
    :goto_0
    return v5

    .line 1392
    :cond_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/smartbonding/SmartBondingService;->mTm:Landroid/telephony/TelephonyManager;

    if-eqz v5, :cond_5

    .line 1393
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/smartbonding/SmartBondingService;->mTm:Landroid/telephony/TelephonyManager;

    invoke-virtual {v5}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_3

    .line 1394
    sget-boolean v5, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v5, :cond_2

    const-string v5, "startSBUsage : no sim"

    invoke-static {v5}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 1395
    :cond_2
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v5

    move-object/from16 v0, p0

    move-object/from16 v1, p7

    invoke-direct {v0, v5, v1}, Lcom/samsung/android/smartbonding/SmartBondingService;->getApplicationName(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const/4 v10, 0x5

    const/16 v11, 0xc

    const-wide/16 v12, 0x0

    const-wide/16 v14, 0x0

    move-object/from16 v5, p0

    move-wide/from16 v6, p1

    move-object/from16 v9, p7

    invoke-direct/range {v5 .. v15}, Lcom/samsung/android/smartbonding/SmartBondingService;->sendStateChangedIntent(JLjava/lang/String;Ljava/lang/String;IIJJ)V

    .line 1396
    const/4 v5, 0x0

    goto :goto_0

    .line 1398
    :cond_3
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/smartbonding/SmartBondingService;->mTm:Landroid/telephony/TelephonyManager;

    invoke-virtual {v5}, Landroid/telephony/TelephonyManager;->isNetworkRoaming()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 1399
    sget-boolean v5, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v5, :cond_4

    const-string v5, "startSBUsage : roaming status"

    invoke-static {v5}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 1400
    :cond_4
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v5

    move-object/from16 v0, p0

    move-object/from16 v1, p7

    invoke-direct {v0, v5, v1}, Lcom/samsung/android/smartbonding/SmartBondingService;->getApplicationName(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const/4 v10, 0x5

    const/16 v11, 0xd

    const-wide/16 v12, 0x0

    const-wide/16 v14, 0x0

    move-object/from16 v5, p0

    move-wide/from16 v6, p1

    move-object/from16 v9, p7

    invoke-direct/range {v5 .. v15}, Lcom/samsung/android/smartbonding/SmartBondingService;->sendStateChangedIntent(JLjava/lang/String;Ljava/lang/String;IIJJ)V

    .line 1401
    const/4 v5, 0x0

    goto :goto_0

    .line 1405
    :cond_5
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/samsung/android/smartbonding/SmartBondingService;->mNetworkEnabled:Z

    if-eqz v5, :cond_6

    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/smartbonding/SmartBondingService;->getMobilePolicyDataEnable()Z

    move-result v5

    if-nez v5, :cond_8

    .line 1406
    :cond_6
    sget-boolean v5, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v5, :cond_7

    const-string v5, "startSBUsage : some interface is not enabled"

    invoke-static {v5}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 1407
    :cond_7
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v5

    move-object/from16 v0, p0

    move-object/from16 v1, p7

    invoke-direct {v0, v5, v1}, Lcom/samsung/android/smartbonding/SmartBondingService;->getApplicationName(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const/4 v10, 0x5

    const/16 v11, 0xb

    const-wide/16 v12, 0x0

    const-wide/16 v14, 0x0

    move-object/from16 v5, p0

    move-wide/from16 v6, p1

    move-object/from16 v9, p7

    invoke-direct/range {v5 .. v15}, Lcom/samsung/android/smartbonding/SmartBondingService;->sendStateChangedIntent(JLjava/lang/String;Ljava/lang/String;IIJJ)V

    .line 1408
    const/4 v5, 0x0

    goto/16 :goto_0

    .line 1411
    :cond_8
    monitor-enter p0

    .line 1412
    :try_start_0
    sget-boolean v5, Lcom/samsung/android/smartbonding/SmartBondingService;->VDBG:Z

    if-eqz v5, :cond_9

    const-string v5, "startSBUsage is called"

    invoke-static {v5}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 1414
    :cond_9
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/smartbonding/SmartBondingService;->getSBEnabled()Z

    move-result v5

    if-nez v5, :cond_b

    .line 1415
    sget-boolean v5, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v5, :cond_a

    const-string v5, "startSBUsage : smart bonding is not enabled"

    invoke-static {v5}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 1416
    :cond_a
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v5

    move-object/from16 v0, p0

    move-object/from16 v1, p7

    invoke-direct {v0, v5, v1}, Lcom/samsung/android/smartbonding/SmartBondingService;->getApplicationName(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const/4 v10, 0x5

    const/16 v11, 0xa

    const-wide/16 v12, 0x0

    const-wide/16 v14, 0x0

    move-object/from16 v5, p0

    move-wide/from16 v6, p1

    move-object/from16 v9, p7

    invoke-direct/range {v5 .. v15}, Lcom/samsung/android/smartbonding/SmartBondingService;->sendStateChangedIntent(JLjava/lang/String;Ljava/lang/String;IIJJ)V

    .line 1417
    const/4 v5, 0x0

    monitor-exit p0

    goto/16 :goto_0

    .line 1548
    :catchall_0
    move-exception v5

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5

    .line 1420
    :cond_b
    :try_start_1
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v25

    .line 1421
    .local v25, "pid":I
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v26

    .line 1423
    .local v26, "uid":I
    sget-boolean v5, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v5, :cond_c

    const-string v5, "startSBUsage : check permission"

    invoke-static {v5}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 1424
    :cond_c
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/smartbonding/SmartBondingService;->mContext:Landroid/content/Context;

    const-string v6, "android.permission.ACCESS_NETWORK_STATE"

    invoke-virtual {v5, v6}, Landroid/content/Context;->checkCallingPermission(Ljava/lang/String;)I

    move-result v5

    const/4 v6, -0x1

    if-ne v5, v6, :cond_e

    .line 1425
    move-object/from16 v0, p0

    move/from16 v1, v25

    move/from16 v2, v26

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/smartbonding/SmartBondingService;->isPermissionAllowed(II)Z

    move-result v5

    if-nez v5, :cond_e

    .line 1426
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v5

    move-object/from16 v0, p0

    move-object/from16 v1, p7

    invoke-direct {v0, v5, v1}, Lcom/samsung/android/smartbonding/SmartBondingService;->getApplicationName(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const/4 v10, 0x5

    const/16 v11, 0x12

    const-wide/16 v12, 0x0

    const-wide/16 v14, 0x0

    move-object/from16 v5, p0

    move-wide/from16 v6, p1

    move-object/from16 v9, p7

    invoke-direct/range {v5 .. v15}, Lcom/samsung/android/smartbonding/SmartBondingService;->sendStateChangedIntent(JLjava/lang/String;Ljava/lang/String;IIJJ)V

    .line 1427
    sget-boolean v5, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v5, :cond_d

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "startSBUsage : no permission for this process : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-direct {v0, v1}, Lcom/samsung/android/smartbonding/SmartBondingService;->getProcessName(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " with this UID: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v26

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 1428
    :cond_d
    const/4 v5, 0x0

    monitor-exit p0

    goto/16 :goto_0

    .line 1433
    :cond_e
    move-object/from16 v0, p0

    move/from16 v1, v25

    invoke-direct {v0, v1}, Lcom/samsung/android/smartbonding/SmartBondingService;->isBlockedProcess(I)Z

    move-result v5

    if-eqz v5, :cond_10

    .line 1434
    sget-boolean v5, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v5, :cond_f

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "startSBUsage : pid : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v25

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " is blocked"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 1435
    :cond_f
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v5

    move-object/from16 v0, p0

    move-object/from16 v1, p7

    invoke-direct {v0, v5, v1}, Lcom/samsung/android/smartbonding/SmartBondingService;->getApplicationName(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const/4 v10, 0x5

    const/16 v11, 0x10

    const-wide/16 v12, 0x0

    const-wide/16 v14, 0x0

    move-object/from16 v5, p0

    move-wide/from16 v6, p1

    move-object/from16 v9, p7

    invoke-direct/range {v5 .. v15}, Lcom/samsung/android/smartbonding/SmartBondingService;->sendStateChangedIntent(JLjava/lang/String;Ljava/lang/String;IIJJ)V

    .line 1436
    const/4 v5, 0x0

    monitor-exit p0

    goto/16 :goto_0

    .line 1440
    :cond_10
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSBUsageStatus:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v22

    .local v22, "i$":Ljava/util/Iterator;
    :cond_11
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_13

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v28

    check-cast v28, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;

    .line 1441
    .local v28, "us":Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;
    move-object/from16 v0, v28

    iget-wide v6, v0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;->mThreadID:J

    cmp-long v5, v6, p1

    if-nez v5, :cond_11

    move-object/from16 v0, v28

    iget v5, v0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;->mStatus:I

    if-nez v5, :cond_11

    .line 1442
    sget-boolean v5, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v5, :cond_12

    const-string v5, "startSBUsage: sb usauge is already started"

    invoke-static {v5}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 1443
    :cond_12
    const/4 v5, 0x0

    monitor-exit p0

    goto/16 :goto_0

    .line 1447
    .end local v28    # "us":Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;
    :cond_13
    move-object/from16 v0, p0

    move-object/from16 v1, p7

    invoke-direct {v0, v1}, Lcom/samsung/android/smartbonding/SmartBondingService;->convertCombinedUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 1448
    .local v8, "combinedUrl":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, p7

    invoke-direct {v0, v1}, Lcom/samsung/android/smartbonding/SmartBondingService;->convertDetailUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 1450
    .local v9, "detailUrl":Ljava/lang/String;
    new-instance v4, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUrlStatus;

    move-object/from16 v5, p0

    move/from16 v6, v25

    move/from16 v7, v26

    invoke-direct/range {v4 .. v9}, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUrlStatus;-><init>(Lcom/samsung/android/smartbonding/SmartBondingService;IILjava/lang/String;Ljava/lang/String;)V

    .line 1451
    .local v4, "curUrlStatus":Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUrlStatus;
    new-instance v10, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;

    move-object/from16 v11, p0

    move/from16 v12, v25

    move/from16 v13, v26

    move-wide/from16 v14, p1

    move-wide/from16 v16, p3

    move-object/from16 v18, p7

    move-wide/from16 v19, p5

    invoke-direct/range {v10 .. v20}, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;-><init>(Lcom/samsung/android/smartbonding/SmartBondingService;IIJJLjava/lang/String;J)V

    .line 1454
    .local v10, "curUsageStatus":Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSBUrlStatus:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v22

    :cond_14
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1c

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUrlStatus;

    .line 1455
    .local v27, "urlStatus":Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUrlStatus;
    move-object/from16 v0, v27

    iget-object v5, v0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUrlStatus;->mUrl:Ljava/lang/String;

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_14

    .line 1456
    sget-boolean v5, Lcom/samsung/android/smartbonding/SmartBondingService;->VDBG:Z

    if-eqz v5, :cond_15

    const-string v5, "startSBUsage : same url is founded"

    invoke-static {v5}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 1457
    :cond_15
    move-object/from16 v0, v27

    iget v5, v0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUrlStatus;->mStatus:I

    const/4 v6, 0x4

    if-ne v5, v6, :cond_17

    const-wide/16 v6, 0x0

    cmp-long v5, p5, v6

    if-eqz v5, :cond_17

    .line 1458
    sget-boolean v5, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v5, :cond_16

    const-string v5, "startSBUsage : current url is not supported"

    invoke-static {v5}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 1459
    :cond_16
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v5

    move-object/from16 v0, p0

    move-object/from16 v1, p7

    invoke-direct {v0, v5, v1}, Lcom/samsung/android/smartbonding/SmartBondingService;->getApplicationName(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v14

    const/16 v16, 0x5

    const/16 v17, 0x2

    const-wide/16 v18, 0x0

    const-wide/16 v20, 0x0

    move-object/from16 v11, p0

    move-wide/from16 v12, p1

    move-object/from16 v15, p7

    invoke-direct/range {v11 .. v21}, Lcom/samsung/android/smartbonding/SmartBondingService;->sendStateChangedIntent(JLjava/lang/String;Ljava/lang/String;IIJJ)V

    .line 1460
    const/4 v5, 0x0

    monitor-exit p0

    goto/16 :goto_0

    .line 1462
    :cond_17
    const-wide/16 v6, 0x0

    cmp-long v5, p5, v6

    if-nez v5, :cond_18

    move-object/from16 v0, p0

    move-object/from16 v1, p7

    invoke-direct {v0, v1}, Lcom/samsung/android/smartbonding/SmartBondingService;->isFirstCombinedRequest(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_14

    .line 1463
    :cond_18
    move-object/from16 v0, v27

    iget v5, v0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUrlStatus;->mStatus:I

    const/4 v6, 0x2

    if-ne v5, v6, :cond_1a

    .line 1464
    sget-boolean v5, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v5, :cond_19

    const-string v5, "startSBUsage : url is already selected as no"

    invoke-static {v5}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 1466
    :cond_19
    move-object/from16 v0, v27

    iput-object v9, v0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUrlStatus;->mStartUrl:Ljava/lang/String;

    .line 1467
    const/4 v5, 0x0

    monitor-exit p0

    goto/16 :goto_0

    .line 1468
    :cond_1a
    move-object/from16 v0, v27

    iget v5, v0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUrlStatus;->mStatus:I

    const/4 v6, 0x1

    if-ne v5, v6, :cond_14

    .line 1469
    sget-boolean v5, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v5, :cond_1b

    const-string v5, "startSBUsage : url is already selected as yes"

    invoke-static {v5}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 1471
    :cond_1b
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/samsung/android/smartbonding/SmartBondingService;->mNeedShowTrafficToast:Z

    .line 1472
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSBUsageStatus:Ljava/util/ArrayList;

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1473
    move-object/from16 v0, v27

    iget-object v5, v0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUrlStatus;->mLinkedUsageStatus:Ljava/util/ArrayList;

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1474
    const/4 v5, 0x1

    move-object/from16 v0, p0

    move-wide/from16 v1, p1

    invoke-virtual {v0, v5, v1, v2}, Lcom/samsung/android/smartbonding/SmartBondingService;->setSBUsageStatus(IJ)V

    .line 1475
    const/4 v5, 0x1

    monitor-exit p0

    goto/16 :goto_0

    .line 1482
    .end local v27    # "urlStatus":Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUrlStatus;
    :cond_1c
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/smartbonding/SmartBondingService;->getSBLimitFileSize()J

    move-result-wide v6

    cmp-long v5, p3, v6

    if-gez v5, :cond_1e

    .line 1483
    sget-boolean v5, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v5, :cond_1d

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "startSBUsage : smart bonding is not enabled because file size : limit "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/smartbonding/SmartBondingService;->getSBLimitFileSize()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " fileSize : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, p3

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 1484
    :cond_1d
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v5

    move-object/from16 v0, p0

    move-object/from16 v1, p7

    invoke-direct {v0, v5, v1}, Lcom/samsung/android/smartbonding/SmartBondingService;->getApplicationName(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v14

    const/16 v16, 0x5

    const/16 v17, 0xf

    const-wide/16 v18, 0x0

    const-wide/16 v20, 0x0

    move-object/from16 v11, p0

    move-wide/from16 v12, p1

    move-object/from16 v15, p7

    invoke-direct/range {v11 .. v21}, Lcom/samsung/android/smartbonding/SmartBondingService;->sendStateChangedIntent(JLjava/lang/String;Ljava/lang/String;IIJJ)V

    .line 1485
    const/4 v5, 0x0

    monitor-exit p0

    goto/16 :goto_0

    .line 1489
    :cond_1e
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/smartbonding/SmartBondingService;->mTm:Landroid/telephony/TelephonyManager;

    if-eqz v5, :cond_20

    .line 1490
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/smartbonding/SmartBondingService;->mTm:Landroid/telephony/TelephonyManager;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/smartbonding/SmartBondingService;->mTm:Landroid/telephony/TelephonyManager;

    invoke-virtual {v5}, Landroid/telephony/TelephonyManager;->getDataNetworkType()I

    move-result v5

    invoke-static {v5}, Landroid/telephony/TelephonyManager;->getNetworkClass(I)I

    move-result v24

    .line 1491
    .local v24, "network_class":I
    const/4 v5, 0x3

    move/from16 v0, v24

    if-eq v0, v5, :cond_20

    .line 1492
    sget-boolean v5, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v5, :cond_1f

    const-string v5, "startSBUsage : network class is not 4G so that smart bonding won\'t work"

    invoke-static {v5}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 1493
    :cond_1f
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v5

    move-object/from16 v0, p0

    move-object/from16 v1, p7

    invoke-direct {v0, v5, v1}, Lcom/samsung/android/smartbonding/SmartBondingService;->getApplicationName(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v14

    const/16 v16, 0x5

    const/16 v17, 0x11

    const-wide/16 v18, 0x0

    const-wide/16 v20, 0x0

    move-object/from16 v11, p0

    move-wide/from16 v12, p1

    move-object/from16 v15, p7

    invoke-direct/range {v11 .. v21}, Lcom/samsung/android/smartbonding/SmartBondingService;->sendStateChangedIntent(JLjava/lang/String;Ljava/lang/String;IIJJ)V

    .line 1494
    const/4 v5, 0x0

    monitor-exit p0

    goto/16 :goto_0

    .line 1508
    .end local v24    # "network_class":I
    :cond_20
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSBUsageStatus:Ljava/util/ArrayList;

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1510
    const/16 v23, 0x0

    .line 1511
    .local v23, "isUrlExist":Z
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSBUrlStatus:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v22

    :cond_21
    :goto_1
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_26

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUrlStatus;

    .line 1512
    .restart local v27    # "urlStatus":Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUrlStatus;
    move-object/from16 v0, v27

    iget-object v5, v0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUrlStatus;->mUrl:Ljava/lang/String;

    iget-object v6, v4, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUrlStatus;->mUrl:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_21

    .line 1513
    sget-boolean v5, Lcom/samsung/android/smartbonding/SmartBondingService;->VDBG:Z

    if-eqz v5, :cond_22

    const-string v5, "startSBUsage : url is updated"

    invoke-static {v5}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 1514
    :cond_22
    move-object/from16 v0, p0

    move-object/from16 v1, p7

    invoke-direct {v0, v1}, Lcom/samsung/android/smartbonding/SmartBondingService;->isFirstCombinedRequest(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_23

    .line 1515
    move-object/from16 v0, v27

    iget-object v5, v0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUrlStatus;->mLinkedUsageStatus:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    .line 1517
    :cond_23
    move-object/from16 v0, v27

    iget-object v5, v0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUrlStatus;->mLinkedUsageStatus:Ljava/util/ArrayList;

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1518
    move-object/from16 v0, v27

    iget-object v5, v0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUrlStatus;->mStartUrl:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_24

    move-object/from16 v0, p0

    move-object/from16 v1, p7

    invoke-direct {v0, v1}, Lcom/samsung/android/smartbonding/SmartBondingService;->isFirstCombinedRequest(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_25

    .line 1519
    :cond_24
    move-object/from16 v0, v27

    iput-object v9, v0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUrlStatus;->mStartUrl:Ljava/lang/String;

    .line 1521
    :cond_25
    const/4 v5, 0x0

    move-object/from16 v0, v27

    iput v5, v0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUrlStatus;->mStatus:I

    .line 1522
    const/16 v23, 0x1

    goto :goto_1

    .line 1525
    .end local v27    # "urlStatus":Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUrlStatus;
    :cond_26
    if-nez v23, :cond_28

    .line 1526
    sget-boolean v5, Lcom/samsung/android/smartbonding/SmartBondingService;->VDBG:Z

    if-eqz v5, :cond_27

    const-string v5, "startSBUsage : add url status in list"

    invoke-static {v5}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 1527
    :cond_27
    iget-object v5, v4, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUrlStatus;->mLinkedUsageStatus:Ljava/util/ArrayList;

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1528
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSBUrlStatus:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1546
    :cond_28
    const/4 v5, 0x1

    move-object/from16 v0, p0

    move-wide/from16 v1, p1

    invoke-virtual {v0, v5, v1, v2}, Lcom/samsung/android/smartbonding/SmartBondingService;->setSBUsageStatus(IJ)V

    .line 1547
    const/4 v5, 0x1

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0
.end method

.method public stopSBUsage(J)I
    .locals 11
    .param p1, "threadID"    # J

    .prologue
    const/4 v9, 0x0

    .line 1656
    monitor-enter p0

    .line 1657
    :try_start_0
    sget-boolean v5, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v5, :cond_0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "stopSBUsage : sb usauge is stopped : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 1658
    :cond_0
    const/4 v5, 0x2

    invoke-virtual {p0, v5, p1, p2}, Lcom/samsung/android/smartbonding/SmartBondingService;->setSBInternalUsageStatus(IJ)V

    .line 1661
    const-string v2, ""

    .line 1662
    .local v2, "url":Ljava/lang/String;
    iget-object v5, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSBUsageStatus:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;

    .line 1663
    .local v0, "UsageStatus":Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;
    iget-wide v6, v0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;->mThreadID:J

    cmp-long v5, v6, p1

    if-nez v5, :cond_1

    .line 1664
    iget-object v2, v0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;->mUrl:Ljava/lang/String;

    goto :goto_0

    .line 1668
    .end local v0    # "UsageStatus":Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;
    :cond_2
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_5

    .line 1669
    const-string v5, "\\?"

    invoke-virtual {v2, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 1670
    .local v4, "urls":[Ljava/lang/String;
    sget-boolean v5, Lcom/samsung/android/smartbonding/SmartBondingService;->VDBG:Z

    if-eqz v5, :cond_3

    const-string v5, "stopSBUsage : spilt done"

    invoke-static {v5}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 1671
    :cond_3
    const/4 v5, 0x0

    aget-object v5, v4, v5

    if-eqz v5, :cond_5

    const/4 v5, 0x0

    aget-object v5, v4, v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_5

    const/4 v5, 0x0

    aget-object v5, v4, v5

    const-string v6, "/market/GetBinary/GetBinary"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 1672
    sget-boolean v5, Lcom/samsung/android/smartbonding/SmartBondingService;->VDBG:Z

    if-eqz v5, :cond_4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "stopSBUsage : it\'s google play contents (url[0] : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/4 v6, 0x0

    aget-object v6, v4, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 1673
    :cond_4
    const/4 v5, 0x0

    aget-object v5, v4, v5

    const-string v6, "/o"

    invoke-virtual {v5, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 1674
    sget-boolean v5, Lcom/samsung/android/smartbonding/SmartBondingService;->VDBG:Z

    if-eqz v5, :cond_5

    const-string v5, "stopSBUsage : it\'s first contents"

    invoke-static {v5}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 1691
    .end local v4    # "urls":[Ljava/lang/String;
    :cond_5
    invoke-direct {p0, v2}, Lcom/samsung/android/smartbonding/SmartBondingService;->isYouTubeContent(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 1692
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/smartbonding/SmartBondingService;->removeSBUsage(J)V

    .line 1696
    :cond_6
    monitor-exit p0

    return v9

    .line 1676
    .restart local v4    # "urls":[Ljava/lang/String;
    :cond_7
    const/4 v5, 0x0

    aget-object v5, v4, v5

    const-string v6, "/p"

    invoke-virtual {v5, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 1677
    const/4 v5, 0x0

    const/4 v6, 0x0

    aget-object v6, v4, v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    aget-object v8, v4, v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, -0x2

    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    .line 1679
    :cond_8
    sget-boolean v5, Lcom/samsung/android/smartbonding/SmartBondingService;->VDBG:Z

    if-eqz v5, :cond_9

    const-string v5, "stopSBUsage : it\'s second contents"

    invoke-static {v5}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 1680
    :cond_9
    iget-object v5, p0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSBUrlStatus:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_a
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUrlStatus;

    .line 1681
    .local v3, "urlStatus":Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUrlStatus;
    iget-object v5, v3, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUrlStatus;->mUrl:Ljava/lang/String;

    const/4 v6, 0x0

    aget-object v6, v4, v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 1682
    sget-boolean v5, Lcom/samsung/android/smartbonding/SmartBondingService;->VDBG:Z

    if-eqz v5, :cond_b

    const-string v5, "stopSBUsage : same url is found. set it as none status"

    invoke-static {v5}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 1683
    :cond_b
    const/4 v5, 0x0

    iput v5, v3, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUrlStatus;->mStatus:I

    goto :goto_1

    .line 1697
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "url":Ljava/lang/String;
    .end local v3    # "urlStatus":Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUrlStatus;
    .end local v4    # "urls":[Ljava/lang/String;
    :catchall_0
    move-exception v5

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5
.end method

.method public stopSBUsageWithReason(JI)I
    .locals 21
    .param p1, "threadID"    # J
    .param p3, "errorNo"    # I

    .prologue
    .line 1553
    monitor-enter p0

    .line 1554
    :try_start_0
    sget-boolean v5, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v5, :cond_0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "stopSBUsageWithReason : reason "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p3

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 1556
    :cond_0
    const/4 v5, 0x2

    move/from16 v0, p3

    if-ne v0, v5, :cond_3

    .line 1557
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/smartbonding/SmartBondingService;->mHandler:Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;

    const/4 v6, 0x4

    invoke-virtual {v5, v6}, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v17

    .line 1558
    .local v17, "message":Landroid/os/Message;
    const/4 v5, 0x2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v17

    iput-object v5, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1559
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/smartbonding/SmartBondingService;->mHandler:Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->sendMessage(Landroid/os/Message;)Z

    .line 1592
    .end local v17    # "message":Landroid/os/Message;
    :cond_1
    :goto_0
    const-string v9, ""

    .line 1593
    .local v9, "url":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSBUsageStatus:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v16

    .local v16, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_b

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;

    .line 1594
    .local v20, "usageStatus":Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;
    move-object/from16 v0, v20

    iget-wide v6, v0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;->mThreadID:J

    cmp-long v5, v6, p1

    if-nez v5, :cond_2

    .line 1595
    move-object/from16 v0, v20

    iget-object v9, v0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;->mUrl:Ljava/lang/String;

    .line 1596
    if-gez p3, :cond_a

    .line 1597
    move-object/from16 v0, v20

    iget v5, v0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;->mPid:I

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v9}, Lcom/samsung/android/smartbonding/SmartBondingService;->getApplicationName(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const/4 v10, 0x4

    const-wide/16 v12, 0x0

    const-wide/16 v14, 0x0

    move-object/from16 v5, p0

    move-wide/from16 v6, p1

    move/from16 v11, p3

    invoke-direct/range {v5 .. v15}, Lcom/samsung/android/smartbonding/SmartBondingService;->sendStateChangedIntent(JLjava/lang/String;Ljava/lang/String;IIJJ)V

    goto :goto_1

    .line 1652
    .end local v9    # "url":Ljava/lang/String;
    .end local v16    # "i$":Ljava/util/Iterator;
    .end local v20    # "usageStatus":Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;
    :catchall_0
    move-exception v5

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5

    .line 1560
    :cond_3
    const/4 v5, -0x1

    move/from16 v0, p3

    if-ne v0, v5, :cond_5

    .line 1561
    :try_start_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/smartbonding/SmartBondingService;->mHandler:Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;

    const/4 v6, 0x4

    invoke-virtual {v5, v6}, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v18

    .line 1562
    .local v18, "message2":Landroid/os/Message;
    const/16 v5, 0x9

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v18

    iput-object v5, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1563
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/smartbonding/SmartBondingService;->mHandler:Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->sendMessage(Landroid/os/Message;)Z

    .line 1564
    sget-boolean v5, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v5, :cond_4

    const-string v5, "stopSBUsageWithReason : 1:4 algorithm is happened / use only wifi for downloading"

    invoke-static {v5}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 1565
    :cond_4
    const/4 v5, 0x0

    monitor-exit p0

    .line 1651
    .end local v18    # "message2":Landroid/os/Message;
    :goto_2
    return v5

    .line 1566
    :cond_5
    const/4 v5, -0x2

    move/from16 v0, p3

    if-ne v0, v5, :cond_7

    .line 1567
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/smartbonding/SmartBondingService;->mHandler:Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;

    const/4 v6, 0x4

    invoke-virtual {v5, v6}, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v18

    .line 1568
    .restart local v18    # "message2":Landroid/os/Message;
    const/16 v5, 0xa

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v18

    iput-object v5, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1569
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/smartbonding/SmartBondingService;->mHandler:Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->sendMessage(Landroid/os/Message;)Z

    .line 1570
    sget-boolean v5, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v5, :cond_6

    const-string v5, "stopSBUsageWithReason : 1:4 algorithm is happened / use only lte for downloading"

    invoke-static {v5}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 1571
    :cond_6
    const/4 v5, 0x0

    monitor-exit p0

    goto :goto_2

    .line 1572
    .end local v18    # "message2":Landroid/os/Message;
    :cond_7
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/smartbonding/SmartBondingService;->isMidHighDebugLevel()Z

    move-result v5

    if-eqz v5, :cond_9

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/smartbonding/SmartBondingService;->getSBInterfaceState()I

    move-result v5

    const/4 v6, 0x3

    if-ne v5, v6, :cond_9

    const-string v5, "persist.sys.sb.warning.show"

    const/4 v6, 0x0

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 1574
    const/4 v5, -0x3

    move/from16 v0, p3

    if-ne v0, v5, :cond_8

    .line 1575
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/smartbonding/SmartBondingService;->mHandler:Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;

    const/4 v6, 0x4

    invoke-virtual {v5, v6}, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v17

    .line 1576
    .restart local v17    # "message":Landroid/os/Message;
    const/16 v5, 0xb

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v17

    iput-object v5, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1577
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/smartbonding/SmartBondingService;->mHandler:Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->sendMessage(Landroid/os/Message;)Z

    .line 1578
    const/4 v5, 0x0

    monitor-exit p0

    goto :goto_2

    .line 1581
    .end local v17    # "message":Landroid/os/Message;
    :cond_8
    const/4 v5, -0x4

    move/from16 v0, p3

    if-ne v0, v5, :cond_1

    .line 1582
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/smartbonding/SmartBondingService;->mHandler:Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;

    const/4 v6, 0x4

    invoke-virtual {v5, v6}, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v17

    .line 1583
    .restart local v17    # "message":Landroid/os/Message;
    const/16 v5, 0xc

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v17

    iput-object v5, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1584
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/smartbonding/SmartBondingService;->mHandler:Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->sendMessage(Landroid/os/Message;)Z

    .line 1585
    const/4 v5, 0x0

    monitor-exit p0

    goto/16 :goto_2

    .line 1588
    .end local v17    # "message":Landroid/os/Message;
    :cond_9
    sget-boolean v5, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v5, :cond_1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unsupported errorno: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p3

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1599
    .restart local v9    # "url":Ljava/lang/String;
    .restart local v16    # "i$":Ljava/util/Iterator;
    .restart local v20    # "usageStatus":Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;
    :cond_a
    move-object/from16 v0, v20

    iget v5, v0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;->mPid:I

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v9}, Lcom/samsung/android/smartbonding/SmartBondingService;->getApplicationName(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const/4 v10, 0x3

    const-wide/16 v12, 0x0

    const-wide/16 v14, 0x0

    move-object/from16 v5, p0

    move-wide/from16 v6, p1

    move/from16 v11, p3

    invoke-direct/range {v5 .. v15}, Lcom/samsung/android/smartbonding/SmartBondingService;->sendStateChangedIntent(JLjava/lang/String;Ljava/lang/String;IIJJ)V

    goto/16 :goto_1

    .line 1605
    .end local v20    # "usageStatus":Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUsageStatus;
    :cond_b
    if-gez p3, :cond_c

    .line 1606
    const/4 v5, 0x0

    monitor-exit p0

    goto/16 :goto_2

    .line 1608
    :cond_c
    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/samsung/android/smartbonding/SmartBondingService;->convertCombinedUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1610
    .local v4, "combinedUrl":Ljava/lang/String;
    sget-boolean v5, Lcom/samsung/android/smartbonding/SmartBondingService;->VDBG:Z

    if-eqz v5, :cond_d

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "stopSBUsageWithReason : sb usauge is stopped : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, p1

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 1612
    :cond_d
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/samsung/android/smartbonding/SmartBondingService;->mIsStoppedByGooglePlay:Z

    if-nez v5, :cond_f

    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/samsung/android/smartbonding/SmartBondingService;->isGooglePlayContent(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_f

    .line 1613
    sget-boolean v5, Lcom/samsung/android/smartbonding/SmartBondingService;->DBG:Z

    if-eqz v5, :cond_e

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "stopSBUsageWithReason : stop with delay for waiting next contents : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-wide/from16 v0, p1

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 1614
    :cond_e
    const/4 v5, 0x1

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/samsung/android/smartbonding/SmartBondingService;->mIsStoppedByGooglePlay:Z

    .line 1617
    :cond_f
    const/4 v5, 0x2

    move-object/from16 v0, p0

    move-wide/from16 v1, p1

    invoke-virtual {v0, v5, v1, v2}, Lcom/samsung/android/smartbonding/SmartBondingService;->setSBInternalUsageStatus(IJ)V

    .line 1620
    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/samsung/android/smartbonding/SmartBondingService;->isYouTubeContent(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_10

    if-eqz p3, :cond_14

    .line 1621
    :cond_10
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSBUrlStatus:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :cond_11
    :goto_3
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_13

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUrlStatus;

    .line 1622
    .local v19, "urlStatus":Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUrlStatus;
    move-object/from16 v0, v19

    iget-object v5, v0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUrlStatus;->mUrl:Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_11

    .line 1623
    sget-boolean v5, Lcom/samsung/android/smartbonding/SmartBondingService;->VDBG:Z

    if-eqz v5, :cond_12

    const-string v5, "stopSBUsageWithReason : canceled so that list is cleared"

    invoke-static {v5}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 1624
    :cond_12
    move-object/from16 v0, v19

    iget-object v5, v0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUrlStatus;->mLinkedUsageStatus:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    goto :goto_3

    .line 1627
    .end local v19    # "urlStatus":Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUrlStatus;
    :cond_13
    invoke-direct/range {p0 .. p2}, Lcom/samsung/android/smartbonding/SmartBondingService;->removeSBUsage(J)V

    .line 1630
    :cond_14
    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/samsung/android/smartbonding/SmartBondingService;->isFirstCombinedRequest(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_17

    const/4 v5, 0x1

    move/from16 v0, p3

    if-ne v0, v5, :cond_17

    .line 1631
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSBUrlStatus:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :cond_15
    :goto_4
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_17

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUrlStatus;

    .line 1632
    .restart local v19    # "urlStatus":Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUrlStatus;
    move-object/from16 v0, v19

    iget-object v5, v0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUrlStatus;->mUrl:Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_15

    .line 1633
    sget-boolean v5, Lcom/samsung/android/smartbonding/SmartBondingService;->VDBG:Z

    if-eqz v5, :cond_16

    const-string v5, "stopSBUsageWithReason : download is cancelled. set it as none status"

    invoke-static {v5}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 1634
    :cond_16
    const/4 v5, 0x0

    move-object/from16 v0, v19

    iput v5, v0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUrlStatus;->mStatus:I

    .line 1635
    const-string v5, ""

    move-object/from16 v0, v19

    iput-object v5, v0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUrlStatus;->mStartUrl:Ljava/lang/String;

    goto :goto_4

    .line 1640
    .end local v19    # "urlStatus":Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUrlStatus;
    :cond_17
    const/4 v5, 0x2

    move/from16 v0, p3

    if-ne v0, v5, :cond_1a

    .line 1641
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/smartbonding/SmartBondingService;->mSBUrlStatus:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :cond_18
    :goto_5
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1a

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUrlStatus;

    .line 1642
    .restart local v19    # "urlStatus":Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUrlStatus;
    move-object/from16 v0, v19

    iget-object v5, v0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUrlStatus;->mUrl:Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_18

    .line 1643
    sget-boolean v5, Lcom/samsung/android/smartbonding/SmartBondingService;->VDBG:Z

    if-eqz v5, :cond_19

    const-string v5, "stopSBUsageWithReason : download is not supported. set it as not support status"

    invoke-static {v5}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 1644
    :cond_19
    const/4 v5, 0x4

    move-object/from16 v0, v19

    iput v5, v0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUrlStatus;->mStatus:I

    goto :goto_5

    .line 1651
    .end local v19    # "urlStatus":Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingUrlStatus;
    :cond_1a
    const/4 v5, 0x0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_2
.end method

.method public submitMultiSocketData([J[J)V
    .locals 4
    .param p1, "lens"    # [J
    .param p2, "times"    # [J

    .prologue
    const/4 v2, 0x1

    .line 1178
    :try_start_0
    sget-boolean v1, Lcom/samsung/android/smartbonding/SmartBondingService;->VDBG:Z

    if-eqz v1, :cond_0

    const-string v1, "submitMultiSocketData called"

    invoke-static {v1}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 1179
    :cond_0
    monitor-enter p0
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 1180
    :try_start_1
    array-length v1, p1

    if-ne v1, v2, :cond_2

    invoke-virtual {p0}, Lcom/samsung/android/smartbonding/SmartBondingService;->getSBEnabled()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/samsung/android/smartbonding/SmartBondingService;->getSBInterface(I)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/samsung/android/smartbonding/SmartBondingService;->getSBInterface(I)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1181
    sget-boolean v1, Lcom/samsung/android/smartbonding/SmartBondingService;->VDBG:Z

    if-eqz v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "receive command to switch dns but its not working in this version "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x0

    aget-wide v2, p1, v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/String;)V

    .line 1187
    :cond_1
    :goto_0
    monitor-exit p0

    .line 1191
    :goto_1
    return-void

    .line 1184
    :cond_2
    new-instance v1, Ljava/lang/Integer;

    invoke-static {}, Lcom/samsung/android/smartbonding/SmartBondingService;->getCallingPid()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-direct {p0, v1, p1, p2}, Lcom/samsung/android/smartbonding/SmartBondingService;->updateStatisticsData(Ljava/lang/Integer;[J[J)V

    .line 1185
    sget-object v1, Lcom/samsung/android/smartbonding/SmartBondingService;->INT_TOTAL:Ljava/lang/Integer;

    invoke-direct {p0, v1, p1, p2}, Lcom/samsung/android/smartbonding/SmartBondingService;->updateStatisticsData(Ljava/lang/Integer;[J[J)V

    goto :goto_0

    .line 1187
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v1
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    .line 1188
    :catch_0
    move-exception v0

    .line 1189
    .local v0, "e":Ljava/lang/Throwable;
    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->log(Ljava/lang/Throwable;)V

    goto :goto_1
.end method
