.class Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingSettingObserver;
.super Landroid/database/ContentObserver;
.source "SmartBondingService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/smartbonding/SmartBondingService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SmartBondingSettingObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/smartbonding/SmartBondingService;


# direct methods
.method constructor <init>(Lcom/samsung/android/smartbonding/SmartBondingService;Landroid/os/Handler;)V
    .locals 0
    .param p2, "handler"    # Landroid/os/Handler;

    .prologue
    .line 2596
    iput-object p1, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingSettingObserver;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    .line 2597
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 2598
    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 4
    .param p1, "selfChange"    # Z

    .prologue
    .line 2612
    iget-object v0, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingSettingObserver;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    # getter for: Lcom/samsung/android/smartbonding/SmartBondingService;->mHandler:Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;
    invoke-static {v0}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$300(Lcom/samsung/android/smartbonding/SmartBondingService;)Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingSettingObserver;->this$0:Lcom/samsung/android/smartbonding/SmartBondingService;

    # getter for: Lcom/samsung/android/smartbonding/SmartBondingService;->mHandler:Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;
    invoke-static {v1}, Lcom/samsung/android/smartbonding/SmartBondingService;->access$300(Lcom/samsung/android/smartbonding/SmartBondingService;)Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/smartbonding/SmartBondingService$SmartBondingHandler;->sendMessage(Landroid/os/Message;)Z

    .line 2613
    return-void
.end method

.method public register(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 2601
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 2602
    .local v0, "resolver":Landroid/content/ContentResolver;
    const-string v1, "smart_bonding"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 2603
    return-void
.end method

.method public unregister(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 2606
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 2607
    .local v0, "resolver":Landroid/content/ContentResolver;
    invoke-virtual {v0, p0}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 2608
    return-void
.end method
