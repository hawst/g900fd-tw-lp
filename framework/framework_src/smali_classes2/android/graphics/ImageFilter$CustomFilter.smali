.class public Landroid/graphics/ImageFilter$CustomFilter;
.super Landroid/graphics/ImageFilter;
.source "ImageFilter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/graphics/ImageFilter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CustomFilter"
.end annotation


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "vcode"    # Ljava/lang/String;
    .param p2, "fcode"    # Ljava/lang/String;

    .prologue
    .line 821
    const/16 v0, 0xee

    invoke-direct {p0, v0}, Landroid/graphics/ImageFilter;-><init>(I)V

    .line 822
    invoke-virtual {p0, p1}, Landroid/graphics/ImageFilter$CustomFilter;->setVertexShader(Ljava/lang/String;)V

    .line 823
    invoke-virtual {p0, p2}, Landroid/graphics/ImageFilter$CustomFilter;->setFragmentShader(Ljava/lang/String;)V

    .line 824
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/ImageFilter$1;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/String;
    .param p2, "x1"    # Ljava/lang/String;
    .param p3, "x2"    # Landroid/graphics/ImageFilter$1;

    .prologue
    .line 818
    invoke-direct {p0, p1, p2}, Landroid/graphics/ImageFilter$CustomFilter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public setSamplerBitmap(Ljava/lang/String;ILandroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "id"    # I
    .param p3, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 994
    invoke-super {p0, p1, p2, p3}, Landroid/graphics/ImageFilter;->setSamplerBitmap(Ljava/lang/String;ILandroid/graphics/Bitmap;)V

    .line 995
    return-void
.end method

.method public setUniform1f(Ljava/lang/String;F)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "v0"    # F

    .prologue
    const/4 v2, 0x1

    .line 837
    new-array v0, v2, [F

    .line 838
    .local v0, "value":[F
    const/4 v1, 0x0

    aput p2, v0, v1

    .line 839
    invoke-super {p0, p1, v2, v2, v0}, Landroid/graphics/ImageFilter;->setUniformf(Ljava/lang/String;II[F)V

    .line 840
    return-void
.end method

.method public setUniform1i(Ljava/lang/String;I)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "v0"    # I

    .prologue
    const/4 v2, 0x1

    .line 909
    new-array v0, v2, [I

    .line 910
    .local v0, "value":[I
    const/4 v1, 0x0

    aput p2, v0, v1

    .line 911
    invoke-super {p0, p1, v2, v2, v0}, Landroid/graphics/ImageFilter;->setUniformi(Ljava/lang/String;II[I)V

    .line 912
    return-void
.end method

.method public setUniform2f(Ljava/lang/String;FF)V
    .locals 4
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "v0"    # F
    .param p3, "v1"    # F

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 850
    new-array v0, v3, [F

    .line 851
    .local v0, "value":[F
    const/4 v1, 0x0

    aput p2, v0, v1

    .line 852
    aput p3, v0, v2

    .line 853
    invoke-super {p0, p1, v3, v2, v0}, Landroid/graphics/ImageFilter;->setUniformf(Ljava/lang/String;II[F)V

    .line 854
    return-void
.end method

.method public setUniform2i(Ljava/lang/String;II)V
    .locals 4
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "v0"    # I
    .param p3, "v1"    # I

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 922
    new-array v0, v3, [I

    .line 923
    .local v0, "value":[I
    const/4 v1, 0x0

    aput p2, v0, v1

    .line 924
    aput p3, v0, v2

    .line 925
    invoke-super {p0, p1, v3, v2, v0}, Landroid/graphics/ImageFilter;->setUniformi(Ljava/lang/String;II[I)V

    .line 926
    return-void
.end method

.method public setUniform3f(Ljava/lang/String;FFF)V
    .locals 4
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "v0"    # F
    .param p3, "v1"    # F
    .param p4, "v2"    # F

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x1

    .line 865
    new-array v0, v3, [F

    .line 866
    .local v0, "value":[F
    const/4 v1, 0x0

    aput p2, v0, v1

    .line 867
    aput p3, v0, v2

    .line 868
    const/4 v1, 0x2

    aput p4, v0, v1

    .line 869
    invoke-super {p0, p1, v3, v2, v0}, Landroid/graphics/ImageFilter;->setUniformf(Ljava/lang/String;II[F)V

    .line 870
    return-void
.end method

.method public setUniform3i(Ljava/lang/String;III)V
    .locals 4
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "v0"    # I
    .param p3, "v1"    # I
    .param p4, "v2"    # I

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x1

    .line 937
    new-array v0, v3, [I

    .line 938
    .local v0, "value":[I
    const/4 v1, 0x0

    aput p2, v0, v1

    .line 939
    aput p3, v0, v2

    .line 940
    const/4 v1, 0x2

    aput p4, v0, v1

    .line 941
    invoke-super {p0, p1, v3, v2, v0}, Landroid/graphics/ImageFilter;->setUniformi(Ljava/lang/String;II[I)V

    .line 942
    return-void
.end method

.method public setUniform4f(Ljava/lang/String;FFFF)V
    .locals 4
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "v0"    # F
    .param p3, "v1"    # F
    .param p4, "v2"    # F
    .param p5, "v3"    # F

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x1

    .line 882
    new-array v0, v3, [F

    .line 883
    .local v0, "value":[F
    const/4 v1, 0x0

    aput p2, v0, v1

    .line 884
    aput p3, v0, v2

    .line 885
    const/4 v1, 0x2

    aput p4, v0, v1

    .line 886
    const/4 v1, 0x3

    aput p5, v0, v1

    .line 887
    invoke-super {p0, p1, v3, v2, v0}, Landroid/graphics/ImageFilter;->setUniformf(Ljava/lang/String;II[F)V

    .line 888
    return-void
.end method

.method public setUniform4i(Ljava/lang/String;IIII)V
    .locals 4
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "v0"    # I
    .param p3, "v1"    # I
    .param p4, "v2"    # I
    .param p5, "v3"    # I

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x1

    .line 954
    new-array v0, v3, [I

    .line 955
    .local v0, "value":[I
    const/4 v1, 0x0

    aput p2, v0, v1

    .line 956
    aput p3, v0, v2

    .line 957
    const/4 v1, 0x2

    aput p4, v0, v1

    .line 958
    const/4 v1, 0x3

    aput p5, v0, v1

    .line 959
    invoke-super {p0, p1, v3, v2, v0}, Landroid/graphics/ImageFilter;->setUniformi(Ljava/lang/String;II[I)V

    .line 960
    return-void
.end method

.method public setUniformMatrix(Ljava/lang/String;II[F)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "row"    # I
    .param p3, "col"    # I
    .param p4, "value"    # [F

    .prologue
    .line 983
    invoke-super {p0, p1, p2, p3, p4}, Landroid/graphics/ImageFilter;->setUniformMatrix(Ljava/lang/String;II[F)V

    .line 984
    return-void
.end method

.method public setUniformfv(Ljava/lang/String;II[F)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "vec"    # I
    .param p3, "count"    # I
    .param p4, "value"    # [F

    .prologue
    .line 899
    invoke-super {p0, p1, p2, p3, p4}, Landroid/graphics/ImageFilter;->setUniformf(Ljava/lang/String;II[F)V

    .line 900
    return-void
.end method

.method public setUniformiv(Ljava/lang/String;II[I)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "vec"    # I
    .param p3, "count"    # I
    .param p4, "value"    # [I

    .prologue
    .line 971
    invoke-super {p0, p1, p2, p3, p4}, Landroid/graphics/ImageFilter;->setUniformi(Ljava/lang/String;II[I)V

    .line 972
    return-void
.end method

.method public setUpdateMargin(IIII)V
    .locals 0
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I

    .prologue
    .line 828
    invoke-super {p0, p1, p2, p3, p4}, Landroid/graphics/ImageFilter;->setUpdateMargin(IIII)V

    .line 829
    return-void
.end method
