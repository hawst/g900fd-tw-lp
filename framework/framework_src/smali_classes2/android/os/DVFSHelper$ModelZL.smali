.class Landroid/os/DVFSHelper$ModelZL;
.super Landroid/os/DVFSHelper$ModelJBP;
.source "DVFSHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/os/DVFSHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ModelZL"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/os/DVFSHelper;


# direct methods
.method constructor <init>(Landroid/os/DVFSHelper;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 1639
    iput-object p1, p0, Landroid/os/DVFSHelper$ModelZL;->this$0:Landroid/os/DVFSHelper;

    invoke-direct {p0, p1}, Landroid/os/DVFSHelper$ModelJBP;-><init>(Landroid/os/DVFSHelper;)V

    .line 1640
    iput v0, p0, Landroid/os/DVFSHelper$ModelZL;->BROWSER_TOUCH_ARM_FREQ:I

    .line 1641
    iput v0, p0, Landroid/os/DVFSHelper$ModelZL;->BROWSER_TOUCH_BUS_FREQ:I

    .line 1642
    iput v0, p0, Landroid/os/DVFSHelper$ModelZL;->GALLERY_TOUCH_ARM_FREQ:I

    .line 1643
    iput v0, p0, Landroid/os/DVFSHelper$ModelZL;->GALLERY_TOUCH_BUS_FREQ:I

    .line 1644
    iput v0, p0, Landroid/os/DVFSHelper$ModelZL;->LAUNCHER_TOUCH_ARM_FREQ:I

    .line 1645
    iput v0, p0, Landroid/os/DVFSHelper$ModelZL;->LAUNCHER_TOUCH_BUS_FREQ:I

    .line 1646
    iput v0, p0, Landroid/os/DVFSHelper$ModelZL;->LAUNCHER_TOUCH_GPU_FREQ:I

    .line 1647
    iput v1, p0, Landroid/os/DVFSHelper$ModelZL;->LAUNCHER_TOUCH_CPU_CORE:I

    .line 1648
    iput v0, p0, Landroid/os/DVFSHelper$ModelZL;->GROUP_PLAY_ARM_FREQ:I

    .line 1649
    iput v0, p0, Landroid/os/DVFSHelper$ModelZL;->LIST_SCROLL_ARM_FREQ:I

    .line 1650
    iput v0, p0, Landroid/os/DVFSHelper$ModelZL;->LIST_SCROLL_GPU_FREQ:I

    .line 1651
    iput v0, p0, Landroid/os/DVFSHelper$ModelZL;->LIST_SCROLL_BUS_FREQ:I

    .line 1652
    sput v1, Landroid/os/DVFSHelper;->LIST_SCROLL_BOOSTER_CORE_NUM:I

    .line 1653
    iput v0, p0, Landroid/os/DVFSHelper$ModelZL;->AMS_RESUME_ARM_FREQ:I

    .line 1654
    iput v0, p0, Landroid/os/DVFSHelper$ModelZL;->AMS_RESUME_GPU_FREQ:I

    .line 1655
    iput v0, p0, Landroid/os/DVFSHelper$ModelZL;->AMS_RESUME_BUS_FREQ:I

    .line 1656
    iput v1, p0, Landroid/os/DVFSHelper$ModelZL;->AMS_RESUME_CPU_CORE:I

    .line 1657
    iput v0, p0, Landroid/os/DVFSHelper$ModelZL;->APP_LAUNCH_GPU_FREQ:I

    .line 1658
    iput v0, p0, Landroid/os/DVFSHelper$ModelZL;->APP_LAUNCH_BUS_FREQ:I

    .line 1659
    iput v0, p0, Landroid/os/DVFSHelper$ModelZL;->BROWSER_FLING_ARM_FREQ:I

    .line 1660
    iput v0, p0, Landroid/os/DVFSHelper$ModelZL;->DEVICE_WAKEUP_ARM_FREQ:I

    .line 1661
    iput v0, p0, Landroid/os/DVFSHelper$ModelZL;->ROTATION_ARM_FREQ:I

    .line 1662
    return-void
.end method
