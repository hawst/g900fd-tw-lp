.class Landroid/os/DVFSHelper$ModelZQ;
.super Landroid/os/DVFSHelper$ModelJBP;
.source "DVFSHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/os/DVFSHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ModelZQ"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/os/DVFSHelper;


# direct methods
.method constructor <init>(Landroid/os/DVFSHelper;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 1666
    iput-object p1, p0, Landroid/os/DVFSHelper$ModelZQ;->this$0:Landroid/os/DVFSHelper;

    invoke-direct {p0, p1}, Landroid/os/DVFSHelper$ModelJBP;-><init>(Landroid/os/DVFSHelper;)V

    .line 1667
    iput v0, p0, Landroid/os/DVFSHelper$ModelZQ;->BROWSER_TOUCH_ARM_FREQ:I

    .line 1668
    iput v0, p0, Landroid/os/DVFSHelper$ModelZQ;->BROWSER_TOUCH_BUS_FREQ:I

    .line 1669
    iput v0, p0, Landroid/os/DVFSHelper$ModelZQ;->GALLERY_TOUCH_ARM_FREQ:I

    .line 1670
    iput v0, p0, Landroid/os/DVFSHelper$ModelZQ;->GALLERY_TOUCH_BUS_FREQ:I

    .line 1671
    iput v0, p0, Landroid/os/DVFSHelper$ModelZQ;->LAUNCHER_TOUCH_ARM_FREQ:I

    .line 1672
    iput v0, p0, Landroid/os/DVFSHelper$ModelZQ;->LAUNCHER_TOUCH_BUS_FREQ:I

    .line 1673
    iput v0, p0, Landroid/os/DVFSHelper$ModelZQ;->LAUNCHER_TOUCH_GPU_FREQ:I

    .line 1674
    iput v1, p0, Landroid/os/DVFSHelper$ModelZQ;->LAUNCHER_TOUCH_CPU_CORE:I

    .line 1675
    iput v0, p0, Landroid/os/DVFSHelper$ModelZQ;->GROUP_PLAY_ARM_FREQ:I

    .line 1676
    iput v0, p0, Landroid/os/DVFSHelper$ModelZQ;->LIST_SCROLL_ARM_FREQ:I

    .line 1677
    iput v0, p0, Landroid/os/DVFSHelper$ModelZQ;->LIST_SCROLL_GPU_FREQ:I

    .line 1678
    iput v0, p0, Landroid/os/DVFSHelper$ModelZQ;->LIST_SCROLL_BUS_FREQ:I

    .line 1679
    sput v1, Landroid/os/DVFSHelper;->LIST_SCROLL_BOOSTER_CORE_NUM:I

    .line 1680
    iput v0, p0, Landroid/os/DVFSHelper$ModelZQ;->AMS_RESUME_ARM_FREQ:I

    .line 1681
    iput v0, p0, Landroid/os/DVFSHelper$ModelZQ;->AMS_RESUME_GPU_FREQ:I

    .line 1682
    iput v0, p0, Landroid/os/DVFSHelper$ModelZQ;->AMS_RESUME_BUS_FREQ:I

    .line 1683
    iput v1, p0, Landroid/os/DVFSHelper$ModelZQ;->AMS_RESUME_CPU_CORE:I

    .line 1684
    iput v0, p0, Landroid/os/DVFSHelper$ModelZQ;->APP_LAUNCH_GPU_FREQ:I

    .line 1685
    iput v0, p0, Landroid/os/DVFSHelper$ModelZQ;->APP_LAUNCH_BUS_FREQ:I

    .line 1686
    iput v0, p0, Landroid/os/DVFSHelper$ModelZQ;->BROWSER_FLING_ARM_FREQ:I

    .line 1687
    iput v0, p0, Landroid/os/DVFSHelper$ModelZQ;->DEVICE_WAKEUP_ARM_FREQ:I

    .line 1688
    iput v0, p0, Landroid/os/DVFSHelper$ModelZQ;->ROTATION_ARM_FREQ:I

    .line 1689
    return-void
.end method
