.class public Landroid/hardware/scontext/SContextPedometerAttribute;
.super Landroid/hardware/scontext/SContextAttribute;
.source "SContextPedometerAttribute.java"


# static fields
.field static MODE_EXERCISE:I

.field static MODE_USER_INFO:I


# instance fields
.field private mExerciseMode:I

.field private mGender:I

.field private mHeight:D

.field private mMode:I

.field private mWeight:D


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const/4 v0, 0x0

    sput v0, Landroid/hardware/scontext/SContextPedometerAttribute;->MODE_USER_INFO:I

    .line 25
    const/4 v0, 0x1

    sput v0, Landroid/hardware/scontext/SContextPedometerAttribute;->MODE_EXERCISE:I

    return-void
.end method

.method constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 40
    invoke-direct {p0}, Landroid/hardware/scontext/SContextAttribute;-><init>()V

    .line 27
    const/4 v0, 0x1

    iput v0, p0, Landroid/hardware/scontext/SContextPedometerAttribute;->mGender:I

    .line 29
    const-wide v0, 0x4065400000000000L    # 170.0

    iput-wide v0, p0, Landroid/hardware/scontext/SContextPedometerAttribute;->mHeight:D

    .line 31
    const-wide/high16 v0, 0x404e000000000000L    # 60.0

    iput-wide v0, p0, Landroid/hardware/scontext/SContextPedometerAttribute;->mWeight:D

    .line 33
    iput v2, p0, Landroid/hardware/scontext/SContextPedometerAttribute;->mExerciseMode:I

    .line 35
    iput v2, p0, Landroid/hardware/scontext/SContextPedometerAttribute;->mMode:I

    .line 41
    return-void
.end method

.method public constructor <init>(I)V
    .locals 3
    .param p1, "exerciseMode"    # I

    .prologue
    const/4 v2, -0x1

    .line 58
    invoke-direct {p0}, Landroid/hardware/scontext/SContextAttribute;-><init>()V

    .line 27
    const/4 v0, 0x1

    iput v0, p0, Landroid/hardware/scontext/SContextPedometerAttribute;->mGender:I

    .line 29
    const-wide v0, 0x4065400000000000L    # 170.0

    iput-wide v0, p0, Landroid/hardware/scontext/SContextPedometerAttribute;->mHeight:D

    .line 31
    const-wide/high16 v0, 0x404e000000000000L    # 60.0

    iput-wide v0, p0, Landroid/hardware/scontext/SContextPedometerAttribute;->mWeight:D

    .line 33
    iput v2, p0, Landroid/hardware/scontext/SContextPedometerAttribute;->mExerciseMode:I

    .line 35
    iput v2, p0, Landroid/hardware/scontext/SContextPedometerAttribute;->mMode:I

    .line 59
    sget v0, Landroid/hardware/scontext/SContextPedometerAttribute;->MODE_EXERCISE:I

    iput v0, p0, Landroid/hardware/scontext/SContextPedometerAttribute;->mMode:I

    .line 60
    iput p1, p0, Landroid/hardware/scontext/SContextPedometerAttribute;->mExerciseMode:I

    .line 61
    return-void
.end method

.method public constructor <init>(IDD)V
    .locals 4
    .param p1, "gender"    # I
    .param p2, "height"    # D
    .param p4, "weight"    # D

    .prologue
    const/4 v2, -0x1

    .line 51
    invoke-direct {p0}, Landroid/hardware/scontext/SContextAttribute;-><init>()V

    .line 27
    const/4 v0, 0x1

    iput v0, p0, Landroid/hardware/scontext/SContextPedometerAttribute;->mGender:I

    .line 29
    const-wide v0, 0x4065400000000000L    # 170.0

    iput-wide v0, p0, Landroid/hardware/scontext/SContextPedometerAttribute;->mHeight:D

    .line 31
    const-wide/high16 v0, 0x404e000000000000L    # 60.0

    iput-wide v0, p0, Landroid/hardware/scontext/SContextPedometerAttribute;->mWeight:D

    .line 33
    iput v2, p0, Landroid/hardware/scontext/SContextPedometerAttribute;->mExerciseMode:I

    .line 35
    iput v2, p0, Landroid/hardware/scontext/SContextPedometerAttribute;->mMode:I

    .line 52
    sget v0, Landroid/hardware/scontext/SContextPedometerAttribute;->MODE_USER_INFO:I

    iput v0, p0, Landroid/hardware/scontext/SContextPedometerAttribute;->mMode:I

    .line 53
    iput p1, p0, Landroid/hardware/scontext/SContextPedometerAttribute;->mGender:I

    .line 54
    iput-wide p2, p0, Landroid/hardware/scontext/SContextPedometerAttribute;->mHeight:D

    .line 55
    iput-wide p4, p0, Landroid/hardware/scontext/SContextPedometerAttribute;->mWeight:D

    .line 56
    return-void
.end method


# virtual methods
.method getExerciseMode()I
    .locals 1

    .prologue
    .line 80
    iget v0, p0, Landroid/hardware/scontext/SContextPedometerAttribute;->mExerciseMode:I

    return v0
.end method

.method getGender()I
    .locals 1

    .prologue
    .line 68
    iget v0, p0, Landroid/hardware/scontext/SContextPedometerAttribute;->mGender:I

    return v0
.end method

.method getHeight()D
    .locals 2

    .prologue
    .line 72
    iget-wide v0, p0, Landroid/hardware/scontext/SContextPedometerAttribute;->mHeight:D

    return-wide v0
.end method

.method getMode()I
    .locals 1

    .prologue
    .line 64
    iget v0, p0, Landroid/hardware/scontext/SContextPedometerAttribute;->mMode:I

    return v0
.end method

.method getWeight()D
    .locals 2

    .prologue
    .line 76
    iget-wide v0, p0, Landroid/hardware/scontext/SContextPedometerAttribute;->mWeight:D

    return-wide v0
.end method
