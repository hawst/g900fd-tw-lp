.class public Landroid/hardware/scontext/SContextManager;
.super Ljava/lang/Object;
.source "SContextManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/hardware/scontext/SContextManager$SContextListenerDelegate;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "SContextManager"


# instance fields
.field private mAvailableServiceMap:Ljava/util/HashMap;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mListenerDelegates:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Landroid/hardware/scontext/SContextManager$SContextListenerDelegate;",
            ">;"
        }
    .end annotation
.end field

.field mMainLooper:Landroid/os/Looper;

.field private mSContextService:Landroid/hardware/scontext/ISContextService;


# direct methods
.method public constructor <init>(Landroid/os/Looper;)V
    .locals 1
    .param p1, "mainLooper"    # Landroid/os/Looper;

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Landroid/hardware/scontext/SContextManager;->mListenerDelegates:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 49
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/hardware/scontext/SContextManager;->mAvailableServiceMap:Ljava/util/HashMap;

    .line 59
    const-string/jumbo v0, "scontext"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/hardware/scontext/ISContextService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/hardware/scontext/ISContextService;

    move-result-object v0

    iput-object v0, p0, Landroid/hardware/scontext/SContextManager;->mSContextService:Landroid/hardware/scontext/ISContextService;

    .line 61
    iput-object p1, p0, Landroid/hardware/scontext/SContextManager;->mMainLooper:Landroid/os/Looper;

    .line 62
    return-void
.end method

.method private changeParameters(Landroid/hardware/scontext/SContextListener;Landroid/hardware/scontext/SContextProperty;)Z
    .locals 6
    .param p1, "listener"    # Landroid/hardware/scontext/SContextListener;
    .param p2, "property"    # Landroid/hardware/scontext/SContextProperty;

    .prologue
    const/4 v3, 0x0

    .line 904
    invoke-virtual {p2}, Landroid/hardware/scontext/SContextProperty;->getType()I

    move-result v2

    .line 905
    .local v2, "service":I
    invoke-direct {p0, p1, v2}, Landroid/hardware/scontext/SContextManager;->checkListenerAndService(Landroid/hardware/scontext/SContextListener;I)Z

    move-result v4

    if-nez v4, :cond_0

    .line 925
    :goto_0
    return v3

    .line 909
    :cond_0
    invoke-direct {p0, p1}, Landroid/hardware/scontext/SContextManager;->getListenerDelegate(Landroid/hardware/scontext/SContextListener;)Landroid/hardware/scontext/SContextManager$SContextListenerDelegate;

    move-result-object v1

    .line 911
    .local v1, "scontextListener":Landroid/hardware/scontext/SContextManager$SContextListenerDelegate;
    if-nez v1, :cond_1

    .line 912
    const-string v4, "SContextManager"

    const-string v5, "  .changeParameters : SContextListener is null!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 917
    :cond_1
    :try_start_0
    iget-object v3, p0, Landroid/hardware/scontext/SContextManager;->mSContextService:Landroid/hardware/scontext/ISContextService;

    invoke-interface {v3, v1, p2}, Landroid/hardware/scontext/ISContextService;->changeParameters(Landroid/os/IBinder;Landroid/hardware/scontext/SContextProperty;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 918
    const-string v3, "SContextManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "  .changeParameters : listener = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", service="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v2}, Landroid/hardware/scontext/SContext;->getServiceName(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 925
    :cond_2
    :goto_1
    const/4 v3, 0x1

    goto :goto_0

    .line 922
    :catch_0
    move-exception v0

    .line 923
    .local v0, "e":Landroid/os/RemoteException;
    const-string v3, "SContextManager"

    const-string v4, "RemoteException in changeParameters: "

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method private checkListenerAndService(Landroid/hardware/scontext/SContextListener;I)Z
    .locals 4
    .param p1, "listener"    # Landroid/hardware/scontext/SContextListener;
    .param p2, "service"    # I

    .prologue
    .line 1118
    const/4 v0, 0x0

    .line 1119
    .local v0, "res":Z
    if-nez p1, :cond_0

    .line 1120
    const-string v2, "SContextManager"

    const-string v3, "Listener is null!"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v0

    .line 1124
    .end local v0    # "res":Z
    .local v1, "res":I
    :goto_0
    return v1

    .line 1123
    .end local v1    # "res":I
    .restart local v0    # "res":Z
    :cond_0
    invoke-virtual {p0, p2}, Landroid/hardware/scontext/SContextManager;->isAvailableService(I)Z

    move-result v0

    move v1, v0

    .line 1124
    .restart local v1    # "res":I
    goto :goto_0
.end method

.method private getAvailableServiceMap()Ljava/util/HashMap;
    .locals 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "UseSparseArrays"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1077
    const/4 v2, 0x0

    .line 1079
    .local v2, "serviceMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    :try_start_0
    iget-object v3, p0, Landroid/hardware/scontext/SContextManager;->mSContextService:Landroid/hardware/scontext/ISContextService;

    invoke-interface {v3}, Landroid/hardware/scontext/ISContextService;->getAvailableServiceMap()Ljava/util/Map;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/util/HashMap;

    move-object v2, v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1083
    :goto_0
    return-object v2

    .line 1080
    :catch_0
    move-exception v1

    .line 1081
    .local v1, "e":Landroid/os/RemoteException;
    const-string v3, "SContextManager"

    const-string v4, "RemoteException in getAvailableServiceMap: "

    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private getListenerDelegate(Landroid/hardware/scontext/SContextListener;)Landroid/hardware/scontext/SContextManager$SContextListenerDelegate;
    .locals 4
    .param p1, "listener"    # Landroid/hardware/scontext/SContextListener;

    .prologue
    .line 1093
    if-eqz p1, :cond_0

    iget-object v3, p0, Landroid/hardware/scontext/SContextManager;->mListenerDelegates:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v3}, Ljava/util/concurrent/CopyOnWriteArrayList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1094
    :cond_0
    const/4 v2, 0x0

    .line 1108
    :cond_1
    :goto_0
    return-object v2

    .line 1097
    :cond_2
    const/4 v2, 0x0

    .line 1099
    .local v2, "scontextListener":Landroid/hardware/scontext/SContextManager$SContextListenerDelegate;
    iget-object v3, p0, Landroid/hardware/scontext/SContextManager;->mListenerDelegates:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v3}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 1100
    .local v1, "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/hardware/scontext/SContextManager$SContextListenerDelegate;>;"
    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1101
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/scontext/SContextManager$SContextListenerDelegate;

    .line 1102
    .local v0, "delegate":Landroid/hardware/scontext/SContextManager$SContextListenerDelegate;
    invoke-virtual {v0}, Landroid/hardware/scontext/SContextManager$SContextListenerDelegate;->getListener()Landroid/hardware/scontext/SContextListener;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1103
    move-object v2, v0

    .line 1104
    goto :goto_0
.end method

.method private registerListener(Landroid/hardware/scontext/SContextListener;Landroid/hardware/scontext/SContextProperty;)Z
    .locals 6
    .param p1, "listener"    # Landroid/hardware/scontext/SContextListener;
    .param p2, "property"    # Landroid/hardware/scontext/SContextProperty;

    .prologue
    .line 632
    invoke-virtual {p2}, Landroid/hardware/scontext/SContextProperty;->getType()I

    move-result v2

    .line 633
    .local v2, "service":I
    invoke-direct {p0, p1, v2}, Landroid/hardware/scontext/SContextManager;->checkListenerAndService(Landroid/hardware/scontext/SContextListener;I)Z

    move-result v3

    if-nez v3, :cond_0

    .line 634
    const/4 v3, 0x0

    .line 652
    :goto_0
    return v3

    .line 637
    :cond_0
    invoke-direct {p0, p1}, Landroid/hardware/scontext/SContextManager;->getListenerDelegate(Landroid/hardware/scontext/SContextListener;)Landroid/hardware/scontext/SContextManager$SContextListenerDelegate;

    move-result-object v1

    .line 639
    .local v1, "scontextListener":Landroid/hardware/scontext/SContextManager$SContextListenerDelegate;
    if-nez v1, :cond_1

    .line 640
    new-instance v1, Landroid/hardware/scontext/SContextManager$SContextListenerDelegate;

    .end local v1    # "scontextListener":Landroid/hardware/scontext/SContextManager$SContextListenerDelegate;
    const/4 v3, 0x0

    invoke-direct {v1, p0, p1, v3}, Landroid/hardware/scontext/SContextManager$SContextListenerDelegate;-><init>(Landroid/hardware/scontext/SContextManager;Landroid/hardware/scontext/SContextListener;Landroid/os/Handler;)V

    .line 641
    .restart local v1    # "scontextListener":Landroid/hardware/scontext/SContextManager$SContextListenerDelegate;
    iget-object v3, p0, Landroid/hardware/scontext/SContextManager;->mListenerDelegates:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v3, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 645
    :cond_1
    :try_start_0
    iget-object v3, p0, Landroid/hardware/scontext/SContextManager;->mSContextService:Landroid/hardware/scontext/ISContextService;

    invoke-interface {v3, v1, p2}, Landroid/hardware/scontext/ISContextService;->registerCallback(Landroid/os/IBinder;Landroid/hardware/scontext/SContextProperty;)V

    .line 646
    const-string v3, "SContextManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "  .registerListener : listener = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", service="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v2}, Landroid/hardware/scontext/SContext;->getServiceName(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 652
    :goto_1
    const/4 v3, 0x1

    goto :goto_0

    .line 649
    :catch_0
    move-exception v0

    .line 650
    .local v0, "e":Landroid/os/RemoteException;
    const-string v3, "SContextManager"

    const-string v4, "RemoteException in registerListener: "

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method


# virtual methods
.method public changeParameters(Landroid/hardware/scontext/SContextListener;II)Z
    .locals 3
    .param p1, "listener"    # Landroid/hardware/scontext/SContextListener;
    .param p2, "service"    # I
    .param p3, "arg1"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 881
    new-instance v0, Landroid/hardware/scontext/SContextProperty;

    invoke-direct {v0, p2}, Landroid/hardware/scontext/SContextProperty;-><init>(I)V

    .line 882
    .local v0, "property":Landroid/hardware/scontext/SContextProperty;
    const/16 v2, 0x21

    if-ne p2, v2, :cond_0

    .line 883
    const-string/jumbo v2, "step_level_monitor_duration"

    invoke-virtual {v0, v2, p3}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    .line 889
    :goto_0
    invoke-direct {p0, p1, v0}, Landroid/hardware/scontext/SContextManager;->changeParameters(Landroid/hardware/scontext/SContextListener;Landroid/hardware/scontext/SContextProperty;)Z

    move-result v1

    .line 891
    :goto_1
    return v1

    .line 884
    :cond_0
    const/4 v2, 0x2

    if-ne p2, v2, :cond_1

    .line 885
    const-string v2, "pedometer_exercise_mode"

    invoke-virtual {v0, v2, p3}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    goto :goto_0

    .line 887
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public changeParameters(Landroid/hardware/scontext/SContextListener;IIDD)Z
    .locals 4
    .param p1, "listener"    # Landroid/hardware/scontext/SContextListener;
    .param p2, "service"    # I
    .param p3, "arg1"    # I
    .param p4, "arg2"    # D
    .param p6, "arg3"    # D
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v3, 0x2

    const/4 v1, 0x0

    .line 847
    new-instance v0, Landroid/hardware/scontext/SContextProperty;

    invoke-direct {v0, p2}, Landroid/hardware/scontext/SContextProperty;-><init>(I)V

    .line 848
    .local v0, "property":Landroid/hardware/scontext/SContextProperty;
    if-ne p2, v3, :cond_1

    .line 849
    const/4 v2, 0x1

    if-eq p3, v2, :cond_0

    if-ne p3, v3, :cond_2

    .line 851
    :cond_0
    const-string v2, "pedometer_gender"

    invoke-virtual {v0, v2, p3}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    .line 852
    const-string v2, "pedometer_height"

    invoke-virtual {v0, v2, p4, p5}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;D)V

    .line 853
    const-string v2, "pedometer_weight"

    invoke-virtual {v0, v2, p6, p7}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;D)V

    .line 861
    invoke-direct {p0, p1, v0}, Landroid/hardware/scontext/SContextManager;->changeParameters(Landroid/hardware/scontext/SContextListener;Landroid/hardware/scontext/SContextProperty;)Z

    move-result v1

    .line 863
    :cond_1
    :goto_0
    return v1

    .line 855
    :cond_2
    const-string v2, "SContextManager"

    const-string v3, "The arg is wrong!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public changeParameters(Landroid/hardware/scontext/SContextListener;IIIII)Z
    .locals 3
    .param p1, "listener"    # Landroid/hardware/scontext/SContextListener;
    .param p2, "service"    # I
    .param p3, "arg1"    # I
    .param p4, "arg2"    # I
    .param p5, "arg3"    # I
    .param p6, "arg4"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 818
    new-instance v0, Landroid/hardware/scontext/SContextProperty;

    invoke-direct {v0, p2}, Landroid/hardware/scontext/SContextProperty;-><init>(I)V

    .line 819
    .local v0, "property":Landroid/hardware/scontext/SContextProperty;
    const/16 v2, 0x23

    if-ne p2, v2, :cond_0

    .line 820
    const-string v2, "inactive_timer_alert_count"

    invoke-virtual {v0, v2, p3}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    .line 821
    const-string v2, "inactive_timer_start_time"

    invoke-virtual {v0, v2, p4}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    .line 822
    const-string v2, "inactive_timer_end_time"

    invoke-virtual {v0, v2, p5}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    .line 823
    const-string v2, "inactive_timer_duration"

    invoke-virtual {v0, v2, p6}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    .line 827
    invoke-direct {p0, p1, v0}, Landroid/hardware/scontext/SContextManager;->changeParameters(Landroid/hardware/scontext/SContextListener;Landroid/hardware/scontext/SContextProperty;)Z

    move-result v1

    .line 829
    :goto_0
    return v1

    .line 825
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public changeParameters(Landroid/hardware/scontext/SContextListener;ILandroid/hardware/scontext/SContextAttribute;)Z
    .locals 6
    .param p1, "listener"    # Landroid/hardware/scontext/SContextListener;
    .param p2, "service"    # I
    .param p3, "attribute"    # Landroid/hardware/scontext/SContextAttribute;

    .prologue
    .line 756
    new-instance v0, Landroid/hardware/scontext/SContextProperty;

    invoke-direct {v0, p2}, Landroid/hardware/scontext/SContextProperty;-><init>(I)V

    .line 757
    .local v0, "property":Landroid/hardware/scontext/SContextProperty;
    sparse-switch p2, :sswitch_data_0

    .line 791
    const-string v2, "SContextManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "changeParameters() doesn\'t support this service ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p2}, Landroid/hardware/scontext/SContext;->getServiceName(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 797
    .end local p3    # "attribute":Landroid/hardware/scontext/SContextAttribute;
    :cond_0
    :goto_0
    invoke-direct {p0, p1, v0}, Landroid/hardware/scontext/SContextManager;->changeParameters(Landroid/hardware/scontext/SContextListener;Landroid/hardware/scontext/SContextProperty;)Z

    move-result v1

    .line 799
    .local v1, "res":Z
    return v1

    .end local v1    # "res":Z
    .restart local p3    # "attribute":Landroid/hardware/scontext/SContextAttribute;
    :sswitch_0
    move-object v2, p3

    .line 759
    check-cast v2, Landroid/hardware/scontext/SContextPedometerAttribute;

    invoke-virtual {v2}, Landroid/hardware/scontext/SContextPedometerAttribute;->getMode()I

    move-result v2

    sget v3, Landroid/hardware/scontext/SContextPedometerAttribute;->MODE_USER_INFO:I

    if-ne v2, v3, :cond_1

    .line 760
    const-string v3, "pedometer_gender"

    move-object v2, p3

    check-cast v2, Landroid/hardware/scontext/SContextPedometerAttribute;

    invoke-virtual {v2}, Landroid/hardware/scontext/SContextPedometerAttribute;->getGender()I

    move-result v2

    invoke-virtual {v0, v3, v2}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    .line 762
    const-string v3, "pedometer_height"

    move-object v2, p3

    check-cast v2, Landroid/hardware/scontext/SContextPedometerAttribute;

    invoke-virtual {v2}, Landroid/hardware/scontext/SContextPedometerAttribute;->getHeight()D

    move-result-wide v4

    invoke-virtual {v0, v3, v4, v5}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;D)V

    .line 764
    const-string v2, "pedometer_weight"

    check-cast p3, Landroid/hardware/scontext/SContextPedometerAttribute;

    .end local p3    # "attribute":Landroid/hardware/scontext/SContextAttribute;
    invoke-virtual {p3}, Landroid/hardware/scontext/SContextPedometerAttribute;->getWeight()D

    move-result-wide v4

    invoke-virtual {v0, v2, v4, v5}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;D)V

    goto :goto_0

    .restart local p3    # "attribute":Landroid/hardware/scontext/SContextAttribute;
    :cond_1
    move-object v2, p3

    .line 766
    check-cast v2, Landroid/hardware/scontext/SContextPedometerAttribute;

    invoke-virtual {v2}, Landroid/hardware/scontext/SContextPedometerAttribute;->getMode()I

    move-result v2

    sget v3, Landroid/hardware/scontext/SContextPedometerAttribute;->MODE_EXERCISE:I

    if-ne v2, v3, :cond_0

    .line 767
    const-string v2, "pedometer_exercise_mode"

    check-cast p3, Landroid/hardware/scontext/SContextPedometerAttribute;

    .end local p3    # "attribute":Landroid/hardware/scontext/SContextAttribute;
    invoke-virtual {p3}, Landroid/hardware/scontext/SContextPedometerAttribute;->getExerciseMode()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    goto :goto_0

    .line 773
    .restart local p3    # "attribute":Landroid/hardware/scontext/SContextAttribute;
    :sswitch_1
    const-string/jumbo v2, "step_level_monitor_duration"

    check-cast p3, Landroid/hardware/scontext/SContextStepLevelMonitorAttribute;

    .end local p3    # "attribute":Landroid/hardware/scontext/SContextAttribute;
    invoke-virtual {p3}, Landroid/hardware/scontext/SContextStepLevelMonitorAttribute;->getDuration()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    goto :goto_0

    .line 778
    .restart local p3    # "attribute":Landroid/hardware/scontext/SContextAttribute;
    :sswitch_2
    const-string v3, "inactive_timer_device_type"

    move-object v2, p3

    check-cast v2, Landroid/hardware/scontext/SContextInactiveTimerAttribute;

    invoke-virtual {v2}, Landroid/hardware/scontext/SContextInactiveTimerAttribute;->getDeviceType()I

    move-result v2

    invoke-virtual {v0, v3, v2}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    .line 780
    const-string v3, "inactive_timer_duration"

    move-object v2, p3

    check-cast v2, Landroid/hardware/scontext/SContextInactiveTimerAttribute;

    invoke-virtual {v2}, Landroid/hardware/scontext/SContextInactiveTimerAttribute;->getDuration()I

    move-result v2

    invoke-virtual {v0, v3, v2}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    .line 782
    const-string v3, "inactive_timer_alert_count"

    move-object v2, p3

    check-cast v2, Landroid/hardware/scontext/SContextInactiveTimerAttribute;

    invoke-virtual {v2}, Landroid/hardware/scontext/SContextInactiveTimerAttribute;->getAlertCount()I

    move-result v2

    invoke-virtual {v0, v3, v2}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    .line 784
    const-string v3, "inactive_timer_start_time"

    move-object v2, p3

    check-cast v2, Landroid/hardware/scontext/SContextInactiveTimerAttribute;

    invoke-virtual {v2}, Landroid/hardware/scontext/SContextInactiveTimerAttribute;->getStartTime()I

    move-result v2

    invoke-virtual {v0, v3, v2}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    .line 786
    const-string v2, "inactive_timer_end_time"

    check-cast p3, Landroid/hardware/scontext/SContextInactiveTimerAttribute;

    .end local p3    # "attribute":Landroid/hardware/scontext/SContextAttribute;
    invoke-virtual {p3}, Landroid/hardware/scontext/SContextInactiveTimerAttribute;->getEndTime()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 757
    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x21 -> :sswitch_1
        0x23 -> :sswitch_2
    .end sparse-switch
.end method

.method public getFeatureLevel(I)I
    .locals 2
    .param p1, "service"    # I

    .prologue
    .line 990
    invoke-virtual {p0, p1}, Landroid/hardware/scontext/SContextManager;->isAvailableService(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 991
    iget-object v0, p0, Landroid/hardware/scontext/SContextManager;->mAvailableServiceMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 993
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public initializeSContextService(Landroid/hardware/scontext/SContextListener;I)V
    .locals 5
    .param p1, "listener"    # Landroid/hardware/scontext/SContextListener;
    .param p2, "service"    # I

    .prologue
    .line 723
    invoke-virtual {p0, p2}, Landroid/hardware/scontext/SContextManager;->isAvailableService(I)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x3

    if-eq p2, v2, :cond_1

    .line 741
    :cond_0
    :goto_0
    return-void

    .line 727
    :cond_1
    invoke-direct {p0, p1}, Landroid/hardware/scontext/SContextManager;->getListenerDelegate(Landroid/hardware/scontext/SContextListener;)Landroid/hardware/scontext/SContextManager$SContextListenerDelegate;

    move-result-object v1

    .line 729
    .local v1, "scontextListener":Landroid/hardware/scontext/SContextManager$SContextListenerDelegate;
    if-nez v1, :cond_2

    .line 730
    const-string v2, "SContextManager"

    const-string v3, "  .initializeSContextService : SContextListener is null!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 735
    :cond_2
    :try_start_0
    iget-object v2, p0, Landroid/hardware/scontext/SContextManager;->mSContextService:Landroid/hardware/scontext/ISContextService;

    invoke-interface {v2, v1, p2}, Landroid/hardware/scontext/ISContextService;->initializeSContextService(Landroid/os/IBinder;I)V

    .line 736
    const-string v2, "SContextManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "  .initializeSContextService : listener = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", service="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p2}, Landroid/hardware/scontext/SContext;->getServiceName(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 738
    :catch_0
    move-exception v0

    .line 739
    .local v0, "e":Landroid/os/RemoteException;
    const-string v2, "SContextManager"

    const-string v3, "RemoteException in initializeSContextService: "

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public isAvailableService(I)Z
    .locals 3
    .param p1, "service"    # I

    .prologue
    .line 973
    const/4 v0, 0x0

    .line 974
    .local v0, "res":Z
    iget-object v1, p0, Landroid/hardware/scontext/SContextManager;->mAvailableServiceMap:Ljava/util/HashMap;

    if-nez v1, :cond_0

    .line 975
    invoke-direct {p0}, Landroid/hardware/scontext/SContextManager;->getAvailableServiceMap()Ljava/util/HashMap;

    move-result-object v1

    iput-object v1, p0, Landroid/hardware/scontext/SContextManager;->mAvailableServiceMap:Ljava/util/HashMap;

    .line 977
    :cond_0
    iget-object v1, p0, Landroid/hardware/scontext/SContextManager;->mAvailableServiceMap:Ljava/util/HashMap;

    if-eqz v1, :cond_1

    .line 978
    iget-object v1, p0, Landroid/hardware/scontext/SContextManager;->mAvailableServiceMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    .line 980
    :cond_1
    return v0
.end method

.method public registerListener(Landroid/hardware/scontext/SContextListener;I)Z
    .locals 2
    .param p1, "listener"    # Landroid/hardware/scontext/SContextListener;
    .param p2, "service"    # I

    .prologue
    .line 73
    new-instance v0, Landroid/hardware/scontext/SContextProperty;

    invoke-direct {v0, p2}, Landroid/hardware/scontext/SContextProperty;-><init>(I)V

    .line 74
    .local v0, "property":Landroid/hardware/scontext/SContextProperty;
    invoke-direct {p0, p1, v0}, Landroid/hardware/scontext/SContextManager;->registerListener(Landroid/hardware/scontext/SContextListener;Landroid/hardware/scontext/SContextProperty;)Z

    move-result v1

    .line 76
    .local v1, "res":Z
    return v1
.end method

.method public registerListener(Landroid/hardware/scontext/SContextListener;II)Z
    .locals 4
    .param p1, "listener"    # Landroid/hardware/scontext/SContextListener;
    .param p2, "service"    # I
    .param p3, "arg"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v3, 0x2

    const/4 v1, 0x0

    .line 242
    new-instance v0, Landroid/hardware/scontext/SContextProperty;

    invoke-direct {v0, p2}, Landroid/hardware/scontext/SContextProperty;-><init>(I)V

    .line 243
    .local v0, "property":Landroid/hardware/scontext/SContextProperty;
    const/4 v2, 0x3

    if-ne p2, v2, :cond_1

    .line 244
    const-string/jumbo v2, "step_cout_alert_step"

    invoke-virtual {v0, v2, p3}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    .line 267
    :cond_0
    :goto_0
    invoke-direct {p0, p1, v0}, Landroid/hardware/scontext/SContextManager;->registerListener(Landroid/hardware/scontext/SContextListener;Landroid/hardware/scontext/SContextProperty;)Z

    move-result v1

    .line 269
    :goto_1
    return v1

    .line 245
    :cond_1
    const/4 v2, 0x6

    if-ne p2, v2, :cond_4

    .line 246
    if-eqz p3, :cond_2

    if-eq p3, v3, :cond_2

    const/4 v2, 0x4

    if-ne p3, v2, :cond_3

    .line 249
    :cond_2
    const-string v2, "auto_rotation_device_type"

    invoke-virtual {v0, v2, p3}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    goto :goto_0

    .line 251
    :cond_3
    const-string v2, "SContextManager"

    const-string v3, "The arg is wrong!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 254
    :cond_4
    const/16 v2, 0x10

    if-ne p2, v2, :cond_7

    .line 255
    const/4 v2, 0x1

    if-eq p3, v2, :cond_5

    if-ne p3, v3, :cond_6

    .line 257
    :cond_5
    const-string/jumbo v2, "wake_up_voice_mode"

    invoke-virtual {v0, v2, p3}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    goto :goto_0

    .line 259
    :cond_6
    const-string v2, "SContextManager"

    const-string v3, "The arg is wrong!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 262
    :cond_7
    const/16 v2, 0x21

    if-ne p2, v2, :cond_8

    .line 263
    const-string/jumbo v2, "step_level_monitor_duration"

    invoke-virtual {v0, v2, p3}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    goto :goto_0

    .line 264
    :cond_8
    const/16 v2, 0x24

    if-ne p2, v2, :cond_0

    .line 265
    const-string v2, "flat_motion_for_table_mode_duration"

    invoke-virtual {v0, v2, p3}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public registerListener(Landroid/hardware/scontext/SContextListener;IIDD)Z
    .locals 8
    .param p1, "listener"    # Landroid/hardware/scontext/SContextListener;
    .param p2, "service"    # I
    .param p3, "arg1"    # I
    .param p4, "arg2"    # D
    .param p6, "arg3"    # D
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v6, 0x2

    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    .line 414
    const/4 v1, 0x0

    .line 415
    .local v1, "res":Z
    new-instance v0, Landroid/hardware/scontext/SContextProperty;

    invoke-direct {v0, p2}, Landroid/hardware/scontext/SContextProperty;-><init>(I)V

    .line 416
    .local v0, "property":Landroid/hardware/scontext/SContextProperty;
    if-ne p2, v6, :cond_3

    .line 417
    const/4 v3, 0x1

    if-eq p3, v3, :cond_0

    if-eq p3, v6, :cond_0

    .line 419
    const-string v3, "SContextManager"

    const-string v4, "The gender is wrong!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 439
    :goto_0
    return v2

    .line 422
    :cond_0
    cmpg-double v3, p4, v4

    if-gtz v3, :cond_1

    .line 423
    const-string v3, "SContextManager"

    const-string v4, "The height cannot be less than or equal to 0.0[cm]!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 426
    :cond_1
    cmpg-double v3, p6, v4

    if-gtz v3, :cond_2

    .line 427
    const-string v3, "SContextManager"

    const-string v4, "The weight cannot be less than or equal to 0.0[kg]!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 431
    :cond_2
    const-string v2, "pedometer_gender"

    invoke-virtual {v0, v2, p3}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    .line 432
    const-string v2, "pedometer_height"

    invoke-virtual {v0, v2, p4, p5}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;D)V

    .line 433
    const-string v2, "pedometer_weight"

    invoke-virtual {v0, v2, p6, p7}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;D)V

    .line 434
    const-string v2, "pedometer_exercise_mode"

    const/4 v3, -0x1

    invoke-virtual {v0, v2, v3}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    .line 436
    invoke-direct {p0, p1, v0}, Landroid/hardware/scontext/SContextManager;->registerListener(Landroid/hardware/scontext/SContextListener;Landroid/hardware/scontext/SContextProperty;)Z

    move-result v1

    :cond_3
    move v2, v1

    .line 439
    goto :goto_0
.end method

.method public registerListener(Landroid/hardware/scontext/SContextListener;III)Z
    .locals 4
    .param p1, "listener"    # Landroid/hardware/scontext/SContextListener;
    .param p2, "service"    # I
    .param p3, "arg1"    # I
    .param p4, "arg2"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 348
    new-instance v0, Landroid/hardware/scontext/SContextProperty;

    invoke-direct {v0, p2}, Landroid/hardware/scontext/SContextProperty;-><init>(I)V

    .line 349
    .local v0, "property":Landroid/hardware/scontext/SContextProperty;
    const/16 v2, 0x8

    if-ne p2, v2, :cond_2

    .line 350
    const/4 v2, 0x1

    if-ne p3, v2, :cond_1

    .line 351
    const-string v2, "environment_sensor_type"

    invoke-virtual {v0, v2, p3}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    .line 352
    const-string v2, "environment_update_interval"

    invoke-virtual {v0, v2, p4}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    .line 365
    :cond_0
    :goto_0
    invoke-direct {p0, p1, v0}, Landroid/hardware/scontext/SContextManager;->registerListener(Landroid/hardware/scontext/SContextListener;Landroid/hardware/scontext/SContextProperty;)Z

    move-result v1

    .line 367
    :goto_1
    return v1

    .line 354
    :cond_1
    const-string v2, "SContextManager"

    const-string v3, "The arg is wrong!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 355
    const/4 v1, 0x0

    goto :goto_1

    .line 357
    :cond_2
    const/16 v2, 0xc

    if-ne p2, v2, :cond_3

    .line 358
    const-string/jumbo v2, "shake_motion_strength"

    invoke-virtual {v0, v2, p3}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    .line 359
    const-string/jumbo v2, "shake_motion_duration"

    invoke-virtual {v0, v2, p4}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    goto :goto_0

    .line 360
    :cond_3
    const/16 v2, 0x1d

    if-ne p2, v2, :cond_0

    .line 361
    const-string/jumbo v2, "sleep_monitor_sensibility"

    invoke-virtual {v0, v2, p3}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    .line 362
    const-string/jumbo v2, "sleep_monitor_sampling_interval"

    invoke-virtual {v0, v2, p4}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public registerListener(Landroid/hardware/scontext/SContextListener;IIIDI)Z
    .locals 11
    .param p1, "listener"    # Landroid/hardware/scontext/SContextListener;
    .param p2, "service"    # I
    .param p3, "arg1"    # I
    .param p4, "arg2"    # I
    .param p5, "arg3"    # D
    .param p7, "arg4"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 509
    const/4 v10, 0x0

    .line 510
    .local v10, "res":Z
    new-instance v9, Landroid/hardware/scontext/SContextProperty;

    invoke-direct {v9, p2}, Landroid/hardware/scontext/SContextProperty;-><init>(I)V

    .line 511
    .local v9, "property":Landroid/hardware/scontext/SContextProperty;
    const/16 v2, 0x9

    if-ne p2, v2, :cond_1

    .line 512
    const-string v2, "mfp_nomove_duration_thrs"

    invoke-virtual {v9, v2, p3}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    .line 513
    const-string v2, "mfp_move_duration_thrs"

    invoke-virtual {v9, v2, p4}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    .line 514
    const-string v2, "mfp_move_distance_thrs"

    move-wide/from16 v0, p5

    invoke-virtual {v9, v2, v0, v1}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;D)V

    .line 515
    const-string v2, "mfp_move_min_duration_thrs"

    move/from16 v0, p7

    invoke-virtual {v9, v2, v0}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    .line 516
    invoke-direct {p0, p1, v9}, Landroid/hardware/scontext/SContextManager;->registerListener(Landroid/hardware/scontext/SContextListener;Landroid/hardware/scontext/SContextProperty;)Z

    move-result v10

    .line 521
    :cond_0
    :goto_0
    return v10

    .line 517
    :cond_1
    const/16 v2, 0x1c

    if-ne p2, v2, :cond_0

    .line 518
    move-wide/from16 v0, p5

    double-to-int v7, v0

    move-object v2, p0

    move-object v3, p1

    move v4, p2

    move v5, p3

    move v6, p4

    move/from16 v8, p7

    invoke-virtual/range {v2 .. v8}, Landroid/hardware/scontext/SContextManager;->registerListener(Landroid/hardware/scontext/SContextListener;IIIII)Z

    move-result v10

    goto :goto_0
.end method

.method public registerListener(Landroid/hardware/scontext/SContextListener;IIII)Z
    .locals 3
    .param p1, "listener"    # Landroid/hardware/scontext/SContextListener;
    .param p2, "service"    # I
    .param p3, "arg1"    # I
    .param p4, "arg2"    # I
    .param p5, "arg3"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 387
    const/4 v1, 0x0

    .line 388
    .local v1, "res":Z
    new-instance v0, Landroid/hardware/scontext/SContextProperty;

    invoke-direct {v0, p2}, Landroid/hardware/scontext/SContextProperty;-><init>(I)V

    .line 389
    .local v0, "property":Landroid/hardware/scontext/SContextProperty;
    const/16 v2, 0x23

    if-ne p2, v2, :cond_0

    .line 390
    const-string v2, "inactive_timer_device_type"

    invoke-virtual {v0, v2, p3}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    .line 391
    const-string v2, "inactive_timer_duration"

    invoke-virtual {v0, v2, p4}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    .line 392
    const-string v2, "inactive_timer_alert_count"

    invoke-virtual {v0, v2, p5}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    .line 393
    invoke-direct {p0, p1, v0}, Landroid/hardware/scontext/SContextManager;->registerListener(Landroid/hardware/scontext/SContextListener;Landroid/hardware/scontext/SContextProperty;)Z

    move-result v1

    .line 396
    :cond_0
    return v1
.end method

.method public registerListener(Landroid/hardware/scontext/SContextListener;IIIII)Z
    .locals 12
    .param p1, "listener"    # Landroid/hardware/scontext/SContextListener;
    .param p2, "service"    # I
    .param p3, "arg1"    # I
    .param p4, "arg2"    # I
    .param p5, "arg3"    # I
    .param p6, "arg4"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 545
    const/4 v11, 0x0

    .line 546
    .local v11, "res":Z
    new-instance v2, Landroid/hardware/scontext/SContextProperty;

    invoke-direct {v2, p2}, Landroid/hardware/scontext/SContextProperty;-><init>(I)V

    .line 547
    .local v2, "property":Landroid/hardware/scontext/SContextProperty;
    const/16 v3, 0x1c

    if-ne p2, v3, :cond_6

    .line 548
    const/16 v3, 0x5a

    move/from16 v0, p4

    if-gt v0, v3, :cond_0

    const/16 v3, -0x5a

    move/from16 v0, p4

    if-lt v0, v3, :cond_0

    const/16 v3, 0x5a

    move/from16 v0, p5

    if-gt v0, v3, :cond_0

    const/16 v3, -0x5a

    move/from16 v0, p5

    if-ge v0, v3, :cond_1

    .line 549
    :cond_0
    const-string v3, "SContextManager"

    const-string v4, "The angle must be between -90 and 90 !"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 550
    const/4 v3, 0x0

    .line 571
    :goto_0
    return v3

    .line 553
    :cond_1
    move/from16 v0, p4

    move/from16 v1, p5

    if-le v0, v1, :cond_2

    .line 554
    const-string v3, "SContextManager"

    const-string v4, "The munumum angle must be less than the maximum angle !"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 555
    const/4 v3, 0x0

    goto :goto_0

    .line 558
    :cond_2
    if-ltz p6, :cond_3

    const/16 v3, 0x2710

    move/from16 v0, p6

    if-le v0, v3, :cond_4

    .line 559
    :cond_3
    const-string v3, "SContextManager"

    const-string v4, "The moving threshold must be between 0 and 10000 !"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 560
    const/4 v3, 0x0

    goto :goto_0

    .line 562
    :cond_4
    const-string/jumbo v3, "specific_pose_alert_retention_time"

    invoke-virtual {v2, v3, p3}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    .line 563
    const-string/jumbo v3, "specific_pose_alert_minimum_angle"

    move/from16 v0, p4

    invoke-virtual {v2, v3, v0}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    .line 564
    const-string/jumbo v3, "specific_pose_alert_maximum_angle"

    move/from16 v0, p5

    invoke-virtual {v2, v3, v0}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    .line 565
    const-string/jumbo v3, "specific_pose_alert_moving_thrs"

    move/from16 v0, p6

    invoke-virtual {v2, v3, v0}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    .line 566
    invoke-direct {p0, p1, v2}, Landroid/hardware/scontext/SContextManager;->registerListener(Landroid/hardware/scontext/SContextListener;Landroid/hardware/scontext/SContextProperty;)Z

    move-result v11

    :cond_5
    :goto_1
    move v3, v11

    .line 571
    goto :goto_0

    .line 567
    :cond_6
    const/16 v3, 0x9

    if-ne p2, v3, :cond_5

    .line 568
    move/from16 v0, p5

    int-to-double v8, v0

    move-object v3, p0

    move-object v4, p1

    move v5, p2

    move v6, p3

    move/from16 v7, p4

    move/from16 v10, p6

    invoke-virtual/range {v3 .. v10}, Landroid/hardware/scontext/SContextManager;->registerListener(Landroid/hardware/scontext/SContextListener;IIIDI)Z

    move-result v11

    goto :goto_1
.end method

.method public registerListener(Landroid/hardware/scontext/SContextListener;IIIIII)Z
    .locals 3
    .param p1, "listener"    # Landroid/hardware/scontext/SContextListener;
    .param p2, "service"    # I
    .param p3, "arg1"    # I
    .param p4, "arg2"    # I
    .param p5, "arg3"    # I
    .param p6, "arg4"    # I
    .param p7, "arg5"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 601
    const/4 v1, 0x0

    .line 602
    .local v1, "res":Z
    new-instance v0, Landroid/hardware/scontext/SContextProperty;

    invoke-direct {v0, p2}, Landroid/hardware/scontext/SContextProperty;-><init>(I)V

    .line 603
    .local v0, "property":Landroid/hardware/scontext/SContextProperty;
    const/16 v2, 0x18

    if-ne p2, v2, :cond_1

    .line 604
    const-string v2, "activity_location_logging_stop_period"

    invoke-virtual {v0, v2, p3}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    .line 605
    const-string v2, "activity_location_logging_wait_period"

    invoke-virtual {v0, v2, p4}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    .line 606
    const-string v2, "activity_location_logging_statying radius"

    invoke-virtual {v0, v2, p5}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    .line 607
    const-string v2, "activity_location_logging_area_radius"

    invoke-virtual {v0, v2, p6}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    .line 608
    const-string v2, "activity_location_logging_lpp_resolution"

    invoke-virtual {v0, v2, p7}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    .line 609
    invoke-direct {p0, p1, v0}, Landroid/hardware/scontext/SContextManager;->registerListener(Landroid/hardware/scontext/SContextListener;Landroid/hardware/scontext/SContextProperty;)Z

    move-result v1

    .line 619
    :cond_0
    :goto_0
    return v1

    .line 610
    :cond_1
    const/16 v2, 0x23

    if-ne p2, v2, :cond_0

    .line 611
    const-string v2, "inactive_timer_device_type"

    invoke-virtual {v0, v2, p3}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    .line 612
    const-string v2, "inactive_timer_duration"

    invoke-virtual {v0, v2, p4}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    .line 613
    const-string v2, "inactive_timer_alert_count"

    invoke-virtual {v0, v2, p5}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    .line 614
    const-string v2, "inactive_timer_start_time"

    invoke-virtual {v0, v2, p6}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    .line 615
    const-string v2, "inactive_timer_end_time"

    invoke-virtual {v0, v2, p7}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    .line 616
    invoke-direct {p0, p1, v0}, Landroid/hardware/scontext/SContextManager;->registerListener(Landroid/hardware/scontext/SContextListener;Landroid/hardware/scontext/SContextProperty;)Z

    move-result v1

    goto :goto_0
.end method

.method public registerListener(Landroid/hardware/scontext/SContextListener;IIIZ)Z
    .locals 6
    .param p1, "listener"    # Landroid/hardware/scontext/SContextListener;
    .param p2, "service"    # I
    .param p3, "arg1"    # I
    .param p4, "arg2"    # I
    .param p5, "arg3"    # Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/16 v5, 0x7f

    const/16 v4, -0x80

    const/4 v2, 0x0

    .line 461
    const/4 v1, 0x0

    .line 462
    .local v1, "res":Z
    new-instance v0, Landroid/hardware/scontext/SContextProperty;

    invoke-direct {v0, p2}, Landroid/hardware/scontext/SContextProperty;-><init>(I)V

    .line 463
    .local v0, "property":Landroid/hardware/scontext/SContextProperty;
    const/16 v3, 0x17

    if-ne p2, v3, :cond_4

    .line 464
    if-gt p3, v5, :cond_0

    if-ge p3, v4, :cond_1

    .line 466
    :cond_0
    const-string v3, "SContextManager"

    const-string v4, "Low temperature must be between -128 and 127."

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 485
    :goto_0
    return v2

    .line 469
    :cond_1
    if-gt p4, v5, :cond_2

    if-ge p4, v4, :cond_3

    .line 471
    :cond_2
    const-string v3, "SContextManager"

    const-string v4, "High temperature must be between -128 and 127."

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 474
    :cond_3
    if-ge p3, p4, :cond_5

    .line 475
    const-string/jumbo v2, "temperature_alert_low_temperature"

    invoke-virtual {v0, v2, p3}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    .line 476
    const-string/jumbo v2, "temperature_alert_high_temperature"

    invoke-virtual {v0, v2, p4}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    .line 477
    const-string/jumbo v2, "temperature_alert_is_including"

    invoke-virtual {v0, v2, p5}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;Z)V

    .line 482
    invoke-direct {p0, p1, v0}, Landroid/hardware/scontext/SContextManager;->registerListener(Landroid/hardware/scontext/SContextListener;Landroid/hardware/scontext/SContextProperty;)Z

    move-result v1

    :cond_4
    move v2, v1

    .line 485
    goto :goto_0

    .line 479
    :cond_5
    const-string v3, "SContextManager"

    const-string v4, "Low temperature must be less than high temperature."

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public registerListener(Landroid/hardware/scontext/SContextListener;ILandroid/hardware/scontext/SContextAttribute;)Z
    .locals 6
    .param p1, "listener"    # Landroid/hardware/scontext/SContextListener;
    .param p2, "service"    # I
    .param p3, "attribute"    # Landroid/hardware/scontext/SContextAttribute;

    .prologue
    .line 93
    new-instance v0, Landroid/hardware/scontext/SContextProperty;

    invoke-direct {v0, p2}, Landroid/hardware/scontext/SContextProperty;-><init>(I)V

    .line 94
    .local v0, "property":Landroid/hardware/scontext/SContextProperty;
    packed-switch p2, :pswitch_data_0

    .line 220
    .end local p3    # "attribute":Landroid/hardware/scontext/SContextAttribute;
    :goto_0
    :pswitch_0
    invoke-direct {p0, p1, v0}, Landroid/hardware/scontext/SContextManager;->registerListener(Landroid/hardware/scontext/SContextListener;Landroid/hardware/scontext/SContextProperty;)Z

    move-result v1

    .line 222
    .local v1, "res":Z
    return v1

    .line 96
    .end local v1    # "res":Z
    .restart local p3    # "attribute":Landroid/hardware/scontext/SContextAttribute;
    :pswitch_1
    const-string v3, "pedometer_gender"

    move-object v2, p3

    check-cast v2, Landroid/hardware/scontext/SContextPedometerAttribute;

    invoke-virtual {v2}, Landroid/hardware/scontext/SContextPedometerAttribute;->getGender()I

    move-result v2

    invoke-virtual {v0, v3, v2}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    .line 98
    const-string v3, "pedometer_height"

    move-object v2, p3

    check-cast v2, Landroid/hardware/scontext/SContextPedometerAttribute;

    invoke-virtual {v2}, Landroid/hardware/scontext/SContextPedometerAttribute;->getHeight()D

    move-result-wide v4

    invoke-virtual {v0, v3, v4, v5}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;D)V

    .line 100
    const-string v2, "pedometer_weight"

    check-cast p3, Landroid/hardware/scontext/SContextPedometerAttribute;

    .end local p3    # "attribute":Landroid/hardware/scontext/SContextAttribute;
    invoke-virtual {p3}, Landroid/hardware/scontext/SContextPedometerAttribute;->getWeight()D

    move-result-wide v4

    invoke-virtual {v0, v2, v4, v5}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;D)V

    goto :goto_0

    .line 105
    .restart local p3    # "attribute":Landroid/hardware/scontext/SContextAttribute;
    :pswitch_2
    const-string/jumbo v2, "step_cout_alert_step"

    check-cast p3, Landroid/hardware/scontext/SContextStepCountAlertAttribute;

    .end local p3    # "attribute":Landroid/hardware/scontext/SContextAttribute;
    invoke-virtual {p3}, Landroid/hardware/scontext/SContextStepCountAlertAttribute;->getStepCount()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    goto :goto_0

    .line 110
    .restart local p3    # "attribute":Landroid/hardware/scontext/SContextAttribute;
    :pswitch_3
    const-string v2, "auto_rotation_device_type"

    check-cast p3, Landroid/hardware/scontext/SContextAutoRotationAttribute;

    .end local p3    # "attribute":Landroid/hardware/scontext/SContextAttribute;
    invoke-virtual {p3}, Landroid/hardware/scontext/SContextAutoRotationAttribute;->getDeviceType()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    goto :goto_0

    .line 115
    .restart local p3    # "attribute":Landroid/hardware/scontext/SContextAttribute;
    :pswitch_4
    const-string v3, "environment_sensor_type"

    move-object v2, p3

    check-cast v2, Landroid/hardware/scontext/SContextEnvironmentAttribute;

    invoke-virtual {v2}, Landroid/hardware/scontext/SContextEnvironmentAttribute;->getSensorType()I

    move-result v2

    invoke-virtual {v0, v3, v2}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    .line 117
    const-string v2, "environment_update_interval"

    check-cast p3, Landroid/hardware/scontext/SContextEnvironmentAttribute;

    .end local p3    # "attribute":Landroid/hardware/scontext/SContextAttribute;
    invoke-virtual {p3}, Landroid/hardware/scontext/SContextEnvironmentAttribute;->getUpdateInterval()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    goto :goto_0

    .line 122
    .restart local p3    # "attribute":Landroid/hardware/scontext/SContextAttribute;
    :pswitch_5
    const-string v3, "mfp_nomove_duration_thrs"

    move-object v2, p3

    check-cast v2, Landroid/hardware/scontext/SContextMovementForPositioningAttribute;

    invoke-virtual {v2}, Landroid/hardware/scontext/SContextMovementForPositioningAttribute;->getNomoveDurationThrs()I

    move-result v2

    invoke-virtual {v0, v3, v2}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    .line 125
    const-string v3, "mfp_move_duration_thrs"

    move-object v2, p3

    check-cast v2, Landroid/hardware/scontext/SContextMovementForPositioningAttribute;

    invoke-virtual {v2}, Landroid/hardware/scontext/SContextMovementForPositioningAttribute;->getMoveDurationThrs()I

    move-result v2

    invoke-virtual {v0, v3, v2}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    .line 127
    const-string v3, "mfp_move_min_duration_thrs"

    move-object v2, p3

    check-cast v2, Landroid/hardware/scontext/SContextMovementForPositioningAttribute;

    invoke-virtual {v2}, Landroid/hardware/scontext/SContextMovementForPositioningAttribute;->getMoveMinDurationThrs()I

    move-result v2

    invoke-virtual {v0, v3, v2}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    .line 130
    const-string v2, "mfp_move_distance_thrs"

    check-cast p3, Landroid/hardware/scontext/SContextMovementForPositioningAttribute;

    .end local p3    # "attribute":Landroid/hardware/scontext/SContextAttribute;
    invoke-virtual {p3}, Landroid/hardware/scontext/SContextMovementForPositioningAttribute;->getMoveDistanceThrs()D

    move-result-wide v4

    invoke-virtual {v0, v2, v4, v5}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;D)V

    goto/16 :goto_0

    .line 135
    .restart local p3    # "attribute":Landroid/hardware/scontext/SContextAttribute;
    :pswitch_6
    const-string/jumbo v3, "shake_motion_strength"

    move-object v2, p3

    check-cast v2, Landroid/hardware/scontext/SContextShakeMotionAttribute;

    invoke-virtual {v2}, Landroid/hardware/scontext/SContextShakeMotionAttribute;->getStrength()I

    move-result v2

    invoke-virtual {v0, v3, v2}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    .line 137
    const-string/jumbo v2, "shake_motion_duration"

    check-cast p3, Landroid/hardware/scontext/SContextShakeMotionAttribute;

    .end local p3    # "attribute":Landroid/hardware/scontext/SContextAttribute;
    invoke-virtual {p3}, Landroid/hardware/scontext/SContextShakeMotionAttribute;->getDuration()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 142
    .restart local p3    # "attribute":Landroid/hardware/scontext/SContextAttribute;
    :pswitch_7
    const-string/jumbo v3, "temperature_alert_low_temperature"

    move-object v2, p3

    check-cast v2, Landroid/hardware/scontext/SContextTemperatureAlertAttribute;

    invoke-virtual {v2}, Landroid/hardware/scontext/SContextTemperatureAlertAttribute;->getLowTemperature()I

    move-result v2

    invoke-virtual {v0, v3, v2}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    .line 144
    const-string/jumbo v3, "temperature_alert_high_temperature"

    move-object v2, p3

    check-cast v2, Landroid/hardware/scontext/SContextTemperatureAlertAttribute;

    invoke-virtual {v2}, Landroid/hardware/scontext/SContextTemperatureAlertAttribute;->getHighTemperature()I

    move-result v2

    invoke-virtual {v0, v3, v2}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    .line 146
    const-string/jumbo v2, "temperature_alert_is_including"

    check-cast p3, Landroid/hardware/scontext/SContextTemperatureAlertAttribute;

    .end local p3    # "attribute":Landroid/hardware/scontext/SContextAttribute;
    invoke-virtual {p3}, Landroid/hardware/scontext/SContextTemperatureAlertAttribute;->isIncluding()Z

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 151
    .restart local p3    # "attribute":Landroid/hardware/scontext/SContextAttribute;
    :pswitch_8
    const-string v3, "activity_location_logging_stop_period"

    move-object v2, p3

    check-cast v2, Landroid/hardware/scontext/SContextActivityLocationLoggingAttribute;

    invoke-virtual {v2}, Landroid/hardware/scontext/SContextActivityLocationLoggingAttribute;->getStopPeriod()I

    move-result v2

    invoke-virtual {v0, v3, v2}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    .line 153
    const-string v3, "activity_location_logging_wait_period"

    move-object v2, p3

    check-cast v2, Landroid/hardware/scontext/SContextActivityLocationLoggingAttribute;

    invoke-virtual {v2}, Landroid/hardware/scontext/SContextActivityLocationLoggingAttribute;->getWitPeriod()I

    move-result v2

    invoke-virtual {v0, v3, v2}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    .line 155
    const-string v3, "activity_location_logging_statying radius"

    move-object v2, p3

    check-cast v2, Landroid/hardware/scontext/SContextActivityLocationLoggingAttribute;

    invoke-virtual {v2}, Landroid/hardware/scontext/SContextActivityLocationLoggingAttribute;->getStayingRadius()I

    move-result v2

    invoke-virtual {v0, v3, v2}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    .line 157
    const-string v3, "activity_location_logging_area_radius"

    move-object v2, p3

    check-cast v2, Landroid/hardware/scontext/SContextActivityLocationLoggingAttribute;

    invoke-virtual {v2}, Landroid/hardware/scontext/SContextActivityLocationLoggingAttribute;->getAreaRadius()I

    move-result v2

    invoke-virtual {v0, v3, v2}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    .line 159
    const-string v2, "activity_location_logging_lpp_resolution"

    check-cast p3, Landroid/hardware/scontext/SContextActivityLocationLoggingAttribute;

    .end local p3    # "attribute":Landroid/hardware/scontext/SContextAttribute;
    invoke-virtual {p3}, Landroid/hardware/scontext/SContextActivityLocationLoggingAttribute;->getLppResolution()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 164
    .restart local p3    # "attribute":Landroid/hardware/scontext/SContextAttribute;
    :pswitch_9
    const-string v2, "activity_notification_activity_filter"

    check-cast p3, Landroid/hardware/scontext/SContextActivityNotificationAttribute;

    .end local p3    # "attribute":Landroid/hardware/scontext/SContextAttribute;
    invoke-virtual {p3}, Landroid/hardware/scontext/SContextActivityNotificationAttribute;->getActivityFilter()[I

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;[I)V

    goto/16 :goto_0

    .line 169
    .restart local p3    # "attribute":Landroid/hardware/scontext/SContextAttribute;
    :pswitch_a
    const-string/jumbo v3, "specific_pose_alert_retention_time"

    move-object v2, p3

    check-cast v2, Landroid/hardware/scontext/SContextSpecificPoseAlertAttribute;

    invoke-virtual {v2}, Landroid/hardware/scontext/SContextSpecificPoseAlertAttribute;->getRetentionTime()I

    move-result v2

    invoke-virtual {v0, v3, v2}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    .line 171
    const-string/jumbo v3, "specific_pose_alert_minimum_angle"

    move-object v2, p3

    check-cast v2, Landroid/hardware/scontext/SContextSpecificPoseAlertAttribute;

    invoke-virtual {v2}, Landroid/hardware/scontext/SContextSpecificPoseAlertAttribute;->getMinimumAngle()I

    move-result v2

    invoke-virtual {v0, v3, v2}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    .line 173
    const-string/jumbo v3, "specific_pose_alert_maximum_angle"

    move-object v2, p3

    check-cast v2, Landroid/hardware/scontext/SContextSpecificPoseAlertAttribute;

    invoke-virtual {v2}, Landroid/hardware/scontext/SContextSpecificPoseAlertAttribute;->getMaximumAngle()I

    move-result v2

    invoke-virtual {v0, v3, v2}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    .line 175
    const-string/jumbo v2, "specific_pose_alert_moving_thrs"

    check-cast p3, Landroid/hardware/scontext/SContextSpecificPoseAlertAttribute;

    .end local p3    # "attribute":Landroid/hardware/scontext/SContextAttribute;
    invoke-virtual {p3}, Landroid/hardware/scontext/SContextSpecificPoseAlertAttribute;->getMovingThrs()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 180
    .restart local p3    # "attribute":Landroid/hardware/scontext/SContextAttribute;
    :pswitch_b
    const-string/jumbo v3, "sleep_monitor_sensibility"

    move-object v2, p3

    check-cast v2, Landroid/hardware/scontext/SContextSleepMonitorAttribute;

    invoke-virtual {v2}, Landroid/hardware/scontext/SContextSleepMonitorAttribute;->getSensibility()I

    move-result v2

    invoke-virtual {v0, v3, v2}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    .line 182
    const-string/jumbo v2, "sleep_monitor_sampling_interval"

    check-cast p3, Landroid/hardware/scontext/SContextSleepMonitorAttribute;

    .end local p3    # "attribute":Landroid/hardware/scontext/SContextAttribute;
    invoke-virtual {p3}, Landroid/hardware/scontext/SContextSleepMonitorAttribute;->getSamplingInterval()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 187
    .restart local p3    # "attribute":Landroid/hardware/scontext/SContextAttribute;
    :pswitch_c
    const-string v3, "activity_notification_ex_activity_filter"

    move-object v2, p3

    check-cast v2, Landroid/hardware/scontext/SContextActivityNotificationExAttribute;

    invoke-virtual {v2}, Landroid/hardware/scontext/SContextActivityNotificationExAttribute;->getActivityFilter()[I

    move-result-object v2

    invoke-virtual {v0, v3, v2}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;[I)V

    .line 189
    const-string v2, "activity_notification_ex_time_duration"

    check-cast p3, Landroid/hardware/scontext/SContextActivityNotificationExAttribute;

    .end local p3    # "attribute":Landroid/hardware/scontext/SContextAttribute;
    invoke-virtual {p3}, Landroid/hardware/scontext/SContextActivityNotificationExAttribute;->getDuration()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 194
    .restart local p3    # "attribute":Landroid/hardware/scontext/SContextAttribute;
    :pswitch_d
    const-string/jumbo v2, "step_level_monitor_duration"

    check-cast p3, Landroid/hardware/scontext/SContextStepLevelMonitorAttribute;

    .end local p3    # "attribute":Landroid/hardware/scontext/SContextAttribute;
    invoke-virtual {p3}, Landroid/hardware/scontext/SContextStepLevelMonitorAttribute;->getDuration()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 199
    .restart local p3    # "attribute":Landroid/hardware/scontext/SContextAttribute;
    :pswitch_e
    const-string v3, "inactive_timer_device_type"

    move-object v2, p3

    check-cast v2, Landroid/hardware/scontext/SContextInactiveTimerAttribute;

    invoke-virtual {v2}, Landroid/hardware/scontext/SContextInactiveTimerAttribute;->getDeviceType()I

    move-result v2

    invoke-virtual {v0, v3, v2}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    .line 201
    const-string v3, "inactive_timer_duration"

    move-object v2, p3

    check-cast v2, Landroid/hardware/scontext/SContextInactiveTimerAttribute;

    invoke-virtual {v2}, Landroid/hardware/scontext/SContextInactiveTimerAttribute;->getDuration()I

    move-result v2

    invoke-virtual {v0, v3, v2}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    .line 203
    const-string v3, "inactive_timer_alert_count"

    move-object v2, p3

    check-cast v2, Landroid/hardware/scontext/SContextInactiveTimerAttribute;

    invoke-virtual {v2}, Landroid/hardware/scontext/SContextInactiveTimerAttribute;->getAlertCount()I

    move-result v2

    invoke-virtual {v0, v3, v2}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    .line 205
    const-string v3, "inactive_timer_start_time"

    move-object v2, p3

    check-cast v2, Landroid/hardware/scontext/SContextInactiveTimerAttribute;

    invoke-virtual {v2}, Landroid/hardware/scontext/SContextInactiveTimerAttribute;->getStartTime()I

    move-result v2

    invoke-virtual {v0, v3, v2}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    .line 207
    const-string v2, "inactive_timer_end_time"

    check-cast p3, Landroid/hardware/scontext/SContextInactiveTimerAttribute;

    .end local p3    # "attribute":Landroid/hardware/scontext/SContextAttribute;
    invoke-virtual {p3}, Landroid/hardware/scontext/SContextInactiveTimerAttribute;->getEndTime()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 212
    .restart local p3    # "attribute":Landroid/hardware/scontext/SContextAttribute;
    :pswitch_f
    const-string v2, "flat_motion_for_table_mode_duration"

    check-cast p3, Landroid/hardware/scontext/SContextFlatMotionForTableModeAttribute;

    .end local p3    # "attribute":Landroid/hardware/scontext/SContextAttribute;
    invoke-virtual {p3}, Landroid/hardware/scontext/SContextFlatMotionForTableModeAttribute;->getDuration()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 94
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_7
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_0
        :pswitch_0
        :pswitch_d
        :pswitch_0
        :pswitch_e
        :pswitch_f
    .end packed-switch
.end method

.method public registerListener(Landroid/hardware/scontext/SContextListener;I[I)Z
    .locals 7
    .param p1, "listener"    # Landroid/hardware/scontext/SContextListener;
    .param p2, "service"    # I
    .param p3, "arg"    # [I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 286
    new-instance v3, Landroid/hardware/scontext/SContextProperty;

    invoke-direct {v3, p2}, Landroid/hardware/scontext/SContextProperty;-><init>(I)V

    .line 287
    .local v3, "property":Landroid/hardware/scontext/SContextProperty;
    const/16 v5, 0x1b

    if-ne p2, v5, :cond_3

    .line 288
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 289
    .local v2, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v5, p3

    if-ge v0, v5, :cond_2

    .line 290
    aget v5, p3, v0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 291
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    if-ge v1, v0, :cond_1

    .line 292
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    if-ne v5, v6, :cond_0

    .line 293
    const-string v5, "SContextManager"

    const-string v6, "This arg cannot have duplicated status."

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 294
    const/4 v4, 0x0

    .line 302
    .end local v0    # "i":I
    .end local v1    # "j":I
    .end local v2    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :goto_2
    return v4

    .line 291
    .restart local v0    # "i":I
    .restart local v1    # "j":I
    .restart local v2    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 289
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 298
    .end local v1    # "j":I
    :cond_2
    const-string v5, "activity_notification_activity_filter"

    invoke-virtual {v3, v5, p3}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;[I)V

    .line 300
    .end local v0    # "i":I
    .end local v2    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :cond_3
    invoke-direct {p0, p1, v3}, Landroid/hardware/scontext/SContextManager;->registerListener(Landroid/hardware/scontext/SContextListener;Landroid/hardware/scontext/SContextProperty;)Z

    move-result v4

    .line 302
    .local v4, "res":Z
    goto :goto_2
.end method

.method public registerListener(Landroid/hardware/scontext/SContextListener;I[II)Z
    .locals 3
    .param p1, "listener"    # Landroid/hardware/scontext/SContextListener;
    .param p2, "service"    # I
    .param p3, "arg1"    # [I
    .param p4, "arg2"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 321
    new-instance v0, Landroid/hardware/scontext/SContextProperty;

    invoke-direct {v0, p2}, Landroid/hardware/scontext/SContextProperty;-><init>(I)V

    .line 322
    .local v0, "property":Landroid/hardware/scontext/SContextProperty;
    const/16 v2, 0x1e

    if-ne p2, v2, :cond_0

    .line 323
    const-string v2, "activity_notification_ex_activity_filter"

    invoke-virtual {v0, v2, p3}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;[I)V

    .line 324
    const-string v2, "activity_notification_ex_time_duration"

    invoke-virtual {v0, v2, p4}, Landroid/hardware/scontext/SContextProperty;->setProperty(Ljava/lang/String;I)V

    .line 327
    :cond_0
    invoke-direct {p0, p1, v0}, Landroid/hardware/scontext/SContextManager;->registerListener(Landroid/hardware/scontext/SContextListener;Landroid/hardware/scontext/SContextProperty;)Z

    move-result v1

    .line 329
    .local v1, "res":Z
    return v1
.end method

.method public requestToUpdate(Landroid/hardware/scontext/SContextListener;I)V
    .locals 5
    .param p1, "listener"    # Landroid/hardware/scontext/SContextListener;
    .param p2, "service"    # I

    .prologue
    .line 937
    invoke-virtual {p0, p2}, Landroid/hardware/scontext/SContextManager;->isAvailableService(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 964
    :goto_0
    return-void

    .line 941
    :cond_0
    const/4 v2, 0x2

    if-eq p2, v2, :cond_1

    const/16 v2, 0x19

    if-eq p2, v2, :cond_1

    const/16 v2, 0x1a

    if-eq p2, v2, :cond_1

    const/16 v2, 0x1d

    if-eq p2, v2, :cond_1

    .line 945
    const-string v2, "SContextManager"

    const-string v3, "  .requestToUpdate : This service is not supported!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 949
    :cond_1
    invoke-direct {p0, p1}, Landroid/hardware/scontext/SContextManager;->getListenerDelegate(Landroid/hardware/scontext/SContextListener;)Landroid/hardware/scontext/SContextManager$SContextListenerDelegate;

    move-result-object v1

    .line 951
    .local v1, "scontextListener":Landroid/hardware/scontext/SContextManager$SContextListenerDelegate;
    if-nez v1, :cond_2

    .line 952
    const-string v2, "SContextManager"

    const-string v3, "  .requestToUpdate : SContextListener is null!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 957
    :cond_2
    :try_start_0
    iget-object v2, p0, Landroid/hardware/scontext/SContextManager;->mSContextService:Landroid/hardware/scontext/ISContextService;

    invoke-interface {v2, v1, p2}, Landroid/hardware/scontext/ISContextService;->requestToUpdate(Landroid/os/IBinder;I)V

    .line 958
    const-string v2, "SContextManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "  .requestToUpdate : listener = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", service="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p2}, Landroid/hardware/scontext/SContext;->getServiceName(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 961
    :catch_0
    move-exception v0

    .line 962
    .local v0, "e":Landroid/os/RemoteException;
    const-string v2, "SContextManager"

    const-string v3, "RemoteException in changeParameters: "

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public setReferenceData(ILandroid/hardware/scontext/SContextAttribute;)Z
    .locals 8
    .param p1, "service"    # I
    .param p2, "attribute"    # Landroid/hardware/scontext/SContextAttribute;

    .prologue
    .line 1042
    const/4 v4, 0x0

    .line 1043
    .local v4, "res":Z
    iget-object v6, p0, Landroid/hardware/scontext/SContextManager;->mSContextService:Landroid/hardware/scontext/ISContextService;

    if-nez v6, :cond_0

    move v5, v4

    .line 1068
    .end local v4    # "res":Z
    .end local p2    # "attribute":Landroid/hardware/scontext/SContextAttribute;
    .local v5, "res":I
    :goto_0
    return v5

    .line 1048
    .end local v5    # "res":I
    .restart local v4    # "res":Z
    .restart local p2    # "attribute":Landroid/hardware/scontext/SContextAttribute;
    :cond_0
    packed-switch p1, :pswitch_data_0

    .end local p2    # "attribute":Landroid/hardware/scontext/SContextAttribute;
    :cond_1
    :goto_1
    move v5, v4

    .line 1068
    .restart local v5    # "res":I
    goto :goto_0

    .line 1050
    .end local v5    # "res":I
    .restart local p2    # "attribute":Landroid/hardware/scontext/SContextAttribute;
    :pswitch_0
    :try_start_0
    move-object v0, p2

    check-cast v0, Landroid/hardware/scontext/SContextWakeUpVoiceAttribute;

    move-object v6, v0

    invoke-virtual {v6}, Landroid/hardware/scontext/SContextWakeUpVoiceAttribute;->getNetData()[B

    move-result-object v1

    .line 1051
    .local v1, "amData":[B
    check-cast p2, Landroid/hardware/scontext/SContextWakeUpVoiceAttribute;

    .end local p2    # "attribute":Landroid/hardware/scontext/SContextAttribute;
    invoke-virtual {p2}, Landroid/hardware/scontext/SContextWakeUpVoiceAttribute;->getGramData()[B

    move-result-object v3

    .line 1052
    .local v3, "lmData":[B
    if-eqz v1, :cond_2

    if-nez v3, :cond_3

    :cond_2
    move v5, v4

    .line 1053
    .restart local v5    # "res":I
    goto :goto_0

    .line 1055
    .end local v5    # "res":I
    :cond_3
    iget-object v6, p0, Landroid/hardware/scontext/SContextManager;->mSContextService:Landroid/hardware/scontext/ISContextService;

    const/4 v7, 0x1

    invoke-interface {v6, v7, v1}, Landroid/hardware/scontext/ISContextService;->setReferenceData(I[B)Z

    move-result v6

    if-eqz v6, :cond_1

    iget-object v6, p0, Landroid/hardware/scontext/SContextManager;->mSContextService:Landroid/hardware/scontext/ISContextService;

    const/4 v7, 0x2

    invoke-interface {v6, v7, v3}, Landroid/hardware/scontext/ISContextService;->setReferenceData(I[B)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v6

    if-eqz v6, :cond_1

    .line 1059
    const/4 v4, 0x1

    goto :goto_1

    .line 1065
    .end local v1    # "amData":[B
    .end local v3    # "lmData":[B
    :catch_0
    move-exception v2

    .line 1066
    .local v2, "e":Landroid/os/RemoteException;
    const-string v6, "SContextManager"

    const-string v7, "RemoteException in initializeSContextService: "

    invoke-static {v6, v7, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 1048
    nop

    :pswitch_data_0
    .packed-switch 0x10
        :pswitch_0
    .end packed-switch
.end method

.method public setReferenceData(I[B[B)Z
    .locals 5
    .param p1, "service"    # I
    .param p2, "data1"    # [B
    .param p3, "data2"    # [B
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1011
    const/4 v1, 0x0

    .line 1012
    .local v1, "res":Z
    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    iget-object v3, p0, Landroid/hardware/scontext/SContextManager;->mSContextService:Landroid/hardware/scontext/ISContextService;

    if-nez v3, :cond_1

    :cond_0
    move v2, v1

    .line 1030
    .end local v1    # "res":Z
    .local v2, "res":I
    :goto_0
    return v2

    .line 1017
    .end local v2    # "res":I
    .restart local v1    # "res":Z
    :cond_1
    const/16 v3, 0x10

    if-ne p1, v3, :cond_2

    .line 1018
    :try_start_0
    iget-object v3, p0, Landroid/hardware/scontext/SContextManager;->mSContextService:Landroid/hardware/scontext/ISContextService;

    const/4 v4, 0x1

    invoke-interface {v3, v4, p2}, Landroid/hardware/scontext/ISContextService;->setReferenceData(I[B)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Landroid/hardware/scontext/SContextManager;->mSContextService:Landroid/hardware/scontext/ISContextService;

    const/4 v4, 0x2

    invoke-interface {v3, v4, p3}, Landroid/hardware/scontext/ISContextService;->setReferenceData(I[B)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-eqz v3, :cond_3

    .line 1022
    const/4 v1, 0x1

    :cond_2
    :goto_1
    move v2, v1

    .line 1030
    .restart local v2    # "res":I
    goto :goto_0

    .line 1024
    .end local v2    # "res":I
    :cond_3
    const/4 v1, 0x0

    goto :goto_1

    .line 1027
    :catch_0
    move-exception v0

    .line 1028
    .local v0, "e":Landroid/os/RemoteException;
    const-string v3, "SContextManager"

    const-string v4, "RemoteException in initializeSContextService: "

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public unregisterListener(Landroid/hardware/scontext/SContextListener;)V
    .locals 7
    .param p1, "listener"    # Landroid/hardware/scontext/SContextListener;

    .prologue
    .line 661
    if-eqz p1, :cond_0

    iget-object v4, p0, Landroid/hardware/scontext/SContextManager;->mAvailableServiceMap:Ljava/util/HashMap;

    if-nez v4, :cond_1

    .line 683
    :cond_0
    :goto_0
    return-void

    .line 665
    :cond_1
    iget-object v4, p0, Landroid/hardware/scontext/SContextManager;->mAvailableServiceMap:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 666
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 667
    .local v3, "service":I
    invoke-direct {p0, p1}, Landroid/hardware/scontext/SContextManager;->getListenerDelegate(Landroid/hardware/scontext/SContextListener;)Landroid/hardware/scontext/SContextManager$SContextListenerDelegate;

    move-result-object v2

    .line 668
    .local v2, "scontextListener":Landroid/hardware/scontext/SContextManager$SContextListenerDelegate;
    if-eqz v2, :cond_0

    .line 673
    :try_start_0
    iget-object v4, p0, Landroid/hardware/scontext/SContextManager;->mSContextService:Landroid/hardware/scontext/ISContextService;

    invoke-interface {v4, v2, v3}, Landroid/hardware/scontext/ISContextService;->unregisterCallback(Landroid/os/IBinder;I)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 674
    iget-object v4, p0, Landroid/hardware/scontext/SContextManager;->mListenerDelegates:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v4, v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 675
    const-string v4, "SContextManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "  .unregisterListener : listener = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 678
    :catch_0
    move-exception v0

    .line 679
    .local v0, "e":Landroid/os/RemoteException;
    const-string v4, "SContextManager"

    const-string v5, "RemoteException in unregisterListener: "

    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public unregisterListener(Landroid/hardware/scontext/SContextListener;I)V
    .locals 5
    .param p1, "listener"    # Landroid/hardware/scontext/SContextListener;
    .param p2, "service"    # I

    .prologue
    .line 693
    invoke-direct {p0, p1, p2}, Landroid/hardware/scontext/SContextManager;->checkListenerAndService(Landroid/hardware/scontext/SContextListener;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 714
    :goto_0
    return-void

    .line 697
    :cond_0
    invoke-direct {p0, p1}, Landroid/hardware/scontext/SContextManager;->getListenerDelegate(Landroid/hardware/scontext/SContextListener;)Landroid/hardware/scontext/SContextManager$SContextListenerDelegate;

    move-result-object v1

    .line 699
    .local v1, "scontextListener":Landroid/hardware/scontext/SContextManager$SContextListenerDelegate;
    if-nez v1, :cond_1

    .line 700
    const-string v2, "SContextManager"

    const-string v3, "  .unregisterListener : SContextListener is null!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 705
    :cond_1
    :try_start_0
    iget-object v2, p0, Landroid/hardware/scontext/SContextManager;->mSContextService:Landroid/hardware/scontext/ISContextService;

    invoke-interface {v2, v1, p2}, Landroid/hardware/scontext/ISContextService;->unregisterCallback(Landroid/os/IBinder;I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 706
    iget-object v2, p0, Landroid/hardware/scontext/SContextManager;->mListenerDelegates:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 708
    :cond_2
    const-string v2, "SContextManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "  .unregisterListener : listener = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", service="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p2}, Landroid/hardware/scontext/SContext;->getServiceName(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 711
    :catch_0
    move-exception v0

    .line 712
    .local v0, "e":Landroid/os/RemoteException;
    const-string v2, "SContextManager"

    const-string v3, "RemoteException in unregisterListener: "

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
