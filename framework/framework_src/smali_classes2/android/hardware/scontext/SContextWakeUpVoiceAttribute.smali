.class public Landroid/hardware/scontext/SContextWakeUpVoiceAttribute;
.super Landroid/hardware/scontext/SContextAttribute;
.source "SContextWakeUpVoiceAttribute.java"


# instance fields
.field private mGramData:[B

.field private mNetData:[B


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Landroid/hardware/scontext/SContextAttribute;-><init>()V

    .line 31
    return-void
.end method

.method public constructor <init>([B[B)V
    .locals 0
    .param p1, "netData"    # [B
    .param p2, "gramData"    # [B

    .prologue
    .line 38
    invoke-direct {p0}, Landroid/hardware/scontext/SContextAttribute;-><init>()V

    .line 39
    iput-object p1, p0, Landroid/hardware/scontext/SContextWakeUpVoiceAttribute;->mNetData:[B

    .line 40
    iput-object p2, p0, Landroid/hardware/scontext/SContextWakeUpVoiceAttribute;->mGramData:[B

    .line 41
    return-void
.end method


# virtual methods
.method getGramData()[B
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Landroid/hardware/scontext/SContextWakeUpVoiceAttribute;->mGramData:[B

    return-object v0
.end method

.method getNetData()[B
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Landroid/hardware/scontext/SContextWakeUpVoiceAttribute;->mNetData:[B

    return-object v0
.end method
