.class public Landroid/hardware/scontext/SContextShakeMotionAttribute;
.super Landroid/hardware/scontext/SContextAttribute;
.source "SContextShakeMotionAttribute.java"


# instance fields
.field private mDurtaion:I

.field private mStrength:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Landroid/hardware/scontext/SContextAttribute;-><init>()V

    .line 23
    const/4 v0, 0x2

    iput v0, p0, Landroid/hardware/scontext/SContextShakeMotionAttribute;->mStrength:I

    .line 25
    const/16 v0, 0x320

    iput v0, p0, Landroid/hardware/scontext/SContextShakeMotionAttribute;->mDurtaion:I

    .line 31
    return-void
.end method

.method public constructor <init>(II)V
    .locals 1
    .param p1, "strength"    # I
    .param p2, "duration"    # I

    .prologue
    .line 40
    invoke-direct {p0}, Landroid/hardware/scontext/SContextAttribute;-><init>()V

    .line 23
    const/4 v0, 0x2

    iput v0, p0, Landroid/hardware/scontext/SContextShakeMotionAttribute;->mStrength:I

    .line 25
    const/16 v0, 0x320

    iput v0, p0, Landroid/hardware/scontext/SContextShakeMotionAttribute;->mDurtaion:I

    .line 41
    iput p1, p0, Landroid/hardware/scontext/SContextShakeMotionAttribute;->mStrength:I

    .line 42
    iput p2, p0, Landroid/hardware/scontext/SContextShakeMotionAttribute;->mDurtaion:I

    .line 43
    return-void
.end method


# virtual methods
.method getDuration()I
    .locals 1

    .prologue
    .line 50
    iget v0, p0, Landroid/hardware/scontext/SContextShakeMotionAttribute;->mDurtaion:I

    return v0
.end method

.method getStrength()I
    .locals 1

    .prologue
    .line 46
    iget v0, p0, Landroid/hardware/scontext/SContextShakeMotionAttribute;->mStrength:I

    return v0
.end method
