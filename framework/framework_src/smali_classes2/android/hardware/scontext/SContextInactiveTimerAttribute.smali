.class public Landroid/hardware/scontext/SContextInactiveTimerAttribute;
.super Landroid/hardware/scontext/SContextAttribute;
.source "SContextInactiveTimerAttribute.java"


# instance fields
.field private mAlertCount:I

.field private mDeviceType:I

.field private mDuration:I

.field private mEndTime:I

.field private mStartTime:I


# direct methods
.method constructor <init>()V
    .locals 3

    .prologue
    const/16 v2, 0x5dc

    const/4 v1, 0x1

    .line 36
    invoke-direct {p0}, Landroid/hardware/scontext/SContextAttribute;-><init>()V

    .line 23
    iput v1, p0, Landroid/hardware/scontext/SContextInactiveTimerAttribute;->mDeviceType:I

    .line 25
    const/16 v0, 0xe10

    iput v0, p0, Landroid/hardware/scontext/SContextInactiveTimerAttribute;->mDuration:I

    .line 27
    iput v1, p0, Landroid/hardware/scontext/SContextInactiveTimerAttribute;->mAlertCount:I

    .line 29
    iput v2, p0, Landroid/hardware/scontext/SContextInactiveTimerAttribute;->mStartTime:I

    .line 31
    iput v2, p0, Landroid/hardware/scontext/SContextInactiveTimerAttribute;->mEndTime:I

    .line 37
    return-void
.end method

.method public constructor <init>(IIIII)V
    .locals 3
    .param p1, "deviceType"    # I
    .param p2, "duration"    # I
    .param p3, "alertCount"    # I
    .param p4, "startTime"    # I
    .param p5, "endTime"    # I

    .prologue
    const/16 v2, 0x5dc

    const/4 v1, 0x1

    .line 48
    invoke-direct {p0}, Landroid/hardware/scontext/SContextAttribute;-><init>()V

    .line 23
    iput v1, p0, Landroid/hardware/scontext/SContextInactiveTimerAttribute;->mDeviceType:I

    .line 25
    const/16 v0, 0xe10

    iput v0, p0, Landroid/hardware/scontext/SContextInactiveTimerAttribute;->mDuration:I

    .line 27
    iput v1, p0, Landroid/hardware/scontext/SContextInactiveTimerAttribute;->mAlertCount:I

    .line 29
    iput v2, p0, Landroid/hardware/scontext/SContextInactiveTimerAttribute;->mStartTime:I

    .line 31
    iput v2, p0, Landroid/hardware/scontext/SContextInactiveTimerAttribute;->mEndTime:I

    .line 49
    iput p1, p0, Landroid/hardware/scontext/SContextInactiveTimerAttribute;->mDeviceType:I

    .line 50
    iput p2, p0, Landroid/hardware/scontext/SContextInactiveTimerAttribute;->mDuration:I

    .line 51
    iput p3, p0, Landroid/hardware/scontext/SContextInactiveTimerAttribute;->mAlertCount:I

    .line 52
    iput p4, p0, Landroid/hardware/scontext/SContextInactiveTimerAttribute;->mStartTime:I

    .line 53
    iput p5, p0, Landroid/hardware/scontext/SContextInactiveTimerAttribute;->mEndTime:I

    .line 54
    return-void
.end method


# virtual methods
.method getAlertCount()I
    .locals 1

    .prologue
    .line 65
    iget v0, p0, Landroid/hardware/scontext/SContextInactiveTimerAttribute;->mAlertCount:I

    return v0
.end method

.method getDeviceType()I
    .locals 1

    .prologue
    .line 57
    iget v0, p0, Landroid/hardware/scontext/SContextInactiveTimerAttribute;->mDeviceType:I

    return v0
.end method

.method getDuration()I
    .locals 1

    .prologue
    .line 61
    iget v0, p0, Landroid/hardware/scontext/SContextInactiveTimerAttribute;->mDuration:I

    return v0
.end method

.method getEndTime()I
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Landroid/hardware/scontext/SContextInactiveTimerAttribute;->mEndTime:I

    return v0
.end method

.method getStartTime()I
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Landroid/hardware/scontext/SContextInactiveTimerAttribute;->mStartTime:I

    return v0
.end method
