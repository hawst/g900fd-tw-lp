.class public Landroid/media/SamsungAudioService;
.super Ljava/lang/Object;
.source "SamsungAudioService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/media/SamsungAudioService$SamsungAudioServiceHandler;,
        Landroid/media/SamsungAudioService$SamsungAudioSettingsObserver;
    }
.end annotation


# static fields
.field private static final DEBUG_AUDIO:Z = true

.field private static final EARJACK_COUNT_PATH:Ljava/lang/String; = "/efs/FactoryApp/earjack_count"

.field private static final MSG_CHECK_EARCARE_STATE:I = 0x1

.field private static final MSG_END_SOUND_MANNER_MODE:I = 0x5

.field private static final MSG_PERFORM_SOFT_RESET:I = 0x2

.field private static final MSG_PLAY_SOUND_MANNER_MODE:I = 0x3

.field private static final MSG_USB_CHECK_RELEASE:I = 0x0

.field private static final MSG_VIBRATE_MANNER_MODE:I = 0x4

.field private static final SENDMSG_NOOP:I = 0x1

.field private static final SENDMSG_QUEUE:I = 0x2

.field private static final SENDMSG_REPLACE:I = 0x0

.field private static final TAG:Ljava/lang/String;

.field private static final TMS_ACTION_TYPE_START:I = 0x1

.field private static final TMS_ACTION_TYPE_STOP:I = 0x2


# instance fields
.field private emMgr:Lcom/sec/android/emergencymode/EmergencyManager;

.field private mAllSoundMute:I

.field private final mAudioService:Landroid/media/AudioService;

.field private mAutoHaptic:I

.field private final mContentResolver:Landroid/content/ContentResolver;

.field private final mContext:Landroid/content/Context;

.field private mCoverManager:Lcom/samsung/android/cover/CoverManager;

.field private mCpuCoreNumHelper:Landroid/os/DVFSHelper;

.field private mCpuHelper:Landroid/os/DVFSHelper;

.field private mDualMicMode:I

.field private mExternalUsbInfo:Ljava/lang/String;

.field private mFakeState:Z

.field private mIsEarCareEnabled:Z

.field private mIsEarCareSettingOn:Z

.field private mIsPlaySilentModeOff:Z

.field private mMonoMode:I

.field private mNaturalSound:I

.field private mOldIsSmartdock:Z

.field private mSamsungAudioServiceHandler:Landroid/media/SamsungAudioService$SamsungAudioServiceHandler;

.field mSamsungAudioServiceReceiver:Landroid/content/BroadcastReceiver;

.field private mSamsungAudioSettingsObserver:Landroid/media/SamsungAudioService$SamsungAudioSettingsObserver;

.field private mSilentModeOff:Z

.field private mSoundBalance:I

.field private mStatusbarExpanded:Z

.field private mSystemReady:Z

.field private final mUEventUsbConnectObserver:Landroid/os/UEventObserver;

.field private mUSBDetected:Z

.field private mUsbSupportedFormat:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    const-class v0, Landroid/media/SamsungAudioService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Landroid/media/SamsungAudioService;->TAG:Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>(Landroid/os/Looper;Landroid/content/Context;Landroid/media/AudioService$VolumeController;Landroid/media/AudioService;)V
    .locals 4
    .param p1, "looper"    # Landroid/os/Looper;
    .param p2, "cntxt"    # Landroid/content/Context;
    .param p3, "volumeCtrl"    # Landroid/media/AudioService$VolumeController;
    .param p4, "as"    # Landroid/media/AudioService;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 121
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    iput-object v3, p0, Landroid/media/SamsungAudioService;->mCpuHelper:Landroid/os/DVFSHelper;

    .line 84
    iput-object v3, p0, Landroid/media/SamsungAudioService;->mCpuCoreNumHelper:Landroid/os/DVFSHelper;

    .line 91
    iput-boolean v2, p0, Landroid/media/SamsungAudioService;->mIsEarCareSettingOn:Z

    .line 92
    iput-boolean v2, p0, Landroid/media/SamsungAudioService;->mIsEarCareEnabled:Z

    .line 95
    iput-object v3, p0, Landroid/media/SamsungAudioService;->emMgr:Lcom/sec/android/emergencymode/EmergencyManager;

    .line 98
    iput-boolean v2, p0, Landroid/media/SamsungAudioService;->mOldIsSmartdock:Z

    .line 100
    iput v2, p0, Landroid/media/SamsungAudioService;->mUsbSupportedFormat:I

    .line 102
    iput-object v3, p0, Landroid/media/SamsungAudioService;->mCoverManager:Lcom/samsung/android/cover/CoverManager;

    .line 103
    iput-boolean v2, p0, Landroid/media/SamsungAudioService;->mSilentModeOff:Z

    .line 104
    iput-boolean v2, p0, Landroid/media/SamsungAudioService;->mIsPlaySilentModeOff:Z

    .line 242
    new-instance v1, Landroid/media/SamsungAudioService$1;

    invoke-direct {v1, p0}, Landroid/media/SamsungAudioService$1;-><init>(Landroid/media/SamsungAudioService;)V

    iput-object v1, p0, Landroid/media/SamsungAudioService;->mSamsungAudioServiceReceiver:Landroid/content/BroadcastReceiver;

    .line 603
    new-instance v1, Landroid/media/SamsungAudioService$2;

    invoke-direct {v1, p0}, Landroid/media/SamsungAudioService$2;-><init>(Landroid/media/SamsungAudioService;)V

    iput-object v1, p0, Landroid/media/SamsungAudioService;->mUEventUsbConnectObserver:Landroid/os/UEventObserver;

    .line 122
    new-instance v1, Landroid/media/SamsungAudioService$SamsungAudioServiceHandler;

    invoke-direct {v1, p0, p1}, Landroid/media/SamsungAudioService$SamsungAudioServiceHandler;-><init>(Landroid/media/SamsungAudioService;Landroid/os/Looper;)V

    iput-object v1, p0, Landroid/media/SamsungAudioService;->mSamsungAudioServiceHandler:Landroid/media/SamsungAudioService$SamsungAudioServiceHandler;

    .line 123
    iput-object p2, p0, Landroid/media/SamsungAudioService;->mContext:Landroid/content/Context;

    .line 124
    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iput-object v1, p0, Landroid/media/SamsungAudioService;->mContentResolver:Landroid/content/ContentResolver;

    .line 125
    iput-object p4, p0, Landroid/media/SamsungAudioService;->mAudioService:Landroid/media/AudioService;

    .line 127
    iput-boolean v2, p0, Landroid/media/SamsungAudioService;->mUSBDetected:Z

    .line 128
    iput-boolean v2, p0, Landroid/media/SamsungAudioService;->mFakeState:Z

    .line 129
    new-instance v1, Landroid/media/SamsungAudioService$SamsungAudioSettingsObserver;

    invoke-direct {v1, p0}, Landroid/media/SamsungAudioService$SamsungAudioSettingsObserver;-><init>(Landroid/media/SamsungAudioService;)V

    iput-object v1, p0, Landroid/media/SamsungAudioService;->mSamsungAudioSettingsObserver:Landroid/media/SamsungAudioService$SamsungAudioSettingsObserver;

    .line 130
    iget-object v1, p0, Landroid/media/SamsungAudioService;->mUEventUsbConnectObserver:Landroid/os/UEventObserver;

    const-string v2, "USB_CONNECTION"

    invoke-virtual {v1, v2}, Landroid/os/UEventObserver;->startObserving(Ljava/lang/String;)V

    .line 132
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.samsung.android.app.audio.epinforequest"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 134
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.SETTINGS_SOFT_RESET"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 136
    const-string v1, "com.sec.tms.audio.server"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 137
    const-string v1, "android.intent.action.EMERGENCY_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 139
    const-string v1, "com.sec.factory.app.factorytest.FTA_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 140
    const-string v1, "com.sec.factory.app.factorytest.FTA_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 143
    const-string v1, "com.sec.android.intent.action.INTERNAL_SPEAKER"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 144
    const-string v1, "android.intent.action.EXTERNAL_USB_HEADSET_PLUG"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 146
    const-string v1, "android.intent.action.proximity_sensor"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 147
    const-string v1, "android.intent.action.WIFIDISPLAY_NOTI_CONNECTION_MODE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 149
    const-string v1, "android.settings.MONO_AUDIO_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 151
    const-string v1, "android.settings.ALL_SOUND_MUTE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 153
    const-string v1, "com.android.phone.NOISE_REDUCTION"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 155
    const-string v1, "com.android.phone.NATURAL_SOUND"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 156
    const-string v1, "com.android.systemui.statusbar.ANIMATING"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 157
    const-string v1, "com.android.systemui.statusbar.COLLAPSED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 159
    iget-object v1, p0, Landroid/media/SamsungAudioService;->mSamsungAudioServiceReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p2, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 161
    iget-object v1, p0, Landroid/media/SamsungAudioService;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/emergencymode/EmergencyManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/emergencymode/EmergencyManager;

    move-result-object v1

    iput-object v1, p0, Landroid/media/SamsungAudioService;->emMgr:Lcom/sec/android/emergencymode/EmergencyManager;

    .line 162
    iget-object v1, p0, Landroid/media/SamsungAudioService;->emMgr:Lcom/sec/android/emergencymode/EmergencyManager;

    iget-object v1, p0, Landroid/media/SamsungAudioService;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/emergencymode/EmergencyManager;->isEmergencyMode(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Landroid/media/SamsungAudioService;->emMgr:Lcom/sec/android/emergencymode/EmergencyManager;

    const/16 v2, 0x200

    invoke-virtual {v1, v2}, Lcom/sec/android/emergencymode/EmergencyManager;->checkModeType(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 163
    const-string v1, "emergency_mode=on"

    invoke-static {v1}, Landroid/media/AudioSystem;->setParameters(Ljava/lang/String;)I

    .line 167
    :cond_0
    iput-object v3, p0, Landroid/media/SamsungAudioService;->mExternalUsbInfo:Ljava/lang/String;

    .line 169
    new-instance v1, Lcom/samsung/android/cover/CoverManager;

    invoke-direct {v1, p2}, Lcom/samsung/android/cover/CoverManager;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Landroid/media/SamsungAudioService;->mCoverManager:Lcom/samsung/android/cover/CoverManager;

    .line 171
    return-void
.end method

.method static synthetic access$000(Landroid/media/SamsungAudioService;)Landroid/content/ContentResolver;
    .locals 1
    .param p0, "x0"    # Landroid/media/SamsungAudioService;

    .prologue
    .line 51
    iget-object v0, p0, Landroid/media/SamsungAudioService;->mContentResolver:Landroid/content/ContentResolver;

    return-object v0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    sget-object v0, Landroid/media/SamsungAudioService;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1000(Landroid/media/SamsungAudioService;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Landroid/media/SamsungAudioService;

    .prologue
    .line 51
    iget-object v0, p0, Landroid/media/SamsungAudioService;->mExternalUsbInfo:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1002(Landroid/media/SamsungAudioService;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Landroid/media/SamsungAudioService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 51
    iput-object p1, p0, Landroid/media/SamsungAudioService;->mExternalUsbInfo:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1100(Landroid/media/SamsungAudioService;)I
    .locals 1
    .param p0, "x0"    # Landroid/media/SamsungAudioService;

    .prologue
    .line 51
    iget v0, p0, Landroid/media/SamsungAudioService;->mMonoMode:I

    return v0
.end method

.method static synthetic access$1102(Landroid/media/SamsungAudioService;I)I
    .locals 0
    .param p0, "x0"    # Landroid/media/SamsungAudioService;
    .param p1, "x1"    # I

    .prologue
    .line 51
    iput p1, p0, Landroid/media/SamsungAudioService;->mMonoMode:I

    return p1
.end method

.method static synthetic access$1200(Landroid/media/SamsungAudioService;I)V
    .locals 0
    .param p0, "x0"    # Landroid/media/SamsungAudioService;
    .param p1, "x1"    # I

    .prologue
    .line 51
    invoke-direct {p0, p1}, Landroid/media/SamsungAudioService;->setMonoMode(I)V

    return-void
.end method

.method static synthetic access$1300(Landroid/media/SamsungAudioService;)I
    .locals 1
    .param p0, "x0"    # Landroid/media/SamsungAudioService;

    .prologue
    .line 51
    iget v0, p0, Landroid/media/SamsungAudioService;->mDualMicMode:I

    return v0
.end method

.method static synthetic access$1302(Landroid/media/SamsungAudioService;I)I
    .locals 0
    .param p0, "x0"    # Landroid/media/SamsungAudioService;
    .param p1, "x1"    # I

    .prologue
    .line 51
    iput p1, p0, Landroid/media/SamsungAudioService;->mDualMicMode:I

    return p1
.end method

.method static synthetic access$1400(Landroid/media/SamsungAudioService;I)V
    .locals 0
    .param p0, "x0"    # Landroid/media/SamsungAudioService;
    .param p1, "x1"    # I

    .prologue
    .line 51
    invoke-direct {p0, p1}, Landroid/media/SamsungAudioService;->setDualMicMode(I)V

    return-void
.end method

.method static synthetic access$1500(Landroid/media/SamsungAudioService;)I
    .locals 1
    .param p0, "x0"    # Landroid/media/SamsungAudioService;

    .prologue
    .line 51
    iget v0, p0, Landroid/media/SamsungAudioService;->mNaturalSound:I

    return v0
.end method

.method static synthetic access$1502(Landroid/media/SamsungAudioService;I)I
    .locals 0
    .param p0, "x0"    # Landroid/media/SamsungAudioService;
    .param p1, "x1"    # I

    .prologue
    .line 51
    iput p1, p0, Landroid/media/SamsungAudioService;->mNaturalSound:I

    return p1
.end method

.method static synthetic access$1600(Landroid/media/SamsungAudioService;I)V
    .locals 0
    .param p0, "x0"    # Landroid/media/SamsungAudioService;
    .param p1, "x1"    # I

    .prologue
    .line 51
    invoke-direct {p0, p1}, Landroid/media/SamsungAudioService;->setNaturalSoundMode(I)V

    return-void
.end method

.method static synthetic access$1702(Landroid/media/SamsungAudioService;I)I
    .locals 0
    .param p0, "x0"    # Landroid/media/SamsungAudioService;
    .param p1, "x1"    # I

    .prologue
    .line 51
    iput p1, p0, Landroid/media/SamsungAudioService;->mAllSoundMute:I

    return p1
.end method

.method static synthetic access$1802(Landroid/media/SamsungAudioService;Z)Z
    .locals 0
    .param p0, "x0"    # Landroid/media/SamsungAudioService;
    .param p1, "x1"    # Z

    .prologue
    .line 51
    iput-boolean p1, p0, Landroid/media/SamsungAudioService;->mStatusbarExpanded:Z

    return p1
.end method

.method static synthetic access$1900(Landroid/media/SamsungAudioService;)Z
    .locals 1
    .param p0, "x0"    # Landroid/media/SamsungAudioService;

    .prologue
    .line 51
    iget-boolean v0, p0, Landroid/media/SamsungAudioService;->mSystemReady:Z

    return v0
.end method

.method static synthetic access$200(Landroid/media/SamsungAudioService;)Z
    .locals 1
    .param p0, "x0"    # Landroid/media/SamsungAudioService;

    .prologue
    .line 51
    iget-boolean v0, p0, Landroid/media/SamsungAudioService;->mFakeState:Z

    return v0
.end method

.method static synthetic access$2000(Landroid/media/SamsungAudioService;)Z
    .locals 1
    .param p0, "x0"    # Landroid/media/SamsungAudioService;

    .prologue
    .line 51
    invoke-direct {p0}, Landroid/media/SamsungAudioService;->isUSBCheckStreamActive()Z

    move-result v0

    return v0
.end method

.method static synthetic access$202(Landroid/media/SamsungAudioService;Z)Z
    .locals 0
    .param p0, "x0"    # Landroid/media/SamsungAudioService;
    .param p1, "x1"    # Z

    .prologue
    .line 51
    iput-boolean p1, p0, Landroid/media/SamsungAudioService;->mFakeState:Z

    return p1
.end method

.method static synthetic access$2100(Landroid/media/SamsungAudioService;)Z
    .locals 1
    .param p0, "x0"    # Landroid/media/SamsungAudioService;

    .prologue
    .line 51
    iget-boolean v0, p0, Landroid/media/SamsungAudioService;->mUSBDetected:Z

    return v0
.end method

.method static synthetic access$2102(Landroid/media/SamsungAudioService;Z)Z
    .locals 0
    .param p0, "x0"    # Landroid/media/SamsungAudioService;
    .param p1, "x1"    # Z

    .prologue
    .line 51
    iput-boolean p1, p0, Landroid/media/SamsungAudioService;->mUSBDetected:Z

    return p1
.end method

.method static synthetic access$300(Landroid/media/SamsungAudioService;)V
    .locals 0
    .param p0, "x0"    # Landroid/media/SamsungAudioService;

    .prologue
    .line 51
    invoke-direct {p0}, Landroid/media/SamsungAudioService;->playSilentModeSound()V

    return-void
.end method

.method static synthetic access$402(Landroid/media/SamsungAudioService;Z)Z
    .locals 0
    .param p0, "x0"    # Landroid/media/SamsungAudioService;
    .param p1, "x1"    # Z

    .prologue
    .line 51
    iput-boolean p1, p0, Landroid/media/SamsungAudioService;->mIsPlaySilentModeOff:Z

    return p1
.end method

.method static synthetic access$502(Landroid/media/SamsungAudioService;Z)Z
    .locals 0
    .param p0, "x0"    # Landroid/media/SamsungAudioService;
    .param p1, "x1"    # Z

    .prologue
    .line 51
    iput-boolean p1, p0, Landroid/media/SamsungAudioService;->mIsEarCareSettingOn:Z

    return p1
.end method

.method static synthetic access$600(Landroid/media/SamsungAudioService;)Landroid/media/SamsungAudioService$SamsungAudioServiceHandler;
    .locals 1
    .param p0, "x0"    # Landroid/media/SamsungAudioService;

    .prologue
    .line 51
    iget-object v0, p0, Landroid/media/SamsungAudioService;->mSamsungAudioServiceHandler:Landroid/media/SamsungAudioService$SamsungAudioServiceHandler;

    return-object v0
.end method

.method static synthetic access$700(Landroid/os/Handler;IIIILjava/lang/Object;I)V
    .locals 0
    .param p0, "x0"    # Landroid/os/Handler;
    .param p1, "x1"    # I
    .param p2, "x2"    # I
    .param p3, "x3"    # I
    .param p4, "x4"    # I
    .param p5, "x5"    # Ljava/lang/Object;
    .param p6, "x6"    # I

    .prologue
    .line 51
    invoke-static/range {p0 .. p6}, Landroid/media/SamsungAudioService;->sendMsg(Landroid/os/Handler;IIIILjava/lang/Object;I)V

    return-void
.end method

.method static synthetic access$800(Landroid/media/SamsungAudioService;)Landroid/media/AudioService;
    .locals 1
    .param p0, "x0"    # Landroid/media/SamsungAudioService;

    .prologue
    .line 51
    iget-object v0, p0, Landroid/media/SamsungAudioService;->mAudioService:Landroid/media/AudioService;

    return-object v0
.end method

.method static synthetic access$900(Landroid/media/SamsungAudioService;)Lcom/sec/android/emergencymode/EmergencyManager;
    .locals 1
    .param p0, "x0"    # Landroid/media/SamsungAudioService;

    .prologue
    .line 51
    iget-object v0, p0, Landroid/media/SamsungAudioService;->emMgr:Lcom/sec/android/emergencymode/EmergencyManager;

    return-object v0
.end method

.method private getActiveStreamCount()I
    .locals 3

    .prologue
    .line 753
    const/4 v0, 0x0

    .line 754
    .local v0, "nReturn":I
    const/4 v1, 0x0

    .local v1, "nStreamNum":I
    :goto_0
    invoke-static {}, Landroid/media/AudioSystem;->getNumStreamTypes()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 755
    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/media/AudioSystem;->isStreamActive(II)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 756
    add-int/lit8 v0, v0, 0x1

    .line 754
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 759
    :cond_1
    return v0
.end method

.method protected static getFactoryMode()Z
    .locals 5

    .prologue
    .line 788
    const/4 v1, 0x0

    .line 790
    .local v1, "userMode":Ljava/lang/String;
    :try_start_0
    new-instance v2, Ljava/io/File;

    const-string v3, "/efs/FactoryApp/factorymode"

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/16 v3, 0x20

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/os/FileUtils;->readTextFile(Ljava/io/File;ILjava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 795
    :goto_0
    if-eqz v1, :cond_0

    const-string v2, "ON"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 796
    const/4 v2, 0x0

    .line 799
    :goto_1
    return v2

    .line 791
    :catch_0
    move-exception v0

    .line 792
    .local v0, "e1":Ljava/io/IOException;
    const-string v1, "OFF"

    .line 793
    sget-object v2, Landroid/media/SamsungAudioService;->TAG:Ljava/lang/String;

    const-string v3, "cannot open file : /efs/FactoryApp/factorymode "

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 798
    .end local v0    # "e1":Ljava/io/IOException;
    :cond_0
    sget-object v2, Landroid/media/SamsungAudioService;->TAG:Ljava/lang/String;

    const-string v3, "Current mode is Factorymode, So Popup UI will not be apear"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 799
    const/4 v2, 0x1

    goto :goto_1
.end method

.method private isUSBCheckStreamActive()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 598
    const/4 v1, 0x4

    invoke-static {v1, v0}, Landroid/media/AudioSystem;->isStreamActive(II)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x5

    invoke-static {v1, v0}, Landroid/media/AudioSystem;->isStreamActive(II)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x2

    invoke-static {v1, v0}, Landroid/media/AudioSystem;->isStreamActive(II)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private playSilentModeSound()V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 871
    iget-object v0, p0, Landroid/media/SamsungAudioService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "sound_effects_enabled"

    const/4 v4, -0x2

    invoke-static {v0, v1, v3, v4}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    if-nez v0, :cond_1

    .line 880
    :cond_0
    :goto_0
    return-void

    .line 874
    :cond_1
    iget-boolean v0, p0, Landroid/media/SamsungAudioService;->mSilentModeOff:Z

    if-nez v0, :cond_0

    .line 877
    iput-boolean v2, p0, Landroid/media/SamsungAudioService;->mIsPlaySilentModeOff:Z

    .line 878
    iget-object v0, p0, Landroid/media/SamsungAudioService;->mAudioService:Landroid/media/AudioService;

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/media/AudioService;->playSoundEffect(I)V

    .line 879
    iget-object v0, p0, Landroid/media/SamsungAudioService;->mSamsungAudioServiceHandler:Landroid/media/SamsungAudioService$SamsungAudioServiceHandler;

    const/4 v1, 0x5

    const/4 v5, 0x0

    const/16 v6, 0x1f4

    move v4, v3

    invoke-static/range {v0 .. v6}, Landroid/media/SamsungAudioService;->sendMsg(Landroid/os/Handler;IIIILjava/lang/Object;I)V

    goto :goto_0
.end method

.method private static sendMsg(Landroid/os/Handler;IIIILjava/lang/Object;I)V
    .locals 4
    .param p0, "handler"    # Landroid/os/Handler;
    .param p1, "msg"    # I
    .param p2, "existingMsgPolicy"    # I
    .param p3, "arg1"    # I
    .param p4, "arg2"    # I
    .param p5, "obj"    # Ljava/lang/Object;
    .param p6, "delay"    # I

    .prologue
    .line 419
    if-nez p2, :cond_1

    .line 420
    invoke-virtual {p0, p1}, Landroid/os/Handler;->removeMessages(I)V

    .line 425
    :cond_0
    invoke-virtual {p0, p1, p3, p4, p5}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    int-to-long v2, p6

    invoke-virtual {p0, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 426
    :goto_0
    return-void

    .line 421
    :cond_1
    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    invoke-virtual {p0, p1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0
.end method

.method private setAutoHaptic(I)V
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 943
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "audioParam;auto_haptic_enable="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/media/AudioSystem;->setParameters(Ljava/lang/String;)I

    .line 945
    return-void
.end method

.method private setDualMicMode(I)V
    .locals 1
    .param p1, "dualmicMode"    # I

    .prologue
    .line 910
    if-lez p1, :cond_0

    .line 911
    const-string v0, "dualmic_enabled=true"

    invoke-static {v0}, Landroid/media/AudioSystem;->setParameters(Ljava/lang/String;)I

    .line 916
    :goto_0
    return-void

    .line 913
    :cond_0
    const-string v0, "dualmic_enabled=false"

    invoke-static {v0}, Landroid/media/AudioSystem;->setParameters(Ljava/lang/String;)I

    goto :goto_0
.end method

.method private setMonoMode(I)V
    .locals 2
    .param p1, "monoMode"    # I

    .prologue
    .line 903
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "toMono="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/media/AudioSystem;->setParameters(Ljava/lang/String;)I

    .line 904
    return-void
.end method

.method private setNaturalSoundMode(I)V
    .locals 3
    .param p1, "mode"    # I

    .prologue
    .line 921
    sget-object v0, Landroid/media/SamsungAudioService;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "setNaturalSoundMode() - mode:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 922
    if-lez p1, :cond_0

    .line 923
    const-string v0, "bwe=on"

    invoke-static {v0}, Landroid/media/AudioSystem;->setParameters(Ljava/lang/String;)I

    .line 927
    :goto_0
    return-void

    .line 925
    :cond_0
    const-string v0, "bwe=off"

    invoke-static {v0}, Landroid/media/AudioSystem;->setParameters(Ljava/lang/String;)I

    goto :goto_0
.end method

.method private setSoundBalance()V
    .locals 5

    .prologue
    .line 950
    iget-object v1, p0, Landroid/media/SamsungAudioService;->mContentResolver:Landroid/content/ContentResolver;

    const-string/jumbo v2, "sound_balance"

    const/16 v3, 0x32

    const/4 v4, -0x2

    invoke-static {v1, v2, v3, v4}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v0

    .line 951
    .local v0, "soundBalance":I
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "sound_balance="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/media/AudioSystem;->setParameters(Ljava/lang/String;)I

    .line 952
    return-void
.end method


# virtual methods
.method protected ChangeDefaultVolume()V
    .locals 6

    .prologue
    const/16 v5, 0x9

    const/4 v4, 0x7

    const/4 v3, 0x4

    const/16 v2, 0xb

    .line 771
    sget-object v0, Landroid/media/AudioManager;->DEFAULT_STREAM_VOLUME:[I

    const/4 v1, 0x0

    aput v3, v0, v1

    .line 772
    sget-object v0, Landroid/media/AudioManager;->DEFAULT_STREAM_VOLUME:[I

    const/4 v1, 0x1

    aput v5, v0, v1

    .line 773
    sget-object v0, Landroid/media/AudioManager;->DEFAULT_STREAM_VOLUME:[I

    const/4 v1, 0x2

    aput v2, v0, v1

    .line 774
    sget-object v0, Landroid/media/AudioManager;->DEFAULT_STREAM_VOLUME:[I

    const/4 v1, 0x3

    aput v5, v0, v1

    .line 775
    sget-object v0, Landroid/media/AudioManager;->DEFAULT_STREAM_VOLUME:[I

    aput v2, v0, v3

    .line 776
    sget-object v0, Landroid/media/AudioManager;->DEFAULT_STREAM_VOLUME:[I

    const/4 v1, 0x5

    aput v2, v0, v1

    .line 777
    sget-object v0, Landroid/media/AudioManager;->DEFAULT_STREAM_VOLUME:[I

    const/4 v1, 0x6

    aput v4, v0, v1

    .line 778
    sget-object v0, Landroid/media/AudioManager;->DEFAULT_STREAM_VOLUME:[I

    const/16 v1, 0xe

    aput v1, v0, v4

    .line 779
    sget-object v0, Landroid/media/AudioManager;->DEFAULT_STREAM_VOLUME:[I

    const/16 v1, 0x8

    aput v2, v0, v1

    .line 780
    sget-object v0, Landroid/media/AudioManager;->DEFAULT_STREAM_VOLUME:[I

    aput v2, v0, v5

    .line 781
    sget-object v0, Landroid/media/AudioManager;->DEFAULT_STREAM_VOLUME:[I

    const/16 v1, 0xa

    aput v4, v0, v1

    .line 782
    sget-object v0, Landroid/media/AudioManager;->DEFAULT_STREAM_VOLUME:[I

    aput v3, v0, v2

    .line 783
    sget-object v0, Landroid/media/AudioManager;->DEFAULT_STREAM_VOLUME:[I

    const/16 v1, 0xc

    aput v3, v0, v1

    .line 785
    return-void
.end method

.method protected checkAndSendEarCareInfo()V
    .locals 14

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 705
    iget-boolean v0, p0, Landroid/media/SamsungAudioService;->mSystemReady:Z

    if-nez v0, :cond_1

    .line 750
    :cond_0
    :goto_0
    return-void

    .line 707
    :cond_1
    new-instance v7, Landroid/content/Intent;

    const-string v0, "com.samsung.android.app.audio.epinforesponse"

    invoke-direct {v7, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 710
    .local v7, "broadcast":Landroid/content/Intent;
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 711
    .local v8, "extras":Landroid/os/Bundle;
    iget-object v0, p0, Landroid/media/SamsungAudioService;->mAudioService:Landroid/media/AudioService;

    const/16 v2, 0x80

    invoke-virtual {v0, v2}, Landroid/media/AudioService;->isDeviceConnected(I)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Landroid/media/SamsungAudioService;->mAudioService:Landroid/media/AudioService;

    const/16 v2, 0x100

    invoke-virtual {v0, v2}, Landroid/media/AudioService;->isDeviceConnected(I)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_2
    move v9, v1

    .line 713
    .local v9, "isDeviceConnected":Z
    :goto_1
    iget-boolean v0, p0, Landroid/media/SamsungAudioService;->mIsEarCareSettingOn:Z

    if-eqz v0, :cond_6

    if-eqz v9, :cond_6

    .line 714
    const/4 v11, 0x0

    .line 715
    .local v11, "nState":I
    invoke-direct {p0}, Landroid/media/SamsungAudioService;->getActiveStreamCount()I

    move-result v0

    if-lez v0, :cond_5

    .line 716
    const-string v0, "earcare=on"

    invoke-static {v0}, Landroid/media/AudioSystem;->setParameters(Ljava/lang/String;)I

    .line 717
    const/4 v11, 0x1

    .line 722
    :goto_2
    const-string v0, "earcare_percent"

    invoke-static {v0}, Landroid/media/AudioSystem;->getParameters(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 723
    .local v13, "strState":Ljava/lang/String;
    if-eqz v13, :cond_3

    const-string v0, ""

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 724
    const-string v12, "earcare_percent="

    .line 725
    .local v12, "strKey":Ljava/lang/String;
    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v2

    if-le v0, v2, :cond_0

    .line 726
    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {v13, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v13

    .line 729
    invoke-static {v13}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v10

    .line 730
    .local v10, "nPercent":I
    sget-object v0, Landroid/media/SamsungAudioService;->TAG:Ljava/lang/String;

    const-string v2, "checkAndSendEarCareInfo() - send intent"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 731
    const-string/jumbo v0, "state"

    invoke-virtual {v8, v0, v11}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 732
    const-string/jumbo v0, "percent"

    invoke-virtual {v8, v0, v10}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 733
    invoke-virtual {v7, v8}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 734
    iget-object v0, p0, Landroid/media/SamsungAudioService;->mAudioService:Landroid/media/AudioService;

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v0, v7, v2}, Landroid/media/AudioService;->sendBroadcastToUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 735
    sget-object v0, Landroid/media/SamsungAudioService;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "checkAndSendEarCareInfo() state: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " percent: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 747
    .end local v10    # "nPercent":I
    .end local v12    # "strKey":Ljava/lang/String;
    :cond_3
    sget-object v0, Landroid/media/SamsungAudioService;->TAG:Ljava/lang/String;

    const-string v2, "checkAndSendEarCareInfo() - Loop"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 749
    iget-object v0, p0, Landroid/media/SamsungAudioService;->mSamsungAudioServiceHandler:Landroid/media/SamsungAudioService$SamsungAudioServiceHandler;

    const/4 v5, 0x0

    const v6, 0xea60

    move v2, v1

    move v4, v3

    invoke-static/range {v0 .. v6}, Landroid/media/SamsungAudioService;->sendMsg(Landroid/os/Handler;IIIILjava/lang/Object;I)V

    goto/16 :goto_0

    .end local v9    # "isDeviceConnected":Z
    .end local v11    # "nState":I
    .end local v13    # "strState":Ljava/lang/String;
    :cond_4
    move v9, v3

    .line 711
    goto/16 :goto_1

    .line 719
    .restart local v9    # "isDeviceConnected":Z
    .restart local v11    # "nState":I
    :cond_5
    const-string v0, "earcare=noActive"

    invoke-static {v0}, Landroid/media/AudioSystem;->setParameters(Ljava/lang/String;)I

    .line 720
    const/4 v11, 0x0

    goto/16 :goto_2

    .line 739
    .end local v11    # "nState":I
    :cond_6
    sget-object v0, Landroid/media/SamsungAudioService;->TAG:Ljava/lang/String;

    const-string v2, "checkAndSendEarCareInfo() - send off"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 740
    const-string v0, "earcare=off"

    invoke-static {v0}, Landroid/media/AudioSystem;->setParameters(Ljava/lang/String;)I

    .line 741
    const-string/jumbo v0, "state"

    invoke-virtual {v8, v0, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 742
    invoke-virtual {v7, v8}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 743
    iget-object v0, p0, Landroid/media/SamsungAudioService;->mAudioService:Landroid/media/AudioService;

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v0, v7, v2}, Landroid/media/AudioService;->sendBroadcastToUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 744
    iget-object v0, p0, Landroid/media/SamsungAudioService;->mSamsungAudioServiceHandler:Landroid/media/SamsungAudioService$SamsungAudioServiceHandler;

    invoke-virtual {v0, v1}, Landroid/media/SamsungAudioService$SamsungAudioServiceHandler;->removeMessages(I)V

    goto/16 :goto_0
.end method

.method protected dump(Ljava/io/PrintWriter;)V
    .locals 1
    .param p1, "pw"    # Ljava/io/PrintWriter;

    .prologue
    .line 175
    const-string v0, "\nSamsungAudioService dump:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 176
    return-void
.end method

.method protected getAllSoundMute()I
    .locals 1

    .prologue
    .line 936
    iget v0, p0, Landroid/media/SamsungAudioService;->mAllSoundMute:I

    return v0
.end method

.method protected getCurOutDevice()I
    .locals 3

    .prologue
    .line 818
    const-string v1, "audioParam;outDevice"

    invoke-static {v1}, Landroid/media/AudioSystem;->getParameters(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 819
    .local v0, "path":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v1, ""

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 820
    :cond_0
    sget-object v1, Landroid/media/SamsungAudioService;->TAG:Ljava/lang/String;

    const-string v2, "getCurOutDevice : Can\'t get outDevice"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 821
    const/4 v1, -0x1

    .line 823
    :goto_0
    return v1

    :cond_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_0
.end method

.method protected increaseEARCount()V
    .locals 8

    .prologue
    .line 828
    :try_start_0
    new-instance v4, Ljava/io/File;

    const-string v5, "/efs/FactoryApp/earjack_count"

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-static {v4, v5, v6}, Landroid/os/FileUtils;->readTextFile(Ljava/io/File;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 829
    .local v1, "strEARCount":Ljava/lang/String;
    if-eqz v1, :cond_0

    const-string v4, ""

    if-eq v1, v4, :cond_0

    .line 830
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    const-wide/16 v6, 0x1

    add-long v2, v4, v6

    .line 831
    .local v2, "earjack_count":J
    const-string v4, "/efs/FactoryApp/earjack_count"

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/os/FileUtils;->stringToFile(Ljava/lang/String;Ljava/lang/String;)V

    .line 840
    .end local v1    # "strEARCount":Ljava/lang/String;
    .end local v2    # "earjack_count":J
    :goto_0
    return-void

    .line 833
    .restart local v1    # "strEARCount":Ljava/lang/String;
    :cond_0
    const-string v4, "/efs/FactoryApp/earjack_count"

    const-string v5, "0"

    invoke-static {v4, v5}, Landroid/os/FileUtils;->stringToFile(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 835
    .end local v1    # "strEARCount":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 836
    .local v0, "e":Ljava/io/IOException;
    sget-object v4, Landroid/media/SamsungAudioService;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "IOException : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 837
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 838
    .local v0, "e":Ljava/lang/NumberFormatException;
    sget-object v4, Landroid/media/SamsungAudioService;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "NumberFormatException : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected initCPUBoost()V
    .locals 0

    .prologue
    .line 672
    return-void
.end method

.method protected isCoverOpen()Z
    .locals 2

    .prologue
    .line 844
    iget-object v1, p0, Landroid/media/SamsungAudioService;->mCoverManager:Lcom/samsung/android/cover/CoverManager;

    invoke-virtual {v1}, Lcom/samsung/android/cover/CoverManager;->getCoverState()Lcom/samsung/android/cover/CoverState;

    move-result-object v0

    .line 845
    .local v0, "state":Lcom/samsung/android/cover/CoverState;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/samsung/android/cover/CoverState;->getSwitchState()Z

    move-result v1

    if-nez v1, :cond_0

    .line 846
    const/4 v1, 0x0

    .line 847
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method protected isFactorySim()Z
    .locals 7

    .prologue
    .line 804
    :try_start_0
    iget-object v4, p0, Landroid/media/SamsungAudioService;->mContext:Landroid/content/Context;

    const-string/jumbo v5, "phone"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/telephony/TelephonyManager;

    .line 805
    .local v3, "tm":Landroid/telephony/TelephonyManager;
    const-string v0, "999999999999999"

    .line 806
    .local v0, "IMSI":Ljava/lang/String;
    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getSubscriberId()Ljava/lang/String;

    move-result-object v2

    .line 807
    .local v2, "imsi":Ljava/lang/String;
    if-eqz v2, :cond_0

    const-string v4, "999999999999999"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 808
    sget-object v4, Landroid/media/SamsungAudioService;->TAG:Ljava/lang/String;

    const-string v5, "Factory SIM is used now, So Popup UI will not be apear"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 809
    const/4 v4, 0x1

    .line 814
    .end local v0    # "IMSI":Ljava/lang/String;
    .end local v2    # "imsi":Ljava/lang/String;
    .end local v3    # "tm":Landroid/telephony/TelephonyManager;
    :goto_0
    return v4

    .line 811
    :catch_0
    move-exception v1

    .line 812
    .local v1, "e":Ljava/lang/Exception;
    sget-object v4, Landroid/media/SamsungAudioService;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Error checking factory SIM: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 814
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method

.method protected isPlaySilentModeOff()Z
    .locals 1

    .prologue
    .line 898
    iget-boolean v0, p0, Landroid/media/SamsungAudioService;->mIsPlaySilentModeOff:Z

    return v0
.end method

.method protected isStatusbarExpanded()Z
    .locals 1

    .prologue
    .line 956
    iget-boolean v0, p0, Landroid/media/SamsungAudioService;->mStatusbarExpanded:Z

    return v0
.end method

.method protected mediaServerDied()V
    .locals 4

    .prologue
    .line 473
    iget v2, p0, Landroid/media/SamsungAudioService;->mMonoMode:I

    invoke-direct {p0, v2}, Landroid/media/SamsungAudioService;->setMonoMode(I)V

    .line 475
    iget v2, p0, Landroid/media/SamsungAudioService;->mDualMicMode:I

    invoke-direct {p0, v2}, Landroid/media/SamsungAudioService;->setDualMicMode(I)V

    .line 477
    invoke-virtual {p0}, Landroid/media/SamsungAudioService;->setAllSoundMute()V

    .line 479
    iget v2, p0, Landroid/media/SamsungAudioService;->mNaturalSound:I

    invoke-direct {p0, v2}, Landroid/media/SamsungAudioService;->setNaturalSoundMode(I)V

    .line 481
    iget v2, p0, Landroid/media/SamsungAudioService;->mAutoHaptic:I

    invoke-direct {p0, v2}, Landroid/media/SamsungAudioService;->setAutoHaptic(I)V

    .line 483
    invoke-direct {p0}, Landroid/media/SamsungAudioService;->setSoundBalance()V

    .line 485
    const-string/jumbo v2, "persist.audio.voicetrig"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 486
    .local v0, "voicetrig":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 487
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "voice_trig="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/media/AudioSystem;->setParameters(Ljava/lang/String;)I

    .line 489
    :cond_0
    const-string/jumbo v2, "persist.audio.voicewakeup"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 490
    .local v1, "voicewakeup":Ljava/lang/String;
    if-eqz v1, :cond_2

    const-string/jumbo v2, "on"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string/jumbo v2, "off"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 491
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "voice_wakeup_mic="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/media/AudioSystem;->setParameters(Ljava/lang/String;)I

    .line 493
    :cond_2
    return-void
.end method

.method protected onAudioService()V
    .locals 2

    .prologue
    .line 436
    iget v0, p0, Landroid/media/SamsungAudioService;->mMonoMode:I

    invoke-direct {p0, v0}, Landroid/media/SamsungAudioService;->setMonoMode(I)V

    .line 438
    iget v0, p0, Landroid/media/SamsungAudioService;->mDualMicMode:I

    invoke-direct {p0, v0}, Landroid/media/SamsungAudioService;->setDualMicMode(I)V

    .line 440
    iget v0, p0, Landroid/media/SamsungAudioService;->mAllSoundMute:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 441
    invoke-virtual {p0}, Landroid/media/SamsungAudioService;->setAllSoundMute()V

    .line 443
    :cond_0
    iget v0, p0, Landroid/media/SamsungAudioService;->mNaturalSound:I

    invoke-direct {p0, v0}, Landroid/media/SamsungAudioService;->setNaturalSoundMode(I)V

    .line 445
    iget v0, p0, Landroid/media/SamsungAudioService;->mAutoHaptic:I

    if-eqz v0, :cond_1

    .line 446
    iget v0, p0, Landroid/media/SamsungAudioService;->mAutoHaptic:I

    invoke-direct {p0, v0}, Landroid/media/SamsungAudioService;->setAutoHaptic(I)V

    .line 448
    :cond_1
    invoke-direct {p0}, Landroid/media/SamsungAudioService;->setSoundBalance()V

    .line 449
    return-void
.end method

.method protected performSoftReset()V
    .locals 5

    .prologue
    const/4 v4, -0x2

    const/4 v3, 0x0

    .line 498
    iput v3, p0, Landroid/media/SamsungAudioService;->mMonoMode:I

    .line 499
    iget-object v0, p0, Landroid/media/SamsungAudioService;->mContentResolver:Landroid/content/ContentResolver;

    const-string/jumbo v1, "mono_audio_db"

    iget v2, p0, Landroid/media/SamsungAudioService;->mMonoMode:I

    invoke-static {v0, v1, v2, v4}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 500
    iget v0, p0, Landroid/media/SamsungAudioService;->mMonoMode:I

    invoke-direct {p0, v0}, Landroid/media/SamsungAudioService;->setMonoMode(I)V

    .line 502
    iput v3, p0, Landroid/media/SamsungAudioService;->mDualMicMode:I

    .line 503
    iget-object v0, p0, Landroid/media/SamsungAudioService;->mContentResolver:Landroid/content/ContentResolver;

    const-string v1, "call_noise_reduction"

    iget v2, p0, Landroid/media/SamsungAudioService;->mDualMicMode:I

    invoke-static {v0, v1, v2, v4}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 504
    iget v0, p0, Landroid/media/SamsungAudioService;->mDualMicMode:I

    invoke-direct {p0, v0}, Landroid/media/SamsungAudioService;->setDualMicMode(I)V

    .line 506
    iput v3, p0, Landroid/media/SamsungAudioService;->mAllSoundMute:I

    .line 507
    iget-object v0, p0, Landroid/media/SamsungAudioService;->mContentResolver:Landroid/content/ContentResolver;

    const-string v1, "all_sound_off"

    iget v2, p0, Landroid/media/SamsungAudioService;->mAllSoundMute:I

    invoke-static {v0, v1, v2, v4}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 508
    invoke-virtual {p0}, Landroid/media/SamsungAudioService;->setAllSoundMute()V

    .line 510
    iput v3, p0, Landroid/media/SamsungAudioService;->mNaturalSound:I

    .line 511
    iget-object v0, p0, Landroid/media/SamsungAudioService;->mContentResolver:Landroid/content/ContentResolver;

    const-string v1, "call_natural_sound"

    iget v2, p0, Landroid/media/SamsungAudioService;->mNaturalSound:I

    invoke-static {v0, v1, v2, v4}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 512
    iget v0, p0, Landroid/media/SamsungAudioService;->mNaturalSound:I

    invoke-direct {p0, v0}, Landroid/media/SamsungAudioService;->setNaturalSoundMode(I)V

    .line 514
    iput v3, p0, Landroid/media/SamsungAudioService;->mAutoHaptic:I

    .line 516
    iget-object v0, p0, Landroid/media/SamsungAudioService;->mContentResolver:Landroid/content/ContentResolver;

    const-string v1, "def_tactileassist_enable"

    iget v2, p0, Landroid/media/SamsungAudioService;->mAutoHaptic:I

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 517
    iget v0, p0, Landroid/media/SamsungAudioService;->mAutoHaptic:I

    invoke-direct {p0, v0}, Landroid/media/SamsungAudioService;->setAutoHaptic(I)V

    .line 520
    return-void
.end method

.method protected porcessUsbAudioDevicePlug(Landroid/content/Intent;)V
    .locals 26
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 525
    const/16 v18, 0x0

    .line 526
    .local v18, "outDevice":I
    const/4 v14, 0x0

    .line 527
    .local v14, "inDevice":I
    const-string/jumbo v22, "state"

    const/16 v23, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v20

    .line 528
    .local v20, "state":I
    const-string v22, "card"

    const/16 v23, -0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 529
    .local v4, "alsaCard":I
    const-string v22, "device"

    const/16 v23, -0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    .line 530
    .local v6, "alsaDevice":I
    const-string v22, "24format"

    const/16 v23, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v11

    .line 531
    .local v11, "format":I
    const-string/jumbo v22, "supportedformat"

    const/16 v23, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v21

    .line 532
    .local v21, "supportedFormat":I
    const-string/jumbo v22, "samplingRate24"

    const/16 v23, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v8

    .line 533
    .local v8, "alsaSamplingRate24":I
    const-string v22, "channels"

    const/16 v23, -0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    .line 534
    .local v5, "alsaChannels":I
    const-string/jumbo v22, "samplingRate2"

    const/16 v23, -0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    .line 535
    .local v7, "alsaSamplingRate2":I
    const-string/jumbo v22, "samplingRate6"

    const/16 v23, -0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v9

    .line 536
    .local v9, "alsaSamplingRate6":I
    const-string/jumbo v22, "usb_headset"

    const/16 v23, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v17

    .line 537
    .local v17, "isUsbHeadset":I
    const-string v22, "hostDevice"

    const/16 v23, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v15

    .line 538
    .local v15, "isHostDevice":I
    const-string v22, "hasPlayback"

    const/16 v23, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v13

    .line 539
    .local v13, "hasPlayback":Z
    const-string v22, "hasCapture"

    const/16 v23, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v12

    .line 540
    .local v12, "hasCapture":Z
    const/16 v19, 0x0

    .line 541
    .local v19, "params":Ljava/lang/String;
    if-nez v15, :cond_4

    .line 542
    const/16 v22, -0x1

    move/from16 v0, v22

    if-ne v4, v0, :cond_3

    const/16 v22, -0x1

    move/from16 v0, v22

    if-ne v6, v0, :cond_3

    const-string v19, ""

    .line 547
    :goto_0
    if-nez v15, :cond_6

    .line 548
    if-eqz v13, :cond_0

    .line 549
    const/16 v18, 0x4000

    .line 551
    :cond_0
    sget-object v22, Landroid/media/SamsungAudioService;->TAG:Ljava/lang/String;

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Broadcast Receiver: Got ACTION_USB_AUDIO_DEVICE_PLUG, state = "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, ", card: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, ", device: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, ", format: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 569
    :cond_1
    :goto_1
    const-string/jumbo v22, "issmartdock"

    const/16 v23, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v16

    .line 570
    .local v16, "isSmartdock":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/media/SamsungAudioService;->mContentResolver:Landroid/content/ContentResolver;

    move-object/from16 v22, v0

    const-string v23, "cradle_enable"

    const/16 v24, 0x0

    const/16 v25, -0x2

    invoke-static/range {v22 .. v25}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v10

    .line 571
    .local v10, "cradleEnabled":I
    move-object/from16 v0, p0

    iget-boolean v0, v0, Landroid/media/SamsungAudioService;->mOldIsSmartdock:Z

    move/from16 v22, v0

    move/from16 v0, v16

    move/from16 v1, v22

    if-eq v0, v1, :cond_d

    .line 572
    move/from16 v0, v16

    move-object/from16 v1, p0

    iput-boolean v0, v1, Landroid/media/SamsungAudioService;->mOldIsSmartdock:Z

    .line 573
    const/16 v22, 0x1

    move/from16 v0, v22

    if-ne v10, v0, :cond_c

    .line 574
    sget-object v22, Landroid/media/SamsungAudioService;->TAG:Ljava/lang/String;

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "cradleEnabled change path, state : "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 575
    const/16 v22, 0x1

    move/from16 v0, v20

    move/from16 v1, v22

    if-ne v0, v1, :cond_b

    move-object/from16 v22, v19

    :goto_2
    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Landroid/media/SamsungAudioService;->mExternalUsbInfo:Ljava/lang/String;

    .line 576
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/media/SamsungAudioService;->mAudioService:Landroid/media/AudioService;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    move/from16 v1, v18

    move/from16 v2, v20

    move-object/from16 v3, v19

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioService;->setWiredDeviceConnectionStateWithoutIntent(IILjava/lang/String;)V

    .line 587
    :goto_3
    if-eqz v12, :cond_2

    .line 588
    const/16 v22, -0x1

    move/from16 v0, v22

    if-ne v4, v0, :cond_e

    const/16 v22, -0x1

    move/from16 v0, v22

    if-ne v6, v0, :cond_e

    const-string v19, ""

    .line 590
    :goto_4
    sget-object v22, Landroid/media/SamsungAudioService;->TAG:Ljava/lang/String;

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "HasCapture : "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, ", params : "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 591
    const v14, -0x7ffff000

    .line 592
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/media/SamsungAudioService;->mAudioService:Landroid/media/AudioService;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    move/from16 v1, v20

    move-object/from16 v2, v19

    invoke-virtual {v0, v14, v1, v2}, Landroid/media/AudioService;->setWiredDeviceConnectionState(IILjava/lang/String;)V

    .line 594
    .end local v10    # "cradleEnabled":I
    .end local v16    # "isSmartdock":Z
    :cond_2
    return-void

    .line 542
    :cond_3
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "card="

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, ";device="

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    goto/16 :goto_0

    .line 544
    :cond_4
    const/16 v22, -0x1

    move/from16 v0, v22

    if-ne v4, v0, :cond_5

    const/16 v22, -0x1

    move/from16 v0, v22

    if-ne v6, v0, :cond_5

    const/16 v22, -0x1

    move/from16 v0, v22

    if-ne v5, v0, :cond_5

    const/16 v22, -0x1

    move/from16 v0, v22

    if-ne v7, v0, :cond_5

    const/16 v22, -0x1

    move/from16 v0, v22

    if-ne v9, v0, :cond_5

    const-string v19, ""

    :goto_5
    goto/16 :goto_0

    :cond_5
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v22

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, ":"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, ":"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, ":"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, ":"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, ":"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, ":"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, ":"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    goto :goto_5

    .line 554
    :cond_6
    const/16 v22, 0x1

    move/from16 v0, v17

    move/from16 v1, v22

    if-ne v0, v1, :cond_1

    .line 555
    if-eqz v13, :cond_9

    const/16 v18, 0x1000

    .line 557
    :goto_6
    const/16 v22, 0x1

    move/from16 v0, v20

    move/from16 v1, v22

    if-ne v0, v1, :cond_7

    const/16 v22, 0x1000

    move/from16 v0, v18

    move/from16 v1, v22

    if-ne v0, v1, :cond_7

    const/16 v22, 0x10

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_7

    move-object/from16 v0, p0

    iget v0, v0, Landroid/media/SamsungAudioService;->mUsbSupportedFormat:I

    move/from16 v22, v0

    if-nez v22, :cond_2

    .line 559
    :cond_7
    const/16 v22, 0x1

    move/from16 v0, v20

    move/from16 v1, v22

    if-ne v0, v1, :cond_a

    const/16 v22, 0x1000

    move/from16 v0, v18

    move/from16 v1, v22

    if-ne v0, v1, :cond_a

    .line 560
    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Landroid/media/SamsungAudioService;->mUsbSupportedFormat:I

    .line 564
    :cond_8
    :goto_7
    sget-object v22, Landroid/media/SamsungAudioService;->TAG:Ljava/lang/String;

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "Broadcast Receiver: Got ACTION_USB_AUDIO_DEVICE_PLUG params: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, " state: "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 555
    :cond_9
    const v18, -0x7ffffc00

    goto :goto_6

    .line 561
    :cond_a
    if-nez v20, :cond_8

    const/16 v22, 0x1000

    move/from16 v0, v18

    move/from16 v1, v22

    if-ne v0, v1, :cond_8

    .line 562
    const/16 v22, 0x0

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput v0, v1, Landroid/media/SamsungAudioService;->mUsbSupportedFormat:I

    goto :goto_7

    .line 575
    .restart local v10    # "cradleEnabled":I
    .restart local v16    # "isSmartdock":Z
    :cond_b
    const/16 v22, 0x0

    goto/16 :goto_2

    .line 578
    :cond_c
    sget-object v22, Landroid/media/SamsungAudioService;->TAG:Ljava/lang/String;

    const-string v23, "cradle disabled no any action"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_3

    .line 583
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/media/SamsungAudioService;->mAudioService:Landroid/media/AudioService;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    move/from16 v1, v18

    move/from16 v2, v20

    move-object/from16 v3, v19

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioService;->setWiredDeviceConnectionState(IILjava/lang/String;)V

    goto/16 :goto_3

    .line 588
    :cond_e
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "card="

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, ";device="

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    goto/16 :goto_4
.end method

.method protected readPersistedSettings()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x2

    .line 453
    iget-object v0, p0, Landroid/media/SamsungAudioService;->mContentResolver:Landroid/content/ContentResolver;

    .line 455
    .local v0, "cr":Landroid/content/ContentResolver;
    const-string/jumbo v1, "mono_audio_db"

    invoke-static {v0, v1, v4, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    iput v1, p0, Landroid/media/SamsungAudioService;->mMonoMode:I

    .line 457
    const-string v1, "call_noise_reduction"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    iput v1, p0, Landroid/media/SamsungAudioService;->mDualMicMode:I

    .line 459
    const-string v1, "all_sound_off"

    invoke-static {v0, v1, v4, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    iput v1, p0, Landroid/media/SamsungAudioService;->mAllSoundMute:I

    .line 461
    const-string v1, "call_natural_sound"

    invoke-static {v0, v1, v4, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    iput v1, p0, Landroid/media/SamsungAudioService;->mNaturalSound:I

    .line 464
    iget-object v1, p0, Landroid/media/SamsungAudioService;->mContentResolver:Landroid/content/ContentResolver;

    const-string v2, "def_tactileassist_enable"

    invoke-static {v1, v2, v4}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Landroid/media/SamsungAudioService;->mAutoHaptic:I

    .line 468
    return-void
.end method

.method protected setAllSoundMute()V
    .locals 2

    .prologue
    .line 932
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "audioParam;allsoundmute="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Landroid/media/SamsungAudioService;->mAllSoundMute:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/media/AudioSystem;->setParameters(Ljava/lang/String;)I

    .line 933
    const-string/jumbo v0, "persist.audio.allsoundmute"

    iget v1, p0, Landroid/media/SamsungAudioService;->mAllSoundMute:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 934
    return-void
.end method

.method protected setRingerMode(I)V
    .locals 7
    .param p1, "ringerMode"    # I

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 853
    packed-switch p1, :pswitch_data_0

    .line 868
    :goto_0
    return-void

    .line 855
    :pswitch_0
    sget-object v0, Landroid/media/SamsungAudioService;->TAG:Ljava/lang/String;

    const-string v1, "Ringer mode : silent & set driving mode off"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 856
    iput-boolean v3, p0, Landroid/media/SamsungAudioService;->mSilentModeOff:Z

    goto :goto_0

    .line 859
    :pswitch_1
    sget-object v0, Landroid/media/SamsungAudioService;->TAG:Ljava/lang/String;

    const-string v1, "Ringer mode : vibrate"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 860
    iput-boolean v3, p0, Landroid/media/SamsungAudioService;->mSilentModeOff:Z

    .line 861
    iget-object v0, p0, Landroid/media/SamsungAudioService;->mSamsungAudioServiceHandler:Landroid/media/SamsungAudioService$SamsungAudioServiceHandler;

    const/4 v1, 0x4

    move v4, v3

    move v6, v3

    invoke-static/range {v0 .. v6}, Landroid/media/SamsungAudioService;->sendMsg(Landroid/os/Handler;IIIILjava/lang/Object;I)V

    goto :goto_0

    .line 864
    :pswitch_2
    sget-object v0, Landroid/media/SamsungAudioService;->TAG:Ljava/lang/String;

    const-string v1, "Ringer mode : normal"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 865
    iget-object v0, p0, Landroid/media/SamsungAudioService;->mSamsungAudioServiceHandler:Landroid/media/SamsungAudioService$SamsungAudioServiceHandler;

    const/4 v1, 0x3

    move v4, v3

    move v6, v3

    invoke-static/range {v0 .. v6}, Landroid/media/SamsungAudioService;->sendMsg(Landroid/os/Handler;IIIILjava/lang/Object;I)V

    goto :goto_0

    .line 853
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected setSilentModeOff(Z)V
    .locals 0
    .param p1, "onOff"    # Z

    .prologue
    .line 894
    iput-boolean p1, p0, Landroid/media/SamsungAudioService;->mSilentModeOff:Z

    .line 895
    return-void
.end method

.method protected setSystemReady(Z)V
    .locals 0
    .param p1, "bEable"    # Z

    .prologue
    .line 430
    iput-boolean p1, p0, Landroid/media/SamsungAudioService;->mSystemReady:Z

    .line 431
    return-void
.end method

.method protected startCPUBoost()V
    .locals 0

    .prologue
    .line 686
    return-void
.end method

.method protected stopCPUBoost()V
    .locals 0

    .prologue
    .line 700
    return-void
.end method

.method protected vibrateCall()V
    .locals 4

    .prologue
    .line 884
    :try_start_0
    iget-object v2, p0, Landroid/media/SamsungAudioService;->mAudioService:Landroid/media/AudioService;

    invoke-virtual {v2}, Landroid/media/AudioService;->hasVibrator()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 885
    iget-object v2, p0, Landroid/media/SamsungAudioService;->mContext:Landroid/content/Context;

    const-string/jumbo v3, "vibrator"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/SystemVibrator;

    .line 886
    .local v1, "vibrator":Landroid/os/SystemVibrator;
    const/16 v2, 0xc

    invoke-virtual {v1, v2}, Landroid/os/SystemVibrator;->vibrateCall(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 891
    .end local v1    # "vibrator":Landroid/os/SystemVibrator;
    :cond_0
    :goto_0
    return-void

    .line 888
    :catch_0
    move-exception v0

    .line 889
    .local v0, "e":Ljava/lang/Exception;
    sget-object v2, Landroid/media/SamsungAudioService;->TAG:Ljava/lang/String;

    const-string v3, "Vibrator error"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
