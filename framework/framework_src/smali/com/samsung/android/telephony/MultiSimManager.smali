.class public Lcom/samsung/android/telephony/MultiSimManager;
.super Ljava/lang/Object;
.source "MultiSimManager.java"


# static fields
.field static final LOG_TAG:Ljava/lang/String; = "MultiSimManager"

.field private static final PHONE_ID_TYPE_FOREGROUND_CALL:I = 0x0

.field private static final PHONE_ID_TYPE_REJECT_CALL:I = 0x1

.field public static final SIMSLOT1:I = 0x0

.field public static final SIMSLOT2:I = 0x1

.field public static final SIMSLOT3:I = 0x2

.field public static final SIMSLOT4:I = 0x3

.field public static final SIMSLOT5:I = 0x4

.field private static final SIMSLOT_COUNT:I

.field public static final TYPE_DATA:I = 0x3

.field public static final TYPE_DEFAULT:I = 0x0

.field public static final TYPE_SMS:I = 0x2

.field public static final TYPE_VOICE:I = 0x1

.field private static final mPhoneOnKey:[Ljava/lang/String;

.field private static final mSimIconKey:[Ljava/lang/String;

.field private static final mSimNameKey:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 78
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "select_name_1"

    aput-object v1, v0, v2

    const-string/jumbo v1, "select_name_2"

    aput-object v1, v0, v3

    const-string/jumbo v1, "select_name_3"

    aput-object v1, v0, v4

    const-string/jumbo v1, "select_name_4"

    aput-object v1, v0, v5

    const-string/jumbo v1, "select_name_5"

    aput-object v1, v0, v6

    sput-object v0, Lcom/samsung/android/telephony/MultiSimManager;->mSimNameKey:[Ljava/lang/String;

    .line 81
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "select_icon_1"

    aput-object v1, v0, v2

    const-string/jumbo v1, "select_icon_2"

    aput-object v1, v0, v3

    const-string/jumbo v1, "select_icon_3"

    aput-object v1, v0, v4

    const-string/jumbo v1, "select_icon_4"

    aput-object v1, v0, v5

    const-string/jumbo v1, "select_icon_5"

    aput-object v1, v0, v6

    sput-object v0, Lcom/samsung/android/telephony/MultiSimManager;->mSimIconKey:[Ljava/lang/String;

    .line 84
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "phone1_on"

    aput-object v1, v0, v2

    const-string/jumbo v1, "phone2_on"

    aput-object v1, v0, v3

    const-string/jumbo v1, "phone3_on"

    aput-object v1, v0, v4

    const-string/jumbo v1, "phone4_on"

    aput-object v1, v0, v5

    const-string/jumbo v1, "phone5_on"

    aput-object v1, v0, v6

    sput-object v0, Lcom/samsung/android/telephony/MultiSimManager;->mPhoneOnKey:[Ljava/lang/String;

    .line 88
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getPhoneCount()I

    move-result v0

    sput v0, Lcom/samsung/android/telephony/MultiSimManager;->SIMSLOT_COUNT:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static activateSubId(J)V
    .locals 0
    .param p0, "subId"    # J

    .prologue
    .line 2369
    invoke-static {p0, p1}, Landroid/telephony/SubscriptionManager;->activateSubId(J)V

    .line 2370
    return-void
.end method

.method public static addSubInfoRecord(Ljava/lang/String;I)Landroid/net/Uri;
    .locals 1
    .param p0, "iccId"    # Ljava/lang/String;
    .param p1, "slotId"    # I

    .prologue
    .line 2013
    invoke-static {p0, p1}, Landroid/telephony/SubscriptionManager;->addSubInfoRecord(Ljava/lang/String;I)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static allDefaultsSelected()Z
    .locals 1

    .prologue
    .line 2311
    invoke-static {}, Landroid/telephony/SubscriptionManager;->allDefaultsSelected()Z

    move-result v0

    return v0
.end method

.method public static answerRingingCall(I)V
    .locals 4
    .param p0, "slotId"    # I

    .prologue
    .line 1554
    invoke-static {p0}, Landroid/telephony/SubscriptionManager;->getSubId(I)[J

    move-result-object v0

    .line 1556
    .local v0, "subId":[J
    :try_start_0
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v1

    const/4 v2, 0x0

    aget-wide v2, v0, v2

    invoke-virtual {v1, v2, v3}, Landroid/telephony/TelephonyManager;->answerRingingCall(J)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1559
    :goto_0
    return-void

    .line 1557
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static answerRingingCallUsingSubId(J)V
    .locals 2
    .param p0, "subId"    # J

    .prologue
    .line 1572
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Landroid/telephony/TelephonyManager;->answerRingingCall(J)V

    .line 1573
    return-void
.end method

.method public static appendSimSlot(Ljava/lang/String;I)Ljava/lang/String;
    .locals 5
    .param p0, "text"    # Ljava/lang/String;
    .param p1, "simSlot"    # I

    .prologue
    .line 195
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 197
    .local v1, "str":Ljava/lang/StringBuilder;
    if-ltz p1, :cond_1

    sget v2, Lcom/samsung/android/telephony/MultiSimManager;->SIMSLOT_COUNT:I

    if-ge p1, v2, :cond_1

    .line 198
    if-eqz p1, :cond_0

    add-int/lit8 v2, p1, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 209
    :cond_0
    :goto_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 200
    :cond_1
    const-string v2, "MultiSimManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SimSlot value is bigger than SIMSLOT_COUNT or smaller than 0.(text : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", simSlot : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/telephony/Rlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 202
    :try_start_0
    new-instance v2, Ljava/lang/Exception;

    const-string v3, "appendSimSlot method exception"

    invoke-direct {v2, v3}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 204
    :catch_0
    move-exception v0

    .line 205
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static call(ILjava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p0, "slotId"    # I
    .param p1, "callingPackage"    # Ljava/lang/String;
    .param p2, "number"    # Ljava/lang/String;

    .prologue
    .line 1527
    invoke-static {p0}, Landroid/telephony/SubscriptionManager;->getSubId(I)[J

    move-result-object v0

    .line 1529
    .local v0, "subId":[J
    :try_start_0
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v1

    const/4 v2, 0x0

    aget-wide v2, v0, v2

    invoke-virtual {v1, v2, v3, p1, p2}, Landroid/telephony/TelephonyManager;->call(JLjava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1532
    :goto_0
    return-void

    .line 1530
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static callUsingSubId(JLjava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0, "subId"    # J
    .param p2, "callingPackage"    # Ljava/lang/String;
    .param p3, "number"    # Ljava/lang/String;

    .prologue
    .line 1541
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0, p0, p1, p2, p3}, Landroid/telephony/TelephonyManager;->call(JLjava/lang/String;Ljava/lang/String;)V

    .line 1542
    return-void
.end method

.method public static clearDefaultsForInactiveSubIds()V
    .locals 0

    .prologue
    .line 2319
    invoke-static {}, Landroid/telephony/SubscriptionManager;->clearDefaultsForInactiveSubIds()V

    .line 2320
    return-void
.end method

.method public static clearSubInfo()V
    .locals 0

    .prologue
    .line 2304
    invoke-static {}, Landroid/telephony/SubscriptionManager;->clearSubInfo()V

    .line 2305
    return-void
.end method

.method public static deactivateSubId(J)V
    .locals 0
    .param p0, "subId"    # J

    .prologue
    .line 2378
    invoke-static {p0, p1}, Landroid/telephony/SubscriptionManager;->deactivateSubId(J)V

    .line 2379
    return-void
.end method

.method public static dial(ILjava/lang/String;)V
    .locals 4
    .param p0, "slotId"    # I
    .param p1, "number"    # Ljava/lang/String;

    .prologue
    .line 1502
    invoke-static {p0}, Landroid/telephony/SubscriptionManager;->getSubId(I)[J

    move-result-object v0

    .line 1504
    .local v0, "subId":[J
    :try_start_0
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v1

    const/4 v2, 0x0

    aget-wide v2, v0, v2

    invoke-virtual {v1, v2, v3, p1}, Landroid/telephony/TelephonyManager;->dial(JLjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1507
    :goto_0
    return-void

    .line 1505
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static dialUsingSubId(JLjava/lang/String;)V
    .locals 2
    .param p0, "subId"    # J
    .param p2, "number"    # Ljava/lang/String;

    .prologue
    .line 1518
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0, p0, p1, p2}, Landroid/telephony/TelephonyManager;->dial(JLjava/lang/String;)V

    .line 1519
    return-void
.end method

.method public static enableSimplifiedNetworkSettings(IZ)V
    .locals 4
    .param p0, "slotId"    # I
    .param p1, "enable"    # Z

    .prologue
    .line 1354
    invoke-static {p0}, Landroid/telephony/SubscriptionManager;->getSubId(I)[J

    move-result-object v0

    .line 1356
    .local v0, "subId":[J
    :try_start_0
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v1

    const/4 v2, 0x0

    aget-wide v2, v0, v2

    invoke-virtual {v1, v2, v3, p1}, Landroid/telephony/TelephonyManager;->enableSimplifiedNetworkSettingsForSubscriber(JZ)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1359
    :goto_0
    return-void

    .line 1357
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static enableSimplifiedNetworkSettingsUsingSubId(JZ)V
    .locals 2
    .param p0, "subId"    # J
    .param p2, "enable"    # Z

    .prologue
    .line 1374
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0, p0, p1, p2}, Landroid/telephony/TelephonyManager;->enableSimplifiedNetworkSettingsForSubscriber(JZ)V

    .line 1375
    return-void
.end method

.method public static endCall(I)Z
    .locals 6
    .param p0, "slotId"    # I

    .prologue
    const/4 v2, 0x0

    .line 1581
    invoke-static {p0}, Landroid/telephony/SubscriptionManager;->getSubId(I)[J

    move-result-object v1

    .line 1583
    .local v1, "subId":[J
    :try_start_0
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v3

    const/4 v4, 0x0

    aget-wide v4, v1, v4

    invoke-virtual {v3, v4, v5}, Landroid/telephony/TelephonyManager;->endCall(J)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 1585
    :goto_0
    return v2

    .line 1584
    :catch_0
    move-exception v0

    .line 1585
    .local v0, "ex":Ljava/lang/NullPointerException;
    goto :goto_0
.end method

.method public static endCallUsingSubId(J)Z
    .locals 2
    .param p0, "subId"    # J

    .prologue
    .line 1596
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Landroid/telephony/TelephonyManager;->endCall(J)Z

    move-result v0

    return v0
.end method

.method public static getActiveSubIdList()[J
    .locals 1

    .prologue
    .line 2360
    invoke-static {}, Landroid/telephony/SubscriptionManager;->getActiveSubIdList()[J

    move-result-object v0

    return-object v0
.end method

.method public static getActiveSubInfoCount()I
    .locals 1

    .prologue
    .line 2002
    invoke-static {}, Landroid/telephony/SubscriptionManager;->getActiveSubInfoCount()I

    move-result v0

    return v0
.end method

.method public static getActiveSubInfoList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/telephony/SubInfoRecord;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1986
    invoke-static {}, Landroid/telephony/SubscriptionManager;->getActiveSubInfoList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static getAllSubInfoCount()I
    .locals 1

    .prologue
    .line 1994
    invoke-static {}, Landroid/telephony/SubscriptionManager;->getAllSubInfoCount()I

    move-result v0

    return v0
.end method

.method public static getAllSubInfoList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/telephony/SubInfoRecord;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1978
    invoke-static {}, Landroid/telephony/SubscriptionManager;->getAllSubInfoList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static getCallState(I)I
    .locals 6
    .param p0, "slotId"    # I

    .prologue
    const/4 v2, 0x0

    .line 1113
    invoke-static {p0}, Landroid/telephony/SubscriptionManager;->getSubId(I)[J

    move-result-object v1

    .line 1115
    .local v1, "subId":[J
    :try_start_0
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v3

    const/4 v4, 0x0

    aget-wide v4, v1, v4

    invoke-virtual {v3, v4, v5}, Landroid/telephony/TelephonyManager;->getCallState(J)I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 1117
    :goto_0
    return v2

    .line 1116
    :catch_0
    move-exception v0

    .line 1117
    .local v0, "ex":Ljava/lang/NullPointerException;
    goto :goto_0
.end method

.method public static getCallStateUsingSubId(J)I
    .locals 2
    .param p0, "subId"    # J

    .prologue
    .line 1130
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Landroid/telephony/TelephonyManager;->getCallState(J)I

    move-result v0

    return v0
.end method

.method public static getCdmaEriIconIndex(I)I
    .locals 6
    .param p0, "slotId"    # I

    .prologue
    .line 1171
    invoke-static {p0}, Landroid/telephony/SubscriptionManager;->getSubId(I)[J

    move-result-object v1

    .line 1173
    .local v1, "subId":[J
    :try_start_0
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v2

    const/4 v3, 0x0

    aget-wide v4, v1, v3

    invoke-virtual {v2, v4, v5}, Landroid/telephony/TelephonyManager;->getCdmaEriIconIndex(J)I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 1175
    :goto_0
    return v2

    .line 1174
    :catch_0
    move-exception v0

    .line 1175
    .local v0, "ex":Ljava/lang/NullPointerException;
    const/4 v2, -0x1

    goto :goto_0
.end method

.method public static getCdmaEriIconIndexUsingSubId(J)I
    .locals 2
    .param p0, "subId"    # J

    .prologue
    .line 1187
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Landroid/telephony/TelephonyManager;->getCdmaEriIconIndex(J)I

    move-result v0

    return v0
.end method

.method public static getCdmaEriIconMode(I)I
    .locals 6
    .param p0, "slotId"    # I

    .prologue
    .line 1199
    invoke-static {p0}, Landroid/telephony/SubscriptionManager;->getSubId(I)[J

    move-result-object v1

    .line 1201
    .local v1, "subId":[J
    :try_start_0
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v2

    const/4 v3, 0x0

    aget-wide v4, v1, v3

    invoke-virtual {v2, v4, v5}, Landroid/telephony/TelephonyManager;->getCdmaEriIconMode(J)I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 1203
    :goto_0
    return v2

    .line 1202
    :catch_0
    move-exception v0

    .line 1203
    .local v0, "ex":Ljava/lang/NullPointerException;
    const/4 v2, -0x1

    goto :goto_0
.end method

.method public static getCdmaEriIconModeUsingSubId(J)I
    .locals 2
    .param p0, "subId"    # J

    .prologue
    .line 1215
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Landroid/telephony/TelephonyManager;->getCdmaEriIconMode(J)I

    move-result v0

    return v0
.end method

.method public static getCdmaEriText(I)Ljava/lang/String;
    .locals 6
    .param p0, "slotId"    # I

    .prologue
    .line 1227
    invoke-static {p0}, Landroid/telephony/SubscriptionManager;->getSubId(I)[J

    move-result-object v1

    .line 1229
    .local v1, "subId":[J
    :try_start_0
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v2

    const/4 v3, 0x0

    aget-wide v4, v1, v3

    invoke-virtual {v2, v4, v5}, Landroid/telephony/TelephonyManager;->getCdmaEriText(J)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 1231
    :goto_0
    return-object v2

    .line 1230
    :catch_0
    move-exception v0

    .line 1231
    .local v0, "ex":Ljava/lang/NullPointerException;
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static getCdmaEriTextUsingSubId(J)Ljava/lang/String;
    .locals 2
    .param p0, "subId"    # J

    .prologue
    .line 1243
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Landroid/telephony/TelephonyManager;->getCdmaEriText(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getCurrentPhoneType(I)I
    .locals 6
    .param p0, "slotId"    # I

    .prologue
    .line 426
    invoke-static {p0}, Landroid/telephony/SubscriptionManager;->getSubId(I)[J

    move-result-object v1

    .line 428
    .local v1, "subId":[J
    :try_start_0
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v2

    const/4 v3, 0x0

    aget-wide v4, v1, v3

    invoke-virtual {v2, v4, v5}, Landroid/telephony/TelephonyManager;->getCurrentPhoneType(J)I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 430
    :goto_0
    return v2

    .line 429
    :catch_0
    move-exception v0

    .line 430
    .local v0, "ex":Ljava/lang/NullPointerException;
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public static getCurrentPhoneTypeUsingSubId(J)I
    .locals 2
    .param p0, "subId"    # J

    .prologue
    .line 442
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Landroid/telephony/TelephonyManager;->getCurrentPhoneType(J)I

    move-result v0

    return v0
.end method

.method public static getDataNetworkType(I)I
    .locals 6
    .param p0, "slotId"    # I

    .prologue
    const/4 v2, 0x0

    .line 606
    invoke-static {p0}, Landroid/telephony/SubscriptionManager;->getSubId(I)[J

    move-result-object v1

    .line 608
    .local v1, "subId":[J
    :try_start_0
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v3

    const/4 v4, 0x0

    aget-wide v4, v1, v4

    invoke-virtual {v3, v4, v5}, Landroid/telephony/TelephonyManager;->getDataNetworkType(J)I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 610
    :goto_0
    return v2

    .line 609
    :catch_0
    move-exception v0

    .line 610
    .local v0, "ex":Ljava/lang/NullPointerException;
    goto :goto_0
.end method

.method public static getDataNetworkTypeUsingSubId(J)I
    .locals 2
    .param p0, "subId"    # J

    .prologue
    .line 623
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Landroid/telephony/TelephonyManager;->getDataNetworkType(J)I

    move-result v0

    return v0
.end method

.method public static getDataRoamingEnabled(I)Z
    .locals 6
    .param p0, "slotId"    # I

    .prologue
    const/4 v2, 0x0

    .line 1421
    invoke-static {p0}, Landroid/telephony/SubscriptionManager;->getSubId(I)[J

    move-result-object v1

    .line 1423
    .local v1, "subId":[J
    :try_start_0
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v3

    const/4 v4, 0x0

    aget-wide v4, v1, v4

    invoke-virtual {v3, v4, v5}, Landroid/telephony/TelephonyManager;->getDataRoamingEnabled(J)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 1425
    :goto_0
    return v2

    .line 1424
    :catch_0
    move-exception v0

    .line 1425
    .local v0, "ex":Ljava/lang/NullPointerException;
    goto :goto_0
.end method

.method public static getDataRoamingEnabledUsingSubId(J)Z
    .locals 2
    .param p0, "subId"    # J

    .prologue
    .line 1437
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Landroid/telephony/TelephonyManager;->getDataRoamingEnabled(J)Z

    move-result v0

    return v0
.end method

.method public static getDataServiceState(I)I
    .locals 6
    .param p0, "slotId"    # I

    .prologue
    const/4 v2, 0x0

    .line 1474
    invoke-static {p0}, Landroid/telephony/SubscriptionManager;->getSubId(I)[J

    move-result-object v1

    .line 1476
    .local v1, "subId":[J
    :try_start_0
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v3

    const/4 v4, 0x0

    aget-wide v4, v1, v4

    invoke-virtual {v3, v4, v5}, Landroid/telephony/TelephonyManager;->getDataServiceState(J)I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 1478
    :goto_0
    return v2

    .line 1477
    :catch_0
    move-exception v0

    .line 1478
    .local v0, "ex":Ljava/lang/NullPointerException;
    goto :goto_0
.end method

.method public static getDataServiceStateUsingSubId(J)I
    .locals 2
    .param p0, "subId"    # J

    .prologue
    .line 1491
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Landroid/telephony/TelephonyManager;->getDataServiceState(J)I

    move-result v0

    return v0
.end method

.method public static getDataState(I)I
    .locals 6
    .param p0, "slotId"    # I

    .prologue
    const/4 v2, 0x0

    .line 1142
    invoke-static {p0}, Landroid/telephony/SubscriptionManager;->getSubId(I)[J

    move-result-object v1

    .line 1144
    .local v1, "subId":[J
    :try_start_0
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v3

    const/4 v4, 0x0

    aget-wide v4, v1, v4

    invoke-virtual {v3, v4, v5}, Landroid/telephony/TelephonyManager;->getDataState(J)I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 1146
    :goto_0
    return v2

    .line 1145
    :catch_0
    move-exception v0

    .line 1146
    .local v0, "ex":Ljava/lang/NullPointerException;
    goto :goto_0
.end method

.method public static getDataStateUsingSubId(J)I
    .locals 2
    .param p0, "subId"    # J

    .prologue
    .line 1159
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Landroid/telephony/TelephonyManager;->getDataState(J)I

    move-result v0

    return v0
.end method

.method public static getDefaultPhoneId(I)I
    .locals 3
    .param p0, "type"    # I

    .prologue
    .line 2284
    packed-switch p0, :pswitch_data_0

    .line 2294
    const-string v0, "MultiSimManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[getDefaultPhoneId] Invalid Type:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/telephony/Rlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2295
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 2287
    :pswitch_0
    invoke-static {}, Landroid/telephony/SubscriptionManager;->getDefaultVoicePhoneId()I

    move-result v0

    goto :goto_0

    .line 2289
    :pswitch_1
    invoke-static {}, Landroid/telephony/SubscriptionManager;->getDefaultSmsPhoneId()I

    move-result v0

    goto :goto_0

    .line 2291
    :pswitch_2
    invoke-static {}, Landroid/telephony/SubscriptionManager;->getDefaultDataPhoneId()I

    move-result v0

    goto :goto_0

    .line 2284
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static getDefaultSubId(I)J
    .locals 2
    .param p0, "type"    # I

    .prologue
    .line 2218
    packed-switch p0, :pswitch_data_0

    .line 2228
    invoke-static {}, Landroid/telephony/SubscriptionManager;->getDefaultSubId()J

    move-result-wide v0

    :goto_0
    return-wide v0

    .line 2221
    :pswitch_0
    invoke-static {}, Landroid/telephony/SubscriptionManager;->getDefaultVoiceSubId()J

    move-result-wide v0

    goto :goto_0

    .line 2223
    :pswitch_1
    invoke-static {}, Landroid/telephony/SubscriptionManager;->getDefaultSmsSubId()J

    move-result-wide v0

    goto :goto_0

    .line 2225
    :pswitch_2
    invoke-static {}, Landroid/telephony/SubscriptionManager;->getDefaultDataSubId()J

    move-result-wide v0

    goto :goto_0

    .line 2218
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static getDefaultSubInfo(I)Landroid/telephony/SubInfoRecord;
    .locals 3
    .param p0, "type"    # I

    .prologue
    .line 2262
    packed-switch p0, :pswitch_data_0

    .line 2272
    const-string v0, "MultiSimManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[getDefaultSubInfo] Invalid Type:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/telephony/Rlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2273
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2265
    :pswitch_0
    invoke-static {}, Landroid/telephony/SubscriptionManager;->getDefaultVoiceSubInfo()Landroid/telephony/SubInfoRecord;

    move-result-object v0

    goto :goto_0

    .line 2267
    :pswitch_1
    invoke-static {}, Landroid/telephony/SubscriptionManager;->getDefaultSmsSubInfo()Landroid/telephony/SubInfoRecord;

    move-result-object v0

    goto :goto_0

    .line 2269
    :pswitch_2
    invoke-static {}, Landroid/telephony/SubscriptionManager;->getDefaultDataSubInfo()Landroid/telephony/SubInfoRecord;

    move-result-object v0

    goto :goto_0

    .line 2262
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static getDeviceId(I)Ljava/lang/String;
    .locals 1
    .param p0, "slotId"    # I

    .prologue
    .line 308
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/telephony/TelephonyManager;->getDeviceId(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getDeviceIdUsingSubId(J)Ljava/lang/String;
    .locals 2
    .param p0, "subId"    # J

    .prologue
    .line 322
    invoke-static {p0, p1}, Landroid/telephony/SubscriptionManager;->getSlotId(J)I

    move-result v0

    .line 323
    .local v0, "slotId":I
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/telephony/TelephonyManager;->getDeviceId(I)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getEnabledSimCount(Landroid/content/Context;)I
    .locals 9
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 218
    const/4 v1, 0x0

    .line 220
    .local v1, "count":I
    invoke-static {}, Landroid/telephony/SubscriptionManager;->getActiveSubInfoList()Ljava/util/List;

    move-result-object v0

    .line 222
    .local v0, "activeSubList":Ljava/util/List;, "Ljava/util/List<Landroid/telephony/SubInfoRecord;>;"
    if-eqz v0, :cond_1

    .line 223
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/telephony/SubInfoRecord;

    .line 224
    .local v4, "subInfoRecord":Landroid/telephony/SubInfoRecord;
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    const-string/jumbo v5, "ril.ICC_TYPE"

    iget-wide v6, v4, Landroid/telephony/SubInfoRecord;->subId:J

    const-string v8, "0"

    invoke-static {v5, v6, v7, v8}, Landroid/telephony/TelephonyManager;->getTelephonyProperty(Ljava/lang/String;JLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 225
    .local v3, "iccType":Ljava/lang/String;
    const-string v5, "0"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    iget v5, v4, Landroid/telephony/SubInfoRecord;->slotId:I

    invoke-static {p0, v5}, Lcom/samsung/android/telephony/MultiSimManager;->isEnabledSim(Landroid/content/Context;I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 226
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 231
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "iccType":Ljava/lang/String;
    .end local v4    # "subInfoRecord":Landroid/telephony/SubInfoRecord;
    :cond_1
    return v1
.end method

.method public static getGroupIdLevel1(I)Ljava/lang/String;
    .locals 6
    .param p0, "slotId"    # I

    .prologue
    .line 826
    invoke-static {p0}, Landroid/telephony/SubscriptionManager;->getSubId(I)[J

    move-result-object v1

    .line 828
    .local v1, "subId":[J
    :try_start_0
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v2

    const/4 v3, 0x0

    aget-wide v4, v1, v3

    invoke-virtual {v2, v4, v5}, Landroid/telephony/TelephonyManager;->getGroupIdLevel1(J)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 830
    :goto_0
    return-object v2

    .line 829
    :catch_0
    move-exception v0

    .line 830
    .local v0, "ex":Ljava/lang/NullPointerException;
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static getGroupIdLevel1UsingSubId(J)Ljava/lang/String;
    .locals 2
    .param p0, "subId"    # J

    .prologue
    .line 846
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Landroid/telephony/TelephonyManager;->getGroupIdLevel1(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getIccSimChallengeResponse(IILjava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "slotId"    # I
    .param p1, "appType"    # I
    .param p2, "data"    # Ljava/lang/String;

    .prologue
    .line 1318
    invoke-static {p0}, Landroid/telephony/SubscriptionManager;->getSubId(I)[J

    move-result-object v1

    .line 1320
    .local v1, "subId":[J
    :try_start_0
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v2

    const/4 v3, 0x0

    aget-wide v4, v1, v3

    invoke-virtual {v2, v4, v5, p1, p2}, Landroid/telephony/TelephonyManager;->getIccSimChallengeResponse(JILjava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 1322
    :goto_0
    return-object v2

    .line 1321
    :catch_0
    move-exception v0

    .line 1322
    .local v0, "ex":Ljava/lang/NullPointerException;
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static getIccSimChallengeResponseUsingSubId(JILjava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "subId"    # J
    .param p2, "appType"    # I
    .param p3, "data"    # Ljava/lang/String;

    .prologue
    .line 1337
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0, p0, p1, p2, p3}, Landroid/telephony/TelephonyManager;->getIccSimChallengeResponse(JILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getImei(I)Ljava/lang/String;
    .locals 1
    .param p0, "slotId"    # I

    .prologue
    .line 401
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/telephony/TelephonyManager;->getImei(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getImeiUsingSubId(J)Ljava/lang/String;
    .locals 2
    .param p0, "subId"    # J

    .prologue
    .line 414
    invoke-static {p0, p1}, Landroid/telephony/SubscriptionManager;->getSlotId(J)I

    move-result v0

    .line 415
    .local v0, "slotId":I
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/telephony/TelephonyManager;->getImei(I)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getInsertedSimCount()I
    .locals 1

    .prologue
    .line 107
    invoke-static {}, Landroid/telephony/SubscriptionManager;->getActiveSubInfoCount()I

    move-result v0

    return v0
.end method

.method public static getLine1AlphaTag(I)Ljava/lang/String;
    .locals 6
    .param p0, "slotId"    # I

    .prologue
    .line 906
    invoke-static {p0}, Landroid/telephony/SubscriptionManager;->getSubId(I)[J

    move-result-object v1

    .line 908
    .local v1, "subId":[J
    :try_start_0
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v2

    const/4 v3, 0x0

    aget-wide v4, v1, v3

    invoke-virtual {v2, v4, v5}, Landroid/telephony/TelephonyManager;->getLine1AlphaTagForSubscriber(J)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 910
    :goto_0
    return-object v2

    .line 909
    :catch_0
    move-exception v0

    .line 910
    .local v0, "ex":Ljava/lang/NullPointerException;
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static getLine1AlphaTagUsingSubId(J)Ljava/lang/String;
    .locals 2
    .param p0, "subId"    # J

    .prologue
    .line 928
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Landroid/telephony/TelephonyManager;->getLine1AlphaTagForSubscriber(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getLine1Number(I)Ljava/lang/String;
    .locals 6
    .param p0, "slotId"    # I

    .prologue
    .line 370
    invoke-static {p0}, Landroid/telephony/SubscriptionManager;->getSubId(I)[J

    move-result-object v1

    .line 372
    .local v1, "subId":[J
    :try_start_0
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v2

    const/4 v3, 0x0

    aget-wide v4, v1, v3

    invoke-virtual {v2, v4, v5}, Landroid/telephony/TelephonyManager;->getLine1NumberForSubscriber(J)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 374
    :goto_0
    return-object v2

    .line 373
    :catch_0
    move-exception v0

    .line 374
    .local v0, "ex":Ljava/lang/NullPointerException;
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static getLine1NumberUsingSubId(J)Ljava/lang/String;
    .locals 2
    .param p0, "subId"    # J

    .prologue
    .line 389
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Landroid/telephony/TelephonyManager;->getLine1NumberForSubscriber(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getLteOnCdmaMode(I)I
    .locals 6
    .param p0, "slotId"    # I

    .prologue
    .line 793
    invoke-static {p0}, Landroid/telephony/SubscriptionManager;->getSubId(I)[J

    move-result-object v1

    .line 795
    .local v1, "subId":[J
    :try_start_0
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v2

    const/4 v3, 0x0

    aget-wide v4, v1, v3

    invoke-virtual {v2, v4, v5}, Landroid/telephony/TelephonyManager;->getLteOnCdmaMode(J)I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 797
    :goto_0
    return v2

    .line 796
    :catch_0
    move-exception v0

    .line 797
    .local v0, "ex":Ljava/lang/NullPointerException;
    const/4 v2, -0x1

    goto :goto_0
.end method

.method public static getLteOnCdmaModeUsingSubId(J)I
    .locals 2
    .param p0, "subId"    # J

    .prologue
    .line 811
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Landroid/telephony/TelephonyManager;->getLteOnCdmaMode(J)I

    move-result v0

    return v0
.end method

.method public static getMsisdn(I)Ljava/lang/String;
    .locals 6
    .param p0, "slotId"    # I

    .prologue
    .line 943
    invoke-static {p0}, Landroid/telephony/SubscriptionManager;->getSubId(I)[J

    move-result-object v1

    .line 945
    .local v1, "subId":[J
    :try_start_0
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v2

    const/4 v3, 0x0

    aget-wide v4, v1, v3

    invoke-virtual {v2, v4, v5}, Landroid/telephony/TelephonyManager;->getMsisdn(J)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 947
    :goto_0
    return-object v2

    .line 946
    :catch_0
    move-exception v0

    .line 947
    .local v0, "ex":Ljava/lang/NullPointerException;
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static getMsisdnUsingSubId(J)Ljava/lang/String;
    .locals 2
    .param p0, "subId"    # J

    .prologue
    .line 963
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Landroid/telephony/TelephonyManager;->getMsisdn(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getMultiSimPhoneId(I)I
    .locals 1
    .param p0, "type"    # I

    .prologue
    .line 1933
    packed-switch p0, :pswitch_data_0

    .line 1939
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 1935
    :pswitch_0
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getMultiSimForegroundPhoneId()I

    move-result v0

    goto :goto_0

    .line 1937
    :pswitch_1
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getMultiSimLastRejectIncomingCallPhoneId()I

    move-result v0

    goto :goto_0

    .line 1933
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static getNetworkCountryIso(I)Ljava/lang/String;
    .locals 6
    .param p0, "slotId"    # I

    .prologue
    .line 545
    invoke-static {p0}, Landroid/telephony/SubscriptionManager;->getSubId(I)[J

    move-result-object v1

    .line 547
    .local v1, "subId":[J
    :try_start_0
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v2

    const/4 v3, 0x0

    aget-wide v4, v1, v3

    invoke-virtual {v2, v4, v5}, Landroid/telephony/TelephonyManager;->getNetworkCountryIso(J)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 549
    :goto_0
    return-object v2

    .line 548
    :catch_0
    move-exception v0

    .line 549
    .local v0, "ex":Ljava/lang/NullPointerException;
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static getNetworkCountryIsoUsingSubId(J)Ljava/lang/String;
    .locals 2
    .param p0, "subId"    # J

    .prologue
    .line 565
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Landroid/telephony/TelephonyManager;->getNetworkCountryIso(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getNetworkOperator(I)Ljava/lang/String;
    .locals 6
    .param p0, "slotId"    # I

    .prologue
    .line 483
    invoke-static {p0}, Landroid/telephony/SubscriptionManager;->getSubId(I)[J

    move-result-object v1

    .line 485
    .local v1, "subId":[J
    :try_start_0
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v2

    const/4 v3, 0x0

    aget-wide v4, v1, v3

    invoke-virtual {v2, v4, v5}, Landroid/telephony/TelephonyManager;->getNetworkOperator(J)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 487
    :goto_0
    return-object v2

    .line 486
    :catch_0
    move-exception v0

    .line 487
    .local v0, "ex":Ljava/lang/NullPointerException;
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static getNetworkOperatorName(I)Ljava/lang/String;
    .locals 6
    .param p0, "slotId"    # I

    .prologue
    .line 454
    invoke-static {p0}, Landroid/telephony/SubscriptionManager;->getSubId(I)[J

    move-result-object v1

    .line 456
    .local v1, "subId":[J
    :try_start_0
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v2

    const/4 v3, 0x0

    aget-wide v4, v1, v3

    invoke-virtual {v2, v4, v5}, Landroid/telephony/TelephonyManager;->getNetworkOperatorName(J)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 458
    :goto_0
    return-object v2

    .line 457
    :catch_0
    move-exception v0

    .line 458
    .local v0, "ex":Ljava/lang/NullPointerException;
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static getNetworkOperatorNameUsingSubId(J)Ljava/lang/String;
    .locals 2
    .param p0, "subId"    # J

    .prologue
    .line 471
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Landroid/telephony/TelephonyManager;->getNetworkOperatorName(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getNetworkOperatorUsingSubId(J)Ljava/lang/String;
    .locals 2
    .param p0, "subId"    # J

    .prologue
    .line 501
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Landroid/telephony/TelephonyManager;->getNetworkOperator(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getNetworkType(I)I
    .locals 6
    .param p0, "slotId"    # I

    .prologue
    const/4 v2, 0x0

    .line 577
    invoke-static {p0}, Landroid/telephony/SubscriptionManager;->getSubId(I)[J

    move-result-object v1

    .line 579
    .local v1, "subId":[J
    :try_start_0
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v3

    const/4 v4, 0x0

    aget-wide v4, v1, v4

    invoke-virtual {v3, v4, v5}, Landroid/telephony/TelephonyManager;->getNetworkType(J)I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 581
    :goto_0
    return v2

    .line 580
    :catch_0
    move-exception v0

    .line 581
    .local v0, "ex":Ljava/lang/NullPointerException;
    goto :goto_0
.end method

.method public static getNetworkTypeUsingSubId(J)I
    .locals 2
    .param p0, "subId"    # J

    .prologue
    .line 594
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Landroid/telephony/TelephonyManager;->getNetworkType(J)I

    move-result v0

    return v0
.end method

.method public static getPhoneId(J)I
    .locals 2
    .param p0, "subId"    # J

    .prologue
    .line 2207
    invoke-static {p0, p1}, Landroid/telephony/SubscriptionManager;->getPhoneId(J)I

    move-result v0

    return v0
.end method

.method public static getServiceState(I)I
    .locals 6
    .param p0, "slotId"    # I

    .prologue
    const/4 v2, 0x0

    .line 1448
    invoke-static {p0}, Landroid/telephony/SubscriptionManager;->getSubId(I)[J

    move-result-object v1

    .line 1450
    .local v1, "subId":[J
    :try_start_0
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v3

    const/4 v4, 0x0

    aget-wide v4, v1, v4

    invoke-virtual {v3, v4, v5}, Landroid/telephony/TelephonyManager;->getServiceState(J)I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 1452
    :goto_0
    return v2

    .line 1451
    :catch_0
    move-exception v0

    .line 1452
    .local v0, "ex":Ljava/lang/NullPointerException;
    goto :goto_0
.end method

.method public static getServiceStateUsingSubId(J)I
    .locals 2
    .param p0, "subId"    # J

    .prologue
    .line 1463
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Landroid/telephony/TelephonyManager;->getServiceState(J)I

    move-result v0

    return v0
.end method

.method public static getSimCountryIso(I)Ljava/lang/String;
    .locals 6
    .param p0, "slotId"    # I

    .prologue
    .line 764
    invoke-static {p0}, Landroid/telephony/SubscriptionManager;->getSubId(I)[J

    move-result-object v1

    .line 766
    .local v1, "subId":[J
    :try_start_0
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v2

    const/4 v3, 0x0

    aget-wide v4, v1, v3

    invoke-virtual {v2, v4, v5}, Landroid/telephony/TelephonyManager;->getSimCountryIso(J)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 768
    :goto_0
    return-object v2

    .line 767
    :catch_0
    move-exception v0

    .line 768
    .local v0, "ex":Ljava/lang/NullPointerException;
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static getSimCountryIsoUsingSubId(J)Ljava/lang/String;
    .locals 2
    .param p0, "subId"    # J

    .prologue
    .line 780
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Landroid/telephony/TelephonyManager;->getSimCountryIso(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getSimIcon(Landroid/content/Context;I)I
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "simSlot"    # I

    .prologue
    .line 153
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/telephony/MultiSimManager;->mSimIconKey:[Ljava/lang/String;

    aget-object v2, v2, p1

    const/4 v3, -0x1

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 155
    .local v0, "simIcon":I
    const-string v1, "MultiSimManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getSimIcon ="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", simSlot = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/telephony/Rlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    if-gez v0, :cond_0

    .line 157
    packed-switch p1, :pswitch_data_0

    .line 174
    const/4 v0, 0x0

    .line 179
    :cond_0
    :goto_0
    return v0

    .line 159
    :pswitch_0
    const/4 v0, 0x0

    .line 160
    goto :goto_0

    .line 162
    :pswitch_1
    const/4 v0, 0x1

    .line 163
    goto :goto_0

    .line 165
    :pswitch_2
    const/4 v0, 0x2

    .line 166
    goto :goto_0

    .line 168
    :pswitch_3
    const/4 v0, 0x3

    .line 169
    goto :goto_0

    .line 171
    :pswitch_4
    const/4 v0, 0x4

    .line 172
    goto :goto_0

    .line 157
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static getSimName(Landroid/content/Context;I)Ljava/lang/String;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "simSlot"    # I

    .prologue
    .line 117
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/telephony/MultiSimManager;->mSimNameKey:[Ljava/lang/String;

    aget-object v2, v2, p1

    invoke-static {v1, v2}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 119
    .local v0, "simName":Ljava/lang/String;
    const-string v1, "MultiSimManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getSimName ="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", simSlot = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/telephony/Rlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    if-nez v0, :cond_0

    .line 121
    packed-switch p1, :pswitch_data_0

    .line 138
    const-string v0, "SIM 1"

    .line 143
    :cond_0
    :goto_0
    return-object v0

    .line 123
    :pswitch_0
    const-string v0, "SIM 1"

    .line 124
    goto :goto_0

    .line 126
    :pswitch_1
    const-string v0, "SIM 2"

    .line 127
    goto :goto_0

    .line 129
    :pswitch_2
    const-string v0, "SIM 3"

    .line 130
    goto :goto_0

    .line 132
    :pswitch_3
    const-string v0, "SIM 4"

    .line 133
    goto :goto_0

    .line 135
    :pswitch_4
    const-string v0, "SIM 5"

    .line 136
    goto :goto_0

    .line 121
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static getSimOperator(I)Ljava/lang/String;
    .locals 6
    .param p0, "slotId"    # I

    .prologue
    .line 709
    invoke-static {p0}, Landroid/telephony/SubscriptionManager;->getSubId(I)[J

    move-result-object v1

    .line 711
    .local v1, "subId":[J
    :try_start_0
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v2

    const/4 v3, 0x0

    aget-wide v4, v1, v3

    invoke-virtual {v2, v4, v5}, Landroid/telephony/TelephonyManager;->getSimOperator(J)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 713
    :goto_0
    return-object v2

    .line 712
    :catch_0
    move-exception v0

    .line 713
    .local v0, "ex":Ljava/lang/NullPointerException;
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static getSimOperatorName(I)Ljava/lang/String;
    .locals 6
    .param p0, "slotId"    # I

    .prologue
    .line 737
    invoke-static {p0}, Landroid/telephony/SubscriptionManager;->getSubId(I)[J

    move-result-object v1

    .line 739
    .local v1, "subId":[J
    :try_start_0
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v2

    const/4 v3, 0x0

    aget-wide v4, v1, v3

    invoke-virtual {v2, v4, v5}, Landroid/telephony/TelephonyManager;->getSimOperatorName(J)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 741
    :goto_0
    return-object v2

    .line 740
    :catch_0
    move-exception v0

    .line 741
    .local v0, "ex":Ljava/lang/NullPointerException;
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static getSimOperatorNameUsingSubId(J)Ljava/lang/String;
    .locals 2
    .param p0, "subId"    # J

    .prologue
    .line 753
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Landroid/telephony/TelephonyManager;->getSimOperatorName(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getSimOperatorUsingSubId(J)Ljava/lang/String;
    .locals 2
    .param p0, "subId"    # J

    .prologue
    .line 726
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Landroid/telephony/TelephonyManager;->getSimOperator(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getSimSerialNumber(I)Ljava/lang/String;
    .locals 6
    .param p0, "slotId"    # I

    .prologue
    .line 337
    invoke-static {p0}, Landroid/telephony/SubscriptionManager;->getSubId(I)[J

    move-result-object v1

    .line 339
    .local v1, "subId":[J
    :try_start_0
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v2

    const/4 v3, 0x0

    aget-wide v4, v1, v3

    invoke-virtual {v2, v4, v5}, Landroid/telephony/TelephonyManager;->getSimSerialNumber(J)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 341
    :goto_0
    return-object v2

    .line 340
    :catch_0
    move-exception v0

    .line 341
    .local v0, "ex":Ljava/lang/NullPointerException;
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static getSimSerialNumberUsingSubId(J)Ljava/lang/String;
    .locals 2
    .param p0, "subId"    # J

    .prologue
    .line 356
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Landroid/telephony/TelephonyManager;->getSimSerialNumber(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getSimSlotCount()I
    .locals 1

    .prologue
    .line 99
    sget v0, Lcom/samsung/android/telephony/MultiSimManager;->SIMSLOT_COUNT:I

    return v0
.end method

.method public static getSimState(I)I
    .locals 1
    .param p0, "slotId"    # I

    .prologue
    .line 684
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/telephony/TelephonyManager;->getSimState(I)I

    move-result v0

    return v0
.end method

.method public static getSimStateUsingSubId(J)I
    .locals 2
    .param p0, "subId"    # J

    .prologue
    .line 696
    invoke-static {p0, p1}, Landroid/telephony/SubscriptionManager;->getSlotId(J)I

    move-result v0

    .line 697
    .local v0, "slotId":I
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/telephony/TelephonyManager;->getSimState(I)I

    move-result v1

    return v1
.end method

.method public static getSimplifiedNetworkSettingsEnabled(I)Z
    .locals 6
    .param p0, "slotId"    # I

    .prologue
    const/4 v2, 0x0

    .line 1390
    invoke-static {p0}, Landroid/telephony/SubscriptionManager;->getSubId(I)[J

    move-result-object v1

    .line 1392
    .local v1, "subId":[J
    :try_start_0
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v3

    const/4 v4, 0x0

    aget-wide v4, v1, v4

    invoke-virtual {v3, v4, v5}, Landroid/telephony/TelephonyManager;->getSimplifiedNetworkSettingsEnabledForSubscriber(J)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 1394
    :goto_0
    return v2

    .line 1393
    :catch_0
    move-exception v0

    .line 1394
    .local v0, "ex":Ljava/lang/NullPointerException;
    goto :goto_0
.end method

.method public static getSimplifiedNetworkSettingsEnabledUsingSubId(J)Z
    .locals 2
    .param p0, "subId"    # J

    .prologue
    .line 1410
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Landroid/telephony/TelephonyManager;->getSimplifiedNetworkSettingsEnabledForSubscriber(J)Z

    move-result v0

    return v0
.end method

.method public static getSlotId(J)I
    .locals 2
    .param p0, "subId"    # J

    .prologue
    .line 2189
    invoke-static {p0, p1}, Landroid/telephony/SubscriptionManager;->getSlotId(J)I

    move-result v0

    return v0
.end method

.method public static getSubId(I)[J
    .locals 1
    .param p0, "slotId"    # I

    .prologue
    .line 2198
    invoke-static {p0}, Landroid/telephony/SubscriptionManager;->getSubId(I)[J

    move-result-object v0

    return-object v0
.end method

.method public static getSubInfoForSubscriber(J)Landroid/telephony/SubInfoRecord;
    .locals 2
    .param p0, "subId"    # J

    .prologue
    .line 1951
    invoke-static {p0, p1}, Landroid/telephony/SubscriptionManager;->getSubInfoForSubscriber(J)Landroid/telephony/SubInfoRecord;

    move-result-object v0

    return-object v0
.end method

.method public static getSubInfoUsingIccId(Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .param p0, "iccId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/telephony/SubInfoRecord;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1960
    invoke-static {p0}, Landroid/telephony/SubscriptionManager;->getSubInfoUsingIccId(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static getSubInfoUsingSlotId(I)Ljava/util/List;
    .locals 1
    .param p0, "slotId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Landroid/telephony/SubInfoRecord;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1969
    invoke-static {p0}, Landroid/telephony/SubscriptionManager;->getSubInfoUsingSlotId(I)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static getSubscriberId(I)Ljava/lang/String;
    .locals 6
    .param p0, "slotId"    # I

    .prologue
    .line 277
    invoke-static {p0}, Landroid/telephony/SubscriptionManager;->getSubId(I)[J

    move-result-object v1

    .line 279
    .local v1, "subId":[J
    :try_start_0
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v2

    const/4 v3, 0x0

    aget-wide v4, v1, v3

    invoke-virtual {v2, v4, v5}, Landroid/telephony/TelephonyManager;->getSubscriberId(J)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 281
    :goto_0
    return-object v2

    .line 280
    :catch_0
    move-exception v0

    .line 281
    .local v0, "ex":Ljava/lang/NullPointerException;
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static getSubscriberIdUsingSubId(J)Ljava/lang/String;
    .locals 2
    .param p0, "subId"    # J

    .prologue
    .line 295
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Landroid/telephony/TelephonyManager;->getSubscriberId(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getTelephonyProperty(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "property"    # Ljava/lang/String;
    .param p1, "slotId"    # I
    .param p2, "defaultVal"    # Ljava/lang/String;

    .prologue
    .line 1285
    invoke-static {p1}, Landroid/telephony/SubscriptionManager;->getSubId(I)[J

    move-result-object v1

    .line 1287
    .local v1, "subId":[J
    const/4 v2, 0x0

    :try_start_0
    aget-wide v2, v1, v2

    invoke-static {p0, v2, v3, p2}, Landroid/telephony/TelephonyManager;->getTelephonyProperty(Ljava/lang/String;JLjava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p2

    .line 1289
    .end local p2    # "defaultVal":Ljava/lang/String;
    :goto_0
    return-object p2

    .line 1288
    .restart local p2    # "defaultVal":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 1289
    .local v0, "ex":Ljava/lang/NullPointerException;
    goto :goto_0
.end method

.method public static getTelephonyPropertyUsingSubId(Ljava/lang/String;JLjava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "property"    # Ljava/lang/String;
    .param p1, "subId"    # J
    .param p3, "defaultVal"    # Ljava/lang/String;

    .prologue
    .line 1303
    invoke-static {p0, p1, p2, p3}, Landroid/telephony/TelephonyManager;->getTelephonyProperty(Ljava/lang/String;JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getVoiceMailAlphaTag(I)Ljava/lang/String;
    .locals 6
    .param p0, "slotId"    # I

    .prologue
    .line 1080
    invoke-static {p0}, Landroid/telephony/SubscriptionManager;->getSubId(I)[J

    move-result-object v1

    .line 1082
    .local v1, "subId":[J
    :try_start_0
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v2

    const/4 v3, 0x0

    aget-wide v4, v1, v3

    invoke-virtual {v2, v4, v5}, Landroid/telephony/TelephonyManager;->getVoiceMailAlphaTag(J)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 1084
    :goto_0
    return-object v2

    .line 1083
    :catch_0
    move-exception v0

    .line 1084
    .local v0, "ex":Ljava/lang/NullPointerException;
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static getVoiceMailAlphaTagUsingSubId(J)Ljava/lang/String;
    .locals 2
    .param p0, "subId"    # J

    .prologue
    .line 1101
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Landroid/telephony/TelephonyManager;->getVoiceMailAlphaTag(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getVoiceMailNumber(I)Ljava/lang/String;
    .locals 6
    .param p0, "slotId"    # I

    .prologue
    .line 978
    invoke-static {p0}, Landroid/telephony/SubscriptionManager;->getSubId(I)[J

    move-result-object v1

    .line 980
    .local v1, "subId":[J
    :try_start_0
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v2

    const/4 v3, 0x0

    aget-wide v4, v1, v3

    invoke-virtual {v2, v4, v5}, Landroid/telephony/TelephonyManager;->getVoiceMailNumber(J)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 982
    :goto_0
    return-object v2

    .line 981
    :catch_0
    move-exception v0

    .line 982
    .local v0, "ex":Ljava/lang/NullPointerException;
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static getVoiceMailNumberUsingSubId(J)Ljava/lang/String;
    .locals 2
    .param p0, "subId"    # J

    .prologue
    .line 998
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Landroid/telephony/TelephonyManager;->getVoiceMailNumber(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getVoiceMessageCount(I)I
    .locals 6
    .param p0, "slotId"    # I

    .prologue
    const/4 v2, 0x0

    .line 1045
    invoke-static {p0}, Landroid/telephony/SubscriptionManager;->getSubId(I)[J

    move-result-object v1

    .line 1047
    .local v1, "subId":[J
    :try_start_0
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v3

    const/4 v4, 0x0

    aget-wide v4, v1, v4

    invoke-virtual {v3, v4, v5}, Landroid/telephony/TelephonyManager;->getVoiceMessageCount(J)I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 1049
    :goto_0
    return v2

    .line 1048
    :catch_0
    move-exception v0

    .line 1049
    .local v0, "ex":Ljava/lang/NullPointerException;
    goto :goto_0
.end method

.method public static getVoiceMessageCountUsingSubId(J)I
    .locals 2
    .param p0, "subId"    # J

    .prologue
    .line 1064
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Landroid/telephony/TelephonyManager;->getVoiceMessageCount(J)I

    move-result v0

    return v0
.end method

.method public static getVoiceNetworkType(I)I
    .locals 6
    .param p0, "slotId"    # I

    .prologue
    const/4 v2, 0x0

    .line 634
    invoke-static {p0}, Landroid/telephony/SubscriptionManager;->getSubId(I)[J

    move-result-object v1

    .line 636
    .local v1, "subId":[J
    :try_start_0
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v3

    const/4 v4, 0x0

    aget-wide v4, v1, v4

    invoke-virtual {v3, v4, v5}, Landroid/telephony/TelephonyManager;->getVoiceNetworkType(J)I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 638
    :goto_0
    return v2

    .line 637
    :catch_0
    move-exception v0

    .line 638
    .local v0, "ex":Ljava/lang/NullPointerException;
    goto :goto_0
.end method

.method public static getVoiceNetworkTypeUsingSubId(J)I
    .locals 2
    .param p0, "subId"    # J

    .prologue
    .line 650
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Landroid/telephony/TelephonyManager;->getVoiceNetworkType(J)I

    move-result v0

    return v0
.end method

.method public static handlePinMmi(ILjava/lang/String;)Z
    .locals 6
    .param p0, "slotId"    # I
    .param p1, "dialString"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 1834
    invoke-static {p0}, Landroid/telephony/SubscriptionManager;->getSubId(I)[J

    move-result-object v1

    .line 1836
    .local v1, "subId":[J
    :try_start_0
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v3

    const/4 v4, 0x0

    aget-wide v4, v1, v4

    invoke-virtual {v3, v4, v5, p1}, Landroid/telephony/TelephonyManager;->handlePinMmi(JLjava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 1838
    :goto_0
    return v2

    .line 1837
    :catch_0
    move-exception v0

    .line 1838
    .local v0, "ex":Ljava/lang/NullPointerException;
    goto :goto_0
.end method

.method public static handlePinMmiUsingSubId(JLjava/lang/String;)Z
    .locals 2
    .param p0, "subId"    # J
    .param p2, "dialString"    # Ljava/lang/String;

    .prologue
    .line 1852
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0, p0, p1, p2}, Landroid/telephony/TelephonyManager;->handlePinMmi(JLjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static hasIccCard(I)Z
    .locals 4
    .param p0, "slotId"    # I

    .prologue
    .line 661
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v0

    int-to-long v2, p0

    invoke-virtual {v0, v2, v3}, Landroid/telephony/TelephonyManager;->hasIccCard(J)Z

    move-result v0

    return v0
.end method

.method public static hasIccCardUsingSubId(J)Z
    .locals 4
    .param p0, "subId"    # J

    .prologue
    .line 672
    invoke-static {p0, p1}, Landroid/telephony/SubscriptionManager;->getSlotId(J)I

    move-result v0

    .line 673
    .local v0, "slotId":I
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v1

    int-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Landroid/telephony/TelephonyManager;->hasIccCard(J)Z

    move-result v1

    return v1
.end method

.method public static isEmergencyNumber(ILjava/lang/String;)Z
    .locals 6
    .param p0, "slotId"    # I
    .param p1, "number"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 1918
    invoke-static {p0}, Landroid/telephony/SubscriptionManager;->getSubId(I)[J

    move-result-object v1

    .line 1920
    .local v1, "subId":[J
    const/4 v3, 0x0

    :try_start_0
    aget-wide v4, v1, v3

    invoke-static {v4, v5, p1}, Landroid/telephony/PhoneNumberUtils;->isEmergencyNumber(JLjava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 1922
    :goto_0
    return v2

    .line 1921
    :catch_0
    move-exception v0

    .line 1922
    .local v0, "ex":Ljava/lang/NullPointerException;
    goto :goto_0
.end method

.method public static isEnabledSim(Landroid/content/Context;I)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "simSlot"    # I

    .prologue
    const/4 v1, 0x1

    .line 259
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/telephony/MultiSimManager;->mPhoneOnKey:[Ljava/lang/String;

    aget-object v3, v3, p1

    const/4 v4, -0x1

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 260
    .local v0, "simActive":I
    if-ne v0, v1, :cond_0

    .line 263
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isIdle(I)Z
    .locals 6
    .param p0, "slotId"    # I

    .prologue
    .line 1660
    invoke-static {p0}, Landroid/telephony/SubscriptionManager;->getSubId(I)[J

    move-result-object v1

    .line 1662
    .local v1, "subId":[J
    :try_start_0
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v2

    const/4 v3, 0x0

    aget-wide v4, v1, v3

    invoke-virtual {v2, v4, v5}, Landroid/telephony/TelephonyManager;->isIdle(J)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 1664
    :goto_0
    return v2

    .line 1663
    :catch_0
    move-exception v0

    .line 1664
    .local v0, "ex":Ljava/lang/NullPointerException;
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public static isIdleUsingSubId(J)Z
    .locals 2
    .param p0, "subId"    # J

    .prologue
    .line 1676
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Landroid/telephony/TelephonyManager;->isIdle(J)Z

    move-result v0

    return v0
.end method

.method public static isNetworkRoaming(I)Z
    .locals 6
    .param p0, "slotId"    # I

    .prologue
    const/4 v2, 0x0

    .line 513
    invoke-static {p0}, Landroid/telephony/SubscriptionManager;->getSubId(I)[J

    move-result-object v1

    .line 515
    .local v1, "subId":[J
    :try_start_0
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v3

    const/4 v4, 0x0

    aget-wide v4, v1, v4

    invoke-virtual {v3, v4, v5}, Landroid/telephony/TelephonyManager;->isNetworkRoaming(J)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 517
    :goto_0
    return v2

    .line 516
    :catch_0
    move-exception v0

    .line 517
    .local v0, "ex":Ljava/lang/NullPointerException;
    goto :goto_0
.end method

.method public static isNetworkRoamingUsingSubId(J)Z
    .locals 2
    .param p0, "subId"    # J

    .prologue
    .line 530
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Landroid/telephony/TelephonyManager;->isNetworkRoaming(J)Z

    move-result v0

    return v0
.end method

.method public static isNoSIM()Z
    .locals 5

    .prologue
    .line 239
    const/4 v0, 0x0

    .line 240
    .local v0, "count":I
    const/4 v2, 0x0

    .local v2, "simSlotNum":I
    :goto_0
    invoke-static {}, Lcom/samsung/android/telephony/MultiSimManager;->getSimSlotCount()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 241
    const-string/jumbo v3, "ril.ICC_TYPE"

    const-string v4, "0"

    invoke-static {v3, v2, v4}, Lcom/samsung/android/telephony/MultiSimManager;->getTelephonyProperty(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 242
    .local v1, "iccType":Ljava/lang/String;
    const-string v3, "0"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 243
    add-int/lit8 v0, v0, 0x1

    .line 240
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 246
    .end local v1    # "iccType":Ljava/lang/String;
    :cond_1
    invoke-static {}, Lcom/samsung/android/telephony/MultiSimManager;->getSimSlotCount()I

    move-result v3

    if-ne v0, v3, :cond_2

    .line 247
    const/4 v3, 0x1

    .line 249
    :goto_1
    return v3

    :cond_2
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public static isOffhook(I)Z
    .locals 6
    .param p0, "slotId"    # I

    .prologue
    const/4 v2, 0x0

    .line 1606
    invoke-static {p0}, Landroid/telephony/SubscriptionManager;->getSubId(I)[J

    move-result-object v1

    .line 1608
    .local v1, "subId":[J
    :try_start_0
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v3

    const/4 v4, 0x0

    aget-wide v4, v1, v4

    invoke-virtual {v3, v4, v5}, Landroid/telephony/TelephonyManager;->isOffhook(J)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 1610
    :goto_0
    return v2

    .line 1609
    :catch_0
    move-exception v0

    .line 1610
    .local v0, "ex":Ljava/lang/NullPointerException;
    goto :goto_0
.end method

.method public static isOffhookUsingSubId(J)Z
    .locals 2
    .param p0, "subId"    # J

    .prologue
    .line 1622
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Landroid/telephony/TelephonyManager;->isOffhook(J)Z

    move-result v0

    return v0
.end method

.method public static isRadioOn(I)Z
    .locals 6
    .param p0, "slotId"    # I

    .prologue
    const/4 v2, 0x0

    .line 1685
    invoke-static {p0}, Landroid/telephony/SubscriptionManager;->getSubId(I)[J

    move-result-object v1

    .line 1687
    .local v1, "subId":[J
    :try_start_0
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v3

    const/4 v4, 0x0

    aget-wide v4, v1, v4

    invoke-virtual {v3, v4, v5}, Landroid/telephony/TelephonyManager;->isRadioOn(J)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 1689
    :goto_0
    return v2

    .line 1688
    :catch_0
    move-exception v0

    .line 1689
    .local v0, "ex":Ljava/lang/NullPointerException;
    goto :goto_0
.end method

.method public static isRadioOnUsingSubId(J)Z
    .locals 2
    .param p0, "subId"    # J

    .prologue
    .line 1700
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Landroid/telephony/TelephonyManager;->isRadioOn(J)Z

    move-result v0

    return v0
.end method

.method public static isRinging(I)Z
    .locals 6
    .param p0, "slotId"    # I

    .prologue
    const/4 v2, 0x0

    .line 1633
    invoke-static {p0}, Landroid/telephony/SubscriptionManager;->getSubId(I)[J

    move-result-object v1

    .line 1635
    .local v1, "subId":[J
    :try_start_0
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v3

    const/4 v4, 0x0

    aget-wide v4, v1, v4

    invoke-virtual {v3, v4, v5}, Landroid/telephony/TelephonyManager;->isRinging(J)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 1637
    :goto_0
    return v2

    .line 1636
    :catch_0
    move-exception v0

    .line 1637
    .local v0, "ex":Ljava/lang/NullPointerException;
    goto :goto_0
.end method

.method public static isRingingUsingSubId(J)Z
    .locals 2
    .param p0, "subId"    # J

    .prologue
    .line 1650
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Landroid/telephony/TelephonyManager;->isRinging(J)Z

    move-result v0

    return v0
.end method

.method public static isValidPhoneId(I)Z
    .locals 1
    .param p0, "phoneId"    # I

    .prologue
    .line 2333
    invoke-static {p0}, Landroid/telephony/SubscriptionManager;->isValidPhoneId(I)Z

    move-result v0

    return v0
.end method

.method public static isValidSubId(J)Z
    .locals 2
    .param p0, "subId"    # J

    .prologue
    .line 2326
    invoke-static {p0, p1}, Landroid/telephony/SubscriptionManager;->isValidSubId(J)Z

    move-result v0

    return v0
.end method

.method public static putPhoneIdAndSubIdExtra(Landroid/content/Intent;I)V
    .locals 0
    .param p0, "intent"    # Landroid/content/Intent;
    .param p1, "phoneId"    # I

    .prologue
    .line 2342
    invoke-static {p0, p1}, Landroid/telephony/SubscriptionManager;->putPhoneIdAndSubIdExtra(Landroid/content/Intent;I)V

    .line 2343
    return-void
.end method

.method public static putPhoneIdAndSubIdExtra(Landroid/content/Intent;IJ)V
    .locals 0
    .param p0, "intent"    # Landroid/content/Intent;
    .param p1, "phoneId"    # I
    .param p2, "subId"    # J

    .prologue
    .line 2352
    invoke-static {p0, p1, p2, p3}, Landroid/telephony/SubscriptionManager;->putPhoneIdAndSubIdExtra(Landroid/content/Intent;IJ)V

    .line 2353
    return-void
.end method

.method public static setColor(II)I
    .locals 4
    .param p0, "color"    # I
    .param p1, "slotId"    # I

    .prologue
    .line 2035
    invoke-static {p1}, Landroid/telephony/SubscriptionManager;->getSubId(I)[J

    move-result-object v0

    .line 2036
    .local v0, "subId":[J
    const/4 v1, 0x0

    aget-wide v2, v0, v1

    invoke-static {p0, v2, v3}, Landroid/telephony/SubscriptionManager;->setColor(IJ)I

    move-result v1

    return v1
.end method

.method public static setColorUsingSubId(IJ)I
    .locals 1
    .param p0, "color"    # I
    .param p1, "subId"    # J

    .prologue
    .line 2024
    invoke-static {p0, p1, p2}, Landroid/telephony/SubscriptionManager;->setColor(IJ)I

    move-result v0

    return v0
.end method

.method public static setDataEnabled(Z)V
    .locals 1
    .param p0, "enable"    # Z

    .prologue
    .line 1905
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/telephony/TelephonyManager;->setDataEnabled(Z)V

    .line 1906
    return-void
.end method

.method public static setDataRoaming(II)I
    .locals 4
    .param p0, "roaming"    # I
    .param p1, "slotId"    # I

    .prologue
    .line 2174
    invoke-static {p1}, Landroid/telephony/SubscriptionManager;->getSubId(I)[J

    move-result-object v1

    .line 2176
    .local v1, "subId":[J
    const/4 v2, 0x0

    :try_start_0
    aget-wide v2, v1, v2

    invoke-static {p0, v2, v3}, Landroid/telephony/SubscriptionManager;->setDataRoaming(IJ)I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 2178
    :goto_0
    return v2

    .line 2177
    :catch_0
    move-exception v0

    .line 2178
    .local v0, "ex":Ljava/lang/NullPointerException;
    const/4 v2, -0x1

    goto :goto_0
.end method

.method public static setDataRoamingUsingSubId(IJ)I
    .locals 1
    .param p0, "roaming"    # I
    .param p1, "subId"    # J

    .prologue
    .line 2162
    invoke-static {p0, p1, p2}, Landroid/telephony/SubscriptionManager;->setDataRoaming(IJ)I

    move-result v0

    return v0
.end method

.method public static setDefaultSubId(IJ)V
    .locals 3
    .param p0, "type"    # I
    .param p1, "subId"    # J

    .prologue
    .line 2238
    packed-switch p0, :pswitch_data_0

    .line 2251
    const-string v0, "MultiSimManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[setDefaultSubId] Invalid Type:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/telephony/Rlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2254
    :goto_0
    return-void

    .line 2241
    :pswitch_0
    invoke-static {p1, p2}, Landroid/telephony/SubscriptionManager;->setDefaultVoiceSubId(J)V

    goto :goto_0

    .line 2244
    :pswitch_1
    invoke-static {p1, p2}, Landroid/telephony/SubscriptionManager;->setDefaultSmsSubId(J)V

    goto :goto_0

    .line 2247
    :pswitch_2
    invoke-static {p1, p2}, Landroid/telephony/SubscriptionManager;->setDefaultDataSubId(J)V

    goto :goto_0

    .line 2238
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static setDisplayName(Ljava/lang/String;I)I
    .locals 4
    .param p0, "displayName"    # Ljava/lang/String;
    .param p1, "slotId"    # I

    .prologue
    .line 2058
    invoke-static {p1}, Landroid/telephony/SubscriptionManager;->getSubId(I)[J

    move-result-object v1

    .line 2060
    .local v1, "subId":[J
    const/4 v2, 0x0

    :try_start_0
    aget-wide v2, v1, v2

    invoke-static {p0, v2, v3}, Landroid/telephony/SubscriptionManager;->setDisplayName(Ljava/lang/String;J)I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 2062
    :goto_0
    return v2

    .line 2061
    :catch_0
    move-exception v0

    .line 2062
    .local v0, "ex":Ljava/lang/NullPointerException;
    const/4 v2, -0x1

    goto :goto_0
.end method

.method public static setDisplayName(Ljava/lang/String;IJ)I
    .locals 4
    .param p0, "displayName"    # Ljava/lang/String;
    .param p1, "slotId"    # I
    .param p2, "nameSource"    # J

    .prologue
    .line 2090
    invoke-static {p1}, Landroid/telephony/SubscriptionManager;->getSubId(I)[J

    move-result-object v1

    .line 2092
    .local v1, "subId":[J
    const/4 v2, 0x0

    :try_start_0
    aget-wide v2, v1, v2

    invoke-static {p0, v2, v3, p2, p3}, Landroid/telephony/SubscriptionManager;->setDisplayName(Ljava/lang/String;JJ)I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 2094
    :goto_0
    return v2

    .line 2093
    :catch_0
    move-exception v0

    .line 2094
    .local v0, "ex":Ljava/lang/NullPointerException;
    const/4 v2, -0x1

    goto :goto_0
.end method

.method public static setDisplayNameUsingSubId(Ljava/lang/String;J)I
    .locals 1
    .param p0, "displayName"    # Ljava/lang/String;
    .param p1, "subId"    # J

    .prologue
    .line 2047
    invoke-static {p0, p1, p2}, Landroid/telephony/SubscriptionManager;->setDisplayName(Ljava/lang/String;J)I

    move-result v0

    return v0
.end method

.method public static setDisplayNameUsingSubId(Ljava/lang/String;JJ)I
    .locals 1
    .param p0, "displayName"    # Ljava/lang/String;
    .param p1, "subId"    # J
    .param p3, "nameSource"    # J

    .prologue
    .line 2076
    invoke-static {p0, p1, p2, p3, p4}, Landroid/telephony/SubscriptionManager;->setDisplayName(Ljava/lang/String;JJ)I

    move-result v0

    return v0
.end method

.method public static setDisplayNumber(Ljava/lang/String;I)I
    .locals 4
    .param p0, "number"    # Ljava/lang/String;
    .param p1, "slotId"    # I

    .prologue
    .line 2118
    invoke-static {p1}, Landroid/telephony/SubscriptionManager;->getSubId(I)[J

    move-result-object v1

    .line 2120
    .local v1, "subId":[J
    const/4 v2, 0x0

    :try_start_0
    aget-wide v2, v1, v2

    invoke-static {p0, v2, v3}, Landroid/telephony/SubscriptionManager;->setDisplayNumber(Ljava/lang/String;J)I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 2122
    :goto_0
    return v2

    .line 2121
    :catch_0
    move-exception v0

    .line 2122
    .local v0, "ex":Ljava/lang/NullPointerException;
    const/4 v2, -0x1

    goto :goto_0
.end method

.method public static setDisplayNumberFormat(II)I
    .locals 4
    .param p0, "format"    # I
    .param p1, "slotId"    # I

    .prologue
    .line 2146
    invoke-static {p1}, Landroid/telephony/SubscriptionManager;->getSubId(I)[J

    move-result-object v1

    .line 2148
    .local v1, "subId":[J
    const/4 v2, 0x0

    :try_start_0
    aget-wide v2, v1, v2

    invoke-static {p0, v2, v3}, Landroid/telephony/SubscriptionManager;->setDisplayNumberFormat(IJ)I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 2150
    :goto_0
    return v2

    .line 2149
    :catch_0
    move-exception v0

    .line 2150
    .local v0, "ex":Ljava/lang/NullPointerException;
    const/4 v2, -0x1

    goto :goto_0
.end method

.method public static setDisplayNumberFormatUsingSubId(IJ)I
    .locals 1
    .param p0, "format"    # I
    .param p1, "subId"    # J

    .prologue
    .line 2134
    invoke-static {p0, p1, p2}, Landroid/telephony/SubscriptionManager;->setDisplayNumberFormat(IJ)I

    move-result v0

    return v0
.end method

.method public static setDisplayNumberUsingSubId(Ljava/lang/String;J)I
    .locals 1
    .param p0, "number"    # Ljava/lang/String;
    .param p1, "subId"    # J

    .prologue
    .line 2106
    invoke-static {p0, p1, p2}, Landroid/telephony/SubscriptionManager;->setDisplayNumber(Ljava/lang/String;J)I

    move-result v0

    return v0
.end method

.method public static setLine1NumberForDisplay(ILjava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p0, "slotId"    # I
    .param p1, "alphaTag"    # Ljava/lang/String;
    .param p2, "number"    # Ljava/lang/String;

    .prologue
    .line 866
    invoke-static {p0}, Landroid/telephony/SubscriptionManager;->getSubId(I)[J

    move-result-object v0

    .line 868
    .local v0, "subId":[J
    :try_start_0
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v1

    const/4 v2, 0x0

    aget-wide v2, v0, v2

    invoke-virtual {v1, v2, v3, p1, p2}, Landroid/telephony/TelephonyManager;->setLine1NumberForDisplayForSubscriber(JLjava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 871
    :goto_0
    return-void

    .line 869
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static setLine1NumberForDisplayUsingSubId(JLjava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0, "subId"    # J
    .param p2, "alphaTag"    # Ljava/lang/String;
    .param p3, "number"    # Ljava/lang/String;

    .prologue
    .line 889
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0, p0, p1, p2, p3}, Landroid/telephony/TelephonyManager;->setLine1NumberForDisplayForSubscriber(JLjava/lang/String;Ljava/lang/String;)V

    .line 890
    return-void
.end method

.method public static setRadio(IZ)Z
    .locals 6
    .param p0, "slotId"    # I
    .param p1, "turnOn"    # Z

    .prologue
    const/4 v2, 0x0

    .line 1883
    invoke-static {p0}, Landroid/telephony/SubscriptionManager;->getSubId(I)[J

    move-result-object v1

    .line 1885
    .local v1, "subId":[J
    :try_start_0
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v3

    const/4 v4, 0x0

    aget-wide v4, v1, v4

    invoke-virtual {v3, v4, v5, p1}, Landroid/telephony/TelephonyManager;->setRadio(JZ)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 1887
    :goto_0
    return v2

    .line 1886
    :catch_0
    move-exception v0

    .line 1887
    .local v0, "ex":Ljava/lang/NullPointerException;
    goto :goto_0
.end method

.method public static setRadioUsingSubId(JZ)Z
    .locals 2
    .param p0, "subId"    # J
    .param p2, "turnOn"    # Z

    .prologue
    .line 1897
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0, p0, p1, p2}, Landroid/telephony/TelephonyManager;->setRadio(JZ)Z

    move-result v0

    return v0
.end method

.method public static setTelephonyProperty(Ljava/lang/String;ILjava/lang/String;)V
    .locals 4
    .param p0, "property"    # Ljava/lang/String;
    .param p1, "slotId"    # I
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 1256
    invoke-static {p1}, Landroid/telephony/SubscriptionManager;->getSubId(I)[J

    move-result-object v0

    .line 1258
    .local v0, "subId":[J
    const/4 v1, 0x0

    :try_start_0
    aget-wide v2, v0, v1

    invoke-static {p0, v2, v3, p2}, Landroid/telephony/TelephonyManager;->setTelephonyProperty(Ljava/lang/String;JLjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1261
    :goto_0
    return-void

    .line 1259
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static setTelephonyPropertyUsingSubId(Ljava/lang/String;JLjava/lang/String;)V
    .locals 1
    .param p0, "property"    # Ljava/lang/String;
    .param p1, "subId"    # J
    .param p3, "value"    # Ljava/lang/String;

    .prologue
    .line 1272
    invoke-static {p0, p1, p2, p3}, Landroid/telephony/TelephonyManager;->setTelephonyProperty(Ljava/lang/String;JLjava/lang/String;)V

    .line 1273
    return-void
.end method

.method public static supplyPin(ILjava/lang/String;)Z
    .locals 6
    .param p0, "slotId"    # I
    .param p1, "pin"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 1711
    invoke-static {p0}, Landroid/telephony/SubscriptionManager;->getSubId(I)[J

    move-result-object v1

    .line 1713
    .local v1, "subId":[J
    :try_start_0
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v3

    const/4 v4, 0x0

    aget-wide v4, v1, v4

    invoke-virtual {v3, v4, v5, p1}, Landroid/telephony/TelephonyManager;->supplyPin(JLjava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 1715
    :goto_0
    return v2

    .line 1714
    :catch_0
    move-exception v0

    .line 1715
    .local v0, "ex":Ljava/lang/NullPointerException;
    goto :goto_0
.end method

.method public static supplyPinReportResult(ILjava/lang/String;)[I
    .locals 7
    .param p0, "slotId"    # I
    .param p1, "pin"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 1770
    invoke-static {p0}, Landroid/telephony/SubscriptionManager;->getSubId(I)[J

    move-result-object v1

    .line 1772
    .local v1, "subId":[J
    :try_start_0
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v2

    const/4 v3, 0x0

    aget-wide v4, v1, v3

    invoke-virtual {v2, v4, v5, p1}, Landroid/telephony/TelephonyManager;->supplyPinReportResult(JLjava/lang/String;)[I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 1774
    :goto_0
    return-object v2

    .line 1773
    :catch_0
    move-exception v0

    .line 1774
    .local v0, "ex":Ljava/lang/NullPointerException;
    new-array v2, v6, [I

    goto :goto_0
.end method

.method public static supplyPinReportResultUsingSubId(JLjava/lang/String;)[I
    .locals 2
    .param p0, "subId"    # J
    .param p2, "pin"    # Ljava/lang/String;

    .prologue
    .line 1788
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0, p0, p1, p2}, Landroid/telephony/TelephonyManager;->supplyPinReportResult(JLjava/lang/String;)[I

    move-result-object v0

    return-object v0
.end method

.method public static supplyPinUsingSubId(JLjava/lang/String;)Z
    .locals 2
    .param p0, "subId"    # J
    .param p2, "pin"    # Ljava/lang/String;

    .prologue
    .line 1728
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0, p0, p1, p2}, Landroid/telephony/TelephonyManager;->supplyPin(JLjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static supplyPuk(ILjava/lang/String;Ljava/lang/String;)Z
    .locals 6
    .param p0, "slotId"    # I
    .param p1, "puk"    # Ljava/lang/String;
    .param p2, "pin"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 1740
    invoke-static {p0}, Landroid/telephony/SubscriptionManager;->getSubId(I)[J

    move-result-object v1

    .line 1742
    .local v1, "subId":[J
    :try_start_0
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v3

    const/4 v4, 0x0

    aget-wide v4, v1, v4

    invoke-virtual {v3, v4, v5, p1, p2}, Landroid/telephony/TelephonyManager;->supplyPuk(JLjava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 1744
    :goto_0
    return v2

    .line 1743
    :catch_0
    move-exception v0

    .line 1744
    .local v0, "ex":Ljava/lang/NullPointerException;
    goto :goto_0
.end method

.method public static supplyPukReportResult(ILjava/lang/String;Ljava/lang/String;)[I
    .locals 7
    .param p0, "slotId"    # I
    .param p1, "puk"    # Ljava/lang/String;
    .param p2, "pin"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 1802
    invoke-static {p0}, Landroid/telephony/SubscriptionManager;->getSubId(I)[J

    move-result-object v1

    .line 1804
    .local v1, "subId":[J
    :try_start_0
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v2

    const/4 v3, 0x0

    aget-wide v4, v1, v3

    invoke-virtual {v2, v4, v5, p1, p2}, Landroid/telephony/TelephonyManager;->supplyPukReportResult(JLjava/lang/String;Ljava/lang/String;)[I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 1806
    :goto_0
    return-object v2

    .line 1805
    :catch_0
    move-exception v0

    .line 1806
    .local v0, "ex":Ljava/lang/NullPointerException;
    new-array v2, v6, [I

    goto :goto_0
.end method

.method public static supplyPukReportResultUsingSubId(JLjava/lang/String;Ljava/lang/String;)[I
    .locals 2
    .param p0, "subId"    # J
    .param p2, "puk"    # Ljava/lang/String;
    .param p3, "pin"    # Ljava/lang/String;

    .prologue
    .line 1822
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0, p0, p1, p2, p3}, Landroid/telephony/TelephonyManager;->supplyPukReportResult(JLjava/lang/String;Ljava/lang/String;)[I

    move-result-object v0

    return-object v0
.end method

.method public static supplyPukUsingSubId(JLjava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p0, "subId"    # J
    .param p2, "puk"    # Ljava/lang/String;
    .param p3, "pin"    # Ljava/lang/String;

    .prologue
    .line 1758
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0, p0, p1, p2, p3}, Landroid/telephony/TelephonyManager;->supplyPuk(JLjava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static toggleRadioOnOff(I)V
    .locals 4
    .param p0, "slotId"    # I

    .prologue
    .line 1861
    invoke-static {p0}, Landroid/telephony/SubscriptionManager;->getSubId(I)[J

    move-result-object v0

    .line 1863
    .local v0, "subId":[J
    :try_start_0
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v1

    const/4 v2, 0x0

    aget-wide v2, v0, v2

    invoke-virtual {v1, v2, v3}, Landroid/telephony/TelephonyManager;->toggleRadioOnOff(J)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1866
    :goto_0
    return-void

    .line 1864
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static toggleRadioOnOffUsingSubId(J)V
    .locals 2
    .param p0, "subId"    # J

    .prologue
    .line 1874
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Landroid/telephony/TelephonyManager;->toggleRadioOnOff(J)V

    .line 1875
    return-void
.end method


# virtual methods
.method public getCompleteVoiceMailNumber(I)Ljava/lang/String;
    .locals 6
    .param p1, "slotId"    # I

    .prologue
    .line 1012
    invoke-static {p1}, Landroid/telephony/SubscriptionManager;->getSubId(I)[J

    move-result-object v1

    .line 1014
    .local v1, "subId":[J
    :try_start_0
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v2

    const/4 v3, 0x0

    aget-wide v4, v1, v3

    invoke-virtual {v2, v4, v5}, Landroid/telephony/TelephonyManager;->getCompleteVoiceMailNumber(J)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 1016
    :goto_0
    return-object v2

    .line 1015
    :catch_0
    move-exception v0

    .line 1016
    .local v0, "ex":Ljava/lang/NullPointerException;
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getCompleteVoiceMailNumberUsingSubId(J)Ljava/lang/String;
    .locals 1
    .param p1, "subId"    # J

    .prologue
    .line 1031
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/telephony/TelephonyManager;->getCompleteVoiceMailNumber(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
