.class final enum Lcom/samsung/android/contextaware/dataprovider/sensorhubprovider/builtin/PedoCalibrationRunner$ContextValIndex;
.super Ljava/lang/Enum;
.source "PedoCalibrationRunner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/contextaware/dataprovider/sensorhubprovider/builtin/PedoCalibrationRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "ContextValIndex"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/contextaware/dataprovider/sensorhubprovider/builtin/PedoCalibrationRunner$ContextValIndex;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/contextaware/dataprovider/sensorhubprovider/builtin/PedoCalibrationRunner$ContextValIndex;

.field public static final enum MessageType:Lcom/samsung/android/contextaware/dataprovider/sensorhubprovider/builtin/PedoCalibrationRunner$ContextValIndex;

.field public static final enum ScaleFactor:Lcom/samsung/android/contextaware/dataprovider/sensorhubprovider/builtin/PedoCalibrationRunner$ContextValIndex;

.field public static final enum ScaleFactorArray:Lcom/samsung/android/contextaware/dataprovider/sensorhubprovider/builtin/PedoCalibrationRunner$ContextValIndex;

.field public static final enum ScaleFactorCount:Lcom/samsung/android/contextaware/dataprovider/sensorhubprovider/builtin/PedoCalibrationRunner$ContextValIndex;

.field public static final enum StepCount:Lcom/samsung/android/contextaware/dataprovider/sensorhubprovider/builtin/PedoCalibrationRunner$ContextValIndex;

.field public static final enum StepLength:Lcom/samsung/android/contextaware/dataprovider/sensorhubprovider/builtin/PedoCalibrationRunner$ContextValIndex;


# instance fields
.field private val:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 51
    new-instance v0, Lcom/samsung/android/contextaware/dataprovider/sensorhubprovider/builtin/PedoCalibrationRunner$ContextValIndex;

    const-string v1, "MessageType"

    invoke-direct {v0, v1, v4, v4}, Lcom/samsung/android/contextaware/dataprovider/sensorhubprovider/builtin/PedoCalibrationRunner$ContextValIndex;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/android/contextaware/dataprovider/sensorhubprovider/builtin/PedoCalibrationRunner$ContextValIndex;->MessageType:Lcom/samsung/android/contextaware/dataprovider/sensorhubprovider/builtin/PedoCalibrationRunner$ContextValIndex;

    .line 52
    new-instance v0, Lcom/samsung/android/contextaware/dataprovider/sensorhubprovider/builtin/PedoCalibrationRunner$ContextValIndex;

    const-string v1, "StepCount"

    invoke-direct {v0, v1, v5, v5}, Lcom/samsung/android/contextaware/dataprovider/sensorhubprovider/builtin/PedoCalibrationRunner$ContextValIndex;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/android/contextaware/dataprovider/sensorhubprovider/builtin/PedoCalibrationRunner$ContextValIndex;->StepCount:Lcom/samsung/android/contextaware/dataprovider/sensorhubprovider/builtin/PedoCalibrationRunner$ContextValIndex;

    .line 53
    new-instance v0, Lcom/samsung/android/contextaware/dataprovider/sensorhubprovider/builtin/PedoCalibrationRunner$ContextValIndex;

    const-string v1, "StepLength"

    invoke-direct {v0, v1, v6, v6}, Lcom/samsung/android/contextaware/dataprovider/sensorhubprovider/builtin/PedoCalibrationRunner$ContextValIndex;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/android/contextaware/dataprovider/sensorhubprovider/builtin/PedoCalibrationRunner$ContextValIndex;->StepLength:Lcom/samsung/android/contextaware/dataprovider/sensorhubprovider/builtin/PedoCalibrationRunner$ContextValIndex;

    .line 54
    new-instance v0, Lcom/samsung/android/contextaware/dataprovider/sensorhubprovider/builtin/PedoCalibrationRunner$ContextValIndex;

    const-string v1, "ScaleFactor"

    invoke-direct {v0, v1, v7, v7}, Lcom/samsung/android/contextaware/dataprovider/sensorhubprovider/builtin/PedoCalibrationRunner$ContextValIndex;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/android/contextaware/dataprovider/sensorhubprovider/builtin/PedoCalibrationRunner$ContextValIndex;->ScaleFactor:Lcom/samsung/android/contextaware/dataprovider/sensorhubprovider/builtin/PedoCalibrationRunner$ContextValIndex;

    .line 55
    new-instance v0, Lcom/samsung/android/contextaware/dataprovider/sensorhubprovider/builtin/PedoCalibrationRunner$ContextValIndex;

    const-string v1, "ScaleFactorCount"

    invoke-direct {v0, v1, v8, v8}, Lcom/samsung/android/contextaware/dataprovider/sensorhubprovider/builtin/PedoCalibrationRunner$ContextValIndex;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/android/contextaware/dataprovider/sensorhubprovider/builtin/PedoCalibrationRunner$ContextValIndex;->ScaleFactorCount:Lcom/samsung/android/contextaware/dataprovider/sensorhubprovider/builtin/PedoCalibrationRunner$ContextValIndex;

    .line 56
    new-instance v0, Lcom/samsung/android/contextaware/dataprovider/sensorhubprovider/builtin/PedoCalibrationRunner$ContextValIndex;

    const-string v1, "ScaleFactorArray"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/contextaware/dataprovider/sensorhubprovider/builtin/PedoCalibrationRunner$ContextValIndex;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/samsung/android/contextaware/dataprovider/sensorhubprovider/builtin/PedoCalibrationRunner$ContextValIndex;->ScaleFactorArray:Lcom/samsung/android/contextaware/dataprovider/sensorhubprovider/builtin/PedoCalibrationRunner$ContextValIndex;

    .line 48
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/samsung/android/contextaware/dataprovider/sensorhubprovider/builtin/PedoCalibrationRunner$ContextValIndex;

    sget-object v1, Lcom/samsung/android/contextaware/dataprovider/sensorhubprovider/builtin/PedoCalibrationRunner$ContextValIndex;->MessageType:Lcom/samsung/android/contextaware/dataprovider/sensorhubprovider/builtin/PedoCalibrationRunner$ContextValIndex;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/android/contextaware/dataprovider/sensorhubprovider/builtin/PedoCalibrationRunner$ContextValIndex;->StepCount:Lcom/samsung/android/contextaware/dataprovider/sensorhubprovider/builtin/PedoCalibrationRunner$ContextValIndex;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/android/contextaware/dataprovider/sensorhubprovider/builtin/PedoCalibrationRunner$ContextValIndex;->StepLength:Lcom/samsung/android/contextaware/dataprovider/sensorhubprovider/builtin/PedoCalibrationRunner$ContextValIndex;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/android/contextaware/dataprovider/sensorhubprovider/builtin/PedoCalibrationRunner$ContextValIndex;->ScaleFactor:Lcom/samsung/android/contextaware/dataprovider/sensorhubprovider/builtin/PedoCalibrationRunner$ContextValIndex;

    aput-object v1, v0, v7

    sget-object v1, Lcom/samsung/android/contextaware/dataprovider/sensorhubprovider/builtin/PedoCalibrationRunner$ContextValIndex;->ScaleFactorCount:Lcom/samsung/android/contextaware/dataprovider/sensorhubprovider/builtin/PedoCalibrationRunner$ContextValIndex;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/android/contextaware/dataprovider/sensorhubprovider/builtin/PedoCalibrationRunner$ContextValIndex;->ScaleFactorArray:Lcom/samsung/android/contextaware/dataprovider/sensorhubprovider/builtin/PedoCalibrationRunner$ContextValIndex;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/android/contextaware/dataprovider/sensorhubprovider/builtin/PedoCalibrationRunner$ContextValIndex;->$VALUES:[Lcom/samsung/android/contextaware/dataprovider/sensorhubprovider/builtin/PedoCalibrationRunner$ContextValIndex;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "v"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 60
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 61
    iput p3, p0, Lcom/samsung/android/contextaware/dataprovider/sensorhubprovider/builtin/PedoCalibrationRunner$ContextValIndex;->val:I

    .line 62
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/contextaware/dataprovider/sensorhubprovider/builtin/PedoCalibrationRunner$ContextValIndex;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/contextaware/dataprovider/sensorhubprovider/builtin/PedoCalibrationRunner$ContextValIndex;

    .prologue
    .line 48
    iget v0, p0, Lcom/samsung/android/contextaware/dataprovider/sensorhubprovider/builtin/PedoCalibrationRunner$ContextValIndex;->val:I

    return v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/contextaware/dataprovider/sensorhubprovider/builtin/PedoCalibrationRunner$ContextValIndex;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 48
    const-class v0, Lcom/samsung/android/contextaware/dataprovider/sensorhubprovider/builtin/PedoCalibrationRunner$ContextValIndex;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/contextaware/dataprovider/sensorhubprovider/builtin/PedoCalibrationRunner$ContextValIndex;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/contextaware/dataprovider/sensorhubprovider/builtin/PedoCalibrationRunner$ContextValIndex;
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lcom/samsung/android/contextaware/dataprovider/sensorhubprovider/builtin/PedoCalibrationRunner$ContextValIndex;->$VALUES:[Lcom/samsung/android/contextaware/dataprovider/sensorhubprovider/builtin/PedoCalibrationRunner$ContextValIndex;

    invoke-virtual {v0}, [Lcom/samsung/android/contextaware/dataprovider/sensorhubprovider/builtin/PedoCalibrationRunner$ContextValIndex;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/contextaware/dataprovider/sensorhubprovider/builtin/PedoCalibrationRunner$ContextValIndex;

    return-object v0
.end method
