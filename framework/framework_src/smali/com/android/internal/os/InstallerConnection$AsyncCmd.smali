.class Lcom/android/internal/os/InstallerConnection$AsyncCmd;
.super Ljava/lang/Object;
.source "InstallerConnection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/os/InstallerConnection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AsyncCmd"
.end annotation


# instance fields
.field private ackTime:J

.field private cmd:I

.field private cmdBuilder:Ljava/lang/StringBuilder;

.field private failCount:I

.field private replyTime:J

.field private startTime:J

.field private targetPkgStr:Ljava/lang/String;

.field final synthetic this$0:Lcom/android/internal/os/InstallerConnection;


# direct methods
.method public constructor <init>(Lcom/android/internal/os/InstallerConnection;I)V
    .locals 2
    .param p2, "cmd"    # I

    .prologue
    .line 438
    iput-object p1, p0, Lcom/android/internal/os/InstallerConnection$AsyncCmd;->this$0:Lcom/android/internal/os/InstallerConnection;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 439
    iput p2, p0, Lcom/android/internal/os/InstallerConnection$AsyncCmd;->cmd:I

    .line 440
    invoke-direct {p0, p2}, Lcom/android/internal/os/InstallerConnection$AsyncCmd;->convertCmdIntToStr(I)Ljava/lang/String;

    move-result-object v0

    .line 441
    .local v0, "cmdStr":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/android/internal/os/InstallerConnection$AsyncCmd;->cmdBuilder:Ljava/lang/StringBuilder;

    .line 442
    return-void
.end method

.method private convertCmdIntToStr(I)Ljava/lang/String;
    .locals 3
    .param p1, "command"    # I

    .prologue
    .line 445
    const/4 v0, 0x0

    .line 446
    .local v0, "ret":Ljava/lang/String;
    iget v1, p0, Lcom/android/internal/os/InstallerConnection$AsyncCmd;->cmd:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 447
    const-string v0, "asyncDexopt"

    .line 449
    :cond_0
    return-object v0
.end method


# virtual methods
.method public append(Ljava/lang/Object;)Lcom/android/internal/os/InstallerConnection$AsyncCmd;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)",
            "Lcom/android/internal/os/InstallerConnection$AsyncCmd;"
        }
    .end annotation

    .prologue
    .line 459
    .local p1, "a":Ljava/lang/Object;, "TT;"
    iget-object v0, p0, Lcom/android/internal/os/InstallerConnection$AsyncCmd;->cmdBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 460
    return-object p0
.end method

.method public appendPackage(Ljava/lang/String;)Lcom/android/internal/os/InstallerConnection$AsyncCmd;
    .locals 1
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 453
    iput-object p1, p0, Lcom/android/internal/os/InstallerConnection$AsyncCmd;->targetPkgStr:Ljava/lang/String;

    .line 454
    iget-object v0, p0, Lcom/android/internal/os/InstallerConnection$AsyncCmd;->cmdBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 455
    return-object p0
.end method

.method public checkAck(Lcom/android/internal/os/InstallerConnection$AsyncReply;)Z
    .locals 4
    .param p1, "reply"    # Lcom/android/internal/os/InstallerConnection$AsyncReply;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 469
    invoke-virtual {p1}, Lcom/android/internal/os/InstallerConnection$AsyncReply;->getCmd()I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 470
    iget-object v2, p0, Lcom/android/internal/os/InstallerConnection$AsyncCmd;->targetPkgStr:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lcom/android/internal/os/InstallerConnection$AsyncReply;->getExtra(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 474
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public checkMatched(Lcom/android/internal/os/InstallerConnection$AsyncReply;)Z
    .locals 3
    .param p1, "reply"    # Lcom/android/internal/os/InstallerConnection$AsyncReply;

    .prologue
    const/4 v0, 0x0

    .line 478
    iget v1, p0, Lcom/android/internal/os/InstallerConnection$AsyncCmd;->cmd:I

    invoke-virtual {p1}, Lcom/android/internal/os/InstallerConnection$AsyncReply;->getCmd()I

    move-result v2

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/android/internal/os/InstallerConnection$AsyncCmd;->targetPkgStr:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/android/internal/os/InstallerConnection$AsyncReply;->getExtra(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 480
    const/4 v0, 0x1

    .line 482
    :cond_0
    return v0
.end method

.method public fail()V
    .locals 1

    .prologue
    .line 486
    iget v0, p0, Lcom/android/internal/os/InstallerConnection$AsyncCmd;->failCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/internal/os/InstallerConnection$AsyncCmd;->failCount:I

    .line 487
    return-void
.end method

.method public getFailCount()I
    .locals 1

    .prologue
    .line 490
    iget v0, p0, Lcom/android/internal/os/InstallerConnection$AsyncCmd;->failCount:I

    return v0
.end method

.method public recordAckTime()V
    .locals 0

    .prologue
    .line 503
    return-void
.end method

.method public recordReplyTime()V
    .locals 0

    .prologue
    .line 509
    return-void
.end method

.method public recordStartTime()V
    .locals 0

    .prologue
    .line 497
    return-void
.end method

.method public recordStatistics()V
    .locals 0

    .prologue
    .line 517
    return-void
.end method

.method public timesToString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 527
    const-string v0, "NO DEBUG BUILD"

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 465
    iget-object v0, p0, Lcom/android/internal/os/InstallerConnection$AsyncCmd;->cmdBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
