.class public Lcom/android/internal/os/InstallerConnection;
.super Ljava/lang/Object;
.source "InstallerConnection.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/os/InstallerConnection$AsyncReply;,
        Lcom/android/internal/os/InstallerConnection$AsyncCmd;
    }
.end annotation


# static fields
.field private static final ACK:I = 0x1

.field private static final ACK_STR:Ljava/lang/String; = "CommandACK"

.field private static final ASYNC_DEXOPT:I = 0x2

.field private static final ASYNC_DEXOPT_STR:Ljava/lang/String; = "asyncDexopt"

.field private static final FAIL:I = -0x1

.field private static final LOCAL_DEBUG:Z = false

.field private static final MAX_ASYNC_COUNT:I

.field private static final MAX_RETRY:I = 0x5

.field private static final SUCCESS:I = 0x0

.field private static final TAG:Ljava/lang/String; = "InstallerConnection"


# instance fields
.field private ackDurationSum:F

.field private asyncCmds:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/internal/os/InstallerConnection$AsyncCmd;",
            ">;"
        }
    .end annotation
.end field

.field private final buf:[B

.field private buflen:I

.field private dexoptDurationSum:F

.field private mIn:Ljava/io/InputStream;

.field private mOut:Ljava/io/OutputStream;

.field private mSocket:Landroid/net/LocalSocket;

.field private totalDurationSum:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 63
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Runtime;->availableProcessors()I

    move-result v0

    sput v0, Lcom/android/internal/os/InstallerConnection;->MAX_ASYNC_COUNT:I

    .line 64
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/internal/os/InstallerConnection;->asyncCmds:Ljava/util/ArrayList;

    .line 60
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/internal/os/InstallerConnection;->buflen:I

    .line 71
    const/16 v0, 0x400

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/android/internal/os/InstallerConnection;->buf:[B

    .line 74
    return-void
.end method

.method private declared-synchronized addStatisticsData(FFF)V
    .locals 0
    .param p1, "ack"    # F
    .param p2, "dexopt"    # F
    .param p3, "total"    # F

    .prologue
    .line 618
    monitor-enter p0

    monitor-exit p0

    return-void
.end method

.method private asyncExecute(Lcom/android/internal/os/InstallerConnection$AsyncCmd;)Z
    .locals 6
    .param p1, "cmd"    # Lcom/android/internal/os/InstallerConnection$AsyncCmd;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 358
    invoke-direct {p0}, Lcom/android/internal/os/InstallerConnection;->connect()Z

    move-result v4

    if-nez v4, :cond_1

    .line 359
    const-string v3, "InstallerConnection"

    const-string v4, "connection failed"

    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 395
    :cond_0
    :goto_0
    return v2

    .line 363
    :cond_1
    :goto_1
    iget-object v4, p0, Lcom/android/internal/os/InstallerConnection;->asyncCmds:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    sget v5, Lcom/android/internal/os/InstallerConnection;->MAX_ASYNC_COUNT:I

    if-le v4, v5, :cond_2

    .line 365
    :try_start_0
    invoke-virtual {p0}, Lcom/android/internal/os/InstallerConnection;->processAsync()Lcom/android/internal/os/InstallerConnection$AsyncReply;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 366
    :catch_0
    move-exception v0

    .line 367
    .local v0, "e":Ljava/io/IOException;
    const-string v4, "InstallerConnection"

    const-string v5, "async command result error"

    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 371
    .end local v0    # "e":Ljava/io/IOException;
    :cond_2
    invoke-virtual {p1}, Lcom/android/internal/os/InstallerConnection$AsyncCmd;->recordStartTime()V

    .line 372
    invoke-virtual {p1}, Lcom/android/internal/os/InstallerConnection$AsyncCmd;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/android/internal/os/InstallerConnection;->writeCommand(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 373
    const-string v3, "InstallerConnection"

    const-string v4, "Write cmd fail"

    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 380
    :cond_3
    :try_start_1
    invoke-virtual {p0}, Lcom/android/internal/os/InstallerConnection;->processAsync()Lcom/android/internal/os/InstallerConnection$AsyncReply;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v1

    .line 385
    .local v1, "reply":Lcom/android/internal/os/InstallerConnection$AsyncReply;
    :goto_2
    invoke-virtual {v1}, Lcom/android/internal/os/InstallerConnection$AsyncReply;->getCmd()I

    move-result v4

    if-ne v4, v3, :cond_3

    .line 387
    invoke-virtual {p1}, Lcom/android/internal/os/InstallerConnection$AsyncCmd;->recordAckTime()V

    .line 388
    invoke-virtual {v1}, Lcom/android/internal/os/InstallerConnection$AsyncReply;->checkSuccess()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 389
    iget-object v2, p0, Lcom/android/internal/os/InstallerConnection;->asyncCmds:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v2, v3

    .line 390
    goto :goto_0

    .line 381
    .end local v1    # "reply":Lcom/android/internal/os/InstallerConnection$AsyncReply;
    :catch_1
    move-exception v0

    .line 382
    .restart local v0    # "e":Ljava/io/IOException;
    const-string v4, "InstallerConnection"

    const-string v5, "async command result error"

    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 383
    new-instance v1, Lcom/android/internal/os/InstallerConnection$AsyncReply;

    invoke-direct {v1, p0}, Lcom/android/internal/os/InstallerConnection$AsyncReply;-><init>(Lcom/android/internal/os/InstallerConnection;)V

    .restart local v1    # "reply":Lcom/android/internal/os/InstallerConnection$AsyncReply;
    goto :goto_2

    .line 391
    .end local v0    # "e":Ljava/io/IOException;
    :cond_4
    invoke-virtual {p1}, Lcom/android/internal/os/InstallerConnection$AsyncCmd;->getFailCount()I

    move-result v3

    const/4 v4, 0x5

    if-ge v3, v4, :cond_0

    .line 392
    invoke-virtual {p1}, Lcom/android/internal/os/InstallerConnection$AsyncCmd;->fail()V

    .line 393
    invoke-direct {p0, p1}, Lcom/android/internal/os/InstallerConnection;->asyncExecute(Lcom/android/internal/os/InstallerConnection$AsyncCmd;)Z

    move-result v2

    goto :goto_0
.end method

.method private connect()Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 201
    iget-object v3, p0, Lcom/android/internal/os/InstallerConnection;->mSocket:Landroid/net/LocalSocket;

    if-eqz v3, :cond_0

    .line 219
    :goto_0
    return v2

    .line 204
    :cond_0
    const-string v3, "InstallerConnection"

    const-string v4, "connecting..."

    invoke-static {v3, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 206
    :try_start_0
    new-instance v3, Landroid/net/LocalSocket;

    invoke-direct {v3}, Landroid/net/LocalSocket;-><init>()V

    iput-object v3, p0, Lcom/android/internal/os/InstallerConnection;->mSocket:Landroid/net/LocalSocket;

    .line 208
    new-instance v0, Landroid/net/LocalSocketAddress;

    const-string v3, "installd"

    sget-object v4, Landroid/net/LocalSocketAddress$Namespace;->RESERVED:Landroid/net/LocalSocketAddress$Namespace;

    invoke-direct {v0, v3, v4}, Landroid/net/LocalSocketAddress;-><init>(Ljava/lang/String;Landroid/net/LocalSocketAddress$Namespace;)V

    .line 211
    .local v0, "address":Landroid/net/LocalSocketAddress;
    iget-object v3, p0, Lcom/android/internal/os/InstallerConnection;->mSocket:Landroid/net/LocalSocket;

    invoke-virtual {v3, v0}, Landroid/net/LocalSocket;->connect(Landroid/net/LocalSocketAddress;)V

    .line 213
    iget-object v3, p0, Lcom/android/internal/os/InstallerConnection;->mSocket:Landroid/net/LocalSocket;

    invoke-virtual {v3}, Landroid/net/LocalSocket;->getInputStream()Ljava/io/InputStream;

    move-result-object v3

    iput-object v3, p0, Lcom/android/internal/os/InstallerConnection;->mIn:Ljava/io/InputStream;

    .line 214
    iget-object v3, p0, Lcom/android/internal/os/InstallerConnection;->mSocket:Landroid/net/LocalSocket;

    invoke-virtual {v3}, Landroid/net/LocalSocket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v3

    iput-object v3, p0, Lcom/android/internal/os/InstallerConnection;->mOut:Ljava/io/OutputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 215
    .end local v0    # "address":Landroid/net/LocalSocketAddress;
    :catch_0
    move-exception v1

    .line 216
    .local v1, "ex":Ljava/io/IOException;
    invoke-virtual {p0}, Lcom/android/internal/os/InstallerConnection;->disconnect()V

    .line 217
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private readBytes([BI)Z
    .locals 7
    .param p1, "buffer"    # [B
    .param p2, "len"    # I

    .prologue
    const/4 v3, 0x0

    .line 252
    const/4 v2, 0x0

    .line 253
    .local v2, "off":I
    if-gez p2, :cond_1

    .line 274
    :goto_0
    return v3

    .line 262
    .local v0, "count":I
    :cond_0
    add-int/2addr v2, v0

    .line 255
    .end local v0    # "count":I
    :cond_1
    if-eq v2, p2, :cond_2

    .line 257
    :try_start_0
    iget-object v4, p0, Lcom/android/internal/os/InstallerConnection;->mIn:Ljava/io/InputStream;

    sub-int v5, p2, v2

    invoke-virtual {v4, p1, v2, v5}, Ljava/io/InputStream;->read([BII)I

    move-result v0

    .line 258
    .restart local v0    # "count":I
    if-gtz v0, :cond_0

    .line 259
    const-string v4, "InstallerConnection"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "read error "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 271
    .end local v0    # "count":I
    :cond_2
    :goto_1
    if-ne v2, p2, :cond_3

    .line 272
    const/4 v3, 0x1

    goto :goto_0

    .line 263
    :catch_0
    move-exception v1

    .line 264
    .local v1, "ex":Ljava/io/IOException;
    const-string v4, "InstallerConnection"

    const-string/jumbo v5, "read exception"

    invoke-static {v4, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 273
    .end local v1    # "ex":Ljava/io/IOException;
    :cond_3
    invoke-virtual {p0}, Lcom/android/internal/os/InstallerConnection;->disconnect()V

    goto :goto_0
.end method

.method private readFully([BI)Z
    .locals 4
    .param p1, "buffer"    # [B
    .param p2, "len"    # I

    .prologue
    const/4 v1, 0x0

    .line 236
    :try_start_0
    iget-object v2, p0, Lcom/android/internal/os/InstallerConnection;->mIn:Ljava/io/InputStream;

    const/4 v3, 0x0

    invoke-static {v2, p1, v3, p2}, Llibcore/io/Streams;->readFully(Ljava/io/InputStream;[BII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 247
    const/4 v1, 0x1

    :goto_0
    return v1

    .line 237
    :catch_0
    move-exception v0

    .line 238
    .local v0, "ioe":Ljava/io/IOException;
    const-string v2, "InstallerConnection"

    const-string/jumbo v3, "read exception"

    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 239
    invoke-virtual {p0}, Lcom/android/internal/os/InstallerConnection;->disconnect()V

    goto :goto_0
.end method

.method private readReply()I
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, -0x1

    .line 279
    iget-object v2, p0, Lcom/android/internal/os/InstallerConnection;->buf:[B

    const/4 v3, 0x2

    invoke-direct {p0, v2, v3}, Lcom/android/internal/os/InstallerConnection;->readFully([BI)Z

    move-result v2

    if-nez v2, :cond_1

    move v0, v1

    .line 294
    :cond_0
    :goto_0
    return v0

    .line 283
    :cond_1
    iget-object v2, p0, Lcom/android/internal/os/InstallerConnection;->buf:[B

    const/4 v3, 0x0

    aget-byte v2, v2, v3

    and-int/lit16 v2, v2, 0xff

    iget-object v3, p0, Lcom/android/internal/os/InstallerConnection;->buf:[B

    aget-byte v3, v3, v4

    and-int/lit16 v3, v3, 0xff

    shl-int/lit8 v3, v3, 0x8

    or-int v0, v2, v3

    .line 284
    .local v0, "len":I
    if-lt v0, v4, :cond_2

    iget-object v2, p0, Lcom/android/internal/os/InstallerConnection;->buf:[B

    array-length v2, v2

    if-le v0, v2, :cond_3

    .line 285
    :cond_2
    const-string v2, "InstallerConnection"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "invalid reply length ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 286
    invoke-virtual {p0}, Lcom/android/internal/os/InstallerConnection;->disconnect()V

    move v0, v1

    .line 287
    goto :goto_0

    .line 290
    :cond_3
    iget-object v2, p0, Lcom/android/internal/os/InstallerConnection;->buf:[B

    invoke-direct {p0, v2, v0}, Lcom/android/internal/os/InstallerConnection;->readFully([BI)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 291
    goto :goto_0
.end method

.method private readReplyFromSocket()Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 304
    iput v1, p0, Lcom/android/internal/os/InstallerConnection;->buflen:I

    .line 305
    iget-object v3, p0, Lcom/android/internal/os/InstallerConnection;->buf:[B

    const/4 v4, 0x2

    invoke-direct {p0, v3, v4}, Lcom/android/internal/os/InstallerConnection;->readBytes([BI)Z

    move-result v3

    if-nez v3, :cond_1

    .line 316
    :cond_0
    :goto_0
    return v1

    .line 307
    :cond_1
    iget-object v3, p0, Lcom/android/internal/os/InstallerConnection;->buf:[B

    aget-byte v3, v3, v1

    and-int/lit16 v3, v3, 0xff

    iget-object v4, p0, Lcom/android/internal/os/InstallerConnection;->buf:[B

    aget-byte v4, v4, v2

    and-int/lit16 v4, v4, 0xff

    shl-int/lit8 v4, v4, 0x8

    or-int v0, v3, v4

    .line 308
    .local v0, "len":I
    if-lt v0, v2, :cond_2

    const/16 v3, 0x400

    if-le v0, v3, :cond_3

    .line 309
    :cond_2
    const-string v2, "InstallerConnection"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "invalid reply length ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 310
    invoke-virtual {p0}, Lcom/android/internal/os/InstallerConnection;->disconnect()V

    goto :goto_0

    .line 313
    :cond_3
    iget-object v3, p0, Lcom/android/internal/os/InstallerConnection;->buf:[B

    invoke-direct {p0, v3, v0}, Lcom/android/internal/os/InstallerConnection;->readBytes([BI)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 315
    iput v0, p0, Lcom/android/internal/os/InstallerConnection;->buflen:I

    move v1, v2

    .line 316
    goto :goto_0
.end method

.method private readReplyString()Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 342
    invoke-direct {p0}, Lcom/android/internal/os/InstallerConnection;->readReplyFromSocket()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 343
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/android/internal/os/InstallerConnection;->buf:[B

    const/4 v2, 0x0

    iget v3, p0, Lcom/android/internal/os/InstallerConnection;->buflen:I

    invoke-direct {v0, v1, v2, v3}, Ljava/lang/String;-><init>([BII)V

    .line 347
    .local v0, "s":Ljava/lang/String;
    return-object v0

    .line 352
    .end local v0    # "s":Ljava/lang/String;
    :cond_0
    new-instance v1, Ljava/io/IOException;

    const-string/jumbo v2, "read reply fail"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private writeCommand(Ljava/lang/String;)Z
    .locals 9
    .param p1, "cmdString"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 321
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    .line 322
    .local v0, "cmd":[B
    array-length v2, v0

    .line 323
    .local v2, "len":I
    if-lt v2, v3, :cond_0

    iget-object v5, p0, Lcom/android/internal/os/InstallerConnection;->buf:[B

    array-length v5, v5

    if-le v2, v5, :cond_1

    :cond_0
    move v3, v4

    .line 337
    :goto_0
    return v3

    .line 327
    :cond_1
    iget-object v5, p0, Lcom/android/internal/os/InstallerConnection;->buf:[B

    and-int/lit16 v6, v2, 0xff

    int-to-byte v6, v6

    aput-byte v6, v5, v4

    .line 328
    iget-object v5, p0, Lcom/android/internal/os/InstallerConnection;->buf:[B

    shr-int/lit8 v6, v2, 0x8

    and-int/lit16 v6, v6, 0xff

    int-to-byte v6, v6

    aput-byte v6, v5, v3

    .line 330
    :try_start_0
    iget-object v5, p0, Lcom/android/internal/os/InstallerConnection;->mOut:Ljava/io/OutputStream;

    iget-object v6, p0, Lcom/android/internal/os/InstallerConnection;->buf:[B

    const/4 v7, 0x0

    const/4 v8, 0x2

    invoke-virtual {v5, v6, v7, v8}, Ljava/io/OutputStream;->write([BII)V

    .line 331
    iget-object v5, p0, Lcom/android/internal/os/InstallerConnection;->mOut:Ljava/io/OutputStream;

    const/4 v6, 0x0

    invoke-virtual {v5, v0, v6, v2}, Ljava/io/OutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 332
    :catch_0
    move-exception v1

    .line 333
    .local v1, "ex":Ljava/io/IOException;
    const-string v3, "InstallerConnection"

    const-string/jumbo v5, "write error"

    invoke-static {v3, v5}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 334
    invoke-virtual {p0}, Lcom/android/internal/os/InstallerConnection;->disconnect()V

    move v3, v4

    .line 335
    goto :goto_0
.end method


# virtual methods
.method public asyncCallSatistics()Ljava/lang/String;
    .locals 2

    .prologue
    .line 621
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "dexoptTime statistics : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 622
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string v1, "ack : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 623
    iget v1, p0, Lcom/android/internal/os/InstallerConnection;->ackDurationSum:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 624
    const-string v1, ", dexopt : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 625
    iget v1, p0, Lcom/android/internal/os/InstallerConnection;->dexoptDurationSum:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 626
    const-string v1, ", total : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 627
    iget v1, p0, Lcom/android/internal/os/InstallerConnection;->totalDurationSum:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 628
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public asyncDexopt(Ljava/lang/String;IZLjava/lang/String;Ljava/lang/String;ZZ)I
    .locals 3
    .param p1, "apkPath"    # Ljava/lang/String;
    .param p2, "uid"    # I
    .param p3, "isPublic"    # Z
    .param p4, "pkgName"    # Ljava/lang/String;
    .param p5, "instructionSet"    # Ljava/lang/String;
    .param p6, "vmSafeMode"    # Z
    .param p7, "interpret_only"    # Z

    .prologue
    const/16 v2, 0x20

    .line 158
    new-instance v0, Lcom/android/internal/os/InstallerConnection$AsyncCmd;

    const/4 v1, 0x2

    invoke-direct {v0, p0, v1}, Lcom/android/internal/os/InstallerConnection$AsyncCmd;-><init>(Lcom/android/internal/os/InstallerConnection;I)V

    .line 159
    .local v0, "cmd":Lcom/android/internal/os/InstallerConnection$AsyncCmd;
    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/internal/os/InstallerConnection$AsyncCmd;->append(Ljava/lang/Object;)Lcom/android/internal/os/InstallerConnection$AsyncCmd;

    .line 160
    invoke-virtual {v0, p1}, Lcom/android/internal/os/InstallerConnection$AsyncCmd;->appendPackage(Ljava/lang/String;)Lcom/android/internal/os/InstallerConnection$AsyncCmd;

    .line 161
    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/internal/os/InstallerConnection$AsyncCmd;->append(Ljava/lang/Object;)Lcom/android/internal/os/InstallerConnection$AsyncCmd;

    .line 162
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/internal/os/InstallerConnection$AsyncCmd;->append(Ljava/lang/Object;)Lcom/android/internal/os/InstallerConnection$AsyncCmd;

    .line 163
    if-eqz p3, :cond_0

    const-string v1, " 1"

    :goto_0
    invoke-virtual {v0, v1}, Lcom/android/internal/os/InstallerConnection$AsyncCmd;->append(Ljava/lang/Object;)Lcom/android/internal/os/InstallerConnection$AsyncCmd;

    .line 164
    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/internal/os/InstallerConnection$AsyncCmd;->append(Ljava/lang/Object;)Lcom/android/internal/os/InstallerConnection$AsyncCmd;

    .line 165
    invoke-virtual {v0, p4}, Lcom/android/internal/os/InstallerConnection$AsyncCmd;->append(Ljava/lang/Object;)Lcom/android/internal/os/InstallerConnection$AsyncCmd;

    .line 166
    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/internal/os/InstallerConnection$AsyncCmd;->append(Ljava/lang/Object;)Lcom/android/internal/os/InstallerConnection$AsyncCmd;

    .line 167
    invoke-virtual {v0, p5}, Lcom/android/internal/os/InstallerConnection$AsyncCmd;->append(Ljava/lang/Object;)Lcom/android/internal/os/InstallerConnection$AsyncCmd;

    .line 168
    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/internal/os/InstallerConnection$AsyncCmd;->append(Ljava/lang/Object;)Lcom/android/internal/os/InstallerConnection$AsyncCmd;

    .line 169
    if-eqz p6, :cond_1

    const-string v1, " 1"

    :goto_1
    invoke-virtual {v0, v1}, Lcom/android/internal/os/InstallerConnection$AsyncCmd;->append(Ljava/lang/Object;)Lcom/android/internal/os/InstallerConnection$AsyncCmd;

    .line 170
    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/internal/os/InstallerConnection$AsyncCmd;->append(Ljava/lang/Object;)Lcom/android/internal/os/InstallerConnection$AsyncCmd;

    .line 171
    if-eqz p7, :cond_2

    const-string v1, " 1"

    :goto_2
    invoke-virtual {v0, v1}, Lcom/android/internal/os/InstallerConnection$AsyncCmd;->append(Ljava/lang/Object;)Lcom/android/internal/os/InstallerConnection$AsyncCmd;

    .line 172
    invoke-direct {p0, v0}, Lcom/android/internal/os/InstallerConnection;->asyncExecute(Lcom/android/internal/os/InstallerConnection$AsyncCmd;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 173
    const/4 v1, 0x0

    .line 176
    :goto_3
    return v1

    .line 163
    :cond_0
    const-string v1, " 0"

    goto :goto_0

    .line 169
    :cond_1
    const-string v1, " 0"

    goto :goto_1

    .line 171
    :cond_2
    const-string v1, " 0"

    goto :goto_2

    .line 175
    :cond_3
    const-string v1, "InstallerConnection"

    const-string v2, "asyncExecute fail"

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    const/4 v1, -0x1

    goto :goto_3
.end method

.method public dexopt(Ljava/lang/String;IZLjava/lang/String;)I
    .locals 8
    .param p1, "apkPath"    # Ljava/lang/String;
    .param p2, "uid"    # I
    .param p3, "isPublic"    # Z
    .param p4, "instructionSet"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 123
    const-string v4, "*"

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v5, p4

    move v7, v6

    invoke-virtual/range {v0 .. v7}, Lcom/android/internal/os/InstallerConnection;->dexopt(Ljava/lang/String;IZLjava/lang/String;Ljava/lang/String;ZZ)I

    move-result v0

    return v0
.end method

.method public dexopt(Ljava/lang/String;IZLjava/lang/String;Ljava/lang/String;Z)I
    .locals 8
    .param p1, "apkPath"    # Ljava/lang/String;
    .param p2, "uid"    # I
    .param p3, "isPublic"    # Z
    .param p4, "pkgName"    # Ljava/lang/String;
    .param p5, "instructionSet"    # Ljava/lang/String;
    .param p6, "vmSafeMode"    # Z

    .prologue
    .line 130
    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v7}, Lcom/android/internal/os/InstallerConnection;->dexopt(Ljava/lang/String;IZLjava/lang/String;Ljava/lang/String;ZZ)I

    move-result v0

    return v0
.end method

.method public dexopt(Ljava/lang/String;IZLjava/lang/String;Ljava/lang/String;ZZ)I
    .locals 3
    .param p1, "apkPath"    # Ljava/lang/String;
    .param p2, "uid"    # I
    .param p3, "isPublic"    # Z
    .param p4, "pkgName"    # Ljava/lang/String;
    .param p5, "instructionSet"    # Ljava/lang/String;
    .param p6, "vmSafeMode"    # Z
    .param p7, "interpret_only"    # Z

    .prologue
    const/16 v2, 0x20

    .line 137
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "dexopt"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 138
    .local v0, "builder":Ljava/lang/StringBuilder;
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 139
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 140
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 141
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 142
    if-eqz p3, :cond_0

    const-string v1, " 1"

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 143
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 144
    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 145
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 146
    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 147
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 148
    if-eqz p6, :cond_1

    const-string v1, " 1"

    :goto_1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 149
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 150
    if-eqz p7, :cond_2

    const-string v1, " 1"

    :goto_2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 151
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/internal/os/InstallerConnection;->execute(Ljava/lang/String;)I

    move-result v1

    return v1

    .line 142
    :cond_0
    const-string v1, " 0"

    goto :goto_0

    .line 148
    :cond_1
    const-string v1, " 0"

    goto :goto_1

    .line 150
    :cond_2
    const-string v1, " 0"

    goto :goto_2
.end method

.method public disconnect()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 223
    const-string v0, "InstallerConnection"

    const-string v1, "disconnecting..."

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 224
    iget-object v0, p0, Lcom/android/internal/os/InstallerConnection;->mSocket:Landroid/net/LocalSocket;

    invoke-static {v0}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 225
    iget-object v0, p0, Lcom/android/internal/os/InstallerConnection;->mIn:Ljava/io/InputStream;

    invoke-static {v0}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 226
    iget-object v0, p0, Lcom/android/internal/os/InstallerConnection;->mOut:Ljava/io/OutputStream;

    invoke-static {v0}, Llibcore/io/IoUtils;->closeQuietly(Ljava/lang/AutoCloseable;)V

    .line 228
    iput-object v2, p0, Lcom/android/internal/os/InstallerConnection;->mSocket:Landroid/net/LocalSocket;

    .line 229
    iput-object v2, p0, Lcom/android/internal/os/InstallerConnection;->mIn:Ljava/io/InputStream;

    .line 230
    iput-object v2, p0, Lcom/android/internal/os/InstallerConnection;->mOut:Ljava/io/OutputStream;

    .line 231
    return-void
.end method

.method public execute(Ljava/lang/String;)I
    .locals 3
    .param p1, "cmd"    # Ljava/lang/String;

    .prologue
    .line 113
    invoke-virtual {p0, p1}, Lcom/android/internal/os/InstallerConnection;->transact(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 115
    .local v1, "res":Ljava/lang/String;
    :try_start_0
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 117
    :goto_0
    return v2

    .line 116
    :catch_0
    move-exception v0

    .line 117
    .local v0, "ex":Ljava/lang/NumberFormatException;
    const/4 v2, -0x1

    goto :goto_0
.end method

.method public getRemainAsyncCmdsSize()I
    .locals 1

    .prologue
    .line 299
    iget-object v0, p0, Lcom/android/internal/os/InstallerConnection;->asyncCmds:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public patchoat(Ljava/lang/String;IZLjava/lang/String;)I
    .locals 6
    .param p1, "apkPath"    # Ljava/lang/String;
    .param p2, "uid"    # I
    .param p3, "isPublic"    # Z
    .param p4, "instructionSet"    # Ljava/lang/String;

    .prologue
    .line 182
    const-string v4, "*"

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/android/internal/os/InstallerConnection;->patchoat(Ljava/lang/String;IZLjava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public patchoat(Ljava/lang/String;IZLjava/lang/String;Ljava/lang/String;)I
    .locals 3
    .param p1, "apkPath"    # Ljava/lang/String;
    .param p2, "uid"    # I
    .param p3, "isPublic"    # Z
    .param p4, "pkgName"    # Ljava/lang/String;
    .param p5, "instructionSet"    # Ljava/lang/String;

    .prologue
    const/16 v2, 0x20

    .line 187
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "patchoat"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 188
    .local v0, "builder":Ljava/lang/StringBuilder;
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 189
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 190
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 191
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 192
    if-eqz p3, :cond_0

    const-string v1, " 1"

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 193
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 194
    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 195
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 196
    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 197
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/internal/os/InstallerConnection;->execute(Ljava/lang/String;)I

    move-result v1

    return v1

    .line 192
    :cond_0
    const-string v1, " 0"

    goto :goto_0
.end method

.method public processAsync()Lcom/android/internal/os/InstallerConnection$AsyncReply;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 401
    invoke-direct {p0}, Lcom/android/internal/os/InstallerConnection;->readReplyString()Ljava/lang/String;

    move-result-object v3

    .line 402
    .local v3, "replyStr":Ljava/lang/String;
    const-string v5, "InstallerConnection"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "replyStr : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 403
    new-instance v2, Lcom/android/internal/os/InstallerConnection$AsyncReply;

    invoke-direct {v2, p0, v3}, Lcom/android/internal/os/InstallerConnection$AsyncReply;-><init>(Lcom/android/internal/os/InstallerConnection;Ljava/lang/String;)V

    .line 406
    .local v2, "reply":Lcom/android/internal/os/InstallerConnection$AsyncReply;
    iget-object v5, p0, Lcom/android/internal/os/InstallerConnection;->asyncCmds:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 407
    .local v1, "i":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/android/internal/os/InstallerConnection$AsyncCmd;>;"
    const/4 v4, 0x0

    .line 408
    .local v4, "ret":Z
    const-string v5, "InstallerConnection"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "reply to string : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Lcom/android/internal/os/InstallerConnection$AsyncReply;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 409
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 410
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/internal/os/InstallerConnection$AsyncCmd;

    .line 411
    .local v0, "cmd":Lcom/android/internal/os/InstallerConnection$AsyncCmd;
    invoke-virtual {v0, v2}, Lcom/android/internal/os/InstallerConnection$AsyncCmd;->checkMatched(Lcom/android/internal/os/InstallerConnection$AsyncReply;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 412
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    .line 413
    invoke-virtual {v0}, Lcom/android/internal/os/InstallerConnection$AsyncCmd;->recordReplyTime()V

    .line 414
    invoke-virtual {v0}, Lcom/android/internal/os/InstallerConnection$AsyncCmd;->recordStatistics()V

    .line 415
    invoke-virtual {v2}, Lcom/android/internal/os/InstallerConnection$AsyncReply;->checkSuccess()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 416
    const-string v5, "InstallerConnection"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "asyncDexopt success : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Lcom/android/internal/os/InstallerConnection$AsyncReply;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 427
    .end local v0    # "cmd":Lcom/android/internal/os/InstallerConnection$AsyncCmd;
    :cond_1
    :goto_0
    return-object v2

    .line 417
    .restart local v0    # "cmd":Lcom/android/internal/os/InstallerConnection$AsyncCmd;
    :cond_2
    invoke-virtual {v0}, Lcom/android/internal/os/InstallerConnection$AsyncCmd;->getFailCount()I

    move-result v5

    const/4 v6, 0x5

    if-ge v5, v6, :cond_3

    .line 418
    const-string v5, "InstallerConnection"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "asyncDexopt retry : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Lcom/android/internal/os/InstallerConnection$AsyncReply;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 419
    invoke-virtual {v0}, Lcom/android/internal/os/InstallerConnection$AsyncCmd;->fail()V

    .line 420
    invoke-direct {p0, v0}, Lcom/android/internal/os/InstallerConnection;->asyncExecute(Lcom/android/internal/os/InstallerConnection$AsyncCmd;)Z

    goto :goto_0

    .line 422
    :cond_3
    const-string v5, "InstallerConnection"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "asyncDexopt fail : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Lcom/android/internal/os/InstallerConnection$AsyncReply;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public declared-synchronized transact(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "cmd"    # Ljava/lang/String;

    .prologue
    .line 77
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/android/internal/os/InstallerConnection;->connect()Z

    move-result v2

    if-nez v2, :cond_0

    .line 78
    const-string v2, "InstallerConnection"

    const-string v3, "connection failed"

    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    const-string v1, "-1"
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 108
    :goto_0
    monitor-exit p0

    return-object v1

    .line 82
    :cond_0
    :try_start_1
    invoke-direct {p0, p1}, Lcom/android/internal/os/InstallerConnection;->writeCommand(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 88
    const-string v2, "InstallerConnection"

    const-string/jumbo v3, "write command failed? reconnect!"

    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    invoke-direct {p0}, Lcom/android/internal/os/InstallerConnection;->connect()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-direct {p0, p1}, Lcom/android/internal/os/InstallerConnection;->writeCommand(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 90
    :cond_1
    const-string v1, "-1"

    goto :goto_0

    .line 97
    :cond_2
    invoke-direct {p0}, Lcom/android/internal/os/InstallerConnection;->readReply()I

    move-result v0

    .line 98
    .local v0, "replyLength":I
    if-lez v0, :cond_3

    .line 99
    new-instance v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/android/internal/os/InstallerConnection;->buf:[B

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3, v0}, Ljava/lang/String;-><init>([BII)V

    .line 103
    .local v1, "s":Ljava/lang/String;
    goto :goto_0

    .line 108
    .end local v1    # "s":Ljava/lang/String;
    :cond_3
    const-string v1, "-1"
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 77
    .end local v0    # "replyLength":I
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method
