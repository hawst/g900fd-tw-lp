.class Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsService$NativeRcsImsSettings;
.super Ljava/lang/Object;
.source "QrcsImsSettingsService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "NativeRcsImsSettings"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsService;


# direct methods
.method constructor <init>(Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsService;)V
    .locals 0

    .prologue
    .line 81
    iput-object p1, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsService$NativeRcsImsSettings;->this$0:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public native QrcsImsSettings_Deregister(Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;)Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
.end method

.method public native QrcsImsSettings_GetPresenceConfig(Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;)Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
.end method

.method public native QrcsImsSettings_GetQipcallConfig(Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;)Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
.end method

.method public native QrcsImsSettings_Register(Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsListener;Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;)Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
.end method

.method public native QrcsImsSettings_SetPresenceConfig(Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;)Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
.end method

.method public native QrcsImsSettings_SetQipcallConfig(Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;)Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
.end method
