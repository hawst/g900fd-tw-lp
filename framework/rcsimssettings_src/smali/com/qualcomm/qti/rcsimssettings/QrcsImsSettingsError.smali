.class public Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
.super Ljava/lang/Object;
.source "QrcsImsSettingsError.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private eQrcsImsSettingsError:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 179
    new-instance v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 192
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 193
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;->readFromParcel(Landroid/os/Parcel;)V

    .line 194
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$1;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public static getQrcsImsSettingsErrorInstance()Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
    .locals 1

    .prologue
    .line 68
    new-instance v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;

    invoke-direct {v0}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;-><init>()V

    return-object v0
.end method

.method private writeToParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;

    .prologue
    .line 176
    iget-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;->eQrcsImsSettingsError:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 177
    return-void

    .line 176
    :cond_0
    iget-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;->eQrcsImsSettingsError:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    invoke-virtual {v0}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 166
    const/4 v0, 0x0

    return v0
.end method

.method public getQrcsImsSettingsError()I
    .locals 2

    .prologue
    .line 83
    iget-object v1, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;->eQrcsImsSettingsError:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    invoke-virtual {v1}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;->ordinal()I

    move-result v0

    .line 84
    .local v0, "error":I
    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 199
    :try_start_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;->valueOf(Ljava/lang/String;)Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    move-result-object v1

    iput-object v1, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;->eQrcsImsSettingsError:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 204
    :goto_0
    return-void

    .line 200
    :catch_0
    move-exception v0

    .line 201
    .local v0, "x":Ljava/lang/IllegalArgumentException;
    const-string v1, "AIDL"

    const-string v2, "eQrcsImsSettingsError IllegalArgumentException"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 202
    sget-object v1, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;->QRCS_IMS_ERR_UNKNOWN:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    iput-object v1, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;->eQrcsImsSettingsError:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    goto :goto_0
.end method

.method public setQrcsImsSettingsError(I)V
    .locals 3
    .param p1, "nQrcsImsSettingsError"    # I

    .prologue
    .line 100
    const-string v0, "AIDL"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "default nQrcsImsSettingsError = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    sparse-switch p1, :sswitch_data_0

    .line 153
    const-string v0, "AIDL"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "default nQrcsImsSettingsError = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 154
    sget-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;->QRCS_IMS_ERR_UNKNOWN:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;->eQrcsImsSettingsError:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    .line 157
    :goto_0
    return-void

    .line 105
    :sswitch_0
    sget-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;->QRCS_IMS_NO_ERR:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;->eQrcsImsSettingsError:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    goto :goto_0

    .line 109
    :sswitch_1
    sget-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;->QRCS_IMS_INTERNAL_ERR:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;->eQrcsImsSettingsError:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    goto :goto_0

    .line 113
    :sswitch_2
    sget-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;->QRCS_IMS_SERVICE_ERR:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;->eQrcsImsSettingsError:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    goto :goto_0

    .line 117
    :sswitch_3
    sget-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;->QRCS_IMS_TIMEOUT_ERR:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;->eQrcsImsSettingsError:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    goto :goto_0

    .line 121
    :sswitch_4
    sget-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;->QRCS_IMS_EXTENDED_ERR:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;->eQrcsImsSettingsError:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    goto :goto_0

    .line 125
    :sswitch_5
    sget-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;->QRCS_IMS_PORT_NOT_OPEN_ERR:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;->eQrcsImsSettingsError:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    goto :goto_0

    .line 129
    :sswitch_6
    sget-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;->QRCS_IMS_MEMORY_ERR:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;->eQrcsImsSettingsError:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    goto :goto_0

    .line 133
    :sswitch_7
    sget-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;->QRCS_IMS_INVALID_TXN:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;->eQrcsImsSettingsError:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    goto :goto_0

    .line 137
    :sswitch_8
    sget-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;->QRCS_IMS_CLIENT_ALLOC_FAILURE:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;->eQrcsImsSettingsError:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    goto :goto_0

    .line 141
    :sswitch_9
    sget-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;->QRCS_IMS_INVALID_PARAM:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;->eQrcsImsSettingsError:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    goto :goto_0

    .line 145
    :sswitch_a
    sget-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;->QRCS_IMS_GENERAL_ERR:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;->eQrcsImsSettingsError:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    goto :goto_0

    .line 149
    :sswitch_b
    sget-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;->QRCS_IMS_ERR_MAX:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;->eQrcsImsSettingsError:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    goto :goto_0

    .line 102
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0x10000000 -> :sswitch_b
    .end sparse-switch
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 170
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;->writeToParcel(Landroid/os/Parcel;)V

    .line 172
    return-void
.end method
