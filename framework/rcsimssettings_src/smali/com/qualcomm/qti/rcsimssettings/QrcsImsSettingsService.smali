.class public Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsService;
.super Landroid/app/Service;
.source "QrcsImsSettingsService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsService$NativeRcsImsSettings;
    }
.end annotation


# static fields
.field public static TAG:Ljava/lang/String;

.field static clientIDMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static isLibsLoaded:Z


# instance fields
.field private final RcsImsSettingsServiceImpl:Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsService$Stub;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 28
    const-string v1, "QrcsImsSettingsService"

    sput-object v1, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsService;->TAG:Ljava/lang/String;

    .line 29
    sput-boolean v3, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsService;->isLibsLoaded:Z

    .line 31
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    sput-object v1, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsService;->clientIDMap:Ljava/util/HashMap;

    .line 37
    :try_start_0
    sget-object v1, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsService;->TAG:Ljava/lang/String;

    const-string v2, "Loading Presence libraries "

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 38
    const-string v1, "idl"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 39
    const-string v1, "cutils"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 40
    const-string v1, "qmiservices"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 41
    const-string v1, "qmi_client_qmux"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 42
    const-string v1, "qmi_cci"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 43
    const-string v1, "-imss"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 44
    const-string v1, "-rcsimssjni"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 45
    const/4 v1, 0x1

    sput-boolean v1, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsService;->isLibsLoaded:Z
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 51
    .local v0, "e":Ljava/lang/UnsatisfiedLinkError;
    :goto_0
    return-void

    .line 46
    .end local v0    # "e":Ljava/lang/UnsatisfiedLinkError;
    :catch_0
    move-exception v0

    .line 48
    .restart local v0    # "e":Ljava/lang/UnsatisfiedLinkError;
    sput-boolean v3, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsService;->isLibsLoaded:Z

    .line 49
    sget-object v1, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsService;->TAG:Ljava/lang/String;

    const-string v2, "ERROR LOAD LIBRAY EXCEPTION MISSING PRESENCE LIBRARY"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 106
    new-instance v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsService$1;

    invoke-direct {v0, p0}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsService$1;-><init>(Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsService;)V

    iput-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsService;->RcsImsSettingsServiceImpl:Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsService$Stub;

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 55
    iget-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsService;->RcsImsSettingsServiceImpl:Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsService$Stub;

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 60
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 61
    const-string v0, "AIDL"

    const-string v1, " QrcsImsSettingsService OnCreate()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 66
    const-string v0, "AIDL"

    const-string v1, "QrcsImsSettingsService onDestroy()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 68
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 72
    const-string v0, "AIDL"

    const-string v1, "QrcsImsSettingsService onStartCommand()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    move-result v0

    return v0
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 78
    invoke-super {p0, p1}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method
