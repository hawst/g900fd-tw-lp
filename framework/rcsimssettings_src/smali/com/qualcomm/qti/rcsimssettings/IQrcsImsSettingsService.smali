.class public interface abstract Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsService;
.super Ljava/lang/Object;
.source "IQrcsImsSettingsService.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsService$Stub;
    }
.end annotation


# virtual methods
.method public abstract QrcsImsSettings_Deregister(Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;)Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QrcsImsSettings_GetPresenceConfig(Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;)Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QrcsImsSettings_GetQipcallConfig(Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;)Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QrcsImsSettings_Register(Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsListener;Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;)Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QrcsImsSettings_SetPresenceConfig(Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;)Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QrcsImsSettings_SetQipcallConfig(Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;)Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getLibLoadStatus()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
