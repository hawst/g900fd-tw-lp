.class public Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;
.super Ljava/lang/Object;
.source "QrcsImsSettingsPresenceConfig.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private bAvailability_cache_expiration_valid:Z

.field private bCapabilites_cache_expiration_valid:Z

.field private bCapability_discovery_enable:Z

.field private bCapability_discovery_enable_valid:Z

.field private bCapability_poll_interval_valid:Z

.field private bCapability_poll_list_subscription_expiry_timer_valid:Z

.field private bMax_subcription_list_entries_valid:Z

.field private bMinimum_publish_interval_valid:Z

.field private bPublish_error_recovery_timer_valid:Z

.field private bPublish_expiry_timer_valid:Z

.field private bPublish_extended_expiry_timer_valid:Z

.field private bVolte_user_opted_in_status:Z

.field private bVolte_user_opted_in_status_valid:Z

.field private nAvailability_cache_expiration:I

.field private nCapabilites_cache_expiration:I

.field private nCapability_poll_interval:I

.field private nCapability_poll_list_subscription_expiry_timer:I

.field private nMax_subcription_list_entries:I

.field private nMinimum_publish_interval:I

.field private nPublish_error_recovery_timer:I

.field private nPublish_expiry_timer:I

.field private nPublish_extended_expiry_timer:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 871
    new-instance v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 129
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 130
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 884
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 885
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->readFromParcel(Landroid/os/Parcel;)V

    .line 886
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig$1;

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public static getQrcsImsSettingsPresenceConfigInstance()Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;
    .locals 1

    .prologue
    .line 828
    new-instance v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;

    invoke-direct {v0}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;-><init>()V

    return-object v0
.end method

.method private writeToParcel(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "out"    # Landroid/os/Parcel;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 843
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->bPublish_expiry_timer_valid:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 844
    iget v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->nPublish_expiry_timer:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 845
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->bPublish_extended_expiry_timer_valid:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 846
    iget v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->nPublish_extended_expiry_timer:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 847
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->bMinimum_publish_interval_valid:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 848
    iget v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->nMinimum_publish_interval:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 849
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->bCapability_poll_list_subscription_expiry_timer_valid:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 850
    iget v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->nCapability_poll_list_subscription_expiry_timer:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 851
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->bCapability_discovery_enable_valid:Z

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 852
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->bCapability_discovery_enable:Z

    if-eqz v0, :cond_5

    move v0, v1

    :goto_5
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 853
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->bCapabilites_cache_expiration_valid:Z

    if-eqz v0, :cond_6

    move v0, v1

    :goto_6
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 854
    iget v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->nCapabilites_cache_expiration:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 855
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->bAvailability_cache_expiration_valid:Z

    if-eqz v0, :cond_7

    move v0, v1

    :goto_7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 856
    iget v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->nAvailability_cache_expiration:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 857
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->bCapability_poll_interval_valid:Z

    if-eqz v0, :cond_8

    move v0, v1

    :goto_8
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 858
    iget v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->nCapability_poll_interval:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 859
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->bMax_subcription_list_entries_valid:Z

    if-eqz v0, :cond_9

    move v0, v1

    :goto_9
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 860
    iget v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->nMax_subcription_list_entries:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 861
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->bVolte_user_opted_in_status_valid:Z

    if-eqz v0, :cond_a

    move v0, v1

    :goto_a
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 862
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->bVolte_user_opted_in_status:Z

    if-eqz v0, :cond_b

    move v0, v1

    :goto_b
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 866
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->bPublish_error_recovery_timer_valid:Z

    if-eqz v0, :cond_c

    :goto_c
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 867
    iget v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->nPublish_error_recovery_timer:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 869
    return-void

    :cond_0
    move v0, v2

    .line 843
    goto/16 :goto_0

    :cond_1
    move v0, v2

    .line 845
    goto/16 :goto_1

    :cond_2
    move v0, v2

    .line 847
    goto :goto_2

    :cond_3
    move v0, v2

    .line 849
    goto :goto_3

    :cond_4
    move v0, v2

    .line 851
    goto :goto_4

    :cond_5
    move v0, v2

    .line 852
    goto :goto_5

    :cond_6
    move v0, v2

    .line 853
    goto :goto_6

    :cond_7
    move v0, v2

    .line 855
    goto :goto_7

    :cond_8
    move v0, v2

    .line 857
    goto :goto_8

    :cond_9
    move v0, v2

    .line 859
    goto :goto_9

    :cond_a
    move v0, v2

    .line 861
    goto :goto_a

    :cond_b
    move v0, v2

    .line 862
    goto :goto_b

    :cond_c
    move v1, v2

    .line 866
    goto :goto_c
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 833
    const/4 v0, 0x0

    return v0
.end method

.method public getAvailability_cache_expiration()I
    .locals 1

    .prologue
    .line 550
    iget v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->nAvailability_cache_expiration:I

    return v0
.end method

.method public getCapabilites_cache_expiration()I
    .locals 1

    .prologue
    .line 488
    iget v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->nCapabilites_cache_expiration:I

    return v0
.end method

.method public getCapability_poll_interval()I
    .locals 1

    .prologue
    .line 611
    iget v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->nCapability_poll_interval:I

    return v0
.end method

.method public getCapability_poll_list_subscription_expiry_timer()I
    .locals 1

    .prologue
    .line 361
    iget v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->nCapability_poll_list_subscription_expiry_timer:I

    return v0
.end method

.method public getMax_subcription_list_entries()I
    .locals 1

    .prologue
    .line 673
    iget v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->nMax_subcription_list_entries:I

    return v0
.end method

.method public getMinimum_publish_interval()I
    .locals 1

    .prologue
    .line 299
    iget v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->nMinimum_publish_interval:I

    return v0
.end method

.method public getPublish_error_recovery_timer()I
    .locals 1

    .prologue
    .line 798
    iget v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->nPublish_error_recovery_timer:I

    return v0
.end method

.method public getPublish_expiry_timer()I
    .locals 1

    .prologue
    .line 173
    iget v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->nPublish_expiry_timer:I

    return v0
.end method

.method public getPublish_extended_expiry_timer()I
    .locals 1

    .prologue
    .line 236
    iget v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->nPublish_extended_expiry_timer:I

    return v0
.end method

.method public isAvailability_cache_expiration_valid()Z
    .locals 1

    .prologue
    .line 519
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->bAvailability_cache_expiration_valid:Z

    return v0
.end method

.method public isCapabilites_cache_expiration_valid()Z
    .locals 1

    .prologue
    .line 457
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->bCapabilites_cache_expiration_valid:Z

    return v0
.end method

.method public isCapability_discovery_enable()Z
    .locals 1

    .prologue
    .line 426
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->bCapability_discovery_enable:Z

    return v0
.end method

.method public isCapability_discovery_enable_valid()Z
    .locals 1

    .prologue
    .line 395
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->bCapability_discovery_enable_valid:Z

    return v0
.end method

.method public isCapability_poll_interval_valid()Z
    .locals 1

    .prologue
    .line 581
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->bCapability_poll_interval_valid:Z

    return v0
.end method

.method public isCapability_poll_list_subscription_expiry_timer_valid()Z
    .locals 1

    .prologue
    .line 330
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->bCapability_poll_list_subscription_expiry_timer_valid:Z

    return v0
.end method

.method public isMax_subcription_list_entries_valid()Z
    .locals 1

    .prologue
    .line 642
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->bMax_subcription_list_entries_valid:Z

    return v0
.end method

.method public isMinimum_publish_interval_valid()Z
    .locals 1

    .prologue
    .line 268
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->bMinimum_publish_interval_valid:Z

    return v0
.end method

.method public isPublish_error_recovery_timer_valid()Z
    .locals 1

    .prologue
    .line 768
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->bPublish_error_recovery_timer_valid:Z

    return v0
.end method

.method public isPublish_expiry_timer_valid()Z
    .locals 1

    .prologue
    .line 143
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->bPublish_expiry_timer_valid:Z

    return v0
.end method

.method public isPublish_extended_expiry_timer_valid()Z
    .locals 1

    .prologue
    .line 205
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->bPublish_extended_expiry_timer_valid:Z

    return v0
.end method

.method public isVolte_user_opted_in_status()Z
    .locals 1

    .prologue
    .line 735
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->bVolte_user_opted_in_status:Z

    return v0
.end method

.method public isVolte_user_opted_in_status_valid()Z
    .locals 1

    .prologue
    .line 704
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->bVolte_user_opted_in_status_valid:Z

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 889
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->bPublish_expiry_timer_valid:Z

    .line 890
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->nPublish_expiry_timer:I

    .line 891
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->bPublish_extended_expiry_timer_valid:Z

    .line 892
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->nPublish_extended_expiry_timer:I

    .line 893
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->bMinimum_publish_interval_valid:Z

    .line 894
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->nMinimum_publish_interval:I

    .line 895
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->bCapability_poll_list_subscription_expiry_timer_valid:Z

    .line 897
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->nCapability_poll_list_subscription_expiry_timer:I

    .line 898
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    iput-boolean v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->bCapability_discovery_enable_valid:Z

    .line 899
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    iput-boolean v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->bCapability_discovery_enable:Z

    .line 900
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_6

    move v0, v1

    :goto_6
    iput-boolean v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->bCapabilites_cache_expiration_valid:Z

    .line 901
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->nCapabilites_cache_expiration:I

    .line 902
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_7

    move v0, v1

    :goto_7
    iput-boolean v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->bAvailability_cache_expiration_valid:Z

    .line 903
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->nAvailability_cache_expiration:I

    .line 904
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_8

    move v0, v1

    :goto_8
    iput-boolean v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->bCapability_poll_interval_valid:Z

    .line 905
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->nCapability_poll_interval:I

    .line 906
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_9

    move v0, v1

    :goto_9
    iput-boolean v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->bMax_subcription_list_entries_valid:Z

    .line 907
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->nMax_subcription_list_entries:I

    .line 908
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_a

    move v0, v1

    :goto_a
    iput-boolean v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->bVolte_user_opted_in_status_valid:Z

    .line 909
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_b

    move v0, v1

    :goto_b
    iput-boolean v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->bVolte_user_opted_in_status:Z

    .line 912
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_c

    :goto_c
    iput-boolean v1, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->bPublish_error_recovery_timer_valid:Z

    .line 913
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->nPublish_error_recovery_timer:I

    .line 914
    return-void

    :cond_0
    move v0, v2

    .line 889
    goto/16 :goto_0

    :cond_1
    move v0, v2

    .line 891
    goto/16 :goto_1

    :cond_2
    move v0, v2

    .line 893
    goto/16 :goto_2

    :cond_3
    move v0, v2

    .line 895
    goto/16 :goto_3

    :cond_4
    move v0, v2

    .line 898
    goto :goto_4

    :cond_5
    move v0, v2

    .line 899
    goto :goto_5

    :cond_6
    move v0, v2

    .line 900
    goto :goto_6

    :cond_7
    move v0, v2

    .line 902
    goto :goto_7

    :cond_8
    move v0, v2

    .line 904
    goto :goto_8

    :cond_9
    move v0, v2

    .line 906
    goto :goto_9

    :cond_a
    move v0, v2

    .line 908
    goto :goto_a

    :cond_b
    move v0, v2

    .line 909
    goto :goto_b

    :cond_c
    move v1, v2

    .line 912
    goto :goto_c
.end method

.method public setAvailability_cache_expiration(I)V
    .locals 1
    .param p1, "availability_cache_expiration"    # I

    .prologue
    .line 565
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->setAvailability_cache_expiration_valid(Z)V

    .line 566
    iput p1, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->nAvailability_cache_expiration:I

    .line 567
    return-void
.end method

.method public setAvailability_cache_expiration_valid(Z)V
    .locals 0
    .param p1, "availability_cache_expiration_valid"    # Z

    .prologue
    .line 535
    iput-boolean p1, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->bAvailability_cache_expiration_valid:Z

    .line 536
    return-void
.end method

.method public setCapabilites_cache_expiration(I)V
    .locals 1
    .param p1, "capabilites_cache_expiration"    # I

    .prologue
    .line 503
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->setCapabilites_cache_expiration_valid(Z)V

    .line 504
    iput p1, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->nCapabilites_cache_expiration:I

    .line 505
    return-void
.end method

.method public setCapabilites_cache_expiration_valid(Z)V
    .locals 0
    .param p1, "capabilites_cache_expiration_valid"    # Z

    .prologue
    .line 473
    iput-boolean p1, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->bCapabilites_cache_expiration_valid:Z

    .line 474
    return-void
.end method

.method public setCapability_discovery_enable(Z)V
    .locals 1
    .param p1, "capability_discovery_enable"    # Z

    .prologue
    .line 441
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->setCapability_discovery_enable_valid(Z)V

    .line 442
    iput-boolean p1, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->bCapability_discovery_enable:Z

    .line 443
    return-void
.end method

.method public setCapability_discovery_enable_valid(Z)V
    .locals 0
    .param p1, "capability_discovery_enable_valid"    # Z

    .prologue
    .line 411
    iput-boolean p1, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->bCapability_discovery_enable_valid:Z

    .line 412
    return-void
.end method

.method public setCapability_poll_interval(I)V
    .locals 1
    .param p1, "capability_poll_interval"    # I

    .prologue
    .line 626
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->setCapability_poll_interval_valid(Z)V

    .line 627
    iput p1, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->nCapability_poll_interval:I

    .line 628
    return-void
.end method

.method public setCapability_poll_interval_valid(Z)V
    .locals 0
    .param p1, "capability_poll_interval_valid"    # Z

    .prologue
    .line 596
    iput-boolean p1, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->bCapability_poll_interval_valid:Z

    .line 597
    return-void
.end method

.method public setCapability_poll_list_subscription_expiry_timer(I)V
    .locals 1
    .param p1, "capability_poll_list_subscription_expiry_timer"    # I

    .prologue
    .line 378
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->setCapability_poll_list_subscription_expiry_timer_valid(Z)V

    .line 379
    iput p1, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->nCapability_poll_list_subscription_expiry_timer:I

    .line 380
    return-void
.end method

.method public setCapability_poll_list_subscription_expiry_timer_valid(Z)V
    .locals 0
    .param p1, "capability_poll_list_subscription_expiry_timer_valid"    # Z

    .prologue
    .line 346
    iput-boolean p1, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->bCapability_poll_list_subscription_expiry_timer_valid:Z

    .line 347
    return-void
.end method

.method public setMax_subcription_list_entries(I)V
    .locals 1
    .param p1, "max_subcription_list_entries"    # I

    .prologue
    .line 688
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->setMax_subcription_list_entries_valid(Z)V

    .line 689
    iput p1, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->nMax_subcription_list_entries:I

    .line 690
    return-void
.end method

.method public setMax_subcription_list_entries_valid(Z)V
    .locals 0
    .param p1, "max_subcription_list_entries_valid"    # Z

    .prologue
    .line 658
    iput-boolean p1, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->bMax_subcription_list_entries_valid:Z

    .line 659
    return-void
.end method

.method public setMinimum_publish_interval(I)V
    .locals 1
    .param p1, "minimum_publish_interval"    # I

    .prologue
    .line 314
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->setMinimum_publish_interval_valid(Z)V

    .line 315
    iput p1, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->nMinimum_publish_interval:I

    .line 316
    return-void
.end method

.method public setMinimum_publish_interval_valid(Z)V
    .locals 0
    .param p1, "minimum_publish_interval_valid"    # Z

    .prologue
    .line 284
    iput-boolean p1, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->bMinimum_publish_interval_valid:Z

    .line 285
    return-void
.end method

.method public setPublish_error_recovery_timer(I)V
    .locals 1
    .param p1, "publish_error_recovery_timer"    # I

    .prologue
    .line 813
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->setPublish_error_recovery_timer_valid(Z)V

    .line 814
    iput p1, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->nPublish_error_recovery_timer:I

    .line 815
    return-void
.end method

.method public setPublish_error_recovery_timer_valid(Z)V
    .locals 0
    .param p1, "publish_error_recovery_timer_valid"    # Z

    .prologue
    .line 783
    iput-boolean p1, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->bPublish_error_recovery_timer_valid:Z

    .line 784
    return-void
.end method

.method public setPublish_expiry_timer(I)V
    .locals 1
    .param p1, "publish_expiry_timer"    # I

    .prologue
    .line 188
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->setPublish_expiry_timer_valid(Z)V

    .line 189
    iput p1, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->nPublish_expiry_timer:I

    .line 190
    return-void
.end method

.method public setPublish_expiry_timer_valid(Z)V
    .locals 0
    .param p1, "publish_expiry_timer_valid"    # Z

    .prologue
    .line 158
    iput-boolean p1, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->bPublish_expiry_timer_valid:Z

    .line 159
    return-void
.end method

.method public setPublish_extended_expiry_timer(I)V
    .locals 1
    .param p1, "publish_extended_expiry_timer"    # I

    .prologue
    .line 251
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->setPublish_extended_expiry_timer_valid(Z)V

    .line 252
    iput p1, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->nPublish_extended_expiry_timer:I

    .line 253
    return-void
.end method

.method public setPublish_extended_expiry_timer_valid(Z)V
    .locals 0
    .param p1, "publish_extended_expiry_timer_valid"    # Z

    .prologue
    .line 221
    iput-boolean p1, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->bPublish_extended_expiry_timer_valid:Z

    .line 222
    return-void
.end method

.method public setVolte_user_opted_in_status(Z)V
    .locals 1
    .param p1, "volte_user_opted_in_status"    # Z

    .prologue
    .line 750
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->setVolte_user_opted_in_status_valid(Z)V

    .line 751
    iput-boolean p1, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->bVolte_user_opted_in_status:Z

    .line 752
    return-void
.end method

.method public setVolte_user_opted_in_status_valid(Z)V
    .locals 0
    .param p1, "volte_user_opted_in_status_valid"    # Z

    .prologue
    .line 720
    iput-boolean p1, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->bVolte_user_opted_in_status_valid:Z

    .line 721
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 837
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->writeToParcel(Landroid/os/Parcel;)V

    .line 839
    return-void
.end method
