.class public interface abstract Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsListener;
.super Ljava/lang/Object;
.source "IQrcsImsSettingsListener.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsListener$Stub;
    }
.end annotation


# virtual methods
.method public abstract QrcsImsSettings_ErrorCb(Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QrcsImsSettings_IndicationCb(Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIndId;Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsConfigInd;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QrcsImsSettings_PresenceConfigResponseCb(Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId;Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfigResp;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract QrcsImsSettings_QipcallConfigResponseCb(Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId;Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfigResp;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
