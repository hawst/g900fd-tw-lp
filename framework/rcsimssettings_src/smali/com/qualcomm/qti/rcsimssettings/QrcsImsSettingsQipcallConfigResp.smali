.class public Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfigResp;
.super Ljava/lang/Object;
.source "QrcsImsSettingsQipcallConfigResp.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfigResp;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public mQrcsImsSettingsQipcallConfig:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;

.field public mQrcsImsSettingsResp:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 132
    new-instance v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfigResp$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfigResp$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfigResp;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 102
    new-instance v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;

    invoke-direct {v0}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;-><init>()V

    iput-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfigResp;->mQrcsImsSettingsQipcallConfig:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;

    .line 103
    new-instance v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp;

    invoke-direct {v0}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp;-><init>()V

    iput-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfigResp;->mQrcsImsSettingsResp:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp;

    .line 104
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 145
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 146
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfigResp;->readFromParcel(Landroid/os/Parcel;)V

    .line 147
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfigResp$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfigResp$1;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfigResp;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public static getQrcsImsSettingsQipcallConfigRespInstance()Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfigResp;
    .locals 1

    .prologue
    .line 113
    new-instance v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfigResp;

    invoke-direct {v0}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfigResp;-><init>()V

    return-object v0
.end method

.method private writeToParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;

    .prologue
    .line 128
    iget-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfigResp;->mQrcsImsSettingsResp:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 129
    iget-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfigResp;->mQrcsImsSettingsQipcallConfig:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 130
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 118
    const/4 v0, 0x0

    return v0
.end method

.method public getQrcsImsSettingsQipcallConfig()Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfigResp;->mQrcsImsSettingsQipcallConfig:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;

    return-object v0
.end method

.method public getQrcsImsSettingsResp()Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfigResp;->mQrcsImsSettingsResp:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 150
    const-class v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfigResp;->mQrcsImsSettingsResp:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp;

    .line 152
    const-class v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfigResp;->mQrcsImsSettingsQipcallConfig:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;

    .line 154
    return-void
.end method

.method public setQrcsImsSettingsQipcallConfig(Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;)V
    .locals 0
    .param p1, "mQrcsImsSettingsQipcallConfig"    # Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;

    .prologue
    .line 91
    iput-object p1, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfigResp;->mQrcsImsSettingsQipcallConfig:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;

    .line 92
    return-void
.end method

.method public setQrcsImsSettingsResp(Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp;)V
    .locals 0
    .param p1, "mQrcsImsSettingsResp"    # Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfigResp;->mQrcsImsSettingsResp:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp;

    .line 63
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 122
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfigResp;->writeToParcel(Landroid/os/Parcel;)V

    .line 124
    return-void
.end method
