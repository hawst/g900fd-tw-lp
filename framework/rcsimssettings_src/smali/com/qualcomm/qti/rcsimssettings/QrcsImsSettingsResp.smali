.class public Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp;
.super Ljava/lang/Object;
.source "QrcsImsSettingsResp.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private bQmi_response_result:Z

.field private eIms_settings_rsp:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;

.field private nQmi_response_error:I

.field private nSettings_resp_valid:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 270
    new-instance v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 239
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 240
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 283
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 284
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp;->readFromParcel(Landroid/os/Parcel;)V

    .line 285
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$1;

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public static getQrcsImsSettingsRespInstance()Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp;
    .locals 1

    .prologue
    .line 249
    new-instance v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp;

    invoke-direct {v0}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp;-><init>()V

    return-object v0
.end method

.method private writeToParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;

    .prologue
    .line 264
    iget v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp;->nSettings_resp_valid:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 265
    iget v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp;->nQmi_response_error:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 266
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp;->bQmi_response_result:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 267
    iget-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp;->eIms_settings_rsp:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;

    if-nez v0, :cond_1

    const-string v0, ""

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 268
    return-void

    .line 266
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 267
    :cond_1
    iget-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp;->eIms_settings_rsp:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;

    invoke-virtual {v0}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 254
    const/4 v0, 0x0

    return v0
.end method

.method public getImsSettingsResp()I
    .locals 2

    .prologue
    .line 90
    iget-object v1, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp;->eIms_settings_rsp:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;

    invoke-virtual {v1}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;->ordinal()I

    move-result v0

    .line 91
    .local v0, "result":I
    return v0
.end method

.method public getQmi_response_error()I
    .locals 1

    .prologue
    .line 159
    iget v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp;->nQmi_response_error:I

    return v0
.end method

.method public getSettings_resp_valid()I
    .locals 1

    .prologue
    .line 217
    iget v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp;->nSettings_resp_valid:I

    return v0
.end method

.method public isQmi_response_result()Z
    .locals 1

    .prologue
    .line 187
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp;->bQmi_response_result:Z

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 288
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp;->nSettings_resp_valid:I

    .line 289
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp;->nQmi_response_error:I

    .line 290
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    iput-boolean v1, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp;->bQmi_response_result:Z

    .line 293
    :try_start_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;->valueOf(Ljava/lang/String;)Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;

    move-result-object v1

    iput-object v1, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp;->eIms_settings_rsp:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 298
    :goto_1
    return-void

    .line 290
    :cond_0
    const/4 v1, 0x1

    goto :goto_0

    .line 294
    :catch_0
    move-exception v0

    .line 295
    .local v0, "x":Ljava/lang/IllegalArgumentException;
    const-string v1, "AIDL"

    const-string v2, "eIms_settings_rsp IllegalArgumentException"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 296
    sget-object v1, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;->IMS_SETTINGS_RSP_UNKNOWN:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;

    iput-object v1, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp;->eIms_settings_rsp:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;

    goto :goto_1
.end method

.method public setImsSettingsResp(I)V
    .locals 3
    .param p1, "ims_settings_rsp"    # I

    .prologue
    .line 107
    const-string v0, "AIDL"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "nPublishStatus= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    sparse-switch p1, :sswitch_data_0

    .line 143
    const-string v0, "AIDL"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "default = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    sget-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;->IMS_SETTINGS_RSP_UNKNOWN:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp;->eIms_settings_rsp:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;

    .line 147
    :goto_0
    return-void

    .line 111
    :sswitch_0
    sget-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;->IMS_SETTINGS_RSP_ENUM_MIN_ENUM_VAL:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp;->eIms_settings_rsp:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;

    goto :goto_0

    .line 115
    :sswitch_1
    sget-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;->IMS_SETTINGS_RSP_NO_ERR:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp;->eIms_settings_rsp:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;

    goto :goto_0

    .line 119
    :sswitch_2
    sget-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;->IMS_SETTINGS_RSP_IMS_NOT_READY:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp;->eIms_settings_rsp:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;

    goto :goto_0

    .line 123
    :sswitch_3
    sget-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;->IMS_SETTINGS_RSP_FILE_NOT_AVAILABLE:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp;->eIms_settings_rsp:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;

    goto :goto_0

    .line 127
    :sswitch_4
    sget-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;->IMS_SETTINGS_RSP_READ_FAILED:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp;->eIms_settings_rsp:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;

    goto :goto_0

    .line 131
    :sswitch_5
    sget-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;->IMS_SETTINGS_RSP_WRITE_FAILED:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp;->eIms_settings_rsp:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;

    goto :goto_0

    .line 135
    :sswitch_6
    sget-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;->IMS_SETTINGS_RSP_OTHER_INTERNAL_ERR:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp;->eIms_settings_rsp:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;

    goto :goto_0

    .line 139
    :sswitch_7
    sget-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;->IMS_SETTINGS_RSP_ENUM_MAX_ENUM_VAL:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp;->eIms_settings_rsp:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;

    goto :goto_0

    .line 108
    :sswitch_data_0
    .sparse-switch
        -0x7fffffff -> :sswitch_0
        0x0 -> :sswitch_1
        0x1 -> :sswitch_2
        0x2 -> :sswitch_3
        0x3 -> :sswitch_4
        0x4 -> :sswitch_5
        0x5 -> :sswitch_6
        0x7fffffff -> :sswitch_7
    .end sparse-switch
.end method

.method public setQmi_response_error(I)V
    .locals 0
    .param p1, "nQmi_response_error"    # I

    .prologue
    .line 174
    iput p1, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp;->nQmi_response_error:I

    .line 175
    return-void
.end method

.method public setQmi_response_result(Z)V
    .locals 0
    .param p1, "bQmi_response_result"    # Z

    .prologue
    .line 202
    iput-boolean p1, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp;->bQmi_response_result:Z

    .line 203
    return-void
.end method

.method public setSettings_resp_valid(I)V
    .locals 0
    .param p1, "nSettings_resp_valid"    # I

    .prologue
    .line 232
    iput p1, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp;->nSettings_resp_valid:I

    .line 233
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 258
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp;->writeToParcel(Landroid/os/Parcel;)V

    .line 260
    return-void
.end method
