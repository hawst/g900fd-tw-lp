.class public Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;
.super Ljava/lang/Object;
.source "QrcsImsSettingsIntType.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private nClientID:I

.field private nQrcsImsSettingsIntType:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 132
    new-instance v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const/16 v0, 0x3e9

    iput v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;->nClientID:I

    .line 39
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 143
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const/16 v0, 0x3e9

    iput v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;->nClientID:I

    .line 144
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;->readFromParcel(Landroid/os/Parcel;)V

    .line 145
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType$1;

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public static getQrcsImsSettingsIntTypeInstance()Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;
    .locals 1

    .prologue
    .line 114
    new-instance v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;

    invoke-direct {v0}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;-><init>()V

    return-object v0
.end method

.method private writeToParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "out"    # Landroid/os/Parcel;

    .prologue
    .line 128
    iget-wide v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;->nQrcsImsSettingsIntType:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 129
    iget v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;->nClientID:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 130
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 118
    const/4 v0, 0x0

    return v0
.end method

.method public getClientID()I
    .locals 1

    .prologue
    .line 81
    iget v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;->nClientID:I

    return v0
.end method

.method public getQrcsImsSettingsIntType()J
    .locals 2

    .prologue
    .line 53
    iget-wide v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;->nQrcsImsSettingsIntType:J

    return-wide v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 148
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;->nQrcsImsSettingsIntType:J

    .line 149
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;->nClientID:I

    .line 150
    return-void
.end method

.method public setClientID(I)V
    .locals 0
    .param p1, "nClientID"    # I

    .prologue
    .line 96
    iput p1, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;->nClientID:I

    .line 97
    return-void
.end method

.method public setQrcsImsSettingsIntType(J)V
    .locals 1
    .param p1, "nQrcsImsSettingsInt"    # J

    .prologue
    .line 68
    iput-wide p1, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;->nQrcsImsSettingsIntType:J

    .line 69
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 122
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;->writeToParcel(Landroid/os/Parcel;)V

    .line 124
    return-void
.end method
