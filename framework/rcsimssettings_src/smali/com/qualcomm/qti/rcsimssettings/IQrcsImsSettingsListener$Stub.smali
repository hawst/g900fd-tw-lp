.class public abstract Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsListener$Stub;
.super Landroid/os/Binder;
.source "IQrcsImsSettingsListener.java"

# interfaces
.implements Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsListener$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.qualcomm.qti.rcsimssettings.IQrcsImsSettingsListener"

.field static final TRANSACTION_QrcsImsSettings_ErrorCb:I = 0x1

.field static final TRANSACTION_QrcsImsSettings_IndicationCb:I = 0x3

.field static final TRANSACTION_QrcsImsSettings_PresenceConfigResponseCb:I = 0x4

.field static final TRANSACTION_QrcsImsSettings_QipcallConfigResponseCb:I = 0x2


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 17
    const-string v0, "com.qualcomm.qti.rcsimssettings.IQrcsImsSettingsListener"

    invoke-virtual {p0, p0, v0}, Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsListener$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 18
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsListener;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 25
    if-nez p0, :cond_0

    .line 26
    const/4 v0, 0x0

    .line 32
    :goto_0
    return-object v0

    .line 28
    :cond_0
    const-string v1, "com.qualcomm.qti.rcsimssettings.IQrcsImsSettingsListener"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 29
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsListener;

    if-eqz v1, :cond_1

    .line 30
    check-cast v0, Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsListener;

    goto :goto_0

    .line 32
    :cond_1
    new-instance v0, Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsListener$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsListener$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 36
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 6
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x1

    .line 40
    sparse-switch p1, :sswitch_data_0

    .line 230
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v3

    :goto_0
    return v3

    .line 44
    :sswitch_0
    const-string v4, "com.qualcomm.qti.rcsimssettings.IQrcsImsSettingsListener"

    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 49
    :sswitch_1
    const-string v4, "com.qualcomm.qti.rcsimssettings.IQrcsImsSettingsListener"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 51
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_0

    .line 52
    sget-object v4, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;

    .line 58
    .local v0, "_arg0":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;
    :goto_1
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_1

    .line 59
    sget-object v4, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;

    .line 64
    .local v1, "_arg1":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
    :goto_2
    invoke-virtual {p0, v0, v1}, Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsListener$Stub;->QrcsImsSettings_ErrorCb(Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;)V

    .line 65
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 66
    if-eqz v0, :cond_2

    .line 67
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 68
    invoke-virtual {v0, p3, v3}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;->writeToParcel(Landroid/os/Parcel;I)V

    .line 73
    :goto_3
    if-eqz v1, :cond_3

    .line 74
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 75
    invoke-virtual {v1, p3, v3}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    .line 55
    .end local v0    # "_arg0":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;
    .end local v1    # "_arg1":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
    :cond_0
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;
    goto :goto_1

    .line 62
    :cond_1
    const/4 v1, 0x0

    .restart local v1    # "_arg1":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
    goto :goto_2

    .line 71
    :cond_2
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_3

    .line 78
    :cond_3
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 84
    .end local v0    # "_arg0":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;
    .end local v1    # "_arg1":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
    :sswitch_2
    const-string v4, "com.qualcomm.qti.rcsimssettings.IQrcsImsSettingsListener"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 86
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_4

    .line 87
    sget-object v4, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;

    .line 93
    .restart local v0    # "_arg0":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;
    :goto_4
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_5

    .line 94
    sget-object v4, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId;

    .line 100
    .local v1, "_arg1":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId;
    :goto_5
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_6

    .line 101
    sget-object v4, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfigResp;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfigResp;

    .line 106
    .local v2, "_arg2":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfigResp;
    :goto_6
    invoke-virtual {p0, v0, v1, v2}, Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsListener$Stub;->QrcsImsSettings_QipcallConfigResponseCb(Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId;Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfigResp;)V

    .line 107
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 108
    if-eqz v0, :cond_7

    .line 109
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 110
    invoke-virtual {v0, p3, v3}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;->writeToParcel(Landroid/os/Parcel;I)V

    .line 115
    :goto_7
    if-eqz v1, :cond_8

    .line 116
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 117
    invoke-virtual {v1, p3, v3}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId;->writeToParcel(Landroid/os/Parcel;I)V

    .line 122
    :goto_8
    if-eqz v2, :cond_9

    .line 123
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 124
    invoke-virtual {v2, p3, v3}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfigResp;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 90
    .end local v0    # "_arg0":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;
    .end local v1    # "_arg1":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId;
    .end local v2    # "_arg2":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfigResp;
    :cond_4
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;
    goto :goto_4

    .line 97
    :cond_5
    const/4 v1, 0x0

    .restart local v1    # "_arg1":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId;
    goto :goto_5

    .line 104
    :cond_6
    const/4 v2, 0x0

    .restart local v2    # "_arg2":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfigResp;
    goto :goto_6

    .line 113
    :cond_7
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_7

    .line 120
    :cond_8
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_8

    .line 127
    :cond_9
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 133
    .end local v0    # "_arg0":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;
    .end local v1    # "_arg1":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId;
    .end local v2    # "_arg2":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfigResp;
    :sswitch_3
    const-string v4, "com.qualcomm.qti.rcsimssettings.IQrcsImsSettingsListener"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 135
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_a

    .line 136
    sget-object v4, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;

    .line 142
    .restart local v0    # "_arg0":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;
    :goto_9
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_b

    .line 143
    sget-object v4, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIndId;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIndId;

    .line 149
    .local v1, "_arg1":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIndId;
    :goto_a
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_c

    .line 150
    sget-object v4, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsConfigInd;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsConfigInd;

    .line 155
    .local v2, "_arg2":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsConfigInd;
    :goto_b
    invoke-virtual {p0, v0, v1, v2}, Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsListener$Stub;->QrcsImsSettings_IndicationCb(Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIndId;Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsConfigInd;)V

    .line 156
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 157
    if-eqz v0, :cond_d

    .line 158
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 159
    invoke-virtual {v0, p3, v3}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;->writeToParcel(Landroid/os/Parcel;I)V

    .line 164
    :goto_c
    if-eqz v1, :cond_e

    .line 165
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 166
    invoke-virtual {v1, p3, v3}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIndId;->writeToParcel(Landroid/os/Parcel;I)V

    .line 171
    :goto_d
    if-eqz v2, :cond_f

    .line 172
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 173
    invoke-virtual {v2, p3, v3}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsConfigInd;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 139
    .end local v0    # "_arg0":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;
    .end local v1    # "_arg1":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIndId;
    .end local v2    # "_arg2":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsConfigInd;
    :cond_a
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;
    goto :goto_9

    .line 146
    :cond_b
    const/4 v1, 0x0

    .restart local v1    # "_arg1":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIndId;
    goto :goto_a

    .line 153
    :cond_c
    const/4 v2, 0x0

    .restart local v2    # "_arg2":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsConfigInd;
    goto :goto_b

    .line 162
    :cond_d
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_c

    .line 169
    :cond_e
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_d

    .line 176
    :cond_f
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 182
    .end local v0    # "_arg0":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;
    .end local v1    # "_arg1":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIndId;
    .end local v2    # "_arg2":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsConfigInd;
    :sswitch_4
    const-string v4, "com.qualcomm.qti.rcsimssettings.IQrcsImsSettingsListener"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 184
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_10

    .line 185
    sget-object v4, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;

    .line 191
    .restart local v0    # "_arg0":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;
    :goto_e
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_11

    .line 192
    sget-object v4, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId;

    .line 198
    .local v1, "_arg1":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId;
    :goto_f
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_12

    .line 199
    sget-object v4, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfigResp;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfigResp;

    .line 204
    .local v2, "_arg2":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfigResp;
    :goto_10
    invoke-virtual {p0, v0, v1, v2}, Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsListener$Stub;->QrcsImsSettings_PresenceConfigResponseCb(Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId;Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfigResp;)V

    .line 205
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 206
    if-eqz v0, :cond_13

    .line 207
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 208
    invoke-virtual {v0, p3, v3}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;->writeToParcel(Landroid/os/Parcel;I)V

    .line 213
    :goto_11
    if-eqz v1, :cond_14

    .line 214
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 215
    invoke-virtual {v1, p3, v3}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId;->writeToParcel(Landroid/os/Parcel;I)V

    .line 220
    :goto_12
    if-eqz v2, :cond_15

    .line 221
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 222
    invoke-virtual {v2, p3, v3}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfigResp;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 188
    .end local v0    # "_arg0":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;
    .end local v1    # "_arg1":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId;
    .end local v2    # "_arg2":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfigResp;
    :cond_10
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;
    goto :goto_e

    .line 195
    :cond_11
    const/4 v1, 0x0

    .restart local v1    # "_arg1":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId;
    goto :goto_f

    .line 202
    :cond_12
    const/4 v2, 0x0

    .restart local v2    # "_arg2":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfigResp;
    goto :goto_10

    .line 211
    :cond_13
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_11

    .line 218
    :cond_14
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_12

    .line 225
    :cond_15
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 40
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
