.class public final enum Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;
.super Ljava/lang/Enum;
.source "QrcsImsSettingsError.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "qrcs_ims_error"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

.field public static final enum QRCS_IMS_CLIENT_ALLOC_FAILURE:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

.field public static final enum QRCS_IMS_ERR_MAX:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

.field public static final enum QRCS_IMS_ERR_UNKNOWN:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

.field public static final enum QRCS_IMS_EXTENDED_ERR:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

.field public static final enum QRCS_IMS_GENERAL_ERR:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

.field public static final enum QRCS_IMS_INTERNAL_ERR:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

.field public static final enum QRCS_IMS_INVALID_PARAM:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

.field public static final enum QRCS_IMS_INVALID_TXN:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

.field public static final enum QRCS_IMS_MEMORY_ERR:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

.field public static final enum QRCS_IMS_NO_ERR:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

.field public static final enum QRCS_IMS_PORT_NOT_OPEN_ERR:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

.field public static final enum QRCS_IMS_SERVICE_ERR:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

.field public static final enum QRCS_IMS_TIMEOUT_ERR:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 32
    new-instance v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    const-string v1, "QRCS_IMS_NO_ERR"

    invoke-direct {v0, v1, v3}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;->QRCS_IMS_NO_ERR:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    .line 33
    new-instance v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    const-string v1, "QRCS_IMS_INTERNAL_ERR"

    invoke-direct {v0, v1, v4}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;->QRCS_IMS_INTERNAL_ERR:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    .line 34
    new-instance v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    const-string v1, "QRCS_IMS_SERVICE_ERR"

    invoke-direct {v0, v1, v5}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;->QRCS_IMS_SERVICE_ERR:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    .line 35
    new-instance v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    const-string v1, "QRCS_IMS_TIMEOUT_ERR"

    invoke-direct {v0, v1, v6}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;->QRCS_IMS_TIMEOUT_ERR:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    .line 36
    new-instance v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    const-string v1, "QRCS_IMS_EXTENDED_ERR"

    invoke-direct {v0, v1, v7}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;->QRCS_IMS_EXTENDED_ERR:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    .line 37
    new-instance v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    const-string v1, "QRCS_IMS_PORT_NOT_OPEN_ERR"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;->QRCS_IMS_PORT_NOT_OPEN_ERR:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    .line 38
    new-instance v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    const-string v1, "QRCS_IMS_MEMORY_ERR"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;->QRCS_IMS_MEMORY_ERR:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    .line 39
    new-instance v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    const-string v1, "QRCS_IMS_INVALID_TXN"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;->QRCS_IMS_INVALID_TXN:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    .line 40
    new-instance v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    const-string v1, "QRCS_IMS_CLIENT_ALLOC_FAILURE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;->QRCS_IMS_CLIENT_ALLOC_FAILURE:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    .line 41
    new-instance v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    const-string v1, "QRCS_IMS_INVALID_PARAM"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;->QRCS_IMS_INVALID_PARAM:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    .line 42
    new-instance v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    const-string v1, "QRCS_IMS_GENERAL_ERR"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;->QRCS_IMS_GENERAL_ERR:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    .line 44
    new-instance v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    const-string v1, "QRCS_IMS_ERR_MAX"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;->QRCS_IMS_ERR_MAX:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    .line 46
    new-instance v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    const-string v1, "QRCS_IMS_ERR_UNKNOWN"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;->QRCS_IMS_ERR_UNKNOWN:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    .line 30
    const/16 v0, 0xd

    new-array v0, v0, [Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    sget-object v1, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;->QRCS_IMS_NO_ERR:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    aput-object v1, v0, v3

    sget-object v1, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;->QRCS_IMS_INTERNAL_ERR:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    aput-object v1, v0, v4

    sget-object v1, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;->QRCS_IMS_SERVICE_ERR:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    aput-object v1, v0, v5

    sget-object v1, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;->QRCS_IMS_TIMEOUT_ERR:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    aput-object v1, v0, v6

    sget-object v1, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;->QRCS_IMS_EXTENDED_ERR:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;->QRCS_IMS_PORT_NOT_OPEN_ERR:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;->QRCS_IMS_MEMORY_ERR:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;->QRCS_IMS_INVALID_TXN:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;->QRCS_IMS_CLIENT_ALLOC_FAILURE:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;->QRCS_IMS_INVALID_PARAM:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;->QRCS_IMS_GENERAL_ERR:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;->QRCS_IMS_ERR_MAX:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;->QRCS_IMS_ERR_UNKNOWN:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    aput-object v2, v0, v1

    sput-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;->$VALUES:[Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 30
    const-class v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    return-object v0
.end method

.method public static values()[Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;->$VALUES:[Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    invoke-virtual {v0}, [Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError$qrcs_ims_error;

    return-object v0
.end method
