.class public Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfigResp;
.super Ljava/lang/Object;
.source "QrcsImsSettingsPresenceConfigResp.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfigResp;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public mQrcsImsSettingsPresenceConfig:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;

.field public mQrcsImsSettingsResp:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 131
    new-instance v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfigResp$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfigResp$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfigResp;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    new-instance v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;

    invoke-direct {v0}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;-><init>()V

    iput-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfigResp;->mQrcsImsSettingsPresenceConfig:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;

    .line 102
    new-instance v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp;

    invoke-direct {v0}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp;-><init>()V

    iput-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfigResp;->mQrcsImsSettingsResp:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp;

    .line 103
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 144
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 145
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfigResp;->readFromParcel(Landroid/os/Parcel;)V

    .line 146
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfigResp$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfigResp$1;

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfigResp;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public static getQrcsImsSettingsPresenceConfigRespInstance()Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfigResp;
    .locals 1

    .prologue
    .line 112
    new-instance v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfigResp;

    invoke-direct {v0}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfigResp;-><init>()V

    return-object v0
.end method

.method private writeToParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;

    .prologue
    .line 127
    iget-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfigResp;->mQrcsImsSettingsPresenceConfig:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 128
    iget-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfigResp;->mQrcsImsSettingsResp:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 129
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 117
    const/4 v0, 0x0

    return v0
.end method

.method public getQrcsImsSettingsPresenceConfig()Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfigResp;->mQrcsImsSettingsPresenceConfig:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;

    return-object v0
.end method

.method public getQrcsImsSettingsResp()Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfigResp;->mQrcsImsSettingsResp:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 149
    const-class v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfigResp;->mQrcsImsSettingsPresenceConfig:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;

    .line 151
    const-class v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfigResp;->mQrcsImsSettingsResp:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp;

    .line 153
    return-void
.end method

.method public setQrcsImsSettingsPresenceConfig(Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;)V
    .locals 0
    .param p1, "mQrcsImsSettingsPresenceConfig"    # Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;

    .prologue
    .line 90
    iput-object p1, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfigResp;->mQrcsImsSettingsPresenceConfig:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;

    .line 91
    return-void
.end method

.method public setQrcsImsSettingsResp(Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp;)V
    .locals 0
    .param p1, "mQrcsImsSettingsResp"    # Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfigResp;->mQrcsImsSettingsResp:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp;

    .line 62
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 121
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfigResp;->writeToParcel(Landroid/os/Parcel;)V

    .line 123
    return-void
.end method
