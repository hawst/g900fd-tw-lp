.class public final enum Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;
.super Ljava/lang/Enum;
.source "QrcsImsSettingsRespId.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "qrcs_ims_settings_resp_id"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;

.field public static final enum QRCS_IMS_SETTINGS_GET_PRESENCE_CONFIG_RESP:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;

.field public static final enum QRCS_IMS_SETTINGS_GET_QIPCALL_CONFIG_RESP:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;

.field public static final enum QRCS_IMS_SETTINGS_RESP_ID_INVALID:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;

.field public static final enum QRCS_IMS_SETTINGS_RESP_ID_MAX:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;

.field public static final enum QRCS_IMS_SETTINGS_RESP_UNKNOWN:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;

.field public static final enum QRCS_IMS_SETTINGS_SET_PRESENCE_CONFIG_RESP:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;

.field public static final enum QRCS_IMS_SETTINGS_SET_QIPCALL_CONFIG_RESP:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 32
    new-instance v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;

    const-string v1, "QRCS_IMS_SETTINGS_GET_QIPCALL_CONFIG_RESP"

    invoke-direct {v0, v1, v3}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;->QRCS_IMS_SETTINGS_GET_QIPCALL_CONFIG_RESP:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;

    .line 33
    new-instance v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;

    const-string v1, "QRCS_IMS_SETTINGS_SET_QIPCALL_CONFIG_RESP"

    invoke-direct {v0, v1, v4}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;->QRCS_IMS_SETTINGS_SET_QIPCALL_CONFIG_RESP:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;

    .line 34
    new-instance v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;

    const-string v1, "QRCS_IMS_SETTINGS_GET_PRESENCE_CONFIG_RESP"

    invoke-direct {v0, v1, v5}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;->QRCS_IMS_SETTINGS_GET_PRESENCE_CONFIG_RESP:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;

    .line 35
    new-instance v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;

    const-string v1, "QRCS_IMS_SETTINGS_SET_PRESENCE_CONFIG_RESP"

    invoke-direct {v0, v1, v6}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;->QRCS_IMS_SETTINGS_SET_PRESENCE_CONFIG_RESP:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;

    .line 36
    new-instance v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;

    const-string v1, "QRCS_IMS_SETTINGS_RESP_ID_INVALID"

    invoke-direct {v0, v1, v7}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;->QRCS_IMS_SETTINGS_RESP_ID_INVALID:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;

    .line 38
    new-instance v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;

    const-string v1, "QRCS_IMS_SETTINGS_RESP_ID_MAX"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;->QRCS_IMS_SETTINGS_RESP_ID_MAX:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;

    .line 40
    new-instance v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;

    const-string v1, "QRCS_IMS_SETTINGS_RESP_UNKNOWN"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;->QRCS_IMS_SETTINGS_RESP_UNKNOWN:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;

    .line 30
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;

    sget-object v1, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;->QRCS_IMS_SETTINGS_GET_QIPCALL_CONFIG_RESP:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;

    aput-object v1, v0, v3

    sget-object v1, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;->QRCS_IMS_SETTINGS_SET_QIPCALL_CONFIG_RESP:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;

    aput-object v1, v0, v4

    sget-object v1, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;->QRCS_IMS_SETTINGS_GET_PRESENCE_CONFIG_RESP:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;

    aput-object v1, v0, v5

    sget-object v1, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;->QRCS_IMS_SETTINGS_SET_PRESENCE_CONFIG_RESP:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;

    aput-object v1, v0, v6

    sget-object v1, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;->QRCS_IMS_SETTINGS_RESP_ID_INVALID:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;->QRCS_IMS_SETTINGS_RESP_ID_MAX:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;->QRCS_IMS_SETTINGS_RESP_UNKNOWN:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;

    aput-object v2, v0, v1

    sput-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;->$VALUES:[Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 30
    const-class v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;

    return-object v0
.end method

.method public static values()[Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;->$VALUES:[Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;

    invoke-virtual {v0}, [Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;

    return-object v0
.end method
