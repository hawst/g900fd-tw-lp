.class public Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIndId;
.super Ljava/lang/Object;
.source "QrcsImsSettingsIndId.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIndId$qrcs_ims_settings_ind_id;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIndId;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private eQrcsImsSettingsIndId:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIndId$qrcs_ims_settings_ind_id;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 151
    new-instance v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIndId$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIndId$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIndId;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 164
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 165
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIndId;->readFromParcel(Landroid/os/Parcel;)V

    .line 166
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIndId$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIndId$1;

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIndId;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public static getQrcsImsSettingsIndIdInstance()Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIndId;
    .locals 1

    .prologue
    .line 62
    new-instance v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIndId;

    invoke-direct {v0}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIndId;-><init>()V

    return-object v0
.end method

.method private writeToParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;

    .prologue
    .line 148
    iget-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIndId;->eQrcsImsSettingsIndId:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIndId$qrcs_ims_settings_ind_id;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 149
    return-void

    .line 148
    :cond_0
    iget-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIndId;->eQrcsImsSettingsIndId:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIndId$qrcs_ims_settings_ind_id;

    invoke-virtual {v0}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIndId$qrcs_ims_settings_ind_id;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 138
    const/4 v0, 0x0

    return v0
.end method

.method public getQrcsImsSettingsIndId()I
    .locals 2

    .prologue
    .line 77
    iget-object v1, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIndId;->eQrcsImsSettingsIndId:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIndId$qrcs_ims_settings_ind_id;

    invoke-virtual {v1}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIndId$qrcs_ims_settings_ind_id;->ordinal()I

    move-result v0

    .line 78
    .local v0, "indication_id":I
    return v0
.end method

.method public getQrcsImsSettingsIndIdObj()Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIndId$qrcs_ims_settings_ind_id;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIndId;->eQrcsImsSettingsIndId:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIndId$qrcs_ims_settings_ind_id;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 171
    :try_start_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIndId$qrcs_ims_settings_ind_id;->valueOf(Ljava/lang/String;)Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIndId$qrcs_ims_settings_ind_id;

    move-result-object v1

    iput-object v1, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIndId;->eQrcsImsSettingsIndId:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIndId$qrcs_ims_settings_ind_id;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 176
    :goto_0
    return-void

    .line 172
    :catch_0
    move-exception v0

    .line 173
    .local v0, "x":Ljava/lang/IllegalArgumentException;
    const-string v1, "AIDL"

    const-string v2, "eQrcsImsSettingsIndId IllegalArgumentException"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 174
    sget-object v1, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIndId$qrcs_ims_settings_ind_id;->QRCS_IMS_SETTINGS_IND_ID_UNKNOWN:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIndId$qrcs_ims_settings_ind_id;

    iput-object v1, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIndId;->eQrcsImsSettingsIndId:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIndId$qrcs_ims_settings_ind_id;

    goto :goto_0
.end method

.method public setQrcsImsSettingsIndId(I)V
    .locals 3
    .param p1, "nQrcsImsSettingsIndId"    # I

    .prologue
    .line 105
    const-string v0, "AIDL"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "nQrcsImsSettingsIndId = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    sparse-switch p1, :sswitch_data_0

    .line 124
    const-string v0, "AIDL"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "default nQrcsImsSettingsIndId = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    sget-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIndId$qrcs_ims_settings_ind_id;->QRCS_IMS_SETTINGS_IND_ID_UNKNOWN:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIndId$qrcs_ims_settings_ind_id;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIndId;->eQrcsImsSettingsIndId:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIndId$qrcs_ims_settings_ind_id;

    .line 129
    :goto_0
    return-void

    .line 109
    :sswitch_0
    sget-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIndId$qrcs_ims_settings_ind_id;->QRCS_IMS_SETTINGS_QIPCALL_CONFIG_IND:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIndId$qrcs_ims_settings_ind_id;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIndId;->eQrcsImsSettingsIndId:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIndId$qrcs_ims_settings_ind_id;

    goto :goto_0

    .line 113
    :sswitch_1
    sget-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIndId$qrcs_ims_settings_ind_id;->QRCS_IMS_SETTINGS_PRESENCE_CONFIG_IND:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIndId$qrcs_ims_settings_ind_id;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIndId;->eQrcsImsSettingsIndId:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIndId$qrcs_ims_settings_ind_id;

    goto :goto_0

    .line 117
    :sswitch_2
    sget-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIndId$qrcs_ims_settings_ind_id;->QRCS_IMS_SETTINGS_IND_ID_INVALID:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIndId$qrcs_ims_settings_ind_id;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIndId;->eQrcsImsSettingsIndId:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIndId$qrcs_ims_settings_ind_id;

    goto :goto_0

    .line 121
    :sswitch_3
    sget-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIndId$qrcs_ims_settings_ind_id;->QRCS_IMS_SETTINGS_IND_ID_MAX:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIndId$qrcs_ims_settings_ind_id;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIndId;->eQrcsImsSettingsIndId:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIndId$qrcs_ims_settings_ind_id;

    goto :goto_0

    .line 106
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x10000000 -> :sswitch_3
    .end sparse-switch
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 142
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIndId;->writeToParcel(Landroid/os/Parcel;)V

    .line 144
    return-void
.end method
