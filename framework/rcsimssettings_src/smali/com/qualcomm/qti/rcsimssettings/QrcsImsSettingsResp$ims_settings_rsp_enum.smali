.class public final enum Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;
.super Ljava/lang/Enum;
.source "QrcsImsSettingsResp.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ims_settings_rsp_enum"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;

.field public static final enum IMS_SETTINGS_RSP_ENUM_MAX_ENUM_VAL:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;

.field public static final enum IMS_SETTINGS_RSP_ENUM_MIN_ENUM_VAL:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;

.field public static final enum IMS_SETTINGS_RSP_FILE_NOT_AVAILABLE:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;

.field public static final enum IMS_SETTINGS_RSP_IMS_NOT_READY:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;

.field public static final enum IMS_SETTINGS_RSP_NO_ERR:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;

.field public static final enum IMS_SETTINGS_RSP_OTHER_INTERNAL_ERR:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;

.field public static final enum IMS_SETTINGS_RSP_READ_FAILED:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;

.field public static final enum IMS_SETTINGS_RSP_UNKNOWN:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;

.field public static final enum IMS_SETTINGS_RSP_WRITE_FAILED:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 34
    new-instance v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;

    const-string v1, "IMS_SETTINGS_RSP_ENUM_MIN_ENUM_VAL"

    invoke-direct {v0, v1, v3}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;->IMS_SETTINGS_RSP_ENUM_MIN_ENUM_VAL:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;

    .line 39
    new-instance v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;

    const-string v1, "IMS_SETTINGS_RSP_NO_ERR"

    invoke-direct {v0, v1, v4}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;->IMS_SETTINGS_RSP_NO_ERR:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;

    .line 40
    new-instance v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;

    const-string v1, "IMS_SETTINGS_RSP_IMS_NOT_READY"

    invoke-direct {v0, v1, v5}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;->IMS_SETTINGS_RSP_IMS_NOT_READY:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;

    .line 41
    new-instance v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;

    const-string v1, "IMS_SETTINGS_RSP_FILE_NOT_AVAILABLE"

    invoke-direct {v0, v1, v6}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;->IMS_SETTINGS_RSP_FILE_NOT_AVAILABLE:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;

    .line 42
    new-instance v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;

    const-string v1, "IMS_SETTINGS_RSP_READ_FAILED"

    invoke-direct {v0, v1, v7}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;->IMS_SETTINGS_RSP_READ_FAILED:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;

    .line 43
    new-instance v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;

    const-string v1, "IMS_SETTINGS_RSP_WRITE_FAILED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;->IMS_SETTINGS_RSP_WRITE_FAILED:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;

    .line 44
    new-instance v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;

    const-string v1, "IMS_SETTINGS_RSP_OTHER_INTERNAL_ERR"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;->IMS_SETTINGS_RSP_OTHER_INTERNAL_ERR:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;

    .line 46
    new-instance v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;

    const-string v1, "IMS_SETTINGS_RSP_ENUM_MAX_ENUM_VAL"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;->IMS_SETTINGS_RSP_ENUM_MAX_ENUM_VAL:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;

    .line 48
    new-instance v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;

    const-string v1, "IMS_SETTINGS_RSP_UNKNOWN"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;->IMS_SETTINGS_RSP_UNKNOWN:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;

    .line 31
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;

    sget-object v1, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;->IMS_SETTINGS_RSP_ENUM_MIN_ENUM_VAL:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;

    aput-object v1, v0, v3

    sget-object v1, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;->IMS_SETTINGS_RSP_NO_ERR:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;

    aput-object v1, v0, v4

    sget-object v1, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;->IMS_SETTINGS_RSP_IMS_NOT_READY:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;

    aput-object v1, v0, v5

    sget-object v1, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;->IMS_SETTINGS_RSP_FILE_NOT_AVAILABLE:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;

    aput-object v1, v0, v6

    sget-object v1, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;->IMS_SETTINGS_RSP_READ_FAILED:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;->IMS_SETTINGS_RSP_WRITE_FAILED:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;->IMS_SETTINGS_RSP_OTHER_INTERNAL_ERR:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;->IMS_SETTINGS_RSP_ENUM_MAX_ENUM_VAL:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;->IMS_SETTINGS_RSP_UNKNOWN:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;

    aput-object v2, v0, v1

    sput-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;->$VALUES:[Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 31
    const-class v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;

    return-object v0
.end method

.method public static values()[Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;->$VALUES:[Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;

    invoke-virtual {v0}, [Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsResp$ims_settings_rsp_enum;

    return-object v0
.end method
