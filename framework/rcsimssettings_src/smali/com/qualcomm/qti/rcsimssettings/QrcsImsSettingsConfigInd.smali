.class public Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsConfigInd;
.super Ljava/lang/Object;
.source "QrcsImsSettingsConfigInd.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsConfigInd;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public mQrcsImsSettingsPresenceConfig:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;

.field public mQrcsImsSettingsQipcallConfig:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 126
    new-instance v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsConfigInd$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsConfigInd$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsConfigInd;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsConfigInd;->mQrcsImsSettingsQipcallConfig:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;

    .line 24
    iput-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsConfigInd;->mQrcsImsSettingsPresenceConfig:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;

    .line 98
    new-instance v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;

    invoke-direct {v0}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;-><init>()V

    iput-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsConfigInd;->mQrcsImsSettingsQipcallConfig:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;

    .line 99
    new-instance v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;

    invoke-direct {v0}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;-><init>()V

    iput-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsConfigInd;->mQrcsImsSettingsPresenceConfig:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;

    .line 101
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    const/4 v0, 0x0

    .line 139
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsConfigInd;->mQrcsImsSettingsQipcallConfig:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;

    .line 24
    iput-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsConfigInd;->mQrcsImsSettingsPresenceConfig:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;

    .line 140
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsConfigInd;->readFromParcel(Landroid/os/Parcel;)V

    .line 141
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsConfigInd$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsConfigInd$1;

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsConfigInd;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private writeToParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;

    .prologue
    .line 115
    iget-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsConfigInd;->mQrcsImsSettingsPresenceConfig:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;

    if-eqz v0, :cond_0

    .line 117
    iget-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsConfigInd;->mQrcsImsSettingsPresenceConfig:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 119
    :cond_0
    iget-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsConfigInd;->mQrcsImsSettingsQipcallConfig:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;

    if-eqz v0, :cond_1

    .line 121
    iget-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsConfigInd;->mQrcsImsSettingsQipcallConfig:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 124
    :cond_1
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 105
    const/4 v0, 0x0

    return v0
.end method

.method public getQrcsImsSettingsPresenceConfig()Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsConfigInd;->mQrcsImsSettingsPresenceConfig:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;

    return-object v0
.end method

.method public getQrcsImsSettingsQipcallConfig()Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsConfigInd;->mQrcsImsSettingsQipcallConfig:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 144
    const-class v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsConfigInd;->mQrcsImsSettingsPresenceConfig:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;

    .line 145
    const-class v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsConfigInd;->mQrcsImsSettingsQipcallConfig:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;

    .line 146
    return-void
.end method

.method public setQrcsImsSettingsPresenceConfig(Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;)V
    .locals 0
    .param p1, "mQrcsImsSettingsPresenceConfig"    # Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsConfigInd;->mQrcsImsSettingsPresenceConfig:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;

    .line 57
    return-void
.end method

.method public setQrcsImsSettingsQipcallConfig(Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;)V
    .locals 0
    .param p1, "mQrcsImsSettingsQipcallConfig"    # Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;

    .prologue
    .line 87
    iput-object p1, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsConfigInd;->mQrcsImsSettingsQipcallConfig:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;

    .line 88
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 109
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsConfigInd;->writeToParcel(Landroid/os/Parcel;)V

    .line 111
    return-void
.end method
