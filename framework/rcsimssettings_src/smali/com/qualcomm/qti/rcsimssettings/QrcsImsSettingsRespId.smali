.class public Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId;
.super Ljava/lang/Object;
.source "QrcsImsSettingsRespId.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private eQrcsImsSettingsRespId:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 144
    new-instance v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 157
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 158
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId;->readFromParcel(Landroid/os/Parcel;)V

    .line 159
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$1;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public static getQrcsImsSettingsRespIdInstance()Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId;
    .locals 1

    .prologue
    .line 61
    new-instance v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId;

    invoke-direct {v0}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId;-><init>()V

    return-object v0
.end method

.method private writeToParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId;->eQrcsImsSettingsRespId:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 142
    return-void

    .line 141
    :cond_0
    iget-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId;->eQrcsImsSettingsRespId:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;

    invoke-virtual {v0}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 131
    const/4 v0, 0x0

    return v0
.end method

.method public getQrcsImsSettingsRespId()Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId;->eQrcsImsSettingsRespId:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 164
    :try_start_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;->valueOf(Ljava/lang/String;)Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;

    move-result-object v1

    iput-object v1, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId;->eQrcsImsSettingsRespId:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 169
    :goto_0
    return-void

    .line 165
    :catch_0
    move-exception v0

    .line 166
    .local v0, "x":Ljava/lang/IllegalArgumentException;
    const-string v1, "AIDL"

    const-string v2, "eQrcsImsSettingsRespId IllegalArgumentException"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    sget-object v1, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;->QRCS_IMS_SETTINGS_RESP_UNKNOWN:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;

    iput-object v1, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId;->eQrcsImsSettingsRespId:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;

    goto :goto_0
.end method

.method public setQrcsImsSettingsRespId(I)V
    .locals 3
    .param p1, "nQrcsImsSettingsRespId"    # I

    .prologue
    .line 91
    const-string v0, "AIDL"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "nQrcsImsSettingsRespId = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    sparse-switch p1, :sswitch_data_0

    .line 119
    const-string v0, "AIDL"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "default nQrcsImsSettingsRespId = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    sget-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;->QRCS_IMS_SETTINGS_RESP_UNKNOWN:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId;->eQrcsImsSettingsRespId:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;

    .line 123
    :goto_0
    return-void

    .line 95
    :sswitch_0
    sget-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;->QRCS_IMS_SETTINGS_GET_QIPCALL_CONFIG_RESP:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId;->eQrcsImsSettingsRespId:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;

    goto :goto_0

    .line 99
    :sswitch_1
    sget-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;->QRCS_IMS_SETTINGS_SET_QIPCALL_CONFIG_RESP:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId;->eQrcsImsSettingsRespId:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;

    goto :goto_0

    .line 103
    :sswitch_2
    sget-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;->QRCS_IMS_SETTINGS_GET_PRESENCE_CONFIG_RESP:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId;->eQrcsImsSettingsRespId:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;

    goto :goto_0

    .line 107
    :sswitch_3
    sget-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;->QRCS_IMS_SETTINGS_SET_PRESENCE_CONFIG_RESP:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId;->eQrcsImsSettingsRespId:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;

    goto :goto_0

    .line 111
    :sswitch_4
    sget-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;->QRCS_IMS_SETTINGS_RESP_ID_INVALID:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId;->eQrcsImsSettingsRespId:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;

    goto :goto_0

    .line 115
    :sswitch_5
    sget-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;->QRCS_IMS_SETTINGS_RESP_ID_MAX:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;

    iput-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId;->eQrcsImsSettingsRespId:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId$qrcs_ims_settings_resp_id;

    goto :goto_0

    .line 92
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x10000000 -> :sswitch_5
    .end sparse-switch
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 135
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId;->writeToParcel(Landroid/os/Parcel;)V

    .line 137
    return-void
.end method
