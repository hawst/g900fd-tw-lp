.class Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsListener$Stub$Proxy;
.super Ljava/lang/Object;
.source "IQrcsImsSettingsListener.java"

# interfaces
.implements Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsListener$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .locals 0
    .param p1, "remote"    # Landroid/os/IBinder;

    .prologue
    .line 236
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 237
    iput-object p1, p0, Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    .line 238
    return-void
.end method


# virtual methods
.method public QrcsImsSettings_ErrorCb(Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;)V
    .locals 5
    .param p1, "client_handle"    # Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;
    .param p2, "error"    # Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 262
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 263
    .local v0, "_data":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 265
    .local v1, "_reply":Landroid/os/Parcel;
    :try_start_0
    const-string v2, "com.qualcomm.qti.rcsimssettings.IQrcsImsSettingsListener"

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 266
    if-eqz p1, :cond_2

    .line 267
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 268
    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;->writeToParcel(Landroid/os/Parcel;I)V

    .line 273
    :goto_0
    if-eqz p2, :cond_3

    .line 274
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 275
    const/4 v2, 0x0

    invoke-virtual {p2, v0, v2}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;->writeToParcel(Landroid/os/Parcel;I)V

    .line 280
    :goto_1
    iget-object v2, p0, Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 281
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    .line 282
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_0

    .line 283
    invoke-virtual {p1, v1}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;->readFromParcel(Landroid/os/Parcel;)V

    .line 285
    :cond_0
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_1

    .line 286
    invoke-virtual {p2, v1}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;->readFromParcel(Landroid/os/Parcel;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 290
    :cond_1
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 291
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 293
    return-void

    .line 271
    :cond_2
    const/4 v2, 0x0

    :try_start_1
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 290
    :catchall_0
    move-exception v2

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 291
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    throw v2

    .line 278
    :cond_3
    const/4 v2, 0x0

    :try_start_2
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public QrcsImsSettings_IndicationCb(Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIndId;Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsConfigInd;)V
    .locals 5
    .param p1, "client_handle"    # Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;
    .param p2, "ind_id"    # Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIndId;
    .param p3, "ind_msg"    # Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsConfigInd;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 368
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 369
    .local v0, "_data":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 371
    .local v1, "_reply":Landroid/os/Parcel;
    :try_start_0
    const-string v2, "com.qualcomm.qti.rcsimssettings.IQrcsImsSettingsListener"

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 372
    if-eqz p1, :cond_3

    .line 373
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 374
    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;->writeToParcel(Landroid/os/Parcel;I)V

    .line 379
    :goto_0
    if-eqz p2, :cond_4

    .line 380
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 381
    const/4 v2, 0x0

    invoke-virtual {p2, v0, v2}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIndId;->writeToParcel(Landroid/os/Parcel;I)V

    .line 386
    :goto_1
    if-eqz p3, :cond_5

    .line 387
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 388
    const/4 v2, 0x0

    invoke-virtual {p3, v0, v2}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsConfigInd;->writeToParcel(Landroid/os/Parcel;I)V

    .line 393
    :goto_2
    iget-object v2, p0, Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    const/4 v3, 0x3

    const/4 v4, 0x0

    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 394
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    .line 395
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_0

    .line 396
    invoke-virtual {p1, v1}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;->readFromParcel(Landroid/os/Parcel;)V

    .line 398
    :cond_0
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_1

    .line 399
    invoke-virtual {p2, v1}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIndId;->readFromParcel(Landroid/os/Parcel;)V

    .line 401
    :cond_1
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_2

    .line 402
    invoke-virtual {p3, v1}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsConfigInd;->readFromParcel(Landroid/os/Parcel;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 406
    :cond_2
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 407
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 409
    return-void

    .line 377
    :cond_3
    const/4 v2, 0x0

    :try_start_1
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 406
    :catchall_0
    move-exception v2

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 407
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    throw v2

    .line 384
    :cond_4
    const/4 v2, 0x0

    :try_start_2
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_1

    .line 391
    :cond_5
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2
.end method

.method public QrcsImsSettings_PresenceConfigResponseCb(Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId;Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfigResp;)V
    .locals 5
    .param p1, "client_handle"    # Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;
    .param p2, "resp_id"    # Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId;
    .param p3, "resp_msg"    # Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfigResp;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 426
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 427
    .local v0, "_data":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 429
    .local v1, "_reply":Landroid/os/Parcel;
    :try_start_0
    const-string v2, "com.qualcomm.qti.rcsimssettings.IQrcsImsSettingsListener"

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 430
    if-eqz p1, :cond_3

    .line 431
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 432
    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;->writeToParcel(Landroid/os/Parcel;I)V

    .line 437
    :goto_0
    if-eqz p2, :cond_4

    .line 438
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 439
    const/4 v2, 0x0

    invoke-virtual {p2, v0, v2}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId;->writeToParcel(Landroid/os/Parcel;I)V

    .line 444
    :goto_1
    if-eqz p3, :cond_5

    .line 445
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 446
    const/4 v2, 0x0

    invoke-virtual {p3, v0, v2}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfigResp;->writeToParcel(Landroid/os/Parcel;I)V

    .line 451
    :goto_2
    iget-object v2, p0, Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    const/4 v3, 0x4

    const/4 v4, 0x0

    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 452
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    .line 453
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_0

    .line 454
    invoke-virtual {p1, v1}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;->readFromParcel(Landroid/os/Parcel;)V

    .line 456
    :cond_0
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_1

    .line 457
    invoke-virtual {p2, v1}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId;->readFromParcel(Landroid/os/Parcel;)V

    .line 459
    :cond_1
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_2

    .line 460
    invoke-virtual {p3, v1}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfigResp;->readFromParcel(Landroid/os/Parcel;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 464
    :cond_2
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 465
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 467
    return-void

    .line 435
    :cond_3
    const/4 v2, 0x0

    :try_start_1
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 464
    :catchall_0
    move-exception v2

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 465
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    throw v2

    .line 442
    :cond_4
    const/4 v2, 0x0

    :try_start_2
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_1

    .line 449
    :cond_5
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2
.end method

.method public QrcsImsSettings_QipcallConfigResponseCb(Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId;Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfigResp;)V
    .locals 5
    .param p1, "client_handle"    # Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;
    .param p2, "resp_id"    # Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId;
    .param p3, "resp_msg"    # Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfigResp;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 310
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 311
    .local v0, "_data":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 313
    .local v1, "_reply":Landroid/os/Parcel;
    :try_start_0
    const-string v2, "com.qualcomm.qti.rcsimssettings.IQrcsImsSettingsListener"

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 314
    if-eqz p1, :cond_3

    .line 315
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 316
    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;->writeToParcel(Landroid/os/Parcel;I)V

    .line 321
    :goto_0
    if-eqz p2, :cond_4

    .line 322
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 323
    const/4 v2, 0x0

    invoke-virtual {p2, v0, v2}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId;->writeToParcel(Landroid/os/Parcel;I)V

    .line 328
    :goto_1
    if-eqz p3, :cond_5

    .line 329
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 330
    const/4 v2, 0x0

    invoke-virtual {p3, v0, v2}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfigResp;->writeToParcel(Landroid/os/Parcel;I)V

    .line 335
    :goto_2
    iget-object v2, p0, Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 336
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    .line 337
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_0

    .line 338
    invoke-virtual {p1, v1}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;->readFromParcel(Landroid/os/Parcel;)V

    .line 340
    :cond_0
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_1

    .line 341
    invoke-virtual {p2, v1}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsRespId;->readFromParcel(Landroid/os/Parcel;)V

    .line 343
    :cond_1
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_2

    .line 344
    invoke-virtual {p3, v1}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfigResp;->readFromParcel(Landroid/os/Parcel;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 348
    :cond_2
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 349
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 351
    return-void

    .line 319
    :cond_3
    const/4 v2, 0x0

    :try_start_1
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 348
    :catchall_0
    move-exception v2

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 349
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    throw v2

    .line 326
    :cond_4
    const/4 v2, 0x0

    :try_start_2
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_1

    .line 333
    :cond_5
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2
.end method

.method public asBinder()Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 241
    iget-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    return-object v0
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 245
    const-string v0, "com.qualcomm.qti.rcsimssettings.IQrcsImsSettingsListener"

    return-object v0
.end method
