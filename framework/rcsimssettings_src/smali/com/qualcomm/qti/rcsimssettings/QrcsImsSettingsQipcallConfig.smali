.class public Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;
.super Ljava/lang/Object;
.source "QrcsImsSettingsQipcallConfig.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private bMobile_data_enabled:Z

.field private bMobile_data_enabled_valid:Z

.field private bVolte_enabled:Z

.field private bVolte_enabled_valid:Z

.field private bVt_calling_enabled:Z

.field private bVt_calling_enabled_valid:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 273
    new-instance v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig$1;

    invoke-direct {v0}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig$1;-><init>()V

    sput-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 240
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 241
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 286
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 287
    invoke-virtual {p0, p1}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;->readFromParcel(Landroid/os/Parcel;)V

    .line 288
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig$1;

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public static getQrcsImsSettingsQipcallConfigInstance()Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;
    .locals 1

    .prologue
    .line 250
    new-instance v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;

    invoke-direct {v0}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;-><init>()V

    return-object v0
.end method

.method private writeToParcel(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "out"    # Landroid/os/Parcel;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 265
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;->bVt_calling_enabled_valid:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 266
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;->bVt_calling_enabled:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 267
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;->bMobile_data_enabled_valid:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 268
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;->bMobile_data_enabled:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 269
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;->bVolte_enabled_valid:Z

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 270
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;->bVolte_enabled:Z

    if-eqz v0, :cond_5

    :goto_5
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 271
    return-void

    :cond_0
    move v0, v2

    .line 265
    goto :goto_0

    :cond_1
    move v0, v2

    .line 266
    goto :goto_1

    :cond_2
    move v0, v2

    .line 267
    goto :goto_2

    :cond_3
    move v0, v2

    .line 268
    goto :goto_3

    :cond_4
    move v0, v2

    .line 269
    goto :goto_4

    :cond_5
    move v1, v2

    .line 270
    goto :goto_5
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 255
    const/4 v0, 0x0

    return v0
.end method

.method public isMobile_data_enabled()Z
    .locals 1

    .prologue
    .line 152
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;->bMobile_data_enabled:Z

    return v0
.end method

.method public isMobile_data_enabled_valid()Z
    .locals 1

    .prologue
    .line 123
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;->bMobile_data_enabled_valid:Z

    return v0
.end method

.method public isVolte_enabled()Z
    .locals 1

    .prologue
    .line 214
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;->bVolte_enabled:Z

    return v0
.end method

.method public isVolte_enabled_valid()Z
    .locals 1

    .prologue
    .line 184
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;->bVolte_enabled_valid:Z

    return v0
.end method

.method public isVt_calling_enabled()Z
    .locals 1

    .prologue
    .line 91
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;->bVt_calling_enabled:Z

    return v0
.end method

.method public isVt_calling_enabled_valid()Z
    .locals 1

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;->bVt_calling_enabled_valid:Z

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 291
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;->bVt_calling_enabled_valid:Z

    .line 292
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;->bVt_calling_enabled:Z

    .line 293
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;->bMobile_data_enabled_valid:Z

    .line 294
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;->bMobile_data_enabled:Z

    .line 295
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    iput-boolean v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;->bVolte_enabled_valid:Z

    .line 296
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_5

    :goto_5
    iput-boolean v1, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;->bVolte_enabled:Z

    .line 297
    return-void

    :cond_0
    move v0, v2

    .line 291
    goto :goto_0

    :cond_1
    move v0, v2

    .line 292
    goto :goto_1

    :cond_2
    move v0, v2

    .line 293
    goto :goto_2

    :cond_3
    move v0, v2

    .line 294
    goto :goto_3

    :cond_4
    move v0, v2

    .line 295
    goto :goto_4

    :cond_5
    move v1, v2

    .line 296
    goto :goto_5
.end method

.method public setMobile_data_enabled(Z)V
    .locals 1
    .param p1, "bMobile_data_enabled"    # Z

    .prologue
    .line 167
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;->setMobile_data_enabled_valid(Z)V

    .line 168
    iput-boolean p1, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;->bMobile_data_enabled:Z

    .line 169
    return-void
.end method

.method public setMobile_data_enabled_valid(Z)V
    .locals 0
    .param p1, "bMobile_data_enabled_valid"    # Z

    .prologue
    .line 138
    iput-boolean p1, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;->bMobile_data_enabled_valid:Z

    .line 139
    return-void
.end method

.method public setVolte_enabled(Z)V
    .locals 1
    .param p1, "bVolte_enabled"    # Z

    .prologue
    .line 229
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;->setVolte_enabled_valid(Z)V

    .line 230
    iput-boolean p1, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;->bVolte_enabled:Z

    .line 231
    return-void
.end method

.method public setVolte_enabled_valid(Z)V
    .locals 0
    .param p1, "volte_enabled_valid"    # Z

    .prologue
    .line 199
    iput-boolean p1, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;->bVolte_enabled_valid:Z

    .line 200
    return-void
.end method

.method public setVt_calling_enabled(Z)V
    .locals 1
    .param p1, "bVt_calling_enabled"    # Z

    .prologue
    .line 106
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;->setVt_calling_enabled_valid(Z)V

    .line 107
    iput-boolean p1, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;->bVt_calling_enabled:Z

    .line 108
    return-void
.end method

.method public setVt_calling_enabled_valid(Z)V
    .locals 0
    .param p1, "bVt_calling_enabled_valid"    # Z

    .prologue
    .line 76
    iput-boolean p1, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;->bVt_calling_enabled_valid:Z

    .line 77
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 259
    invoke-direct {p0, p1}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;->writeToParcel(Landroid/os/Parcel;)V

    .line 261
    return-void
.end method
