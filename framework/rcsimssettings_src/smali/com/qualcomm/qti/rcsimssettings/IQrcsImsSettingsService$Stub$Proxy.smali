.class Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsService$Stub$Proxy;
.super Ljava/lang/Object;
.source "IQrcsImsSettingsService.java"

# interfaces
.implements Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsService$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .locals 0
    .param p1, "remote"    # Landroid/os/IBinder;

    .prologue
    .line 260
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 261
    iput-object p1, p0, Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    .line 262
    return-void
.end method


# virtual methods
.method public QrcsImsSettings_Deregister(Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;)Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
    .locals 6
    .param p1, "client_handle"    # Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 329
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 330
    .local v0, "_data":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 333
    .local v1, "_reply":Landroid/os/Parcel;
    :try_start_0
    const-string v3, "com.qualcomm.qti.rcsimssettings.IQrcsImsSettingsService"

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 334
    if-eqz p1, :cond_1

    .line 335
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 336
    const/4 v3, 0x0

    invoke-virtual {p1, v0, v3}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;->writeToParcel(Landroid/os/Parcel;I)V

    .line 341
    :goto_0
    iget-object v3, p0, Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    const/4 v4, 0x2

    const/4 v5, 0x0

    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 342
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    .line 343
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_2

    .line 344
    sget-object v3, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;

    .line 349
    .local v2, "_result":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
    :goto_1
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_0

    .line 350
    invoke-virtual {p1, v1}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;->readFromParcel(Landroid/os/Parcel;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 354
    :cond_0
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 355
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 357
    return-object v2

    .line 339
    .end local v2    # "_result":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
    :cond_1
    const/4 v3, 0x0

    :try_start_1
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 354
    :catchall_0
    move-exception v3

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 355
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    throw v3

    .line 347
    :cond_2
    const/4 v2, 0x0

    .restart local v2    # "_result":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
    goto :goto_1
.end method

.method public QrcsImsSettings_GetPresenceConfig(Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;)Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
    .locals 6
    .param p1, "client_handle"    # Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 469
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 470
    .local v0, "_data":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 473
    .local v1, "_reply":Landroid/os/Parcel;
    :try_start_0
    const-string v3, "com.qualcomm.qti.rcsimssettings.IQrcsImsSettingsService"

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 474
    if-eqz p1, :cond_1

    .line 475
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 476
    const/4 v3, 0x0

    invoke-virtual {p1, v0, v3}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;->writeToParcel(Landroid/os/Parcel;I)V

    .line 481
    :goto_0
    iget-object v3, p0, Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    const/4 v4, 0x5

    const/4 v5, 0x0

    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 482
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    .line 483
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_2

    .line 484
    sget-object v3, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;

    .line 489
    .local v2, "_result":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
    :goto_1
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_0

    .line 490
    invoke-virtual {p1, v1}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;->readFromParcel(Landroid/os/Parcel;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 494
    :cond_0
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 495
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 497
    return-object v2

    .line 479
    .end local v2    # "_result":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
    :cond_1
    const/4 v3, 0x0

    :try_start_1
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 494
    :catchall_0
    move-exception v3

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 495
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    throw v3

    .line 487
    :cond_2
    const/4 v2, 0x0

    .restart local v2    # "_result":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
    goto :goto_1
.end method

.method public QrcsImsSettings_GetQipcallConfig(Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;)Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
    .locals 6
    .param p1, "client_handle"    # Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 372
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 373
    .local v0, "_data":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 376
    .local v1, "_reply":Landroid/os/Parcel;
    :try_start_0
    const-string v3, "com.qualcomm.qti.rcsimssettings.IQrcsImsSettingsService"

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 377
    if-eqz p1, :cond_1

    .line 378
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 379
    const/4 v3, 0x0

    invoke-virtual {p1, v0, v3}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;->writeToParcel(Landroid/os/Parcel;I)V

    .line 384
    :goto_0
    iget-object v3, p0, Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    const/4 v4, 0x3

    const/4 v5, 0x0

    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 385
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    .line 386
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_2

    .line 387
    sget-object v3, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;

    .line 392
    .local v2, "_result":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
    :goto_1
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_0

    .line 393
    invoke-virtual {p1, v1}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;->readFromParcel(Landroid/os/Parcel;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 397
    :cond_0
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 398
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 400
    return-object v2

    .line 382
    .end local v2    # "_result":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
    :cond_1
    const/4 v3, 0x0

    :try_start_1
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 397
    :catchall_0
    move-exception v3

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 398
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    throw v3

    .line 390
    :cond_2
    const/4 v2, 0x0

    .restart local v2    # "_result":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
    goto :goto_1
.end method

.method public QrcsImsSettings_Register(Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsListener;Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;)Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
    .locals 6
    .param p1, "client_callback"    # Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsListener;
    .param p2, "client_handle"    # Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 285
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 286
    .local v0, "_data":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 289
    .local v1, "_reply":Landroid/os/Parcel;
    :try_start_0
    const-string v3, "com.qualcomm.qti.rcsimssettings.IQrcsImsSettingsService"

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 290
    if-eqz p1, :cond_1

    invoke-interface {p1}, Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsListener;->asBinder()Landroid/os/IBinder;

    move-result-object v3

    :goto_0
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    .line 291
    if-eqz p2, :cond_2

    .line 292
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 293
    const/4 v3, 0x0

    invoke-virtual {p2, v0, v3}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;->writeToParcel(Landroid/os/Parcel;I)V

    .line 298
    :goto_1
    iget-object v3, p0, Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 299
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    .line 300
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_3

    .line 301
    sget-object v3, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;

    .line 306
    .local v2, "_result":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
    :goto_2
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_0

    .line 307
    invoke-virtual {p2, v1}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;->readFromParcel(Landroid/os/Parcel;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 311
    :cond_0
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 312
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 314
    return-object v2

    .line 290
    .end local v2    # "_result":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
    :cond_1
    const/4 v3, 0x0

    goto :goto_0

    .line 296
    :cond_2
    const/4 v3, 0x0

    :try_start_1
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 311
    :catchall_0
    move-exception v3

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 312
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    throw v3

    .line 304
    :cond_3
    const/4 v2, 0x0

    .restart local v2    # "_result":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
    goto :goto_2
.end method

.method public QrcsImsSettings_SetPresenceConfig(Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;)Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
    .locals 6
    .param p1, "client_handle"    # Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;
    .param p2, "presence_config"    # Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 513
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 514
    .local v0, "_data":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 517
    .local v1, "_reply":Landroid/os/Parcel;
    :try_start_0
    const-string v3, "com.qualcomm.qti.rcsimssettings.IQrcsImsSettingsService"

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 518
    if-eqz p1, :cond_2

    .line 519
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 520
    const/4 v3, 0x0

    invoke-virtual {p1, v0, v3}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;->writeToParcel(Landroid/os/Parcel;I)V

    .line 525
    :goto_0
    if-eqz p2, :cond_3

    .line 526
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 527
    const/4 v3, 0x0

    invoke-virtual {p2, v0, v3}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->writeToParcel(Landroid/os/Parcel;I)V

    .line 532
    :goto_1
    iget-object v3, p0, Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    const/4 v4, 0x6

    const/4 v5, 0x0

    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 533
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    .line 534
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_4

    .line 535
    sget-object v3, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;

    .line 540
    .local v2, "_result":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
    :goto_2
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_0

    .line 541
    invoke-virtual {p1, v1}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;->readFromParcel(Landroid/os/Parcel;)V

    .line 543
    :cond_0
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_1

    .line 544
    invoke-virtual {p2, v1}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->readFromParcel(Landroid/os/Parcel;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 548
    :cond_1
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 549
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 551
    return-object v2

    .line 523
    .end local v2    # "_result":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
    :cond_2
    const/4 v3, 0x0

    :try_start_1
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 548
    :catchall_0
    move-exception v3

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 549
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    throw v3

    .line 530
    :cond_3
    const/4 v3, 0x0

    :try_start_2
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 538
    :cond_4
    const/4 v2, 0x0

    .restart local v2    # "_result":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
    goto :goto_2
.end method

.method public QrcsImsSettings_SetQipcallConfig(Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;)Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
    .locals 6
    .param p1, "client_handle"    # Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;
    .param p2, "qipcall_config"    # Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 416
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 417
    .local v0, "_data":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 420
    .local v1, "_reply":Landroid/os/Parcel;
    :try_start_0
    const-string v3, "com.qualcomm.qti.rcsimssettings.IQrcsImsSettingsService"

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 421
    if-eqz p1, :cond_2

    .line 422
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 423
    const/4 v3, 0x0

    invoke-virtual {p1, v0, v3}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;->writeToParcel(Landroid/os/Parcel;I)V

    .line 428
    :goto_0
    if-eqz p2, :cond_3

    .line 429
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 430
    const/4 v3, 0x0

    invoke-virtual {p2, v0, v3}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;->writeToParcel(Landroid/os/Parcel;I)V

    .line 435
    :goto_1
    iget-object v3, p0, Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    const/4 v4, 0x4

    const/4 v5, 0x0

    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 436
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    .line 437
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_4

    .line 438
    sget-object v3, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v3, v1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;

    .line 443
    .local v2, "_result":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
    :goto_2
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_0

    .line 444
    invoke-virtual {p1, v1}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;->readFromParcel(Landroid/os/Parcel;)V

    .line 446
    :cond_0
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_1

    .line 447
    invoke-virtual {p2, v1}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;->readFromParcel(Landroid/os/Parcel;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 451
    :cond_1
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 452
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 454
    return-object v2

    .line 426
    .end local v2    # "_result":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
    :cond_2
    const/4 v3, 0x0

    :try_start_1
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 451
    :catchall_0
    move-exception v3

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 452
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    throw v3

    .line 433
    :cond_3
    const/4 v3, 0x0

    :try_start_2
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 441
    :cond_4
    const/4 v2, 0x0

    .restart local v2    # "_result":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
    goto :goto_2
.end method

.method public asBinder()Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 265
    iget-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    return-object v0
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 269
    const-string v0, "com.qualcomm.qti.rcsimssettings.IQrcsImsSettingsService"

    return-object v0
.end method

.method public getLibLoadStatus()Z
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 564
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 565
    .local v0, "_data":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 568
    .local v1, "_reply":Landroid/os/Parcel;
    :try_start_0
    const-string v3, "com.qualcomm.qti.rcsimssettings.IQrcsImsSettingsService"

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    .line 569
    iget-object v3, p0, Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsService$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    const/4 v4, 0x7

    const/4 v5, 0x0

    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    .line 570
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    .line 571
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x1

    .line 574
    .local v2, "_result":Z
    :cond_0
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 575
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 577
    return v2

    .line 574
    .end local v2    # "_result":Z
    :catchall_0
    move-exception v3

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 575
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    throw v3
.end method
