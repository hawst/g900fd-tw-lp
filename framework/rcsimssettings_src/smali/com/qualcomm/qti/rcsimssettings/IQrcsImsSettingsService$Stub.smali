.class public abstract Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsService$Stub;
.super Landroid/os/Binder;
.source "IQrcsImsSettingsService.java"

# interfaces
.implements Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsService$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.qualcomm.qti.rcsimssettings.IQrcsImsSettingsService"

.field static final TRANSACTION_QrcsImsSettings_Deregister:I = 0x2

.field static final TRANSACTION_QrcsImsSettings_GetPresenceConfig:I = 0x5

.field static final TRANSACTION_QrcsImsSettings_GetQipcallConfig:I = 0x3

.field static final TRANSACTION_QrcsImsSettings_Register:I = 0x1

.field static final TRANSACTION_QrcsImsSettings_SetPresenceConfig:I = 0x6

.field static final TRANSACTION_QrcsImsSettings_SetQipcallConfig:I = 0x4

.field static final TRANSACTION_getLibLoadStatus:I = 0x7


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 17
    const-string v0, "com.qualcomm.qti.rcsimssettings.IQrcsImsSettingsService"

    invoke-virtual {p0, p0, v0}, Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 18
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsService;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 25
    if-nez p0, :cond_0

    .line 26
    const/4 v0, 0x0

    .line 32
    :goto_0
    return-object v0

    .line 28
    :cond_0
    const-string v1, "com.qualcomm.qti.rcsimssettings.IQrcsImsSettingsService"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 29
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsService;

    if-eqz v1, :cond_1

    .line 30
    check-cast v0, Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsService;

    goto :goto_0

    .line 32
    :cond_1
    new-instance v0, Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsService$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 36
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 6
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 40
    sparse-switch p1, :sswitch_data_0

    .line 254
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v4

    :goto_0
    return v4

    .line 44
    :sswitch_0
    const-string v3, "com.qualcomm.qti.rcsimssettings.IQrcsImsSettingsService"

    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 49
    :sswitch_1
    const-string v5, "com.qualcomm.qti.rcsimssettings.IQrcsImsSettingsService"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 51
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v5

    invoke-static {v5}, Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsListener;

    move-result-object v0

    .line 53
    .local v0, "_arg0":Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsListener;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-eqz v5, :cond_0

    .line 54
    sget-object v5, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;

    .line 59
    .local v1, "_arg1":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;
    :goto_1
    invoke-virtual {p0, v0, v1}, Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsService$Stub;->QrcsImsSettings_Register(Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsListener;Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;)Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;

    move-result-object v2

    .line 60
    .local v2, "_result":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 61
    if-eqz v2, :cond_1

    .line 62
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 63
    invoke-virtual {v2, p3, v4}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;->writeToParcel(Landroid/os/Parcel;I)V

    .line 68
    :goto_2
    if-eqz v1, :cond_2

    .line 69
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 70
    invoke-virtual {v1, p3, v4}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    .line 57
    .end local v1    # "_arg1":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;
    .end local v2    # "_result":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
    :cond_0
    const/4 v1, 0x0

    .restart local v1    # "_arg1":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;
    goto :goto_1

    .line 66
    .restart local v2    # "_result":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
    :cond_1
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_2

    .line 73
    :cond_2
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 79
    .end local v0    # "_arg0":Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsListener;
    .end local v1    # "_arg1":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;
    .end local v2    # "_result":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
    :sswitch_2
    const-string v5, "com.qualcomm.qti.rcsimssettings.IQrcsImsSettingsService"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 81
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-eqz v5, :cond_3

    .line 82
    sget-object v5, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;

    .line 87
    .local v0, "_arg0":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;
    :goto_3
    invoke-virtual {p0, v0}, Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsService$Stub;->QrcsImsSettings_Deregister(Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;)Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;

    move-result-object v2

    .line 88
    .restart local v2    # "_result":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 89
    if-eqz v2, :cond_4

    .line 90
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 91
    invoke-virtual {v2, p3, v4}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;->writeToParcel(Landroid/os/Parcel;I)V

    .line 96
    :goto_4
    if-eqz v0, :cond_5

    .line 97
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 98
    invoke-virtual {v0, p3, v4}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    .line 85
    .end local v0    # "_arg0":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;
    .end local v2    # "_result":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
    :cond_3
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;
    goto :goto_3

    .line 94
    .restart local v2    # "_result":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
    :cond_4
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_4

    .line 101
    :cond_5
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 107
    .end local v0    # "_arg0":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;
    .end local v2    # "_result":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
    :sswitch_3
    const-string v5, "com.qualcomm.qti.rcsimssettings.IQrcsImsSettingsService"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 109
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-eqz v5, :cond_6

    .line 110
    sget-object v5, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;

    .line 115
    .restart local v0    # "_arg0":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;
    :goto_5
    invoke-virtual {p0, v0}, Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsService$Stub;->QrcsImsSettings_GetQipcallConfig(Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;)Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;

    move-result-object v2

    .line 116
    .restart local v2    # "_result":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 117
    if-eqz v2, :cond_7

    .line 118
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 119
    invoke-virtual {v2, p3, v4}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;->writeToParcel(Landroid/os/Parcel;I)V

    .line 124
    :goto_6
    if-eqz v0, :cond_8

    .line 125
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 126
    invoke-virtual {v0, p3, v4}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 113
    .end local v0    # "_arg0":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;
    .end local v2    # "_result":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
    :cond_6
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;
    goto :goto_5

    .line 122
    .restart local v2    # "_result":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
    :cond_7
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_6

    .line 129
    :cond_8
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 135
    .end local v0    # "_arg0":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;
    .end local v2    # "_result":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
    :sswitch_4
    const-string v5, "com.qualcomm.qti.rcsimssettings.IQrcsImsSettingsService"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 137
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-eqz v5, :cond_9

    .line 138
    sget-object v5, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;

    .line 144
    .restart local v0    # "_arg0":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;
    :goto_7
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-eqz v5, :cond_a

    .line 145
    sget-object v5, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;

    .line 150
    .local v1, "_arg1":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;
    :goto_8
    invoke-virtual {p0, v0, v1}, Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsService$Stub;->QrcsImsSettings_SetQipcallConfig(Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;)Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;

    move-result-object v2

    .line 151
    .restart local v2    # "_result":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 152
    if-eqz v2, :cond_b

    .line 153
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 154
    invoke-virtual {v2, p3, v4}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;->writeToParcel(Landroid/os/Parcel;I)V

    .line 159
    :goto_9
    if-eqz v0, :cond_c

    .line 160
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 161
    invoke-virtual {v0, p3, v4}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;->writeToParcel(Landroid/os/Parcel;I)V

    .line 166
    :goto_a
    if-eqz v1, :cond_d

    .line 167
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 168
    invoke-virtual {v1, p3, v4}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 141
    .end local v0    # "_arg0":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;
    .end local v1    # "_arg1":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;
    .end local v2    # "_result":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
    :cond_9
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;
    goto :goto_7

    .line 148
    :cond_a
    const/4 v1, 0x0

    .restart local v1    # "_arg1":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;
    goto :goto_8

    .line 157
    .restart local v2    # "_result":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
    :cond_b
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_9

    .line 164
    :cond_c
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_a

    .line 171
    :cond_d
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 177
    .end local v0    # "_arg0":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;
    .end local v1    # "_arg1":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;
    .end local v2    # "_result":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
    :sswitch_5
    const-string v5, "com.qualcomm.qti.rcsimssettings.IQrcsImsSettingsService"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 179
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-eqz v5, :cond_e

    .line 180
    sget-object v5, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;

    .line 185
    .restart local v0    # "_arg0":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;
    :goto_b
    invoke-virtual {p0, v0}, Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsService$Stub;->QrcsImsSettings_GetPresenceConfig(Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;)Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;

    move-result-object v2

    .line 186
    .restart local v2    # "_result":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 187
    if-eqz v2, :cond_f

    .line 188
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 189
    invoke-virtual {v2, p3, v4}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;->writeToParcel(Landroid/os/Parcel;I)V

    .line 194
    :goto_c
    if-eqz v0, :cond_10

    .line 195
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 196
    invoke-virtual {v0, p3, v4}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 183
    .end local v0    # "_arg0":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;
    .end local v2    # "_result":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
    :cond_e
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;
    goto :goto_b

    .line 192
    .restart local v2    # "_result":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
    :cond_f
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_c

    .line 199
    :cond_10
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 205
    .end local v0    # "_arg0":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;
    .end local v2    # "_result":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
    :sswitch_6
    const-string v5, "com.qualcomm.qti.rcsimssettings.IQrcsImsSettingsService"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 207
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-eqz v5, :cond_11

    .line 208
    sget-object v5, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;

    .line 214
    .restart local v0    # "_arg0":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;
    :goto_d
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-eqz v5, :cond_12

    .line 215
    sget-object v5, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;

    .line 220
    .local v1, "_arg1":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;
    :goto_e
    invoke-virtual {p0, v0, v1}, Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsService$Stub;->QrcsImsSettings_SetPresenceConfig(Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;)Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;

    move-result-object v2

    .line 221
    .restart local v2    # "_result":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 222
    if-eqz v2, :cond_13

    .line 223
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 224
    invoke-virtual {v2, p3, v4}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;->writeToParcel(Landroid/os/Parcel;I)V

    .line 229
    :goto_f
    if-eqz v0, :cond_14

    .line 230
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 231
    invoke-virtual {v0, p3, v4}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;->writeToParcel(Landroid/os/Parcel;I)V

    .line 236
    :goto_10
    if-eqz v1, :cond_15

    .line 237
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 238
    invoke-virtual {v1, p3, v4}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 211
    .end local v0    # "_arg0":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;
    .end local v1    # "_arg1":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;
    .end local v2    # "_result":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
    :cond_11
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;
    goto :goto_d

    .line 218
    :cond_12
    const/4 v1, 0x0

    .restart local v1    # "_arg1":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;
    goto :goto_e

    .line 227
    .restart local v2    # "_result":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
    :cond_13
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_f

    .line 234
    :cond_14
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_10

    .line 241
    :cond_15
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 247
    .end local v0    # "_arg0":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;
    .end local v1    # "_arg1":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;
    .end local v2    # "_result":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
    :sswitch_7
    const-string v5, "com.qualcomm.qti.rcsimssettings.IQrcsImsSettingsService"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 248
    invoke-virtual {p0}, Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsService$Stub;->getLibLoadStatus()Z

    move-result v2

    .line 249
    .local v2, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 250
    if-eqz v2, :cond_16

    move v3, v4

    :cond_16
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 40
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
