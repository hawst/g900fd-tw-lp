.class Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsService$1;
.super Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsService$Stub;
.source "QrcsImsSettingsService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field nativeRcsImsSettings:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsService$NativeRcsImsSettings;

.field final synthetic this$0:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsService;


# direct methods
.method constructor <init>(Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsService;)V
    .locals 2

    .prologue
    .line 106
    iput-object p1, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsService$1;->this$0:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsService;

    invoke-direct {p0}, Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsService$Stub;-><init>()V

    .line 107
    new-instance v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsService$NativeRcsImsSettings;

    iget-object v1, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsService$1;->this$0:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsService;

    invoke-direct {v0, v1}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsService$NativeRcsImsSettings;-><init>(Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsService;)V

    iput-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsService$1;->nativeRcsImsSettings:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsService$NativeRcsImsSettings;

    return-void
.end method


# virtual methods
.method public QrcsImsSettings_Deregister(Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;)Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
    .locals 2
    .param p1, "client_handle"    # Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 159
    sget-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsService;->clientIDMap:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsService;->clientIDMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    if-lez v0, :cond_0

    sget-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsService;->clientIDMap:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;->getClientID()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 162
    const-string v0, "AIDL"

    const-string v1, "QrcsImsSettingsService : Before removing data to hasmap for setting Register"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    sget-object v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsService;->clientIDMap:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;->getClientID()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 165
    const-string v0, "AIDL"

    const-string v1, "QrcsImsSettingsService : after removing data from hasmap for setting Register"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 169
    :cond_0
    iget-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsService$1;->nativeRcsImsSettings:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsService$NativeRcsImsSettings;

    invoke-virtual {v0, p1}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsService$NativeRcsImsSettings;->QrcsImsSettings_Deregister(Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;)Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;

    move-result-object v0

    return-object v0
.end method

.method public QrcsImsSettings_GetPresenceConfig(Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;)Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
    .locals 1
    .param p1, "client_handle"    # Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 189
    iget-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsService$1;->nativeRcsImsSettings:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsService$NativeRcsImsSettings;

    invoke-virtual {v0, p1}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsService$NativeRcsImsSettings;->QrcsImsSettings_GetPresenceConfig(Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;)Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;

    move-result-object v0

    return-object v0
.end method

.method public QrcsImsSettings_GetQipcallConfig(Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;)Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
    .locals 1
    .param p1, "client_handle"    # Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 175
    iget-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsService$1;->nativeRcsImsSettings:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsService$NativeRcsImsSettings;

    invoke-virtual {v0, p1}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsService$NativeRcsImsSettings;->QrcsImsSettings_GetQipcallConfig(Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;)Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;

    move-result-object v0

    return-object v0
.end method

.method public QrcsImsSettings_Register(Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsListener;Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;)Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
    .locals 8
    .param p1, "client_callback"    # Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsListener;
    .param p2, "client_handle"    # Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 113
    sget-object v4, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsService;->clientIDMap:Ljava/util/HashMap;

    if-eqz v4, :cond_1

    sget-object v4, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsService;->clientIDMap:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->size()I

    move-result v4

    if-lez v4, :cond_1

    sget-object v4, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsService;->clientIDMap:Ljava/util/HashMap;

    invoke-virtual {p2}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;->getClientID()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 116
    const-string v4, "AIDL"

    const-string v5, "QrcsImsSettingsService : Before calling setting Deregister"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    sget-object v4, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsService;->clientIDMap:Ljava/util/HashMap;

    invoke-virtual {p2}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;->getClientID()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 118
    .local v0, "localClientHandle":J
    new-instance v3, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;

    invoke-direct {v3}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;-><init>()V

    .line 119
    .local v3, "locat_Client_handle":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;
    invoke-virtual {v3, v0, v1}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;->setQrcsImsSettingsIntType(J)V

    .line 120
    const-string v4, "AIDL"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "QrcsImsSettingsService : getQrcsImsSettingsIntType "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v3}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;->getQrcsImsSettingsIntType()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    invoke-virtual {p0, v3}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsService$1;->QrcsImsSettings_Deregister(Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;)Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;

    .line 123
    const-string v4, "AIDL"

    const-string v5, "QrcsImsSettingsService : after callingsetting Deregister"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 135
    .end local v0    # "localClientHandle":J
    .end local v3    # "locat_Client_handle":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;
    :goto_0
    iget-object v4, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsService$1;->nativeRcsImsSettings:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsService$NativeRcsImsSettings;

    invoke-virtual {v4, p1, p2}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsService$NativeRcsImsSettings;->QrcsImsSettings_Register(Lcom/qualcomm/qti/rcsimssettings/IQrcsImsSettingsListener;Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;)Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;

    move-result-object v2

    .line 138
    .local v2, "localQrcsImsSettingsError":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
    invoke-virtual {v2}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;->getQrcsImsSettingsError()I

    move-result v4

    if-nez v4, :cond_0

    .line 140
    sget-object v4, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsService;->clientIDMap:Ljava/util/HashMap;

    if-eqz v4, :cond_0

    .line 142
    const-string v4, "AIDL"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "QrcsImsSettingsService : Before adding data to hasmap for setting Register getQrcsImsSettingsIntType "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p2}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;->getQrcsImsSettingsIntType()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    sget-object v4, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsService;->clientIDMap:Ljava/util/HashMap;

    invoke-virtual {p2}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;->getClientID()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {p2}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;->getQrcsImsSettingsIntType()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 147
    const-string v4, "AIDL"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "QrcsImsSettingsService : after adding data to hasmap for setting Register getClientID"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p2}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;->getClientID()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    const-string v4, "AIDL"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "QrcsImsSettingsService : after adding data to hasmap for setting Register size"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsService;->clientIDMap:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    :cond_0
    return-object v2

    .line 127
    .end local v2    # "localQrcsImsSettingsError":Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
    :cond_1
    const-string v4, "AIDL"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "QrcsImsSettingsService : else Before calling setting Deregister"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p2}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;->getClientID()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    sget-object v4, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsService;->clientIDMap:Ljava/util/HashMap;

    if-eqz v4, :cond_2

    sget-object v4, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsService;->clientIDMap:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->size()I

    move-result v4

    if-lez v4, :cond_2

    .line 130
    const-string v4, "AIDL"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "QrcsImsSettingsService : size "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsService;->clientIDMap:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    const-string v4, "AIDL"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "QrcsImsSettingsService : containsKey "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsService;->clientIDMap:Ljava/util/HashMap;

    invoke-virtual {p2}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;->getClientID()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    :cond_2
    const-string v4, "AIDL"

    const-string v5, "QrcsImsSettingsService : else After calling setting Deregister"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public QrcsImsSettings_SetPresenceConfig(Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;)Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
    .locals 1
    .param p1, "client_handle"    # Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;
    .param p2, "presence_config"    # Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 197
    iget-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsService$1;->nativeRcsImsSettings:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsService$NativeRcsImsSettings;

    invoke-virtual {v0, p1, p2}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsService$NativeRcsImsSettings;->QrcsImsSettings_SetPresenceConfig(Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsPresenceConfig;)Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;

    move-result-object v0

    return-object v0
.end method

.method public QrcsImsSettings_SetQipcallConfig(Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;)Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;
    .locals 1
    .param p1, "client_handle"    # Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;
    .param p2, "qipcall_config"    # Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 183
    iget-object v0, p0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsService$1;->nativeRcsImsSettings:Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsService$NativeRcsImsSettings;

    invoke-virtual {v0, p1, p2}, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsService$NativeRcsImsSettings;->QrcsImsSettings_SetQipcallConfig(Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsIntType;Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsQipcallConfig;)Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsError;

    move-result-object v0

    return-object v0
.end method

.method public getLibLoadStatus()Z
    .locals 1

    .prologue
    .line 203
    sget-boolean v0, Lcom/qualcomm/qti/rcsimssettings/QrcsImsSettingsService;->isLibsLoaded:Z

    return v0
.end method
