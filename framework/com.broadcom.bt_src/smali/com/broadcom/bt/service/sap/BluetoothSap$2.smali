.class Lcom/broadcom/bt/service/sap/BluetoothSap$2;
.super Ljava/lang/Object;
.source "BluetoothSap.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/broadcom/bt/service/sap/BluetoothSap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/broadcom/bt/service/sap/BluetoothSap;


# direct methods
.method constructor <init>(Lcom/broadcom/bt/service/sap/BluetoothSap;)V
    .locals 0

    .prologue
    .line 316
    iput-object p1, p0, Lcom/broadcom/bt/service/sap/BluetoothSap$2;->this$0:Lcom/broadcom/bt/service/sap/BluetoothSap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3
    .param p1, "className"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 318
    const-string v0, "Bluetoothsap"

    const-string v1, "BluetoothSAP Proxy object connected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 319
    iget-object v0, p0, Lcom/broadcom/bt/service/sap/BluetoothSap$2;->this$0:Lcom/broadcom/bt/service/sap/BluetoothSap;

    invoke-static {p2}, Lcom/broadcom/bt/service/sap/IBluetoothSap$Stub;->asInterface(Landroid/os/IBinder;)Lcom/broadcom/bt/service/sap/IBluetoothSap;

    move-result-object v1

    # setter for: Lcom/broadcom/bt/service/sap/BluetoothSap;->mSapService:Lcom/broadcom/bt/service/sap/IBluetoothSap;
    invoke-static {v0, v1}, Lcom/broadcom/bt/service/sap/BluetoothSap;->access$102(Lcom/broadcom/bt/service/sap/BluetoothSap;Lcom/broadcom/bt/service/sap/IBluetoothSap;)Lcom/broadcom/bt/service/sap/IBluetoothSap;

    .line 321
    iget-object v0, p0, Lcom/broadcom/bt/service/sap/BluetoothSap$2;->this$0:Lcom/broadcom/bt/service/sap/BluetoothSap;

    # getter for: Lcom/broadcom/bt/service/sap/BluetoothSap;->mServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;
    invoke-static {v0}, Lcom/broadcom/bt/service/sap/BluetoothSap;->access$300(Lcom/broadcom/bt/service/sap/BluetoothSap;)Landroid/bluetooth/BluetoothProfile$ServiceListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 322
    iget-object v0, p0, Lcom/broadcom/bt/service/sap/BluetoothSap$2;->this$0:Lcom/broadcom/bt/service/sap/BluetoothSap;

    # getter for: Lcom/broadcom/bt/service/sap/BluetoothSap;->mServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;
    invoke-static {v0}, Lcom/broadcom/bt/service/sap/BluetoothSap;->access$300(Lcom/broadcom/bt/service/sap/BluetoothSap;)Landroid/bluetooth/BluetoothProfile$ServiceListener;

    move-result-object v0

    const/16 v1, 0x66

    iget-object v2, p0, Lcom/broadcom/bt/service/sap/BluetoothSap$2;->this$0:Lcom/broadcom/bt/service/sap/BluetoothSap;

    invoke-interface {v0, v1, v2}, Landroid/bluetooth/BluetoothProfile$ServiceListener;->onServiceConnected(ILandroid/bluetooth/BluetoothProfile;)V

    .line 325
    :cond_0
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 4
    .param p1, "className"    # Landroid/content/ComponentName;

    .prologue
    .line 327
    const-string v0, "Bluetoothsap"

    const-string v1, "BluetoothSAP Proxy object disconnected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 328
    iget-object v0, p0, Lcom/broadcom/bt/service/sap/BluetoothSap$2;->this$0:Lcom/broadcom/bt/service/sap/BluetoothSap;

    const/4 v1, 0x0

    # setter for: Lcom/broadcom/bt/service/sap/BluetoothSap;->mSapService:Lcom/broadcom/bt/service/sap/IBluetoothSap;
    invoke-static {v0, v1}, Lcom/broadcom/bt/service/sap/BluetoothSap;->access$102(Lcom/broadcom/bt/service/sap/BluetoothSap;Lcom/broadcom/bt/service/sap/IBluetoothSap;)Lcom/broadcom/bt/service/sap/IBluetoothSap;

    .line 329
    iget-object v0, p0, Lcom/broadcom/bt/service/sap/BluetoothSap$2;->this$0:Lcom/broadcom/bt/service/sap/BluetoothSap;

    # getter for: Lcom/broadcom/bt/service/sap/BluetoothSap;->mServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;
    invoke-static {v0}, Lcom/broadcom/bt/service/sap/BluetoothSap;->access$300(Lcom/broadcom/bt/service/sap/BluetoothSap;)Landroid/bluetooth/BluetoothProfile$ServiceListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 330
    iget-object v0, p0, Lcom/broadcom/bt/service/sap/BluetoothSap$2;->this$0:Lcom/broadcom/bt/service/sap/BluetoothSap;

    # getter for: Lcom/broadcom/bt/service/sap/BluetoothSap;->mServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;
    invoke-static {v0}, Lcom/broadcom/bt/service/sap/BluetoothSap;->access$300(Lcom/broadcom/bt/service/sap/BluetoothSap;)Landroid/bluetooth/BluetoothProfile$ServiceListener;

    move-result-object v0

    const/16 v1, 0x66

    invoke-interface {v0, v1}, Landroid/bluetooth/BluetoothProfile$ServiceListener;->onServiceDisconnected(I)V

    .line 332
    :cond_0
    iget-object v0, p0, Lcom/broadcom/bt/service/sap/BluetoothSap$2;->this$0:Lcom/broadcom/bt/service/sap/BluetoothSap;

    # getter for: Lcom/broadcom/bt/service/sap/BluetoothSap;->mAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v0}, Lcom/broadcom/bt/service/sap/BluetoothSap;->access$400(Lcom/broadcom/bt/service/sap/BluetoothSap;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getState()I

    move-result v0

    const/16 v1, 0xc

    if-ne v0, v1, :cond_1

    .line 333
    const-string v0, "Bluetoothsap"

    const-string v1, "BluetoothSAP Try Rebinding back to SAP service"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 334
    iget-object v0, p0, Lcom/broadcom/bt/service/sap/BluetoothSap$2;->this$0:Lcom/broadcom/bt/service/sap/BluetoothSap;

    # getter for: Lcom/broadcom/bt/service/sap/BluetoothSap;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/broadcom/bt/service/sap/BluetoothSap;->access$200(Lcom/broadcom/bt/service/sap/BluetoothSap;)Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/broadcom/bt/service/sap/IBluetoothSap;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/broadcom/bt/service/sap/BluetoothSap$2;->this$0:Lcom/broadcom/bt/service/sap/BluetoothSap;

    # getter for: Lcom/broadcom/bt/service/sap/BluetoothSap;->mConnection:Landroid/content/ServiceConnection;
    invoke-static {v2}, Lcom/broadcom/bt/service/sap/BluetoothSap;->access$000(Lcom/broadcom/bt/service/sap/BluetoothSap;)Landroid/content/ServiceConnection;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 336
    const-string v0, "Bluetoothsap"

    const-string v1, "Could not bind to Bluetooth SAP Service"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 339
    :cond_1
    return-void
.end method
