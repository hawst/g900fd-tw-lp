.class public final Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;
.super Ljava/lang/Object;
.source "BluetoothRadioManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$BluetoothGattServiceStateChangeCallback;,
        Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$BluetoothRadioStateChangeCallback;
    }
.end annotation


# static fields
.field private static final DBG:Z = true

.field public static final RADIO_TYPE_ALL:I = 0xff

.field public static final RADIO_TYPE_ANT:I = 0x2

.field public static final RADIO_TYPE_BLE:I = 0x1

.field public static final RADIO_TYPE_FM:I = 0x0

.field public static final RADIO_TYPE_OTHER:I = 0x3

.field private static final TAG:Ljava/lang/String; = "BluetoothRadioManager"

.field private static sRadioManager:Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;


# instance fields
.field private final mAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private mAirplaneOn:Z

.field private mAntCount:I

.field private mBleCount:I

.field private mBluetoothGatt:Landroid/bluetooth/IBluetoothGatt;

.field private mFMCount:I

.field private mGattSrvStChangeCallback:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$BluetoothGattServiceStateChangeCallback;",
            ">;"
        }
    .end annotation
.end field

.field private final mManagerCallback:Landroid/bluetooth/IBluetoothManagerCallback;

.field private mManagerService:Landroid/bluetooth/IBluetoothManager;

.field private final mRadioMgrCallback:Landroid/bluetooth/IBluetoothRadioMgrCallback;

.field private final mRadioStChangeCallback:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$BluetoothRadioStateChangeCallback;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mRadioType:I

.field private mService:Landroid/bluetooth/IBluetooth;

.field private mUUID:Landroid/os/ParcelUuid;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 129
    const/4 v0, 0x0

    sput-object v0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->sRadioManager:Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    return-void
.end method

.method constructor <init>(Landroid/bluetooth/IBluetoothManager;)V
    .locals 3
    .param p1, "managerService"    # Landroid/bluetooth/IBluetoothManager;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 158
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 117
    iput v1, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mFMCount:I

    .line 118
    iput v1, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mBleCount:I

    .line 119
    iput v1, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mAntCount:I

    .line 121
    iput-boolean v1, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mAirplaneOn:Z

    .line 130
    iput-object v2, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mManagerService:Landroid/bluetooth/IBluetoothManager;

    .line 132
    iput-object v2, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mBluetoothGatt:Landroid/bluetooth/IBluetoothGatt;

    .line 453
    new-instance v1, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$1;

    invoke-direct {v1, p0}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$1;-><init>(Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;)V

    iput-object v1, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mManagerCallback:Landroid/bluetooth/IBluetoothManagerCallback;

    .line 515
    new-instance v1, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$2;

    invoke-direct {v1, p0}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$2;-><init>(Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;)V

    iput-object v1, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mRadioMgrCallback:Landroid/bluetooth/IBluetoothRadioMgrCallback;

    .line 159
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v1

    iput-object v1, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 160
    iput-object p1, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mManagerService:Landroid/bluetooth/IBluetoothManager;

    .line 161
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mRadioStChangeCallback:Ljava/util/Map;

    .line 162
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mGattSrvStChangeCallback:Ljava/util/ArrayList;

    .line 163
    new-instance v1, Landroid/os/ParcelUuid;

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/ParcelUuid;-><init>(Ljava/util/UUID;)V

    iput-object v1, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mUUID:Landroid/os/ParcelUuid;

    .line 164
    iget-object v1, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mManagerService:Landroid/bluetooth/IBluetoothManager;

    if-eqz v1, :cond_0

    .line 166
    :try_start_0
    iget-object v1, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mManagerService:Landroid/bluetooth/IBluetoothManager;

    iget-object v2, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mManagerCallback:Landroid/bluetooth/IBluetoothManagerCallback;

    invoke-interface {v1, v2}, Landroid/bluetooth/IBluetoothManager;->registerAdapter(Landroid/bluetooth/IBluetoothManagerCallback;)Landroid/bluetooth/IBluetooth;

    move-result-object v1

    iput-object v1, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mService:Landroid/bluetooth/IBluetooth;

    .line 167
    iget-object v1, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mManagerService:Landroid/bluetooth/IBluetoothManager;

    iget-object v2, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mRadioMgrCallback:Landroid/bluetooth/IBluetoothRadioMgrCallback;

    invoke-interface {v1, v2}, Landroid/bluetooth/IBluetoothManager;->registerRadioMgrCallback(Landroid/bluetooth/IBluetoothRadioMgrCallback;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 170
    :cond_0
    :goto_0
    return-void

    .line 168
    :catch_0
    move-exception v0

    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "BluetoothRadioManager"

    const-string v2, ""

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;)Landroid/bluetooth/IBluetooth;
    .locals 1
    .param p0, "x0"    # Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mService:Landroid/bluetooth/IBluetooth;

    return-object v0
.end method

.method static synthetic access$002(Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;Landroid/bluetooth/IBluetooth;)Landroid/bluetooth/IBluetooth;
    .locals 0
    .param p0, "x0"    # Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;
    .param p1, "x1"    # Landroid/bluetooth/IBluetooth;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mService:Landroid/bluetooth/IBluetooth;

    return-object p1
.end method

.method static synthetic access$1000(Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mGattSrvStChangeCallback:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$102(Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;
    .param p1, "x1"    # Z

    .prologue
    .line 83
    iput-boolean p1, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mAirplaneOn:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mRadioStChangeCallback:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;)Landroid/bluetooth/IBluetoothManager;
    .locals 1
    .param p0, "x0"    # Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mManagerService:Landroid/bluetooth/IBluetoothManager;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    .prologue
    .line 83
    invoke-direct {p0}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->reEnableRadio()V

    return-void
.end method

.method static synthetic access$200(Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;)I
    .locals 1
    .param p0, "x0"    # Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    .prologue
    .line 83
    iget v0, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mFMCount:I

    return v0
.end method

.method static synthetic access$202(Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;I)I
    .locals 0
    .param p0, "x0"    # Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;
    .param p1, "x1"    # I

    .prologue
    .line 83
    iput p1, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mFMCount:I

    return p1
.end method

.method static synthetic access$300(Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;)I
    .locals 1
    .param p0, "x0"    # Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    .prologue
    .line 83
    iget v0, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mBleCount:I

    return v0
.end method

.method static synthetic access$302(Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;I)I
    .locals 0
    .param p0, "x0"    # Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;
    .param p1, "x1"    # I

    .prologue
    .line 83
    iput p1, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mBleCount:I

    return p1
.end method

.method static synthetic access$400(Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;)I
    .locals 1
    .param p0, "x0"    # Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    .prologue
    .line 83
    iget v0, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mAntCount:I

    return v0
.end method

.method static synthetic access$402(Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;I)I
    .locals 0
    .param p0, "x0"    # Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;
    .param p1, "x1"    # I

    .prologue
    .line 83
    iput p1, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mAntCount:I

    return p1
.end method

.method static synthetic access$500(Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;)Landroid/os/ParcelUuid;
    .locals 1
    .param p0, "x0"    # Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mUUID:Landroid/os/ParcelUuid;

    return-object v0
.end method

.method static synthetic access$600(Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;)Landroid/bluetooth/IBluetoothManagerCallback;
    .locals 1
    .param p0, "x0"    # Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mManagerCallback:Landroid/bluetooth/IBluetoothManagerCallback;

    return-object v0
.end method

.method static synthetic access$700(Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;)I
    .locals 1
    .param p0, "x0"    # Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    .prologue
    .line 83
    iget v0, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mRadioType:I

    return v0
.end method

.method static synthetic access$800(Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;)Landroid/bluetooth/IBluetoothGatt;
    .locals 1
    .param p0, "x0"    # Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mBluetoothGatt:Landroid/bluetooth/IBluetoothGatt;

    return-object v0
.end method

.method static synthetic access$802(Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;Landroid/bluetooth/IBluetoothGatt;)Landroid/bluetooth/IBluetoothGatt;
    .locals 0
    .param p0, "x0"    # Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;
    .param p1, "x1"    # Landroid/bluetooth/IBluetoothGatt;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mBluetoothGatt:Landroid/bluetooth/IBluetoothGatt;

    return-object p1
.end method

.method static synthetic access$900(Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;)Landroid/bluetooth/IBluetoothRadioMgrCallback;
    .locals 1
    .param p0, "x0"    # Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mRadioMgrCallback:Landroid/bluetooth/IBluetoothRadioMgrCallback;

    return-object v0
.end method

.method public static declared-synchronized getRadioManager()Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;
    .locals 5

    .prologue
    .line 140
    const-class v3, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    monitor-enter v3

    :try_start_0
    sget-object v2, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->sRadioManager:Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    if-nez v2, :cond_0

    .line 141
    const-string v2, "bluetooth_manager"

    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    .line 142
    .local v0, "b":Landroid/os/IBinder;
    if-eqz v0, :cond_1

    .line 143
    invoke-static {v0}, Landroid/bluetooth/IBluetoothManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/bluetooth/IBluetoothManager;

    move-result-object v1

    .line 144
    .local v1, "managerService":Landroid/bluetooth/IBluetoothManager;
    new-instance v2, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    invoke-direct {v2, v1}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;-><init>(Landroid/bluetooth/IBluetoothManager;)V

    sput-object v2, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->sRadioManager:Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    .line 149
    .end local v1    # "managerService":Landroid/bluetooth/IBluetoothManager;
    :cond_0
    :goto_0
    sget-object v2, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->sRadioManager:Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v3

    return-object v2

    .line 146
    :cond_1
    :try_start_1
    const-string v2, "BluetoothRadioManager"

    const-string v4, "Bluetooth binder is null"

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 140
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2
.end method

.method private reEnableRadio()V
    .locals 8

    .prologue
    .line 485
    const-string v5, "BluetoothRadioManager"

    const-string v6, "onUserSwitched:"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 489
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v5, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mGattSrvStChangeCallback:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v2, v5, :cond_0

    .line 491
    :try_start_0
    iget-object v5, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mManagerService:Landroid/bluetooth/IBluetoothManager;

    const/4 v6, 0x1

    invoke-interface {v5, v6}, Landroid/bluetooth/IBluetoothManager;->enableRadio(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 489
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 492
    :catch_0
    move-exception v1

    .local v1, "e":Landroid/os/RemoteException;
    const-string v5, "BluetoothRadioManager"

    const-string v6, ""

    invoke-static {v5, v6, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 495
    .end local v1    # "e":Landroid/os/RemoteException;
    :cond_0
    iget-object v5, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mRadioStChangeCallback:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 496
    .local v3, "it":Ljava/util/Iterator;
    :cond_1
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 497
    const-string v5, "BluetoothRadioManager"

    const-string v6, "Entry found in mGattSrvStChangeCallback"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 498
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    .line 499
    .local v4, "pairs":Ljava/util/Map$Entry;
    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$BluetoothRadioStateChangeCallback;

    .line 502
    .local v0, "cb":Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$BluetoothRadioStateChangeCallback;
    if-eqz v0, :cond_1

    .line 503
    :try_start_1
    const-string v5, "BluetoothRadioManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Calling enable Radio for radio Type = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 505
    iget-object v6, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mManagerService:Landroid/bluetooth/IBluetoothManager;

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-interface {v6, v5}, Landroid/bluetooth/IBluetoothManager;->enableRadio(I)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    .line 507
    :catch_1
    move-exception v1

    .local v1, "e":Ljava/lang/Exception;
    const-string v5, "BluetoothRadioManager"

    const-string v6, ""

    invoke-static {v5, v6, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    .line 509
    .end local v0    # "cb":Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$BluetoothRadioStateChangeCallback;
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v4    # "pairs":Ljava/util/Map$Entry;
    :cond_2
    return-void
.end method


# virtual methods
.method public addLeRadioReference(Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$BluetoothGattServiceStateChangeCallback;)Z
    .locals 5
    .param p1, "cb"    # Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$BluetoothGattServiceStateChangeCallback;

    .prologue
    const/4 v1, 0x1

    .line 251
    if-eqz p1, :cond_0

    iget-object v2, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mGattSrvStChangeCallback:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 252
    const-string v2, "BluetoothRadioManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "addLeRadioReference: Add CB  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 253
    iget-object v2, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mGattSrvStChangeCallback:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 255
    :cond_0
    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->addRadioReference(ILcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$BluetoothRadioStateChangeCallback;)Z

    .line 256
    const-string v2, "BluetoothRadioManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "addLeRadioReference: isRadioEnabled() =  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->isRadioEnabled()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 257
    invoke-virtual {p0}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->isRadioEnabled()Z

    move-result v2

    if-ne v2, v1, :cond_1

    iget-object v2, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mBluetoothGatt:Landroid/bluetooth/IBluetoothGatt;

    if-nez v2, :cond_1

    .line 261
    :try_start_0
    iget-object v2, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mManagerService:Landroid/bluetooth/IBluetoothManager;

    invoke-interface {v2}, Landroid/bluetooth/IBluetoothManager;->getBluetoothGatt()Landroid/bluetooth/IBluetoothGatt;

    move-result-object v2

    iput-object v2, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mBluetoothGatt:Landroid/bluetooth/IBluetoothGatt;

    .line 262
    iget-object v2, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mBluetoothGatt:Landroid/bluetooth/IBluetoothGatt;

    if-nez v2, :cond_2

    .line 263
    iget-object v2, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mManagerService:Landroid/bluetooth/IBluetoothManager;

    invoke-interface {v2}, Landroid/bluetooth/IBluetoothManager;->enableGatt()Z

    .line 272
    :cond_1
    :goto_0
    return v1

    .line 264
    :cond_2
    if-eqz p1, :cond_1

    .line 265
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mBluetoothGatt:Landroid/bluetooth/IBluetoothGatt;

    invoke-interface {p1, v2, v3}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$BluetoothGattServiceStateChangeCallback;->onGattServiceStateChange(ZLandroid/bluetooth/IBluetoothGatt;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 267
    :catch_0
    move-exception v0

    .line 268
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "BluetoothRadioManager"

    const-string v2, ""

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 269
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public addRadioReference(ILcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$BluetoothRadioStateChangeCallback;)Z
    .locals 7
    .param p1, "radioType"    # I
    .param p2, "cb"    # Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$BluetoothRadioStateChangeCallback;

    .prologue
    const/4 v3, 0x0

    .line 180
    invoke-virtual {p0}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->isRadioEnabled()Z

    move-result v1

    .line 181
    .local v1, "enabled":Z
    const-string v4, "BluetoothRadioManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " addRadioReference enabled = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 182
    const/4 v2, 0x0

    .line 184
    .local v2, "ret":Z
    iput p1, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mRadioType:I

    .line 185
    iget v4, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mRadioType:I

    packed-switch v4, :pswitch_data_0

    .line 221
    const-string v4, "BluetoothRadioManager"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " Unsupported Radio type. "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mRadioType:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 241
    :goto_0
    return v3

    .line 188
    :pswitch_0
    iget v4, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mFMCount:I

    if-lez v4, :cond_0

    .line 189
    const-string v4, "BluetoothRadioManager"

    const-string v5, " FM Radio Already enabled "

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 193
    :cond_0
    iget v3, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mFMCount:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mFMCount:I

    .line 194
    const-string v3, "BluetoothRadioManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " FM Radio count is : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mFMCount:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    :goto_1
    if-eqz p2, :cond_1

    iget-object v3, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mRadioStChangeCallback:Ljava/util/Map;

    invoke-interface {v3, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 226
    const-string v3, "BluetoothRadioManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "addRadioReference: Add CB = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "Radio Type = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mRadioType:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 227
    iget-object v3, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mRadioStChangeCallback:Ljava/util/Map;

    iget v4, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mRadioType:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, p2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 231
    :cond_1
    if-nez v1, :cond_2

    :try_start_0
    iget-object v3, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 232
    :cond_2
    const-string v3, "BluetoothRadioManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "addRadioReference()  registerRadioClient mUUID = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mUUID:Landroid/os/ParcelUuid;

    invoke-virtual {v5}, Landroid/os/ParcelUuid;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 233
    iget-object v3, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mService:Landroid/bluetooth/IBluetooth;

    iget-object v4, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mManagerCallback:Landroid/bluetooth/IBluetoothManagerCallback;

    iget-object v5, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mUUID:Landroid/os/ParcelUuid;

    iget v6, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mRadioType:I

    invoke-interface {v3, v4, v5, v6}, Landroid/bluetooth/IBluetooth;->registerRadioClient(Landroid/bluetooth/IBluetoothManagerCallback;Landroid/os/ParcelUuid;I)Z

    .line 237
    :cond_3
    iget-object v3, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mManagerService:Landroid/bluetooth/IBluetoothManager;

    iget v4, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mRadioType:I

    invoke-interface {v3, v4}, Landroid/bluetooth/IBluetoothManager;->enableRadio(I)Z

    move-result v2

    .line 238
    if-eqz v2, :cond_4

    if-eqz v1, :cond_4

    if-eqz p2, :cond_4

    .line 239
    const/4 v3, 0x1

    invoke-interface {p2, v3}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$BluetoothRadioStateChangeCallback;->onRadioStateChange(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_4
    :goto_2
    move v3, v2

    .line 241
    goto/16 :goto_0

    .line 199
    :pswitch_1
    iget v4, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mAntCount:I

    if-lez v4, :cond_5

    .line 200
    const-string v4, "BluetoothRadioManager"

    const-string v5, " ANT Radio Already enabled "

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 204
    :cond_5
    iget v3, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mAntCount:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mAntCount:I

    .line 205
    const-string v3, "BluetoothRadioManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " ANT Radio count : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mAntCount:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 210
    :pswitch_2
    iget v4, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mBleCount:I

    if-lez v4, :cond_6

    .line 211
    const-string v4, "BluetoothRadioManager"

    const-string v5, " BLE Radio Already enabled "

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 215
    :cond_6
    iget v3, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mBleCount:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mBleCount:I

    .line 216
    const-string v3, "BluetoothRadioManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " BLE Radio count is : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mBleCount:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 240
    :catch_0
    move-exception v0

    .local v0, "e":Landroid/os/RemoteException;
    const-string v3, "BluetoothRadioManager"

    const-string v4, ""

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    .line 185
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method protected finalize()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 425
    :try_start_0
    iget-object v0, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mManagerService:Landroid/bluetooth/IBluetoothManager;

    iget-object v1, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mRadioMgrCallback:Landroid/bluetooth/IBluetoothRadioMgrCallback;

    invoke-interface {v0, v1}, Landroid/bluetooth/IBluetoothManager;->unregisterRadioMgrCallback(Landroid/bluetooth/IBluetoothRadioMgrCallback;)Z

    .line 426
    iget-object v0, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mManagerService:Landroid/bluetooth/IBluetoothManager;

    iget-object v1, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mManagerCallback:Landroid/bluetooth/IBluetoothManagerCallback;

    invoke-interface {v0, v1}, Landroid/bluetooth/IBluetoothManager;->unregisterAdapter(Landroid/bluetooth/IBluetoothManagerCallback;)V

    .line 427
    iget-object v0, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mService:Landroid/bluetooth/IBluetooth;

    iget-object v1, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mUUID:Landroid/os/ParcelUuid;

    invoke-interface {v0, v1}, Landroid/bluetooth/IBluetooth;->unregisterRadioClient(Landroid/os/ParcelUuid;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 429
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 431
    return-void

    .line 429
    :catchall_0
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    throw v0
.end method

.method public getRadioStates()B
    .locals 3

    .prologue
    .line 311
    :try_start_0
    iget-object v1, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mService:Landroid/bluetooth/IBluetooth;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mService:Landroid/bluetooth/IBluetooth;

    invoke-interface {v1}, Landroid/bluetooth/IBluetooth;->getRadioStates()B
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 314
    :goto_0
    return v1

    .line 312
    :catch_0
    move-exception v0

    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "BluetoothRadioManager"

    const-string v2, ""

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 314
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isGattServiceReady()Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 324
    const/4 v0, 0x0

    .line 326
    .local v0, "bRadioEnabled":Z
    :try_start_0
    iget-object v4, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mManagerCallback:Landroid/bluetooth/IBluetoothManagerCallback;

    monitor-enter v4
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 327
    :try_start_1
    iget-object v3, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mService:Landroid/bluetooth/IBluetooth;

    if-eqz v3, :cond_0

    .line 328
    iget-object v3, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mService:Landroid/bluetooth/IBluetooth;

    invoke-interface {v3}, Landroid/bluetooth/IBluetooth;->isRadioEnabled()Z

    move-result v0

    .line 329
    :cond_0
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 332
    :goto_0
    if-ne v0, v2, :cond_1

    iget-object v3, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mBluetoothGatt:Landroid/bluetooth/IBluetoothGatt;

    if-eqz v3, :cond_1

    .line 335
    :goto_1
    return v2

    .line 329
    :catchall_0
    move-exception v3

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v3
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0

    .line 330
    :catch_0
    move-exception v1

    .local v1, "e":Landroid/os/RemoteException;
    const-string v3, "BluetoothRadioManager"

    const-string v4, ""

    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 335
    .end local v1    # "e":Landroid/os/RemoteException;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public isRadioEnabled()Z
    .locals 3

    .prologue
    .line 282
    :try_start_0
    iget-object v2, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mManagerCallback:Landroid/bluetooth/IBluetoothManagerCallback;

    monitor-enter v2
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 283
    :try_start_1
    iget-object v1, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mService:Landroid/bluetooth/IBluetooth;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mService:Landroid/bluetooth/IBluetooth;

    invoke-interface {v1}, Landroid/bluetooth/IBluetooth;->isRadioEnabled()Z

    move-result v1

    monitor-exit v2

    .line 286
    :goto_0
    return v1

    .line 284
    :cond_0
    monitor-exit v2

    .line 286
    :goto_1
    const/4 v1, 0x0

    goto :goto_0

    .line 284
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v1
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    .line 285
    :catch_0
    move-exception v0

    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "BluetoothRadioManager"

    const-string v2, ""

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public isRadioEnabled(I)Z
    .locals 4
    .param p1, "radioType"    # I

    .prologue
    const/4 v2, 0x1

    .line 297
    shl-int v3, v2, p1

    int-to-byte v0, v3

    .line 298
    .local v0, "btMsk":B
    invoke-virtual {p0}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->getRadioStates()B

    move-result v1

    .line 300
    .local v1, "btStats":B
    and-int v3, v1, v0

    if-lez v3, :cond_0

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public releaseLeRadioReference(Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$BluetoothGattServiceStateChangeCallback;)Z
    .locals 3
    .param p1, "cb"    # Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$BluetoothGattServiceStateChangeCallback;

    .prologue
    const/4 v2, 0x0

    .line 414
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v2}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->releaseRadioReference(ILcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$BluetoothRadioStateChangeCallback;)Z

    move-result v0

    .line 415
    .local v0, "ret":Z
    if-eqz p1, :cond_0

    .line 416
    const/4 v1, 0x0

    invoke-interface {p1, v1, v2}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$BluetoothGattServiceStateChangeCallback;->onGattServiceStateChange(ZLandroid/bluetooth/IBluetoothGatt;)V

    .line 417
    iget-object v1, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mGattSrvStChangeCallback:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 419
    :cond_0
    iput-object v2, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mBluetoothGatt:Landroid/bluetooth/IBluetoothGatt;

    .line 420
    return v0
.end method

.method public releaseRadioReference(ILcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$BluetoothRadioStateChangeCallback;)Z
    .locals 6
    .param p1, "radioType"    # I
    .param p2, "cb"    # Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$BluetoothRadioStateChangeCallback;

    .prologue
    const/4 v2, 0x0

    .line 345
    const-string v3, "BluetoothRadioManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "releaseRadioReference Radio Type = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 347
    packed-switch p1, :pswitch_data_0

    .line 377
    const-string v3, "BluetoothRadioManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " Unsupported Radio type. "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 406
    :cond_0
    :goto_0
    return v2

    .line 350
    :pswitch_0
    iget v3, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mFMCount:I

    if-lez v3, :cond_0

    .line 351
    iget v3, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mFMCount:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mFMCount:I

    .line 352
    const-string v3, "BluetoothRadioManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " mFMCount Radio count is : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mFMCount:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 381
    :goto_1
    if-eqz p2, :cond_1

    iget-object v3, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mRadioStChangeCallback:Ljava/util/Map;

    invoke-interface {v3, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 384
    :try_start_0
    iget-object v3, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mService:Landroid/bluetooth/IBluetooth;

    invoke-interface {v3}, Landroid/bluetooth/IBluetooth;->getRadioCount()I

    move-result v1

    .line 385
    .local v1, "radioCount":I
    const-string v3, "BluetoothRadioManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "KHT:radioCount = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 386
    const/4 v3, 0x1

    if-le v1, v3, :cond_1

    .line 387
    const/4 v3, 0x0

    invoke-interface {p2, v3}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$BluetoothRadioStateChangeCallback;->onRadioStateChange(Z)V

    .line 388
    iget-object v3, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mRadioStChangeCallback:Ljava/util/Map;

    invoke-interface {v3, p2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 396
    .end local v1    # "radioCount":I
    :cond_1
    :goto_2
    :try_start_1
    iget-object v3, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mService:Landroid/bluetooth/IBluetooth;

    if-eqz v3, :cond_3

    .line 397
    iget v3, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mFMCount:I

    if-nez v3, :cond_2

    iget v3, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mAntCount:I

    if-nez v3, :cond_2

    iget v3, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mBleCount:I

    if-nez v3, :cond_2

    .line 398
    const-string v3, "BluetoothRadioManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " unregisterRadioClient. "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 399
    iget-object v3, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mService:Landroid/bluetooth/IBluetooth;

    iget-object v4, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mUUID:Landroid/os/ParcelUuid;

    invoke-interface {v3, v4}, Landroid/bluetooth/IBluetooth;->unregisterRadioClient(Landroid/os/ParcelUuid;)Z

    .line 401
    :cond_2
    iget-object v3, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mManagerService:Landroid/bluetooth/IBluetoothManager;

    invoke-interface {v3, p1}, Landroid/bluetooth/IBluetoothManager;->disableRadio(I)Z
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v2

    goto/16 :goto_0

    .line 359
    :pswitch_1
    iget v3, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mAntCount:I

    if-lez v3, :cond_0

    .line 360
    iget v3, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mAntCount:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mAntCount:I

    .line 361
    const-string v3, "BluetoothRadioManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " mAntCount Radio count is : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mAntCount:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 368
    :pswitch_2
    iget v3, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mBleCount:I

    if-lez v3, :cond_0

    .line 369
    iget v3, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mBleCount:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mBleCount:I

    .line 370
    const-string v3, "BluetoothRadioManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " mBleCount Radio count is : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mBleCount:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 390
    :catch_0
    move-exception v0

    .line 391
    .local v0, "e":Landroid/os/RemoteException;
    const-string v3, "BluetoothRadioManager"

    const-string v4, ""

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_2

    .line 403
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_3
    :try_start_2
    const-string v3, "BluetoothRadioManager"

    const-string v4, "Service is not exist!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_0

    .line 405
    :catch_1
    move-exception v0

    .restart local v0    # "e":Landroid/os/RemoteException;
    const-string v3, "BluetoothRadioManager"

    const-string v4, ""

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    .line 347
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
