.class Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$2;
.super Landroid/bluetooth/IBluetoothRadioMgrCallback$Stub;
.source "BluetoothRadioManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;


# direct methods
.method constructor <init>(Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;)V
    .locals 0

    .prologue
    .line 516
    iput-object p1, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$2;->this$0:Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    invoke-direct {p0}, Landroid/bluetooth/IBluetoothRadioMgrCallback$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onAirplaneModeChange(Z)V
    .locals 6
    .param p1, "on"    # Z

    .prologue
    const/4 v5, 0x1

    .line 594
    const-string v2, "BluetoothRadioManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onAirplaneModeChange:  on = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 595
    iget-object v2, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$2;->this$0:Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    # setter for: Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mAirplaneOn:Z
    invoke-static {v2, p1}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->access$102(Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;Z)Z

    .line 596
    if-eq p1, v5, :cond_0

    .line 598
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$2;->this$0:Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    # getter for: Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mGattSrvStChangeCallback:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->access$1000(Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 600
    :try_start_0
    iget-object v2, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$2;->this$0:Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    # getter for: Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mManagerService:Landroid/bluetooth/IBluetoothManager;
    invoke-static {v2}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->access$1200(Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;)Landroid/bluetooth/IBluetoothManager;

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v2, v3}, Landroid/bluetooth/IBluetoothManager;->enableRadio(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 598
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 601
    :catch_0
    move-exception v0

    .local v0, "e":Landroid/os/RemoteException;
    const-string v2, "BluetoothRadioManager"

    const-string v3, ""

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 624
    .end local v0    # "e":Landroid/os/RemoteException;
    .end local v1    # "i":I
    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$2;->this$0:Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    # getter for: Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mManagerService:Landroid/bluetooth/IBluetoothManager;
    invoke-static {v2}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->access$1200(Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;)Landroid/bluetooth/IBluetoothManager;

    move-result-object v2

    const/16 v3, 0xff

    invoke-interface {v2, v3}, Landroid/bluetooth/IBluetoothManager;->disableRadio(I)Z
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 627
    :cond_1
    :goto_2
    return-void

    .line 625
    :catch_1
    move-exception v0

    .restart local v0    # "e":Landroid/os/RemoteException;
    const-string v2, "BluetoothRadioManager"

    const-string v3, ""

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2
.end method

.method public onBTRadioStateChange(Z)V
    .locals 8
    .param p1, "up"    # Z

    .prologue
    .line 546
    const-string v5, "BluetoothRadioManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onBTRadioStateChange:  up = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 549
    const/4 v3, 0x0

    .line 551
    .local v3, "it":Ljava/util/Iterator;
    :try_start_0
    iget-object v5, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$2;->this$0:Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    # getter for: Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mRadioStChangeCallback:Ljava/util/Map;
    invoke-static {v5}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->access$1100(Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;)Ljava/util/Map;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v3

    .line 556
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 557
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    .line 558
    .local v4, "pairs":Ljava/util/Map$Entry;
    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$BluetoothRadioStateChangeCallback;

    .line 561
    .local v0, "cb":Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$BluetoothRadioStateChangeCallback;
    if-eqz v0, :cond_1

    .line 562
    :try_start_1
    invoke-interface {v0, p1}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$BluetoothRadioStateChangeCallback;->onRadioStateChange(Z)V

    .line 564
    :cond_1
    if-nez p1, :cond_0

    .line 565
    iget-object v5, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$2;->this$0:Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    # getter for: Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mRadioStChangeCallback:Ljava/util/Map;
    invoke-static {v5}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->access$1100(Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;)Ljava/util/Map;

    move-result-object v5

    invoke-interface {v5, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 567
    :catch_0
    move-exception v1

    .local v1, "e":Ljava/lang/Exception;
    const-string v5, "BluetoothRadioManager"

    const-string v6, ""

    invoke-static {v5, v6, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 552
    .end local v0    # "cb":Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$BluetoothRadioStateChangeCallback;
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v4    # "pairs":Ljava/util/Map$Entry;
    :catch_1
    move-exception v1

    .line 553
    .local v1, "e":Ljava/lang/NullPointerException;
    const-string v5, "BluetoothRadioManager"

    const-string v6, ""

    invoke-static {v5, v6, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 591
    .end local v1    # "e":Ljava/lang/NullPointerException;
    :cond_2
    :goto_1
    return-void

    .line 570
    :cond_3
    const/4 v5, 0x1

    if-ne p1, v5, :cond_2

    .line 571
    iget-object v5, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$2;->this$0:Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    # getter for: Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mGattSrvStChangeCallback:Ljava/util/ArrayList;
    invoke-static {v5}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->access$1000(Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_2

    iget-object v5, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$2;->this$0:Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    # getter for: Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mBleCount:I
    invoke-static {v5}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->access$300(Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;)I

    move-result v5

    if-lez v5, :cond_2

    .line 573
    :try_start_2
    iget-object v5, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$2;->this$0:Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    iget-object v6, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$2;->this$0:Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    # getter for: Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mManagerService:Landroid/bluetooth/IBluetoothManager;
    invoke-static {v6}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->access$1200(Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;)Landroid/bluetooth/IBluetoothManager;

    move-result-object v6

    invoke-interface {v6}, Landroid/bluetooth/IBluetoothManager;->getBluetoothGatt()Landroid/bluetooth/IBluetoothGatt;

    move-result-object v6

    # setter for: Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mBluetoothGatt:Landroid/bluetooth/IBluetoothGatt;
    invoke-static {v5, v6}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->access$802(Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;Landroid/bluetooth/IBluetoothGatt;)Landroid/bluetooth/IBluetoothGatt;

    .line 574
    iget-object v5, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$2;->this$0:Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    # getter for: Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mBluetoothGatt:Landroid/bluetooth/IBluetoothGatt;
    invoke-static {v5}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->access$800(Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;)Landroid/bluetooth/IBluetoothGatt;

    move-result-object v5

    if-nez v5, :cond_4

    .line 575
    iget-object v5, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$2;->this$0:Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    # getter for: Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mManagerService:Landroid/bluetooth/IBluetoothManager;
    invoke-static {v5}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->access$1200(Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;)Landroid/bluetooth/IBluetoothManager;

    move-result-object v5

    invoke-interface {v5}, Landroid/bluetooth/IBluetoothManager;->enableGatt()Z
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_1

    .line 585
    :catch_2
    move-exception v1

    .local v1, "e":Landroid/os/RemoteException;
    const-string v5, "BluetoothRadioManager"

    const-string v6, ""

    invoke-static {v5, v6, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 577
    .end local v1    # "e":Landroid/os/RemoteException;
    :cond_4
    :try_start_3
    iget-object v5, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$2;->this$0:Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    # getter for: Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mGattSrvStChangeCallback:Ljava/util/ArrayList;
    invoke-static {v5}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->access$1000(Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$BluetoothGattServiceStateChangeCallback;

    .line 578
    .local v0, "cb":Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$BluetoothGattServiceStateChangeCallback;
    if-eqz v0, :cond_5

    .line 579
    iget-object v5, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$2;->this$0:Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    # getter for: Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mBluetoothGatt:Landroid/bluetooth/IBluetoothGatt;
    invoke-static {v5}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->access$800(Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;)Landroid/bluetooth/IBluetoothGatt;

    move-result-object v5

    invoke-interface {v0, p1, v5}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$BluetoothGattServiceStateChangeCallback;->onGattServiceStateChange(ZLandroid/bluetooth/IBluetoothGatt;)V

    goto :goto_2

    .line 581
    :cond_5
    const-string v5, "BluetoothRadioManager"

    const-string v6, "onGattServiceUp: cb is null!!!"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_2
.end method

.method public onGattServiceStateChange(ZLandroid/bluetooth/IBluetoothGatt;)V
    .locals 6
    .param p1, "up"    # Z
    .param p2, "bluetoothGatt"    # Landroid/bluetooth/IBluetoothGatt;

    .prologue
    .line 519
    const-string v3, "BluetoothRadioManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onGattServiceStateChange: up = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "mBleCount = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$2;->this$0:Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    # getter for: Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mBleCount:I
    invoke-static {v5}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->access$300(Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 521
    iget-object v3, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$2;->this$0:Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    # getter for: Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mRadioMgrCallback:Landroid/bluetooth/IBluetoothRadioMgrCallback;
    invoke-static {v3}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->access$900(Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;)Landroid/bluetooth/IBluetoothRadioMgrCallback;

    move-result-object v4

    monitor-enter v4

    .line 522
    if-eqz p1, :cond_1

    .line 524
    :try_start_0
    iget-object v3, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$2;->this$0:Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    # getter for: Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mBleCount:I
    invoke-static {v3}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->access$300(Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;)I

    move-result v3

    if-nez v3, :cond_0

    .line 526
    const-string v3, "BluetoothRadioManager"

    const-string v5, "onGattServiceStateChange: ignore"

    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 527
    iget-object v3, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$2;->this$0:Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    const/4 v5, 0x0

    # setter for: Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mBluetoothGatt:Landroid/bluetooth/IBluetoothGatt;
    invoke-static {v3, v5}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->access$802(Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;Landroid/bluetooth/IBluetoothGatt;)Landroid/bluetooth/IBluetoothGatt;

    .line 528
    monitor-exit v4

    .line 543
    :goto_0
    return-void

    .line 530
    :cond_0
    iget-object v3, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$2;->this$0:Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    # setter for: Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mBluetoothGatt:Landroid/bluetooth/IBluetoothGatt;
    invoke-static {v3, p2}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->access$802(Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;Landroid/bluetooth/IBluetoothGatt;)Landroid/bluetooth/IBluetoothGatt;

    .line 533
    :cond_1
    iget-object v3, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$2;->this$0:Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    # getter for: Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mGattSrvStChangeCallback:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->access$1000(Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$BluetoothGattServiceStateChangeCallback;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 535
    .local v0, "cb":Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$BluetoothGattServiceStateChangeCallback;
    if-eqz v0, :cond_2

    .line 536
    :try_start_1
    iget-object v3, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$2;->this$0:Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    # getter for: Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mBluetoothGatt:Landroid/bluetooth/IBluetoothGatt;
    invoke-static {v3}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->access$800(Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;)Landroid/bluetooth/IBluetoothGatt;

    move-result-object v3

    invoke-interface {v0, p1, v3}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$BluetoothGattServiceStateChangeCallback;->onGattServiceStateChange(ZLandroid/bluetooth/IBluetoothGatt;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 540
    :catch_0
    move-exception v1

    .local v1, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v3, "BluetoothRadioManager"

    const-string v5, ""

    invoke-static {v3, v5, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 542
    .end local v0    # "cb":Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$BluetoothGattServiceStateChangeCallback;
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v2    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3

    .line 538
    .restart local v0    # "cb":Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$BluetoothGattServiceStateChangeCallback;
    .restart local v2    # "i$":Ljava/util/Iterator;
    :cond_2
    :try_start_3
    const-string v3, "BluetoothRadioManager"

    const-string v5, "onGattServiceUp: cb is null!!!"

    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 542
    .end local v0    # "cb":Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$BluetoothGattServiceStateChangeCallback;
    :cond_3
    :try_start_4
    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0
.end method

.method public onUserSwitched()V
    .locals 1

    .prologue
    .line 630
    iget-object v0, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$2;->this$0:Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    # invokes: Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->reEnableRadio()V
    invoke-static {v0}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->access$1300(Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;)V

    .line 631
    return-void
.end method
