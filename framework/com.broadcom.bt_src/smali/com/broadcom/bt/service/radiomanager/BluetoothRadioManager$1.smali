.class Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$1;
.super Landroid/bluetooth/IBluetoothManagerCallback$Stub;
.source "BluetoothRadioManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;


# direct methods
.method constructor <init>(Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;)V
    .locals 0

    .prologue
    .line 454
    iput-object p1, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$1;->this$0:Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    invoke-direct {p0}, Landroid/bluetooth/IBluetoothManagerCallback$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onBluetoothServiceDown()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 470
    const-string v0, "BluetoothRadioManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onBluetoothServiceDown: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$1;->this$0:Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    # getter for: Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mService:Landroid/bluetooth/IBluetooth;
    invoke-static {v2}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->access$000(Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;)Landroid/bluetooth/IBluetooth;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 471
    iget-object v0, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$1;->this$0:Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    # setter for: Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mService:Landroid/bluetooth/IBluetooth;
    invoke-static {v0, v4}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->access$002(Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;Landroid/bluetooth/IBluetooth;)Landroid/bluetooth/IBluetooth;

    .line 472
    iget-object v0, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$1;->this$0:Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    # setter for: Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mBluetoothGatt:Landroid/bluetooth/IBluetoothGatt;
    invoke-static {v0, v4}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->access$802(Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;Landroid/bluetooth/IBluetoothGatt;)Landroid/bluetooth/IBluetoothGatt;

    .line 473
    iget-object v0, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$1;->this$0:Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    # setter for: Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mFMCount:I
    invoke-static {v0, v3}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->access$202(Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;I)I

    .line 474
    iget-object v0, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$1;->this$0:Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    # setter for: Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mBleCount:I
    invoke-static {v0, v3}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->access$302(Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;I)I

    .line 475
    iget-object v0, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$1;->this$0:Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    # setter for: Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mAntCount:I
    invoke-static {v0, v3}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->access$402(Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;I)I

    .line 480
    return-void
.end method

.method public onBluetoothServiceUp(Landroid/bluetooth/IBluetooth;)V
    .locals 5
    .param p1, "bluetoothService"    # Landroid/bluetooth/IBluetooth;

    .prologue
    .line 457
    const-string v1, "BluetoothRadioManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onBluetoothServiceUp: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 458
    iget-object v1, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$1;->this$0:Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    # setter for: Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mService:Landroid/bluetooth/IBluetooth;
    invoke-static {v1, p1}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->access$002(Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;Landroid/bluetooth/IBluetooth;)Landroid/bluetooth/IBluetooth;

    .line 459
    iget-object v1, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$1;->this$0:Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    const/4 v2, 0x0

    # setter for: Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mAirplaneOn:Z
    invoke-static {v1, v2}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->access$102(Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;Z)Z

    .line 461
    iget-object v1, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$1;->this$0:Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    # getter for: Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mFMCount:I
    invoke-static {v1}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->access$200(Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;)I

    move-result v1

    if-gtz v1, :cond_0

    iget-object v1, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$1;->this$0:Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    # getter for: Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mBleCount:I
    invoke-static {v1}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->access$300(Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;)I

    move-result v1

    if-gtz v1, :cond_0

    iget-object v1, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$1;->this$0:Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    # getter for: Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mAntCount:I
    invoke-static {v1}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->access$400(Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;)I

    move-result v1

    if-lez v1, :cond_1

    .line 463
    :cond_0
    :try_start_0
    const-string v1, "BluetoothRadioManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onBluetoothServiceUp()  registerRadioClient mUUID = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$1;->this$0:Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    # getter for: Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mUUID:Landroid/os/ParcelUuid;
    invoke-static {v3}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->access$500(Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;)Landroid/os/ParcelUuid;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/ParcelUuid;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 464
    iget-object v1, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$1;->this$0:Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    # getter for: Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mService:Landroid/bluetooth/IBluetooth;
    invoke-static {v1}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->access$000(Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;)Landroid/bluetooth/IBluetooth;

    move-result-object v1

    iget-object v2, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$1;->this$0:Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    # getter for: Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mManagerCallback:Landroid/bluetooth/IBluetoothManagerCallback;
    invoke-static {v2}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->access$600(Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;)Landroid/bluetooth/IBluetoothManagerCallback;

    move-result-object v2

    iget-object v3, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$1;->this$0:Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    # getter for: Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mUUID:Landroid/os/ParcelUuid;
    invoke-static {v3}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->access$500(Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;)Landroid/os/ParcelUuid;

    move-result-object v3

    iget-object v4, p0, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager$1;->this$0:Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;

    # getter for: Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->mRadioType:I
    invoke-static {v4}, Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;->access$700(Lcom/broadcom/bt/service/radiomanager/BluetoothRadioManager;)I

    move-result v4

    invoke-interface {v1, v2, v3, v4}, Landroid/bluetooth/IBluetooth;->registerRadioClient(Landroid/bluetooth/IBluetoothManagerCallback;Landroid/os/ParcelUuid;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 467
    :cond_1
    :goto_0
    return-void

    .line 465
    :catch_0
    move-exception v0

    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "BluetoothRadioManager"

    const-string v2, ""

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
