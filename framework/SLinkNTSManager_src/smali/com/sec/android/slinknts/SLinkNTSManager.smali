.class public Lcom/sec/android/slinknts/SLinkNTSManager;
.super Ljava/lang/Object;
.source "SLinkNTSManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/slinknts/SLinkNTSManager$EventHandler;,
        Lcom/sec/android/slinknts/SLinkNTSManager$OnErrorListener;,
        Lcom/sec/android/slinknts/SLinkNTSManager$OnNTSStatusChangeListener;
    }
.end annotation


# static fields
.field public static final NOT_SUPPORTED:I = -0x1

.field public static final NTS_CORE_ERROR:I = 0x7d0

.field public static final NTS_CORE_ERROR_ACCESS_TOKEN_EXPIRED:I = -0x138c

.field public static final NTS_CORE_FAILURE:I = 0x3ea

.field public static final NTS_CORE_INITIALIZED:I = 0x3e8

.field private static final NTS_CORE_STATUS:I = 0x1f4

.field public static final NTS_CORE_TERMINATED:I = 0x3e9

.field private static final NTS_ERROR:I = 0x1f5

.field public static final OK:I = 0x0

.field private static final TAG:Ljava/lang/String; = "SLinkNTSManager"


# instance fields
.field private mAppId:Ljava/lang/String;

.field private mEventHandler:Lcom/sec/android/slinknts/SLinkNTSManager$EventHandler;

.field private mNativeContext:I

.field private mOnErrorListener:Lcom/sec/android/slinknts/SLinkNTSManager$OnErrorListener;

.field private mOnNTSStatusChangeListener:Lcom/sec/android/slinknts/SLinkNTSManager$OnNTSStatusChangeListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    const-string v0, "SLinkNTSMngr_jni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 49
    invoke-static {}, Lcom/sec/android/slinknts/SLinkNTSManager;->native_init()V

    .line 50
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 3
    .param p1, "appId"    # Ljava/lang/String;

    .prologue
    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    const-string v1, "SLinkNTSManager"

    const-string v2, "SLinkNTSManager enter!!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    iput-object p1, p0, Lcom/sec/android/slinknts/SLinkNTSManager;->mAppId:Ljava/lang/String;

    .line 92
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    .local v0, "looper":Landroid/os/Looper;
    if-eqz v0, :cond_0

    .line 93
    new-instance v1, Lcom/sec/android/slinknts/SLinkNTSManager$EventHandler;

    invoke-direct {v1, p0, p0, v0}, Lcom/sec/android/slinknts/SLinkNTSManager$EventHandler;-><init>(Lcom/sec/android/slinknts/SLinkNTSManager;Lcom/sec/android/slinknts/SLinkNTSManager;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/sec/android/slinknts/SLinkNTSManager;->mEventHandler:Lcom/sec/android/slinknts/SLinkNTSManager$EventHandler;

    .line 103
    :goto_0
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-direct {p0, v1, p1}, Lcom/sec/android/slinknts/SLinkNTSManager;->native_setup(Ljava/lang/Object;Ljava/lang/String;)V

    .line 104
    return-void

    .line 94
    :cond_0
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 95
    new-instance v1, Lcom/sec/android/slinknts/SLinkNTSManager$EventHandler;

    invoke-direct {v1, p0, p0, v0}, Lcom/sec/android/slinknts/SLinkNTSManager$EventHandler;-><init>(Lcom/sec/android/slinknts/SLinkNTSManager;Lcom/sec/android/slinknts/SLinkNTSManager;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/sec/android/slinknts/SLinkNTSManager;->mEventHandler:Lcom/sec/android/slinknts/SLinkNTSManager$EventHandler;

    goto :goto_0

    .line 97
    :cond_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/slinknts/SLinkNTSManager;->mEventHandler:Lcom/sec/android/slinknts/SLinkNTSManager$EventHandler;

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/sec/android/slinknts/SLinkNTSManager;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/slinknts/SLinkNTSManager;

    .prologue
    .line 45
    iget v0, p0, Lcom/sec/android/slinknts/SLinkNTSManager;->mNativeContext:I

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/slinknts/SLinkNTSManager;)Lcom/sec/android/slinknts/SLinkNTSManager$OnNTSStatusChangeListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/slinknts/SLinkNTSManager;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/slinknts/SLinkNTSManager;->mOnNTSStatusChangeListener:Lcom/sec/android/slinknts/SLinkNTSManager$OnNTSStatusChangeListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/slinknts/SLinkNTSManager;)Lcom/sec/android/slinknts/SLinkNTSManager$OnErrorListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/slinknts/SLinkNTSManager;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/slinknts/SLinkNTSManager;->mOnErrorListener:Lcom/sec/android/slinknts/SLinkNTSManager$OnErrorListener;

    return-object v0
.end method

.method private final native native_finalize()V
.end method

.method private static final native native_init()V
.end method

.method private final native native_setup(Ljava/lang/Object;Ljava/lang/String;)V
.end method

.method private static postEventFromNative(Ljava/lang/Object;IIILjava/lang/Object;)V
    .locals 5
    .param p0, "ntsmanager_ref"    # Ljava/lang/Object;
    .param p1, "what"    # I
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "obj"    # Ljava/lang/Object;

    .prologue
    .line 234
    const-string v2, "SLinkNTSManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "what: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " arg1: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " arg2: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 235
    if-eqz p4, :cond_0

    .line 236
    const-string v3, "SLinkNTSManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "postEventFromNative Error String is"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object v2, p4

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 238
    :cond_0
    check-cast p0, Ljava/lang/ref/WeakReference;

    .end local p0    # "ntsmanager_ref":Ljava/lang/Object;
    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/slinknts/SLinkNTSManager;

    .line 239
    .local v1, "nm":Lcom/sec/android/slinknts/SLinkNTSManager;
    if-nez v1, :cond_2

    .line 247
    :cond_1
    :goto_0
    return-void

    .line 243
    :cond_2
    iget-object v2, v1, Lcom/sec/android/slinknts/SLinkNTSManager;->mEventHandler:Lcom/sec/android/slinknts/SLinkNTSManager$EventHandler;

    if-eqz v2, :cond_1

    .line 244
    iget-object v2, v1, Lcom/sec/android/slinknts/SLinkNTSManager;->mEventHandler:Lcom/sec/android/slinknts/SLinkNTSManager$EventHandler;

    invoke-virtual {v2, p1, p2, p3, p4}, Lcom/sec/android/slinknts/SLinkNTSManager$EventHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 245
    .local v0, "m":Landroid/os/Message;
    iget-object v2, v1, Lcom/sec/android/slinknts/SLinkNTSManager;->mEventHandler:Lcom/sec/android/slinknts/SLinkNTSManager$EventHandler;

    invoke-virtual {v2, v0}, Lcom/sec/android/slinknts/SLinkNTSManager$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method


# virtual methods
.method public native _isNTSInitialized(Ljava/lang/String;)I
.end method

.method public native _setCurrentDevice(Ljava/lang/String;Ljava/lang/String;)I
.end method

.method public native _terminateCore(Ljava/lang/String;)V
.end method

.method protected finalize()V
    .locals 0

    .prologue
    .line 115
    invoke-direct {p0}, Lcom/sec/android/slinknts/SLinkNTSManager;->native_finalize()V

    return-void
.end method

.method public native initializeCore(Ljava/lang/String;)I
.end method

.method public isNTSInitialized()I
    .locals 1

    .prologue
    .line 255
    iget-object v0, p0, Lcom/sec/android/slinknts/SLinkNTSManager;->mAppId:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/sec/android/slinknts/SLinkNTSManager;->_isNTSInitialized(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public isSupportSLPF()I
    .locals 1

    .prologue
    .line 107
    const/4 v0, 0x0

    return v0
.end method

.method public setCurrentDevice(Ljava/lang/String;)I
    .locals 1
    .param p1, "peerId"    # Ljava/lang/String;

    .prologue
    .line 267
    iget-object v0, p0, Lcom/sec/android/slinknts/SLinkNTSManager;->mAppId:Ljava/lang/String;

    invoke-virtual {p0, v0, p1}, Lcom/sec/android/slinknts/SLinkNTSManager;->_setCurrentDevice(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public setOnErrorListener(Lcom/sec/android/slinknts/SLinkNTSManager$OnErrorListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/slinknts/SLinkNTSManager$OnErrorListener;

    .prologue
    .line 185
    iput-object p1, p0, Lcom/sec/android/slinknts/SLinkNTSManager;->mOnErrorListener:Lcom/sec/android/slinknts/SLinkNTSManager$OnErrorListener;

    .line 186
    return-void
.end method

.method public setOnNTSStatusChangeListener(Lcom/sec/android/slinknts/SLinkNTSManager$OnNTSStatusChangeListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/slinknts/SLinkNTSManager$OnNTSStatusChangeListener;

    .prologue
    .line 147
    iput-object p1, p0, Lcom/sec/android/slinknts/SLinkNTSManager;->mOnNTSStatusChangeListener:Lcom/sec/android/slinknts/SLinkNTSManager$OnNTSStatusChangeListener;

    .line 148
    return-void
.end method

.method public terminateCore()V
    .locals 1

    .prologue
    .line 278
    iget-object v0, p0, Lcom/sec/android/slinknts/SLinkNTSManager;->mAppId:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/sec/android/slinknts/SLinkNTSManager;->_terminateCore(Ljava/lang/String;)V

    .line 279
    return-void
.end method
