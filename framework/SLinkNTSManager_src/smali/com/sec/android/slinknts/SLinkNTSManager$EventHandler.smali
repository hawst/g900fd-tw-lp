.class Lcom/sec/android/slinknts/SLinkNTSManager$EventHandler;
.super Landroid/os/Handler;
.source "SLinkNTSManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/slinknts/SLinkNTSManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EventHandler"
.end annotation


# instance fields
.field private mNTSManager:Lcom/sec/android/slinknts/SLinkNTSManager;

.field final synthetic this$0:Lcom/sec/android/slinknts/SLinkNTSManager;


# direct methods
.method public constructor <init>(Lcom/sec/android/slinknts/SLinkNTSManager;Lcom/sec/android/slinknts/SLinkNTSManager;Landroid/os/Looper;)V
    .locals 0
    .param p2, "nm"    # Lcom/sec/android/slinknts/SLinkNTSManager;
    .param p3, "looper"    # Landroid/os/Looper;

    .prologue
    .line 194
    iput-object p1, p0, Lcom/sec/android/slinknts/SLinkNTSManager$EventHandler;->this$0:Lcom/sec/android/slinknts/SLinkNTSManager;

    .line 195
    invoke-direct {p0, p3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 196
    iput-object p2, p0, Lcom/sec/android/slinknts/SLinkNTSManager$EventHandler;->mNTSManager:Lcom/sec/android/slinknts/SLinkNTSManager;

    .line 197
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 201
    iget-object v0, p0, Lcom/sec/android/slinknts/SLinkNTSManager$EventHandler;->mNTSManager:Lcom/sec/android/slinknts/SLinkNTSManager;

    # getter for: Lcom/sec/android/slinknts/SLinkNTSManager;->mNativeContext:I
    invoke-static {v0}, Lcom/sec/android/slinknts/SLinkNTSManager;->access$000(Lcom/sec/android/slinknts/SLinkNTSManager;)I

    move-result v0

    if-nez v0, :cond_1

    .line 202
    const-string v0, "SLinkNTSManager"

    const-string v1, "NTSManager went away with unhandled events"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 221
    :cond_0
    :goto_0
    return-void

    .line 205
    :cond_1
    const-string v0, "SLinkNTSManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "received message :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",ext:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 206
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 218
    const-string v0, "SLinkNTSManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown message type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 208
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/slinknts/SLinkNTSManager$EventHandler;->this$0:Lcom/sec/android/slinknts/SLinkNTSManager;

    # getter for: Lcom/sec/android/slinknts/SLinkNTSManager;->mOnNTSStatusChangeListener:Lcom/sec/android/slinknts/SLinkNTSManager$OnNTSStatusChangeListener;
    invoke-static {v0}, Lcom/sec/android/slinknts/SLinkNTSManager;->access$100(Lcom/sec/android/slinknts/SLinkNTSManager;)Lcom/sec/android/slinknts/SLinkNTSManager$OnNTSStatusChangeListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 209
    iget-object v0, p0, Lcom/sec/android/slinknts/SLinkNTSManager$EventHandler;->this$0:Lcom/sec/android/slinknts/SLinkNTSManager;

    # getter for: Lcom/sec/android/slinknts/SLinkNTSManager;->mOnNTSStatusChangeListener:Lcom/sec/android/slinknts/SLinkNTSManager$OnNTSStatusChangeListener;
    invoke-static {v0}, Lcom/sec/android/slinknts/SLinkNTSManager;->access$100(Lcom/sec/android/slinknts/SLinkNTSManager;)Lcom/sec/android/slinknts/SLinkNTSManager$OnNTSStatusChangeListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/slinknts/SLinkNTSManager$EventHandler;->mNTSManager:Lcom/sec/android/slinknts/SLinkNTSManager;

    iget v2, p1, Landroid/os/Message;->arg1:I

    invoke-interface {v0, v1, v2}, Lcom/sec/android/slinknts/SLinkNTSManager$OnNTSStatusChangeListener;->onNTSStatusChanged(Lcom/sec/android/slinknts/SLinkNTSManager;I)Z

    goto :goto_0

    .line 213
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/slinknts/SLinkNTSManager$EventHandler;->this$0:Lcom/sec/android/slinknts/SLinkNTSManager;

    # getter for: Lcom/sec/android/slinknts/SLinkNTSManager;->mOnErrorListener:Lcom/sec/android/slinknts/SLinkNTSManager$OnErrorListener;
    invoke-static {v0}, Lcom/sec/android/slinknts/SLinkNTSManager;->access$200(Lcom/sec/android/slinknts/SLinkNTSManager;)Lcom/sec/android/slinknts/SLinkNTSManager$OnErrorListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 214
    iget-object v0, p0, Lcom/sec/android/slinknts/SLinkNTSManager$EventHandler;->this$0:Lcom/sec/android/slinknts/SLinkNTSManager;

    # getter for: Lcom/sec/android/slinknts/SLinkNTSManager;->mOnErrorListener:Lcom/sec/android/slinknts/SLinkNTSManager$OnErrorListener;
    invoke-static {v0}, Lcom/sec/android/slinknts/SLinkNTSManager;->access$200(Lcom/sec/android/slinknts/SLinkNTSManager;)Lcom/sec/android/slinknts/SLinkNTSManager$OnErrorListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/slinknts/SLinkNTSManager$EventHandler;->mNTSManager:Lcom/sec/android/slinknts/SLinkNTSManager;

    iget v2, p1, Landroid/os/Message;->arg1:I

    iget v3, p1, Landroid/os/Message;->arg2:I

    invoke-interface {v0, v1, v2, v3}, Lcom/sec/android/slinknts/SLinkNTSManager$OnErrorListener;->onError(Lcom/sec/android/slinknts/SLinkNTSManager;II)Z

    goto :goto_0

    .line 206
    :pswitch_data_0
    .packed-switch 0x1f4
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
