.class public Lcom/sec/android/secmarcoa/SecMarCoa;
.super Ljava/lang/Object;
.source "SecMarCoa.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/secmarcoa/SecMarCoa$EventHandler;,
        Lcom/sec/android/secmarcoa/SecMarCoa$OnInfoListener;,
        Lcom/sec/android/secmarcoa/SecMarCoa$OnErrorListener;,
        Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;,
        Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;
    }
.end annotation


# static fields
.field public static final MARCOA_MODE_DIRECTOR:I = 0x1

.field public static final MARCOA_MODE_DIRECTOR_BGM:I = 0x5

.field public static final MARCOA_MODE_MASTER:I = 0x3

.field public static final MARCOA_MODE_MASTER_BGM:I = 0x7

.field public static final MARCOA_MODE_SLAVE:I = 0x2

.field public static final MARCOA_MODE_SLAVE_BGM:I = 0x6

.field public static final MAR_COA_ERROR_CLIENT_DISCONNECT:I = 0x2

.field public static final MAR_COA_ERROR_UNKNOWN:I = 0x1

.field public static final MAR_COA_INFO_COA_STARTED:I = 0x388

.field public static final MAR_COA_INFO_COMPLETION_STATUS:I = 0x322

.field public static final MAR_COA_INFO_COMPOSE_COMPLETE:I = 0x386

.field public static final MAR_COA_INFO_COMPOSE_PROGRESS:I = 0x387

.field public static final MAR_COA_INFO_DURATION_PROGRESS:I = 0x385

.field public static final MAR_COA_INFO_FILESIZE_PROGRESS:I = 0x384

.field public static final MAR_COA_INFO_MAX_DURATION_REACHED:I = 0x320

.field public static final MAR_COA_INFO_MAX_FILESIZE_REACHED:I = 0x321

.field public static final MAR_COA_INFO_PROGRESS_FRAME_STATUS:I = 0x323

.field public static final MAR_COA_INFO_PROGRESS_TIME_STATUS:I = 0x324

.field public static final MAR_COA_INFO_UNKNOWN:I = 0x1

.field public static final MAR_COA_TRACK_INFO_COMPLETION_STATUS:I = 0x3e8

.field public static final MAR_COA_TRACK_INFO_DATA_KBYTES:I = 0x3f1

.field public static final MAR_COA_TRACK_INFO_DURATION_MS:I = 0x3eb

.field public static final MAR_COA_TRACK_INFO_ENCODED_FRAMES:I = 0x3ed

.field public static final MAR_COA_TRACK_INFO_INITIAL_DELAY_MS:I = 0x3ef

.field public static final MAR_COA_TRACK_INFO_LIST_END:I = 0x7d0

.field public static final MAR_COA_TRACK_INFO_LIST_START:I = 0x3e8

.field public static final MAR_COA_TRACK_INFO_MAX_CHUNK_DUR_MS:I = 0x3ec

.field public static final MAR_COA_TRACK_INFO_PROGRESS_IN_TIME:I = 0x3e9

.field public static final MAR_COA_TRACK_INFO_START_OFFSET_MS:I = 0x3f0

.field public static final MAR_COA_TRACK_INFO_TYPE:I = 0x3ea

.field public static final MAR_COA_TRACK_INTER_CHUNK_TIME_MS:I = 0x3ee

.field private static final TAG:Ljava/lang/String; = "SecMarCoa"


# instance fields
.field private mCoaMode:I

.field private mCurrentState:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

.field private mEventHandler:Lcom/sec/android/secmarcoa/SecMarCoa$EventHandler;

.field private mLastCamChangeDevice:I

.field private mLastCamChangeTime:I

.field private mNativeContext:I

.field private mOnErrorListener:Lcom/sec/android/secmarcoa/SecMarCoa$OnErrorListener;

.field private mOnInfoListener:Lcom/sec/android/secmarcoa/SecMarCoa$OnInfoListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 61
    const-string v0, "secmarcoa_jni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 62
    invoke-static {}, Lcom/sec/android/secmarcoa/SecMarCoa;->native_init()V

    .line 63
    return-void
.end method

.method public constructor <init>(I)V
    .locals 4
    .param p1, "mode"    # I

    .prologue
    const/16 v1, -0x64

    .line 123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    iput v1, p0, Lcom/sec/android/secmarcoa/SecMarCoa;->mLastCamChangeDevice:I

    .line 74
    iput v1, p0, Lcom/sec/android/secmarcoa/SecMarCoa;->mLastCamChangeTime:I

    .line 75
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/secmarcoa/SecMarCoa;->mCoaMode:I

    .line 105
    sget-object v1, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;->IDLE:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    iput-object v1, p0, Lcom/sec/android/secmarcoa/SecMarCoa;->mCurrentState:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    .line 124
    const-string v1, "SecMarCoa"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SecMarCoa enter! COA mode:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    .local v0, "looper":Landroid/os/Looper;
    if-eqz v0, :cond_0

    .line 127
    new-instance v1, Lcom/sec/android/secmarcoa/SecMarCoa$EventHandler;

    invoke-direct {v1, p0, p0, v0}, Lcom/sec/android/secmarcoa/SecMarCoa$EventHandler;-><init>(Lcom/sec/android/secmarcoa/SecMarCoa;Lcom/sec/android/secmarcoa/SecMarCoa;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/sec/android/secmarcoa/SecMarCoa;->mEventHandler:Lcom/sec/android/secmarcoa/SecMarCoa$EventHandler;

    .line 133
    :goto_0
    iput p1, p0, Lcom/sec/android/secmarcoa/SecMarCoa;->mCoaMode:I

    .line 134
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iget v2, p0, Lcom/sec/android/secmarcoa/SecMarCoa;->mCoaMode:I

    invoke-direct {p0, v1, v2}, Lcom/sec/android/secmarcoa/SecMarCoa;->native_setup(Ljava/lang/Object;I)V

    .line 135
    sget-object v1, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;->INITIALIZED:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    iput-object v1, p0, Lcom/sec/android/secmarcoa/SecMarCoa;->mCurrentState:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    .line 136
    return-void

    .line 128
    :cond_0
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 129
    new-instance v1, Lcom/sec/android/secmarcoa/SecMarCoa$EventHandler;

    invoke-direct {v1, p0, p0, v0}, Lcom/sec/android/secmarcoa/SecMarCoa$EventHandler;-><init>(Lcom/sec/android/secmarcoa/SecMarCoa;Lcom/sec/android/secmarcoa/SecMarCoa;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/sec/android/secmarcoa/SecMarCoa;->mEventHandler:Lcom/sec/android/secmarcoa/SecMarCoa$EventHandler;

    goto :goto_0

    .line 131
    :cond_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/secmarcoa/SecMarCoa;->mEventHandler:Lcom/sec/android/secmarcoa/SecMarCoa$EventHandler;

    goto :goto_0
.end method

.method private native _cancel()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method private native _cmdChangeView(II)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method private native _cmdEndDeviceTime(II)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method private native _cmdPause(II)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method private native _cmdResume(II)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method private native _prepare()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method private native _setServerInputAudioEncoder(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method private native _setServerInputFormat(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method private native _setServerInputVideoEncoder(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method private native _setServerInputVideoFrameRate(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method private native _setServerInputVideoSize(II)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method private native _setServerSceneChangeEffect(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation
.end method

.method private native _start()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method private native _stop()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method static synthetic access$000(Lcom/sec/android/secmarcoa/SecMarCoa;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/secmarcoa/SecMarCoa;

    .prologue
    .line 58
    iget v0, p0, Lcom/sec/android/secmarcoa/SecMarCoa;->mNativeContext:I

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/secmarcoa/SecMarCoa;)Lcom/sec/android/secmarcoa/SecMarCoa$OnErrorListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/secmarcoa/SecMarCoa;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/secmarcoa/SecMarCoa;->mOnErrorListener:Lcom/sec/android/secmarcoa/SecMarCoa$OnErrorListener;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/secmarcoa/SecMarCoa;Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;)Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/secmarcoa/SecMarCoa;
    .param p1, "x1"    # Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    .prologue
    .line 58
    iput-object p1, p0, Lcom/sec/android/secmarcoa/SecMarCoa;->mCurrentState:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    return-object p1
.end method

.method static synthetic access$300(Lcom/sec/android/secmarcoa/SecMarCoa;)Lcom/sec/android/secmarcoa/SecMarCoa$OnInfoListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/secmarcoa/SecMarCoa;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/secmarcoa/SecMarCoa;->mOnInfoListener:Lcom/sec/android/secmarcoa/SecMarCoa$OnInfoListener;

    return-object v0
.end method

.method private final native native_finalize()V
.end method

.method private static final native native_init()V
.end method

.method private native native_reset()V
.end method

.method private final native native_setup(Ljava/lang/Object;I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method private static postEventFromNative(Ljava/lang/Object;IIILjava/lang/Object;)V
    .locals 5
    .param p0, "secmarcoa_ref"    # Ljava/lang/Object;
    .param p1, "what"    # I
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "obj"    # Ljava/lang/Object;

    .prologue
    .line 350
    const-string v2, "SecMarCoa"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "postEventFromNative enter "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 351
    check-cast p0, Ljava/lang/ref/WeakReference;

    .end local p0    # "secmarcoa_ref":Ljava/lang/Object;
    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/secmarcoa/SecMarCoa;

    .line 352
    .local v0, "coa":Lcom/sec/android/secmarcoa/SecMarCoa;
    if-nez v0, :cond_1

    .line 360
    :cond_0
    :goto_0
    return-void

    .line 356
    :cond_1
    iget-object v2, v0, Lcom/sec/android/secmarcoa/SecMarCoa;->mEventHandler:Lcom/sec/android/secmarcoa/SecMarCoa$EventHandler;

    if-eqz v2, :cond_0

    .line 357
    iget-object v2, v0, Lcom/sec/android/secmarcoa/SecMarCoa;->mEventHandler:Lcom/sec/android/secmarcoa/SecMarCoa$EventHandler;

    invoke-virtual {v2, p1, p2, p3, p4}, Lcom/sec/android/secmarcoa/SecMarCoa$EventHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 358
    .local v1, "m":Landroid/os/Message;
    iget-object v2, v0, Lcom/sec/android/secmarcoa/SecMarCoa;->mEventHandler:Lcom/sec/android/secmarcoa/SecMarCoa$EventHandler;

    invoke-virtual {v2, v1}, Lcom/sec/android/secmarcoa/SecMarCoa$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private final native sendcommand(IIII)V
.end method

.method private native setParameter(Ljava/lang/String;)V
.end method


# virtual methods
.method public cancel()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 751
    const-string v0, "SecMarCoa"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "cancel, current State["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/secmarcoa/SecMarCoa;->mCurrentState:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    invoke-virtual {v2}, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 752
    sget-object v0, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;->STOPPING:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    iput-object v0, p0, Lcom/sec/android/secmarcoa/SecMarCoa;->mCurrentState:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    .line 753
    invoke-direct {p0}, Lcom/sec/android/secmarcoa/SecMarCoa;->_cancel()V

    .line 754
    sget-object v0, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;->STOPPED:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    iput-object v0, p0, Lcom/sec/android/secmarcoa/SecMarCoa;->mCurrentState:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    .line 755
    return-void
.end method

.method public cmdChangeView(II)Z
    .locals 5
    .param p1, "device"    # I
    .param p2, "time"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 669
    const-string v2, "SecMarCoa"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "cmdChangeView : time ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] device ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 670
    iget v2, p0, Lcom/sec/android/secmarcoa/SecMarCoa;->mLastCamChangeTime:I

    if-lt v2, p2, :cond_0

    .line 671
    const-string v1, "SecMarCoa"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cmdChangeView failed : last : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/secmarcoa/SecMarCoa;->mLastCamChangeTime:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " curr : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 692
    :goto_0
    return v0

    .line 675
    :cond_0
    iget-object v2, p0, Lcom/sec/android/secmarcoa/SecMarCoa;->mCurrentState:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    sget-object v3, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;->PAUSED:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    if-ne v2, v3, :cond_1

    .line 676
    mul-int/lit16 v0, p2, 0x3e8

    mul-int/lit16 v0, v0, 0x3e8

    invoke-direct {p0, p1, v0}, Lcom/sec/android/secmarcoa/SecMarCoa;->_cmdResume(II)V

    .line 677
    sget-object v0, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;->STARTDONE:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    iput-object v0, p0, Lcom/sec/android/secmarcoa/SecMarCoa;->mCurrentState:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    :goto_1
    move v0, v1

    .line 692
    goto :goto_0

    .line 678
    :cond_1
    iget-object v2, p0, Lcom/sec/android/secmarcoa/SecMarCoa;->mCurrentState:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    sget-object v3, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;->STARTDONE:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    if-eq v2, v3, :cond_2

    iget-object v2, p0, Lcom/sec/android/secmarcoa/SecMarCoa;->mCurrentState:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    sget-object v3, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;->START:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    if-ne v2, v3, :cond_4

    .line 679
    :cond_2
    iget v0, p0, Lcom/sec/android/secmarcoa/SecMarCoa;->mLastCamChangeDevice:I

    if-ne v0, p1, :cond_3

    .line 680
    const-string v0, "SecMarCoa"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cmdChangeView ignored : device same - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/secmarcoa/SecMarCoa;->mLastCamChangeDevice:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 681
    goto :goto_0

    .line 683
    :cond_3
    mul-int/lit16 v0, p2, 0x3e8

    mul-int/lit16 v0, v0, 0x3e8

    invoke-direct {p0, p1, v0}, Lcom/sec/android/secmarcoa/SecMarCoa;->_cmdChangeView(II)V

    .line 684
    iput p1, p0, Lcom/sec/android/secmarcoa/SecMarCoa;->mLastCamChangeDevice:I

    .line 685
    iput p2, p0, Lcom/sec/android/secmarcoa/SecMarCoa;->mLastCamChangeTime:I

    goto :goto_1

    .line 687
    :cond_4
    const-string v1, "SecMarCoa"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cmdChangeView ignored : invalid cunnent State - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/secmarcoa/SecMarCoa;->mCurrentState:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    invoke-virtual {v3}, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public native cmdClientDisconnected(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method public cmdEndDeviceTime(II)Z
    .locals 4
    .param p1, "device"    # I
    .param p2, "time"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 701
    const-string v1, "SecMarCoa"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cmdEndDeviceTime : time ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] device ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 702
    iget-object v1, p0, Lcom/sec/android/secmarcoa/SecMarCoa;->mCurrentState:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    sget-object v2, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;->STARTDONE:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    if-ne v1, v2, :cond_2

    .line 703
    iget v1, p0, Lcom/sec/android/secmarcoa/SecMarCoa;->mLastCamChangeTime:I

    if-ne v1, p2, :cond_1

    .line 704
    const-string v0, "SecMarCoa"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "cmdEndDeviceTime failed : last : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/secmarcoa/SecMarCoa;->mLastCamChangeTime:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " curr : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 709
    :cond_0
    mul-int/lit16 v0, p2, 0x3e8

    mul-int/lit16 v0, v0, 0x3e8

    invoke-direct {p0, p1, v0}, Lcom/sec/android/secmarcoa/SecMarCoa;->_cmdEndDeviceTime(II)V

    .line 717
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0

    .line 705
    :cond_1
    iget v1, p0, Lcom/sec/android/secmarcoa/SecMarCoa;->mLastCamChangeTime:I

    if-le v1, p2, :cond_0

    .line 706
    const-string v1, "SecMarCoa"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cmdEndDeviceTime failed : last : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/secmarcoa/SecMarCoa;->mLastCamChangeTime:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " curr : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 710
    :cond_2
    iget-object v1, p0, Lcom/sec/android/secmarcoa/SecMarCoa;->mCurrentState:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    sget-object v2, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;->PAUSED:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    if-ne v1, v2, :cond_3

    .line 711
    mul-int/lit16 v0, p2, 0x3e8

    mul-int/lit16 v0, v0, 0x3e8

    invoke-direct {p0, p1, v0}, Lcom/sec/android/secmarcoa/SecMarCoa;->_cmdEndDeviceTime(II)V

    goto :goto_0

    .line 713
    :cond_3
    const-string v1, "SecMarCoa"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cmdChangeView ignored : invalid cunnent State - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/secmarcoa/SecMarCoa;->mCurrentState:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    invoke-virtual {v3}, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public native cmdNetStatusLevel(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method public cmdPauseView(II)Z
    .locals 4
    .param p1, "device"    # I
    .param p2, "time"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 637
    iget-object v2, p0, Lcom/sec/android/secmarcoa/SecMarCoa;->mCurrentState:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    sget-object v3, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;->PAUSED:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    if-ne v2, v3, :cond_0

    .line 638
    const-string v1, "SecMarCoa"

    const-string v2, "cmdPauseView ignored : coa already paused"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 658
    :goto_0
    return v0

    .line 640
    :cond_0
    iget-object v2, p0, Lcom/sec/android/secmarcoa/SecMarCoa;->mCurrentState:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    sget-object v3, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;->STARTDONE:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    if-ne v2, v3, :cond_3

    .line 641
    iget v2, p0, Lcom/sec/android/secmarcoa/SecMarCoa;->mLastCamChangeTime:I

    if-lt v2, p2, :cond_1

    .line 642
    const-string v0, "SecMarCoa"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cmdPauseView failed : last : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/secmarcoa/SecMarCoa;->mLastCamChangeTime:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " curr : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 643
    goto :goto_0

    .line 645
    :cond_1
    iget v2, p0, Lcom/sec/android/secmarcoa/SecMarCoa;->mLastCamChangeDevice:I

    if-eq v2, p1, :cond_2

    .line 646
    const-string v0, "SecMarCoa"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cmdPauseView failed : last : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/secmarcoa/SecMarCoa;->mLastCamChangeDevice:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " curr : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 647
    goto :goto_0

    .line 650
    :cond_2
    mul-int/lit16 v1, p2, 0x3e8

    mul-int/lit16 v1, v1, 0x3e8

    invoke-direct {p0, p1, v1}, Lcom/sec/android/secmarcoa/SecMarCoa;->_cmdPause(II)V

    .line 651
    sget-object v1, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;->PAUSED:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    iput-object v1, p0, Lcom/sec/android/secmarcoa/SecMarCoa;->mCurrentState:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    .line 652
    iput p1, p0, Lcom/sec/android/secmarcoa/SecMarCoa;->mLastCamChangeDevice:I

    .line 653
    iput p2, p0, Lcom/sec/android/secmarcoa/SecMarCoa;->mLastCamChangeTime:I

    goto :goto_0

    .line 656
    :cond_3
    const-string v0, "SecMarCoa"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cmdPauseView ignored : invalid cunnent State - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/secmarcoa/SecMarCoa;->mCurrentState:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    invoke-virtual {v3}, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 658
    goto/16 :goto_0
.end method

.method protected finalize()V
    .locals 0

    .prologue
    .line 782
    invoke-direct {p0}, Lcom/sec/android/secmarcoa/SecMarCoa;->native_finalize()V

    return-void
.end method

.method public native getCoaVersion()I
.end method

.method public prepare()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 603
    const-string v0, "SecMarCoa"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "prepare, current State["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/secmarcoa/SecMarCoa;->mCurrentState:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    invoke-virtual {v2}, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 604
    iget-object v0, p0, Lcom/sec/android/secmarcoa/SecMarCoa;->mCurrentState:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    sget-object v1, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;->INITIALIZED:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    if-ne v0, v1, :cond_0

    .line 605
    sget-object v0, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;->PREPARE:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    iput-object v0, p0, Lcom/sec/android/secmarcoa/SecMarCoa;->mCurrentState:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    .line 606
    invoke-direct {p0}, Lcom/sec/android/secmarcoa/SecMarCoa;->_prepare()V

    .line 607
    sget-object v0, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;->PREPAREDONE:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    iput-object v0, p0, Lcom/sec/android/secmarcoa/SecMarCoa;->mCurrentState:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    .line 612
    return-void

    .line 609
    :cond_0
    const-string v0, "SecMarCoa"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "prepare failed : coa state : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/secmarcoa/SecMarCoa;->mCurrentState:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    invoke-virtual {v2}, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 610
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "invalid coa state"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public native release()V
.end method

.method public reset()V
    .locals 3

    .prologue
    .line 760
    const-string v0, "SecMarCoa"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "reset, current State["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/secmarcoa/SecMarCoa;->mCurrentState:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    invoke-virtual {v2}, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 761
    invoke-direct {p0}, Lcom/sec/android/secmarcoa/SecMarCoa;->native_reset()V

    .line 764
    iget-object v0, p0, Lcom/sec/android/secmarcoa/SecMarCoa;->mEventHandler:Lcom/sec/android/secmarcoa/SecMarCoa$EventHandler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/secmarcoa/SecMarCoa$EventHandler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 765
    sget-object v0, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;->IDLE:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    iput-object v0, p0, Lcom/sec/android/secmarcoa/SecMarCoa;->mCurrentState:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    .line 766
    return-void
.end method

.method public native setClientIP(ILjava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation
.end method

.method public native setClientIndexFile(Ljava/io/FileDescriptor;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public setClientIndexFile(Ljava/lang/String;)V
    .locals 3
    .param p1, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 575
    if-eqz p1, :cond_0

    .line 576
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 578
    .local v0, "fosorg":Ljava/io/FileInputStream;
    :try_start_0
    invoke-virtual {v0}, Ljava/io/FileInputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/secmarcoa/SecMarCoa;->setClientIndexFile(Ljava/io/FileDescriptor;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 580
    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V

    .line 585
    return-void

    .line 580
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V

    throw v1

    .line 583
    .end local v0    # "fosorg":Ljava/io/FileInputStream;
    :cond_0
    new-instance v1, Ljava/io/IOException;

    const-string v2, "No valid output file"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public native setClientInputFile(Ljava/io/FileDescriptor;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public setClientInputFile(Ljava/lang/String;)V
    .locals 3
    .param p1, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 555
    if-eqz p1, :cond_0

    .line 556
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 558
    .local v0, "fosorg":Ljava/io/FileInputStream;
    :try_start_0
    invoke-virtual {v0}, Ljava/io/FileInputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/secmarcoa/SecMarCoa;->setClientInputFile(Ljava/io/FileDescriptor;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 560
    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V

    .line 565
    return-void

    .line 560
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V

    throw v1

    .line 563
    .end local v0    # "fosorg":Ljava/io/FileInputStream;
    :cond_0
    new-instance v1, Ljava/io/IOException;

    const-string v2, "No valid output file"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public setMasterLocation(FF)V
    .locals 9
    .param p1, "latitude"    # F
    .param p2, "longitude"    # F

    .prologue
    const v8, 0x461c4000    # 10000.0f

    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    .line 422
    mul-float v3, p1, v8

    float-to-double v4, v3

    add-double/2addr v4, v6

    double-to-int v0, v4

    .line 423
    .local v0, "latitudex10000":I
    mul-float v3, p2, v8

    float-to-double v4, v3

    add-double/2addr v4, v6

    double-to-int v1, v4

    .line 425
    .local v1, "longitudex10000":I
    const v3, 0xdbba0

    if-gt v0, v3, :cond_0

    const v3, -0xdbba0

    if-ge v0, v3, :cond_1

    .line 426
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Latitude: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " out of range."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 427
    .local v2, "msg":Ljava/lang/String;
    new-instance v3, Ljava/lang/IllegalArgumentException;

    invoke-direct {v3, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 429
    .end local v2    # "msg":Ljava/lang/String;
    :cond_1
    const v3, 0x1b7740

    if-gt v1, v3, :cond_2

    const v3, -0x1b7740

    if-ge v1, v3, :cond_3

    .line 430
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Longitude: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " out of range"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 431
    .restart local v2    # "msg":Ljava/lang/String;
    new-instance v3, Ljava/lang/IllegalArgumentException;

    invoke-direct {v3, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 434
    .end local v2    # "msg":Ljava/lang/String;
    :cond_3
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "param-geotag-latitude="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/android/secmarcoa/SecMarCoa;->setParameter(Ljava/lang/String;)V

    .line 435
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "param-geotag-longitude="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/android/secmarcoa/SecMarCoa;->setParameter(Ljava/lang/String;)V

    .line 436
    return-void
.end method

.method public setOnErrorListener(Lcom/sec/android/secmarcoa/SecMarCoa$OnErrorListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/secmarcoa/SecMarCoa$OnErrorListener;

    .prologue
    .line 162
    iput-object p1, p0, Lcom/sec/android/secmarcoa/SecMarCoa;->mOnErrorListener:Lcom/sec/android/secmarcoa/SecMarCoa$OnErrorListener;

    .line 163
    return-void
.end method

.method public setOnInfoListener(Lcom/sec/android/secmarcoa/SecMarCoa$OnInfoListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/secmarcoa/SecMarCoa$OnInfoListener;

    .prologue
    .line 275
    iput-object p1, p0, Lcom/sec/android/secmarcoa/SecMarCoa;->mOnInfoListener:Lcom/sec/android/secmarcoa/SecMarCoa$OnInfoListener;

    .line 276
    return-void
.end method

.method public native setServerIP(Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation
.end method

.method public setServerInputAudioBitRate(I)V
    .locals 2
    .param p1, "bitRate"    # I

    .prologue
    .line 450
    if-gtz p1, :cond_0

    .line 451
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Audio encoding bit rate is not positive"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 453
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "audio-param-encoding-bitrate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/secmarcoa/SecMarCoa;->setParameter(Ljava/lang/String;)V

    .line 454
    return-void
.end method

.method public setServerInputAudioChannels(I)V
    .locals 2
    .param p1, "numChannels"    # I

    .prologue
    .line 459
    if-gtz p1, :cond_0

    .line 460
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Number of channels is not positive"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 462
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "audio-param-number-of-channels="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/secmarcoa/SecMarCoa;->setParameter(Ljava/lang/String;)V

    .line 463
    return-void
.end method

.method public setServerInputAudioEncoder(I)V
    .locals 0
    .param p1, "audio_encoder"    # I

    .prologue
    .line 479
    invoke-direct {p0, p1}, Lcom/sec/android/secmarcoa/SecMarCoa;->_setServerInputAudioEncoder(I)V

    .line 480
    return-void
.end method

.method public setServerInputAudioSamplingRate(I)V
    .locals 2
    .param p1, "samplingRate"    # I

    .prologue
    .line 468
    if-gtz p1, :cond_0

    .line 469
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Audio sampling rate is not positive"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 471
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "audio-param-sampling-rate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/secmarcoa/SecMarCoa;->setParameter(Ljava/lang/String;)V

    .line 472
    return-void
.end method

.method public setServerInputAuthor(I)V
    .locals 2
    .param p1, "author"    # I

    .prologue
    .line 441
    const/4 v0, 0x5

    if-eq p1, v0, :cond_0

    .line 442
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "author is invalid value"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 444
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "param-meta-author="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/secmarcoa/SecMarCoa;->setParameter(Ljava/lang/String;)V

    .line 445
    return-void
.end method

.method public setServerInputBGMFile(Ljava/io/FileDescriptor;)V
    .locals 6
    .param p1, "fd"    # Ljava/io/FileDescriptor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v2, 0x0

    .line 543
    move-object v0, p0

    move-object v1, p1

    move-wide v4, v2

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/secmarcoa/SecMarCoa;->setServerInputBGMFile(Ljava/io/FileDescriptor;JJ)V

    .line 544
    return-void
.end method

.method public native setServerInputBGMFile(Ljava/io/FileDescriptor;JJ)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public setServerInputBGMFile(Ljava/lang/String;)V
    .locals 8
    .param p1, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 528
    if-eqz p1, :cond_0

    .line 529
    new-instance v6, Ljava/io/File;

    invoke-direct {v6, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 530
    .local v6, "bgmFile":Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->length()J

    move-result-wide v4

    .line 531
    .local v4, "size":J
    new-instance v7, Ljava/io/FileInputStream;

    invoke-direct {v7, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 533
    .local v7, "fosorg":Ljava/io/FileInputStream;
    :try_start_0
    invoke-virtual {v7}, Ljava/io/FileInputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v1

    const-wide/16 v2, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/secmarcoa/SecMarCoa;->setServerInputBGMFile(Ljava/io/FileDescriptor;JJ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 535
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V

    .line 540
    return-void

    .line 535
    :catchall_0
    move-exception v0

    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V

    throw v0

    .line 538
    .end local v4    # "size":J
    .end local v6    # "bgmFile":Ljava/io/File;
    .end local v7    # "fosorg":Ljava/io/FileInputStream;
    :cond_0
    new-instance v0, Ljava/io/IOException;

    const-string v1, "BGM file is not valid"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setServerInputFormat(I)V
    .locals 0
    .param p1, "output_format"    # I

    .prologue
    .line 382
    invoke-direct {p0, p1}, Lcom/sec/android/secmarcoa/SecMarCoa;->_setServerInputFormat(I)V

    .line 383
    return-void
.end method

.method public setServerInputVideoBitRate(I)V
    .locals 2
    .param p1, "bitRate"    # I

    .prologue
    .line 405
    if-gtz p1, :cond_0

    .line 406
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Video encoding bit rate is not positive"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 408
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "video-param-encoding-bitrate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/secmarcoa/SecMarCoa;->setParameter(Ljava/lang/String;)V

    .line 409
    return-void
.end method

.method public setServerInputVideoEncoder(I)V
    .locals 0
    .param p1, "video_encoder"    # I

    .prologue
    .line 416
    invoke-direct {p0, p1}, Lcom/sec/android/secmarcoa/SecMarCoa;->_setServerInputVideoEncoder(I)V

    .line 417
    return-void
.end method

.method public setServerInputVideoFrameRate(I)V
    .locals 0
    .param p1, "rate"    # I

    .prologue
    .line 390
    invoke-direct {p0, p1}, Lcom/sec/android/secmarcoa/SecMarCoa;->_setServerInputVideoFrameRate(I)V

    .line 391
    return-void
.end method

.method public setServerInputVideoSize(II)V
    .locals 0
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 398
    invoke-direct {p0, p1, p2}, Lcom/sec/android/secmarcoa/SecMarCoa;->_setServerInputVideoSize(II)V

    .line 399
    return-void
.end method

.method public setServerInput_Profile(Landroid/media/CamcorderProfile;)V
    .locals 2
    .param p1, "profile"    # Landroid/media/CamcorderProfile;

    .prologue
    .line 366
    iget v0, p1, Landroid/media/CamcorderProfile;->fileFormat:I

    invoke-virtual {p0, v0}, Lcom/sec/android/secmarcoa/SecMarCoa;->setServerInputFormat(I)V

    .line 367
    iget v0, p1, Landroid/media/CamcorderProfile;->videoFrameRate:I

    invoke-virtual {p0, v0}, Lcom/sec/android/secmarcoa/SecMarCoa;->setServerInputVideoFrameRate(I)V

    .line 368
    iget v0, p1, Landroid/media/CamcorderProfile;->videoFrameWidth:I

    iget v1, p1, Landroid/media/CamcorderProfile;->videoFrameHeight:I

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/secmarcoa/SecMarCoa;->setServerInputVideoSize(II)V

    .line 369
    iget v0, p1, Landroid/media/CamcorderProfile;->videoBitRate:I

    invoke-virtual {p0, v0}, Lcom/sec/android/secmarcoa/SecMarCoa;->setServerInputVideoBitRate(I)V

    .line 370
    iget v0, p1, Landroid/media/CamcorderProfile;->videoCodec:I

    invoke-virtual {p0, v0}, Lcom/sec/android/secmarcoa/SecMarCoa;->setServerInputVideoEncoder(I)V

    .line 371
    iget v0, p1, Landroid/media/CamcorderProfile;->audioBitRate:I

    invoke-virtual {p0, v0}, Lcom/sec/android/secmarcoa/SecMarCoa;->setServerInputAudioBitRate(I)V

    .line 372
    iget v0, p1, Landroid/media/CamcorderProfile;->audioChannels:I

    invoke-virtual {p0, v0}, Lcom/sec/android/secmarcoa/SecMarCoa;->setServerInputAudioChannels(I)V

    .line 373
    iget v0, p1, Landroid/media/CamcorderProfile;->audioSampleRate:I

    invoke-virtual {p0, v0}, Lcom/sec/android/secmarcoa/SecMarCoa;->setServerInputAudioSamplingRate(I)V

    .line 374
    iget v0, p1, Landroid/media/CamcorderProfile;->audioCodec:I

    invoke-virtual {p0, v0}, Lcom/sec/android/secmarcoa/SecMarCoa;->setServerInputAudioEncoder(I)V

    .line 375
    return-void
.end method

.method public native setServerOutputFile(Ljava/io/FileDescriptor;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public setServerOutputFile(Ljava/lang/String;)V
    .locals 3
    .param p1, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 510
    if-eqz p1, :cond_0

    .line 511
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 513
    .local v0, "fosorg":Ljava/io/FileOutputStream;
    :try_start_0
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/secmarcoa/SecMarCoa;->setServerOutputFile(Ljava/io/FileDescriptor;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 515
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V

    .line 520
    return-void

    .line 515
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V

    throw v1

    .line 518
    .end local v0    # "fosorg":Ljava/io/FileOutputStream;
    :cond_0
    new-instance v1, Ljava/io/IOException;

    const-string v2, "No valid output file"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public setServerOutputOrientationInfo(I)V
    .locals 3
    .param p1, "degrees"    # I

    .prologue
    .line 485
    if-eqz p1, :cond_0

    const/16 v0, 0x5a

    if-eq p1, v0, :cond_0

    const/16 v0, 0xb4

    if-eq p1, v0, :cond_0

    const/16 v0, 0x10e

    if-eq p1, v0, :cond_0

    .line 489
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported angle: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 491
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "output-orientation-info="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/secmarcoa/SecMarCoa;->setParameter(Ljava/lang/String;)V

    .line 492
    return-void
.end method

.method public setServerSceneChangeEffect(Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;)V
    .locals 3
    .param p1, "effectType"    # Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 593
    const-string v0, "SecMarCoa"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setServerSceneChangeEffect : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;->ordinal()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 594
    invoke-virtual {p1}, Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;->ordinal()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/sec/android/secmarcoa/SecMarCoa;->_setServerSceneChangeEffect(I)V

    .line 595
    return-void
.end method

.method public start()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 620
    const-string v0, "SecMarCoa"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "start, current State["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/secmarcoa/SecMarCoa;->mCurrentState:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    invoke-virtual {v2}, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 621
    iget-object v0, p0, Lcom/sec/android/secmarcoa/SecMarCoa;->mCurrentState:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    sget-object v1, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;->PREPAREDONE:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    if-ne v0, v1, :cond_0

    .line 622
    sget-object v0, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;->STARTING:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    iput-object v0, p0, Lcom/sec/android/secmarcoa/SecMarCoa;->mCurrentState:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    .line 623
    invoke-direct {p0}, Lcom/sec/android/secmarcoa/SecMarCoa;->_start()V

    .line 624
    sget-object v0, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;->START:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    iput-object v0, p0, Lcom/sec/android/secmarcoa/SecMarCoa;->mCurrentState:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    .line 629
    return-void

    .line 626
    :cond_0
    const-string v0, "SecMarCoa"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "start failed : coa state : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/secmarcoa/SecMarCoa;->mCurrentState:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    invoke-virtual {v2}, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 627
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "invalid coa state"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public stop()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 733
    const-string v0, "SecMarCoa"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "stop, current State["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/secmarcoa/SecMarCoa;->mCurrentState:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    invoke-virtual {v2}, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 734
    iget-object v0, p0, Lcom/sec/android/secmarcoa/SecMarCoa;->mCurrentState:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    sget-object v1, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;->STARTDONE:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/secmarcoa/SecMarCoa;->mCurrentState:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    sget-object v1, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;->START:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/secmarcoa/SecMarCoa;->mCurrentState:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    sget-object v1, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;->PAUSED:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    if-ne v0, v1, :cond_1

    .line 737
    :cond_0
    sget-object v0, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;->STOPPING:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    iput-object v0, p0, Lcom/sec/android/secmarcoa/SecMarCoa;->mCurrentState:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    .line 738
    invoke-direct {p0}, Lcom/sec/android/secmarcoa/SecMarCoa;->_stop()V

    .line 739
    sget-object v0, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;->STOPPED:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    iput-object v0, p0, Lcom/sec/android/secmarcoa/SecMarCoa;->mCurrentState:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    .line 744
    return-void

    .line 741
    :cond_1
    const-string v0, "SecMarCoa"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "stop failed : coa state : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/secmarcoa/SecMarCoa;->mCurrentState:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    invoke-virtual {v2}, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 742
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "invalid coa state"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
