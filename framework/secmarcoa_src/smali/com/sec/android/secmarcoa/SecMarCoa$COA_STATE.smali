.class final enum Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;
.super Ljava/lang/Enum;
.source "SecMarCoa.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/secmarcoa/SecMarCoa;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "COA_STATE"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

.field public static final enum IDLE:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

.field public static final enum INITIALIZED:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

.field public static final enum MAX:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

.field public static final enum PAUSED:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

.field public static final enum PREPARE:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

.field public static final enum PREPAREDONE:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

.field public static final enum START:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

.field public static final enum STARTDONE:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

.field public static final enum STARTING:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

.field public static final enum STOPPED:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

.field public static final enum STOPPING:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 92
    new-instance v0, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    const-string v1, "IDLE"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;->IDLE:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    .line 93
    new-instance v0, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    const-string v1, "INITIALIZED"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;->INITIALIZED:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    .line 94
    new-instance v0, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    const-string v1, "PREPARE"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;->PREPARE:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    .line 95
    new-instance v0, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    const-string v1, "PREPAREDONE"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;->PREPAREDONE:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    .line 96
    new-instance v0, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    const-string v1, "STARTING"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;->STARTING:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    .line 97
    new-instance v0, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    const-string v1, "START"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;->START:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    .line 98
    new-instance v0, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    const-string v1, "STARTDONE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;->STARTDONE:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    .line 99
    new-instance v0, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    const-string v1, "PAUSED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;->PAUSED:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    .line 100
    new-instance v0, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    const-string v1, "STOPPING"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;->STOPPING:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    .line 101
    new-instance v0, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    const-string v1, "STOPPED"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;->STOPPED:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    .line 102
    new-instance v0, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    const-string v1, "MAX"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;->MAX:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    .line 91
    const/16 v0, 0xb

    new-array v0, v0, [Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    sget-object v1, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;->IDLE:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;->INITIALIZED:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;->PREPARE:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;->PREPAREDONE:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;->STARTING:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;->START:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;->STARTDONE:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;->PAUSED:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;->STOPPING:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;->STOPPED:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;->MAX:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;->$VALUES:[Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 91
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 91
    const-class v0, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;
    .locals 1

    .prologue
    .line 91
    sget-object v0, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;->$VALUES:[Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    invoke-virtual {v0}, [Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    return-object v0
.end method
