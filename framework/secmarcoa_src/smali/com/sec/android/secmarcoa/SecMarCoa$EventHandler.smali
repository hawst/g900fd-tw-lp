.class Lcom/sec/android/secmarcoa/SecMarCoa$EventHandler;
.super Landroid/os/Handler;
.source "SecMarCoa.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/secmarcoa/SecMarCoa;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EventHandler"
.end annotation


# static fields
.field private static final MAR_COA_EVENT_ERROR:I = 0x1

.field private static final MAR_COA_EVENT_INFO:I = 0x2

.field private static final MAR_COA_EVENT_LIST_END:I = 0x63

.field private static final MAR_COA_EVENT_LIST_START:I = 0x1

.field private static final MAR_COA_TRACK_EVENT_ERROR:I = 0x64

.field private static final MAR_COA_TRACK_EVENT_INFO:I = 0x65

.field private static final MAR_COA_TRACK_EVENT_LIST_END:I = 0x3e8

.field private static final MAR_COA_TRACK_EVENT_LIST_START:I = 0x64


# instance fields
.field private mSecMarCoa:Lcom/sec/android/secmarcoa/SecMarCoa;

.field final synthetic this$0:Lcom/sec/android/secmarcoa/SecMarCoa;


# direct methods
.method public constructor <init>(Lcom/sec/android/secmarcoa/SecMarCoa;Lcom/sec/android/secmarcoa/SecMarCoa;Landroid/os/Looper;)V
    .locals 0
    .param p2, "coa"    # Lcom/sec/android/secmarcoa/SecMarCoa;
    .param p3, "looper"    # Landroid/os/Looper;

    .prologue
    .line 282
    iput-object p1, p0, Lcom/sec/android/secmarcoa/SecMarCoa$EventHandler;->this$0:Lcom/sec/android/secmarcoa/SecMarCoa;

    .line 283
    invoke-direct {p0, p3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 284
    iput-object p2, p0, Lcom/sec/android/secmarcoa/SecMarCoa$EventHandler;->mSecMarCoa:Lcom/sec/android/secmarcoa/SecMarCoa;

    .line 285
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 304
    const-string v0, "SecMarCoa"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SecMarCoa handleMessage"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 305
    iget-object v0, p0, Lcom/sec/android/secmarcoa/SecMarCoa$EventHandler;->mSecMarCoa:Lcom/sec/android/secmarcoa/SecMarCoa;

    # getter for: Lcom/sec/android/secmarcoa/SecMarCoa;->mNativeContext:I
    invoke-static {v0}, Lcom/sec/android/secmarcoa/SecMarCoa;->access$000(Lcom/sec/android/secmarcoa/SecMarCoa;)I

    move-result v0

    if-nez v0, :cond_1

    .line 306
    const-string v0, "SecMarCoa"

    const-string v1, "SecMarCoa went away with unhandled events"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 335
    :cond_0
    :goto_0
    return-void

    .line 309
    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 334
    const-string v0, "SecMarCoa"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown message type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 312
    :sswitch_0
    iget-object v0, p0, Lcom/sec/android/secmarcoa/SecMarCoa$EventHandler;->this$0:Lcom/sec/android/secmarcoa/SecMarCoa;

    # getter for: Lcom/sec/android/secmarcoa/SecMarCoa;->mOnErrorListener:Lcom/sec/android/secmarcoa/SecMarCoa$OnErrorListener;
    invoke-static {v0}, Lcom/sec/android/secmarcoa/SecMarCoa;->access$100(Lcom/sec/android/secmarcoa/SecMarCoa;)Lcom/sec/android/secmarcoa/SecMarCoa$OnErrorListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 313
    iget-object v0, p0, Lcom/sec/android/secmarcoa/SecMarCoa$EventHandler;->this$0:Lcom/sec/android/secmarcoa/SecMarCoa;

    # getter for: Lcom/sec/android/secmarcoa/SecMarCoa;->mOnErrorListener:Lcom/sec/android/secmarcoa/SecMarCoa$OnErrorListener;
    invoke-static {v0}, Lcom/sec/android/secmarcoa/SecMarCoa;->access$100(Lcom/sec/android/secmarcoa/SecMarCoa;)Lcom/sec/android/secmarcoa/SecMarCoa$OnErrorListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/secmarcoa/SecMarCoa$EventHandler;->mSecMarCoa:Lcom/sec/android/secmarcoa/SecMarCoa;

    iget v2, p1, Landroid/os/Message;->arg1:I

    iget v3, p1, Landroid/os/Message;->arg2:I

    invoke-interface {v0, v1, v2, v3}, Lcom/sec/android/secmarcoa/SecMarCoa$OnErrorListener;->onError(Lcom/sec/android/secmarcoa/SecMarCoa;II)V

    goto :goto_0

    .line 318
    :sswitch_1
    iget v0, p1, Landroid/os/Message;->arg1:I

    const/16 v1, 0x388

    if-ne v0, v1, :cond_2

    .line 319
    iget-object v0, p0, Lcom/sec/android/secmarcoa/SecMarCoa$EventHandler;->this$0:Lcom/sec/android/secmarcoa/SecMarCoa;

    sget-object v1, Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;->STARTDONE:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    # setter for: Lcom/sec/android/secmarcoa/SecMarCoa;->mCurrentState:Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;
    invoke-static {v0, v1}, Lcom/sec/android/secmarcoa/SecMarCoa;->access$202(Lcom/sec/android/secmarcoa/SecMarCoa;Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;)Lcom/sec/android/secmarcoa/SecMarCoa$COA_STATE;

    .line 322
    :cond_2
    iget-object v0, p0, Lcom/sec/android/secmarcoa/SecMarCoa$EventHandler;->this$0:Lcom/sec/android/secmarcoa/SecMarCoa;

    # getter for: Lcom/sec/android/secmarcoa/SecMarCoa;->mOnInfoListener:Lcom/sec/android/secmarcoa/SecMarCoa$OnInfoListener;
    invoke-static {v0}, Lcom/sec/android/secmarcoa/SecMarCoa;->access$300(Lcom/sec/android/secmarcoa/SecMarCoa;)Lcom/sec/android/secmarcoa/SecMarCoa$OnInfoListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 323
    iget-object v0, p0, Lcom/sec/android/secmarcoa/SecMarCoa$EventHandler;->this$0:Lcom/sec/android/secmarcoa/SecMarCoa;

    # getter for: Lcom/sec/android/secmarcoa/SecMarCoa;->mOnInfoListener:Lcom/sec/android/secmarcoa/SecMarCoa$OnInfoListener;
    invoke-static {v0}, Lcom/sec/android/secmarcoa/SecMarCoa;->access$300(Lcom/sec/android/secmarcoa/SecMarCoa;)Lcom/sec/android/secmarcoa/SecMarCoa$OnInfoListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/secmarcoa/SecMarCoa$EventHandler;->mSecMarCoa:Lcom/sec/android/secmarcoa/SecMarCoa;

    iget v2, p1, Landroid/os/Message;->arg1:I

    iget v3, p1, Landroid/os/Message;->arg2:I

    invoke-interface {v0, v1, v2, v3}, Lcom/sec/android/secmarcoa/SecMarCoa$OnInfoListener;->onInfo(Lcom/sec/android/secmarcoa/SecMarCoa;II)V

    goto :goto_0

    .line 328
    :sswitch_2
    iget-object v0, p0, Lcom/sec/android/secmarcoa/SecMarCoa$EventHandler;->this$0:Lcom/sec/android/secmarcoa/SecMarCoa;

    # getter for: Lcom/sec/android/secmarcoa/SecMarCoa;->mOnInfoListener:Lcom/sec/android/secmarcoa/SecMarCoa$OnInfoListener;
    invoke-static {v0}, Lcom/sec/android/secmarcoa/SecMarCoa;->access$300(Lcom/sec/android/secmarcoa/SecMarCoa;)Lcom/sec/android/secmarcoa/SecMarCoa$OnInfoListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 329
    iget-object v0, p0, Lcom/sec/android/secmarcoa/SecMarCoa$EventHandler;->this$0:Lcom/sec/android/secmarcoa/SecMarCoa;

    # getter for: Lcom/sec/android/secmarcoa/SecMarCoa;->mOnInfoListener:Lcom/sec/android/secmarcoa/SecMarCoa$OnInfoListener;
    invoke-static {v0}, Lcom/sec/android/secmarcoa/SecMarCoa;->access$300(Lcom/sec/android/secmarcoa/SecMarCoa;)Lcom/sec/android/secmarcoa/SecMarCoa$OnInfoListener;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/secmarcoa/SecMarCoa$EventHandler;->mSecMarCoa:Lcom/sec/android/secmarcoa/SecMarCoa;

    iget v2, p1, Landroid/os/Message;->arg1:I

    iget v3, p1, Landroid/os/Message;->arg2:I

    invoke-interface {v0, v1, v2, v3}, Lcom/sec/android/secmarcoa/SecMarCoa$OnInfoListener;->onInfo(Lcom/sec/android/secmarcoa/SecMarCoa;II)V

    goto :goto_0

    .line 309
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x64 -> :sswitch_0
        0x65 -> :sswitch_2
    .end sparse-switch
.end method
