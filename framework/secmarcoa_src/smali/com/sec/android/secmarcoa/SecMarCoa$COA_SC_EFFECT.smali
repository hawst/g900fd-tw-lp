.class public final enum Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;
.super Ljava/lang/Enum;
.source "SecMarCoa.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/secmarcoa/SecMarCoa;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "COA_SC_EFFECT"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;

.field public static final enum DISSOLVE:Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;

.field public static final enum FADE_BLACK_IN_ONLY:Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;

.field public static final enum FADE_BLACK_IN_OUT:Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;

.field public static final enum FADE_GRAY_IN_OUT:Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;

.field public static final enum FADE_WHITE_IN_OUT:Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;

.field public static final enum MAX:Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;

.field public static final enum NONE:Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;

.field public static final enum SLIDEDOWN:Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 109
    new-instance v0, Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v3}, Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;->NONE:Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;

    .line 110
    new-instance v0, Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;

    const-string v1, "FADE_BLACK_IN_OUT"

    invoke-direct {v0, v1, v4}, Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;->FADE_BLACK_IN_OUT:Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;

    .line 111
    new-instance v0, Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;

    const-string v1, "FADE_GRAY_IN_OUT"

    invoke-direct {v0, v1, v5}, Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;->FADE_GRAY_IN_OUT:Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;

    .line 112
    new-instance v0, Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;

    const-string v1, "FADE_WHITE_IN_OUT"

    invoke-direct {v0, v1, v6}, Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;->FADE_WHITE_IN_OUT:Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;

    .line 113
    new-instance v0, Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;

    const-string v1, "FADE_BLACK_IN_ONLY"

    invoke-direct {v0, v1, v7}, Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;->FADE_BLACK_IN_ONLY:Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;

    .line 114
    new-instance v0, Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;

    const-string v1, "DISSOLVE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;->DISSOLVE:Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;

    .line 115
    new-instance v0, Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;

    const-string v1, "SLIDEDOWN"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;->SLIDEDOWN:Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;

    .line 116
    new-instance v0, Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;

    const-string v1, "MAX"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;->MAX:Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;

    .line 108
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;

    sget-object v1, Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;->NONE:Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;->FADE_BLACK_IN_OUT:Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;->FADE_GRAY_IN_OUT:Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;->FADE_WHITE_IN_OUT:Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;->FADE_BLACK_IN_ONLY:Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;->DISSOLVE:Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;->SLIDEDOWN:Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;->MAX:Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;->$VALUES:[Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 108
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 108
    const-class v0, Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;

    return-object v0
.end method

.method public static values()[Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;
    .locals 1

    .prologue
    .line 108
    sget-object v0, Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;->$VALUES:[Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;

    invoke-virtual {v0}, [Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/android/secmarcoa/SecMarCoa$COA_SC_EFFECT;

    return-object v0
.end method
