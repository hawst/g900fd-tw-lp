.class public Lcom/qti/ims/connectionmanager/ConnectionNativeImpl;
.super Ljava/lang/Object;
.source "ConnectionNativeImpl.java"


# instance fields
.field private featureTag:Ljava/lang/String;

.field protected mNativeContext:I


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "feature_tag"    # Ljava/lang/String;

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/qti/ims/connectionmanager/ConnectionNativeImpl;->featureTag:Ljava/lang/String;

    .line 16
    return-void
.end method


# virtual methods
.method public native addListener(Lcom/qti/ims/connectionmanager/ConnectionListener;)I
.end method

.method public native close()I
.end method

.method public native closeAllTransactions()I
.end method

.method public native closeTransaction(Ljava/lang/String;)I
.end method

.method public getFeatureTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/qti/ims/connectionmanager/ConnectionNativeImpl;->featureTag:Ljava/lang/String;

    return-object v0
.end method

.method public native removeListener(Lcom/qti/ims/connectionmanager/ConnectionListener;)I
.end method

.method public native sendMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)I
.end method
