.class public Lcom/qti/ims/connectionmanager/ConnectionManagerNativeImpl;
.super Ljava/lang/Object;
.source "ConnectionManagerNativeImpl.java"


# static fields
.field private static libraryLoaded:Z

.field private static singletonManager:Lcom/qti/ims/connectionmanager/ConnectionManagerNativeImpl;


# instance fields
.field private mNativeContext:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 13
    sput-boolean v3, Lcom/qti/ims/connectionmanager/ConnectionManagerNativeImpl;->libraryLoaded:Z

    .line 14
    const/4 v1, 0x0

    sput-object v1, Lcom/qti/ims/connectionmanager/ConnectionManagerNativeImpl;->singletonManager:Lcom/qti/ims/connectionmanager/ConnectionManagerNativeImpl;

    .line 21
    :try_start_0
    const-string v1, "CMJava"

    const-string v2, "Loading rcscmclientjni jni library"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 22
    const-string v1, "-ims-rcscmjni"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 23
    const/4 v1, 0x1

    sput-boolean v1, Lcom/qti/ims/connectionmanager/ConnectionManagerNativeImpl;->libraryLoaded:Z
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 34
    :goto_0
    return-void

    .line 25
    :catch_0
    move-exception v0

    .line 27
    .local v0, "e":Ljava/lang/UnsatisfiedLinkError;
    sput-boolean v3, Lcom/qti/ims/connectionmanager/ConnectionManagerNativeImpl;->libraryLoaded:Z

    .line 28
    const-string v1, "CMJava"

    const-string v2, "---ERROR in LOADING LIBRARY UnsatisfiedLinkerError EXCEPTION, JNI library is not loaded---"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 30
    .end local v0    # "e":Ljava/lang/UnsatisfiedLinkError;
    :catch_1
    move-exception v0

    .line 31
    .local v0, "e":Ljava/lang/Exception;
    sput-boolean v3, Lcom/qti/ims/connectionmanager/ConnectionManagerNativeImpl;->libraryLoaded:Z

    .line 32
    const-string v1, "CMJava"

    const-string v2, "---ERROR in LOADING LIBRAY EXCEPTION, JNI library is not loaded---"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    return-void
.end method

.method private native closeNative()I
.end method

.method private native createConnectionNative(Ljava/lang/Object;Ljava/lang/String;)I
.end method

.method public static startService()Lcom/qti/ims/connectionmanager/ConnectionManagerNativeImpl;
    .locals 4

    .prologue
    .line 43
    const-string v1, "CMJava"

    const-string v2, "ConnectionManagerNativeImpl startService()"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 44
    sget-object v1, Lcom/qti/ims/connectionmanager/ConnectionManagerNativeImpl;->singletonManager:Lcom/qti/ims/connectionmanager/ConnectionManagerNativeImpl;

    if-nez v1, :cond_0

    .line 47
    const-string v1, "CMJava"

    const-string v2, "ConnectionManagerNativeImpl startService() == null"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    new-instance v1, Lcom/qti/ims/connectionmanager/ConnectionManagerNativeImpl;

    invoke-direct {v1}, Lcom/qti/ims/connectionmanager/ConnectionManagerNativeImpl;-><init>()V

    sput-object v1, Lcom/qti/ims/connectionmanager/ConnectionManagerNativeImpl;->singletonManager:Lcom/qti/ims/connectionmanager/ConnectionManagerNativeImpl;

    .line 49
    sget-object v1, Lcom/qti/ims/connectionmanager/ConnectionManagerNativeImpl;->singletonManager:Lcom/qti/ims/connectionmanager/ConnectionManagerNativeImpl;

    sget-object v2, Lcom/qti/ims/connectionmanager/ConnectionManagerNativeImpl;->singletonManager:Lcom/qti/ims/connectionmanager/ConnectionManagerNativeImpl;

    invoke-direct {v1, v2}, Lcom/qti/ims/connectionmanager/ConnectionManagerNativeImpl;->startServiceNative(Ljava/lang/Object;)I

    move-result v0

    .line 51
    .local v0, "status":I
    if-eqz v0, :cond_0

    .line 53
    const-string v1, "CMJava"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "error in starting the service "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 54
    const/4 v1, 0x0

    sput-object v1, Lcom/qti/ims/connectionmanager/ConnectionManagerNativeImpl;->singletonManager:Lcom/qti/ims/connectionmanager/ConnectionManagerNativeImpl;

    .line 58
    :cond_0
    sget-object v1, Lcom/qti/ims/connectionmanager/ConnectionManagerNativeImpl;->singletonManager:Lcom/qti/ims/connectionmanager/ConnectionManagerNativeImpl;

    return-object v1
.end method

.method private native startServiceNative(Ljava/lang/Object;)I
.end method


# virtual methods
.method public native addListener(Lcom/qti/ims/connectionmanager/ConnectionManagerListener;)I
.end method

.method public close()I
    .locals 2

    .prologue
    .line 87
    const/4 v1, 0x0

    sput-object v1, Lcom/qti/ims/connectionmanager/ConnectionManagerNativeImpl;->singletonManager:Lcom/qti/ims/connectionmanager/ConnectionManagerNativeImpl;

    .line 88
    invoke-direct {p0}, Lcom/qti/ims/connectionmanager/ConnectionManagerNativeImpl;->closeNative()I

    move-result v0

    .line 90
    .local v0, "status":I
    return v0
.end method

.method public createConnection(Ljava/lang/String;)Lcom/qti/ims/connectionmanager/ConnectionNativeImpl;
    .locals 5
    .param p1, "uri"    # Ljava/lang/String;

    .prologue
    .line 69
    const-string v2, "CMJava"

    const-string v3, "ConnectionManagerNativeImpl createConnection()"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    new-instance v0, Lcom/qti/ims/connectionmanager/ConnectionNativeImpl;

    invoke-direct {v0, p1}, Lcom/qti/ims/connectionmanager/ConnectionNativeImpl;-><init>(Ljava/lang/String;)V

    .line 71
    .local v0, "connection":Lcom/qti/ims/connectionmanager/ConnectionNativeImpl;
    invoke-direct {p0, v0, p1}, Lcom/qti/ims/connectionmanager/ConnectionManagerNativeImpl;->createConnectionNative(Ljava/lang/Object;Ljava/lang/String;)I

    move-result v1

    .line 73
    .local v1, "status":I
    if-eqz v1, :cond_0

    .line 75
    const-string v2, "CMJava"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "error in creating the connection "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    const/4 v0, 0x0

    .line 79
    :cond_0
    return-object v0
.end method

.method public native getDeviceConfiguration()Lcom/qti/ims/connectionmanager/ConnectionManagerDeviceConfiguration;
.end method

.method public native getUserConfiguration()Lcom/qti/ims/connectionmanager/ConnectionManagerUserConfiguration;
.end method

.method public native initialize()I
.end method

.method public native removeListener(Lcom/qti/ims/connectionmanager/ConnectionManagerListener;)I
.end method

.method public native triggerRegistration()I
.end method
