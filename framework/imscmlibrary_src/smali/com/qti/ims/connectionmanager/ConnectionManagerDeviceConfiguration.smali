.class public Lcom/qti/ims/connectionmanager/ConnectionManagerDeviceConfiguration;
.super Ljava/lang/Object;
.source "ConnectionManagerDeviceConfiguration.java"


# instance fields
.field public AuthChallenge:Ljava/lang/String;

.field public CompactFormEnabled:Z

.field public GruuEnabled:Z

.field public IPSecEnabled:Z

.field public KeepAliveStatusEnabled:Z

.field public NonceCount:Ljava/lang/String;

.field public PCSCFClientPort:I

.field public PCSCFOldSAClientPort:I

.field public PCSCFServerPort:I

.field public SecurityVerify:Ljava/lang/String;

.field public ServiceRoute:Ljava/lang/String;

.field public SipOutBoundProxyName:Ljava/lang/String;

.field public SipOutBoundProxyPort:I

.field public UEBehindNAT:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
