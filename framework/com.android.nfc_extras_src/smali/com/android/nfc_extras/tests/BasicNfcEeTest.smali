.class public Lcom/android/nfc_extras/tests/BasicNfcEeTest;
.super Landroid/test/InstrumentationTestCase;
.source "BasicNfcEeTest.java"


# static fields
.field public static final SELECT_CARD_MANAGER_COMMAND:[B

.field public static final SELECT_CARD_MANAGER_RESPONSE:[B


# instance fields
.field private mAdapterExtras:Lcom/android/nfc_extras/NfcAdapterExtras;

.field private mContext:Landroid/content/Context;

.field private mEe:Lcom/android/nfc_extras/NfcExecutionEnvironment;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const/16 v0, 0xe

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/nfc_extras/tests/BasicNfcEeTest;->SELECT_CARD_MANAGER_COMMAND:[B

    .line 44
    const/4 v0, 0x2

    new-array v0, v0, [B

    fill-array-data v0, :array_1

    sput-object v0, Lcom/android/nfc_extras/tests/BasicNfcEeTest;->SELECT_CARD_MANAGER_RESPONSE:[B

    return-void

    .line 36
    :array_0
    .array-data 1
        0x0t
        -0x5ct
        0x4t
        0x0t
        0x8t
        -0x60t
        0x0t
        0x0t
        0x0t
        0x3t
        0x0t
        0x0t
        0x0t
        0x0t
    .end array-data

    .line 44
    nop

    :array_1
    .array-data 1
        -0x70t
        0x0t
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Landroid/test/InstrumentationTestCase;-><init>()V

    return-void
.end method

.method private static assertByteArrayEquals([B[B)V
    .locals 3
    .param p0, "b1"    # [B
    .param p1, "b2"    # [B

    .prologue
    .line 112
    array-length v1, p0

    array-length v2, p1

    invoke-static {v1, v2}, Lcom/android/nfc_extras/tests/BasicNfcEeTest;->assertEquals(II)V

    .line 113
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p0

    if-ge v0, v1, :cond_0

    .line 114
    aget-byte v1, p0, v0

    aget-byte v2, p1, v0

    invoke-static {v1, v2}, Lcom/android/nfc_extras/tests/BasicNfcEeTest;->assertEquals(BB)V

    .line 113
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 116
    :cond_0
    return-void
.end method


# virtual methods
.method protected setUp()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 50
    invoke-super {p0}, Landroid/test/InstrumentationTestCase;->setUp()V

    .line 51
    invoke-virtual {p0}, Lcom/android/nfc_extras/tests/BasicNfcEeTest;->getInstrumentation()Landroid/app/Instrumentation;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Instrumentation;->getTargetContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/android/nfc_extras/tests/BasicNfcEeTest;->mContext:Landroid/content/Context;

    .line 52
    iget-object v0, p0, Lcom/android/nfc_extras/tests/BasicNfcEeTest;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/nfc/NfcAdapter;->getDefaultAdapter(Landroid/content/Context;)Landroid/nfc/NfcAdapter;

    move-result-object v0

    invoke-static {v0}, Lcom/android/nfc_extras/NfcAdapterExtras;->get(Landroid/nfc/NfcAdapter;)Lcom/android/nfc_extras/NfcAdapterExtras;

    move-result-object v0

    iput-object v0, p0, Lcom/android/nfc_extras/tests/BasicNfcEeTest;->mAdapterExtras:Lcom/android/nfc_extras/NfcAdapterExtras;

    .line 53
    iget-object v0, p0, Lcom/android/nfc_extras/tests/BasicNfcEeTest;->mAdapterExtras:Lcom/android/nfc_extras/NfcAdapterExtras;

    invoke-virtual {v0}, Lcom/android/nfc_extras/NfcAdapterExtras;->getEmbeddedExecutionEnvironment()Lcom/android/nfc_extras/NfcExecutionEnvironment;

    move-result-object v0

    iput-object v0, p0, Lcom/android/nfc_extras/tests/BasicNfcEeTest;->mEe:Lcom/android/nfc_extras/NfcExecutionEnvironment;

    .line 54
    return-void
.end method

.method public testDisableEe()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 104
    iget-object v1, p0, Lcom/android/nfc_extras/tests/BasicNfcEeTest;->mAdapterExtras:Lcom/android/nfc_extras/NfcAdapterExtras;

    new-instance v2, Lcom/android/nfc_extras/NfcAdapterExtras$CardEmulationRoute;

    const/4 v3, 0x0

    invoke-direct {v2, v4, v3}, Lcom/android/nfc_extras/NfcAdapterExtras$CardEmulationRoute;-><init>(ILcom/android/nfc_extras/NfcExecutionEnvironment;)V

    invoke-virtual {v1, v2}, Lcom/android/nfc_extras/NfcAdapterExtras;->setCardEmulationRoute(Lcom/android/nfc_extras/NfcAdapterExtras$CardEmulationRoute;)V

    .line 106
    iget-object v1, p0, Lcom/android/nfc_extras/tests/BasicNfcEeTest;->mAdapterExtras:Lcom/android/nfc_extras/NfcAdapterExtras;

    invoke-virtual {v1}, Lcom/android/nfc_extras/NfcAdapterExtras;->getCardEmulationRoute()Lcom/android/nfc_extras/NfcAdapterExtras$CardEmulationRoute;

    move-result-object v0

    .line 107
    .local v0, "newRoute":Lcom/android/nfc_extras/NfcAdapterExtras$CardEmulationRoute;
    iget v1, v0, Lcom/android/nfc_extras/NfcAdapterExtras$CardEmulationRoute;->route:I

    invoke-static {v4, v1}, Lcom/android/nfc_extras/tests/BasicNfcEeTest;->assertEquals(II)V

    .line 108
    iget-object v1, v0, Lcom/android/nfc_extras/NfcAdapterExtras$CardEmulationRoute;->nfcEe:Lcom/android/nfc_extras/NfcExecutionEnvironment;

    invoke-static {v1}, Lcom/android/nfc_extras/tests/BasicNfcEeTest;->assertNull(Ljava/lang/Object;)V

    .line 109
    return-void
.end method

.method public testEnableEe()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 96
    iget-object v1, p0, Lcom/android/nfc_extras/tests/BasicNfcEeTest;->mAdapterExtras:Lcom/android/nfc_extras/NfcAdapterExtras;

    new-instance v2, Lcom/android/nfc_extras/NfcAdapterExtras$CardEmulationRoute;

    iget-object v3, p0, Lcom/android/nfc_extras/tests/BasicNfcEeTest;->mEe:Lcom/android/nfc_extras/NfcExecutionEnvironment;

    invoke-direct {v2, v4, v3}, Lcom/android/nfc_extras/NfcAdapterExtras$CardEmulationRoute;-><init>(ILcom/android/nfc_extras/NfcExecutionEnvironment;)V

    invoke-virtual {v1, v2}, Lcom/android/nfc_extras/NfcAdapterExtras;->setCardEmulationRoute(Lcom/android/nfc_extras/NfcAdapterExtras$CardEmulationRoute;)V

    .line 98
    iget-object v1, p0, Lcom/android/nfc_extras/tests/BasicNfcEeTest;->mAdapterExtras:Lcom/android/nfc_extras/NfcAdapterExtras;

    invoke-virtual {v1}, Lcom/android/nfc_extras/NfcAdapterExtras;->getCardEmulationRoute()Lcom/android/nfc_extras/NfcAdapterExtras$CardEmulationRoute;

    move-result-object v0

    .line 99
    .local v0, "newRoute":Lcom/android/nfc_extras/NfcAdapterExtras$CardEmulationRoute;
    iget v1, v0, Lcom/android/nfc_extras/NfcAdapterExtras$CardEmulationRoute;->route:I

    invoke-static {v4, v1}, Lcom/android/nfc_extras/tests/BasicNfcEeTest;->assertEquals(II)V

    .line 100
    iget-object v1, p0, Lcom/android/nfc_extras/tests/BasicNfcEeTest;->mEe:Lcom/android/nfc_extras/NfcExecutionEnvironment;

    iget-object v2, v0, Lcom/android/nfc_extras/NfcAdapterExtras$CardEmulationRoute;->nfcEe:Lcom/android/nfc_extras/NfcExecutionEnvironment;

    invoke-static {v1, v2}, Lcom/android/nfc_extras/tests/BasicNfcEeTest;->assertEquals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 101
    return-void
.end method

.method public testSendCardManagerApdu()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 57
    iget-object v2, p0, Lcom/android/nfc_extras/tests/BasicNfcEeTest;->mEe:Lcom/android/nfc_extras/NfcExecutionEnvironment;

    invoke-virtual {v2}, Lcom/android/nfc_extras/NfcExecutionEnvironment;->open()V

    .line 60
    :try_start_0
    iget-object v2, p0, Lcom/android/nfc_extras/tests/BasicNfcEeTest;->mEe:Lcom/android/nfc_extras/NfcExecutionEnvironment;

    sget-object v3, Lcom/android/nfc_extras/tests/BasicNfcEeTest;->SELECT_CARD_MANAGER_COMMAND:[B

    invoke-virtual {v2, v3}, Lcom/android/nfc_extras/NfcExecutionEnvironment;->transceive([B)[B

    move-result-object v0

    .line 61
    .local v0, "out":[B
    array-length v2, v0

    sget-object v3, Lcom/android/nfc_extras/tests/BasicNfcEeTest;->SELECT_CARD_MANAGER_RESPONSE:[B

    array-length v3, v3

    if-lt v2, v3, :cond_0

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, Lcom/android/nfc_extras/tests/BasicNfcEeTest;->assertTrue(Z)V

    .line 62
    array-length v2, v0

    sget-object v3, Lcom/android/nfc_extras/tests/BasicNfcEeTest;->SELECT_CARD_MANAGER_RESPONSE:[B

    array-length v3, v3

    sub-int/2addr v2, v3

    array-length v3, v0

    invoke-static {v0, v2, v3}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v1

    .line 65
    .local v1, "trailing":[B
    sget-object v2, Lcom/android/nfc_extras/tests/BasicNfcEeTest;->SELECT_CARD_MANAGER_RESPONSE:[B

    invoke-static {v2, v1}, Lcom/android/nfc_extras/tests/BasicNfcEeTest;->assertByteArrayEquals([B[B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 68
    iget-object v2, p0, Lcom/android/nfc_extras/tests/BasicNfcEeTest;->mEe:Lcom/android/nfc_extras/NfcExecutionEnvironment;

    invoke-virtual {v2}, Lcom/android/nfc_extras/NfcExecutionEnvironment;->close()V

    .line 71
    return-void

    .line 61
    .end local v1    # "trailing":[B
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 68
    .end local v0    # "out":[B
    :catchall_0
    move-exception v2

    iget-object v3, p0, Lcom/android/nfc_extras/tests/BasicNfcEeTest;->mEe:Lcom/android/nfc_extras/NfcExecutionEnvironment;

    invoke-virtual {v3}, Lcom/android/nfc_extras/NfcExecutionEnvironment;->close()V

    throw v2
.end method

.method public testSendCardManagerApduMultiple()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 74
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v2, 0xa

    if-ge v0, v2, :cond_0

    .line 76
    :try_start_0
    iget-object v2, p0, Lcom/android/nfc_extras/tests/BasicNfcEeTest;->mEe:Lcom/android/nfc_extras/NfcExecutionEnvironment;

    invoke-virtual {v2}, Lcom/android/nfc_extras/NfcExecutionEnvironment;->open()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 79
    :try_start_1
    iget-object v2, p0, Lcom/android/nfc_extras/tests/BasicNfcEeTest;->mEe:Lcom/android/nfc_extras/NfcExecutionEnvironment;

    sget-object v3, Lcom/android/nfc_extras/tests/BasicNfcEeTest;->SELECT_CARD_MANAGER_COMMAND:[B

    invoke-virtual {v2, v3}, Lcom/android/nfc_extras/NfcExecutionEnvironment;->transceive([B)[B

    move-result-object v1

    .line 80
    .local v1, "out":[B
    array-length v2, v1

    sget-object v3, Lcom/android/nfc_extras/tests/BasicNfcEeTest;->SELECT_CARD_MANAGER_RESPONSE:[B

    array-length v3, v3

    sub-int/2addr v2, v3

    array-length v3, v1

    invoke-static {v1, v2, v3}, Ljava/util/Arrays;->copyOfRange([BII)[B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 85
    const-wide/16 v2, 0x3e8

    :try_start_2
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 86
    :goto_1
    :try_start_3
    iget-object v2, p0, Lcom/android/nfc_extras/tests/BasicNfcEeTest;->mEe:Lcom/android/nfc_extras/NfcExecutionEnvironment;

    invoke-virtual {v2}, Lcom/android/nfc_extras/NfcExecutionEnvironment;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    .line 74
    .end local v1    # "out":[B
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 85
    :catchall_0
    move-exception v2

    const-wide/16 v4, 0x3e8

    :try_start_4
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    .line 86
    :goto_3
    :try_start_5
    iget-object v3, p0, Lcom/android/nfc_extras/tests/BasicNfcEeTest;->mEe:Lcom/android/nfc_extras/NfcExecutionEnvironment;

    invoke-virtual {v3}, Lcom/android/nfc_extras/NfcExecutionEnvironment;->close()V

    throw v2
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0

    .line 88
    :catch_0
    move-exception v2

    goto :goto_2

    .line 91
    :cond_0
    invoke-virtual {p0}, Lcom/android/nfc_extras/tests/BasicNfcEeTest;->testSendCardManagerApdu()V

    .line 93
    return-void

    .line 85
    .restart local v1    # "out":[B
    :catch_1
    move-exception v2

    goto :goto_1

    .end local v1    # "out":[B
    :catch_2
    move-exception v3

    goto :goto_3
.end method
