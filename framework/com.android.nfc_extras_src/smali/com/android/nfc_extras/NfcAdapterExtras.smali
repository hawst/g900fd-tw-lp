.class public final Lcom/android/nfc_extras/NfcAdapterExtras;
.super Ljava/lang/Object;
.source "NfcAdapterExtras.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/nfc_extras/NfcAdapterExtras$CardEmulationRoute;
    }
.end annotation


# static fields
.field public static final ACTION_RF_FIELD_OFF_DETECTED:Ljava/lang/String; = "com.android.nfc_extras.action.RF_FIELD_OFF_DETECTED"

.field public static final ACTION_RF_FIELD_ON_DETECTED:Ljava/lang/String; = "com.android.nfc_extras.action.RF_FIELD_ON_DETECTED"

.field private static final ROUTE_OFF:Lcom/android/nfc_extras/NfcAdapterExtras$CardEmulationRoute;

.field private static final TAG:Ljava/lang/String; = "NfcAdapterExtras"

.field private static final sNfcExtras:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/nfc/NfcAdapter;",
            "Lcom/android/nfc_extras/NfcAdapterExtras;",
            ">;"
        }
    .end annotation
.end field

.field private static sService:Landroid/nfc/INfcAdapterExtras;


# instance fields
.field private final mAdapter:Landroid/nfc/NfcAdapter;

.field private final mEmbeddedEe:Lcom/android/nfc_extras/NfcExecutionEnvironment;

.field final mPackageName:Ljava/lang/String;

.field private final mRouteOnWhenScreenOn:Lcom/android/nfc_extras/NfcAdapterExtras$CardEmulationRoute;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 63
    new-instance v0, Lcom/android/nfc_extras/NfcAdapterExtras$CardEmulationRoute;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/android/nfc_extras/NfcAdapterExtras$CardEmulationRoute;-><init>(ILcom/android/nfc_extras/NfcExecutionEnvironment;)V

    sput-object v0, Lcom/android/nfc_extras/NfcAdapterExtras;->ROUTE_OFF:Lcom/android/nfc_extras/NfcAdapterExtras$CardEmulationRoute;

    .line 67
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/android/nfc_extras/NfcAdapterExtras;->sNfcExtras:Ljava/util/HashMap;

    return-void
.end method

.method private constructor <init>(Landroid/nfc/NfcAdapter;)V
    .locals 3
    .param p1, "adapter"    # Landroid/nfc/NfcAdapter;

    .prologue
    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 114
    iput-object p1, p0, Lcom/android/nfc_extras/NfcAdapterExtras;->mAdapter:Landroid/nfc/NfcAdapter;

    .line 115
    invoke-virtual {p1}, Landroid/nfc/NfcAdapter;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/nfc_extras/NfcAdapterExtras;->mPackageName:Ljava/lang/String;

    .line 116
    new-instance v0, Lcom/android/nfc_extras/NfcExecutionEnvironment;

    invoke-direct {v0, p0}, Lcom/android/nfc_extras/NfcExecutionEnvironment;-><init>(Lcom/android/nfc_extras/NfcAdapterExtras;)V

    iput-object v0, p0, Lcom/android/nfc_extras/NfcAdapterExtras;->mEmbeddedEe:Lcom/android/nfc_extras/NfcExecutionEnvironment;

    .line 117
    new-instance v0, Lcom/android/nfc_extras/NfcAdapterExtras$CardEmulationRoute;

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/android/nfc_extras/NfcAdapterExtras;->mEmbeddedEe:Lcom/android/nfc_extras/NfcExecutionEnvironment;

    invoke-direct {v0, v1, v2}, Lcom/android/nfc_extras/NfcAdapterExtras$CardEmulationRoute;-><init>(ILcom/android/nfc_extras/NfcExecutionEnvironment;)V

    iput-object v0, p0, Lcom/android/nfc_extras/NfcAdapterExtras;->mRouteOnWhenScreenOn:Lcom/android/nfc_extras/NfcAdapterExtras$CardEmulationRoute;

    .line 119
    return-void
.end method

.method public static get(Landroid/nfc/NfcAdapter;)Lcom/android/nfc_extras/NfcAdapterExtras;
    .locals 4
    .param p0, "adapter"    # Landroid/nfc/NfcAdapter;

    .prologue
    .line 94
    invoke-virtual {p0}, Landroid/nfc/NfcAdapter;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 95
    .local v0, "context":Landroid/content/Context;
    if-nez v0, :cond_0

    .line 96
    new-instance v2, Ljava/lang/UnsupportedOperationException;

    const-string v3, "You must pass a context to your NfcAdapter to use the NFC extras APIs"

    invoke-direct {v2, v3}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 100
    :cond_0
    const-class v3, Lcom/android/nfc_extras/NfcAdapterExtras;

    monitor-enter v3

    .line 101
    :try_start_0
    sget-object v2, Lcom/android/nfc_extras/NfcAdapterExtras;->sService:Landroid/nfc/INfcAdapterExtras;

    if-nez v2, :cond_1

    .line 102
    invoke-static {p0}, Lcom/android/nfc_extras/NfcAdapterExtras;->initService(Landroid/nfc/NfcAdapter;)V

    .line 104
    :cond_1
    sget-object v2, Lcom/android/nfc_extras/NfcAdapterExtras;->sNfcExtras:Ljava/util/HashMap;

    invoke-virtual {v2, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/nfc_extras/NfcAdapterExtras;

    .line 105
    .local v1, "extras":Lcom/android/nfc_extras/NfcAdapterExtras;
    if-nez v1, :cond_2

    .line 106
    new-instance v1, Lcom/android/nfc_extras/NfcAdapterExtras;

    .end local v1    # "extras":Lcom/android/nfc_extras/NfcAdapterExtras;
    invoke-direct {v1, p0}, Lcom/android/nfc_extras/NfcAdapterExtras;-><init>(Landroid/nfc/NfcAdapter;)V

    .line 107
    .restart local v1    # "extras":Lcom/android/nfc_extras/NfcAdapterExtras;
    sget-object v2, Lcom/android/nfc_extras/NfcAdapterExtras;->sNfcExtras:Ljava/util/HashMap;

    invoke-virtual {v2, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    :cond_2
    monitor-exit v3

    return-object v1

    .line 110
    .end local v1    # "extras":Lcom/android/nfc_extras/NfcAdapterExtras;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method private static initService(Landroid/nfc/NfcAdapter;)V
    .locals 1
    .param p0, "adapter"    # Landroid/nfc/NfcAdapter;

    .prologue
    .line 77
    invoke-virtual {p0}, Landroid/nfc/NfcAdapter;->getNfcAdapterExtrasInterface()Landroid/nfc/INfcAdapterExtras;

    move-result-object v0

    .line 78
    .local v0, "service":Landroid/nfc/INfcAdapterExtras;
    if-eqz v0, :cond_0

    .line 80
    sput-object v0, Lcom/android/nfc_extras/NfcAdapterExtras;->sService:Landroid/nfc/INfcAdapterExtras;

    .line 82
    :cond_0
    return-void
.end method


# virtual methods
.method attemptDeadServiceRecovery(Ljava/lang/Exception;)V
    .locals 2
    .param p1, "e"    # Ljava/lang/Exception;

    .prologue
    .line 163
    const-string v0, "NfcAdapterExtras"

    const-string v1, "NFC Adapter Extras dead - attempting to recover"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    iget-object v0, p0, Lcom/android/nfc_extras/NfcAdapterExtras;->mAdapter:Landroid/nfc/NfcAdapter;

    invoke-virtual {v0, p1}, Landroid/nfc/NfcAdapter;->attemptDeadServiceRecovery(Ljava/lang/Exception;)V

    .line 165
    iget-object v0, p0, Lcom/android/nfc_extras/NfcAdapterExtras;->mAdapter:Landroid/nfc/NfcAdapter;

    invoke-static {v0}, Lcom/android/nfc_extras/NfcAdapterExtras;->initService(Landroid/nfc/NfcAdapter;)V

    .line 166
    return-void
.end method

.method public authenticate([B)V
    .locals 3
    .param p1, "token"    # [B

    .prologue
    .line 232
    :try_start_0
    sget-object v1, Lcom/android/nfc_extras/NfcAdapterExtras;->sService:Landroid/nfc/INfcAdapterExtras;

    iget-object v2, p0, Lcom/android/nfc_extras/NfcAdapterExtras;->mPackageName:Ljava/lang/String;

    invoke-interface {v1, v2, p1}, Landroid/nfc/INfcAdapterExtras;->authenticate(Ljava/lang/String;[B)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 236
    :goto_0
    return-void

    .line 233
    :catch_0
    move-exception v0

    .line 234
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {p0, v0}, Lcom/android/nfc_extras/NfcAdapterExtras;->attemptDeadServiceRecovery(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public getCardEmulationRoute()Lcom/android/nfc_extras/NfcAdapterExtras$CardEmulationRoute;
    .locals 4

    .prologue
    .line 180
    :try_start_0
    sget-object v2, Lcom/android/nfc_extras/NfcAdapterExtras;->sService:Landroid/nfc/INfcAdapterExtras;

    iget-object v3, p0, Lcom/android/nfc_extras/NfcAdapterExtras;->mPackageName:Ljava/lang/String;

    invoke-interface {v2, v3}, Landroid/nfc/INfcAdapterExtras;->getCardEmulationRoute(Ljava/lang/String;)I

    move-result v1

    .line 181
    .local v1, "route":I
    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    sget-object v2, Lcom/android/nfc_extras/NfcAdapterExtras;->ROUTE_OFF:Lcom/android/nfc_extras/NfcAdapterExtras$CardEmulationRoute;

    .line 186
    .end local v1    # "route":I
    :goto_0
    return-object v2

    .line 181
    .restart local v1    # "route":I
    :cond_0
    iget-object v2, p0, Lcom/android/nfc_extras/NfcAdapterExtras;->mRouteOnWhenScreenOn:Lcom/android/nfc_extras/NfcAdapterExtras$CardEmulationRoute;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 184
    .end local v1    # "route":I
    :catch_0
    move-exception v0

    .line 185
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {p0, v0}, Lcom/android/nfc_extras/NfcAdapterExtras;->attemptDeadServiceRecovery(Ljava/lang/Exception;)V

    .line 186
    sget-object v2, Lcom/android/nfc_extras/NfcAdapterExtras;->ROUTE_OFF:Lcom/android/nfc_extras/NfcAdapterExtras$CardEmulationRoute;

    goto :goto_0
.end method

.method public getDriverName()Ljava/lang/String;
    .locals 3

    .prologue
    .line 248
    :try_start_0
    sget-object v1, Lcom/android/nfc_extras/NfcAdapterExtras;->sService:Landroid/nfc/INfcAdapterExtras;

    iget-object v2, p0, Lcom/android/nfc_extras/NfcAdapterExtras;->mPackageName:Ljava/lang/String;

    invoke-interface {v1, v2}, Landroid/nfc/INfcAdapterExtras;->getDriverName(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 251
    :goto_0
    return-object v1

    .line 249
    :catch_0
    move-exception v0

    .line 250
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {p0, v0}, Lcom/android/nfc_extras/NfcAdapterExtras;->attemptDeadServiceRecovery(Ljava/lang/Exception;)V

    .line 251
    const-string v1, ""

    goto :goto_0
.end method

.method public getEmbeddedExecutionEnvironment()Lcom/android/nfc_extras/NfcExecutionEnvironment;
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lcom/android/nfc_extras/NfcAdapterExtras;->mEmbeddedEe:Lcom/android/nfc_extras/NfcExecutionEnvironment;

    return-object v0
.end method

.method getService()Landroid/nfc/INfcAdapterExtras;
    .locals 1

    .prologue
    .line 169
    sget-object v0, Lcom/android/nfc_extras/NfcAdapterExtras;->sService:Landroid/nfc/INfcAdapterExtras;

    return-object v0
.end method

.method public setCardEmulationRoute(Lcom/android/nfc_extras/NfcAdapterExtras$CardEmulationRoute;)V
    .locals 4
    .param p1, "route"    # Lcom/android/nfc_extras/NfcAdapterExtras$CardEmulationRoute;

    .prologue
    .line 202
    :try_start_0
    sget-object v1, Lcom/android/nfc_extras/NfcAdapterExtras;->sService:Landroid/nfc/INfcAdapterExtras;

    iget-object v2, p0, Lcom/android/nfc_extras/NfcAdapterExtras;->mPackageName:Ljava/lang/String;

    iget v3, p1, Lcom/android/nfc_extras/NfcAdapterExtras$CardEmulationRoute;->route:I

    invoke-interface {v1, v2, v3}, Landroid/nfc/INfcAdapterExtras;->setCardEmulationRoute(Ljava/lang/String;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 206
    :goto_0
    return-void

    .line 203
    :catch_0
    move-exception v0

    .line 204
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {p0, v0}, Lcom/android/nfc_extras/NfcAdapterExtras;->attemptDeadServiceRecovery(Ljava/lang/Exception;)V

    goto :goto_0
.end method
