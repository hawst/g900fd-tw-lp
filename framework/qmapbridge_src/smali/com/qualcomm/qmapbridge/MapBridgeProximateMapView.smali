.class public Lcom/qualcomm/qmapbridge/MapBridgeProximateMapView;
.super Landroid/view/ViewGroup;
.source "MapBridgeProximateMapView.java"


# instance fields
.field protected alarmArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/qualcomm/qmapbridge/MapViewPosition;",
            ">;"
        }
    .end annotation
.end field

.field protected bSatellite:Z

.field protected bScreenEditMode:Z

.field protected bTraffic:Z

.field protected currentLocation:Lcom/qualcomm/qmapbridge/MapBridgeGeoPoint;

.field protected dciReportArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/qualcomm/qmapbridge/MapViewPosition;",
            ">;"
        }
    .end annotation
.end field

.field protected drawScale:Z

.field protected longPressGeoPoint:Lcom/qualcomm/qmapbridge/MapBridgeGeoPoint;

.field protected mapHelper:Lcom/qualcomm/qmapbridge/MapHelper;

.field protected posMarkerRadius:I

.field protected strEditingProximityName:Ljava/lang/String;

.field protected trackLocation:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "arg0"    # Landroid/content/Context;
    .param p2, "arg1"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 53
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 160
    iput-object v0, p0, Lcom/qualcomm/qmapbridge/MapBridgeProximateMapView;->longPressGeoPoint:Lcom/qualcomm/qmapbridge/MapBridgeGeoPoint;

    .line 161
    iput-object v0, p0, Lcom/qualcomm/qmapbridge/MapBridgeProximateMapView;->currentLocation:Lcom/qualcomm/qmapbridge/MapBridgeGeoPoint;

    .line 162
    iput v1, p0, Lcom/qualcomm/qmapbridge/MapBridgeProximateMapView;->posMarkerRadius:I

    .line 164
    iput-boolean v1, p0, Lcom/qualcomm/qmapbridge/MapBridgeProximateMapView;->trackLocation:Z

    .line 165
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/qualcomm/qmapbridge/MapBridgeProximateMapView;->drawScale:Z

    .line 167
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/qualcomm/qmapbridge/MapBridgeProximateMapView;->alarmArray:Ljava/util/ArrayList;

    .line 169
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/qualcomm/qmapbridge/MapBridgeProximateMapView;->dciReportArray:Ljava/util/ArrayList;

    .line 171
    iput-boolean v1, p0, Lcom/qualcomm/qmapbridge/MapBridgeProximateMapView;->bScreenEditMode:Z

    .line 173
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qmapbridge/MapBridgeProximateMapView;->strEditingProximityName:Ljava/lang/String;

    .line 54
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "arg0"    # Landroid/content/Context;
    .param p2, "arg1"    # Landroid/util/AttributeSet;
    .param p3, "arg2"    # I

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 58
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 160
    iput-object v0, p0, Lcom/qualcomm/qmapbridge/MapBridgeProximateMapView;->longPressGeoPoint:Lcom/qualcomm/qmapbridge/MapBridgeGeoPoint;

    .line 161
    iput-object v0, p0, Lcom/qualcomm/qmapbridge/MapBridgeProximateMapView;->currentLocation:Lcom/qualcomm/qmapbridge/MapBridgeGeoPoint;

    .line 162
    iput v1, p0, Lcom/qualcomm/qmapbridge/MapBridgeProximateMapView;->posMarkerRadius:I

    .line 164
    iput-boolean v1, p0, Lcom/qualcomm/qmapbridge/MapBridgeProximateMapView;->trackLocation:Z

    .line 165
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/qualcomm/qmapbridge/MapBridgeProximateMapView;->drawScale:Z

    .line 167
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/qualcomm/qmapbridge/MapBridgeProximateMapView;->alarmArray:Ljava/util/ArrayList;

    .line 169
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/qualcomm/qmapbridge/MapBridgeProximateMapView;->dciReportArray:Ljava/util/ArrayList;

    .line 171
    iput-boolean v1, p0, Lcom/qualcomm/qmapbridge/MapBridgeProximateMapView;->bScreenEditMode:Z

    .line 173
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qmapbridge/MapBridgeProximateMapView;->strEditingProximityName:Ljava/lang/String;

    .line 59
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p1, "arg0"    # Landroid/content/Context;
    .param p2, "arg1"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 48
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 160
    iput-object v0, p0, Lcom/qualcomm/qmapbridge/MapBridgeProximateMapView;->longPressGeoPoint:Lcom/qualcomm/qmapbridge/MapBridgeGeoPoint;

    .line 161
    iput-object v0, p0, Lcom/qualcomm/qmapbridge/MapBridgeProximateMapView;->currentLocation:Lcom/qualcomm/qmapbridge/MapBridgeGeoPoint;

    .line 162
    iput v1, p0, Lcom/qualcomm/qmapbridge/MapBridgeProximateMapView;->posMarkerRadius:I

    .line 164
    iput-boolean v1, p0, Lcom/qualcomm/qmapbridge/MapBridgeProximateMapView;->trackLocation:Z

    .line 165
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/qualcomm/qmapbridge/MapBridgeProximateMapView;->drawScale:Z

    .line 167
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/qualcomm/qmapbridge/MapBridgeProximateMapView;->alarmArray:Ljava/util/ArrayList;

    .line 169
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/qualcomm/qmapbridge/MapBridgeProximateMapView;->dciReportArray:Ljava/util/ArrayList;

    .line 171
    iput-boolean v1, p0, Lcom/qualcomm/qmapbridge/MapBridgeProximateMapView;->bScreenEditMode:Z

    .line 173
    const-string v0, ""

    iput-object v0, p0, Lcom/qualcomm/qmapbridge/MapBridgeProximateMapView;->strEditingProximityName:Ljava/lang/String;

    .line 49
    return-void
.end method

.method public static isDump()Z
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x1

    return v0
.end method


# virtual methods
.method public IsScreenEditMode()Z
    .locals 1

    .prologue
    .line 172
    iget-boolean v0, p0, Lcom/qualcomm/qmapbridge/MapBridgeProximateMapView;->bScreenEditMode:Z

    return v0
.end method

.method protected UpdateMapPosList()V
    .locals 0

    .prologue
    .line 156
    return-void
.end method

.method public canCoverCenter()Z
    .locals 1

    .prologue
    .line 103
    const/4 v0, 0x0

    return v0
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1
    .param p1, "arg0"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 97
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v0

    return v0
.end method

.method public computeScroll()V
    .locals 0

    .prologue
    .line 71
    invoke-super {p0}, Landroid/view/ViewGroup;->computeScroll()V

    return-void
.end method

.method public controller_animateTo(Lcom/qualcomm/qmapbridge/MapBridgeGeoPoint;)V
    .locals 0
    .param p1, "arg0"    # Lcom/qualcomm/qmapbridge/MapBridgeGeoPoint;

    .prologue
    .line 14
    return-void
.end method

.method public controller_animateTo(Lcom/qualcomm/qmapbridge/MapBridgeGeoPoint;Landroid/os/Message;)V
    .locals 0
    .param p1, "arg0"    # Lcom/qualcomm/qmapbridge/MapBridgeGeoPoint;
    .param p2, "arg1"    # Landroid/os/Message;

    .prologue
    .line 18
    return-void
.end method

.method public controller_animateTo(Lcom/qualcomm/qmapbridge/MapBridgeGeoPoint;Ljava/lang/Runnable;)V
    .locals 0
    .param p1, "arg0"    # Lcom/qualcomm/qmapbridge/MapBridgeGeoPoint;
    .param p2, "arg1"    # Ljava/lang/Runnable;

    .prologue
    .line 22
    return-void
.end method

.method public controller_setZoom(I)I
    .locals 1
    .param p1, "arg0"    # I

    .prologue
    .line 26
    const/4 v0, 0x0

    return v0
.end method

.method public controller_zoomToSpan(II)V
    .locals 0
    .param p1, "arg0"    # I
    .param p2, "arg1"    # I

    .prologue
    .line 31
    return-void
.end method

.method public displayZoomControls(Z)V
    .locals 0
    .param p1, "arg0"    # Z

    .prologue
    .line 101
    return-void
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1
    .param p1, "arg0"    # Landroid/util/AttributeSet;

    .prologue
    .line 95
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1
    .param p1, "arg0"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 99
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method public getLatitudeSpan()I
    .locals 1

    .prologue
    .line 119
    const/4 v0, 0x0

    return v0
.end method

.method public getLongitudeSpan()I
    .locals 1

    .prologue
    .line 120
    const/4 v0, 0x0

    return v0
.end method

.method public getMapCenter()Lcom/qualcomm/qmapbridge/MapBridgeGeoPoint;
    .locals 1

    .prologue
    .line 117
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMaxZoomLevel()I
    .locals 1

    .prologue
    .line 124
    const/4 v0, 0x1

    return v0
.end method

.method public getZoomLevel()I
    .locals 1

    .prologue
    .line 107
    const/4 v0, 0x0

    return v0
.end method

.method public isOpaque()Z
    .locals 1

    .prologue
    .line 73
    invoke-super {p0}, Landroid/view/ViewGroup;->isOpaque()Z

    move-result v0

    return v0
.end method

.method public isSatellite()Z
    .locals 1

    .prologue
    .line 111
    iget-boolean v0, p0, Lcom/qualcomm/qmapbridge/MapBridgeProximateMapView;->bSatellite:Z

    return v0
.end method

.method public isTraffic()Z
    .locals 1

    .prologue
    .line 115
    iget-boolean v0, p0, Lcom/qualcomm/qmapbridge/MapBridgeProximateMapView;->bTraffic:Z

    return v0
.end method

.method public mapHelper_updateScale()V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 146
    invoke-virtual {p0}, Lcom/qualcomm/qmapbridge/MapBridgeProximateMapView;->getWidth()I

    move-result v8

    .line 147
    .local v8, "widthPix":I
    invoke-virtual {p0}, Lcom/qualcomm/qmapbridge/MapBridgeProximateMapView;->getHeight()I

    move-result v0

    .line 148
    .local v0, "heightPix":I
    invoke-virtual {p0, v1, v1}, Lcom/qualcomm/qmapbridge/MapBridgeProximateMapView;->projection_fromPixels(II)Lcom/qualcomm/qmapbridge/MapBridgeGeoPoint;

    move-result-object v6

    .line 149
    .local v6, "ulPoint":Lcom/qualcomm/qmapbridge/MapBridgeGeoPoint;
    invoke-virtual {p0, v8, v0}, Lcom/qualcomm/qmapbridge/MapBridgeProximateMapView;->projection_fromPixels(II)Lcom/qualcomm/qmapbridge/MapBridgeGeoPoint;

    move-result-object v7

    .line 150
    .local v7, "lrPoint":Lcom/qualcomm/qmapbridge/MapBridgeGeoPoint;
    iget-object v1, p0, Lcom/qualcomm/qmapbridge/MapBridgeProximateMapView;->mapHelper:Lcom/qualcomm/qmapbridge/MapHelper;

    int-to-double v2, v8

    int-to-double v4, v0

    invoke-virtual/range {v1 .. v7}, Lcom/qualcomm/qmapbridge/MapHelper;->updateScale(DDLcom/qualcomm/qmapbridge/MapBridgeGeoPoint;Lcom/qualcomm/qmapbridge/MapBridgeGeoPoint;)V

    .line 151
    return-void
.end method

.method public mapInitiate()V
    .locals 1

    .prologue
    .line 136
    new-instance v0, Lcom/qualcomm/qmapbridge/MapHelper;

    invoke-direct {v0}, Lcom/qualcomm/qmapbridge/MapHelper;-><init>()V

    iput-object v0, p0, Lcom/qualcomm/qmapbridge/MapBridgeProximateMapView;->mapHelper:Lcom/qualcomm/qmapbridge/MapHelper;

    .line 137
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 69
    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    return-void
.end method

.method protected final onDraw(Landroid/graphics/Canvas;)V
    .locals 0
    .param p1, "arg0"    # Landroid/graphics/Canvas;

    .prologue
    .line 75
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onDraw(Landroid/graphics/Canvas;)V

    return-void
.end method

.method public onFocusChanged(ZILandroid/graphics/Rect;)V
    .locals 0
    .param p1, "arg0"    # Z
    .param p2, "arg1"    # I
    .param p3, "arg2"    # Landroid/graphics/Rect;

    .prologue
    .line 81
    invoke-super {p0, p1, p2, p3}, Landroid/view/ViewGroup;->onFocusChanged(ZILandroid/graphics/Rect;)V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "arg0"    # I
    .param p2, "arg1"    # Landroid/view/KeyEvent;

    .prologue
    .line 83
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "arg0"    # I
    .param p2, "arg1"    # Landroid/view/KeyEvent;

    .prologue
    .line 85
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method protected onLayout(ZIIII)V
    .locals 0
    .param p1, "arg0"    # Z
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "arg3"    # I
    .param p5, "arg4"    # I

    .prologue
    .line 63
    return-void
.end method

.method protected final onMeasure(II)V
    .locals 0
    .param p1, "arg0"    # I
    .param p2, "arg1"    # I

    .prologue
    .line 77
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "arg0"    # Landroid/os/Bundle;

    .prologue
    .line 128
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "arg0"    # Landroid/os/Bundle;

    .prologue
    .line 126
    invoke-super {p0}, Landroid/view/ViewGroup;->onSaveInstanceState()Landroid/os/Parcelable;

    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 0
    .param p1, "arg0"    # I
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "arg3"    # I

    .prologue
    .line 67
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->onSizeChanged(IIII)V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "arg0"    # Landroid/view/MotionEvent;

    .prologue
    .line 89
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onTrackballEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "arg0"    # Landroid/view/MotionEvent;

    .prologue
    .line 87
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onTrackballEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method protected onVisibilityChanged(Landroid/view/View;I)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "arg1"    # I

    .prologue
    .line 91
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onVisibilityChanged(Landroid/view/View;I)V

    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 0
    .param p1, "arg0"    # Z

    .prologue
    .line 79
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onWindowFocusChanged(Z)V

    return-void
.end method

.method public overlay_zoomDataExtents()V
    .locals 0

    .prologue
    .line 142
    return-void
.end method

.method public preLoad()V
    .locals 0

    .prologue
    .line 105
    return-void
.end method

.method public projection_fromPixels(II)Lcom/qualcomm/qmapbridge/MapBridgeGeoPoint;
    .locals 1
    .param p1, "arg0"    # I
    .param p2, "arg1"    # I

    .prologue
    .line 40
    new-instance v0, Lcom/qualcomm/qmapbridge/MapBridgeGeoPoint;

    invoke-direct {v0, p1, p2}, Lcom/qualcomm/qmapbridge/MapBridgeGeoPoint;-><init>(II)V

    .line 41
    .local v0, "point":Lcom/qualcomm/qmapbridge/MapBridgeGeoPoint;
    return-object v0
.end method

.method public projection_toPixels(Lcom/qualcomm/qmapbridge/MapBridgeGeoPoint;Landroid/graphics/Point;)Landroid/graphics/Point;
    .locals 0
    .param p1, "arg0"    # Lcom/qualcomm/qmapbridge/MapBridgeGeoPoint;
    .param p2, "arg1"    # Landroid/graphics/Point;

    .prologue
    .line 35
    return-object p2
.end method

.method public setBuiltInZoomControls(Z)V
    .locals 0
    .param p1, "arg0"    # Z

    .prologue
    .line 130
    return-void
.end method

.method public setSatellite(Z)V
    .locals 0
    .param p1, "arg0"    # Z

    .prologue
    .line 110
    iput-boolean p1, p0, Lcom/qualcomm/qmapbridge/MapBridgeProximateMapView;->bSatellite:Z

    return-void
.end method

.method public setTraffic(Z)V
    .locals 0
    .param p1, "arg0"    # Z

    .prologue
    .line 114
    iput-boolean p1, p0, Lcom/qualcomm/qmapbridge/MapBridgeProximateMapView;->bTraffic:Z

    return-void
.end method
