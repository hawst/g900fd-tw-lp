.class public Lcom/qualcomm/qmapbridge/GplsBridgeLocationRequest;
.super Ljava/lang/Object;
.source "GplsBridgeLocationRequest.java"


# static fields
.field public static final PRIORITY_BALANCED_POWER_ACCURACY:I = 0x66

.field public static final PRIORITY_HIGH_ACCURACY:I = 0x64

.field public static final PRIORITY_LOW_POWER:I = 0x68

.field public static final PRIORITY_NO_POWER:I = 0x69


# instance fields
.field public fMinDistance:F

.field public nFastestInterval:J

.field public nInterval:J

.field public nPriority:I

.field public numUpdates:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
