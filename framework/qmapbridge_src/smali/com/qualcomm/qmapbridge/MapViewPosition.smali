.class public Lcom/qualcomm/qmapbridge/MapViewPosition;
.super Ljava/lang/Object;
.source "MapViewPosition.java"


# static fields
.field public static final VALID_HEADING:J = 0x800L

.field public static final VALID_HEADING_UNC:J = 0x40000L

.field public static final VALID_HOR_UNC_CIRCULAR:J = 0x1000L

.field public static final VALID_LATITUDE:J = 0x20L

.field public static final VALID_LONGITUDE:J = 0x40L

.field public static final VALID_SENSOR_DATA:J = 0x10000000L

.field public static final VALID_SPEED_HORIZONTAL:J = 0x200L

.field public static final VALID_TIMESTAMP_UTC:J = 0x4L


# instance fields
.field protected heading:F

.field protected heading_unc:F

.field protected hor_unc_circular:F

.field protected latitude:D

.field protected longitude:D

.field protected sensor_data_aiding_ind_mask:I

.field protected sensor_data_usage_mask:I

.field protected speed_horizontal:F

.field protected timestamp_utc:J

.field protected valid_mask:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getHeading()F
    .locals 1

    .prologue
    .line 76
    iget v0, p0, Lcom/qualcomm/qmapbridge/MapViewPosition;->heading:F

    return v0
.end method

.method public getHeading_unc()F
    .locals 1

    .prologue
    .line 98
    iget v0, p0, Lcom/qualcomm/qmapbridge/MapViewPosition;->heading_unc:F

    return v0
.end method

.method public getHor_unc_circular()F
    .locals 1

    .prologue
    .line 87
    iget v0, p0, Lcom/qualcomm/qmapbridge/MapViewPosition;->hor_unc_circular:F

    return v0
.end method

.method public getLatitude()D
    .locals 2

    .prologue
    .line 43
    iget-wide v0, p0, Lcom/qualcomm/qmapbridge/MapViewPosition;->latitude:D

    return-wide v0
.end method

.method public getLongitude()D
    .locals 2

    .prologue
    .line 54
    iget-wide v0, p0, Lcom/qualcomm/qmapbridge/MapViewPosition;->longitude:D

    return-wide v0
.end method

.method public getSensor_data_aiding_ind_mask()I
    .locals 1

    .prologue
    .line 116
    iget v0, p0, Lcom/qualcomm/qmapbridge/MapViewPosition;->sensor_data_aiding_ind_mask:I

    return v0
.end method

.method public getSensor_data_usage_mask()I
    .locals 1

    .prologue
    .line 109
    iget v0, p0, Lcom/qualcomm/qmapbridge/MapViewPosition;->sensor_data_usage_mask:I

    return v0
.end method

.method public getSpeed_horizontal()F
    .locals 1

    .prologue
    .line 65
    iget v0, p0, Lcom/qualcomm/qmapbridge/MapViewPosition;->speed_horizontal:F

    return v0
.end method

.method public getTimestamp_utc()J
    .locals 2

    .prologue
    .line 32
    iget-wide v0, p0, Lcom/qualcomm/qmapbridge/MapViewPosition;->timestamp_utc:J

    return-wide v0
.end method

.method public isValidHeading()Z
    .locals 4

    .prologue
    .line 73
    iget-wide v0, p0, Lcom/qualcomm/qmapbridge/MapViewPosition;->valid_mask:J

    const-wide/16 v2, 0x800

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isValidHor_unc_circular()Z
    .locals 4

    .prologue
    .line 84
    iget-wide v0, p0, Lcom/qualcomm/qmapbridge/MapViewPosition;->valid_mask:J

    const-wide/16 v2, 0x1000

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isValidLatitude()Z
    .locals 4

    .prologue
    .line 40
    iget-wide v0, p0, Lcom/qualcomm/qmapbridge/MapViewPosition;->valid_mask:J

    const-wide/16 v2, 0x20

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isValidLongitude()Z
    .locals 4

    .prologue
    .line 51
    iget-wide v0, p0, Lcom/qualcomm/qmapbridge/MapViewPosition;->valid_mask:J

    const-wide/16 v2, 0x40

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isValidSensor_data()Z
    .locals 4

    .prologue
    .line 106
    iget-wide v0, p0, Lcom/qualcomm/qmapbridge/MapViewPosition;->valid_mask:J

    const-wide/32 v2, 0x10000000

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isValidSpeed_horizontal()Z
    .locals 4

    .prologue
    .line 62
    iget-wide v0, p0, Lcom/qualcomm/qmapbridge/MapViewPosition;->valid_mask:J

    const-wide/16 v2, 0x200

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isValidTimestamp_utc()Z
    .locals 4

    .prologue
    .line 29
    iget-wide v0, p0, Lcom/qualcomm/qmapbridge/MapViewPosition;->valid_mask:J

    const-wide/16 v2, 0x4

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isValidUnc_heading()Z
    .locals 4

    .prologue
    .line 95
    iget-wide v0, p0, Lcom/qualcomm/qmapbridge/MapViewPosition;->valid_mask:J

    const-wide/32 v2, 0x40000

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setHeading(F)V
    .locals 4
    .param p1, "heading"    # F

    .prologue
    .line 79
    iget-wide v0, p0, Lcom/qualcomm/qmapbridge/MapViewPosition;->valid_mask:J

    const-wide/16 v2, 0x800

    or-long/2addr v0, v2

    iput-wide v0, p0, Lcom/qualcomm/qmapbridge/MapViewPosition;->valid_mask:J

    .line 80
    iput p1, p0, Lcom/qualcomm/qmapbridge/MapViewPosition;->heading:F

    .line 81
    return-void
.end method

.method public setHeading_unc(F)V
    .locals 4
    .param p1, "headingUnc"    # F

    .prologue
    .line 101
    iget-wide v0, p0, Lcom/qualcomm/qmapbridge/MapViewPosition;->valid_mask:J

    const-wide/32 v2, 0x40000

    or-long/2addr v0, v2

    iput-wide v0, p0, Lcom/qualcomm/qmapbridge/MapViewPosition;->valid_mask:J

    .line 102
    iput p1, p0, Lcom/qualcomm/qmapbridge/MapViewPosition;->heading_unc:F

    .line 103
    return-void
.end method

.method public setHor_unc_circular(F)V
    .locals 4
    .param p1, "horUncCircular"    # F

    .prologue
    .line 90
    iget-wide v0, p0, Lcom/qualcomm/qmapbridge/MapViewPosition;->valid_mask:J

    const-wide/16 v2, 0x1000

    or-long/2addr v0, v2

    iput-wide v0, p0, Lcom/qualcomm/qmapbridge/MapViewPosition;->valid_mask:J

    .line 91
    iput p1, p0, Lcom/qualcomm/qmapbridge/MapViewPosition;->hor_unc_circular:F

    .line 92
    return-void
.end method

.method public setLatitude(D)V
    .locals 5
    .param p1, "latitude"    # D

    .prologue
    .line 46
    iget-wide v0, p0, Lcom/qualcomm/qmapbridge/MapViewPosition;->valid_mask:J

    const-wide/16 v2, 0x20

    or-long/2addr v0, v2

    iput-wide v0, p0, Lcom/qualcomm/qmapbridge/MapViewPosition;->valid_mask:J

    .line 47
    iput-wide p1, p0, Lcom/qualcomm/qmapbridge/MapViewPosition;->latitude:D

    .line 48
    return-void
.end method

.method public setLongitude(D)V
    .locals 5
    .param p1, "longitude"    # D

    .prologue
    .line 57
    iget-wide v0, p0, Lcom/qualcomm/qmapbridge/MapViewPosition;->valid_mask:J

    const-wide/16 v2, 0x40

    or-long/2addr v0, v2

    iput-wide v0, p0, Lcom/qualcomm/qmapbridge/MapViewPosition;->valid_mask:J

    .line 58
    iput-wide p1, p0, Lcom/qualcomm/qmapbridge/MapViewPosition;->longitude:D

    .line 59
    return-void
.end method

.method public setSensor_data_aiding_ind_mask(I)V
    .locals 4
    .param p1, "sensorDataAidingIndMask"    # I

    .prologue
    .line 119
    iget-wide v0, p0, Lcom/qualcomm/qmapbridge/MapViewPosition;->valid_mask:J

    const-wide/32 v2, 0x10000000

    or-long/2addr v0, v2

    iput-wide v0, p0, Lcom/qualcomm/qmapbridge/MapViewPosition;->valid_mask:J

    .line 120
    iput p1, p0, Lcom/qualcomm/qmapbridge/MapViewPosition;->sensor_data_aiding_ind_mask:I

    .line 121
    return-void
.end method

.method public setSensor_data_usage_mask(I)V
    .locals 4
    .param p1, "sensorDataUsageMask"    # I

    .prologue
    .line 112
    iget-wide v0, p0, Lcom/qualcomm/qmapbridge/MapViewPosition;->valid_mask:J

    const-wide/32 v2, 0x10000000

    or-long/2addr v0, v2

    iput-wide v0, p0, Lcom/qualcomm/qmapbridge/MapViewPosition;->valid_mask:J

    .line 113
    iput p1, p0, Lcom/qualcomm/qmapbridge/MapViewPosition;->sensor_data_usage_mask:I

    .line 114
    return-void
.end method

.method public setSpeed_horizontal(F)V
    .locals 4
    .param p1, "speedHorizontal"    # F

    .prologue
    .line 68
    iget-wide v0, p0, Lcom/qualcomm/qmapbridge/MapViewPosition;->valid_mask:J

    const-wide/16 v2, 0x200

    or-long/2addr v0, v2

    iput-wide v0, p0, Lcom/qualcomm/qmapbridge/MapViewPosition;->valid_mask:J

    .line 69
    iput p1, p0, Lcom/qualcomm/qmapbridge/MapViewPosition;->speed_horizontal:F

    .line 70
    return-void
.end method

.method public setTimestamp_utc(J)V
    .locals 5
    .param p1, "timestampUtc"    # J

    .prologue
    .line 35
    iget-wide v0, p0, Lcom/qualcomm/qmapbridge/MapViewPosition;->valid_mask:J

    const-wide/16 v2, 0x4

    or-long/2addr v0, v2

    iput-wide v0, p0, Lcom/qualcomm/qmapbridge/MapViewPosition;->valid_mask:J

    .line 36
    iput-wide p1, p0, Lcom/qualcomm/qmapbridge/MapViewPosition;->timestamp_utc:J

    .line 37
    return-void
.end method
