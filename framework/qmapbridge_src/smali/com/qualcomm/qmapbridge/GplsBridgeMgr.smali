.class public Lcom/qualcomm/qmapbridge/GplsBridgeMgr;
.super Ljava/lang/Object;
.source "GplsBridgeMgr.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/qmapbridge/GplsBridgeMgr$OnLocationChangedEventListener;,
        Lcom/qualcomm/qmapbridge/GplsBridgeMgr$OnGeoFenceAlertEventListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static gplsInstance:Lcom/qualcomm/qmapbridge/GplsBridgeMgr;


# instance fields
.field private bConnected:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const-class v0, Lcom/qualcomm/qmapbridge/GplsBridgeMgr;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/qualcomm/qmapbridge/GplsBridgeMgr;->TAG:Ljava/lang/String;

    .line 35
    const/4 v0, 0x0

    sput-object v0, Lcom/qualcomm/qmapbridge/GplsBridgeMgr;->gplsInstance:Lcom/qualcomm/qmapbridge/GplsBridgeMgr;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/qualcomm/qmapbridge/GplsBridgeMgr;->bConnected:Z

    .line 59
    return-void
.end method

.method public static getGPLSInstance(Landroid/content/Context;)Lcom/qualcomm/qmapbridge/GplsBridgeMgr;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 38
    if-eqz p0, :cond_1

    .line 40
    invoke-static {p0}, Lcom/qualcomm/qmapbridge/GplsBridgeMgr;->isGPLSupported(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 42
    sget-object v0, Lcom/qualcomm/qmapbridge/GplsBridgeMgr;->gplsInstance:Lcom/qualcomm/qmapbridge/GplsBridgeMgr;

    if-nez v0, :cond_0

    .line 43
    new-instance v0, Lcom/qualcomm/qmapbridge/GplsBridgeMgr;

    invoke-direct {v0, p0}, Lcom/qualcomm/qmapbridge/GplsBridgeMgr;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/qualcomm/qmapbridge/GplsBridgeMgr;->gplsInstance:Lcom/qualcomm/qmapbridge/GplsBridgeMgr;

    .line 45
    :cond_0
    sget-object v0, Lcom/qualcomm/qmapbridge/GplsBridgeMgr;->gplsInstance:Lcom/qualcomm/qmapbridge/GplsBridgeMgr;

    if-eqz v0, :cond_1

    .line 47
    sget-object v0, Lcom/qualcomm/qmapbridge/GplsBridgeMgr;->gplsInstance:Lcom/qualcomm/qmapbridge/GplsBridgeMgr;

    invoke-virtual {v0}, Lcom/qualcomm/qmapbridge/GplsBridgeMgr;->Connect()V

    .line 51
    :cond_1
    sget-object v0, Lcom/qualcomm/qmapbridge/GplsBridgeMgr;->gplsInstance:Lcom/qualcomm/qmapbridge/GplsBridgeMgr;

    return-object v0
.end method

.method private getTransitionPendingIntent(Landroid/content/Context;)Landroid/app/PendingIntent;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 101
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/qualcomm/qmapbridge/GplsBridgeRecvGeoFenceIntentService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 107
    .local v0, "intent":Landroid/content/Intent;
    const/4 v1, 0x0

    const/high16 v2, 0x8000000

    invoke-static {p1, v1, v0, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    return-object v1
.end method

.method public static isDump()Z
    .locals 1

    .prologue
    .line 20
    const/4 v0, 0x1

    return v0
.end method

.method public static isGPLSupported(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 26
    invoke-static {}, Lcom/qualcomm/qmapbridge/GplsBridgeMgr;->isDump()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 31
    :cond_0
    return v1
.end method


# virtual methods
.method public Connect()V
    .locals 0

    .prologue
    .line 64
    return-void
.end method

.method public Disconnect()V
    .locals 0

    .prologue
    .line 69
    return-void
.end method

.method IsConnected()Z
    .locals 1

    .prologue
    .line 73
    iget-boolean v0, p0, Lcom/qualcomm/qmapbridge/GplsBridgeMgr;->bConnected:Z

    return v0
.end method

.method public ReceiveGeofenceAlertEvent(Ljava/lang/String;I)V
    .locals 0
    .param p1, "strReqId"    # Ljava/lang/String;
    .param p2, "transitionType"    # I

    .prologue
    .line 141
    return-void
.end method

.method public addProximityAlert(Ljava/lang/String;DDFJJLcom/qualcomm/qmapbridge/GplsBridgeMgr$OnGeoFenceAlertEventListener;)V
    .locals 3
    .param p1, "strReqName"    # Ljava/lang/String;
    .param p2, "latitude"    # D
    .param p4, "longitude"    # D
    .param p6, "radius"    # F
    .param p7, "expiration"    # J
    .param p9, "responsivenessMs"    # J
    .param p11, "alertListener"    # Lcom/qualcomm/qmapbridge/GplsBridgeMgr$OnGeoFenceAlertEventListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 118
    sget-object v0, Lcom/qualcomm/qmapbridge/GplsBridgeMgr;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "addProximityAlert: ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4, p5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p7, p8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p9, p10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",3)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    invoke-virtual {p0}, Lcom/qualcomm/qmapbridge/GplsBridgeMgr;->IsConnected()Z

    move-result v0

    if-nez v0, :cond_0

    .line 123
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "GplsBridgeMgr. addProximityAlert failed GPL service not connected"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 125
    :cond_0
    return-void
.end method

.method public removeProximityAlert(Ljava/lang/String;)V
    .locals 3
    .param p1, "strReqName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 129
    sget-object v0, Lcom/qualcomm/qmapbridge/GplsBridgeMgr;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "removeProximityAlert: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    invoke-virtual {p0}, Lcom/qualcomm/qmapbridge/GplsBridgeMgr;->IsConnected()Z

    move-result v0

    if-nez v0, :cond_0

    .line 133
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "GplsBridgeMgr. removeProximityAlert failed GPL service not connected"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 135
    :cond_0
    return-void
.end method

.method public removeUpdates(Lcom/qualcomm/qmapbridge/GplsBridgeMgr$OnLocationChangedEventListener;)V
    .locals 2
    .param p1, "extListner"    # Lcom/qualcomm/qmapbridge/GplsBridgeMgr$OnLocationChangedEventListener;

    .prologue
    .line 90
    sget-object v0, Lcom/qualcomm/qmapbridge/GplsBridgeMgr;->TAG:Ljava/lang/String;

    const-string v1, "removeUpdates"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    invoke-virtual {p0}, Lcom/qualcomm/qmapbridge/GplsBridgeMgr;->IsConnected()Z

    move-result v0

    if-nez v0, :cond_0

    .line 94
    sget-object v0, Lcom/qualcomm/qmapbridge/GplsBridgeMgr;->TAG:Ljava/lang/String;

    const-string v1, "exit removeUpdates: no connection"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    :cond_0
    return-void
.end method

.method public requestLocationUpdates(Lcom/qualcomm/qmapbridge/GplsBridgeLocationRequest;Lcom/qualcomm/qmapbridge/GplsBridgeMgr$OnLocationChangedEventListener;Landroid/os/Looper;)V
    .locals 2
    .param p1, "request"    # Lcom/qualcomm/qmapbridge/GplsBridgeLocationRequest;
    .param p2, "extListner"    # Lcom/qualcomm/qmapbridge/GplsBridgeMgr$OnLocationChangedEventListener;
    .param p3, "looper"    # Landroid/os/Looper;

    .prologue
    .line 79
    sget-object v0, Lcom/qualcomm/qmapbridge/GplsBridgeMgr;->TAG:Ljava/lang/String;

    const-string v1, "requestLocationUpdates"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    invoke-virtual {p0}, Lcom/qualcomm/qmapbridge/GplsBridgeMgr;->IsConnected()Z

    move-result v0

    if-nez v0, :cond_0

    .line 83
    sget-object v0, Lcom/qualcomm/qmapbridge/GplsBridgeMgr;->TAG:Ljava/lang/String;

    const-string v1, "exit requestLocationUpdates: no connection"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    :cond_0
    return-void
.end method
