.class public Lcom/qualcomm/qmapbridge/MapHelper;
.super Ljava/lang/Object;
.source "MapHelper.java"


# static fields
.field static final R:D = 6371000.0


# instance fields
.field protected XPixelsToMeters:D

.field protected XSpanMeters:D

.field protected YPixelsToMeters:D

.field protected YSpanMeters:D

.field protected lrLat:D

.field protected lrLon:D

.field protected ulLat:D

.field protected ulLon:D


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    const-wide/16 v0, 0x0

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput-wide v2, p0, Lcom/qualcomm/qmapbridge/MapHelper;->XPixelsToMeters:D

    .line 11
    iput-wide v2, p0, Lcom/qualcomm/qmapbridge/MapHelper;->YPixelsToMeters:D

    .line 12
    iput-wide v0, p0, Lcom/qualcomm/qmapbridge/MapHelper;->XSpanMeters:D

    .line 13
    iput-wide v0, p0, Lcom/qualcomm/qmapbridge/MapHelper;->YSpanMeters:D

    .line 15
    iput-wide v0, p0, Lcom/qualcomm/qmapbridge/MapHelper;->ulLat:D

    .line 16
    iput-wide v0, p0, Lcom/qualcomm/qmapbridge/MapHelper;->ulLon:D

    .line 18
    iput-wide v0, p0, Lcom/qualcomm/qmapbridge/MapHelper;->lrLat:D

    .line 19
    iput-wide v0, p0, Lcom/qualcomm/qmapbridge/MapHelper;->lrLon:D

    .line 65
    return-void
.end method

.method public static azimuthToCircularDegs(D)D
    .locals 6
    .param p0, "a"    # D

    .prologue
    const-wide v4, 0x4056800000000000L    # 90.0

    .line 120
    const-wide/16 v0, 0x0

    .line 121
    .local v0, "degs":D
    const-wide/16 v2, 0x0

    cmpl-double v2, p0, v2

    if-ltz v2, :cond_0

    cmpg-double v2, p0, v4

    if-gtz v2, :cond_0

    .line 122
    sub-double v0, v4, p0

    .line 127
    :goto_0
    return-wide v0

    .line 125
    :cond_0
    const-wide v2, 0x407c200000000000L    # 450.0

    sub-double v0, v2, p0

    goto :goto_0
.end method

.method public static distance(DDDD)D
    .locals 16
    .param p0, "lat1"    # D
    .param p2, "lon1"    # D
    .param p4, "lat2"    # D
    .param p6, "lon2"    # D

    .prologue
    .line 150
    invoke-static/range {p0 .. p1}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide p0

    .line 151
    invoke-static/range {p2 .. p3}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide p2

    .line 152
    invoke-static/range {p4 .. p5}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide p4

    .line 153
    invoke-static/range {p6 .. p7}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide p6

    .line 154
    sub-double v4, p4, p0

    .line 155
    .local v4, "dLat":D
    sub-double v6, p6, p2

    .line 156
    .local v6, "dLon":D
    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    div-double v10, v4, v10

    invoke-static {v10, v11}, Ljava/lang/Math;->sin(D)D

    move-result-wide v10

    const-wide/high16 v12, 0x4000000000000000L    # 2.0

    div-double v12, v4, v12

    invoke-static {v12, v13}, Ljava/lang/Math;->sin(D)D

    move-result-wide v12

    mul-double/2addr v10, v12

    const-wide/high16 v12, 0x4000000000000000L    # 2.0

    div-double v12, v6, v12

    invoke-static {v12, v13}, Ljava/lang/Math;->sin(D)D

    move-result-wide v12

    const-wide/high16 v14, 0x4000000000000000L    # 2.0

    div-double v14, v6, v14

    invoke-static {v14, v15}, Ljava/lang/Math;->sin(D)D

    move-result-wide v14

    mul-double/2addr v12, v14

    invoke-static/range {p0 .. p1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v14

    mul-double/2addr v12, v14

    invoke-static/range {p4 .. p5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v14

    mul-double/2addr v12, v14

    add-double v0, v10, v12

    .line 157
    .local v0, "a":D
    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v12

    const-wide/high16 v14, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v14, v0

    invoke-static {v14, v15}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v14

    invoke-static {v12, v13, v14, v15}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v12

    mul-double v2, v10, v12

    .line 158
    .local v2, "c":D
    const-wide v10, 0x41584dae00000000L    # 6371000.0

    mul-double v8, v10, v2

    .line 159
    .local v8, "distance":D
    return-wide v8
.end method

.method public static microToDegress(I)D
    .locals 4
    .param p0, "microDegrees"    # I

    .prologue
    .line 137
    int-to-double v0, p0

    const-wide v2, 0x412e848000000000L    # 1000000.0

    div-double/2addr v0, v2

    return-wide v0
.end method


# virtual methods
.method convertToPositiveLon(D)D
    .locals 3
    .param p1, "lon"    # D

    .prologue
    .line 58
    const-wide/16 v0, 0x0

    cmpl-double v0, p1, v0

    if-ltz v0, :cond_0

    .line 61
    .end local p1    # "lon":D
    :goto_0
    return-wide p1

    .restart local p1    # "lon":D
    :cond_0
    const-wide v0, 0x4076800000000000L    # 360.0

    add-double/2addr p1, v0

    goto :goto_0
.end method

.method public getXMetersToPixels()D
    .locals 4

    .prologue
    .line 78
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    iget-wide v2, p0, Lcom/qualcomm/qmapbridge/MapHelper;->XPixelsToMeters:D

    div-double/2addr v0, v2

    return-wide v0
.end method

.method public getXPixelsToMeters()D
    .locals 2

    .prologue
    .line 68
    iget-wide v0, p0, Lcom/qualcomm/qmapbridge/MapHelper;->XPixelsToMeters:D

    return-wide v0
.end method

.method public getXSpanMeters()D
    .locals 2

    .prologue
    .line 87
    iget-wide v0, p0, Lcom/qualcomm/qmapbridge/MapHelper;->XSpanMeters:D

    return-wide v0
.end method

.method public getYMetersToPixels()D
    .locals 4

    .prologue
    .line 83
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    iget-wide v2, p0, Lcom/qualcomm/qmapbridge/MapHelper;->YPixelsToMeters:D

    div-double/2addr v0, v2

    return-wide v0
.end method

.method public getYPixelsToMeters()D
    .locals 2

    .prologue
    .line 73
    iget-wide v0, p0, Lcom/qualcomm/qmapbridge/MapHelper;->YPixelsToMeters:D

    return-wide v0
.end method

.method public getYSpanMeters()D
    .locals 2

    .prologue
    .line 92
    iget-wide v0, p0, Lcom/qualcomm/qmapbridge/MapHelper;->YSpanMeters:D

    return-wide v0
.end method

.method public isInView(DD)Z
    .locals 11
    .param p1, "lat"    # D
    .param p3, "lon"    # D

    .prologue
    const/4 v4, 0x1

    const-wide/16 v8, 0x0

    const/4 v5, 0x0

    .line 30
    iget-wide v6, p0, Lcom/qualcomm/qmapbridge/MapHelper;->ulLat:D

    cmpl-double v6, p1, v6

    if-gtz v6, :cond_0

    iget-wide v6, p0, Lcom/qualcomm/qmapbridge/MapHelper;->lrLat:D

    cmpg-double v6, p1, v6

    if-gez v6, :cond_2

    :cond_0
    move v4, v5

    .line 54
    :cond_1
    :goto_0
    return v4

    .line 35
    :cond_2
    iget-wide v6, p0, Lcom/qualcomm/qmapbridge/MapHelper;->ulLon:D

    invoke-virtual {p0, v6, v7}, Lcom/qualcomm/qmapbridge/MapHelper;->convertToPositiveLon(D)D

    move-result-wide v2

    .line 36
    .local v2, "minLon":D
    iget-wide v6, p0, Lcom/qualcomm/qmapbridge/MapHelper;->lrLon:D

    invoke-virtual {p0, v6, v7}, Lcom/qualcomm/qmapbridge/MapHelper;->convertToPositiveLon(D)D

    move-result-wide v0

    .line 37
    .local v0, "maxLon":D
    invoke-virtual {p0, p3, p4}, Lcom/qualcomm/qmapbridge/MapHelper;->convertToPositiveLon(D)D

    move-result-wide p3

    .line 40
    sub-double v6, v0, v2

    cmpg-double v6, v6, v8

    if-gez v6, :cond_5

    .line 41
    cmpl-double v6, p3, v8

    if-ltz v6, :cond_3

    cmpg-double v6, p3, v0

    if-ltz v6, :cond_1

    :cond_3
    cmpl-double v6, p3, v2

    if-ltz v6, :cond_4

    const-wide v6, 0x4076800000000000L    # 360.0

    cmpg-double v6, p3, v6

    if-lez v6, :cond_1

    :cond_4
    move v4, v5

    .line 47
    goto :goto_0

    .line 50
    :cond_5
    cmpg-double v6, p3, v2

    if-ltz v6, :cond_6

    cmpl-double v6, p3, v0

    if-lez v6, :cond_1

    :cond_6
    move v4, v5

    .line 51
    goto :goto_0
.end method

.method public updateScale(DDLcom/qualcomm/qmapbridge/MapBridgeGeoPoint;Lcom/qualcomm/qmapbridge/MapBridgeGeoPoint;)V
    .locals 9
    .param p1, "widthPix"    # D
    .param p3, "heightPix"    # D
    .param p5, "ulPoint"    # Lcom/qualcomm/qmapbridge/MapBridgeGeoPoint;
    .param p6, "lrPoint"    # Lcom/qualcomm/qmapbridge/MapBridgeGeoPoint;

    .prologue
    .line 100
    invoke-virtual {p5}, Lcom/qualcomm/qmapbridge/MapBridgeGeoPoint;->getLatitudeE6()I

    move-result v0

    invoke-static {v0}, Lcom/qualcomm/qmapbridge/MapHelper;->microToDegress(I)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/qualcomm/qmapbridge/MapHelper;->ulLat:D

    .line 101
    invoke-virtual {p5}, Lcom/qualcomm/qmapbridge/MapBridgeGeoPoint;->getLongitudeE6()I

    move-result v0

    invoke-static {v0}, Lcom/qualcomm/qmapbridge/MapHelper;->microToDegress(I)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/qualcomm/qmapbridge/MapHelper;->ulLon:D

    .line 103
    invoke-virtual {p6}, Lcom/qualcomm/qmapbridge/MapBridgeGeoPoint;->getLatitudeE6()I

    move-result v0

    invoke-static {v0}, Lcom/qualcomm/qmapbridge/MapHelper;->microToDegress(I)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/qualcomm/qmapbridge/MapHelper;->lrLat:D

    .line 104
    invoke-virtual {p6}, Lcom/qualcomm/qmapbridge/MapBridgeGeoPoint;->getLongitudeE6()I

    move-result v0

    invoke-static {v0}, Lcom/qualcomm/qmapbridge/MapHelper;->microToDegress(I)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/qualcomm/qmapbridge/MapHelper;->lrLon:D

    .line 106
    iget-wide v0, p0, Lcom/qualcomm/qmapbridge/MapHelper;->ulLat:D

    iget-wide v2, p0, Lcom/qualcomm/qmapbridge/MapHelper;->lrLon:D

    iget-wide v4, p0, Lcom/qualcomm/qmapbridge/MapHelper;->ulLat:D

    iget-wide v6, p0, Lcom/qualcomm/qmapbridge/MapHelper;->ulLon:D

    invoke-static/range {v0 .. v7}, Lcom/qualcomm/qmapbridge/MapHelper;->distance(DDDD)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/qualcomm/qmapbridge/MapHelper;->XSpanMeters:D

    .line 107
    iget-wide v0, p0, Lcom/qualcomm/qmapbridge/MapHelper;->lrLat:D

    iget-wide v2, p0, Lcom/qualcomm/qmapbridge/MapHelper;->lrLon:D

    iget-wide v4, p0, Lcom/qualcomm/qmapbridge/MapHelper;->ulLat:D

    iget-wide v6, p0, Lcom/qualcomm/qmapbridge/MapHelper;->lrLon:D

    invoke-static/range {v0 .. v7}, Lcom/qualcomm/qmapbridge/MapHelper;->distance(DDDD)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/qualcomm/qmapbridge/MapHelper;->YSpanMeters:D

    .line 109
    iget-wide v0, p0, Lcom/qualcomm/qmapbridge/MapHelper;->XSpanMeters:D

    div-double v0, p1, v0

    iput-wide v0, p0, Lcom/qualcomm/qmapbridge/MapHelper;->XPixelsToMeters:D

    .line 110
    iget-wide v0, p0, Lcom/qualcomm/qmapbridge/MapHelper;->YSpanMeters:D

    div-double v0, p3, v0

    iput-wide v0, p0, Lcom/qualcomm/qmapbridge/MapHelper;->YPixelsToMeters:D

    .line 111
    return-void
.end method
