.class public Lcom/qualcomm/qmapbridge/MapBridgeGoogleMapView;
.super Landroid/view/ViewGroup;
.source "MapBridgeGoogleMapView.java"


# instance fields
.field protected bSatellite:Z

.field protected bTraffic:Z

.field protected clearFixesIndex:I

.field protected drawHeadingPosMarker:Z

.field protected drawScaleAndLegend:Z

.field protected drawUncHistory:Z

.field protected drawVelVector:Z

.field protected mapHelper:Lcom/qualcomm/qmapbridge/MapHelper;

.field protected posArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/qualcomm/qmapbridge/MapViewPosition;",
            ">;"
        }
    .end annotation
.end field

.field protected posMarkerRadius:I

.field protected rectF:Landroid/graphics/RectF;

.field protected replayStartPointIndex:I

.field protected replayStopPointIndex:I

.field protected replaySubPointsSetByUser:Z

.field protected satelliteViewOn:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "arg0"    # Landroid/content/Context;
    .param p2, "arg1"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 56
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 163
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/qualcomm/qmapbridge/MapBridgeGoogleMapView;->rectF:Landroid/graphics/RectF;

    .line 164
    iput v1, p0, Lcom/qualcomm/qmapbridge/MapBridgeGoogleMapView;->posMarkerRadius:I

    .line 165
    iput v1, p0, Lcom/qualcomm/qmapbridge/MapBridgeGoogleMapView;->clearFixesIndex:I

    .line 166
    iput v1, p0, Lcom/qualcomm/qmapbridge/MapBridgeGoogleMapView;->replayStartPointIndex:I

    .line 167
    iput v1, p0, Lcom/qualcomm/qmapbridge/MapBridgeGoogleMapView;->replayStopPointIndex:I

    .line 168
    iput-boolean v1, p0, Lcom/qualcomm/qmapbridge/MapBridgeGoogleMapView;->replaySubPointsSetByUser:Z

    .line 169
    iput-boolean v2, p0, Lcom/qualcomm/qmapbridge/MapBridgeGoogleMapView;->drawVelVector:Z

    .line 171
    iput-boolean v1, p0, Lcom/qualcomm/qmapbridge/MapBridgeGoogleMapView;->drawUncHistory:Z

    .line 172
    iput-boolean v2, p0, Lcom/qualcomm/qmapbridge/MapBridgeGoogleMapView;->drawScaleAndLegend:Z

    .line 173
    iput-boolean v2, p0, Lcom/qualcomm/qmapbridge/MapBridgeGoogleMapView;->satelliteViewOn:Z

    .line 175
    iput-boolean v1, p0, Lcom/qualcomm/qmapbridge/MapBridgeGoogleMapView;->drawHeadingPosMarker:Z

    .line 177
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/qualcomm/qmapbridge/MapBridgeGoogleMapView;->posArray:Ljava/util/ArrayList;

    .line 57
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "arg0"    # Landroid/content/Context;
    .param p2, "arg1"    # Landroid/util/AttributeSet;
    .param p3, "arg2"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 61
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 163
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/qualcomm/qmapbridge/MapBridgeGoogleMapView;->rectF:Landroid/graphics/RectF;

    .line 164
    iput v1, p0, Lcom/qualcomm/qmapbridge/MapBridgeGoogleMapView;->posMarkerRadius:I

    .line 165
    iput v1, p0, Lcom/qualcomm/qmapbridge/MapBridgeGoogleMapView;->clearFixesIndex:I

    .line 166
    iput v1, p0, Lcom/qualcomm/qmapbridge/MapBridgeGoogleMapView;->replayStartPointIndex:I

    .line 167
    iput v1, p0, Lcom/qualcomm/qmapbridge/MapBridgeGoogleMapView;->replayStopPointIndex:I

    .line 168
    iput-boolean v1, p0, Lcom/qualcomm/qmapbridge/MapBridgeGoogleMapView;->replaySubPointsSetByUser:Z

    .line 169
    iput-boolean v2, p0, Lcom/qualcomm/qmapbridge/MapBridgeGoogleMapView;->drawVelVector:Z

    .line 171
    iput-boolean v1, p0, Lcom/qualcomm/qmapbridge/MapBridgeGoogleMapView;->drawUncHistory:Z

    .line 172
    iput-boolean v2, p0, Lcom/qualcomm/qmapbridge/MapBridgeGoogleMapView;->drawScaleAndLegend:Z

    .line 173
    iput-boolean v2, p0, Lcom/qualcomm/qmapbridge/MapBridgeGoogleMapView;->satelliteViewOn:Z

    .line 175
    iput-boolean v1, p0, Lcom/qualcomm/qmapbridge/MapBridgeGoogleMapView;->drawHeadingPosMarker:Z

    .line 177
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/qualcomm/qmapbridge/MapBridgeGoogleMapView;->posArray:Ljava/util/ArrayList;

    .line 62
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3
    .param p1, "arg0"    # Landroid/content/Context;
    .param p2, "arg1"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 51
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 163
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/qualcomm/qmapbridge/MapBridgeGoogleMapView;->rectF:Landroid/graphics/RectF;

    .line 164
    iput v1, p0, Lcom/qualcomm/qmapbridge/MapBridgeGoogleMapView;->posMarkerRadius:I

    .line 165
    iput v1, p0, Lcom/qualcomm/qmapbridge/MapBridgeGoogleMapView;->clearFixesIndex:I

    .line 166
    iput v1, p0, Lcom/qualcomm/qmapbridge/MapBridgeGoogleMapView;->replayStartPointIndex:I

    .line 167
    iput v1, p0, Lcom/qualcomm/qmapbridge/MapBridgeGoogleMapView;->replayStopPointIndex:I

    .line 168
    iput-boolean v1, p0, Lcom/qualcomm/qmapbridge/MapBridgeGoogleMapView;->replaySubPointsSetByUser:Z

    .line 169
    iput-boolean v2, p0, Lcom/qualcomm/qmapbridge/MapBridgeGoogleMapView;->drawVelVector:Z

    .line 171
    iput-boolean v1, p0, Lcom/qualcomm/qmapbridge/MapBridgeGoogleMapView;->drawUncHistory:Z

    .line 172
    iput-boolean v2, p0, Lcom/qualcomm/qmapbridge/MapBridgeGoogleMapView;->drawScaleAndLegend:Z

    .line 173
    iput-boolean v2, p0, Lcom/qualcomm/qmapbridge/MapBridgeGoogleMapView;->satelliteViewOn:Z

    .line 175
    iput-boolean v1, p0, Lcom/qualcomm/qmapbridge/MapBridgeGoogleMapView;->drawHeadingPosMarker:Z

    .line 177
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/qualcomm/qmapbridge/MapBridgeGoogleMapView;->posArray:Ljava/util/ArrayList;

    .line 52
    return-void
.end method

.method public static isDump()Z
    .locals 1

    .prologue
    .line 12
    const/4 v0, 0x1

    return v0
.end method


# virtual methods
.method protected UpdateMapPosList()V
    .locals 0

    .prologue
    .line 159
    return-void
.end method

.method public canCoverCenter()Z
    .locals 1

    .prologue
    .line 106
    const/4 v0, 0x0

    return v0
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1
    .param p1, "arg0"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 100
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v0

    return v0
.end method

.method public computeScroll()V
    .locals 0

    .prologue
    .line 74
    invoke-super {p0}, Landroid/view/ViewGroup;->computeScroll()V

    return-void
.end method

.method public controller_animateTo(Lcom/qualcomm/qmapbridge/MapBridgeGeoPoint;)V
    .locals 0
    .param p1, "arg0"    # Lcom/qualcomm/qmapbridge/MapBridgeGeoPoint;

    .prologue
    .line 17
    return-void
.end method

.method public controller_animateTo(Lcom/qualcomm/qmapbridge/MapBridgeGeoPoint;Landroid/os/Message;)V
    .locals 0
    .param p1, "arg0"    # Lcom/qualcomm/qmapbridge/MapBridgeGeoPoint;
    .param p2, "arg1"    # Landroid/os/Message;

    .prologue
    .line 21
    return-void
.end method

.method public controller_animateTo(Lcom/qualcomm/qmapbridge/MapBridgeGeoPoint;Ljava/lang/Runnable;)V
    .locals 0
    .param p1, "arg0"    # Lcom/qualcomm/qmapbridge/MapBridgeGeoPoint;
    .param p2, "arg1"    # Ljava/lang/Runnable;

    .prologue
    .line 25
    return-void
.end method

.method public controller_setZoom(I)I
    .locals 1
    .param p1, "arg0"    # I

    .prologue
    .line 29
    const/4 v0, 0x0

    return v0
.end method

.method public controller_zoomToSpan(II)V
    .locals 0
    .param p1, "arg0"    # I
    .param p2, "arg1"    # I

    .prologue
    .line 34
    return-void
.end method

.method public displayZoomControls(Z)V
    .locals 0
    .param p1, "arg0"    # Z

    .prologue
    .line 104
    return-void
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1
    .param p1, "arg0"    # Landroid/util/AttributeSet;

    .prologue
    .line 98
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1
    .param p1, "arg0"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 102
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method public getLatitudeSpan()I
    .locals 1

    .prologue
    .line 122
    const/4 v0, 0x0

    return v0
.end method

.method public getLongitudeSpan()I
    .locals 1

    .prologue
    .line 123
    const/4 v0, 0x0

    return v0
.end method

.method public getMapCenter()Lcom/qualcomm/qmapbridge/MapBridgeGeoPoint;
    .locals 1

    .prologue
    .line 120
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMaxZoomLevel()I
    .locals 1

    .prologue
    .line 127
    const/4 v0, 0x1

    return v0
.end method

.method public getZoomLevel()I
    .locals 1

    .prologue
    .line 110
    const/4 v0, 0x0

    return v0
.end method

.method public isOpaque()Z
    .locals 1

    .prologue
    .line 76
    invoke-super {p0}, Landroid/view/ViewGroup;->isOpaque()Z

    move-result v0

    return v0
.end method

.method public isSatellite()Z
    .locals 1

    .prologue
    .line 114
    iget-boolean v0, p0, Lcom/qualcomm/qmapbridge/MapBridgeGoogleMapView;->bSatellite:Z

    return v0
.end method

.method public isTraffic()Z
    .locals 1

    .prologue
    .line 118
    iget-boolean v0, p0, Lcom/qualcomm/qmapbridge/MapBridgeGoogleMapView;->bTraffic:Z

    return v0
.end method

.method public mapHelper_updateScale()V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 149
    invoke-virtual {p0}, Lcom/qualcomm/qmapbridge/MapBridgeGoogleMapView;->getWidth()I

    move-result v8

    .line 150
    .local v8, "widthPix":I
    invoke-virtual {p0}, Lcom/qualcomm/qmapbridge/MapBridgeGoogleMapView;->getHeight()I

    move-result v0

    .line 151
    .local v0, "heightPix":I
    invoke-virtual {p0, v1, v1}, Lcom/qualcomm/qmapbridge/MapBridgeGoogleMapView;->projection_fromPixels(II)Lcom/qualcomm/qmapbridge/MapBridgeGeoPoint;

    move-result-object v6

    .line 152
    .local v6, "ulPoint":Lcom/qualcomm/qmapbridge/MapBridgeGeoPoint;
    invoke-virtual {p0, v8, v0}, Lcom/qualcomm/qmapbridge/MapBridgeGoogleMapView;->projection_fromPixels(II)Lcom/qualcomm/qmapbridge/MapBridgeGeoPoint;

    move-result-object v7

    .line 153
    .local v7, "lrPoint":Lcom/qualcomm/qmapbridge/MapBridgeGeoPoint;
    iget-object v1, p0, Lcom/qualcomm/qmapbridge/MapBridgeGoogleMapView;->mapHelper:Lcom/qualcomm/qmapbridge/MapHelper;

    int-to-double v2, v8

    int-to-double v4, v0

    invoke-virtual/range {v1 .. v7}, Lcom/qualcomm/qmapbridge/MapHelper;->updateScale(DDLcom/qualcomm/qmapbridge/MapBridgeGeoPoint;Lcom/qualcomm/qmapbridge/MapBridgeGeoPoint;)V

    .line 154
    return-void
.end method

.method public mapInitiate()V
    .locals 1

    .prologue
    .line 139
    new-instance v0, Lcom/qualcomm/qmapbridge/MapHelper;

    invoke-direct {v0}, Lcom/qualcomm/qmapbridge/MapHelper;-><init>()V

    iput-object v0, p0, Lcom/qualcomm/qmapbridge/MapBridgeGoogleMapView;->mapHelper:Lcom/qualcomm/qmapbridge/MapHelper;

    .line 140
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 72
    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    return-void
.end method

.method protected final onDraw(Landroid/graphics/Canvas;)V
    .locals 0
    .param p1, "arg0"    # Landroid/graphics/Canvas;

    .prologue
    .line 78
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onDraw(Landroid/graphics/Canvas;)V

    return-void
.end method

.method public onFocusChanged(ZILandroid/graphics/Rect;)V
    .locals 0
    .param p1, "arg0"    # Z
    .param p2, "arg1"    # I
    .param p3, "arg2"    # Landroid/graphics/Rect;

    .prologue
    .line 84
    invoke-super {p0, p1, p2, p3}, Landroid/view/ViewGroup;->onFocusChanged(ZILandroid/graphics/Rect;)V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "arg0"    # I
    .param p2, "arg1"    # Landroid/view/KeyEvent;

    .prologue
    .line 86
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "arg0"    # I
    .param p2, "arg1"    # Landroid/view/KeyEvent;

    .prologue
    .line 88
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method protected onLayout(ZIIII)V
    .locals 0
    .param p1, "arg0"    # Z
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "arg3"    # I
    .param p5, "arg4"    # I

    .prologue
    .line 66
    return-void
.end method

.method protected final onMeasure(II)V
    .locals 0
    .param p1, "arg0"    # I
    .param p2, "arg1"    # I

    .prologue
    .line 80
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "arg0"    # Landroid/os/Bundle;

    .prologue
    .line 131
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "arg0"    # Landroid/os/Bundle;

    .prologue
    .line 129
    invoke-super {p0}, Landroid/view/ViewGroup;->onSaveInstanceState()Landroid/os/Parcelable;

    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 0
    .param p1, "arg0"    # I
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "arg3"    # I

    .prologue
    .line 70
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->onSizeChanged(IIII)V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "arg0"    # Landroid/view/MotionEvent;

    .prologue
    .line 92
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onTrackballEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "arg0"    # Landroid/view/MotionEvent;

    .prologue
    .line 90
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onTrackballEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method protected onVisibilityChanged(Landroid/view/View;I)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "arg1"    # I

    .prologue
    .line 94
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onVisibilityChanged(Landroid/view/View;I)V

    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 0
    .param p1, "arg0"    # Z

    .prologue
    .line 82
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onWindowFocusChanged(Z)V

    return-void
.end method

.method public overlay_zoomDataExtents()V
    .locals 0

    .prologue
    .line 145
    return-void
.end method

.method public preLoad()V
    .locals 0

    .prologue
    .line 108
    return-void
.end method

.method public projection_fromPixels(II)Lcom/qualcomm/qmapbridge/MapBridgeGeoPoint;
    .locals 1
    .param p1, "arg0"    # I
    .param p2, "arg1"    # I

    .prologue
    .line 43
    new-instance v0, Lcom/qualcomm/qmapbridge/MapBridgeGeoPoint;

    invoke-direct {v0, p1, p2}, Lcom/qualcomm/qmapbridge/MapBridgeGeoPoint;-><init>(II)V

    .line 44
    .local v0, "point":Lcom/qualcomm/qmapbridge/MapBridgeGeoPoint;
    return-object v0
.end method

.method public projection_toPixels(Lcom/qualcomm/qmapbridge/MapBridgeGeoPoint;Landroid/graphics/Point;)Landroid/graphics/Point;
    .locals 0
    .param p1, "arg0"    # Lcom/qualcomm/qmapbridge/MapBridgeGeoPoint;
    .param p2, "arg1"    # Landroid/graphics/Point;

    .prologue
    .line 38
    return-object p2
.end method

.method public setBuiltInZoomControls(Z)V
    .locals 0
    .param p1, "arg0"    # Z

    .prologue
    .line 133
    return-void
.end method

.method public setSatellite(Z)V
    .locals 0
    .param p1, "arg0"    # Z

    .prologue
    .line 113
    iput-boolean p1, p0, Lcom/qualcomm/qmapbridge/MapBridgeGoogleMapView;->bSatellite:Z

    return-void
.end method

.method public setTraffic(Z)V
    .locals 0
    .param p1, "arg0"    # Z

    .prologue
    .line 117
    iput-boolean p1, p0, Lcom/qualcomm/qmapbridge/MapBridgeGoogleMapView;->bTraffic:Z

    return-void
.end method
