.class Ljava/nio/DatagramChannelImpl$DatagramSocketAdapter;
.super Ljava/net/DatagramSocket;
.source "DatagramChannelImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ljava/nio/DatagramChannelImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DatagramSocketAdapter"
.end annotation


# instance fields
.field private final channelImpl:Ljava/nio/DatagramChannelImpl;


# direct methods
.method constructor <init>(Ljava/net/DatagramSocketImpl;Ljava/nio/DatagramChannelImpl;)V
    .locals 2
    .param p1, "socketimpl"    # Ljava/net/DatagramSocketImpl;
    .param p2, "channelImpl"    # Ljava/nio/DatagramChannelImpl;

    .prologue
    .line 527
    invoke-direct {p0, p1}, Ljava/net/DatagramSocket;-><init>(Ljava/net/DatagramSocketImpl;)V

    .line 528
    iput-object p2, p0, Ljava/nio/DatagramChannelImpl$DatagramSocketAdapter;->channelImpl:Ljava/nio/DatagramChannelImpl;

    .line 531
    iget-boolean v0, p2, Ljava/nio/DatagramChannelImpl;->isBound:Z

    if-eqz v0, :cond_0

    .line 532
    iget-object v0, p2, Ljava/nio/DatagramChannelImpl;->localAddress:Ljava/net/InetAddress;

    # getter for: Ljava/nio/DatagramChannelImpl;->localPort:I
    invoke-static {p2}, Ljava/nio/DatagramChannelImpl;->access$000(Ljava/nio/DatagramChannelImpl;)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Ljava/nio/DatagramChannelImpl$DatagramSocketAdapter;->onBind(Ljava/net/InetAddress;I)V

    .line 534
    :cond_0
    iget-boolean v0, p2, Ljava/nio/DatagramChannelImpl;->connected:Z

    if-eqz v0, :cond_2

    .line 535
    iget-object v0, p2, Ljava/nio/DatagramChannelImpl;->connectAddress:Ljava/net/InetSocketAddress;

    invoke-virtual {v0}, Ljava/net/InetSocketAddress;->getAddress()Ljava/net/InetAddress;

    move-result-object v0

    iget-object v1, p2, Ljava/nio/DatagramChannelImpl;->connectAddress:Ljava/net/InetSocketAddress;

    invoke-virtual {v1}, Ljava/net/InetSocketAddress;->getPort()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Ljava/nio/DatagramChannelImpl$DatagramSocketAdapter;->onConnect(Ljava/net/InetAddress;I)V

    .line 541
    :goto_0
    invoke-virtual {p2}, Ljava/nio/DatagramChannelImpl;->isOpen()Z

    move-result v0

    if-nez v0, :cond_1

    .line 542
    invoke-virtual {p0}, Ljava/nio/DatagramChannelImpl$DatagramSocketAdapter;->onClose()V

    .line 544
    :cond_1
    return-void

    .line 539
    :cond_2
    invoke-virtual {p0}, Ljava/nio/DatagramChannelImpl$DatagramSocketAdapter;->onDisconnect()V

    goto :goto_0
.end method


# virtual methods
.method public bind(Ljava/net/SocketAddress;)V
    .locals 2
    .param p1, "localAddr"    # Ljava/net/SocketAddress;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/SocketException;
        }
    .end annotation

    .prologue
    .line 556
    iget-object v0, p0, Ljava/nio/DatagramChannelImpl$DatagramSocketAdapter;->channelImpl:Ljava/nio/DatagramChannelImpl;

    invoke-virtual {v0}, Ljava/nio/DatagramChannelImpl;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 557
    new-instance v0, Ljava/nio/channels/AlreadyConnectedException;

    invoke-direct {v0}, Ljava/nio/channels/AlreadyConnectedException;-><init>()V

    throw v0

    .line 559
    :cond_0
    invoke-super {p0, p1}, Ljava/net/DatagramSocket;->bind(Ljava/net/SocketAddress;)V

    .line 560
    iget-object v0, p0, Ljava/nio/DatagramChannelImpl$DatagramSocketAdapter;->channelImpl:Ljava/nio/DatagramChannelImpl;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/nio/DatagramChannelImpl;->onBind(Z)V

    .line 561
    return-void
.end method

.method public close()V
    .locals 2

    .prologue
    .line 624
    iget-object v1, p0, Ljava/nio/DatagramChannelImpl$DatagramSocketAdapter;->channelImpl:Ljava/nio/DatagramChannelImpl;

    monitor-enter v1

    .line 625
    :try_start_0
    invoke-super {p0}, Ljava/net/DatagramSocket;->close()V

    .line 626
    iget-object v0, p0, Ljava/nio/DatagramChannelImpl$DatagramSocketAdapter;->channelImpl:Ljava/nio/DatagramChannelImpl;

    invoke-virtual {v0}, Ljava/nio/DatagramChannelImpl;->isOpen()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 628
    :try_start_1
    iget-object v0, p0, Ljava/nio/DatagramChannelImpl$DatagramSocketAdapter;->channelImpl:Ljava/nio/DatagramChannelImpl;

    invoke-virtual {v0}, Ljava/nio/DatagramChannelImpl;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 633
    :cond_0
    :goto_0
    :try_start_2
    monitor-exit v1

    .line 634
    return-void

    .line 633
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 629
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public connect(Ljava/net/InetAddress;I)V
    .locals 1
    .param p1, "address"    # Ljava/net/InetAddress;
    .param p2, "port"    # I

    .prologue
    .line 584
    :try_start_0
    new-instance v0, Ljava/net/InetSocketAddress;

    invoke-direct {v0, p1, p2}, Ljava/net/InetSocketAddress;-><init>(Ljava/net/InetAddress;I)V

    invoke-virtual {p0, v0}, Ljava/nio/DatagramChannelImpl$DatagramSocketAdapter;->connect(Ljava/net/SocketAddress;)V
    :try_end_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_0

    .line 588
    :goto_0
    return-void

    .line 585
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public connect(Ljava/net/SocketAddress;)V
    .locals 5
    .param p1, "peer"    # Ljava/net/SocketAddress;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/SocketException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 565
    invoke-virtual {p0}, Ljava/nio/DatagramChannelImpl$DatagramSocketAdapter;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 567
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Socket is already connected."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 569
    :cond_0
    invoke-super {p0, p1}, Ljava/net/DatagramSocket;->connect(Ljava/net/SocketAddress;)V

    .line 571
    iget-object v1, p0, Ljava/nio/DatagramChannelImpl$DatagramSocketAdapter;->channelImpl:Ljava/nio/DatagramChannelImpl;

    invoke-virtual {v1, v4}, Ljava/nio/DatagramChannelImpl;->onBind(Z)V

    move-object v0, p1

    .line 573
    check-cast v0, Ljava/net/InetSocketAddress;

    .line 574
    .local v0, "inetSocketAddress":Ljava/net/InetSocketAddress;
    iget-object v1, p0, Ljava/nio/DatagramChannelImpl$DatagramSocketAdapter;->channelImpl:Ljava/nio/DatagramChannelImpl;

    invoke-virtual {v0}, Ljava/net/InetSocketAddress;->getAddress()Ljava/net/InetAddress;

    move-result-object v2

    invoke-virtual {v0}, Ljava/net/InetSocketAddress;->getPort()I

    move-result v3

    invoke-virtual {v1, v2, v3, v4}, Ljava/nio/DatagramChannelImpl;->onConnect(Ljava/net/InetAddress;IZ)V

    .line 577
    return-void
.end method

.method public disconnect()V
    .locals 2

    .prologue
    .line 638
    invoke-super {p0}, Ljava/net/DatagramSocket;->disconnect()V

    .line 639
    iget-object v0, p0, Ljava/nio/DatagramChannelImpl$DatagramSocketAdapter;->channelImpl:Ljava/nio/DatagramChannelImpl;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/nio/DatagramChannelImpl;->onDisconnect(Z)V

    .line 640
    return-void
.end method

.method public getChannel()Ljava/nio/channels/DatagramChannel;
    .locals 1

    .prologue
    .line 551
    iget-object v0, p0, Ljava/nio/DatagramChannelImpl$DatagramSocketAdapter;->channelImpl:Ljava/nio/DatagramChannelImpl;

    return-object v0
.end method

.method public receive(Ljava/net/DatagramPacket;)V
    .locals 3
    .param p1, "packet"    # Ljava/net/DatagramPacket;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 592
    iget-object v1, p0, Ljava/nio/DatagramChannelImpl$DatagramSocketAdapter;->channelImpl:Ljava/nio/DatagramChannelImpl;

    invoke-virtual {v1}, Ljava/nio/DatagramChannelImpl;->isBlocking()Z

    move-result v1

    if-nez v1, :cond_0

    .line 593
    new-instance v1, Ljava/nio/channels/IllegalBlockingModeException;

    invoke-direct {v1}, Ljava/nio/channels/IllegalBlockingModeException;-><init>()V

    throw v1

    .line 596
    :cond_0
    invoke-virtual {p0}, Ljava/nio/DatagramChannelImpl$DatagramSocketAdapter;->isBound()Z

    move-result v0

    .line 597
    .local v0, "wasBound":Z
    invoke-super {p0, p1}, Ljava/net/DatagramSocket;->receive(Ljava/net/DatagramPacket;)V

    .line 598
    if-nez v0, :cond_1

    .line 601
    iget-object v1, p0, Ljava/nio/DatagramChannelImpl$DatagramSocketAdapter;->channelImpl:Ljava/nio/DatagramChannelImpl;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/nio/DatagramChannelImpl;->onBind(Z)V

    .line 603
    :cond_1
    return-void
.end method

.method public send(Ljava/net/DatagramPacket;)V
    .locals 3
    .param p1, "packet"    # Ljava/net/DatagramPacket;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 607
    iget-object v1, p0, Ljava/nio/DatagramChannelImpl$DatagramSocketAdapter;->channelImpl:Ljava/nio/DatagramChannelImpl;

    invoke-virtual {v1}, Ljava/nio/DatagramChannelImpl;->isBlocking()Z

    move-result v1

    if-nez v1, :cond_0

    .line 608
    new-instance v1, Ljava/nio/channels/IllegalBlockingModeException;

    invoke-direct {v1}, Ljava/nio/channels/IllegalBlockingModeException;-><init>()V

    throw v1

    .line 613
    :cond_0
    invoke-virtual {p0}, Ljava/nio/DatagramChannelImpl$DatagramSocketAdapter;->isBound()Z

    move-result v0

    .line 614
    .local v0, "wasBound":Z
    invoke-super {p0, p1}, Ljava/net/DatagramSocket;->send(Ljava/net/DatagramPacket;)V

    .line 615
    if-nez v0, :cond_1

    .line 618
    iget-object v1, p0, Ljava/nio/DatagramChannelImpl$DatagramSocketAdapter;->channelImpl:Ljava/nio/DatagramChannelImpl;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/nio/DatagramChannelImpl;->onBind(Z)V

    .line 620
    :cond_1
    return-void
.end method
