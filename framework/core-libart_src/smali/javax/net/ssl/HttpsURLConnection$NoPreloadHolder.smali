.class Ljavax/net/ssl/HttpsURLConnection$NoPreloadHolder;
.super Ljava/lang/Object;
.source "HttpsURLConnection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ljavax/net/ssl/HttpsURLConnection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "NoPreloadHolder"
.end annotation


# static fields
.field public static defaultHostnameVerifier:Ljavax/net/ssl/HostnameVerifier;

.field public static defaultSSLSocketFactory:Ljavax/net/ssl/SSLSocketFactory;

.field private static mdppHostnameVerifier:Ljavax/net/ssl/HostnameVerifier;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 120
    new-instance v0, Ljavax/net/ssl/DefaultHostnameVerifier;

    invoke-direct {v0}, Ljavax/net/ssl/DefaultHostnameVerifier;-><init>()V

    sput-object v0, Ljavax/net/ssl/HttpsURLConnection$NoPreloadHolder;->defaultHostnameVerifier:Ljavax/net/ssl/HostnameVerifier;

    .line 124
    new-instance v0, Ljavax/net/ssl/DefaultHostnameVerifier;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljavax/net/ssl/DefaultHostnameVerifier;-><init>(Z)V

    sput-object v0, Ljavax/net/ssl/HttpsURLConnection$NoPreloadHolder;->mdppHostnameVerifier:Ljavax/net/ssl/HostnameVerifier;

    .line 129
    invoke-static {}, Ljavax/net/ssl/SSLSocketFactory;->getDefault()Ljavax/net/SocketFactory;

    move-result-object v0

    check-cast v0, Ljavax/net/ssl/SSLSocketFactory;

    sput-object v0, Ljavax/net/ssl/HttpsURLConnection$NoPreloadHolder;->defaultSSLSocketFactory:Ljavax/net/ssl/SSLSocketFactory;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Ljavax/net/ssl/HostnameVerifier;
    .locals 1

    .prologue
    .line 119
    sget-object v0, Ljavax/net/ssl/HttpsURLConnection$NoPreloadHolder;->mdppHostnameVerifier:Ljavax/net/ssl/HostnameVerifier;

    return-object v0
.end method
