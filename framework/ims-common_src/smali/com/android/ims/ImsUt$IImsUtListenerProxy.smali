.class Lcom/android/ims/ImsUt$IImsUtListenerProxy;
.super Lcom/android/ims/internal/IImsUtListener$Stub;
.source "ImsUt.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/ims/ImsUt;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "IImsUtListenerProxy"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/ims/ImsUt;


# direct methods
.method private constructor <init>(Lcom/android/ims/ImsUt;)V
    .locals 0

    .prologue
    .line 583
    iput-object p1, p0, Lcom/android/ims/ImsUt$IImsUtListenerProxy;->this$0:Lcom/android/ims/ImsUt;

    invoke-direct {p0}, Lcom/android/ims/internal/IImsUtListener$Stub;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/ims/ImsUt;Lcom/android/ims/ImsUt$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/android/ims/ImsUt;
    .param p2, "x1"    # Lcom/android/ims/ImsUt$1;

    .prologue
    .line 583
    invoke-direct {p0, p1}, Lcom/android/ims/ImsUt$IImsUtListenerProxy;-><init>(Lcom/android/ims/ImsUt;)V

    return-void
.end method


# virtual methods
.method public utConfigurationCallBarringQueried(Lcom/android/ims/internal/IImsUt;I[Lcom/android/ims/ImsSsInfo;)V
    .locals 4
    .param p1, "ut"    # Lcom/android/ims/internal/IImsUt;
    .param p2, "id"    # I
    .param p3, "cbInfo"    # [Lcom/android/ims/ImsSsInfo;

    .prologue
    .line 636
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 638
    .local v0, "key":Ljava/lang/Integer;
    iget-object v1, p0, Lcom/android/ims/ImsUt$IImsUtListenerProxy;->this$0:Lcom/android/ims/ImsUt;

    # getter for: Lcom/android/ims/ImsUt;->mLockObj:Ljava/lang/Object;
    invoke-static {v1}, Lcom/android/ims/ImsUt;->access$100(Lcom/android/ims/ImsUt;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 639
    :try_start_0
    iget-object v3, p0, Lcom/android/ims/ImsUt$IImsUtListenerProxy;->this$0:Lcom/android/ims/ImsUt;

    iget-object v1, p0, Lcom/android/ims/ImsUt$IImsUtListenerProxy;->this$0:Lcom/android/ims/ImsUt;

    # getter for: Lcom/android/ims/ImsUt;->mPendingCmds:Ljava/util/HashMap;
    invoke-static {v1}, Lcom/android/ims/ImsUt;->access$200(Lcom/android/ims/ImsUt;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Message;

    # invokes: Lcom/android/ims/ImsUt;->sendSuccessReport(Landroid/os/Message;Ljava/lang/Object;)V
    invoke-static {v3, v1, p3}, Lcom/android/ims/ImsUt;->access$500(Lcom/android/ims/ImsUt;Landroid/os/Message;Ljava/lang/Object;)V

    .line 640
    iget-object v1, p0, Lcom/android/ims/ImsUt$IImsUtListenerProxy;->this$0:Lcom/android/ims/ImsUt;

    # getter for: Lcom/android/ims/ImsUt;->mPendingCmds:Ljava/util/HashMap;
    invoke-static {v1}, Lcom/android/ims/ImsUt;->access$200(Lcom/android/ims/ImsUt;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 641
    monitor-exit v2

    .line 642
    return-void

    .line 641
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public utConfigurationCallForwardQueried(Lcom/android/ims/internal/IImsUt;I[Lcom/android/ims/ImsCallForwardInfo;)V
    .locals 4
    .param p1, "ut"    # Lcom/android/ims/internal/IImsUt;
    .param p2, "id"    # I
    .param p3, "cfInfo"    # [Lcom/android/ims/ImsCallForwardInfo;

    .prologue
    .line 650
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 652
    .local v0, "key":Ljava/lang/Integer;
    iget-object v1, p0, Lcom/android/ims/ImsUt$IImsUtListenerProxy;->this$0:Lcom/android/ims/ImsUt;

    # getter for: Lcom/android/ims/ImsUt;->mLockObj:Ljava/lang/Object;
    invoke-static {v1}, Lcom/android/ims/ImsUt;->access$100(Lcom/android/ims/ImsUt;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 653
    :try_start_0
    iget-object v3, p0, Lcom/android/ims/ImsUt$IImsUtListenerProxy;->this$0:Lcom/android/ims/ImsUt;

    iget-object v1, p0, Lcom/android/ims/ImsUt$IImsUtListenerProxy;->this$0:Lcom/android/ims/ImsUt;

    # getter for: Lcom/android/ims/ImsUt;->mPendingCmds:Ljava/util/HashMap;
    invoke-static {v1}, Lcom/android/ims/ImsUt;->access$200(Lcom/android/ims/ImsUt;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Message;

    # invokes: Lcom/android/ims/ImsUt;->sendSuccessReport(Landroid/os/Message;Ljava/lang/Object;)V
    invoke-static {v3, v1, p3}, Lcom/android/ims/ImsUt;->access$500(Lcom/android/ims/ImsUt;Landroid/os/Message;Ljava/lang/Object;)V

    .line 654
    iget-object v1, p0, Lcom/android/ims/ImsUt$IImsUtListenerProxy;->this$0:Lcom/android/ims/ImsUt;

    # getter for: Lcom/android/ims/ImsUt;->mPendingCmds:Ljava/util/HashMap;
    invoke-static {v1}, Lcom/android/ims/ImsUt;->access$200(Lcom/android/ims/ImsUt;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 655
    monitor-exit v2

    .line 656
    return-void

    .line 655
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public utConfigurationCallWaitingQueried(Lcom/android/ims/internal/IImsUt;I[Lcom/android/ims/ImsSsInfo;)V
    .locals 4
    .param p1, "ut"    # Lcom/android/ims/internal/IImsUt;
    .param p2, "id"    # I
    .param p3, "cwInfo"    # [Lcom/android/ims/ImsSsInfo;

    .prologue
    .line 664
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 666
    .local v0, "key":Ljava/lang/Integer;
    iget-object v1, p0, Lcom/android/ims/ImsUt$IImsUtListenerProxy;->this$0:Lcom/android/ims/ImsUt;

    # getter for: Lcom/android/ims/ImsUt;->mLockObj:Ljava/lang/Object;
    invoke-static {v1}, Lcom/android/ims/ImsUt;->access$100(Lcom/android/ims/ImsUt;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 667
    :try_start_0
    iget-object v3, p0, Lcom/android/ims/ImsUt$IImsUtListenerProxy;->this$0:Lcom/android/ims/ImsUt;

    iget-object v1, p0, Lcom/android/ims/ImsUt$IImsUtListenerProxy;->this$0:Lcom/android/ims/ImsUt;

    # getter for: Lcom/android/ims/ImsUt;->mPendingCmds:Ljava/util/HashMap;
    invoke-static {v1}, Lcom/android/ims/ImsUt;->access$200(Lcom/android/ims/ImsUt;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Message;

    # invokes: Lcom/android/ims/ImsUt;->sendSuccessReport(Landroid/os/Message;Ljava/lang/Object;)V
    invoke-static {v3, v1, p3}, Lcom/android/ims/ImsUt;->access$500(Lcom/android/ims/ImsUt;Landroid/os/Message;Ljava/lang/Object;)V

    .line 668
    iget-object v1, p0, Lcom/android/ims/ImsUt$IImsUtListenerProxy;->this$0:Lcom/android/ims/ImsUt;

    # getter for: Lcom/android/ims/ImsUt;->mPendingCmds:Ljava/util/HashMap;
    invoke-static {v1}, Lcom/android/ims/ImsUt;->access$200(Lcom/android/ims/ImsUt;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 669
    monitor-exit v2

    .line 670
    return-void

    .line 669
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public utConfigurationQueried(Lcom/android/ims/internal/IImsUt;ILandroid/os/Bundle;)V
    .locals 4
    .param p1, "ut"    # Lcom/android/ims/internal/IImsUt;
    .param p2, "id"    # I
    .param p3, "ssInfo"    # Landroid/os/Bundle;

    .prologue
    .line 612
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 614
    .local v0, "key":Ljava/lang/Integer;
    iget-object v1, p0, Lcom/android/ims/ImsUt$IImsUtListenerProxy;->this$0:Lcom/android/ims/ImsUt;

    # getter for: Lcom/android/ims/ImsUt;->mLockObj:Ljava/lang/Object;
    invoke-static {v1}, Lcom/android/ims/ImsUt;->access$100(Lcom/android/ims/ImsUt;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 615
    :try_start_0
    iget-object v3, p0, Lcom/android/ims/ImsUt$IImsUtListenerProxy;->this$0:Lcom/android/ims/ImsUt;

    iget-object v1, p0, Lcom/android/ims/ImsUt$IImsUtListenerProxy;->this$0:Lcom/android/ims/ImsUt;

    # getter for: Lcom/android/ims/ImsUt;->mPendingCmds:Ljava/util/HashMap;
    invoke-static {v1}, Lcom/android/ims/ImsUt;->access$200(Lcom/android/ims/ImsUt;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Message;

    # invokes: Lcom/android/ims/ImsUt;->sendSuccessReport(Landroid/os/Message;Ljava/lang/Object;)V
    invoke-static {v3, v1, p3}, Lcom/android/ims/ImsUt;->access$500(Lcom/android/ims/ImsUt;Landroid/os/Message;Ljava/lang/Object;)V

    .line 616
    iget-object v1, p0, Lcom/android/ims/ImsUt$IImsUtListenerProxy;->this$0:Lcom/android/ims/ImsUt;

    # getter for: Lcom/android/ims/ImsUt;->mPendingCmds:Ljava/util/HashMap;
    invoke-static {v1}, Lcom/android/ims/ImsUt;->access$200(Lcom/android/ims/ImsUt;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 617
    monitor-exit v2

    .line 618
    return-void

    .line 617
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public utConfigurationQueryFailed(Lcom/android/ims/internal/IImsUt;ILcom/android/ims/ImsReasonInfo;)V
    .locals 4
    .param p1, "ut"    # Lcom/android/ims/internal/IImsUt;
    .param p2, "id"    # I
    .param p3, "error"    # Lcom/android/ims/ImsReasonInfo;

    .prologue
    .line 622
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 624
    .local v0, "key":Ljava/lang/Integer;
    iget-object v1, p0, Lcom/android/ims/ImsUt$IImsUtListenerProxy;->this$0:Lcom/android/ims/ImsUt;

    # getter for: Lcom/android/ims/ImsUt;->mLockObj:Ljava/lang/Object;
    invoke-static {v1}, Lcom/android/ims/ImsUt;->access$100(Lcom/android/ims/ImsUt;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 625
    :try_start_0
    iget-object v3, p0, Lcom/android/ims/ImsUt$IImsUtListenerProxy;->this$0:Lcom/android/ims/ImsUt;

    iget-object v1, p0, Lcom/android/ims/ImsUt$IImsUtListenerProxy;->this$0:Lcom/android/ims/ImsUt;

    # getter for: Lcom/android/ims/ImsUt;->mPendingCmds:Ljava/util/HashMap;
    invoke-static {v1}, Lcom/android/ims/ImsUt;->access$200(Lcom/android/ims/ImsUt;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Message;

    # invokes: Lcom/android/ims/ImsUt;->sendFailureReport(Landroid/os/Message;Lcom/android/ims/ImsReasonInfo;)V
    invoke-static {v3, v1, p3}, Lcom/android/ims/ImsUt;->access$400(Lcom/android/ims/ImsUt;Landroid/os/Message;Lcom/android/ims/ImsReasonInfo;)V

    .line 626
    iget-object v1, p0, Lcom/android/ims/ImsUt$IImsUtListenerProxy;->this$0:Lcom/android/ims/ImsUt;

    # getter for: Lcom/android/ims/ImsUt;->mPendingCmds:Ljava/util/HashMap;
    invoke-static {v1}, Lcom/android/ims/ImsUt;->access$200(Lcom/android/ims/ImsUt;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 627
    monitor-exit v2

    .line 628
    return-void

    .line 627
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public utConfigurationUpdateFailed(Lcom/android/ims/internal/IImsUt;ILcom/android/ims/ImsReasonInfo;)V
    .locals 4
    .param p1, "ut"    # Lcom/android/ims/internal/IImsUt;
    .param p2, "id"    # I
    .param p3, "error"    # Lcom/android/ims/ImsReasonInfo;

    .prologue
    .line 599
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 601
    .local v0, "key":Ljava/lang/Integer;
    iget-object v1, p0, Lcom/android/ims/ImsUt$IImsUtListenerProxy;->this$0:Lcom/android/ims/ImsUt;

    # getter for: Lcom/android/ims/ImsUt;->mLockObj:Ljava/lang/Object;
    invoke-static {v1}, Lcom/android/ims/ImsUt;->access$100(Lcom/android/ims/ImsUt;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 602
    :try_start_0
    iget-object v3, p0, Lcom/android/ims/ImsUt$IImsUtListenerProxy;->this$0:Lcom/android/ims/ImsUt;

    iget-object v1, p0, Lcom/android/ims/ImsUt$IImsUtListenerProxy;->this$0:Lcom/android/ims/ImsUt;

    # getter for: Lcom/android/ims/ImsUt;->mPendingCmds:Ljava/util/HashMap;
    invoke-static {v1}, Lcom/android/ims/ImsUt;->access$200(Lcom/android/ims/ImsUt;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Message;

    # invokes: Lcom/android/ims/ImsUt;->sendFailureReport(Landroid/os/Message;Lcom/android/ims/ImsReasonInfo;)V
    invoke-static {v3, v1, p3}, Lcom/android/ims/ImsUt;->access$400(Lcom/android/ims/ImsUt;Landroid/os/Message;Lcom/android/ims/ImsReasonInfo;)V

    .line 603
    iget-object v1, p0, Lcom/android/ims/ImsUt$IImsUtListenerProxy;->this$0:Lcom/android/ims/ImsUt;

    # getter for: Lcom/android/ims/ImsUt;->mPendingCmds:Ljava/util/HashMap;
    invoke-static {v1}, Lcom/android/ims/ImsUt;->access$200(Lcom/android/ims/ImsUt;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 604
    monitor-exit v2

    .line 605
    return-void

    .line 604
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public utConfigurationUpdated(Lcom/android/ims/internal/IImsUt;I)V
    .locals 4
    .param p1, "ut"    # Lcom/android/ims/internal/IImsUt;
    .param p2, "id"    # I

    .prologue
    .line 589
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 591
    .local v0, "key":Ljava/lang/Integer;
    iget-object v1, p0, Lcom/android/ims/ImsUt$IImsUtListenerProxy;->this$0:Lcom/android/ims/ImsUt;

    # getter for: Lcom/android/ims/ImsUt;->mLockObj:Ljava/lang/Object;
    invoke-static {v1}, Lcom/android/ims/ImsUt;->access$100(Lcom/android/ims/ImsUt;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 592
    :try_start_0
    iget-object v3, p0, Lcom/android/ims/ImsUt$IImsUtListenerProxy;->this$0:Lcom/android/ims/ImsUt;

    iget-object v1, p0, Lcom/android/ims/ImsUt$IImsUtListenerProxy;->this$0:Lcom/android/ims/ImsUt;

    # getter for: Lcom/android/ims/ImsUt;->mPendingCmds:Ljava/util/HashMap;
    invoke-static {v1}, Lcom/android/ims/ImsUt;->access$200(Lcom/android/ims/ImsUt;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Message;

    # invokes: Lcom/android/ims/ImsUt;->sendSuccessReport(Landroid/os/Message;)V
    invoke-static {v3, v1}, Lcom/android/ims/ImsUt;->access$300(Lcom/android/ims/ImsUt;Landroid/os/Message;)V

    .line 593
    iget-object v1, p0, Lcom/android/ims/ImsUt$IImsUtListenerProxy;->this$0:Lcom/android/ims/ImsUt;

    # getter for: Lcom/android/ims/ImsUt;->mPendingCmds:Ljava/util/HashMap;
    invoke-static {v1}, Lcom/android/ims/ImsUt;->access$200(Lcom/android/ims/ImsUt;)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 594
    monitor-exit v2

    .line 595
    return-void

    .line 594
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
