.class public Lcom/android/ims/ImsConfig$ConfigConstants;
.super Ljava/lang/Object;
.source "ImsConfig.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/ims/ImsConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ConfigConstants"
.end annotation


# static fields
.field public static final CANCELLATION_TIMER:I = 0x4

.field public static final CONFIG_START:I = 0x0

.field public static final LVC_SETTING_ENABLED:I = 0xb

.field public static final MIN_SE:I = 0x3

.field public static final PROVISIONED_CONFIG_END:I = 0xb

.field public static final PROVISIONED_CONFIG_START:I = 0x0

.field public static final SILENT_REDIAL_ENABLE:I = 0x6

.field public static final SIP_SESSION_TIMER:I = 0x2

.field public static final SIP_T1_TIMER:I = 0x7

.field public static final SIP_T2_TIMER:I = 0x8

.field public static final SIP_TF_TIMER:I = 0x9

.field public static final TDELAY:I = 0x5

.field public static final VLT_SETTING_ENABLED:I = 0xa

.field public static final VOCODER_AMRMODESET:I = 0x0

.field public static final VOCODER_AMRWBMODESET:I = 0x1


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
