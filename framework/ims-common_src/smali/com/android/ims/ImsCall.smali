.class public Lcom/android/ims/ImsCall;
.super Ljava/lang/Object;
.source "ImsCall.java"

# interfaces
.implements Lcom/android/ims/internal/ICall;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/ims/ImsCall$1;,
        Lcom/android/ims/ImsCall$ImsCallSessionListenerProxy;,
        Lcom/android/ims/ImsCall$Listener;
    }
.end annotation


# static fields
.field public static final CALL_STATE_ACTIVE_TO_HOLD:I = 0x1

.field public static final CALL_STATE_HOLD_TO_ACTIVE:I = 0x2

.field private static final DBG:Z = true

.field private static final TAG:Ljava/lang/String; = "ImsCall"

.field private static final UPDATE_EXTEND_TO_CONFERENCE:I = 0x5

.field private static final UPDATE_HOLD:I = 0x1

.field private static final UPDATE_HOLD_MERGE:I = 0x2

.field private static final UPDATE_MERGE:I = 0x4

.field private static final UPDATE_NONE:I = 0x0

.field private static final UPDATE_RESUME:I = 0x3

.field private static final UPDATE_UNSPECIFIED:I = 0x6

.field public static final USSD_MODE_NOTIFY:I = 0x0

.field public static final USSD_MODE_REQUEST:I = 0x1


# instance fields
.field private mCallGroup:Lcom/android/ims/internal/CallGroup;

.field private mCallProfile:Lcom/android/ims/ImsCallProfile;

.field private mContext:Landroid/content/Context;

.field private mHold:Z

.field private mInCall:Z

.field private mLastReasonInfo:Lcom/android/ims/ImsReasonInfo;

.field private mListener:Lcom/android/ims/ImsCall$Listener;

.field private mLockObj:Ljava/lang/Object;

.field private mMediaSession:Lcom/android/ims/internal/ImsStreamMediaSession;

.field private mMute:Z

.field private mProposedCallProfile:Lcom/android/ims/ImsCallProfile;

.field private mSession:Lcom/android/ims/internal/ImsCallSession;

.field private mUpdateRequest:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/ims/ImsCallProfile;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "profile"    # Lcom/android/ims/ImsCallProfile;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 419
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 380
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/ims/ImsCall;->mLockObj:Ljava/lang/Object;

    .line 384
    iput-boolean v2, p0, Lcom/android/ims/ImsCall;->mInCall:Z

    .line 388
    iput-boolean v2, p0, Lcom/android/ims/ImsCall;->mHold:Z

    .line 390
    iput-boolean v2, p0, Lcom/android/ims/ImsCall;->mMute:Z

    .line 392
    iput v2, p0, Lcom/android/ims/ImsCall;->mUpdateRequest:I

    .line 394
    iput-object v1, p0, Lcom/android/ims/ImsCall;->mListener:Lcom/android/ims/ImsCall$Listener;

    .line 397
    iput-object v1, p0, Lcom/android/ims/ImsCall;->mCallGroup:Lcom/android/ims/internal/CallGroup;

    .line 400
    iput-object v1, p0, Lcom/android/ims/ImsCall;->mSession:Lcom/android/ims/internal/ImsCallSession;

    .line 403
    iput-object v1, p0, Lcom/android/ims/ImsCall;->mCallProfile:Lcom/android/ims/ImsCallProfile;

    .line 407
    iput-object v1, p0, Lcom/android/ims/ImsCall;->mProposedCallProfile:Lcom/android/ims/ImsCallProfile;

    .line 408
    iput-object v1, p0, Lcom/android/ims/ImsCall;->mLastReasonInfo:Lcom/android/ims/ImsReasonInfo;

    .line 411
    iput-object v1, p0, Lcom/android/ims/ImsCall;->mMediaSession:Lcom/android/ims/internal/ImsStreamMediaSession;

    .line 420
    iput-object p1, p0, Lcom/android/ims/ImsCall;->mContext:Landroid/content/Context;

    .line 421
    iput-object p2, p0, Lcom/android/ims/ImsCall;->mCallProfile:Lcom/android/ims/ImsCallProfile;

    .line 422
    return-void
.end method

.method static synthetic access$100(Lcom/android/ims/ImsCall;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/ims/ImsCall;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/android/ims/ImsCall;->log(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/android/ims/ImsCall;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/ims/ImsCall;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/android/ims/ImsCall;->mergeInternal()V

    return-void
.end method

.method static synthetic access$1100(Lcom/android/ims/ImsCall;I)V
    .locals 0
    .param p0, "x0"    # Lcom/android/ims/ImsCall;
    .param p1, "x1"    # I

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/android/ims/ImsCall;->notifyConferenceStateUpdatedThroughGroupOwner(I)V

    return-void
.end method

.method static synthetic access$1200(Lcom/android/ims/ImsCall;Lcom/android/ims/internal/ImsCallSession;Lcom/android/ims/ImsCallProfile;)Lcom/android/ims/ImsCall;
    .locals 1
    .param p0, "x0"    # Lcom/android/ims/ImsCall;
    .param p1, "x1"    # Lcom/android/ims/internal/ImsCallSession;
    .param p2, "x2"    # Lcom/android/ims/ImsCallProfile;

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Lcom/android/ims/ImsCall;->createNewCall(Lcom/android/ims/internal/ImsCallSession;Lcom/android/ims/ImsCallProfile;)Lcom/android/ims/ImsCall;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1300(Lcom/android/ims/ImsCall;Lcom/android/ims/ImsCall;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/ims/ImsCall;
    .param p1, "x1"    # Lcom/android/ims/ImsCall;

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/android/ims/ImsCall;->updateCallGroup(Lcom/android/ims/ImsCall;)V

    return-void
.end method

.method static synthetic access$1400(Lcom/android/ims/ImsCall;Lcom/android/ims/internal/CallGroup;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/ims/ImsCall;
    .param p1, "x1"    # Lcom/android/ims/internal/CallGroup;

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/android/ims/ImsCall;->setCallGroup(Lcom/android/ims/internal/CallGroup;)V

    return-void
.end method

.method static synthetic access$1502(Lcom/android/ims/ImsCall;Lcom/android/ims/ImsCallProfile;)Lcom/android/ims/ImsCallProfile;
    .locals 0
    .param p0, "x0"    # Lcom/android/ims/ImsCall;
    .param p1, "x1"    # Lcom/android/ims/ImsCallProfile;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/android/ims/ImsCall;->mProposedCallProfile:Lcom/android/ims/ImsCallProfile;

    return-object p1
.end method

.method static synthetic access$1600(Lcom/android/ims/ImsCall;Lcom/android/ims/ImsConferenceState;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/ims/ImsCall;
    .param p1, "x1"    # Lcom/android/ims/ImsConferenceState;

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/android/ims/ImsCall;->notifyConferenceStateUpdated(Lcom/android/ims/ImsConferenceState;)V

    return-void
.end method

.method static synthetic access$200(Lcom/android/ims/ImsCall;)Lcom/android/ims/ImsCall$Listener;
    .locals 1
    .param p0, "x0"    # Lcom/android/ims/ImsCall;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/android/ims/ImsCall;->mListener:Lcom/android/ims/ImsCall$Listener;

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/ims/ImsCall;)Lcom/android/ims/ImsCallProfile;
    .locals 1
    .param p0, "x0"    # Lcom/android/ims/ImsCall;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/android/ims/ImsCall;->mCallProfile:Lcom/android/ims/ImsCallProfile;

    return-object v0
.end method

.method static synthetic access$302(Lcom/android/ims/ImsCall;Lcom/android/ims/ImsCallProfile;)Lcom/android/ims/ImsCallProfile;
    .locals 0
    .param p0, "x0"    # Lcom/android/ims/ImsCall;
    .param p1, "x1"    # Lcom/android/ims/ImsCallProfile;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/android/ims/ImsCall;->mCallProfile:Lcom/android/ims/ImsCallProfile;

    return-object p1
.end method

.method static synthetic access$400(Lcom/android/ims/ImsCall;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/ims/ImsCall;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/Throwable;

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Lcom/android/ims/ImsCall;->loge(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method static synthetic access$502(Lcom/android/ims/ImsCall;Lcom/android/ims/ImsReasonInfo;)Lcom/android/ims/ImsReasonInfo;
    .locals 0
    .param p0, "x0"    # Lcom/android/ims/ImsCall;
    .param p1, "x1"    # Lcom/android/ims/ImsReasonInfo;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/android/ims/ImsCall;->mLastReasonInfo:Lcom/android/ims/ImsReasonInfo;

    return-object p1
.end method

.method static synthetic access$600(Lcom/android/ims/ImsCall;)Lcom/android/ims/internal/CallGroup;
    .locals 1
    .param p0, "x0"    # Lcom/android/ims/ImsCall;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/android/ims/ImsCall;->mCallGroup:Lcom/android/ims/internal/CallGroup;

    return-object v0
.end method

.method static synthetic access$700(Lcom/android/ims/ImsCall;Lcom/android/ims/ImsReasonInfo;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/ims/ImsCall;
    .param p1, "x1"    # Lcom/android/ims/ImsReasonInfo;

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/android/ims/ImsCall;->notifyConferenceSessionTerminated(Lcom/android/ims/ImsReasonInfo;)V

    return-void
.end method

.method static synthetic access$800(Lcom/android/ims/ImsCall;Lcom/android/ims/ImsReasonInfo;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/ims/ImsCall;
    .param p1, "x1"    # Lcom/android/ims/ImsReasonInfo;

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/android/ims/ImsCall;->clear(Lcom/android/ims/ImsReasonInfo;)V

    return-void
.end method

.method static synthetic access$900(Lcom/android/ims/ImsCall;)I
    .locals 1
    .param p0, "x0"    # Lcom/android/ims/ImsCall;

    .prologue
    .line 44
    iget v0, p0, Lcom/android/ims/ImsCall;->mUpdateRequest:I

    return v0
.end method

.method static synthetic access$902(Lcom/android/ims/ImsCall;I)I
    .locals 0
    .param p0, "x0"    # Lcom/android/ims/ImsCall;
    .param p1, "x1"    # I

    .prologue
    .line 44
    iput p1, p0, Lcom/android/ims/ImsCall;->mUpdateRequest:I

    return p1
.end method

.method private clear(Lcom/android/ims/ImsReasonInfo;)V
    .locals 1
    .param p1, "lastReasonInfo"    # Lcom/android/ims/ImsReasonInfo;

    .prologue
    const/4 v0, 0x0

    .line 1270
    iput-boolean v0, p0, Lcom/android/ims/ImsCall;->mInCall:Z

    .line 1271
    iput-boolean v0, p0, Lcom/android/ims/ImsCall;->mHold:Z

    .line 1272
    iput v0, p0, Lcom/android/ims/ImsCall;->mUpdateRequest:I

    .line 1273
    iput-object p1, p0, Lcom/android/ims/ImsCall;->mLastReasonInfo:Lcom/android/ims/ImsReasonInfo;

    .line 1274
    invoke-direct {p0}, Lcom/android/ims/ImsCall;->destroyCallGroup()V

    .line 1275
    return-void
.end method

.method private createCallGroup(Lcom/android/ims/ImsCall;)V
    .locals 3
    .param p1, "neutralReferrer"    # Lcom/android/ims/ImsCall;

    .prologue
    .line 1278
    invoke-direct {p1}, Lcom/android/ims/ImsCall;->getCallGroup()Lcom/android/ims/internal/CallGroup;

    move-result-object v0

    .line 1280
    .local v0, "referrerCallGroup":Lcom/android/ims/internal/CallGroup;
    iget-object v1, p0, Lcom/android/ims/ImsCall;->mCallGroup:Lcom/android/ims/internal/CallGroup;

    if-nez v1, :cond_2

    .line 1281
    if-nez v0, :cond_1

    .line 1282
    invoke-static {}, Lcom/android/ims/internal/CallGroupManager;->getInstance()Lcom/android/ims/internal/CallGroupManager;

    move-result-object v1

    new-instance v2, Lcom/android/ims/ImsCallGroup;

    invoke-direct {v2}, Lcom/android/ims/ImsCallGroup;-><init>()V

    invoke-virtual {v1, v2}, Lcom/android/ims/internal/CallGroupManager;->createCallGroup(Lcom/android/ims/internal/ICallGroup;)Lcom/android/ims/internal/CallGroup;

    move-result-object v1

    iput-object v1, p0, Lcom/android/ims/ImsCall;->mCallGroup:Lcom/android/ims/internal/CallGroup;

    .line 1287
    :goto_0
    iget-object v1, p0, Lcom/android/ims/ImsCall;->mCallGroup:Lcom/android/ims/internal/CallGroup;

    if-eqz v1, :cond_0

    .line 1288
    iget-object v1, p0, Lcom/android/ims/ImsCall;->mCallGroup:Lcom/android/ims/internal/CallGroup;

    invoke-virtual {v1, p1}, Lcom/android/ims/internal/CallGroup;->setNeutralReferrer(Lcom/android/ims/internal/ICall;)V

    .line 1298
    :cond_0
    :goto_1
    return-void

    .line 1284
    :cond_1
    iput-object v0, p0, Lcom/android/ims/ImsCall;->mCallGroup:Lcom/android/ims/internal/CallGroup;

    goto :goto_0

    .line 1291
    :cond_2
    iget-object v1, p0, Lcom/android/ims/ImsCall;->mCallGroup:Lcom/android/ims/internal/CallGroup;

    invoke-virtual {v1, p1}, Lcom/android/ims/internal/CallGroup;->setNeutralReferrer(Lcom/android/ims/internal/ICall;)V

    .line 1293
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/ims/ImsCall;->mCallGroup:Lcom/android/ims/internal/CallGroup;

    if-eq v1, v0, :cond_0

    .line 1295
    const-string v1, "fatal :: call group is mismatched; call is corrupted..."

    invoke-direct {p0, v1}, Lcom/android/ims/ImsCall;->loge(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private createCallSessionListener()Lcom/android/ims/internal/ImsCallSession$Listener;
    .locals 2

    .prologue
    .line 1366
    new-instance v0, Lcom/android/ims/ImsCall$ImsCallSessionListenerProxy;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/ims/ImsCall$ImsCallSessionListenerProxy;-><init>(Lcom/android/ims/ImsCall;Lcom/android/ims/ImsCall$1;)V

    return-object v0
.end method

.method private createHoldMediaProfile()Lcom/android/ims/ImsStreamMediaProfile;
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 1387
    new-instance v0, Lcom/android/ims/ImsStreamMediaProfile;

    invoke-direct {v0}, Lcom/android/ims/ImsStreamMediaProfile;-><init>()V

    .line 1389
    .local v0, "mediaProfile":Lcom/android/ims/ImsStreamMediaProfile;
    iget-object v1, p0, Lcom/android/ims/ImsCall;->mCallProfile:Lcom/android/ims/ImsCallProfile;

    if-nez v1, :cond_1

    .line 1401
    :cond_0
    :goto_0
    return-object v0

    .line 1393
    :cond_1
    iget-object v1, p0, Lcom/android/ims/ImsCall;->mCallProfile:Lcom/android/ims/ImsCallProfile;

    iget-object v1, v1, Lcom/android/ims/ImsCallProfile;->mMediaProfile:Lcom/android/ims/ImsStreamMediaProfile;

    iget v1, v1, Lcom/android/ims/ImsStreamMediaProfile;->mAudioQuality:I

    iput v1, v0, Lcom/android/ims/ImsStreamMediaProfile;->mAudioQuality:I

    .line 1394
    iget-object v1, p0, Lcom/android/ims/ImsCall;->mCallProfile:Lcom/android/ims/ImsCallProfile;

    iget-object v1, v1, Lcom/android/ims/ImsCallProfile;->mMediaProfile:Lcom/android/ims/ImsStreamMediaProfile;

    iget v1, v1, Lcom/android/ims/ImsStreamMediaProfile;->mVideoQuality:I

    iput v1, v0, Lcom/android/ims/ImsStreamMediaProfile;->mVideoQuality:I

    .line 1395
    iput v2, v0, Lcom/android/ims/ImsStreamMediaProfile;->mAudioDirection:I

    .line 1397
    iget v1, v0, Lcom/android/ims/ImsStreamMediaProfile;->mVideoQuality:I

    if-eqz v1, :cond_0

    .line 1398
    iput v2, v0, Lcom/android/ims/ImsStreamMediaProfile;->mVideoDirection:I

    goto :goto_0
.end method

.method private createNewCall(Lcom/android/ims/internal/ImsCallSession;Lcom/android/ims/ImsCallProfile;)Lcom/android/ims/ImsCall;
    .locals 3
    .param p1, "session"    # Lcom/android/ims/internal/ImsCallSession;
    .param p2, "profile"    # Lcom/android/ims/ImsCallProfile;

    .prologue
    .line 1370
    new-instance v0, Lcom/android/ims/ImsCall;

    iget-object v2, p0, Lcom/android/ims/ImsCall;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2, p2}, Lcom/android/ims/ImsCall;-><init>(Landroid/content/Context;Lcom/android/ims/ImsCallProfile;)V

    .line 1373
    .local v0, "call":Lcom/android/ims/ImsCall;
    :try_start_0
    invoke-virtual {v0, p1}, Lcom/android/ims/ImsCall;->attachSession(Lcom/android/ims/internal/ImsCallSession;)V
    :try_end_0
    .catch Lcom/android/ims/ImsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1383
    :cond_0
    :goto_0
    return-object v0

    .line 1374
    :catch_0
    move-exception v1

    .line 1375
    .local v1, "e":Lcom/android/ims/ImsException;
    if-eqz v0, :cond_0

    .line 1376
    invoke-virtual {v0}, Lcom/android/ims/ImsCall;->close()V

    .line 1377
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private createResumeMediaProfile()Lcom/android/ims/ImsStreamMediaProfile;
    .locals 3

    .prologue
    const/4 v2, 0x3

    .line 1405
    new-instance v0, Lcom/android/ims/ImsStreamMediaProfile;

    invoke-direct {v0}, Lcom/android/ims/ImsStreamMediaProfile;-><init>()V

    .line 1407
    .local v0, "mediaProfile":Lcom/android/ims/ImsStreamMediaProfile;
    iget-object v1, p0, Lcom/android/ims/ImsCall;->mCallProfile:Lcom/android/ims/ImsCallProfile;

    if-nez v1, :cond_1

    .line 1419
    :cond_0
    :goto_0
    return-object v0

    .line 1411
    :cond_1
    iget-object v1, p0, Lcom/android/ims/ImsCall;->mCallProfile:Lcom/android/ims/ImsCallProfile;

    iget-object v1, v1, Lcom/android/ims/ImsCallProfile;->mMediaProfile:Lcom/android/ims/ImsStreamMediaProfile;

    iget v1, v1, Lcom/android/ims/ImsStreamMediaProfile;->mAudioQuality:I

    iput v1, v0, Lcom/android/ims/ImsStreamMediaProfile;->mAudioQuality:I

    .line 1412
    iget-object v1, p0, Lcom/android/ims/ImsCall;->mCallProfile:Lcom/android/ims/ImsCallProfile;

    iget-object v1, v1, Lcom/android/ims/ImsCallProfile;->mMediaProfile:Lcom/android/ims/ImsStreamMediaProfile;

    iget v1, v1, Lcom/android/ims/ImsStreamMediaProfile;->mVideoQuality:I

    iput v1, v0, Lcom/android/ims/ImsStreamMediaProfile;->mVideoQuality:I

    .line 1413
    iput v2, v0, Lcom/android/ims/ImsStreamMediaProfile;->mAudioDirection:I

    .line 1415
    iget v1, v0, Lcom/android/ims/ImsStreamMediaProfile;->mVideoQuality:I

    if-eqz v1, :cond_0

    .line 1416
    iput v2, v0, Lcom/android/ims/ImsStreamMediaProfile;->mVideoDirection:I

    goto :goto_0
.end method

.method private destroyCallGroup()V
    .locals 2

    .prologue
    .line 1337
    iget-object v0, p0, Lcom/android/ims/ImsCall;->mCallGroup:Lcom/android/ims/internal/CallGroup;

    if-nez v0, :cond_0

    .line 1348
    :goto_0
    return-void

    .line 1341
    :cond_0
    iget-object v0, p0, Lcom/android/ims/ImsCall;->mCallGroup:Lcom/android/ims/internal/CallGroup;

    invoke-virtual {v0, p0}, Lcom/android/ims/internal/CallGroup;->removeReferrer(Lcom/android/ims/internal/ICall;)V

    .line 1343
    iget-object v0, p0, Lcom/android/ims/ImsCall;->mCallGroup:Lcom/android/ims/internal/CallGroup;

    invoke-virtual {v0}, Lcom/android/ims/internal/CallGroup;->hasReferrer()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1344
    invoke-static {}, Lcom/android/ims/internal/CallGroupManager;->getInstance()Lcom/android/ims/internal/CallGroupManager;

    move-result-object v0

    iget-object v1, p0, Lcom/android/ims/ImsCall;->mCallGroup:Lcom/android/ims/internal/CallGroup;

    invoke-virtual {v0, v1}, Lcom/android/ims/internal/CallGroupManager;->destroyCallGroup(Lcom/android/ims/internal/CallGroup;)V

    .line 1347
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/ims/ImsCall;->mCallGroup:Lcom/android/ims/internal/CallGroup;

    goto :goto_0
.end method

.method private enforceConversationMode()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1423
    iget-boolean v0, p0, Lcom/android/ims/ImsCall;->mInCall:Z

    if-eqz v0, :cond_0

    .line 1424
    iput-boolean v1, p0, Lcom/android/ims/ImsCall;->mHold:Z

    .line 1425
    iput v1, p0, Lcom/android/ims/ImsCall;->mUpdateRequest:I

    .line 1427
    :cond_0
    return-void
.end method

.method private getCallGroup()Lcom/android/ims/internal/CallGroup;
    .locals 2

    .prologue
    .line 1351
    iget-object v1, p0, Lcom/android/ims/ImsCall;->mLockObj:Ljava/lang/Object;

    monitor-enter v1

    .line 1352
    :try_start_0
    iget-object v0, p0, Lcom/android/ims/ImsCall;->mCallGroup:Lcom/android/ims/internal/CallGroup;

    monitor-exit v1

    return-object v0

    .line 1353
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private log(Ljava/lang/String;)V
    .locals 1
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 1612
    const-string v0, "ImsCall"

    invoke-static {v0, p1}, Landroid/telephony/Rlog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1613
    return-void
.end method

.method private loge(Ljava/lang/String;)V
    .locals 1
    .param p1, "s"    # Ljava/lang/String;

    .prologue
    .line 1616
    const-string v0, "ImsCall"

    invoke-static {v0, p1}, Landroid/telephony/Rlog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1617
    return-void
.end method

.method private loge(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "s"    # Ljava/lang/String;
    .param p2, "t"    # Ljava/lang/Throwable;

    .prologue
    .line 1620
    const-string v0, "ImsCall"

    invoke-static {v0, p1, p2}, Landroid/telephony/Rlog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1621
    return-void
.end method

.method private mergeInternal()V
    .locals 2

    .prologue
    .line 1431
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mergeInternal :: session="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/ims/ImsCall;->mSession:Lcom/android/ims/internal/ImsCallSession;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/ims/ImsCall;->log(Ljava/lang/String;)V

    .line 1434
    iget-object v0, p0, Lcom/android/ims/ImsCall;->mSession:Lcom/android/ims/internal/ImsCallSession;

    invoke-virtual {v0}, Lcom/android/ims/internal/ImsCallSession;->merge()V

    .line 1435
    const/4 v0, 0x4

    iput v0, p0, Lcom/android/ims/ImsCall;->mUpdateRequest:I

    .line 1436
    return-void
.end method

.method private notifyCallStateChanged()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1439
    const/4 v0, 0x0

    .line 1441
    .local v0, "state":I
    iget-boolean v2, p0, Lcom/android/ims/ImsCall;->mInCall:Z

    if-eqz v2, :cond_2

    iget v2, p0, Lcom/android/ims/ImsCall;->mUpdateRequest:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    .line 1442
    const/4 v0, 0x1

    .line 1443
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/android/ims/ImsCall;->mHold:Z

    .line 1451
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    .line 1452
    iget-object v2, p0, Lcom/android/ims/ImsCall;->mListener:Lcom/android/ims/ImsCall$Listener;

    if-eqz v2, :cond_1

    .line 1454
    :try_start_0
    iget-object v2, p0, Lcom/android/ims/ImsCall;->mListener:Lcom/android/ims/ImsCall$Listener;

    invoke-virtual {v2, p0, v0}, Lcom/android/ims/ImsCall$Listener;->onCallStateChanged(Lcom/android/ims/ImsCall;I)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 1460
    :cond_1
    :goto_1
    return-void

    .line 1444
    :cond_2
    iget-boolean v2, p0, Lcom/android/ims/ImsCall;->mInCall:Z

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/android/ims/ImsCall;->mUpdateRequest:I

    const/4 v3, 0x4

    if-eq v2, v3, :cond_3

    iget v2, p0, Lcom/android/ims/ImsCall;->mUpdateRequest:I

    const/4 v3, 0x5

    if-ne v2, v3, :cond_0

    .line 1446
    :cond_3
    const/4 v0, 0x2

    .line 1447
    iput-boolean v4, p0, Lcom/android/ims/ImsCall;->mHold:Z

    .line 1448
    iput-boolean v4, p0, Lcom/android/ims/ImsCall;->mMute:Z

    goto :goto_0

    .line 1455
    :catch_0
    move-exception v1

    .line 1456
    .local v1, "t":Ljava/lang/Throwable;
    const-string v2, "notifyCallStateChanged :: "

    invoke-direct {p0, v2, v1}, Lcom/android/ims/ImsCall;->loge(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method private notifyConferenceSessionTerminated(Lcom/android/ims/ImsReasonInfo;)V
    .locals 5
    .param p1, "reasonInfo"    # Lcom/android/ims/ImsReasonInfo;

    .prologue
    .line 1465
    iget-object v3, p0, Lcom/android/ims/ImsCall;->mCallGroup:Lcom/android/ims/internal/CallGroup;

    invoke-virtual {v3, p0}, Lcom/android/ims/internal/CallGroup;->isOwner(Lcom/android/ims/internal/ICall;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1466
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Group Owner! Size of referrers list = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/ims/ImsCall;->mCallGroup:Lcom/android/ims/internal/CallGroup;

    invoke-virtual {v4}, Lcom/android/ims/internal/CallGroup;->getReferrers()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/android/ims/ImsCall;->log(Ljava/lang/String;)V

    .line 1467
    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/android/ims/ImsCall;->mCallGroup:Lcom/android/ims/internal/CallGroup;

    invoke-virtual {v3}, Lcom/android/ims/internal/CallGroup;->hasReferrer()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1468
    iget-object v3, p0, Lcom/android/ims/ImsCall;->mCallGroup:Lcom/android/ims/internal/CallGroup;

    invoke-virtual {v3}, Lcom/android/ims/internal/CallGroup;->getReferrers()Ljava/util/ArrayList;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/ims/ImsCall;

    .line 1469
    .local v0, "call":Lcom/android/ims/ImsCall;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onCallTerminated to be called for the call:: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/android/ims/ImsCall;->log(Ljava/lang/String;)V

    .line 1471
    if-eqz v0, :cond_0

    .line 1475
    iget-object v1, v0, Lcom/android/ims/ImsCall;->mListener:Lcom/android/ims/ImsCall$Listener;

    .line 1476
    .local v1, "listener":Lcom/android/ims/ImsCall$Listener;
    invoke-direct {v0, p1}, Lcom/android/ims/ImsCall;->clear(Lcom/android/ims/ImsReasonInfo;)V

    .line 1478
    if-eqz v1, :cond_0

    .line 1480
    :try_start_0
    invoke-virtual {v1, v0, p1}, Lcom/android/ims/ImsCall$Listener;->onCallTerminated(Lcom/android/ims/ImsCall;Lcom/android/ims/ImsReasonInfo;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1481
    :catch_0
    move-exception v2

    .line 1482
    .local v2, "t":Ljava/lang/Throwable;
    const-string v3, "notifyConferenceSessionTerminated :: "

    invoke-direct {p0, v3, v2}, Lcom/android/ims/ImsCall;->loge(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1486
    .end local v0    # "call":Lcom/android/ims/ImsCall;
    .end local v1    # "listener":Lcom/android/ims/ImsCall$Listener;
    .end local v2    # "t":Ljava/lang/Throwable;
    :cond_1
    iget-object v3, p0, Lcom/android/ims/ImsCall;->mCallGroup:Lcom/android/ims/internal/CallGroup;

    invoke-virtual {v3, p0}, Lcom/android/ims/internal/CallGroup;->isReferrer(Lcom/android/ims/internal/ICall;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 1500
    :cond_2
    :goto_1
    return-void

    .line 1490
    :cond_3
    iget-object v1, p0, Lcom/android/ims/ImsCall;->mListener:Lcom/android/ims/ImsCall$Listener;

    .line 1491
    .restart local v1    # "listener":Lcom/android/ims/ImsCall$Listener;
    invoke-direct {p0, p1}, Lcom/android/ims/ImsCall;->clear(Lcom/android/ims/ImsReasonInfo;)V

    .line 1493
    if-eqz v1, :cond_2

    .line 1495
    :try_start_1
    invoke-virtual {v1, p0, p1}, Lcom/android/ims/ImsCall$Listener;->onCallTerminated(Lcom/android/ims/ImsCall;Lcom/android/ims/ImsReasonInfo;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 1496
    :catch_1
    move-exception v2

    .line 1497
    .restart local v2    # "t":Ljava/lang/Throwable;
    const-string v3, "notifyConferenceSessionTerminated :: "

    invoke-direct {p0, v3, v2}, Lcom/android/ims/ImsCall;->loge(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method private notifyConferenceStateUpdated(Lcom/android/ims/ImsConferenceState;)V
    .locals 12
    .param p1, "state"    # Lcom/android/ims/ImsConferenceState;

    .prologue
    .line 1539
    iget-object v10, p1, Lcom/android/ims/ImsConferenceState;->mParticipants:Ljava/util/HashMap;

    invoke-virtual {v10}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v5

    .line 1541
    .local v5, "paticipants":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Landroid/os/Bundle;>;>;"
    if-nez v5, :cond_1

    .line 1598
    :cond_0
    return-void

    .line 1545
    :cond_1
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 1547
    .local v3, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Landroid/os/Bundle;>;>;"
    :cond_2
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    .line 1548
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 1550
    .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Landroid/os/Bundle;>;"
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 1551
    .local v4, "key":Ljava/lang/String;
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 1552
    .local v0, "confInfo":Landroid/os/Bundle;
    const-string v10, "status"

    invoke-virtual {v0, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1553
    .local v7, "status":Ljava/lang/String;
    const-string v10, "user"

    invoke-virtual {v0, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 1554
    .local v9, "user":Ljava/lang/String;
    const-string v10, "endpoint"

    invoke-virtual {v0, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1557
    .local v1, "endpoint":Ljava/lang/String;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "notifyConferenceStateUpdated :: key="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", status="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", user="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", endpoint="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {p0, v10}, Lcom/android/ims/ImsCall;->log(Ljava/lang/String;)V

    .line 1563
    iget-object v10, p0, Lcom/android/ims/ImsCall;->mCallGroup:Lcom/android/ims/internal/CallGroup;

    if-eqz v10, :cond_3

    iget-object v10, p0, Lcom/android/ims/ImsCall;->mCallGroup:Lcom/android/ims/internal/CallGroup;

    invoke-virtual {v10, p0}, Lcom/android/ims/internal/CallGroup;->isOwner(Lcom/android/ims/internal/ICall;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 1567
    :cond_3
    iget-object v10, p0, Lcom/android/ims/ImsCall;->mCallGroup:Lcom/android/ims/internal/CallGroup;

    invoke-virtual {v10, v1}, Lcom/android/ims/internal/CallGroup;->getReferrer(Ljava/lang/String;)Lcom/android/ims/internal/ICall;

    move-result-object v6

    check-cast v6, Lcom/android/ims/ImsCall;

    .line 1569
    .local v6, "referrer":Lcom/android/ims/ImsCall;
    if-eqz v6, :cond_2

    .line 1573
    iget-object v10, v6, Lcom/android/ims/ImsCall;->mListener:Lcom/android/ims/ImsCall$Listener;

    if-eqz v10, :cond_2

    .line 1578
    :try_start_0
    const-string v10, "alerting"

    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 1579
    iget-object v10, v6, Lcom/android/ims/ImsCall;->mListener:Lcom/android/ims/ImsCall$Listener;

    invoke-virtual {v10, v6}, Lcom/android/ims/ImsCall$Listener;->onCallProgressing(Lcom/android/ims/ImsCall;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 1594
    :catch_0
    move-exception v8

    .line 1595
    .local v8, "t":Ljava/lang/Throwable;
    const-string v10, "notifyConferenceStateUpdated :: "

    invoke-direct {p0, v10, v8}, Lcom/android/ims/ImsCall;->loge(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 1581
    .end local v8    # "t":Ljava/lang/Throwable;
    :cond_4
    :try_start_1
    const-string v10, "connect-fail"

    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 1582
    iget-object v10, v6, Lcom/android/ims/ImsCall;->mListener:Lcom/android/ims/ImsCall$Listener;

    new-instance v11, Lcom/android/ims/ImsReasonInfo;

    invoke-direct {v11}, Lcom/android/ims/ImsReasonInfo;-><init>()V

    invoke-virtual {v10, v6, v11}, Lcom/android/ims/ImsCall$Listener;->onCallStartFailed(Lcom/android/ims/ImsCall;Lcom/android/ims/ImsReasonInfo;)V

    goto/16 :goto_0

    .line 1584
    :cond_5
    const-string v10, "on-hold"

    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 1585
    iget-object v10, v6, Lcom/android/ims/ImsCall;->mListener:Lcom/android/ims/ImsCall$Listener;

    invoke-virtual {v10, v6}, Lcom/android/ims/ImsCall$Listener;->onCallHoldReceived(Lcom/android/ims/ImsCall;)V

    goto/16 :goto_0

    .line 1587
    :cond_6
    const-string v10, "connected"

    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 1588
    iget-object v10, v6, Lcom/android/ims/ImsCall;->mListener:Lcom/android/ims/ImsCall$Listener;

    invoke-virtual {v10, v6}, Lcom/android/ims/ImsCall$Listener;->onCallStarted(Lcom/android/ims/ImsCall;)V

    goto/16 :goto_0

    .line 1590
    :cond_7
    const-string v10, "disconnected"

    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 1591
    new-instance v10, Lcom/android/ims/ImsReasonInfo;

    invoke-direct {v10}, Lcom/android/ims/ImsReasonInfo;-><init>()V

    invoke-direct {v6, v10}, Lcom/android/ims/ImsCall;->clear(Lcom/android/ims/ImsReasonInfo;)V

    .line 1592
    iget-object v10, v6, Lcom/android/ims/ImsCall;->mListener:Lcom/android/ims/ImsCall$Listener;

    iget-object v11, v6, Lcom/android/ims/ImsCall;->mLastReasonInfo:Lcom/android/ims/ImsReasonInfo;

    invoke-virtual {v10, v6, v11}, Lcom/android/ims/ImsCall$Listener;->onCallTerminated(Lcom/android/ims/ImsCall;Lcom/android/ims/ImsReasonInfo;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method private notifyConferenceStateUpdatedThroughGroupOwner(I)V
    .locals 7
    .param p1, "update"    # I

    .prologue
    .line 1505
    iget-object v5, p0, Lcom/android/ims/ImsCall;->mCallGroup:Lcom/android/ims/internal/CallGroup;

    invoke-virtual {v5, p0}, Lcom/android/ims/internal/CallGroup;->isOwner(Lcom/android/ims/internal/ICall;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1506
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Group Owner! Size of referrers list = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/ims/ImsCall;->mCallGroup:Lcom/android/ims/internal/CallGroup;

    invoke-virtual {v6}, Lcom/android/ims/internal/CallGroup;->getReferrers()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/android/ims/ImsCall;->log(Ljava/lang/String;)V

    .line 1507
    iget-object v5, p0, Lcom/android/ims/ImsCall;->mCallGroup:Lcom/android/ims/internal/CallGroup;

    invoke-virtual {v5}, Lcom/android/ims/internal/CallGroup;->getReferrers()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/ims/internal/ICall;

    .local v2, "icall":Lcom/android/ims/internal/ICall;
    move-object v0, v2

    .line 1508
    check-cast v0, Lcom/android/ims/ImsCall;

    .line 1509
    .local v0, "call":Lcom/android/ims/ImsCall;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "notifyConferenceStateUpdatedThroughGroupOwner to be called for the call:: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/android/ims/ImsCall;->log(Ljava/lang/String;)V

    .line 1511
    if-eqz v0, :cond_0

    .line 1515
    iget-object v3, v0, Lcom/android/ims/ImsCall;->mListener:Lcom/android/ims/ImsCall$Listener;

    .line 1517
    .local v3, "listener":Lcom/android/ims/ImsCall$Listener;
    if-eqz v3, :cond_0

    .line 1519
    packed-switch p1, :pswitch_data_0

    .line 1527
    :pswitch_0
    :try_start_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "notifyConferenceStateUpdatedThroughGroupOwner :: not handled update "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/android/ims/ImsCall;->loge(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1530
    :catch_0
    move-exception v4

    .line 1531
    .local v4, "t":Ljava/lang/Throwable;
    const-string v5, "notifyConferenceStateUpdatedThroughGroupOwner :: "

    invoke-direct {p0, v5, v4}, Lcom/android/ims/ImsCall;->loge(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1521
    .end local v4    # "t":Ljava/lang/Throwable;
    :pswitch_1
    :try_start_1
    invoke-virtual {v3, v0}, Lcom/android/ims/ImsCall$Listener;->onCallHeld(Lcom/android/ims/ImsCall;)V

    goto :goto_0

    .line 1524
    :pswitch_2
    invoke-virtual {v3, v0}, Lcom/android/ims/ImsCall$Listener;->onCallResumed(Lcom/android/ims/ImsCall;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 1536
    .end local v0    # "call":Lcom/android/ims/ImsCall;
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "icall":Lcom/android/ims/internal/ICall;
    .end local v3    # "listener":Lcom/android/ims/ImsCall$Listener;
    :cond_1
    return-void

    .line 1519
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private notifyError(IILjava/lang/String;)V
    .locals 0
    .param p1, "reason"    # I
    .param p2, "statusCode"    # I
    .param p3, "message"    # Ljava/lang/String;

    .prologue
    .line 1601
    return-void
.end method

.method private setCallGroup(Lcom/android/ims/internal/CallGroup;)V
    .locals 2
    .param p1, "callGroup"    # Lcom/android/ims/internal/CallGroup;

    .prologue
    .line 1357
    iget-object v1, p0, Lcom/android/ims/ImsCall;->mLockObj:Ljava/lang/Object;

    monitor-enter v1

    .line 1358
    :try_start_0
    iput-object p1, p0, Lcom/android/ims/ImsCall;->mCallGroup:Lcom/android/ims/internal/CallGroup;

    .line 1359
    monitor-exit v1

    .line 1360
    return-void

    .line 1359
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private throwImsException(Ljava/lang/Throwable;I)V
    .locals 2
    .param p1, "t"    # Ljava/lang/Throwable;
    .param p2, "code"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/ims/ImsException;
        }
    .end annotation

    .prologue
    .line 1604
    instance-of v0, p1, Lcom/android/ims/ImsException;

    if-eqz v0, :cond_0

    .line 1605
    check-cast p1, Lcom/android/ims/ImsException;

    .end local p1    # "t":Ljava/lang/Throwable;
    throw p1

    .line 1607
    .restart local p1    # "t":Ljava/lang/Throwable;
    :cond_0
    new-instance v0, Lcom/android/ims/ImsException;

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1, p2}, Lcom/android/ims/ImsException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;I)V

    throw v0
.end method

.method private updateCallGroup(Lcom/android/ims/ImsCall;)V
    .locals 4
    .param p1, "owner"    # Lcom/android/ims/ImsCall;

    .prologue
    .line 1301
    iget-object v2, p0, Lcom/android/ims/ImsCall;->mCallGroup:Lcom/android/ims/internal/CallGroup;

    if-nez v2, :cond_1

    .line 1334
    :cond_0
    :goto_0
    return-void

    .line 1305
    :cond_1
    iget-object v2, p0, Lcom/android/ims/ImsCall;->mCallGroup:Lcom/android/ims/internal/CallGroup;

    invoke-virtual {v2}, Lcom/android/ims/internal/CallGroup;->getNeutralReferrer()Lcom/android/ims/internal/ICall;

    move-result-object v1

    check-cast v1, Lcom/android/ims/ImsCall;

    .line 1307
    .local v1, "neutralReferrer":Lcom/android/ims/ImsCall;
    if-nez p1, :cond_2

    .line 1309
    iget-object v2, p0, Lcom/android/ims/ImsCall;->mCallGroup:Lcom/android/ims/internal/CallGroup;

    invoke-virtual {v2}, Lcom/android/ims/internal/CallGroup;->hasReferrer()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1310
    invoke-static {}, Lcom/android/ims/internal/CallGroupManager;->getInstance()Lcom/android/ims/internal/CallGroupManager;

    move-result-object v2

    iget-object v3, p0, Lcom/android/ims/ImsCall;->mCallGroup:Lcom/android/ims/internal/CallGroup;

    invoke-virtual {v2, v3}, Lcom/android/ims/internal/CallGroupManager;->destroyCallGroup(Lcom/android/ims/internal/CallGroup;)V

    .line 1311
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/android/ims/ImsCall;->mCallGroup:Lcom/android/ims/internal/CallGroup;

    goto :goto_0

    .line 1314
    :cond_2
    iget-object v2, p0, Lcom/android/ims/ImsCall;->mCallGroup:Lcom/android/ims/internal/CallGroup;

    invoke-virtual {v2, p0}, Lcom/android/ims/internal/CallGroup;->addReferrer(Lcom/android/ims/internal/ICall;)V

    .line 1316
    if-eqz v1, :cond_4

    .line 1317
    invoke-direct {v1}, Lcom/android/ims/ImsCall;->getCallGroup()Lcom/android/ims/internal/CallGroup;

    move-result-object v2

    if-nez v2, :cond_3

    .line 1318
    iget-object v2, p0, Lcom/android/ims/ImsCall;->mCallGroup:Lcom/android/ims/internal/CallGroup;

    invoke-direct {v1, v2}, Lcom/android/ims/ImsCall;->setCallGroup(Lcom/android/ims/internal/CallGroup;)V

    .line 1319
    iget-object v2, p0, Lcom/android/ims/ImsCall;->mCallGroup:Lcom/android/ims/internal/CallGroup;

    invoke-virtual {v2, v1}, Lcom/android/ims/internal/CallGroup;->addReferrer(Lcom/android/ims/internal/ICall;)V

    .line 1322
    :cond_3
    invoke-direct {v1}, Lcom/android/ims/ImsCall;->enforceConversationMode()V

    .line 1326
    :cond_4
    iget-object v2, p0, Lcom/android/ims/ImsCall;->mCallGroup:Lcom/android/ims/internal/CallGroup;

    invoke-virtual {v2}, Lcom/android/ims/internal/CallGroup;->getOwner()Lcom/android/ims/internal/ICall;

    move-result-object v0

    check-cast v0, Lcom/android/ims/ImsCall;

    .line 1328
    .local v0, "exOwner":Lcom/android/ims/ImsCall;
    iget-object v2, p0, Lcom/android/ims/ImsCall;->mCallGroup:Lcom/android/ims/internal/CallGroup;

    invoke-virtual {v2, p1}, Lcom/android/ims/internal/CallGroup;->setOwner(Lcom/android/ims/internal/ICall;)V

    .line 1330
    if-eqz v0, :cond_0

    .line 1331
    invoke-virtual {v0}, Lcom/android/ims/ImsCall;->close()V

    goto :goto_0
.end method


# virtual methods
.method public accept(I)V
    .locals 2
    .param p1, "callType"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/ims/ImsException;
        }
    .end annotation

    .prologue
    .line 819
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "accept :: session="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/ims/ImsCall;->mSession:Lcom/android/ims/internal/ImsCallSession;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/ims/ImsCall;->log(Ljava/lang/String;)V

    .line 822
    new-instance v0, Lcom/android/ims/ImsStreamMediaProfile;

    invoke-direct {v0}, Lcom/android/ims/ImsStreamMediaProfile;-><init>()V

    invoke-virtual {p0, p1, v0}, Lcom/android/ims/ImsCall;->accept(ILcom/android/ims/ImsStreamMediaProfile;)V

    .line 823
    return-void
.end method

.method public accept(ILcom/android/ims/ImsStreamMediaProfile;)V
    .locals 5
    .param p1, "callType"    # I
    .param p2, "profile"    # Lcom/android/ims/ImsStreamMediaProfile;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/ims/ImsException;
        }
    .end annotation

    .prologue
    .line 835
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "accept :: session="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/ims/ImsCall;->mSession:Lcom/android/ims/internal/ImsCallSession;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", callType="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", profile="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/ims/ImsCall;->log(Ljava/lang/String;)V

    .line 839
    iget-object v2, p0, Lcom/android/ims/ImsCall;->mLockObj:Ljava/lang/Object;

    monitor-enter v2

    .line 840
    :try_start_0
    iget-object v1, p0, Lcom/android/ims/ImsCall;->mSession:Lcom/android/ims/internal/ImsCallSession;

    if-nez v1, :cond_0

    .line 841
    new-instance v1, Lcom/android/ims/ImsException;

    const-string v3, "No call to answer"

    const/16 v4, 0x94

    invoke-direct {v1, v3, v4}, Lcom/android/ims/ImsException;-><init>(Ljava/lang/String;I)V

    throw v1

    .line 865
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 846
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/android/ims/ImsCall;->mSession:Lcom/android/ims/internal/ImsCallSession;

    invoke-virtual {v1, p1, p2}, Lcom/android/ims/internal/ImsCallSession;->accept(ILcom/android/ims/ImsStreamMediaProfile;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 852
    :try_start_2
    iget-boolean v1, p0, Lcom/android/ims/ImsCall;->mInCall:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/ims/ImsCall;->mProposedCallProfile:Lcom/android/ims/ImsCallProfile;

    if-eqz v1, :cond_1

    .line 854
    const-string v1, "accept :: call profile will be updated"

    invoke-direct {p0, v1}, Lcom/android/ims/ImsCall;->log(Ljava/lang/String;)V

    .line 857
    iget-object v1, p0, Lcom/android/ims/ImsCall;->mProposedCallProfile:Lcom/android/ims/ImsCallProfile;

    iput-object v1, p0, Lcom/android/ims/ImsCall;->mCallProfile:Lcom/android/ims/ImsCallProfile;

    .line 858
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/ims/ImsCall;->mProposedCallProfile:Lcom/android/ims/ImsCallProfile;

    .line 862
    :cond_1
    iget-boolean v1, p0, Lcom/android/ims/ImsCall;->mInCall:Z

    if-eqz v1, :cond_2

    iget v1, p0, Lcom/android/ims/ImsCall;->mUpdateRequest:I

    const/4 v3, 0x6

    if-ne v1, v3, :cond_2

    .line 863
    const/4 v1, 0x0

    iput v1, p0, Lcom/android/ims/ImsCall;->mUpdateRequest:I

    .line 865
    :cond_2
    monitor-exit v2

    .line 866
    return-void

    .line 847
    :catch_0
    move-exception v0

    .line 848
    .local v0, "t":Ljava/lang/Throwable;
    const-string v1, "accept :: "

    invoke-direct {p0, v1, v0}, Lcom/android/ims/ImsCall;->loge(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 849
    new-instance v1, Lcom/android/ims/ImsException;

    const-string v3, "accept()"

    const/4 v4, 0x0

    invoke-direct {v1, v3, v0, v4}, Lcom/android/ims/ImsException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;I)V

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method

.method public attachSession(Lcom/android/ims/internal/ImsCallSession;)V
    .locals 4
    .param p1, "session"    # Lcom/android/ims/internal/ImsCallSession;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/ims/ImsException;
        }
    .end annotation

    .prologue
    .line 740
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "attachSession :: session="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/ims/ImsCall;->log(Ljava/lang/String;)V

    .line 743
    iget-object v2, p0, Lcom/android/ims/ImsCall;->mLockObj:Ljava/lang/Object;

    monitor-enter v2

    .line 744
    :try_start_0
    iput-object p1, p0, Lcom/android/ims/ImsCall;->mSession:Lcom/android/ims/internal/ImsCallSession;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 747
    :try_start_1
    iget-object v1, p0, Lcom/android/ims/ImsCall;->mSession:Lcom/android/ims/internal/ImsCallSession;

    invoke-direct {p0}, Lcom/android/ims/ImsCall;->createCallSessionListener()Lcom/android/ims/internal/ImsCallSession$Listener;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/android/ims/internal/ImsCallSession;->setListener(Lcom/android/ims/internal/ImsCallSession$Listener;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 752
    :goto_0
    :try_start_2
    monitor-exit v2

    .line 753
    return-void

    .line 748
    :catch_0
    move-exception v0

    .line 749
    .local v0, "t":Ljava/lang/Throwable;
    const-string v1, "attachSession :: "

    invoke-direct {p0, v1, v0}, Lcom/android/ims/ImsCall;->loge(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 750
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/android/ims/ImsCall;->throwImsException(Ljava/lang/Throwable;I)V

    goto :goto_0

    .line 752
    .end local v0    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method public checkIfRemoteUserIsSame(Ljava/lang/String;)Z
    .locals 3
    .param p1, "userId"    # Ljava/lang/String;

    .prologue
    .line 452
    if-nez p1, :cond_0

    .line 453
    const/4 v0, 0x0

    .line 456
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/ims/ImsCall;->mCallProfile:Lcom/android/ims/ImsCallProfile;

    const-string v1, "remote_uri"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/android/ims/ImsCallProfile;->getCallExtra(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public close()V
    .locals 2

    .prologue
    .line 429
    iget-object v1, p0, Lcom/android/ims/ImsCall;->mLockObj:Ljava/lang/Object;

    monitor-enter v1

    .line 430
    :try_start_0
    invoke-direct {p0}, Lcom/android/ims/ImsCall;->destroyCallGroup()V

    .line 432
    iget-object v0, p0, Lcom/android/ims/ImsCall;->mSession:Lcom/android/ims/internal/ImsCallSession;

    if-eqz v0, :cond_0

    .line 433
    iget-object v0, p0, Lcom/android/ims/ImsCall;->mSession:Lcom/android/ims/internal/ImsCallSession;

    invoke-virtual {v0}, Lcom/android/ims/internal/ImsCallSession;->close()V

    .line 434
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/ims/ImsCall;->mSession:Lcom/android/ims/internal/ImsCallSession;

    .line 437
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/ims/ImsCall;->mCallProfile:Lcom/android/ims/ImsCallProfile;

    .line 438
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/ims/ImsCall;->mProposedCallProfile:Lcom/android/ims/ImsCallProfile;

    .line 439
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/ims/ImsCall;->mLastReasonInfo:Lcom/android/ims/ImsReasonInfo;

    .line 440
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/ims/ImsCall;->mMediaSession:Lcom/android/ims/internal/ImsStreamMediaSession;

    .line 441
    monitor-exit v1

    .line 442
    return-void

    .line 441
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public equalsTo(Lcom/android/ims/internal/ICall;)Z
    .locals 2
    .param p1, "call"    # Lcom/android/ims/internal/ICall;

    .prologue
    const/4 v0, 0x0

    .line 467
    if-nez p1, :cond_1

    .line 475
    .end local p1    # "call":Lcom/android/ims/internal/ICall;
    :cond_0
    :goto_0
    return v0

    .line 471
    .restart local p1    # "call":Lcom/android/ims/internal/ICall;
    :cond_1
    instance-of v1, p1, Lcom/android/ims/ImsCall;

    if-eqz v1, :cond_0

    .line 472
    check-cast p1, Lcom/android/ims/ImsCall;

    .end local p1    # "call":Lcom/android/ims/internal/ICall;
    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public extendToConference([Ljava/lang/String;)V
    .locals 4
    .param p1, "participants"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/ims/ImsException;
        }
    .end annotation

    .prologue
    const/16 v2, 0x66

    .line 1141
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "extendToConference :: session="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/ims/ImsCall;->mSession:Lcom/android/ims/internal/ImsCallSession;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/ims/ImsCall;->log(Ljava/lang/String;)V

    .line 1144
    invoke-virtual {p0}, Lcom/android/ims/ImsCall;->isOnHold()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1146
    const-string v0, "extendToConference :: call is on hold"

    invoke-direct {p0, v0}, Lcom/android/ims/ImsCall;->log(Ljava/lang/String;)V

    .line 1148
    new-instance v0, Lcom/android/ims/ImsException;

    const-string v1, "Not in a call to extend a call to conference"

    invoke-direct {v0, v1, v2}, Lcom/android/ims/ImsException;-><init>(Ljava/lang/String;I)V

    throw v0

    .line 1152
    :cond_0
    iget-object v1, p0, Lcom/android/ims/ImsCall;->mLockObj:Ljava/lang/Object;

    monitor-enter v1

    .line 1153
    :try_start_0
    iget v0, p0, Lcom/android/ims/ImsCall;->mUpdateRequest:I

    if-eqz v0, :cond_1

    .line 1155
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "extendToConference :: update is in progress; request="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/android/ims/ImsCall;->mUpdateRequest:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/ims/ImsCall;->log(Ljava/lang/String;)V

    .line 1157
    new-instance v0, Lcom/android/ims/ImsException;

    const-string v2, "Call update is in progress"

    const/16 v3, 0x66

    invoke-direct {v0, v2, v3}, Lcom/android/ims/ImsException;-><init>(Ljava/lang/String;I)V

    throw v0

    .line 1169
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1161
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/android/ims/ImsCall;->mSession:Lcom/android/ims/internal/ImsCallSession;

    if-nez v0, :cond_2

    .line 1162
    const-string v0, "extendToConference :: "

    invoke-direct {p0, v0}, Lcom/android/ims/ImsCall;->loge(Ljava/lang/String;)V

    .line 1163
    new-instance v0, Lcom/android/ims/ImsException;

    const-string v2, "No call session"

    const/16 v3, 0x94

    invoke-direct {v0, v2, v3}, Lcom/android/ims/ImsException;-><init>(Ljava/lang/String;I)V

    throw v0

    .line 1167
    :cond_2
    iget-object v0, p0, Lcom/android/ims/ImsCall;->mSession:Lcom/android/ims/internal/ImsCallSession;

    invoke-virtual {v0, p1}, Lcom/android/ims/internal/ImsCallSession;->extendToConference([Ljava/lang/String;)V

    .line 1168
    const/4 v0, 0x5

    iput v0, p0, Lcom/android/ims/ImsCall;->mUpdateRequest:I

    .line 1169
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1170
    return-void
.end method

.method public getCallExtra(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/ims/ImsException;
        }
    .end annotation

    .prologue
    .line 575
    iget-object v2, p0, Lcom/android/ims/ImsCall;->mLockObj:Ljava/lang/Object;

    monitor-enter v2

    .line 577
    :try_start_0
    iget-object v1, p0, Lcom/android/ims/ImsCall;->mSession:Lcom/android/ims/internal/ImsCallSession;

    if-nez v1, :cond_0

    .line 578
    new-instance v1, Lcom/android/ims/ImsException;

    const-string v3, "No call session"

    const/16 v4, 0x94

    invoke-direct {v1, v3, v4}, Lcom/android/ims/ImsException;-><init>(Ljava/lang/String;I)V

    throw v1

    .line 588
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 583
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/android/ims/ImsCall;->mSession:Lcom/android/ims/internal/ImsCallSession;

    invoke-virtual {v1, p1}, Lcom/android/ims/internal/ImsCallSession;->getProperty(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    :try_start_2
    monitor-exit v2

    return-object v1

    .line 584
    :catch_0
    move-exception v0

    .line 585
    .local v0, "t":Ljava/lang/Throwable;
    const-string v1, "getCallExtra :: "

    invoke-direct {p0, v1, v0}, Lcom/android/ims/ImsCall;->loge(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 586
    new-instance v1, Lcom/android/ims/ImsException;

    const-string v3, "getCallExtra()"

    const/4 v4, 0x0

    invoke-direct {v1, v3, v0, v4}, Lcom/android/ims/ImsException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;I)V

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method

.method public getCallProfile()Lcom/android/ims/ImsCallProfile;
    .locals 2

    .prologue
    .line 484
    iget-object v1, p0, Lcom/android/ims/ImsCall;->mLockObj:Ljava/lang/Object;

    monitor-enter v1

    .line 485
    :try_start_0
    iget-object v0, p0, Lcom/android/ims/ImsCall;->mCallProfile:Lcom/android/ims/ImsCallProfile;

    monitor-exit v1

    return-object v0

    .line 486
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getCallSession()Lcom/android/ims/internal/ImsCallSession;
    .locals 2

    .prologue
    .line 548
    iget-object v1, p0, Lcom/android/ims/ImsCall;->mLockObj:Ljava/lang/Object;

    monitor-enter v1

    .line 549
    :try_start_0
    iget-object v0, p0, Lcom/android/ims/ImsCall;->mSession:Lcom/android/ims/internal/ImsCallSession;

    monitor-exit v1

    return-object v0

    .line 550
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getLastReasonInfo()Lcom/android/ims/ImsReasonInfo;
    .locals 2

    .prologue
    .line 597
    iget-object v1, p0, Lcom/android/ims/ImsCall;->mLockObj:Ljava/lang/Object;

    monitor-enter v1

    .line 598
    :try_start_0
    iget-object v0, p0, Lcom/android/ims/ImsCall;->mLastReasonInfo:Lcom/android/ims/ImsReasonInfo;

    monitor-exit v1

    return-object v0

    .line 599
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getLocalCallProfile()Lcom/android/ims/ImsCallProfile;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/ims/ImsException;
        }
    .end annotation

    .prologue
    .line 495
    iget-object v2, p0, Lcom/android/ims/ImsCall;->mLockObj:Ljava/lang/Object;

    monitor-enter v2

    .line 496
    :try_start_0
    iget-object v1, p0, Lcom/android/ims/ImsCall;->mSession:Lcom/android/ims/internal/ImsCallSession;

    if-nez v1, :cond_0

    .line 497
    new-instance v1, Lcom/android/ims/ImsException;

    const-string v3, "No call session"

    const/16 v4, 0x94

    invoke-direct {v1, v3, v4}, Lcom/android/ims/ImsException;-><init>(Ljava/lang/String;I)V

    throw v1

    .line 507
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 502
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/android/ims/ImsCall;->mSession:Lcom/android/ims/internal/ImsCallSession;

    invoke-virtual {v1}, Lcom/android/ims/internal/ImsCallSession;->getLocalCallProfile()Lcom/android/ims/ImsCallProfile;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    :try_start_2
    monitor-exit v2

    return-object v1

    .line 503
    :catch_0
    move-exception v0

    .line 504
    .local v0, "t":Ljava/lang/Throwable;
    const-string v1, "getLocalCallProfile :: "

    invoke-direct {p0, v1, v0}, Lcom/android/ims/ImsCall;->loge(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 505
    new-instance v1, Lcom/android/ims/ImsException;

    const-string v3, "getLocalCallProfile()"

    const/4 v4, 0x0

    invoke-direct {v1, v3, v0, v4}, Lcom/android/ims/ImsException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;I)V

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method

.method public getMediaSession()Lcom/android/ims/internal/ImsStreamMediaSession;
    .locals 2

    .prologue
    .line 561
    iget-object v1, p0, Lcom/android/ims/ImsCall;->mLockObj:Ljava/lang/Object;

    monitor-enter v1

    .line 562
    :try_start_0
    iget-object v0, p0, Lcom/android/ims/ImsCall;->mMediaSession:Lcom/android/ims/internal/ImsStreamMediaSession;

    monitor-exit v1

    return-object v0

    .line 563
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getProposedCallProfile()Lcom/android/ims/ImsCallProfile;
    .locals 2

    .prologue
    .line 516
    iget-object v1, p0, Lcom/android/ims/ImsCall;->mLockObj:Ljava/lang/Object;

    monitor-enter v1

    .line 517
    :try_start_0
    invoke-virtual {p0}, Lcom/android/ims/ImsCall;->isInCall()Z

    move-result v0

    if-nez v0, :cond_0

    .line 518
    const/4 v0, 0x0

    monitor-exit v1

    .line 521
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/ims/ImsCall;->mProposedCallProfile:Lcom/android/ims/ImsCallProfile;

    monitor-exit v1

    goto :goto_0

    .line 522
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getState()I
    .locals 2

    .prologue
    .line 532
    iget-object v1, p0, Lcom/android/ims/ImsCall;->mLockObj:Ljava/lang/Object;

    monitor-enter v1

    .line 533
    :try_start_0
    iget-object v0, p0, Lcom/android/ims/ImsCall;->mSession:Lcom/android/ims/internal/ImsCallSession;

    if-nez v0, :cond_0

    .line 534
    const/4 v0, 0x0

    monitor-exit v1

    .line 537
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/ims/ImsCall;->mSession:Lcom/android/ims/internal/ImsCallSession;

    invoke-virtual {v0}, Lcom/android/ims/internal/ImsCallSession;->getState()I

    move-result v0

    monitor-exit v1

    goto :goto_0

    .line 538
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public hasPendingUpdate()Z
    .locals 2

    .prologue
    .line 608
    iget-object v1, p0, Lcom/android/ims/ImsCall;->mLockObj:Ljava/lang/Object;

    monitor-enter v1

    .line 609
    :try_start_0
    iget v0, p0, Lcom/android/ims/ImsCall;->mUpdateRequest:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 610
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public hold()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/ims/ImsException;
        }
    .end annotation

    .prologue
    .line 939
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "hold :: session="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/ims/ImsCall;->mSession:Lcom/android/ims/internal/ImsCallSession;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/android/ims/ImsCall;->log(Ljava/lang/String;)V

    .line 944
    iget-object v3, p0, Lcom/android/ims/ImsCall;->mLockObj:Ljava/lang/Object;

    monitor-enter v3

    .line 945
    :try_start_0
    iget-object v0, p0, Lcom/android/ims/ImsCall;->mCallGroup:Lcom/android/ims/internal/CallGroup;

    .line 946
    .local v0, "callGroup":Lcom/android/ims/internal/CallGroup;
    if-eqz v0, :cond_0

    invoke-virtual {v0, p0}, Lcom/android/ims/internal/CallGroup;->isOwner(Lcom/android/ims/internal/ICall;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 947
    const-string v2, "hold owner of the call group"

    invoke-direct {p0, v2}, Lcom/android/ims/ImsCall;->log(Ljava/lang/String;)V

    .line 948
    invoke-virtual {v0}, Lcom/android/ims/internal/CallGroup;->getOwner()Lcom/android/ims/internal/ICall;

    move-result-object v1

    check-cast v1, Lcom/android/ims/ImsCall;

    .line 949
    .local v1, "owner":Lcom/android/ims/ImsCall;
    if-eqz v1, :cond_0

    .line 950
    invoke-virtual {v1}, Lcom/android/ims/ImsCall;->hold()V

    .line 951
    monitor-exit v3

    .line 981
    .end local v1    # "owner":Lcom/android/ims/ImsCall;
    :goto_0
    return-void

    .line 954
    :cond_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 956
    invoke-virtual {p0}, Lcom/android/ims/ImsCall;->isOnHold()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 958
    const-string v2, "hold :: call is already on hold"

    invoke-direct {p0, v2}, Lcom/android/ims/ImsCall;->log(Ljava/lang/String;)V

    goto :goto_0

    .line 954
    .end local v0    # "callGroup":Lcom/android/ims/internal/CallGroup;
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 963
    .restart local v0    # "callGroup":Lcom/android/ims/internal/CallGroup;
    :cond_1
    iget-object v3, p0, Lcom/android/ims/ImsCall;->mLockObj:Ljava/lang/Object;

    monitor-enter v3

    .line 964
    :try_start_2
    iget v2, p0, Lcom/android/ims/ImsCall;->mUpdateRequest:I

    if-eqz v2, :cond_2

    .line 965
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "hold :: update is in progress; request="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v4, p0, Lcom/android/ims/ImsCall;->mUpdateRequest:I

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/android/ims/ImsCall;->loge(Ljava/lang/String;)V

    .line 966
    new-instance v2, Lcom/android/ims/ImsException;

    const-string v4, "Call update is in progress"

    const/16 v5, 0x66

    invoke-direct {v2, v4, v5}, Lcom/android/ims/ImsException;-><init>(Ljava/lang/String;I)V

    throw v2

    .line 980
    :catchall_1
    move-exception v2

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v2

    .line 970
    :cond_2
    :try_start_3
    iget-object v2, p0, Lcom/android/ims/ImsCall;->mSession:Lcom/android/ims/internal/ImsCallSession;

    if-nez v2, :cond_3

    .line 971
    const-string v2, "hold :: "

    invoke-direct {p0, v2}, Lcom/android/ims/ImsCall;->loge(Ljava/lang/String;)V

    .line 972
    new-instance v2, Lcom/android/ims/ImsException;

    const-string v4, "No call session"

    const/16 v5, 0x94

    invoke-direct {v2, v4, v5}, Lcom/android/ims/ImsException;-><init>(Ljava/lang/String;I)V

    throw v2

    .line 976
    :cond_3
    iget-object v2, p0, Lcom/android/ims/ImsCall;->mSession:Lcom/android/ims/internal/ImsCallSession;

    invoke-direct {p0}, Lcom/android/ims/ImsCall;->createHoldMediaProfile()Lcom/android/ims/ImsStreamMediaProfile;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/android/ims/internal/ImsCallSession;->hold(Lcom/android/ims/ImsStreamMediaProfile;)V

    .line 978
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/android/ims/ImsCall;->mHold:Z

    .line 979
    const/4 v2, 0x1

    iput v2, p0, Lcom/android/ims/ImsCall;->mUpdateRequest:I

    .line 980
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0
.end method

.method public inviteParticipants([Ljava/lang/String;)V
    .locals 4
    .param p1, "participants"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/ims/ImsException;
        }
    .end annotation

    .prologue
    .line 1178
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "inviteParticipants :: session="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/ims/ImsCall;->mSession:Lcom/android/ims/internal/ImsCallSession;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/ims/ImsCall;->log(Ljava/lang/String;)V

    .line 1181
    iget-object v1, p0, Lcom/android/ims/ImsCall;->mLockObj:Ljava/lang/Object;

    monitor-enter v1

    .line 1182
    :try_start_0
    iget-object v0, p0, Lcom/android/ims/ImsCall;->mSession:Lcom/android/ims/internal/ImsCallSession;

    if-nez v0, :cond_0

    .line 1183
    const-string v0, "inviteParticipants :: "

    invoke-direct {p0, v0}, Lcom/android/ims/ImsCall;->loge(Ljava/lang/String;)V

    .line 1184
    new-instance v0, Lcom/android/ims/ImsException;

    const-string v2, "No call session"

    const/16 v3, 0x94

    invoke-direct {v0, v2, v3}, Lcom/android/ims/ImsException;-><init>(Ljava/lang/String;I)V

    throw v0

    .line 1189
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1188
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/android/ims/ImsCall;->mSession:Lcom/android/ims/internal/ImsCallSession;

    invoke-virtual {v0, p1}, Lcom/android/ims/internal/ImsCallSession;->inviteParticipants([Ljava/lang/String;)V

    .line 1189
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1190
    return-void
.end method

.method public isInCall()Z
    .locals 2

    .prologue
    .line 619
    iget-object v1, p0, Lcom/android/ims/ImsCall;->mLockObj:Ljava/lang/Object;

    monitor-enter v1

    .line 620
    :try_start_0
    iget-boolean v0, p0, Lcom/android/ims/ImsCall;->mInCall:Z

    monitor-exit v1

    return v0

    .line 621
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public isMuted()Z
    .locals 2

    .prologue
    .line 630
    iget-object v1, p0, Lcom/android/ims/ImsCall;->mLockObj:Ljava/lang/Object;

    monitor-enter v1

    .line 631
    :try_start_0
    iget-boolean v0, p0, Lcom/android/ims/ImsCall;->mMute:Z

    monitor-exit v1

    return v0

    .line 632
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public isOnHold()Z
    .locals 2

    .prologue
    .line 641
    iget-object v1, p0, Lcom/android/ims/ImsCall;->mLockObj:Ljava/lang/Object;

    monitor-enter v1

    .line 642
    :try_start_0
    iget-boolean v0, p0, Lcom/android/ims/ImsCall;->mHold:Z

    monitor-exit v1

    return v0

    .line 643
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public merge()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/ims/ImsException;
        }
    .end annotation

    .prologue
    .line 1043
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "merge :: session="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/ims/ImsCall;->mSession:Lcom/android/ims/internal/ImsCallSession;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/ims/ImsCall;->log(Ljava/lang/String;)V

    .line 1046
    iget-object v1, p0, Lcom/android/ims/ImsCall;->mLockObj:Ljava/lang/Object;

    monitor-enter v1

    .line 1047
    :try_start_0
    iget v0, p0, Lcom/android/ims/ImsCall;->mUpdateRequest:I

    if-eqz v0, :cond_0

    .line 1048
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "merge :: update is in progress; request="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/android/ims/ImsCall;->mUpdateRequest:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/ims/ImsCall;->loge(Ljava/lang/String;)V

    .line 1049
    new-instance v0, Lcom/android/ims/ImsException;

    const-string v2, "Call update is in progress"

    const/16 v3, 0x66

    invoke-direct {v0, v2, v3}, Lcom/android/ims/ImsException;-><init>(Ljava/lang/String;I)V

    throw v0

    .line 1071
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1053
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/android/ims/ImsCall;->mSession:Lcom/android/ims/internal/ImsCallSession;

    if-nez v0, :cond_1

    .line 1054
    const-string v0, "merge :: "

    invoke-direct {p0, v0}, Lcom/android/ims/ImsCall;->loge(Ljava/lang/String;)V

    .line 1055
    new-instance v0, Lcom/android/ims/ImsException;

    const-string v2, "No call session"

    const/16 v3, 0x94

    invoke-direct {v0, v2, v3}, Lcom/android/ims/ImsException;-><init>(Ljava/lang/String;I)V

    throw v0

    .line 1061
    :cond_1
    iget-boolean v0, p0, Lcom/android/ims/ImsCall;->mHold:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/ims/ImsCall;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x1120073

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1063
    :cond_2
    iget-object v0, p0, Lcom/android/ims/ImsCall;->mSession:Lcom/android/ims/internal/ImsCallSession;

    invoke-virtual {v0}, Lcom/android/ims/internal/ImsCallSession;->merge()V

    .line 1064
    const/4 v0, 0x4

    iput v0, p0, Lcom/android/ims/ImsCall;->mUpdateRequest:I

    .line 1071
    :goto_0
    monitor-exit v1

    .line 1072
    return-void

    .line 1066
    :cond_3
    iget-object v0, p0, Lcom/android/ims/ImsCall;->mSession:Lcom/android/ims/internal/ImsCallSession;

    invoke-direct {p0}, Lcom/android/ims/ImsCall;->createHoldMediaProfile()Lcom/android/ims/ImsStreamMediaProfile;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/android/ims/internal/ImsCallSession;->hold(Lcom/android/ims/ImsStreamMediaProfile;)V

    .line 1068
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/ims/ImsCall;->mHold:Z

    .line 1069
    const/4 v0, 0x2

    iput v0, p0, Lcom/android/ims/ImsCall;->mUpdateRequest:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public merge(Lcom/android/ims/ImsCall;)V
    .locals 3
    .param p1, "bgCall"    # Lcom/android/ims/ImsCall;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/ims/ImsException;
        }
    .end annotation

    .prologue
    .line 1083
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "merge(1) :: session="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/ims/ImsCall;->mSession:Lcom/android/ims/internal/ImsCallSession;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/ims/ImsCall;->log(Ljava/lang/String;)V

    .line 1086
    if-nez p1, :cond_0

    .line 1087
    new-instance v0, Lcom/android/ims/ImsException;

    const-string v1, "No background call"

    const/16 v2, 0x65

    invoke-direct {v0, v1, v2}, Lcom/android/ims/ImsException;-><init>(Ljava/lang/String;I)V

    throw v0

    .line 1091
    :cond_0
    iget-object v1, p0, Lcom/android/ims/ImsCall;->mLockObj:Ljava/lang/Object;

    monitor-enter v1

    .line 1092
    :try_start_0
    invoke-direct {p0, p1}, Lcom/android/ims/ImsCall;->createCallGroup(Lcom/android/ims/ImsCall;)V

    .line 1093
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1095
    invoke-virtual {p0}, Lcom/android/ims/ImsCall;->merge()V

    .line 1096
    return-void

    .line 1093
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public reject(I)V
    .locals 3
    .param p1, "reason"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/ims/ImsException;
        }
    .end annotation

    .prologue
    .line 877
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "reject :: session="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/ims/ImsCall;->mSession:Lcom/android/ims/internal/ImsCallSession;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", reason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/ims/ImsCall;->log(Ljava/lang/String;)V

    .line 880
    iget-object v1, p0, Lcom/android/ims/ImsCall;->mLockObj:Ljava/lang/Object;

    monitor-enter v1

    .line 881
    :try_start_0
    iget-object v0, p0, Lcom/android/ims/ImsCall;->mSession:Lcom/android/ims/internal/ImsCallSession;

    if-eqz v0, :cond_0

    .line 882
    iget-object v0, p0, Lcom/android/ims/ImsCall;->mSession:Lcom/android/ims/internal/ImsCallSession;

    invoke-virtual {v0, p1}, Lcom/android/ims/internal/ImsCallSession;->reject(I)V

    .line 885
    :cond_0
    iget-boolean v0, p0, Lcom/android/ims/ImsCall;->mInCall:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/ims/ImsCall;->mProposedCallProfile:Lcom/android/ims/ImsCallProfile;

    if-eqz v0, :cond_1

    .line 887
    const-string v0, "reject :: call profile is not updated; destroy it..."

    invoke-direct {p0, v0}, Lcom/android/ims/ImsCall;->log(Ljava/lang/String;)V

    .line 890
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/ims/ImsCall;->mProposedCallProfile:Lcom/android/ims/ImsCallProfile;

    .line 894
    :cond_1
    iget-boolean v0, p0, Lcom/android/ims/ImsCall;->mInCall:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/android/ims/ImsCall;->mUpdateRequest:I

    const/4 v2, 0x6

    if-ne v0, v2, :cond_2

    .line 895
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/ims/ImsCall;->mUpdateRequest:I

    .line 897
    :cond_2
    monitor-exit v1

    .line 898
    return-void

    .line 897
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public removeParticipants([Ljava/lang/String;)V
    .locals 4
    .param p1, "participants"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/ims/ImsException;
        }
    .end annotation

    .prologue
    .line 1198
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "removeParticipants :: session="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/ims/ImsCall;->mSession:Lcom/android/ims/internal/ImsCallSession;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/ims/ImsCall;->log(Ljava/lang/String;)V

    .line 1201
    iget-object v1, p0, Lcom/android/ims/ImsCall;->mLockObj:Ljava/lang/Object;

    monitor-enter v1

    .line 1202
    :try_start_0
    iget-object v0, p0, Lcom/android/ims/ImsCall;->mSession:Lcom/android/ims/internal/ImsCallSession;

    if-nez v0, :cond_0

    .line 1203
    const-string v0, "removeParticipants :: "

    invoke-direct {p0, v0}, Lcom/android/ims/ImsCall;->loge(Ljava/lang/String;)V

    .line 1204
    new-instance v0, Lcom/android/ims/ImsException;

    const-string v2, "No call session"

    const/16 v3, 0x94

    invoke-direct {v0, v2, v3}, Lcom/android/ims/ImsException;-><init>(Ljava/lang/String;I)V

    throw v0

    .line 1209
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1208
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/android/ims/ImsCall;->mSession:Lcom/android/ims/internal/ImsCallSession;

    invoke-virtual {v0, p1}, Lcom/android/ims/internal/ImsCallSession;->removeParticipants([Ljava/lang/String;)V

    .line 1209
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1210
    return-void
.end method

.method public resume()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/ims/ImsException;
        }
    .end annotation

    .prologue
    .line 991
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "resume :: session="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/ims/ImsCall;->mSession:Lcom/android/ims/internal/ImsCallSession;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/android/ims/ImsCall;->log(Ljava/lang/String;)V

    .line 996
    iget-object v3, p0, Lcom/android/ims/ImsCall;->mLockObj:Ljava/lang/Object;

    monitor-enter v3

    .line 997
    :try_start_0
    iget-object v0, p0, Lcom/android/ims/ImsCall;->mCallGroup:Lcom/android/ims/internal/CallGroup;

    .line 998
    .local v0, "callGroup":Lcom/android/ims/internal/CallGroup;
    if-eqz v0, :cond_0

    invoke-virtual {v0, p0}, Lcom/android/ims/internal/CallGroup;->isOwner(Lcom/android/ims/internal/ICall;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 999
    const-string v2, "resume owner of the call group"

    invoke-direct {p0, v2}, Lcom/android/ims/ImsCall;->log(Ljava/lang/String;)V

    .line 1000
    invoke-virtual {v0}, Lcom/android/ims/internal/CallGroup;->getOwner()Lcom/android/ims/internal/ICall;

    move-result-object v1

    check-cast v1, Lcom/android/ims/ImsCall;

    .line 1001
    .local v1, "owner":Lcom/android/ims/ImsCall;
    if-eqz v1, :cond_0

    .line 1002
    invoke-virtual {v1}, Lcom/android/ims/ImsCall;->resume()V

    .line 1003
    monitor-exit v3

    .line 1033
    .end local v1    # "owner":Lcom/android/ims/ImsCall;
    :goto_0
    return-void

    .line 1006
    :cond_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1008
    invoke-virtual {p0}, Lcom/android/ims/ImsCall;->isOnHold()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1010
    const-string v2, "resume :: call is in conversation"

    invoke-direct {p0, v2}, Lcom/android/ims/ImsCall;->log(Ljava/lang/String;)V

    goto :goto_0

    .line 1006
    .end local v0    # "callGroup":Lcom/android/ims/internal/CallGroup;
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 1015
    .restart local v0    # "callGroup":Lcom/android/ims/internal/CallGroup;
    :cond_1
    iget-object v3, p0, Lcom/android/ims/ImsCall;->mLockObj:Ljava/lang/Object;

    monitor-enter v3

    .line 1016
    :try_start_2
    iget v2, p0, Lcom/android/ims/ImsCall;->mUpdateRequest:I

    if-eqz v2, :cond_2

    .line 1017
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "resume :: update is in progress; request="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v4, p0, Lcom/android/ims/ImsCall;->mUpdateRequest:I

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/android/ims/ImsCall;->loge(Ljava/lang/String;)V

    .line 1018
    new-instance v2, Lcom/android/ims/ImsException;

    const-string v4, "Call update is in progress"

    const/16 v5, 0x66

    invoke-direct {v2, v4, v5}, Lcom/android/ims/ImsException;-><init>(Ljava/lang/String;I)V

    throw v2

    .line 1032
    :catchall_1
    move-exception v2

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v2

    .line 1022
    :cond_2
    :try_start_3
    iget-object v2, p0, Lcom/android/ims/ImsCall;->mSession:Lcom/android/ims/internal/ImsCallSession;

    if-nez v2, :cond_3

    .line 1023
    const-string v2, "resume :: "

    invoke-direct {p0, v2}, Lcom/android/ims/ImsCall;->loge(Ljava/lang/String;)V

    .line 1024
    new-instance v2, Lcom/android/ims/ImsException;

    const-string v4, "No call session"

    const/16 v5, 0x94

    invoke-direct {v2, v4, v5}, Lcom/android/ims/ImsException;-><init>(Ljava/lang/String;I)V

    throw v2

    .line 1028
    :cond_3
    iget-object v2, p0, Lcom/android/ims/ImsCall;->mSession:Lcom/android/ims/internal/ImsCallSession;

    invoke-direct {p0}, Lcom/android/ims/ImsCall;->createResumeMediaProfile()Lcom/android/ims/ImsStreamMediaProfile;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/android/ims/internal/ImsCallSession;->resume(Lcom/android/ims/ImsStreamMediaProfile;)V

    .line 1030
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/android/ims/ImsCall;->mHold:Z

    .line 1031
    const/4 v2, 0x3

    iput v2, p0, Lcom/android/ims/ImsCall;->mUpdateRequest:I

    .line 1032
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0
.end method

.method public sendDtmf(C)V
    .locals 1
    .param p1, "c"    # C

    .prologue
    .line 1221
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/android/ims/ImsCall;->sendDtmf(CLandroid/os/Message;)V

    .line 1222
    return-void
.end method

.method public sendDtmf(CLandroid/os/Message;)V
    .locals 2
    .param p1, "c"    # C
    .param p2, "result"    # Landroid/os/Message;

    .prologue
    .line 1234
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "sendDtmf :: session="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/ims/ImsCall;->mSession:Lcom/android/ims/internal/ImsCallSession;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/ims/ImsCall;->log(Ljava/lang/String;)V

    .line 1237
    iget-object v1, p0, Lcom/android/ims/ImsCall;->mLockObj:Ljava/lang/Object;

    monitor-enter v1

    .line 1238
    :try_start_0
    iget-object v0, p0, Lcom/android/ims/ImsCall;->mSession:Lcom/android/ims/internal/ImsCallSession;

    if-eqz v0, :cond_0

    .line 1239
    iget-object v0, p0, Lcom/android/ims/ImsCall;->mSession:Lcom/android/ims/internal/ImsCallSession;

    invoke-virtual {v0, p1}, Lcom/android/ims/internal/ImsCallSession;->sendDtmf(C)V

    .line 1241
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1243
    if-eqz p2, :cond_1

    .line 1244
    invoke-virtual {p2}, Landroid/os/Message;->sendToTarget()V

    .line 1246
    :cond_1
    return-void

    .line 1241
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public sendUssd(Ljava/lang/String;)V
    .locals 4
    .param p1, "ussdMessage"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/ims/ImsException;
        }
    .end annotation

    .prologue
    .line 1255
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "sendUssd :: session="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/ims/ImsCall;->mSession:Lcom/android/ims/internal/ImsCallSession;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", ussdMessage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/ims/ImsCall;->log(Ljava/lang/String;)V

    .line 1258
    iget-object v1, p0, Lcom/android/ims/ImsCall;->mLockObj:Ljava/lang/Object;

    monitor-enter v1

    .line 1259
    :try_start_0
    iget-object v0, p0, Lcom/android/ims/ImsCall;->mSession:Lcom/android/ims/internal/ImsCallSession;

    if-nez v0, :cond_0

    .line 1260
    const-string v0, "sendUssd :: "

    invoke-direct {p0, v0}, Lcom/android/ims/ImsCall;->loge(Ljava/lang/String;)V

    .line 1261
    new-instance v0, Lcom/android/ims/ImsException;

    const-string v2, "No call session"

    const/16 v3, 0x94

    invoke-direct {v0, v2, v3}, Lcom/android/ims/ImsException;-><init>(Ljava/lang/String;I)V

    throw v0

    .line 1266
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1265
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/android/ims/ImsCall;->mSession:Lcom/android/ims/internal/ImsCallSession;

    invoke-virtual {v0, p1}, Lcom/android/ims/internal/ImsCallSession;->sendUssd(Ljava/lang/String;)V

    .line 1266
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1267
    return-void
.end method

.method public setListener(Lcom/android/ims/ImsCall$Listener;)V
    .locals 1
    .param p1, "listener"    # Lcom/android/ims/ImsCall$Listener;

    .prologue
    .line 654
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/android/ims/ImsCall;->setListener(Lcom/android/ims/ImsCall$Listener;Z)V

    .line 655
    return-void
.end method

.method public setListener(Lcom/android/ims/ImsCall$Listener;Z)V
    .locals 7
    .param p1, "listener"    # Lcom/android/ims/ImsCall$Listener;
    .param p2, "callbackImmediately"    # Z

    .prologue
    .line 672
    iget-object v6, p0, Lcom/android/ims/ImsCall;->mLockObj:Ljava/lang/Object;

    monitor-enter v6

    .line 673
    :try_start_0
    iput-object p1, p0, Lcom/android/ims/ImsCall;->mListener:Lcom/android/ims/ImsCall$Listener;

    .line 675
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 676
    :cond_0
    monitor-exit v6

    .line 710
    :goto_0
    return-void

    .line 679
    :cond_1
    iget-boolean v0, p0, Lcom/android/ims/ImsCall;->mInCall:Z

    .line 680
    .local v0, "inCall":Z
    iget-boolean v2, p0, Lcom/android/ims/ImsCall;->mHold:Z

    .line 681
    .local v2, "onHold":Z
    invoke-virtual {p0}, Lcom/android/ims/ImsCall;->getState()I

    move-result v3

    .line 682
    .local v3, "state":I
    iget-object v1, p0, Lcom/android/ims/ImsCall;->mLastReasonInfo:Lcom/android/ims/ImsReasonInfo;

    .line 683
    .local v1, "lastReasonInfo":Lcom/android/ims/ImsReasonInfo;
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 686
    if-eqz v1, :cond_2

    .line 687
    :try_start_1
    invoke-virtual {p1, p0, v1}, Lcom/android/ims/ImsCall$Listener;->onCallError(Lcom/android/ims/ImsCall;Lcom/android/ims/ImsReasonInfo;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 707
    :catch_0
    move-exception v4

    .line 708
    .local v4, "t":Ljava/lang/Throwable;
    const-string v5, "setListener()"

    invoke-direct {p0, v5, v4}, Lcom/android/ims/ImsCall;->loge(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 683
    .end local v0    # "inCall":Z
    .end local v1    # "lastReasonInfo":Lcom/android/ims/ImsReasonInfo;
    .end local v2    # "onHold":Z
    .end local v3    # "state":I
    .end local v4    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v5

    :try_start_2
    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v5

    .line 688
    .restart local v0    # "inCall":Z
    .restart local v1    # "lastReasonInfo":Lcom/android/ims/ImsReasonInfo;
    .restart local v2    # "onHold":Z
    .restart local v3    # "state":I
    :cond_2
    if-eqz v0, :cond_4

    .line 689
    if-eqz v2, :cond_3

    .line 690
    :try_start_3
    invoke-virtual {p1, p0}, Lcom/android/ims/ImsCall$Listener;->onCallHeld(Lcom/android/ims/ImsCall;)V

    goto :goto_0

    .line 692
    :cond_3
    invoke-virtual {p1, p0}, Lcom/android/ims/ImsCall$Listener;->onCallStarted(Lcom/android/ims/ImsCall;)V

    goto :goto_0

    .line 695
    :cond_4
    sparse-switch v3, :sswitch_data_0

    goto :goto_0

    .line 697
    :sswitch_0
    invoke-virtual {p1, p0}, Lcom/android/ims/ImsCall$Listener;->onCallProgressing(Lcom/android/ims/ImsCall;)V

    goto :goto_0

    .line 700
    :sswitch_1
    invoke-virtual {p1, p0, v1}, Lcom/android/ims/ImsCall$Listener;->onCallTerminated(Lcom/android/ims/ImsCall;Lcom/android/ims/ImsReasonInfo;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    .line 695
    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method public setMute(Z)V
    .locals 3
    .param p1, "muted"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/ims/ImsException;
        }
    .end annotation

    .prologue
    .line 718
    iget-object v2, p0, Lcom/android/ims/ImsCall;->mLockObj:Ljava/lang/Object;

    monitor-enter v2

    .line 719
    :try_start_0
    iget-boolean v1, p0, Lcom/android/ims/ImsCall;->mMute:Z

    if-eq v1, p1, :cond_0

    .line 720
    iput-boolean p1, p0, Lcom/android/ims/ImsCall;->mMute:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 723
    :try_start_1
    iget-object v1, p0, Lcom/android/ims/ImsCall;->mSession:Lcom/android/ims/internal/ImsCallSession;

    invoke-virtual {v1, p1}, Lcom/android/ims/internal/ImsCallSession;->setMute(Z)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 729
    :cond_0
    :goto_0
    :try_start_2
    monitor-exit v2

    .line 730
    return-void

    .line 724
    :catch_0
    move-exception v0

    .line 725
    .local v0, "t":Ljava/lang/Throwable;
    const-string v1, "setMute :: "

    invoke-direct {p0, v1, v0}, Lcom/android/ims/ImsCall;->loge(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 726
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/android/ims/ImsCall;->throwImsException(Ljava/lang/Throwable;I)V

    goto :goto_0

    .line 729
    .end local v0    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method public start(Lcom/android/ims/internal/ImsCallSession;Ljava/lang/String;)V
    .locals 5
    .param p1, "session"    # Lcom/android/ims/internal/ImsCallSession;
    .param p2, "callee"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/ims/ImsException;
        }
    .end annotation

    .prologue
    .line 766
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "start(1) :: session="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", callee="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/ims/ImsCall;->log(Ljava/lang/String;)V

    .line 769
    iget-object v2, p0, Lcom/android/ims/ImsCall;->mLockObj:Ljava/lang/Object;

    monitor-enter v2

    .line 770
    :try_start_0
    iput-object p1, p0, Lcom/android/ims/ImsCall;->mSession:Lcom/android/ims/internal/ImsCallSession;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 773
    :try_start_1
    invoke-direct {p0}, Lcom/android/ims/ImsCall;->createCallSessionListener()Lcom/android/ims/internal/ImsCallSession$Listener;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/android/ims/internal/ImsCallSession;->setListener(Lcom/android/ims/internal/ImsCallSession$Listener;)V

    .line 774
    iget-object v1, p0, Lcom/android/ims/ImsCall;->mCallProfile:Lcom/android/ims/ImsCallProfile;

    invoke-virtual {p1, p2, v1}, Lcom/android/ims/internal/ImsCallSession;->start(Ljava/lang/String;Lcom/android/ims/ImsCallProfile;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 779
    :try_start_2
    monitor-exit v2

    .line 780
    return-void

    .line 775
    :catch_0
    move-exception v0

    .line 776
    .local v0, "t":Ljava/lang/Throwable;
    const-string v1, "start(1) :: "

    invoke-direct {p0, v1, v0}, Lcom/android/ims/ImsCall;->loge(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 777
    new-instance v1, Lcom/android/ims/ImsException;

    const-string v3, "start(1)"

    const/4 v4, 0x0

    invoke-direct {v1, v3, v0, v4}, Lcom/android/ims/ImsException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;I)V

    throw v1

    .line 779
    .end local v0    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method public start(Lcom/android/ims/internal/ImsCallSession;[Ljava/lang/String;)V
    .locals 5
    .param p1, "session"    # Lcom/android/ims/internal/ImsCallSession;
    .param p2, "participants"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/ims/ImsException;
        }
    .end annotation

    .prologue
    .line 793
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "start(n) :: session="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", callee="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/ims/ImsCall;->log(Ljava/lang/String;)V

    .line 796
    iget-object v2, p0, Lcom/android/ims/ImsCall;->mLockObj:Ljava/lang/Object;

    monitor-enter v2

    .line 797
    :try_start_0
    iput-object p1, p0, Lcom/android/ims/ImsCall;->mSession:Lcom/android/ims/internal/ImsCallSession;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 800
    :try_start_1
    invoke-direct {p0}, Lcom/android/ims/ImsCall;->createCallSessionListener()Lcom/android/ims/internal/ImsCallSession$Listener;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/android/ims/internal/ImsCallSession;->setListener(Lcom/android/ims/internal/ImsCallSession$Listener;)V

    .line 801
    iget-object v1, p0, Lcom/android/ims/ImsCall;->mCallProfile:Lcom/android/ims/ImsCallProfile;

    invoke-virtual {p1, p2, v1}, Lcom/android/ims/internal/ImsCallSession;->start([Ljava/lang/String;Lcom/android/ims/ImsCallProfile;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 806
    :try_start_2
    monitor-exit v2

    .line 807
    return-void

    .line 802
    :catch_0
    move-exception v0

    .line 803
    .local v0, "t":Ljava/lang/Throwable;
    const-string v1, "start(n) :: "

    invoke-direct {p0, v1, v0}, Lcom/android/ims/ImsCall;->loge(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 804
    new-instance v1, Lcom/android/ims/ImsException;

    const-string v3, "start(n)"

    const/4 v4, 0x0

    invoke-direct {v1, v3, v0, v4}, Lcom/android/ims/ImsException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;I)V

    throw v1

    .line 806
    .end local v0    # "t":Ljava/lang/Throwable;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method public terminate(I)V
    .locals 4
    .param p1, "reason"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/ims/ImsException;
        }
    .end annotation

    .prologue
    .line 908
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "terminate :: session="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/ims/ImsCall;->mSession:Lcom/android/ims/internal/ImsCallSession;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", reason="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/android/ims/ImsCall;->log(Ljava/lang/String;)V

    .line 911
    iget-object v3, p0, Lcom/android/ims/ImsCall;->mLockObj:Ljava/lang/Object;

    monitor-enter v3

    .line 912
    const/4 v2, 0x0

    :try_start_0
    iput-boolean v2, p0, Lcom/android/ims/ImsCall;->mHold:Z

    .line 913
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/android/ims/ImsCall;->mInCall:Z

    .line 914
    invoke-direct {p0}, Lcom/android/ims/ImsCall;->getCallGroup()Lcom/android/ims/internal/CallGroup;

    move-result-object v0

    .line 916
    .local v0, "callGroup":Lcom/android/ims/internal/CallGroup;
    iget-object v2, p0, Lcom/android/ims/ImsCall;->mSession:Lcom/android/ims/internal/ImsCallSession;

    if-eqz v2, :cond_1

    .line 917
    if-eqz v0, :cond_0

    invoke-virtual {v0, p0}, Lcom/android/ims/internal/CallGroup;->isOwner(Lcom/android/ims/internal/ICall;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 918
    const-string v2, "terminate owner of the call group"

    invoke-direct {p0, v2}, Lcom/android/ims/ImsCall;->log(Ljava/lang/String;)V

    .line 919
    invoke-virtual {v0}, Lcom/android/ims/internal/CallGroup;->getOwner()Lcom/android/ims/internal/ICall;

    move-result-object v1

    check-cast v1, Lcom/android/ims/ImsCall;

    .line 920
    .local v1, "owner":Lcom/android/ims/ImsCall;
    if-eqz v1, :cond_0

    .line 921
    invoke-virtual {v1, p1}, Lcom/android/ims/ImsCall;->terminate(I)V

    .line 922
    monitor-exit v3

    .line 928
    .end local v1    # "owner":Lcom/android/ims/ImsCall;
    :goto_0
    return-void

    .line 925
    :cond_0
    iget-object v2, p0, Lcom/android/ims/ImsCall;->mSession:Lcom/android/ims/internal/ImsCallSession;

    invoke-virtual {v2, p1}, Lcom/android/ims/internal/ImsCallSession;->terminate(I)V

    .line 927
    :cond_1
    monitor-exit v3

    goto :goto_0

    .end local v0    # "callGroup":Lcom/android/ims/internal/CallGroup;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public update(ILcom/android/ims/ImsStreamMediaProfile;)V
    .locals 4
    .param p1, "callType"    # I
    .param p2, "mediaProfile"    # Lcom/android/ims/ImsStreamMediaProfile;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/ims/ImsException;
        }
    .end annotation

    .prologue
    const/16 v2, 0x66

    .line 1103
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "update :: session="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/ims/ImsCall;->mSession:Lcom/android/ims/internal/ImsCallSession;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/ims/ImsCall;->log(Ljava/lang/String;)V

    .line 1106
    invoke-virtual {p0}, Lcom/android/ims/ImsCall;->isOnHold()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1108
    const-string v0, "update :: call is on hold"

    invoke-direct {p0, v0}, Lcom/android/ims/ImsCall;->log(Ljava/lang/String;)V

    .line 1110
    new-instance v0, Lcom/android/ims/ImsException;

    const-string v1, "Not in a call to update call"

    invoke-direct {v0, v1, v2}, Lcom/android/ims/ImsException;-><init>(Ljava/lang/String;I)V

    throw v0

    .line 1114
    :cond_0
    iget-object v1, p0, Lcom/android/ims/ImsCall;->mLockObj:Ljava/lang/Object;

    monitor-enter v1

    .line 1115
    :try_start_0
    iget v0, p0, Lcom/android/ims/ImsCall;->mUpdateRequest:I

    if-eqz v0, :cond_1

    .line 1117
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "update :: update is in progress; request="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/android/ims/ImsCall;->mUpdateRequest:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/ims/ImsCall;->log(Ljava/lang/String;)V

    .line 1119
    new-instance v0, Lcom/android/ims/ImsException;

    const-string v2, "Call update is in progress"

    const/16 v3, 0x66

    invoke-direct {v0, v2, v3}, Lcom/android/ims/ImsException;-><init>(Ljava/lang/String;I)V

    throw v0

    .line 1131
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1123
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/android/ims/ImsCall;->mSession:Lcom/android/ims/internal/ImsCallSession;

    if-nez v0, :cond_2

    .line 1124
    const-string v0, "update :: "

    invoke-direct {p0, v0}, Lcom/android/ims/ImsCall;->loge(Ljava/lang/String;)V

    .line 1125
    new-instance v0, Lcom/android/ims/ImsException;

    const-string v2, "No call session"

    const/16 v3, 0x94

    invoke-direct {v0, v2, v3}, Lcom/android/ims/ImsException;-><init>(Ljava/lang/String;I)V

    throw v0

    .line 1129
    :cond_2
    iget-object v0, p0, Lcom/android/ims/ImsCall;->mSession:Lcom/android/ims/internal/ImsCallSession;

    invoke-virtual {v0, p1, p2}, Lcom/android/ims/internal/ImsCallSession;->update(ILcom/android/ims/ImsStreamMediaProfile;)V

    .line 1130
    const/4 v0, 0x6

    iput v0, p0, Lcom/android/ims/ImsCall;->mUpdateRequest:I

    .line 1131
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1132
    return-void
.end method
