.class public Lcom/quicinc/cneapiclient/CNEManager;
.super Ljava/lang/Object;
.source "CNEManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/quicinc/cneapiclient/CNEManager$1;,
        Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureGetType;,
        Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureSetType;,
        Lcom/quicinc/cneapiclient/CNEManager$FeatureType;
    }
.end annotation


# static fields
.field private static final CNE_FEATURE_OFF:I = 0x1

.field private static final CNE_FEATURE_ON:I = 0x2

.field public static final CNE_PREFERENCE_CHANGED_ACTION:Ljava/lang/String; = "com.quicinc.cne.CNE_PREFERENCE_CHANGED"

.field private static final CNE_RET_BUSY:I = -0x2

.field private static final CNE_RET_ERROR:I = -0x1

.field private static final CNE_RET_FEATURE_UNSUPPORTED:I = -0x4

.field private static final CNE_RET_FILE_SIZE_TOO_LARGE:I = -0x5

.field private static final CNE_RET_INVALID_ARGS:I = -0x3

.field private static final CNE_RET_INVALID_VERSION:I = -0x8

.field private static final CNE_RET_PATH_ACCESS_DENIED:I = -0x6

.field private static final CNE_RET_PATH_NAME_TOO_LONG:I = -0x7

.field private static final CNE_RET_SUCCESS:I = 0x3e8

.field public static final EXTRA_FEATURE_ID:Ljava/lang/String; = "cneFeatureId"

.field public static final EXTRA_FEATURE_PARAMETER:Ljava/lang/String; = "cneFeatureParameter"

.field public static final EXTRA_PARAMETER_VALUE:Ljava/lang/String; = "cneParameterValue"

.field public static final EXTRA_VALUE_UNKNOWN:I = -0x3e9

.field public static final IWLAN_FEATURE:I = 0x2

.field public static final IWLAN_FEATURE_ENABLED:I = 0x1

.field public static final IWLAN_FEATURE_OFF:I = 0x1

.field public static final IWLAN_FEATURE_ON:I = 0x2

.field public static final NSRM_FEATURE:I = 0x3

.field public static final NSRM_FEATURE_ENABLED:I = 0x1

.field public static final POLICY_TYPE_ANDSF:I = 0x1

.field public static final POLICY_TYPE_NSRM:I = 0x2

.field private static TAG:Ljava/lang/String; = null

.field public static final VERSION_UNKNOWN:Ljava/lang/String; = "unknown"

.field public static final WQE_FEATURE:I = 0x1

.field public static final WQE_FEATURE_ENABLED:I = 0x1

.field public static final WQE_FEATURE_OFF:I = 0x1

.field public static final WQE_FEATURE_ON:I = 0x2


# instance fields
.field private mCneManagerClass:Ljava/lang/Class;

.field private mCneService:Ljava/lang/Object;

.field private mContext:Landroid/content/Context;

.field private mGetPolVersionMethod:Ljava/lang/reflect/Method;

.field private mIWLANGetMethod:Ljava/lang/reflect/Method;

.field private mIWLANSetMethod:Ljava/lang/reflect/Method;

.field private mNSRMGetMethod:Ljava/lang/reflect/Method;

.field private mNSRMSetMethod:Ljava/lang/reflect/Method;

.field private mUpdatePolMethod:Ljava/lang/reflect/Method;

.field private mWQEGetMethod:Ljava/lang/reflect/Method;

.field private mWQESetMethod:Ljava/lang/reflect/Method;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    const-string v0, "CNEManager"

    sput-object v0, Lcom/quicinc/cneapiclient/CNEManager;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 184
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object v1, p0, Lcom/quicinc/cneapiclient/CNEManager;->mCneManagerClass:Ljava/lang/Class;

    .line 56
    iput-object v1, p0, Lcom/quicinc/cneapiclient/CNEManager;->mCneService:Ljava/lang/Object;

    .line 57
    iput-object v1, p0, Lcom/quicinc/cneapiclient/CNEManager;->mWQEGetMethod:Ljava/lang/reflect/Method;

    .line 58
    iput-object v1, p0, Lcom/quicinc/cneapiclient/CNEManager;->mWQESetMethod:Ljava/lang/reflect/Method;

    .line 59
    iput-object v1, p0, Lcom/quicinc/cneapiclient/CNEManager;->mIWLANGetMethod:Ljava/lang/reflect/Method;

    .line 60
    iput-object v1, p0, Lcom/quicinc/cneapiclient/CNEManager;->mIWLANSetMethod:Ljava/lang/reflect/Method;

    .line 61
    iput-object v1, p0, Lcom/quicinc/cneapiclient/CNEManager;->mNSRMGetMethod:Ljava/lang/reflect/Method;

    .line 62
    iput-object v1, p0, Lcom/quicinc/cneapiclient/CNEManager;->mNSRMSetMethod:Ljava/lang/reflect/Method;

    .line 63
    iput-object v1, p0, Lcom/quicinc/cneapiclient/CNEManager;->mUpdatePolMethod:Ljava/lang/reflect/Method;

    .line 64
    iput-object v1, p0, Lcom/quicinc/cneapiclient/CNEManager;->mGetPolVersionMethod:Ljava/lang/reflect/Method;

    .line 65
    iput-object v1, p0, Lcom/quicinc/cneapiclient/CNEManager;->mContext:Landroid/content/Context;

    .line 186
    :try_start_0
    invoke-direct {p0}, Lcom/quicinc/cneapiclient/CNEManager;->loadCNEService()V

    .line 187
    invoke-direct {p0}, Lcom/quicinc/cneapiclient/CNEManager;->loadMethods()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 192
    return-void

    .line 189
    :catch_0
    move-exception v0

    .line 190
    .local v0, "e":Ljava/lang/Exception;
    throw v0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 202
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object v1, p0, Lcom/quicinc/cneapiclient/CNEManager;->mCneManagerClass:Ljava/lang/Class;

    .line 56
    iput-object v1, p0, Lcom/quicinc/cneapiclient/CNEManager;->mCneService:Ljava/lang/Object;

    .line 57
    iput-object v1, p0, Lcom/quicinc/cneapiclient/CNEManager;->mWQEGetMethod:Ljava/lang/reflect/Method;

    .line 58
    iput-object v1, p0, Lcom/quicinc/cneapiclient/CNEManager;->mWQESetMethod:Ljava/lang/reflect/Method;

    .line 59
    iput-object v1, p0, Lcom/quicinc/cneapiclient/CNEManager;->mIWLANGetMethod:Ljava/lang/reflect/Method;

    .line 60
    iput-object v1, p0, Lcom/quicinc/cneapiclient/CNEManager;->mIWLANSetMethod:Ljava/lang/reflect/Method;

    .line 61
    iput-object v1, p0, Lcom/quicinc/cneapiclient/CNEManager;->mNSRMGetMethod:Ljava/lang/reflect/Method;

    .line 62
    iput-object v1, p0, Lcom/quicinc/cneapiclient/CNEManager;->mNSRMSetMethod:Ljava/lang/reflect/Method;

    .line 63
    iput-object v1, p0, Lcom/quicinc/cneapiclient/CNEManager;->mUpdatePolMethod:Ljava/lang/reflect/Method;

    .line 64
    iput-object v1, p0, Lcom/quicinc/cneapiclient/CNEManager;->mGetPolVersionMethod:Ljava/lang/reflect/Method;

    .line 65
    iput-object v1, p0, Lcom/quicinc/cneapiclient/CNEManager;->mContext:Landroid/content/Context;

    .line 203
    if-eqz p1, :cond_0

    .line 204
    iput-object p1, p0, Lcom/quicinc/cneapiclient/CNEManager;->mContext:Landroid/content/Context;

    .line 209
    :try_start_0
    invoke-direct {p0}, Lcom/quicinc/cneapiclient/CNEManager;->loadCNEService()V

    .line 210
    invoke-direct {p0}, Lcom/quicinc/cneapiclient/CNEManager;->loadMethods()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 215
    return-void

    .line 206
    :cond_0
    new-instance v1, Lcom/quicinc/cneapiclient/CNEManagerInvalidArgumentException;

    const-string v2, "Context cannot be null"

    invoke-direct {v1, v2}, Lcom/quicinc/cneapiclient/CNEManagerInvalidArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 212
    :catch_0
    move-exception v0

    .line 213
    .local v0, "e":Ljava/lang/Exception;
    throw v0
.end method

.method private checkFeatureEnabled(Lcom/quicinc/cneapiclient/CNEManager$FeatureType;)V
    .locals 7
    .param p1, "feature"    # Lcom/quicinc/cneapiclient/CNEManager$FeatureType;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/quicinc/cneapiclient/CNEManagerFeatureUnsupportedException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x6

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 462
    const-string v4, "persist.cne.feature"

    invoke-static {v4, v2}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 463
    .local v1, "value":I
    const/4 v0, 0x0

    .line 464
    .local v0, "enabled":Z
    sget-object v4, Lcom/quicinc/cneapiclient/CNEManager$1;->$SwitchMap$com$quicinc$cneapiclient$CNEManager$FeatureType:[I

    invoke-virtual {p1}, Lcom/quicinc/cneapiclient/CNEManager$FeatureType;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 467
    const/4 v0, 0x0

    .line 469
    :goto_0
    if-nez v0, :cond_4

    .line 470
    new-instance v2, Lcom/quicinc/cneapiclient/CNEManagerFeatureUnsupportedException;

    invoke-direct {v2}, Lcom/quicinc/cneapiclient/CNEManagerFeatureUnsupportedException;-><init>()V

    throw v2

    .line 465
    :pswitch_0
    const/4 v4, 0x4

    if-eq v1, v4, :cond_0

    if-ne v1, v6, :cond_1

    :cond_0
    move v0, v3

    :goto_1
    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    .line 466
    :pswitch_1
    const/4 v4, 0x3

    if-eq v1, v4, :cond_2

    if-ne v1, v6, :cond_3

    :cond_2
    move v0, v3

    :goto_2
    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_2

    .line 472
    :cond_4
    return-void

    .line 464
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private checkPermissions()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/quicinc/cneapiclient/CNEManagerSecurityException;
        }
    .end annotation

    .prologue
    .line 452
    iget-object v1, p0, Lcom/quicinc/cneapiclient/CNEManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 453
    .local v0, "appInfo":Landroid/content/pm/ApplicationInfo;
    iget v1, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v1, v1, 0x1

    if-nez v1, :cond_0

    iget v1, v0, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit16 v1, v1, 0x80

    if-nez v1, :cond_0

    .line 455
    new-instance v1, Lcom/quicinc/cneapiclient/CNEManagerSecurityException;

    const-string v2, "Not a system app!!"

    invoke-direct {v1, v2}, Lcom/quicinc/cneapiclient/CNEManagerSecurityException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 457
    :cond_0
    return-void
.end method

.method private convertVersionToString(I)Ljava/lang/String;
    .locals 4
    .param p1, "intVersion"    # I

    .prologue
    .line 856
    div-int/lit8 v0, p1, 0x64

    .line 857
    .local v0, "major":I
    rem-int/lit8 v1, p1, 0x64

    .line 858
    .local v1, "minor":I
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method private getFeatureEnabled(Ljava/lang/reflect/Method;)Z
    .locals 7
    .param p1, "mGetMethod"    # Ljava/lang/reflect/Method;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/quicinc/cneapiclient/CNEManagerException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 347
    :try_start_0
    iget-object v5, p0, Lcom/quicinc/cneapiclient/CNEManager;->mCneService:Ljava/lang/Object;

    if-eqz v5, :cond_2

    if-eqz p1, :cond_2

    .line 348
    iget-object v5, p0, Lcom/quicinc/cneapiclient/CNEManager;->mCneService:Ljava/lang/Object;

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {p1, v5, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 349
    .local v2, "result":Ljava/lang/Object;
    instance-of v5, v2, Ljava/lang/Integer;

    if-eqz v5, :cond_1

    .line 350
    check-cast v2, Ljava/lang/Integer;

    .end local v2    # "result":Ljava/lang/Object;
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 351
    .local v3, "rv":I
    invoke-direct {p0, v3}, Lcom/quicinc/cneapiclient/CNEManager;->mapErrorToException(I)V

    .line 352
    const/4 v5, 0x2

    if-ne v3, v5, :cond_0

    .line 353
    const/4 v4, 0x1

    .line 367
    .end local v3    # "rv":I
    :cond_0
    :goto_0
    return v4

    .line 355
    .restart local v2    # "result":Ljava/lang/Object;
    :cond_1
    sget-object v5, Lcom/quicinc/cneapiclient/CNEManager;->TAG:Ljava/lang/String;

    const-string v6, "Return value of getFeatureEnabled method has unexpected type."

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lcom/quicinc/cneapiclient/CNEManagerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 361
    .end local v2    # "result":Ljava/lang/Object;
    :catch_0
    move-exception v0

    .line 362
    .local v0, "cne":Lcom/quicinc/cneapiclient/CNEManagerException;
    throw v0

    .line 359
    .end local v0    # "cne":Lcom/quicinc/cneapiclient/CNEManagerException;
    :cond_2
    :try_start_1
    sget-object v5, Lcom/quicinc/cneapiclient/CNEManager;->TAG:Ljava/lang/String;

    const-string v6, "CNE preference interface is not available."

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Lcom/quicinc/cneapiclient/CNEManagerException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 363
    :catch_1
    move-exception v1

    .line 364
    .local v1, "e":Ljava/lang/Exception;
    sget-object v4, Lcom/quicinc/cneapiclient/CNEManager;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Exception happened: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 365
    new-instance v4, Lcom/quicinc/cneapiclient/CNEManagerException;

    invoke-direct {v4}, Lcom/quicinc/cneapiclient/CNEManagerException;-><init>()V

    throw v4
.end method

.method private getIWLANStatus()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/quicinc/cneapiclient/CNEManagerException;
        }
    .end annotation

    .prologue
    .line 650
    invoke-direct {p0}, Lcom/quicinc/cneapiclient/CNEManager;->checkPermissions()V

    .line 651
    iget-object v0, p0, Lcom/quicinc/cneapiclient/CNEManager;->mIWLANGetMethod:Ljava/lang/reflect/Method;

    invoke-direct {p0, v0}, Lcom/quicinc/cneapiclient/CNEManager;->getFeatureEnabled(Ljava/lang/reflect/Method;)Z

    move-result v0

    return v0
.end method

.method private getNSRMFeatureEnabled(Ljava/lang/reflect/Method;Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureGetType;)Z
    .locals 8
    .param p1, "mGetMethod"    # Ljava/lang/reflect/Method;
    .param p2, "feature"    # Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureGetType;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/quicinc/cneapiclient/CNEManagerException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 374
    :try_start_0
    iget-object v6, p0, Lcom/quicinc/cneapiclient/CNEManager;->mCneService:Ljava/lang/Object;

    if-eqz v6, :cond_5

    if-eqz p1, :cond_5

    .line 375
    iget-object v6, p0, Lcom/quicinc/cneapiclient/CNEManager;->mCneService:Ljava/lang/Object;

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-virtual {p1, v6, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 376
    .local v2, "result":Ljava/lang/Object;
    instance-of v6, v2, Ljava/lang/Integer;

    if-eqz v6, :cond_4

    .line 377
    check-cast v2, Ljava/lang/Integer;

    .end local v2    # "result":Ljava/lang/Object;
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 378
    .local v3, "rv":I
    invoke-direct {p0, v3}, Lcom/quicinc/cneapiclient/CNEManager;->mapErrorToException(I)V

    .line 379
    sget-object v6, Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureSetType;->SYNC_CONNECT_DNS_WRITE:Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureSetType;

    # getter for: Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureSetType;->value:I
    invoke-static {v6}, Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureSetType;->access$000(Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureSetType;)I

    move-result v6

    if-ne v3, v6, :cond_1

    .line 380
    sget-object v6, Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureGetType;->SYNC_CONNECT_DNS_WRITE:Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureGetType;

    if-eq p2, v6, :cond_0

    sget-object v6, Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureGetType;->SYNC_CONNECT_DNS:Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureGetType;

    if-ne p2, v6, :cond_2

    .line 406
    :cond_0
    :goto_0
    return v4

    .line 384
    :cond_1
    sget-object v6, Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureSetType;->SYNC_CONNECT_DNS:Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureSetType;

    # getter for: Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureSetType;->value:I
    invoke-static {v6}, Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureSetType;->access$000(Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureSetType;)I

    move-result v6

    if-ne v3, v6, :cond_3

    .line 385
    sget-object v6, Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureGetType;->SYNC_CONNECT_DNS:Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureGetType;

    if-eq p2, v6, :cond_0

    :cond_2
    move v4, v5

    .line 406
    goto :goto_0

    :cond_3
    move v4, v5

    .line 389
    goto :goto_0

    .line 392
    .end local v3    # "rv":I
    .restart local v2    # "result":Ljava/lang/Object;
    :cond_4
    sget-object v4, Lcom/quicinc/cneapiclient/CNEManager;->TAG:Ljava/lang/String;

    const-string v5, "Return value of getNSRMFeatureEnabled method has unexpected type."

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 394
    new-instance v4, Lcom/quicinc/cneapiclient/CNEManagerException;

    invoke-direct {v4}, Lcom/quicinc/cneapiclient/CNEManagerException;-><init>()V

    throw v4
    :try_end_0
    .catch Lcom/quicinc/cneapiclient/CNEManagerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 400
    .end local v2    # "result":Ljava/lang/Object;
    :catch_0
    move-exception v0

    .line 401
    .local v0, "cne":Lcom/quicinc/cneapiclient/CNEManagerException;
    throw v0

    .line 397
    .end local v0    # "cne":Lcom/quicinc/cneapiclient/CNEManagerException;
    :cond_5
    :try_start_1
    sget-object v4, Lcom/quicinc/cneapiclient/CNEManager;->TAG:Ljava/lang/String;

    const-string v5, "CNE preference interface is not available."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 398
    new-instance v4, Lcom/quicinc/cneapiclient/CNEManagerException;

    invoke-direct {v4}, Lcom/quicinc/cneapiclient/CNEManagerException;-><init>()V

    throw v4
    :try_end_1
    .catch Lcom/quicinc/cneapiclient/CNEManagerException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 402
    :catch_1
    move-exception v1

    .line 403
    .local v1, "e":Ljava/lang/Exception;
    sget-object v4, Lcom/quicinc/cneapiclient/CNEManager;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Exception happened: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 404
    new-instance v4, Lcom/quicinc/cneapiclient/CNEManagerException;

    invoke-direct {v4}, Lcom/quicinc/cneapiclient/CNEManagerException;-><init>()V

    throw v4
.end method

.method private loadCNEService()V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 300
    const-string v5, "cneservice"

    invoke-static {v5}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v2

    .line 301
    .local v2, "cneService":Landroid/os/IBinder;
    if-eqz v2, :cond_4

    .line 303
    :try_start_0
    new-instance v1, Ldalvik/system/PathClassLoader;

    const-string v5, "/system/framework/com.quicinc.cne.jar"

    invoke-static {}, Ljava/lang/ClassLoader;->getSystemClassLoader()Ljava/lang/ClassLoader;

    move-result-object v6

    invoke-direct {v1, v5, v6}, Ldalvik/system/PathClassLoader;-><init>(Ljava/lang/String;Ljava/lang/ClassLoader;)V

    .line 306
    .local v1, "cneClassLoader":Ldalvik/system/PathClassLoader;
    const-string v5, "com.quicinc.cne.ICNEManager"

    invoke-virtual {v1, v5}, Ldalvik/system/PathClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v5

    iput-object v5, p0, Lcom/quicinc/cneapiclient/CNEManager;->mCneManagerClass:Ljava/lang/Class;

    .line 308
    iget-object v5, p0, Lcom/quicinc/cneapiclient/CNEManager;->mCneManagerClass:Ljava/lang/Class;

    if-nez v5, :cond_1

    .line 309
    sget-object v5, Lcom/quicinc/cneapiclient/CNEManager;->TAG:Ljava/lang/String;

    const-string v6, "com.quicinc.cne.ICNEManager class is not found in cne library."

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 341
    .end local v1    # "cneClassLoader":Ldalvik/system/PathClassLoader;
    :cond_0
    :goto_0
    return-void

    .line 314
    .restart local v1    # "cneClassLoader":Ldalvik/system/PathClassLoader;
    :cond_1
    const-string v5, "com.quicinc.cne.ICNEManager$Stub"

    invoke-virtual {v1, v5}, Ldalvik/system/PathClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    .line 316
    .local v3, "cneStubClass":Ljava/lang/Class;
    if-nez v3, :cond_2

    .line 317
    sget-object v5, Lcom/quicinc/cneapiclient/CNEManager;->TAG:Ljava/lang/String;

    const-string v6, "ICNEManager$Stub class is not found in cne library."

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 334
    .end local v1    # "cneClassLoader":Ldalvik/system/PathClassLoader;
    .end local v3    # "cneStubClass":Ljava/lang/Class;
    :catch_0
    move-exception v4

    .line 335
    .local v4, "e":Ljava/lang/Exception;
    sget-object v5, Lcom/quicinc/cneapiclient/CNEManager;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Exception happened: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v4}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 336
    throw v4

    .line 321
    .end local v4    # "e":Ljava/lang/Exception;
    .restart local v1    # "cneClassLoader":Ldalvik/system/PathClassLoader;
    .restart local v3    # "cneStubClass":Ljava/lang/Class;
    :cond_2
    :try_start_1
    const-string v5, "asInterface"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v7, 0x0

    const-class v8, Landroid/os/IBinder;

    aput-object v8, v6, v7

    invoke-virtual {v3, v5, v6}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 324
    .local v0, "asInterfaceMethod":Ljava/lang/reflect/Method;
    if-nez v0, :cond_3

    .line 325
    sget-object v5, Lcom/quicinc/cneapiclient/CNEManager;->TAG:Ljava/lang/String;

    const-string v6, "Method asInterface is not fount in ICNEManager$Stub class."

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 329
    :cond_3
    const/4 v5, 0x0

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v2, v6, v7

    invoke-virtual {v0, v5, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    iput-object v5, p0, Lcom/quicinc/cneapiclient/CNEManager;->mCneService:Ljava/lang/Object;

    .line 330
    iget-object v5, p0, Lcom/quicinc/cneapiclient/CNEManager;->mCneService:Ljava/lang/Object;

    if-nez v5, :cond_0

    .line 331
    sget-object v5, Lcom/quicinc/cneapiclient/CNEManager;->TAG:Ljava/lang/String;

    const-string v6, "Failed to get CNEService as interface."

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 339
    .end local v0    # "asInterfaceMethod":Ljava/lang/reflect/Method;
    .end local v1    # "cneClassLoader":Ldalvik/system/PathClassLoader;
    .end local v3    # "cneStubClass":Ljava/lang/Class;
    :cond_4
    sget-object v5, Lcom/quicinc/cneapiclient/CNEManager;->TAG:Ljava/lang/String;

    const-string v6, "CNE service is not found."

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private loadMethods()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 225
    :try_start_0
    iget-object v2, p0, Lcom/quicinc/cneapiclient/CNEManager;->mCneManagerClass:Ljava/lang/Class;

    if-eqz v2, :cond_8

    .line 226
    iget-object v2, p0, Lcom/quicinc/cneapiclient/CNEManager;->mCneManagerClass:Ljava/lang/Class;

    const-string v3, "setWQEEnabled"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    sget-object v6, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    iput-object v2, p0, Lcom/quicinc/cneapiclient/CNEManager;->mWQESetMethod:Ljava/lang/reflect/Method;

    .line 228
    iget-object v2, p0, Lcom/quicinc/cneapiclient/CNEManager;->mWQESetMethod:Ljava/lang/reflect/Method;

    if-nez v2, :cond_1

    .line 229
    sget-object v2, Lcom/quicinc/cneapiclient/CNEManager;->TAG:Ljava/lang/String;

    const-string v3, "Cne preference interface is not accessible."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 291
    :cond_0
    :goto_0
    return-void

    .line 233
    :cond_1
    iget-object v2, p0, Lcom/quicinc/cneapiclient/CNEManager;->mCneManagerClass:Ljava/lang/Class;

    const-string v3, "getWQEEnabled"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    iput-object v2, p0, Lcom/quicinc/cneapiclient/CNEManager;->mWQEGetMethod:Ljava/lang/reflect/Method;

    .line 235
    iget-object v2, p0, Lcom/quicinc/cneapiclient/CNEManager;->mWQEGetMethod:Ljava/lang/reflect/Method;

    if-nez v2, :cond_2

    .line 236
    sget-object v2, Lcom/quicinc/cneapiclient/CNEManager;->TAG:Ljava/lang/String;

    const-string v3, "Cne preference interface is not accessible."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 287
    :catch_0
    move-exception v1

    .line 288
    .local v1, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/quicinc/cneapiclient/CNEManager;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Exception happened: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 289
    throw v1

    .line 240
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_2
    :try_start_1
    iget-object v2, p0, Lcom/quicinc/cneapiclient/CNEManager;->mCneManagerClass:Ljava/lang/Class;

    const-string v3, "setIWLANEnabled"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    sget-object v6, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    iput-object v2, p0, Lcom/quicinc/cneapiclient/CNEManager;->mIWLANSetMethod:Ljava/lang/reflect/Method;

    .line 242
    iget-object v2, p0, Lcom/quicinc/cneapiclient/CNEManager;->mIWLANSetMethod:Ljava/lang/reflect/Method;

    if-nez v2, :cond_3

    .line 243
    sget-object v2, Lcom/quicinc/cneapiclient/CNEManager;->TAG:Ljava/lang/String;

    const-string v3, "Cne preference interface is not accessible."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 247
    :cond_3
    iget-object v2, p0, Lcom/quicinc/cneapiclient/CNEManager;->mCneManagerClass:Ljava/lang/Class;

    const-string v3, "getIWLANEnabled"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    iput-object v2, p0, Lcom/quicinc/cneapiclient/CNEManager;->mIWLANGetMethod:Ljava/lang/reflect/Method;

    .line 249
    iget-object v2, p0, Lcom/quicinc/cneapiclient/CNEManager;->mWQEGetMethod:Ljava/lang/reflect/Method;

    if-nez v2, :cond_4

    .line 250
    sget-object v2, Lcom/quicinc/cneapiclient/CNEManager;->TAG:Ljava/lang/String;

    const-string v3, "Cne preference interface is not accessible."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 254
    :cond_4
    iget-object v2, p0, Lcom/quicinc/cneapiclient/CNEManager;->mCneManagerClass:Ljava/lang/Class;

    const-string v3, "setNSRMEnabled"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    iput-object v2, p0, Lcom/quicinc/cneapiclient/CNEManager;->mNSRMSetMethod:Ljava/lang/reflect/Method;

    .line 256
    iget-object v2, p0, Lcom/quicinc/cneapiclient/CNEManager;->mNSRMSetMethod:Ljava/lang/reflect/Method;

    if-nez v2, :cond_5

    .line 257
    sget-object v2, Lcom/quicinc/cneapiclient/CNEManager;->TAG:Ljava/lang/String;

    const-string v3, "Cne preference interface is not accessible."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 261
    :cond_5
    iget-object v2, p0, Lcom/quicinc/cneapiclient/CNEManager;->mCneManagerClass:Ljava/lang/Class;

    const-string v3, "getNSRMEnabled"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    iput-object v2, p0, Lcom/quicinc/cneapiclient/CNEManager;->mNSRMGetMethod:Ljava/lang/reflect/Method;

    .line 263
    iget-object v2, p0, Lcom/quicinc/cneapiclient/CNEManager;->mNSRMGetMethod:Ljava/lang/reflect/Method;

    if-nez v2, :cond_6

    .line 264
    sget-object v2, Lcom/quicinc/cneapiclient/CNEManager;->TAG:Ljava/lang/String;

    const-string v3, "Cne preference interface is not accessible."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 268
    :cond_6
    const/4 v2, 0x2

    new-array v0, v2, [Ljava/lang/Class;

    .line 269
    .local v0, "args":[Ljava/lang/Class;
    const/4 v2, 0x0

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v3, v0, v2

    .line 270
    const/4 v2, 0x1

    const-class v3, Ljava/lang/String;

    aput-object v3, v0, v2

    .line 271
    iget-object v2, p0, Lcom/quicinc/cneapiclient/CNEManager;->mCneManagerClass:Ljava/lang/Class;

    const-string v3, "updatePolicy"

    invoke-virtual {v2, v3, v0}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    iput-object v2, p0, Lcom/quicinc/cneapiclient/CNEManager;->mUpdatePolMethod:Ljava/lang/reflect/Method;

    .line 273
    iget-object v2, p0, Lcom/quicinc/cneapiclient/CNEManager;->mUpdatePolMethod:Ljava/lang/reflect/Method;

    if-nez v2, :cond_7

    .line 274
    sget-object v2, Lcom/quicinc/cneapiclient/CNEManager;->TAG:Ljava/lang/String;

    const-string v3, "Cne UpdatePolicy interface is not accessible."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 278
    :cond_7
    iget-object v2, p0, Lcom/quicinc/cneapiclient/CNEManager;->mCneManagerClass:Ljava/lang/Class;

    const-string v3, "getPolicyVersion"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    iput-object v2, p0, Lcom/quicinc/cneapiclient/CNEManager;->mGetPolVersionMethod:Ljava/lang/reflect/Method;

    .line 280
    iget-object v2, p0, Lcom/quicinc/cneapiclient/CNEManager;->mGetPolVersionMethod:Ljava/lang/reflect/Method;

    if-nez v2, :cond_0

    .line 281
    sget-object v2, Lcom/quicinc/cneapiclient/CNEManager;->TAG:Ljava/lang/String;

    const-string v3, "Cne GetPolVersion interface is not accessible."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 285
    .end local v0    # "args":[Ljava/lang/Class;
    :cond_8
    sget-object v2, Lcom/quicinc/cneapiclient/CNEManager;->TAG:Ljava/lang/String;

    const-string v3, "Cne preference interface is not accessible."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method private mapErrorToException(I)V
    .locals 2
    .param p1, "err"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/quicinc/cneapiclient/CNEManagerException;
        }
    .end annotation

    .prologue
    .line 862
    sparse-switch p1, :sswitch_data_0

    .line 884
    :sswitch_0
    return-void

    .line 866
    :sswitch_1
    new-instance v0, Lcom/quicinc/cneapiclient/CNEManagerInvalidArgumentException;

    invoke-direct {v0}, Lcom/quicinc/cneapiclient/CNEManagerInvalidArgumentException;-><init>()V

    throw v0

    .line 868
    :sswitch_2
    new-instance v0, Lcom/quicinc/cneapiclient/CNEManagerBusyException;

    invoke-direct {v0}, Lcom/quicinc/cneapiclient/CNEManagerBusyException;-><init>()V

    throw v0

    .line 870
    :sswitch_3
    new-instance v0, Lcom/quicinc/cneapiclient/CNEManagerException;

    invoke-direct {v0}, Lcom/quicinc/cneapiclient/CNEManagerException;-><init>()V

    throw v0

    .line 872
    :sswitch_4
    new-instance v0, Lcom/quicinc/cneapiclient/CNEManagerFeatureUnsupportedException;

    invoke-direct {v0}, Lcom/quicinc/cneapiclient/CNEManagerFeatureUnsupportedException;-><init>()V

    throw v0

    .line 874
    :sswitch_5
    new-instance v0, Lcom/quicinc/cneapiclient/CNEManagerInvalidArgumentException;

    const-string v1, "File Size is too large"

    invoke-direct {v0, v1}, Lcom/quicinc/cneapiclient/CNEManagerInvalidArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 876
    :sswitch_6
    new-instance v0, Lcom/quicinc/cneapiclient/CNEManagerInvalidArgumentException;

    const-string v1, "File Path access deined"

    invoke-direct {v0, v1}, Lcom/quicinc/cneapiclient/CNEManagerInvalidArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 878
    :sswitch_7
    new-instance v0, Lcom/quicinc/cneapiclient/CNEManagerInvalidArgumentException;

    const-string v1, "File Path is too long"

    invoke-direct {v0, v1}, Lcom/quicinc/cneapiclient/CNEManagerInvalidArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 880
    :sswitch_8
    new-instance v0, Lcom/quicinc/cneapiclient/CNEManagerFeatureUnsupportedException;

    const-string v1, "Supported version not found"

    invoke-direct {v0, v1}, Lcom/quicinc/cneapiclient/CNEManagerFeatureUnsupportedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 862
    :sswitch_data_0
    .sparse-switch
        -0x8 -> :sswitch_8
        -0x7 -> :sswitch_7
        -0x6 -> :sswitch_6
        -0x5 -> :sswitch_5
        -0x4 -> :sswitch_4
        -0x3 -> :sswitch_1
        -0x2 -> :sswitch_2
        -0x1 -> :sswitch_3
        0x3e8 -> :sswitch_0
    .end sparse-switch
.end method

.method private setFeatureEnabled(Ljava/lang/Object;Ljava/lang/reflect/Method;)V
    .locals 7
    .param p1, "value"    # Ljava/lang/Object;
    .param p2, "mSetMethod"    # Ljava/lang/reflect/Method;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/quicinc/cneapiclient/CNEManagerException;
        }
    .end annotation

    .prologue
    .line 428
    :try_start_0
    iget-object v4, p0, Lcom/quicinc/cneapiclient/CNEManager;->mCneService:Ljava/lang/Object;

    if-eqz v4, :cond_3

    if-eqz p2, :cond_3

    .line 429
    instance-of v4, p1, Ljava/lang/Integer;

    if-nez v4, :cond_0

    instance-of v4, p1, Ljava/lang/Boolean;

    if-eqz v4, :cond_2

    .line 430
    :cond_0
    iget-object v4, p0, Lcom/quicinc/cneapiclient/CNEManager;->mCneService:Ljava/lang/Object;

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    invoke-virtual {p2, v4, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 431
    .local v2, "result":Ljava/lang/Object;
    instance-of v4, v2, Ljava/lang/Integer;

    if-eqz v4, :cond_1

    .line 432
    check-cast v2, Ljava/lang/Integer;

    .end local v2    # "result":Ljava/lang/Object;
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 433
    .local v3, "rv":I
    invoke-direct {p0, v3}, Lcom/quicinc/cneapiclient/CNEManager;->mapErrorToException(I)V

    .line 449
    .end local v3    # "rv":I
    :goto_0
    return-void

    .line 435
    .restart local v2    # "result":Ljava/lang/Object;
    :cond_1
    sget-object v4, Lcom/quicinc/cneapiclient/CNEManager;->TAG:Ljava/lang/String;

    const-string v5, "Return value of get method has unexpected type."

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lcom/quicinc/cneapiclient/CNEManagerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 443
    .end local v2    # "result":Ljava/lang/Object;
    :catch_0
    move-exception v0

    .line 444
    .local v0, "cne":Lcom/quicinc/cneapiclient/CNEManagerException;
    throw v0

    .line 438
    .end local v0    # "cne":Lcom/quicinc/cneapiclient/CNEManagerException;
    :cond_2
    :try_start_1
    sget-object v4, Lcom/quicinc/cneapiclient/CNEManager;->TAG:Ljava/lang/String;

    const-string v5, "Passing non-boolean non-integer parameter to setFeatureEnabled."

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Lcom/quicinc/cneapiclient/CNEManagerException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 445
    :catch_1
    move-exception v1

    .line 446
    .local v1, "e":Ljava/lang/Exception;
    sget-object v4, Lcom/quicinc/cneapiclient/CNEManager;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Exception happened: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 447
    new-instance v4, Lcom/quicinc/cneapiclient/CNEManagerException;

    invoke-direct {v4}, Lcom/quicinc/cneapiclient/CNEManagerException;-><init>()V

    throw v4

    .line 441
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_3
    :try_start_2
    sget-object v4, Lcom/quicinc/cneapiclient/CNEManager;->TAG:Ljava/lang/String;

    const-string v5, "CNE preference interface is not available."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Lcom/quicinc/cneapiclient/CNEManagerException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0
.end method

.method private setIWLANStatus(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/quicinc/cneapiclient/CNEManagerException;
        }
    .end annotation

    .prologue
    .line 679
    invoke-direct {p0}, Lcom/quicinc/cneapiclient/CNEManager;->checkPermissions()V

    .line 680
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iget-object v1, p0, Lcom/quicinc/cneapiclient/CNEManager;->mIWLANSetMethod:Ljava/lang/reflect/Method;

    invoke-direct {p0, v0, v1}, Lcom/quicinc/cneapiclient/CNEManager;->setFeatureEnabled(Ljava/lang/Object;Ljava/lang/reflect/Method;)V

    .line 681
    return-void
.end method

.method private setNSRMFeatureEnabled(Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureSetType;Ljava/lang/reflect/Method;)V
    .locals 5
    .param p1, "feature"    # Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureSetType;
    .param p2, "mSetMethod"    # Ljava/lang/reflect/Method;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/quicinc/cneapiclient/CNEManagerException;
        }
    .end annotation

    .prologue
    .line 413
    :try_start_0
    iget-object v1, p0, Lcom/quicinc/cneapiclient/CNEManager;->mCneService:Ljava/lang/Object;

    if-eqz v1, :cond_0

    if-eqz p2, :cond_0

    .line 414
    iget-object v1, p0, Lcom/quicinc/cneapiclient/CNEManager;->mCneService:Ljava/lang/Object;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    # getter for: Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureSetType;->value:I
    invoke-static {p1}, Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureSetType;->access$000(Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureSetType;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p2, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 422
    :goto_0
    return-void

    .line 416
    :cond_0
    sget-object v1, Lcom/quicinc/cneapiclient/CNEManager;->TAG:Ljava/lang/String;

    const-string v2, "CNE preference interface is not available."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 418
    :catch_0
    move-exception v0

    .line 419
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/quicinc/cneapiclient/CNEManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception happened: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 420
    new-instance v1, Lcom/quicinc/cneapiclient/CNEManagerException;

    invoke-direct {v1}, Lcom/quicinc/cneapiclient/CNEManagerException;-><init>()V

    throw v1
.end method


# virtual methods
.method public getIWLANEnabled()Z
    .locals 2

    .prologue
    .line 641
    :try_start_0
    invoke-direct {p0}, Lcom/quicinc/cneapiclient/CNEManager;->getIWLANStatus()Z
    :try_end_0
    .catch Lcom/quicinc/cneapiclient/CNEManagerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 644
    :goto_0
    return v1

    .line 642
    :catch_0
    move-exception v0

    .line 643
    .local v0, "e":Lcom/quicinc/cneapiclient/CNEManagerException;
    invoke-virtual {v0}, Lcom/quicinc/cneapiclient/CNEManagerException;->printStackTrace()V

    .line 644
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getNSRMEnabled(Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureGetType;)Z
    .locals 1
    .param p1, "feature"    # Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureGetType;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/quicinc/cneapiclient/CNEManagerException;
        }
    .end annotation

    .prologue
    .line 507
    invoke-direct {p0}, Lcom/quicinc/cneapiclient/CNEManager;->checkPermissions()V

    .line 508
    sget-object v0, Lcom/quicinc/cneapiclient/CNEManager$FeatureType;->NSRM:Lcom/quicinc/cneapiclient/CNEManager$FeatureType;

    invoke-direct {p0, v0}, Lcom/quicinc/cneapiclient/CNEManager;->checkFeatureEnabled(Lcom/quicinc/cneapiclient/CNEManager$FeatureType;)V

    .line 509
    iget-object v0, p0, Lcom/quicinc/cneapiclient/CNEManager;->mNSRMGetMethod:Ljava/lang/reflect/Method;

    invoke-direct {p0, v0, p1}, Lcom/quicinc/cneapiclient/CNEManager;->getNSRMFeatureEnabled(Ljava/lang/reflect/Method;Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureGetType;)Z

    move-result v0

    return v0
.end method

.method public getPolicyVersion(I)Ljava/lang/String;
    .locals 8
    .param p1, "policyType"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/quicinc/cneapiclient/CNEManagerException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 827
    const/4 v2, -0x1

    .line 828
    .local v2, "retVal":I
    invoke-direct {p0}, Lcom/quicinc/cneapiclient/CNEManager;->checkPermissions()V

    .line 829
    if-ne p1, v3, :cond_0

    sget-object v3, Lcom/quicinc/cneapiclient/CNEManager$FeatureType;->WQE:Lcom/quicinc/cneapiclient/CNEManager$FeatureType;

    invoke-direct {p0, v3}, Lcom/quicinc/cneapiclient/CNEManager;->checkFeatureEnabled(Lcom/quicinc/cneapiclient/CNEManager$FeatureType;)V

    .line 830
    :cond_0
    const/4 v3, 0x2

    if-ne p1, v3, :cond_1

    sget-object v3, Lcom/quicinc/cneapiclient/CNEManager$FeatureType;->NSRM:Lcom/quicinc/cneapiclient/CNEManager$FeatureType;

    invoke-direct {p0, v3}, Lcom/quicinc/cneapiclient/CNEManager;->checkFeatureEnabled(Lcom/quicinc/cneapiclient/CNEManager$FeatureType;)V

    .line 832
    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/quicinc/cneapiclient/CNEManager;->mCneService:Ljava/lang/Object;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/quicinc/cneapiclient/CNEManager;->mGetPolVersionMethod:Ljava/lang/reflect/Method;

    if-eqz v3, :cond_3

    .line 833
    iget-object v3, p0, Lcom/quicinc/cneapiclient/CNEManager;->mGetPolVersionMethod:Ljava/lang/reflect/Method;

    iget-object v4, p0, Lcom/quicinc/cneapiclient/CNEManager;->mCneService:Ljava/lang/Object;

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 834
    .local v1, "result":Ljava/lang/Object;
    instance-of v3, v1, Ljava/lang/Integer;

    if-eqz v3, :cond_2

    .line 835
    check-cast v1, Ljava/lang/Integer;

    .end local v1    # "result":Ljava/lang/Object;
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v2

    .line 848
    :goto_0
    const/4 v3, -0x1

    if-gt v2, v3, :cond_4

    .line 849
    invoke-direct {p0, v2}, Lcom/quicinc/cneapiclient/CNEManager;->mapErrorToException(I)V

    .line 850
    const-string v3, "unknown"

    .line 852
    :goto_1
    return-object v3

    .line 837
    .restart local v1    # "result":Ljava/lang/Object;
    :cond_2
    :try_start_1
    sget-object v3, Lcom/quicinc/cneapiclient/CNEManager;->TAG:Ljava/lang/String;

    const-string v4, "Return value of getPolicyVersion method has unexpected type."

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 843
    .end local v1    # "result":Ljava/lang/Object;
    :catch_0
    move-exception v0

    .line 844
    .local v0, "e":Ljava/lang/IllegalAccessException;
    sget-object v3, Lcom/quicinc/cneapiclient/CNEManager;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "IllegalAccessException happened: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 841
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :cond_3
    :try_start_2
    sget-object v3, Lcom/quicinc/cneapiclient/CNEManager;->TAG:Ljava/lang/String;

    const-string v4, "CNE getPolicyVersion interface is not available."

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 845
    :catch_1
    move-exception v0

    .line 846
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    sget-object v3, Lcom/quicinc/cneapiclient/CNEManager;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "InvocationTargetException happened: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 852
    .end local v0    # "e":Ljava/lang/reflect/InvocationTargetException;
    :cond_4
    invoke-direct {p0, v2}, Lcom/quicinc/cneapiclient/CNEManager;->convertVersionToString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_1
.end method

.method public getWQEEnabled()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/quicinc/cneapiclient/CNEManagerException;
        }
    .end annotation

    .prologue
    .line 581
    invoke-direct {p0}, Lcom/quicinc/cneapiclient/CNEManager;->checkPermissions()V

    .line 582
    sget-object v0, Lcom/quicinc/cneapiclient/CNEManager$FeatureType;->WQE:Lcom/quicinc/cneapiclient/CNEManager$FeatureType;

    invoke-direct {p0, v0}, Lcom/quicinc/cneapiclient/CNEManager;->checkFeatureEnabled(Lcom/quicinc/cneapiclient/CNEManager$FeatureType;)V

    .line 583
    iget-object v0, p0, Lcom/quicinc/cneapiclient/CNEManager;->mWQEGetMethod:Ljava/lang/reflect/Method;

    invoke-direct {p0, v0}, Lcom/quicinc/cneapiclient/CNEManager;->getFeatureEnabled(Ljava/lang/reflect/Method;)Z

    move-result v0

    return v0
.end method

.method public setIWLANEnabled(Z)V
    .locals 1
    .param p1, "value"    # Z

    .prologue
    .line 670
    :try_start_0
    invoke-direct {p0, p1}, Lcom/quicinc/cneapiclient/CNEManager;->setIWLANStatus(Z)V
    :try_end_0
    .catch Lcom/quicinc/cneapiclient/CNEManagerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 674
    :goto_0
    return-void

    .line 671
    :catch_0
    move-exception v0

    .line 672
    .local v0, "e":Lcom/quicinc/cneapiclient/CNEManagerException;
    invoke-virtual {v0}, Lcom/quicinc/cneapiclient/CNEManagerException;->printStackTrace()V

    goto :goto_0
.end method

.method public setNSRMEnabled(Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureSetType;)V
    .locals 1
    .param p1, "value"    # Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureSetType;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/quicinc/cneapiclient/CNEManagerException;
        }
    .end annotation

    .prologue
    .line 550
    invoke-direct {p0}, Lcom/quicinc/cneapiclient/CNEManager;->checkPermissions()V

    .line 551
    sget-object v0, Lcom/quicinc/cneapiclient/CNEManager$FeatureType;->NSRM:Lcom/quicinc/cneapiclient/CNEManager$FeatureType;

    invoke-direct {p0, v0}, Lcom/quicinc/cneapiclient/CNEManager;->checkFeatureEnabled(Lcom/quicinc/cneapiclient/CNEManager$FeatureType;)V

    .line 552
    iget-object v0, p0, Lcom/quicinc/cneapiclient/CNEManager;->mNSRMSetMethod:Ljava/lang/reflect/Method;

    invoke-direct {p0, p1, v0}, Lcom/quicinc/cneapiclient/CNEManager;->setNSRMFeatureEnabled(Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureSetType;Ljava/lang/reflect/Method;)V

    .line 553
    return-void
.end method

.method public setWQEEnabled(Z)V
    .locals 2
    .param p1, "value"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/quicinc/cneapiclient/CNEManagerException;
        }
    .end annotation

    .prologue
    .line 623
    invoke-direct {p0}, Lcom/quicinc/cneapiclient/CNEManager;->checkPermissions()V

    .line 624
    sget-object v0, Lcom/quicinc/cneapiclient/CNEManager$FeatureType;->WQE:Lcom/quicinc/cneapiclient/CNEManager$FeatureType;

    invoke-direct {p0, v0}, Lcom/quicinc/cneapiclient/CNEManager;->checkFeatureEnabled(Lcom/quicinc/cneapiclient/CNEManager$FeatureType;)V

    .line 625
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iget-object v1, p0, Lcom/quicinc/cneapiclient/CNEManager;->mWQESetMethod:Ljava/lang/reflect/Method;

    invoke-direct {p0, v0, v1}, Lcom/quicinc/cneapiclient/CNEManager;->setFeatureEnabled(Ljava/lang/Object;Ljava/lang/reflect/Method;)V

    .line 626
    return-void
.end method

.method public updateOperatorPolicy(Ljava/lang/String;)Z
    .locals 3
    .param p1, "filepath"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    .line 711
    const/4 v2, 0x1

    :try_start_0
    invoke-virtual {p0, v2, p1}, Lcom/quicinc/cneapiclient/CNEManager;->updatePolicy(ILjava/lang/String;)V
    :try_end_0
    .catch Lcom/quicinc/cneapiclient/CNEManagerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 716
    :goto_0
    return v1

    .line 712
    :catch_0
    move-exception v0

    .line 713
    .local v0, "e":Lcom/quicinc/cneapiclient/CNEManagerException;
    invoke-virtual {v0}, Lcom/quicinc/cneapiclient/CNEManagerException;->printStackTrace()V

    .line 714
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public updatePolicy(ILjava/lang/String;)V
    .locals 8
    .param p1, "policyType"    # I
    .param p2, "filepath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/quicinc/cneapiclient/CNEManagerException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 768
    const/4 v2, -0x1

    .line 769
    .local v2, "retVal":I
    invoke-direct {p0}, Lcom/quicinc/cneapiclient/CNEManager;->checkPermissions()V

    .line 770
    if-ne p1, v3, :cond_0

    sget-object v3, Lcom/quicinc/cneapiclient/CNEManager$FeatureType;->WQE:Lcom/quicinc/cneapiclient/CNEManager$FeatureType;

    invoke-direct {p0, v3}, Lcom/quicinc/cneapiclient/CNEManager;->checkFeatureEnabled(Lcom/quicinc/cneapiclient/CNEManager$FeatureType;)V

    .line 771
    :cond_0
    if-ne p1, v4, :cond_1

    sget-object v3, Lcom/quicinc/cneapiclient/CNEManager$FeatureType;->NSRM:Lcom/quicinc/cneapiclient/CNEManager$FeatureType;

    invoke-direct {p0, v3}, Lcom/quicinc/cneapiclient/CNEManager;->checkFeatureEnabled(Lcom/quicinc/cneapiclient/CNEManager$FeatureType;)V

    .line 772
    :cond_1
    sget-object v3, Lcom/quicinc/cneapiclient/CNEManager;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Trying to update policy "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " at "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 774
    :try_start_0
    iget-object v3, p0, Lcom/quicinc/cneapiclient/CNEManager;->mCneService:Ljava/lang/Object;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/quicinc/cneapiclient/CNEManager;->mUpdatePolMethod:Ljava/lang/reflect/Method;

    if-eqz v3, :cond_3

    .line 775
    iget-object v3, p0, Lcom/quicinc/cneapiclient/CNEManager;->mUpdatePolMethod:Ljava/lang/reflect/Method;

    iget-object v4, p0, Lcom/quicinc/cneapiclient/CNEManager;->mCneService:Ljava/lang/Object;

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    aput-object p2, v5, v6

    invoke-virtual {v3, v4, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 776
    .local v1, "result":Ljava/lang/Object;
    instance-of v3, v1, Ljava/lang/Integer;

    if-eqz v3, :cond_2

    .line 777
    check-cast v1, Ljava/lang/Integer;

    .end local v1    # "result":Ljava/lang/Object;
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v2

    .line 790
    :goto_0
    invoke-direct {p0, v2}, Lcom/quicinc/cneapiclient/CNEManager;->mapErrorToException(I)V

    .line 791
    return-void

    .line 779
    .restart local v1    # "result":Ljava/lang/Object;
    :cond_2
    :try_start_1
    sget-object v3, Lcom/quicinc/cneapiclient/CNEManager;->TAG:Ljava/lang/String;

    const-string v4, "Return value of updatePolicy method has unexpected type."

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 785
    .end local v1    # "result":Ljava/lang/Object;
    :catch_0
    move-exception v0

    .line 786
    .local v0, "e":Ljava/lang/IllegalAccessException;
    sget-object v3, Lcom/quicinc/cneapiclient/CNEManager;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "IllegalAccessException happened: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 783
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :cond_3
    :try_start_2
    sget-object v3, Lcom/quicinc/cneapiclient/CNEManager;->TAG:Ljava/lang/String;

    const-string v4, "CNE UpdatePolicy interface is not available."

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 787
    :catch_1
    move-exception v0

    .line 788
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    sget-object v3, Lcom/quicinc/cneapiclient/CNEManager;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "InvocationTargetException happened: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
