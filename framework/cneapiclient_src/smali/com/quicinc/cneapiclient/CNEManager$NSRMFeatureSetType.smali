.class public final enum Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureSetType;
.super Ljava/lang/Enum;
.source "CNEManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/quicinc/cneapiclient/CNEManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "NSRMFeatureSetType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureSetType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureSetType;

.field public static final enum OFF:Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureSetType;

.field public static final enum SYNC_CONNECT_DNS:Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureSetType;

.field public static final enum SYNC_CONNECT_DNS_WRITE:Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureSetType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 141
    new-instance v0, Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureSetType;

    const-string v1, "OFF"

    invoke-direct {v0, v1, v4, v2}, Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureSetType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureSetType;->OFF:Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureSetType;

    .line 142
    new-instance v0, Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureSetType;

    const-string v1, "SYNC_CONNECT_DNS"

    invoke-direct {v0, v1, v2, v3}, Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureSetType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureSetType;->SYNC_CONNECT_DNS:Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureSetType;

    .line 144
    new-instance v0, Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureSetType;

    const-string v1, "SYNC_CONNECT_DNS_WRITE"

    invoke-direct {v0, v1, v3, v5}, Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureSetType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureSetType;->SYNC_CONNECT_DNS_WRITE:Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureSetType;

    .line 140
    new-array v0, v5, [Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureSetType;

    sget-object v1, Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureSetType;->OFF:Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureSetType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureSetType;->SYNC_CONNECT_DNS:Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureSetType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureSetType;->SYNC_CONNECT_DNS_WRITE:Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureSetType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureSetType;->$VALUES:[Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureSetType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 149
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 150
    iput p3, p0, Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureSetType;->value:I

    .line 151
    return-void
.end method

.method static synthetic access$000(Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureSetType;)I
    .locals 1
    .param p0, "x0"    # Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureSetType;

    .prologue
    .line 140
    iget v0, p0, Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureSetType;->value:I

    return v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureSetType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 140
    const-class v0, Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureSetType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureSetType;

    return-object v0
.end method

.method public static values()[Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureSetType;
    .locals 1

    .prologue
    .line 140
    sget-object v0, Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureSetType;->$VALUES:[Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureSetType;

    invoke-virtual {v0}, [Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureSetType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureSetType;

    return-object v0
.end method


# virtual methods
.method public value()I
    .locals 1

    .prologue
    .line 153
    iget v0, p0, Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureSetType;->value:I

    return v0
.end method
