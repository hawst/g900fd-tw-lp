.class public final enum Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureGetType;
.super Ljava/lang/Enum;
.source "CNEManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/quicinc/cneapiclient/CNEManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "NSRMFeatureGetType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureGetType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureGetType;

.field public static final enum SYNC_CONNECT_DNS:Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureGetType;

.field public static final enum SYNC_CONNECT_DNS_WRITE:Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureGetType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 158
    new-instance v0, Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureGetType;

    const-string v1, "SYNC_CONNECT_DNS"

    invoke-direct {v0, v1, v3, v2}, Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureGetType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureGetType;->SYNC_CONNECT_DNS:Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureGetType;

    .line 160
    new-instance v0, Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureGetType;

    const-string v1, "SYNC_CONNECT_DNS_WRITE"

    invoke-direct {v0, v1, v2, v4}, Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureGetType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureGetType;->SYNC_CONNECT_DNS_WRITE:Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureGetType;

    .line 157
    new-array v0, v4, [Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureGetType;

    sget-object v1, Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureGetType;->SYNC_CONNECT_DNS:Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureGetType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureGetType;->SYNC_CONNECT_DNS_WRITE:Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureGetType;

    aput-object v1, v0, v2

    sput-object v0, Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureGetType;->$VALUES:[Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureGetType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 165
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 166
    iput p3, p0, Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureGetType;->value:I

    .line 167
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureGetType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 157
    const-class v0, Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureGetType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureGetType;

    return-object v0
.end method

.method public static values()[Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureGetType;
    .locals 1

    .prologue
    .line 157
    sget-object v0, Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureGetType;->$VALUES:[Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureGetType;

    invoke-virtual {v0}, [Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureGetType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureGetType;

    return-object v0
.end method


# virtual methods
.method public value()I
    .locals 1

    .prologue
    .line 169
    iget v0, p0, Lcom/quicinc/cneapiclient/CNEManager$NSRMFeatureGetType;->value:I

    return v0
.end method
