.class public Lcom/android/org/conscrypt/OpenSSLEngineImpl;
.super Ljavax/net/ssl/SSLEngine;
.source "OpenSSLEngineImpl.java"

# interfaces
.implements Lcom/android/org/conscrypt/NativeCrypto$SSLHandshakeCallbacks;
.implements Lcom/android/org/conscrypt/SSLParametersImpl$AliasChooser;
.implements Lcom/android/org/conscrypt/SSLParametersImpl$PSKCallbacks;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/org/conscrypt/OpenSSLEngineImpl$1;,
        Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;
    }
.end annotation


# static fields
.field private static nullSource:Lcom/android/org/conscrypt/OpenSSLBIOSource;


# instance fields
.field channelIdPrivateKey:Lcom/android/org/conscrypt/OpenSSLKey;

.field private engineState:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

.field private handshakeSession:Lcom/android/org/conscrypt/OpenSSLSessionImpl;

.field private handshakeSink:Lcom/android/org/conscrypt/OpenSSLBIOSink;

.field private final localToRemoteSink:Lcom/android/org/conscrypt/OpenSSLBIOSink;

.field private sslNativePointer:J

.field private final sslParameters:Lcom/android/org/conscrypt/SSLParametersImpl;

.field private sslSession:Lcom/android/org/conscrypt/OpenSSLSessionImpl;

.field private final stateLock:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 102
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, Lcom/android/org/conscrypt/OpenSSLBIOSource;->wrap(Ljava/nio/ByteBuffer;)Lcom/android/org/conscrypt/OpenSSLBIOSource;

    move-result-object v0

    sput-object v0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->nullSource:Lcom/android/org/conscrypt/OpenSSLBIOSource;

    return-void
.end method

.method public constructor <init>(Lcom/android/org/conscrypt/SSLParametersImpl;)V
    .locals 1
    .param p1, "sslParameters"    # Lcom/android/org/conscrypt/SSLParametersImpl;

    .prologue
    .line 122
    invoke-direct {p0}, Ljavax/net/ssl/SSLEngine;-><init>()V

    .line 48
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->stateLock:Ljava/lang/Object;

    .line 91
    sget-object v0, Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;->NEW:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    iput-object v0, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    .line 108
    invoke-static {}, Lcom/android/org/conscrypt/OpenSSLBIOSink;->create()Lcom/android/org/conscrypt/OpenSSLBIOSink;

    move-result-object v0

    iput-object v0, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->localToRemoteSink:Lcom/android/org/conscrypt/OpenSSLBIOSink;

    .line 123
    iput-object p1, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->sslParameters:Lcom/android/org/conscrypt/SSLParametersImpl;

    .line 124
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILcom/android/org/conscrypt/SSLParametersImpl;)V
    .locals 1
    .param p1, "host"    # Ljava/lang/String;
    .param p2, "port"    # I
    .param p3, "sslParameters"    # Lcom/android/org/conscrypt/SSLParametersImpl;

    .prologue
    .line 127
    invoke-direct {p0, p1, p2}, Ljavax/net/ssl/SSLEngine;-><init>(Ljava/lang/String;I)V

    .line 48
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->stateLock:Ljava/lang/Object;

    .line 91
    sget-object v0, Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;->NEW:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    iput-object v0, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    .line 108
    invoke-static {}, Lcom/android/org/conscrypt/OpenSSLBIOSink;->create()Lcom/android/org/conscrypt/OpenSSLBIOSink;

    move-result-object v0

    iput-object v0, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->localToRemoteSink:Lcom/android/org/conscrypt/OpenSSLBIOSink;

    .line 128
    iput-object p3, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->sslParameters:Lcom/android/org/conscrypt/SSLParametersImpl;

    .line 129
    return-void
.end method

.method private static checkIndex(III)V
    .locals 2
    .param p0, "length"    # I
    .param p1, "offset"    # I
    .param p2, "count"    # I

    .prologue
    .line 374
    if-gez p1, :cond_0

    .line 375
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    const-string v1, "offset < 0"

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 376
    :cond_0
    if-gez p2, :cond_1

    .line 377
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    const-string v1, "count < 0"

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 378
    :cond_1
    if-le p1, p0, :cond_2

    .line 379
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    const-string v1, "offset > length"

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 380
    :cond_2
    sub-int v0, p0, p2

    if-le p1, v0, :cond_3

    .line 381
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    const-string v1, "offset + count > length"

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 383
    :cond_3
    return-void
.end method

.method private free()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 703
    iget-wide v0, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->sslNativePointer:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 708
    :goto_0
    return-void

    .line 706
    :cond_0
    iget-wide v0, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->sslNativePointer:J

    invoke-static {v0, v1}, Lcom/android/org/conscrypt/NativeCrypto;->SSL_free(J)V

    .line 707
    iput-wide v2, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->sslNativePointer:J

    goto :goto_0
.end method

.method private getNextAvailableByteBuffer([Ljava/nio/ByteBuffer;II)Ljava/nio/ByteBuffer;
    .locals 2
    .param p1, "buffers"    # [Ljava/nio/ByteBuffer;
    .param p2, "offset"    # I
    .param p3, "length"    # I

    .prologue
    .line 499
    move v0, p2

    .local v0, "i":I
    :goto_0
    if-ge v0, p3, :cond_1

    .line 500
    aget-object v1, p1, v0

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v1

    if-lez v1, :cond_0

    .line 501
    aget-object v1, p1, v0

    .line 504
    :goto_1
    return-object v1

    .line 499
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 504
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private shutdown()V
    .locals 7

    .prologue
    .line 684
    :try_start_0
    iget-wide v0, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->sslNativePointer:J

    sget-object v2, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->nullSource:Lcom/android/org/conscrypt/OpenSSLBIOSource;

    invoke-virtual {v2}, Lcom/android/org/conscrypt/OpenSSLBIOSource;->getContext()J

    move-result-wide v2

    iget-object v4, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->localToRemoteSink:Lcom/android/org/conscrypt/OpenSSLBIOSink;

    invoke-virtual {v4}, Lcom/android/org/conscrypt/OpenSSLBIOSink;->getContext()J

    move-result-wide v4

    move-object v6, p0

    invoke-static/range {v0 .. v6}, Lcom/android/org/conscrypt/NativeCrypto;->SSL_shutdown_BIO(JJJLcom/android/org/conscrypt/NativeCrypto$SSLHandshakeCallbacks;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 692
    :goto_0
    return-void

    .line 686
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private shutdownAndFreeSslNative()V
    .locals 1

    .prologue
    .line 696
    :try_start_0
    invoke-direct {p0}, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->shutdown()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 698
    invoke-direct {p0}, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->free()V

    .line 700
    return-void

    .line 698
    :catchall_0
    move-exception v0

    invoke-direct {p0}, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->free()V

    throw v0
.end method

.method private static writeSinkToByteBuffer(Lcom/android/org/conscrypt/OpenSSLBIOSink;Ljava/nio/ByteBuffer;)I
    .locals 4
    .param p0, "sink"    # Lcom/android/org/conscrypt/OpenSSLBIOSink;
    .param p1, "dst"    # Ljava/nio/ByteBuffer;

    .prologue
    .line 601
    invoke-virtual {p0}, Lcom/android/org/conscrypt/OpenSSLBIOSink;->available()I

    move-result v1

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 602
    .local v0, "toWrite":I
    invoke-virtual {p0}, Lcom/android/org/conscrypt/OpenSSLBIOSink;->toByteArray()[B

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/org/conscrypt/OpenSSLBIOSink;->position()I

    move-result v2

    invoke-virtual {p1, v1, v2, v0}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    .line 603
    int-to-long v2, v0

    invoke-virtual {p0, v2, v3}, Lcom/android/org/conscrypt/OpenSSLBIOSink;->skip(J)J

    .line 604
    return v0
.end method


# virtual methods
.method public beginHandshake()V
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/net/ssl/SSLException;
        }
    .end annotation

    .prologue
    .line 133
    iget-object v4, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->stateLock:Ljava/lang/Object;

    monitor-enter v4

    .line 134
    :try_start_0
    iget-object v1, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    sget-object v5, Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;->CLOSED:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    if-eq v1, v5, :cond_0

    iget-object v1, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    sget-object v5, Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;->CLOSED_OUTBOUND:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    if-eq v1, v5, :cond_0

    iget-object v1, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    sget-object v5, Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;->CLOSED_INBOUND:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    if-ne v1, v5, :cond_1

    .line 136
    :cond_0
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v5, "Engine has already been closed"

    invoke-direct {v1, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 149
    :catchall_0
    move-exception v1

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 138
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    sget-object v5, Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;->HANDSHAKE_STARTED:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    if-ne v1, v5, :cond_2

    .line 139
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v5, "Handshake has already been started"

    invoke-direct {v1, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 141
    :cond_2
    iget-object v1, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    sget-object v5, Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;->MODE_SET:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    if-eq v1, v5, :cond_3

    .line 142
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v5, "Client/server mode must be set before handshake"

    invoke-direct {v1, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 144
    :cond_3
    invoke-virtual {p0}, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->getUseClientMode()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 145
    sget-object v1, Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;->HANDSHAKE_WANTED:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    iput-object v1, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    .line 149
    :goto_0
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 151
    const/4 v11, 0x1

    .line 153
    .local v11, "releaseResources":Z
    :try_start_2
    iget-object v1, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->sslParameters:Lcom/android/org/conscrypt/SSLParametersImpl;

    invoke-virtual {v1}, Lcom/android/org/conscrypt/SSLParametersImpl;->getSessionContext()Lcom/android/org/conscrypt/AbstractSessionContext;

    move-result-object v12

    .line 154
    .local v12, "sessionContext":Lcom/android/org/conscrypt/AbstractSessionContext;
    iget-wide v2, v12, Lcom/android/org/conscrypt/AbstractSessionContext;->sslCtxNativePointer:J

    .line 155
    .local v2, "sslCtxNativePointer":J
    invoke-static {v2, v3}, Lcom/android/org/conscrypt/NativeCrypto;->SSL_new(J)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->sslNativePointer:J

    .line 156
    iget-object v1, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->sslParameters:Lcom/android/org/conscrypt/SSLParametersImpl;

    iget-wide v4, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->sslNativePointer:J

    invoke-virtual {p0}, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->getPeerHost()Ljava/lang/String;

    move-result-object v8

    move-object v6, p0

    move-object v7, p0

    invoke-virtual/range {v1 .. v8}, Lcom/android/org/conscrypt/SSLParametersImpl;->setSSLParameters(JJLcom/android/org/conscrypt/SSLParametersImpl$AliasChooser;Lcom/android/org/conscrypt/SSLParametersImpl$PSKCallbacks;Ljava/lang/String;)V

    .line 158
    iget-object v1, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->sslParameters:Lcom/android/org/conscrypt/SSLParametersImpl;

    iget-wide v4, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->sslNativePointer:J

    invoke-virtual {v1, v4, v5}, Lcom/android/org/conscrypt/SSLParametersImpl;->setCertificateValidation(J)V

    .line 159
    iget-object v1, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->sslParameters:Lcom/android/org/conscrypt/SSLParametersImpl;

    iget-wide v4, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->sslNativePointer:J

    iget-object v6, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->channelIdPrivateKey:Lcom/android/org/conscrypt/OpenSSLKey;

    invoke-virtual {v1, v4, v5, v6}, Lcom/android/org/conscrypt/SSLParametersImpl;->setTlsChannelId(JLcom/android/org/conscrypt/OpenSSLKey;)V

    .line 160
    invoke-virtual {p0}, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->getUseClientMode()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 161
    iget-wide v4, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->sslNativePointer:J

    invoke-static {v4, v5}, Lcom/android/org/conscrypt/NativeCrypto;->SSL_set_connect_state(J)V

    .line 165
    :goto_1
    invoke-static {}, Lcom/android/org/conscrypt/OpenSSLBIOSink;->create()Lcom/android/org/conscrypt/OpenSSLBIOSink;

    move-result-object v1

    iput-object v1, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->handshakeSink:Lcom/android/org/conscrypt/OpenSSLBIOSink;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 166
    const/4 v11, 0x0

    .line 177
    if-eqz v11, :cond_4

    .line 178
    iget-object v4, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->stateLock:Ljava/lang/Object;

    monitor-enter v4

    .line 179
    :try_start_3
    sget-object v1, Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;->CLOSED:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    iput-object v1, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    .line 180
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 181
    invoke-direct {p0}, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->shutdownAndFreeSslNative()V

    .line 184
    :cond_4
    return-void

    .line 147
    .end local v2    # "sslCtxNativePointer":J
    .end local v11    # "releaseResources":Z
    .end local v12    # "sessionContext":Lcom/android/org/conscrypt/AbstractSessionContext;
    :cond_5
    :try_start_4
    sget-object v1, Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;->HANDSHAKE_STARTED:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    iput-object v1, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 163
    .restart local v2    # "sslCtxNativePointer":J
    .restart local v11    # "releaseResources":Z
    .restart local v12    # "sessionContext":Lcom/android/org/conscrypt/AbstractSessionContext;
    :cond_6
    :try_start_5
    iget-wide v4, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->sslNativePointer:J

    invoke-static {v4, v5}, Lcom/android/org/conscrypt/NativeCrypto;->SSL_set_accept_state(J)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_1

    .line 167
    .end local v2    # "sslCtxNativePointer":J
    .end local v12    # "sessionContext":Lcom/android/org/conscrypt/AbstractSessionContext;
    :catch_0
    move-exception v0

    .line 169
    .local v0, "e":Ljava/io/IOException;
    :try_start_6
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v10

    .line 171
    .local v10, "message":Ljava/lang/String;
    const-string v1, "unexpected CCS"

    invoke-virtual {v10, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 172
    const-string v1, "ssl_unexpected_ccs: host=%s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->getPeerHost()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v1, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    .line 173
    .local v9, "logMessage":Ljava/lang/String;
    invoke-static {v9}, Lcom/android/org/conscrypt/Platform;->logEvent(Ljava/lang/String;)V

    .line 175
    .end local v9    # "logMessage":Ljava/lang/String;
    :cond_7
    new-instance v1, Ljavax/net/ssl/SSLException;

    invoke-direct {v1, v0}, Ljavax/net/ssl/SSLException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 177
    .end local v0    # "e":Ljava/io/IOException;
    .end local v10    # "message":Ljava/lang/String;
    :catchall_1
    move-exception v1

    if-eqz v11, :cond_8

    .line 178
    iget-object v4, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->stateLock:Ljava/lang/Object;

    monitor-enter v4

    .line 179
    :try_start_7
    sget-object v5, Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;->CLOSED:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    iput-object v5, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    .line 180
    monitor-exit v4
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    .line 181
    invoke-direct {p0}, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->shutdownAndFreeSslNative()V

    :cond_8
    throw v1

    .line 180
    .restart local v2    # "sslCtxNativePointer":J
    .restart local v12    # "sessionContext":Lcom/android/org/conscrypt/AbstractSessionContext;
    :catchall_2
    move-exception v1

    :try_start_8
    monitor-exit v4
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    throw v1

    .end local v2    # "sslCtxNativePointer":J
    .end local v12    # "sessionContext":Lcom/android/org/conscrypt/AbstractSessionContext;
    :catchall_3
    move-exception v1

    :try_start_9
    monitor-exit v4
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    throw v1
.end method

.method public chooseClientAlias(Ljavax/net/ssl/X509KeyManager;[Ljavax/security/auth/x500/X500Principal;[Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "keyManager"    # Ljavax/net/ssl/X509KeyManager;
    .param p2, "issuers"    # [Ljavax/security/auth/x500/X500Principal;
    .param p3, "keyTypes"    # [Ljava/lang/String;

    .prologue
    .line 732
    instance-of v1, p1, Ljavax/net/ssl/X509ExtendedKeyManager;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 733
    check-cast v0, Ljavax/net/ssl/X509ExtendedKeyManager;

    .line 734
    .local v0, "ekm":Ljavax/net/ssl/X509ExtendedKeyManager;
    invoke-virtual {v0, p3, p2, p0}, Ljavax/net/ssl/X509ExtendedKeyManager;->chooseEngineClientAlias([Ljava/lang/String;[Ljava/security/Principal;Ljavax/net/ssl/SSLEngine;)Ljava/lang/String;

    move-result-object v1

    .line 736
    .end local v0    # "ekm":Ljavax/net/ssl/X509ExtendedKeyManager;
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    invoke-interface {p1, p3, p2, v1}, Ljavax/net/ssl/X509KeyManager;->chooseClientAlias([Ljava/lang/String;[Ljava/security/Principal;Ljava/net/Socket;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public chooseClientPSKIdentity(Lcom/android/org/conscrypt/PSKKeyManager;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "keyManager"    # Lcom/android/org/conscrypt/PSKKeyManager;
    .param p2, "identityHint"    # Ljava/lang/String;

    .prologue
    .line 747
    invoke-interface {p1, p2, p0}, Lcom/android/org/conscrypt/PSKKeyManager;->chooseClientKeyIdentity(Ljava/lang/String;Ljavax/net/ssl/SSLEngine;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public chooseServerAlias(Ljavax/net/ssl/X509KeyManager;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "keyManager"    # Ljavax/net/ssl/X509KeyManager;
    .param p2, "keyType"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 721
    instance-of v1, p1, Ljavax/net/ssl/X509ExtendedKeyManager;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 722
    check-cast v0, Ljavax/net/ssl/X509ExtendedKeyManager;

    .line 723
    .local v0, "ekm":Ljavax/net/ssl/X509ExtendedKeyManager;
    invoke-virtual {v0, p2, v2, p0}, Ljavax/net/ssl/X509ExtendedKeyManager;->chooseEngineServerAlias(Ljava/lang/String;[Ljava/security/Principal;Ljavax/net/ssl/SSLEngine;)Ljava/lang/String;

    move-result-object v1

    .line 725
    .end local v0    # "ekm":Ljavax/net/ssl/X509ExtendedKeyManager;
    :goto_0
    return-object v1

    :cond_0
    invoke-interface {p1, p2, v2, v2}, Ljavax/net/ssl/X509KeyManager;->chooseServerAlias(Ljava/lang/String;[Ljava/security/Principal;Ljava/net/Socket;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public chooseServerPSKIdentityHint(Lcom/android/org/conscrypt/PSKKeyManager;)Ljava/lang/String;
    .locals 1
    .param p1, "keyManager"    # Lcom/android/org/conscrypt/PSKKeyManager;

    .prologue
    .line 742
    invoke-interface {p1, p0}, Lcom/android/org/conscrypt/PSKKeyManager;->chooseServerKeyIdentityHint(Ljavax/net/ssl/SSLEngine;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public clientCertificateRequested([B[[B)V
    .locals 7
    .param p1, "keyTypeBytes"    # [B
    .param p2, "asn1DerEncodedPrincipals"    # [[B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/cert/CertificateEncodingException;,
            Ljavax/net/ssl/SSLException;
        }
    .end annotation

    .prologue
    .line 678
    iget-object v1, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->sslParameters:Lcom/android/org/conscrypt/SSLParametersImpl;

    iget-wide v4, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->sslNativePointer:J

    move-object v2, p1

    move-object v3, p2

    move-object v6, p0

    invoke-virtual/range {v1 .. v6}, Lcom/android/org/conscrypt/SSLParametersImpl;->chooseClientCertificate([B[[BJLcom/android/org/conscrypt/SSLParametersImpl$AliasChooser;)V

    .line 680
    return-void
.end method

.method public clientPSKKeyRequested(Ljava/lang/String;[B[B)I
    .locals 1
    .param p1, "identityHint"    # Ljava/lang/String;
    .param p2, "identity"    # [B
    .param p3, "key"    # [B

    .prologue
    .line 609
    iget-object v0, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->sslParameters:Lcom/android/org/conscrypt/SSLParametersImpl;

    invoke-virtual {v0, p1, p2, p3, p0}, Lcom/android/org/conscrypt/SSLParametersImpl;->clientPSKKeyRequested(Ljava/lang/String;[B[BLcom/android/org/conscrypt/SSLParametersImpl$PSKCallbacks;)I

    move-result v0

    return v0
.end method

.method public closeInbound()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/net/ssl/SSLException;
        }
    .end annotation

    .prologue
    .line 188
    iget-object v1, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->stateLock:Ljava/lang/Object;

    monitor-enter v1

    .line 189
    :try_start_0
    iget-object v0, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    sget-object v2, Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;->CLOSED:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    if-ne v0, v2, :cond_0

    .line 190
    monitor-exit v1

    .line 199
    :goto_0
    return-void

    .line 192
    :cond_0
    iget-object v0, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    sget-object v2, Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;->CLOSED_OUTBOUND:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    if-ne v0, v2, :cond_1

    .line 193
    sget-object v0, Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;->CLOSED:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    iput-object v0, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    .line 197
    :goto_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 195
    :cond_1
    :try_start_1
    sget-object v0, Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;->CLOSED_INBOUND:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    iput-object v0, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method public closeOutbound()V
    .locals 3

    .prologue
    .line 203
    iget-object v1, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->stateLock:Ljava/lang/Object;

    monitor-enter v1

    .line 204
    :try_start_0
    iget-object v0, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    sget-object v2, Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;->CLOSED:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    if-eq v0, v2, :cond_0

    iget-object v0, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    sget-object v2, Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;->CLOSED_OUTBOUND:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    if-ne v0, v2, :cond_1

    .line 205
    :cond_0
    monitor-exit v1

    .line 217
    :goto_0
    return-void

    .line 207
    :cond_1
    iget-object v0, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    sget-object v2, Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;->MODE_SET:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    if-eq v0, v2, :cond_2

    iget-object v0, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    sget-object v2, Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;->NEW:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    if-eq v0, v2, :cond_2

    .line 208
    invoke-direct {p0}, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->shutdownAndFreeSslNative()V

    .line 210
    :cond_2
    iget-object v0, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    sget-object v2, Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;->CLOSED_INBOUND:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    if-ne v0, v2, :cond_3

    .line 211
    sget-object v0, Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;->CLOSED:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    iput-object v0, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    .line 215
    :goto_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 216
    invoke-direct {p0}, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->shutdown()V

    goto :goto_0

    .line 213
    :cond_3
    :try_start_1
    sget-object v0, Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;->CLOSED_OUTBOUND:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    iput-object v0, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    goto :goto_1

    .line 215
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method protected finalize()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 713
    :try_start_0
    invoke-direct {p0}, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->free()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 715
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 717
    return-void

    .line 715
    :catchall_0
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    throw v0
.end method

.method public getDelegatedTask()Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 222
    const/4 v0, 0x0

    return-object v0
.end method

.method public getEnableSessionCreation()Z
    .locals 1

    .prologue
    .line 237
    iget-object v0, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->sslParameters:Lcom/android/org/conscrypt/SSLParametersImpl;

    invoke-virtual {v0}, Lcom/android/org/conscrypt/SSLParametersImpl;->getEnableSessionCreation()Z

    move-result v0

    return v0
.end method

.method public getEnabledCipherSuites()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->sslParameters:Lcom/android/org/conscrypt/SSLParametersImpl;

    invoke-virtual {v0}, Lcom/android/org/conscrypt/SSLParametersImpl;->getEnabledCipherSuites()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEnabledProtocols()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->sslParameters:Lcom/android/org/conscrypt/SSLParametersImpl;

    invoke-virtual {v0}, Lcom/android/org/conscrypt/SSLParametersImpl;->getEnabledProtocols()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getHandshakeStatus()Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;
    .locals 4

    .prologue
    .line 242
    iget-object v1, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->stateLock:Ljava/lang/Object;

    monitor-enter v1

    .line 243
    :try_start_0
    sget-object v0, Lcom/android/org/conscrypt/OpenSSLEngineImpl$1;->$SwitchMap$org$conscrypt$OpenSSLEngineImpl$EngineState:[I

    iget-object v2, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    invoke-virtual {v2}, Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 275
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected engine state: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 276
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 245
    :pswitch_0
    :try_start_1
    invoke-virtual {p0}, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->getUseClientMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 246
    sget-object v0, Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;->NEED_WRAP:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    monitor-exit v1

    .line 271
    :goto_0
    return-object v0

    .line 248
    :cond_0
    sget-object v0, Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;->NEED_UNWRAP:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    monitor-exit v1

    goto :goto_0

    .line 251
    :pswitch_1
    iget-object v0, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->handshakeSink:Lcom/android/org/conscrypt/OpenSSLBIOSink;

    invoke-virtual {v0}, Lcom/android/org/conscrypt/OpenSSLBIOSink;->available()I

    move-result v0

    if-lez v0, :cond_1

    .line 252
    sget-object v0, Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;->NEED_WRAP:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    monitor-exit v1

    goto :goto_0

    .line 254
    :cond_1
    sget-object v0, Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;->NEED_UNWRAP:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    monitor-exit v1

    goto :goto_0

    .line 257
    :pswitch_2
    iget-object v0, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->handshakeSink:Lcom/android/org/conscrypt/OpenSSLBIOSink;

    invoke-virtual {v0}, Lcom/android/org/conscrypt/OpenSSLBIOSink;->available()I

    move-result v0

    if-nez v0, :cond_2

    .line 258
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->handshakeSink:Lcom/android/org/conscrypt/OpenSSLBIOSink;

    .line 259
    sget-object v0, Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;->READY:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    iput-object v0, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    .line 260
    sget-object v0, Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;->FINISHED:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    monitor-exit v1

    goto :goto_0

    .line 262
    :cond_2
    sget-object v0, Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;->NEED_WRAP:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    monitor-exit v1

    goto :goto_0

    .line 271
    :pswitch_3
    sget-object v0, Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;->NOT_HANDSHAKING:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 243
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public getNeedClientAuth()Z
    .locals 1

    .prologue
    .line 281
    iget-object v0, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->sslParameters:Lcom/android/org/conscrypt/SSLParametersImpl;

    invoke-virtual {v0}, Lcom/android/org/conscrypt/SSLParametersImpl;->getNeedClientAuth()Z

    move-result v0

    return v0
.end method

.method public getPSKKey(Lcom/android/org/conscrypt/PSKKeyManager;Ljava/lang/String;Ljava/lang/String;)Ljavax/crypto/SecretKey;
    .locals 1
    .param p1, "keyManager"    # Lcom/android/org/conscrypt/PSKKeyManager;
    .param p2, "identityHint"    # Ljava/lang/String;
    .param p3, "identity"    # Ljava/lang/String;

    .prologue
    .line 752
    invoke-interface {p1, p2, p3, p0}, Lcom/android/org/conscrypt/PSKKeyManager;->getKey(Ljava/lang/String;Ljava/lang/String;Ljavax/net/ssl/SSLEngine;)Ljavax/crypto/SecretKey;

    move-result-object v0

    return-object v0
.end method

.method public getSession()Ljavax/net/ssl/SSLSession;
    .locals 1

    .prologue
    .line 286
    iget-object v0, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->sslSession:Lcom/android/org/conscrypt/OpenSSLSessionImpl;

    if-nez v0, :cond_0

    .line 287
    invoke-static {}, Lcom/android/org/conscrypt/SSLNullSession;->getNullSession()Ljavax/net/ssl/SSLSession;

    move-result-object v0

    .line 289
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->sslSession:Lcom/android/org/conscrypt/OpenSSLSessionImpl;

    goto :goto_0
.end method

.method public getSupportedCipherSuites()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 294
    invoke-static {}, Lcom/android/org/conscrypt/NativeCrypto;->getSupportedCipherSuites()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSupportedProtocols()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 299
    invoke-static {}, Lcom/android/org/conscrypt/NativeCrypto;->getSupportedProtocols()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUseClientMode()Z
    .locals 1

    .prologue
    .line 304
    iget-object v0, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->sslParameters:Lcom/android/org/conscrypt/SSLParametersImpl;

    invoke-virtual {v0}, Lcom/android/org/conscrypt/SSLParametersImpl;->getUseClientMode()Z

    move-result v0

    return v0
.end method

.method public getWantClientAuth()Z
    .locals 1

    .prologue
    .line 309
    iget-object v0, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->sslParameters:Lcom/android/org/conscrypt/SSLParametersImpl;

    invoke-virtual {v0}, Lcom/android/org/conscrypt/SSLParametersImpl;->getWantClientAuth()Z

    move-result v0

    return v0
.end method

.method public isInboundDone()Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 314
    iget-wide v2, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->sslNativePointer:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    .line 315
    iget-object v2, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->stateLock:Ljava/lang/Object;

    monitor-enter v2

    .line 316
    :try_start_0
    iget-object v3, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    sget-object v4, Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;->CLOSED:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    if-eq v3, v4, :cond_0

    iget-object v3, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    sget-object v4, Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;->CLOSED_INBOUND:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    if-ne v3, v4, :cond_2

    :cond_0
    :goto_0
    monitor-exit v2

    .line 320
    :cond_1
    :goto_1
    return v0

    :cond_2
    move v0, v1

    .line 316
    goto :goto_0

    .line 318
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 320
    :cond_3
    iget-wide v2, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->sslNativePointer:J

    invoke-static {v2, v3}, Lcom/android/org/conscrypt/NativeCrypto;->SSL_get_shutdown(J)I

    move-result v2

    and-int/lit8 v2, v2, 0x2

    if-nez v2, :cond_1

    move v0, v1

    goto :goto_1
.end method

.method public isOutboundDone()Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 326
    iget-wide v2, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->sslNativePointer:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    .line 327
    iget-object v2, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->stateLock:Ljava/lang/Object;

    monitor-enter v2

    .line 328
    :try_start_0
    iget-object v3, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    sget-object v4, Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;->CLOSED:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    if-eq v3, v4, :cond_0

    iget-object v3, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    sget-object v4, Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;->CLOSED_OUTBOUND:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    if-ne v3, v4, :cond_2

    :cond_0
    :goto_0
    monitor-exit v2

    .line 332
    :cond_1
    :goto_1
    return v0

    :cond_2
    move v0, v1

    .line 328
    goto :goto_0

    .line 330
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 332
    :cond_3
    iget-wide v2, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->sslNativePointer:J

    invoke-static {v2, v3}, Lcom/android/org/conscrypt/NativeCrypto;->SSL_get_shutdown(J)I

    move-result v2

    and-int/lit8 v2, v2, 0x1

    if-nez v2, :cond_1

    move v0, v1

    goto :goto_1
.end method

.method public onSSLStateChange(JII)V
    .locals 4
    .param p1, "sslSessionNativePtr"    # J
    .param p3, "type"    # I
    .param p4, "val"    # I

    .prologue
    .line 619
    iget-object v1, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->stateLock:Ljava/lang/Object;

    monitor-enter v1

    .line 620
    sparse-switch p3, :sswitch_data_0

    .line 635
    :goto_0
    :try_start_0
    monitor-exit v1

    .line 636
    return-void

    .line 622
    :sswitch_0
    iget-object v0, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    sget-object v2, Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;->HANDSHAKE_STARTED:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    if-eq v0, v2, :cond_0

    iget-object v0, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    sget-object v2, Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;->READY_HANDSHAKE_CUT_THROUGH:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    if-eq v0, v2, :cond_0

    .line 624
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Completed handshake while in mode "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 635
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 627
    :cond_0
    :try_start_1
    sget-object v0, Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;->HANDSHAKE_COMPLETED:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    iput-object v0, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    goto :goto_0

    .line 632
    :sswitch_1
    sget-object v0, Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;->HANDSHAKE_STARTED:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    iput-object v0, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 620
    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_1
        0x20 -> :sswitch_0
    .end sparse-switch
.end method

.method public serverPSKKeyRequested(Ljava/lang/String;Ljava/lang/String;[B)I
    .locals 1
    .param p1, "identityHint"    # Ljava/lang/String;
    .param p2, "identity"    # Ljava/lang/String;
    .param p3, "key"    # [B

    .prologue
    .line 614
    iget-object v0, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->sslParameters:Lcom/android/org/conscrypt/SSLParametersImpl;

    invoke-virtual {v0, p1, p2, p3, p0}, Lcom/android/org/conscrypt/SSLParametersImpl;->serverPSKKeyRequested(Ljava/lang/String;Ljava/lang/String;[BLcom/android/org/conscrypt/SSLParametersImpl$PSKCallbacks;)I

    move-result v0

    return v0
.end method

.method public setEnableSessionCreation(Z)V
    .locals 1
    .param p1, "flag"    # Z

    .prologue
    .line 348
    iget-object v0, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->sslParameters:Lcom/android/org/conscrypt/SSLParametersImpl;

    invoke-virtual {v0, p1}, Lcom/android/org/conscrypt/SSLParametersImpl;->setEnableSessionCreation(Z)V

    .line 349
    return-void
.end method

.method public setEnabledCipherSuites([Ljava/lang/String;)V
    .locals 1
    .param p1, "suites"    # [Ljava/lang/String;

    .prologue
    .line 338
    iget-object v0, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->sslParameters:Lcom/android/org/conscrypt/SSLParametersImpl;

    invoke-virtual {v0, p1}, Lcom/android/org/conscrypt/SSLParametersImpl;->setEnabledCipherSuites([Ljava/lang/String;)V

    .line 339
    return-void
.end method

.method public setEnabledProtocols([Ljava/lang/String;)V
    .locals 1
    .param p1, "protocols"    # [Ljava/lang/String;

    .prologue
    .line 343
    iget-object v0, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->sslParameters:Lcom/android/org/conscrypt/SSLParametersImpl;

    invoke-virtual {v0, p1}, Lcom/android/org/conscrypt/SSLParametersImpl;->setEnabledProtocols([Ljava/lang/String;)V

    .line 344
    return-void
.end method

.method public setNeedClientAuth(Z)V
    .locals 1
    .param p1, "need"    # Z

    .prologue
    .line 353
    iget-object v0, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->sslParameters:Lcom/android/org/conscrypt/SSLParametersImpl;

    invoke-virtual {v0, p1}, Lcom/android/org/conscrypt/SSLParametersImpl;->setNeedClientAuth(Z)V

    .line 354
    return-void
.end method

.method public setUseClientMode(Z)V
    .locals 4
    .param p1, "mode"    # Z

    .prologue
    .line 358
    iget-object v1, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->stateLock:Ljava/lang/Object;

    monitor-enter v1

    .line 359
    :try_start_0
    iget-object v0, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    sget-object v2, Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;->MODE_SET:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    if-eq v0, v2, :cond_0

    iget-object v0, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    sget-object v2, Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;->NEW:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    if-eq v0, v2, :cond_0

    .line 360
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Can not change mode after handshake: engineState == "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 364
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 363
    :cond_0
    :try_start_1
    sget-object v0, Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;->MODE_SET:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    iput-object v0, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    .line 364
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 365
    iget-object v0, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->sslParameters:Lcom/android/org/conscrypt/SSLParametersImpl;

    invoke-virtual {v0, p1}, Lcom/android/org/conscrypt/SSLParametersImpl;->setUseClientMode(Z)V

    .line 366
    return-void
.end method

.method public setWantClientAuth(Z)V
    .locals 1
    .param p1, "want"    # Z

    .prologue
    .line 370
    iget-object v0, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->sslParameters:Lcom/android/org/conscrypt/SSLParametersImpl;

    invoke-virtual {v0, p1}, Lcom/android/org/conscrypt/SSLParametersImpl;->setWantClientAuth(Z)V

    .line 371
    return-void
.end method

.method public unwrap(Ljava/nio/ByteBuffer;[Ljava/nio/ByteBuffer;II)Ljavax/net/ssl/SSLEngineResult;
    .locals 36
    .param p1, "src"    # Ljava/nio/ByteBuffer;
    .param p2, "dsts"    # [Ljava/nio/ByteBuffer;
    .param p3, "offset"    # I
    .param p4, "length"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/net/ssl/SSLException;
        }
    .end annotation

    .prologue
    .line 388
    if-nez p1, :cond_0

    .line 389
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "src == null"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 390
    :cond_0
    if-nez p2, :cond_1

    .line 391
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "dsts == null"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 393
    :cond_1
    move-object/from16 v0, p2

    array-length v4, v0

    move/from16 v0, p3

    move/from16 v1, p4

    invoke-static {v4, v0, v1}, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->checkIndex(III)V

    .line 394
    const/16 v23, 0x0

    .line 395
    .local v23, "dstRemaining":I
    const/16 v26, 0x0

    .local v26, "i":I
    :goto_0
    move-object/from16 v0, p2

    array-length v4, v0

    move/from16 v0, v26

    if-ge v0, v4, :cond_5

    .line 396
    aget-object v22, p2, v26

    .line 397
    .local v22, "dst":Ljava/nio/ByteBuffer;
    if-nez v22, :cond_2

    .line 398
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "one of the dst == null"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 399
    :cond_2
    invoke-virtual/range {v22 .. v22}, Ljava/nio/ByteBuffer;->isReadOnly()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 400
    new-instance v4, Ljava/nio/ReadOnlyBufferException;

    invoke-direct {v4}, Ljava/nio/ReadOnlyBufferException;-><init>()V

    throw v4

    .line 402
    :cond_3
    move/from16 v0, v26

    move/from16 v1, p3

    if-lt v0, v1, :cond_4

    add-int v4, p3, p4

    move/from16 v0, v26

    if-ge v0, v4, :cond_4

    .line 403
    invoke-virtual/range {v22 .. v22}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v4

    add-int v23, v23, v4

    .line 395
    :cond_4
    add-int/lit8 v26, v26, 0x1

    goto :goto_0

    .line 407
    .end local v22    # "dst":Ljava/nio/ByteBuffer;
    :cond_5
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->stateLock:Ljava/lang/Object;

    monitor-enter v5

    .line 409
    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    sget-object v8, Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;->CLOSED:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    if-eq v4, v8, :cond_6

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    sget-object v8, Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;->CLOSED_INBOUND:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    if-ne v4, v8, :cond_7

    .line 410
    :cond_6
    new-instance v4, Ljavax/net/ssl/SSLEngineResult;

    sget-object v8, Ljavax/net/ssl/SSLEngineResult$Status;->CLOSED:Ljavax/net/ssl/SSLEngineResult$Status;

    invoke-virtual/range {p0 .. p0}, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->getHandshakeStatus()Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    move-result-object v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-direct {v4, v8, v9, v10, v11}, Ljavax/net/ssl/SSLEngineResult;-><init>(Ljavax/net/ssl/SSLEngineResult$Status;Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;II)V

    monitor-exit v5

    .line 493
    :goto_1
    return-object v4

    .line 412
    :cond_7
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    sget-object v8, Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;->NEW:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    if-eq v4, v8, :cond_8

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    sget-object v8, Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;->MODE_SET:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    if-ne v4, v8, :cond_9

    .line 413
    :cond_8
    invoke-virtual/range {p0 .. p0}, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->beginHandshake()V

    .line 415
    :cond_9
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 418
    invoke-virtual/range {p0 .. p0}, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->getHandshakeStatus()Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    move-result-object v25

    .line 419
    .local v25, "handshakeStatus":Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;
    sget-object v4, Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;->NEED_UNWRAP:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    move-object/from16 v0, v25

    if-ne v0, v4, :cond_e

    .line 420
    invoke-static/range {p1 .. p1}, Lcom/android/org/conscrypt/OpenSSLBIOSource;->wrap(Ljava/nio/ByteBuffer;)Lcom/android/org/conscrypt/OpenSSLBIOSource;

    move-result-object v31

    .line 421
    .local v31, "source":Lcom/android/org/conscrypt/OpenSSLBIOSource;
    const-wide/16 v34, 0x0

    .line 423
    .local v34, "sslSessionCtx":J
    :try_start_1
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->sslNativePointer:J

    invoke-virtual/range {v31 .. v31}, Lcom/android/org/conscrypt/OpenSSLBIOSource;->getContext()J

    move-result-wide v6

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->handshakeSink:Lcom/android/org/conscrypt/OpenSSLBIOSink;

    invoke-virtual {v8}, Lcom/android/org/conscrypt/OpenSSLBIOSink;->getContext()J

    move-result-wide v8

    invoke-virtual/range {p0 .. p0}, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->getUseClientMode()Z

    move-result v11

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->sslParameters:Lcom/android/org/conscrypt/SSLParametersImpl;

    iget-object v12, v10, Lcom/android/org/conscrypt/SSLParametersImpl;->npnProtocols:[B

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->sslParameters:Lcom/android/org/conscrypt/SSLParametersImpl;

    iget-object v13, v10, Lcom/android/org/conscrypt/SSLParametersImpl;->alpnProtocols:[B

    move-object/from16 v10, p0

    invoke-static/range {v4 .. v13}, Lcom/android/org/conscrypt/NativeCrypto;->SSL_do_handshake_bio(JJJLcom/android/org/conscrypt/NativeCrypto$SSLHandshakeCallbacks;Z[B[B)J
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    move-result-wide v6

    .line 426
    .end local v34    # "sslSessionCtx":J
    .local v6, "sslSessionCtx":J
    const-wide/16 v4, 0x0

    cmp-long v4, v6, v4

    if-eqz v4, :cond_b

    .line 427
    :try_start_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->sslSession:Lcom/android/org/conscrypt/OpenSSLSessionImpl;

    if-eqz v4, :cond_a

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    sget-object v5, Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;->HANDSHAKE_STARTED:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    if-ne v4, v5, :cond_a

    .line 428
    sget-object v4, Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;->READY_HANDSHAKE_CUT_THROUGH:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    .line 430
    :cond_a
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->sslParameters:Lcom/android/org/conscrypt/SSLParametersImpl;

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->sslNativePointer:J

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->sslSession:Lcom/android/org/conscrypt/OpenSSLSessionImpl;

    invoke-virtual/range {p0 .. p0}, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->getPeerHost()Ljava/lang/String;

    move-result-object v11

    invoke-virtual/range {p0 .. p0}, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->getPeerPort()I

    move-result v12

    const/4 v13, 0x1

    invoke-virtual/range {v5 .. v13}, Lcom/android/org/conscrypt/SSLParametersImpl;->setupSession(JJLcom/android/org/conscrypt/OpenSSLSessionImpl;Ljava/lang/String;IZ)Lcom/android/org/conscrypt/OpenSSLSessionImpl;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->sslSession:Lcom/android/org/conscrypt/OpenSSLSessionImpl;

    .line 433
    :cond_b
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->handshakeSink:Lcom/android/org/conscrypt/OpenSSLBIOSink;

    invoke-virtual {v4}, Lcom/android/org/conscrypt/OpenSSLBIOSink;->position()I

    move-result v20

    .line 434
    .local v20, "bytesWritten":I
    new-instance v4, Ljavax/net/ssl/SSLEngineResult;

    sget-object v5, Ljavax/net/ssl/SSLEngineResult$Status;->OK:Ljavax/net/ssl/SSLEngineResult$Status;

    invoke-virtual/range {p0 .. p0}, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->getHandshakeStatus()Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    move-result-object v8

    const/4 v9, 0x0

    move/from16 v0, v20

    invoke-direct {v4, v5, v8, v9, v0}, Ljavax/net/ssl/SSLEngineResult;-><init>(Ljavax/net/ssl/SSLEngineResult$Status;Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;II)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 439
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->sslSession:Lcom/android/org/conscrypt/OpenSSLSessionImpl;

    if-nez v5, :cond_c

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-eqz v5, :cond_c

    .line 440
    invoke-static {v6, v7}, Lcom/android/org/conscrypt/NativeCrypto;->SSL_SESSION_free(J)V

    .line 442
    :cond_c
    invoke-virtual/range {v31 .. v31}, Lcom/android/org/conscrypt/OpenSSLBIOSource;->release()V

    goto/16 :goto_1

    .line 415
    .end local v6    # "sslSessionCtx":J
    .end local v20    # "bytesWritten":I
    .end local v25    # "handshakeStatus":Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;
    .end local v31    # "source":Lcom/android/org/conscrypt/OpenSSLBIOSource;
    :catchall_0
    move-exception v4

    :try_start_3
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v4

    .line 435
    .restart local v25    # "handshakeStatus":Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;
    .restart local v31    # "source":Lcom/android/org/conscrypt/OpenSSLBIOSource;
    .restart local v34    # "sslSessionCtx":J
    :catch_0
    move-exception v24

    move-wide/from16 v6, v34

    .line 436
    .end local v34    # "sslSessionCtx":J
    .restart local v6    # "sslSessionCtx":J
    .local v24, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_4
    new-instance v4, Ljavax/net/ssl/SSLHandshakeException;

    const-string v5, "Handshake failed"

    invoke-direct {v4, v5}, Ljavax/net/ssl/SSLHandshakeException;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v24

    invoke-virtual {v4, v0}, Ljavax/net/ssl/SSLHandshakeException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v4

    check-cast v4, Ljavax/net/ssl/SSLHandshakeException;

    throw v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 439
    .end local v24    # "e":Ljava/lang/Exception;
    :catchall_1
    move-exception v4

    :goto_3
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->sslSession:Lcom/android/org/conscrypt/OpenSSLSessionImpl;

    if-nez v5, :cond_d

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-eqz v5, :cond_d

    .line 440
    invoke-static {v6, v7}, Lcom/android/org/conscrypt/NativeCrypto;->SSL_SESSION_free(J)V

    .line 442
    :cond_d
    invoke-virtual/range {v31 .. v31}, Lcom/android/org/conscrypt/OpenSSLBIOSource;->release()V

    throw v4

    .line 444
    .end local v6    # "sslSessionCtx":J
    .end local v31    # "source":Lcom/android/org/conscrypt/OpenSSLBIOSource;
    :cond_e
    sget-object v4, Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;->NOT_HANDSHAKING:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    move-object/from16 v0, v25

    if-eq v0, v4, :cond_f

    .line 445
    new-instance v4, Ljavax/net/ssl/SSLEngineResult;

    sget-object v5, Ljavax/net/ssl/SSLEngineResult$Status;->OK:Ljavax/net/ssl/SSLEngineResult$Status;

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v0, v25

    invoke-direct {v4, v5, v0, v8, v9}, Ljavax/net/ssl/SSLEngineResult;-><init>(Ljavax/net/ssl/SSLEngineResult$Status;Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;II)V

    goto/16 :goto_1

    .line 448
    :cond_f
    if-nez v23, :cond_10

    .line 449
    new-instance v4, Ljavax/net/ssl/SSLEngineResult;

    sget-object v5, Ljavax/net/ssl/SSLEngineResult$Status;->BUFFER_OVERFLOW:Ljavax/net/ssl/SSLEngineResult$Status;

    invoke-virtual/range {p0 .. p0}, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->getHandshakeStatus()Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-direct {v4, v5, v8, v9, v10}, Ljavax/net/ssl/SSLEngineResult;-><init>(Ljavax/net/ssl/SSLEngineResult$Status;Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;II)V

    goto/16 :goto_1

    .line 452
    :cond_10
    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->duplicate()Ljava/nio/ByteBuffer;

    move-result-object v32

    .line 453
    .local v32, "srcDuplicate":Ljava/nio/ByteBuffer;
    invoke-static/range {v32 .. v32}, Lcom/android/org/conscrypt/OpenSSLBIOSource;->wrap(Ljava/nio/ByteBuffer;)Lcom/android/org/conscrypt/OpenSSLBIOSource;

    move-result-object v31

    .line 455
    .restart local v31    # "source":Lcom/android/org/conscrypt/OpenSSLBIOSource;
    :try_start_5
    invoke-virtual/range {v32 .. v32}, Ljava/nio/ByteBuffer;->position()I

    move-result v28

    .line 456
    .local v28, "positionBeforeRead":I
    const/16 v29, 0x0

    .line 457
    .local v29, "produced":I
    const/16 v30, 0x0

    .line 459
    .local v30, "shouldStop":Z
    :cond_11
    :goto_4
    if-nez v30, :cond_15

    .line 460
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move/from16 v2, p3

    move/from16 v3, p4

    invoke-direct {v0, v1, v2, v3}, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->getNextAvailableByteBuffer([Ljava/nio/ByteBuffer;II)Ljava/nio/ByteBuffer;

    move-result-object v22

    .line 461
    .restart local v22    # "dst":Ljava/nio/ByteBuffer;
    if-nez v22, :cond_12

    .line 462
    const/16 v30, 0x1

    .line 463
    goto :goto_4

    .line 465
    :cond_12
    move-object/from16 v19, v22

    .line 466
    .local v19, "arrayDst":Ljava/nio/ByteBuffer;
    invoke-virtual/range {v22 .. v22}, Ljava/nio/ByteBuffer;->isDirect()Z

    move-result v4

    if-eqz v4, :cond_13

    .line 467
    invoke-virtual/range {v22 .. v22}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v19

    .line 470
    :cond_13
    invoke-virtual/range {v19 .. v19}, Ljava/nio/ByteBuffer;->arrayOffset()I

    move-result v4

    invoke-virtual/range {v19 .. v19}, Ljava/nio/ByteBuffer;->position()I

    move-result v5

    add-int v12, v4, v5

    .line 472
    .local v12, "dstOffset":I
    move-object/from16 v0, p0

    iget-wide v9, v0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->sslNativePointer:J

    invoke-virtual/range {v19 .. v19}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v11

    invoke-virtual/range {v22 .. v22}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v13

    invoke-virtual/range {v31 .. v31}, Lcom/android/org/conscrypt/OpenSSLBIOSource;->getContext()J

    move-result-wide v14

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->localToRemoteSink:Lcom/android/org/conscrypt/OpenSSLBIOSink;

    invoke-virtual {v4}, Lcom/android/org/conscrypt/OpenSSLBIOSink;->getContext()J

    move-result-wide v16

    move-object/from16 v18, p0

    invoke-static/range {v9 .. v18}, Lcom/android/org/conscrypt/NativeCrypto;->SSL_read_BIO(J[BIIJJLcom/android/org/conscrypt/NativeCrypto$SSLHandshakeCallbacks;)I

    move-result v27

    .line 475
    .local v27, "internalProduced":I
    if-gtz v27, :cond_14

    .line 476
    const/16 v30, 0x1

    .line 477
    goto :goto_4

    .line 479
    :cond_14
    invoke-virtual/range {v19 .. v19}, Ljava/nio/ByteBuffer;->position()I

    move-result v4

    add-int v4, v4, v27

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 480
    add-int v29, v29, v27

    .line 481
    move-object/from16 v0, v22

    move-object/from16 v1, v19

    if-eq v0, v1, :cond_11

    .line 482
    invoke-virtual/range {v19 .. v19}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 483
    move-object/from16 v0, v22

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    goto :goto_4

    .line 490
    .end local v12    # "dstOffset":I
    .end local v19    # "arrayDst":Ljava/nio/ByteBuffer;
    .end local v22    # "dst":Ljava/nio/ByteBuffer;
    .end local v27    # "internalProduced":I
    .end local v28    # "positionBeforeRead":I
    .end local v29    # "produced":I
    .end local v30    # "shouldStop":Z
    :catch_1
    move-exception v24

    .line 491
    .local v24, "e":Ljava/io/IOException;
    :try_start_6
    new-instance v4, Ljavax/net/ssl/SSLException;

    move-object/from16 v0, v24

    invoke-direct {v4, v0}, Ljavax/net/ssl/SSLException;-><init>(Ljava/lang/Throwable;)V

    throw v4
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 493
    .end local v24    # "e":Ljava/io/IOException;
    :catchall_2
    move-exception v4

    invoke-virtual/range {v31 .. v31}, Lcom/android/org/conscrypt/OpenSSLBIOSource;->release()V

    throw v4

    .line 487
    .restart local v28    # "positionBeforeRead":I
    .restart local v29    # "produced":I
    .restart local v30    # "shouldStop":Z
    :cond_15
    :try_start_7
    invoke-virtual/range {v32 .. v32}, Ljava/nio/ByteBuffer;->position()I

    move-result v4

    sub-int v21, v4, v28

    .line 488
    .local v21, "consumed":I
    invoke-virtual/range {v32 .. v32}, Ljava/nio/ByteBuffer;->position()I

    move-result v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 489
    new-instance v4, Ljavax/net/ssl/SSLEngineResult;

    sget-object v5, Ljavax/net/ssl/SSLEngineResult$Status;->OK:Ljavax/net/ssl/SSLEngineResult$Status;

    invoke-virtual/range {p0 .. p0}, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->getHandshakeStatus()Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    move-result-object v8

    move/from16 v0, v21

    move/from16 v1, v29

    invoke-direct {v4, v5, v8, v0, v1}, Ljavax/net/ssl/SSLEngineResult;-><init>(Ljavax/net/ssl/SSLEngineResult$Status;Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;II)V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 493
    invoke-virtual/range {v31 .. v31}, Lcom/android/org/conscrypt/OpenSSLBIOSource;->release()V

    goto/16 :goto_1

    .line 439
    .end local v21    # "consumed":I
    .end local v28    # "positionBeforeRead":I
    .end local v29    # "produced":I
    .end local v30    # "shouldStop":Z
    .end local v32    # "srcDuplicate":Ljava/nio/ByteBuffer;
    .restart local v34    # "sslSessionCtx":J
    :catchall_3
    move-exception v4

    move-wide/from16 v6, v34

    .end local v34    # "sslSessionCtx":J
    .restart local v6    # "sslSessionCtx":J
    goto/16 :goto_3

    .line 435
    :catch_2
    move-exception v24

    goto/16 :goto_2
.end method

.method public verifyCertificateChain(J[JLjava/lang/String;)V
    .locals 15
    .param p1, "sslSessionNativePtr"    # J
    .param p3, "certRefs"    # [J
    .param p4, "authMethod"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/cert/CertificateException;
        }
    .end annotation

    .prologue
    .line 642
    :try_start_0
    iget-object v3, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->sslParameters:Lcom/android/org/conscrypt/SSLParametersImpl;

    invoke-virtual {v3}, Lcom/android/org/conscrypt/SSLParametersImpl;->getX509TrustManager()Ljavax/net/ssl/X509TrustManager;

    move-result-object v14

    .line 643
    .local v14, "x509tm":Ljavax/net/ssl/X509TrustManager;
    if-nez v14, :cond_0

    .line 644
    new-instance v3, Ljava/security/cert/CertificateException;

    const-string v4, "No X.509 TrustManager"

    invoke-direct {v3, v4}, Ljava/security/cert/CertificateException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catch Ljava/security/cert/CertificateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 665
    .end local v14    # "x509tm":Ljavax/net/ssl/X509TrustManager;
    :catch_0
    move-exception v12

    .line 666
    .local v12, "e":Ljava/security/cert/CertificateException;
    :try_start_1
    throw v12
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 671
    .end local v12    # "e":Ljava/security/cert/CertificateException;
    :catchall_0
    move-exception v3

    const/4 v4, 0x0

    iput-object v4, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->handshakeSession:Lcom/android/org/conscrypt/OpenSSLSessionImpl;

    throw v3

    .line 646
    .restart local v14    # "x509tm":Ljavax/net/ssl/X509TrustManager;
    :cond_0
    if-eqz p3, :cond_1

    :try_start_2
    move-object/from16 v0, p3

    array-length v3, v0

    if-nez v3, :cond_2

    .line 647
    :cond_1
    new-instance v3, Ljavax/net/ssl/SSLException;

    const-string v4, "Peer sent no certificate"

    invoke-direct {v3, v4}, Ljavax/net/ssl/SSLException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_2
    .catch Ljava/security/cert/CertificateException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 667
    .end local v14    # "x509tm":Ljavax/net/ssl/X509TrustManager;
    :catch_1
    move-exception v12

    .line 668
    .local v12, "e":Ljava/lang/Exception;
    :try_start_3
    new-instance v3, Ljava/security/cert/CertificateException;

    invoke-direct {v3, v12}, Ljava/security/cert/CertificateException;-><init>(Ljava/lang/Throwable;)V

    throw v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 649
    .end local v12    # "e":Ljava/lang/Exception;
    .restart local v14    # "x509tm":Ljavax/net/ssl/X509TrustManager;
    :cond_2
    :try_start_4
    move-object/from16 v0, p3

    array-length v3, v0

    new-array v7, v3, [Lcom/android/org/conscrypt/OpenSSLX509Certificate;

    .line 650
    .local v7, "peerCertChain":[Lcom/android/org/conscrypt/OpenSSLX509Certificate;
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_0
    move-object/from16 v0, p3

    array-length v3, v0

    if-ge v13, v3, :cond_3

    .line 651
    new-instance v3, Lcom/android/org/conscrypt/OpenSSLX509Certificate;

    aget-wide v4, p3, v13

    invoke-direct {v3, v4, v5}, Lcom/android/org/conscrypt/OpenSSLX509Certificate;-><init>(J)V

    aput-object v3, v7, v13

    .line 650
    add-int/lit8 v13, v13, 0x1

    goto :goto_0

    .line 655
    :cond_3
    new-instance v3, Lcom/android/org/conscrypt/OpenSSLSessionImpl;

    const/4 v6, 0x0

    invoke-virtual {p0}, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->getPeerHost()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0}, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->getPeerPort()I

    move-result v9

    const/4 v10, 0x0

    move-wide/from16 v4, p1

    invoke-direct/range {v3 .. v10}, Lcom/android/org/conscrypt/OpenSSLSessionImpl;-><init>(J[Ljava/security/cert/X509Certificate;[Ljava/security/cert/X509Certificate;Ljava/lang/String;ILcom/android/org/conscrypt/AbstractSessionContext;)V

    iput-object v3, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->handshakeSession:Lcom/android/org/conscrypt/OpenSSLSessionImpl;

    .line 658
    iget-object v3, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->sslParameters:Lcom/android/org/conscrypt/SSLParametersImpl;

    invoke-virtual {v3}, Lcom/android/org/conscrypt/SSLParametersImpl;->getUseClientMode()Z

    move-result v11

    .line 659
    .local v11, "client":Z
    if-eqz v11, :cond_4

    .line 660
    invoke-virtual {p0}, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->getPeerHost()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p4

    invoke-static {v14, v7, v0, v3}, Lcom/android/org/conscrypt/Platform;->checkServerTrusted(Ljavax/net/ssl/X509TrustManager;[Ljava/security/cert/X509Certificate;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/security/cert/CertificateException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 671
    :goto_1
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->handshakeSession:Lcom/android/org/conscrypt/OpenSSLSessionImpl;

    .line 673
    return-void

    .line 662
    :cond_4
    const/4 v3, 0x0

    :try_start_5
    aget-object v3, v7, v3

    invoke-virtual {v3}, Lcom/android/org/conscrypt/OpenSSLX509Certificate;->getPublicKey()Ljava/security/PublicKey;

    move-result-object v3

    invoke-interface {v3}, Ljava/security/PublicKey;->getAlgorithm()Ljava/lang/String;

    move-result-object v2

    .line 663
    .local v2, "authType":Ljava/lang/String;
    invoke-interface {v14, v7, v2}, Ljavax/net/ssl/X509TrustManager;->checkClientTrusted([Ljava/security/cert/X509Certificate;Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/security/cert/CertificateException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1
.end method

.method public wrap([Ljava/nio/ByteBuffer;IILjava/nio/ByteBuffer;)Ljavax/net/ssl/SSLEngineResult;
    .locals 24
    .param p1, "srcs"    # [Ljava/nio/ByteBuffer;
    .param p2, "offset"    # I
    .param p3, "length"    # I
    .param p4, "dst"    # Ljava/nio/ByteBuffer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/net/ssl/SSLException;
        }
    .end annotation

    .prologue
    .line 510
    if-nez p1, :cond_0

    .line 511
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "srcs == null"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 512
    :cond_0
    if-nez p4, :cond_1

    .line 513
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "dst == null"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 514
    :cond_1
    invoke-virtual/range {p4 .. p4}, Ljava/nio/ByteBuffer;->isReadOnly()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 515
    new-instance v2, Ljava/nio/ReadOnlyBufferException;

    invoke-direct {v2}, Ljava/nio/ReadOnlyBufferException;-><init>()V

    throw v2

    .line 517
    :cond_2
    move-object/from16 v13, p1

    .local v13, "arr$":[Ljava/nio/ByteBuffer;
    array-length v0, v13

    move/from16 v18, v0

    .local v18, "len$":I
    const/16 v17, 0x0

    .local v17, "i$":I
    :goto_0
    move/from16 v0, v17

    move/from16 v1, v18

    if-ge v0, v1, :cond_4

    aget-object v20, v13, v17

    .line 518
    .local v20, "src":Ljava/nio/ByteBuffer;
    if-nez v20, :cond_3

    .line 519
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "one of the src == null"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 517
    :cond_3
    add-int/lit8 v17, v17, 0x1

    goto :goto_0

    .line 522
    .end local v20    # "src":Ljava/nio/ByteBuffer;
    :cond_4
    move-object/from16 v0, p1

    array-length v2, v0

    move/from16 v0, p2

    move/from16 v1, p3

    invoke-static {v2, v0, v1}, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->checkIndex(III)V

    .line 524
    invoke-virtual/range {p4 .. p4}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v2

    const/16 v3, 0x4145

    if-ge v2, v3, :cond_5

    .line 525
    new-instance v2, Ljavax/net/ssl/SSLEngineResult;

    sget-object v3, Ljavax/net/ssl/SSLEngineResult$Status;->BUFFER_OVERFLOW:Ljavax/net/ssl/SSLEngineResult$Status;

    invoke-virtual/range {p0 .. p0}, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->getHandshakeStatus()Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v10, 0x0

    invoke-direct {v2, v3, v6, v7, v10}, Ljavax/net/ssl/SSLEngineResult;-><init>(Ljavax/net/ssl/SSLEngineResult$Status;Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;II)V

    .line 592
    :goto_1
    return-object v2

    .line 528
    :cond_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->stateLock:Ljava/lang/Object;

    monitor-enter v3

    .line 530
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    sget-object v6, Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;->CLOSED:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    if-eq v2, v6, :cond_6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    sget-object v6, Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;->CLOSED_OUTBOUND:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    if-ne v2, v6, :cond_7

    .line 531
    :cond_6
    new-instance v2, Ljavax/net/ssl/SSLEngineResult;

    sget-object v6, Ljavax/net/ssl/SSLEngineResult$Status;->CLOSED:Ljavax/net/ssl/SSLEngineResult$Status;

    invoke-virtual/range {p0 .. p0}, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->getHandshakeStatus()Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    move-result-object v7

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-direct {v2, v6, v7, v10, v11}, Ljavax/net/ssl/SSLEngineResult;-><init>(Ljavax/net/ssl/SSLEngineResult$Status;Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;II)V

    monitor-exit v3

    goto :goto_1

    .line 536
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 533
    :cond_7
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    sget-object v6, Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;->NEW:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    if-eq v2, v6, :cond_8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    sget-object v6, Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;->MODE_SET:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    if-ne v2, v6, :cond_9

    .line 534
    :cond_8
    invoke-virtual/range {p0 .. p0}, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->beginHandshake()V

    .line 536
    :cond_9
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 539
    invoke-virtual/range {p0 .. p0}, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->getHandshakeStatus()Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    move-result-object v16

    .line 540
    .local v16, "handshakeStatus":Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;
    sget-object v2, Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;->NEED_WRAP:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    move-object/from16 v0, v16

    if-ne v0, v2, :cond_e

    .line 541
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->handshakeSink:Lcom/android/org/conscrypt/OpenSSLBIOSink;

    invoke-virtual {v2}, Lcom/android/org/conscrypt/OpenSSLBIOSink;->available()I

    move-result v2

    if-nez v2, :cond_c

    .line 542
    const-wide/16 v22, 0x0

    .line 544
    .local v22, "sslSessionCtx":J
    :try_start_2
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->sslNativePointer:J

    sget-object v6, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->nullSource:Lcom/android/org/conscrypt/OpenSSLBIOSource;

    invoke-virtual {v6}, Lcom/android/org/conscrypt/OpenSSLBIOSource;->getContext()J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->handshakeSink:Lcom/android/org/conscrypt/OpenSSLBIOSink;

    invoke-virtual {v6}, Lcom/android/org/conscrypt/OpenSSLBIOSink;->getContext()J

    move-result-wide v6

    invoke-virtual/range {p0 .. p0}, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->getUseClientMode()Z

    move-result v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->sslParameters:Lcom/android/org/conscrypt/SSLParametersImpl;

    iget-object v10, v10, Lcom/android/org/conscrypt/SSLParametersImpl;->npnProtocols:[B

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->sslParameters:Lcom/android/org/conscrypt/SSLParametersImpl;

    iget-object v11, v11, Lcom/android/org/conscrypt/SSLParametersImpl;->alpnProtocols:[B

    move-object/from16 v8, p0

    invoke-static/range {v2 .. v11}, Lcom/android/org/conscrypt/NativeCrypto;->SSL_do_handshake_bio(JJJLcom/android/org/conscrypt/NativeCrypto$SSLHandshakeCallbacks;Z[B[B)J
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result-wide v4

    .line 548
    .end local v22    # "sslSessionCtx":J
    .local v4, "sslSessionCtx":J
    const-wide/16 v2, 0x0

    cmp-long v2, v4, v2

    if-eqz v2, :cond_b

    .line 549
    :try_start_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->sslSession:Lcom/android/org/conscrypt/OpenSSLSessionImpl;

    if-eqz v2, :cond_a

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    sget-object v3, Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;->HANDSHAKE_STARTED:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    if-ne v2, v3, :cond_a

    .line 550
    sget-object v2, Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;->READY_HANDSHAKE_CUT_THROUGH:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->engineState:Lcom/android/org/conscrypt/OpenSSLEngineImpl$EngineState;

    .line 552
    :cond_a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->sslParameters:Lcom/android/org/conscrypt/SSLParametersImpl;

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->sslNativePointer:J

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->sslSession:Lcom/android/org/conscrypt/OpenSSLSessionImpl;

    invoke-virtual/range {p0 .. p0}, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->getPeerHost()Ljava/lang/String;

    move-result-object v9

    invoke-virtual/range {p0 .. p0}, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->getPeerPort()I

    move-result v10

    const/4 v11, 0x1

    invoke-virtual/range {v3 .. v11}, Lcom/android/org/conscrypt/SSLParametersImpl;->setupSession(JJLcom/android/org/conscrypt/OpenSSLSessionImpl;Ljava/lang/String;IZ)Lcom/android/org/conscrypt/OpenSSLSessionImpl;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->sslSession:Lcom/android/org/conscrypt/OpenSSLSessionImpl;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 559
    :cond_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->sslSession:Lcom/android/org/conscrypt/OpenSSLSessionImpl;

    if-nez v2, :cond_c

    const-wide/16 v2, 0x0

    cmp-long v2, v4, v2

    if-eqz v2, :cond_c

    .line 560
    invoke-static {v4, v5}, Lcom/android/org/conscrypt/NativeCrypto;->SSL_SESSION_free(J)V

    .line 564
    .end local v4    # "sslSessionCtx":J
    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->handshakeSink:Lcom/android/org/conscrypt/OpenSSLBIOSink;

    move-object/from16 v0, p4

    invoke-static {v2, v0}, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->writeSinkToByteBuffer(Lcom/android/org/conscrypt/OpenSSLBIOSink;Ljava/nio/ByteBuffer;)I

    move-result v14

    .line 565
    .local v14, "bytesWritten":I
    new-instance v2, Ljavax/net/ssl/SSLEngineResult;

    sget-object v3, Ljavax/net/ssl/SSLEngineResult$Status;->OK:Ljavax/net/ssl/SSLEngineResult$Status;

    invoke-virtual/range {p0 .. p0}, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->getHandshakeStatus()Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    move-result-object v6

    const/4 v7, 0x0

    invoke-direct {v2, v3, v6, v7, v14}, Ljavax/net/ssl/SSLEngineResult;-><init>(Ljavax/net/ssl/SSLEngineResult$Status;Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;II)V

    goto/16 :goto_1

    .line 555
    .end local v14    # "bytesWritten":I
    .restart local v22    # "sslSessionCtx":J
    :catch_0
    move-exception v15

    move-wide/from16 v4, v22

    .line 556
    .end local v22    # "sslSessionCtx":J
    .restart local v4    # "sslSessionCtx":J
    .local v15, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_4
    new-instance v2, Ljavax/net/ssl/SSLHandshakeException;

    const-string v3, "Handshake failed"

    invoke-direct {v2, v3}, Ljavax/net/ssl/SSLHandshakeException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v15}, Ljavax/net/ssl/SSLHandshakeException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v2

    check-cast v2, Ljavax/net/ssl/SSLHandshakeException;

    throw v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 559
    .end local v15    # "e":Ljava/lang/Exception;
    :catchall_1
    move-exception v2

    :goto_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->sslSession:Lcom/android/org/conscrypt/OpenSSLSessionImpl;

    if-nez v3, :cond_d

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-eqz v3, :cond_d

    .line 560
    invoke-static {v4, v5}, Lcom/android/org/conscrypt/NativeCrypto;->SSL_SESSION_free(J)V

    :cond_d
    throw v2

    .line 566
    .end local v4    # "sslSessionCtx":J
    :cond_e
    sget-object v2, Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;->NOT_HANDSHAKING:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    move-object/from16 v0, v16

    if-eq v0, v2, :cond_f

    .line 567
    new-instance v2, Ljavax/net/ssl/SSLEngineResult;

    sget-object v3, Ljavax/net/ssl/SSLEngineResult$Status;->OK:Ljavax/net/ssl/SSLEngineResult$Status;

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, v16

    invoke-direct {v2, v3, v0, v6, v7}, Ljavax/net/ssl/SSLEngineResult;-><init>(Ljavax/net/ssl/SSLEngineResult$Status;Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;II)V

    goto/16 :goto_1

    .line 571
    :cond_f
    const/16 v21, 0x0

    .line 572
    .local v21, "totalRead":I
    const/4 v8, 0x0

    .line 574
    .local v8, "buffer":[B
    move-object/from16 v13, p1

    :try_start_5
    array-length v0, v13

    move/from16 v18, v0

    const/16 v17, 0x0

    :goto_4
    move/from16 v0, v17

    move/from16 v1, v18

    if-ge v0, v1, :cond_13

    aget-object v20, v13, v17

    .line 575
    .restart local v20    # "src":Ljava/nio/ByteBuffer;
    invoke-virtual/range {v20 .. v20}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v9

    .line 576
    .local v9, "toRead":I
    if-eqz v8, :cond_10

    array-length v2, v8

    if-le v9, v2, :cond_11

    .line 577
    :cond_10
    new-array v8, v9, [B

    .line 583
    :cond_11
    invoke-virtual/range {v20 .. v20}, Ljava/nio/ByteBuffer;->duplicate()Ljava/nio/ByteBuffer;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v8, v3, v9}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 584
    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->sslNativePointer:J

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->localToRemoteSink:Lcom/android/org/conscrypt/OpenSSLBIOSink;

    invoke-virtual {v2}, Lcom/android/org/conscrypt/OpenSSLBIOSink;->getContext()J

    move-result-wide v10

    move-object/from16 v12, p0

    invoke-static/range {v6 .. v12}, Lcom/android/org/conscrypt/NativeCrypto;->SSL_write_BIO(J[BIJLcom/android/org/conscrypt/NativeCrypto$SSLHandshakeCallbacks;)I

    move-result v19

    .line 586
    .local v19, "numRead":I
    if-lez v19, :cond_12

    .line 587
    invoke-virtual/range {v20 .. v20}, Ljava/nio/ByteBuffer;->position()I

    move-result v2

    add-int v2, v2, v19

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 588
    add-int v21, v21, v19

    .line 574
    :cond_12
    add-int/lit8 v17, v17, 0x1

    goto :goto_4

    .line 592
    .end local v9    # "toRead":I
    .end local v19    # "numRead":I
    .end local v20    # "src":Ljava/nio/ByteBuffer;
    :cond_13
    new-instance v2, Ljavax/net/ssl/SSLEngineResult;

    sget-object v3, Ljavax/net/ssl/SSLEngineResult$Status;->OK:Ljavax/net/ssl/SSLEngineResult$Status;

    invoke-virtual/range {p0 .. p0}, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->getHandshakeStatus()Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->localToRemoteSink:Lcom/android/org/conscrypt/OpenSSLBIOSink;

    move-object/from16 v0, p4

    invoke-static {v7, v0}, Lcom/android/org/conscrypt/OpenSSLEngineImpl;->writeSinkToByteBuffer(Lcom/android/org/conscrypt/OpenSSLBIOSink;Ljava/nio/ByteBuffer;)I

    move-result v7

    move/from16 v0, v21

    invoke-direct {v2, v3, v6, v0, v7}, Ljavax/net/ssl/SSLEngineResult;-><init>(Ljavax/net/ssl/SSLEngineResult$Status;Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;II)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    goto/16 :goto_1

    .line 594
    :catch_1
    move-exception v15

    .line 595
    .local v15, "e":Ljava/io/IOException;
    new-instance v2, Ljavax/net/ssl/SSLException;

    invoke-direct {v2, v15}, Ljavax/net/ssl/SSLException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    .line 559
    .end local v8    # "buffer":[B
    .end local v15    # "e":Ljava/io/IOException;
    .end local v21    # "totalRead":I
    .restart local v22    # "sslSessionCtx":J
    :catchall_2
    move-exception v2

    move-wide/from16 v4, v22

    .end local v22    # "sslSessionCtx":J
    .restart local v4    # "sslSessionCtx":J
    goto/16 :goto_3

    .line 555
    :catch_2
    move-exception v15

    goto/16 :goto_2
.end method
