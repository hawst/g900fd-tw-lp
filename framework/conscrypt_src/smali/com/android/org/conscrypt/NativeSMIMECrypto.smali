.class public Lcom/android/org/conscrypt/NativeSMIMECrypto;
.super Ljava/lang/Object;
.source "NativeSMIMECrypto.java"


# static fields
.field public static final DO_CERTIFICATE_VERIFICATION:I = 0x1000

.field public static final DO_CRL_CHECK:I = 0x2000

.field public static final DO_OCSP_CHECK:I = 0x4000

.field public static final ENCRYPTION_ALGORITHM_3DES:Ljava/lang/String; = "-des3"

.field public static final ENCRYPTION_ALGORITHM_AES128:Ljava/lang/String; = "-aes-128"

.field public static final ENCRYPTION_ALGORITHM_AES192:Ljava/lang/String; = "-aes-192"

.field public static final ENCRYPTION_ALGORITHM_AES256:Ljava/lang/String; = "-aes-256"

.field public static final ENCRYPTION_ALGORITHM_DES:Ljava/lang/String; = "-des"

.field public static final ENCRYPTION_ALGORITHM_RC2128:Ljava/lang/String; = "-rc2-128"

.field public static final ENCRYPTION_ALGORITHM_RC240:Ljava/lang/String; = "-rc2-40"

.field public static final ENCRYPTION_ALGORITHM_RC264:Ljava/lang/String; = "-rc2-64"

.field public static final OCSP_ERROR_CODE_BASE:I = 0x3e8

.field public static final OpenSSLProviderName:Ljava/lang/String; = "AndroidOpenSSL"

.field public static final SIGNING_ALGORITHM_ECDSA:Ljava/lang/String; = "-ecdsa"

.field public static final SIGNING_ALGORITHM_MD5:Ljava/lang/String; = "-md5"

.field public static final SIGNING_ALGORITHM_SHA:Ljava/lang/String; = "-sha1"

.field public static final SIGNING_ALGORITHM_SHA256:Ljava/lang/String; = "-sha-256"

.field public static final SIGNING_ALGORITHM_SHA384:Ljava/lang/String; = "-sha-384"

.field public static final SPC_OCSPRESULT_CERTIFICATE_REVOKED:I = 0x3e8

.field public static final SPC_OCSPRESULT_CERTIFICATE_VALID:I = 0x3e9

.field public static final SPC_OCSPRESULT_ERROR_BADOCSPADDRESS:I = 0x3f1

.field public static final SPC_OCSPRESULT_ERROR_CONNECTFAILURE:I = 0x3f3

.field public static final SPC_OCSPRESULT_ERROR_INTERNALERROR:I = 0x3eb

.field public static final SPC_OCSPRESULT_ERROR_INVALIDRESPONSE:I = 0x3f4

.field public static final SPC_OCSPRESULT_ERROR_MALFORMEDREQUEST:I = 0x3ea

.field public static final SPC_OCSPRESULT_ERROR_OUTOFMEMORY:I = 0x3f0

.field public static final SPC_OCSPRESULT_ERROR_SIGNFAILURE:I = 0x3f2

.field public static final SPC_OCSPRESULT_ERROR_SIGREQUIRED:I = 0x3ed

.field public static final SPC_OCSPRESULT_ERROR_TRYLATER:I = 0x3ec

.field public static final SPC_OCSPRESULT_ERROR_UNAUTHORIZED:I = 0x3ee

.field public static final SPC_OCSPRESULT_ERROR_UNKNOWN:I = 0x3ef

.field private static final TAG:Ljava/lang/String; = "NativeSMIMECrypto"

.field public static final X509_V_ERR_AKID_ISSUER_SERIAL_MISMATCH:I = 0x1f

.field public static final X509_V_ERR_AKID_SKID_MISMATCH:I = 0x1e

.field public static final X509_V_ERR_APPLICATION_VERIFICATION:I = 0x32

.field public static final X509_V_ERR_CERT_CHAIN_TOO_LONG:I = 0x16

.field public static final X509_V_ERR_CERT_HAS_EXPIRED:I = 0xa

.field public static final X509_V_ERR_CERT_NOT_YET_VALID:I = 0x9

.field public static final X509_V_ERR_CERT_REJECTED:I = 0x1c

.field public static final X509_V_ERR_CERT_REVOKED:I = 0x17

.field public static final X509_V_ERR_CERT_SIGNATURE_FAILURE:I = 0x7

.field public static final X509_V_ERR_CERT_UNTRUSTED:I = 0x1b

.field public static final X509_V_ERR_CRL_HAS_EXPIRED:I = 0xc

.field public static final X509_V_ERR_CRL_NOT_YET_VALID:I = 0xb

.field public static final X509_V_ERR_CRL_PATH_VALIDATION_ERROR:I = 0x36

.field public static final X509_V_ERR_CRL_SIGNATURE_FAILURE:I = 0x8

.field public static final X509_V_ERR_DEPTH_ZERO_SELF_SIGNED_CERT:I = 0x12

.field public static final X509_V_ERR_DIFFERENT_CRL_SCOPE:I = 0x2c

.field public static final X509_V_ERR_ERROR_IN_CERT_NOT_AFTER_FIELD:I = 0xe

.field public static final X509_V_ERR_ERROR_IN_CERT_NOT_BEFORE_FIELD:I = 0xd

.field public static final X509_V_ERR_ERROR_IN_CRL_LAST_UPDATE_FIELD:I = 0xf

.field public static final X509_V_ERR_ERROR_IN_CRL_NEXT_UPDATE_FIELD:I = 0x10

.field public static final X509_V_ERR_EXCLUDED_VIOLATION:I = 0x30

.field public static final X509_V_ERR_INVALID_CA:I = 0x18

.field public static final X509_V_ERR_INVALID_EXTENSION:I = 0x29

.field public static final X509_V_ERR_INVALID_NON_CA:I = 0x25

.field public static final X509_V_ERR_INVALID_POLICY_EXTENSION:I = 0x2a

.field public static final X509_V_ERR_INVALID_PURPOSE:I = 0x1a

.field public static final X509_V_ERR_KEYUSAGE_NO_CERTSIGN:I = 0x20

.field public static final X509_V_ERR_KEYUSAGE_NO_CRL_SIGN:I = 0x23

.field public static final X509_V_ERR_KEYUSAGE_NO_DIGITAL_SIGNATURE:I = 0x27

.field public static final X509_V_ERR_NO_EXPLICIT_POLICY:I = 0x2b

.field public static final X509_V_ERR_OUT_OF_MEM:I = 0x11

.field public static final X509_V_ERR_PATH_LENGTH_EXCEEDED:I = 0x19

.field public static final X509_V_ERR_PERMITTED_VIOLATION:I = 0x2f

.field public static final X509_V_ERR_PROXY_CERTIFICATES_NOT_ALLOWED:I = 0x28

.field public static final X509_V_ERR_PROXY_PATH_LENGTH_EXCEEDED:I = 0x26

.field public static final X509_V_ERR_SELF_SIGNED_CERT_IN_CHAIN:I = 0x13

.field public static final X509_V_ERR_SUBJECT_ISSUER_MISMATCH:I = 0x1d

.field public static final X509_V_ERR_SUBTREE_MINMAX:I = 0x31

.field public static final X509_V_ERR_UNABLE_TO_DECODE_ISSUER_PUBLIC_KEY:I = 0x6

.field public static final X509_V_ERR_UNABLE_TO_DECRYPT_CERT_SIGNATURE:I = 0x4

.field public static final X509_V_ERR_UNABLE_TO_DECRYPT_CRL_SIGNATURE:I = 0x5

.field public static final X509_V_ERR_UNABLE_TO_GET_CRL:I = 0x3

.field public static final X509_V_ERR_UNABLE_TO_GET_CRL_ISSUER:I = 0x21

.field public static final X509_V_ERR_UNABLE_TO_GET_ISSUER_CERT:I = 0x2

.field public static final X509_V_ERR_UNABLE_TO_GET_ISSUER_CERT_LOCALLY:I = 0x14

.field public static final X509_V_ERR_UNABLE_TO_VERIFY_LEAF_SIGNATURE:I = 0x15

.field public static final X509_V_ERR_UNHANDLED_CRITICAL_CRL_EXTENSION:I = 0x24

.field public static final X509_V_ERR_UNHANDLED_CRITICAL_EXTENSION:I = 0x22

.field public static final X509_V_ERR_UNNESTED_RESOURCE:I = 0x2e

.field public static final X509_V_ERR_UNSUPPORTED_CONSTRAINT_SYNTAX:I = 0x34

.field public static final X509_V_ERR_UNSUPPORTED_CONSTRAINT_TYPE:I = 0x33

.field public static final X509_V_ERR_UNSUPPORTED_EXTENSION_FEATURE:I = 0x2d

.field public static final X509_V_ERR_UNSUPPORTED_NAME_SYNTAX:I = 0x35

.field public static final X509_V_OK:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 141
    const-string v0, "com.android.org.conscrypt"

    const-class v1, Lcom/android/org/conscrypt/NativeSMIMECrypto;

    invoke-virtual {v1}, Ljava/lang/Class;->getPackage()Ljava/lang/Package;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Package;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 142
    const-string v0, "javacrypto"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 147
    :goto_0
    invoke-static {}, Lcom/android/org/conscrypt/NativeSMIMECrypto;->SMIMEclinit()V

    .line 148
    return-void

    .line 144
    :cond_0
    const-string v0, "conscrypt_jni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static native PKCS7decrypt(Ljava/lang/String;JJLjava/lang/String;Z)Z
.end method

.method private static native PKCS7encrypt(J[JLjava/lang/String;Ljava/lang/String;)Z
.end method

.method private static native PKCS7sign(Ljava/lang/String;Ljava/lang/String;JJ[JLjava/lang/String;)Z
.end method

.method private static native PKCS7verify(Ljava/lang/String;Ljava/lang/String;[J)[J
.end method

.method private static native SMIMEclinit()V
.end method

.method private static native getCDP(J)[B
.end method

.method public static getCRLDistributionPoint([B)Ljava/lang/String;
    .locals 11
    .param p0, "PEMcertificateBytes"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/cert/CertificateEncodingException;,
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 270
    const/4 v6, 0x0

    .line 271
    .local v6, "crl_url":Ljava/lang/String;
    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, p0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 272
    .local v1, "certIs":Ljava/io/InputStream;
    invoke-static {v1}, Lcom/android/org/conscrypt/OpenSSLX509Certificate;->fromX509PemInputStream(Ljava/io/InputStream;)Lcom/android/org/conscrypt/OpenSSLX509Certificate;

    move-result-object v7

    .line 273
    .local v7, "openSSLcert":Lcom/android/org/conscrypt/OpenSSLX509Certificate;
    invoke-virtual {v7}, Lcom/android/org/conscrypt/OpenSSLX509Certificate;->getEncoded()[B

    move-result-object v8

    .line 274
    .local v8, "openSSLcertEncoded":[B
    invoke-static {v8}, Lcom/android/org/conscrypt/NativeCrypto;->d2i_X509([B)J

    move-result-wide v2

    .line 275
    .local v2, "certRef":J
    invoke-static {v2, v3}, Lcom/android/org/conscrypt/NativeSMIMECrypto;->getCDP(J)[B

    move-result-object v0

    .line 276
    .local v0, "cdp":[B
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v0}, Ljava/lang/String;-><init>([B)V

    .line 278
    .local v4, "crl":Ljava/lang/String;
    const-string v9, "http:"

    invoke-virtual {v4, v9}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 279
    .local v5, "crlSplit":[Ljava/lang/String;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "http:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const/4 v10, 0x1

    aget-object v10, v5, v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 283
    return-object v6
.end method

.method public static getCertificateChain(Ljava/security/cert/X509Certificate;)Ljava/util/List;
    .locals 2
    .param p0, "cert"    # Ljava/security/cert/X509Certificate;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/security/cert/X509Certificate;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/security/cert/X509Certificate;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 323
    new-instance v0, Lcom/android/org/conscrypt/TrustedCertificateStore;

    invoke-direct {v0}, Lcom/android/org/conscrypt/TrustedCertificateStore;-><init>()V

    .line 325
    .local v0, "store":Lcom/android/org/conscrypt/TrustedCertificateStore;
    if-nez v0, :cond_0

    .line 326
    const/4 v1, 0x0

    .line 328
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0, p0}, Lcom/android/org/conscrypt/TrustedCertificateStore;->getCertificateChain(Ljava/security/cert/X509Certificate;)Ljava/util/List;

    move-result-object v1

    goto :goto_0
.end method

.method public static openSSLPKCS7Sign(Ljava/io/File;Ljava/io/File;Ljava/security/PrivateKey;[BLjava/util/List;Ljava/lang/String;)Z
    .locals 22
    .param p0, "inputFile"    # Ljava/io/File;
    .param p1, "outputFile"    # Ljava/io/File;
    .param p2, "privateKey"    # Ljava/security/PrivateKey;
    .param p3, "PEMcertificateBytes"    # [B
    .param p5, "signingAlgorithm"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Ljava/io/File;",
            "Ljava/security/PrivateKey;",
            "[B",
            "Ljava/util/List",
            "<[B>;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Ljava/lang/NullPointerException;,
            Ljava/security/InvalidKeyException;,
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 192
    .local p4, "PEMcertificateChainBytes":Ljava/util/List;, "Ljava/util/List<[B>;"
    invoke-virtual/range {p0 .. p0}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    .line 193
    new-instance v3, Ljava/io/FileNotFoundException;

    const-string v19, "Input File does not exist."

    move-object/from16 v0, v19

    invoke-direct {v3, v0}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 194
    :cond_0
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_1

    .line 195
    new-instance v3, Ljava/io/FileNotFoundException;

    const-string v19, "Output File does not exist."

    move-object/from16 v0, v19

    invoke-direct {v3, v0}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 197
    :cond_1
    invoke-virtual/range {p0 .. p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    .line 198
    .local v2, "inputFilePath":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    .line 201
    .local v9, "outputFilePath":Ljava/lang/String;
    new-instance v12, Ljava/io/ByteArrayInputStream;

    move-object/from16 v0, p3

    invoke-direct {v12, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 202
    .local v12, "certIs":Ljava/io/InputStream;
    invoke-static {v12}, Lcom/android/org/conscrypt/OpenSSLX509Certificate;->fromX509PemInputStream(Ljava/io/InputStream;)Lcom/android/org/conscrypt/OpenSSLX509Certificate;

    move-result-object v15

    .line 203
    .local v15, "openSSLcert":Lcom/android/org/conscrypt/OpenSSLX509Certificate;
    invoke-virtual {v15}, Lcom/android/org/conscrypt/OpenSSLX509Certificate;->getEncoded()[B

    move-result-object v16

    .line 204
    .local v16, "openSSLcertEncoded":[B
    invoke-static/range {v16 .. v16}, Lcom/android/org/conscrypt/NativeCrypto;->d2i_X509([B)J

    move-result-wide v4

    .line 206
    .local v4, "signCertRef":J
    invoke-static/range {p2 .. p2}, Lcom/android/org/conscrypt/OpenSSLKey;->fromPrivateKey(Ljava/security/PrivateKey;)Lcom/android/org/conscrypt/OpenSSLKey;

    move-result-object v14

    .line 207
    .local v14, "oKey":Lcom/android/org/conscrypt/OpenSSLKey;
    invoke-virtual {v14}, Lcom/android/org/conscrypt/OpenSSLKey;->getPkeyContext()J

    move-result-wide v6

    .line 210
    .local v6, "evpKeyRef":J
    const/4 v3, 0x0

    new-array v8, v3, [J

    .line 211
    .local v8, "arr1":[J
    if-eqz p4, :cond_2

    .line 212
    invoke-interface/range {p4 .. p4}, Ljava/util/List;->size()I

    move-result v17

    .line 213
    .local v17, "size":I
    move/from16 v0, v17

    new-array v8, v0, [J

    .line 214
    const/16 v18, 0x0

    .local v18, "u":I
    :goto_0
    move/from16 v0, v18

    move/from16 v1, v17

    if-ge v0, v1, :cond_2

    .line 215
    move-object/from16 v0, p4

    move/from16 v1, v18

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, [B

    .line 216
    .local v10, "a":[B
    new-instance v11, Ljava/io/ByteArrayInputStream;

    invoke-direct {v11, v10}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 217
    .local v11, "cIs":Ljava/io/InputStream;
    invoke-static {v11}, Lcom/android/org/conscrypt/OpenSSLX509Certificate;->fromX509PemInputStream(Ljava/io/InputStream;)Lcom/android/org/conscrypt/OpenSSLX509Certificate;

    move-result-object v13

    .line 218
    .local v13, "chainCert":Lcom/android/org/conscrypt/OpenSSLX509Certificate;
    invoke-virtual {v13}, Lcom/android/org/conscrypt/OpenSSLX509Certificate;->getEncoded()[B

    move-result-object v3

    invoke-static {v3}, Lcom/android/org/conscrypt/NativeCrypto;->d2i_X509([B)J

    move-result-wide v20

    aput-wide v20, v8, v18

    .line 214
    add-int/lit8 v18, v18, 0x1

    goto :goto_0

    .end local v10    # "a":[B
    .end local v11    # "cIs":Ljava/io/InputStream;
    .end local v13    # "chainCert":Lcom/android/org/conscrypt/OpenSSLX509Certificate;
    .end local v17    # "size":I
    .end local v18    # "u":I
    :cond_2
    move-object/from16 v3, p5

    .line 221
    invoke-static/range {v2 .. v9}, Lcom/android/org/conscrypt/NativeSMIMECrypto;->PKCS7sign(Ljava/lang/String;Ljava/lang/String;JJ[JLjava/lang/String;)Z

    move-result v3

    return v3
.end method

.method public static openSSLPKCS7decrypt(Ljava/io/File;Ljava/io/File;Ljava/security/PrivateKey;[BZ)Z
    .locals 11
    .param p0, "inputfile"    # Ljava/io/File;
    .param p1, "outputfile"    # Ljava/io/File;
    .param p2, "privateKey"    # Ljava/security/PrivateKey;
    .param p3, "PEMcertificateBytes"    # [B
    .param p4, "isDER"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Ljava/security/cert/CertificateEncodingException;,
            Ljava/lang/NullPointerException;,
            Ljava/security/InvalidKeyException;,
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 251
    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 252
    new-instance v1, Ljava/io/FileNotFoundException;

    const-string v7, "Input File does not exist."

    invoke-direct {v1, v7}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 253
    :cond_0
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_1

    .line 254
    new-instance v1, Ljava/io/FileNotFoundException;

    const-string v7, "Output File does not exist."

    invoke-direct {v1, v7}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 256
    :cond_1
    invoke-static {p2}, Lcom/android/org/conscrypt/OpenSSLKey;->fromPrivateKey(Ljava/security/PrivateKey;)Lcom/android/org/conscrypt/OpenSSLKey;

    move-result-object v8

    .line 257
    .local v8, "oKey":Lcom/android/org/conscrypt/OpenSSLKey;
    invoke-virtual {v8}, Lcom/android/org/conscrypt/OpenSSLKey;->getPkeyContext()J

    move-result-wide v2

    .line 259
    .local v2, "evpKeyRef":J
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, p3}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 260
    .local v0, "certIs":Ljava/io/InputStream;
    invoke-static {v0}, Lcom/android/org/conscrypt/OpenSSLX509Certificate;->fromX509PemInputStream(Ljava/io/InputStream;)Lcom/android/org/conscrypt/OpenSSLX509Certificate;

    move-result-object v9

    .line 261
    .local v9, "openSSLcert":Lcom/android/org/conscrypt/OpenSSLX509Certificate;
    invoke-virtual {v9}, Lcom/android/org/conscrypt/OpenSSLX509Certificate;->getEncoded()[B

    move-result-object v10

    .line 262
    .local v10, "openSSLcertEncoded":[B
    invoke-static {v10}, Lcom/android/org/conscrypt/NativeCrypto;->d2i_X509([B)J

    move-result-wide v4

    .line 263
    .local v4, "certRef":J
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    .line 265
    .local v6, "outfile":Ljava/lang/String;
    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    move v7, p4

    invoke-static/range {v1 .. v7}, Lcom/android/org/conscrypt/NativeSMIMECrypto;->PKCS7decrypt(Ljava/lang/String;JJLjava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method public static openSSLPKCS7encrypt(Ljava/io/File;Ljava/io/File;Ljava/util/List;Ljava/lang/String;)Z
    .locals 16
    .param p0, "inputData"    # Ljava/io/File;
    .param p1, "output"    # Ljava/io/File;
    .param p3, "encryptionAlgorithm"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Ljava/io/File;",
            "Ljava/util/List",
            "<[B>;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Ljava/security/cert/CertificateEncodingException;,
            Ljava/lang/NullPointerException;,
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 226
    .local p2, "PEMcertificateBytes":Ljava/util/List;, "Ljava/util/List<[B>;"
    invoke-virtual/range {p0 .. p0}, Ljava/io/File;->exists()Z

    move-result v14

    if-nez v14, :cond_0

    .line 227
    new-instance v14, Ljava/io/FileNotFoundException;

    const-string v15, "Input File does not exist."

    invoke-direct {v14, v15}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v14

    .line 228
    :cond_0
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->exists()Z

    move-result v14

    if-nez v14, :cond_1

    .line 229
    new-instance v14, Ljava/io/FileNotFoundException;

    const-string v15, "Output File does not exist."

    invoke-direct {v14, v15}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v14

    .line 231
    :cond_1
    new-instance v9, Ljava/io/FileInputStream;

    move-object/from16 v0, p0

    invoke-direct {v9, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 232
    .local v9, "fis":Ljava/io/FileInputStream;
    new-instance v3, Lcom/android/org/conscrypt/OpenSSLBIOInputStream;

    invoke-direct {v3, v9}, Lcom/android/org/conscrypt/OpenSSLBIOInputStream;-><init>(Ljava/io/InputStream;)V

    .line 233
    .local v3, "bis":Lcom/android/org/conscrypt/OpenSSLBIOInputStream;
    invoke-static {v3}, Lcom/android/org/conscrypt/NativeCrypto;->create_BIO_InputStream(Lcom/android/org/conscrypt/OpenSSLBIOInputStream;)J

    move-result-wide v4

    .line 235
    .local v4, "bioRef":J
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v8

    .line 236
    .local v8, "certsRefArrLength":I
    new-array v7, v8, [J

    .line 237
    .local v7, "certsRefArr":[J
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_0
    if-ge v10, v8, :cond_2

    .line 238
    move-object/from16 v0, p2

    invoke-interface {v0, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [B

    .line 239
    .local v2, "arr":[B
    new-instance v6, Ljava/io/ByteArrayInputStream;

    invoke-direct {v6, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 240
    .local v6, "certIs":Ljava/io/InputStream;
    invoke-static {v6}, Lcom/android/org/conscrypt/OpenSSLX509Certificate;->fromX509PemInputStream(Ljava/io/InputStream;)Lcom/android/org/conscrypt/OpenSSLX509Certificate;

    move-result-object v11

    .line 241
    .local v11, "openSSLcert":Lcom/android/org/conscrypt/OpenSSLX509Certificate;
    invoke-virtual {v11}, Lcom/android/org/conscrypt/OpenSSLX509Certificate;->getEncoded()[B

    move-result-object v12

    .line 242
    .local v12, "openSSLcertEncoded":[B
    invoke-static {v12}, Lcom/android/org/conscrypt/NativeCrypto;->d2i_X509([B)J

    move-result-wide v14

    aput-wide v14, v7, v10

    .line 237
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 245
    .end local v2    # "arr":[B
    .end local v6    # "certIs":Ljava/io/InputStream;
    .end local v11    # "openSSLcert":Lcom/android/org/conscrypt/OpenSSLX509Certificate;
    .end local v12    # "openSSLcertEncoded":[B
    :cond_2
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v13

    .line 246
    .local v13, "outputFilePath":Ljava/lang/String;
    move-object/from16 v0, p3

    invoke-static {v4, v5, v7, v13, v0}, Lcom/android/org/conscrypt/NativeSMIMECrypto;->PKCS7encrypt(J[JLjava/lang/String;Ljava/lang/String;)Z

    move-result v14

    return v14
.end method

.method public static openSSLPKCS7verify(Ljava/io/File;Ljava/io/File;Ljava/util/List;)[Ljava/security/cert/X509Certificate;
    .locals 16
    .param p0, "inputData"    # Ljava/io/File;
    .param p1, "outputData"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Ljava/io/File;",
            "Ljava/util/List",
            "<[B>;)[",
            "Ljava/security/cert/X509Certificate;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;,
            Ljava/lang/NullPointerException;,
            Ljava/security/cert/CertificateEncodingException;,
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 152
    .local p2, "PEMsignCertificateBytes":Ljava/util/List;, "Ljava/util/List<[B>;"
    invoke-virtual/range {p0 .. p0}, Ljava/io/File;->exists()Z

    move-result v12

    if-nez v12, :cond_0

    .line 153
    new-instance v12, Ljava/io/FileNotFoundException;

    const-string v13, "Input File does not exist."

    invoke-direct {v12, v13}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v12

    .line 155
    :cond_0
    const/4 v5, 0x0

    .line 156
    .local v5, "certsRef":[J
    if-eqz p2, :cond_1

    .line 157
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v6

    .line 158
    .local v6, "certsRefArrLength":I
    new-array v5, v6, [J

    .line 159
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    if-ge v7, v6, :cond_1

    .line 161
    move-object/from16 v0, p2

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [B

    .line 162
    .local v2, "arr":[B
    new-instance v3, Ljava/io/ByteArrayInputStream;

    invoke-direct {v3, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 163
    .local v3, "certIs":Ljava/io/InputStream;
    invoke-static {v3}, Lcom/android/org/conscrypt/OpenSSLX509Certificate;->fromX509PemInputStream(Ljava/io/InputStream;)Lcom/android/org/conscrypt/OpenSSLX509Certificate;

    move-result-object v8

    .line 164
    .local v8, "openSSLcert":Lcom/android/org/conscrypt/OpenSSLX509Certificate;
    invoke-virtual {v8}, Lcom/android/org/conscrypt/OpenSSLX509Certificate;->getEncoded()[B

    move-result-object v9

    .line 165
    .local v9, "openSSLcertEncoded":[B
    invoke-static {v9}, Lcom/android/org/conscrypt/NativeCrypto;->d2i_X509([B)J

    move-result-wide v12

    aput-wide v12, v5, v7

    .line 159
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 169
    .end local v2    # "arr":[B
    .end local v3    # "certIs":Ljava/io/InputStream;
    .end local v6    # "certsRefArrLength":I
    .end local v7    # "i":I
    .end local v8    # "openSSLcert":Lcom/android/org/conscrypt/OpenSSLX509Certificate;
    .end local v9    # "openSSLcertEncoded":[B
    :cond_1
    const/4 v10, 0x0

    .line 170
    .local v10, "outputFilePath":Ljava/lang/String;
    if-eqz p1, :cond_2

    .line 171
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->exists()Z

    move-result v12

    if-eqz v12, :cond_4

    .line 172
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    .line 177
    :cond_2
    invoke-virtual/range {p0 .. p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12, v10, v5}, Lcom/android/org/conscrypt/NativeSMIMECrypto;->PKCS7verify(Ljava/lang/String;Ljava/lang/String;[J)[J

    move-result-object v4

    .line 178
    .local v4, "certs":[J
    if-nez v4, :cond_5

    .line 179
    sget-object v12, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v13, "Certs returned from PKCS7 is null."

    invoke-virtual {v12, v13}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 180
    const/4 v11, 0x0

    .line 186
    :cond_3
    return-object v11

    .line 174
    .end local v4    # "certs":[J
    :cond_4
    new-instance v12, Ljava/io/FileNotFoundException;

    const-string v13, "Ouput File does not exist."

    invoke-direct {v12, v13}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v12

    .line 182
    .restart local v4    # "certs":[J
    :cond_5
    array-length v12, v4

    new-array v11, v12, [Ljava/security/cert/X509Certificate;

    .line 183
    .local v11, "signerCert":[Ljava/security/cert/X509Certificate;
    const/4 v7, 0x0

    .restart local v7    # "i":I
    :goto_1
    array-length v12, v4

    if-ge v7, v12, :cond_3

    .line 184
    new-instance v12, Lcom/android/org/conscrypt/OpenSSLX509Certificate;

    aget-wide v14, v4, v7

    invoke-direct {v12, v14, v15}, Lcom/android/org/conscrypt/OpenSSLX509Certificate;-><init>(J)V

    aput-object v12, v11, v7

    .line 183
    add-int/lit8 v7, v7, 0x1

    goto :goto_1
.end method

.method public static openSSLvalidateCertificate([BLjava/util/List;Ljava/lang/String;[Ljava/lang/String;I)I
    .locals 22
    .param p0, "certificatePEMBytes"    # [B
    .param p2, "CAfilepath"    # Ljava/lang/String;
    .param p3, "CRLFiles"    # [Ljava/lang/String;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B",
            "Ljava/util/List",
            "<[B>;",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            "I)I"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/cert/CertificateEncodingException;,
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 288
    .local p1, "certificateChainPEMbytes":Ljava/util/List;, "Ljava/util/List<[B>;"
    new-instance v11, Ljava/io/ByteArrayInputStream;

    move-object/from16 v0, p0

    invoke-direct {v11, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 289
    .local v11, "certIs":Ljava/io/InputStream;
    invoke-static {v11}, Lcom/android/org/conscrypt/OpenSSLX509Certificate;->fromX509PemInputStream(Ljava/io/InputStream;)Lcom/android/org/conscrypt/OpenSSLX509Certificate;

    move-result-object v16

    .line 290
    .local v16, "openSSLcert":Lcom/android/org/conscrypt/OpenSSLX509Certificate;
    invoke-virtual/range {v16 .. v16}, Lcom/android/org/conscrypt/OpenSSLX509Certificate;->getEncoded()[B

    move-result-object v17

    .line 291
    .local v17, "openSSLcertEncoded":[B
    invoke-static/range {v17 .. v17}, Lcom/android/org/conscrypt/NativeCrypto;->d2i_X509([B)J

    move-result-wide v2

    .line 293
    .local v2, "certRef":J
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v12

    .line 294
    .local v12, "certsRefArrLength":I
    new-array v4, v12, [J

    .line 295
    .local v4, "certsRef":[J
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_0
    if-ge v14, v12, :cond_0

    .line 296
    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [B

    .line 297
    .local v8, "arr":[B
    new-instance v10, Ljava/io/ByteArrayInputStream;

    invoke-direct {v10, v8}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 298
    .local v10, "certChainIs":Ljava/io/InputStream;
    invoke-static {v10}, Lcom/android/org/conscrypt/OpenSSLX509Certificate;->fromX509PemInputStream(Ljava/io/InputStream;)Lcom/android/org/conscrypt/OpenSSLX509Certificate;

    move-result-object v15

    .line 299
    .local v15, "openSSLChainCert":Lcom/android/org/conscrypt/OpenSSLX509Certificate;
    invoke-virtual {v15}, Lcom/android/org/conscrypt/OpenSSLX509Certificate;->getEncoded()[B

    move-result-object v18

    .line 300
    .local v18, "openSSLchainCertEncoded":[B
    invoke-static/range {v18 .. v18}, Lcom/android/org/conscrypt/NativeCrypto;->d2i_X509([B)J

    move-result-wide v20

    aput-wide v20, v4, v14

    .line 295
    add-int/lit8 v14, v14, 0x1

    goto :goto_0

    .line 308
    .end local v8    # "arr":[B
    .end local v10    # "certChainIs":Ljava/io/InputStream;
    .end local v15    # "openSSLChainCert":Lcom/android/org/conscrypt/OpenSSLX509Certificate;
    .end local v18    # "openSSLchainCertEncoded":[B
    :cond_0
    const/4 v6, 0x0

    .line 309
    .local v6, "crlCtx":[J
    if-eqz p3, :cond_1

    .line 310
    move-object/from16 v0, p3

    array-length v5, v0

    new-array v6, v5, [J

    .line 311
    const/4 v14, 0x0

    :goto_1
    move-object/from16 v0, p3

    array-length v5, v0

    if-ge v14, v5, :cond_1

    .line 312
    new-instance v13, Ljava/io/FileInputStream;

    new-instance v5, Ljava/io/File;

    aget-object v7, p3, v14

    invoke-direct {v5, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v13, v5}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 313
    .local v13, "crlIs":Ljava/io/InputStream;
    new-instance v9, Lcom/android/org/conscrypt/OpenSSLBIOInputStream;

    invoke-direct {v9, v13}, Lcom/android/org/conscrypt/OpenSSLBIOInputStream;-><init>(Ljava/io/InputStream;)V

    .line 314
    .local v9, "bis":Lcom/android/org/conscrypt/OpenSSLBIOInputStream;
    invoke-virtual {v9}, Lcom/android/org/conscrypt/OpenSSLBIOInputStream;->getBioContext()J

    move-result-wide v20

    invoke-static/range {v20 .. v21}, Lcom/android/org/conscrypt/NativeCrypto;->d2i_X509_CRL_bio(J)J

    move-result-wide v20

    aput-wide v20, v6, v14

    .line 311
    add-int/lit8 v14, v14, 0x1

    goto :goto_1

    .end local v9    # "bis":Lcom/android/org/conscrypt/OpenSSLBIOInputStream;
    .end local v13    # "crlIs":Ljava/io/InputStream;
    :cond_1
    move-object/from16 v5, p2

    move/from16 v7, p4

    .line 318
    invoke-static/range {v2 .. v7}, Lcom/android/org/conscrypt/NativeSMIMECrypto;->validateCertificate(J[JLjava/lang/String;[JI)I

    move-result v5

    return v5
.end method

.method private static native validateCertificate(J[JLjava/lang/String;[JI)I
.end method
