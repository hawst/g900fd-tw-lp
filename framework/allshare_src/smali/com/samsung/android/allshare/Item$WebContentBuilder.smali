.class public Lcom/samsung/android/allshare/Item$WebContentBuilder;
.super Ljava/lang/Object;
.source "Item.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/Item;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "WebContentBuilder"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/Item$WebContentBuilder$PlayMode;,
        Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;
    }
.end annotation


# instance fields
.field private mAlbumTitle:Ljava/lang/String;

.field private mArtist:Ljava/lang/String;

.field private mDate:Ljava/util/Date;

.field private mDeliveryMode:Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

.field private mDuration:J

.field private mGenre:Ljava/lang/String;

.field private mMimetype:Ljava/lang/String;

.field private mSubtitlePath:Ljava/lang/String;

.field private mTitle:Ljava/lang/String;

.field private mUri:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Landroid/net/Uri;Ljava/lang/String;)V
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "mimeType"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 1238
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1106
    iput-object v0, p0, Lcom/samsung/android/allshare/Item$WebContentBuilder;->mUri:Landroid/net/Uri;

    .line 1108
    iput-object v0, p0, Lcom/samsung/android/allshare/Item$WebContentBuilder;->mMimetype:Ljava/lang/String;

    .line 1110
    iput-object v0, p0, Lcom/samsung/android/allshare/Item$WebContentBuilder;->mTitle:Ljava/lang/String;

    .line 1112
    iput-object v0, p0, Lcom/samsung/android/allshare/Item$WebContentBuilder;->mDeliveryMode:Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

    .line 1114
    iput-object v0, p0, Lcom/samsung/android/allshare/Item$WebContentBuilder;->mSubtitlePath:Ljava/lang/String;

    .line 1116
    iput-object v0, p0, Lcom/samsung/android/allshare/Item$WebContentBuilder;->mArtist:Ljava/lang/String;

    .line 1118
    iput-object v0, p0, Lcom/samsung/android/allshare/Item$WebContentBuilder;->mAlbumTitle:Ljava/lang/String;

    .line 1120
    iput-object v0, p0, Lcom/samsung/android/allshare/Item$WebContentBuilder;->mGenre:Ljava/lang/String;

    .line 1122
    iput-object v0, p0, Lcom/samsung/android/allshare/Item$WebContentBuilder;->mDate:Ljava/util/Date;

    .line 1124
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/samsung/android/allshare/Item$WebContentBuilder;->mDuration:J

    .line 1239
    iput-object p1, p0, Lcom/samsung/android/allshare/Item$WebContentBuilder;->mUri:Landroid/net/Uri;

    .line 1240
    iput-object p2, p0, Lcom/samsung/android/allshare/Item$WebContentBuilder;->mMimetype:Ljava/lang/String;

    .line 1241
    return-void
.end method

.method private checkSubtitlePathValid(Ljava/lang/String;)Z
    .locals 5
    .param p1, "subtitleFilePath"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 1415
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_1

    .line 1435
    :cond_0
    :goto_0
    return v3

    .line 1418
    :cond_1
    const/4 v0, 0x0

    .line 1419
    .local v0, "absoluteFilePath":Ljava/lang/String;
    const-string v2, "file://"

    .line 1421
    .local v2, "headString":Ljava/lang/String;
    const-string v4, "file:"

    invoke-virtual {p1, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1422
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {p1, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 1427
    :goto_1
    const-string v4, "/data/data"

    invoke-virtual {v0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1430
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1431
    .local v1, "file":Ljava/io/File;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1432
    const/4 v3, 0x1

    goto :goto_0

    .line 1424
    .end local v1    # "file":Ljava/io/File;
    :cond_2
    move-object v0, p1

    goto :goto_1
.end method


# virtual methods
.method public build()Lcom/samsung/android/allshare/Item;
    .locals 18

    .prologue
    .line 1317
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/allshare/Item$WebContentBuilder;->mUri:Landroid/net/Uri;

    if-eqz v3, :cond_0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/allshare/Item$WebContentBuilder;->mMimetype:Ljava/lang/String;

    if-nez v3, :cond_1

    .line 1318
    :cond_0
    const-string v3, "Item"

    const-string v4, "build error! mUri == null || mMimetype == null"

    invoke-static {v3, v4}, Lcom/samsung/android/allshare/DLog;->e_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 1319
    const/4 v3, 0x0

    .line 1351
    :goto_0
    return-object v3

    .line 1322
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/allshare/Item$WebContentBuilder;->mSubtitlePath:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/samsung/android/allshare/Item$WebContentBuilder;->checkSubtitlePathValid(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 1323
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/allshare/Item$WebContentBuilder;->mSubtitlePath:Ljava/lang/String;

    .line 1325
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/allshare/Item$WebContentBuilder;->mMimetype:Ljava/lang/String;

    const-string v4, "video/*"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1326
    const-string v3, "video/mp4"

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/allshare/Item$WebContentBuilder;->mMimetype:Ljava/lang/String;

    .line 1328
    :cond_3
    const-string v3, "Item"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "item build mime : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/allshare/Item$WebContentBuilder;->mMimetype:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " item build uri: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/allshare/Item$WebContentBuilder;->mUri:Landroid/net/Uri;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/allshare/DLog;->i_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 1330
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/allshare/Item$WebContentBuilder;->mDeliveryMode:Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

    if-nez v3, :cond_4

    .line 1331
    sget-object v3, Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;->UNKNOWN:Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/allshare/Item$WebContentBuilder;->mDeliveryMode:Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

    .line 1333
    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/allshare/Item$WebContentBuilder;->mUri:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    .line 1334
    .local v2, "scheme":Ljava/lang/String;
    if-eqz v2, :cond_5

    const-string v3, "content"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_5

    const-string v3, "file"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 1335
    :cond_5
    const-string v3, "Item"

    const-string v4, "build error! scheme == null || scheme.contains(content) || scheme.contains(file)"

    invoke-static {v3, v4}, Lcom/samsung/android/allshare/DLog;->e_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 1337
    const/4 v3, 0x0

    goto :goto_0

    .line 1340
    :cond_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/allshare/Item$WebContentBuilder;->mMimetype:Ljava/lang/String;

    # invokes: Lcom/samsung/android/allshare/Item;->convertItemTypeFromMimeType(Ljava/lang/String;)Lcom/samsung/android/allshare/Item$MediaType;
    invoke-static {v3}, Lcom/samsung/android/allshare/Item;->access$100(Ljava/lang/String;)Lcom/samsung/android/allshare/Item$MediaType;

    move-result-object v17

    .line 1341
    .local v17, "type":Lcom/samsung/android/allshare/Item$MediaType;
    sget-object v3, Lcom/samsung/android/allshare/Item$1;->$SwitchMap$com$samsung$android$allshare$Item$MediaType:[I

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/android/allshare/Item$MediaType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 1351
    const/4 v3, 0x0

    goto/16 :goto_0

    .line 1347
    :pswitch_0
    new-instance v3, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;

    sget-object v4, Lcom/samsung/android/allshare/ItemCreator$ConstructorType;->WEB_CONTENT:Lcom/samsung/android/allshare/ItemCreator$ConstructorType;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/allshare/Item$WebContentBuilder;->mUri:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/allshare/Item$WebContentBuilder;->mTitle:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/allshare/Item$WebContentBuilder;->mMimetype:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/allshare/Item$WebContentBuilder;->mSubtitlePath:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/allshare/Item$WebContentBuilder;->mDeliveryMode:Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/allshare/Item$WebContentBuilder;->mArtist:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/allshare/Item$WebContentBuilder;->mAlbumTitle:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/allshare/Item$WebContentBuilder;->mGenre:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/allshare/Item$WebContentBuilder;->mDate:Ljava/util/Date;

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/samsung/android/allshare/Item$WebContentBuilder;->mDuration:J

    const/16 v16, 0x0

    invoke-direct/range {v3 .. v16}, Lcom/samsung/android/allshare/Item$BuilderGeneratedItem;-><init>(Lcom/samsung/android/allshare/ItemCreator$ConstructorType;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;JLcom/samsung/android/allshare/Item$1;)V

    goto/16 :goto_0

    .line 1341
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public setAlbumTitle(Ljava/lang/String;)Lcom/samsung/android/allshare/Item$WebContentBuilder;
    .locals 0
    .param p1, "albumTitle"    # Ljava/lang/String;

    .prologue
    .line 1374
    iput-object p1, p0, Lcom/samsung/android/allshare/Item$WebContentBuilder;->mAlbumTitle:Ljava/lang/String;

    .line 1375
    return-object p0
.end method

.method public setArtist(Ljava/lang/String;)Lcom/samsung/android/allshare/Item$WebContentBuilder;
    .locals 0
    .param p1, "artist"    # Ljava/lang/String;

    .prologue
    .line 1362
    iput-object p1, p0, Lcom/samsung/android/allshare/Item$WebContentBuilder;->mArtist:Ljava/lang/String;

    .line 1363
    return-object p0
.end method

.method public setDate(Ljava/util/Date;)Lcom/samsung/android/allshare/Item$WebContentBuilder;
    .locals 0
    .param p1, "date"    # Ljava/util/Date;

    .prologue
    .line 1398
    iput-object p1, p0, Lcom/samsung/android/allshare/Item$WebContentBuilder;->mDate:Ljava/util/Date;

    .line 1399
    return-object p0
.end method

.method public setDeliveryMode(Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;)Lcom/samsung/android/allshare/Item$WebContentBuilder;
    .locals 0
    .param p1, "deliverymode"    # Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

    .prologue
    .line 1280
    iput-object p1, p0, Lcom/samsung/android/allshare/Item$WebContentBuilder;->mDeliveryMode:Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

    .line 1281
    return-object p0
.end method

.method public setDuration(J)Lcom/samsung/android/allshare/Item$WebContentBuilder;
    .locals 1
    .param p1, "duration"    # J

    .prologue
    .line 1410
    iput-wide p1, p0, Lcom/samsung/android/allshare/Item$WebContentBuilder;->mDuration:J

    .line 1411
    return-object p0
.end method

.method public setGenre(Ljava/lang/String;)Lcom/samsung/android/allshare/Item$WebContentBuilder;
    .locals 0
    .param p1, "genre"    # Ljava/lang/String;

    .prologue
    .line 1386
    iput-object p1, p0, Lcom/samsung/android/allshare/Item$WebContentBuilder;->mGenre:Ljava/lang/String;

    .line 1387
    return-object p0
.end method

.method public setPlayMode(Lcom/samsung/android/allshare/Item$WebContentBuilder$PlayMode;)Lcom/samsung/android/allshare/Item$WebContentBuilder;
    .locals 1
    .param p1, "playmode"    # Lcom/samsung/android/allshare/Item$WebContentBuilder$PlayMode;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1297
    sget-object v0, Lcom/samsung/android/allshare/Item$WebContentBuilder$PlayMode;->REDIRECT:Lcom/samsung/android/allshare/Item$WebContentBuilder$PlayMode;

    if-ne p1, v0, :cond_0

    .line 1298
    sget-object v0, Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;->REDIRECT:Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

    iput-object v0, p0, Lcom/samsung/android/allshare/Item$WebContentBuilder;->mDeliveryMode:Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

    .line 1304
    :goto_0
    return-object p0

    .line 1299
    :cond_0
    sget-object v0, Lcom/samsung/android/allshare/Item$WebContentBuilder$PlayMode;->RELAY:Lcom/samsung/android/allshare/Item$WebContentBuilder$PlayMode;

    if-ne p1, v0, :cond_1

    .line 1300
    sget-object v0, Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;->RELAY:Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

    iput-object v0, p0, Lcom/samsung/android/allshare/Item$WebContentBuilder;->mDeliveryMode:Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

    goto :goto_0

    .line 1302
    :cond_1
    sget-object v0, Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;->UNKNOWN:Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

    iput-object v0, p0, Lcom/samsung/android/allshare/Item$WebContentBuilder;->mDeliveryMode:Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

    goto :goto_0
.end method

.method public setSubtitle(Ljava/lang/String;)Lcom/samsung/android/allshare/Item$WebContentBuilder;
    .locals 0
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 1262
    iput-object p1, p0, Lcom/samsung/android/allshare/Item$WebContentBuilder;->mSubtitlePath:Ljava/lang/String;

    .line 1263
    return-object p0
.end method

.method public setTitle(Ljava/lang/String;)Lcom/samsung/android/allshare/Item$WebContentBuilder;
    .locals 0
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 1256
    iput-object p1, p0, Lcom/samsung/android/allshare/Item$WebContentBuilder;->mTitle:Ljava/lang/String;

    .line 1257
    return-object p0
.end method
