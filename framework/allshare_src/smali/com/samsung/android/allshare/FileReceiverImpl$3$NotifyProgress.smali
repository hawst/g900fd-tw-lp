.class Lcom/samsung/android/allshare/FileReceiverImpl$3$NotifyProgress;
.super Ljava/lang/Object;
.source "FileReceiverImpl.java"

# interfaces
.implements Lcom/samsung/android/allshare/FileReceiverImpl$INotifyProgressEvent;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/FileReceiverImpl$3;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "NotifyProgress"
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/allshare/FileReceiverImpl$3;


# direct methods
.method constructor <init>(Lcom/samsung/android/allshare/FileReceiverImpl$3;)V
    .locals 0

    .prologue
    .line 512
    iput-object p1, p0, Lcom/samsung/android/allshare/FileReceiverImpl$3$NotifyProgress;->this$1:Lcom/samsung/android/allshare/FileReceiverImpl$3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNotifyEvent(Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverProgressUpdateEventListener;Landroid/os/Bundle;)V
    .locals 11
    .param p1, "listener"    # Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverProgressUpdateEventListener;
    .param p2, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 517
    sget-object v9, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    .line 519
    .local v9, "error":Lcom/samsung/android/allshare/ERROR;
    const-string v1, "BUNDLE_LONG_FILE_PROGRESS"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 520
    .local v4, "progress":J
    const-string v1, "BUNDLE_LONG_FILE_SIZE"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v6

    .line 521
    .local v6, "size":J
    const-string v1, "BUNDLE_STRING_FILE_PATH"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 522
    .local v10, "path":Ljava/lang/String;
    const-string v1, "BUNDLE_STRING_SESSIONID"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 524
    .local v3, "sessionId":Ljava/lang/String;
    const-string v1, "BUNDLE_ENUM_ERROR"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 525
    .local v0, "errorStr":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 526
    sget-object v9, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    .line 530
    :goto_0
    new-instance v8, Ljava/io/File;

    invoke-direct {v8, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 532
    .local v8, "file":Ljava/io/File;
    iget-object v1, p0, Lcom/samsung/android/allshare/FileReceiverImpl$3$NotifyProgress;->this$1:Lcom/samsung/android/allshare/FileReceiverImpl$3;

    iget-object v1, v1, Lcom/samsung/android/allshare/FileReceiverImpl$3;->this$0:Lcom/samsung/android/allshare/FileReceiverImpl;

    # getter for: Lcom/samsung/android/allshare/FileReceiverImpl;->mReceiver:Lcom/samsung/android/allshare/file/FileReceiver;
    invoke-static {v1}, Lcom/samsung/android/allshare/FileReceiverImpl;->access$100(Lcom/samsung/android/allshare/FileReceiverImpl;)Lcom/samsung/android/allshare/file/FileReceiver;

    move-result-object v2

    move-object v1, p1

    invoke-interface/range {v1 .. v9}, Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverProgressUpdateEventListener;->onProgressUpdated(Lcom/samsung/android/allshare/file/FileReceiver;Ljava/lang/String;JJLjava/io/File;Lcom/samsung/android/allshare/ERROR;)V

    .line 533
    return-void

    .line 528
    .end local v8    # "file":Ljava/io/File;
    :cond_0
    invoke-static {v0}, Lcom/samsung/android/allshare/ERROR;->stringToEnum(Ljava/lang/String;)Lcom/samsung/android/allshare/ERROR;

    move-result-object v9

    goto :goto_0
.end method
