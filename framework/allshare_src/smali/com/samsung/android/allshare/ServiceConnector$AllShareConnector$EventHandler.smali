.class Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector$EventHandler;
.super Ljava/lang/Object;
.source "ServiceConnector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EventHandler"
.end annotation


# instance fields
.field mBundle:Landroid/os/Bundle;

.field mEventId:Ljava/lang/String;

.field mHanlder:Lcom/samsung/android/allshare/AllShareEventHandler;

.field final synthetic this$0:Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;


# direct methods
.method public constructor <init>(Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;Ljava/lang/String;Landroid/os/Bundle;Lcom/samsung/android/allshare/AllShareEventHandler;)V
    .locals 1
    .param p2, "event"    # Ljava/lang/String;
    .param p3, "bundle"    # Landroid/os/Bundle;
    .param p4, "handler"    # Lcom/samsung/android/allshare/AllShareEventHandler;

    .prologue
    const/4 v0, 0x0

    .line 1310
    iput-object p1, p0, Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector$EventHandler;->this$0:Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1304
    iput-object v0, p0, Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector$EventHandler;->mEventId:Ljava/lang/String;

    .line 1306
    iput-object v0, p0, Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector$EventHandler;->mBundle:Landroid/os/Bundle;

    .line 1308
    iput-object v0, p0, Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector$EventHandler;->mHanlder:Lcom/samsung/android/allshare/AllShareEventHandler;

    .line 1311
    iput-object p2, p0, Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector$EventHandler;->mEventId:Ljava/lang/String;

    .line 1312
    iput-object p3, p0, Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector$EventHandler;->mBundle:Landroid/os/Bundle;

    .line 1313
    iput-object p4, p0, Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector$EventHandler;->mHanlder:Lcom/samsung/android/allshare/AllShareEventHandler;

    .line 1314
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1318
    move-object v0, p1

    check-cast v0, Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector$EventHandler;

    .line 1319
    .local v0, "obj":Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector$EventHandler;
    if-nez p1, :cond_1

    .line 1328
    :cond_0
    :goto_0
    return v1

    .line 1321
    :cond_1
    if-ne p0, p1, :cond_2

    move v1, v2

    .line 1322
    goto :goto_0

    .line 1323
    :cond_2
    instance-of v3, p1, Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector$EventHandler;

    if-eqz v3, :cond_0

    .line 1325
    iget-object v3, v0, Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector$EventHandler;->mEventId:Ljava/lang/String;

    iget-object v4, p0, Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector$EventHandler;->mEventId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, v0, Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector$EventHandler;->mBundle:Landroid/os/Bundle;

    iget-object v4, p0, Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector$EventHandler;->mBundle:Landroid/os/Bundle;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, v0, Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector$EventHandler;->mHanlder:Lcom/samsung/android/allshare/AllShareEventHandler;

    iget-object v4, p0, Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector$EventHandler;->mHanlder:Lcom/samsung/android/allshare/AllShareEventHandler;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v1, v2

    .line 1327
    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 1333
    invoke-super {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method
