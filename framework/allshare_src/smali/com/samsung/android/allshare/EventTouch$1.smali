.class final Lcom/samsung/android/allshare/EventTouch$1;
.super Ljava/lang/Object;
.source "IAppControlAPI.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/EventTouch;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/samsung/android/allshare/EventTouch;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 1657
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/samsung/android/allshare/EventTouch;
    .locals 2
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 1659
    new-instance v0, Lcom/samsung/android/allshare/EventTouch;

    invoke-direct {v0}, Lcom/samsung/android/allshare/EventTouch;-><init>()V

    .line 1661
    .local v0, "eventsync":Lcom/samsung/android/allshare/EventTouch;
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, v0, Lcom/samsung/android/allshare/EventTouch;->mX:I

    .line 1662
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, v0, Lcom/samsung/android/allshare/EventTouch;->mY:I

    .line 1663
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, v0, Lcom/samsung/android/allshare/EventTouch;->mDX:I

    .line 1664
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, v0, Lcom/samsung/android/allshare/EventTouch;->mDY:I

    .line 1665
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, v0, Lcom/samsung/android/allshare/EventTouch;->mAccelLevel:I

    .line 1666
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, v0, Lcom/samsung/android/allshare/EventTouch;->mFingerId:I

    .line 1667
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, v0, Lcom/samsung/android/allshare/EventTouch;->mType:I

    .line 1668
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, v0, Lcom/samsung/android/allshare/EventTouch;->mDistance:I

    .line 1669
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, v0, Lcom/samsung/android/allshare/EventTouch;->mDegree:I

    .line 1670
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/android/allshare/EventTouch;->mStr:Ljava/lang/String;

    .line 1671
    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Landroid/os/Parcel;

    .prologue
    .line 1657
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/EventTouch$1;->createFromParcel(Landroid/os/Parcel;)Lcom/samsung/android/allshare/EventTouch;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/samsung/android/allshare/EventTouch;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 1675
    new-array v0, p1, [Lcom/samsung/android/allshare/EventTouch;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 1657
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/EventTouch$1;->newArray(I)[Lcom/samsung/android/allshare/EventTouch;

    move-result-object v0

    return-object v0
.end method
