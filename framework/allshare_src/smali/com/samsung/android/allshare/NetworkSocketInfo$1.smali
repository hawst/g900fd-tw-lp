.class final Lcom/samsung/android/allshare/NetworkSocketInfo$1;
.super Ljava/lang/Object;
.source "IAppControlAPI.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/NetworkSocketInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/samsung/android/allshare/NetworkSocketInfo;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 1489
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/samsung/android/allshare/NetworkSocketInfo;
    .locals 2
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 1491
    new-instance v0, Lcom/samsung/android/allshare/NetworkSocketInfo;

    invoke-direct {v0}, Lcom/samsung/android/allshare/NetworkSocketInfo;-><init>()V

    .line 1492
    .local v0, "netinfo":Lcom/samsung/android/allshare/NetworkSocketInfo;
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, v0, Lcom/samsung/android/allshare/NetworkSocketInfo;->mProtocol:I

    .line 1493
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/android/allshare/NetworkSocketInfo;->mIpAddress:Ljava/lang/String;

    .line 1494
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, v0, Lcom/samsung/android/allshare/NetworkSocketInfo;->mPort:I

    .line 1495
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/android/allshare/NetworkSocketInfo;->mDeviceClass:Ljava/lang/String;

    .line 1496
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/android/allshare/NetworkSocketInfo;->mMacAddr:Ljava/lang/String;

    .line 1497
    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Landroid/os/Parcel;

    .prologue
    .line 1489
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/NetworkSocketInfo$1;->createFromParcel(Landroid/os/Parcel;)Lcom/samsung/android/allshare/NetworkSocketInfo;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/samsung/android/allshare/NetworkSocketInfo;
    .locals 1
    .param p1, "arg0"    # I

    .prologue
    .line 1502
    new-array v0, p1, [Lcom/samsung/android/allshare/NetworkSocketInfo;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 1489
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/NetworkSocketInfo$1;->newArray(I)[Lcom/samsung/android/allshare/NetworkSocketInfo;

    move-result-object v0

    return-object v0
.end method
