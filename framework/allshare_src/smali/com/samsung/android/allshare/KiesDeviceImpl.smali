.class final Lcom/samsung/android/allshare/KiesDeviceImpl;
.super Lcom/samsung/android/allshare/KiesDevice;
.source "KiesDeviceImpl.java"

# interfaces
.implements Lcom/sec/android/allshare/iface/IBundleHolder;
.implements Lcom/sec/android/allshare/iface/IHandlerHolder;


# static fields
.field private static final TAG_CLASS:Ljava/lang/String; = "UPnPDeviceImpl"


# instance fields
.field private mActionResponseListener:Lcom/samsung/android/allshare/KiesDevice$IKiesActionResponseListner;

.field private mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

.field private mAllShareRespHandler:Lcom/samsung/android/allshare/AllShareResponseHandler;

.field private mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

.field private mEventHandler:Lcom/samsung/android/allshare/AllShareEventHandler;

.field private mIsSubscribed:Z

.field private mUPnPDeviceEventListener:Lcom/samsung/android/allshare/KiesDevice$IKiesEventListener;


# direct methods
.method protected constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 52
    invoke-direct {p0}, Lcom/samsung/android/allshare/KiesDevice;-><init>()V

    .line 42
    iput-object v0, p0, Lcom/samsung/android/allshare/KiesDeviceImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    .line 46
    iput-object v0, p0, Lcom/samsung/android/allshare/KiesDeviceImpl;->mUPnPDeviceEventListener:Lcom/samsung/android/allshare/KiesDevice$IKiesEventListener;

    .line 48
    iput-object v0, p0, Lcom/samsung/android/allshare/KiesDeviceImpl;->mActionResponseListener:Lcom/samsung/android/allshare/KiesDevice$IKiesActionResponseListner;

    .line 50
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/allshare/KiesDeviceImpl;->mIsSubscribed:Z

    .line 67
    new-instance v0, Lcom/samsung/android/allshare/KiesDeviceImpl$1;

    invoke-static {}, Lcom/samsung/android/allshare/ServiceConnector;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/allshare/KiesDeviceImpl$1;-><init>(Lcom/samsung/android/allshare/KiesDeviceImpl;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/samsung/android/allshare/KiesDeviceImpl;->mEventHandler:Lcom/samsung/android/allshare/AllShareEventHandler;

    .line 277
    new-instance v0, Lcom/samsung/android/allshare/KiesDeviceImpl$2;

    invoke-static {}, Lcom/samsung/android/allshare/ServiceConnector;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/allshare/KiesDeviceImpl$2;-><init>(Lcom/samsung/android/allshare/KiesDeviceImpl;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/samsung/android/allshare/KiesDeviceImpl;->mAllShareRespHandler:Lcom/samsung/android/allshare/AllShareResponseHandler;

    .line 54
    return-void
.end method

.method constructor <init>(Lcom/samsung/android/allshare/IAllShareConnector;Lcom/samsung/android/allshare/DeviceImpl;)V
    .locals 2
    .param p1, "connector"    # Lcom/samsung/android/allshare/IAllShareConnector;
    .param p2, "deviceImpl"    # Lcom/samsung/android/allshare/DeviceImpl;

    .prologue
    const/4 v0, 0x0

    .line 56
    invoke-direct {p0}, Lcom/samsung/android/allshare/KiesDevice;-><init>()V

    .line 42
    iput-object v0, p0, Lcom/samsung/android/allshare/KiesDeviceImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    .line 46
    iput-object v0, p0, Lcom/samsung/android/allshare/KiesDeviceImpl;->mUPnPDeviceEventListener:Lcom/samsung/android/allshare/KiesDevice$IKiesEventListener;

    .line 48
    iput-object v0, p0, Lcom/samsung/android/allshare/KiesDeviceImpl;->mActionResponseListener:Lcom/samsung/android/allshare/KiesDevice$IKiesActionResponseListner;

    .line 50
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/allshare/KiesDeviceImpl;->mIsSubscribed:Z

    .line 67
    new-instance v0, Lcom/samsung/android/allshare/KiesDeviceImpl$1;

    invoke-static {}, Lcom/samsung/android/allshare/ServiceConnector;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/allshare/KiesDeviceImpl$1;-><init>(Lcom/samsung/android/allshare/KiesDeviceImpl;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/samsung/android/allshare/KiesDeviceImpl;->mEventHandler:Lcom/samsung/android/allshare/AllShareEventHandler;

    .line 277
    new-instance v0, Lcom/samsung/android/allshare/KiesDeviceImpl$2;

    invoke-static {}, Lcom/samsung/android/allshare/ServiceConnector;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/allshare/KiesDeviceImpl$2;-><init>(Lcom/samsung/android/allshare/KiesDeviceImpl;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/samsung/android/allshare/KiesDeviceImpl;->mAllShareRespHandler:Lcom/samsung/android/allshare/AllShareResponseHandler;

    .line 57
    if-nez p1, :cond_0

    .line 59
    const-string v0, "UPnPDeviceImpl"

    const-string v1, "Connection FAIL: AllShare Service Connector does not exist"

    invoke-static {v0, v1}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    :goto_0
    return-void

    .line 63
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/allshare/KiesDeviceImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    .line 64
    iput-object p2, p0, Lcom/samsung/android/allshare/KiesDeviceImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/samsung/android/allshare/KiesDeviceImpl;)Lcom/samsung/android/allshare/KiesDevice$IKiesEventListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/KiesDeviceImpl;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/samsung/android/allshare/KiesDeviceImpl;->mUPnPDeviceEventListener:Lcom/samsung/android/allshare/KiesDevice$IKiesEventListener;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/allshare/KiesDeviceImpl;)Lcom/samsung/android/allshare/KiesDevice$IKiesActionResponseListner;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/KiesDeviceImpl;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/samsung/android/allshare/KiesDeviceImpl;->mActionResponseListener:Lcom/samsung/android/allshare/KiesDevice$IKiesActionResponseListner;

    return-object v0
.end method


# virtual methods
.method public getBundle()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/samsung/android/allshare/KiesDeviceImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    if-nez v0, :cond_0

    .line 169
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 171
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/KiesDeviceImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/DeviceImpl;->getBundle()Landroid/os/Bundle;

    move-result-object v0

    goto :goto_0
.end method

.method public getDeviceDomain()Lcom/samsung/android/allshare/Device$DeviceDomain;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/samsung/android/allshare/KiesDeviceImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    if-nez v0, :cond_0

    .line 93
    sget-object v0, Lcom/samsung/android/allshare/Device$DeviceDomain;->UNKNOWN:Lcom/samsung/android/allshare/Device$DeviceDomain;

    .line 95
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/KiesDeviceImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/DeviceImpl;->getDeviceDomain()Lcom/samsung/android/allshare/Device$DeviceDomain;

    move-result-object v0

    goto :goto_0
.end method

.method public getDeviceType()Lcom/samsung/android/allshare/Device$DeviceType;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/samsung/android/allshare/KiesDeviceImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    if-nez v0, :cond_0

    .line 102
    sget-object v0, Lcom/samsung/android/allshare/Device$DeviceType;->UNKNOWN:Lcom/samsung/android/allshare/Device$DeviceType;

    .line 104
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/KiesDeviceImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/DeviceImpl;->getDeviceType()Lcom/samsung/android/allshare/Device$DeviceType;

    move-result-object v0

    goto :goto_0
.end method

.method public getID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/samsung/android/allshare/KiesDeviceImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    if-nez v0, :cond_0

    .line 110
    const-string v0, ""

    .line 112
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/KiesDeviceImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/DeviceImpl;->getID()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getIPAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/samsung/android/allshare/KiesDeviceImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    if-nez v0, :cond_0

    .line 125
    const-string v0, ""

    .line 127
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/KiesDeviceImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/DeviceImpl;->getIPAddress()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getIPAdress()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 118
    invoke-virtual {p0}, Lcom/samsung/android/allshare/KiesDeviceImpl;->getIPAddress()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getIcon()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/samsung/android/allshare/KiesDeviceImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    if-nez v0, :cond_0

    .line 134
    const-string v0, ""

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 136
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/KiesDeviceImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/DeviceImpl;->getIcon()Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public getIconList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/Icon;",
            ">;"
        }
    .end annotation

    .prologue
    .line 142
    iget-object v0, p0, Lcom/samsung/android/allshare/KiesDeviceImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    if-nez v0, :cond_0

    .line 143
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 145
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/KiesDeviceImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/DeviceImpl;->getIconList()Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0
.end method

.method public getModelName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/samsung/android/allshare/KiesDeviceImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    if-nez v0, :cond_0

    .line 152
    const-string v0, ""

    .line 154
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/KiesDeviceImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/DeviceImpl;->getModelName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getNIC()Ljava/lang/String;
    .locals 1

    .prologue
    .line 332
    iget-object v0, p0, Lcom/samsung/android/allshare/KiesDeviceImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    if-nez v0, :cond_0

    .line 333
    const-string v0, ""

    .line 335
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/KiesDeviceImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/DeviceImpl;->getNIC()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcom/samsung/android/allshare/KiesDeviceImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    if-nez v0, :cond_0

    .line 160
    const-string v0, ""

    .line 162
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/KiesDeviceImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/DeviceImpl;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getP2pMacAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 361
    const-string v0, ""

    return-object v0
.end method

.method public isSeekableOnPaused()Z
    .locals 1

    .prologue
    .line 348
    const/4 v0, 0x0

    return v0
.end method

.method public isWholeHomeAudio()Z
    .locals 1

    .prologue
    .line 353
    const/4 v0, 0x0

    return v0
.end method

.method public optionalCommand(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9
    .param p1, "ipAddress"    # Ljava/lang/String;
    .param p2, "commandName"    # Ljava/lang/String;
    .param p3, "arg1"    # Ljava/lang/String;
    .param p4, "arg2"    # Ljava/lang/String;

    .prologue
    .line 256
    iget-object v0, p0, Lcom/samsung/android/allshare/KiesDeviceImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/allshare/KiesDeviceImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/samsung/android/allshare/IAllShareConnector;->isAllShareServiceConnected()Z

    move-result v0

    if-nez v0, :cond_1

    .line 257
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/KiesDeviceImpl;->mActionResponseListener:Lcom/samsung/android/allshare/KiesDevice$IKiesActionResponseListner;

    const-string v1, "Fail"

    sget-object v6, Lcom/samsung/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/samsung/android/allshare/ERROR;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-interface/range {v0 .. v6}, Lcom/samsung/android/allshare/KiesDevice$IKiesActionResponseListner;->onOptionalCommandResponseReceived(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/ERROR;)V

    .line 275
    :goto_0
    return-void

    .line 262
    :cond_1
    new-instance v8, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v8}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 263
    .local v8, "msg":Lcom/sec/android/allshare/iface/CVMessage;
    const-string v0, "com.sec.android.allshare.action.ACTION_KIES_DEVICE_OPTIONAL_COMMAND"

    invoke-virtual {v8, v0}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 265
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 266
    .local v7, "bundle":Landroid/os/Bundle;
    const-string v0, "BUNDLE_STRING_ID"

    invoke-virtual {p0}, Lcom/samsung/android/allshare/KiesDeviceImpl;->getID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 267
    const-string v0, "BUNDLE_STRING_KIES_DEVICE_IPADDRESS"

    invoke-virtual {v7, v0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    const-string v0, "BUNDLE_STRING_KIES_DEVICE_COMMANDNAME"

    invoke-virtual {v7, v0, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    const-string v0, "BUNDLE_STRING_KIES_DEVICE_ARG1"

    invoke-virtual {v7, v0, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    const-string v0, "BUNDLE_STRING_KIES_DEVICE_ARG2"

    invoke-virtual {v7, v0, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    invoke-virtual {v8, v7}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 273
    iget-object v0, p0, Lcom/samsung/android/allshare/KiesDeviceImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    iget-object v1, p0, Lcom/samsung/android/allshare/KiesDeviceImpl;->mAllShareRespHandler:Lcom/samsung/android/allshare/AllShareResponseHandler;

    invoke-interface {v0, v8, v1}, Lcom/samsung/android/allshare/IAllShareConnector;->requestCVMAsync(Lcom/sec/android/allshare/iface/CVMessage;Lcom/samsung/android/allshare/AllShareResponseHandler;)J

    goto :goto_0
.end method

.method public removeEventHandler()V
    .locals 4

    .prologue
    .line 340
    iget-object v0, p0, Lcom/samsung/android/allshare/KiesDeviceImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    const-string v1, "com.sec.android.allshare.event.EVENT_DEVICE_SUBSCRIBE"

    invoke-virtual {p0}, Lcom/samsung/android/allshare/KiesDeviceImpl;->getBundle()Landroid/os/Bundle;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/allshare/KiesDeviceImpl;->mEventHandler:Lcom/samsung/android/allshare/AllShareEventHandler;

    invoke-interface {v0, v1, v2, v3}, Lcom/samsung/android/allshare/IAllShareConnector;->unsubscribeAllShareEvent(Ljava/lang/String;Landroid/os/Bundle;Lcom/samsung/android/allshare/AllShareEventHandler;)V

    .line 342
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/allshare/KiesDeviceImpl;->mIsSubscribed:Z

    .line 343
    return-void
.end method

.method public setActionListener(Lcom/samsung/android/allshare/KiesDevice$IKiesActionResponseListner;)Lcom/samsung/android/allshare/ERROR;
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/allshare/KiesDevice$IKiesActionResponseListner;

    .prologue
    .line 176
    iput-object p1, p0, Lcom/samsung/android/allshare/KiesDeviceImpl;->mActionResponseListener:Lcom/samsung/android/allshare/KiesDevice$IKiesActionResponseListner;

    .line 177
    sget-object v0, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    return-object v0
.end method

.method public setAutoSyncConnectionInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 10
    .param p1, "uniqueDeviceID"    # Ljava/lang/String;
    .param p2, "ipAddress"    # Ljava/lang/String;
    .param p3, "portNumber"    # Ljava/lang/String;
    .param p4, "deviceModel"    # Ljava/lang/String;
    .param p5, "connectionType"    # Ljava/lang/String;

    .prologue
    .line 231
    iget-object v0, p0, Lcom/samsung/android/allshare/KiesDeviceImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/allshare/KiesDeviceImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/samsung/android/allshare/IAllShareConnector;->isAllShareServiceConnected()Z

    move-result v0

    if-nez v0, :cond_1

    .line 232
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/KiesDeviceImpl;->mActionResponseListener:Lcom/samsung/android/allshare/KiesDevice$IKiesActionResponseListner;

    const-string v1, "Fail"

    sget-object v7, Lcom/samsung/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/samsung/android/allshare/ERROR;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-interface/range {v0 .. v7}, Lcom/samsung/android/allshare/KiesDevice$IKiesActionResponseListner;->onSetAutoSyncConnectionResponseReceived(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/ERROR;)V

    .line 252
    :goto_0
    return-void

    .line 238
    :cond_1
    new-instance v9, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v9}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 239
    .local v9, "msg":Lcom/sec/android/allshare/iface/CVMessage;
    const-string v0, "com.sec.android.allshare.action.ACTION_KIES_DEVICE_SET_AUTO_SYNC_CONNECTION"

    invoke-virtual {v9, v0}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 241
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 242
    .local v8, "bundle":Landroid/os/Bundle;
    const-string v0, "BUNDLE_STRING_ID"

    invoke-virtual {p0}, Lcom/samsung/android/allshare/KiesDeviceImpl;->getID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    const-string v0, "BUNDLE_STRING_KIES_DEVICE_UNIQUEID"

    invoke-virtual {v8, v0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    const-string v0, "BUNDLE_STRING_KIES_DEVICE_IPADDRESS"

    invoke-virtual {v8, v0, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    const-string v0, "BUNDLE_STRING_KIES_DEVICE_PORTNUMBER"

    invoke-virtual {v8, v0, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    const-string v0, "BUNDLE_STRING_KIES_DEVICE_MODEL"

    invoke-virtual {v8, v0, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    const-string v0, "BUNDLE_STRING_KIES_DEVICE_CONNECTIONTYPE"

    invoke-virtual {v8, v0, p5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    invoke-virtual {v9, v8}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 250
    iget-object v0, p0, Lcom/samsung/android/allshare/KiesDeviceImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    iget-object v1, p0, Lcom/samsung/android/allshare/KiesDeviceImpl;->mAllShareRespHandler:Lcom/samsung/android/allshare/AllShareResponseHandler;

    invoke-interface {v0, v9, v1}, Lcom/samsung/android/allshare/IAllShareConnector;->requestCVMAsync(Lcom/sec/android/allshare/iface/CVMessage;Lcom/samsung/android/allshare/AllShareResponseHandler;)J

    goto :goto_0
.end method

.method public setEventListener(Lcom/samsung/android/allshare/KiesDevice$IKiesEventListener;)Lcom/samsung/android/allshare/ERROR;
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/allshare/KiesDevice$IKiesEventListener;

    .prologue
    .line 182
    iget-object v0, p0, Lcom/samsung/android/allshare/KiesDeviceImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/allshare/KiesDeviceImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/samsung/android/allshare/IAllShareConnector;->isAllShareServiceConnected()Z

    move-result v0

    if-nez v0, :cond_1

    .line 183
    :cond_0
    const-string v0, "UPnPDeviceImpl"

    const-string v1, "setEventListener error! AllShareService is not connected"

    invoke-static {v0, v1}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    sget-object v0, Lcom/samsung/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/samsung/android/allshare/ERROR;

    .line 201
    :goto_0
    return-object v0

    .line 187
    :cond_1
    iput-object p1, p0, Lcom/samsung/android/allshare/KiesDeviceImpl;->mUPnPDeviceEventListener:Lcom/samsung/android/allshare/KiesDevice$IKiesEventListener;

    .line 189
    iget-boolean v0, p0, Lcom/samsung/android/allshare/KiesDeviceImpl;->mIsSubscribed:Z

    if-nez v0, :cond_3

    if-eqz p1, :cond_3

    .line 190
    iget-object v0, p0, Lcom/samsung/android/allshare/KiesDeviceImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    const-string v1, "com.sec.android.allshare.event.EVENT_DEVICE_SUBSCRIBE"

    iget-object v2, p0, Lcom/samsung/android/allshare/KiesDeviceImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/DeviceImpl;->getBundle()Landroid/os/Bundle;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/allshare/KiesDeviceImpl;->mEventHandler:Lcom/samsung/android/allshare/AllShareEventHandler;

    invoke-interface {v0, v1, v2, v3}, Lcom/samsung/android/allshare/IAllShareConnector;->subscribeAllShareEvent(Ljava/lang/String;Landroid/os/Bundle;Lcom/samsung/android/allshare/AllShareEventHandler;)Z

    .line 192
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/allshare/KiesDeviceImpl;->mIsSubscribed:Z

    .line 201
    :cond_2
    :goto_1
    sget-object v0, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    goto :goto_0

    .line 193
    :cond_3
    iget-boolean v0, p0, Lcom/samsung/android/allshare/KiesDeviceImpl;->mIsSubscribed:Z

    if-eqz v0, :cond_2

    if-nez p1, :cond_2

    .line 194
    iget-object v0, p0, Lcom/samsung/android/allshare/KiesDeviceImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    const-string v1, "com.sec.android.allshare.event.EVENT_DEVICE_SUBSCRIBE"

    iget-object v2, p0, Lcom/samsung/android/allshare/KiesDeviceImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/DeviceImpl;->getBundle()Landroid/os/Bundle;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/allshare/KiesDeviceImpl;->mEventHandler:Lcom/samsung/android/allshare/AllShareEventHandler;

    invoke-interface {v0, v1, v2, v3}, Lcom/samsung/android/allshare/IAllShareConnector;->unsubscribeAllShareEvent(Ljava/lang/String;Landroid/os/Bundle;Lcom/samsung/android/allshare/AllShareEventHandler;)V

    .line 196
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/allshare/KiesDeviceImpl;->mIsSubscribed:Z

    goto :goto_1
.end method

.method public setSyncConnectionInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9
    .param p1, "ipAddress"    # Ljava/lang/String;
    .param p2, "portNumber"    # Ljava/lang/String;
    .param p3, "deviceModel"    # Ljava/lang/String;
    .param p4, "connectionType"    # Ljava/lang/String;

    .prologue
    .line 207
    iget-object v0, p0, Lcom/samsung/android/allshare/KiesDeviceImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/allshare/KiesDeviceImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/samsung/android/allshare/IAllShareConnector;->isAllShareServiceConnected()Z

    move-result v0

    if-nez v0, :cond_1

    .line 208
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/KiesDeviceImpl;->mActionResponseListener:Lcom/samsung/android/allshare/KiesDevice$IKiesActionResponseListner;

    const-string v1, "Fail"

    sget-object v6, Lcom/samsung/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/samsung/android/allshare/ERROR;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-interface/range {v0 .. v6}, Lcom/samsung/android/allshare/KiesDevice$IKiesActionResponseListner;->onSetSyncConnectionResponseReceived(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/ERROR;)V

    .line 226
    :goto_0
    return-void

    .line 213
    :cond_1
    new-instance v8, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v8}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 214
    .local v8, "msg":Lcom/sec/android/allshare/iface/CVMessage;
    const-string v0, "com.sec.android.allshare.action.ACTION_KIES_DEVICE_SET_SYNC_CONNECTION"

    invoke-virtual {v8, v0}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 216
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 217
    .local v7, "bundle":Landroid/os/Bundle;
    const-string v0, "BUNDLE_STRING_ID"

    invoke-virtual {p0}, Lcom/samsung/android/allshare/KiesDeviceImpl;->getID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    const-string v0, "BUNDLE_STRING_KIES_DEVICE_IPADDRESS"

    invoke-virtual {v7, v0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    const-string v0, "BUNDLE_STRING_KIES_DEVICE_PORTNUMBER"

    invoke-virtual {v7, v0, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    const-string v0, "BUNDLE_STRING_KIES_DEVICE_MODEL"

    invoke-virtual {v7, v0, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    const-string v0, "BUNDLE_STRING_KIES_DEVICE_CONNECTIONTYPE"

    invoke-virtual {v7, v0, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    invoke-virtual {v8, v7}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 224
    iget-object v0, p0, Lcom/samsung/android/allshare/KiesDeviceImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    iget-object v1, p0, Lcom/samsung/android/allshare/KiesDeviceImpl;->mAllShareRespHandler:Lcom/samsung/android/allshare/AllShareResponseHandler;

    invoke-interface {v0, v8, v1}, Lcom/samsung/android/allshare/IAllShareConnector;->requestCVMAsync(Lcom/sec/android/allshare/iface/CVMessage;Lcom/samsung/android/allshare/AllShareResponseHandler;)J

    goto :goto_0
.end method
