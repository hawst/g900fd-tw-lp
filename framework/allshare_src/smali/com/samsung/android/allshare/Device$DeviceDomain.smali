.class public final enum Lcom/samsung/android/allshare/Device$DeviceDomain;
.super Ljava/lang/Enum;
.source "Device.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/Device;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DeviceDomain"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/allshare/Device$DeviceDomain;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/allshare/Device$DeviceDomain;

.field public static final enum LOCAL_NETWORK:Lcom/samsung/android/allshare/Device$DeviceDomain;

.field public static final enum MY_DEVICE:Lcom/samsung/android/allshare/Device$DeviceDomain;

.field public static final enum REMOTE_NETWORK:Lcom/samsung/android/allshare/Device$DeviceDomain;

.field public static final enum UNKNOWN:Lcom/samsung/android/allshare/Device$DeviceDomain;


# instance fields
.field private final enumString:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 47
    new-instance v0, Lcom/samsung/android/allshare/Device$DeviceDomain;

    const-string v1, "MY_DEVICE"

    const-string v2, "MY_DEVICE"

    invoke-direct {v0, v1, v3, v2}, Lcom/samsung/android/allshare/Device$DeviceDomain;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/allshare/Device$DeviceDomain;->MY_DEVICE:Lcom/samsung/android/allshare/Device$DeviceDomain;

    .line 51
    new-instance v0, Lcom/samsung/android/allshare/Device$DeviceDomain;

    const-string v1, "LOCAL_NETWORK"

    const-string v2, "LOCAL_NETWORK"

    invoke-direct {v0, v1, v4, v2}, Lcom/samsung/android/allshare/Device$DeviceDomain;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/allshare/Device$DeviceDomain;->LOCAL_NETWORK:Lcom/samsung/android/allshare/Device$DeviceDomain;

    .line 55
    new-instance v0, Lcom/samsung/android/allshare/Device$DeviceDomain;

    const-string v1, "REMOTE_NETWORK"

    const-string v2, "REMOTE_NETWORK"

    invoke-direct {v0, v1, v5, v2}, Lcom/samsung/android/allshare/Device$DeviceDomain;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/allshare/Device$DeviceDomain;->REMOTE_NETWORK:Lcom/samsung/android/allshare/Device$DeviceDomain;

    .line 59
    new-instance v0, Lcom/samsung/android/allshare/Device$DeviceDomain;

    const-string v1, "UNKNOWN"

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v1, v6, v2}, Lcom/samsung/android/allshare/Device$DeviceDomain;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/allshare/Device$DeviceDomain;->UNKNOWN:Lcom/samsung/android/allshare/Device$DeviceDomain;

    .line 43
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/samsung/android/allshare/Device$DeviceDomain;

    sget-object v1, Lcom/samsung/android/allshare/Device$DeviceDomain;->MY_DEVICE:Lcom/samsung/android/allshare/Device$DeviceDomain;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/allshare/Device$DeviceDomain;->LOCAL_NETWORK:Lcom/samsung/android/allshare/Device$DeviceDomain;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/android/allshare/Device$DeviceDomain;->REMOTE_NETWORK:Lcom/samsung/android/allshare/Device$DeviceDomain;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/android/allshare/Device$DeviceDomain;->UNKNOWN:Lcom/samsung/android/allshare/Device$DeviceDomain;

    aput-object v1, v0, v6

    sput-object v0, Lcom/samsung/android/allshare/Device$DeviceDomain;->$VALUES:[Lcom/samsung/android/allshare/Device$DeviceDomain;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "enumStr"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 64
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 65
    iput-object p3, p0, Lcom/samsung/android/allshare/Device$DeviceDomain;->enumString:Ljava/lang/String;

    .line 66
    return-void
.end method

.method public static stringToEnum(Ljava/lang/String;)Lcom/samsung/android/allshare/Device$DeviceDomain;
    .locals 1
    .param p0, "enumStr"    # Ljava/lang/String;

    .prologue
    .line 77
    if-nez p0, :cond_0

    .line 78
    sget-object v0, Lcom/samsung/android/allshare/Device$DeviceDomain;->UNKNOWN:Lcom/samsung/android/allshare/Device$DeviceDomain;

    .line 89
    :goto_0
    return-object v0

    .line 80
    :cond_0
    const-string v0, "LOCAL_NETWORK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 81
    sget-object v0, Lcom/samsung/android/allshare/Device$DeviceDomain;->LOCAL_NETWORK:Lcom/samsung/android/allshare/Device$DeviceDomain;

    goto :goto_0

    .line 82
    :cond_1
    const-string v0, "MY_DEVICE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 83
    sget-object v0, Lcom/samsung/android/allshare/Device$DeviceDomain;->MY_DEVICE:Lcom/samsung/android/allshare/Device$DeviceDomain;

    goto :goto_0

    .line 84
    :cond_2
    const-string v0, "REMOTE_NETWORK"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 85
    sget-object v0, Lcom/samsung/android/allshare/Device$DeviceDomain;->REMOTE_NETWORK:Lcom/samsung/android/allshare/Device$DeviceDomain;

    goto :goto_0

    .line 86
    :cond_3
    const-string v0, "UNKNOWN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 87
    sget-object v0, Lcom/samsung/android/allshare/Device$DeviceDomain;->UNKNOWN:Lcom/samsung/android/allshare/Device$DeviceDomain;

    goto :goto_0

    .line 89
    :cond_4
    sget-object v0, Lcom/samsung/android/allshare/Device$DeviceDomain;->UNKNOWN:Lcom/samsung/android/allshare/Device$DeviceDomain;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/allshare/Device$DeviceDomain;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 43
    const-class v0, Lcom/samsung/android/allshare/Device$DeviceDomain;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/Device$DeviceDomain;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/allshare/Device$DeviceDomain;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/samsung/android/allshare/Device$DeviceDomain;->$VALUES:[Lcom/samsung/android/allshare/Device$DeviceDomain;

    invoke-virtual {v0}, [Lcom/samsung/android/allshare/Device$DeviceDomain;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/allshare/Device$DeviceDomain;

    return-object v0
.end method


# virtual methods
.method public enumToString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/samsung/android/allshare/Device$DeviceDomain;->enumString:Ljava/lang/String;

    return-object v0
.end method
