.class Lcom/samsung/android/allshare/FileReceiverImpl$4;
.super Ljava/lang/Object;
.source "FileReceiverImpl.java"

# interfaces
.implements Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverProgressUpdateEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/FileReceiverImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/allshare/FileReceiverImpl;


# direct methods
.method constructor <init>(Lcom/samsung/android/allshare/FileReceiverImpl;)V
    .locals 0

    .prologue
    .line 578
    iput-object p1, p0, Lcom/samsung/android/allshare/FileReceiverImpl$4;->this$0:Lcom/samsung/android/allshare/FileReceiverImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompleted(Lcom/samsung/android/allshare/file/FileReceiver;Ljava/lang/String;Ljava/io/File;Lcom/samsung/android/allshare/ERROR;)V
    .locals 5
    .param p1, "receiver"    # Lcom/samsung/android/allshare/file/FileReceiver;
    .param p2, "sessionID"    # Ljava/lang/String;
    .param p3, "file"    # Ljava/io/File;
    .param p4, "err"    # Lcom/samsung/android/allshare/ERROR;

    .prologue
    .line 640
    const-string v2, "FileReceiverImpl"

    const-string v3, "onCompleted()"

    invoke-static {v2, v3}, Lcom/samsung/android/allshare/DLog;->d_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 642
    const/4 v0, 0x0

    .line 644
    .local v0, "listener":Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverProgressUpdateEventListener;
    iget-object v2, p0, Lcom/samsung/android/allshare/FileReceiverImpl$4;->this$0:Lcom/samsung/android/allshare/FileReceiverImpl;

    # invokes: Lcom/samsung/android/allshare/FileReceiverImpl;->getSessionKeyInfoMap(Ljava/lang/String;)Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;
    invoke-static {v2, p2}, Lcom/samsung/android/allshare/FileReceiverImpl;->access$000(Lcom/samsung/android/allshare/FileReceiverImpl;Ljava/lang/String;)Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;

    move-result-object v1

    .line 645
    .local v1, "tempSessionInfo":Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;
    if-nez v1, :cond_1

    .line 646
    const-string v2, "FileReceiverImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Null pointer Error!, sessionId="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 665
    :cond_0
    :goto_0
    return-void

    .line 650
    :cond_1
    invoke-virtual {v1}, Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;->getEventListener()Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverProgressUpdateEventListener;

    move-result-object v0

    .line 653
    if-eqz v0, :cond_2

    .line 654
    invoke-interface {v0, p1, p2, p3, p4}, Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverProgressUpdateEventListener;->onCompleted(Lcom/samsung/android/allshare/file/FileReceiver;Ljava/lang/String;Ljava/io/File;Lcom/samsung/android/allshare/ERROR;)V

    .line 659
    :goto_1
    invoke-virtual {v1}, Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;->completed()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 660
    const-string v2, "FileReceiverImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "All of FileTransfer was completed ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ",["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;->getCount()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/allshare/DLog;->i_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 662
    iget-object v2, p0, Lcom/samsung/android/allshare/FileReceiverImpl$4;->this$0:Lcom/samsung/android/allshare/FileReceiverImpl;

    # invokes: Lcom/samsung/android/allshare/FileReceiverImpl;->removeSessionKeyInfoMap(Ljava/lang/String;)V
    invoke-static {v2, p2}, Lcom/samsung/android/allshare/FileReceiverImpl;->access$200(Lcom/samsung/android/allshare/FileReceiverImpl;Ljava/lang/String;)V

    goto :goto_0

    .line 656
    :cond_2
    const-string v2, "FileReceiverImpl"

    const-string v3, "onCompleted listener is null!"

    invoke-static {v2, v3}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public onFailed(Lcom/samsung/android/allshare/file/FileReceiver;Ljava/lang/String;Ljava/io/File;Lcom/samsung/android/allshare/ERROR;)V
    .locals 5
    .param p1, "receiver"    # Lcom/samsung/android/allshare/file/FileReceiver;
    .param p2, "sessionID"    # Ljava/lang/String;
    .param p3, "file"    # Ljava/io/File;
    .param p4, "err"    # Lcom/samsung/android/allshare/ERROR;

    .prologue
    .line 608
    const-string v2, "FileReceiverImpl"

    const-string v3, "onFailed()"

    invoke-static {v2, v3}, Lcom/samsung/android/allshare/DLog;->d_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 610
    const/4 v0, 0x0

    .line 612
    .local v0, "listener":Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverProgressUpdateEventListener;
    iget-object v2, p0, Lcom/samsung/android/allshare/FileReceiverImpl$4;->this$0:Lcom/samsung/android/allshare/FileReceiverImpl;

    # invokes: Lcom/samsung/android/allshare/FileReceiverImpl;->getSessionKeyInfoMap(Ljava/lang/String;)Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;
    invoke-static {v2, p2}, Lcom/samsung/android/allshare/FileReceiverImpl;->access$000(Lcom/samsung/android/allshare/FileReceiverImpl;Ljava/lang/String;)Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;

    move-result-object v1

    .line 613
    .local v1, "tempSessionInfo":Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;
    if-nez v1, :cond_1

    .line 614
    const-string v2, "FileReceiverImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Null pointer Error!, sessionId="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 636
    :cond_0
    :goto_0
    return-void

    .line 619
    :cond_1
    invoke-virtual {v1}, Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;->getEventListener()Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverProgressUpdateEventListener;

    move-result-object v0

    .line 622
    if-eqz v0, :cond_2

    .line 623
    invoke-interface {v0, p1, p2, p3, p4}, Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverProgressUpdateEventListener;->onFailed(Lcom/samsung/android/allshare/file/FileReceiver;Ljava/lang/String;Ljava/io/File;Lcom/samsung/android/allshare/ERROR;)V

    .line 627
    :goto_1
    iget-object v2, p0, Lcom/samsung/android/allshare/FileReceiverImpl$4;->this$0:Lcom/samsung/android/allshare/FileReceiverImpl;

    # invokes: Lcom/samsung/android/allshare/FileReceiverImpl;->isCancelRequest()Z
    invoke-static {v2}, Lcom/samsung/android/allshare/FileReceiverImpl;->access$600(Lcom/samsung/android/allshare/FileReceiverImpl;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v1}, Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;->removed()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 629
    iget-object v2, p0, Lcom/samsung/android/allshare/FileReceiverImpl$4;->this$0:Lcom/samsung/android/allshare/FileReceiverImpl;

    # invokes: Lcom/samsung/android/allshare/FileReceiverImpl;->removeSessionKeyInfoMap(Ljava/lang/String;)V
    invoke-static {v2, p2}, Lcom/samsung/android/allshare/FileReceiverImpl;->access$200(Lcom/samsung/android/allshare/FileReceiverImpl;Ljava/lang/String;)V

    .line 631
    iget-object v2, p0, Lcom/samsung/android/allshare/FileReceiverImpl$4;->this$0:Lcom/samsung/android/allshare/FileReceiverImpl;

    # invokes: Lcom/samsung/android/allshare/FileReceiverImpl;->releaseCancelRequest()V
    invoke-static {v2}, Lcom/samsung/android/allshare/FileReceiverImpl;->access$700(Lcom/samsung/android/allshare/FileReceiverImpl;)V

    goto :goto_0

    .line 625
    :cond_2
    const-string v2, "FileReceiverImpl"

    const-string v3, "onFailed listener is null!"

    invoke-static {v2, v3}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 632
    :cond_3
    iget-object v2, p0, Lcom/samsung/android/allshare/FileReceiverImpl$4;->this$0:Lcom/samsung/android/allshare/FileReceiverImpl;

    # invokes: Lcom/samsung/android/allshare/FileReceiverImpl;->isCancelRequest()Z
    invoke-static {v2}, Lcom/samsung/android/allshare/FileReceiverImpl;->access$600(Lcom/samsung/android/allshare/FileReceiverImpl;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 634
    iget-object v2, p0, Lcom/samsung/android/allshare/FileReceiverImpl$4;->this$0:Lcom/samsung/android/allshare/FileReceiverImpl;

    # invokes: Lcom/samsung/android/allshare/FileReceiverImpl;->removeSessionKeyInfoMap(Ljava/lang/String;)V
    invoke-static {v2, p2}, Lcom/samsung/android/allshare/FileReceiverImpl;->access$200(Lcom/samsung/android/allshare/FileReceiverImpl;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onProgressUpdated(Lcom/samsung/android/allshare/file/FileReceiver;Ljava/lang/String;JJLjava/io/File;Lcom/samsung/android/allshare/ERROR;)V
    .locals 11
    .param p1, "receiver"    # Lcom/samsung/android/allshare/file/FileReceiver;
    .param p2, "sessionID"    # Ljava/lang/String;
    .param p3, "receivedSize"    # J
    .param p5, "totalSize"    # J
    .param p7, "file"    # Ljava/io/File;
    .param p8, "err"    # Lcom/samsung/android/allshare/ERROR;

    .prologue
    .line 583
    const-string v2, "FileReceiverImpl"

    const-string v3, "onProgressUpdated()"

    invoke-static {v2, v3}, Lcom/samsung/android/allshare/DLog;->d_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 586
    const/4 v1, 0x0

    .line 588
    .local v1, "listener":Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverProgressUpdateEventListener;
    iget-object v2, p0, Lcom/samsung/android/allshare/FileReceiverImpl$4;->this$0:Lcom/samsung/android/allshare/FileReceiverImpl;

    # invokes: Lcom/samsung/android/allshare/FileReceiverImpl;->getSessionKeyInfoMap(Ljava/lang/String;)Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;
    invoke-static {v2, p2}, Lcom/samsung/android/allshare/FileReceiverImpl;->access$000(Lcom/samsung/android/allshare/FileReceiverImpl;Ljava/lang/String;)Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;

    move-result-object v0

    .line 590
    .local v0, "tempSessionInfo":Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;
    if-nez v0, :cond_0

    .line 591
    const-string v2, "FileReceiverImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Null pointer Error!, sessionId="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 604
    :goto_0
    return-void

    .line 596
    :cond_0
    invoke-virtual {v0}, Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;->getEventListener()Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverProgressUpdateEventListener;

    move-result-object v1

    .line 599
    if-eqz v1, :cond_1

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-wide/from16 v6, p5

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    .line 600
    invoke-interface/range {v1 .. v9}, Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverProgressUpdateEventListener;->onProgressUpdated(Lcom/samsung/android/allshare/file/FileReceiver;Ljava/lang/String;JJLjava/io/File;Lcom/samsung/android/allshare/ERROR;)V

    goto :goto_0

    .line 602
    :cond_1
    const-string v2, "FileReceiverImpl"

    const-string v3, "onProgressUpdated listener is null!"

    invoke-static {v2, v3}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
