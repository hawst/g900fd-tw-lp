.class public final enum Lcom/samsung/android/allshare/Item$ContentBuildType;
.super Ljava/lang/Enum;
.source "Item.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/Item;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ContentBuildType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/allshare/Item$ContentBuildType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/allshare/Item$ContentBuildType;

.field public static final enum LOCAL:Lcom/samsung/android/allshare/Item$ContentBuildType;

.field public static final enum PROVIDER:Lcom/samsung/android/allshare/Item$ContentBuildType;

.field public static final enum UNKNOWN:Lcom/samsung/android/allshare/Item$ContentBuildType;

.field public static final enum WEB:Lcom/samsung/android/allshare/Item$ContentBuildType;


# instance fields
.field private final enumString:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 123
    new-instance v0, Lcom/samsung/android/allshare/Item$ContentBuildType;

    const-string v1, "LOCAL"

    const-string v2, "LOCAL"

    invoke-direct {v0, v1, v3, v2}, Lcom/samsung/android/allshare/Item$ContentBuildType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/allshare/Item$ContentBuildType;->LOCAL:Lcom/samsung/android/allshare/Item$ContentBuildType;

    .line 128
    new-instance v0, Lcom/samsung/android/allshare/Item$ContentBuildType;

    const-string v1, "PROVIDER"

    const-string v2, "PROVIDER"

    invoke-direct {v0, v1, v4, v2}, Lcom/samsung/android/allshare/Item$ContentBuildType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/allshare/Item$ContentBuildType;->PROVIDER:Lcom/samsung/android/allshare/Item$ContentBuildType;

    .line 130
    new-instance v0, Lcom/samsung/android/allshare/Item$ContentBuildType;

    const-string v1, "WEB"

    const-string v2, "WEB"

    invoke-direct {v0, v1, v5, v2}, Lcom/samsung/android/allshare/Item$ContentBuildType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/allshare/Item$ContentBuildType;->WEB:Lcom/samsung/android/allshare/Item$ContentBuildType;

    .line 132
    new-instance v0, Lcom/samsung/android/allshare/Item$ContentBuildType;

    const-string v1, "UNKNOWN"

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v1, v6, v2}, Lcom/samsung/android/allshare/Item$ContentBuildType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/allshare/Item$ContentBuildType;->UNKNOWN:Lcom/samsung/android/allshare/Item$ContentBuildType;

    .line 121
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/samsung/android/allshare/Item$ContentBuildType;

    sget-object v1, Lcom/samsung/android/allshare/Item$ContentBuildType;->LOCAL:Lcom/samsung/android/allshare/Item$ContentBuildType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/allshare/Item$ContentBuildType;->PROVIDER:Lcom/samsung/android/allshare/Item$ContentBuildType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/android/allshare/Item$ContentBuildType;->WEB:Lcom/samsung/android/allshare/Item$ContentBuildType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/android/allshare/Item$ContentBuildType;->UNKNOWN:Lcom/samsung/android/allshare/Item$ContentBuildType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/samsung/android/allshare/Item$ContentBuildType;->$VALUES:[Lcom/samsung/android/allshare/Item$ContentBuildType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "enumStr"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 138
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 139
    iput-object p3, p0, Lcom/samsung/android/allshare/Item$ContentBuildType;->enumString:Ljava/lang/String;

    .line 140
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/allshare/Item$ContentBuildType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 121
    const-class v0, Lcom/samsung/android/allshare/Item$ContentBuildType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/Item$ContentBuildType;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/allshare/Item$ContentBuildType;
    .locals 1

    .prologue
    .line 121
    sget-object v0, Lcom/samsung/android/allshare/Item$ContentBuildType;->$VALUES:[Lcom/samsung/android/allshare/Item$ContentBuildType;

    invoke-virtual {v0}, [Lcom/samsung/android/allshare/Item$ContentBuildType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/allshare/Item$ContentBuildType;

    return-object v0
.end method


# virtual methods
.method public enumToString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/samsung/android/allshare/Item$ContentBuildType;->enumString:Ljava/lang/String;

    return-object v0
.end method
