.class Lcom/samsung/android/allshare/FileReceiverImpl$2;
.super Lcom/samsung/android/allshare/AllShareResponseHandler;
.source "FileReceiverImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/FileReceiverImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/allshare/FileReceiverImpl;


# direct methods
.method constructor <init>(Lcom/samsung/android/allshare/FileReceiverImpl;Landroid/os/Looper;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Looper;

    .prologue
    .line 318
    iput-object p1, p0, Lcom/samsung/android/allshare/FileReceiverImpl$2;->this$0:Lcom/samsung/android/allshare/FileReceiverImpl;

    invoke-direct {p0, p2}, Lcom/samsung/android/allshare/AllShareResponseHandler;-><init>(Landroid/os/Looper;)V

    return-void
.end method

.method private notifyListResponse(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 15
    .param p1, "action"    # Ljava/lang/String;
    .param p2, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 336
    const-string v2, "BUNDLE_ENUM_ERROR"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 337
    .local v7, "errStr":Ljava/lang/String;
    const-string v2, "BUNDLE_STRING_NAME"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 339
    .local v5, "name":Ljava/lang/String;
    const-string v2, "BUNDLE_STRING_SESSIONID"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 340
    .local v3, "sessionId":Ljava/lang/String;
    const-string v2, "BUNDLE_STRING_UNIQUEKEY"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 342
    .local v12, "uniqueKey":Ljava/lang/String;
    const-string v2, "FileReceiverImpl"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "action : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " sessionID : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v2, v13}, Lcom/samsung/android/allshare/DLog;->d_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 345
    invoke-static {v7}, Lcom/samsung/android/allshare/ERROR;->stringToEnum(Ljava/lang/String;)Lcom/samsung/android/allshare/ERROR;

    move-result-object v6

    .line 346
    .local v6, "err":Lcom/samsung/android/allshare/ERROR;
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 348
    .local v4, "fileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    const-string v2, "com.sec.android.allshare.action.ACTION_FILE_ARRAYLIST_RECEIVER_RECEIVE"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 349
    const-string v2, "FileReceiverImpl"

    const-string v13, "notifyListResponse() ACTION_FILE_ARRAYLIST_RECEIVER_RECEIVE"

    invoke-static {v2, v13}, Lcom/samsung/android/allshare/DLog;->d_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 352
    iget-object v2, p0, Lcom/samsung/android/allshare/FileReceiverImpl$2;->this$0:Lcom/samsung/android/allshare/FileReceiverImpl;

    # invokes: Lcom/samsung/android/allshare/FileReceiverImpl;->getTimeKeyInfoMap(Ljava/lang/String;)Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;
    invoke-static {v2, v12}, Lcom/samsung/android/allshare/FileReceiverImpl;->access$300(Lcom/samsung/android/allshare/FileReceiverImpl;Ljava/lang/String;)Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;

    move-result-object v11

    .line 353
    .local v11, "tempSessionInfo":Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;
    if-nez v11, :cond_1

    .line 354
    const-string v2, "FileReceiverImpl"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Null pointer Error!, uniqueKey="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v2, v13}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 412
    .end local v11    # "tempSessionInfo":Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;
    :cond_0
    :goto_0
    return-void

    .line 357
    .restart local v11    # "tempSessionInfo":Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/allshare/FileReceiverImpl$2;->this$0:Lcom/samsung/android/allshare/FileReceiverImpl;

    # invokes: Lcom/samsung/android/allshare/FileReceiverImpl;->putSessionKeyInfoMap(Ljava/lang/String;Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;)V
    invoke-static {v2, v3, v11}, Lcom/samsung/android/allshare/FileReceiverImpl;->access$400(Lcom/samsung/android/allshare/FileReceiverImpl;Ljava/lang/String;Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;)V

    .line 360
    iget-object v2, p0, Lcom/samsung/android/allshare/FileReceiverImpl$2;->this$0:Lcom/samsung/android/allshare/FileReceiverImpl;

    # invokes: Lcom/samsung/android/allshare/FileReceiverImpl;->removeTimeKeyInfoMap(Ljava/lang/String;)V
    invoke-static {v2, v12}, Lcom/samsung/android/allshare/FileReceiverImpl;->access$500(Lcom/samsung/android/allshare/FileReceiverImpl;Ljava/lang/String;)V

    .line 362
    const/4 v1, 0x0

    .line 364
    .local v1, "listener":Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverReceiveResponseListener;
    iget-object v2, p0, Lcom/samsung/android/allshare/FileReceiverImpl$2;->this$0:Lcom/samsung/android/allshare/FileReceiverImpl;

    # invokes: Lcom/samsung/android/allshare/FileReceiverImpl;->getSessionKeyInfoMap(Ljava/lang/String;)Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;
    invoke-static {v2, v3}, Lcom/samsung/android/allshare/FileReceiverImpl;->access$000(Lcom/samsung/android/allshare/FileReceiverImpl;Ljava/lang/String;)Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;

    move-result-object v11

    .line 366
    if-nez v11, :cond_2

    .line 367
    const-string v2, "FileReceiverImpl"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Null pointer Error!, sessionId="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v2, v13}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 371
    :cond_2
    invoke-virtual {v11}, Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;->getResponseListener()Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverReceiveResponseListener;

    move-result-object v1

    .line 373
    const-string v2, "BUNDLE_STRING_ARRAYLIST_FILE_PATH"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v8

    .line 375
    .local v8, "filePathList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 376
    .local v10, "path":Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 378
    .end local v10    # "path":Ljava/lang/String;
    :cond_3
    if-eqz v1, :cond_4

    .line 379
    iget-object v2, p0, Lcom/samsung/android/allshare/FileReceiverImpl$2;->this$0:Lcom/samsung/android/allshare/FileReceiverImpl;

    # getter for: Lcom/samsung/android/allshare/FileReceiverImpl;->mReceiver:Lcom/samsung/android/allshare/file/FileReceiver;
    invoke-static {v2}, Lcom/samsung/android/allshare/FileReceiverImpl;->access$100(Lcom/samsung/android/allshare/FileReceiverImpl;)Lcom/samsung/android/allshare/file/FileReceiver;

    move-result-object v2

    invoke-interface/range {v1 .. v6}, Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverReceiveResponseListener;->onReceiveResponseReceived(Lcom/samsung/android/allshare/file/FileReceiver;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Lcom/samsung/android/allshare/ERROR;)V

    goto :goto_0

    .line 381
    :cond_4
    const-string v2, "FileReceiverImpl"

    const-string v13, "onReceiveResponseReceived listener is null!"

    invoke-static {v2, v13}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 383
    .end local v1    # "listener":Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverReceiveResponseListener;
    .end local v8    # "filePathList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v9    # "i$":Ljava/util/Iterator;
    .end local v11    # "tempSessionInfo":Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;
    :cond_5
    const-string v2, "com.sec.android.allshare.action.ACTION_FILE_ARRAYLIST_RECEIVER_CANCEL"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 384
    const-string v2, "FileReceiverImpl"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "notifyListResponse() ACTION_FILE_ARRAYLIST_RECEIVER_CANCEL  sessionID : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v2, v13}, Lcom/samsung/android/allshare/DLog;->d_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 388
    const/4 v1, 0x0

    .line 390
    .restart local v1    # "listener":Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverReceiveResponseListener;
    iget-object v2, p0, Lcom/samsung/android/allshare/FileReceiverImpl$2;->this$0:Lcom/samsung/android/allshare/FileReceiverImpl;

    # invokes: Lcom/samsung/android/allshare/FileReceiverImpl;->getSessionKeyInfoMap(Ljava/lang/String;)Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;
    invoke-static {v2, v3}, Lcom/samsung/android/allshare/FileReceiverImpl;->access$000(Lcom/samsung/android/allshare/FileReceiverImpl;Ljava/lang/String;)Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;

    move-result-object v11

    .line 391
    .restart local v11    # "tempSessionInfo":Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;
    if-nez v11, :cond_6

    .line 392
    const-string v2, "FileReceiverImpl"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Null pointer Error!, sessionId= "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v2, v13}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 396
    :cond_6
    invoke-virtual {v11}, Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;->getResponseListener()Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverReceiveResponseListener;

    move-result-object v1

    .line 398
    if-eqz v1, :cond_7

    .line 399
    const-string v2, "FileReceiverImpl"

    const-string v13, "listener.onCancelResponseReceived( mReceiver, sessionId, err )"

    invoke-static {v2, v13}, Lcom/samsung/android/allshare/DLog;->d_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 401
    iget-object v2, p0, Lcom/samsung/android/allshare/FileReceiverImpl$2;->this$0:Lcom/samsung/android/allshare/FileReceiverImpl;

    # getter for: Lcom/samsung/android/allshare/FileReceiverImpl;->mReceiver:Lcom/samsung/android/allshare/file/FileReceiver;
    invoke-static {v2}, Lcom/samsung/android/allshare/FileReceiverImpl;->access$100(Lcom/samsung/android/allshare/FileReceiverImpl;)Lcom/samsung/android/allshare/file/FileReceiver;

    move-result-object v2

    invoke-interface {v1, v2, v3, v6}, Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverReceiveResponseListener;->onCancelResponseReceived(Lcom/samsung/android/allshare/file/FileReceiver;Ljava/lang/String;Lcom/samsung/android/allshare/ERROR;)V

    .line 403
    iget-object v2, p0, Lcom/samsung/android/allshare/FileReceiverImpl$2;->this$0:Lcom/samsung/android/allshare/FileReceiverImpl;

    # invokes: Lcom/samsung/android/allshare/FileReceiverImpl;->isCancelRequest()Z
    invoke-static {v2}, Lcom/samsung/android/allshare/FileReceiverImpl;->access$600(Lcom/samsung/android/allshare/FileReceiverImpl;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v11}, Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;->removed()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 405
    iget-object v2, p0, Lcom/samsung/android/allshare/FileReceiverImpl$2;->this$0:Lcom/samsung/android/allshare/FileReceiverImpl;

    # invokes: Lcom/samsung/android/allshare/FileReceiverImpl;->removeSessionKeyInfoMap(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/samsung/android/allshare/FileReceiverImpl;->access$200(Lcom/samsung/android/allshare/FileReceiverImpl;Ljava/lang/String;)V

    .line 407
    iget-object v2, p0, Lcom/samsung/android/allshare/FileReceiverImpl$2;->this$0:Lcom/samsung/android/allshare/FileReceiverImpl;

    # invokes: Lcom/samsung/android/allshare/FileReceiverImpl;->releaseCancelRequest()V
    invoke-static {v2}, Lcom/samsung/android/allshare/FileReceiverImpl;->access$700(Lcom/samsung/android/allshare/FileReceiverImpl;)V

    goto/16 :goto_0

    .line 410
    :cond_7
    const-string v2, "FileReceiverImpl"

    const-string v13, "onCancelResponseReceived listener is null!"

    invoke-static {v2, v13}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private notifyResponse(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 12
    .param p1, "action"    # Ljava/lang/String;
    .param p2, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 415
    const-string v1, "BUNDLE_ENUM_ERROR"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 416
    .local v6, "errStr":Ljava/lang/String;
    const-string v1, "BUNDLE_STRING_NAME"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 417
    .local v4, "name":Ljava/lang/String;
    const-string v1, "BUNDLE_STRING_SESSIONID"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 418
    .local v2, "sessionId":Ljava/lang/String;
    const-string v1, "BUNDLE_STRING_UNIQUEKEY"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 420
    .local v9, "uniqueKey":Ljava/lang/String;
    invoke-static {v6}, Lcom/samsung/android/allshare/ERROR;->stringToEnum(Ljava/lang/String;)Lcom/samsung/android/allshare/ERROR;

    move-result-object v5

    .line 422
    .local v5, "err":Lcom/samsung/android/allshare/ERROR;
    const-string v1, "com.sec.android.allshare.action.ACTION_FILE_RECEIVER_RECEIVE"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 423
    const-string v1, "FileReceiverImpl"

    const-string v10, "notifyListResponse()- ACTION_FILE_RECEIVER_RECEIVE"

    invoke-static {v1, v10}, Lcom/samsung/android/allshare/DLog;->d_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 425
    const/4 v0, 0x0

    .line 427
    .local v0, "listener":Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverReceiveResponseListener;
    iget-object v1, p0, Lcom/samsung/android/allshare/FileReceiverImpl$2;->this$0:Lcom/samsung/android/allshare/FileReceiverImpl;

    # invokes: Lcom/samsung/android/allshare/FileReceiverImpl;->getTimeKeyInfoMap(Ljava/lang/String;)Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;
    invoke-static {v1, v9}, Lcom/samsung/android/allshare/FileReceiverImpl;->access$300(Lcom/samsung/android/allshare/FileReceiverImpl;Ljava/lang/String;)Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;

    move-result-object v8

    .line 428
    .local v8, "tempSessionInfo":Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;
    if-nez v8, :cond_1

    .line 429
    const-string v1, "FileReceiverImpl"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Null pointer Error!, uniqueKey="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v1, v10}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 479
    .end local v0    # "listener":Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverReceiveResponseListener;
    .end local v8    # "tempSessionInfo":Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;
    :cond_0
    :goto_0
    return-void

    .line 433
    .restart local v0    # "listener":Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverReceiveResponseListener;
    .restart local v8    # "tempSessionInfo":Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/allshare/FileReceiverImpl$2;->this$0:Lcom/samsung/android/allshare/FileReceiverImpl;

    # invokes: Lcom/samsung/android/allshare/FileReceiverImpl;->putSessionKeyInfoMap(Ljava/lang/String;Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;)V
    invoke-static {v1, v2, v8}, Lcom/samsung/android/allshare/FileReceiverImpl;->access$400(Lcom/samsung/android/allshare/FileReceiverImpl;Ljava/lang/String;Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;)V

    .line 435
    iget-object v1, p0, Lcom/samsung/android/allshare/FileReceiverImpl$2;->this$0:Lcom/samsung/android/allshare/FileReceiverImpl;

    # invokes: Lcom/samsung/android/allshare/FileReceiverImpl;->removeTimeKeyInfoMap(Ljava/lang/String;)V
    invoke-static {v1, v9}, Lcom/samsung/android/allshare/FileReceiverImpl;->access$500(Lcom/samsung/android/allshare/FileReceiverImpl;Ljava/lang/String;)V

    .line 437
    iget-object v1, p0, Lcom/samsung/android/allshare/FileReceiverImpl$2;->this$0:Lcom/samsung/android/allshare/FileReceiverImpl;

    # invokes: Lcom/samsung/android/allshare/FileReceiverImpl;->getSessionKeyInfoMap(Ljava/lang/String;)Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;
    invoke-static {v1, v2}, Lcom/samsung/android/allshare/FileReceiverImpl;->access$000(Lcom/samsung/android/allshare/FileReceiverImpl;Ljava/lang/String;)Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;

    move-result-object v8

    .line 438
    if-nez v8, :cond_2

    .line 439
    const-string v1, "FileReceiverImpl"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Null pointer Error!, sessionId="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v1, v10}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 443
    :cond_2
    invoke-virtual {v8}, Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;->getResponseListener()Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverReceiveResponseListener;

    move-result-object v0

    .line 445
    const-string v1, "BUNDLE_STRING_FILE_PATH"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 446
    .local v7, "filePath":Ljava/lang/String;
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 447
    .local v3, "fileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 449
    if-eqz v0, :cond_3

    .line 450
    iget-object v1, p0, Lcom/samsung/android/allshare/FileReceiverImpl$2;->this$0:Lcom/samsung/android/allshare/FileReceiverImpl;

    # getter for: Lcom/samsung/android/allshare/FileReceiverImpl;->mReceiver:Lcom/samsung/android/allshare/file/FileReceiver;
    invoke-static {v1}, Lcom/samsung/android/allshare/FileReceiverImpl;->access$100(Lcom/samsung/android/allshare/FileReceiverImpl;)Lcom/samsung/android/allshare/file/FileReceiver;

    move-result-object v1

    invoke-interface/range {v0 .. v5}, Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverReceiveResponseListener;->onReceiveResponseReceived(Lcom/samsung/android/allshare/file/FileReceiver;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Lcom/samsung/android/allshare/ERROR;)V

    goto :goto_0

    .line 452
    :cond_3
    const-string v1, "FileReceiverImpl"

    const-string v10, "onReceiveResponseReceived listener is null!"

    invoke-static {v1, v10}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 454
    .end local v0    # "listener":Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverReceiveResponseListener;
    .end local v3    # "fileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    .end local v7    # "filePath":Ljava/lang/String;
    .end local v8    # "tempSessionInfo":Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;
    :cond_4
    const-string v1, "com.sec.android.allshare.action.ACTION_FILE_RECEIVER_CANCEL"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 455
    const-string v1, "FileReceiverImpl"

    const-string v10, "notifyListResponse()- ACTION_FILE_RECEIVER_CANCEL"

    invoke-static {v1, v10}, Lcom/samsung/android/allshare/DLog;->d_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 457
    const/4 v0, 0x0

    .line 459
    .restart local v0    # "listener":Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverReceiveResponseListener;
    iget-object v1, p0, Lcom/samsung/android/allshare/FileReceiverImpl$2;->this$0:Lcom/samsung/android/allshare/FileReceiverImpl;

    # invokes: Lcom/samsung/android/allshare/FileReceiverImpl;->getSessionKeyInfoMap(Ljava/lang/String;)Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;
    invoke-static {v1, v2}, Lcom/samsung/android/allshare/FileReceiverImpl;->access$000(Lcom/samsung/android/allshare/FileReceiverImpl;Ljava/lang/String;)Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;

    move-result-object v8

    .line 460
    .restart local v8    # "tempSessionInfo":Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;
    if-nez v8, :cond_5

    .line 461
    const-string v1, "FileReceiverImpl"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Null pointer Error!, sessionId="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v1, v10}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 466
    :cond_5
    invoke-virtual {v8}, Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;->getResponseListener()Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverReceiveResponseListener;

    move-result-object v0

    .line 467
    if-eqz v0, :cond_6

    .line 468
    iget-object v1, p0, Lcom/samsung/android/allshare/FileReceiverImpl$2;->this$0:Lcom/samsung/android/allshare/FileReceiverImpl;

    # getter for: Lcom/samsung/android/allshare/FileReceiverImpl;->mReceiver:Lcom/samsung/android/allshare/file/FileReceiver;
    invoke-static {v1}, Lcom/samsung/android/allshare/FileReceiverImpl;->access$100(Lcom/samsung/android/allshare/FileReceiverImpl;)Lcom/samsung/android/allshare/file/FileReceiver;

    move-result-object v1

    invoke-interface {v0, v1, v2, v5}, Lcom/samsung/android/allshare/file/FileReceiver$IFileReceiverReceiveResponseListener;->onCancelResponseReceived(Lcom/samsung/android/allshare/file/FileReceiver;Ljava/lang/String;Lcom/samsung/android/allshare/ERROR;)V

    .line 470
    iget-object v1, p0, Lcom/samsung/android/allshare/FileReceiverImpl$2;->this$0:Lcom/samsung/android/allshare/FileReceiverImpl;

    # invokes: Lcom/samsung/android/allshare/FileReceiverImpl;->isCancelRequest()Z
    invoke-static {v1}, Lcom/samsung/android/allshare/FileReceiverImpl;->access$600(Lcom/samsung/android/allshare/FileReceiverImpl;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v8}, Lcom/samsung/android/allshare/FileReceiverImpl$SessionInfo;->removed()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 472
    iget-object v1, p0, Lcom/samsung/android/allshare/FileReceiverImpl$2;->this$0:Lcom/samsung/android/allshare/FileReceiverImpl;

    # invokes: Lcom/samsung/android/allshare/FileReceiverImpl;->removeSessionKeyInfoMap(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/samsung/android/allshare/FileReceiverImpl;->access$200(Lcom/samsung/android/allshare/FileReceiverImpl;Ljava/lang/String;)V

    .line 474
    iget-object v1, p0, Lcom/samsung/android/allshare/FileReceiverImpl$2;->this$0:Lcom/samsung/android/allshare/FileReceiverImpl;

    # invokes: Lcom/samsung/android/allshare/FileReceiverImpl;->releaseCancelRequest()V
    invoke-static {v1}, Lcom/samsung/android/allshare/FileReceiverImpl;->access$700(Lcom/samsung/android/allshare/FileReceiverImpl;)V

    goto/16 :goto_0

    .line 477
    :cond_6
    const-string v1, "FileReceiverImpl"

    const-string v10, "onCancelResponseReceived listener is null!"

    invoke-static {v1, v10}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public handleResponseMessage(Lcom/sec/android/allshare/iface/CVMessage;)V
    .locals 3
    .param p1, "cvm"    # Lcom/sec/android/allshare/iface/CVMessage;

    .prologue
    .line 321
    invoke-virtual {p1}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v0

    .line 322
    .local v0, "action":Ljava/lang/String;
    const-string v1, "com.sec.android.allshare.action.ACTION_FILE_RECEIVER_RECEIVE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 323
    invoke-virtual {p1}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/allshare/FileReceiverImpl$2;->notifyResponse(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 327
    :cond_0
    :goto_0
    const-string v1, "com.sec.android.allshare.action.ACTION_FILE_ARRAYLIST_RECEIVER_RECEIVE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 328
    const-string v1, "FileReceiverImpl"

    const-string v2, "mRespHandler.handleResponseMessage() called.."

    invoke-static {v1, v2}, Lcom/samsung/android/allshare/DLog;->d_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 329
    invoke-virtual {p1}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/allshare/FileReceiverImpl$2;->notifyListResponse(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 333
    :cond_1
    :goto_1
    return-void

    .line 324
    :cond_2
    const-string v1, "com.sec.android.allshare.action.ACTION_FILE_RECEIVER_CANCEL"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 325
    invoke-virtual {p1}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/allshare/FileReceiverImpl$2;->notifyResponse(Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0

    .line 330
    :cond_3
    const-string v1, "com.sec.android.allshare.action.ACTION_FILE_ARRAYLIST_RECEIVER_CANCEL"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 331
    invoke-virtual {p1}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/allshare/FileReceiverImpl$2;->notifyListResponse(Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_1
.end method
