.class Lcom/samsung/android/allshare/ServiceConnector$ProfileConstData;
.super Ljava/lang/Object;
.source "ServiceConnector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/ServiceConnector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ProfileConstData"
.end annotation


# instance fields
.field public ALLSHARE_FRAMEWORK_VERSION:J

.field public CP_NAME:Ljava/lang/String;

.field public DEV_MODE:Z

.field public SERVICE_MANAGER_NAME_VERSION_1:Ljava/lang/String;

.field public SET_NAME_MESSAGE:Ljava/lang/String;

.field public START_MESSAGE:Ljava/lang/String;

.field public START_SERVICE:Ljava/lang/String;

.field public STOP_MESSAGE:Ljava/lang/String;

.field public SUBSCRIBER_FIELD:Ljava/lang/String;

.field public SUBSCRIPTION_MESSAGE:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 709
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 686
    const-wide/16 v0, 0x1

    iput-wide v0, p0, Lcom/samsung/android/allshare/ServiceConnector$ProfileConstData;->ALLSHARE_FRAMEWORK_VERSION:J

    .line 688
    sget-boolean v0, Lcom/sec/android/allshare/iface/Const;->DEV_MODE:Z

    iput-boolean v0, p0, Lcom/samsung/android/allshare/ServiceConnector$ProfileConstData;->DEV_MODE:Z

    .line 693
    const-string v0, "com.sec.android.allshare.framework.ServiceManager.START_SERVICE"

    iput-object v0, p0, Lcom/samsung/android/allshare/ServiceConnector$ProfileConstData;->START_SERVICE:Ljava/lang/String;

    .line 695
    const-string v0, "com.sec.android.allshare.framework.ServiceManager.START_COMPLETED"

    iput-object v0, p0, Lcom/samsung/android/allshare/ServiceConnector$ProfileConstData;->START_MESSAGE:Ljava/lang/String;

    .line 697
    const-string v0, "com.sec.android.allshare.framework.ServiceManager.STOP_COMPLETED"

    iput-object v0, p0, Lcom/samsung/android/allshare/ServiceConnector$ProfileConstData;->STOP_MESSAGE:Ljava/lang/String;

    .line 699
    const-string v0, "com.sec.android.allshare.framework.ServiceManager"

    iput-object v0, p0, Lcom/samsung/android/allshare/ServiceConnector$ProfileConstData;->SERVICE_MANAGER_NAME_VERSION_1:Ljava/lang/String;

    .line 701
    const-string v0, "com.sec.android.allshare.iface.subscriber"

    iput-object v0, p0, Lcom/samsung/android/allshare/ServiceConnector$ProfileConstData;->SUBSCRIBER_FIELD:Ljava/lang/String;

    .line 703
    const-string v0, "com.sec.android.allshare.framework.SUBSCRIBE_SERVICE"

    iput-object v0, p0, Lcom/samsung/android/allshare/ServiceConnector$ProfileConstData;->SUBSCRIPTION_MESSAGE:Ljava/lang/String;

    .line 705
    const-string v0, "com.sec.android.allshare.framework.AllShareCpName"

    iput-object v0, p0, Lcom/samsung/android/allshare/ServiceConnector$ProfileConstData;->SET_NAME_MESSAGE:Ljava/lang/String;

    .line 707
    const-string v0, "com.sec.android.allshare.CpName"

    iput-object v0, p0, Lcom/samsung/android/allshare/ServiceConnector$ProfileConstData;->CP_NAME:Ljava/lang/String;

    .line 710
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 4
    .param p1, "serviceType"    # Ljava/lang/String;

    .prologue
    const-wide/16 v2, 0x1

    .line 712
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 686
    iput-wide v2, p0, Lcom/samsung/android/allshare/ServiceConnector$ProfileConstData;->ALLSHARE_FRAMEWORK_VERSION:J

    .line 688
    sget-boolean v0, Lcom/sec/android/allshare/iface/Const;->DEV_MODE:Z

    iput-boolean v0, p0, Lcom/samsung/android/allshare/ServiceConnector$ProfileConstData;->DEV_MODE:Z

    .line 693
    const-string v0, "com.sec.android.allshare.framework.ServiceManager.START_SERVICE"

    iput-object v0, p0, Lcom/samsung/android/allshare/ServiceConnector$ProfileConstData;->START_SERVICE:Ljava/lang/String;

    .line 695
    const-string v0, "com.sec.android.allshare.framework.ServiceManager.START_COMPLETED"

    iput-object v0, p0, Lcom/samsung/android/allshare/ServiceConnector$ProfileConstData;->START_MESSAGE:Ljava/lang/String;

    .line 697
    const-string v0, "com.sec.android.allshare.framework.ServiceManager.STOP_COMPLETED"

    iput-object v0, p0, Lcom/samsung/android/allshare/ServiceConnector$ProfileConstData;->STOP_MESSAGE:Ljava/lang/String;

    .line 699
    const-string v0, "com.sec.android.allshare.framework.ServiceManager"

    iput-object v0, p0, Lcom/samsung/android/allshare/ServiceConnector$ProfileConstData;->SERVICE_MANAGER_NAME_VERSION_1:Ljava/lang/String;

    .line 701
    const-string v0, "com.sec.android.allshare.iface.subscriber"

    iput-object v0, p0, Lcom/samsung/android/allshare/ServiceConnector$ProfileConstData;->SUBSCRIBER_FIELD:Ljava/lang/String;

    .line 703
    const-string v0, "com.sec.android.allshare.framework.SUBSCRIBE_SERVICE"

    iput-object v0, p0, Lcom/samsung/android/allshare/ServiceConnector$ProfileConstData;->SUBSCRIPTION_MESSAGE:Ljava/lang/String;

    .line 705
    const-string v0, "com.sec.android.allshare.framework.AllShareCpName"

    iput-object v0, p0, Lcom/samsung/android/allshare/ServiceConnector$ProfileConstData;->SET_NAME_MESSAGE:Ljava/lang/String;

    .line 707
    const-string v0, "com.sec.android.allshare.CpName"

    iput-object v0, p0, Lcom/samsung/android/allshare/ServiceConnector$ProfileConstData;->CP_NAME:Ljava/lang/String;

    .line 713
    if-eqz p1, :cond_1

    const-string v0, "com.samsung.android.allshare.service.mediashare"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 715
    iput-wide v2, p0, Lcom/samsung/android/allshare/ServiceConnector$ProfileConstData;->ALLSHARE_FRAMEWORK_VERSION:J

    .line 716
    sget-boolean v0, Lcom/samsung/android/allshare/media/Const;->DEV_MODE:Z

    iput-boolean v0, p0, Lcom/samsung/android/allshare/ServiceConnector$ProfileConstData;->DEV_MODE:Z

    .line 719
    const-string v0, "com.samsung.android.allshare.service.mediashare.ServiceManager.START_SERVICE"

    iput-object v0, p0, Lcom/samsung/android/allshare/ServiceConnector$ProfileConstData;->START_SERVICE:Ljava/lang/String;

    .line 720
    const-string v0, "com.samsung.android.allshare.service.mediashare.ServiceManager.START_COMPLETED"

    iput-object v0, p0, Lcom/samsung/android/allshare/ServiceConnector$ProfileConstData;->START_MESSAGE:Ljava/lang/String;

    .line 721
    const-string v0, "com.samsung.android.allshare.service.mediashare.ServiceManager.STOP_COMPLETED"

    iput-object v0, p0, Lcom/samsung/android/allshare/ServiceConnector$ProfileConstData;->STOP_MESSAGE:Ljava/lang/String;

    .line 722
    const-string v0, "com.samsung.android.allshare.service.mediashare.ServiceManager"

    iput-object v0, p0, Lcom/samsung/android/allshare/ServiceConnector$ProfileConstData;->SERVICE_MANAGER_NAME_VERSION_1:Ljava/lang/String;

    .line 723
    const-string v0, "com.sec.android.allshare.iface.subscriber"

    iput-object v0, p0, Lcom/samsung/android/allshare/ServiceConnector$ProfileConstData;->SUBSCRIBER_FIELD:Ljava/lang/String;

    .line 724
    const-string v0, "com.samsung.android.allshare.service.mediashare.SUBSCRIBE_SERVICE"

    iput-object v0, p0, Lcom/samsung/android/allshare/ServiceConnector$ProfileConstData;->SUBSCRIPTION_MESSAGE:Ljava/lang/String;

    .line 725
    const-string v0, "com.samsung.android.allshare.service.mediashare.AllShareCpName"

    iput-object v0, p0, Lcom/samsung/android/allshare/ServiceConnector$ProfileConstData;->SET_NAME_MESSAGE:Ljava/lang/String;

    .line 726
    const-string v0, "com.samsung.android.allshare.CpName"

    iput-object v0, p0, Lcom/samsung/android/allshare/ServiceConnector$ProfileConstData;->CP_NAME:Ljava/lang/String;

    .line 742
    :cond_0
    :goto_0
    return-void

    .line 727
    :cond_1
    if-eqz p1, :cond_0

    const-string v0, "com.samsung.android.allshare.service.fileshare"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 729
    iput-wide v2, p0, Lcom/samsung/android/allshare/ServiceConnector$ProfileConstData;->ALLSHARE_FRAMEWORK_VERSION:J

    .line 730
    sget-boolean v0, Lcom/samsung/android/allshare/file/Const;->DEV_MODE:Z

    iput-boolean v0, p0, Lcom/samsung/android/allshare/ServiceConnector$ProfileConstData;->DEV_MODE:Z

    .line 733
    const-string v0, "com.samsung.android.allshare.service.fileshare.ServiceManager.START_SERVICE"

    iput-object v0, p0, Lcom/samsung/android/allshare/ServiceConnector$ProfileConstData;->START_SERVICE:Ljava/lang/String;

    .line 734
    const-string v0, "com.samsung.android.allshare.service.fileshare.ServiceManager.START_COMPLETED"

    iput-object v0, p0, Lcom/samsung/android/allshare/ServiceConnector$ProfileConstData;->START_MESSAGE:Ljava/lang/String;

    .line 735
    const-string v0, "com.samsung.android.allshare.service.fileshare.ServiceManager.STOP_COMPLETED"

    iput-object v0, p0, Lcom/samsung/android/allshare/ServiceConnector$ProfileConstData;->STOP_MESSAGE:Ljava/lang/String;

    .line 736
    const-string v0, "com.samsung.android.allshare.service.fileshare.ServiceManager"

    iput-object v0, p0, Lcom/samsung/android/allshare/ServiceConnector$ProfileConstData;->SERVICE_MANAGER_NAME_VERSION_1:Ljava/lang/String;

    .line 737
    const-string v0, "com.sec.android.allshare.iface.subscriber"

    iput-object v0, p0, Lcom/samsung/android/allshare/ServiceConnector$ProfileConstData;->SUBSCRIBER_FIELD:Ljava/lang/String;

    .line 738
    const-string v0, "com.samsung.android.allshare.service.fileshare.SUBSCRIBE_SERVICE"

    iput-object v0, p0, Lcom/samsung/android/allshare/ServiceConnector$ProfileConstData;->SUBSCRIPTION_MESSAGE:Ljava/lang/String;

    .line 739
    const-string v0, "com.samsung.android.allshare.service.fileshare.AllShareCpName"

    iput-object v0, p0, Lcom/samsung/android/allshare/ServiceConnector$ProfileConstData;->SET_NAME_MESSAGE:Ljava/lang/String;

    .line 740
    const-string v0, "com.samsung.android.allshare.CpName"

    iput-object v0, p0, Lcom/samsung/android/allshare/ServiceConnector$ProfileConstData;->CP_NAME:Ljava/lang/String;

    goto :goto_0
.end method
