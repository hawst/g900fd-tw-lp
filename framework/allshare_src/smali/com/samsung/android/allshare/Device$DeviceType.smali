.class public final enum Lcom/samsung/android/allshare/Device$DeviceType;
.super Ljava/lang/Enum;
.source "Device.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/Device;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DeviceType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/allshare/Device$DeviceType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/allshare/Device$DeviceType;

.field public static final enum DEVICE_AVPLAYER:Lcom/samsung/android/allshare/Device$DeviceType;

.field public static final enum DEVICE_FILERECEIVER:Lcom/samsung/android/allshare/Device$DeviceType;

.field public static final enum DEVICE_IMAGEVIEWER:Lcom/samsung/android/allshare/Device$DeviceType;

.field public static final enum DEVICE_KIES:Lcom/samsung/android/allshare/Device$DeviceType;

.field public static final enum DEVICE_PROVIDER:Lcom/samsung/android/allshare/Device$DeviceType;

.field public static final enum DEVICE_TV_CONTROLLER:Lcom/samsung/android/allshare/Device$DeviceType;

.field public static final enum UNKNOWN:Lcom/samsung/android/allshare/Device$DeviceType;


# instance fields
.field private final enumString:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 106
    new-instance v0, Lcom/samsung/android/allshare/Device$DeviceType;

    const-string v1, "DEVICE_IMAGEVIEWER"

    const-string v2, "DEVICE_IMAGEVIEWER"

    invoke-direct {v0, v1, v4, v2}, Lcom/samsung/android/allshare/Device$DeviceType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_IMAGEVIEWER:Lcom/samsung/android/allshare/Device$DeviceType;

    .line 111
    new-instance v0, Lcom/samsung/android/allshare/Device$DeviceType;

    const-string v1, "DEVICE_AVPLAYER"

    const-string v2, "DEVICE_AVPLAYER"

    invoke-direct {v0, v1, v5, v2}, Lcom/samsung/android/allshare/Device$DeviceType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_AVPLAYER:Lcom/samsung/android/allshare/Device$DeviceType;

    .line 116
    new-instance v0, Lcom/samsung/android/allshare/Device$DeviceType;

    const-string v1, "DEVICE_PROVIDER"

    const-string v2, "DEVICE_PROVIDER"

    invoke-direct {v0, v1, v6, v2}, Lcom/samsung/android/allshare/Device$DeviceType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_PROVIDER:Lcom/samsung/android/allshare/Device$DeviceType;

    .line 121
    new-instance v0, Lcom/samsung/android/allshare/Device$DeviceType;

    const-string v1, "DEVICE_FILERECEIVER"

    const-string v2, "DEVICE_FILERECEIVER"

    invoke-direct {v0, v1, v7, v2}, Lcom/samsung/android/allshare/Device$DeviceType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_FILERECEIVER:Lcom/samsung/android/allshare/Device$DeviceType;

    .line 126
    new-instance v0, Lcom/samsung/android/allshare/Device$DeviceType;

    const-string v1, "DEVICE_TV_CONTROLLER"

    const-string v2, "DEVICE_TV_CONTROLLER"

    invoke-direct {v0, v1, v8, v2}, Lcom/samsung/android/allshare/Device$DeviceType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_TV_CONTROLLER:Lcom/samsung/android/allshare/Device$DeviceType;

    .line 131
    new-instance v0, Lcom/samsung/android/allshare/Device$DeviceType;

    const-string v1, "DEVICE_KIES"

    const/4 v2, 0x5

    const-string v3, "DEVICE_KIES"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/allshare/Device$DeviceType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_KIES:Lcom/samsung/android/allshare/Device$DeviceType;

    .line 133
    new-instance v0, Lcom/samsung/android/allshare/Device$DeviceType;

    const-string v1, "UNKNOWN"

    const/4 v2, 0x6

    const-string v3, "UNKNOWN"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/allshare/Device$DeviceType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/allshare/Device$DeviceType;->UNKNOWN:Lcom/samsung/android/allshare/Device$DeviceType;

    .line 101
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/samsung/android/allshare/Device$DeviceType;

    sget-object v1, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_IMAGEVIEWER:Lcom/samsung/android/allshare/Device$DeviceType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_AVPLAYER:Lcom/samsung/android/allshare/Device$DeviceType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_PROVIDER:Lcom/samsung/android/allshare/Device$DeviceType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_FILERECEIVER:Lcom/samsung/android/allshare/Device$DeviceType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_TV_CONTROLLER:Lcom/samsung/android/allshare/Device$DeviceType;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_KIES:Lcom/samsung/android/allshare/Device$DeviceType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/samsung/android/allshare/Device$DeviceType;->UNKNOWN:Lcom/samsung/android/allshare/Device$DeviceType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/android/allshare/Device$DeviceType;->$VALUES:[Lcom/samsung/android/allshare/Device$DeviceType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "enumStr"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 139
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 140
    iput-object p3, p0, Lcom/samsung/android/allshare/Device$DeviceType;->enumString:Ljava/lang/String;

    .line 141
    return-void
.end method

.method public static stringToEnum(Ljava/lang/String;)Lcom/samsung/android/allshare/Device$DeviceType;
    .locals 1
    .param p0, "enumStr"    # Ljava/lang/String;

    .prologue
    .line 152
    if-nez p0, :cond_0

    .line 153
    sget-object v0, Lcom/samsung/android/allshare/Device$DeviceType;->UNKNOWN:Lcom/samsung/android/allshare/Device$DeviceType;

    .line 170
    :goto_0
    return-object v0

    .line 155
    :cond_0
    const-string v0, "DEVICE_AVPLAYER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 156
    sget-object v0, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_AVPLAYER:Lcom/samsung/android/allshare/Device$DeviceType;

    goto :goto_0

    .line 157
    :cond_1
    const-string v0, "DEVICE_FILERECEIVER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 158
    sget-object v0, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_FILERECEIVER:Lcom/samsung/android/allshare/Device$DeviceType;

    goto :goto_0

    .line 159
    :cond_2
    const-string v0, "DEVICE_IMAGEVIEWER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 160
    sget-object v0, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_IMAGEVIEWER:Lcom/samsung/android/allshare/Device$DeviceType;

    goto :goto_0

    .line 161
    :cond_3
    const-string v0, "DEVICE_KIES"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 162
    sget-object v0, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_KIES:Lcom/samsung/android/allshare/Device$DeviceType;

    goto :goto_0

    .line 163
    :cond_4
    const-string v0, "DEVICE_PROVIDER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 164
    sget-object v0, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_PROVIDER:Lcom/samsung/android/allshare/Device$DeviceType;

    goto :goto_0

    .line 165
    :cond_5
    const-string v0, "DEVICE_TV_CONTROLLER"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 166
    sget-object v0, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_TV_CONTROLLER:Lcom/samsung/android/allshare/Device$DeviceType;

    goto :goto_0

    .line 167
    :cond_6
    const-string v0, "UNKNOWN"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 168
    sget-object v0, Lcom/samsung/android/allshare/Device$DeviceType;->UNKNOWN:Lcom/samsung/android/allshare/Device$DeviceType;

    goto :goto_0

    .line 170
    :cond_7
    sget-object v0, Lcom/samsung/android/allshare/Device$DeviceType;->UNKNOWN:Lcom/samsung/android/allshare/Device$DeviceType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/allshare/Device$DeviceType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 101
    const-class v0, Lcom/samsung/android/allshare/Device$DeviceType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/Device$DeviceType;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/allshare/Device$DeviceType;
    .locals 1

    .prologue
    .line 101
    sget-object v0, Lcom/samsung/android/allshare/Device$DeviceType;->$VALUES:[Lcom/samsung/android/allshare/Device$DeviceType;

    invoke-virtual {v0}, [Lcom/samsung/android/allshare/Device$DeviceType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/allshare/Device$DeviceType;

    return-object v0
.end method


# virtual methods
.method public enumToString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/samsung/android/allshare/Device$DeviceType;->enumString:Ljava/lang/String;

    return-object v0
.end method
