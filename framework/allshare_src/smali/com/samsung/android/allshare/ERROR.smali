.class public final enum Lcom/samsung/android/allshare/ERROR;
.super Ljava/lang/Enum;
.source "ERROR.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/allshare/ERROR;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/allshare/ERROR;

.field public static final enum BAD_RESPONSE:Lcom/samsung/android/allshare/ERROR;

.field public static final enum CONTENT_NOT_AVAILABLE:Lcom/samsung/android/allshare/ERROR;

.field public static final enum DELETED:Lcom/samsung/android/allshare/ERROR;

.field public static final enum FAIL:Lcom/samsung/android/allshare/ERROR;

.field public static final enum FEATURE_NOT_SUPPORTED:Lcom/samsung/android/allshare/ERROR;

.field public static final enum FRAMEWORK_NOT_INSTALLED:Lcom/samsung/android/allshare/ERROR;

.field public static final enum INVALID_ARGUMENT:Lcom/samsung/android/allshare/ERROR;

.field public static final enum INVALID_DEVICE:Lcom/samsung/android/allshare/ERROR;

.field public static final enum INVALID_OBJECT:Lcom/samsung/android/allshare/ERROR;

.field public static final enum INVALID_STATE:Lcom/samsung/android/allshare/ERROR;

.field public static final enum ITEM_NOT_EXIST:Lcom/samsung/android/allshare/ERROR;

.field public static final enum NETWORK_NOT_AVAILABLE:Lcom/samsung/android/allshare/ERROR;

.field public static final enum NOT_SUPPORTED_FRAMEWORK_VERSION:Lcom/samsung/android/allshare/ERROR;

.field public static final enum NO_RESPONSE:Lcom/samsung/android/allshare/ERROR;

.field public static final enum OUT_OF_MEMORY:Lcom/samsung/android/allshare/ERROR;

.field public static final enum PERMISSION_NOT_ALLOWED:Lcom/samsung/android/allshare/ERROR;

.field public static final enum SERVICE_NOT_CONNECTED:Lcom/samsung/android/allshare/ERROR;

.field public static final enum SUCCESS:Lcom/samsung/android/allshare/ERROR;

.field public static final enum TIME_OUT:Lcom/samsung/android/allshare/ERROR;


# instance fields
.field private final enumString:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 30
    new-instance v0, Lcom/samsung/android/allshare/ERROR;

    const-string v1, "SUCCESS"

    const-string v2, "SUCCESS"

    invoke-direct {v0, v1, v4, v2}, Lcom/samsung/android/allshare/ERROR;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    .line 36
    new-instance v0, Lcom/samsung/android/allshare/ERROR;

    const-string v1, "OUT_OF_MEMORY"

    const-string v2, "OUT_OF_MEMORY"

    invoke-direct {v0, v1, v5, v2}, Lcom/samsung/android/allshare/ERROR;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/allshare/ERROR;->OUT_OF_MEMORY:Lcom/samsung/android/allshare/ERROR;

    .line 42
    new-instance v0, Lcom/samsung/android/allshare/ERROR;

    const-string v1, "INVALID_ARGUMENT"

    const-string v2, "INVALID_ARGUMENT"

    invoke-direct {v0, v1, v6, v2}, Lcom/samsung/android/allshare/ERROR;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/samsung/android/allshare/ERROR;

    .line 48
    new-instance v0, Lcom/samsung/android/allshare/ERROR;

    const-string v1, "INVALID_OBJECT"

    const-string v2, "INVALID_OBJECT"

    invoke-direct {v0, v1, v7, v2}, Lcom/samsung/android/allshare/ERROR;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/allshare/ERROR;->INVALID_OBJECT:Lcom/samsung/android/allshare/ERROR;

    .line 53
    new-instance v0, Lcom/samsung/android/allshare/ERROR;

    const-string v1, "INVALID_STATE"

    const-string v2, "INVALID_STATE"

    invoke-direct {v0, v1, v8, v2}, Lcom/samsung/android/allshare/ERROR;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/allshare/ERROR;->INVALID_STATE:Lcom/samsung/android/allshare/ERROR;

    .line 61
    new-instance v0, Lcom/samsung/android/allshare/ERROR;

    const-string v1, "SERVICE_NOT_CONNECTED"

    const/4 v2, 0x5

    const-string v3, "SERVICE_NOT_CONNECTED"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/allshare/ERROR;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/samsung/android/allshare/ERROR;

    .line 67
    new-instance v0, Lcom/samsung/android/allshare/ERROR;

    const-string v1, "NO_RESPONSE"

    const/4 v2, 0x6

    const-string v3, "NO_RESPONSE"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/allshare/ERROR;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/allshare/ERROR;->NO_RESPONSE:Lcom/samsung/android/allshare/ERROR;

    .line 74
    new-instance v0, Lcom/samsung/android/allshare/ERROR;

    const-string v1, "BAD_RESPONSE"

    const/4 v2, 0x7

    const-string v3, "BAD_RESPONSE"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/allshare/ERROR;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/allshare/ERROR;->BAD_RESPONSE:Lcom/samsung/android/allshare/ERROR;

    .line 80
    new-instance v0, Lcom/samsung/android/allshare/ERROR;

    const-string v1, "NETWORK_NOT_AVAILABLE"

    const/16 v2, 0x8

    const-string v3, "NETWORK_NOT_AVAILABLE"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/allshare/ERROR;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/allshare/ERROR;->NETWORK_NOT_AVAILABLE:Lcom/samsung/android/allshare/ERROR;

    .line 85
    new-instance v0, Lcom/samsung/android/allshare/ERROR;

    const-string v1, "CONTENT_NOT_AVAILABLE"

    const/16 v2, 0x9

    const-string v3, "CONTENT_NOT_AVAILABLE"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/allshare/ERROR;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/allshare/ERROR;->CONTENT_NOT_AVAILABLE:Lcom/samsung/android/allshare/ERROR;

    .line 91
    new-instance v0, Lcom/samsung/android/allshare/ERROR;

    const-string v1, "INVALID_DEVICE"

    const/16 v2, 0xa

    const-string v3, "INVALID_DEVICE"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/allshare/ERROR;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/allshare/ERROR;->INVALID_DEVICE:Lcom/samsung/android/allshare/ERROR;

    .line 97
    new-instance v0, Lcom/samsung/android/allshare/ERROR;

    const-string v1, "FEATURE_NOT_SUPPORTED"

    const/16 v2, 0xb

    const-string v3, "FEATURE_NOT_SUPPORTED"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/allshare/ERROR;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/allshare/ERROR;->FEATURE_NOT_SUPPORTED:Lcom/samsung/android/allshare/ERROR;

    .line 102
    new-instance v0, Lcom/samsung/android/allshare/ERROR;

    const-string v1, "PERMISSION_NOT_ALLOWED"

    const/16 v2, 0xc

    const-string v3, "PERMISSION_NOT_ALLOWED"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/allshare/ERROR;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/allshare/ERROR;->PERMISSION_NOT_ALLOWED:Lcom/samsung/android/allshare/ERROR;

    .line 107
    new-instance v0, Lcom/samsung/android/allshare/ERROR;

    const-string v1, "TIME_OUT"

    const/16 v2, 0xd

    const-string v3, "TIME_OUT"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/allshare/ERROR;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/allshare/ERROR;->TIME_OUT:Lcom/samsung/android/allshare/ERROR;

    .line 113
    new-instance v0, Lcom/samsung/android/allshare/ERROR;

    const-string v1, "ITEM_NOT_EXIST"

    const/16 v2, 0xe

    const-string v3, "ITEM_NOT_EXIST"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/allshare/ERROR;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/allshare/ERROR;->ITEM_NOT_EXIST:Lcom/samsung/android/allshare/ERROR;

    .line 119
    new-instance v0, Lcom/samsung/android/allshare/ERROR;

    const-string v1, "DELETED"

    const/16 v2, 0xf

    const-string v3, "DELETED"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/allshare/ERROR;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/allshare/ERROR;->DELETED:Lcom/samsung/android/allshare/ERROR;

    .line 124
    new-instance v0, Lcom/samsung/android/allshare/ERROR;

    const-string v1, "FRAMEWORK_NOT_INSTALLED"

    const/16 v2, 0x10

    const-string v3, "FRAMEWORK_NOT_INSTALLED"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/allshare/ERROR;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/allshare/ERROR;->FRAMEWORK_NOT_INSTALLED:Lcom/samsung/android/allshare/ERROR;

    .line 129
    new-instance v0, Lcom/samsung/android/allshare/ERROR;

    const-string v1, "FAIL"

    const/16 v2, 0x11

    const-string v3, "FAIL"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/allshare/ERROR;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    .line 135
    new-instance v0, Lcom/samsung/android/allshare/ERROR;

    const-string v1, "NOT_SUPPORTED_FRAMEWORK_VERSION"

    const/16 v2, 0x12

    const-string v3, "NOT_SUPPORTED_FRAMEWORK_VERSION"

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/allshare/ERROR;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/samsung/android/allshare/ERROR;->NOT_SUPPORTED_FRAMEWORK_VERSION:Lcom/samsung/android/allshare/ERROR;

    .line 25
    const/16 v0, 0x13

    new-array v0, v0, [Lcom/samsung/android/allshare/ERROR;

    sget-object v1, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/android/allshare/ERROR;->OUT_OF_MEMORY:Lcom/samsung/android/allshare/ERROR;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/samsung/android/allshare/ERROR;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/android/allshare/ERROR;->INVALID_OBJECT:Lcom/samsung/android/allshare/ERROR;

    aput-object v1, v0, v7

    sget-object v1, Lcom/samsung/android/allshare/ERROR;->INVALID_STATE:Lcom/samsung/android/allshare/ERROR;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/samsung/android/allshare/ERROR;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/samsung/android/allshare/ERROR;->NO_RESPONSE:Lcom/samsung/android/allshare/ERROR;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/samsung/android/allshare/ERROR;->BAD_RESPONSE:Lcom/samsung/android/allshare/ERROR;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/samsung/android/allshare/ERROR;->NETWORK_NOT_AVAILABLE:Lcom/samsung/android/allshare/ERROR;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/samsung/android/allshare/ERROR;->CONTENT_NOT_AVAILABLE:Lcom/samsung/android/allshare/ERROR;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/samsung/android/allshare/ERROR;->INVALID_DEVICE:Lcom/samsung/android/allshare/ERROR;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/samsung/android/allshare/ERROR;->FEATURE_NOT_SUPPORTED:Lcom/samsung/android/allshare/ERROR;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/samsung/android/allshare/ERROR;->PERMISSION_NOT_ALLOWED:Lcom/samsung/android/allshare/ERROR;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/samsung/android/allshare/ERROR;->TIME_OUT:Lcom/samsung/android/allshare/ERROR;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/samsung/android/allshare/ERROR;->ITEM_NOT_EXIST:Lcom/samsung/android/allshare/ERROR;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/samsung/android/allshare/ERROR;->DELETED:Lcom/samsung/android/allshare/ERROR;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/samsung/android/allshare/ERROR;->FRAMEWORK_NOT_INSTALLED:Lcom/samsung/android/allshare/ERROR;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/samsung/android/allshare/ERROR;->NOT_SUPPORTED_FRAMEWORK_VERSION:Lcom/samsung/android/allshare/ERROR;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/android/allshare/ERROR;->$VALUES:[Lcom/samsung/android/allshare/ERROR;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "enumStr"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 141
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 142
    iput-object p3, p0, Lcom/samsung/android/allshare/ERROR;->enumString:Ljava/lang/String;

    .line 143
    return-void
.end method

.method public static stringToEnum(Ljava/lang/String;)Lcom/samsung/android/allshare/ERROR;
    .locals 1
    .param p0, "enumStr"    # Ljava/lang/String;

    .prologue
    .line 154
    if-nez p0, :cond_0

    .line 155
    sget-object v0, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    .line 196
    :goto_0
    return-object v0

    .line 157
    :cond_0
    const-string v0, "SUCCESS"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 158
    sget-object v0, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    goto :goto_0

    .line 159
    :cond_1
    const-string v0, "OUT_OF_MEMORY"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 160
    sget-object v0, Lcom/samsung/android/allshare/ERROR;->OUT_OF_MEMORY:Lcom/samsung/android/allshare/ERROR;

    goto :goto_0

    .line 161
    :cond_2
    const-string v0, "INVALID_ARGUMENT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 162
    sget-object v0, Lcom/samsung/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/samsung/android/allshare/ERROR;

    goto :goto_0

    .line 163
    :cond_3
    const-string v0, "BAD_RESPONSE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 164
    sget-object v0, Lcom/samsung/android/allshare/ERROR;->BAD_RESPONSE:Lcom/samsung/android/allshare/ERROR;

    goto :goto_0

    .line 165
    :cond_4
    const-string v0, "CONTENT_NOT_AVAILABLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 166
    sget-object v0, Lcom/samsung/android/allshare/ERROR;->CONTENT_NOT_AVAILABLE:Lcom/samsung/android/allshare/ERROR;

    goto :goto_0

    .line 167
    :cond_5
    const-string v0, "DELETED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 168
    sget-object v0, Lcom/samsung/android/allshare/ERROR;->DELETED:Lcom/samsung/android/allshare/ERROR;

    goto :goto_0

    .line 169
    :cond_6
    const-string v0, "FAIL"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 170
    sget-object v0, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    goto :goto_0

    .line 171
    :cond_7
    const-string v0, "FEATURE_NOT_SUPPORTED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 172
    sget-object v0, Lcom/samsung/android/allshare/ERROR;->FEATURE_NOT_SUPPORTED:Lcom/samsung/android/allshare/ERROR;

    goto :goto_0

    .line 173
    :cond_8
    const-string v0, "FRAMEWORK_NOT_INSTALLED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 174
    sget-object v0, Lcom/samsung/android/allshare/ERROR;->FRAMEWORK_NOT_INSTALLED:Lcom/samsung/android/allshare/ERROR;

    goto :goto_0

    .line 175
    :cond_9
    const-string v0, "INVALID_DEVICE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 176
    sget-object v0, Lcom/samsung/android/allshare/ERROR;->INVALID_DEVICE:Lcom/samsung/android/allshare/ERROR;

    goto :goto_0

    .line 177
    :cond_a
    const-string v0, "INVALID_OBJECT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 178
    sget-object v0, Lcom/samsung/android/allshare/ERROR;->INVALID_OBJECT:Lcom/samsung/android/allshare/ERROR;

    goto :goto_0

    .line 179
    :cond_b
    const-string v0, "INVALID_STATE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 180
    sget-object v0, Lcom/samsung/android/allshare/ERROR;->INVALID_STATE:Lcom/samsung/android/allshare/ERROR;

    goto/16 :goto_0

    .line 181
    :cond_c
    const-string v0, "ITEM_NOT_EXIST"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 182
    sget-object v0, Lcom/samsung/android/allshare/ERROR;->ITEM_NOT_EXIST:Lcom/samsung/android/allshare/ERROR;

    goto/16 :goto_0

    .line 183
    :cond_d
    const-string v0, "NETWORK_NOT_AVAILABLE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 184
    sget-object v0, Lcom/samsung/android/allshare/ERROR;->NETWORK_NOT_AVAILABLE:Lcom/samsung/android/allshare/ERROR;

    goto/16 :goto_0

    .line 185
    :cond_e
    const-string v0, "NO_RESPONSE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 186
    sget-object v0, Lcom/samsung/android/allshare/ERROR;->NO_RESPONSE:Lcom/samsung/android/allshare/ERROR;

    goto/16 :goto_0

    .line 187
    :cond_f
    const-string v0, "NOT_SUPPORTED_FRAMEWORK_VERSION"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 188
    sget-object v0, Lcom/samsung/android/allshare/ERROR;->NOT_SUPPORTED_FRAMEWORK_VERSION:Lcom/samsung/android/allshare/ERROR;

    goto/16 :goto_0

    .line 189
    :cond_10
    const-string v0, "PERMISSION_NOT_ALLOWED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 190
    sget-object v0, Lcom/samsung/android/allshare/ERROR;->PERMISSION_NOT_ALLOWED:Lcom/samsung/android/allshare/ERROR;

    goto/16 :goto_0

    .line 191
    :cond_11
    const-string v0, "SERVICE_NOT_CONNECTED"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 192
    sget-object v0, Lcom/samsung/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/samsung/android/allshare/ERROR;

    goto/16 :goto_0

    .line 193
    :cond_12
    const-string v0, "TIME_OUT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 194
    sget-object v0, Lcom/samsung/android/allshare/ERROR;->TIME_OUT:Lcom/samsung/android/allshare/ERROR;

    goto/16 :goto_0

    .line 196
    :cond_13
    sget-object v0, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    goto/16 :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/allshare/ERROR;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 25
    const-class v0, Lcom/samsung/android/allshare/ERROR;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/ERROR;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/allshare/ERROR;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/samsung/android/allshare/ERROR;->$VALUES:[Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v0}, [Lcom/samsung/android/allshare/ERROR;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/allshare/ERROR;

    return-object v0
.end method


# virtual methods
.method public enumToString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/samsung/android/allshare/ERROR;->enumString:Ljava/lang/String;

    return-object v0
.end method
