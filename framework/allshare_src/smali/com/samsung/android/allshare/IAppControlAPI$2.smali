.class Lcom/samsung/android/allshare/IAppControlAPI$2;
.super Ljava/lang/Object;
.source "IAppControlAPI.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/allshare/IAppControlAPI;->startControl(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/allshare/IAppControlAPI;

.field final synthetic val$inet:Lcom/samsung/android/allshare/NetworkSocketInfo;


# direct methods
.method constructor <init>(Lcom/samsung/android/allshare/IAppControlAPI;Lcom/samsung/android/allshare/NetworkSocketInfo;)V
    .locals 0

    .prologue
    .line 194
    iput-object p1, p0, Lcom/samsung/android/allshare/IAppControlAPI$2;->this$0:Lcom/samsung/android/allshare/IAppControlAPI;

    iput-object p2, p0, Lcom/samsung/android/allshare/IAppControlAPI$2;->val$inet:Lcom/samsung/android/allshare/NetworkSocketInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 197
    iget-object v0, p0, Lcom/samsung/android/allshare/IAppControlAPI$2;->this$0:Lcom/samsung/android/allshare/IAppControlAPI;

    # getter for: Lcom/samsung/android/allshare/IAppControlAPI;->mLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/samsung/android/allshare/IAppControlAPI;->access$000(Lcom/samsung/android/allshare/IAppControlAPI;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 198
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/allshare/IAppControlAPI$2;->this$0:Lcom/samsung/android/allshare/IAppControlAPI;

    iget-object v0, v0, Lcom/samsung/android/allshare/IAppControlAPI;->mTvMsgSender:Lcom/samsung/android/allshare/TVMessageSender;

    if-eqz v0, :cond_0

    .line 199
    iget-object v0, p0, Lcom/samsung/android/allshare/IAppControlAPI$2;->this$0:Lcom/samsung/android/allshare/IAppControlAPI;

    iget-object v0, v0, Lcom/samsung/android/allshare/IAppControlAPI;->mTvMsgSender:Lcom/samsung/android/allshare/TVMessageSender;

    iget-object v2, p0, Lcom/samsung/android/allshare/IAppControlAPI$2;->val$inet:Lcom/samsung/android/allshare/NetworkSocketInfo;

    invoke-virtual {v0, v2}, Lcom/samsung/android/allshare/TVMessageSender;->initSender(Lcom/samsung/android/allshare/NetworkSocketInfo;)V

    .line 200
    iget-object v0, p0, Lcom/samsung/android/allshare/IAppControlAPI$2;->this$0:Lcom/samsung/android/allshare/IAppControlAPI;

    # invokes: Lcom/samsung/android/allshare/IAppControlAPI;->createControlReceiver()V
    invoke-static {v0}, Lcom/samsung/android/allshare/IAppControlAPI;->access$100(Lcom/samsung/android/allshare/IAppControlAPI;)V

    .line 202
    iget-object v0, p0, Lcom/samsung/android/allshare/IAppControlAPI$2;->this$0:Lcom/samsung/android/allshare/IAppControlAPI;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/IAppControlAPI;->sendAuthentication()V

    .line 205
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/IAppControlAPI$2;->this$0:Lcom/samsung/android/allshare/IAppControlAPI;

    const/4 v2, 0x0

    # setter for: Lcom/samsung/android/allshare/IAppControlAPI;->bIsRunningConnection:Z
    invoke-static {v0, v2}, Lcom/samsung/android/allshare/IAppControlAPI;->access$202(Lcom/samsung/android/allshare/IAppControlAPI;Z)Z

    .line 206
    monitor-exit v1

    .line 207
    return-void

    .line 206
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
