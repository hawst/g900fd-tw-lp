.class Lcom/samsung/android/allshare/ServiceConnector$FileServiceProviderImpl;
.super Lcom/samsung/android/allshare/file/FileServiceProvider;
.source "ServiceConnector.java"

# interfaces
.implements Lcom/samsung/android/allshare/ServiceConnector$IServiceConnectorGetter;
.implements Lcom/samsung/android/allshare/ServiceConnector$IServiceStateSetter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/ServiceConnector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "FileServiceProviderImpl"
.end annotation


# instance fields
.field mFileDeviceFinder:Lcom/samsung/android/allshare/FileDeviceFinderImpl;

.field mFileServiceConnector:Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;

.field mFileServiceState:Lcom/samsung/android/allshare/ServiceConnector$ServiceState;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;)V
    .locals 2
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "connector"    # Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;

    .prologue
    const/4 v1, 0x0

    .line 638
    invoke-direct {p0}, Lcom/samsung/android/allshare/file/FileServiceProvider;-><init>()V

    .line 632
    sget-object v0, Lcom/samsung/android/allshare/ServiceConnector$ServiceState;->DISABLED:Lcom/samsung/android/allshare/ServiceConnector$ServiceState;

    iput-object v0, p0, Lcom/samsung/android/allshare/ServiceConnector$FileServiceProviderImpl;->mFileServiceState:Lcom/samsung/android/allshare/ServiceConnector$ServiceState;

    .line 634
    iput-object v1, p0, Lcom/samsung/android/allshare/ServiceConnector$FileServiceProviderImpl;->mFileDeviceFinder:Lcom/samsung/android/allshare/FileDeviceFinderImpl;

    .line 636
    iput-object v1, p0, Lcom/samsung/android/allshare/ServiceConnector$FileServiceProviderImpl;->mFileServiceConnector:Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;

    .line 639
    iput-object p2, p0, Lcom/samsung/android/allshare/ServiceConnector$FileServiceProviderImpl;->mFileServiceConnector:Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;

    .line 640
    new-instance v0, Lcom/samsung/android/allshare/FileDeviceFinderImpl;

    invoke-direct {v0, p2}, Lcom/samsung/android/allshare/FileDeviceFinderImpl;-><init>(Lcom/samsung/android/allshare/IAllShareConnector;)V

    iput-object v0, p0, Lcom/samsung/android/allshare/ServiceConnector$FileServiceProviderImpl;->mFileDeviceFinder:Lcom/samsung/android/allshare/FileDeviceFinderImpl;

    .line 641
    return-void
.end method


# virtual methods
.method public getAllShareConnector()Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;
    .locals 1

    .prologue
    .line 679
    iget-object v0, p0, Lcom/samsung/android/allshare/ServiceConnector$FileServiceProviderImpl;->mFileServiceConnector:Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;

    return-object v0
.end method

.method public bridge synthetic getDeviceFinder()Lcom/samsung/android/allshare/DeviceFinder;
    .locals 1

    .prologue
    .line 629
    invoke-virtual {p0}, Lcom/samsung/android/allshare/ServiceConnector$FileServiceProviderImpl;->getDeviceFinder()Lcom/samsung/android/allshare/FileDeviceFinderImpl;

    move-result-object v0

    return-object v0
.end method

.method public getDeviceFinder()Lcom/samsung/android/allshare/FileDeviceFinderImpl;
    .locals 1

    .prologue
    .line 653
    iget-object v0, p0, Lcom/samsung/android/allshare/ServiceConnector$FileServiceProviderImpl;->mFileDeviceFinder:Lcom/samsung/android/allshare/FileDeviceFinderImpl;

    return-object v0
.end method

.method public bridge synthetic getDeviceFinder()Lcom/samsung/android/allshare/file/FileDeviceFinder;
    .locals 1

    .prologue
    .line 629
    invoke-virtual {p0}, Lcom/samsung/android/allshare/ServiceConnector$FileServiceProviderImpl;->getDeviceFinder()Lcom/samsung/android/allshare/FileDeviceFinderImpl;

    move-result-object v0

    return-object v0
.end method

.method public getDownloader()Lcom/samsung/android/allshare/extension/SECDownloader;
    .locals 1

    .prologue
    .line 646
    const/4 v0, 0x0

    return-object v0
.end method

.method public getServiceState()Lcom/samsung/android/allshare/ServiceConnector$ServiceState;
    .locals 1

    .prologue
    .line 658
    iget-object v0, p0, Lcom/samsung/android/allshare/ServiceConnector$FileServiceProviderImpl;->mFileServiceState:Lcom/samsung/android/allshare/ServiceConnector$ServiceState;

    return-object v0
.end method

.method public getServiceVersion()Ljava/lang/String;
    .locals 2

    .prologue
    .line 663
    iget-object v0, p0, Lcom/samsung/android/allshare/ServiceConnector$FileServiceProviderImpl;->mFileServiceConnector:Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;

    if-nez v0, :cond_0

    .line 664
    const-string v0, "FileServiceProviderImpl"

    const-string v1, "Connection FAIL: AllShare File Service Connector does not exist"

    invoke-static {v0, v1}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 666
    const-string v0, ""

    .line 669
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/ServiceConnector$FileServiceProviderImpl;->mFileServiceConnector:Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;->getServiceVersion()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public setServiceState(Lcom/samsung/android/allshare/ServiceConnector$ServiceState;)V
    .locals 0
    .param p1, "serviceState"    # Lcom/samsung/android/allshare/ServiceConnector$ServiceState;

    .prologue
    .line 674
    iput-object p1, p0, Lcom/samsung/android/allshare/ServiceConnector$FileServiceProviderImpl;->mFileServiceState:Lcom/samsung/android/allshare/ServiceConnector$ServiceState;

    .line 675
    return-void
.end method
