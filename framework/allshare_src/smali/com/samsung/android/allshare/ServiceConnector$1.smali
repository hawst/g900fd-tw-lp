.class final Lcom/samsung/android/allshare/ServiceConnector$1;
.super Ljava/lang/Object;
.source "ServiceConnector.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/allshare/ServiceConnector;->createServiceProvider(Landroid/content/Context;Lcom/samsung/android/allshare/ServiceConnector$IServiceConnectEventListener;)Lcom/samsung/android/allshare/ERROR;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field private mListener:Lcom/samsung/android/allshare/ServiceConnector$IServiceConnectEventListener;

.field private mServiceProvider:Lcom/samsung/android/allshare/ServiceConnector$ServiceProviderImpl;

.field final synthetic val$connector:Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;

.field final synthetic val$ctx:Landroid/content/Context;

.field final synthetic val$l:Lcom/samsung/android/allshare/ServiceConnector$IServiceConnectEventListener;


# direct methods
.method constructor <init>(Lcom/samsung/android/allshare/ServiceConnector$IServiceConnectEventListener;Landroid/content/Context;Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;)V
    .locals 3

    .prologue
    .line 245
    iput-object p1, p0, Lcom/samsung/android/allshare/ServiceConnector$1;->val$l:Lcom/samsung/android/allshare/ServiceConnector$IServiceConnectEventListener;

    iput-object p2, p0, Lcom/samsung/android/allshare/ServiceConnector$1;->val$ctx:Landroid/content/Context;

    iput-object p3, p0, Lcom/samsung/android/allshare/ServiceConnector$1;->val$connector:Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 246
    iget-object v0, p0, Lcom/samsung/android/allshare/ServiceConnector$1;->val$l:Lcom/samsung/android/allshare/ServiceConnector$IServiceConnectEventListener;

    iput-object v0, p0, Lcom/samsung/android/allshare/ServiceConnector$1;->mListener:Lcom/samsung/android/allshare/ServiceConnector$IServiceConnectEventListener;

    .line 248
    new-instance v0, Lcom/samsung/android/allshare/ServiceConnector$ServiceProviderImpl;

    iget-object v1, p0, Lcom/samsung/android/allshare/ServiceConnector$1;->val$ctx:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/allshare/ServiceConnector$1;->val$connector:Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/allshare/ServiceConnector$ServiceProviderImpl;-><init>(Landroid/content/Context;Lcom/samsung/android/allshare/ServiceConnector$AllShareConnector;)V

    iput-object v0, p0, Lcom/samsung/android/allshare/ServiceConnector$1;->mServiceProvider:Lcom/samsung/android/allshare/ServiceConnector$ServiceProviderImpl;

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 253
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    if-eqz v3, :cond_0

    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v3, v3, Lcom/samsung/android/allshare/IAllShareConnector$AllShareServiceState;

    if-nez v3, :cond_1

    .line 254
    :cond_0
    const/4 v3, 0x0

    .line 283
    :goto_0
    return v3

    .line 256
    :cond_1
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Lcom/samsung/android/allshare/IAllShareConnector$AllShareServiceState;

    .line 258
    .local v2, "state":Lcom/samsung/android/allshare/IAllShareConnector$AllShareServiceState;
    sget-object v3, Lcom/samsung/android/allshare/ServiceConnector$3;->$SwitchMap$com$samsung$android$allshare$IAllShareConnector$AllShareServiceState:[I

    invoke-virtual {v2}, Lcom/samsung/android/allshare/IAllShareConnector$AllShareServiceState;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 283
    :goto_1
    const/4 v3, 0x1

    goto :goto_0

    .line 260
    :pswitch_0
    iget-object v3, p0, Lcom/samsung/android/allshare/ServiceConnector$1;->mServiceProvider:Lcom/samsung/android/allshare/ServiceConnector$ServiceProviderImpl;

    sget-object v4, Lcom/samsung/android/allshare/ServiceConnector$ServiceState;->ENABLED:Lcom/samsung/android/allshare/ServiceConnector$ServiceState;

    iput-object v4, v3, Lcom/samsung/android/allshare/ServiceConnector$ServiceProviderImpl;->mServiceState:Lcom/samsung/android/allshare/ServiceConnector$ServiceState;

    .line 262
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/allshare/ServiceConnector$1;->mListener:Lcom/samsung/android/allshare/ServiceConnector$IServiceConnectEventListener;

    iget-object v4, p0, Lcom/samsung/android/allshare/ServiceConnector$1;->mServiceProvider:Lcom/samsung/android/allshare/ServiceConnector$ServiceProviderImpl;

    sget-object v5, Lcom/samsung/android/allshare/ServiceConnector$ServiceState;->ENABLED:Lcom/samsung/android/allshare/ServiceConnector$ServiceState;

    invoke-interface {v3, v4, v5}, Lcom/samsung/android/allshare/ServiceConnector$IServiceConnectEventListener;->onCreated(Lcom/samsung/android/allshare/ServiceProvider;Lcom/samsung/android/allshare/ServiceConnector$ServiceState;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    .line 263
    :catch_0
    move-exception v0

    .line 264
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "ServiceConnector"

    const-string v4, "handleMessage Exception"

    invoke-static {v3, v4, v0}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1

    .line 265
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    .line 266
    .local v0, "e":Ljava/lang/Error;
    const-string v3, "ServiceConnector"

    const-string v4, "handleMessage Error"

    invoke-static {v3, v4, v0}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Error;)V

    goto :goto_1

    .line 271
    .end local v0    # "e":Ljava/lang/Error;
    :pswitch_1
    iget-object v3, p0, Lcom/samsung/android/allshare/ServiceConnector$1;->mServiceProvider:Lcom/samsung/android/allshare/ServiceConnector$ServiceProviderImpl;

    sget-object v4, Lcom/samsung/android/allshare/ServiceConnector$ServiceState;->DISABLED:Lcom/samsung/android/allshare/ServiceConnector$ServiceState;

    iput-object v4, v3, Lcom/samsung/android/allshare/ServiceConnector$ServiceProviderImpl;->mServiceState:Lcom/samsung/android/allshare/ServiceConnector$ServiceState;

    .line 273
    :try_start_1
    iget-object v3, p0, Lcom/samsung/android/allshare/ServiceConnector$1;->mListener:Lcom/samsung/android/allshare/ServiceConnector$IServiceConnectEventListener;

    iget-object v4, p0, Lcom/samsung/android/allshare/ServiceConnector$1;->mServiceProvider:Lcom/samsung/android/allshare/ServiceConnector$ServiceProviderImpl;

    invoke-interface {v3, v4}, Lcom/samsung/android/allshare/ServiceConnector$IServiceConnectEventListener;->onDeleted(Lcom/samsung/android/allshare/ServiceProvider;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Error; {:try_start_1 .. :try_end_1} :catch_3

    goto :goto_1

    .line 274
    :catch_2
    move-exception v0

    .line 275
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "ServiceConnector"

    const-string v4, ""

    invoke-static {v3, v4, v0}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1

    .line 276
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_3
    move-exception v1

    .line 277
    .local v1, "err":Ljava/lang/Error;
    const-string v3, "ServiceConnector"

    const-string v4, ""

    invoke-static {v3, v4, v1}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Error;)V

    goto :goto_1

    .line 258
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
