.class final Lcom/samsung/android/allshare/ItemImpl;
.super Lcom/samsung/android/allshare/Item;
.source "ItemImpl.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/ItemImpl$2;,
        Lcom/samsung/android/allshare/ItemImpl$ResourceImpl;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/android/allshare/ItemImpl;",
            ">;"
        }
    .end annotation
.end field

.field private static final DATETIME_FORMAT:Ljava/lang/String; = "CCYY-MM-DDThh:mm:ss"

.field private static final DATETIME_FORMAT_WITH_MS:Ljava/lang/String; = "CCYY-MM-DDThh:mm:ss.sss"

.field private static final DATETIME_FORMAT_WITH_MS_OFFSET:Ljava/lang/String; = "CCYY-MM-DDThh:mm:ss.sss+hh:mm"

.field private static final DATETIME_FORMAT_WITH_MS_OFFSET_Z:Ljava/lang/String; = "CCYY-MM-DDThh:mm:ss.sssZ"

.field private static final DATETIME_FORMAT_WITH_OFFSET:Ljava/lang/String; = "CCYY-MM-DDThh:mm:ss+hh:mm"

.field private static final DATETIME_FORMAT_WITH_OFFSET_Z:Ljava/lang/String; = "CCYY-MM-DDThh:mm:ssZ"

.field private static final DATETIME_PATTERN:Ljava/lang/String; = "yyyy-MM-dd\'T\'HH:mm:ss"

.field private static final DATETIME_PATTERN_WITH_MS:Ljava/lang/String; = "yyyy-MM-dd\'T\'HH:mm:ss.SSS"

.field private static final DATETIME_PATTERN_WITH_MS_WITH_OFFSET:Ljava/lang/String; = "yyyy-MM-dd\'T\'HH:mm:ss.SSSZZZZZ"

.field private static final DATETIME_PATTERN_WITH_MS_WITH_OFFSET_Z:Ljava/lang/String; = "yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'"

.field private static final DATETIME_PATTERN_WITH_OFFSET:Ljava/lang/String; = "yyyy-MM-dd\'T\'HH:mm:ssZZZZZ"

.field private static final DATETIME_PATTERN_WITH_OFFSET_Z:Ljava/lang/String; = "yyyy-MM-dd\'T\'HH:mm:ss\'Z\'"

.field private static final DATE_FORMAT:Ljava/lang/String; = "CCYY-MM-DD"

.field private static final DATE_PATTERN:Ljava/lang/String; = "yyyy-MM-dd"

.field private static final TAG:Ljava/lang/String; = "ItemImpl"


# instance fields
.field private mBundle:Landroid/os/Bundle;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 208
    new-instance v0, Lcom/samsung/android/allshare/ItemImpl$1;

    invoke-direct {v0}, Lcom/samsung/android/allshare/ItemImpl$1;-><init>()V

    sput-object v0, Lcom/samsung/android/allshare/ItemImpl;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method protected constructor <init>(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/samsung/android/allshare/Item;-><init>()V

    .line 46
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/allshare/ItemImpl;->mBundle:Landroid/os/Bundle;

    .line 49
    iput-object p1, p0, Lcom/samsung/android/allshare/ItemImpl;->mBundle:Landroid/os/Bundle;

    .line 50
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "src"    # Landroid/os/Parcel;

    .prologue
    .line 201
    invoke-direct {p0}, Lcom/samsung/android/allshare/Item;-><init>()V

    .line 46
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/allshare/ItemImpl;->mBundle:Landroid/os/Bundle;

    .line 202
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/ItemImpl;->readFromParcel(Landroid/os/Parcel;)V

    .line 203
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/samsung/android/allshare/ItemImpl$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/samsung/android/allshare/ItemImpl$1;

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/ItemImpl;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private static getFormatter(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "dateStr"    # Ljava/lang/String;

    .prologue
    .line 80
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    .line 81
    .local v0, "dateStrLen":I
    const-string v1, "CCYY-MM-DD"

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 83
    const-string v1, "yyyy-MM-dd"

    .line 105
    :goto_0
    return-object v1

    .line 84
    :cond_0
    const-string v1, "CCYY-MM-DDThh:mm:ss"

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 86
    const-string v1, "yyyy-MM-dd\'T\'HH:mm:ss"

    goto :goto_0

    .line 87
    :cond_1
    const-string v1, "CCYY-MM-DDThh:mm:ss+hh:mm"

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-ne v0, v1, :cond_2

    .line 89
    const-string v1, "yyyy-MM-dd\'T\'HH:mm:ssZZZZZ"

    goto :goto_0

    .line 90
    :cond_2
    const-string v1, "CCYY-MM-DDThh:mm:ssZ"

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-ne v0, v1, :cond_3

    .line 92
    const-string v1, "yyyy-MM-dd\'T\'HH:mm:ss\'Z\'"

    goto :goto_0

    .line 93
    :cond_3
    const-string v1, "CCYY-MM-DDThh:mm:ss.sss"

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-ne v0, v1, :cond_4

    .line 95
    const-string v1, "yyyy-MM-dd\'T\'HH:mm:ss.SSS"

    goto :goto_0

    .line 96
    :cond_4
    const-string v1, "CCYY-MM-DDThh:mm:ss.sss+hh:mm"

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-ne v0, v1, :cond_5

    .line 98
    const-string v1, "yyyy-MM-dd\'T\'HH:mm:ss.SSSZZZZZ"

    goto :goto_0

    .line 99
    :cond_5
    const-string v1, "CCYY-MM-DDThh:mm:ss.sssZ"

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-ne v0, v1, :cond_6

    .line 101
    const-string v1, "yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'"

    goto :goto_0

    .line 105
    :cond_6
    const/4 v1, 0x0

    goto :goto_0
.end method

.method static getItem(Landroid/os/Bundle;)Lcom/samsung/android/allshare/Item;
    .locals 5
    .param p0, "b"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    .line 159
    if-nez p0, :cond_1

    .line 175
    :cond_0
    :goto_0
    return-object v2

    .line 161
    :cond_1
    const-string v3, "BUNDLE_STRING_ITEM_TYPE"

    invoke-virtual {p0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 162
    .local v1, "typeStr":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 165
    invoke-static {v1}, Lcom/samsung/android/allshare/Item$MediaType;->stringToEnum(Ljava/lang/String;)Lcom/samsung/android/allshare/Item$MediaType;

    move-result-object v0

    .line 167
    .local v0, "type":Lcom/samsung/android/allshare/Item$MediaType;
    sget-object v3, Lcom/samsung/android/allshare/ItemImpl$2;->$SwitchMap$com$samsung$android$allshare$Item$MediaType:[I

    invoke-virtual {v0}, Lcom/samsung/android/allshare/Item$MediaType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    goto :goto_0

    .line 169
    :pswitch_0
    new-instance v2, Lcom/samsung/android/allshare/AudioItemImpl;

    invoke-direct {v2, p0}, Lcom/samsung/android/allshare/AudioItemImpl;-><init>(Landroid/os/Bundle;)V

    goto :goto_0

    .line 171
    :pswitch_1
    new-instance v2, Lcom/samsung/android/allshare/ImageItemImpl;

    invoke-direct {v2, p0}, Lcom/samsung/android/allshare/ImageItemImpl;-><init>(Landroid/os/Bundle;)V

    goto :goto_0

    .line 173
    :pswitch_2
    new-instance v2, Lcom/samsung/android/allshare/VideoItemImpl;

    invoke-direct {v2, p0}, Lcom/samsung/android/allshare/VideoItemImpl;-><init>(Landroid/os/Bundle;)V

    goto :goto_0

    .line 167
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "src"    # Landroid/os/Parcel;

    .prologue
    .line 193
    const-class v1, Landroid/os/Bundle;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 194
    .local v0, "bundle":Landroid/os/Bundle;
    iput-object v0, p0, Lcom/samsung/android/allshare/ItemImpl;->mBundle:Landroid/os/Bundle;

    .line 195
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 181
    const/4 v0, 0x0

    return v0
.end method

.method public getAlbumTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 235
    const/4 v0, 0x0

    return-object v0
.end method

.method public getArtist()Ljava/lang/String;
    .locals 1

    .prologue
    .line 240
    const/4 v0, 0x0

    return-object v0
.end method

.method public getBitrate()I
    .locals 1

    .prologue
    .line 352
    const/4 v0, -0x1

    return v0
.end method

.method getBundle()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/samsung/android/allshare/ItemImpl;->mBundle:Landroid/os/Bundle;

    return-object v0
.end method

.method public getChannelNr()Ljava/lang/String;
    .locals 3

    .prologue
    .line 502
    iget-object v1, p0, Lcom/samsung/android/allshare/ItemImpl;->mBundle:Landroid/os/Bundle;

    if-nez v1, :cond_0

    .line 503
    const-string v0, ""

    .line 507
    :goto_0
    return-object v0

    .line 505
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/allshare/ItemImpl;->mBundle:Landroid/os/Bundle;

    const-string v2, "BUNDLE_INT_ITEM_CHANNELNR"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 507
    .local v0, "channelNr":Ljava/lang/String;
    goto :goto_0
.end method

.method public getContentBuildType()Lcom/samsung/android/allshare/Item$ContentBuildType;
    .locals 4

    .prologue
    .line 280
    iget-object v2, p0, Lcom/samsung/android/allshare/ItemImpl;->mBundle:Landroid/os/Bundle;

    if-nez v2, :cond_0

    .line 281
    sget-object v2, Lcom/samsung/android/allshare/Item$ContentBuildType;->UNKNOWN:Lcom/samsung/android/allshare/Item$ContentBuildType;

    .line 302
    :goto_0
    return-object v2

    .line 283
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/allshare/ItemImpl;->mBundle:Landroid/os/Bundle;

    const-string v3, "BUNDLE_STRING_ITEM_CONSTRUCTOR_KEY"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 285
    .local v1, "itemConstructor":Ljava/lang/String;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 286
    :cond_1
    sget-object v2, Lcom/samsung/android/allshare/Item$ContentBuildType;->UNKNOWN:Lcom/samsung/android/allshare/Item$ContentBuildType;

    goto :goto_0

    .line 288
    :cond_2
    invoke-static {v1}, Lcom/samsung/android/allshare/ItemCreator$ConstructorType;->stringToEnum(Ljava/lang/String;)Lcom/samsung/android/allshare/ItemCreator$ConstructorType;

    move-result-object v0

    .line 290
    .local v0, "conType":Lcom/samsung/android/allshare/ItemCreator$ConstructorType;
    sget-object v2, Lcom/samsung/android/allshare/ItemImpl$2;->$SwitchMap$com$samsung$android$allshare$ItemCreator$ConstructorType:[I

    invoke-virtual {v0}, Lcom/samsung/android/allshare/ItemCreator$ConstructorType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 302
    sget-object v2, Lcom/samsung/android/allshare/Item$ContentBuildType;->UNKNOWN:Lcom/samsung/android/allshare/Item$ContentBuildType;

    goto :goto_0

    .line 292
    :pswitch_0
    sget-object v2, Lcom/samsung/android/allshare/Item$ContentBuildType;->LOCAL:Lcom/samsung/android/allshare/Item$ContentBuildType;

    goto :goto_0

    .line 294
    :pswitch_1
    sget-object v2, Lcom/samsung/android/allshare/Item$ContentBuildType;->PROVIDER:Lcom/samsung/android/allshare/Item$ContentBuildType;

    goto :goto_0

    .line 296
    :pswitch_2
    sget-object v2, Lcom/samsung/android/allshare/Item$ContentBuildType;->WEB:Lcom/samsung/android/allshare/Item$ContentBuildType;

    goto :goto_0

    .line 298
    :pswitch_3
    sget-object v2, Lcom/samsung/android/allshare/Item$ContentBuildType;->UNKNOWN:Lcom/samsung/android/allshare/Item$ContentBuildType;

    goto :goto_0

    .line 290
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public getDate()Ljava/util/Date;
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 111
    iget-object v5, p0, Lcom/samsung/android/allshare/ItemImpl;->mBundle:Landroid/os/Bundle;

    if-nez v5, :cond_1

    .line 129
    :cond_0
    :goto_0
    return-object v0

    .line 113
    :cond_1
    iget-object v5, p0, Lcom/samsung/android/allshare/ItemImpl;->mBundle:Landroid/os/Bundle;

    const-string v6, "BUNDLE_STRING_ITEM_DATE"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 115
    .local v1, "dateStr":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    .line 117
    const/4 v0, 0x0

    .line 119
    .local v0, "date":Ljava/util/Date;
    :try_start_0
    invoke-static {v1}, Lcom/samsung/android/allshare/ItemImpl;->getFormatter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 120
    .local v3, "format":Ljava/lang/String;
    if-eqz v3, :cond_0

    .line 122
    new-instance v2, Ljava/text/SimpleDateFormat;

    invoke-direct {v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 123
    .local v2, "dateTime_format":Ljava/text/SimpleDateFormat;
    const-string v5, "UTC"

    invoke-static {v5}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 124
    invoke-virtual {v2, v1}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 126
    .end local v2    # "dateTime_format":Ljava/text/SimpleDateFormat;
    .end local v3    # "format":Ljava/lang/String;
    :catch_0
    move-exception v4

    .line 127
    .local v4, "p1":Ljava/text/ParseException;
    const-string v5, "ItemImpl"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getDate  ParseException: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/android/allshare/ItemImpl;->mBundle:Landroid/os/Bundle;

    const-string v8, "BUNDLE_STRING_ITEM_TITLE"

    invoke-virtual {v7, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v4}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public getDuration()J
    .locals 2

    .prologue
    .line 245
    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public getExtension()Ljava/lang/String;
    .locals 2

    .prologue
    .line 230
    iget-object v0, p0, Lcom/samsung/android/allshare/ItemImpl;->mBundle:Landroid/os/Bundle;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/ItemImpl;->mBundle:Landroid/os/Bundle;

    const-string v1, "BUNDLE_STRING_ITEM_EXTENSION"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getFileSize()J
    .locals 2

    .prologue
    .line 220
    iget-object v0, p0, Lcom/samsung/android/allshare/ItemImpl;->mBundle:Landroid/os/Bundle;

    if-nez v0, :cond_0

    const-wide/16 v0, -0x1

    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/ItemImpl;->mBundle:Landroid/os/Bundle;

    const-string v1, "BUNDLE_LONG_ITEM_FILE_SIZE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    goto :goto_0
.end method

.method public getGenre()Ljava/lang/String;
    .locals 1

    .prologue
    .line 250
    const/4 v0, 0x0

    return-object v0
.end method

.method public getLocation()Landroid/location/Location;
    .locals 1

    .prologue
    .line 255
    const/4 v0, 0x0

    return-object v0
.end method

.method public getMimetype()Ljava/lang/String;
    .locals 2

    .prologue
    .line 225
    iget-object v0, p0, Lcom/samsung/android/allshare/ItemImpl;->mBundle:Landroid/os/Bundle;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/ItemImpl;->mBundle:Landroid/os/Bundle;

    const-string v1, "BUNDLE_STRING_ITEM_MIMETYPE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected final getObjectID()Ljava/lang/String;
    .locals 3

    .prologue
    .line 137
    invoke-virtual {p0}, Lcom/samsung/android/allshare/ItemImpl;->getBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 139
    .local v0, "bundle":Landroid/os/Bundle;
    if-nez v0, :cond_1

    .line 140
    const-string v1, ""

    .line 147
    :cond_0
    :goto_0
    return-object v1

    .line 142
    :cond_1
    const-string v2, "BUNDLE_STRING_OBJECT_ID"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 144
    .local v1, "objID":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 145
    const-string v1, ""

    goto :goto_0
.end method

.method public getResolution()Ljava/lang/String;
    .locals 1

    .prologue
    .line 260
    const/4 v0, 0x0

    return-object v0
.end method

.method public getResourceList()Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/Item$Resource;",
            ">;"
        }
    .end annotation

    .prologue
    .line 357
    iget-object v4, p0, Lcom/samsung/android/allshare/ItemImpl;->mBundle:Landroid/os/Bundle;

    if-nez v4, :cond_1

    .line 358
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 371
    :cond_0
    return-object v3

    .line 360
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/allshare/ItemImpl;->mBundle:Landroid/os/Bundle;

    const-string v5, "BUNDLE_PARCELABLE_ITEM_RESOURCE_LIST"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 362
    .local v2, "resourceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Parcelable;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 364
    .local v3, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Item$Resource;>;"
    if-eqz v2, :cond_0

    .line 367
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Parcelable;

    .line 368
    .local v1, "parcel":Landroid/os/Parcelable;
    new-instance v4, Lcom/samsung/android/allshare/ItemImpl$ResourceImpl;

    check-cast v1, Landroid/os/Bundle;

    .end local v1    # "parcel":Landroid/os/Parcelable;
    invoke-direct {v4, p0, v1}, Lcom/samsung/android/allshare/ItemImpl$ResourceImpl;-><init>(Lcom/samsung/android/allshare/ItemImpl;Landroid/os/Bundle;)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public getSeekMode()Lcom/samsung/android/allshare/Item$SeekMode;
    .locals 1

    .prologue
    .line 347
    sget-object v0, Lcom/samsung/android/allshare/Item$SeekMode;->NONE:Lcom/samsung/android/allshare/Item$SeekMode;

    return-object v0
.end method

.method public getSubtitle()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 265
    const/4 v0, 0x0

    return-object v0
.end method

.method public getSubtitleList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/Subtitle;",
            ">;"
        }
    .end annotation

    .prologue
    .line 341
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    return-object v0
.end method

.method public getThumbnail()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 270
    const/4 v0, 0x0

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 2

    .prologue
    .line 54
    iget-object v0, p0, Lcom/samsung/android/allshare/ItemImpl;->mBundle:Landroid/os/Bundle;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/ItemImpl;->mBundle:Landroid/os/Bundle;

    const-string v1, "BUNDLE_STRING_ITEM_TITLE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getType()Lcom/samsung/android/allshare/Item$MediaType;
    .locals 1

    .prologue
    .line 152
    sget-object v0, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_UNKNOWN:Lcom/samsung/android/allshare/Item$MediaType;

    return-object v0
.end method

.method public getURI()Landroid/net/Uri;
    .locals 2

    .prologue
    .line 59
    iget-object v0, p0, Lcom/samsung/android/allshare/ItemImpl;->mBundle:Landroid/os/Bundle;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    check-cast v0, Landroid/net/Uri;

    check-cast v0, Landroid/net/Uri;

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/ItemImpl;->mBundle:Landroid/os/Bundle;

    const-string v1, "BUNDLE_PARCELABLE_ITEM_URI"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    goto :goto_0
.end method

.method public getWebContentDeliveryMode()Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;
    .locals 4

    .prologue
    .line 307
    iget-object v2, p0, Lcom/samsung/android/allshare/ItemImpl;->mBundle:Landroid/os/Bundle;

    if-nez v2, :cond_0

    .line 308
    sget-object v0, Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;->UNKNOWN:Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

    .line 319
    :goto_0
    return-object v0

    .line 310
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/allshare/ItemImpl;->mBundle:Landroid/os/Bundle;

    const-string v3, "BUNDLE_STRING_WEB_PLAY_MODE"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 312
    .local v1, "deliveryModeStr":Ljava/lang/String;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 313
    :cond_1
    const-string v2, "ItemImpl"

    const-string v3, " getWebContentDeliveryMode() : deliveryModeStr is null or empty! "

    invoke-static {v2, v3}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 314
    sget-object v0, Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;->UNKNOWN:Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

    goto :goto_0

    .line 317
    :cond_2
    invoke-static {v1}, Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;->stringToEnum(Ljava/lang/String;)Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;

    move-result-object v0

    .line 319
    .local v0, "deliveryMode":Lcom/samsung/android/allshare/Item$WebContentBuilder$DeliveryMode;
    goto :goto_0
.end method

.method public getWebContentPlayMode()Lcom/samsung/android/allshare/Item$WebContentBuilder$PlayMode;
    .locals 4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 326
    iget-object v2, p0, Lcom/samsung/android/allshare/ItemImpl;->mBundle:Landroid/os/Bundle;

    if-nez v2, :cond_0

    .line 327
    sget-object v0, Lcom/samsung/android/allshare/Item$WebContentBuilder$PlayMode;->UNKNOWN:Lcom/samsung/android/allshare/Item$WebContentBuilder$PlayMode;

    .line 336
    :goto_0
    return-object v0

    .line 329
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/allshare/ItemImpl;->mBundle:Landroid/os/Bundle;

    const-string v3, "BUNDLE_STRING_WEB_PLAY_MODE"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 331
    .local v1, "playModeStr":Ljava/lang/String;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 332
    :cond_1
    sget-object v0, Lcom/samsung/android/allshare/Item$WebContentBuilder$PlayMode;->UNKNOWN:Lcom/samsung/android/allshare/Item$WebContentBuilder$PlayMode;

    goto :goto_0

    .line 334
    :cond_2
    invoke-static {v1}, Lcom/samsung/android/allshare/Item$WebContentBuilder$PlayMode;->stringToEnum(Ljava/lang/String;)Lcom/samsung/android/allshare/Item$WebContentBuilder$PlayMode;

    move-result-object v0

    .line 336
    .local v0, "playMode":Lcom/samsung/android/allshare/Item$WebContentBuilder$PlayMode;
    goto :goto_0
.end method

.method public isRootFolder()Z
    .locals 1

    .prologue
    .line 275
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 186
    invoke-virtual {p0}, Lcom/samsung/android/allshare/ItemImpl;->getBundle()Landroid/os/Bundle;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 187
    return-void
.end method
