.class final Lcom/samsung/android/allshare/SlideShowPlayerImpl;
.super Lcom/samsung/android/allshare/media/SlideShowPlayer;
.source "SlideShowPlayerImpl.java"

# interfaces
.implements Lcom/sec/android/allshare/iface/IBundleHolder;


# static fields
.field private static final TAG:Ljava/lang/String; = "SlideShowPlayerImpl"


# instance fields
.field private mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

.field private mAllShareEventHandler:Lcom/samsung/android/allshare/AllShareEventHandler;

.field private mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

.field private mEventListener:Lcom/samsung/android/allshare/media/SlideShowPlayer$ISlideShowPlayerEventListener;

.field private mIsSubscribed:Z

.field mResponseHandler:Lcom/samsung/android/allshare/AllShareResponseHandler;

.field private mResponseListener:Lcom/samsung/android/allshare/media/SlideShowPlayer$ISlideShowPlayerResponseListener;


# direct methods
.method constructor <init>(Lcom/samsung/android/allshare/IAllShareConnector;Lcom/samsung/android/allshare/DeviceImpl;)V
    .locals 2
    .param p1, "connector"    # Lcom/samsung/android/allshare/IAllShareConnector;
    .param p2, "deviceImpl"    # Lcom/samsung/android/allshare/DeviceImpl;

    .prologue
    const/4 v0, 0x0

    .line 41
    invoke-direct {p0}, Lcom/samsung/android/allshare/media/SlideShowPlayer;-><init>()V

    .line 31
    iput-object v0, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    .line 33
    iput-object v0, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mEventListener:Lcom/samsung/android/allshare/media/SlideShowPlayer$ISlideShowPlayerEventListener;

    .line 35
    iput-object v0, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mResponseListener:Lcom/samsung/android/allshare/media/SlideShowPlayer$ISlideShowPlayerResponseListener;

    .line 37
    iput-object v0, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    .line 39
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mIsSubscribed:Z

    .line 68
    new-instance v0, Lcom/samsung/android/allshare/SlideShowPlayerImpl$1;

    invoke-static {}, Lcom/samsung/android/allshare/ServiceConnector;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/allshare/SlideShowPlayerImpl$1;-><init>(Lcom/samsung/android/allshare/SlideShowPlayerImpl;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mResponseHandler:Lcom/samsung/android/allshare/AllShareResponseHandler;

    .line 143
    new-instance v0, Lcom/samsung/android/allshare/SlideShowPlayerImpl$2;

    invoke-static {}, Lcom/samsung/android/allshare/ServiceConnector;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/allshare/SlideShowPlayerImpl$2;-><init>(Lcom/samsung/android/allshare/SlideShowPlayerImpl;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mAllShareEventHandler:Lcom/samsung/android/allshare/AllShareEventHandler;

    .line 42
    if-nez p1, :cond_0

    .line 44
    const-string v0, "SlideShowPlayerImpl"

    const-string v1, "Connection FAIL: AllShare Service Connector does not exist"

    invoke-static {v0, v1}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    :goto_0
    return-void

    .line 48
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    .line 50
    iput-object p2, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/samsung/android/allshare/SlideShowPlayerImpl;)Lcom/samsung/android/allshare/media/SlideShowPlayer$ISlideShowPlayerResponseListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/SlideShowPlayerImpl;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mResponseListener:Lcom/samsung/android/allshare/media/SlideShowPlayer$ISlideShowPlayerResponseListener;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/allshare/SlideShowPlayerImpl;)Lcom/samsung/android/allshare/media/SlideShowPlayer$ISlideShowPlayerEventListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/SlideShowPlayerImpl;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mEventListener:Lcom/samsung/android/allshare/media/SlideShowPlayer$ISlideShowPlayerEventListener;

    return-object v0
.end method


# virtual methods
.method public getBundle()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    if-nez v0, :cond_0

    .line 63
    const/4 v0, 0x0

    .line 65
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/DeviceImpl;->getBundle()Landroid/os/Bundle;

    move-result-object v0

    goto :goto_0
.end method

.method public getID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    if-nez v0, :cond_0

    .line 55
    const-string v0, ""

    .line 57
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/DeviceImpl;->getID()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public removeEventHandler()V
    .locals 4

    .prologue
    .line 352
    iget-object v0, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    const-string v1, "com.sec.android.allshare.event.EVENT_DEVICE_SUBSCRIBE"

    iget-object v2, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/DeviceImpl;->getBundle()Landroid/os/Bundle;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mAllShareEventHandler:Lcom/samsung/android/allshare/AllShareEventHandler;

    invoke-interface {v0, v1, v2, v3}, Lcom/samsung/android/allshare/IAllShareConnector;->unsubscribeAllShareEvent(Ljava/lang/String;Landroid/os/Bundle;Lcom/samsung/android/allshare/AllShareEventHandler;)V

    .line 354
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mIsSubscribed:Z

    .line 355
    return-void
.end method

.method public setBGMList(Ljava/util/ArrayList;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/Item;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 272
    .local p1, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Item;>;"
    iget-object v6, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v6}, Lcom/samsung/android/allshare/IAllShareConnector;->isAllShareServiceConnected()Z

    move-result v6

    if-nez v6, :cond_2

    .line 273
    :cond_0
    iget-object v6, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mResponseListener:Lcom/samsung/android/allshare/media/SlideShowPlayer$ISlideShowPlayerResponseListener;

    if-eqz v6, :cond_1

    .line 274
    iget-object v6, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mResponseListener:Lcom/samsung/android/allshare/media/SlideShowPlayer$ISlideShowPlayerResponseListener;

    sget-object v7, Lcom/samsung/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/samsung/android/allshare/ERROR;

    invoke-interface {v6, p1, v7}, Lcom/samsung/android/allshare/media/SlideShowPlayer$ISlideShowPlayerResponseListener;->onSetBGMListResponseReceived(Ljava/util/ArrayList;Lcom/samsung/android/allshare/ERROR;)V

    .line 307
    :cond_1
    :goto_0
    return-void

    .line 278
    :cond_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 279
    .local v0, "bundleList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    const-string v3, ""

    .line 281
    .local v3, "mimeType":Ljava/lang/String;
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/allshare/Item;

    .line 282
    .local v2, "item":Lcom/samsung/android/allshare/Item;
    if-eqz v2, :cond_3

    .line 283
    invoke-virtual {v2}, Lcom/samsung/android/allshare/Item;->getMimetype()Ljava/lang/String;

    move-result-object v3

    .line 285
    const-string v6, "audio"

    invoke-virtual {v3, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 286
    iget-object v6, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mResponseListener:Lcom/samsung/android/allshare/media/SlideShowPlayer$ISlideShowPlayerResponseListener;

    if-eqz v6, :cond_1

    .line 287
    iget-object v6, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mResponseListener:Lcom/samsung/android/allshare/media/SlideShowPlayer$ISlideShowPlayerResponseListener;

    sget-object v7, Lcom/samsung/android/allshare/ERROR;->INVALID_OBJECT:Lcom/samsung/android/allshare/ERROR;

    invoke-interface {v6, p1, v7}, Lcom/samsung/android/allshare/media/SlideShowPlayer$ISlideShowPlayerResponseListener;->onSetBGMListResponseReceived(Ljava/util/ArrayList;Lcom/samsung/android/allshare/ERROR;)V

    goto :goto_0

    .line 291
    :cond_4
    instance-of v6, v2, Lcom/sec/android/allshare/iface/IBundleHolder;

    if-eqz v6, :cond_3

    .line 292
    check-cast v2, Lcom/sec/android/allshare/iface/IBundleHolder;

    .end local v2    # "item":Lcom/samsung/android/allshare/Item;
    invoke-interface {v2}, Lcom/sec/android/allshare/iface/IBundleHolder;->getBundle()Landroid/os/Bundle;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 297
    :cond_5
    new-instance v5, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v5}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 298
    .local v5, "req_msg":Lcom/sec/android/allshare/iface/CVMessage;
    const-string v6, "com.sec.android.allshare.action.ACTION_SLIDESHOW_PLAYER_SETBGMLIST"

    invoke-virtual {v5, v6}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 300
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 301
    .local v4, "req_bundle":Landroid/os/Bundle;
    const-string v6, "BUNDLE_STRING_ID"

    invoke-virtual {p0}, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->getID()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 302
    const-string v6, "BUNDLE_PARCELABLE_ARRAYLIST_SLIDESHOW_AUDIO_CONTENT_URI"

    invoke-virtual {v4, v6, v0}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 304
    invoke-virtual {v5, v4}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 306
    iget-object v6, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    iget-object v7, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mResponseHandler:Lcom/samsung/android/allshare/AllShareResponseHandler;

    invoke-interface {v6, v5, v7}, Lcom/samsung/android/allshare/IAllShareConnector;->requestCVMAsync(Lcom/sec/android/allshare/iface/CVMessage;Lcom/samsung/android/allshare/AllShareResponseHandler;)J

    goto :goto_0
.end method

.method public setBGMVolume(I)V
    .locals 4
    .param p1, "volume"    # I

    .prologue
    .line 311
    iget-object v2, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v2}, Lcom/samsung/android/allshare/IAllShareConnector;->isAllShareServiceConnected()Z

    move-result v2

    if-nez v2, :cond_2

    .line 312
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mResponseListener:Lcom/samsung/android/allshare/media/SlideShowPlayer$ISlideShowPlayerResponseListener;

    if-eqz v2, :cond_1

    .line 313
    iget-object v2, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mResponseListener:Lcom/samsung/android/allshare/media/SlideShowPlayer$ISlideShowPlayerResponseListener;

    sget-object v3, Lcom/samsung/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/samsung/android/allshare/ERROR;

    invoke-interface {v2, p1, v3}, Lcom/samsung/android/allshare/media/SlideShowPlayer$ISlideShowPlayerResponseListener;->onSetBGMVolumeResponseReceived(ILcom/samsung/android/allshare/ERROR;)V

    .line 327
    :cond_1
    :goto_0
    return-void

    .line 318
    :cond_2
    new-instance v1, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v1}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 319
    .local v1, "req_msg":Lcom/sec/android/allshare/iface/CVMessage;
    const-string v2, "com.sec.android.allshare.action.ACTION_SLIDESHOW_PLAYER_SETBGMVOLUME"

    invoke-virtual {v1, v2}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 321
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 322
    .local v0, "req_bundle":Landroid/os/Bundle;
    const-string v2, "BUNDLE_STRING_ID"

    invoke-virtual {p0}, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    const-string v2, "BUNDLE_INT_SLIDESHOW_BGM_VOLUME"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 324
    invoke-virtual {v1, v0}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 326
    iget-object v2, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    iget-object v3, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mResponseHandler:Lcom/samsung/android/allshare/AllShareResponseHandler;

    invoke-interface {v2, v1, v3}, Lcom/samsung/android/allshare/IAllShareConnector;->requestCVMAsync(Lcom/sec/android/allshare/iface/CVMessage;Lcom/samsung/android/allshare/AllShareResponseHandler;)J

    goto :goto_0
.end method

.method public setEventListener(Lcom/samsung/android/allshare/media/SlideShowPlayer$ISlideShowPlayerEventListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/allshare/media/SlideShowPlayer$ISlideShowPlayerEventListener;

    .prologue
    .line 331
    iget-object v0, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v0}, Lcom/samsung/android/allshare/IAllShareConnector;->isAllShareServiceConnected()Z

    move-result v0

    if-nez v0, :cond_2

    .line 332
    :cond_0
    const-string v0, "SlideShowPlayerImpl"

    const-string v1, "setEventListener error! AllShareService is not connected"

    invoke-static {v0, v1}, Lcom/samsung/android/allshare/DLog;->w_api(Ljava/lang/String;Ljava/lang/String;)V

    .line 349
    :cond_1
    :goto_0
    return-void

    .line 336
    :cond_2
    iput-object p1, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mEventListener:Lcom/samsung/android/allshare/media/SlideShowPlayer$ISlideShowPlayerEventListener;

    .line 338
    iget-boolean v0, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mIsSubscribed:Z

    if-nez v0, :cond_3

    if-eqz p1, :cond_3

    .line 339
    iget-object v0, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    const-string v1, "com.sec.android.allshare.event.EVENT_DEVICE_SUBSCRIBE"

    iget-object v2, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/DeviceImpl;->getBundle()Landroid/os/Bundle;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mAllShareEventHandler:Lcom/samsung/android/allshare/AllShareEventHandler;

    invoke-interface {v0, v1, v2, v3}, Lcom/samsung/android/allshare/IAllShareConnector;->subscribeAllShareEvent(Ljava/lang/String;Landroid/os/Bundle;Lcom/samsung/android/allshare/AllShareEventHandler;)Z

    .line 341
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mIsSubscribed:Z

    goto :goto_0

    .line 342
    :cond_3
    iget-boolean v0, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mIsSubscribed:Z

    if-eqz v0, :cond_1

    if-nez p1, :cond_1

    .line 343
    iget-object v0, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    const-string v1, "com.sec.android.allshare.event.EVENT_DEVICE_SUBSCRIBE"

    iget-object v2, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mDeviceImpl:Lcom/samsung/android/allshare/DeviceImpl;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/DeviceImpl;->getBundle()Landroid/os/Bundle;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mAllShareEventHandler:Lcom/samsung/android/allshare/AllShareEventHandler;

    invoke-interface {v0, v1, v2, v3}, Lcom/samsung/android/allshare/IAllShareConnector;->unsubscribeAllShareEvent(Ljava/lang/String;Landroid/os/Bundle;Lcom/samsung/android/allshare/AllShareEventHandler;)V

    .line 345
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mIsSubscribed:Z

    goto :goto_0
.end method

.method public setList(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 8
    .param p1, "albumTitle"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/Item;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 230
    .local p2, "imageItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Item;>;"
    iget-object v6, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v6}, Lcom/samsung/android/allshare/IAllShareConnector;->isAllShareServiceConnected()Z

    move-result v6

    if-nez v6, :cond_2

    .line 231
    :cond_0
    iget-object v6, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mResponseListener:Lcom/samsung/android/allshare/media/SlideShowPlayer$ISlideShowPlayerResponseListener;

    if-eqz v6, :cond_1

    .line 232
    iget-object v6, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mResponseListener:Lcom/samsung/android/allshare/media/SlideShowPlayer$ISlideShowPlayerResponseListener;

    sget-object v7, Lcom/samsung/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/samsung/android/allshare/ERROR;

    invoke-interface {v6, p1, p2, v7}, Lcom/samsung/android/allshare/media/SlideShowPlayer$ISlideShowPlayerResponseListener;->onSetListResponseReceived(Ljava/lang/String;Ljava/util/ArrayList;Lcom/samsung/android/allshare/ERROR;)V

    .line 268
    :cond_1
    :goto_0
    return-void

    .line 237
    :cond_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 238
    .local v0, "bundleList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    const-string v3, ""

    .line 240
    .local v3, "mimeType":Ljava/lang/String;
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/allshare/Item;

    .line 241
    .local v2, "item":Lcom/samsung/android/allshare/Item;
    if-eqz v2, :cond_3

    .line 242
    invoke-virtual {v2}, Lcom/samsung/android/allshare/Item;->getMimetype()Ljava/lang/String;

    move-result-object v3

    .line 244
    const-string v6, "image"

    invoke-virtual {v3, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 245
    iget-object v6, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mResponseListener:Lcom/samsung/android/allshare/media/SlideShowPlayer$ISlideShowPlayerResponseListener;

    if-eqz v6, :cond_1

    .line 246
    iget-object v6, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mResponseListener:Lcom/samsung/android/allshare/media/SlideShowPlayer$ISlideShowPlayerResponseListener;

    sget-object v7, Lcom/samsung/android/allshare/ERROR;->INVALID_OBJECT:Lcom/samsung/android/allshare/ERROR;

    invoke-interface {v6, p1, p2, v7}, Lcom/samsung/android/allshare/media/SlideShowPlayer$ISlideShowPlayerResponseListener;->onSetListResponseReceived(Ljava/lang/String;Ljava/util/ArrayList;Lcom/samsung/android/allshare/ERROR;)V

    goto :goto_0

    .line 251
    :cond_4
    instance-of v6, v2, Lcom/sec/android/allshare/iface/IBundleHolder;

    if-eqz v6, :cond_3

    .line 252
    check-cast v2, Lcom/sec/android/allshare/iface/IBundleHolder;

    .end local v2    # "item":Lcom/samsung/android/allshare/Item;
    invoke-interface {v2}, Lcom/sec/android/allshare/iface/IBundleHolder;->getBundle()Landroid/os/Bundle;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 257
    :cond_5
    new-instance v5, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v5}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 258
    .local v5, "req_msg":Lcom/sec/android/allshare/iface/CVMessage;
    const-string v6, "com.sec.android.allshare.action.ACTION_SLIDESHOW_PLAYER_SETLIST"

    invoke-virtual {v5, v6}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 260
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 261
    .local v4, "req_bundle":Landroid/os/Bundle;
    const-string v6, "BUNDLE_STRING_ID"

    invoke-virtual {p0}, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->getID()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    const-string v6, "BUNDLE_STRING_SLIDESHOW_ALBUM_TITLE"

    invoke-virtual {v4, v6, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    const-string v6, "BUNDLE_PARCELABLE_ARRAYLIST_SLIDESHOW_IMAGE_CONTENT_URI"

    invoke-virtual {v4, v6, v0}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 265
    invoke-virtual {v5, v4}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 267
    iget-object v6, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    iget-object v7, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mResponseHandler:Lcom/samsung/android/allshare/AllShareResponseHandler;

    invoke-interface {v6, v5, v7}, Lcom/samsung/android/allshare/IAllShareConnector;->requestCVMAsync(Lcom/sec/android/allshare/iface/CVMessage;Lcom/samsung/android/allshare/AllShareResponseHandler;)J

    goto :goto_0
.end method

.method public setResponseListener(Lcom/samsung/android/allshare/media/SlideShowPlayer$ISlideShowPlayerResponseListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/allshare/media/SlideShowPlayer$ISlideShowPlayerResponseListener;

    .prologue
    .line 359
    iput-object p1, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mResponseListener:Lcom/samsung/android/allshare/media/SlideShowPlayer$ISlideShowPlayerResponseListener;

    .line 360
    return-void
.end method

.method public start(I)V
    .locals 4
    .param p1, "interval"    # I

    .prologue
    .line 193
    iget-object v2, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v2}, Lcom/samsung/android/allshare/IAllShareConnector;->isAllShareServiceConnected()Z

    move-result v2

    if-nez v2, :cond_2

    .line 194
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mResponseListener:Lcom/samsung/android/allshare/media/SlideShowPlayer$ISlideShowPlayerResponseListener;

    if-eqz v2, :cond_1

    .line 195
    iget-object v2, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mResponseListener:Lcom/samsung/android/allshare/media/SlideShowPlayer$ISlideShowPlayerResponseListener;

    sget-object v3, Lcom/samsung/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/samsung/android/allshare/ERROR;

    invoke-interface {v2, p1, v3}, Lcom/samsung/android/allshare/media/SlideShowPlayer$ISlideShowPlayerResponseListener;->onStartResponseReceived(ILcom/samsung/android/allshare/ERROR;)V

    .line 208
    :cond_1
    :goto_0
    return-void

    .line 199
    :cond_2
    new-instance v1, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v1}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 200
    .local v1, "req_msg":Lcom/sec/android/allshare/iface/CVMessage;
    const-string v2, "com.sec.android.allshare.action.ACTION_SLIDESHOW_PLAYER_START"

    invoke-virtual {v1, v2}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 202
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 203
    .local v0, "req_bundle":Landroid/os/Bundle;
    const-string v2, "BUNDLE_STRING_ID"

    invoke-virtual {p0}, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    const-string v2, "BUNDLE_INT_SLIDESHOW_INTERVAL"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 205
    invoke-virtual {v1, v0}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 207
    iget-object v2, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    iget-object v3, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mResponseHandler:Lcom/samsung/android/allshare/AllShareResponseHandler;

    invoke-interface {v2, v1, v3}, Lcom/samsung/android/allshare/IAllShareConnector;->requestCVMAsync(Lcom/sec/android/allshare/iface/CVMessage;Lcom/samsung/android/allshare/AllShareResponseHandler;)J

    goto :goto_0
.end method

.method public stop()V
    .locals 4

    .prologue
    .line 212
    iget-object v2, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    invoke-interface {v2}, Lcom/samsung/android/allshare/IAllShareConnector;->isAllShareServiceConnected()Z

    move-result v2

    if-nez v2, :cond_2

    .line 213
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mResponseListener:Lcom/samsung/android/allshare/media/SlideShowPlayer$ISlideShowPlayerResponseListener;

    if-eqz v2, :cond_1

    .line 214
    iget-object v2, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mResponseListener:Lcom/samsung/android/allshare/media/SlideShowPlayer$ISlideShowPlayerResponseListener;

    sget-object v3, Lcom/samsung/android/allshare/ERROR;->SERVICE_NOT_CONNECTED:Lcom/samsung/android/allshare/ERROR;

    invoke-interface {v2, v3}, Lcom/samsung/android/allshare/media/SlideShowPlayer$ISlideShowPlayerResponseListener;->onStopResponseReceived(Lcom/samsung/android/allshare/ERROR;)V

    .line 226
    :cond_1
    :goto_0
    return-void

    .line 218
    :cond_2
    new-instance v1, Lcom/sec/android/allshare/iface/CVMessage;

    invoke-direct {v1}, Lcom/sec/android/allshare/iface/CVMessage;-><init>()V

    .line 219
    .local v1, "req_msg":Lcom/sec/android/allshare/iface/CVMessage;
    const-string v2, "com.sec.android.allshare.action.ACTION_SLIDESHOW_PLAYER_STOP"

    invoke-virtual {v1, v2}, Lcom/sec/android/allshare/iface/CVMessage;->setActionID(Ljava/lang/String;)V

    .line 221
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 222
    .local v0, "req_bundle":Landroid/os/Bundle;
    const-string v2, "BUNDLE_STRING_ID"

    invoke-virtual {p0}, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    invoke-virtual {v1, v0}, Lcom/sec/android/allshare/iface/CVMessage;->setBundle(Landroid/os/Bundle;)V

    .line 225
    iget-object v2, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mAllShareConnector:Lcom/samsung/android/allshare/IAllShareConnector;

    iget-object v3, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mResponseHandler:Lcom/samsung/android/allshare/AllShareResponseHandler;

    invoke-interface {v2, v1, v3}, Lcom/samsung/android/allshare/IAllShareConnector;->requestCVMAsync(Lcom/sec/android/allshare/iface/CVMessage;Lcom/samsung/android/allshare/AllShareResponseHandler;)J

    goto :goto_0
.end method
