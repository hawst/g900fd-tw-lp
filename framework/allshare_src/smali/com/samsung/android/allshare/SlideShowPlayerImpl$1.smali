.class Lcom/samsung/android/allshare/SlideShowPlayerImpl$1;
.super Lcom/samsung/android/allshare/AllShareResponseHandler;
.source "SlideShowPlayerImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/SlideShowPlayerImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/allshare/SlideShowPlayerImpl;


# direct methods
.method constructor <init>(Lcom/samsung/android/allshare/SlideShowPlayerImpl;Landroid/os/Looper;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Looper;

    .prologue
    .line 69
    iput-object p1, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl$1;->this$0:Lcom/samsung/android/allshare/SlideShowPlayerImpl;

    invoke-direct {p0, p2}, Lcom/samsung/android/allshare/AllShareResponseHandler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleResponseMessage(Lcom/sec/android/allshare/iface/CVMessage;)V
    .locals 14
    .param p1, "cvm"    # Lcom/sec/android/allshare/iface/CVMessage;

    .prologue
    .line 73
    invoke-virtual {p1}, Lcom/sec/android/allshare/iface/CVMessage;->getActionID()Ljava/lang/String;

    move-result-object v0

    .line 74
    .local v0, "actionID":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/sec/android/allshare/iface/CVMessage;->getBundle()Landroid/os/Bundle;

    move-result-object v11

    .line 76
    .local v11, "resBundle":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    if-nez v11, :cond_1

    .line 139
    :cond_0
    :goto_0
    return-void

    .line 79
    :cond_1
    sget-object v3, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    .line 80
    .local v3, "error":Lcom/samsung/android/allshare/ERROR;
    const-string v13, "BUNDLE_ENUM_ERROR"

    invoke-virtual {v11, v13}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 81
    .local v4, "errorStr":Ljava/lang/String;
    if-eqz v4, :cond_2

    .line 82
    invoke-static {v4}, Lcom/samsung/android/allshare/ERROR;->stringToEnum(Ljava/lang/String;)Lcom/samsung/android/allshare/ERROR;

    move-result-object v3

    .line 84
    :cond_2
    const-string v13, "com.sec.android.allshare.action.ACTION_SLIDESHOW_PLAYER_START"

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_3

    .line 86
    const-string v13, "BUNDLE_INT_SLIDESHOW_BGM_VOLUME"

    invoke-virtual {v11, v13}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    .line 88
    .local v6, "interval":I
    iget-object v13, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl$1;->this$0:Lcom/samsung/android/allshare/SlideShowPlayerImpl;

    # getter for: Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mResponseListener:Lcom/samsung/android/allshare/media/SlideShowPlayer$ISlideShowPlayerResponseListener;
    invoke-static {v13}, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->access$000(Lcom/samsung/android/allshare/SlideShowPlayerImpl;)Lcom/samsung/android/allshare/media/SlideShowPlayer$ISlideShowPlayerResponseListener;

    move-result-object v13

    if-eqz v13, :cond_0

    .line 89
    iget-object v13, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl$1;->this$0:Lcom/samsung/android/allshare/SlideShowPlayerImpl;

    # getter for: Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mResponseListener:Lcom/samsung/android/allshare/media/SlideShowPlayer$ISlideShowPlayerResponseListener;
    invoke-static {v13}, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->access$000(Lcom/samsung/android/allshare/SlideShowPlayerImpl;)Lcom/samsung/android/allshare/media/SlideShowPlayer$ISlideShowPlayerResponseListener;

    move-result-object v13

    invoke-interface {v13, v6, v3}, Lcom/samsung/android/allshare/media/SlideShowPlayer$ISlideShowPlayerResponseListener;->onStartResponseReceived(ILcom/samsung/android/allshare/ERROR;)V

    goto :goto_0

    .line 91
    .end local v6    # "interval":I
    :cond_3
    const-string v13, "com.sec.android.allshare.action.ACTION_SLIDESHOW_PLAYER_STOP"

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 93
    iget-object v13, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl$1;->this$0:Lcom/samsung/android/allshare/SlideShowPlayerImpl;

    # getter for: Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mResponseListener:Lcom/samsung/android/allshare/media/SlideShowPlayer$ISlideShowPlayerResponseListener;
    invoke-static {v13}, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->access$000(Lcom/samsung/android/allshare/SlideShowPlayerImpl;)Lcom/samsung/android/allshare/media/SlideShowPlayer$ISlideShowPlayerResponseListener;

    move-result-object v13

    if-eqz v13, :cond_0

    .line 94
    iget-object v13, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl$1;->this$0:Lcom/samsung/android/allshare/SlideShowPlayerImpl;

    # getter for: Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mResponseListener:Lcom/samsung/android/allshare/media/SlideShowPlayer$ISlideShowPlayerResponseListener;
    invoke-static {v13}, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->access$000(Lcom/samsung/android/allshare/SlideShowPlayerImpl;)Lcom/samsung/android/allshare/media/SlideShowPlayer$ISlideShowPlayerResponseListener;

    move-result-object v13

    invoke-interface {v13, v3}, Lcom/samsung/android/allshare/media/SlideShowPlayer$ISlideShowPlayerResponseListener;->onStopResponseReceived(Lcom/samsung/android/allshare/ERROR;)V

    goto :goto_0

    .line 96
    :cond_4
    const-string v13, "com.sec.android.allshare.action.ACTION_SLIDESHOW_PLAYER_SETLIST"

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_6

    .line 98
    const-string v13, "BUNDLE_STRING_SLIDESHOW_ALBUM_TITLE"

    invoke-virtual {v11, v13}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 100
    .local v1, "albumTitle":Ljava/lang/String;
    const-string v13, "BUNDLE_PARCELABLE_ARRAYLIST_SLIDESHOW_IMAGE_CONTENT_URI"

    invoke-virtual {v11, v13}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v12

    .line 103
    .local v12, "slideUriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 105
    .local v9, "itemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Item;>;"
    if-eqz v12, :cond_5

    .line 106
    invoke-virtual {v12}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/os/Bundle;

    .line 107
    .local v8, "itemBundle":Landroid/os/Bundle;
    invoke-static {v8}, Lcom/samsung/android/allshare/ItemCreator;->fromBundle(Landroid/os/Bundle;)Lcom/samsung/android/allshare/Item;

    move-result-object v7

    .line 108
    .local v7, "item":Lcom/samsung/android/allshare/Item;
    invoke-virtual {v9, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 112
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v7    # "item":Lcom/samsung/android/allshare/Item;
    .end local v8    # "itemBundle":Landroid/os/Bundle;
    :cond_5
    iget-object v13, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl$1;->this$0:Lcom/samsung/android/allshare/SlideShowPlayerImpl;

    # getter for: Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mResponseListener:Lcom/samsung/android/allshare/media/SlideShowPlayer$ISlideShowPlayerResponseListener;
    invoke-static {v13}, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->access$000(Lcom/samsung/android/allshare/SlideShowPlayerImpl;)Lcom/samsung/android/allshare/media/SlideShowPlayer$ISlideShowPlayerResponseListener;

    move-result-object v13

    if-eqz v13, :cond_0

    .line 113
    iget-object v13, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl$1;->this$0:Lcom/samsung/android/allshare/SlideShowPlayerImpl;

    # getter for: Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mResponseListener:Lcom/samsung/android/allshare/media/SlideShowPlayer$ISlideShowPlayerResponseListener;
    invoke-static {v13}, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->access$000(Lcom/samsung/android/allshare/SlideShowPlayerImpl;)Lcom/samsung/android/allshare/media/SlideShowPlayer$ISlideShowPlayerResponseListener;

    move-result-object v13

    invoke-interface {v13, v1, v9, v3}, Lcom/samsung/android/allshare/media/SlideShowPlayer$ISlideShowPlayerResponseListener;->onSetListResponseReceived(Ljava/lang/String;Ljava/util/ArrayList;Lcom/samsung/android/allshare/ERROR;)V

    goto/16 :goto_0

    .line 115
    .end local v1    # "albumTitle":Ljava/lang/String;
    .end local v9    # "itemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Item;>;"
    .end local v12    # "slideUriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    :cond_6
    const-string v13, "com.sec.android.allshare.action.ACTION_SLIDESHOW_PLAYER_SETBGMLIST"

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_8

    .line 117
    const-string v13, "BUNDLE_PARCELABLE_ARRAYLIST_SLIDESHOW_AUDIO_CONTENT_URI"

    invoke-virtual {v11, v13}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 120
    .local v2, "bgmUriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 122
    .restart local v9    # "itemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Item;>;"
    if-eqz v2, :cond_7

    .line 123
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .restart local v5    # "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_7

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/os/Bundle;

    .line 124
    .restart local v8    # "itemBundle":Landroid/os/Bundle;
    invoke-static {v8}, Lcom/samsung/android/allshare/ItemCreator;->fromBundle(Landroid/os/Bundle;)Lcom/samsung/android/allshare/Item;

    move-result-object v7

    .line 125
    .restart local v7    # "item":Lcom/samsung/android/allshare/Item;
    invoke-virtual {v9, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 129
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v7    # "item":Lcom/samsung/android/allshare/Item;
    .end local v8    # "itemBundle":Landroid/os/Bundle;
    :cond_7
    iget-object v13, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl$1;->this$0:Lcom/samsung/android/allshare/SlideShowPlayerImpl;

    # getter for: Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mResponseListener:Lcom/samsung/android/allshare/media/SlideShowPlayer$ISlideShowPlayerResponseListener;
    invoke-static {v13}, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->access$000(Lcom/samsung/android/allshare/SlideShowPlayerImpl;)Lcom/samsung/android/allshare/media/SlideShowPlayer$ISlideShowPlayerResponseListener;

    move-result-object v13

    if-eqz v13, :cond_0

    .line 130
    iget-object v13, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl$1;->this$0:Lcom/samsung/android/allshare/SlideShowPlayerImpl;

    # getter for: Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mResponseListener:Lcom/samsung/android/allshare/media/SlideShowPlayer$ISlideShowPlayerResponseListener;
    invoke-static {v13}, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->access$000(Lcom/samsung/android/allshare/SlideShowPlayerImpl;)Lcom/samsung/android/allshare/media/SlideShowPlayer$ISlideShowPlayerResponseListener;

    move-result-object v13

    invoke-interface {v13, v9, v3}, Lcom/samsung/android/allshare/media/SlideShowPlayer$ISlideShowPlayerResponseListener;->onSetBGMListResponseReceived(Ljava/util/ArrayList;Lcom/samsung/android/allshare/ERROR;)V

    goto/16 :goto_0

    .line 132
    .end local v2    # "bgmUriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    .end local v9    # "itemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Item;>;"
    :cond_8
    const-string v13, "com.sec.android.allshare.action.ACTION_SLIDESHOW_PLAYER_SETBGMVOLUME"

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 134
    const-string v13, "BUNDLE_INT_SLIDESHOW_BGM_VOLUME"

    invoke-virtual {v11, v13}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v10

    .line 136
    .local v10, "level":I
    iget-object v13, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl$1;->this$0:Lcom/samsung/android/allshare/SlideShowPlayerImpl;

    # getter for: Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mResponseListener:Lcom/samsung/android/allshare/media/SlideShowPlayer$ISlideShowPlayerResponseListener;
    invoke-static {v13}, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->access$000(Lcom/samsung/android/allshare/SlideShowPlayerImpl;)Lcom/samsung/android/allshare/media/SlideShowPlayer$ISlideShowPlayerResponseListener;

    move-result-object v13

    if-eqz v13, :cond_0

    .line 137
    iget-object v13, p0, Lcom/samsung/android/allshare/SlideShowPlayerImpl$1;->this$0:Lcom/samsung/android/allshare/SlideShowPlayerImpl;

    # getter for: Lcom/samsung/android/allshare/SlideShowPlayerImpl;->mResponseListener:Lcom/samsung/android/allshare/media/SlideShowPlayer$ISlideShowPlayerResponseListener;
    invoke-static {v13}, Lcom/samsung/android/allshare/SlideShowPlayerImpl;->access$000(Lcom/samsung/android/allshare/SlideShowPlayerImpl;)Lcom/samsung/android/allshare/media/SlideShowPlayer$ISlideShowPlayerResponseListener;

    move-result-object v13

    invoke-interface {v13, v10, v3}, Lcom/samsung/android/allshare/media/SlideShowPlayer$ISlideShowPlayerResponseListener;->onSetBGMVolumeResponseReceived(ILcom/samsung/android/allshare/ERROR;)V

    goto/16 :goto_0
.end method
